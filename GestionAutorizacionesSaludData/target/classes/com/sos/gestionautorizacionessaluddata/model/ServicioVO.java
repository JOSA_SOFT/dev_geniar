package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;

/**
 * Class ServicioVO
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class ServicioVO implements Serializable{
	private static final long serialVersionUID = 3751945041147664883L;
	/** Nombre del servicio */
	private String name;
	/** Label del servicio, ser� el label que tendr� la opci�n de men� */
	private String label;
	/** Se usa como regla de navegaci�n y debe configurarse en el faces-config */
	private String action;
	/** Atributo boolean que se usa como permiso si puede usar la regla de navegacion  */
	private boolean deshabilitado;
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLabel() {
		return label;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getAction() {
		return action;
	}
	public boolean isDeshabilitado() {
		return deshabilitado;
	}
	public void setDeshabilitado(boolean deshabilitado) {
		this.deshabilitado = deshabilitado;
	}


}
