package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;


/**
 * Class SolicitudVO
 * Clase VO de la solicitud creada
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/02/2016
 *
 */
public class SolicitudVO implements Serializable{
	private static final long serialVersionUID = 1437892392730214152L;

	/** Consecutivo de la solicitud */
	private Integer consecutivoSolicitud;
	
	/** numero de la solicitud */
	private String  numeroSolicitudSOS;
	
	/** informacion de la solicitud que se guarda */
	private RegistrarSolicitudVO registrarSolicitudVO;		
	
	/** informacion del estado de la solicitud */
	private EstadoVO estadoVO;	

	/** informacion de error en el servicio */
	private ServiceErrorVO serviceErrorVO;	
	
	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	public String getNumeroSolicitudSOS() {
		return numeroSolicitudSOS;
	}

	public void setNumeroSolicitudSOS(String numeroSolicitudSOS) {
		this.numeroSolicitudSOS = numeroSolicitudSOS;
	}

	public RegistrarSolicitudVO getRegistrarSolicitudVO() {
		return registrarSolicitudVO;
	}

	public void setRegistrarSolicitudVO(RegistrarSolicitudVO registrarSolicitudVO) {
		this.registrarSolicitudVO = registrarSolicitudVO;
	}	
	
	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
	
	public EstadoVO getEstadoVO() {
		return estadoVO;
	}

	public void setEstadoVO(EstadoVO estadoVO) {
		this.estadoVO = estadoVO;
	}
}
