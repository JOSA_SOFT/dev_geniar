package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;

/**
 * Class AdvertenciaVO
 * Clase VO de los mensajes de advertencia
 * @author ing. Victor Hugo Gil Ramos
 * @version 07/04/2016
 *
 */
public class AdvertenciaVO implements Serializable{
	
	private static final long serialVersionUID = 4537150790717840359L;

	/** Codigo de advertencia del servicio */
	private String codigoAdvertencia;
	
	/** Mensaje de advertencia del servicio */
	private String mensajeAdvertencia;
	
	public String getCodigoAdvertencia() {
		return codigoAdvertencia;
	}

	public void setCodigoAdvertencia(String codigoAdvertencia) {
		this.codigoAdvertencia = codigoAdvertencia;
	}

	public String getMensajeAdvertencia() {
		return mensajeAdvertencia;
	}

	public void setMensajeAdvertencia(String mensajeAdvertencia) {
		this.mensajeAdvertencia = mensajeAdvertencia;
	}

}
