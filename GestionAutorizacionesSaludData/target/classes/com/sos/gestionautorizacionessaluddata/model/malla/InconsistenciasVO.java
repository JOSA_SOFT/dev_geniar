package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;


/**
 * Class InconsistenciasVO
 * Clase VO de las inconsistencias de malla
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class InconsistenciasVO implements Serializable {
	
	private static final long serialVersionUID = 3474481976772287249L;

	/** informacion de error en el servicio */
	private ServiceErrorVO serviceErrorVO;	
	
	/** informacion de la solicitud */
	private SolicitudVO solicitudVO;
	
	/** informacion de la prestacion */
	private List<PrestacionDTO> listaPrestaciones; 

	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
	
	public SolicitudVO getSolicitudVO() {
		return solicitudVO;
	}

	public void setSolicitudVO(SolicitudVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	/**
	 * @return the listaPrestaciones
	 */
	public List<PrestacionDTO> getListaPrestaciones() {
		return listaPrestaciones;
	}

	/**
	 * @param listaPrestaciones the listaPrestaciones to set
	 */
	public void setListaPrestaciones(List<PrestacionDTO> listaPrestaciones) {
		this.listaPrestaciones = listaPrestaciones;
	}

}
