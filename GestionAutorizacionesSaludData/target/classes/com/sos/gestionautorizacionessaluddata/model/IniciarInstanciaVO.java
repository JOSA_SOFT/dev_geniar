package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;

/**
 * Class IniciarInstanciaVO
 * Clase VO para iniciar la instancia VO
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/02/2016
 *
 */
public class IniciarInstanciaVO implements Serializable {
	private static final long serialVersionUID = -2095987254395296524L;

	/** informacion del codigo del resultado */
	private String resultadoCodigo;
	
	/** informacion del mensaje del resultado */
	private String resultadoMensaje;
	
	/** informacion de error en el servicio */
	private ServiceErrorVO serviceErrorVO;	
	
	public String getResultadoCodigo() {
		return resultadoCodigo;
	}

	public void setResultadoCodigo(String resultadoCodigo) {
		this.resultadoCodigo = resultadoCodigo;
	}

	public String getResultadoMensaje() {
		return resultadoMensaje;
	}

	public void setResultadoMensaje(String resultadoMensaje) {
		this.resultadoMensaje = resultadoMensaje;
	}
	
	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
}



