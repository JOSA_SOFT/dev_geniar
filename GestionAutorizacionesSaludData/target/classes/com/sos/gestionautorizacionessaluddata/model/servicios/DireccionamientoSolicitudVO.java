package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;


/**
 * Class DireccionamientoSolicitudVO
 * Clase que permite para almacenar la informacion del direccionamiento
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/03/2016
 *
 */

public class DireccionamientoSolicitudVO implements Serializable{
	
	private static final long serialVersionUID = 3802870566761482305L;

	/* Consecutivo de la solicitud  */
	private Integer consecutivoSolicitud;	

	/*Identifica el numero de radicado*/
	private String numeroRadicacionSolicitud;
	
	/*Identifica el numero de solictud prestador*/
	private String numeroSolicitudPrestador;  
	
	/*Identifica el listado de las prestaciones asociados al direccionamiento */
	private List<PrestacionDireccionamientoVO> lPrestacionDireccionamientoVO;
	
	/*Identifica el objeto con los errores o advertencias del servicio */
	private ServiceErrorVO serviceErrorVO;	


	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	public String getNumeroRadicacionSolicitud() {
		return numeroRadicacionSolicitud;
	}

	public void setNumeroRadicacionSolicitud(String numeroRadicacionSolicitud) {
		this.numeroRadicacionSolicitud = numeroRadicacionSolicitud;
	}

	public String getNumeroSolicitudPrestador() {
		return numeroSolicitudPrestador;
	}

	public void setNumeroSolicitudPrestador(String numeroSolicitudPrestador) {
		this.numeroSolicitudPrestador = numeroSolicitudPrestador;
	}

	public List<PrestacionDireccionamientoVO> getlPrestacionDireccionamientoVO() {
		return lPrestacionDireccionamientoVO;
	}

	public void setlPrestacionDireccionamientoVO(
			List<PrestacionDireccionamientoVO> lPrestacionDireccionamientoVO) {
		this.lPrestacionDireccionamientoVO = lPrestacionDireccionamientoVO;
	}
	
	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
}
	
