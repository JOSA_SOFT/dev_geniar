package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;

/**
 * Class LiquidacionSolicitudVO
 * Clase que permite para almacenar la informacion de la liquidacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 *
 */
public class LiquidacionSolicitudVO implements Serializable{
	private static final long serialVersionUID = -4174685920337442901L;

	/* Consecutivo de la solicitud  */
	private Integer consecutivoSolicitud;
	
	/*Identifica el resultado de la liquidacion*/
	private int resultadoLiquidacion;
	
	/*Identifica el mensaje del resultado de liquidacion*/
	private String mensajeResultadoLiquidacion;
	
	/*Identifica el valor total de liquidacion */
	private Integer valorTotalLiquidacion;
	
	private List<ConceptosLiquidacionVO> lConceptosLiquidacionVO;

	/*Identifica el objeto con los errores o advertencias del servicio */
	private ServiceErrorVO serviceErrorVO;
	
	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	public int getResultadoLiquidacion() {
		return resultadoLiquidacion;
	}

	public void setResultadoLiquidacion(int resultadoLiquidacion) {
		this.resultadoLiquidacion = resultadoLiquidacion;
	}
		
	public String getMensajeResultadoLiquidacion() {
		return mensajeResultadoLiquidacion;
	}

	public void setMensajeResultadoLiquidacion(String mensajeResultadoLiquidacion) {
		this.mensajeResultadoLiquidacion = mensajeResultadoLiquidacion;
	}

	public Integer getValorTotalLiquidacion() {
		return valorTotalLiquidacion;
	}

	public void setValorTotalLiquidacion(Integer valorTotalLiquidacion) {
		this.valorTotalLiquidacion = valorTotalLiquidacion;
	}

	public List<ConceptosLiquidacionVO> getlConceptosLiquidacionVO() {
		return lConceptosLiquidacionVO;
	}

	public void setlConceptosLiquidacionVO(
			List<ConceptosLiquidacionVO> lConceptosLiquidacionVO) {
		this.lConceptosLiquidacionVO = lConceptosLiquidacionVO;
	}

	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
}
