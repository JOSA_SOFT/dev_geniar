package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;

/**
 * Class ConceptosLiquidacionVO 
 * Clase que permite para almacenar la informacion de los conceptos liquidados
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 */

public class ConceptosLiquidacionVO implements Serializable{
	private static final long serialVersionUID = -7511272046752222383L;

	/*Identifica el codigo del concepto */
	private Integer codigoConcepto;
	
	/*Identifica el nombre del concepto */
	private String nombreConcepto;
	
	/*Identifica el valor de liquidacion del concepto */
	private Integer valorLiquidacionConcepto;
	
	/*Identifica el resultado de la liquidacion del concepto */
	private String resultadoLiquidacionConcepto;
	
	public Integer getCodigoConcepto() {
		return codigoConcepto;
	}

	public void setCodigoConcepto(Integer codigoConcepto) {
		this.codigoConcepto = codigoConcepto;
	}

	public String getNombreConcepto() {
		return nombreConcepto;
	}

	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}

	public Integer getValorLiquidacionConcepto() {
		return valorLiquidacionConcepto;
	}

	public void setValorLiquidacionConcepto(Integer valorLiquidacionConcepto) {
		this.valorLiquidacionConcepto = valorLiquidacionConcepto;
	}

	public String getResultadoLiquidacion() {
		return resultadoLiquidacionConcepto;
	}

	public void setResultadoLiquidacion(String resultadoLiquidacion) {
		this.resultadoLiquidacionConcepto = resultadoLiquidacion;
	}
}
