package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;


/**
 * Class FechaEntregaSolicitudVO
 * Clase que permite para almacenar la informacion de la fecha de entrega
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/03/2016
 *
 */
public class FechaEntregaSolicitudVO implements Serializable{
	private static final long serialVersionUID = -1444940463063559877L;

	/* Consecutivo de la solicitud  */
	private Integer consecutivoSolicitud;	
	
	/*Identifica el numero de radicado*/
	private String numeroRadicacionSolicitud;
	
	/*Identifica el tipo de plan*/
	private String tipoPlan;
		
	/* Consecutivo del tipo de plan  */
	private String consecutivoTipoPlan;	
	
	/*Identifica el listado de las prestaciones asociados a la fecha de entrega */
	private List<PrestacionFechaEntregaVO> lPrestacionFechaEntregaVO;
	
	/*Identifica el objeto con los errores o advertencias del servicio */
	private ServiceErrorVO serviceErrorVO;	

	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	public String getNumeroRadicacionSolicitud() {
		return numeroRadicacionSolicitud;
	}

	public void setNumeroRadicacionSolicitud(String numeroRadicacionSolicitud) {
		this.numeroRadicacionSolicitud = numeroRadicacionSolicitud;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public String getConsecutivoTipoPlan() {
		return consecutivoTipoPlan;
	}

	public void setConsecutivoTipoPlan(String consecutivoTipoPlan) {
		this.consecutivoTipoPlan = consecutivoTipoPlan;
	}

	public List<PrestacionFechaEntregaVO> getlPrestacionFechaEntregaVO() {
		return lPrestacionFechaEntregaVO;
	}

	public void setlPrestacionFechaEntregaVO(
			List<PrestacionFechaEntregaVO> lPrestacionFechaEntregaVO) {
		this.lPrestacionFechaEntregaVO = lPrestacionFechaEntregaVO;
	}
	
	public ServiceErrorVO getServiceErrorVO() {
		return serviceErrorVO;
	}

	public void setServiceErrorVO(ServiceErrorVO serviceErrorVO) {
		this.serviceErrorVO = serviceErrorVO;
	}
}
