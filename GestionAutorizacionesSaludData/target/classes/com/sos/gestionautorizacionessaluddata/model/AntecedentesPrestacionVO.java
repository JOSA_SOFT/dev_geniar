package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;
import java.util.Date;


/**
 * Clase VO para los riesgos del afiliado
 * @author Julian Hernandez Gomez
 * @version 10/03/2016
 *
 */
public class AntecedentesPrestacionVO implements Serializable{
	private static final long serialVersionUID = 3888047345959061588L;

	/** **/
	private Date fecha;
	
	/** **/
	private String descEstado;
	
	/** **/
	private String descPrestador;
	
	/** **/
	private Integer cantidad;

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the descEstado
	 */
	public String getDescEstado() {
		return descEstado;
	}

	/**
	 * @param descEstado the descEstado to set
	 */
	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	/**
	 * @return the descPrestador
	 */
	public String getDescPrestador() {
		return descPrestador;
	}

	/**
	 * @param descPrestador the descPrestador to set
	 */
	public void setDescPrestador(String descPrestador) {
		this.descPrestador = descPrestador;
	}

	/**
	 * @return the cantidad
	 */
	public Integer getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
