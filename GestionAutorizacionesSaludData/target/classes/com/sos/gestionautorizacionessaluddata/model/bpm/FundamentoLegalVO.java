package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class FundamentoLegalDTO
 * Clase DTO para almacenar la consulta de los Fundamentos legales
 * @author ing. Rafael Cano
 * @version 02/03/2016
 *
 */

public class FundamentoLegalVO implements Serializable {
	private static final long serialVersionUID = 1048687013281655753L;
	private String fundamentoLegal;
	private String codigoFudamento;
	private Boolean estadoFudamento;
	
	public String getFundamentoLegal() {
		return fundamentoLegal;
	}
	public void setFundamentoLegal(String fundamentoLegal) {
		this.fundamentoLegal = fundamentoLegal;
	}
	public String getCodigoFudamento() {
		return codigoFudamento;
	}
	public void setCodigoFudamento(String codigoFudamento) {
		this.codigoFudamento = codigoFudamento;
	}
	public Boolean getEstadoFudamento() {
		return estadoFudamento;
	}
	public void setEstadoFudamento(Boolean estadoFudamento) {
		this.estadoFudamento = estadoFudamento;
	}

}
	
