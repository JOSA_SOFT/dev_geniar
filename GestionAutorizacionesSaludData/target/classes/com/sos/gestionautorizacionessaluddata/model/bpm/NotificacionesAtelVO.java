package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class NotificacionesAtelDTO
 * Clase DTO para almacenar la consulta de las notificaciones ATEL
 * @author ing. Rafael Cano
 * @version 15/02/2016
 *
 */

public class NotificacionesAtelVO implements Serializable {
	private static final long serialVersionUID = -4050488875749752978L;
	private String consTipo; 
	private String tipo;
	private String consDiagnostico; 
	private String diagnostico;
	private String consEstado;
	private String estado;
	private String numeroNotificacion; 
	private String atel;
	private String tipoIdentificacion; 
	private String identificacion;
	private String numeroUnicoIdentif; 
	private String consecutivoNotif;

	
	public String getConsTipo() {
		return consTipo;
	}
	public void setConsTipo(String consTipo) {
		this.consTipo = consTipo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getConsDiagnostico() {
		return consDiagnostico;
	}
	public void setConsDiagnostico(String consDiagnostico) {
		this.consDiagnostico = consDiagnostico;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getConsEstado() {
		return consEstado;
	}
	public void setConsEstado(String consEstado) {
		this.consEstado = consEstado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNumeroNotificacion() {
		return numeroNotificacion;
	}
	public void setNumeroNotificacion(String numeroNotificacion) {
		this.numeroNotificacion = numeroNotificacion;
	}
	public String getAtel() {
		return atel;
	}
	public void setAtel(String atel) {
		this.atel = atel;
	}
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getNumeroUnicoIdentif() {
		return numeroUnicoIdentif;
	}
	public void setNumeroUnicoIdentif(String numeroUnicoIdentif) {
		this.numeroUnicoIdentif = numeroUnicoIdentif;
	}
	public String getConsecutivoNotif() {
		return consecutivoNotif;
	}
	public void setConsecutivoNotif(String consecutivoNotif) {
		this.consecutivoNotif = consecutivoNotif;
	}

}