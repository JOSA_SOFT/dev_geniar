package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Class DireccionamientoDTO
 * Clase DTO para almacenar la consulta de direccionamiento
 * @author ing. Rafael Cano
 * @version 15/02/2016
 *
 */

public class DireccionamientoVO implements Serializable{
	private static final long serialVersionUID = 4797575029974469219L;
	private Integer consTipoIdentificacion;
	private String codigoTipoIdentificacion;
	private String numeroIdentificacion;
	private String codigoInterno;
	private String sucursal;
	private Date fechaEsperado;
	
	private String justificacion;
	private List<String> motivos = new ArrayList<String>();
	private String consecutivoPrestador;
	/**
	 * @return the consTipoIdentificacion
	 */
	public Integer getConsTipoIdentificacion() {
		return consTipoIdentificacion;
	}
	/**
	 * @param consTipoIdentificacion the consTipoIdentificacion to set
	 */
	public void setConsTipoIdentificacion(Integer consTipoIdentificacion) {
		this.consTipoIdentificacion = consTipoIdentificacion;
	}
	/**
	 * @return the codigoTipoIdentificacion
	 */
	public String getCodigoTipoIdentificacion() {
		return codigoTipoIdentificacion;
	}
	/**
	 * @param codigoTipoIdentificacion the codigoTipoIdentificacion to set
	 */
	public void setCodigoTipoIdentificacion(String codigoTipoIdentificacion) {
		this.codigoTipoIdentificacion = codigoTipoIdentificacion;
	}
	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	/**
	 * @param numeroIdentificacion the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	/**
	 * @return the codigoInterno
	 */
	public String getCodigoInterno() {
		return codigoInterno;
	}
	/**
	 * @param codigoInterno the codigoInterno to set
	 */
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}
	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	/**
	 * @return the fechaEsperado
	 */
	public Date getFechaEsperado() {
		return fechaEsperado;
	}
	/**
	 * @param fechaEsperado the fechaEsperado to set
	 */
	public void setFechaEsperado(Date fechaEsperado) {
		this.fechaEsperado = fechaEsperado;
	}
	/**
	 * @return the justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}
	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}

	/**
	 * @return the consecutivoPrestador
	 */
	public String getConsecutivoPrestador() {
		return consecutivoPrestador;
	}
	/**
	 * @param consecutivoPrestador the consecutivoPrestador to set
	 */
	public void setConsecutivoPrestador(String consecutivoPrestador) {
		this.consecutivoPrestador = consecutivoPrestador;
	}
	/**
	 * @return the motivos
	 */
	public List<String> getMotivos() {
		return motivos;
	}
	/**
	 * @param motivos the motivos to set
	 */
	public void setMotivos(List<String> motivos) {
		this.motivos = motivos;
	}

	

}
	
