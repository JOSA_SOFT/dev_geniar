package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class GrupoAuditorVO
 * Clase DTO para almacenar los grupos auditor
 * @author Julian Hernandez
 * @version 01/03/2016
 *
 */

public class GrupoAuditorVO implements Serializable {
	private static final long serialVersionUID = -3543550431554814583L;
	private Integer codigoGrupoAuditor;
	private String descGrupoAuditor;
	/**
	 * @return the codigoGrupoAuditor
	 */
	public Integer getCodigoGrupoAuditor() {
		return codigoGrupoAuditor;
	}
	/**
	 * @param codigoGrupoAuditor the codigoGrupoAuditor to set
	 */
	public void setCodigoGrupoAuditor(Integer codigoGrupoAuditor) {
		this.codigoGrupoAuditor = codigoGrupoAuditor;
	}
	/**
	 * @return the descGrupoAuditor
	 */
	public String getDescGrupoAuditor() {
		return descGrupoAuditor;
	}
	/**
	 * @param descGrupoAuditor the descGrupoAuditor to set
	 */
	public void setDescGrupoAuditor(String descGrupoAuditor) {
		this.descGrupoAuditor = descGrupoAuditor;
	}
	
	
}
	
