package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class CausasDTO
 * Clase DTO para almacenar la consulta de las causas
 * @author ing. Rafael Cano
 * @version 01/03/2016
 *
 */

public class CausasVO implements Serializable {
	private static final long serialVersionUID = -5892721887050395562L;
	private String causa;
	private String codigocausa;
	private Boolean estadoCausa;
	
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	public String getCodigocausa() {
		return codigocausa;
	}
	public void setCodigocausa(String codigocausa) {
		this.codigocausa = codigocausa;
	}
	public Boolean getEstadoCausa() {
		return estadoCausa;
	}
	public void setEstadoCausa(Boolean estadoCausa) {
		this.estadoCausa = estadoCausa;
	}
	
}
	
