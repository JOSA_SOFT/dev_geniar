package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.MedioContactoVO;

/**
 * Class RegistrarSolicitudVO
 * Clase VO del registro de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class RegistrarSolicitudVO implements Serializable{
	private static final long serialVersionUID = 4581379277877343412L;

	/*Identifica el objeto con la informacion del afiliado*/
	private AfiliadoVO afiliadoVO;
	
	/*Identifica el codigo de la EPS*/
	private String codigoEPS;
	
	/*Identifica las observaciones adicionales de la solicitud*/
	private String observaciones;
	
	/*Identifica el objeto con la informacion del recien nacido*/
	private RecienNacidoVO recienNacidoVO;
	
	/*Identifica el objeto con la informacion de la atencion*/
    private AtencionVO atencionVO;
	
    /*Identifica si la solicitud es hospitalaria */
    private String marcaHospitalizacion;
    
    /*Identifica el objeto con la informacion de la hospitalizacion*/
    private HospitalizacionVO hospitalizacionVO;
    
    /*Identifica el objeto con la informacion de los diagnosticos*/
    private List<DiagnosticoDTO> lDiagnosticosSolicitud;
    
    /*Identifica el objeto con la informacion del medico*/
    private MedicoVO medicoVO;
    
    /*Identifica el objeto con la informacion del prestador*/
    private PrestadorVO prestadorVO;
    
    /*Identifica el objeto con la informacion de las prestaciones*/
    private List<PrestacionDTO> lPrestacionesSolicitud;   
	
	/*Identifica el objeto con la informacion del medio de contacto*/
	private MedioContactoVO medioContactoVO;
	
	/*Identifica el objeto con la informacion del recobro del diagnostico principal*/
	private ContingenciaRecobroVO recobroVO;
	
	/*Identifica el objeto con la informacion de los soportes*/
	private List<SoporteVO> lSoporteVO;
	
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public String getCodigoEPS() {
		return codigoEPS;
	}

	public void setCodigoEPS(String codigoEPS) {
		this.codigoEPS = codigoEPS;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public RecienNacidoVO getRecienNacidoVO() {
		return recienNacidoVO;
	}

	public void setRecienNacidoVO(RecienNacidoVO recienNacidoVO) {
		this.recienNacidoVO = recienNacidoVO;
	}

	public AtencionVO getAtencionVO() {
		return atencionVO;
	}

	public void setAtencionVO(AtencionVO atencionVO) {
		this.atencionVO = atencionVO;
	}

	public String getMarcaHospitalizacion() {
		return marcaHospitalizacion;
	}

	public void setMarcaHospitalizacion(String marcaHospitalizacion) {
		this.marcaHospitalizacion = marcaHospitalizacion;
	}

	public HospitalizacionVO getHospitalizacionVO() {
		return hospitalizacionVO;
	}

	public void setHospitalizacionVO(HospitalizacionVO hospitalizacionVO) {
		this.hospitalizacionVO = hospitalizacionVO;
	}

	public List<DiagnosticoDTO> getlDiagnosticosSolicitud() {
		return lDiagnosticosSolicitud;
	}

	public void setlDiagnosticosSolicitud(
			List<DiagnosticoDTO> lDiagnosticosSolicitud) {
		this.lDiagnosticosSolicitud = lDiagnosticosSolicitud;
	}

	public MedicoVO getMedicoVO() {
		return medicoVO;
	}

	public void setMedicoVO(MedicoVO medicoVO) {
		this.medicoVO = medicoVO;
	}

	public PrestadorVO getPrestadorVO() {
		return prestadorVO;
	}

	public void setPrestadorVO(PrestadorVO prestadorVO) {
		this.prestadorVO = prestadorVO;
	}

	public List<PrestacionDTO> getlPrestacionesSolicitud() {
		return lPrestacionesSolicitud;
	}

	public void setlPrestacionesSolicitud(List<PrestacionDTO> lPrestacionesSolicitud) {
		this.lPrestacionesSolicitud = lPrestacionesSolicitud;
	}	

	public MedioContactoVO getMedioContactoVO() {
		return medioContactoVO;
	}

	public void setMedioContactoVO(MedioContactoVO medioContactoVO) {
		this.medioContactoVO = medioContactoVO;
	}

	public ContingenciaRecobroVO getRecobroVO() {
		return recobroVO;
	}

	public void setRecobroVO(ContingenciaRecobroVO recobroVO) {
		this.recobroVO = recobroVO;
	}

	public List<SoporteVO> getlSoporteVO() {
		return lSoporteVO;
	}

	public void setlSoporteVO(List<SoporteVO> lSoporteVO) {
		this.lSoporteVO = lSoporteVO;
	}
}
