package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;

/**
 * Class MedicamentosVO
 * Clase VO de la concetracion de los medicamentos
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/01/2016
 *
 */
public class MedicamentosVO implements Serializable{
	private static final long serialVersionUID = -7033255963038073526L;

	/*Identifica el consecutivo del medicamento*/
	private Integer consecutivoCodificacionMedicamento;

	/*Identifica el codigo del medicamento*/
	private String codigoCodificacionMedicamento;
	
	/*Identifica la descricion del medicamento*/
	private String descripcionCodificacionMedicamento;
	
	/*Identifica el codigo del medicamento del prestador*/
	private String codigoMedicamentoPrestador;

	/*Identifica la descricion del medicamento del prestador*/
	private String descripcionMedicamentoPrestador;
	
	/*Identifica el registro invima del medicamento*/
	private String registroInvima;
	
	/*Identifica el codigo de la forma farmaceutica del medicamento*/
	private String codigoFormaFarmaceutica;
	
	/*Identifica el codigo de la forma farmaceutica del medicamento*/
	private String descripcionFormaFarmaceutica;
	
	/*Identifica el codigo de la via de administracion del medicamento*/
	private String codigoViaAdministracion;
	
	/*Identifica la descripcion de la via de administracion del medicamento*/
	private String descripcionViaAdministracion;
	
	/*Identifica el codigo del grupo terapeutico del medicamento*/
	private String codigoSubgrupoTerapeutico;
	
	/*Identifica la descripcion de la clase formacologico del medicamento*/
	private String descripcionClaseFarmacologico;
	
	/*Identifica el consecutivo de la clase lateridad*/
	private Integer consecutivoLaterialidad;
	
	/*Identifica el consecutivo de la clase via acceso*/
	private Integer consecutivoViaAcceso;
	
	/*Identifica la cantidad de dosis a recetar*/
	private Integer cantidadMedicamento;
	
	/*Identifica la cantidad de dosis a recetar*/
	private BigDecimal cantidadDosis;
	
	/*Identifica la cantidad de horas a la que se debe tomar el medicamento*/
	private BigDecimal horaFrecuencia;
	
	private DosisVO dosisVO;
	
	private FrecuenciaVO frecuenciaVO;
	
	/*Identifica si el medicamento es no pos*/
	private String indicadorNoPOS;
	
	/*Identifica la concentracion del medicamento*/
	private ConcentracionVO concentracionVO;
	
	/*Identifica la presentacion del medicamento*/
	private PresentacionVO presentacionVO;
	
	/*Identifica el consecutivo del item de presupuesto*/
	private int consecutivoItemPresupuesto;
	
	public MedicamentosVO() {
		super();
	}
	
	public MedicamentosVO(String codigoCodificacionMedicamento, String descripcionCodificacionMedicamento) {
		super();
		this.codigoCodificacionMedicamento = codigoCodificacionMedicamento;
		this.descripcionCodificacionMedicamento = descripcionCodificacionMedicamento;
	}
	
	public Integer getConsecutivoCodificacionMedicamento() {
		return consecutivoCodificacionMedicamento;
	}

	public void setConsecutivoCodificacionMedicamento(
			Integer consecutivoCodificacionMedicamento) {
		this.consecutivoCodificacionMedicamento = consecutivoCodificacionMedicamento;
	}

	public String getCodigoCodificacionMedicamento() {
		return codigoCodificacionMedicamento;
	}

	public void setCodigoCodificacionMedicamento(
			String codigoCodificacionMedicamento) {
		this.codigoCodificacionMedicamento = codigoCodificacionMedicamento;
	}

	public String getDescripcionCodificacionMedicamento() {
		return descripcionCodificacionMedicamento;
	}

	public void setDescripcionCodificacionMedicamento(
			String descripcionCodificacionMedicamento) {
		this.descripcionCodificacionMedicamento = descripcionCodificacionMedicamento;
	}

	public String getCodigoMedicamentoPrestador() {
		return codigoMedicamentoPrestador;
	}

	public void setCodigoMedicamentoPrestador(String codigoMedicamentoPrestador) {
		this.codigoMedicamentoPrestador = codigoMedicamentoPrestador;
	}

	public String getDescripcionMedicamentoPrestador() {
		return descripcionMedicamentoPrestador;
	}

	public void setDescripcionMedicamentoPrestador(
			String descripcionMedicamentoPrestador) {
		this.descripcionMedicamentoPrestador = descripcionMedicamentoPrestador;
	}

	public String getRegistroInvima() {
		return registroInvima;
	}

	public void setRegistroInvima(String registroInvima) {
		this.registroInvima = registroInvima;
	}

	public String getCodigoFormaFarmaceutica() {
		return codigoFormaFarmaceutica;
	}

	public void setCodigoFormaFarmaceutica(String codigoFormaFarmaceutica) {
		this.codigoFormaFarmaceutica = codigoFormaFarmaceutica;
	}

	public String getDescripcionFormaFarmaceutica() {
		return descripcionFormaFarmaceutica;
	}

	public void setDescripcionFormaFarmaceutica(String descripcionFormaFarmaceutica) {
		this.descripcionFormaFarmaceutica = descripcionFormaFarmaceutica;
	}

	public String getCodigoViaAdministracion() {
		return codigoViaAdministracion;
	}

	public void setCodigoViaAdministracion(String codigoViaAdministracion) {
		this.codigoViaAdministracion = codigoViaAdministracion;
	}

	public String getDescripcionViaAdministracion() {
		return descripcionViaAdministracion;
	}

	public void setDescripcionViaAdministracion(String descripcionViaAdministracion) {
		this.descripcionViaAdministracion = descripcionViaAdministracion;
	}

	public String getCodigoSubgrupoTerapeutico() {
		return codigoSubgrupoTerapeutico;
	}

	public void setCodigoSubgrupoTerapeutico(String codigoSubgrupoTerapeutico) {
		this.codigoSubgrupoTerapeutico = codigoSubgrupoTerapeutico;
	}

	public String getDescripcionClaseFarmacologico() {
		return descripcionClaseFarmacologico;
	}

	public void setDescripcionClaseFarmacologico(
			String descripcionClaseFarmacologico) {
		this.descripcionClaseFarmacologico = descripcionClaseFarmacologico;
	}

	public DosisVO getDosisVO() {
		return dosisVO;
	}

	public void setDosisVO(DosisVO dosisVO) {
		this.dosisVO = dosisVO;
	}

	/**
	 * @return the frecuenciaVO
	 */
	public FrecuenciaVO getFrecuenciaVO() {
		return frecuenciaVO;
	}

	/**
	 * @param frecuenciaVO the frecuenciaVO to set
	 */
	public void setFrecuenciaVO(FrecuenciaVO frecuenciaVO) {
		this.frecuenciaVO = frecuenciaVO;
	}

	/**
	 * @return the consecutivoLaterialidad
	 */
	public Integer getConsecutivoLaterialidad() {
		return consecutivoLaterialidad;
	}

	/**
	 * @param consecutivoLaterialidad the consecutivoLaterialidad to set
	 */
	public void setConsecutivoLaterialidad(Integer consecutivoLaterialidad) {
		this.consecutivoLaterialidad = consecutivoLaterialidad;
	}

	/**
	 * @return the consecutivoViaAcceso
	 */
	public Integer getConsecutivoViaAcceso() {
		return consecutivoViaAcceso;
	}

	/**
	 * @param consecutivoViaAcceso the consecutivoViaAcceso to set
	 */
	public void setConsecutivoViaAcceso(Integer consecutivoViaAcceso) {
		this.consecutivoViaAcceso = consecutivoViaAcceso;
	}

	/**
	 * @return the cantidadDosis
	 */
	public BigDecimal getCantidadDosis() {
		return cantidadDosis;
	}

	/**
	 * @param cantidadDosis the cantidadDosis to set
	 */
	public void setCantidadDosis(BigDecimal cantidadDosis) {
		this.cantidadDosis = cantidadDosis;
	}

	/**
	 * @return the horaFrecuencia
	 */
	public BigDecimal getHoraFrecuencia() {
		return horaFrecuencia;
	}

	/**
	 * @param horaFrecuencia the horaFrecuencia to set
	 */
	public void setHoraFrecuencia(BigDecimal horaFrecuencia) {
		this.horaFrecuencia = horaFrecuencia;
	}

	
	public String getIndicadorNoPOS() {
		return indicadorNoPOS;
	}

	public void setIndicadorNoPOS(String indicadorNoPOS) {
		this.indicadorNoPOS = indicadorNoPOS;
	}
	
	public ConcentracionVO getConcentracionVO() {
		return concentracionVO;
	}

	public void setConcentracionVO(ConcentracionVO concentracionVO) {
		this.concentracionVO = concentracionVO;
	}
	
	public PresentacionVO getPresentacionVO() {
		return presentacionVO;
	}

	public void setPresentacionVO(PresentacionVO presentacionVO) {
		this.presentacionVO = presentacionVO;
	}

	/**
	 * @return the cantidadMedicamento
	 */
	public Integer getCantidadMedicamento() {
		return cantidadMedicamento;
	}

	/**
	 * @param cantidadMedicamento the cantidadMedicamento to set
	 */
	public void setCantidadMedicamento(Integer cantidadMedicamento) {
		this.cantidadMedicamento = cantidadMedicamento;
	}
	
	public int getConsecutivoItemPresupuesto() {
		return consecutivoItemPresupuesto;
	}

	public void setConsecutivoItemPresupuesto(int consecutivoItemPresupuesto) {
		this.consecutivoItemPresupuesto = consecutivoItemPresupuesto;
	}
}
