package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;


/**
 * Class HospitalizacionVO
 * Clase VO de la hospitaliacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class HospitalizacionVO implements Serializable{
	private static final long serialVersionUID = 7780302555383910082L;

	/*Identifica el servicio de hospitalizacion */
	private ServicioHospitalizacionVO servicioHospitalizacionVO;
		
	/*Identifica la fecha de ingreso de la hospitalizacion */
	private Date fechaIngreso;
	
	/*Identifica la fecha de egreso de la hospitalizacion */
	private Date fechaEgreso;
	
	/*Identifica el numero de la cama */
	private String cama;
	
	/*Identifica el valor del corte cuenta */
	private String corteCuenta;
	
	/*Identifica la clase de habitacion */
	private ClaseHabitacionVO tipoHabitacionVO;	
	
	public ServicioHospitalizacionVO getServicioHospitalizacionVO() {
		return servicioHospitalizacionVO;
	}

	public void setServicioHospitalizacionVO(
			ServicioHospitalizacionVO servicioHospitalizacionVO) {
		this.servicioHospitalizacionVO = servicioHospitalizacionVO;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaEgreso() {
		return fechaEgreso;
	}

	public void setFechaEgreso(Date fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}

	public String getCama() {
		return cama;
	}

	public void setCama(String cama) {
		this.cama = cama;
	}

	public String getCorteCuenta() {
		return corteCuenta;
	}

	public void setCorteCuenta(String corteCuenta) {
		this.corteCuenta = corteCuenta;
	}

	public ClaseHabitacionVO getTipoHabitacionVO() {
		return tipoHabitacionVO;
	}

	public void setTipoHabitacionVO(ClaseHabitacionVO tipoHabitacionVO) {
		this.tipoHabitacionVO = tipoHabitacionVO;
	}
}
