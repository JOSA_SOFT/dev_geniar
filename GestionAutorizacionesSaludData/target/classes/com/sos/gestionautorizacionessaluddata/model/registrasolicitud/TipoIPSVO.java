package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class TipoIPSVO
 * Clase VO para tipos de ips
 * @author ing. Victor Hugo Gil Ramos
 * @version 07/01/2016
 *
 */
public class TipoIPSVO implements Serializable{
	private static final long serialVersionUID = -4134841740941803510L;

	/*Identifica el consecuutivo codigo del tipo ips */
	private Integer consecutivoCodigoTipoIps;
	
	/*Identifica el codigo del tipo ips */
	private String codigoTipoIps;
	
	/*Identifica la descripcion del tipo ips */
	private String descripcionTipoIps;
	
	public Integer getConsecutivoCodigoTipoIps() {
		return consecutivoCodigoTipoIps;
	}

	public void setConsecutivoCodigoTipoIps(Integer consecutivoCodigoTipoIps) {
		this.consecutivoCodigoTipoIps = consecutivoCodigoTipoIps;
	}

	public String getCodigoTipoIps() {
		return codigoTipoIps;
	}

	public void setCodigoTipoIps(String codigoTipoIps) {
		this.codigoTipoIps = codigoTipoIps;
	}

	public String getDescripcionTipoIps() {
		return descripcionTipoIps;
	}

	public void setDescripcionTipoIps(String descripcionTipoIps) {
		this.descripcionTipoIps = descripcionTipoIps;
	}
}
