package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;

/**
 * Class PrestadorVO
 * Clase VO del prestador
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class PrestadorVO implements Serializable{
	private static final long serialVersionUID = 2558239518050541773L;

	/*Identifica el tipo de identificacion del prestador */
	private TiposIdentificacionVO tiposIdentificacionPrestadorVO;	

	/*Identifica el numero de identificacion del prestador */
	private String numeroIdentificacionPrestador;
	
	/*Identifica el codigo interno del prestador */
	private String codigoInterno;
	
	/*Identifica la razon social del prestador */
	private String razonSocial;
	
	/*Identifica si el prestador es adscrito */
	private String prestadorAdscrito;
	
	/*Identifica la sucursal del prestador */
	private String nombreSucursal;
	
	/*Identifica la ciudad del prestador */
	private CiudadVO ciudadVO;
	
	/*Identifica el tipo de prestador */
	private TipoIPSVO tipoIPSVO;
	
	/*Identifica la direccion del prestador */
	private String direccionPrestador;
		
	/*Identifica el telefono del prestador */
	private String numeroTelefonicoPrestador;
	
	/*Identifica valor del prestador */
	private Integer valor;
	
	/*Identifica el codigo interno del prestador */
	private String nuevoCodigoInterno;
	
	/*Identifica el consecutivo de la ips destino del servicio de direccionamiento */
	private Integer consecutivoIpsDestino;

	public TiposIdentificacionVO getTiposIdentificacionPrestadorVO() {
		return tiposIdentificacionPrestadorVO;
	}

	public void setTiposIdentificacionPrestadorVO(
			TiposIdentificacionVO tiposIdentificacionPrestadorVO) {
		this.tiposIdentificacionPrestadorVO = tiposIdentificacionPrestadorVO;
	}

	public String getNumeroIdentificacionPrestador() {
		return numeroIdentificacionPrestador;
	}

	public void setNumeroIdentificacionPrestador(
			String numeroIdentificacionPrestador) {
		this.numeroIdentificacionPrestador = numeroIdentificacionPrestador;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getPrestadorAdscrito() {
		return prestadorAdscrito;
	}

	public void setPrestadorAdscrito(String prestadorAdscrito) {
		this.prestadorAdscrito = prestadorAdscrito;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public CiudadVO getCiudadVO() {
		return ciudadVO;
	}

	public void setCiudadVO(CiudadVO ciudadVO) {
		this.ciudadVO = ciudadVO;
	}
	
	public TipoIPSVO getTipoIPSVO() {
		return tipoIPSVO;
	}

	public void setTipoIPSVO(TipoIPSVO tipoIPSVO) {
		this.tipoIPSVO = tipoIPSVO;
	}
	
	public String getDireccionPrestador() {
		return direccionPrestador;
	}

	public void setDireccionPrestador(String direccionPrestador) {
		this.direccionPrestador = direccionPrestador;
	}

	public String getNumeroTelefonicoPrestador() {
		return numeroTelefonicoPrestador;
	}

	public void setNumeroTelefonicoPrestador(String numeroTelefonicoPrestador) {
		this.numeroTelefonicoPrestador = numeroTelefonicoPrestador;
	}

	/**
	 * @return the valor
	 */
	public Integer getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	
	public Integer getConsecutivoIpsDestino() {
		return consecutivoIpsDestino;
	}

	public void setConsecutivoIpsDestino(Integer consecutivoIpsDestino) {
		this.consecutivoIpsDestino = consecutivoIpsDestino;
	}

	/**
	 * @return the nuevoCodigoInterno
	 */
	public String getNuevoCodigoInterno() {
		return nuevoCodigoInterno;
	}

	/**
	 * @param nuevoCodigoInterno the nuevoCodigoInterno to set
	 */
	public void setNuevoCodigoInterno(String nuevoCodigoInterno) {
		this.nuevoCodigoInterno = nuevoCodigoInterno;
	}	
	
	
}
