package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class ContingenciasVO
 * Clase VO para las contingencias asociadas al diagnostico
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/12/2015
 *
 */
public class ContingenciasVO implements Serializable{
	private static final long serialVersionUID = 3618505517589505053L;

	/*Identifica el consecutivo de la contingencia.*/
	private Integer consecutivoCodigoContingencia;
	
	/*Identifica el codigo de la contingencia.*/
	private String codigoContingencia;
	
	/*Identifica la descripcion de la contingencia.*/
	private String descripcionContingencia;
	
	/*Identifica si la contingencias se aplica a la maternidad.*/
	private String   aplicaMaternidad;
	
	public Integer getConsecutivoCodigoContingencia() {
		return consecutivoCodigoContingencia;
	}
	public void setConsecutivoCodigoContingencia(Integer consecutivoCodigoContingencia) {
		this.consecutivoCodigoContingencia = consecutivoCodigoContingencia;
	}
	public String getCodigoContingencia() {
		return codigoContingencia;
	}
	public void setCodigoContingencia(String codigoContingencia) {
		this.codigoContingencia = codigoContingencia;
	}
	public String getDescripcionContingencia() {
		return descripcionContingencia;
	}
	public void setDescripcionContingencia(String descripcionContingencia) {
		this.descripcionContingencia = descripcionContingencia;
	}
	public String getAplicaMaternidad() {
		return aplicaMaternidad;
	}
	public void setAplicaMaternidad(String aplicaMaternidad) {
		this.aplicaMaternidad = aplicaMaternidad;
	}
}
