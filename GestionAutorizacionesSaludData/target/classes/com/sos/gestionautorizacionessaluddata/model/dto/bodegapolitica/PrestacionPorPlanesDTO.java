package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO de la información de prestaciones por planes
 * 
 * @author geniarjos
 * @since 14/03/2017
 */
public class PrestacionPorPlanesDTO implements Serializable{
	private static final long serialVersionUID = 2374961043188385946L;
	private String plan;
	private String coberturaPorPlan;
	private Integer semanasDeCarencia;
	private String cantidadMaximaPermitida;
	private Integer frecuenciaEntrega;
	private Integer topeMaximoporOrden;
	private String vigenciaAutorizacionMedica;
	private Integer edadMinima;
	private Integer edadMaxima;
	private Date inicioVigencia;
	private Date finVigencia;

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getCoberturaPorPlan() {
		return coberturaPorPlan;
	}

	public void setCoberturaPorPlan(String coberturaPorPlan) {
		this.coberturaPorPlan = coberturaPorPlan;
	}

	public Integer getSemanasDeCarencia() {
		return semanasDeCarencia;
	}

	public void setSemanasDeCarencia(Integer semanasDeCarencia) {
		this.semanasDeCarencia = semanasDeCarencia;
	}

	public String getCantidadMaximaPermitida() {
		return cantidadMaximaPermitida;
	}

	public void setCantidadMaximaPermitida(String cantidadMaximaPermitida) {
		this.cantidadMaximaPermitida = cantidadMaximaPermitida;
	}

	public Integer getFrecuenciaEntrega() {
		return frecuenciaEntrega;
	}

	public void setFrecuenciaEntrega(Integer frecuenciaEntrega) {
		this.frecuenciaEntrega = frecuenciaEntrega;
	}

	public Integer getTopeMaximoporOrden() {
		return topeMaximoporOrden;
	}

	public void setTopeMaximoporOrden(Integer topeMaximoporOrden) {
		this.topeMaximoporOrden = topeMaximoporOrden;
	}

	public String getVigenciaAutorizacionMedica() {
		return vigenciaAutorizacionMedica;
	}

	public void setVigenciaAutorizacionMedica(String vigenciaAutorizacionMedica) {
		this.vigenciaAutorizacionMedica = vigenciaAutorizacionMedica;
	}

	public Integer getEdadMinima() {
		return edadMinima;
	}

	public void setEdadMinima(Integer edadMinima) {
		this.edadMinima = edadMinima;
	}

	public Integer getEdadMaxima() {
		return edadMaxima;
	}

	public void setEdadMaxima(Integer edadMaxima) {
		this.edadMaxima = edadMaxima;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
}
