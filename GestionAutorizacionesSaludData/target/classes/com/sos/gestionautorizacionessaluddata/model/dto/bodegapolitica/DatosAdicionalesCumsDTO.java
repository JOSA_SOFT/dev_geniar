package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;

public class DatosAdicionalesCumsDTO implements Serializable{
	private static final long serialVersionUID = 2980304897740030438L;
	private String cdgoPrsntcn;
	private String dscrpcnPrsntcn;
	private String cntddPrsntcn;
	private String dscrpcnFrmaEntrga;
	private String obsrvcnsPrsntcn;
	private String cdgoFrmaFrmctca;
	private String dscrpcnFrmaFrmctca;
	private String cntddFrccnda;
	private String indccnsInvma;
	private String cntrndccnsInvma;
	private String cdgoVaAdmnstrcn;
	private String dscrpcnVaAdmnstrcn;
	private String cdgoMdloLqdcnCncpto;
	private String dscrpcnMdloLqdcnCncpto;

	public String getCdgoPrsntcn() {
		return cdgoPrsntcn;
	}

	public void setCdgoPrsntcn(String cdgoPrsntcn) {
		this.cdgoPrsntcn = cdgoPrsntcn;
	}

	public String getDscrpcnPrsntcn() {
		return dscrpcnPrsntcn;
	}

	public void setDscrpcnPrsntcn(String dscrpcnPrsntcn) {
		this.dscrpcnPrsntcn = dscrpcnPrsntcn;
	}

	public String getCntddPrsntcn() {
		return cntddPrsntcn;
	}

	public void setCntddPrsntcn(String cntddPrsntcn) {
		this.cntddPrsntcn = cntddPrsntcn;
	}

	public String getDscrpcnFrmaEntrga() {
		return dscrpcnFrmaEntrga;
	}

	public void setDscrpcnFrmaEntrga(String dscrpcnFrmaEntrga) {
		this.dscrpcnFrmaEntrga = dscrpcnFrmaEntrga;
	}

	public String getObsrvcnsPrsntcn() {
		return obsrvcnsPrsntcn;
	}

	public void setObsrvcnsPrsntcn(String obsrvcnsPrsntcn) {
		this.obsrvcnsPrsntcn = obsrvcnsPrsntcn;
	}

	public String getCdgoFrmaFrmctca() {
		return cdgoFrmaFrmctca;
	}

	public void setCdgoFrmaFrmctca(String cdgoFrmaFrmctca) {
		this.cdgoFrmaFrmctca = cdgoFrmaFrmctca;
	}

	public String getDscrpcnFrmaFrmctca() {
		return dscrpcnFrmaFrmctca;
	}

	public void setDscrpcnFrmaFrmctca(String dscrpcnFrmaFrmctca) {
		this.dscrpcnFrmaFrmctca = dscrpcnFrmaFrmctca;
	}

	public String getCntddFrccnda() {
		return cntddFrccnda;
	}

	public void setCntddFrccnda(String cntddFrccnda) {
		this.cntddFrccnda = cntddFrccnda;
	}

	public String getIndccnsInvma() {
		return indccnsInvma;
	}

	public void setIndccnsInvma(String indccnsInvma) {
		this.indccnsInvma = indccnsInvma;
	}

	public String getCntrndccnsInvma() {
		return cntrndccnsInvma;
	}

	public void setCntrndccnsInvma(String cntrndccnsInvma) {
		this.cntrndccnsInvma = cntrndccnsInvma;
	}

	public String getCdgoVaAdmnstrcn() {
		return cdgoVaAdmnstrcn;
	}

	public void setCdgoVaAdmnstrcn(String cdgoVaAdmnstrcn) {
		this.cdgoVaAdmnstrcn = cdgoVaAdmnstrcn;
	}

	public String getDscrpcnVaAdmnstrcn() {
		return dscrpcnVaAdmnstrcn;
	}

	public void setDscrpcnVaAdmnstrcn(String dscrpcnVaAdmnstrcn) {
		this.dscrpcnVaAdmnstrcn = dscrpcnVaAdmnstrcn;
	}

	public String getCdgoMdloLqdcnCncpto() {
		return cdgoMdloLqdcnCncpto;
	}

	public void setCdgoMdloLqdcnCncpto(String cdgoMdloLqdcnCncpto) {
		this.cdgoMdloLqdcnCncpto = cdgoMdloLqdcnCncpto;
	}

	public String getDscrpcnMdloLqdcnCncpto() {
		return dscrpcnMdloLqdcnCncpto;
	}

	public void setDscrpcnMdloLqdcnCncpto(String dscrpcnMdloLqdcnCncpto) {
		this.dscrpcnMdloLqdcnCncpto = dscrpcnMdloLqdcnCncpto;
	}
}
