package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * DTO de la información cabecera de los datos básicos.
 * 
 * @author geniarjos
 * @since 14/03/2017
 */
public class DatosBasicosBodegaPoliticaDTO implements Serializable{
	private static final long serialVersionUID = 6131519875664061005L;
	private Date inicioVigencia;
	private Date finVigencia;
	private String genero;
	private String nivel;
	private boolean coberturaPos;
	private boolean coberturaNoPos;
	private boolean coberturaPosCond;
	private boolean coberturaAutoF;
	private int tipoCobertura;

	public void setTipoCobertura(int tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}

	public int getTipoCobertura() {

		if (coberturaPos)
			tipoCobertura = ConstantesData.TIPO_COBERTURA_POS;
		if (coberturaNoPos)
			tipoCobertura = ConstantesData.TIPO_COBERTURA_NO_POS;
		if (coberturaPosCond)
			tipoCobertura = ConstantesData.TIPO_COBERTURA_POS_COND;
		if (coberturaAutoF)
			tipoCobertura = ConstantesData.TIPO_COBERTURA_POS_AUTH;

		return tipoCobertura;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public boolean isCoberturaPos() {
		return coberturaPos;
	}

	public void setCoberturaPos(boolean coberturaPos) {
		this.coberturaPos = coberturaPos;
	}

	public boolean isCoberturaNoPos() {
		return coberturaNoPos;
	}

	public void setCoberturaNoPos(boolean coberturaNoPos) {
		this.coberturaNoPos = coberturaNoPos;
	}

	public boolean isCoberturaPosCond() {
		return coberturaPosCond;
	}

	public void setCoberturaPosCond(boolean coberturaPosCond) {
		this.coberturaPosCond = coberturaPosCond;
	}

	public boolean isCoberturaAutoF() {
		return coberturaAutoF;
	}

	public void setCoberturaAutoF(boolean coberturaAutoF) {
		this.coberturaAutoF = coberturaAutoF;
	}

}
