package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class DireccionamientoXRiesgoDTO implements Serializable{
	private static final long serialVersionUID = 1966055491869497592L;
	private String cdgoIntrnoRiesgo;
	private String nmbreScrslRiesgo;
	private String dscrpcnSdeRiesgo;
	private String dscrpcnPlnRiesgo;
	private String cdgoRsgoDgnstcoRiesgo;
	private String dscrpcnRsgoDgnstcoRiesgo;
	private int eddMnmaRiesgo;
	private int eddMxmaRiesgo;
	private String sfcncaIpsRiesgo;
	private String nvlPrrddRiesgo;
	private Date incoVgncaRiesgo;
	private Date fnVgncaRiesgo;

	public String getCdgoIntrnoRiesgo() {
		return cdgoIntrnoRiesgo;
	}

	public void setCdgoIntrnoRiesgo(String cdgoIntrnoRiesgo) {
		this.cdgoIntrnoRiesgo = cdgoIntrnoRiesgo;
	}

	public String getNmbreScrslRiesgo() {
		return nmbreScrslRiesgo;
	}

	public void setNmbreScrslRiesgo(String nmbreScrslRiesgo) {
		this.nmbreScrslRiesgo = nmbreScrslRiesgo;
	}

	public String getDscrpcnSdeRiesgo() {
		return dscrpcnSdeRiesgo;
	}

	public void setDscrpcnSdeRiesgo(String dscrpcnSdeRiesgo) {
		this.dscrpcnSdeRiesgo = dscrpcnSdeRiesgo;
	}

	public String getDscrpcnPlnRiesgo() {
		return dscrpcnPlnRiesgo;
	}

	public void setDscrpcnPlnRiesgo(String dscrpcnPlnRiesgo) {
		this.dscrpcnPlnRiesgo = dscrpcnPlnRiesgo;
	}

	public String getCdgoRsgoDgnstcoRiesgo() {
		return cdgoRsgoDgnstcoRiesgo;
	}

	public void setCdgoRsgoDgnstcoRiesgo(String cdgoRsgoDgnstcoRiesgo) {
		this.cdgoRsgoDgnstcoRiesgo = cdgoRsgoDgnstcoRiesgo;
	}

	public String getDscrpcnRsgoDgnstcoRiesgo() {
		return dscrpcnRsgoDgnstcoRiesgo;
	}

	public void setDscrpcnRsgoDgnstcoRiesgo(String dscrpcnRsgoDgnstcoRiesgo) {
		this.dscrpcnRsgoDgnstcoRiesgo = dscrpcnRsgoDgnstcoRiesgo;
	}

	public int getEddMnmaRiesgo() {
		return eddMnmaRiesgo;
	}

	public void setEddMnmaRiesgo(int eddMnmaRiesgo) {
		this.eddMnmaRiesgo = eddMnmaRiesgo;
	}

	public int getEddMxmaRiesgo() {
		return eddMxmaRiesgo;
	}

	public void setEddMxmaRiesgo(int eddMxmaRiesgo) {
		this.eddMxmaRiesgo = eddMxmaRiesgo;
	}

	public String getSfcncaIpsRiesgo() {
		return sfcncaIpsRiesgo;
	}

	public void setSfcncaIpsRiesgo(String sfcncaIpsRiesgo) {
		this.sfcncaIpsRiesgo = sfcncaIpsRiesgo;
	}

	public String getNvlPrrddRiesgo() {
		return nvlPrrddRiesgo;
	}

	public void setNvlPrrddRiesgo(String nvlPrrddRiesgo) {
		this.nvlPrrddRiesgo = nvlPrrddRiesgo;
	}

	public Date getIncoVgncaRiesgo() {
		return incoVgncaRiesgo;
	}

	public void setIncoVgncaRiesgo(Date incoVgncaRiesgo) {
		this.incoVgncaRiesgo = incoVgncaRiesgo;
	}

	public Date getFnVgncaRiesgo() {
		return fnVgncaRiesgo;
	}

	public void setFnVgncaRiesgo(Date fnVgncaRiesgo) {
		this.fnVgncaRiesgo = fnVgncaRiesgo;
	}

}
