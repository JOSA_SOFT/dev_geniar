package com.sos.gestionautorizacionessaluddata.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Class PrestacionDTO
 * Clase DTO con la informacion de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class GestionAuditorDTO implements Serializable {
	private static final long serialVersionUID = -3413641206314944491L;
	private Integer consecutivoEstadoPrestacion;
	private List<String> lstCausas = new ArrayList<String>();
	private String justificacion;
	private String observacion;
	
	private Integer consLateralidad;
	private String lateralidad;
	private String viaAcceso;
	private String cantidadSOS;
	private Integer consRecobro;
	private String recobro;
	
	private String dosis;
	private String posologiaCUMS;
	private String cada;
	private String frecuencia;
	private Integer causalNoCobroCuota;
	
	/**
	 * @return the consecutivoEstadoPrestacion
	 */
	public Integer getConsecutivoEstadoPrestacion() {
		return consecutivoEstadoPrestacion;
	}
	/**
	 * @param consecutivoEstadoPrestacion the consecutivoEstadoPrestacion to set
	 */
	public void setConsecutivoEstadoPrestacion(Integer consecutivoEstadoPrestacion) {
		this.consecutivoEstadoPrestacion = consecutivoEstadoPrestacion;
	}
	/**
	 * @return the lstCausas
	 */
	public List<String> getLstCausas() {
		return lstCausas;
	}
	/**
	 * @param lstCausas the lstCausas to set
	 */
	public void setLstCausas(List<String> lstCausas) {
		this.lstCausas = lstCausas;
	}
	/**
	 * @return the justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}
	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return observacion;
	}
	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	/**
	 * @return the lateralidad
	 */
	public String getLateralidad() {
		return lateralidad;
	}
	/**
	 * @param lateralidad the lateralidad to set
	 */
	public void setLateralidad(String lateralidad) {
		this.lateralidad = lateralidad;
	}
	/**
	 * @return the viaAcceso
	 */
	public String getViaAcceso() {
		return viaAcceso;
	}
	/**
	 * @param viaAcceso the viaAcceso to set
	 */
	public void setViaAcceso(String viaAcceso) {
		this.viaAcceso = viaAcceso;
	}
	/**
	 * @return the cantidadSOS
	 */
	public String getCantidadSOS() {
		return cantidadSOS;
	}
	/**
	 * @param cantidadSOS the cantidadSOS to set
	 */
	public void setCantidadSOS(String cantidadSOS) {
		this.cantidadSOS = cantidadSOS;
	}

	public String getRecobro() {
		return recobro;
	}
	public void setRecobro(String recobro) {
		this.recobro = recobro;
	}
	
	public String getDosis() {
		return dosis;
	}
	
	public void setDosis(String dosis) {
		this.dosis = dosis;
	}
	
	public String getPosologiaCUMSGeneral() {
		return posologiaCUMS;
	}
	
	public void setPosologiaCUMSGeneral(String posologiaCUMS) {
		this.posologiaCUMS = posologiaCUMS;
	}
	
	public String getCadaGeneral() {
		return cada;
	}
	
	public void setCadaGeneral(String cada) {
		this.cada = cada;
	}
	
	public String getFrecuenciaGeneral() {
		return frecuencia;
	}
	
	public void setFrecuenciaGeneral(String frecuencia) {
		this.frecuencia = frecuencia;
	}
	
	public Integer getConsLateralidad() {
		return consLateralidad;
	}
	
	public void setConsLateralidad(Integer consLateralidad) {
		this.consLateralidad = consLateralidad;
	}
	
	public Integer getConsRecobro() {
		return consRecobro;
	}
	
	public void setConsRecobro(Integer consRecobro) {
		this.consRecobro = consRecobro;
	}
	
	public Integer getCausalNoCobroCuota() {
		return causalNoCobroCuota;
	}
	
	public void setCausalNoCobroCuota(Integer causalNoCobroCuota) {
		this.causalNoCobroCuota = causalNoCobroCuota;
	}
	
}