package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class DireccionamientoNormalDTO implements Serializable{
	private static final long serialVersionUID = -3461334559229121580L;
	private String cdgoIntrnoNormal;
	private String nmbreScrslNormal;
	private String dscrpcnSdeNormal;
	private String dscrpcnZnaNormal;
	private String dscrpcnPlnNormal;
	private int eddMnmaNormal;
	private int eddMxmaNormal;
	private String sfcncaIpsNormal;
	private String nvlPrrddNormal;
	private Date incoVgncaNormal;
	private Date fnVgncaNormal;

	public String getCdgoIntrnoNormal() {
		return cdgoIntrnoNormal;
	}

	public void setCdgoIntrnoNormal(String cdgoIntrnoNormal) {
		this.cdgoIntrnoNormal = cdgoIntrnoNormal;
	}

	public String getNmbreScrslNormal() {
		return nmbreScrslNormal;
	}

	public void setNmbreScrslNormal(String nmbreScrslNormal) {
		this.nmbreScrslNormal = nmbreScrslNormal;
	}

	public String getDscrpcnSdeNormal() {
		return dscrpcnSdeNormal;
	}

	public void setDscrpcnSdeNormal(String dscrpcnSdeNormal) {
		this.dscrpcnSdeNormal = dscrpcnSdeNormal;
	}

	public String getDscrpcnZnaNormal() {
		return dscrpcnZnaNormal;
	}

	public void setDscrpcnZnaNormal(String dscrpcnZnaNormal) {
		this.dscrpcnZnaNormal = dscrpcnZnaNormal;
	}

	public String getDscrpcnPlnNormal() {
		return dscrpcnPlnNormal;
	}

	public void setDscrpcnPlnNormal(String dscrpcnPlnNormal) {
		this.dscrpcnPlnNormal = dscrpcnPlnNormal;
	}

	public int getEddMnmaNormal() {
		return eddMnmaNormal;
	}

	public void setEddMnmaNormal(int eddMnmaNormal) {
		this.eddMnmaNormal = eddMnmaNormal;
	}

	public int getEddMxmaNormal() {
		return eddMxmaNormal;
	}

	public void setEddMxmaNormal(int eddMxmaNormal) {
		this.eddMxmaNormal = eddMxmaNormal;
	}

	public String getSfcncaIpsNormal() {
		return sfcncaIpsNormal;
	}

	public void setSfcncaIpsNormal(String sfcncaIpsNormal) {
		this.sfcncaIpsNormal = sfcncaIpsNormal;
	}

	public String getNvlPrrddNormal() {
		return nvlPrrddNormal;
	}

	public void setNvlPrrddNormal(String nvlPrrddNormal) {
		this.nvlPrrddNormal = nvlPrrddNormal;
	}

	public Date getIncoVgncaNormal() {
		return incoVgncaNormal;
	}

	public void setIncoVgncaNormal(Date incoVgncaNormal) {
		this.incoVgncaNormal = incoVgncaNormal;
	}

	public Date getFnVgncaNormal() {
		return fnVgncaNormal;
	}

	public void setFnVgncaNormal(Date fnVgncaNormal) {
		this.fnVgncaNormal = fnVgncaNormal;
	}

}
