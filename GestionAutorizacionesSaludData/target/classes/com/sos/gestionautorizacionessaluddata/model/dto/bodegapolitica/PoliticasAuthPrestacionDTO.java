package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;

/**
 * DTO de la información políticas de autorización vigentes que tenga asociadas
 * la prestación
 * 
 * @author geniarjos
 * @since 14/03/2017
 */
public class PoliticasAuthPrestacionDTO implements Serializable {
	private static final long serialVersionUID = -3818880610794524213L;
	private String tipoPolitica;
	private String agrupadorPolitica;
	private String descripcionPolitica;
	private String shortDescripcionPolitica;

	public void setShortDescripcionPolitica(String shortDescripcionPolitica) {
		this.shortDescripcionPolitica = shortDescripcionPolitica;
	}

	public String getShortDescripcionPolitica() {
		return shortDescripcionPolitica;
	}

	public String getTipoPolitica() {
		return tipoPolitica;
	}

	public void setTipoPolitica(String tipoPolitica) {
		this.tipoPolitica = tipoPolitica;
	}

	public String getAgrupadorPolitica() {
		return agrupadorPolitica;
	}

	public void setAgrupadorPolitica(String agrupadorPolitica) {
		this.agrupadorPolitica = agrupadorPolitica;
	}

	public String getDescripcionPolitica() {
		return descripcionPolitica;
	}

	public void setDescripcionPolitica(String descripcionPolitica) {
		this.descripcionPolitica = descripcionPolitica;
	}
}
