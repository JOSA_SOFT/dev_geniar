package com.sos.gestionautorizacionessaluddata.model.dto;

public class CambioFechaVencimientoDTO {

	private String numOPS;
	private String nombreAfiliado;
	private String fechaIni;
	private String fechaVig;
	private String estado;
	private String mensajeError;
	private String docAfiliado;	
	
	
	public String getNumOPS() {
		return numOPS;
	}
	public void setNumOPS(String numOPS) {
		this.numOPS = numOPS;
	}
	public String getNombreAfiliado() {
		return nombreAfiliado;
	}
	public void setNombreAfiliado(String nombreAfiliado) {
		this.nombreAfiliado = nombreAfiliado;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaVig() {
		return fechaVig;
	}
	public void setFechaVig(String fechaVig) {
		this.fechaVig = fechaVig;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getDocAfiliado() {
		return docAfiliado;
	}
	public void setDocAfiliado(String docAfiliado) {
		this.docAfiliado = docAfiliado;
	}
	
	
}
