package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class PagoFijoDTO implements Serializable {
	private static final long serialVersionUID = -1462065256631731569L;

	private Integer codigoTipoPago;
	private String descrTipoPago;
	private String descrCiudad;
	private Integer codigoDiagnostico;
	private String descrDiagnostico;
	private Date inicioVigencia;
	private Date finVigencia;
	
	public Integer getCodigoTipoPago() {
		return codigoTipoPago;
	}
	public void setCodigoTipoPago(Integer codigoTipoPago) {
		this.codigoTipoPago = codigoTipoPago;
	}
	public String getDescrTipoPago() {
		return descrTipoPago;
	}
	public void setDescrTipoPago(String descrTipoPago) {
		this.descrTipoPago = descrTipoPago;
	}
	public String getDescrCiudad() {
		return descrCiudad;
	}
	public void setDescrCiudad(String descrCiudad) {
		this.descrCiudad = descrCiudad;
	}
	public Integer getCodigoDiagnostico() {
		return codigoDiagnostico;
	}
	public void setCodigoDiagnostico(Integer codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}
	public String getDescrDiagnostico() {
		return descrDiagnostico;
	}
	public void setDescrDiagnostico(String descrDiagnostico) {
		this.descrDiagnostico = descrDiagnostico;
	}
	public Date getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
}
