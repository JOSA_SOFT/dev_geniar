package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class ConvenioPrestacionDTO implements Serializable{
	private static final long serialVersionUID = 416185905229967425L;
	private String ipsCodigo;
	private String ipsDescripcion;
	private String ciudadDesc;
	private String planDesc;
	private Double valor;
	private String tipoAtencion;
	private String formulaLiquidacion;
	private String accesoDirecto;
	private String direccion;
	private String telefono;
	private Date inicioVigencia;
	private Date finVigencia;

	public String getIpsCodigo() {
		return ipsCodigo;
	}

	public void setIpsCodigo(String ipsCodigo) {
		this.ipsCodigo = ipsCodigo;
	}

	public String getIpsDescripcion() {
		return ipsDescripcion;
	}

	public void setIpsDescripcion(String ipsDescripcion) {
		this.ipsDescripcion = ipsDescripcion;
	}

	public String getCiudadDesc() {
		return ciudadDesc;
	}

	public void setCiudadDesc(String ciudadDesc) {
		this.ciudadDesc = ciudadDesc;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getTipoAtencion() {
		return tipoAtencion;
	}

	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}

	public String getFormulaLiquidacion() {
		return formulaLiquidacion;
	}

	public void setFormulaLiquidacion(String formulaLiquidacion) {
		this.formulaLiquidacion = formulaLiquidacion;
	}

	public String getAccesoDirecto() {
		return accesoDirecto;
	}

	public void setAccesoDirecto(String accesoDirecto) {
		this.accesoDirecto = accesoDirecto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
}
