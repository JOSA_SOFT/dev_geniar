package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class ConvenioIpsDTO implements Serializable {

	private static final long serialVersionUID = -4081366778891831528L;
	
	private Integer consecutivoPrestacion;
	private String descripcionPrestacion;
	private String nombreIps;
	private String descripcionPlan;
	private Integer valorConvenio;
	private String tipoAtencionIps;
	private String formulaLiquidacionIps;
	private String accesoDirectoIps;
	private String direccionTelefonoIps;
	private Date inicioVigenciaMarcaPrestacion;
	private Date finVigenciaMarcaPrestacion;
	
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}
	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}
	public String getNombreIps() {
		return nombreIps;
	}
	public void setNombreIps(String nombreIps) {
		this.nombreIps = nombreIps;
	}
	public String getDescripcionPlan() {
		return descripcionPlan;
	}
	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}
	public Integer getValorConvenio() {
		return valorConvenio;
	}
	public void setValorConvenio(Integer valorConvenio) {
		this.valorConvenio = valorConvenio;
	}
	public String getTipoAtencionIps() {
		return tipoAtencionIps;
	}
	public void setTipoAtencionIps(String tipoAtencionIps) {
		this.tipoAtencionIps = tipoAtencionIps;
	}
	public String getFormulaLiquidacionIps() {
		return formulaLiquidacionIps;
	}
	public void setFormulaLiquidacionIps(String formulaLiquidacionIps) {
		this.formulaLiquidacionIps = formulaLiquidacionIps;
	}
	public String getAccesoDirectoIps() {
		return accesoDirectoIps;
	}
	public void setAccesoDirectoIps(String accesoDirectoIps) {
		this.accesoDirectoIps = accesoDirectoIps;
	}
	public String getDireccionTelefonoIps() {
		return direccionTelefonoIps;
	}
	public void setDireccionTelefonoIps(String direccionTelefonoIps) {
		this.direccionTelefonoIps = direccionTelefonoIps;
	}
	public Date getInicioVigenciaMarcaPrestacion() {
		return inicioVigenciaMarcaPrestacion;
	}
	public void setInicioVigenciaMarcaPrestacion(Date inicioVigenciaMarcaPrestacion) {
		this.inicioVigenciaMarcaPrestacion = inicioVigenciaMarcaPrestacion;
	}
	public Date getFinVigenciaMarcaPrestacion() {
		return finVigenciaMarcaPrestacion;
	}
	public void setFinVigenciaMarcaPrestacion(Date finVigenciaMarcaPrestacion) {
		this.finVigenciaMarcaPrestacion = finVigenciaMarcaPrestacion;
	}
}
