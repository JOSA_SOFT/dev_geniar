package com.sos.gestionautorizacionessaluddata.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO;


/**
 * Class PrestacionDTO
 * Clase DTO con la informacion de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class DatosNegacionDTO implements Serializable{
	private static final long serialVersionUID = 5451434322248735360L;
	private List<String> lstFundamentoLegal = new ArrayList<String>();
	private MedicoVO auditorMedico = new MedicoVO();
	private List<AlternativasVO> lstAlternativas = new ArrayList<AlternativasVO>();
	/**
	 * @return the lstFundamentoLegal
	 */
	public List<String> getLstFundamentoLegal() {
		return lstFundamentoLegal;
	}
	/**
	 * @param lstFundamentoLegal the lstFundamentoLegal to set
	 */
	public void setLstFundamentoLegal(List<String> lstFundamentoLegal) {
		this.lstFundamentoLegal = lstFundamentoLegal;
	}
	/**
	 * @return the auditorMedico
	 */
	public MedicoVO getAuditorMedico() {
		return auditorMedico;
	}
	/**
	 * @param auditorMedico the auditorMedico to set
	 */
	public void setAuditorMedico(MedicoVO auditorMedico) {
		this.auditorMedico = auditorMedico;
	}
	/**
	 * @return the lstAlternativas
	 */
	public List<AlternativasVO> getLstAlternativas() {
		return lstAlternativas;
	}
	/**
	 * @param lstAlternativas the lstAlternativas to set
	 */
	public void setLstAlternativas(List<AlternativasVO> lstAlternativas) {
		this.lstAlternativas = lstAlternativas;
	}


	
	
}