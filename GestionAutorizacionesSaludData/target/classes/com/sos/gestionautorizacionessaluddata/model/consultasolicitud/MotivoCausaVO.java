package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;

/**
 * Class MotivoCausaVO
 * Clase VO para guardar la causa/motivo
 * @author Julian Hernandez
 * @version 11/03/2016
 *
 */
public class MotivoCausaVO implements Serializable{
	private static final long serialVersionUID = -1675557597053780083L;

	/** Identifica el consecutivo MotivoCausaVO*/
	private Integer consecutivoCodMotivoCausa;
	
	/** Identifica la codigo MotivoCausaVO*/
	private Integer codMotivoCausa;
	
	/** Identifica la descripcion MotivoCausaVO*/
	private String desMotivoCausa;
	
	/** Identifica el consecutivo TIPO MotivoCausaVO*/
	private Integer consecutivoTipoMotivoCausa;

	private boolean requiereNumeroAutorizacionIps;
	


	public MotivoCausaVO(Integer consecutivoCodMotivoCausa,
			Integer codMotivoCausa, String desMotivoCausa,
			Integer consecutivoTipoMotivoCausa) {
		super();
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
		this.codMotivoCausa = codMotivoCausa;
		this.desMotivoCausa = desMotivoCausa;
		this.consecutivoTipoMotivoCausa = consecutivoTipoMotivoCausa;
	}
	
	public MotivoCausaVO() {
		super();
	}

	/**
	 * @return the isRequiereNumeroAutorizacionIps
	 */
	public boolean isRequiereNumeroAutorizacionIps() {
		return requiereNumeroAutorizacionIps;
	}

	/**
	 * @param requiereNumeroAutorizacionIps the requiereNumeroAutorizacionIps to set
	 */
	public void setRequiereNumeroAutorizacionIps(
			boolean requiereNumeroAutorizacionIps) {
		this.requiereNumeroAutorizacionIps = requiereNumeroAutorizacionIps;
	}
	
	/**
	 * @return the consecutivoCodMotivoCausa
	 */
	public Integer getConsecutivoCodMotivoCausa() {
		return consecutivoCodMotivoCausa;
	}

	/**
	 * @param consecutivoCodMotivoCausa the consecutivoCodMotivoCausa to set
	 */
	public void setConsecutivoCodMotivoCausa(Integer consecutivoCodMotivoCausa) {
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
	}

	/**
	 * @return the codMotivoCausa
	 */
	public Integer getCodMotivoCausa() {
		return codMotivoCausa;
	}

	/**
	 * @param codMotivoCausa the codMotivoCausa to set
	 */
	public void setCodMotivoCausa(Integer codMotivoCausa) {
		this.codMotivoCausa = codMotivoCausa;
	}

	/**
	 * @return the desMotivoCausa
	 */
	public String getDesMotivoCausa() {
		return desMotivoCausa;
	}

	/**
	 * @param desMotivoCausa the desMotivoCausa to set
	 */
	public void setDesMotivoCausa(String desMotivoCausa) {
		this.desMotivoCausa = desMotivoCausa;
	}

	/**
	 * @return the consecutivoTipoMotivoCausa
	 */
	public Integer getConsecutivoTipoMotivoCausa() {
		return consecutivoTipoMotivoCausa;
	}

	/**
	 * @param consecutivoTipoMotivoCausa the consecutivoTipoMotivoCausa to set
	 */
	public void setConsecutivoTipoMotivoCausa(Integer consecutivoTipoMotivoCausa) {
		this.consecutivoTipoMotivoCausa = consecutivoTipoMotivoCausa;
	}
	
	
	
}
