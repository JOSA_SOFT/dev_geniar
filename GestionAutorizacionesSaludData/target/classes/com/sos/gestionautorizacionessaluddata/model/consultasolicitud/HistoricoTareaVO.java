package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;


/**
 * Class HistoricoTareaVO
 * Clase VO que contiene los datos del historico de tareas
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 *
 */
public class HistoricoTareaVO implements Serializable {
	private static final long serialVersionUID = 8751485960732002452L;

	/** identifica el consecutivo de la tarea*/
	private Integer consecutivoTareaInstancia;
	
	/** identifica el consecutivo de la solicitud*/
	private Integer numeroSolicitud;
	
	/** identifica el identificador del proceso*/
	private String identificadorProceso;
	
	/** identifica el consecutivo de la tarea*/
	private Integer consecutivoTarea;
	
	/** identifica el nombre de la tarea*/
	private String nombreTarea;
	
	/** identifica la fecha de la tarea*/
	private Date fechaTarea;
		
	/** identifica el usuario de la tarea*/
	private String usuarioTarea;
		
	/** identifica el tipo tarea*/
	private int tipoTarea;
	
	/** identifica el nombre del grupo de riesgo */
	private String nombreGrupoRiesgo;
	
	public Integer getConsecutivoTareaInstancia() {
		return consecutivoTareaInstancia;
	}

	public void setConsecutivoTareaInstancia(Integer consecutivoTareaInstancia) {
		this.consecutivoTareaInstancia = consecutivoTareaInstancia;
	}

	public Integer getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(Integer numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getIdentificadorProceso() {
		return identificadorProceso;
	}

	public void setIdentificadorProceso(String identificadorProceso) {
		this.identificadorProceso = identificadorProceso;
	}

	public Integer getConsecutivoTarea() {
		return consecutivoTarea;
	}

	public void setConsecutivoTarea(Integer consecutivoTarea) {
		this.consecutivoTarea = consecutivoTarea;
	}
	
	public String getNombreTarea() {
		return nombreTarea;
	}

	public void setNombreTarea(String nombreTarea) {
		this.nombreTarea = nombreTarea;
	}

	public Date getFechaTarea() {
		return fechaTarea;
	}

	public void setFechaTarea(Date fechaTarea) {
		this.fechaTarea = fechaTarea;
	}

	public String getUsuarioTarea() {
		return usuarioTarea;
	}

	public void setUsuarioTarea(String usuarioTarea) {
		this.usuarioTarea = usuarioTarea;
	}

	public int getTipoTarea() {
		return tipoTarea;
	}

	public void setTipoTarea(int tipoTarea) {
		this.tipoTarea = tipoTarea;
	}

	public String getNombreGrupoRiesgo() {
		return nombreGrupoRiesgo;
	}

	public void setNombreGrupoRiesgo(String nombreGrupoRiesgo) {
		this.nombreGrupoRiesgo = nombreGrupoRiesgo;
	}	
}
