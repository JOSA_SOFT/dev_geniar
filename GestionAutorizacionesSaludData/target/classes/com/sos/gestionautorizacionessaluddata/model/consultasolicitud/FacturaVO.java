package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;


/**
 * Class FacturaVO
 * Clase VO que contiene la informacion del factura relacionada con 
 * los conceptos de gasto
 * @author ing. Victor Hugo Gil Ramos
 * @version 02/05/2016
 *
 */
public class FacturaVO implements Serializable {
	private static final long serialVersionUID = -5588956302867036985L;

	/*Identifica el numero de factura */
	private String numeroFactura;
	
	/*Identifica la fecha de factura */
	private Date fechaFactura;	
	
	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
}
