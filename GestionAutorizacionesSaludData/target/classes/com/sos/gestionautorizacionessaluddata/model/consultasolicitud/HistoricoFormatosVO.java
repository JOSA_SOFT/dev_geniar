package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;


/**
 * Class HistoricoFormatosVO
 * Clase VO del historico para la descarga de formatos
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/03/2016
 *
 */
public class HistoricoFormatosVO implements Serializable {
	private static final long serialVersionUID = 2932829863729464740L;

	/*Identifica la fecha de descarga del documento */
	private Date fechaDescargaFormato;
	
	/*Identifica la descricion del servicio solicitada*/
	private String descripcionServicioSolicitado;
	
	/*Identifica el formato de descarga*/
	private String formatoDescarga;
	
	/*Identifica el usuario que realiza la descarga del formato*/
	private String usuarioFormatoDescarga;
	
	/*Identifica la descripcion del medio por el cual se descarga el formato*/
	private String descripcionMedioContactoDescarga;
	
	private Integer consecutivoPrestacion;
	
/*Identifica la descricion de la Prestación*/
	private String descripcionPrestacion;
	
	public Date getFechaDescargaFormato() {
		return fechaDescargaFormato;
	}

	public void setFechaDescargaFormato(Date fechaDescargaFormato) {
		this.fechaDescargaFormato = fechaDescargaFormato;
	}

	public String getDescripcionServicioSolicitado() {
		return descripcionServicioSolicitado;
	}

	public void setDescripcionServicioSolicitado(
			String descripcionServicioSolicitado) {
		this.descripcionServicioSolicitado = descripcionServicioSolicitado;
	}

	public String getFormatoDescarga() {
		return formatoDescarga;
	}

	public void setFormatoDescarga(String formatoDescarga) {
		this.formatoDescarga = formatoDescarga;
	}

	public String getUsuarioFormatoDescarga() {
		return usuarioFormatoDescarga;
	}

	public void setUsuarioFormatoDescarga(String usuarioFormatoDescarga) {
		this.usuarioFormatoDescarga = usuarioFormatoDescarga;
	}

	public String getDescripcionMedioContactoDescarga() {
		return descripcionMedioContactoDescarga;
	}

	public void setDescripcionMedioContactoDescarga(
			String descripcionMedioContactoDescarga) {
		this.descripcionMedioContactoDescarga = descripcionMedioContactoDescarga;
	}

	/**
	 * @return the consecutivoPrestacion
	 */
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	/**
	 * @param consecutivoPrestacion the consecutivoPrestacion to set
	 */
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}	
	
	
}
