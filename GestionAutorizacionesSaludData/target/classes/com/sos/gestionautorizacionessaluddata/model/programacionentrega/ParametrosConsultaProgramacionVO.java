package com.sos.gestionautorizacionessaluddata.model.programacionentrega;

import java.io.Serializable;
import java.util.Date;

public class ParametrosConsultaProgramacionVO implements Serializable {
	private static final long serialVersionUID = 2438373383831143535L;
	private Integer	numeroUnicoIdentificacion;
	private Date fechaDesde;
	private Date fechaHasta; 
	private Integer	consCodClasificacionEvento;
	private Integer	consCodEstadoEntrega;
	private Integer	tipoPrestacionSeleccionado;
	private Integer	consecutivoPrestacion;
	private String	acta; 
	private String numeroNotificacion;
	
	
	public Integer getNumeroUnicoIdentificacion() {
		return numeroUnicoIdentificacion;
	}
	public void setNumeroUnicoIdentificacion(Integer numeroUnicoIdentificacion) {
		this.numeroUnicoIdentificacion = numeroUnicoIdentificacion;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Integer getConsCodClasificacionEvento() {
		return consCodClasificacionEvento;
	}
	public void setConsCodClasificacionEvento(Integer consCodClasificacionEvento) {
		this.consCodClasificacionEvento = consCodClasificacionEvento;
	}
	public Integer getConsCodEstadoEntrega() {
		return consCodEstadoEntrega;
	}
	public void setConsCodEstadoEntrega(Integer consCodEstadoEntrega) {
		this.consCodEstadoEntrega = consCodEstadoEntrega;
	}
	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}
	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}
	public String getActa() {
		return acta;
	}
	public void setActa(String acta) {
		this.acta = acta;
	}
	public String getNumeroNotificacion() {
		return numeroNotificacion;
	}
	public void setNumeroNotificacion(String numeroNotificacion) {
		this.numeroNotificacion = numeroNotificacion;
	}

}
