package com.sos.gestionautorizacionessaluddata.model.programacionentrega;

import java.io.Serializable;

/**
 * Class ClasificacionEventoXProgramacionVO
 * Clase VO de la claisificacion de eventos
 * @author Julian Hernandez
 * @version 05/07/2016
 *
 */
public class ClasificacionEventoXProgramacionVO implements Serializable{
	private static final long serialVersionUID = -5894724772150511624L;

	/**Identifica la descripcion **/
	private String desClasificacionEvento;
	
	/** Identifica el consecutivo **/
	private Integer consCodClasificacionEvento;
	
	/** Identifica el codigo **/
	private String codClasificacionEvento;

	/**
	 * @return the desClasificacionEvento
	 */
	public String getDesClasificacionEvento() {
		return desClasificacionEvento;
	}

	/**
	 * @param desClasificacionEvento the desClasificacionEvento to set
	 */
	public void setDesClasificacionEvento(String desClasificacionEvento) {
		this.desClasificacionEvento = desClasificacionEvento;
	}

	/**
	 * @return the consCodClasificacionEvento
	 */
	public Integer getConsCodClasificacionEvento() {
		return consCodClasificacionEvento;
	}

	/**
	 * @param consCodClasificacionEvento the consCodClasificacionEvento to set
	 */
	public void setConsCodClasificacionEvento(Integer consCodClasificacionEvento) {
		this.consCodClasificacionEvento = consCodClasificacionEvento;
	}

	/**
	 * @return the codClasificacionEvento
	 */
	public String getCodClasificacionEvento() {
		return codClasificacionEvento;
	}

	/**
	 * @param codClasificacionEvento the codClasificacionEvento to set
	 */
	public void setCodClasificacionEvento(String codClasificacionEvento) {
		this.codClasificacionEvento = codClasificacionEvento;
	}

	
	
}
