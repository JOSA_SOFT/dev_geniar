package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;
import java.util.Date;


/**
 * Clase VO para los riesgos del afiliado
 * @author Julian Hernandez Gomez
 * @version 10/03/2016
 *
 */
public class HistoricoPrestacionVO implements Serializable {
	private static final long serialVersionUID = -364973019753255186L;

	/** **/
	private Date fecha;
	
	/** **/
	private String descripcionTipoPrestacion;
	
	/** **/
	private String descripcionPrestacion;
	
	/** **/
	private String descripcionEstado;
	
	/** **/
	private String descripcionPrestador;

	/** **/
	private Integer cantidadHistorico;
	
	/** **/
	private String descPos;

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the descTipoPrestacion
	 */
	public String getDescripcionTipoPrestacion() {
		return descripcionTipoPrestacion;
	}

	/**
	 * @param descTipoPrestacion the descTipoPrestacion to set
	 */
	public void setDescripcionTipoPrestacion(String descTipoPrestacion) {
		this.descripcionTipoPrestacion = descTipoPrestacion;
	}

	/**
	 * @return the descPrestacion
	 */
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	/**
	 * @param descPrestacion the descPrestacion to set
	 */
	public void setDescripcionPrestacion(String descPrestacion) {
		this.descripcionPrestacion = descPrestacion;
	}

	/**
	 * @return the descEstado
	 */
	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	/**
	 * @param descEstado the descEstado to set
	 */
	public void setDescripcionEstado(String descEstado) {
		this.descripcionEstado = descEstado;
	}

	/**
	 * @return the descPrestador
	 */
	public String getDescripcionPrestador() {
		return descripcionPrestador;
	}

	/**
	 * @param descPrestador the descPrestador to set
	 */
	public void setDescripcionPrestador(String descPrestador) {
		this.descripcionPrestador = descPrestador;
	}

	/**
	 * @return the cantidad
	 */
	public Integer getCantidadHistorico() {
		return cantidadHistorico;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidadHistorico(Integer cantidad) {
		this.cantidadHistorico = cantidad;
	}

	/**
	 * @return the descPos
	 */
	public String getDescPos() {
		return descPos;
	}

	/**
	 * @param descPos the descPos to set
	 */
	public void setDescPos(String descPos) {
		this.descPos = descPos;
	}


	
	
}
