/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que se encarga de pasar los parametros al reporte de cierre de caja detallado
 * private author GENIAR
 */
public class ParametroReporteCierreCajaVO implements Serializable {
	private static final long serialVersionUID = 2619954414968845004L;
	private Integer nui;
	private Integer consecutivoCodigoOficina;
	private String login;
	private Date fechaInicial;
	private Integer numeroOps;
	private Integer numeroRecibo;
	private String numeroID;
	private String tipoID;
	private Integer consecutivoCodigoSede;
	public Integer getNui() {
		return nui;
	}
	public void setNui(Integer nui) {
		this.nui = nui;
	}
	public Integer getConsecutivoCodigoOficina() {
		return consecutivoCodigoOficina;
	}
	public void setConsecutivoCodigoOficina(Integer consecutivoCodigoOficina) {
		this.consecutivoCodigoOficina = consecutivoCodigoOficina;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Integer getNumeroOps() {
		return numeroOps;
	}
	public void setNumeroOps(Integer numeroOps) {
		this.numeroOps = numeroOps;
	}
	public Integer getNumeroRecibo() {
		return numeroRecibo;
	}
	public void setNumeroRecibo(Integer numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	public String getNumeroID() {
		return numeroID;
	}
	public void setNumeroID(String numeroID) {
		this.numeroID = numeroID;
	}
	public String getTipoID() {
		return tipoID;
	}
	public void setTipoID(String tipoID) {
		this.tipoID = tipoID;
	}
	public Integer getConsecutivoCodigoSede() {
		return consecutivoCodigoSede;
	}
	public void setConsecutivoCodigoSede(Integer consecutivoCodigoSede) {
		this.consecutivoCodigoSede = consecutivoCodigoSede;
	}
	
}
