package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Encapsula la causal de descuadre
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class CausalDescuadreVO implements Serializable{
	private static final long serialVersionUID = -2475789818428726468L;
	private Integer cnsctvoCdgoCslDscdre;
	private String cdgoCslDscdre;
	private String dscrpcnCslDscdre;
	private Date fchaCrcn;
	private String usroCrcn;
	private String vsbleUsro;
	private Double valorPopup;

	public Integer getCnsctvoCdgoCslDscdre() {
		return cnsctvoCdgoCslDscdre;
	}

	public void setCnsctvoCdgoCslDscdre(Integer cnsctvoCdgoCslDscdre) {
		this.cnsctvoCdgoCslDscdre = cnsctvoCdgoCslDscdre;
	}

	public String getCdgoCslDscdre() {
		return cdgoCslDscdre;
	}

	public void setCdgoCslDscdre(String cdgoCslDscdre) {
		this.cdgoCslDscdre = cdgoCslDscdre;
	}

	public String getDscrpcnCslDscdre() {
		return dscrpcnCslDscdre;
	}

	public void setDscrpcnCslDscdre(String dscrpcnCslDscdre) {
		this.dscrpcnCslDscdre = dscrpcnCslDscdre;
	}

	public Date getFchaCrcn() {
		return fchaCrcn;
	}

	public void setFchaCrcn(Date fchaCrcn) {
		this.fchaCrcn = fchaCrcn;
	}

	public String getUsroCrcn() {
		return usroCrcn;
	}

	public void setUsroCrcn(String usroCrcn) {
		this.usroCrcn = usroCrcn;
	}

	public String getVsbleUsro() {
		return vsbleUsro;
	}

	public void setVsbleUsro(String vsbleUsro) {
		this.vsbleUsro = vsbleUsro;
	}

	public Double getValorPopup() {
		return valorPopup;
	}

	public void setValorPopup(Double valorPopup) {
		this.valorPopup = valorPopup;
	}
}