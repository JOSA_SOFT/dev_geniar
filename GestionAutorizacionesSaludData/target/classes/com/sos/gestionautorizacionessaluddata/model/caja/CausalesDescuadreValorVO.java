/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Clase que almacena la informacion de descuadre y valor asociada a un movimiento de caja
 * @author GENIAR <jorge.garcia@geniar.net>
 */
public class CausalesDescuadreValorVO implements Serializable {
	private static final long serialVersionUID = -8442837842716517619L;
	private Double valorDescuadre;
	private String usuario;
	private String causalDescuadre;
	public Double getValorDescuadre() {
		return valorDescuadre;
	}
	public void setValorDescuadre(Double valorDescuadre) {
		this.valorDescuadre = valorDescuadre;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCausalDescuadre() {
		return causalDescuadre;
	}
	public void setCausalDescuadre(String causalDescuadre) {
		this.causalDescuadre = causalDescuadre;
	}

}
