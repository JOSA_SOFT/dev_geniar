package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsula la informacion de la tabla del informe de la nota credito
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class InformeOPSVO implements Serializable {
	private static final long serialVersionUID = 4846127107665900299L;
	private String numOPSs;
	private Double valor;

	public String getNumOPSs() {
		return numOPSs;
	}

	public void setNumOPSs(String numOPSs) {
		this.numOPSs = numOPSs;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}