package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Almancena las causales el cual hacen que no sea necesario un pago.
 * 
 * @author GENIAR
 *
 */
public class CausalesExentosVO implements Serializable {
	private static final long serialVersionUID = -3457973515996406012L;
	private int consecutivoCausa;
	private String descripcion;

	public int getConsecutivoCausa() {
		return consecutivoCausa;
	}

	public void setConsecutivoCausa(int consecutivoCausa) {
		this.consecutivoCausa = consecutivoCausa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
