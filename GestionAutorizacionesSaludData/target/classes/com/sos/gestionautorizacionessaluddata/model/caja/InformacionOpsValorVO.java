package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.List;

/**
 * Informaciòn de los codigos ops y valores
 *@author JOSE OLMEDO SOTO AGUIRRE - GENIAR S.A.S
 *
 */
public class InformacionOpsValorVO implements Serializable {
	private static final long serialVersionUID = -3601721826689536676L;
	private int codeOPS;
	private double valueOPS;
	private boolean selected;
	private List<ConceptoPagoOpsVO> listaConceptos;
	private double totalCopago;
	private double totalCuotaModeradora;

	/**
	 * 
	 * @param codeOPS
	 * @param valueOPS
	 */
	public InformacionOpsValorVO(int codeOPS, double valueOPS) {
		this.codeOPS = codeOPS;
		this.valueOPS = valueOPS;
		selected = false;
	}
	
	/**
	 * Constructor por defecto.
	 */
	public InformacionOpsValorVO(){
		//Constructor por defecto sin parametros.
	}
	
	public int getCodeOPS() {
		return codeOPS;
	}

	public void setCodeOPS(int codeOPS) {
		this.codeOPS = codeOPS;
	}

	public double getValueOPS() {
		return valueOPS;
	}

	public void setValueOPS(double valueOPS) {
		this.valueOPS = valueOPS;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public List<ConceptoPagoOpsVO> getListaConceptos() {
		return listaConceptos;
	}

	public void setListaConceptos(List<ConceptoPagoOpsVO> listaConceptos) {
		this.listaConceptos = listaConceptos;
	}

	public double getTotalCopago() {
		return totalCopago;
	}

	public void setTotalCopago(double totalCopago) {
		this.totalCopago = totalCopago;
	}

	public double getTotalCuotaModeradora() {
		return totalCuotaModeradora;
	}

	public void setTotalCuotaModeradora(double totalCuotaModeradora) {
		this.totalCuotaModeradora = totalCuotaModeradora;
	}
} 
