package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.List;

/**
 * Almacena la informacion de las OPS a generar recibo de caja
 * @author GENIAR
 *
 */
public class OPSCajaVO implements Serializable {
	private static final long serialVersionUID = 5217325110295712091L;
	private List<InformacionOpsValorVO> ops;
	private String muestraMensajeIPS;
	private String muestraReciboCaja;
	public List<InformacionOpsValorVO> getOps() {
		return ops;
	}
	public void setOps(List<InformacionOpsValorVO> ops) {
		this.ops = ops;
	}
	public String getMuestraMensajeIPS() {
		return muestraMensajeIPS;
	}
	public void setMuestraMensajeIPS(String muestraMensajeIPS) {
		this.muestraMensajeIPS = muestraMensajeIPS;
	}
	public String getMuestraReciboCaja() {
		return muestraReciboCaja;
	}
	public void setMuestraReciboCaja(String muestraReciboCaja) {
		this.muestraReciboCaja = muestraReciboCaja;
	}
}
