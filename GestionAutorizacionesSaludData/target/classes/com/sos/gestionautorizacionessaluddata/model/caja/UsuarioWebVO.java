package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Usuario Web
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class UsuarioWebVO extends InformacionRegistroVO implements Serializable{
	private static final long serialVersionUID = -5749164156070408899L;
	private String lgnUsro;
	private String psswrdUsro;
	private String dscrpcnUsro;
	private Integer cnsctvoCdgoOfcna;
	private String eml;
	private Integer cnsctvoCdgoTpoIdntfccnPrstdr;
	private String nmroIdntfccnPrstdr;
	private String cdgoIntrno;
	private String nombreCompleto;

	public String getLgnUsro() {
		return lgnUsro;
	}

	public void setLgnUsro(String lgnUsro) {
		this.lgnUsro = lgnUsro;
	}

	public String getPsswrdUsro() {
		return psswrdUsro;
	}

	public void setPsswrdUsro(String psswrdUsro) {
		this.psswrdUsro = psswrdUsro;
	}

	public String getDscrpcnUsro() {
		return dscrpcnUsro;
	}

	public void setDscrpcnUsro(String dscrpcnUsro) {
		this.dscrpcnUsro = dscrpcnUsro;
	}

	public Integer getCnsctvoCdgoOfcna() {
		return cnsctvoCdgoOfcna;
	}

	public void setCnsctvoCdgoOfcna(Integer cnsctvoCdgoOfcna) {
		this.cnsctvoCdgoOfcna = cnsctvoCdgoOfcna;
	}

	public String getEml() {
		return eml;
	}

	public void setEml(String eml) {
		this.eml = eml;
	}

	public Integer getCnsctvoCdgoTpoIdntfccnPrstdr() {
		return cnsctvoCdgoTpoIdntfccnPrstdr;
	}

	public void setCnsctvoCdgoTpoIdntfccnPrstdr(
			Integer cnsctvoCdgoTpoIdntfccnPrstdr) {
		this.cnsctvoCdgoTpoIdntfccnPrstdr = cnsctvoCdgoTpoIdntfccnPrstdr;
	}

	public String getNmroIdntfccnPrstdr() {
		return nmroIdntfccnPrstdr;
	}

	public void setNmroIdntfccnPrstdr(String nmroIdntfccnPrstdr) {
		this.nmroIdntfccnPrstdr = nmroIdntfccnPrstdr;
	}

	public String getCdgoIntrno() {
		return cdgoIntrno;
	}

	public void setCdgoIntrno(String cdgoIntrno) {
		this.cdgoIntrno = cdgoIntrno;
	}

	public String getNombreCompleto() {
		nombreCompleto = getPrmrNmbreUsro().trim() + " "
				+ (getSgndoNmbreUsro() == null ? "" : getSgndoNmbreUsro().trim()) + " "
				+ getPrmrAplldoUsro().trim() + " "
				+ (getSgndoAplldoUsro() == null ? "" : getSgndoAplldoUsro().trim());
		return nombreCompleto.trim();
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
}