package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase para encapsular movimientos de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class MovimientoCajaVO extends InformacionRegistroVO implements Serializable{
	private static final long serialVersionUID = -5045263782650979372L;
	private Integer cnsctvoMvmntoCja;
	private Double bse;
	private Date fchaAprtra;
	private Date fchaCrre;
	private Integer cnsctvoCdgoOfcna;
	private Integer cnsctvoCdgoEstdo;
	private String dscrpcnEstdo;
	private String drccnIp;
	private Double fltnteCrre;
	private Double sbrnteCrre;
	private Double ttlEfctvo;
	private Double ttlCja;
	private Double ttlNtaCrdto;
	private Integer cnsctvoCrreOfcna;
	private Double totalRecibido;

	public Integer getCnsctvoMvmntoCja() {
		return cnsctvoMvmntoCja;
	}

	public void setCnsctvoMvmntoCja(Integer cnsctvoMvmntoCja) {
		this.cnsctvoMvmntoCja = cnsctvoMvmntoCja;
	}

	public Double getBse() {
		return bse;
	}

	public void setBse(Double bse) {
		this.bse = bse;
	}

	public Date getFchaAprtra() {
		return fchaAprtra;
	}

	public void setFchaAprtra(Date fchaAprtra) {
		this.fchaAprtra = fchaAprtra;
	}

	public Date getFchaCrre() {
		return fchaCrre;
	}

	public void setFchaCrre(Date fchaCrre) {
		this.fchaCrre = fchaCrre;
	}

	public Integer getCnsctvoCdgoOfcna() {
		return cnsctvoCdgoOfcna;
	}

	public void setCnsctvoCdgoOfcna(Integer cnsctvoCdgoOfcna) {
		this.cnsctvoCdgoOfcna = cnsctvoCdgoOfcna;
	}

	public Integer getCnsctvoCdgoEstdo() {
		return cnsctvoCdgoEstdo;
	}

	public void setCnsctvoCdgoEstdo(Integer cnsctvoCdgoEstdo) {
		this.cnsctvoCdgoEstdo = cnsctvoCdgoEstdo;
	}

	public String getDscrpcnEstdo() {
		return dscrpcnEstdo;
	}

	public void setDscrpcnEstdo(String dscrpcnEstdo) {
		this.dscrpcnEstdo = dscrpcnEstdo;
	}

	public String getDrccnIp() {
		return drccnIp;
	}

	public void setDrccnIp(String drccnIp) {
		this.drccnIp = drccnIp;
	}

	public Double getFltnteCrre() {
		return fltnteCrre;
	}

	public void setFltnteCrre(Double fltnteCrre) {
		this.fltnteCrre = fltnteCrre;
	}

	public Double getSbrnteCrre() {
		return sbrnteCrre;
	}

	public void setSbrnteCrre(Double sbrnteCrre) {
		this.sbrnteCrre = sbrnteCrre;
	}

	public Double getTtlEfctvo() {
		return ttlEfctvo;
	}

	public void setTtlEfctvo(Double ttlEfctvo) {
		this.ttlEfctvo = ttlEfctvo;
	}

	public Double getTtlCja() {
		return ttlCja;
	}

	public void setTtlCja(Double ttlCja) {
		this.ttlCja = ttlCja;
	}

	public Double getTtlNtaCrdto() {
		return ttlNtaCrdto;
	}

	public void setTtlNtaCrdto(Double ttlNtaCrdto) {
		this.ttlNtaCrdto = ttlNtaCrdto;
	}

	public Integer getCnsctvoCrreOfcna() {
		return cnsctvoCrreOfcna;
	}

	public void setCnsctvoCrreOfcna(Integer cnsctvoCrreOfcna) {
		this.cnsctvoCrreOfcna = cnsctvoCrreOfcna;
	}

	public Double getTotalRecibido() {
		return totalRecibido;
	}

	public void setTotalRecibido(Double totalRecibido) {
		this.totalRecibido = totalRecibido;
	}
}