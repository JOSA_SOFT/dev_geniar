package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Clase que se encarga de almacenar los datos de los combos
 * @author GENIAR
 *
 */
public class ParametroVO implements Serializable {
	private static final long serialVersionUID = -2401867750269770389L;
	private int consecutivo;
	private String descipcion;
	private String codigo;
	public int getConsecutivo() {
		return consecutivo;
	}
	public void setConsecutivo(int consecutivo) {
		this.consecutivo = consecutivo;
	}
	public String getDescipcion() {
		return descipcion;
	}
	public void setDescipcion(String descipcion) {
		this.descipcion = descipcion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
