/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * @author GENIAR
 *
 */
public class InformacionRegistroVO implements Serializable {
	private static final long serialVersionUID = -1073189677750542497L;
	private Date fchaCrcn;
	private String usroCrcn;
	private Date fchaUltmaMdfccn;
	private String usroUltmaMdfccn;
	private String prmrNmbreUsro;
	private String sgndoNmbreUsro;
	private String prmrAplldoUsro;
	private String sgndoAplldoUsro;
	
	public Date getFchaCrcn() {
		return fchaCrcn;
	}
	public void setFchaCrcn(Date fchaCrcn) {
		this.fchaCrcn = fchaCrcn;
	}
	public String getUsroCrcn() {
		return usroCrcn;
	}
	public void setUsroCrcn(String usroCrcn) {
		this.usroCrcn = usroCrcn;
	}
	public Date getFchaUltmaMdfccn() {
		return fchaUltmaMdfccn;
	}
	public void setFchaUltmaMdfccn(Date fchaUltmaMdfccn) {
		this.fchaUltmaMdfccn = fchaUltmaMdfccn;
	}
	public String getUsroUltmaMdfccn() {
		return usroUltmaMdfccn;
	}
	public void setUsroUltmaMdfccn(String usroUltmaMdfccn) {
		this.usroUltmaMdfccn = usroUltmaMdfccn;
	}
	public String getPrmrNmbreUsro() {
		return prmrNmbreUsro;
	}
	public void setPrmrNmbreUsro(String prmrNmbreUsro) {
		this.prmrNmbreUsro = prmrNmbreUsro;
	}
	public String getSgndoNmbreUsro() {
		return sgndoNmbreUsro;
	}
	public void setSgndoNmbreUsro(String sgndoNmbreUsro) {
		this.sgndoNmbreUsro = sgndoNmbreUsro;
	}
	public String getPrmrAplldoUsro() {
		return prmrAplldoUsro;
	}
	public void setPrmrAplldoUsro(String prmrAplldoUsro) {
		this.prmrAplldoUsro = prmrAplldoUsro;
	}
	public String getSgndoAplldoUsro() {
		return sgndoAplldoUsro;
	}
	public void setSgndoAplldoUsro(String sgndoAplldoUsro) {
		this.sgndoAplldoUsro = sgndoAplldoUsro;
	}
}
