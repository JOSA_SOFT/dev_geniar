package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsula la informacion del coordinador
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class CoordinadorVO extends InformacionRegistroVO implements Serializable {
	private static final long serialVersionUID = 4664424716930714010L;
	private Integer cnsctvoCdgoCrdndrAsi;
	private String cdgoCrdndrAsi;
	private Integer cnsctvoCdgoOfcna;
	private String dscrpcnOfcna;
	private String lgnUsro;
	private String nombreCompleto;

	public Integer getCnsctvoCdgoCrdndrAsi() {
		return cnsctvoCdgoCrdndrAsi;
	}

	public void setCnsctvoCdgoCrdndrAsi(Integer cnsctvoCdgoCrdndrAsi) {
		this.cnsctvoCdgoCrdndrAsi = cnsctvoCdgoCrdndrAsi;
	}

	public String getCdgoCrdndrAsi() {
		return cdgoCrdndrAsi;
	}

	public void setCdgoCrdndrAsi(String cdgoCrdndrAsi) {
		this.cdgoCrdndrAsi = cdgoCrdndrAsi;
	}

	public Integer getCnsctvoCdgoOfcna() {
		return cnsctvoCdgoOfcna;
	}

	public void setCnsctvoCdgoOfcna(Integer cnsctvoCdgoOfcna) {
		this.cnsctvoCdgoOfcna = cnsctvoCdgoOfcna;
	}

	public String getDscrpcnOfcna() {
		return dscrpcnOfcna;
	}

	public void setDscrpcnOfcna(String dscrpcnOfcna) {
		this.dscrpcnOfcna = dscrpcnOfcna;
	}

	public String getLgnUsro() {
		return lgnUsro;
	}

	public void setLgnUsro(String lgnUsro) {
		this.lgnUsro = lgnUsro;
	}

	public String getNombreCompleto() {
		nombreCompleto = getPrmrNmbreUsro().trim() + " "
				+ (getSgndoNmbreUsro() == null ? "" : getSgndoNmbreUsro().trim()) + " "
				+ getPrmrAplldoUsro().trim() + " "
				+ (getSgndoAplldoUsro() == null ? "" : getSgndoAplldoUsro().trim());
		return nombreCompleto.trim();
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
}