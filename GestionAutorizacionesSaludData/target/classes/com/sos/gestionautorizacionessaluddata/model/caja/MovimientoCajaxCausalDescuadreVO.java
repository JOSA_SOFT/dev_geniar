package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Encapsula las causales de descuadre por movimiento caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class MovimientoCajaxCausalDescuadreVO implements Serializable {
	private static final long serialVersionUID = -4168901846211089868L;
	private Integer cnsctvoCdgoMvmntoCjaxCslDscdre;
	private Integer cnsctvoMvmntoCja;
	private Integer cnsctvoCdgoCslCscdre;
	private Double vlrDscdre;
	private Date fchaCrcn;
	private String usroCrcn;

	public Integer getCnsctvoCdgoMvmntoCjaxCslDscdre() {
		return cnsctvoCdgoMvmntoCjaxCslDscdre;
	}

	public void setCnsctvoCdgoMvmntoCjaxCslDscdre(
			Integer cnsctvoCdgoMvmntoCjaxCslDscdre) {
		this.cnsctvoCdgoMvmntoCjaxCslDscdre = cnsctvoCdgoMvmntoCjaxCslDscdre;
	}

	public Integer getCnsctvoMvmntoCja() {
		return cnsctvoMvmntoCja;
	}

	public void setCnsctvoMvmntoCja(Integer cnsctvoMvmntoCja) {
		this.cnsctvoMvmntoCja = cnsctvoMvmntoCja;
	}

	public Integer getCnsctvoCdgoCslCscdre() {
		return cnsctvoCdgoCslCscdre;
	}

	public void setCnsctvoCdgoCslCscdre(Integer cnsctvoCdgoCslCscdre) {
		this.cnsctvoCdgoCslCscdre = cnsctvoCdgoCslCscdre;
	}

	public Double getVlrDscdre() {
		return vlrDscdre;
	}

	public void setVlrDscdre(Double vlrDscdre) {
		this.vlrDscdre = vlrDscdre;
	}

	public Date getFchaCrcn() {
		return fchaCrcn;
	}

	public void setFchaCrcn(Date fchaCrcn) {
		this.fchaCrcn = fchaCrcn;
	}

	public String getUsroCrcn() {
		return usroCrcn;
	}

	public void setUsroCrcn(String usroCrcn) {
		this.usroCrcn = usroCrcn;
	}
}