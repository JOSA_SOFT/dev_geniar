package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que almacena el resultado del reporte
 * @author GENIAR
 *
 */
public class InformeNotasCreditoVO implements Serializable {
	private static final long serialVersionUID = 8098387729409922099L;
	private int consecutivoCodigoDocumento;
	private int numeroDocumento;
	private Date fechaDocumento;
	private String estadoDocumento;
	private int nuiAfililado;
	private Double valorDocumento;
	private String nombreCompletoAfiliado;
	private String tipoIdentificacionAfiliado;
	private String numeroIdentificacionAfiliado;
	private int consecutivoCodigoTipoDocumentoAfiliado;
	private int consecutivoEstadoDocumento;
	private int consecutivoContacto;
	private String observacion;
	
	public int getConsecutivoCodigoDocumento() {
		return consecutivoCodigoDocumento;
	}
	public void setConsecutivoCodigoDocumento(int consecutivoCodigoDocumento) {
		this.consecutivoCodigoDocumento = consecutivoCodigoDocumento;
	}
	public int getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(int numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public Date getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getEstadoDocumento() {
		return estadoDocumento;
	}
	public void setEstadoDocumento(String estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}
	public int getNuiAfililado() {
		return nuiAfililado;
	}
	public void setNuiAfililado(int nuiAfililado) {
		this.nuiAfililado = nuiAfililado;
	}
	public Double getValorDocumento() {
		return valorDocumento;
	}
	public void setValorDocumento(Double valorDocumento) {
		this.valorDocumento = valorDocumento;
	}
	public String getNombreCompletoAfiliado() {
		return nombreCompletoAfiliado;
	}
	public void setNombreCompletoAfiliado(String nombreCompletoAfiliado) {
		this.nombreCompletoAfiliado = nombreCompletoAfiliado;
	}
	public String getTipoIdentificacionAfiliado() {
		return tipoIdentificacionAfiliado;
	}
	public void setTipoIdentificacionAfiliado(String tipoIdentificacionAfiliado) {
		this.tipoIdentificacionAfiliado = tipoIdentificacionAfiliado;
	}
	public String getNumeroIdentificacionAfiliado() {
		return numeroIdentificacionAfiliado;
	}
	public void setNumeroIdentificacionAfiliado(String numeroIdentificacionAfiliado) {
		this.numeroIdentificacionAfiliado = numeroIdentificacionAfiliado;
	}
	public int getConsecutivoEstadoDocumento() {
		return consecutivoEstadoDocumento;
	}
	public void setConsecutivoEstadoDocumento(int consecutivoEstadoDocumento) {
		this.consecutivoEstadoDocumento = consecutivoEstadoDocumento;
	}
	public int getConsecutivoCodigoTipoDocumentoAfiliado() {
		return consecutivoCodigoTipoDocumentoAfiliado;
	}
	public void setConsecutivoCodigoTipoDocumentoAfiliado(
			int consecutivoCodigoTipoDocumentoAfiliado) {
		this.consecutivoCodigoTipoDocumentoAfiliado = consecutivoCodigoTipoDocumentoAfiliado;
	}
	public int getConsecutivoContacto() {
		return consecutivoContacto;
	}
	public void setConsecutivoContacto(int consecutivoContacto) {
		this.consecutivoContacto = consecutivoContacto;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
