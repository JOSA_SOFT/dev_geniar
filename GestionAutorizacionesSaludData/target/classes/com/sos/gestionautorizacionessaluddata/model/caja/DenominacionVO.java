package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsulador de denominaciones
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class DenominacionVO implements Serializable{
	private static final long serialVersionUID = 2384970392664336136L;
	private Double denominacion;
	private Integer cantidad;
	/**
	 * Propiedad para reporte;
	 */
	private Double valor;

	/**
	 * Constructor vacio
	 */
	public DenominacionVO() {
		super();
		denominacion = 0.0;
		cantidad = 0;
		valor = 0.0;
	}

	/**
	 * Constructor con todos los parametros
	 * 
	 * @param denominacion
	 * @param cantidad
	 */
	public DenominacionVO(Double denominacion, Integer cantidad) {
		super();
		this.denominacion = denominacion;
		this.cantidad = cantidad;
	}

	public Double getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(Double denominacion) {
		this.denominacion = denominacion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getValor() {
		valor = (cantidad == null ? 0 : cantidad.doubleValue())
				* (denominacion == null ? 0.0 : denominacion);
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}