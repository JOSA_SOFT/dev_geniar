package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Objeto que manejara el resultado del reporte general recibo caja
 * @author GENIAR
 */
public class InformeGeneralReciboVO implements Serializable {
	private static final long serialVersionUID = 3435464556224431449L;
	private Date fechaCierre;
	private Date fechaApertura;
	private Double base;
	private Double totalEfectivo;
	private Double sobranteCierre;
	private Double faltanteCierre;
	private int consecutivoMovimientoCaja;
	private Double totalCaja;
	private String descripcionOficina;
	private String nombreAsi;
	private String usuario;
	
	public Date getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public Date getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public Double getBase() {
		return base;
	}
	public void setBase(Double base) {
		this.base = base;
	}
	public Double getTotalEfectivo() {
		return totalEfectivo;
	}
	public void setTotalEfectivo(Double totalEfectivo) {
		this.totalEfectivo = totalEfectivo;
	}
	public Double getSobranteCierre() {
		return sobranteCierre;
	}
	public void setSobranteCierre(Double sobranteCierre) {
		this.sobranteCierre = sobranteCierre;
	}
	public Double getFaltanteCierre() {
		return faltanteCierre;
	}
	public void setFaltanteCierre(Double faltanteCierre) {
		this.faltanteCierre = faltanteCierre;
	}
	public int getConsecutivoMovimientoCaja() {
		return consecutivoMovimientoCaja;
	}
	public void setConsecutivoMovimientoCaja(int consecutivoMovimientoCaja) {
		this.consecutivoMovimientoCaja = consecutivoMovimientoCaja;
	}
	public Double getTotalCaja() {
		return totalCaja;
	}
	public void setTotalCaja(Double totalCaja) {
		this.totalCaja = totalCaja;
	}
	public String getDescripcionOficina() {
		return descripcionOficina;
	}
	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}
	public String getNombreAsi() {
		return nombreAsi;
	}
	public void setNombreAsi(String nombreAsi) {
		this.nombreAsi = nombreAsi;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
