package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Clase base para enviar al jasper e imprimir el reporte
 * 
 * @author Jose olmedo soto aguirre - Geniar.
 *
 */
public class ReciboCajaJasperVO implements Serializable {
	private static final long serialVersionUID = 7976117140628962887L;

	private List<DocumentoCajaVO> documentos;

	private int idDocumento;
	private String oficina;
	private List<ConceptoPagoOpsVO> conceptosPago;
	private String numeroEnLetra;
	private String usuarioCreador;
	private String identificacionAfiliado;
	private String nombreAfiliado;

	private double totalPagar;
	private double sumatoriaNotasCredito;
	private double valorEfectivo = 0;
	private double valorDevolver = 0;
	
	private Date fechaReporte;
	
	/**
	 * Constructor de la clase
	 */
	public ReciboCajaJasperVO() {
		conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
	}

	/**
	 * @return the documentos
	 */
	public List<DocumentoCajaVO> getDocumentos() {
		return documentos;
	}

	/**
	 * @param documentos
	 *            the documentos to set
	 */
	public void setDocumentos(List<DocumentoCajaVO> documentos) {
		this.documentos = documentos;
	}

	/**
	 * @return the idDocumento
	 */
	public int getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @param idDocumento
	 *            the idDocumento to set
	 */
	public void setIdDocumento(int idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return the oficina
	 */
	public String getOficina() {
		return oficina;
	}

	/**
	 * @param oficina
	 *            the oficina to set
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	/**
	 * @return the numeroEnLetra
	 */
	public String getNumeroEnLetra() {
		return numeroEnLetra;
	}

	/**
	 * @param numeroEnLetra
	 *            the numeroEnLetra to set
	 */
	public void setNumeroEnLetra(String numeroEnLetra) {
		this.numeroEnLetra = numeroEnLetra;
	}

	/**
	 * @return the usuarioCreador
	 */
	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	/**
	 * @param usuarioCreador
	 *            the usuarioCreador to set
	 */
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	/**
	 * @return the identificacionAfiliado
	 */
	public String getIdentificacionAfiliado() {
		return identificacionAfiliado;
	}

	/**
	 * @param identificacionAfiliado
	 *            the identificacionAfiliado to set
	 */
	public void setIdentificacionAfiliado(String identificacionAfiliado) {
		this.identificacionAfiliado = identificacionAfiliado;
	}

	/**
	 * @return the nombreAfiliado
	 */
	public String getNombreAfiliado() {
		return nombreAfiliado;
	}

	/**
	 * @param nombreAfiliado
	 *            the nombreAfiliado to set
	 */
	public void setNombreAfiliado(String nombreAfiliado) {
		this.nombreAfiliado = nombreAfiliado;
	}

	/**
	 * @return the totalPagar
	 */
	public double getTotalPagar() {
		return totalPagar;
	}

	/**
	 * @param totalPagar
	 *            the totalPagar to set
	 */
	public void setTotalPagar(double totalPagar) {
		this.totalPagar = totalPagar;
	}

	/**
	 * @return the sumatoriaNotasCredito
	 */
	public double getSumatoriaNotasCredito() {
		return sumatoriaNotasCredito;
	}

	/**
	 * @param sumatoriaNotasCredito
	 *            the sumatoriaNotasCredito to set
	 */
	public void setSumatoriaNotasCredito(double sumatoriaNotasCredito) {
		this.sumatoriaNotasCredito = sumatoriaNotasCredito;
	}

	/**
	 * @return the valorEfectivo
	 */
	public double getValorEfectivo() {
		return valorEfectivo;
	}

	/**
	 * @param valorEfectivo
	 *            the valorEfectivo to set
	 */
	public void setValorEfectivo(double valorEfectivo) {
		this.valorEfectivo = valorEfectivo;
	}

	/**
	 * @return the valorDevolver
	 */
	public double getValorDevolver() {
		return valorDevolver;
	}

	/**
	 * @param valorDevolver
	 *            the valorDevolver to set
	 */
	public void setValorDevolver(double valorDevolver) {
		this.valorDevolver = valorDevolver;
	}

	public List<ConceptoPagoOpsVO> getConceptosPago() {
		return conceptosPago;
	}

	public void setConceptosPago(List<ConceptoPagoOpsVO> conceptosPago) {
		this.conceptosPago = conceptosPago;
	}

	public Date getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}


}
