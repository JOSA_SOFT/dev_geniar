package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Esta Clase define los campos para un documento de soporte.
 *
 * @author camoreno
 * @version 1.0 06/11/2015
 */
public class DocumentoSoporteVO implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6192870791341871236L;

	/** Consecutivo codigo del documento de soporte *. */
	private Long consecutivoDocumento;

	/** Codigo documento de soporte *. */
	private String codigoDocumentoSoporte;

	/** Descripcion del documento de soporte *. */
	private String nombreDocumento;

	/** Fecha de creacion *. */
	private Date fechaDocumento;

	/** Ruta del documento*. */
	private String pathDocumento;

	/** Documento *. */
	private byte[] datosArchivoSoporte;

	/** Usuario de creacion *. */
	private String usuarioCreacion;

	/** Visible al usuario *. */
	private String visibleUsuario;

	/** Extension de los formatos permitidos *. */
	private String formatosPermitidos;

	/** Tamaño maximo *. */
	private String tamanoMaximo;

	/** Es obligatorio *. */
	private String obligatorio;

	/** URL de ejemplo *. */
	private String url;

	/** Aplicacion web *. */
	private String aplicacionWeb;

	/** Cantidad maxima de soportes *. */
	private Integer cantidadMaximaSoportes;

	/** Cantidad temporal de soportes *. */
	private Integer tempCantidadMaximaSoportes;

	/** identifica el tipo de anexo */
	private Integer consecutivoCodigoTipoAnexo;

	/** identifica el codigo de la codificacion */
	private String codigoCodificacionPrestacion;

	/** identifica el numero de autorizacion */
	private Integer consecutivoSolicitudAutorizacionServicio;

	/** identifica el numero de autorizacion */
	private Long consecutivoDocumentoSoporteVisos;

	/** identifica la descripcion del tipo de soporte */
	private String descripcionTipoDocumentoSoporte;

	/**
	 * Gets the consecutivo codigo documento soporte.
	 *
	 * @return the consecutivoCodigoDocumentoSoporte
	 */
	public Long getConsecutivoDocumento() {
		return consecutivoDocumento;
	}

	/**
	 * Sets the consecutivo codigo documento soporte.
	 *
	 * @param consecutivoCodigoDocumentoSoporte
	 *            the consecutivoCodigoDocumentoSoporte to set
	 */
	public void setConsecutivoDocumento(Long consecutivoCodigoDocumentoSoporte) {
		this.consecutivoDocumento = consecutivoCodigoDocumentoSoporte;
	}

	/**
	 * Gets the codigo documento soporte.
	 *
	 * @return the codigoDocumentoSoporte
	 */
	public String getCodigoDocumentoSoporte() {
		return codigoDocumentoSoporte;
	}

	/**
	 * Sets the codigo documento soporte.
	 *
	 * @param codigoDocumentoSoporte
	 *            the codigoDocumentoSoporte to set
	 */
	public void setCodigoDocumentoSoporte(String codigoDocumentoSoporte) {
		this.codigoDocumentoSoporte = codigoDocumentoSoporte;
	}

	/**
	 * Gets the descripcion documento soporte.
	 *
	 * @return the descripcionDocumentoSoporte
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	/**
	 * Sets the descripcion documento soporte.
	 *
	 * @param descripcionDocumentoSoporte
	 *            the descripcionDocumentoSoporte to set
	 */
	public void setNombreDocumento(String descripcionDocumentoSoporte) {
		this.nombreDocumento = descripcionDocumentoSoporte;
	}

	/**
	 * Gets the fecha creacion.
	 *
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaDocumento;
	}

	/**
	 * Sets the fecha creacion.
	 *
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaDocumento = fechaCreacion;
	}

	/**
	 * @return the pathDocumento
	 */
	public String getPathDocumento() {
		return pathDocumento;
	}

	/**
	 * @param pathDocumento
	 *            the pathDocumento to set
	 */
	public void setPathDocumento(String pathDocumento) {
		this.pathDocumento = pathDocumento;
	}

	/**
	 * @return the datosArchivoSoporte
	 */
	public byte[] getDatosArchivoSoporte() {
		return datosArchivoSoporte;
	}

	/**
	 * @param datosArchivoSoporte
	 *            the datosArchivoSoporte to set
	 */
	public void setDatosArchivoSoporte(byte[] datosArchivoSoporte) {
		this.datosArchivoSoporte = datosArchivoSoporte;
	}

	/**
	 * Gets the usuario creacion.
	 *
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	/**
	 * Sets the usuario creacion.
	 *
	 * @param usuarioCreacion
	 *            the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	/**
	 * Gets the visible usuario.
	 *
	 * @return the visibleUsuario
	 */
	public String getVisibleUsuario() {
		return visibleUsuario;
	}

	/**
	 * Sets the visible usuario.
	 *
	 * @param visibleUsuario
	 *            the visibleUsuario to set
	 */
	public void setVisibleUsuario(String visibleUsuario) {
		this.visibleUsuario = visibleUsuario;
	}

	/**
	 * Gets the formatos permitidos.
	 *
	 * @return the formatosPermitidos
	 */
	public String getFormatosPermitidos() {
		return formatosPermitidos;
	}

	/**
	 * Sets the formatos permitidos.
	 *
	 * @param formatosPermitidos
	 *            the formatosPermitidos to set
	 */
	public void setFormatosPermitidos(String formatosPermitidos) {
		this.formatosPermitidos = formatosPermitidos;
	}

	/**
	 * Gets the tamano maximo.
	 *
	 * @return the tamanoMaximo
	 */
	public String getTamanoMaximo() {
		return tamanoMaximo;
	}

	/**
	 * Sets the tamano maximo.
	 *
	 * @param tamanoMaximo
	 *            the tamanoMaximo to set
	 */
	public void setTamanoMaximo(String tamanoMaximo) {
		this.tamanoMaximo = tamanoMaximo;
	}

	/**
	 * Gets the obligatorio.
	 *
	 * @return the obligatorio
	 */
	public String getObligatorio() {
		return obligatorio;
	}

	/**
	 * Sets the obligatorio.
	 *
	 * @param obligatorio
	 *            the obligatorio to set
	 */
	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	/**
	 * Gets the url ejemplo.
	 *
	 * @return the urlEjemplo
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url ejemplo.
	 *
	 * @param url
	 *            the urlEjemplo to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the aplicacion web.
	 *
	 * @return the aplicacionWeb
	 */
	public String getAplicacionWeb() {
		return aplicacionWeb;
	}

	/**
	 * Sets the aplicacion web.
	 *
	 * @param aplicacionWeb
	 *            the aplicacionWeb to set
	 */
	public void setAplicacionWeb(String aplicacionWeb) {
		this.aplicacionWeb = aplicacionWeb;
	}

	/**
	 * Gets the cantidad maxima soportes.
	 *
	 * @return the cantidadMaximaSoportes
	 */
	public Integer getCantidadMaximaSoportes() {
		return cantidadMaximaSoportes;
	}

	/**
	 * Sets the cantidad maxima soportes.
	 *
	 * @param cantidadMaximaSoportes
	 *            the cantidadMaximaSoportes to set
	 */
	public void setCantidadMaximaSoportes(Integer cantidadMaximaSoportes) {
		this.cantidadMaximaSoportes = cantidadMaximaSoportes;
	}

	/**
	 * Gets the temp cantidad maxima soportes.
	 *
	 * @return the tempCantidadMaximaSoportes
	 */
	public Integer getTempCantidadMaximaSoportes() {
		return tempCantidadMaximaSoportes;
	}

	/**
	 * Sets the temp cantidad maxima soportes.
	 *
	 * @param tempCantidadMaximaSoportes
	 *            the tempCantidadMaximaSoportes to set
	 */
	public void setTempCantidadMaximaSoportes(Integer tempCantidadMaximaSoportes) {
		this.tempCantidadMaximaSoportes = tempCantidadMaximaSoportes;
	}

	public Integer getConsecutivoCodigoTipoAnexo() {
		return consecutivoCodigoTipoAnexo;
	}

	public void setConsecutivoCodigoTipoAnexo(Integer consecutivoCodigoTipoAnexo) {
		this.consecutivoCodigoTipoAnexo = consecutivoCodigoTipoAnexo;
	}

	public String getCodigoCodificacionPrestacion() {
		return codigoCodificacionPrestacion;
	}

	public void setCodigoCodificacionPrestacion(String codigoCodificacionPrestacion) {
		this.codigoCodificacionPrestacion = codigoCodificacionPrestacion;
	}

	public Integer getConsecutivoSolicitudAutorizacionServicio() {
		return consecutivoSolicitudAutorizacionServicio;
	}

	public void setConsecutivoSolicitudAutorizacionServicio(Integer consecutivoSolicitudAutorizacionServicio) {
		this.consecutivoSolicitudAutorizacionServicio = consecutivoSolicitudAutorizacionServicio;
	}

	public Long getConsecutivoDocumentoSoporteVisos() {
		return consecutivoDocumentoSoporteVisos;
	}

	public void setConsecutivoDocumentoSoporteVisos(Long consecutivoDocumentoSoporteVisos) {
		this.consecutivoDocumentoSoporteVisos = consecutivoDocumentoSoporteVisos;
	}

	public String getDescripcionTipoDocumentoSoporte() {
		return descripcionTipoDocumentoSoporte;
	}

	public void setDescripcionTipoDocumentoSoporte(String descripcionTipoDocumentoSoporte) {
		this.descripcionTipoDocumentoSoporte = descripcionTipoDocumentoSoporte;
	}
}
