package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;

/**
 * Esta Clase define los campos para un soporte de pre solicitud.
 *
 * @author camoreno
 * @version 1.0 06/11/2015
 */
public class SoporteVO implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6507751659884941442L;


	/** Consecutivo Soporte*. */
	private Long consecutivoSoporte;

	/** Fecha de creacion *. */
	private java.util.Date fechaCreacion;

	/** Usuario de creacion *. */
	private String usuarioCreacion;

	/** Fecha de ultima modificaci�n *. */
	private java.util.Date fechaUltimaModificacion;

	/** Ultimo usuario que realizo una modificacion *. */
	private String usuarioUltimaModificacion;

	/** Nombre del soporte *. */
	private String nombreSoporte;

	/** Ruta fisica *. */
	private String rutaFisica;

	/** Numero unico de identificacion del usuario *. */
	private Integer numeroUnicIdentifUsuario;

	/** Arreglo de byte del archivo a subir *. */
	private byte[] data;

	/** Consecutivo codigo documento soporte *. */
	private Long consecutivoCodigoDocumentoSoporte;

	/** Descripcion del documento de soporte *. */
	private String descripcionDocumentoSoporte;

	/** Documento de soporte *. */
	private DocumentoSoporteVO documentoSoporteVO;

	/** Tamano archivo *. */
	private Integer tamanoArchivo;
	
	/** identifica el codigo de la codificacion */
	private String codigoCodificacionPrestacion;

	private boolean subidoAVisos;
	
	/**
	 * Gets the fecha creacion.
	 *
	 * @return the fechaCreacion
	 */
	public java.util.Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * Sets the fecha creacion.
	 *
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion(java.util.Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * Gets the usuario creacion.
	 *
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	/**
	 * Sets the usuario creacion.
	 *
	 * @param usuarioCreacion
	 *            the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	/**
	 * Gets the fecha ultima modificacion.
	 *
	 * @return the consecutivoSoportePreSolicitud
	 */
	public java.util.Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	/**
	 * Sets the fecha ultima modificacion.
	 *
	 * @param fechaUltimaModificacion
	 *            the fechaUltimaModificacion to set
	 */
	public void setFechaUltimaModificacion(
			java.util.Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	/**
	 * Gets the usuario ultima modificacion.
	 *
	 * @return the consecutivoSoportePreSolicitud
	 */
	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	/**
	 * Sets the usuario ultima modificacion.
	 *
	 * @param usuarioUltimaModificacion
	 *            the usuarioUltimaModificacion to set
	 */
	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	/**
	 * Gets the nombre soporte.
	 *
	 * @return the nombreSoporte
	 */
	public String getNombreSoporte() {
		return nombreSoporte;
	}

	/**
	 * Sets the nombre soporte.
	 *
	 * @param nombreSoporte
	 *            the nombreSoporte to set
	 */
	public void setNombreSoporte(String nombreSoporte) {
		this.nombreSoporte = nombreSoporte;
	}

	/**
	 * Gets the ruta fisica.
	 *
	 * @return the rutaFisica
	 */
	public String getRutaFisica() {
		return rutaFisica;
	}

	/**
	 * Sets the ruta fisica.
	 *
	 * @param rutaFisica
	 *            the rutaFisica to set
	 */
	public void setRutaFisica(String rutaFisica) {
		this.rutaFisica = rutaFisica;
	}

	/**
	 * Gets the numero unic identif usuario.
	 *
	 * @return the numeroUnicIdentifUsuario
	 */
	public Integer getNumeroUnicIdentifUsuario() {
		return numeroUnicIdentifUsuario;
	}

	/**
	 * Sets the numero unic identif usuario.
	 *
	 * @param numeroUnicIdentifUsuario
	 *            the numeroUnicIdentifUsuario to set
	 */
	public void setNumeroUnicIdentifUsuario(Integer numeroUnicIdentifUsuario) {
		this.numeroUnicIdentifUsuario = numeroUnicIdentifUsuario;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *            the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * Gets the consecutivo codigo documento soporte.
	 *
	 * @return the consecutivoCodigoDocumentoSoporte
	 */
	public Long getConsecutivoCodigoDocumentoSoporte() {
		return consecutivoCodigoDocumentoSoporte;
	}

	/**
	 * Sets the consecutivo codigo documento soporte.
	 *
	 * @param consecutivoCodigoDocumentoSoporte
	 *            the consecutivoCodigoDocumentoSoporte to set
	 */
	public void setConsecutivoCodigoDocumentoSoporte(Long consecutivoCodigoDocumentoSoporte) {
		this.consecutivoCodigoDocumentoSoporte = consecutivoCodigoDocumentoSoporte;
	}

	/**
	 * Gets the descripcion documento soporte.
	 *
	 * @return the descripcionDocumentoSoporte
	 */
	public String getDescripcionDocumentoSoporte() {
		return descripcionDocumentoSoporte;
	}

	/**
	 * Sets the descripcion documento soporte.
	 *
	 * @param descripcionDocumentoSoporte
	 *            the descripcionDocumentoSoporte to set
	 */
	public void setDescripcionDocumentoSoporte(
			String descripcionDocumentoSoporte) {
		this.descripcionDocumentoSoporte = descripcionDocumentoSoporte;
	}

	/**
	 * Gets the documento soporte vo.
	 *
	 * @return getDocumentoSoporteVO
	 */
	public DocumentoSoporteVO getDocumentoSoporteVO() {
		return documentoSoporteVO;
	}

	/**
	 * Sets the documento soporte vo.
	 *
	 * @param documentoSoporteVO
	 *            the new documento soporte vo
	 */
	public void setDocumentoSoporteVO(DocumentoSoporteVO documentoSoporteVO) {
		this.documentoSoporteVO = documentoSoporteVO;
	}

	/**
	 * Sets the tamano archivo.
	 *
	 * @param tamanoArchivo
	 *            the new tamano archivo
	 */
	public void setTamanoArchivo(Integer tamanoArchivo) {
		this.tamanoArchivo = tamanoArchivo;
	}

	/**
	 * Gets the tamano archivo.
	 *
	 * @return getTamanoArchivo
	 */
	public Integer getTamanoArchivo() {
		return tamanoArchivo;
	}

	public Long getConsecutivoSoporte() {
		return consecutivoSoporte;
	}

	public void setConsecutivoSoporte(Long consecutivoSoporte) {
		this.consecutivoSoporte = consecutivoSoporte;
	}
	
	public String getCodigoCodificacionPrestacion() {
		return codigoCodificacionPrestacion;
	}

	public void setCodigoCodificacionPrestacion(String codigoCodificacionPrestacion) {
		this.codigoCodificacionPrestacion = codigoCodificacionPrestacion;
	}

	public boolean isSubidoAVisos() {
		return subidoAVisos;
	}

	public void setSubidoAVisos(boolean subidoAVisos) {
		this.subidoAVisos = subidoAVisos;
	}
}