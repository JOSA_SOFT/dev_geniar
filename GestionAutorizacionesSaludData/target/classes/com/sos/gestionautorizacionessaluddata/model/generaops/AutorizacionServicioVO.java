package com.sos.gestionautorizacionessaluddata.model.generaops;

import java.io.Serializable;
import java.util.Date;


/**
 * Class AutorizacionServicioVO
 * Clase VO de la autorizacion del servicio VO
 * @author ing. Victor Hugo Gil Ramos
 * @version 25/04/2016
 *
 */
public class AutorizacionServicioVO implements Serializable{
	private static final long serialVersionUID = 5305039408164199719L;

	/** Identifica el numero unico de autorizacion*/
	private Integer numeroUnicoAutorizacion;	

	/** Identifica el codigo interno prestador autorizado */
	private String codigoInternoPrestadorAutorizado;
	
	/** Identifica el consecutivo de la solicitud*/
	private Integer consecutivoSolicitud;
	
	/** Identifica el valor total*/
	private Integer valorTotal;
	
    /** Identifica el consecutivoCodEstado*/
	private Integer consecutivoCodigoEstadoOPS;
	
	/** Identifica el descripcion del estado de la OPS*/
	private String descripcionCodigoEstadoOPS;
		
	/** Identifica el fechavencimiento*/
	private Date fechaInicio;
	
	/** Identifica el fechavencimiento*/
	private Date fechaVencimiento;
	
	private Date fechaVencimientoHolgura;
	
	private String numRadicadoSolicitud;
	private Integer consecutivoServicioSolicitado;
	private Integer consecutivoCodServicioSolicitado;
	private String codServicioSolicitado;
	private Integer consEstadoPrestacion;
	private boolean valorConceptoCero;
		
	public Integer getNumeroUnicoAutorizacion() {
		return numeroUnicoAutorizacion;
	}

	public void setNumeroUnicoAutorizacion(Integer numeroUnicoAutorizacion) {
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
	}

	public String getCodigoInternoPrestadorAutorizado() {
		return codigoInternoPrestadorAutorizado;
	}

	public void setCodigoInternoPrestadorAutorizado(
			String codigoInternoPrestadorAutorizado) {
		this.codigoInternoPrestadorAutorizado = codigoInternoPrestadorAutorizado;
	}

	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	public Integer getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Integer valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getConsecutivoCodigoEstadoOPS() {
		return consecutivoCodigoEstadoOPS;
	}

	public void setConsecutivoCodigoEstadoOPS(Integer consecutivoCodigoEstadoOPS) {
		this.consecutivoCodigoEstadoOPS = consecutivoCodigoEstadoOPS;
	}

	public String getDescripcionCodigoEstadoOPS() {
		return descripcionCodigoEstadoOPS;
	}

	public void setDescripcionCodigoEstadoOPS(String descripcionCodigoEstadoOPS) {
		this.descripcionCodigoEstadoOPS = descripcionCodigoEstadoOPS;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * @return the numRadicadoSolicitud
	 */
	public String getNumRadicadoSolicitud() {
		return numRadicadoSolicitud;
	}

	/**
	 * @param numRadicadoSolicitud the numRadicadoSolicitud to set
	 */
	public void setNumRadicadoSolicitud(String numRadicadoSolicitud) {
		this.numRadicadoSolicitud = numRadicadoSolicitud;
	}

	/**
	 * @return the consEstadoPrestacion
	 */
	public Integer getConsEstadoPrestacion() {
		return consEstadoPrestacion;
	}

	/**
	 * @param consEstadoPrestacion the consEstadoPrestacion to set
	 */
	public void setConsEstadoPrestacion(Integer consEstadoPrestacion) {
		this.consEstadoPrestacion = consEstadoPrestacion;
	}

	/**
	 * @return the consecutivoServicioSolicitado
	 */
	public Integer getConsecutivoServicioSolicitado() {
		return consecutivoServicioSolicitado;
	}

	/**
	 * @param consecutivoServicioSolicitado the consecutivoServicioSolicitado to set
	 */
	public void setConsecutivoServicioSolicitado(
			Integer consecutivoServicioSolicitado) {
		this.consecutivoServicioSolicitado = consecutivoServicioSolicitado;
	}

	/**
	 * @return the consecutivoCodServicioSolicitado
	 */
	public Integer getConsecutivoCodServicioSolicitado() {
		return consecutivoCodServicioSolicitado;
	}

	/**
	 * @param consecutivoCodServicioSolicitado the consecutivoCodServicioSolicitado to set
	 */
	public void setConsecutivoCodServicioSolicitado(
			Integer consecutivoCodServicioSolicitado) {
		this.consecutivoCodServicioSolicitado = consecutivoCodServicioSolicitado;
	}

	/**
	 * @return the codServicioSolicitado
	 */
	public String getCodServicioSolicitado() {
		return codServicioSolicitado;
	}

	/**
	 * @param codServicioSolicitado the codServicioSolicitado to set
	 */
	public void setCodServicioSolicitado(String codServicioSolicitado) {
		this.codServicioSolicitado = codServicioSolicitado;
	}

	public Date getFechaVencimientoHolgura() {
		return fechaVencimientoHolgura;
	}

	public void setFechaVencimientoHolgura(Date fechaVencimientoHolgura) {
		this.fechaVencimientoHolgura = fechaVencimientoHolgura;
	}

	public boolean isValorConceptoCero() {
		return valorConceptoCero;
	}

	public void setValorConceptoCero(boolean valorConceptoCero) {
		this.valorConceptoCero = valorConceptoCero;
	}	
	
	
	
}
