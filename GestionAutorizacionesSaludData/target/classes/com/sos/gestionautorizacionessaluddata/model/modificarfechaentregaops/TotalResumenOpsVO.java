package com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Class TotalResumenOpsVO
 * Clase VO de Total Resumen OPS
 * @author Ing. German Perez - Geniar S.A.S
 * @version 28/09/2017
 *
 */
@XStreamAlias("rsmnOps")
public class TotalResumenOpsVO implements Serializable {
	private static final long serialVersionUID = 7783490903523865291L;
	@XStreamAlias("cnsctvoVgncaAgrpdrPrstcn")
	private String consecutivoAgrupador;
	private String descripcionAgrupador;
	private Integer cantidadOpsConsultadas;
	private Double valorOpsConsultadas;
	private Float porcentajeCantidadOpsConsultadas;
	private Float porcentajeValorOpsConsultadas;
	private Integer cantidadOpsAModificar;
	private Double valorOpsAModificar;
	private Float porcentajeCantidadOpsAModificar;
	@XStreamAlias("prctjVlrOpsMdfcds")
	private Float porcentajeValorOpsAModificar;
	private String valorOpsConsultadasFormato;
	private String valorOpsAModificarFormato;
	
	public String getConsecutivoAgrupador() {
		return consecutivoAgrupador;
	}

	public void setConsecutivoAgrupador(String consecutivoAgrupador) {
		this.consecutivoAgrupador = consecutivoAgrupador;
	}
	
	public String getDescripcionAgrupador() {
		return descripcionAgrupador;
	}
	
	public void setDescripcionAgrupador(String descripcionAgrupador) {
		this.descripcionAgrupador = descripcionAgrupador;
	}
	
	public Integer getCantidadOpsConsultadas() {
		return cantidadOpsConsultadas;
	}
	
	public void setCantidadOpsConsultadas(Integer cantidadOpsConsultadas) {
		this.cantidadOpsConsultadas = cantidadOpsConsultadas;
	}
	
	public Double getValorOpsConsultadas() {
		return valorOpsConsultadas;
	}
	
	public void setValorOpsConsultadas(Double valorOpsConsultadas) {
		this.valorOpsConsultadas = valorOpsConsultadas;
	}
	
	public Float getPorcentajeCantidadOpsConsultadas() {
		return porcentajeCantidadOpsConsultadas;
	}
	
	public void setPorcentajeCantidadOpsConsultadas(Float porcentajeCantidadOpsConsultadas) {
		this.porcentajeCantidadOpsConsultadas = porcentajeCantidadOpsConsultadas;
	}
	
	public Float getPorcentajeValorOpsConsultadas() {
		return porcentajeValorOpsConsultadas;
	}
	
	public void setPorcentajeValorOpsConsultadas(Float porcentajeValorOpsConsultadas) {
		this.porcentajeValorOpsConsultadas = porcentajeValorOpsConsultadas;
	}
	
	public Integer getCantidadOpsAModificar() {
		return cantidadOpsAModificar;
	}
	
	public void setCantidadOpsAModificar(Integer cantidadOpsAModificar) {
		this.cantidadOpsAModificar = cantidadOpsAModificar;
	}
	
	public Double getValorOpsAModificar() {
		return valorOpsAModificar;
	}
	
	public void setValorOpsAModificar(Double valorOpsAModificar) {
		this.valorOpsAModificar = valorOpsAModificar;
	}
	
	public Float getPorcentajeCantidadOpsAModificar() {
		return porcentajeCantidadOpsAModificar;
	}
	
	public void setPorcentajeCantidadOpsAModificar(Float porcentajeCantidadOpsAModificar) {
		this.porcentajeCantidadOpsAModificar = porcentajeCantidadOpsAModificar;
	}
	
	public Float getPorcentajeValorOpsAModificar() {
		return porcentajeValorOpsAModificar;
	}
	
	public void setPorcentajeValorOpsAModificar(Float porcentajeValorOpsAModificar) {
		this.porcentajeValorOpsAModificar = porcentajeValorOpsAModificar;
	}

	public String getValorOpsConsultadasFormato() {
		return valorOpsConsultadasFormato;
	}

	public void setValorOpsConsultadasFormato(String valorOpsConsultadasFormato) {
		this.valorOpsConsultadasFormato = valorOpsConsultadasFormato;
	}

	public String getValorOpsAModificarFormato() {
		return valorOpsAModificarFormato;
	}

	public void setValorOpsAModificarFormato(String valorOpsAModificarFormato) {
		this.valorOpsAModificarFormato = valorOpsAModificarFormato;
	}

}
