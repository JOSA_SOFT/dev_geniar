package com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class ResumenOpsVO Clase VO de Resumen OPS
 * 
 * @author Ing. German Perez - Geniar S.A.S
 * @version 28/09/2017
 *
 */

public class ColeccionOpsVO implements Serializable {
	private static final long serialVersionUID = 7783490903523865290L;

	/** Lista que se utiliza para almacenar los detalles delas OPS */
	private List<DetalleOpsVO> lDetalleOps;

	/** Mapa para el manejo de los totales */
	private Map<Integer, String> mTotalResumenOps;

	private List<TotalResumenOpsVO> lTotalResumenOps;

	private int totalOps;

	private Double valorOpsConsultadas;

	private Float porcentajeCantidadOpsConsultadas;

	private String xmlOps;

	private Integer cantidadOpsModicadas;
	private Double valorOpsModificadas;

	private Double valorBase;
	private Double valorBaseTmp;

	public void setValorBaseTmp(Double valorBaseTmp) {
		this.valorBaseTmp = valorBaseTmp;
	}

	public Double getValorBaseTmp() {
		if (valorBaseTmp == null)
			return ConstantesData.VALOR_CERO_D;
		return valorBaseTmp;
	}

	public void setValorBase(Double valorBase) {
		this.valorBase = valorBase;
	}

	public Double getValorBase() {
		if (valorBase == null)
			return ConstantesData.VALOR_CERO_D;
		return valorBase;
	}

	public List<DetalleOpsVO> getlDetalleOps() {
		return lDetalleOps;
	}

	public void setlDetalleOps(List<DetalleOpsVO> lDetalleOps) {
		this.lDetalleOps = lDetalleOps;
	}

	public Map<Integer, String> getmTotalResumenOps() {
		return mTotalResumenOps;
	}

	public void setmTotalResumenOps(Map<Integer, String> mTotalResumenOps) {
		this.mTotalResumenOps = mTotalResumenOps;
	}

	public List<TotalResumenOpsVO> getlTotalResumenOps() {
		return lTotalResumenOps;
	}

	public void setlTotalResumenOps(List<TotalResumenOpsVO> lTotalResumenOps) {
		this.lTotalResumenOps = lTotalResumenOps;
	}

	public int getTotalOps() {
		return totalOps;
	}

	public void setTotalOps(int totalOps) {
		this.totalOps = totalOps;
	}

	public Double getValorOpsConsultadas() {
		if (valorOpsConsultadas == null)
			return ConstantesData.VALOR_CERO_D;
		return valorOpsConsultadas;
	}

	public void setValorOpsConsultadas(Double valorOpsConsultadas) {
		this.valorOpsConsultadas = valorOpsConsultadas;
	}

	public Float getPorcentajeCantidadOpsConsultadas() {
		if (porcentajeCantidadOpsConsultadas == null)
			return (float) ConstantesData.VALOR_CERO;
		return porcentajeCantidadOpsConsultadas;
	}

	public void setPorcentajeCantidadOpsConsultadas(Float porcentajeCantidadOpsConsultadas) {
		this.porcentajeCantidadOpsConsultadas = porcentajeCantidadOpsConsultadas;
	}

	public String getXmlOps() {
		return xmlOps;
	}

	public void setXmlOps(String xmlOps) {
		this.xmlOps = xmlOps;
	}

	public Integer getCantidadOpsModicadas() {
		if (cantidadOpsModicadas == null)
			return ConstantesData.VALOR_CERO;
		return cantidadOpsModicadas;
	}

	public void setCantidadOpsModicadas(Integer cantidadOpsModicadas) {
		this.cantidadOpsModicadas = cantidadOpsModicadas;
	}

	public Double getValorOpsModificadas() {
		if (valorOpsModificadas == null)
			return ConstantesData.VALOR_CERO_D;
		return valorOpsModificadas;
	}

	public void setValorOpsModificadas(Double valorOpsModificadas) {

		this.valorOpsModificadas = valorOpsModificadas;
	}
}
