package com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops;

import java.io.Serializable;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Class DetalleOpsVO
 * Clase VO de detalle OPS
 * @author Ing. German Perez - Geniar S.A.S
 * @version 28/09/2017
 *
 */
@XStreamAlias("detOPS")
public class DetalleOpsVO implements Serializable {
	private static final long serialVersionUID = 7783490903523865290L;
	
	@XStreamAlias("tpo_idntfccn")
	private String tipoIdentificacion;
	@XStreamAlias("nmro_idntfccn")
	private String numeroIdentificacion;
	@XStreamAlias("pln")
	private String plan;
	@XStreamAlias("nmbre_aplldo")
	private String nombreApellido;
	@XStreamAlias("nmro_ops")
	private String numeroOps;
	@XStreamAlias("nuam")
	private Integer nuam;
	@XStreamAlias("cnsctvo_cdgo_ofcna")
	private Integer consecutivoCodigoOficina;
	@XStreamAlias("cdgo_prstcn")
	private String codigoPrestacion;
	@XStreamAlias("dscrpcn_prstcn")
	private String descripcionPrestacion;
	@XStreamAlias("cdgo_grpo_entrga")
	private String codigoGrupoEntrega;
	private String descripcionGrupoEntrega;
	private String tutela;
	private String pac;
	private String incapacitado;
	private String estadoMega;
	private String estadoSipres;
	private Date fechaCreacion;
	private String riesgoAfiliado;
	private Date fechaEstimadaEntrega;
	private String codigoCodificacion;
	private String descripcionCodificacion;
	private String planPac;
	private String incapacidad;
	private String descripcionEstado;
	@XStreamAlias("cdgo_agrpdr")
	private Integer consecutivoAgrupador;
	@XStreamAlias("cnsctvo_prstcn")
	private Integer consecutivoPrestacion;
	private String descripcionAgrupador;
	@XStreamAlias("vlr_ops")
	private Double valorOps;
	private String valorOpsFormato;
	private Date fechaExpedicion;
	private String descripcionClasificacionEvento;
	private String descripcionPlan;
	
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	public String getPlan() {
		return plan;
	}
	
	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public Integer getNuam() {
		return nuam;
	}
	
	public void setNuam(Integer nuam) {
		this.nuam = nuam;
	}
	
	public Integer getConsecutivoCodigoOficina() {
		return consecutivoCodigoOficina;
	}
	
	public void setConsecutivoCodigoOficina(Integer consecutivoCodigoOficina) {
		this.consecutivoCodigoOficina = consecutivoCodigoOficina;
	}
	
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}
	
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}
	
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}
	
	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}
	
	public String getCodigoGrupoEntrega() {
		return codigoGrupoEntrega;
	}
	
	public void setCodigoGrupoEntrega(String codigoGrupoEntrega) {
		this.codigoGrupoEntrega = codigoGrupoEntrega;
	}
	
	public String getDescripcionGrupoEntrega() {
		return descripcionGrupoEntrega;
	}
	
	public void setDescripcionGrupoEntrega(String descripcionGrupoEntrega) {
		this.descripcionGrupoEntrega = descripcionGrupoEntrega;
	}
	
	public String getTutela() {
		return tutela;
	}
	
	public void setTutela(String tutela) {
		this.tutela = tutela;
	}
	
	public String getPac() {
		return pac;
	}
	
	public void setPac(String pac) {
		this.pac = pac;
	}
	
	public String getIncapacitado() {
		return incapacitado;
	}
	
	public void setIncapacitado(String incapacitado) {
		this.incapacitado = incapacitado;
	}
	
	public String getEstadoMega() {
		return estadoMega;
	}
	
	public void setEstadoMega(String estadoMega) {
		this.estadoMega = estadoMega;
	}
	
	public String getEstadoSipres() {
		return estadoSipres;
	}
	
	public void setEstadoSipres(String estadoSipres) {
		this.estadoSipres = estadoSipres;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public String getRiesgoAfiliado() {
		return riesgoAfiliado;
	}
	
	public void setRiesgoAfiliado(String riesgoAfiliado) {
		this.riesgoAfiliado = riesgoAfiliado;
	}
	
	public Date getFechaEstimadaEntrega() {
		return fechaEstimadaEntrega;
	}
	
	public void setFechaEstimadaEntrega(Date fechaEstimadaEntrega) {
		this.fechaEstimadaEntrega = fechaEstimadaEntrega;
	}

	public String getCodigoCodificacion() {
		return codigoCodificacion;
	}

	public void setCodigoCodificacion(String codigoCodificacion) {
		this.codigoCodificacion = codigoCodificacion;
	}

	public String getDescripcionCodificacion() {
		return descripcionCodificacion;
	}

	public void setDescripcionCodificacion(String descripcionCodificacion) {
		this.descripcionCodificacion = descripcionCodificacion;
	}

	public String getPlanPac() {
		return planPac;
	}

	public void setPlanPac(String planPac) {
		this.planPac = planPac;
	}

	public String getIncapacidad() {
		return incapacidad;
	}

	public void setIncapacidad(String incapacidad) {
		this.incapacidad = incapacidad;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	public Integer getConsecutivoAgrupador() {
		return consecutivoAgrupador;
	}

	public void setConsecutivoAgrupador(Integer consecutivoAgrupador) {
		this.consecutivoAgrupador = consecutivoAgrupador;
	}

	public String getDescripcionAgrupador() {
		return descripcionAgrupador;
	}

	public void setDescripcionAgrupador(String descripcionAgrupador) {
		this.descripcionAgrupador = descripcionAgrupador;
	}

	public Double getValorOps() {
		return valorOps;
	}

	public void setValorOps(Double valorOps) {
		this.valorOps = valorOps;
	}

	public String getNumeroOps() {
		return numeroOps;
	}

	public void setNumeroOps(String numeroOps) {
		this.numeroOps = numeroOps;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public String getDescripcionClasificacionEvento() {
		return descripcionClasificacionEvento;
	}

	public void setDescripcionClasificacionEvento(String descripcionClasificacionEvento) {
		this.descripcionClasificacionEvento = descripcionClasificacionEvento;
	}

	public String getDescripcionPlan() {
		return descripcionPlan;
	}

	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}

	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	public String getValorOpsFormato() {
		return valorOpsFormato;
	}

	public void setValorOpsFormato(String valorOpsFormato) {
		this.valorOpsFormato = valorOpsFormato;
	}
}
