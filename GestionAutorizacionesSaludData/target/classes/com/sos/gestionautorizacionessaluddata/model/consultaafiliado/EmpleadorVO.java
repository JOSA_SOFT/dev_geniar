package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;

import java.io.Serializable;
import java.util.Date;

/**
 * Class EmpleadorVO
 * Clase VO del empleador del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class EmpleadorVO implements Serializable {
	private static final long serialVersionUID = -89461828986832301L;

	/*Identifica el tipo de identificacion del empleador*/
	private TiposIdentificacionVO tiposIdentificacionVO;
	
	/*Identifica el numero de identificacion del empleador*/
	private String numeroIdentificacionEmpleador;
	
	/*Identifica la razon social del empleador*/
	private String razonSocialEmpleador;
	
	/*Identifica la identificacion completa concatenada tipo y numero*/
	private String identificacionCompletaEmpleador;
	
	/*Identifica numero unico de identificacion del empleador*/
	private Integer numeroUnicoIdentificacionAportante;
	
	/*Identifica si el empleador es el principal*/
	private String empleadorPrincipal;
	
	/*Identifica el consecutivo de la sucursal del empleador */
	private Integer consecutivoSucursalAportante;
	
	/*Identifica el consecutivo dell producto de la sucursal del empleador */
	private Integer consecutivoProductoSucursal;
	
	/*Identifica la direccion del empleador */
	private String direccion;
	
	/*Identifica el telefono del empleador */
	private String telefono;
	
	/*Identifica la actividad economica del empleador */
	private ActividadEconomicaVO actividadEconomicaVO;
	
	/*Identifica el tipo de cotizante del empleado */
	private TipoCotizanteVO tipoCotizanteVO;	
	
	/*Identifica el cargo del empleado */
	private CargoVO cargoVO;	
	
	/*Identifica la ciudad a la que pertenece el empleador */
	private CiudadVO ciudadVO;
	
	/*Identifica el departamento al que pertenece el empleador */
	private DepartamentoVO departamentoVO;
	
	/*Identifica la entidad ARP a la pertenece el empleado */
	private EntidadVO entidadVO;	
	
	/*Identifica el email del empleador */
	private String emlEmpleador;
	
	/*Identifica el inicio de vigencia de la cobranza */
	private Date inicioVigenciaCobranza;
	
	/*Identifica la fecha fin de vigencia de la cobranza */
	private Date finVigenciaCobranza;
		
	/*Identifica el texto concatenado del copago cuota moderadora*/
	private String textoCopagoTextoCuotaModeradora;
	
	
	public TiposIdentificacionVO getTiposIdentificacionVO() {
		return tiposIdentificacionVO;
	}

	public void setTiposIdentificacionVO(TiposIdentificacionVO tiposIdentificacionVO) {
		this.tiposIdentificacionVO = tiposIdentificacionVO;
	}

	public String getNumeroIdentificacionEmpleador() {
		return numeroIdentificacionEmpleador;
	}

	public void setNumeroIdentificacionEmpleador(
			String numeroIdentificacionEmpleador) {
		this.numeroIdentificacionEmpleador = numeroIdentificacionEmpleador;
	}

	public String getRazonSocialEmpleador() {
		return razonSocialEmpleador;
	}

	public void setRazonSocialEmpleador(String razonSocialEmpleador) {
		this.razonSocialEmpleador = razonSocialEmpleador;
	}

	public String getIdentificacionCompletaEmpleador() {
		return identificacionCompletaEmpleador;
	}

	public void setIdentificacionCompletaEmpleador(
			String identificacionCompletaEmpleador) {
		this.identificacionCompletaEmpleador = identificacionCompletaEmpleador;
	}

	public Integer getNumeroUnicoIdentificacionAportante() {
		return numeroUnicoIdentificacionAportante;
	}

	public void setNumeroUnicoIdentificacionAportante(
			Integer numeroUnicoIdentificacionAportante) {
		this.numeroUnicoIdentificacionAportante = numeroUnicoIdentificacionAportante;
	}

	public String getEmpleadorPrincipal() {
		return empleadorPrincipal;
	}

	public void setEmpleadorPrincipal(String empleadorPrincipal) {
		this.empleadorPrincipal = empleadorPrincipal;
	}

	public Integer getConsecutivoSucursalAportante() {
		return consecutivoSucursalAportante;
	}

	public void setConsecutivoSucursalAportante(Integer consecutivoSucursalAportante) {
		this.consecutivoSucursalAportante = consecutivoSucursalAportante;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public ActividadEconomicaVO getActividadEconomicaVO() {
		return actividadEconomicaVO;
	}

	public void setActividadEconomicaVO(ActividadEconomicaVO actividadEconomicaVO) {
		this.actividadEconomicaVO = actividadEconomicaVO;
	}

	public TipoCotizanteVO getTipoCotizanteVO() {
		return tipoCotizanteVO;
	}

	public void setTipoCotizanteVO(TipoCotizanteVO tipoCotizanteVO) {
		this.tipoCotizanteVO = tipoCotizanteVO;
	}

	public CargoVO getCargoVO() {
		return cargoVO;
	}

	public void setCargoVO(CargoVO cargoVO) {
		this.cargoVO = cargoVO;
	}

	public CiudadVO getCiudadVO() {
		return ciudadVO;
	}

	public void setCiudadVO(CiudadVO ciudadVO) {
		this.ciudadVO = ciudadVO;
	}

	public DepartamentoVO getDepartamentoVO() {
		return departamentoVO;
	}

	public void setDepartamentoVO(DepartamentoVO departamentoVO) {
		this.departamentoVO = departamentoVO;
	}

	public EntidadVO getEntidadVO() {
		return entidadVO;
	}

	public void setEntidadVO(EntidadVO entidadVO) {
		this.entidadVO = entidadVO;
	}

	public String getEmlEmpleador() {
		return emlEmpleador;
	}

	public void setEmlEmpleador(String emlEmpleador) {
		this.emlEmpleador = emlEmpleador;
	}	
	
	public Date getInicioVigenciaCobranza() {
		return inicioVigenciaCobranza;
	}

	public void setInicioVigenciaCobranza(Date inicioVigenciaCobranza) {
		this.inicioVigenciaCobranza = inicioVigenciaCobranza;
	}

	public Date getFinVigenciaCobranza() {
		return finVigenciaCobranza;
	}

	public void setFinVigenciaCobranza(Date finVigenciaCobranza) {
		this.finVigenciaCobranza = finVigenciaCobranza;
	}
	
	public Integer getConsecutivoProductoSucursal() {
		return consecutivoProductoSucursal;
	}

	public void setConsecutivoProductoSucursal(Integer consecutivoProductoSucursal) {
		this.consecutivoProductoSucursal = consecutivoProductoSucursal;
	}
	
	public String getTextoCopagoTextoCuotaModeradora() {
		return textoCopagoTextoCuotaModeradora;
	}

	public void setTextoCopagoTextoCuotaModeradora(
			String textoCopago_textoCuotaModeradora) {
		this.textoCopagoTextoCuotaModeradora = textoCopago_textoCuotaModeradora;
	}	
}
