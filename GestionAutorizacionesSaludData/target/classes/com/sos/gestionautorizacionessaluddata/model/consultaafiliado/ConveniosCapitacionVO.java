package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.List;


/**
 * Class ConvenioCapitacionVO
 * Clase VO para la informacion de cuota moderadora y copago del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class ConveniosCapitacionVO implements Serializable {
	private static final long serialVersionUID = 7483554108116935351L;

	/*Identifica el consecuti del convenio.*/
	private Integer consecutivoConvenio;	

	/*Identifica la descripcion del convenio.*/
	private String descripcionConvenio;
	
	/*Identifica si el convenio se capita.*/
	private String capita;
	
	private List<ServiciosCapitadosVO> lServiciosCapitados;
	
	
	public Integer getConsecutivoConvenio() {
		return consecutivoConvenio;
	}

	public void setConsecutivoConvenio(Integer consecutivoConvenio) {
		this.consecutivoConvenio = consecutivoConvenio;
	}

	public String getDescripcionConvenio() {
		return descripcionConvenio;
	}

	public void setDescripcionConvenio(String descripcionConvenio) {
		this.descripcionConvenio = descripcionConvenio;
	}

	public String getCapita() {
		return capita;
	}

	public void setCapita(String capita) {
		this.capita = capita;
	}
	
	public List<ServiciosCapitadosVO> getlServiciosCapitados() {
		return lServiciosCapitados;
	}

	public void setlServiciosCapitados(
			List<ServiciosCapitadosVO> lServiciosCapitados) {
		this.lServiciosCapitados = lServiciosCapitados;
	}

}
