package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class CargoVO
 * Clase VO del cargo del empleado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class CargoVO implements Serializable {
	private static final long serialVersionUID = 8607468062723442282L;

	/*Identifica el consecutivo cargo del empleado.*/
	private Integer    consecutivoCodigoCargoEmpleado;
	
	/*Identifica el codigo del cargo del empleado.*/
	private String codigoCargoEmpleado;
	
	/*Identifica la descripcion del cargo del empleado.*/
	private String descripcionCargoEmpleado;
	
	public Integer getConsecutivoCodigoCargoEmpleado() {
		return consecutivoCodigoCargoEmpleado;
	}

	public void setConsecutivoCodigoCargoEmpleado(Integer consecutivoCodigoCargoEmpleado) {
		this.consecutivoCodigoCargoEmpleado = consecutivoCodigoCargoEmpleado;
	}

	public String getCodigoCargoEmpleado() {
		return codigoCargoEmpleado;
	}

	public void setCodigoCargoEmpleado(String codigoCargoEmpleado) {
		this.codigoCargoEmpleado = codigoCargoEmpleado;
	}

	public String getDescripcionCargoEmpleado() {
		return descripcionCargoEmpleado;
	}

	public void setDescripcionCargoEmpleado(String descripcionCargoEmpleado) {
		this.descripcionCargoEmpleado = descripcionCargoEmpleado;
	}	
}
