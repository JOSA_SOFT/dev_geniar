package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class EstadoDerechoVO
 * Clase VO del estado derecho del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 18/03/2016
 *
 */
public class EstadoDerechoVO implements Serializable {
	private static final long serialVersionUID = -6567712977250543167L;

	/*Identifica el consecutivo de la causa de derecho del afiliado*/
	private Integer consecutivoCodigoEstadoDerecho;
	
	/*Identifica la descripcion del estado de derecho*/
	private String descripcionDerecho;
	
	/*Identifica la descripcion del estado del afiliado*/
	private String descripcionEstadoAfiliado;
	
	/*Identifica la descripcion del estado del afiliado*/
	private String derecho;
	
	/*Identifica el consecutivo de la causa de derecho del afiliado*/
	private Integer consecutivoCausaDerecho;
	
	/*Identifica la descripcion de la causa de derecho del afiliado*/
	private String descripcionCausaDerecho;
	
	public Integer getConsecutivoCodigoEstadoDerecho() {
		return consecutivoCodigoEstadoDerecho;
	}

	public void setConsecutivoCodigoEstadoDerecho(
			Integer consecutivoCodigoEstadoDerecho) {
		this.consecutivoCodigoEstadoDerecho = consecutivoCodigoEstadoDerecho;
	}

	public String getDescripcionDerecho() {
		return descripcionDerecho;
	}

	public void setDescripcionDerecho(String descripcionDerecho) {
		this.descripcionDerecho = descripcionDerecho;
	}

	public String getDescripcionEstadoAfiliado() {
		return descripcionEstadoAfiliado;
	}

	public void setDescripcionEstadoAfiliado(String descripcionEstadoAfiliado) {
		this.descripcionEstadoAfiliado = descripcionEstadoAfiliado;
	}

	public String getDerecho() {
		return derecho;
	}

	public void setDerecho(String derecho) {
		this.derecho = derecho;
	}

	public Integer getConsecutivoCausaDerecho() {
		return consecutivoCausaDerecho;
	}

	public void setConsecutivoCausaDerecho(Integer consecutivoCausaDerecho) {
		this.consecutivoCausaDerecho = consecutivoCausaDerecho;
	}

	public String getDescripcionCausaDerecho() {
		return descripcionCausaDerecho;
	}

	public void setDescripcionCausaDerecho(String descripcionCausaDerecho) {
		this.descripcionCausaDerecho = descripcionCausaDerecho;
	}
}
