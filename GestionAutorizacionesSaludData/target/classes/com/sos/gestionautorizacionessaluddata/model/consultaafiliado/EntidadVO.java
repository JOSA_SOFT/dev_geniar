package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class EntidadVO
 * Clase VO de la entidad ARP del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class EntidadVO implements Serializable{
	private static final long serialVersionUID = 5720261508607530299L;

	/*Identifica el consecutivo codigo de la entidad*/
	private Integer consecutivoCodigoEntidad;
	
	/*Identifica el codigo de la entidad*/
	private String codigoEntidad;
	
	/*Identifica la descripcion de la entidad*/
	private String descripcionEntidad;
	
	public Integer getConsecutivoCodigoEntidad() {
		return consecutivoCodigoEntidad;
	}

	public void setConsecutivoCodigoEntidad(Integer consecutivoCodigoEntidad) {
		this.consecutivoCodigoEntidad = consecutivoCodigoEntidad;
	}

	public String getCodigoEntidad() {
		return codigoEntidad;
	}

	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}

	public String getDescripcionEntidad() {
		return descripcionEntidad;
	}

	public void setDescripcionEntidad(String descripcionEntidad) {
		this.descripcionEntidad = descripcionEntidad;
	}
}
