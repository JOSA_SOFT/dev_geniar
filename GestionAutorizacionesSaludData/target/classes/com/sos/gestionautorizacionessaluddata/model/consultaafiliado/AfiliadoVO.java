package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;

/**
 * Class AfiliadoVO
 * Clase VO del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 23/12/2015
 *
 */

public class AfiliadoVO implements Serializable {
	private static final long serialVersionUID = 8303726538358993951L;

	/*Identifica la direccion de residencia del afiliado*/
	private String direccionResidencia;
	
	/*Identifica el telefono del afiliado*/
	private String telefono;	
	
	/*Identifica la ciudad de de residencia del afiliado */
	private CiudadVO ciudadResidencia;
	
	/*Identifica el consecutivo del estado civil del afiliado */
	private Integer consecutivoEstadoCivil;	
	
	/*Identifica la descripcion del estado civil del afiliado */
	private String descripcionEstadoCivil;
	
	/*Identifica el consecutivo de la causa de derecho del afiliado*/
	private Integer consecutivoCausaDerecho;
	
	/*Identifica la descripcion de la causa de derecho del afiliado*/
	private String descripcionEstadoDerecho;
		
	/*Identifica la edad del afiliado en años */
	private Integer edadAnos;
	
	/*Identifica el consecutivo del estado del afiliado*/
	private Integer consecutivoEstadoAfiliado;
	
	/*Identifica la descripcion del estado del afiliado*/
	private String descripcionEstadoAfiliado;
	
	/*Identifica la fecha de nacimiento del afiliado */
	private Date fechaNacimiento;
	
	/*Identifica el numero unico de identificacion del afiliado */
	private Integer numeroUnicoIdentificacion;
	
	/*Identifica el numero de identificacion del afiliado */
	private String numeroIdentificacion;
	
	/*Identifica el plan del afiliado */
	private PlanVO plan;
	
	/*Identifica el plan complementario del afiliado */
	private PlanVO planComplementario;
	
	/*Identifica el nombre completo del afiliado */
	private String nombreCompleto;
	
	/*Identifica el primer nombre del afiliado */
	private String primerNombre;
	
	/*Identifica el primer apellido del afiliado */
	private String primerApellido;
	
	/*Identifica el segundo nombre del afiliado */
	private String segundoNombre;	
	
	/*Identifica el segundo apellido del afiliado */
	private String segundoApellido;
	
	/*Identifica el consecutivo del rango salarial del afiliado */
	private Integer consecutivoRangoSalarial;
	
	/*Identifica la descripcion del rango salarial del afiliado */
	private String descripcionRangoSalarial;
	private GeneroVO genero;
		
	/*Identifica el consecutivo del tipo de afiliado*/
	private Integer consecutivoTipoAfiliado;
	
	/*Identifica la descripcion del tipo de afiliado*/
	private String descripcionTipoAfiliado;
	
	/*Identifica el tipo de identificacion del afiliado*/
	private TiposIdentificacionVO tipoIdentificacionAfiliado;
	
	/*Identifica la comuna del afiliado*/
	private String comunaAfiliado;
	
	/*Identifica el email del afiliado*/
	private String email;
	
	/*Identifica el codigo de la IPS primaria del afiliado */
	private String codigoIPSPrimaria;
	
	/*Identifica la descripcion de la IPS primaria del afiliado */
	private String descripcionIPSPrimaria;
	
	/*Identifica la concatenacion del codigo y descripcion de la IPS primaria del afiliado */
	private String desIPSPrimaria;	

	/*Identifica la edad del afiliado en meses */
	private Integer edadMeses;
	
    /*Identifica la edad del afiliado en dias */
	private Integer edadDias;
	
    /*Identifica la fecha de inicio de vigencia del afiliado*/
	private Date fechaInicioVigencia;
	
    /*Identifica la fecha de fin de vigencia del afiliado*/
	private Date fechaFinVigencia;
	private Integer consecutivoParentesco;
	private String descripcionParentesco;	
	
	private Integer semanasAfiliacionPOSAnt;
	private Integer semanasAfiliacionPOSSOS;	
	
	private Integer semanasAfiliacionPACAnt;
	private Integer semanasAfiliacionPACSOS;
	
	/*Identifica las semanas cotizadas del afiliado*/
	private Integer semanasCotizadas;
	
	private String descripcionDepartamento;
		
	private Integer atep;
	private String tutela;
	private String codigoEAPB;	
	
	/*Identifica el tipo de contrato del afiliado*/
	private Integer consecutivoTipoContrato;
	
	/*Identifica el numero de contrato del afiliado*/
	private String numeroContrato;
	
	/*Identifica el tipo de formulario del afiliado*/
	private Integer consecutivoTipoFormulario;
	
	/*Identifica el numero del formulario del afiliado*/
	private String numeroFormulario;
	private Integer consecutivoBeneficiario;
	private Integer consecutivoCodigoSede;
	private String descripcionSede;
	private Integer consecutivoTipoUnidadEdad;	
	private String origen;
	
	private boolean altoRiesgo;
	private Integer consecutivoCodRiesgo;
	private String recienNacido;
	private String nro;
	private String generoRecNac;
	
	/*Identifica la fecha del recien nacido*/
	private Date fechaRecienNacido;	
	
	/*Identifica si fue parto multiple*/
	private String partoMultiple;		

	private Date fechaConsulta;
	private String valEspecial;
	
	private Integer consecutivoPresolicitud;
	/*Identifica el codigo del tipo de afiliado*/
	private String codigoTipoAfiliado;	
	/**
	 * @return the direccionResidencia
	 */
	public String getDireccionResidencia() {
		return direccionResidencia;
	}
	/**
	 * @param direccionResidencia the direccionResidencia to set
	 */
	public void setDireccionResidencia(String direccionResidencia) {
		this.direccionResidencia = direccionResidencia;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the ciudadResidencia
	 */
	public CiudadVO getCiudadResidencia() {
		return ciudadResidencia;
	}
	/**
	 * @param ciudadResidencia the ciudadResidencia to set
	 */
	public void setCiudadResidencia(CiudadVO ciudadResidencia) {
		this.ciudadResidencia = ciudadResidencia;
	}
	/**
	 * @return the consecutivoEstadoCivil
	 */
	public Integer getConsecutivoEstadoCivil() {
		return consecutivoEstadoCivil;
	}
	/**
	 * @param consecutivoEstadoCivil the consecutivoEstadoCivil to set
	 */
	public void setConsecutivoEstadoCivil(Integer consecutivoEstadoCivil) {
		this.consecutivoEstadoCivil = consecutivoEstadoCivil;
	}
	/**
	 * @return the descripcionEstadoCivil
	 */
	public String getDescripcionEstadoCivil() {
		return descripcionEstadoCivil;
	}
	/**
	 * @param descripcionEstadoCivil the descripcionEstadoCivil to set
	 */
	public void setDescripcionEstadoCivil(String descripcionEstadoCivil) {
		this.descripcionEstadoCivil = descripcionEstadoCivil;
	}
	/**
	 * @return the consecutivoCausaDerecho
	 */
	public Integer getConsecutivoCausaDerecho() {
		return consecutivoCausaDerecho;
	}
	/**
	 * @param consecutivoCausaDerecho the consecutivoCausaDerecho to set
	 */
	public void setConsecutivoCausaDerecho(Integer consecutivoCausaDerecho) {
		this.consecutivoCausaDerecho = consecutivoCausaDerecho;
	}
	/**
	 * @return the descripcionEstadoDerecho
	 */
	public String getDescripcionEstadoDerecho() {
		return descripcionEstadoDerecho;
	}
	/**
	 * @param descripcionEstadoDerecho the descripcionEstadoDerecho to set
	 */
	public void setDescripcionEstadoDerecho(String descripcionEstadoDerecho) {
		this.descripcionEstadoDerecho = descripcionEstadoDerecho;
	}
	/**
	 * @return the edadAnos
	 */
	public Integer getEdadAnos() {
		return edadAnos;
	}
	/**
	 * @param edadAnos the edadAnos to set
	 */
	public void setEdadAnos(Integer edadAnos) {
		this.edadAnos = edadAnos;
	}
	/**
	 * @return the consecutivoEstadoAfiliado
	 */
	public Integer getConsecutivoEstadoAfiliado() {
		return consecutivoEstadoAfiliado;
	}
	/**
	 * @param consecutivoEstadoAfiliado the consecutivoEstadoAfiliado to set
	 */
	public void setConsecutivoEstadoAfiliado(Integer consecutivoEstadoAfiliado) {
		this.consecutivoEstadoAfiliado = consecutivoEstadoAfiliado;
	}
	/**
	 * @return the descripcionEstadoAfiliado
	 */
	public String getDescripcionEstadoAfiliado() {
		return descripcionEstadoAfiliado;
	}
	/**
	 * @param descripcionEstadoAfiliado the descripcionEstadoAfiliado to set
	 */
	public void setDescripcionEstadoAfiliado(String descripcionEstadoAfiliado) {
		this.descripcionEstadoAfiliado = descripcionEstadoAfiliado;
	}
	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @return the numeroUnicoIdentificacion
	 */
	public Integer getNumeroUnicoIdentificacion() {
		return numeroUnicoIdentificacion;
	}
	/**
	 * @param numeroUnicoIdentificacion the numeroUnicoIdentificacion to set
	 */
	public void setNumeroUnicoIdentificacion(Integer numeroUnicoIdentificacion) {
		this.numeroUnicoIdentificacion = numeroUnicoIdentificacion;
	}
	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	/**
	 * @param numeroIdentificacion the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	/**
	 * @return the plan
	 */
	public PlanVO getPlan() {
		return plan;
	}
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(PlanVO plan) {
		this.plan = plan;
	}
	/**
	 * @return the planComplementario
	 */
	public PlanVO getPlanComplementario() {
		return planComplementario;
	}
	/**
	 * @param planComplementario the planComplementario to set
	 */
	public void setPlanComplementario(PlanVO planComplementario) {
		this.planComplementario = planComplementario;
	}
	/**
	 * @return the nombreCompleto
	 */
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	/**
	 * @param nombreCompleto the nombreCompleto to set
	 */
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	/**
	 * @return the primerNombre
	 */
	public String getPrimerNombre() {
		return primerNombre;
	}
	/**
	 * @param primerNombre the primerNombre to set
	 */
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	/**
	 * @return the primerApellido
	 */
	public String getPrimerApellido() {
		return primerApellido;
	}
	/**
	 * @param primerApellido the primerApellido to set
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	/**
	 * @return the segundoNombre
	 */
	public String getSegundoNombre() {
		return segundoNombre;
	}
	/**
	 * @param segundoNombre the segundoNombre to set
	 */
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	/**
	 * @return the segundoApellido
	 */
	public String getSegundoApellido() {
		return segundoApellido;
	}
	/**
	 * @param segundoApellido the segundoApellido to set
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	/**
	 * @return the consecutivoRangoSalarial
	 */
	public Integer getConsecutivoRangoSalarial() {
		return consecutivoRangoSalarial;
	}
	/**
	 * @param consecutivoRangoSalarial the consecutivoRangoSalarial to set
	 */
	public void setConsecutivoRangoSalarial(Integer consecutivoRangoSalarial) {
		this.consecutivoRangoSalarial = consecutivoRangoSalarial;
	}
	/**
	 * @return the descripcionRangoSalarial
	 */
	public String getDescripcionRangoSalarial() {
		return descripcionRangoSalarial;
	}
	/**
	 * @param descripcionRangoSalarial the descripcionRangoSalarial to set
	 */
	public void setDescripcionRangoSalarial(String descripcionRangoSalarial) {
		this.descripcionRangoSalarial = descripcionRangoSalarial;
	}
	/**
	 * @return the genero
	 */
	public GeneroVO getGenero() {
		return genero;
	}
	/**
	 * @param genero the genero to set
	 */
	public void setGenero(GeneroVO genero) {
		this.genero = genero;
	}
	/**
	 * @return the consecutivoTipoAfiliado
	 */
	public Integer getConsecutivoTipoAfiliado() {
		return consecutivoTipoAfiliado;
	}
	/**
	 * @param consecutivoTipoAfiliado the consecutivoTipoAfiliado to set
	 */
	public void setConsecutivoTipoAfiliado(Integer consecutivoTipoAfiliado) {
		this.consecutivoTipoAfiliado = consecutivoTipoAfiliado;
	}
	/**
	 * @return the descripcionTipoAfiliado
	 */
	public String getDescripcionTipoAfiliado() {
		return descripcionTipoAfiliado;
	}
	/**
	 * @param descripcionTipoAfiliado the descripcionTipoAfiliado to set
	 */
	public void setDescripcionTipoAfiliado(String descripcionTipoAfiliado) {
		this.descripcionTipoAfiliado = descripcionTipoAfiliado;
	}
	/**
	 * @return the tipoIdentificacionAfiliado
	 */
	public TiposIdentificacionVO getTipoIdentificacionAfiliado() {
		return tipoIdentificacionAfiliado;
	}
	/**
	 * @param tipoIdentificacionAfiliado the tipoIdentificacionAfiliado to set
	 */
	public void setTipoIdentificacionAfiliado(
			TiposIdentificacionVO tipoIdentificacionAfiliado) {
		this.tipoIdentificacionAfiliado = tipoIdentificacionAfiliado;
	}
	/**
	 * @return the comunaAfiliado
	 */
	public String getComunaAfiliado() {
		return comunaAfiliado;
	}
	/**
	 * @param comunaAfiliado the comunaAfiliado to set
	 */
	public void setComunaAfiliado(String comunaAfiliado) {
		this.comunaAfiliado = comunaAfiliado;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the codigoIPSPrimaria
	 */
	public String getCodigoIPSPrimaria() {
		return codigoIPSPrimaria;
	}
	/**
	 * @param codigoIPSPrimaria the codigoIPSPrimaria to set
	 */
	public void setCodigoIPSPrimaria(String codigoIPSPrimaria) {
		this.codigoIPSPrimaria = codigoIPSPrimaria;
	}
	/**
	 * @return the descripcionIPSPrimaria
	 */
	public String getDescripcionIPSPrimaria() {
		return descripcionIPSPrimaria;
	}
	/**
	 * @param descripcionIPSPrimaria the descripcionIPSPrimaria to set
	 */
	public void setDescripcionIPSPrimaria(String descripcionIPSPrimaria) {
		this.descripcionIPSPrimaria = descripcionIPSPrimaria;
	}
	/**
	 * @return the desIPSPrimaria
	 */
	public String getDesIPSPrimaria() {
		return desIPSPrimaria;
	}
	/**
	 * @param desIPSPrimaria the desIPSPrimaria to set
	 */
	public void setDesIPSPrimaria(String desIPSPrimaria) {
		this.desIPSPrimaria = desIPSPrimaria;
	}
	/**
	 * @return the edadMeses
	 */
	public Integer getEdadMeses() {
		return edadMeses;
	}
	/**
	 * @param edadMeses the edadMeses to set
	 */
	public void setEdadMeses(Integer edadMeses) {
		this.edadMeses = edadMeses;
	}
	/**
	 * @return the edadDias
	 */
	public Integer getEdadDias() {
		return edadDias;
	}
	/**
	 * @param edadDias the edadDias to set
	 */
	public void setEdadDias(Integer edadDias) {
		this.edadDias = edadDias;
	}
	/**
	 * @return the fechaInicioVigencia
	 */
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	/**
	 * @param fechaInicioVigencia the fechaInicioVigencia to set
	 */
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	/**
	 * @return the fechaFinVigencia
	 */
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	/**
	 * @param fechaFinVigencia the fechaFinVigencia to set
	 */
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	/**
	 * @return the consecutivoParentesco
	 */
	public Integer getConsecutivoParentesco() {
		return consecutivoParentesco;
	}
	/**
	 * @param consecutivoParentesco the consecutivoParentesco to set
	 */
	public void setConsecutivoParentesco(Integer consecutivoParentesco) {
		this.consecutivoParentesco = consecutivoParentesco;
	}
	/**
	 * @return the descripcionParentesco
	 */
	public String getDescripcionParentesco() {
		return descripcionParentesco;
	}
	/**
	 * @param descripcionParentesco the descripcionParentesco to set
	 */
	public void setDescripcionParentesco(String descripcionParentesco) {
		this.descripcionParentesco = descripcionParentesco;
	}
	/**
	 * @return the semanasAfiliacionPOSAnt
	 */
	public Integer getSemanasAfiliacionPOSAnt() {
		return semanasAfiliacionPOSAnt;
	}
	/**
	 * @param semanasAfiliacionPOSAnt the semanasAfiliacionPOSAnt to set
	 */
	public void setSemanasAfiliacionPOSAnt(Integer semanasAfiliacionPOSAnt) {
		this.semanasAfiliacionPOSAnt = semanasAfiliacionPOSAnt;
	}
	/**
	 * @return the semanasAfiliacionPOSSOS
	 */
	public Integer getSemanasAfiliacionPOSSOS() {
		return semanasAfiliacionPOSSOS;
	}
	/**
	 * @param semanasAfiliacionPOSSOS the semanasAfiliacionPOSSOS to set
	 */
	public void setSemanasAfiliacionPOSSOS(Integer semanasAfiliacionPOSSOS) {
		this.semanasAfiliacionPOSSOS = semanasAfiliacionPOSSOS;
	}
	/**
	 * @return the semanasAfiliacionPACAnt
	 */
	public Integer getSemanasAfiliacionPACAnt() {
		return semanasAfiliacionPACAnt;
	}
	/**
	 * @param semanasAfiliacionPACAnt the semanasAfiliacionPACAnt to set
	 */
	public void setSemanasAfiliacionPACAnt(Integer semanasAfiliacionPACAnt) {
		this.semanasAfiliacionPACAnt = semanasAfiliacionPACAnt;
	}
	/**
	 * @return the semanasAfiliacionPACSOS
	 */
	public Integer getSemanasAfiliacionPACSOS() {
		return semanasAfiliacionPACSOS;
	}
	/**
	 * @param semanasAfiliacionPACSOS the semanasAfiliacionPACSOS to set
	 */
	public void setSemanasAfiliacionPACSOS(Integer semanasAfiliacionPACSOS) {
		this.semanasAfiliacionPACSOS = semanasAfiliacionPACSOS;
	}
	/**
	 * @return the semanasCotizadas
	 */
	public Integer getSemanasCotizadas() {
		return semanasCotizadas;
	}
	/**
	 * @param semanasCotizadas the semanasCotizadas to set
	 */
	public void setSemanasCotizadas(Integer semanasCotizadas) {
		this.semanasCotizadas = semanasCotizadas;
	}
	/**
	 * @return the descripcionDepartamento
	 */
	public String getDescripcionDepartamento() {
		return descripcionDepartamento;
	}
	/**
	 * @param descripcionDepartamento the descripcionDepartamento to set
	 */
	public void setDescripcionDepartamento(String descripcionDepartamento) {
		this.descripcionDepartamento = descripcionDepartamento;
	}
	/**
	 * @return the atep
	 */
	public Integer getAtep() {
		return atep;
	}
	/**
	 * @param atep the atep to set
	 */
	public void setAtep(Integer atep) {
		this.atep = atep;
	}
	/**
	 * @return the tutela
	 */
	public String getTutela() {
		return tutela;
	}
	/**
	 * @param tutela the tutela to set
	 */
	public void setTutela(String tutela) {
		this.tutela = tutela;
	}
	/**
	 * @return the codigoEAPB
	 */
	public String getCodigoEAPB() {
		return codigoEAPB;
	}
	/**
	 * @param codigoEAPB the codigoEAPB to set
	 */
	public void setCodigoEAPB(String codigoEAPB) {
		this.codigoEAPB = codigoEAPB;
	}
	/**
	 * @return the consecutivoTipoContrato
	 */
	public Integer getConsecutivoTipoContrato() {
		return consecutivoTipoContrato;
	}
	/**
	 * @param consecutivoTipoContrato the consecutivoTipoContrato to set
	 */
	public void setConsecutivoTipoContrato(Integer consecutivoTipoContrato) {
		this.consecutivoTipoContrato = consecutivoTipoContrato;
	}
	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the consecutivoTipoFormulario
	 */
	public Integer getConsecutivoTipoFormulario() {
		return consecutivoTipoFormulario;
	}
	/**
	 * @param consecutivoTipoFormulario the consecutivoTipoFormulario to set
	 */
	public void setConsecutivoTipoFormulario(Integer consecutivoTipoFormulario) {
		this.consecutivoTipoFormulario = consecutivoTipoFormulario;
	}
	/**
	 * @return the numeroFormulario
	 */
	public String getNumeroFormulario() {
		return numeroFormulario;
	}
	/**
	 * @param numeroFormulario the numeroFormulario to set
	 */
	public void setNumeroFormulario(String numeroFormulario) {
		this.numeroFormulario = numeroFormulario;
	}
	/**
	 * @return the consecutivoBeneficiario
	 */
	public Integer getConsecutivoBeneficiario() {
		return consecutivoBeneficiario;
	}
	/**
	 * @param consecutivoBeneficiario the consecutivoBeneficiario to set
	 */
	public void setConsecutivoBeneficiario(Integer consecutivoBeneficiario) {
		this.consecutivoBeneficiario = consecutivoBeneficiario;
	}
	/**
	 * @return the consecutivoCodigoSede
	 */
	public Integer getConsecutivoCodigoSede() {
		return consecutivoCodigoSede;
	}
	/**
	 * @param consecutivoCodigoSede the consecutivoCodigoSede to set
	 */
	public void setConsecutivoCodigoSede(Integer consecutivoCodigoSede) {
		this.consecutivoCodigoSede = consecutivoCodigoSede;
	}
	/**
	 * @return the consecutivoTipoUnidadEdad
	 */
	public Integer getConsecutivoTipoUnidadEdad() {
		return consecutivoTipoUnidadEdad;
	}
	/**
	 * @param consecutivoTipoUnidadEdad the consecutivoTipoUnidadEdad to set
	 */
	public void setConsecutivoTipoUnidadEdad(Integer consecutivoTipoUnidadEdad) {
		this.consecutivoTipoUnidadEdad = consecutivoTipoUnidadEdad;
	}
	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}
	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	/**
	 * @return the altoRiesgo
	 */
	public boolean isAltoRiesgo() {
		return altoRiesgo;
	}
	/**
	 * @param altoRiesgo the altoRiesgo to set
	 */
	public void setAltoRiesgo(boolean altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}
	/**
	 * @return the consecutivoCodRiesgo
	 */
	public Integer getConsecutivoCodRiesgo() {
		return consecutivoCodRiesgo;
	}
	/**
	 * @param consecutivoCodRiesgo the consecutivoCodRiesgo to set
	 */
	public void setConsecutivoCodRiesgo(Integer consecutivoCodRiesgo) {
		this.consecutivoCodRiesgo = consecutivoCodRiesgo;
	}
	/**
	 * @return the recienNacido
	 */
	public String getRecienNacido() {
		return recienNacido;
	}
	/**
	 * @param recienNacido the recienNacido to set
	 */
	public void setRecienNacido(String recienNacido) {
		this.recienNacido = recienNacido;
	}
	/**
	 * @return the nro
	 */
	public String getNro() {
		return nro;
	}
	/**
	 * @param nro the nro to set
	 */
	public void setNro(String nro) {
		this.nro = nro;
	}
	/**
	 * @return the generoRecNac
	 */
	public String getGeneroRecNac() {
		return generoRecNac;
	}
	/**
	 * @param generoRecNac the generoRecNac to set
	 */
	public void setGeneroRecNac(String generoRecNac) {
		this.generoRecNac = generoRecNac;
	}
	/**
	 * @return the fechaConsulta
	 */
	public Date getFechaConsulta() {
		return fechaConsulta;
	}
	/**
	 * @param fechaConsulta the fechaConsulta to set
	 */
	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	
	public String getValEspecial() {
		return valEspecial;
	}
	public void setValEspecial(String valEspecial) {
		this.valEspecial = valEspecial;
	}
	
	public Date getFechaRecienNacido() {
		return fechaRecienNacido;
	}
	
	public void setFechaRecienNacido(Date fechaRecienNacido) {
		this.fechaRecienNacido = fechaRecienNacido;
	}
	
	public String getPartoMultiple() {
		return partoMultiple;
	}
	public void setPartoMultiple(String partoMultiple) {
		this.partoMultiple = partoMultiple;
	}
	public String getDescripcionSede() {
		return descripcionSede;
	}
	public void setDescripcionSede(String descripcionSede) {
		this.descripcionSede = descripcionSede;
	}
	
	public Integer getConsecutivoPresolicitud() {
		return consecutivoPresolicitud;
	}
	public void setConsecutivoPresolicitud(Integer consecutivoPresolicitud) {
		this.consecutivoPresolicitud = consecutivoPresolicitud;
	}
	/**
	 * @return the codigoTipoAfiliado
	 */
	public String getCodigoTipoAfiliado() {
		return codigoTipoAfiliado;
	}
	/**
	 * @param codigoTipoAfiliado the codigoTipoAfiliado to set
	 */
	public void setCodigoTipoAfiliado(String codigoTipoAfiliado) {
		this.codigoTipoAfiliado = codigoTipoAfiliado;
	}	
	
}
