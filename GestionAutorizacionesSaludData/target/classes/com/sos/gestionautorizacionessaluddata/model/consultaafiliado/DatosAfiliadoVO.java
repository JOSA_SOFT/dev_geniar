package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class DatosAfiliadoVO
 * Clase VO de los datos adicionales del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/01/2016
 *
 */

public class DatosAfiliadoVO extends AfiliadoVO implements Serializable{
	
	private static final long serialVersionUID = -919751013973446422L;
	private Integer consecutivoCodigoAfp;
	private String codigoAfp;
	private String descripcionAfp;	
	
	/*Identifica el barrio de residencia del afiliado*/
	private String barrio;		
	private String cotiza;
	
	private String ipsCapitacion;	
	
	
	public String getBarrio() {
		return barrio;
	}
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}	
		
	public Integer getConsecutivoCodigoAfp() {
		return consecutivoCodigoAfp;
	}
	public void setConsecutivoCodigoAfp(Integer consecutivoCodigoAfp) {
		this.consecutivoCodigoAfp = consecutivoCodigoAfp;
	}
	public String getCodigoAfp() {
		return codigoAfp;
	}
	public void setCodigoAfp(String codigoAfp) {
		this.codigoAfp = codigoAfp;
	}
	public String getDescripcionAfp() {
		return descripcionAfp;
	}
	public void setDescripcionAfp(String descripcionAfp) {
		this.descripcionAfp = descripcionAfp;
	}	
	
	public String getCotiza() {
		return cotiza;
	}
	public void setCotiza(String cotiza) {
		this.cotiza = cotiza;
	}
	
	public String getIPSCapitacion() {
		return ipsCapitacion;
	}
	
	public void setIPSCapitacion(String iPSCapitacion) {
		ipsCapitacion = iPSCapitacion;
	}
}
