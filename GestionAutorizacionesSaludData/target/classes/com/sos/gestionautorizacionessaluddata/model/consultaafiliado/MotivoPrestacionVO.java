package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Clase VO para los riesgos del afiliado
 * @author Julian Hernandez Gomez
 * @version 10/03/2016
 *
 */
public class MotivoPrestacionVO implements Serializable {
	private static final long serialVersionUID = 3333159490714576700L;

	/** **/
	private Integer consecutivoCodMotivoPrestacion;
	
	/** **/
	private String codMotivoPrestacion;
	
	/** **/
	private String desMotivoPrestacion;

	public Integer getConsecutivoCodMotivoPrestacion() {
		return consecutivoCodMotivoPrestacion;
	}

	public void setConsecutivoCodMotivoPrestacion(
			Integer consecutivoCodMotivoPrestacion) {
		this.consecutivoCodMotivoPrestacion = consecutivoCodMotivoPrestacion;
	}

	public String getCodMotivoPrestacion() {
		return codMotivoPrestacion;
	}

	public void setCodMotivoPrestacion(String codMotivoPrestacion) {
		this.codMotivoPrestacion = codMotivoPrestacion;
	}

	public String getDesMotivoPrestacion() {
		return desMotivoPrestacion;
	}

	public void setDesMotivoPrestacion(String desMotivoPrestacion) {
		this.desMotivoPrestacion = desMotivoPrestacion;
	}
	
	
	
	
	
}
