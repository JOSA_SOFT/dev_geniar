package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class TipoServicioVO
 * Clase VO de los tipos de servicio de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 *
 */

public class TipoServicioVO implements Serializable{
	private static final long serialVersionUID = -229597003083136025L;

	/*Consecutivo del tipo de servicio*/
	private Integer consecutivoTipoServicio;
	
	/*Descripcion del tipo de servicio*/
	private String descripcionTipoServicio;
	
	public Integer getConsecutivoTipoServicio() {
		return consecutivoTipoServicio;
	}
	public void setConsecutivoTipoServicio(Integer consecutivoTipoServicio) {
		this.consecutivoTipoServicio = consecutivoTipoServicio;
	}
	public String getDescripcionTipoServicio() {
		return descripcionTipoServicio;
	}
	public void setDescripcionTipoServicio(String descripcionTipoServicio) {
		this.descripcionTipoServicio = descripcionTipoServicio;
	}
}
