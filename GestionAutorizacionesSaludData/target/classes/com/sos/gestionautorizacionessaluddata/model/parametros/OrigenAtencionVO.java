package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ClaseAtencionVO
 * Clase VO del Origen de Atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class OrigenAtencionVO implements Serializable{
	private static final long serialVersionUID = -4777424773795447957L;

	/*Consecutivo del origen atencion*/
	private Integer consecutivoOrigenAtencion;
	
	/*codigo del origen atencion*/
	private String codigoOrigenAtencion;
	
	/*descripcion del origen atencion*/
	private String descripcionOrigenAtencion;
	
	public Integer getConsecutivoOrigenAtencion() {
		return consecutivoOrigenAtencion;
	}
	public void setConsecutivoOrigenAtencion(Integer consecutivoOrigenAtencion) {
		this.consecutivoOrigenAtencion = consecutivoOrigenAtencion;
	}
	public String getCodigoOrigenAtencion() {
		return codigoOrigenAtencion;
	}
	public void setCodigoOrigenAtencion(String codigoOrigenAtencion) {
		this.codigoOrigenAtencion = codigoOrigenAtencion;
	}
	public String getDescripcionOrigenAtencion() {
		return descripcionOrigenAtencion;
	}
	public void setDescripcionOrigenAtencion(String descripcionOrigenAtencion) {
		this.descripcionOrigenAtencion = descripcionOrigenAtencion;
	}
}
