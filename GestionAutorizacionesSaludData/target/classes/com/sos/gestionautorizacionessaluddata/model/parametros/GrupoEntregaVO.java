package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class GrupoEntregaVO
 * Clase VO de GrupoEntregaVO 
 * @author ing. German Perez - Geniar S.A.S
 * @version 22/09/2017
 *
 */

public class GrupoEntregaVO implements Serializable {
	private static final long serialVersionUID = 3277938242868670699L;

	/** informacion del consecutivo del grupo de entrega */
	private Integer consecutivoCodigoGrupoEntrega;	
	
	/** informacion del codigo del grupo de entrega */
	private String codigoGrupoEntrega;	
	
	/** informacion de la descripcion del grupo de entrega */
	private String descripcionGrupoEntrega;

	public Integer getConsecutivoCodigoGrupoEntrega() {
		return consecutivoCodigoGrupoEntrega;
	}

	public void setConsecutivoCodigoGrupoEntrega(Integer consecutivoCodigoGrupoEntrega) {
		this.consecutivoCodigoGrupoEntrega = consecutivoCodigoGrupoEntrega;
	}

	public String getCodigoGrupoEntrega() {
		return codigoGrupoEntrega;
	}

	public void setCodigoGrupoEntrega(String codigoGrupoEntrega) {
		this.codigoGrupoEntrega = codigoGrupoEntrega;
	}

	public String getDescripcionGrupoEntrega() {
		return descripcionGrupoEntrega;
	}

	public void setDescripcionGrupoEntrega(String descripcionGrupoEntrega) {
		this.descripcionGrupoEntrega = descripcionGrupoEntrega;
	}		
}
