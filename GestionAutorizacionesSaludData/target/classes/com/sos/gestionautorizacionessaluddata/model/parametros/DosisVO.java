package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class FrecuenciaVO
 * Clase VO de la Dosis del medicamento
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/12/2015
 *
 */
public class DosisVO  implements Serializable{
	private static final long serialVersionUID = -6583699942188851244L;

	/*Consecutivo de la dosis*/
	private Integer consecutivoCodigoDosis;
	
	/*Codigo de la dosis*/
	private String codigoDosis;
	
	/*Descripcion de la dosis*/
	private String descripcionDosis;
	
	public Integer getConsecutivoCodigoDosis() {
		return consecutivoCodigoDosis;
	}

	public void setConsecutivoCodigoDosis(Integer consecutivoCodigoDosis) {
		this.consecutivoCodigoDosis = consecutivoCodigoDosis;
	}

	public String getCodigoDosis() {
		return codigoDosis;
	}

	public void setCodigoDosis(String codigoDosis) {
		this.codigoDosis = codigoDosis;
	}

	public String getDescripcionDosis() {
		return descripcionDosis;
	}

	public void setDescripcionDosis(String descripcionDosis) {
		this.descripcionDosis = descripcionDosis;
	}
}
