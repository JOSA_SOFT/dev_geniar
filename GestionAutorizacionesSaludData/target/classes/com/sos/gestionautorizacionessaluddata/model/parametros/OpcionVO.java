package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class OpcionVO
 * Clase VO de la opciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 *
 */
public class OpcionVO implements Serializable{
	private static final long serialVersionUID = -6401822188867572933L;
	/*Consecutivo de la opcion*/
	private Integer consecutivoOpcion;
	/*descripcion de la opcion*/
	private String descripcionOpcion;
	
	public Integer getConsecutivoOpcion() {
		return consecutivoOpcion;
	}

	public void setConsecutivoOpcion(Integer consecutivoOpcion) {
		this.consecutivoOpcion = consecutivoOpcion;
	}

	public String getDescripcionOpcion() {
		return descripcionOpcion;
	}

	public void setDescripcionOpcion(String descripcionOpcion) {
		this.descripcionOpcion = descripcionOpcion;
	}
}
