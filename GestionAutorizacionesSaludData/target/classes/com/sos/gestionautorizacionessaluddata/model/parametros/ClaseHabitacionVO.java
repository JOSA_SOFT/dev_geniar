package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ClaseHabitacionVO
 * Clase VO de la clase de habitacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class ClaseHabitacionVO implements Serializable{
	private static final long serialVersionUID = -6828614918009189065L;

	/*Consecutivo de la clase de habitacion*/
	private Integer consecutivoClaseHabitacion;
	
	/*codigo de la clase de habitacion*/
	private String codigoClaseHabitacion;
	
	/*descripcion de la clase de habitacion*/
	private String descripcionClaseHabitacion;
	
	public Integer getConsecutivoClaseHabitacion() {
		return consecutivoClaseHabitacion;
	}
	public void setConsecutivoClaseHabitacion(Integer consecutivoClaseHabitacion) {
		this.consecutivoClaseHabitacion = consecutivoClaseHabitacion;
	}
	public String getCodigoClaseHabitacion() {
		return codigoClaseHabitacion;
	}
	public void setCodigoClaseHabitacion(String codigoClaseHabitacion) {
		this.codigoClaseHabitacion = codigoClaseHabitacion;
	}
	public String getDescripcionClaseHabitacion() {
		return descripcionClaseHabitacion;
	}
	public void setDescripcionClaseHabitacion(String descripcionClaseHabitacion) {
		this.descripcionClaseHabitacion = descripcionClaseHabitacion;
	}
}
