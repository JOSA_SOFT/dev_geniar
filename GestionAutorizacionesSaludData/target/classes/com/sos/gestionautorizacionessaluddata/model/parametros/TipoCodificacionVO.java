package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class TipoCodificacionVO
 * Clase VO del tipo de codificacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */

public class TipoCodificacionVO implements Serializable{
	private static final long serialVersionUID = 959777587656258951L;

	/*Identifica el consecutivo codigo tipo de codificacion*/
	private Integer consecutivoCodigoTipoCodificacion;
	
	/*Identifica el codigo del tipo de codificacion*/
	private String codigoTipoCodificacion;
	
	/*Identifica la descripcion del tipo de codificacion*/
	private String descripcionTipoCodificacion;
	
	public Integer getConsecutivoCodigoTipoCodificacion() {
		return consecutivoCodigoTipoCodificacion;
	}

	public void setConsecutivoCodigoTipoCodificacion(
			Integer consecutivoCodigoTipoCodificacion) {
		this.consecutivoCodigoTipoCodificacion = consecutivoCodigoTipoCodificacion;
	}

	public String getCodigoTipoCodificacion() {
		return codigoTipoCodificacion;
	}

	public void setCodigoTipoCodificacion(String codigoTipoCodificacion) {
		this.codigoTipoCodificacion = codigoTipoCodificacion;
	}

	public String getDescripcionTipoCodificacion() {
		return descripcionTipoCodificacion;
	}

	public void setDescripcionTipoCodificacion(String descripcionTipoCodificacion) {
		this.descripcionTipoCodificacion = descripcionTipoCodificacion;
	}
}
