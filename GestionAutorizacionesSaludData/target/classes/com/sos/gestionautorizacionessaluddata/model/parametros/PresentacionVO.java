package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Class PresentacionVO Clase VO de la presentacion de los medicamentos
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/01/2016
 *
 */
public class PresentacionVO implements Serializable {
	private static final long serialVersionUID = -2973973421235446780L;

	/* Identifica el consecutivo codiigo de la presentacion del medicamento */
	private BigInteger consecutivoCodigoPresentacion;

	/* Identifica el codigo de la presentacion del medicamento */
	private String codigoPresentacion;

	/* Identifica la descripcion de la presentacion del medicamento */
	private String descripcionPresentacion;

	/* Identifica la cantidad de la presentacion del medicamento */
	private String cantidadPresentacion;

	private String descripcionXCantidad;

	public BigInteger getConsecutivoCodigoPresentacion() {
		return consecutivoCodigoPresentacion;
	}

	public void setConsecutivoCodigoPresentacion(BigInteger consecutivoCodigoPresentacion) {
		this.consecutivoCodigoPresentacion = consecutivoCodigoPresentacion;
	}

	public String getCodigoPresentacion() {
		return codigoPresentacion;
	}

	public void setCodigoPresentacion(String codigoPresentacion) {
		this.codigoPresentacion = codigoPresentacion;
	}

	public String getDescripcionPresentacion() {
		return descripcionPresentacion;
	}

	public void setDescripcionPresentacion(String descripcionPresentacion) {
		this.descripcionPresentacion = descripcionPresentacion;
	}

	public String getCantidadPresentacion() {
		return cantidadPresentacion;
	}

	public void setCantidadPresentacion(String cantidadPresentacion) {
		this.cantidadPresentacion = cantidadPresentacion;
	}

	public String getDescripcionXCantidad() {
		if (descripcionPresentacion != null && cantidadPresentacion != null) {
			descripcionXCantidad = descripcionPresentacion + " x " + cantidadPresentacion;
		}
		return descripcionXCantidad;
	}

}
