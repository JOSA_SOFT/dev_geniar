package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class UbicacionPacienteVO
 * Clase VO con la ubicacion del paciente
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 *
 */
public class UbicacionPacienteVO implements Serializable{
	private static final long serialVersionUID = -5752577379204738409L;

	/*Consecutivo de la ubicacion paciente*/
	private Integer consecutivoUbicacionPaciente;
	
	/*Codigo de la ubicacion paciente*/
	private String codigoUbicacionPaciente;
	
	/*Descripcion de la ubicacion paciente*/
	private String descripcionUbicacionPaciente;
	
	public Integer getConsecutivoUbicacionPaciente() {
		return consecutivoUbicacionPaciente;
	}

	public void setConsecutivoUbicacionPaciente(Integer consecutivoUbicacionPaciente) {
		this.consecutivoUbicacionPaciente = consecutivoUbicacionPaciente;
	}
	
	public String getCodigoUbicacionPaciente() {
		return codigoUbicacionPaciente;
	}

	public void setCodigoUbicacionPaciente(String codigoUbicacionPaciente) {
		this.codigoUbicacionPaciente = codigoUbicacionPaciente;
	}

	public String getDescripcionUbicacionPaciente() {
		return descripcionUbicacionPaciente;
	}

	public void setDescripcionUbicacionPaciente(String descripcionUbicacionPaciente) {
		this.descripcionUbicacionPaciente = descripcionUbicacionPaciente;
	}
}
