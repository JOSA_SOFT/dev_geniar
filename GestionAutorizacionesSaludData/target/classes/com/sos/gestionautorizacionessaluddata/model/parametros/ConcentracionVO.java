package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

public class ConcentracionVO implements Serializable{
	private static final long serialVersionUID = 5959361148045816418L;

	/*Identifica el consecutivo codigo de la concentracion del medicamento*/
	private Integer consecutivoConcentracion;
	
	/*Identifica el codigo de la concentracion del medicamento*/
	private String codigoConcentracion;
	
	/*Identifica la descripcion de la concentracion del medicamento*/
	private String descripcionConcentracion;
	
	/*Identifica la cantidad de la concentracion del medicamento*/
	private String cantidadConcentracion;
	
	
	public Integer getConsecutivoConcentracion() {
		return consecutivoConcentracion;
	}

	public void setConsecutivoConcentracion(Integer consecutivoConcentracion) {
		this.consecutivoConcentracion = consecutivoConcentracion;
	}
	
	public String getCodigoConcentracion() {
		return codigoConcentracion;
	}

	public void setCodigoConcentracion(String codigoConcentracion) {
		this.codigoConcentracion = codigoConcentracion;
	}

	public String getDescripcionConcentracion() {
		return descripcionConcentracion;
	}

	public void setDescripcionConcentracion(String descripcionConcentracion) {
		this.descripcionConcentracion = descripcionConcentracion;
	}

	public String getCantidadConcentracion() {
		return cantidadConcentracion;
	}

	public void setCantidadConcentracion(String cantidadConcentracion) {
		this.cantidadConcentracion = cantidadConcentracion;
	}

}
