package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class MedioContactoVO
 * Clase VO del medio de contacto
 * @author ing. Victor Hugo Gil Ramos
 * @version 08/02/2016
 *
 */
public class MedioContactoVO implements Serializable{
	private static final long serialVersionUID = 4716891704644257670L;

	/*Identifica el consecutivo del medio de contacto*/
	private Integer consecutivoMedioContacto;
	
	/*Identifica el cdigo del medio de contacto*/
	private String codigoMedioContacto;
	
	/*Identifica la descripcion del medio de contacto*/
	private String descripcionMedioContacto;
	
	public Integer getConsecutivoMedioContacto() {
		return consecutivoMedioContacto;
	}

	public void setConsecutivoMedioContacto(Integer consecutivoMedioContacto) {
		this.consecutivoMedioContacto = consecutivoMedioContacto;
	}

	public String getCodigoMedioContacto() {
		return codigoMedioContacto;
	}

	public void setCodigoMedioContacto(String codigoMedioContacto) {
		this.codigoMedioContacto = codigoMedioContacto;
	}

	public String getDescripcionMedioContacto() {
		return descripcionMedioContacto;
	}

	public void setDescripcionMedioContacto(String descripcionMedioContacto) {
		this.descripcionMedioContacto = descripcionMedioContacto;
	}
}
