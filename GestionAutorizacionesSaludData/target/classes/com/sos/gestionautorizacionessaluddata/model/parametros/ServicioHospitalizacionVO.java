package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ServicioHospitalizacionVO
 * Clase VO del servicio de hospitalizacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */
public class ServicioHospitalizacionVO implements Serializable{
	private static final long serialVersionUID = 3529588777919710742L;

	/*Consecutivo servicio de hospitalizacion*/
	private Integer consecutivoSevicioHospitalizacion;
	
	/*Descripcion servicio de hospitalizacion*/
	private String descripcionSevicioHospitalizacion;
	
	public Integer getConsecutivoSevicioHospitalizacion() {
		return consecutivoSevicioHospitalizacion;
	}

	public void setConsecutivoSevicioHospitalizacion(Integer consecutivoSevicioHospitalizacion) {
		this.consecutivoSevicioHospitalizacion = consecutivoSevicioHospitalizacion;
	}

	public String getDescripcionSevicioHospitalizacion() {
		return descripcionSevicioHospitalizacion;
	}

	public void setDescripcionSevicioHospitalizacion(String descripcionSevicioHospitalizacion) {
		this.descripcionSevicioHospitalizacion = descripcionSevicioHospitalizacion;
	}
}
