package com.sos.gestionautorizacionessaluddata.util;

import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.TotalResumenOpsVO;
import com.thoughtworks.xstream.XStream;

public class ConsultaInformacioOps {

	private static final String NODE_INIT = "<rsmnsOps>";
	private static final String NODE_FINAL = "</rsmnsOps>";
	private static final int TAMANO_XML = 4000;

	/**
	 * Metodo encargado de generar un xml con la lista de resumen
	 * 
	 * @param totalResumenOpsVO
	 * @return
	 */
	public String construirXMLResumenOps(TotalResumenOpsVO totalResumenOpsVO) {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(Boolean.TRUE);
		return xstream.toXML(totalResumenOpsVO);
	}

	public String armarXMLResumenOps(List<TotalResumenOpsVO> listaTotalResumenOpsVO) {
		StringBuilder resumeXml = new StringBuilder(TAMANO_XML);
		resumeXml.append(NODE_INIT);
		for (int i = 0; i < listaTotalResumenOpsVO.size(); i++) {
			TotalResumenOpsVO totalResumenOpsVO = listaTotalResumenOpsVO.get(i);
			String xml = construirXMLResumenOps(totalResumenOpsVO);
			resumeXml.append(xml);
		}
		resumeXml.append(NODE_FINAL);
		return resumeXml.toString();

	}
}
