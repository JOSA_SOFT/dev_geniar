package com.sos.gestionautorizacionessaluddata.util;

import java.io.Serializable;

/**
 * Class ConstantesData Clase donde se relacionan las constantes que maneja la
 * aplicacion para el proyecto data
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/03/2016
 *
 */
public class ConstantesData implements Serializable {

	private static final long serialVersionUID = 6721831501896543875L;

	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int CONSECUTIVO_CUPS = 4;

	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final int CONSECUTIVO_CUMS = 5;

	/**
	 * Atributo que determina el consecutvo de insumo
	 */
	public static final int CONSECUTIVO_CUOS = 9;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en devuelto
	 */
	public static final Integer COD_ESTADO_DEVUELTO = 6;
	/**
	 * Atributo que determina la descripcion del COD_ESTADO_ENTREGADA
	 */
	public static final Integer COD_ESTADO_ENTREGADA = 15;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_AUTORIZADO = 11;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_AUDITORIA = 5;

	/**
	 * Atributo que determina la descripcion COD_ESTADO_NO_AUTORIZADO
	 */
	public static final Integer COD_ESTADO_NO_AUTORIZADO = 8;

	/**
	 * Atributo que determina una cadenas vacia
	 */
	public static final String CADENA_VACIA = "";

	/**
	 * Atributo que determina si el tipo de prestacion
	 */
	public static final int TIPO_PRESTACION = 1;
	/**
	 * Atributo que determina si el tipo de prestacion COUS
	 */
	public static final int TIPO_PRESTACION_PIS = 3;

	/**
	 * Atributo que determina un valor en SI
	 */
	public static final String SI = "SI";
	/**
	 * Atributo que determina un valor en NO
	 */
	public static final Integer CODIGO_SI = 1;
	/**
	 * Atributo que determina un valor en SI
	 */
	public static final String S = "S";
	/**
	 * Atributo que determina un valor en NO
	 */
	public static final String N = "N";

	/**
	 * Atributo que determina un valor en NO
	 */
	public static final String NO = "NO";
	/**
	 * Atributo que determina un valor en NO
	 */
	public static final Integer CODIGO_NO = 0;

	/**
	 * Atributo que determina un valor en NO
	 */
	public static final String PUNTO = "\\.";

	/**
	 * Atributo que determina si es recien nacido
	 */
	public static final String RECIEN_NACIDO = "S";

	/**
	 * Atributo que determina si es recien nacido Numero
	 */
	public static final String RECIEN_NACIDO_NUMERO = "1";

	/**
	 * Atributo que determina si es recien nacido
	 */
	public static final int VALIDACION_CUOS = 1;

	/**
	 * Atributo que determina el guardar historico
	 */
	public static final String GUARDAR_HISTORICO_DESCARGAS = "GUARDAR_HISTORICO_DESCARGAS";

	/**
	 * Variable que indica el guardar del recobro modificado.
	 */
	public static final String GUARDAR_MODIFICACION_RECOBRO = "GUARDAR_MODIFICACION_RECOBRO";

	/**
	 * Atributo que determina el guardar gestion contratacion
	 */
	public static final String GUARDAR_GESTION_CONTRATACION = "GUARDAR_GESTION_CONTRATACION";

	/**
	 * Atributo que determina la consulta de documentos soportes
	 */
	public static final String CONSULTA_DOCUMENTOS_SOLICITUD = "CONSULTA_DOCUMENTOS_SOLICITUD";

	/**
	 * Atributo que determina la consulta de documentos soportes
	 */
	public static final String CONSULTA_GRUPO_FAMILIAR = "CONSULTA_GRUPOFAMILIAR";

	/**
	 * Atributo que determina la consulta de estado de derecho
	 */
	public static final String CONSULTA_ESTADO_DERECHO = "CONSULTA_ESTADO_DERECHO";

	/**
	 * Atributo que determina la consulta de estado de derecho
	 */
	public static final String CONSULTA_HISTORICO_DESCARGAS = "CONSULTA_HISTORICO_DESCARGAS";

	/**
	 * Atributo que determina la consulta del histórico de modificaciones
	 */
	public static final String CONSULTA_HISTORICO_MODIFICACION = "CONSULTA_HISTORICO_MODIFICACION";

	/**
	 * Atributo que determina la consulta de estado de derecho
	 */
	public static final String CONSULTA_MOTIVO_CAUSA = "CONSULTA_MOTIVO_CAUSA";

	/**
	 * Atributo que determina la consulta del parametro meses fecha modificación
	 */
	public static final String CONSULTA_MES_MAX_FECHA = "CONSULTA_MES_MAX_FECHA";

	/**
	 * Atributo que determina la consulta de estado de derecho
	 */
	public static final String ANULAR_SOLICITUD = "ANULAR_SOLICITUD";

	/**
	 * Atributo que determina la consulta de estado de derecho
	 */
	public static final String ANULAR_OPS = "ANULAR_OPS";

	/**
	 * Atributo que determina la consulta basica de la solicitud
	 */
	public static final String CONSULTA_SOLICITUDES = "CONSULTA_SOLICITUDES";

	/**
	 * Atributo que define el mensaje para pacientes de alto riesto
	 */
	public static final String MENSAJE_ALTO_RIESGO = "PACIENTE ALTO RIESGO";

	/**
	 * Atributo para el valor boolean true
	 */
	public static final String EQUIVALENTE_VERDADERO = "1";

	/**
	 * Atributo que determina si es recien nacido
	 */
	public static final Integer VALIDACION_INTEGER = 0;

	/**
	 * Atributo que determina la consulta del detalle de la solicitud
	 */
	public static final String CONSULTA_DETALLE_SOLICITUD = "CONSULTA_DETALLE_SOLICITUD";

	/**
	 * Atributo que determina corte cuenta
	 */
	public static final Integer CORTE_CUENTA = 0;

	/**
	 * Atributo que determina CONS_COD_CLASIFICACION_EVENTO_DEFAULT
	 */
	public static final Integer CONS_COD_CLASIFICACION_EVENTO_DEFAULT = 30;

	/**
	 * Atributo que determina si es recien nacido
	 */
	public static final String RECIEN_NACIDO_CADENA = "S";

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_PRESTACIONES_X_SOLICITUD = "CONSULTA_PRESTACIONES_X_SOLICITUD";

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_PRESTACIONES_X_OPS = "CONSULTA_PRESTACIONES_X_OPS";

	/**
	 * Atributo que determina la consulta de las prestaciones aprobadas de la
	 * solicitud
	 */
	public static final String CONSULTA_PRESTACIONES_APROBADAS = "CONSULTA_PRESTACIONES_APROBADAS";

	/**
	 * Atributo que determina la consulta de las prestaciones aprobadas para
	 * modificar fecha Entrega
	 */
	public static final String CONSULTA_PRESTACIONES_FECHA_ENTREGA = "CONSULTA_PRESTACIONES_FECHA_ENTREGA";

	/**
	 * Atributo que determina la consulta de las prestaciones validas para
	 * reliquidación
	 */
	public static final String CONSULTA_PRESTACIONES_RELIQUIDAR = "CONSULTA_PRESTACIONES_RELIQUIDAR";

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_PERMISO_ANULAR_SOLICITUD_OPS = "CONSULTA_PERMISO_ANULAR_SOLICITUD_OPS";

	/**
	 * Atributo que determina la consulta de prestador por codigo interno
	 */
	public static final String CONSULTA_PRESTADOR_X_COD_INTERNO = "CONSULTA_PRESTADOR_X_COD_INTERNO";

	/**
	 * Atributo que determina la consulta de prestador por codigo interno
	 */
	public static final Integer SW_PRESTADOR_COD_INTERNO = 1;

	/**
	 * Atributo que determina el metodo para guardar la modificacion de la
	 * prestacion
	 */
	public static final String GUARDAR_MOD_OPS = "GUARDAR_MOD_OPS";

	/**
	 * Atributo que determina el metodo para guardar la gestion de
	 * autorizaciones
	 */
	public static final String GUARDAR_GESTION_AUTORIZA_PRESTACIONES = "GUARDAR_GESTION_AUTORIZA_PRESTACIONES";

	/**
	 * Atributo que determina el metodo para consultar la gestion de
	 * autorizaciones
	 */
	public static final String CONSULTA_GESTION_AUTORIZA_PRESTACIONES = "CONSULTA_GESTION_AUTORIZA_PRESTACIONES";
	/**
	 * Atributo que determina el metodo para guardar la fecha modificada de
	 * entrega
	 */

	public static final String GUARDAR_FECHA_MODIFICADA = "GUARDAR_FECHA_MODIFICADA";

	/**
	 * Atributo que determina el metodo para guardar la reliquidación
	 */

	public static final String GUARDAR_MOD_RELIQUIDACION = "GUARDAR_MOD_RELIQUIDACION";

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_DOCUMENTOS_SOPORTE = "CONSULTA_DOCUMENTOS_SOPORTE";

	/**
	 * Atributo que determina la consulta las tareas del bpm asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_HISTORICO_INSTANCIAS_SOLICITUD = "CONSULTA_HISTORICO_INSTANCIAS_SOLICITUD";

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas a la
	 * solicitud
	 */
	public static final String CONSULTA_CONCEPTOS_X_SOLICITUD = "CONSULTA_CONCEPTOS_X_SOLICITUD";

	/**
	 * Atributo que determina la consulta la fecha de entrega por prestacion
	 */
	public static final String CONSULTA_FECHA_ENTREGA_X_SOLICITUD = "CONSULTA_FECHA_ENTREGA_X_SOLICITUD";

	/**
	 * Atributo que determina la consulta de las ops generadas por solicitud
	 */
	public static final String CONSULTA_OPS_SOLICITUD = "CONSULTA_OPS_SOLICITUD";

	/**
	 * Atributo que determina el llamado al proceso de actualizacion de la ops
	 */
	public static final String ACTUALIZAR_ESTADO_CONCEPTO = "ACTUALIZAR_ESTADO_CONCEPTO";

	/**
	 * Atributo que determina el guardar historico de la validacion especial
	 */
	public static final String GUARDAR_VALIDACION_ESPECIAL = "GUARDAR_VALIDACION_ESPECIAL";

	/**
	 * Atributo que determina la ejecucion del numero unico OPS
	 */
	public static final String EJECUTA_NUMERO_UNICO_OPS = "EJECUTA_NUMERO_UNICO_OPS";

	/**
	 * Atributo que determina la consulta de la factura asociada al conceepto de
	 * gasto
	 */
	public static final String CONSULTA_FACTURA_SOLICITUD = "CONSULTA_FACTURA_SOLICITUD";

	/**
	 * Atributo que guarda la gestion integral
	 */
	public static final String GUARDAR_GESTION_AUDITORIA_INTEGRAL = "GUARDAR_GESTION_AUDITORIA_INTEGRAL";

	/**
	 * Atributo que guarda la gestion asociar programacion
	 */
	public static final String GUARDAR_ASOCIAR_PROGRAMACION = "GUARDAR_ASOCIAR_PROGRAMACION";

	/**
	 * Atributo conrultar detalle bpm
	 */
	public static final String CONSULTAR_DETALLE_BPM = "CONSULTAR_DETALLE_BPM";

	/**
	 * Atributo guardar cambio de fecha direccionamiento
	 */
	public static final String GUARDAR_CAMBIO_FECHA_DIRECCIONAMIENTO_PRESTACION = "GUARDAR_CAMBIO_FECHA_DIRECCIONAMIENTO_PRESTACION";

	/**
	 * Atributo guardar direccionamiento
	 */
	public static final String GUARDAR_DIRECCIONAMIENTO_PRESTACION = "GUARDAR_DIRECCIONAMIENTO_PRESTACION";

	/**
	 * Atributo CONSULTA_PROGRAMACION_ENTREGA_PARAMETROS
	 */
	public static final String CONSULTA_PROGRAMACION_ENTREGA_PARAMETROS = "CONSULTA_PROGRAMACION_ENTREGA_PARAMETROS";
	/**
	 * Atributo EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA
	 */
	public static final String EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA = "EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA";

	/**
	 * Atributo ACTUALIZAR_ESTADO_PROGRAMACION_ENTREGA
	 */
	public static final String ACTUALIZAR_ESTADO_PROGRAMACION_ENTREGA = "ACTUALIZAR_ESTADO_PROGRAMACION_ENTREGA";

	/**
	 * Atributo guardar gestion domiciliaria
	 */
	public static final String GUARDAR_GESTION_DOMICILIARIA = "GUARDAR_GESTION_DOMICILIARIA";

	/**
	 * Atributo CONSULTA_PROGRAMACION_ENTREGA_DEFAULT
	 */
	public static final String CONSULTA_PROGRAMACION_ENTREGA_DEFAULT = "CONSULTA_PROGRAMACION_ENTREGA_DEFAULT";
	/**
	 * Atributo guardar gestion medicina
	 */
	public static final String GUARDAR_GESTION_MEDICINA = "GUARDAR_GESTION_MEDICINA";

	/**
	 * Atributo para adicionar una prestacion
	 */
	public static final String GUARDAR_ADICIONAR_PRESTACION = "GUARDAR_ADICIONAR_PRESTACION";

	/**
	 * Atributo para adicionar una prestacion
	 */
	public static final String GUARDAR_REASIGNACION_PRESTACION = "GUARDAR_REASIGNACION_PRESTACION";

	/**
	 * Atributo CONSULTA_SOLICITUD_PROGRAMACION_ENTEGA
	 */
	public static final String CONSULTA_SOLICITUD_PROGRAMACION_ENTEGA = "CONSULTA_SOLICITUD_PROGRAMACION_ENTEGA";
	/**
	 * Atributo para CONSULTA_ESTADOS_NOTIFICACION
	 */
	public static final String CONSULTA_ESTADOS_NOTIFICACION = "CONSULTA_ESTADOS_NOTIFICACION";
	/**
	 * Atributo para CONSULTA_PROGRAMACION_ENTREGA
	 */
	public static final String CONSULTA_PROGRAMACION_ENTREGA = "CONSULTA_PROGRAMACION_ENTREGA";
	/**
	 * Atributo para CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION
	 */
	public static final String CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION = "CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION";
	/**
	 * Atributo que determina el llamado al proceso de actualizacion de la ops
	 */
	public static final String CONSULTA_FECHA_ESTADO_OPS = "CONSULTA_FECHA_ESTADO_OPS";

	/**
	 * Atributo que determina la descipcion del estado en BD de un afiliado
	 */
	public static final String DESCIPCION_ESTADO = "A";

	/**
	 * Atributo que determina la homologacion del estado activo
	 */
	public static final String HOMOLOGACION_ESTADO_ACTIVO = "ACTIVO";

	/**
	 * Atributo que determina la homologacion del estado inactivo
	 */
	public static final String HOMOLOGACION_ESTADO_INACTIVO = "INACTIVO";

	/**
	 * Atributo que determina el nombre de la consulta de soportes de la
	 * presolicitud
	 */
	public static final String CONSULTA_SOPORTES_PRESOLICITUD = "CONSULTA_SOPORTES_PRESOLICITUD";

	/**
	 * Atributo que determina el nombre de la consulta de la presolicitud
	 */
	public static final String CONSULTA_PRESOLICITUD = "CONSULTA_PRESOLICITUD";

	/**
	 * Atributo que determina el nombre para guardar la devolucion
	 */
	public static final String GUARDAR_DEVOLUCION = "GUARDAR_DEVOLUCION";

	/**
	 * Atributo que determina el nombre para guardar la devolucion
	 */
	public static final String CONSULTA_TIPO_DOCUMENTO_SOPORTE = "CONSULTA_TIPO_DOCUMENTO_SOPORTE";

	/**
	 * Atributo que determina el nombre del sp de la consulta de motivos de
	 * devolucion
	 */
	public static final String CONSULTA_MOTIVOS_DEVOLUCION_TRANSCRIPCION = "CONSULTA_MOTIVOS_DEVOLUCION_TRANSCRIPCION";

	public static final String CONSULTA_MARCA_ACCESO_DIRECTO = "CONSULTA_MARCA_ACCESO_DIRECTO";
	public static final String DEVOLVER_SERVICIO_SOLICITADO = "DEVOLVER_SERVICIO_SOLICITADO";
	public static final String ACTUALIZA_ESTADO_PRESTACION_ALTA_FRECUENCIA = "ACTUALIZA_ESTADO_PRESTACION_ALTA_FRECUENCIA";
	public static final String CONSULTA_SALARIO_MINIMO = "CONSULTA_SALARIO_MINIMO";
	public static final String VALIDAR_PERFIL_CORDINADOR = "VALIDAR_PERFIL_CORDINADOR";
	public static final String CONSULTA_RESULTADO_MALLA = "CONSULTA_RESULTADO_MALLA";

	/**
	 * Atributo que determina el nombre del sp de la consulta del codigo de
	 * habilitacion
	 */
	public static final String CONSULTA_CODIGO_HABILITACION = "CONSULTA_CODIGO_HABILITACION";

	public static final int MODELO_PROCEDIMIENTOS = 4;

	public static final int MODELO_MEDICAMENTOS = 5;

	/**
	 * Atributo que determina el nombre del sp de la consulta de los conceptos
	 * de pago de una OPS
	 */
	public static final String CONSULTA_CONCEPTOS_PAGO_OPS = "CONSULTA_CONCEPTOS_PAGO_OPS";

	/**
	 * Atributo que determina el nombre del sp del guardado de documentos de una
	 * caja.
	 */
	public static final String GUARDAR_DOCUMENTO_CAJA = "GUARDAR_DOCUMENTO_CAJA";

	public static final String CONSULTA_BANCO_CAJA = "CONSULTA_BANCO_CAJA";

	public static final String CONSULTA_METODO_DEVOLUCION_CAJA = "CONSULTA_METODO_DEVOLUCION_CAJA";

	public static final String GUARDAR_DATOS_CONTACTO_CAJA = "GUARDAR_DATOS_CONTACTO_CAJA";

	public static final String GENERAR_INFORME_CIERRE_CAJA = "GENERAR_INFORME_CIERRE_CAJA";

	public static final String OBTENER_CAUSALES_DESCUADRE_CAJA = "OBTENER_CAUSALES_DESCUADRE_CAJA";
	public static final int CODIGO_TIPO_DOC_RECIBO_CAJA = 1;
	public static final int CODIGO_ESTADO_GENERADO_CAJA = 1;

	/**
	 * No se realizó el almacenamiento del Documento
	 */
	public static final String MENSAJE_ERROR_NO_GUARDA_DOC = "No se realizó el almacenamiento del Documento";
	/**
	 * Mensaje cuando no se guarda el detalle
	 */
	public static final String MSG_ERROR_NO_GUARDA_DETALLE = "No se realizó el almacenamiento del detalle del movimiento";

	public static final String REPORTE_CAJA_DETALLADO = "REPORTE_CAJA_DETALLADO";

	public static final String CONSULTAR_CAJA_ABIERTA = "CONSULTAR_CAJA_ABIERTA";

	public static final String CONSULTAR_CAUSALES_DESCUADRE = "CONSULTAR_CAUSALES_DESCUADRE";

	public static final String CONSULTAR_OFICINA = "CONSULTAR_OFICINA";

	public static final String CONSULTAR_OFICINA_MOVIMIENTO_ACTUAL = "CONSULTAR_OFICINA_MOVIMIENTO_ACTUAL";

	public static final String GUARDAR_SOPORTE_CAJA = "GUARDAR_SOPORTE_CAJA";

	public static final String CIERRE_OFICINA = "CIERRE_OFICINA";

	public static final String INSERTAR_DETALLE_MOV = "INSERTAR_DETALLE_MOV";

	public static final String ACTUALIZAR_CAJA = "ACTUALIZAR_CAJA";

	public static final String INSERTAR_DOC_RELACIONADO = "INSERTAR_DOC_RELACIONADO";

	public static final String OBTENER_NOTAS_CREDITO = "OBTENER_NOTAS_CREDITO";

	public static final String CONSULTA_OPS_VALOR_NOTAS_CREDITO = "CONSULTA_OPS_VALOR_NOTAS_CREDITO";

	public static final String ORQUESTADOR_GUARDAR_RECIBO_CAJA = "ORQUESTADOR_GUARDAR_RECIBO_CAJA";

	public static final String CONSULTA_SEDES = "CONSULTA_SEDES";

	public static final String BUSCADOR_USUARIOS_LOGIN = "BUSCADOR_USUARIOS_LOGIN";

	public static final int CONSECUTIVO_CONCEPTO_CUOTA_MODERADORA = 1;

	public static final String BUSCADOR_AFILIADOS_ID = "BUSCADOR_AFILIADOS_ID";

	public static final String CONSULTA_PRESTACIONES_NUMERO_UNICO_OPS = "CONSULTA_PRESTACIONES_NUMERO_UNICO_OPS";

	public static final String CONSULTA_EXONERACIONES_PAGO = "CONSULTA_EXONERACIONES_PAGO";

	public static final String CONSULTA_ESTADOS_DOCUMENTO_CAJA = "CONSULTA_ESTADOS_DOCUMENTO_CAJA";

	public static final String GENERAR_INFORME_NOTAS_CREDITO = "GENERAR_INFORME_NOTAS_CREDITO";

	public static final String CONSULTA_OPS_DOCUMENTO_CAJA = "CONSULTA_OPS_DOCUMENTO_CAJA";

	public static final String CONSULTA_DATOS_CONTACTO_DOCUMENTO = "CONSULTA_DATOS_CONTACTO_DOCUMENTO";

	public static final String ACTUALIZAR_NOTA_CREDITO_CAJA = "ACTUALIZAR_NOTA_CREDITO_CAJA";

	public static final String ACTUALIZAR_CONTACTO_CAJA = "ACTUALIZAR_CONTACTO_CAJA";

	public static final String GUARDA_APERTURA_CAJA = "GUARDA_APERTURA_CAJA";

	public static final String GUARGAR_CIERRE_CAJA = "GUARGAR_CIERRE_CAJA";

	public static final String CONSULTAR_COORDINADOR = "CONSULTAR_COORDINADOR";

	public static final String CONSULTAR_DENOMINACIONES = "CONSULTAR_DENOMINACIONES";

	public static final String CONSULTAR_MOVIMIENTOS_POR_OFICINA = "CONSULTAR_MOVIMIENTOS_POR_OFICINA";

	public static final String CONSULTAR_USUARIO_WEB = "CONSULTAR_USUARIO_WEB";

	public static final String CONSULTA_OPS_VALOR_NUM_SOL = "CONSULTA_OPS_VALOR_NUM_SOL";

	public static final String TOTALES_CIERRE = "TOTALES_CIERRE";

	public static final String CONSULTA_APERTURA_CAJA = "CONSULTA_APERTURA_CAJA";

	public static final String CONSULTAR_OFICINA_CAJA = "CONSULTAR_OFICINA_CAJA";

	public static final int VER_GENERAR = 1;

	/**
	 * Atributo que determina la consulta de las prestaciones asociadas al tipo
	 * de prestacion
	 */
	public static final String CONSULTA_PRESTACIONES_X_TIPO_PRESTACION = "CONSULTA_PRESTACIONES_X_TIPO_PRESTACION";
	public static final String CONSULTA_TIPOS_CODIFICACION = "CONSULTA_TIPOS_CODIFICACION";
	public static final String CONSULTA_DATOS_BASICOS_PRESTACION = "CONSULTA_DATOS_BASICOS_PRESTACION";
	public static final String CONSULTA_PRESTACION_POR_PLANES = "CONSULTA_PRESTACION_POR_PLANES";
	public static final String CONSULTA_TIEMPO_ENTREGA = "CONSULTA_TIEMPO_ENTREGA";
	public static final String CONSULTA_MARCAS_PRESTACION = "CONSULTA_MARCAS_PRESTACION";
	public static final String CONSULTA_POLIT_AUTH = "CONSULTA_POLIT_AUTH";
	public static final String CONSULTA_CONVENIO_PRESTACION = "CONSULTA_CONVENIO_PRESTACION";
	public static final String CONSULTA_DIRECCIONAMIENTO_NORMAL = "CONSULTA_DIRECCIONAMIENTO_NORMAL";
	public static final String CONSULTA_DIRECCIONAMIENTO_X_RIESGO = "CONSULTA_DIRECCIONAMIENTO_X_RIESGO";
	public static final String CONSULTA_DIRECCIONAMIENTO_EXCEPCION = "CONSULTA_DIRECCIONAMIENTO_EXCEPCION";
	public static final String CONSULTA_HOMOLOGO = "CONSULTA_HOMOLOGO";
	public static final String CONSULTA_CUPSXRIESGO = "CONSULTA_CUPSXRIESGO";
	public static final String CONSULTA_CUPSXPLANXCARGO = "CONSULTA_CUPSXPLANXCARGO";
	public static final String CONSULTA_DATOS_CUMS = "CONSULTA_DATOS_CUMS";
	public static final String CONSULTA_ATRIBUTOS_PRESTACION = "CONSULTA_ATRIBUTOS_PRESTACION";
	public static final String CONSULTA_PAGOS_FIJOS = "CONSULTA_PAGOS_FIJOS";
	public static final String CONSULTA_CONVENIOS_IPS = "CONSULTA_CONVENIOS_IPS";

	/**
	 * Atributos Tipos de Cobertura Bodega politica Datos basicos
	 */

	public static final int TIPO_COBERTURA_POS = 1;
	public static final int TIPO_COBERTURA_NO_POS = 2;
	public static final int TIPO_COBERTURA_POS_COND = 3;
	public static final int TIPO_COBERTURA_POS_AUTH = 4;
	public static final int INICIO_SUBSTRING = 0;
	public static final int FIN_SUBSTRING_DESCRIPCION_LARGA = 50;

	public static final String GUARDAR_DIRECCIONAMIENTO_PRESTACION_SIN_OPS = "GUARDAR_DIRECCIONAMIENTO_PRESTACION_SIN_OPS";
	public static final String CONSULTAR_PRESTACIONES = "CONSULTAR_PRESTACIONES";
	public static final String CONSULTA_MEDICO_AUDITOR = "CONSULTA_MEDICO_AUDITOR";
	public static final String CONSULTA_RECOBROS_AUDITORIA = "CONSULTA_RECOBROS_AUDITORIA";
	public static final String GUARDAR_RECOBRO_X_PRESTACION = "GUARDAR_RECOBRO_X_PRESTACION";
	public static final String CONSULTAR_CAUSALES_NO_COBRO_CUOTA = "CONSULTAR_CAUSALES_NO_COBRO_CUOTA";
	public static final String CONSULTAR_RECIBO_CAJA = "CONSULTAR_RECIBO_CAJA";
	public static final String CONSULTA_GRUPOS_ENTREGA = "CONSULTA_GRUPOS_ENTREGA";
	public static final String CONSULTA_RESUMEN_OPS = "CONSULTA_RESUMEN_OPS";	
	public static final String RECALCULAR_DETALLE_OPS = "RECALCULAR_DETALLE_OPS";
	/**
	 * Constantes para nombres de columnas
	 */

	public static final String CONSECUTIVO_CODIFICACION = "cnsctvo_cdfccn";
	public static final String CODIGO_CODIFICACION = "cdgo_cdfccn";
	public static final String DESCRIPCION_CODIFICACION = "dscrpcn_cdfccn";
	public static final String IPS_CODIGO = "ips_cdgo";
	public static final String IPS_DESCRIPCION = "ips_dscrpcn";
	public static final String PLAN_DESCRIPCION = "pln_dsc";
	public static final String VALOR = "vlr";
	public static final String TIPO_ATENCION = "tpo_atncn";
	public static final String FORMULA_LIQUIDACION = "frmla_lqdcn";
	public static final String ACCESO_DIRECTO = "accso_drcto";
	public static final String DIRECCION = "drccn";
	public static final String TELEFONO = "tlfno";
	public static final String INICIO_VIGENCIA = "inco_vgnca";
	public static final String FIN_VIGENCIA = "fn_vgnca";
	public static final String FINVIGENCIA = "fnVgnca";
	public static final String GENERO = "gnro";
	public static final String NIVEL = "nvl";
	public static final String COBERTURA_POS = "cbrtra_ps";
	public static final String COBERTURA_NO_POS = "cbrtra_no_ps";
	public static final String COBERTURA_POS_COND = "cbrtra_ps_cnd";
	public static final String COBERTURA_AUTO_F = "cbrtra_ato_f";
	public static final String CODIGO_PLAN = "cdgo_pln";
	public static final String COBERTURA_POR_PLAN = "cbrtra_pr_pln";
	public static final String SEMANAS_CARENCIA = "smns_crnca";
	public static final String CANTIDAD_MAXIMA_PERMITIDA = "ctdd_mxma_prmtda";
	public static final String FRECUENCIA_ENTREGA = "frcnca_entrga";
	public static final String TOPE_MAXIMO_POR_ORDEN = "tpe_mxm_pr_ordn";
	public static final String VIGENCIA_AUTORIZACION_MEDICA = "vngcna_atrzcn_mdca";
	public static final String EDAD_MINIMA = "edd_mnma";
	public static final String EDAD_MAXIMA = "edd_mxma";
	public static final String ITEM_PRESUPUESTO = "itm_prspsto";
	public static final String CODIGO_GRUPO = "cdgo_grpo";
	public static final String DESCRIPCION_GRUPO = "dscrpn_grpo";
	public static final String TIEMPO_ENTREGA = "tmpo_entrga";
	public static final String DESCRIPCION_TIPO_MARCA_PRESTACION = "dscrpcn_tpo_mrca_prstcn";
	public static final String INICIO_VIGENCIA_MARCA_PRESTACION = "inco_vgnca_mrca_prstcn";
	public static final String FIN_VIGENCIA_MARCA_PRESTACION = "fn_vgnca_mrca_prstcn";
	public static final String CONSECUTIVO_CODIGO_TIPO_MODELO = "cnsctvo_cdgo_tpo_mdlo";
	public static final String DESCRIPCION_TIPO_MODELO = "dscrpcn_tpo_mdlo";
	public static final String DESCRIPCION_CIUDAD = "dscrpcn_cdd";
	public static final String CODIGO_DIAGNOSTICO = "cdgo_mdlo_cptcn_dgnstcs";
	public static final String DESCRIPCION_DIAGNOSTICO = "dscrpcn_mdlo_cptcn_dgnstcs";
	public static final String CIUDAD_DESCRIPCION = "cdd_dsc";
	public static final String CODIGO_PRESENTACION = "cdgo_prsntcn";
	public static final String DESCRIPCION_PRESENTACION = "dscrpcn_prsntcn";
	public static final String CANTIDAD_PRESENTACION = "cntdd_prsntcn";
	public static final String DESCRIPCION_FORMA_ENTREGA = "dscrpcn_frma_entrga";
	public static final String OBSERVACIONES_PRESENTACION = "obsrvcns_prsntcn";
	public static final String CODIGO_FORMA_FRMCTCA = "cdgo_frma_frmctca";
	public static final String DESCRIPCION_FORMA_FRMCTCA = "dscrpcn_frma_frmctca";
	public static final String CANTIDAD_FRCCNDA = "cntdd_frccnda";
	public static final String INDCCNS_INVMA = "indccns_invma";
	public static final String CNTRNDCCNS_INVIMA = "cntrndccns_invma";
	public static final String CODIGO_VA_ADMINISTRACION = "cdgo_va_admnstrcn";
	public static final String DESCRIPCION_VA_ADMINISTRACION = "dscrpcn_va_admnstrcn";
	public static final String CODIGO_MODULO_LIQUIDACION_CONCEPTO = "cdgo_mdlo_lqdcn_cncpto";
	public static final String DESCRIPCION_MODULO_LIQUIDACION_CONCEPTO = "dscrpcn_mdlo_lqdcn_cncpto";
	public static final String CODIGO_INTERNO = "cdgo_intrno";
	public static final String NOMBRE_SUCURSAL = "nmbre_scrsl";
	public static final String DESCRIPCION_ZONA = "dscrpcn_zna";
	public static final String DESCRIPCION_PLAN = "dscrpcn_pln";
	public static final String SFCNCA_IPS = "sfcnca_ips";
	public static final String NIVEL_PRIORIDAD = "nvl_prrdd";
	public static final String CODIGO_RIESGO_DIAGNOSTICO = "cdgo_rsgo_dgnstco";
	public static final String DESCRIPCION_RIESGO_DIAGNOSTICO = "dscrpcn_rsgo_dgnstco";
	public static final String DESCRIPCION_TIPO_MARCA = "dsc_tpo_mrca";
	public static final String DESCRIPCION_TIPO_AUTH_PRES = "dsc_tpo_ath_prs";
	public static final String DESCRIPCION_AGRUPADOR = "dsc_agrpdr";
	public static final String DESCRIPCION_POLITICA = "dsc_pltca";

	public static final String CONSECUTIVO_CODIGO_TIPO_PLAN = "cnsctvo_cdgo_tpo_pln";
	public static final String CODIGO_TIPO_PLAN = "cdgo_tpo_pln";
	public static final String DESCRIPCION_TIPO_PLAN = "dscrpcn_tpo_pln";
	public static final String CODIGO_TIPO_AFILIADO = "cdgo_tpo_afldo";
	public static final String DESCRIPCION = "dscrpcn";
	public static final String CONSECUTIVO_CODIGO_TIPO_AFILIADO = "cnsctvo_cdgo_tpo_afldo";
	public static final String CONSECUTIVO_CODIGO_ESTADO_MEGA = "cnsctvo_cdgo_estdo_mga";
	public static final String CODIGO_ESTADO_MEGA = "cdgo_estdo_mga";
	public static final String DESCRIPCION_ESTADO_MEGA = "dscrpcn_estdo_mga";
	public static final String CODIGO_ESTADO_ATENCION = "cdgo_estdo_atncn";
	public static final String DESCRIPCION_ESTADO_ATENCION = "dscrpcn_estdo_atncn";
	public static final String CONSECUTIVO_CODIGO_GRUPO_ENTREGA = "cnsctvo_cdgo_grpo_entrga";
	public static final String CODIGO_GRUPO_ENTREGA = "cdgo_grpo_entrga";
	public static final String DESCRIPCION_GRUPO_ENTREGA = "dscrpcn_grpo_entrga";

	public static final String CONSULTA_DATOS_ADICIONALES_PRESTACION = "CONSULTA_DATOS_ADICIONALES_PRESTACION";

	public static final String GUARDAR_MODIFICACION_PRESTACION = "GUARDAR_MODIFICACION_PRESTACION";
	/**
	 * Atributo que determina el estado de la prestacion en ingresado
	 */
	public static final Integer COD_ESTADO_INGRESADO = 2;

	public static final String VALIDAR_EXISTENCIA_RECIBOS_CAJA = "VALIDAR_EXISTENCIA_RECIBOS_CAJA";

	public static final int VALOR_CERO = 0;

	/**
	 * Atributo que determina el estado de la prestacion en aprobado
	 */
	public static final Integer COD_ESTADO_APROBADO = 7;
	public static final String CONSULTA_AFILIADO = "CONSULTA_AFILIADO";
	public static final String PERFIL_COORDINADOR = "Coordinador de Prestaciones Medicas";
	public static final String PERFIL_ADMINISTRADOR_CNA = "AdministradorCNA";

	public static final String CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR = "CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR";

	public static final String CDGO_CDFCCN = "cdgo_cdfccn";
	public static final String DSCRPCN_CDFCCN = "dscrpcn_cdfccn";
	public static final String DSCRPCN_NO_CNFRMDD_VLDCN = "dscrpcn_no_cnfrmdd_vldcn";
	public static final String INFRMCN_ADCNL_NO_CNFRMDD = "infrmcn_adcnl_no_cnfrmdd";
	public static final String ORGN_DVLCN = "orgn_dvlcn";

	public static final String DESCRIPCION_AGRUPADOR_PRESTACION = "dscrpcn_agrpdr_prstcn";
	public static final String CONSECUTIVO_AGRUPADOR_PRESTACION = "cnsctvo_vgnca_agrpdr_prstcn";
	public static final String CANTIDAD_OPS_CONSULTADAS = "cntdd_ops_cnsltds";
	public static final String CANTIDAD_OPS_MODIFICADA = "cntdd_ops_mdfcds";
	public static final String VALOR_OPS_CONSULTADAS = "vlr_ops_cnsltds";
	public static final String VALOR_OPS_MODIFICADAS = "vlr_ops_mdfcds";

	public static final String CONSULTA_TIPOS_AFILIADO = "CONSULTA_TIPOS_AFILIADO";
	public static final String CONSULTA_TIPOS_PLAN = "CONSULTA_TIPOS_PLAN";
	public static final String CONSULTA_ESTADOS_MEGA = "CONSULTA_ESTADOS_MEGA";

	public static final String CONSECUTIVO_TIPO_IDETIFICACION = "cdgo_tpo_idntfccn";
	public static final String NUMERO_IDETIFICACION = "nmro_idntfccn";
	public static final String NOMBRE = "nmbre";
	public static final String NUMERO_OPS = "nmro_ops";
	public static final String TUTELA = "ttla";
	public static final String PLAN_PAC = "pln_pc";
	public static final String INCAPACIDAD = "incpcdd";
	public static final String FECHA_EXPEDICION = "fcha_expdcn";
	public static final String DESCRIPCION_CLASIFICACION_EVENTO = "dscrpcn_clsfccn_evnto";
	public static final String FECHA_ENTREGA = "fcha_entrga";
	public static final String VALOR_OPS = "vlr_rfrnca";
	public static final String CONSECUTIVO_CODIGO_OFICINA = "cnsctvo_cdgo_ofcna";
	public static final String NUAM = "nuam";
	public static final String CAMBIO_FECHA_ENTREGA = "CAMBIO_FECHA_ENTREGA";
	public static final String CONSECUTIVO_PRESTACION = "cnsctvo_prstcn";
	public static final int VALOR_UNO = 1;
	public static final Double VALOR_CERO_D = 0d;
	public static final int CIEN = 100;
	public static final String TOTAL_LABEL = "TOTAL";
	public static final String FORMATO_DECIMAL = "###,###.##";
	public static final String PORCENTAJE_CANTIDAD_OPS_CONSULTADA = "prctj_cntdd_ops_cnsltds";
	public static final String PORCENTAJE_VALOR_OPS_CONSULTADA = "prctj_vlr_ops_cnsltds";
	public static final String PORCENTAJE_CANTIDAD_OPS_MODIFICADA = "prctj_cntdd_ops_mdfcds";
	public static final String PORCENTAJE_VALOR_OPS_MODIFICADA = "prctj_vlr_ops_mdfcds";


	private ConstantesData() {
		/* clase utilizaría constructor privado para evitar instancias */
	}
}
