package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
/**
 * Clase que se encarga de consultar el sp bdcna.cja.spRCObtenerReciboCajaSolicitud
 * @author geniarjag
 *
 */
public class SpRCObtenerReciboCajaSolicitudDelegate implements Serializable, SQLDelegate {
	private static final long serialVersionUID = 6449459315635873274L;
	private int consecutivoSolicitud;
	private ReciboCajaJasperVO reciboCajaJasper;
	private List<ConceptoPagoOpsVO> conceptosPago;
	

	/**
	 * Constructor de la clase.
	 * @param consecutivoSolicitud solicitud a consultar recibo de caja.
	 */
	public SpRCObtenerReciboCajaSolicitudDelegate(int consecutivoSolicitud) {
		super();
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_RECIBO_CAJA));
		stmt.setInt(1, this.consecutivoSolicitud);
		reciboCajaJasper = new ReciboCajaJasperVO();
		conceptosPago = new ArrayList<ConceptoPagoOpsVO>(2);
		ResultSet rs = stmt.executeQuery();
		
		while (rs.next()) {
			ConceptoPagoOpsVO concepto = new ConceptoPagoOpsVO();
			concepto.setConsecutivoCodigoConcepto(rs.getInt("cnsctvo_cdgo_cncpto_pgo"));
			concepto.setNombreConceptoPago(rs.getString("dscrpcn_cncpto_pgo"));
			concepto.setValorConceptoPago(rs.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			agregarConcepto(concepto);
			reciboCajaJasper.setIdDocumento(rs.getInt("nmro_dcmnto"));
			reciboCajaJasper.setIdentificacionAfiliado(rs.getString("cdgo_tpo_idntfccn")+"-"+rs.getString("nmro_idntfccn"));
			reciboCajaJasper.setNombreAfiliado(rs.getString("nmbre_afldo"));
			reciboCajaJasper.setOficina(rs.getString("nmbre_ofcna"));
			reciboCajaJasper.setUsuarioCreador(rs.getString("usro_crcn"));
			reciboCajaJasper.setTotalPagar(rs.getDouble("vlr_rcbo"));
			reciboCajaJasper.setSumatoriaNotasCredito(rs.getDouble("vlr_nta_crdto"));
			reciboCajaJasper.setValorEfectivo(rs.getDouble("vlr_rcbdo"));
			reciboCajaJasper.setValorDevolver(0);
			reciboCajaJasper.setFechaReporte(rs.getDate("fcha_rcbo"));
		}
		reciboCajaJasper.setConceptosPago(conceptosPago);
	}
	
	private void agregarConcepto(ConceptoPagoOpsVO concepto){
		if (conceptosPago.isEmpty()){
			conceptosPago.add(concepto);
		} else {
			procesarListaConceptos(concepto);
		}
	}
	
	private void procesarListaConceptos(ConceptoPagoOpsVO concepto){
		boolean encontroConcepto= false;
		for(int i=0; i<conceptosPago.size(); i++){
			ConceptoPagoOpsVO conceptoPagoOpsVO = conceptosPago.get(i);
			if (conceptoPagoOpsVO.getConsecutivoCodigoConcepto() == concepto.getConsecutivoCodigoConcepto()){
				conceptoPagoOpsVO.setValorConceptoPago(conceptoPagoOpsVO.getValorConceptoPago()+concepto.getValorConceptoPago());
				encontroConcepto = true;
			}
		}
		if (!encontroConcepto){
			conceptosPago.add(concepto);
		}
	}

	public ReciboCajaJasperVO getReciboCajaJasper() {
		return reciboCajaJasper;
	}
}
