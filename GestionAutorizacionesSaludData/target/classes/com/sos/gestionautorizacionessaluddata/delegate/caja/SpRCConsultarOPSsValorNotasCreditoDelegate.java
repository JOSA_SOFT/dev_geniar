package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeOPSVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta las ops y su valor
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarOPSsValorNotasCreditoDelegate implements SQLDelegate {

	private Integer numeroNotaCredito;
	private List<InformeOPSVO> listInformeOPSVO;

	/**
	 * Constructor
	 * 
	 * @param numeroNotaCredito
	 */
	public SpRCConsultarOPSsValorNotasCreditoDelegate(Integer numeroNotaCredito) {
		super();
		this.numeroNotaCredito = numeroNotaCredito;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultaOPSValorNotasCredito
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_OPS_VALOR_NOTAS_CREDITO));

		stmt.setInt(1, this.numeroNotaCredito);

		ResultSet result = stmt.executeQuery();

		int vlrDcmntoCja = result.findColumn("vlr_dcmnto_cja");
		int nmroUncoOpsGrupo = result.findColumn("nmro_unco_ops_grupo");
		listInformeOPSVO = new ArrayList<InformeOPSVO>();
		while (result.next()) {
			InformeOPSVO informeOPSVO = new InformeOPSVO();
			informeOPSVO.setNumOPSs(result.getString(nmroUncoOpsGrupo));
			informeOPSVO.setValor(result.getDouble(vlrDcmntoCja));
			listInformeOPSVO.add(informeOPSVO);
		}
	}

	public List<InformeOPSVO> getListInformeOPSVO() {
		return listInformeOPSVO;
	}
}