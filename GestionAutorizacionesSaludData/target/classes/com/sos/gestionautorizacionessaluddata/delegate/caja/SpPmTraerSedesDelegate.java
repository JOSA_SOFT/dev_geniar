package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que se encarga de consultar el proceso almacenado
 * BDAfiliacionValidador.dbo.SpPmTraerSedes
 * 
 * @author GENIAR
 *
 */
public class SpPmTraerSedesDelegate implements SQLDelegate {

	private String codigoSede;
	private Date fechaReferencia;
	private String descripcion;
	private String visibleUsuario;
	private List<ParametroVO> listaResultadoParametros;

	/**
	 * Constructor de la clase
	 * 
	 * @param codigoSede
	 * @param fechaReferencia
	 * @param descripcion
	 * @param visibleUsuario
	 */
	public SpPmTraerSedesDelegate(String codigoSede, Date fechaReferencia,
			String descripcion, String visibleUsuario) {
		super();
		this.codigoSede = codigoSede;
		this.fechaReferencia = fechaReferencia;
		this.descripcion = descripcion;
		this.visibleUsuario = visibleUsuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		listaResultadoParametros = new ArrayList<ParametroVO>();
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_SEDES));

		if (codigoSede == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setString(1, codigoSede);
		}
		if (fechaReferencia == null) {
			stmt.setNull(2, Types.DATE);
		} else {
			stmt.setDate(2, new java.sql.Date(fechaReferencia.getTime()));
		}
		if (descripcion == null) {
			stmt.setNull(3, Types.CHAR);
		} else {
			stmt.setString(3, descripcion);
		}
		if (visibleUsuario == null) {
			stmt.setNull(4, Types.CHAR);
		} else {
			stmt.setString(4, visibleUsuario);
		}
		
		ResultSet resultS = stmt.executeQuery();

		while (resultS.next()) {
			ParametroVO param = new ParametroVO();
			param.setConsecutivo(resultS.getInt("cnsctvo_cdgo_sde"));
			param.setDescipcion(resultS.getString("dscrpcn_sde"));
			param.setCodigo(resultS.getString("cdgo_sde"));
			listaResultadoParametros.add(param);
		}
	}

	public List<ParametroVO> getListaResultadoParametros() {
		return listaResultadoParametros;
	}

}
