package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consultar la informacion de una oficina
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarOficinaDelegate implements SQLDelegate {

	private Integer cnsctvoCdgoOfcna;
	private OficinasVO oficinasVO;

	/**
	 * Consulta informacion de oficina por el consecutivo
	 * 
	 * @param cnsctvoCdgoOfcna
	 */
	public SpRCConsultarOficinaDelegate(Integer cnsctvoCdgoOfcna) {
		super();
		this.cnsctvoCdgoOfcna = cnsctvoCdgoOfcna;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCAperturaCaja
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_OFICINA));

		stmt.setInt(1, this.cnsctvoCdgoOfcna);

		ResultSet result = stmt.executeQuery();

		int consecutivoCodigoOficina = result.findColumn("cnsctvo_cdgo_ofcna");
		int codigoOficina = result.findColumn("cdgo_ofcna");
		int descripcionOficina = result.findColumn("dscrpcn_ofcna");

		if (result.next()) {
			OficinasVO oficinasVOResult = new OficinasVO();
			oficinasVOResult.setConsecutivoCodigoOficina(result
					.getInt(consecutivoCodigoOficina));
			oficinasVOResult.setCodigoOficina(result.getString(codigoOficina));
			oficinasVOResult.setDescripcionOficina(result.getString(descripcionOficina));
			this.oficinasVO = oficinasVOResult;
		}
	}

	public OficinasVO getOficinasVO() {
		return oficinasVO;
	}

}
