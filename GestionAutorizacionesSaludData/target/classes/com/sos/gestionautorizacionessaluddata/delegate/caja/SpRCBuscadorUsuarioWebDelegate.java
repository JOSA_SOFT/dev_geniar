/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * @author GENIAR
 *
 */
public class SpRCBuscadorUsuarioWebDelegate implements SQLDelegate {

	private String usuario;
	private String login;
	private List<UsuarioWebVO> listaUsuarios;
	
	/**
	 * Constructor de la clase
	 * @param usuario
	 * @param login
	 */
	public SpRCBuscadorUsuarioWebDelegate(String usuario, String login) {
		super();
		this.usuario = usuario;
		this.login = login;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		listaUsuarios = new ArrayList<UsuarioWebVO>();
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.BUSCADOR_USUARIOS_LOGIN));
		
		if (login == null){
			stmt.setNull(1, Types.CHAR);
		}else{
			stmt.setString(1, login);
		}
		
		if (usuario == null){
			stmt.setNull(2, Types.CHAR);
		}else{
			stmt.setString(2, usuario);
		}
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			UsuarioWebVO usu = new UsuarioWebVO();
			usu.setLgnUsro(rs.getString("lgn_usro"));
			usu.setPrmrNmbreUsro(rs.getString("prmr_nmbre_usro"));
			usu.setSgndoNmbreUsro(rs.getString("sgndo_nmbre_usro"));
			usu.setPrmrAplldoUsro(rs.getString("prmr_aplldo_usro"));
			usu.setSgndoAplldoUsro(rs.getString("sgndo_aplldo_usro"));
			listaUsuarios.add(usu);
		}
	}

	public List<UsuarioWebVO> getListaUsuarios() {
		return listaUsuarios;
	}

}
