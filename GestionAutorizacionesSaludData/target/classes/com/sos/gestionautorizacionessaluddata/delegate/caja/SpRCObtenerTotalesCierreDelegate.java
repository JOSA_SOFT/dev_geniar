package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.TotalCierreVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Suma las notas credito en estado cobrado y los recibos de caja en estado
 * generado por medio del consecutivo de un movimiento caja en estado apertura
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCObtenerTotalesCierreDelegate implements SQLDelegate {

	private Integer consecutivoMovimientoCaja;
	private TotalCierreVO totalesCierreVO;

	/**
	 * Recibe el el consecutivo del movimiento caja en estado abierto o apertura
	 * 
	 * @param consecutivoMovimientoCaja
	 */
	public SpRCObtenerTotalesCierreDelegate(Integer consecutivoMovimientoCaja) {
		super();
		this.consecutivoMovimientoCaja = consecutivoMovimientoCaja;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCSumarNotasCreditoEstadoCobrado
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.TOTALES_CIERRE));
		stmt.setInt(1, this.consecutivoMovimientoCaja);

		ResultSet result = stmt.executeQuery();
		int notaCredito = result.findColumn("NOTAS_CREDITO");
		int reciboCaja = result.findColumn("RECIBOS_CAJA");
		if (result.next()) {
			totalesCierreVO = new TotalCierreVO();
			totalesCierreVO.setSumNotaCredito(result.getDouble(notaCredito));
			totalesCierreVO.setSumReciboCaja(result.getDouble(reciboCaja));
		}
	}

	public TotalCierreVO getTotalesCierreVO() {
		return totalesCierreVO;
	}

	public void setTotalesCierreVO(TotalCierreVO totalesCierreVO) {
		this.totalesCierreVO = totalesCierreVO;
	}
}