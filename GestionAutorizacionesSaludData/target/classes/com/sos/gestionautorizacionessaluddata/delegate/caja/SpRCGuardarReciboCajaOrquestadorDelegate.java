package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * DELEGATE OBTENER LAS NOTAS CRÉDITO DEL GRUPO FAMILIAR
 * 
 * @author JOSE OLMEDO SOTO AGUIRRE
 *
 */
public class SpRCGuardarReciboCajaOrquestadorDelegate implements SQLDelegate {
	private int nuiAfiliado;
	private String notas;
	private String opsSeleccionada;
	private ReciboCajaVO recibo;
	private String usuario;
	private int numeroRecibo;

	/**
	 * Constructor de la clase.
	 * @param nuiAfiliado
	 * @param notas
	 * @param opsSeleccionada
	 * @param recibo
	 * @param usuario
	 */
	public SpRCGuardarReciboCajaOrquestadorDelegate(int nuiAfiliado, String notas,
			String opsSeleccionada, ReciboCajaVO recibo, String usuario) {
		super();
		this.nuiAfiliado = nuiAfiliado;
		this.notas = notas;
		this.opsSeleccionada = opsSeleccionada;
		this.recibo = recibo;
		this.usuario = usuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement stmt = conn.prepareCall(ConsultaSp
				.getString(ConstantesData.ORQUESTADOR_GUARDAR_RECIBO_CAJA));
		stmt.setDouble(1, recibo.getTotalPagar());
		stmt.setDouble(2, recibo.getValorEfectivo());
		stmt.setString(3, usuario);
		stmt.setInt(4, nuiAfiliado);
		stmt.setString(5, recibo.getObservaciones());
		stmt.setString(6, opsSeleccionada);
		if (notas == null){
			stmt.setNull(7, Types.VARCHAR);
		}else{
			stmt.setString(7, notas);
		}
		
		
		stmt.registerOutParameter(8, Types.INTEGER);
		stmt.executeUpdate();
		numeroRecibo = stmt.getInt(8);
	}

	public int getNumeroRecibo() {
		return numeroRecibo;
	}
}
