package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Se encarga de consultar el SP cja.spRCObtenerEstadoDocumentos
 * @author GENIAR
 *
 */
public class SpRCObtenerEstadoDocumentosDelegate implements SQLDelegate {

	
	private List<ParametroVO> listaResultadoParametros;
	private String codigo;
	private Date fechaReferencia;
	private String descripcion;
	private String visibleUsuario;
	private Integer tipoDocumento;
	
	public SpRCObtenerEstadoDocumentosDelegate(String codigo,
			Date fechaReferencia, String descripcion, String visibleUsuario,
			Integer tipoDocumento) {
		super();
		this.codigo = codigo;
		this.fechaReferencia = fechaReferencia;
		this.descripcion = descripcion;
		this.visibleUsuario = visibleUsuario;
		this.tipoDocumento = tipoDocumento;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_ESTADOS_DOCUMENTO_CAJA));
		
		if (codigo == null) {
			stmt.setNull(1, Types.CHAR);
		} else {
			stmt.setString(1, codigo);
		}

		if (descripcion == null) {
			stmt.setNull(2, Types.CHAR);
		} else {
			stmt.setString(2, descripcion);
		}
		
		stmt.setDate(3, new java.sql.Date(fechaReferencia.getTime()));
		stmt.setString(4, visibleUsuario);
		stmt.setInt(5, tipoDocumento);
		
		listaResultadoParametros = new ArrayList<ParametroVO>();
		ResultSet resultS = stmt.executeQuery();

		while (resultS.next()) {
			ParametroVO param = new ParametroVO();
			param.setConsecutivo(resultS.getInt("cnsctvo_cdgo_estdo_dcmnto_cja"));
			param.setDescipcion(resultS.getString("dscrpcn_estdo_dcmnto_cja"));
			param.setCodigo(resultS.getString("cdgo_estdo_dcmnto_cja"));
			listaResultadoParametros.add(param);
		}
	}

	public List<ParametroVO> getListaResultadoParametros() {
		return listaResultadoParametros;
	}

}

