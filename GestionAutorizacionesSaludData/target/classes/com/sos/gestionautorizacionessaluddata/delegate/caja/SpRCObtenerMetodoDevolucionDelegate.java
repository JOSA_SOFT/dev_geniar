package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta los metodos de devolucion
 * @author GENIAR
 *
 */
public class SpRCObtenerMetodoDevolucionDelegate implements SQLDelegate {

	private List<ParametroVO> listaResultadoParametros;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		listaResultadoParametros = new ArrayList<ParametroVO>();
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_METODO_DEVOLUCION_CAJA));
		
		ResultSet resultS = stmt.executeQuery();

		while (resultS.next()) {
			ParametroVO param = new ParametroVO();
			param.setConsecutivo(resultS.getInt("cnsctvo_cdgo_mtdo_dvlcn_dnro"));
			param.setDescipcion(resultS.getString("dscrpcn_mtdo_dvlcn_dnro"));
			listaResultadoParametros.add(param);
		}
		
	}

	public List<ParametroVO> getListaResultadoParametros() {
		return listaResultadoParametros;
	}

}
