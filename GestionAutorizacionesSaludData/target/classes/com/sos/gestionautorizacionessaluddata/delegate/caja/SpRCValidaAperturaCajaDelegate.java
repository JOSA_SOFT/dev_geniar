package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * DELEGADO PARA APERTURA DE LA CAJA
 * 
 * @author JOSÉ OLMEDO SOTO AGUIRRE, 28/10/2016
 *
 */
public class SpRCValidaAperturaCajaDelegate implements SQLDelegate {

	private int result;
	private String user = "";

	/**
	 * CONSTRUCTOR QUE INICIALIZA EL NOMBRE DE USUARIO
	 * 
	 * @param user
	 */
	public SpRCValidaAperturaCajaDelegate(String user) {
		this.user = user;

	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_APERTURA_CAJA));

		stmt.setString(1, this.user);
		ResultSet resultS = stmt.executeQuery();

		if (resultS.next()) {
			result = resultS.getInt(1);
		}

	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}
