package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que consume sp: cja.spRCConsultaPrestacionesNumeroOPS 
 * @author GENIAR
 *
 */
public class SpRCConsultaPrestacionesNumeroOPSDelegate implements SQLDelegate {

	private int numeroUnicoOps;
	private List<PrestacionDTO> listaPrestaciones;
	
	/**
	 * Constructor de la clase
	 * @param numeroUnicoOps
	 * @param consecutivoSolicitud
	 */
	public SpRCConsultaPrestacionesNumeroOPSDelegate(int numeroUnicoOps) {
		super();
		this.numeroUnicoOps = numeroUnicoOps;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_NUMERO_UNICO_OPS));
		listaPrestaciones = new ArrayList<PrestacionDTO>();
		stmt.setInt(1, numeroUnicoOps);
		
		
		ResultSet result = stmt.executeQuery();
		
		while(result.next()){
			PrestacionDTO prestacionDTO = new PrestacionDTO();
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString("dscrpcn_tpo_cdfccn"));
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString("dscrpcn_srvco_slctdo"));
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString("cdgo_srvco_slctdo"));
			prestacionDTO.setEstadoPrestacion(result.getString("dscrpcn_estdo_srvco_slctdo"));
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			listaPrestaciones.add(prestacionDTO);
		}
	}

	public List<PrestacionDTO> getListaPrestaciones() {
		return listaPrestaciones;
	}

}
