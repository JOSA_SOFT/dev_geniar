package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * DELEGADO OBTENER LA INFORMACIÓN DE LAS OPS Y EL VALOR DE CADA UNA SEGÚN EL
 * NÚMERO DE SOLICITUD
 * 
 * @author JOSÉ OLMEDO SOTO AGUIRRE, 28/10/2016
 *
 */
public class SpRCObtenerInformacionOPSValorDelegate implements SQLDelegate {

	private int numSolicitud;
	private OPSCajaVO ops;
	private List<InformacionOpsValorVO> listaOps;

	/**
	 * CONSTRUCTOR QUE INICIALIZA EL NÚMERO DE SOLICITUD
	 * 
	 * @param numSolicitud
	 *            : número de la solicitud a consultar
	 */
	public SpRCObtenerInformacionOPSValorDelegate(int numSolicitud) {
		this.numSolicitud = numSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement stmt = conn.prepareCall(ConsultaSp
				.getString(ConstantesData.CONSULTA_OPS_VALOR_NUM_SOL));

		ops = new OPSCajaVO();
		
		stmt.setInt(1, this.numSolicitud);
		stmt.registerOutParameter(2, Types.CHAR);
		stmt.registerOutParameter(3, Types.CHAR);
		
		ResultSet resultS = stmt.executeQuery();

		listaOps = new ArrayList<InformacionOpsValorVO>();
		int i = 0;
		List<ConceptoPagoOpsVO> conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
		while (resultS.next()) {
			InformacionOpsValorVO retornoResult = new InformacionOpsValorVO();
			ConceptoPagoOpsVO conceptoPago = new ConceptoPagoOpsVO(); 
			retornoResult.setCodeOPS(resultS.getInt("nmro_unco_ops"));
			retornoResult.setValueOPS(resultS.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			conceptoPago.setNombreConceptoPago(resultS.getString("dscrpcn_cncpto_pgo"));
			conceptoPago.setValorConceptoPago(resultS.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			conceptoPago.setConsecutivoCodigoConcepto(resultS.getInt("cnsctvo_cdgo_cncpto_pgo"));
			
			if (i==0){
				this.adicionarElementos(conceptoPago, conceptosPago, retornoResult);
				i++;
			}else{
				if (listaOps.get(i-1) != null && listaOps.get(i-1).getCodeOPS() == retornoResult.getCodeOPS()){
					this.modificarOPS(listaOps.get(i-1), conceptoPago);
				}else{
					conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
					this.adicionarElementos(conceptoPago, conceptosPago, retornoResult);
					i++;
				}
			}
		}
		ops.setMuestraMensajeIPS(stmt.getString(2));
		ops.setMuestraReciboCaja(stmt.getString(3));
		ops.setOps(listaOps);
	}
	
	private void modificarOPS(InformacionOpsValorVO valorOPS, ConceptoPagoOpsVO conceptoPago){
		valorOPS.getListaConceptos().add(conceptoPago);
		valorOPS.setValueOPS(valorOPS.getValueOPS()+conceptoPago.getValorConceptoPago());
		if (conceptoPago.getConsecutivoCodigoConcepto() == ConstantesData.CONSECUTIVO_CONCEPTO_CUOTA_MODERADORA){
			valorOPS.setTotalCuotaModeradora(valorOPS.getTotalCuotaModeradora() + conceptoPago.getValorConceptoPago());
		}else{
			valorOPS.setTotalCopago(valorOPS.getTotalCopago() + conceptoPago.getValorConceptoPago());
		}
	}
	
	private void adicionarElementos(ConceptoPagoOpsVO conceptoPago, List<ConceptoPagoOpsVO> conceptosPago, InformacionOpsValorVO retornoResult){
		if (conceptoPago.getConsecutivoCodigoConcepto() == ConstantesData.CONSECUTIVO_CONCEPTO_CUOTA_MODERADORA){
			retornoResult.setTotalCuotaModeradora(conceptoPago.getValorConceptoPago());
		}else{
			retornoResult.setTotalCopago(conceptoPago.getValorConceptoPago());
		}
		conceptosPago.add(conceptoPago);
		retornoResult.setListaConceptos(conceptosPago);
		listaOps.add(retornoResult);
	}

	public OPSCajaVO getOps() {
		return ops;
	}
}
