package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Se encarga de guardar un documento en la base de datos
 * 
 * @author GENIAR
 *
 */
public class SpRCGuardarDocumentoCajaDelegate implements SQLDelegate {

	private DocumentoCajaVO documento;
	private DatosContactoVO contacto;
	private String listadoOPS;
	private String insertaDetalle;

	/**
	 * 
	 * @param documento
	 * @param contacto
	 * @param listadoOPS
	 */
	public SpRCGuardarDocumentoCajaDelegate(DocumentoCajaVO documento,
			DatosContactoVO contacto, String listadoOPS, String insertaDetalle) {
		super();
		this.documento = documento;
		this.contacto = contacto;
		this.listadoOPS = listadoOPS;
		this.insertaDetalle = insertaDetalle;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		if (contacto != null) {
			SpRCActualizarContactoDelegate spContacto = new SpRCActualizarContactoDelegate(
					contacto, documento.getUsuarioCreacion());
			spContacto.ejecutarData(conn);
			documento.setDatoContacto(spContacto.getConsecutivoContacto());
		}

		CallableStatement stmt = conn.prepareCall(ConsultaSp
				.getString(ConstantesData.GUARDAR_DOCUMENTO_CAJA));

		stmt.setInt(1, documento.getNuiAfiliado());
		stmt.setDouble(3, documento.getValorAfiliad());
		stmt.setInt(4, documento.getTipoDocumento());
		stmt.setInt(5, documento.getEstadoDocumento());
		stmt.setString(8, documento.getObservacion());
		stmt.setString(9, documento.getUsuarioCreacion());
		stmt.setString(13, insertaDetalle);

		if (documento.getConceptoGeneracion() == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, documento.getConceptoGeneracion());
		}

		if (documento.getDatoContacto() == null) {
			stmt.setNull(6, Types.INTEGER);
		} else {
			stmt.setInt(6, documento.getDatoContacto());
		}

		if (documento.getDetalleMovimientoCaja() == null) {
			stmt.setNull(7, Types.INTEGER);
		} else {
			stmt.setInt(7, documento.getDetalleMovimientoCaja());
		}
		
		if (listadoOPS == null) {
			stmt.setNull(10, Types.CHAR);
		} else {
			stmt.setString(10, listadoOPS);
		}

		stmt.registerOutParameter(11, Types.INTEGER);
		stmt.registerOutParameter(12, Types.INTEGER);
		
		

		stmt.execute();
		documento.setNumeroDocumento(stmt.getInt(12));
	}

	public DocumentoCajaVO getDocumento() {
		return documento;
	}

}
