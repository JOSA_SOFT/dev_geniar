package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * 
 * @author GENIAR
 *
 */
public class SpRCActualizarContactoDelegate implements SQLDelegate {

	private DatosContactoVO contacto;
	private String usuario;
	private int consecutivoContacto;

	/**
	 * 
	 * @param contacto
	 * @param usuario
	 */
	public SpRCActualizarContactoDelegate(DatosContactoVO contacto, String usuario) {
		super();
		this.contacto = contacto;
		this.usuario = usuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement stmt = conn.prepareCall(ConsultaSp
				.getString(ConstantesData.ACTUALIZAR_CONTACTO_CAJA));

		stmt.setInt(1, contacto.getConsecutivoMetodoDevolucion());

		if (contacto.getNumeroCuenta() == null) {
			stmt.setNull(2, Types.CHAR);
		} else {
			stmt.setString(2, contacto.getNumeroCuenta());
		}

		if (contacto.getConsecutivoBanco() == -1) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, contacto.getConsecutivoBanco());
		}

		if (contacto.getTipoCuenta() == null) {
			stmt.setNull(4, Types.CHAR);
		} else {
			stmt.setString(4, contacto.getTipoCuenta());
		}

		if (contacto.getTitular() == null) {
			stmt.setNull(5, Types.CHAR);
		} else {
			stmt.setString(5, contacto.getTitular());
		}

		if (contacto.getTelefono() == null) {
			stmt.setNull(6, Types.CHAR);
		} else {
			stmt.setString(6, contacto.getTelefono());
		}

		if (contacto.getCorreoElectronico() != null) {
			stmt.setString(7, contacto.getCorreoElectronico());
		} else {
			stmt.setNull(7, Types.VARCHAR);
		}

		stmt.setString(8, usuario);
		
		if (contacto.getConsecutivoDatoContacto() == null){
			stmt.setNull(9, Types.INTEGER);
		}else{
			stmt.setInt(9, contacto.getConsecutivoDatoContacto());
		}
		
		stmt.registerOutParameter(10, Types.INTEGER);
		
		stmt.execute();
		
		consecutivoContacto = stmt.getInt(10);
	}

	public int getConsecutivoContacto() {
		return consecutivoContacto;
	}

}
