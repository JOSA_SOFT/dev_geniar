/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesDescuadreValorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que se encarga de ejecutar el SP spRCConsultaCausalesDescuadrePorCaja
 * @author GENIAR
 *
 */
public class SpRCConsultaCausalesDescuadrePorCajaDelegate implements SQLDelegate {

	private int consecutivoMovimientoCaja;
	private List<CausalesDescuadreValorVO> resultado;

	/**
	 * 
	 * @param consecutivoMovimientoCaja
	 */
	public SpRCConsultaCausalesDescuadrePorCajaDelegate(int consecutivoMovimientoCaja) {
		this.consecutivoMovimientoCaja = consecutivoMovimientoCaja;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.OBTENER_CAUSALES_DESCUADRE_CAJA));
		stmt.setInt(1, consecutivoMovimientoCaja);

		ResultSet rs = stmt.executeQuery();
		resultado = new ArrayList<CausalesDescuadreValorVO>();
		while (rs.next()) {
			CausalesDescuadreValorVO causal = new CausalesDescuadreValorVO();
			causal.setCausalDescuadre(rs.getString("dscrpcn_csl_dscdre"));
			causal.setUsuario(rs.getString("usro_crcn"));
			causal.setValorDescuadre(rs.getDouble("vlr_dscdre"));
			resultado.add(causal);
		}
	}

	public List<CausalesDescuadreValorVO> getResultado() {
		return resultado;
	}

}
