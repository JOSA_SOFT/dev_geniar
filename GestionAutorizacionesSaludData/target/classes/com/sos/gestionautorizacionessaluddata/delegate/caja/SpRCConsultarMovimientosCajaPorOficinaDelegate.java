package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta los movimientos por una oficina y el consicutivo oficina cierre este
 * null
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarMovimientosCajaPorOficinaDelegate implements SQLDelegate {

	private Integer consecutivoOficina;
	private List<MovimientoCajaVO> listMovimientosCaja;

	/**
	 * Recibe el consecutivo de la oficina para buscar los movimientos caja por
	 * ese consecutivo y por consecutivo cierre oficina null
	 * 
	 * @param consecutivoOficina
	 */
	public SpRCConsultarMovimientosCajaPorOficinaDelegate(Integer consecutivoOficina) {
		super();
		listMovimientosCaja = new ArrayList<MovimientoCajaVO>();
		this.consecutivoOficina = consecutivoOficina;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultarMovimientosCajaPorOficina
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_MOVIMIENTOS_POR_OFICINA));
		stmt.setInt(1, this.consecutivoOficina);

		ResultSet result = stmt.executeQuery();

		int cnsctvoMvmntoCja = result.findColumn("cnsctvo_mvmnto_cja");
		int bse = result.findColumn("bse");
		int fchaAprtra = result.findColumn("fcha_aprtra");
		int fchaCrre = result.findColumn("fcha_crre");
		int cnsctvoCdgoOfcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int cnsctvoCdgoEstdo = result.findColumn("cnsctvo_cdgo_estdo");
		int dscrpcnEstdo = result.findColumn("dscrpcn_estdo");
		int drccnIp = result.findColumn("drccn_ip");
		int fltnteCrre = result.findColumn("fltnte_crre");
		int sbrnteCrre = result.findColumn("sbrnte_crre");
		int ttlEfctvo = result.findColumn("ttl_efctvo");
		int ttlCja = result.findColumn("ttl_cja");
		int ttlNtaCrdto = result.findColumn("ttl_nta_crdto");
		int fchaCrcn = result.findColumn("fcha_crcn");
		int fchaUltmaMdfccn = result.findColumn("fcha_ultma_mdfccn");
		int usroCrcn = result.findColumn("usro_crcn");

		while (result.next()) {
			MovimientoCajaVO movimientoCajaVO = new MovimientoCajaVO();
			movimientoCajaVO.setCnsctvoMvmntoCja(result.getInt(cnsctvoMvmntoCja));
			movimientoCajaVO.setBse(result.getDouble(bse));
			movimientoCajaVO.setFchaAprtra(result.getTimestamp(fchaAprtra));
			movimientoCajaVO.setFchaCrre(result.getTimestamp(fchaCrre));
			movimientoCajaVO.setCnsctvoCdgoOfcna(result.getInt(cnsctvoCdgoOfcna));
			movimientoCajaVO.setCnsctvoCdgoEstdo(result.getInt(cnsctvoCdgoEstdo));
			movimientoCajaVO.setDscrpcnEstdo(result.getString(dscrpcnEstdo));
			movimientoCajaVO.setDrccnIp(result.getString(drccnIp));
			movimientoCajaVO.setFltnteCrre(result.getDouble(fltnteCrre));
			movimientoCajaVO.setSbrnteCrre(result.getDouble(sbrnteCrre));
			movimientoCajaVO.setTtlEfctvo(result.getDouble(ttlEfctvo));
			movimientoCajaVO.setTtlCja(result.getDouble(ttlCja));
			movimientoCajaVO.setTtlNtaCrdto(result.getDouble(ttlNtaCrdto));
			movimientoCajaVO.setFchaCrcn(new Date(result.getDate(fchaCrcn).getTime()));
			movimientoCajaVO.setFchaUltmaMdfccn(new Date(result.getDate(fchaUltmaMdfccn).getTime()));
			movimientoCajaVO.setUsroCrcn(result.getString(usroCrcn));
			listMovimientosCaja.add(movimientoCajaVO);
		}
	}

	public List<MovimientoCajaVO> getListMovimientosCaja() {
		return listMovimientosCaja;
	}

}
