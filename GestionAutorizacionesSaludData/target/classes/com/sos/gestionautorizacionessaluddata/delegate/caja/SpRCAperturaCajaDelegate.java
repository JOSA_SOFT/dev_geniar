package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Crea un registro en la tabla bdCNA cja.tbRCMovimientosCaja, para indicar
 * apertura de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCAperturaCajaDelegate implements SQLDelegate {

	private Double bse;
	private String usroCrcn;
	private String drccnIp;
	private Integer cnsctvoCdgoOfcna;
	private Integer cnsctvoCdgoEstdo;
	private Date fecha;

	/**
	 * Recibe los parametros para insertar el registro en
	 * cja.tbRCMovimientosCaja
	 * 
	 * @param bse
	 * @param usroCrcn
	 * @param cnsctvoCdgoOfcna
	 * @param cnsctvoCdgoEstdo
	 * @param drccnIp
	 * @param fecha
	 */
	public SpRCAperturaCajaDelegate(Double bse, String usroCrcn,
			Integer cnsctvoCdgoOfcna, Integer cnsctvoCdgoEstdo, String drccnIp,
			Date fecha) {
		this.bse = bse;
		this.usroCrcn = usroCrcn;
		this.cnsctvoCdgoOfcna = cnsctvoCdgoOfcna;
		this.cnsctvoCdgoEstdo = cnsctvoCdgoEstdo;
		this.drccnIp = drccnIp;
		this.fecha = fecha;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCAperturaCaja
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.GUARDA_APERTURA_CAJA));

		stmt.setDouble(1, this.bse);
		stmt.setString(2, this.usroCrcn);
		stmt.setInt(3, this.cnsctvoCdgoOfcna);
		stmt.setInt(4, this.cnsctvoCdgoEstdo);
		stmt.setString(5, this.drccnIp);
		
		if (this.fecha == null){
			stmt.setNull(6, Types.DATE);
		}else{
			stmt.setDate(6, new java.sql.Date(this.fecha.getTime()));
		}
		

		stmt.executeUpdate();
	}
}