package com.sos.gestionautorizacionessaluddata.delegate.generarops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate
 * Clase delegate que permite consultar las ops generadas para una solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 25/04/2016
 */
public class SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar las ops generadas para una solicitud*/
	private List<AutorizacionServicioVO> lAutorizacionServicioVO;
	private Integer numeroSolicitud;
	private Integer numeroPrestacion;
	
	public SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate(Integer numeroSolicitud, Integer numeroPrestacion){
		this.numeroSolicitud = numeroSolicitud; 	
		this.numeroPrestacion = numeroPrestacion; 	
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_OPS_SOLICITUD));	
		
		stmt.setInt(1, this.numeroSolicitud);	
		
		if (numeroPrestacion != null) {
			stmt.setInt(2, this.numeroPrestacion);
		}else {
			stmt.setNull(2, java.sql.Types.INTEGER);
		}
		
		ResultSet result = stmt.executeQuery();
				
		int indice_nmro_unco_ops              = result.findColumn("nmro_unco_ops");
		int indice_cdgo_intrno_prstdr_atrzdo  = result.findColumn("cdgo_intrno_prstdr_atrzdo");
		int indice_cnsctvo_slctd_atrzcn_srvco = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_vlr_ttal                   = result.findColumn("vlr_ttal");
		
		lAutorizacionServicioVO = new ArrayList<AutorizacionServicioVO>();
		
		while(result.next()){
			AutorizacionServicioVO autorizacionServicioVO = new AutorizacionServicioVO();
			autorizacionServicioVO.setNumeroUnicoAutorizacion(result.getInt(indice_nmro_unco_ops));
			autorizacionServicioVO.setCodigoInternoPrestadorAutorizado(result.getString(indice_cdgo_intrno_prstdr_atrzdo));
			autorizacionServicioVO.setConsecutivoSolicitud(result.getInt(indice_cnsctvo_slctd_atrzcn_srvco));
			autorizacionServicioVO.setValorTotal(result.getInt(indice_vlr_ttal));
			
			lAutorizacionServicioVO.add(autorizacionServicioVO);
		}		
	}
	
	public List<AutorizacionServicioVO> getlAutorizacionServicioVO() {
		return lAutorizacionServicioVO;
	}
}
