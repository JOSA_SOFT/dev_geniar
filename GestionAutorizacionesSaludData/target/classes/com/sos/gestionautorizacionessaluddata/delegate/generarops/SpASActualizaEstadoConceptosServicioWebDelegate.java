package com.sos.gestionautorizacionessaluddata.delegate.generarops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASActualizaEstadoConceptosServicioWebDelegate
 * Clase delegate que permite actualizar el estado de las ops
 * @author ing. Victor Hugo Gil Ramos
 * @version 26/04/2016
 */
public class SpASActualizaEstadoConceptosServicioWebDelegate implements SQLDelegate {

	private Integer numeroUnicoOPS;
	
	public SpASActualizaEstadoConceptosServicioWebDelegate(Integer numeroUnicoOPS){
		this.numeroUnicoOPS = numeroUnicoOPS; 		
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.ACTUALIZAR_ESTADO_CONCEPTO));	
		stmt.setInt(1, this.numeroUnicoOPS);	
		
		stmt.executeUpdate();		
	}
}
