package com.sos.gestionautorizacionessaluddata.delegate.generarops;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultaVigenciaEstadoOPSWebDelegate
 * Clase delegate que permite consultar las fecha de vigencia y estado de la OPS
 * @author ing. Victor Hugo Gil Ramos
 * @version 07/06/2016
 */
public class SpASConsultaVigenciaEstadoOPSWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las ops generadas para una solicitud*/
	private List<AutorizacionServicioVO> lAutorizacionServicioVO;
	private Integer consecutivoSolicitud;
	private Integer consecutivoPrestacion;
	
	public SpASConsultaVigenciaEstadoOPSWebDelegate(Integer consecutivoSolicitud, Integer consecutivoPrestacion){
		this.consecutivoSolicitud = consecutivoSolicitud; 
		this.consecutivoPrestacion = consecutivoPrestacion; 
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_FECHA_ESTADO_OPS));	
		stmt.setInt(1, this.consecutivoSolicitud);
		stmt.setInt(2, this.consecutivoPrestacion);	
		
		ResultSet result = stmt.executeQuery();
		
		int indice_nmro_unco_ops                          = result.findColumn("nmro_unco_ops");
		int indice_fcha_utlzcn_dsde                       = result.findColumn("fcha_utlzcn_dsde");
		int indice_fcha_utlzcn_hsta                       = result.findColumn("fcha_utlzcn_hsta");
		int indice_cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = result.findColumn("cnsctvo_cdgo_estdo_cncpto_srvco_slctdo");
		int indice_dscrpcn_estdo_srvco_slctdo             = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int fcha_utlzcn_hsta_hlgra						  = result.findColumn("fcha_utlzcn_hsta_hlgra"); 
		
		lAutorizacionServicioVO = new ArrayList<AutorizacionServicioVO>();
		
		while(result.next()){
			AutorizacionServicioVO autorizacionServicioVO = new AutorizacionServicioVO();
			autorizacionServicioVO.setNumeroUnicoAutorizacion(result.getInt(indice_nmro_unco_ops));
			autorizacionServicioVO.setFechaInicio(resultStamentDate(result.getDate(indice_fcha_utlzcn_dsde)));
			autorizacionServicioVO.setFechaVencimiento(resultStamentDate(result.getDate(indice_fcha_utlzcn_hsta)));
			autorizacionServicioVO.setConsecutivoCodigoEstadoOPS(result.getInt(indice_cnsctvo_cdgo_estdo_cncpto_srvco_slctdo));		
			autorizacionServicioVO.setDescripcionCodigoEstadoOPS(result.getString(indice_dscrpcn_estdo_srvco_slctdo));
			autorizacionServicioVO.setFechaVencimientoHolgura(result.getDate(fcha_utlzcn_hsta_hlgra));
			lAutorizacionServicioVO.add(autorizacionServicioVO);
		}		
	}

	public List<AutorizacionServicioVO> getlAutorizacionServicioVO() {
		return lAutorizacionServicioVO;
	}
	
	//realiza la validacion de las fechas
	public java.util.Date resultStamentDate(Date fecha) throws SQLException{
		
		if(fecha == null){
			return null;
		}else{
			return new java.util.Date(fecha.getTime());
		}		
	}
}
