package com.sos.gestionautorizacionessaluddata.delegate.generarops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASRegistrarValidacionEspecialWebDelegate
 * Clase delegate que permite guardar la validacion especial realizada
 * @author ing. Victor Hugo Gil Ramos
 * @version 27/04/2016
 */
public class SpASRegistrarValidacionEspecialWebDelegate implements SQLDelegate{
	
	private ValidacionEspecialVO validacionEspecialVO;
	private String usuarioCreacion;
	private String accionValidacion;
	
	public SpASRegistrarValidacionEspecialWebDelegate(ValidacionEspecialVO validacionEspecialVO, String usuarioCreacion, String accionValidacion){
		this.validacionEspecialVO = validacionEspecialVO;
		this.usuarioCreacion = usuarioCreacion;
		this.accionValidacion = accionValidacion;
	}
	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.GUARDAR_VALIDACION_ESPECIAL));	
		
		stmt.setInt(1, validacionEspecialVO.getConsecutivoSolicitud());//consecutivo solicitud
		stmt.setInt(2, validacionEspecialVO.getAfiliadoVO().getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion()); //tipo de identificacion afiliado
		stmt.setString(3, validacionEspecialVO.getAfiliadoVO().getNumeroIdentificacion()); //numero de identificacion afiliado
		stmt.setFloat(4, validacionEspecialVO.getNumeroValidacion().floatValue());//numero de validacion especial
		stmt.setInt(5, validacionEspecialVO.getOficinasVO().getConsecutivoCodigoOficina());//consecutivo oficina validacion
		stmt.setString(6, validacionEspecialVO.getUsroValidacion()); //usuario validacion especial
		stmt.setString(7, this.accionValidacion); //accion de la validacion consultar, ingresar
		stmt.setInt(8, validacionEspecialVO.getNumeroUnicoAutorizacion());//numero unico de OPS
		stmt.setDate(9, new java.sql.Date(validacionEspecialVO.getFechaValidacion().getTime()));//fecha validacion especial
		stmt.setString(10, this.usuarioCreacion);//usuario creacion del registro
		stmt.setInt(11, validacionEspecialVO.getAfiliadoVO().getNumeroUnicoIdentificacion()); //numero unico del afiliado
		
		stmt.executeUpdate();
	}
}
