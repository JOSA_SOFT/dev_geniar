package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosAdicionalesPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate
 * Clase delegate que permite consultar la informacion adicional de las prestaciones relacionadas a una solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/07/2017
 */

public class SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate implements SQLDelegate {
	
	List<DatosAdicionalesPrestacionDTO> lDatosAdicionesPrestacionDTO;
	private Integer consecutivoSolicitud;
	
	public SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {	
		
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DATOS_ADICIONALES_PRESTACION));	
		
		stmt.setInt(1, this.consecutivoSolicitud);	
		
		ResultSet result = stmt.executeQuery();
		
		int index_dscrpcn_tpo_cdfccn         = result.findColumn("dscrpcn_tpo_cdfccn");
		int index_cdgo_cdfccn                = result.findColumn("cdgo_cdfccn");
		int index_dscrpcn_srvco_slctdo       = result.findColumn("dscrpcn_srvco_slctdo");
		int index_dscrpcn_estdo_srvco_slctdo = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int index_usro_atrza                 = result.findColumn("usro_atrza");
		int index_usro_audtr                 = result.findColumn("usro_audtr");		
		int index_rqre_otra_gstn_adtr        = result.findColumn("rqre_otra_gstn_adtr");
		int index_cdgo_intrno                = result.findColumn("cdgo_intrno");
		int index_nmbre_scrsl                = result.findColumn("nmbre_scrsl");
		int index_dscrpcn_nvl                = result.findColumn("dscrpcn_nvl");		
		int index_jstfccn_gstn_adtr          = result.findColumn("jstfccn_gstn_adtr");		
		
		lDatosAdicionesPrestacionDTO = new ArrayList<DatosAdicionalesPrestacionDTO>();
		DatosAdicionalesPrestacionDTO datosAdicionalesPrestacionDTO;
		TipoCodificacionVO tipoCodificacionVO;
		
		while(result.next()){
			datosAdicionalesPrestacionDTO = new DatosAdicionalesPrestacionDTO();
			tipoCodificacionVO = new TipoCodificacionVO();
			
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(index_dscrpcn_tpo_cdfccn));			
			datosAdicionalesPrestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			datosAdicionalesPrestacionDTO.setCodigoCodificacionPrestacion(result.getString(index_cdgo_cdfccn));
			datosAdicionalesPrestacionDTO.setDescripcionCodificacionPrestacion(result.getString(index_dscrpcn_srvco_slctdo));
			datosAdicionalesPrestacionDTO.setDescripcionEstadoPrestacion(result.getString(index_dscrpcn_estdo_srvco_slctdo));
			datosAdicionalesPrestacionDTO.setNombreUsuarioAutoriza(result.getString(index_usro_atrza));
			datosAdicionalesPrestacionDTO.setNombreUsuarioAudita(result.getString(index_usro_audtr));
			datosAdicionalesPrestacionDTO.setRequiereOtraGestionAuditor(result.getString(index_rqre_otra_gstn_adtr));
			datosAdicionalesPrestacionDTO.setCodigoInternoDireccionamiento(result.getString(index_cdgo_intrno));
			datosAdicionalesPrestacionDTO.setPrestadorDireccionamiento(result.getString(index_nmbre_scrsl));
			datosAdicionalesPrestacionDTO.setNivelAtencion(result.getString(index_dscrpcn_nvl));
			datosAdicionalesPrestacionDTO.setJustificacionAuditor(result.getString(index_jstfccn_gstn_adtr));			
			lDatosAdicionesPrestacionDTO.add(datosAdicionalesPrestacionDTO);			
		}				
	}
	
	public List<DatosAdicionalesPrestacionDTO> getlDatosAdicionesPrestacion() {
		return lDatosAdicionesPrestacionDTO;
	}
}
