package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultaPrestacionesSolicitudWebDelegate
 * Clase delegate que permite consultar las prestaciones relacionadas a una solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/03/2016
 */
public class SpASConsultaPrestacionesSolicitudWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las prestaciones relacionadas a una solicitud  */
	private List<PrestacionDTO> lPrestacionesDTO;	
	private Integer numeroSolicitud;
	
	public SpASConsultaPrestacionesSolicitudWebDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_X_SOLICITUD));	
		
		stmt.setInt(1, this.numeroSolicitud);		
		ResultSet result = stmt.executeQuery();
		
		int indice_dscrpcn_tpo_cdfccn       		= result.findColumn("dscrpcn_tpo_cdfccn");
		int indice_cdgo_cdfccn              		= result.findColumn("cdgo_cdfccn");
		int indice_dscrpcn_srvco_slctdo     		= result.findColumn("dscrpcn_srvco_slctdo");
		int indice_cntdd_slctda             		= result.findColumn("cntdd_slctda");
		int indice_indcdr_no_ps             		= result.findColumn("indcdr_no_ps");
		int indice_dscrpcn_ltrldd           		= result.findColumn("dscrpcn_ltrldd");
		int indice_dscrpcn_va_accso         		= result.findColumn("dscrpcn_va_accso");		
		int indice_dscrpcn_dss_mdcmnto      		= result.findColumn("dscrpcn_dss_mdcmnto");
		int indice_dscrpcn_cncntrcn_mdcmnto 		= result.findColumn("dscrpcn_cncntrcn_mdcmnto");
		int indice_dscrpcn_prsntcn_mdcmnto  		= result.findColumn("dscrpcn_prsntcn_mdcmnto");
		int indice_cnsctvo_cdfccn           		= result.findColumn("cnsctvo_cdfccn");
		int indice_cnsctvo_srvco_slctdo 			= result.findColumn("cnsctvo_srvco_slctdo");
		int indice_hblta_dts_atrzcn 				= result.findColumn("hblta_dts_atrzcn");
		int indice_hblta_dts_atrzcn_gstn 			= result.findColumn("hblta_dts_atrzcn_gstn");
		int indice_fcha_estmda_entrga	 			= result.findColumn("fcha_estmda_entrga");
		int indice_cnsctvo_cdgo_rcbro 				= result.findColumn("cnsctvo_cdgo_rcbro");
		int indice_cdgo_rcbro						= result.findColumn("cdgo_rcbro");
		int indice_dscrpcn_rcbro					= result.findColumn("dscrpcn_rcbro");
		int indice_cnsctvo_cdgo_estado				= result.findColumn("cnsctvo_cdgo_estado_servco_slctdo");
		int indice_cnsctvo_cdgo_csa_no_cbro 		= result.findColumn("cnsctvo_cdgo_csa_no_cbro_cta_rcprcn");
		int indice_dscrpcn_csa_no_cbro_cta_rcprcn 	= result.findColumn("dscrpcn_csa_no_cbro_cta_rcprcn");
		
		lPrestacionesDTO = new ArrayList<PrestacionDTO>();
		
		while(result.next()){
			PrestacionDTO prestacionDTO = new PrestacionDTO();
			
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
			PresentacionVO presentacionVO = new PresentacionVO();
			ConcentracionVO concentracionVO = new ConcentracionVO();
			LateralidadesVO lateralidadesVO = new LateralidadesVO();
			ViaAccesoVO viaAccesoVO = new ViaAccesoVO();
			DosisVO dosisVO = new DosisVO();
			
			
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(indice_dscrpcn_tpo_cdfccn));
			presentacionVO.setDescripcionPresentacion(result.getString(indice_dscrpcn_prsntcn_mdcmnto));
			concentracionVO.setDescripcionConcentracion(result.getString(indice_dscrpcn_cncntrcn_mdcmnto));
			dosisVO.setDescripcionDosis(result.getString(indice_dscrpcn_dss_mdcmnto));
	
			lateralidadesVO.setDescripcionLateralidad(result.getString(indice_dscrpcn_ltrldd));
			viaAccesoVO.setDescripcionViaAcceso(result.getString(indice_dscrpcn_va_accso));
			
			
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			prestacionDTO.setPresentacionVO(presentacionVO);
			prestacionDTO.setConcentracionVO(concentracionVO);
			prestacionDTO.setDosisVO(dosisVO);
			
			prestacionDTO.setLateralidadesVO(lateralidadesVO);
			prestacionDTO.setViaAccesoVO(viaAccesoVO);
			String noPos = result.getString(indice_indcdr_no_ps)!=null?result.getString(indice_indcdr_no_ps).trim():null;
			prestacionDTO.setIndicadorNoPOS(ConstantesData.NO.equals(noPos)?ConstantesData.SI:ConstantesData.NO);
			prestacionDTO.setCantidad(result.getInt(indice_cntdd_slctda));
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString(indice_cdgo_cdfccn));
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(indice_dscrpcn_srvco_slctdo));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(indice_cnsctvo_cdfccn));
			
			prestacionDTO.setConsecutivoServicioSolicitado(result.getInt(indice_cnsctvo_srvco_slctdo));
			String hblta_dts_atrzcn = result.getString(indice_hblta_dts_atrzcn)!=null?result.getString(indice_hblta_dts_atrzcn):null;
			prestacionDTO.setHabilitaDatosAutorizacion(ConstantesData.EQUIVALENTE_VERDADERO.equals(hblta_dts_atrzcn)?true:false);
			
			String hblta_dts_atrzcn_gstn = result.getString(indice_hblta_dts_atrzcn_gstn)!=null?result.getString(indice_hblta_dts_atrzcn_gstn):null;
			prestacionDTO.setHabilitaGestionDatosAutorizacion(ConstantesData.EQUIVALENTE_VERDADERO.equals(hblta_dts_atrzcn_gstn)?true:false);
			
			prestacionDTO.setFechaEstimadaEntrega(result.getDate(indice_fcha_estmda_entrga));
			
			prestacionDTO.setConsecutivoRecobro(result.getInt(indice_cnsctvo_cdgo_rcbro));
			prestacionDTO.setCodigoRecobro(result.getString(indice_cdgo_rcbro));
			prestacionDTO.setDescripcionRecobro(result.getString(indice_dscrpcn_rcbro));
			
			prestacionDTO.setConsecutivoEstado(result.getInt(indice_cnsctvo_cdgo_estado));
			
			prestacionDTO.setConsecutivoCausaNoCobroCuota(result.getInt(indice_cnsctvo_cdgo_csa_no_cbro));
			prestacionDTO.setNoCobroCuotaRecuperacion(prestacionDTO.getConsecutivoCausaNoCobroCuota().intValue() != 0);
			prestacionDTO.setDescripcionCausaNoCobro(result.getString(indice_dscrpcn_csa_no_cbro_cta_rcprcn));
			
			lPrestacionesDTO.add(prestacionDTO);			
		}		
	}
	
	public List<PrestacionDTO> getlPrestacionesDTO() {
		return lPrestacionesDTO;
	}
}
