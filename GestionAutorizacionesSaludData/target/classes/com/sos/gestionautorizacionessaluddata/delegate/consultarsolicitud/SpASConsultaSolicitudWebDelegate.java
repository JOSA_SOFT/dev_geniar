package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;




import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConsultaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultaSolicitudWebDelegate
 * Clase delegate que permite consultar la informacion de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 19/03/2016
 */
public class SpASConsultaSolicitudWebDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar los datos basicos de la solicitud */
	private List<DatosSolicitudVO> lDatosSolicitud;
	private ConsultaSolicitudVO consultaSolicitudVO;

	PreparedStatement stmt = null;

	public SpASConsultaSolicitudWebDelegate(ConsultaSolicitudVO consultaSolicitudVO){
		this.consultaSolicitudVO = consultaSolicitudVO;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {

		stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_SOLICITUDES));

		preparedStamentDate(this.consultaSolicitudVO.getFechaDesde(), 1);
		preparedStamentDate(this.consultaSolicitudVO.getFechaHasta(), 2);
		preparedStamentInt(this.consultaSolicitudVO.getConsecutivoPlan(), 3);
		preparedStamentString(this.consultaSolicitudVO.getNumeroRadicadoSolicitud(), 4);
		preparedStamentString(this.consultaSolicitudVO.getNumeroSolicitudProveedor(), 5);
		preparedStamentInt(this.consultaSolicitudVO.getNumeroAutorizacionOPS(), 6);
		preparedStamentInt(this.consultaSolicitudVO.getConsecutivoTipoIdentificacionPrestador(), 7);
		preparedStamentString(this.consultaSolicitudVO.getNumeroIdentificacionPrestador(), 8);
		preparedStamentString(this.consultaSolicitudVO.getCodigoInternoPrestador(), 9);
		preparedStamentInt(this.consultaSolicitudVO.getConsecutivoServicioSolicitado(), 10);	
		preparedStamentInt(this.consultaSolicitudVO.getNumeroUnicoIdentificacionAfiliado(), 11);
		
		ResultSet result = stmt.executeQuery();

		int index_nmro_idntfccn_afldo                    = result.findColumn("nmro_idntfccn_afldo");
		int index_nmbre_afldo                            = result.findColumn("nmbre_afldo");
		int index_nmro_slctd_atrzcn_ss                   = result.findColumn("nmro_slctd_atrzcn_ss");
		int index_fcha_slctd                             = result.findColumn("fcha_slctd");
		int index_cnsctvo_cdgo_srvco_slctdo              = result.findColumn("cnsctvo_cdgo_srvco_slctdo");	
		int index_dscrpcn_srvco_slctdo                   = result.findColumn("dscrpcn_srvco_slctdo");
		int index_dscrpcn_estdo_srvco_slctdo             = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int index_cnsctvo_slctd_atrzcn_srvco             = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int index_fcha_vncmnto_gstn                      = result.findColumn("fcha_vncmnto_gstn");	
		int index_cnsctvo_srvco_slctdo                   = result.findColumn("cnsctvo_srvco_slctdo");		
		int index_nmro_unco_ops                          = result.findColumn("nmro_unco_ops");
		int index_cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = result.findColumn("cnsctvo_cdgo_estdo_cncpto_srvco_slctdo");
		int index_dscrpcn_estdo_cncpto_srvco_slctdo      = result.findColumn("dscrpcn_estdo_cncpto_srvco_slctdo");
		int index_fcha_utlzcn_hsta                       = result.findColumn("fcha_utlzcn_hsta");
		int index_cntdd_dscrga_frmto                     = result.findColumn("cntdd_dscrga_frmto");		
		int index_nmro_prcso_instnca					 = result.findColumn("nmro_prcso_instnca");
		int index_cnsctvo_cdgo_estdo_slctd				 = result.findColumn("cnsctvo_cdgo_estdo_slctd");
		int index_cnsctvo_cdgo_estdo_srvco_slctdo		 = result.findColumn("cnsctvo_cdgo_estdo_srvco_slctdo");
		int index_cnsctvo_cdgo_tpo_idntfccn_afldo		 = result.findColumn("cnsctvo_cdgo_tpo_idntfccn_afldo");
		int index_cnsctvo_cdgo_pln						 = result.findColumn("cnsctvo_cdgo_pln");
		int index_fcha_estmda_entrga                     = result.findColumn("fcha_estmda_entrga");
		int index_cnsctvo_cdgo_cdd_rsdnca_afldo          = result.findColumn("cnsctvo_cdgo_cdd_rsdnca_afldo");
		int index_vlda_vlr_cro_cncpto                    = result.findColumn("vlda_vlr_cro_cncpto");		
		int index_nmro_unco_idntfccn_afldo               = result.findColumn("nmro_unco_idntfccn_afldo");
				
		AfiliadoVO afiliadoVO;
		CiudadVO ciudadResidencia;
		PrestacionDTO prestacionDTO;
		DatosSolicitudVO datosSolicitudVO;
		TiposIdentificacionVO tiposIdentificacionVO;
		PlanVO planVO;		
		
		lDatosSolicitud = new ArrayList<DatosSolicitudVO>();
		
		while(result.next()){

			afiliadoVO 			  = new AfiliadoVO();
			prestacionDTO 		  = new PrestacionDTO();
			datosSolicitudVO 	  = new DatosSolicitudVO();
			tiposIdentificacionVO = new TiposIdentificacionVO();
			planVO 				  = new PlanVO();	
			ciudadResidencia	  = new CiudadVO();

			afiliadoVO.setNombreCompleto(result.getString(index_nmbre_afldo));
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(index_cnsctvo_cdgo_tpo_idntfccn_afldo));
			String[] numAfiliado = result.getString(index_nmro_idntfccn_afldo).split(ConstantesData.PUNTO); 
			tiposIdentificacionVO.setCodigoTipoIdentificacion(numAfiliado[0].trim());
			afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
			afiliadoVO.setNumeroIdentificacion(numAfiliado[1].trim());
			planVO.setConsectivoPlan(result.getInt(index_cnsctvo_cdgo_pln));
			afiliadoVO.setPlan(planVO);
			
			ciudadResidencia.setConsecutivoCodigoCiudad(result.getInt(index_cnsctvo_cdgo_cdd_rsdnca_afldo));
			afiliadoVO.setCiudadResidencia(ciudadResidencia);
			afiliadoVO.setNumeroUnicoIdentificacion(result.getInt(index_nmro_unco_idntfccn_afldo));
			
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(index_dscrpcn_srvco_slctdo));
			prestacionDTO.setEstadoPrestacion(result.getString(index_dscrpcn_estdo_srvco_slctdo));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(index_cnsctvo_cdgo_srvco_slctdo));
			prestacionDTO.setConsecutivoServicioSolicitado(result.getInt(index_cnsctvo_srvco_slctdo));
			prestacionDTO.setConsecutivoEstado(result.getInt(index_cnsctvo_cdgo_estdo_srvco_slctdo));
			prestacionDTO.setFechaEstimadaEntrega(resultStamentDate(result.getDate(index_fcha_estmda_entrga)));	
			
			String vlda_vlr_cro_cncpto = result.getString(index_vlda_vlr_cro_cncpto)!=null?result.getString(index_vlda_vlr_cro_cncpto):null;
			prestacionDTO.setValidacionConceptoCero(ConstantesData.S.equals(vlda_vlr_cro_cncpto)?true:false);
			
			habilitarDescargaReporte(prestacionDTO);
			datosSolicitudVO.setIdentificacionNombreAfiliado(result.getString(index_nmro_idntfccn_afldo));
			datosSolicitudVO.setAfiliadoVO(afiliadoVO);
			datosSolicitudVO.setFechaSolicitud(new java.util.Date(result.getDate(index_fcha_slctd).getTime()));
			datosSolicitudVO.setNumeroRadicado(result.getString(index_nmro_slctd_atrzcn_ss));
			datosSolicitudVO.setNumeroSolicitud(result.getInt(index_cnsctvo_slctd_atrzcn_srvco));
			
			datosSolicitudVO.setFechaVencimientoGestion(resultStamentDate(result.getDate(index_fcha_vncmnto_gstn)));			
			
			datosSolicitudVO.setNumeroUnicoAutorizacion(result.getInt(index_nmro_unco_ops));
			datosSolicitudVO.setConsecutivoEstadoAutorizacion(result.getInt(index_cnsctvo_cdgo_estdo_cncpto_srvco_slctdo));
			datosSolicitudVO.setDescripcionEstadoAutorizacion(result.getString(index_dscrpcn_estdo_cncpto_srvco_slctdo));		
			
			datosSolicitudVO.setFechaVencimientoAutorizacion(resultStamentDate(result.getDate(index_fcha_utlzcn_hsta)));
			
			
			datosSolicitudVO.setCantidadDescarga(result.getInt(index_cntdd_dscrga_frmto));
			datosSolicitudVO.setPrestacionDTO(prestacionDTO);
			datosSolicitudVO.setIdInstanciaProcesoBPM(result.getInt(index_nmro_prcso_instnca));
			datosSolicitudVO.setConsecutivoEstadoSolicitud(result.getInt(index_cnsctvo_cdgo_estdo_slctd));			
						
			lDatosSolicitud.add(datosSolicitudVO);
		}		
	}

	private void habilitarDescargaReporte(PrestacionDTO dto){
		if (ConstantesData.COD_ESTADO_AUTORIZADO.equals(dto.getConsecutivoEstado())
				|| ConstantesData.COD_ESTADO_ENTREGADA.equals(dto.getConsecutivoEstado())
				|| ConstantesData.COD_ESTADO_DEVUELTO.equals(dto.getConsecutivoEstado()) 
				|| ConstantesData.COD_ESTADO_NO_AUTORIZADO.equals(dto.getConsecutivoEstado())) {
			dto.setHabilitaDescarga(true);
		}
	}

	public List<DatosSolicitudVO> getlDatosSolicitud() {
		return lDatosSolicitud;
	}

	//setea los parametros fecha
	public void preparedStamentDate(java.util.Date fecha, int posicion) throws SQLException{

		if(fecha == null){
			stmt.setNull(posicion, java.sql.Types.DATE);
		}else{
			stmt.setDate(posicion, new java.sql.Date(fecha.getTime()));
		}		
	}

	//setea los parametros enteros
	public void preparedStamentInt(Integer campo, int posicion)throws SQLException{

		if(campo == null){
			stmt.setNull(posicion, java.sql.Types.INTEGER);
		}else{
			stmt.setInt(posicion, campo);
		}	
	}

	//setea los parametros cadena
	public void preparedStamentString(String campo, int posicion)throws SQLException{

		if(campo == null || ConstantesData.CADENA_VACIA.equals(campo)){
			stmt.setNull(posicion, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(posicion, campo);
		}
	}
	
	//realiza la validacion de las fechas
	public java.util.Date resultStamentDate(Date fecha) throws SQLException{
		if(fecha == null){
			return null;
		}else{
			return new java.util.Date(fecha.getTime());
		}		
	}
}
