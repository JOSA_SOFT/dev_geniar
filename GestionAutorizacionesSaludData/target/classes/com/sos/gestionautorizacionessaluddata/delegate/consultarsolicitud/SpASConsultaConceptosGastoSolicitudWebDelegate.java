package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultaConceptosGastoSolicitudWebDelegate
 * Clase delegate que permite consultar los conceptos de gasto asociados a la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/04/2016
 */
public class SpASConsultaConceptosGastoSolicitudWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar el detalle de la solicitud */
	private List<ConceptosGastoVO> lConceptosGastoVO;

	private Integer numeroSolicitud;
	
	
	public SpASConsultaConceptosGastoSolicitudWebDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CONCEPTOS_X_SOLICITUD));	
		
		stmt.setInt(1, this.numeroSolicitud);	
		stmt.setNull(2, java.sql.Types.VARCHAR);
		ResultSet result = stmt.executeQuery();
		
		int indice_cnsctvo_cdgo_srvco_slctdo  = result.findColumn("cnsctvo_cdgo_srvco_slctdo");
		int indice_dscrpcn_cdfccn             = result.findColumn("dscrpcn_cdfccn");
		int indice_dscrpcn_cncpto_gsto        = result.findColumn("dscrpcn_cncpto_gsto");
		int indice_nmro_unco_ops              = result.findColumn("nmro_unco_ops");
		int indice_rzn_scl_prstdr             = result.findColumn("rzn_scl_prstdr");
		int indice_vlr_prstcn                 = result.findColumn("vlr_prstcn");
		int indice_vlr_acmldo                 = result.findColumn("vlr_acmldo");
		int indice_fcha_imprsn                = result.findColumn("fcha_imprsn");
		int indice_fcha_utlzcn_dsde           = result.findColumn("fcha_utlzcn_dsde");
		int indice_fcha_utlzcn_hsta           = result.findColumn("fcha_utlzcn_hsta");
		int indice_nmro_rdccn_cm              = result.findColumn("nmro_rdccn_cm");
		int indice_cnsctvo_cdgo_ofcna_cm      = result.findColumn("cnsctvo_cdgo_ofcna_cm");		
		int indice_dscrpcn_estdo_srvco_slctdo = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int indice_dscrpcn_cdgo_csa_nvdd      = result.findColumn("dscrpcn_cdgo_csa_nvdd");
		int indice_usro_ultma_mdfccn          = result.findColumn("usro_ultma_mdfccn");
		int cdgo_intrno_prstdr_atrzdo         = result.findColumn("cdgo_intrno_prstdr_atrzdo");
		int cnsctvo_cdgo_estdo_cncpto_srvco_slctdo  = result.findColumn("cnsctvo_cdgo_estdo_cncpto_srvco_slctdo");
		int indice_vlr_lqdcn_antrr            = result.findColumn("vlr_lqdcn_antrr");
		
		lConceptosGastoVO = new ArrayList<ConceptosGastoVO>();
		
		while(result.next()){
			ConceptosGastoVO conceptosGastoVO = new ConceptosGastoVO();
			
			conceptosGastoVO.setConsecutivoCodigoServicioSolicitado(result.getInt(indice_cnsctvo_cdgo_srvco_slctdo));
			conceptosGastoVO.setDescripcionServicioSolicitado(result.getString(indice_dscrpcn_cdfccn));
			conceptosGastoVO.setDescripcionConceptoGasto(result.getString(indice_dscrpcn_cncpto_gsto));
			conceptosGastoVO.setNumeroUnicoAutorizacionOps(result.getInt(indice_nmro_unco_ops));
			conceptosGastoVO.setRazonSocial(result.getString(indice_rzn_scl_prstdr));
			conceptosGastoVO.setValorPrestacion(result.getInt(indice_vlr_prstcn));
			conceptosGastoVO.setValorAcumulado(result.getInt(indice_vlr_acmldo));
			conceptosGastoVO.setValorAnterior(result.getInt(indice_vlr_lqdcn_antrr));
			
			if (result.getDate(indice_fcha_imprsn)!=null) {
				conceptosGastoVO.setFechaImpresion(new java.util.Date(result.getDate(indice_fcha_imprsn).getTime()));
			}
			
			if (result.getDate(indice_fcha_utlzcn_dsde)!=null) {
				conceptosGastoVO.setFechaUtilizacionDesde(new java.util.Date(result.getDate(indice_fcha_utlzcn_dsde).getTime()));
			}
		
			if (result.getDate(indice_fcha_utlzcn_hsta)!=null) {
				conceptosGastoVO.setFechaUtilizacionHasta(new java.util.Date(result.getDate(indice_fcha_utlzcn_hsta).getTime()));		
			}
			
			conceptosGastoVO.setNumeroRadicacionCuentas(result.getInt(indice_nmro_rdccn_cm));
			conceptosGastoVO.setConsecutivoOficinaCuenta(result.getInt(indice_cnsctvo_cdgo_ofcna_cm));
			conceptosGastoVO.setDescripcionEstadoConcepto(result.getString(indice_dscrpcn_estdo_srvco_slctdo));
			conceptosGastoVO.setDescripcionCausaNovedad(result.getString(indice_dscrpcn_cdgo_csa_nvdd));
			conceptosGastoVO.setUsuarioUltimaModificacion(result.getString(indice_usro_ultma_mdfccn));
			conceptosGastoVO.setCodigoInterno(result.getString(cdgo_intrno_prstdr_atrzdo));
			conceptosGastoVO.setConsecutivoEstadoServicioSolicitado(result.getInt(cnsctvo_cdgo_estdo_cncpto_srvco_slctdo));
			lConceptosGastoVO.add(conceptosGastoVO);
		}		
	}
	
	public List<ConceptosGastoVO> getlConceptosGastoVO() {
		return lConceptosGastoVO;
	}
}
