package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

public class SpAsGuardarModificacionDireccionamientoSolicitudDelegate implements SQLDelegate {

	private Integer consecutivoSolicitud; 
	private Integer consecutivoPrestacion; 
	private Integer consecutivoMotivoCausa; 
	private String observaciones; 
	private String codigoInterno;
	private String usuario;
	
	
	public SpAsGuardarModificacionDireccionamientoSolicitudDelegate(Integer consecutivoSolicitud, Integer consecutivoPrestacion, String codigoInterno, Integer consecutivoMotivoCausa,
			String observaciones, String usuario) {
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.consecutivoMotivoCausa = consecutivoMotivoCausa;
		this.observaciones = observaciones;
		this.codigoInterno = codigoInterno.trim();
		this.usuario = usuario;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  callableStatement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_DIRECCIONAMIENTO_PRESTACION_SIN_OPS));
		callableStatement.setInt(1, consecutivoSolicitud);
		callableStatement.setInt(2, consecutivoPrestacion);
		callableStatement.setString(3, codigoInterno);
		callableStatement.setString(4, usuario);
		callableStatement.setInt(5, consecutivoMotivoCausa);
		callableStatement.setString(6, observaciones);
		callableStatement.registerOutParameter(7, java.sql.Types.INTEGER);
		callableStatement.registerOutParameter(8, java.sql.Types.VARCHAR);

		callableStatement.executeQuery();

		Integer codResultado = callableStatement.getInt(7);
		String msgResultado = callableStatement.getString(8);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
