package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultarResultadoMallayGestionAuditorDelegate Clase delegate que permite
 * consultar la informacion de la malla
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 27/09/2017
 */
public class SpASConsultarResultadoMallayGestionAuditorDelegate implements SQLDelegate  {
	
	/** Consecutivo solicitud */
	private Integer consecutivoSolicitud;
	
	/**
	 * Resultado de la consulta del SP
	 */
	private List<InconsistenciasResponseServiceVO> lResultadoMalla;
	
	public SpASConsultarResultadoMallayGestionAuditorDelegate(Integer consecutivoSolicitud){
		this.consecutivoSolicitud = consecutivoSolicitud;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR));
		stmt.setInt(1, this.consecutivoSolicitud);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cdgo_cdfccn              = result.findColumn(ConstantesData.CDGO_CDFCCN);
		int index_dscrpcn_cdfccn           = result.findColumn(ConstantesData.DSCRPCN_CDFCCN);
		int index_dscrpcn_no_cnfrmdd_vldcn = result.findColumn(ConstantesData.DSCRPCN_NO_CNFRMDD_VLDCN);
		int index_infrmcn_adcnl_no_cnfrmdd = result.findColumn(ConstantesData.INFRMCN_ADCNL_NO_CNFRMDD);
		int index_orgn_dvlcn               = result.findColumn(ConstantesData.ORGN_DVLCN);
		
		lResultadoMalla = new ArrayList<InconsistenciasResponseServiceVO>();		
		while(result.next()){
			InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO = new InconsistenciasResponseServiceVO();
			inconsistenciasResponseServiceVO.setCodigoPrestacion(result.getString(index_cdgo_cdfccn));
			inconsistenciasResponseServiceVO.setDescripcionPrestacion(result.getString(index_dscrpcn_cdfccn));
			inconsistenciasResponseServiceVO.setDescripcionRegla(result.getString(index_dscrpcn_no_cnfrmdd_vldcn));
			inconsistenciasResponseServiceVO.setInformacionAdicional(result.getString(index_infrmcn_adcnl_no_cnfrmdd));
			inconsistenciasResponseServiceVO.setTipoRespuesta(result.getString(index_orgn_dvlcn));
			
			lResultadoMalla.add(inconsistenciasResponseServiceVO);			
		}
		
	}
	
	public List<InconsistenciasResponseServiceVO> getlResultadoMalla() {
		return lResultadoMalla;
	}
}
