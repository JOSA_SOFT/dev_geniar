package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.FacturaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultaInformacionFacturaSolicitudDelegate
 * Clase delegate que permite consultar la informacion de las facturas
 * @author ing. Victor Hugo Gil Ramos
 * @version 02/05/2016
 */
public class SpASConsultaInformacionFacturaSolicitudDelegate implements SQLDelegate{
	
	private Integer numeroRadicacion;
	private Integer consecutivoOficinaRadicacion;

	/** Lista que se utiliza para almacenar la informacion de la factura */
	private List<FacturaVO> lFacturaVO;
	
	
	public SpASConsultaInformacionFacturaSolicitudDelegate(Integer numeroRadicacion, Integer consecutivoOficinaRadicacion){
		this.consecutivoOficinaRadicacion = consecutivoOficinaRadicacion;
		this.numeroRadicacion = numeroRadicacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_FACTURA_SOLICITUD));	
		
		stmt.setInt(1, this.numeroRadicacion);	
		stmt.setInt(2, this.consecutivoOficinaRadicacion);	
		
		ResultSet result = stmt.executeQuery();
		
		int indice_nmro_fctra = result.findColumn("nmro_fctra");
		int indice_fcha_fctra = result.findColumn("fcha_fctra");
		
		lFacturaVO = new ArrayList<FacturaVO>();
		
		while(result.next()){
			FacturaVO facturaVO = new FacturaVO();
			facturaVO.setNumeroFactura(result.getString(indice_nmro_fctra));
			facturaVO.setFechaFactura(new java.util.Date(result.getDate(indice_fcha_fctra).getTime()));
			
			lFacturaVO.add(facturaVO);
		}		
	}
	
	public List<FacturaVO> getlFacturaVO() {
		return lFacturaVO;
	}
}
