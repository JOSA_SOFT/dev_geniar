package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpAsGuardarInformacionModificacionPrestacionDelegate implements SQLDelegate {

	private Integer consecutivoRecobro;
	private Integer consecutivoCausaNoCobro;
	private Integer consecutivoPrestacion; 
	private Integer numSolicitud; 
	private String observacion; 
	private String usuario; 
	private Integer consecutivoContingencia;
	
	public SpAsGuardarInformacionModificacionPrestacionDelegate(Integer consecutivoRecobro,	Integer consecutivoCausaNoCobro,
				Integer consecutivoPrestacion, Integer numSolicitud, String observacion, String usuario, Integer consecutivoContingencia) {
		this.consecutivoCausaNoCobro = consecutivoCausaNoCobro;
		this.consecutivoContingencia = consecutivoContingencia;
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.consecutivoRecobro = consecutivoRecobro;
		this.numSolicitud = numSolicitud;
		this.observacion = observacion;
		this.usuario = usuario;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement statement= conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_MODIFICACION_PRESTACION));
		
		statement.setInt(1, numSolicitud);
		statement.setInt(2, consecutivoPrestacion);
		statement.setInt(3, consecutivoRecobro);
		statement.setInt(4, consecutivoCausaNoCobro);
		statement.setString(5, observacion);
		statement.setInt(6, consecutivoContingencia);
		statement.setString(7, usuario);				
		statement.executeUpdate();				
	}

}
