package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

public class SpASEjecutarAnulacionSolicitudPrestacionDelegate implements SQLDelegate {
	
	private StringBuilder xml;
	
	public SpASEjecutarAnulacionSolicitudPrestacionDelegate(StringBuilder xml) {
		super();
		this.xml = xml;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {		
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.ANULAR_SOLICITUD));
		
		statement.setString(1, xml.toString());
		statement.registerOutParameter(2, java.sql.Types.INTEGER);
		statement.registerOutParameter(3, java.sql.Types.VARCHAR);
		statement.executeUpdate();
		
		int codigoResultado = statement.getInt(2);
		String mensajeResultado = statement.getString(3);
		
		if(codigoResultado < ConstantesData.VALOR_CERO) {
			throw new DataAccessException(mensajeResultado);
		}				
	}

}
