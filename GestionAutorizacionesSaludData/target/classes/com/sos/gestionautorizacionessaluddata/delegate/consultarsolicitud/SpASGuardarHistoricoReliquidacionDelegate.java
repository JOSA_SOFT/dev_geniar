package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASGuardarHistoricoReliquidacionDelegate
 * Clase Guarda la modificacion de la reliquidación
 * @author Giovanny González
 * @version 13/02/2017
 *
 */

public class SpASGuardarHistoricoReliquidacionDelegate implements SQLDelegate{

	
	/** Consecutivo solicitud **/
	private Integer consecutivoSolicitud;
	/** Consecutivo causa**/
	private Integer consecutivoCodMotivoCausa;
	/** Observacion**/
	private String observacion;
	/** Usuario**/
	private String usuario;
	/**
	* Construccion
	 * @param consecutivoSolicitud
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 */
	 
	public SpASGuardarHistoricoReliquidacionDelegate(Integer consecutivoSolicitud, Integer consecutivoCodMotivoCausa,
			String observacion, String usuario){
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
		this.observacion = observacion;
		this.usuario = usuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_MOD_RELIQUIDACION));
		statement.setInt(1, this.consecutivoSolicitud);
		statement.setInt(2, this.consecutivoCodMotivoCausa);
		statement.setString(3, this.observacion);
		statement.setString(4, this.usuario);		
		statement.registerOutParameter(5, java.sql.Types.INTEGER);
		statement.registerOutParameter(6, java.sql.Types.VARCHAR);
		statement.execute();

		Integer resultado = statement.getInt(5);
		String  mensaje = statement.getString(6);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}
}
