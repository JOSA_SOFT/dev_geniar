package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoTareaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate
 * Clase delegate que permite consultar tareas del bpm asociadas a la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 01/04/2016
 */
public class SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar el historico de las tareas asociadas a la solicitud */
	private List<HistoricoTareaVO> lHistoricoTareaVO;		
	private Integer numeroSolicitud;
	
	public SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_HISTORICO_INSTANCIAS_SOLICITUD));
		
		stmt.setInt(1, this.numeroSolicitud);		
		ResultSet result = stmt.executeQuery();
		lHistoricoTareaVO = new ArrayList<HistoricoTareaVO>();
		int indice_cnsctvo_tra_instnca         = result.findColumn("cnsctvo_tra_instnca");		
		int indice_cnsctvo_slctd_atrzcn_srvco  = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_id_prcso                    = result.findColumn("id_prcso");
		int indice_cnsctvo_tra                 = result.findColumn("cnsctvo_tra");
		int indice_nmbre_tra                   = result.findColumn("nmbre_tra");
		int indice_fcha_tra                    = result.findColumn("fcha_tra");
		int indice_usro_tra                    = result.findColumn("usro_tra");
		int indice_tpo_tra                     = result.findColumn("tpo_tra");
		int indice_nmbre_grpo_rsgo             = result.findColumn("nmbre_grpo_rsgo");
		
		while(result.next()){
			HistoricoTareaVO historicoTareaVO = new HistoricoTareaVO();
			historicoTareaVO.setConsecutivoTareaInstancia(result.getInt(indice_cnsctvo_tra_instnca));
			historicoTareaVO.setNumeroSolicitud(result.getInt(indice_cnsctvo_slctd_atrzcn_srvco));
			historicoTareaVO.setIdentificadorProceso(result.getString(indice_id_prcso));
			historicoTareaVO.setConsecutivoTarea(result.getInt(indice_cnsctvo_tra));
			historicoTareaVO.setNombreTarea(result.getString(indice_nmbre_tra));
			historicoTareaVO.setFechaTarea(new java.util.Date(result.getDate(indice_fcha_tra).getTime()));
			historicoTareaVO.setUsuarioTarea(result.getString(indice_usro_tra));
			historicoTareaVO.setTipoTarea(result.getInt(indice_tpo_tra));
			historicoTareaVO.setNombreGrupoRiesgo(result.getString(indice_nmbre_grpo_rsgo));
			
			lHistoricoTareaVO.add(historicoTareaVO);
		}		
	}
	
	public List<HistoricoTareaVO> getlHistoricoTareaVO() {
		return lHistoricoTareaVO;
	}
}
