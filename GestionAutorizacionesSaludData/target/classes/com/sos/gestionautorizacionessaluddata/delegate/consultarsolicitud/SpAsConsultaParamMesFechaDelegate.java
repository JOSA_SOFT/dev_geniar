package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

	import co.eps.sos.dataccess.SQLDelegate;


	/**
	 * Class SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate
	 * Clase delegate que permite generar los numero unicos de ops
	 * asociados a una solicitud
	 * @author Giovanny González
	 * @version 02/11/2016
	 */
	public class SpAsConsultaParamMesFechaDelegate implements SQLDelegate{
		
		/** Objeto que se utiliza para almacenar el resultado del procedimiento almacenado */
		private int paramMes;
		
		private String codParametro;
		private String usuarioVisible;
		private String tipoParametro;
		
		public SpAsConsultaParamMesFechaDelegate(String codParametro, String usuarioVisible, String tipoParametro){
			this.codParametro = codParametro;
			this.usuarioVisible = usuarioVisible;
			this.tipoParametro = tipoParametro;
		}
		

		@Override
		public void ejecutarData(Connection conn) throws SQLException {

			CallableStatement stmt = null;		
			stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_MES_MAX_FECHA));	
			stmt.setString(1, this.codParametro);
			stmt.setString(2, this.usuarioVisible);
			stmt.setString(3, this.codParametro);
			stmt.setString(4, this.usuarioVisible);
			stmt.setString(5, this.tipoParametro);
			stmt.registerOutParameter(6, java.sql.Types.INTEGER);
			stmt.registerOutParameter(7, java.sql.Types.VARCHAR);
			stmt.registerOutParameter(8, java.sql.Types.DATE);
						
			stmt.executeUpdate();
			
			paramMes=stmt.getInt(6);
				
		}
		
		public int getParamMes() {
			return paramMes;
		}


		public void setParamMes(int paramMes) {
			this.paramMes = paramMes;
		}
		
		
	
}
