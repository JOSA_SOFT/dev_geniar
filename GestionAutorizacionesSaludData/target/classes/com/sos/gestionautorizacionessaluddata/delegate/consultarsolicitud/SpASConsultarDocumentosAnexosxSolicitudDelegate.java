package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultarDocumentosAnexosxSolicitudDelegate
 * Clase delegate que permite consultar los documentos soportes x numero de solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 17/03/2016
 */
public class SpASConsultarDocumentosAnexosxSolicitudDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los documentos soporte */
	private List<DocumentoSoporteVO> lDocumentosSoporte;
	private Integer numeroSolicitud;
		
	public SpASConsultarDocumentosAnexosxSolicitudDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DOCUMENTOS_SOLICITUD));	
		
		stmt.setInt(1, this.numeroSolicitud);		
		ResultSet result = stmt.executeQuery();
		
		int indice_cnsctvo_cdgo_dcmnto_sprte = result.findColumn("cnsctvo_cdgo_dcmnto_sprte");
		int indice_cnsctvo_slctd_atrzcn_srvco = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_cnsctvo_cdgo_dcmnto = result.findColumn("cnsctvo_cdgo_dcmnto");
		int indice_dscrpcn_dcmnto_sprte = result.findColumn("dscrpcn_dcmnto_sprte");
		
		lDocumentosSoporte = new ArrayList<DocumentoSoporteVO>();
		
		while(result.next()){
			DocumentoSoporteVO documentosSoporteVO = new DocumentoSoporteVO();
			
			documentosSoporteVO.setConsecutivoDocumento(result.getLong(indice_cnsctvo_cdgo_dcmnto_sprte));
			documentosSoporteVO.setDescripcionTipoDocumentoSoporte(result.getString(indice_dscrpcn_dcmnto_sprte));
			documentosSoporteVO.setConsecutivoSolicitudAutorizacionServicio(result.getInt(indice_cnsctvo_slctd_atrzcn_srvco));
			documentosSoporteVO.setConsecutivoDocumentoSoporteVisos(result.getLong(indice_cnsctvo_cdgo_dcmnto));
			
			lDocumentosSoporte.add(documentosSoporteVO);
		}		
	}
	
	public List<DocumentoSoporteVO> getlDocumentosSoporte() {
		return lDocumentosSoporte;
	}
}
