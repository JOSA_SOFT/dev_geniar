package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASConsultarPerfilUsuario implements SQLDelegate {

	private String usuario;
	private String perfil;

	public SpASConsultarPerfilUsuario(String usuario) {
		super();
		this.usuario = usuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PERMISO_ANULAR_SOLICITUD_OPS));
		stmt.setString(1, this.usuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_dscrpcn_prfl = result.findColumn("dscrpcn_prfl");

		while (result.next()) {
			perfil = result.getString(index_dscrpcn_prfl);
		}

	}

	public String getPerfil() {
		return perfil;
	}

}
