package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EstadoDerechoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpConsultaDerechoAfiliadoAlDiaDelegate
 * Clase delegate que permite guardar en el historico de descargas
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/03/2016
 */

public class SpConsultaAfiliadoAlDiaNUIDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar el historico de formatos */
	private List<EstadoDerechoVO> lEstadosDerecho;
		
	private int numeroUnicoIdentificacionAfiliado;
	private Date fechaConsulta;
	
	
	
	public SpConsultaAfiliadoAlDiaNUIDelegate(int numeroUnicoIdentificacionAfiliado, java.util.Date fechaConsulta){
		this.numeroUnicoIdentificacionAfiliado = numeroUnicoIdentificacionAfiliado;
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement stmt = null;
		
		stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_ESTADO_DERECHO));
		
		stmt.setInt(1, this.numeroUnicoIdentificacionAfiliado);
		stmt.setString(2, ConstantesData.CADENA_VACIA);	
		stmt.setString(3, ConstantesData.CADENA_VACIA);
		stmt.setDate(4, this.fechaConsulta);
		
	    ResultSet result = stmt.executeQuery();
	    	    
	    int index_cnsctvo_cdgo_estdo_drcho = result.findColumn("cnsctvo_cdgo_estdo_drcho");
	    int index_dscrpcn_drcho            = result.findColumn("dscrpcn_drcho");
	    int index_dscrpcn_estdo_drcho      = result.findColumn("dscrpcn_estdo_drcho");
	    int index_drcho                    = result.findColumn("drcho");
	    int index_dscrpcn_csa_drcho        = result.findColumn("dscrpcn_csa_drcho");
	    int index_cnsctvo_cdgo_csa_drcho   = result.findColumn("cnsctvo_cdgo_csa_drcho");
	    	    
	    lEstadosDerecho = new ArrayList<EstadoDerechoVO>();
	    
	    while(result.next()){
	    	EstadoDerechoVO estadoDerechoVO = new EstadoDerechoVO();
	    	
	    	estadoDerechoVO.setConsecutivoCodigoEstadoDerecho(result.getInt(index_cnsctvo_cdgo_estdo_drcho));
	    	estadoDerechoVO.setDescripcionDerecho(result.getString(index_dscrpcn_drcho));
	    	estadoDerechoVO.setDescripcionEstadoAfiliado(result.getString(index_dscrpcn_estdo_drcho));
	    	estadoDerechoVO.setDerecho(result.getString(index_drcho));
	    	estadoDerechoVO.setConsecutivoCausaDerecho(result.getInt(index_cnsctvo_cdgo_csa_drcho));
	    	estadoDerechoVO.setDescripcionCausaDerecho(result.getString(index_dscrpcn_csa_drcho));
	    	
	    	lEstadosDerecho.add(estadoDerechoVO);	    	
	    }	    
	}
	
	public List<EstadoDerechoVO> getlEstadosDerecho() {
		return lEstadosDerecho;
	}
}
