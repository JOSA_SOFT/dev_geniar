package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import co.eps.sos.dataccess.exception.DataAccessException;

/**
 * Class SpAsGuardarGestionAutorizaNoConformidadPrestacion
 * Clase Guarda la la gestión de aprobación de prestaciones devueltas desde la malla
 * @author Jorge Rodriguez
 * @version 12/12/2016
 *
 */
public class SpAsGuardarGestionAutorizaNoConformidadPrestacionDelegate implements SQLDelegate {
	
	/** Consecutivo solicitud **/
	private Integer consecutivoSolicitud;
	
	/** Consecutivo prestación**/
	private Integer consecutivoPrestacion;
	
	/** Consecutivo causa**/
	private Integer consecutivoCodMotivoCausa;
	
	/** Observacion**/
	private String observacion;
	
	/** Numero Autorización IPS**/
	private String numAutorizacionIPS;
	
	/** Codigo interno prestador**/
	private String codInternoPrestador;
	
	/** Usuario**/
	private String usuario;
	
	private int resultado;
	
	public SpAsGuardarGestionAutorizaNoConformidadPrestacionDelegate(
			Integer consecutivoSolicitud, Integer consecutivoPrestacion,
			Integer consecutivoCodMotivoCausa, String observacion,
			String numAutorizacionIPS, String codInternoPrestador,
			String usuario) {

		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
		this.observacion = observacion;
		this.numAutorizacionIPS = numAutorizacionIPS;
		this.codInternoPrestador = codInternoPrestador;
		this.usuario = usuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_GESTION_AUTORIZA_PRESTACIONES));
		
		statement.setInt(1, this.consecutivoSolicitud);
		statement.setInt(2, this.consecutivoPrestacion);
		statement.setInt(3, this.consecutivoCodMotivoCausa);
		statement.setString(4, this.observacion);
		statement.setString(5, this.numAutorizacionIPS);
		statement.setString(6, this.usuario);
		statement.setString(7, this.codInternoPrestador);
		
		
		statement.registerOutParameter(8, java.sql.Types.INTEGER);
		statement.registerOutParameter(9, java.sql.Types.VARCHAR);

		statement.executeUpdate();

		resultado = statement.getInt(8);
		String  mensaje = statement.getString(9);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
		
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}


	
	
	

}
