package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
	

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpPMSelecClasesAtencionDelegate
 * Clase Delegate que consulta la clase de Atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class SpPMSelecClasesAtencionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la clase de atencion */	
	List<ClaseAtencionVO> lClaseAtencion;
	
	private Date fechaConsulta;
	
	public SpPMSelecClasesAtencionDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_CLASEATENCION"));
		stmt.setDate(1, this.fechaConsulta);
				
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_clse_atncn = result.findColumn("cnsctvo_cdgo_clse_atncn");
		int index_dscrpcn_clse_atncn = result.findColumn("dscrpcn");
		int index_cnsctvo_cdgo_frma_atncn = result.findColumn("cnsctvo_cdgo_frma_atncn");
		
		lClaseAtencion = new ArrayList<ClaseAtencionVO>();
				
		while(result.next()){
			
			ClaseAtencionVO atencionVO = new ClaseAtencionVO();
			atencionVO.setConsecutivoClaseAtencion(result.getInt(index_cnsctvo_cdgo_clse_atncn));
			atencionVO.setDescripcionClaseAtencion(result.getString(index_dscrpcn_clse_atncn));
			atencionVO.setConsecutivoFormaAtencion(result.getInt(index_cnsctvo_cdgo_frma_atncn));
			
			lClaseAtencion.add(atencionVO);			
		}				
	}
	
	public List<ClaseAtencionVO> getlClaseAtencion() {
		return lClaseAtencion;
	}
}
