package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerTiposDiagnosticosSaludDelegate
 * Clase Delegate que consulta los tipos de diagnosticos
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/11/2015
 *
 */

public class SpTraerTiposDiagnosticosSaludDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los tipos de diagnosticos */
	private List<TiposDiagnosticosVO> lTiposDiagnosticos;
	
	private String visibleUsuario = "S";
	private Date fechaConsulta;
	
	public SpTraerTiposDiagnosticosSaludDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());	
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_TIPOSDIAGNOSTICOS"));
		stmt.setDate(1, this.fechaConsulta);
		stmt.setString(2, visibleUsuario);
				
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_dgnstco_sld = result.findColumn("cnsctvo_cdgo_tpo_dgnstco_sld");
		int index_cdgo_tpo_dgnstco_sld         = result.findColumn("cdgo_tpo_dgnstco_sld");
		int index_dscrpcn_tpo_dgnstco_sld      = result.findColumn("dscrpcn_tpo_dgnstco_sld");
		
		lTiposDiagnosticos = new ArrayList<TiposDiagnosticosVO>();
		
		while(result.next()){
			TiposDiagnosticosVO tiposDiagnosticosVO = new TiposDiagnosticosVO();
			tiposDiagnosticosVO.setConsecutivoTipoDiagnostico(result.getInt(index_cnsctvo_cdgo_tpo_dgnstco_sld));
			tiposDiagnosticosVO.setCodigoTipoDiagnostico(result.getString(index_cdgo_tpo_dgnstco_sld));
			tiposDiagnosticosVO.setDescripcionTipoDiagnostico(result.getString(index_dscrpcn_tpo_dgnstco_sld));
			
			lTiposDiagnosticos.add(tiposDiagnosticosVO);
		}		
	}
	
	public List<TiposDiagnosticosVO> getlTiposDiagnosticos() {
		return lTiposDiagnosticos;
	}
}
