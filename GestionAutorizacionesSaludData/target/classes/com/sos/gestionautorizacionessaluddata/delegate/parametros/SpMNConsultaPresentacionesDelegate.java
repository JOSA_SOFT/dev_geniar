package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpMNConsultaPresentacionesDelegate Clase Delegate que consulta la
 * presentacion del medicamento
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/01/2016
 *
 */
public class SpMNConsultaPresentacionesDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar la presentacion del medicamento */
	private List<PresentacionVO> lPresentacion;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_PRESENTACION_MED"));

		ResultSet result = stmt.executeQuery();

		int index_cnsctvo_cdgo_prsntcn = result.findColumn("cnsctvo_cdgo_prsntcn");
		int index_cdgo_prsntcn = result.findColumn("cdgo_prsntcn");
		int index_dscrpcn_prsntcn = result.findColumn("dscrpcn_prsntcn");

		lPresentacion = new ArrayList<PresentacionVO>();

		while (result.next()) {
			PresentacionVO presentacionVO = new PresentacionVO();
			presentacionVO.setConsecutivoCodigoPresentacion(result.getString(index_cnsctvo_cdgo_prsntcn) != null ? new BigInteger(result.getString(index_cnsctvo_cdgo_prsntcn)) : null);
			presentacionVO.setCodigoPresentacion(result.getString(index_cdgo_prsntcn));
			presentacionVO.setDescripcionPresentacion(result.getString(index_dscrpcn_prsntcn));

			lPresentacion.add(presentacionVO);
		}
	}

	public List<PresentacionVO> getlPresentacion() {
		return lPresentacion;
	}
}
