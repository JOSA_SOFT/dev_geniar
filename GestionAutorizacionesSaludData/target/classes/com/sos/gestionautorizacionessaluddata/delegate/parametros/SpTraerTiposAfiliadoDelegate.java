package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerTiposAfiliadoDelegate Clase delegate que permite consultar los
 * tipos de afiliado
 * 
 * @author Ing. Germán Pérez
 * @version 22/09/2017
 *
 */

public class SpTraerTiposAfiliadoDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar los tipos de afiliado */
	private List<AfiliadoVO> lTiposAfiliado;

	private String codigoAfiliado;
	private Date fechaReferencia;
	private String cadenaSeleccion;
	private String visibleUsuario;

	public SpTraerTiposAfiliadoDelegate(String codigoAfiliado, java.util.Date fechaReferencia, String cadenaSeleccion,
			String visibleUsuario) {
		this.codigoAfiliado = codigoAfiliado;
		this.fechaReferencia = new Date(fechaReferencia.getTime());
		this.cadenaSeleccion = cadenaSeleccion;
		this.visibleUsuario = visibleUsuario;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaParametrosSp.getString(ConstantesData.CONSULTA_TIPOS_AFILIADO));

		if (this.codigoAfiliado == null) {
			stmt.setNull(1, java.sql.Types.CHAR);
		} else {
			stmt.setString(1, this.codigoAfiliado);
		}

		if (this.fechaReferencia == null) {
			stmt.setNull(2, java.sql.Types.DATE);
		} else {
			stmt.setDate(2, this.fechaReferencia);
		}

		if (this.cadenaSeleccion == null) {
			stmt.setNull(3, java.sql.Types.VARCHAR);
		} else {
			stmt.setString(3, this.cadenaSeleccion);
		}

		if (this.visibleUsuario == null) {
			stmt.setNull(4, java.sql.Types.CHAR);
		} else {
			stmt.setString(4, this.visibleUsuario);
		}

		ResultSet result = stmt.executeQuery();

		int codigoTipoAfiliado = result.findColumn(ConstantesData.CODIGO_TIPO_AFILIADO);
		int descripcion = result.findColumn(ConstantesData.DESCRIPCION);
		int consecutivoCodigoTipoAfiliado = result.findColumn(ConstantesData.CONSECUTIVO_CODIGO_TIPO_AFILIADO);

		lTiposAfiliado = new ArrayList<AfiliadoVO>();

		while (result.next()) {
			AfiliadoVO afiliadoVO = new AfiliadoVO();
			afiliadoVO.setCodigoTipoAfiliado(result.getString(codigoTipoAfiliado));
			afiliadoVO.setDescripcionTipoAfiliado(result.getString(descripcion));
			afiliadoVO.setConsecutivoTipoAfiliado(result.getInt(consecutivoCodigoTipoAfiliado));

			lTiposAfiliado.add(afiliadoVO);
		}

		stmt.close();
	}

	public List<AfiliadoVO> getlTiposAfiliado() {
		return lTiposAfiliado;
	}

}
