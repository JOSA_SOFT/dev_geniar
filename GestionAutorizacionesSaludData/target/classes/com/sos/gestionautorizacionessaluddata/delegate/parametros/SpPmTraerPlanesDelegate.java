package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;


/**
 * Class SpPmTraerPlanesDelegate
 * Clase delegate que permite consultar los planes de salud
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 *
 */

public class SpPmTraerPlanesDelegate implements SQLDelegate{
	
	
	/** Lista que se utiliza para almacenar los planes de salud */	
	private List<PlanVO> lPlanes;

	private int visibleUsuario = 1;
	private Date fechaConsulta;
	
	
	public SpPmTraerPlanesDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_PLANES"));
		stmt.setNull(1, java.sql.Types.VARCHAR);
		stmt.setDate(2, this.fechaConsulta);		
		stmt.setNull(3, java.sql.Types.VARCHAR);
		stmt.setNull(4, java.sql.Types.VARCHAR);
		stmt.setInt(5, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_pln = result.findColumn("cnsctvo_cdgo_pln");
		int index_cdgo_pln         = result.findColumn("cdgo_pln");
		int index_dscrpcn_pln      = result.findColumn("dscrpcn_pln");
		
		lPlanes = new ArrayList<PlanVO>();
		
		while(result.next()){
			
			PlanVO planVO = new PlanVO();
			planVO.setConsectivoPlan(result.getInt(index_cnsctvo_cdgo_pln));
			planVO.setCodigoPlan(result.getString(index_cdgo_pln));
			planVO.setDescripcionPlan(result.getString(index_dscrpcn_pln));
			
			lPlanes.add(planVO);			
		}	
	}
	
	public List<PlanVO> getlPlanes() {
		return lPlanes;
	}
}
