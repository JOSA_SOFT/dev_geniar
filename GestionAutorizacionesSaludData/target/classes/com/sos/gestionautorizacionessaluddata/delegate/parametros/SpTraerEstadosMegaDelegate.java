package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.EstadoMegaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerTiposAfiliadoDelegate Clase delegate que permite consultar los
 * tipos de afiliado
 * 
 * @author Ing. Germán Pérez
 * @version 22/09/2017
 *
 */

public class SpTraerEstadosMegaDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar los estados de MEGA */
	private List<EstadoMegaVO> lEstadosMega;

	private Date fechaActual;

	public SpTraerEstadosMegaDelegate(java.util.Date fechaActual) {
		this.fechaActual = new Date(fechaActual.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaParametrosSp.getString(ConstantesData.CONSULTA_ESTADOS_MEGA));

		if (this.fechaActual == null) {
			stmt.setNull(1, java.sql.Types.DATE);
		} else {
			stmt.setDate(1, this.fechaActual);
		}

		ResultSet result = stmt.executeQuery();

		int consecutivoCodigoEstadoMega = result.findColumn(ConstantesData.CONSECUTIVO_CODIGO_ESTADO_MEGA);
		int codigoEstadoMega = result.findColumn(ConstantesData.CODIGO_ESTADO_MEGA);
		int descripcionEstadoMega = result.findColumn(ConstantesData.DESCRIPCION_ESTADO_MEGA);
		int codigoEstadoAtencion = result.findColumn(ConstantesData.CODIGO_ESTADO_ATENCION);
		int descripcionEstadoAtencion = result.findColumn(ConstantesData.DESCRIPCION_ESTADO_ATENCION);

		lEstadosMega = new ArrayList<EstadoMegaVO>();

		while (result.next()) {
			EstadoMegaVO estadoMegaVO = new EstadoMegaVO();
			estadoMegaVO.setConsecutivoCodigoEstadoMega(result.getInt(consecutivoCodigoEstadoMega));
			estadoMegaVO.setCodigoEstadoMega(result.getString(codigoEstadoMega));
			estadoMegaVO.setDescripcionEstadoMega(result.getString(descripcionEstadoMega));
			estadoMegaVO.setCodigoEstadoAtencion(result.getString(codigoEstadoAtencion));
			estadoMegaVO.setDescripcionEstadoAtencion(result.getString(descripcionEstadoAtencion));

			lEstadosMega.add(estadoMegaVO);
		}

		stmt.close();
	}

	public List<EstadoMegaVO> getlEstadosMega() {
		return lEstadosMega;
	}

}
