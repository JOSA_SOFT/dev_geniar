package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;


/**
 * Class SpASConsultaParametrosGeneralesAplicacionDelegate
 * Clase Delegate que consulta los parametros generales
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/01/2016
 *
 */
public class SpASConsultaParametrosGeneralesAplicacionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar los parametros generales */	
	private List<ParametrosGeneralesVO> lParametrosGenerales;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
	
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_PARAMETROS_GENERALES"));
						
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_prmtro_gnrl = result.findColumn("cnsctvo_cdgo_prmtro_gnrl");	
		int index_vlr_prmtro_nmrco         = result.findColumn("vlr_prmtro_nmrco");	
		
		lParametrosGenerales = new ArrayList<ParametrosGeneralesVO>();
		
		while(result.next()){
			ParametrosGeneralesVO parametrosGeneralesVO = new ParametrosGeneralesVO();
			parametrosGeneralesVO.setConsecutivoCodigoParametroGeneral(Integer.parseInt(result.getString(index_cnsctvo_cdgo_prmtro_gnrl)));
			parametrosGeneralesVO.setValorParametroGeneral(result.getLong(index_vlr_prmtro_nmrco));
			
			lParametrosGenerales.add(parametrosGeneralesVO);
		}			
	}
	
	public List<ParametrosGeneralesVO> getlParametrosGenerales() {
		return lParametrosGenerales;
	}
}
