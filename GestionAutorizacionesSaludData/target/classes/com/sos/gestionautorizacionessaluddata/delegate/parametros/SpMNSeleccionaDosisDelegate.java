package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpMNSeleccionaDosisDelegate
 * Clase delegate que permite consultar la dosis del medicamento
 * @author ing. Victor Hugo Gil Ramos
 * @version 18/12/2015
 */

public class SpMNSeleccionaDosisDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la dosis del medicamento */
	private List<DosisVO> lDosis;
	
	
	private String visibleUsuario = "N";
	private Date fechaConsulta;
	
	
	public SpMNSeleccionaDosisDelegate(java.util.Date fechaConsulta){		
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());		
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_DOSIS_MED"));
		stmt.setDate(1, this.fechaConsulta);		
		stmt.setString(2, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_dss = result.findColumn("cnsctvo_cdgo_dss");
		int index_cdgo_dss         = result.findColumn("cdgo_dss");
		int index_dscrpcn_dss      = result.findColumn("dscrpcn_dss");
				
		lDosis = new ArrayList<DosisVO>();
		
		while(result.next()){
			DosisVO dosisVO = new DosisVO();
			dosisVO.setConsecutivoCodigoDosis(result.getInt(index_cnsctvo_cdgo_dss));
			dosisVO.setCodigoDosis(result.getString(index_cdgo_dss));
			dosisVO.setDescripcionDosis(result.getString(index_dscrpcn_dss));
			
			lDosis.add(dosisVO);			
		}		
	}
	
	public List<DosisVO> getlDosis() {
		return lDosis;
	}
}
