package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpPmTraerTiposPlanDelegate Clase delegate que permite consultar los
 * tipos de plan de salud
 * 
 * @author Ing. Germán Pérez
 * @version 22/09/2017
 *
 */

public class SpPmTraerTiposPlanDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar los planes de salud */
	private List<PlanVO> lTiposPlan;

	private Date fechaConsulta;

	public SpPmTraerTiposPlanDelegate(java.util.Date fechaConsulta) {
		this.fechaConsulta = new Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaParametrosSp.getString(ConstantesData.CONSULTA_TIPOS_PLAN));

		if (this.fechaConsulta == null) {
			stmt.setNull(1, java.sql.Types.DATE);
		} else {
			stmt.setDate(1, this.fechaConsulta);
		}

		ResultSet result = stmt.executeQuery();

		int consecutivoCodigoTipoPlan = result.findColumn(ConstantesData.CONSECUTIVO_CODIGO_TIPO_PLAN);
		int codigoTipoPlan = result.findColumn(ConstantesData.CODIGO_TIPO_PLAN);
		int descripcionTipoPlan = result.findColumn(ConstantesData.DESCRIPCION_TIPO_PLAN);

		lTiposPlan = new ArrayList<PlanVO>();

		while (result.next()) {
			PlanVO planVO = new PlanVO();
			planVO.setConsectivoPlan(result.getInt(consecutivoCodigoTipoPlan));
			planVO.setCodigoPlan(result.getString(codigoTipoPlan));
			planVO.setDescripcionPlan(result.getString(descripcionTipoPlan));

			lTiposPlan.add(planVO);
		}

		stmt.close();
	}

	public List<PlanVO> getlTiposPlan() {
		return lTiposPlan;
	}

}
