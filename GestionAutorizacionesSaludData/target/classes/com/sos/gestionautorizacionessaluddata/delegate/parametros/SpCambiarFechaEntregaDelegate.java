package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpCambiarFechaEntregaDelegate Clase Delegate que realiza el cambio de
 * fecha de entrega
 * 
 * @author ing. Jose Olmedo Soto AGuirre
 * @version 17/10/2017
 *
 */
public class SpCambiarFechaEntregaDelegate implements SQLDelegate {

	private Date fchaMdfcr;
	private String usroCrcn;

	public SpCambiarFechaEntregaDelegate(java.util.Date fchaMdfcr, String usroCrcn) {
		this.fchaMdfcr = new Date(fchaMdfcr.getTime());
		this.usroCrcn = usroCrcn;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CAMBIO_FECHA_ENTREGA));

		stmt.setDate(1, this.fchaMdfcr);
		stmt.setString(2, usroCrcn);
		stmt.executeUpdate();

	}

	public Date getFchaMdfcr() {
		return fchaMdfcr;
	}

	public void setFchaMdfcr(Date fchaMdfcr) {
		this.fchaMdfcr = fchaMdfcr;
	}

	public String getUsroCrcn() {
		return usroCrcn;
	}

	public void setUsroCrcn(String usroCrcn) {
		this.usroCrcn = usroCrcn;
	}

}
