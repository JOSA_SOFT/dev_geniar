package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OpcionVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpMNSeleccionaOpcionDelegate
 * Clase Delegate que consulta las opciones SI NO en los combos
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpMNSeleccionaOpcionDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar las opciones*/
	private List<OpcionVO> lOpcion;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_SELECCIONAOPCION"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_opcn = result.findColumn("cnsctvo_cdgo_opcn");
		int index_dscrpcn_opcn      = result.findColumn("dscrpcn_opcn");
		
		lOpcion = new ArrayList<OpcionVO>();
		while(result.next()){
			OpcionVO opcionVO = new OpcionVO();
		    opcionVO.setConsecutivoOpcion(result.getInt(index_cnsctvo_cdgo_opcn));
		    opcionVO.setDescripcionOpcion(result.getString(index_dscrpcn_opcn));
		    
		    lOpcion.add(opcionVO);
		}		
	}	

	public List<OpcionVO> getlOpcion() {
		return lOpcion;
	}
}
