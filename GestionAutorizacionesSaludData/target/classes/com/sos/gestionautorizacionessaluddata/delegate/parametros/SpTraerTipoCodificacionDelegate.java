package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpTraerTipoCodificacionDelegate
 * Clase Delegate que consulta los tipos de codificaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class SpTraerTipoCodificacionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar los tipos de codificacion */	
	private List<TipoCodificacionVO> lTipoCodificacion;
	private List<SelectItem> lTipoCodificacionBPM;
	private int auxValidacionCUOS; 
	
	public SpTraerTipoCodificacionDelegate(int auxValidacionCUOS){
		this.auxValidacionCUOS = auxValidacionCUOS;
	}
	
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_TIPOS_CODIFICACION"));
		
		stmt.setNull(1, java.sql.Types.VARCHAR);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_cdfccn = result.findColumn("cnsctvo_cdgo_tpo_cdfccn");
		int index_cdgo_tpo_cdfccn         = result.findColumn("cdgo_tpo_cdfccn");
		int index_dscrpcn_tpo_cdfccn      = result.findColumn("dscrpcn_tpo_cdfccn");
		
		lTipoCodificacion = new ArrayList<TipoCodificacionVO>();
		lTipoCodificacionBPM = new ArrayList<SelectItem>();
		
		while(result.next()){
			int consecutivoCodigoTipoCodificacion = result.getInt(index_cnsctvo_cdgo_tpo_cdfccn);			
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
			
			//se valida que el tipo de codificacion sea igual a cups (4) o cums (5)
			if(consecutivoCodigoTipoCodificacion == ConstantesData.CONSECUTIVO_CUPS || consecutivoCodigoTipoCodificacion == ConstantesData.CONSECUTIVO_CUMS){
				
				tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(consecutivoCodigoTipoCodificacion);
				tipoCodificacionVO.setCodigoTipoCodificacion(result.getString(index_cdgo_tpo_cdfccn));
				tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(index_dscrpcn_tpo_cdfccn));						
				lTipoCodificacion.add(tipoCodificacionVO);
				
				lTipoCodificacionBPM.add(generarItem(consecutivoCodigoTipoCodificacion,result.getString(index_dscrpcn_tpo_cdfccn)));
			}
			//se valida que el tipo de codificacion sea igual a cuos(9)
			if(consecutivoCodigoTipoCodificacion == ConstantesData.CONSECUTIVO_CUOS && auxValidacionCUOS == ConstantesData.VALIDACION_CUOS){
				
				tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(consecutivoCodigoTipoCodificacion);
				tipoCodificacionVO.setCodigoTipoCodificacion(result.getString(index_cdgo_tpo_cdfccn));
				tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(index_dscrpcn_tpo_cdfccn));
				lTipoCodificacion.add(tipoCodificacionVO);
				
				lTipoCodificacionBPM.add(generarItem(consecutivoCodigoTipoCodificacion,result.getString(index_dscrpcn_tpo_cdfccn)));
			}			
		}		
	}
	
	public SelectItem generarItem(int consecutivoTipoPrestacion, String descripcionTipoPrestacion){
		SelectItem item = new SelectItem(); 
		item.setValue(consecutivoTipoPrestacion);
		item.setLabel(descripcionTipoPrestacion);		
		return item;
	} 

	public List<TipoCodificacionVO> getlTipoCodificacion() {
		return lTipoCodificacion;
	}
	
	public List<SelectItem> getlTipoCodificacionBPM() {
		return lTipoCodificacionBPM;
	}
}
