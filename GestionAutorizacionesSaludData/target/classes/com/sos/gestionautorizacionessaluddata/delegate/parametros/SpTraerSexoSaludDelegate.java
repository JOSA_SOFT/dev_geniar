package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpTraerSexoSaludDelegate
 * Clase delegate que permite consultar el genero
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class SpTraerSexoSaludDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar los generos consultados de la BD */
	private List<GeneroVO> lGenero;
	private String visibleUsuario = "N";
	private Date fechaConsulta;
	
	
	public SpTraerSexoSaludDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_GENEROS"));
		stmt.setDate(1, this.fechaConsulta);
		stmt.setNull(2, java.sql.Types.VARCHAR);		
		stmt.setString(3, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_sxo = result.findColumn("cnsctvo_cdgo_sxo");
		int index_dscrpcn_sxo = result.findColumn("dscrpcn_sxo");
		int index_cdgo_sxo = result.findColumn("cdgo_sxo");
		
		lGenero = new ArrayList<GeneroVO>();
				
		while(result.next()){
			GeneroVO generoVo = new GeneroVO();
			generoVo.setConsecutivoGenero(result.getInt(index_cnsctvo_cdgo_sxo));
			generoVo.setDescripcionGenero(result.getString(index_dscrpcn_sxo));
			generoVo.setCodigoGenero(result.getString(index_cdgo_sxo));			
			lGenero.add(generoVo);
		}		
	}
	
	public List<GeneroVO> getlGenero() {
		return lGenero;
	}
}
