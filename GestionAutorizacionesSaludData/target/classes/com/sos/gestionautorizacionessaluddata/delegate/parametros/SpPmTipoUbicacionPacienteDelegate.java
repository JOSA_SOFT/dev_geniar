package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpPmTipoUbicacionPaciente
 * Clase Delegate que consulta el tipo de ubicacion del paciente
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpPmTipoUbicacionPacienteDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la ubicacion del paciente */	
	private List<UbicacionPacienteVO> lUbicacionPaciente;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_UBICACION_PACIENTE"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_ubccn_pcnte = result.findColumn("cnsctvo_cdgo_tpo_ubccn_pcnte");
		int index_dscrpcn_tpo_ubccn_pcnte      = result.findColumn("dscrpcn_tpo_ubccn_pcnte");
		
		lUbicacionPaciente = new ArrayList<UbicacionPacienteVO>();
		
		while(result.next()){
			UbicacionPacienteVO ubicacionPacienteVO = new UbicacionPacienteVO();
			ubicacionPacienteVO.setConsecutivoUbicacionPaciente(result.getInt(index_cnsctvo_cdgo_tpo_ubccn_pcnte));
			ubicacionPacienteVO.setDescripcionUbicacionPaciente(result.getString(index_dscrpcn_tpo_ubccn_pcnte));
			
			lUbicacionPaciente.add(ubicacionPacienteVO);			
		}		
	}
	
	public List<UbicacionPacienteVO> getlUbicacionPaciente() {
		return lUbicacionPaciente;
	}
}
