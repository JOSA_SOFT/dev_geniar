package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpPmServiciosHospitalizacion
 * Clase Delegate que consulta los servicios de hospitalizacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpPmServiciosHospitalizacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los servicios de hospitalizacion */
	private List<ServicioHospitalizacionVO> lServicioHospitalizacion;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_SERVICIOHOSPITALIZACION"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_srvco_hsptlzcn = result.findColumn("cnsctvo_cdgo_srvco_hsptlzcn");
		int index_dscrpcn_srvco_hsptlzcn      = result.findColumn("dscrpcn_srvco_hsptlzcn");
		
		lServicioHospitalizacion = new ArrayList<ServicioHospitalizacionVO>();
		
		while(result.next()){
			ServicioHospitalizacionVO servicioHospitalizacionVO = new ServicioHospitalizacionVO();
			servicioHospitalizacionVO.setConsecutivoSevicioHospitalizacion(result.getInt(index_cnsctvo_cdgo_srvco_hsptlzcn));
			servicioHospitalizacionVO.setDescripcionSevicioHospitalizacion(result.getString(index_dscrpcn_srvco_hsptlzcn));
			
			lServicioHospitalizacion.add(servicioHospitalizacionVO);
		}
	}
	
	public List<ServicioHospitalizacionVO> getlServicioHospitalizacion() {
		return lServicioHospitalizacion;
	}
}
