package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;


/**
 * Class SpMNConsultaConcentracionDelegate
 * Clase Delegate que consulta la concentracion del medicamento
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/01/2016
 *
 */
public class SpMNConsultaConcentracionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la concentracion del medicamento */	
	private List<ConcentracionVO> lConcentracion;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_CONCENTRACION_MED"));
						
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_cncntrcn = result.findColumn("cnsctvo_cdgo_cncntrcn");			
		int index_cdgo_cncntrcn         = result.findColumn("cdgo_cncntrcn");	
		int index_dscrpcn_cncntrcn      = result.findColumn("dscrpcn_cncntrcn");	
		
		lConcentracion = new ArrayList<ConcentracionVO>();
		
		while(result.next()){
			ConcentracionVO concentracionVO = new ConcentracionVO();
			
			concentracionVO.setConsecutivoConcentracion(result.getInt(index_cnsctvo_cdgo_cncntrcn));
			concentracionVO.setCodigoConcentracion(result.getString(index_cdgo_cncntrcn));
			concentracionVO.setDescripcionConcentracion(result.getString(index_dscrpcn_cncntrcn));
			
			lConcentracion.add(concentracionVO);
		}		
	}
	
	public List<ConcentracionVO> getlConcentracion() {
		return lConcentracion;
	}	
}
