package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerOficinas
 * Clase delegate que permite consultar las oficinas
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/12/2015
 *
 */
public class SpPMTraerOficinasSedesDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las oficinas */
	private List<OficinasVO> lOficinas;
		
	private String visibleUsuario = "S";
	private Date fechaConsulta;
	private String codigoOficina;
	private String descripcionOficina;
	
	public SpPMTraerOficinasSedesDelegate(java.util.Date fechaConsulta, String codigoOficina, String descripcionOficina){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoOficina = codigoOficina;
		this.descripcionOficina = descripcionOficina;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_OFICINAS"));
		
		if(this.codigoOficina == null || ConstantesData.CADENA_VACIA.equals(this.codigoOficina)){
		    stmt.setNull(1, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(1, this.codigoOficina);
		}
		
		stmt.setDate(2, this.fechaConsulta);	
		
		if(this.descripcionOficina == null || ConstantesData.CADENA_VACIA.equals(this.descripcionOficina)){
		    stmt.setNull(3, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(3, this.descripcionOficina);
		}
		
		stmt.setNull(4, java.sql.Types.INTEGER);
		stmt.setString(5, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_ofcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int index_cnsctvo_cdgo_sde = result.findColumn("cnsctvo_cdgo_sde");
		int index_cdgo_ofcna = result.findColumn("cdgo_ofcna");
		int index_cdgo_sde = result.findColumn("cdgo_sde");
		int index_dscrpcn_ofcna = result.findColumn("dscrpcn_ofcna");
		
		lOficinas = new ArrayList<OficinasVO>();
		
		while(result.next()){
			OficinasVO oficinasVO = new OficinasVO();
			oficinasVO.setConsecutivoCodigoOficina(result.getInt(index_cnsctvo_cdgo_ofcna));
			oficinasVO.setConsecutivoCodigoSede(result.getInt(index_cnsctvo_cdgo_sde));
			oficinasVO.setCodigoOficina(result.getString(index_cdgo_ofcna));
			oficinasVO.setCodigoSede(result.getString(index_cdgo_sde));
			oficinasVO.setDescripcionOficina(result.getString(index_dscrpcn_ofcna));
			
			lOficinas.add(oficinasVO);			
		}			
	}
	
	public List<OficinasVO> getlOficinas() {
		return lOficinas;
	}
}
