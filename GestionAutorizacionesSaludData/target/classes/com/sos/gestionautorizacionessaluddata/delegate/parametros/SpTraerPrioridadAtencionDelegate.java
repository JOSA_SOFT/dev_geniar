package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.PrioridadAtencionVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerPrioridadAtencion
 * Clase Delegate que consulta la prioridad de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpTraerPrioridadAtencionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la prioridad de la atencion */	
	private List<PrioridadAtencionVO> lPrioridadAtencion;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_PRIORIDADATENCION"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_prrdd_atncn = result.findColumn("cnsctvo_cdgo_prrdd_atncn");
		int index_dscrpcn_prrdd_atncn      = result.findColumn("dscrpcn_prrdd_atncn");
		
		lPrioridadAtencion = new ArrayList<PrioridadAtencionVO>();
		
		while(result.next()){
			
			PrioridadAtencionVO prioridadAtencionVO  = new PrioridadAtencionVO();
			prioridadAtencionVO.setConsecutivoPrioridadAtencion(result.getInt(index_cnsctvo_cdgo_prrdd_atncn));
			prioridadAtencionVO.setDescripcionPrioridadAtencion(result.getString(index_dscrpcn_prrdd_atncn));
			
			lPrioridadAtencion.add(prioridadAtencionVO);			
		}		
	}
	
	public List<PrioridadAtencionVO> getlPrioridadAtencion() {
		return lPrioridadAtencion;
	}
}
