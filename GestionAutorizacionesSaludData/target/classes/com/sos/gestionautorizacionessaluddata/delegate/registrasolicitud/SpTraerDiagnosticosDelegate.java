package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpTraerDiagnosticosDelegate
 * Clase Delegate que consulta los diagnosticos por codigo y/o descripcion
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpTraerDiagnosticosDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar los diagnosticos */
	private List<DiagnosticosVO> lDiagnosticos;
	
	private String visibleUsuario = "S";
	private String codigoDiagnostico;
	private String descripcionDiagnostico;
	private Date fechaConsulta;
	
	public SpTraerDiagnosticosDelegate(java.util.Date fechaConsulta, String codigoDiagnostico, String descripcionDiagnostico){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoDiagnostico = codigoDiagnostico;
		this.descripcionDiagnostico = descripcionDiagnostico;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_DIAGNOSTICOS"));
		
		stmt.setDate(1, this.fechaConsulta);
		
		if(this.codigoDiagnostico == null || ConstantesData.CADENA_VACIA.equals(this.codigoDiagnostico)){
		    stmt.setNull(2, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(2, this.codigoDiagnostico);
		}
		
		if( this.descripcionDiagnostico == null || ConstantesData.CADENA_VACIA.equals(this.descripcionDiagnostico)){
		    stmt.setNull(3, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(3, this.descripcionDiagnostico);
		}
		
		stmt.setString(4, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_dgnstco  = result.findColumn("cnsctvo_cdgo_dgnstco");
		int index_cdgo_dgnstco          = result.findColumn("cdgo_dgnstco");
		int index_dscrpcn_dgnstco       = result.findColumn("dscrpcn_dgnstco");		
		int index_cnsctvo_cdgo_cntngnca = result.findColumn("cnsctvo_cdgo_cntngnca");
		int index_cdgo_mrbldd           = result.findColumn("cdgo_mrbldd");
		int index_cnsctvo_cdgo_mrbldd   = result.findColumn("cnsctvo_cdgo_mrbldd");				
		int index_nmro_ds_prmdo         = result.findColumn("nmro_ds_prmdo");		
		int index_cnsctvo_cdgo_rcbro    = result.findColumn("cnsctvo_cdgo_rcbro");		
		int index_cnsctvo_cdgo_sxo      = result.findColumn("cnsctvo_cdgo_sxo");
		
		lDiagnosticos = new ArrayList<DiagnosticosVO>();
		
		while(result.next()){
			GeneroVO generoVO = new GeneroVO();
			DiagnosticosVO diagnosticosVO = new DiagnosticosVO();
			
			generoVO.setConsecutivoGenero(result.getInt(index_cnsctvo_cdgo_sxo));						
			
			diagnosticosVO.setConsecutivoCodigoDiagnostico(result.getInt(index_cnsctvo_cdgo_dgnstco));
			diagnosticosVO.setCodigoDiagnostico(result.getString(index_cdgo_dgnstco));
			diagnosticosVO.setDescripcionDiagnostico(result.getString(index_dscrpcn_dgnstco));
			diagnosticosVO.setConsecutivoCodigoContingencia(result.getInt(index_cnsctvo_cdgo_cntngnca));
			diagnosticosVO.setCodigoMorbilidad(result.getString(index_cdgo_mrbldd));
			diagnosticosVO.setConsecutivoCodigoMorbilidad(result.getInt(index_cnsctvo_cdgo_mrbldd));
			diagnosticosVO.setNumeroDiasPromedio(result.getInt(index_nmro_ds_prmdo));
			diagnosticosVO.setConsecutivoCodigoRecobro(result.getInt(index_cnsctvo_cdgo_rcbro));			
			
			diagnosticosVO.setGeneroVO(generoVO);
			
			lDiagnosticos.add(diagnosticosVO);			
		}			
	}
	
	public List<DiagnosticosVO> getlDiagnosticos() {
		return lDiagnosticos;
	}
}
