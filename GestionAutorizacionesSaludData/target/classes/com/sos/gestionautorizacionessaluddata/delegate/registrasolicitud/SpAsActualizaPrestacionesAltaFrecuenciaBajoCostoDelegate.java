package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpAsDevolverSevicioSolicitadoDelegate
 * Clase que devuelve un servicio solicitado
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpAsActualizaPrestacionesAltaFrecuenciaBajoCostoDelegate implements SQLDelegate{
	
	/** Consecutivo Solicitud **/
	private Integer conSolicitud;
	/** Usuario**/
	private String usuario;
	
	private boolean respuesta = false;
	/**
	 * 
	 * @param conServicio
	 * @param codPlan
	 * @param usuario
	 */
	public SpAsActualizaPrestacionesAltaFrecuenciaBajoCostoDelegate(Integer conSolicitud, String usuario) {
		super();
		this.conSolicitud = conSolicitud;
		this.usuario = usuario;
	}

		
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.ACTUALIZA_ESTADO_PRESTACION_ALTA_FRECUENCIA));
		statement.setInt(1, this.conSolicitud);
		statement.setString(2, this.usuario);
		
		statement.registerOutParameter(3, java.sql.Types.INTEGER);
		statement.registerOutParameter(4, java.sql.Types.VARCHAR);

		statement.execute();

		Integer resultado = statement.getInt(3);
			
		if(resultado == 0){
			respuesta = false;
		}else{
			respuesta = true;
		}

	}


	public boolean getRespuesta() {
		return respuesta;
	}

}
