package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASValidarPerfilCordinadorDelegate
 * Clase Delegate que verifica si el usuario es perfil cordinador
 * @author ing. Julian Hernandez
 * @version 13/01/2017
 *
 */
public class SpASValidarPerfilCordinadorDelegate implements SQLDelegate{
	
	/** Usr del sistema */
	private String usuario;
	
	/**
	 * Marca si el usr es perfil cordinador
	 */
	private boolean esPerfilCordinador;

	
	/**
	 * 
	 * @param conServicio
	 */
	public SpASValidarPerfilCordinadorDelegate(String usuario) {
		super();
		this.usuario = usuario;
	}

	/**
	 * Si devuelve algun valor significa que el servicio esta marcado como acceso directo
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
			
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.VALIDAR_PERFIL_CORDINADOR));
		stmt.setString(1, usuario);
		
		ResultSet result = stmt.executeQuery();
		
		if(result.next()){
			esPerfilCordinador = true;
		}		
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getEsPerfilCordinador() {
		return esPerfilCordinador;
	}
}
