package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.TipoIPSVO;

/**
 * Class SpMNSeleccionarTipoIpsDelegate
 * Clase Delegate que consulta los tipos de ips
 * @author ing. Victor Hugo Gil Ramos
 * @version 06/01/2015
 *
 */
public class SpMNSeleccionarTipoIpsDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los tipos de ips */
	private List<TipoIPSVO> lTiposIPS;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_TIPOS_IPS"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_ips = result.findColumn("cnsctvo_cdgo_tpo_ips");
		int index_cdgo_tpo_ips         = result.findColumn("cdgo_tpo_ips");
		int index_dscrpcn_tpo_ips      = result.findColumn("dscrpcn_tpo_ips");
		
		lTiposIPS = new ArrayList<TipoIPSVO>();
		
		while(result.next()){
			TipoIPSVO ipsvo = new TipoIPSVO();
			ipsvo.setConsecutivoCodigoTipoIps(result.getInt(index_cnsctvo_cdgo_tpo_ips));
			ipsvo.setCodigoTipoIps(result.getString(index_cdgo_tpo_ips));
			ipsvo.setDescripcionTipoIps(result.getString(index_dscrpcn_tpo_ips));
			
			lTiposIPS.add(ipsvo);
		}		
	}	
	
	public List<TipoIPSVO> getlTiposIPS() {
		return lTiposIPS;
	}
}
