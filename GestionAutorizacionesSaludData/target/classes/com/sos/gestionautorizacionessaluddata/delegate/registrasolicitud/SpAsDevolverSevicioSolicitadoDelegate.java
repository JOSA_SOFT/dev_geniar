package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpAsDevolverSevicioSolicitadoDelegate
 * Clase que devuelve un servicio solicitado
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpAsDevolverSevicioSolicitadoDelegate implements SQLDelegate{
	
	/** Consecutivo Solicitud **/
	private Integer conSolicitud;
	/** Consecutivo servicio **/
	/** Codigo plan **/
	private Integer codPlan;
	/** Usuario**/
	private String usuario;
	
	/**
	 * 
	 * @param conServicio
	 * @param codPlan
	 * @param usuario
	 */
	public SpAsDevolverSevicioSolicitadoDelegate(Integer conSolicitud, Integer codPlan, String usuario) {
		super();
		this.conSolicitud = conSolicitud;
		this.codPlan = codPlan;
		this.usuario = usuario;
	}

		
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.DEVOLVER_SERVICIO_SOLICITADO));
		statement.setInt(1, this.conSolicitud);
		statement.setInt(2, this.codPlan);
		statement.setString(3, this.usuario);
		
		statement.registerOutParameter(4, java.sql.Types.INTEGER);
		statement.registerOutParameter(5, java.sql.Types.VARCHAR);
		statement.execute();

		Integer resultado = statement.getInt(4);
		String  mensaje = statement.getString(5);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}

}
