package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class spAsConsultaResultadoMallaDelegate
 * Clase Delegate que consulta el resultado de la malla para cada solicitud
 * @author ing. Julian Hernandez
 * @version 13/01/2017
 *
 */
public class SpAsConsultaResultadoMallaDelegate implements SQLDelegate{
	
	/** Consecutivo solicitud */
	private Integer conSolicitud;
	
	/**
	 * 
	 */
	private List<InconsistenciasResponseServiceVO> lstInconsistencias;
	
	/**
	 * 
	 * @param conSolicitud
	 */
	public SpAsConsultaResultadoMallaDelegate(Integer conSolicitud) {
		super();
		this.conSolicitud = conSolicitud;
	}

	/**
	 * 
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
			
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_RESULTADO_MALLA));
		stmt.setInt(1, this.conSolicitud);
		
		ResultSet result = stmt.executeQuery();
		int llve_prmra_cncpto_prncpl_vlddo      = result.findColumn("llve_prmra_cncpto_prncpl_vlddo");
		int infrmcn_adcnl_no_cnfrmdd      	 	= result.findColumn("infrmcn_adcnl_no_cnfrmdd");
		int cnsctvo_cdgo_srvco_slctdo       	= result.findColumn("cnsctvo_cdgo_srvco_slctdo");
		int dscrpcn_srvco_slctdo      	 		= result.findColumn("dscrpcn_srvco_slctdo");
		
		lstInconsistencias = new ArrayList<InconsistenciasResponseServiceVO>();
		InconsistenciasResponseServiceVO vo;
		while(result.next()){
			vo = new InconsistenciasResponseServiceVO();
			vo.setConsecutivoPrestacion(new BigInteger(result.getString(llve_prmra_cncpto_prncpl_vlddo)));
			vo.setCodigoPrestacion(result.getString(cnsctvo_cdgo_srvco_slctdo));
			vo.setDescripcionPrestacion(result.getString(dscrpcn_srvco_slctdo));
			vo.setMensaje(result.getString(infrmcn_adcnl_no_cnfrmdd));
			vo.setInformacionAdicional(result.getString(infrmcn_adcnl_no_cnfrmdd));
			lstInconsistencias.add(vo);
		}		
	}
	
	/**
	 * 
	 * @return
	 */
	public List<InconsistenciasResponseServiceVO> getlstInconsistencias() {
		return lstInconsistencias;
	}
}
