package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpConsultaProcedimientosDelegate
 * Clase Delegate que consulta los procedimentos y/o servicios
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/12/2015
 *
 */
public class SpAsConsultaProcedimientosDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los procedimientos */
	private List<ProcedimientosVO> lProcedimientos;
	
	private String codigoCodificacionProcedimiento;
	private String descripcionCodificacionProcedimiento;
	private Date fechaConsulta;
	
	private String codigoTipoIdPrestador;
	private String numeroIdentificacion;
	private String codigoHabilitacion;
	private int consecPlan;
	
	
	public SpAsConsultaProcedimientosDelegate(java.util.Date fechaConsulta, String codigoCodificacionProcedimiento, String descripcionCodificacionProcedimiento, String codigoTipoIdPrestador, String numeroIdentificacion, String codigoHabilitacion, int consecPlan){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoCodificacionProcedimiento = codigoCodificacionProcedimiento;
		this.descripcionCodificacionProcedimiento = descripcionCodificacionProcedimiento;		
		
		this.codigoTipoIdPrestador = codigoTipoIdPrestador;
		this.numeroIdentificacion  = numeroIdentificacion;
		this.codigoHabilitacion    = codigoHabilitacion;
		this.consecPlan = consecPlan;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_PROCEDIMIENTOS"));	
		stmt.setString(1, codigoTipoIdPrestador);
		if(ConstantesData.CADENA_VACIA.equals(this.numeroIdentificacion)){
			stmt.setNull(2, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(2, numeroIdentificacion);
		}
		
		stmt.setString(3, codigoHabilitacion);
		stmt.setDate(4, this.fechaConsulta);
		stmt.setNull(5, java.sql.Types.VARCHAR);
		stmt.setInt(6, ConstantesData.MODELO_PROCEDIMIENTOS);
		
		if(this.consecPlan == 0){
			stmt.setNull(7, java.sql.Types.VARCHAR);
		}else{
			stmt.setInt(7, consecPlan);
		}
		stmt.setNull(8, java.sql.Types.VARCHAR);
		
		if(this.codigoCodificacionProcedimiento == null || ConstantesData.CADENA_VACIA.equals(this.codigoCodificacionProcedimiento)){
		    stmt.setNull(9, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(9, this.codigoCodificacionProcedimiento);
		}
		
		stmt.setNull(10, java.sql.Types.VARCHAR);
		
		if(this.descripcionCodificacionProcedimiento == null || ConstantesData.CADENA_VACIA.equals(this.descripcionCodificacionProcedimiento)){
		    stmt.setNull(11, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(11, this.descripcionCodificacionProcedimiento);
		}
		
	    ResultSet result = stmt.executeQuery();
		
	    int index_cnsctvo_cdfccn           = result.findColumn("cnsctvo_cdfccn");
		int index_cdgo_cdfccn              = result.findColumn("cdgo_cdfccn");
		int index_dscrpcn_cdfccn           = result.findColumn("dscrpcn_cdfccn");
		int index_codigoServicio           = result.findColumn("codigoServicio");
		int index_descripcionServicio      = result.findColumn("descripcionServicio");
		int index_indcdr_no_ps             = result.findColumn("no_pos");
		int index_cnsctvo_cdgo_itm_prspsto = result.findColumn("cnsctvo_cdgo_itm_prspsto");
		int index_estnca                   = result.findColumn("estnca");
		int index_ltrldd                   = result.findColumn("ltrldd");
		
		lProcedimientos = new ArrayList<ProcedimientosVO>();
		
		while(result.next()){
			ProcedimientosVO procedimientosVO = new ProcedimientosVO();
			procedimientosVO.setConsecutivoCodificacionProcedimiento(result.getInt(index_cnsctvo_cdfccn));
			procedimientosVO.setCodigoCodificacionProcedimiento(result.getString(index_cdgo_cdfccn));
			procedimientosVO.setDescripcionCodificacionProcedimiento(result.getString(index_dscrpcn_cdfccn));
			procedimientosVO.setCodigoServicio(result.getString(index_codigoServicio));
			procedimientosVO.setDescripcionServicio(result.getString(index_descripcionServicio));
			String noPos = result.getString(index_indcdr_no_ps)!=null?result.getString(index_indcdr_no_ps).trim():null;
			procedimientosVO.setIndicadorNoPOS("N".equals(noPos)?"S":"N");
			procedimientosVO.setConsecutivoItemPresupuesto(result.getInt(index_cnsctvo_cdgo_itm_prspsto));
			procedimientosVO.setAplicaLaterialidad(result.getString(index_ltrldd));
			procedimientosVO.setEstanciaProcedimiento(result.getString(index_estnca));
			
			lProcedimientos.add(procedimientosVO);
		}		
	}	
	
	public List<ProcedimientosVO> getlProcedimientos() {
		return lProcedimientos;
	}
	
	
}
