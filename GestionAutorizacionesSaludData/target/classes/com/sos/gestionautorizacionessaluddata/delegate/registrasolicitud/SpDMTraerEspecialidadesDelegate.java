package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.EspecialidadVO;

/**
 * Class SpDMTraerEspecialidadesDelegate
 * Clase Delegate que consulta las especialidades del medico
 * @author ing. Victor Hugo Gil Ramos
 * @version 06/01/2015
 *
 */
public class SpDMTraerEspecialidadesDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las especialidades del medico */
	private List<EspecialidadVO> lEspecialidades;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
			
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_ESPECIALIDADES_MEDICO"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_espcldd = result.findColumn("cnsctvo_cdgo_espcldd");
		int index_cdgo_espcldd         = result.findColumn("cdgo_espcldd");
		int index_dscrpcn_espcldd      = result.findColumn("dscrpcn_espcldd");
		
		lEspecialidades = new ArrayList<EspecialidadVO>();
		
		while(result.next()){
			EspecialidadVO especialidadVO = new EspecialidadVO();
			especialidadVO.setConsecutivoCodigoEspecialidad(result.getInt(index_cnsctvo_cdgo_espcldd));
			especialidadVO.setCodigoEspecialidad(result.getString(index_cdgo_espcldd));
			especialidadVO.setDescripcionEspecialidad(result.getString(index_dscrpcn_espcldd));
			
			lEspecialidades.add(especialidadVO);			
		}		
	}
	
	public List<EspecialidadVO> getlEspecialidades() {
		return lEspecialidades;
	}
}
