package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASConsultaMarcaAccesoDirectoDelegate
 * Clase Delegate que consulta si el servicio esta marcado como acceso directo
 * @author ing. Julian Hernandez
 * @version 13/01/2017
 *
 */
public class SpASConsultaMarcaAccesoDirectoDelegate implements SQLDelegate{
	
	/** Consecutivo servicio solicitado */
	private Integer conServicio;
	
	/**
	 * Marca acceso directo
	 */
	private BigDecimal valorLiquidacion;


	/**
	 * 
	 * @param conServicio
	 */
	public SpASConsultaMarcaAccesoDirectoDelegate(Integer conServicio) {
		super();
		this.conServicio = conServicio;
	}

	/**
	 * Si devuelve algun valor significa que el servicio esta marcado como acceso directo
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		valorLiquidacion = new BigDecimal("0");	
		
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_MARCA_ACCESO_DIRECTO));
		stmt.setInt(1, conServicio);
		
		ResultSet result = stmt.executeQuery();
		int idx_vlr_lqdcn      = result.findColumn("vlr_lqdcn");
		if(result.next()){
			valorLiquidacion = valorLiquidacion.add(new BigDecimal(result.getInt(idx_vlr_lqdcn)));
		}
	}
	
	/**
	 * 
	 * @return
	 */
	
	public BigDecimal getValorLiquidacion() {
		return valorLiquidacion;
	}

	

}
