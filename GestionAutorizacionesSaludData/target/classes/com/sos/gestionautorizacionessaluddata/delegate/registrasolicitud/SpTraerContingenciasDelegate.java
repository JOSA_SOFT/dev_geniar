package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciasVO;

/**
 * Class SpTraerContingenciasDelegate
 * Clase Delegate que consulta las contingencias asociadas al diagnostico
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/11/2015
 *
 */
public class SpTraerContingenciasDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las contingencias asociadas al diagnostico */
	private List<ContingenciasVO> lContingencias;
	
	private String visibleUsuario = "S";
	private Date fechaConsulta;
	private int consecutivoCodigoContingencia;
	
	public SpTraerContingenciasDelegate(java.util.Date fechaConsulta, int consecutivoCodigoContingencia){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.consecutivoCodigoContingencia = consecutivoCodigoContingencia;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_CONTINGENCIAS"));
		stmt.setDate(1, this.fechaConsulta);
		stmt.setInt(2, this.consecutivoCodigoContingencia);
		stmt.setString(3, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_cntngnca  = result.findColumn("cnsctvo_cdgo_cntngnca");
		int index_cdgo_cntngnca          = result.findColumn("cdgo_cntngnca");
		int index_dscrpcn_cntngnca       = result.findColumn("dscrpcn_cntngnca");
		int index_aplca_mtrndd           = result.findColumn("aplca_mtrndd");
		
		lContingencias = new ArrayList<ContingenciasVO>();
		
		while(result.next()){
			ContingenciasVO contingenciasVO = new ContingenciasVO();
			contingenciasVO.setConsecutivoCodigoContingencia(result.getInt(index_cnsctvo_cdgo_cntngnca));
			contingenciasVO.setCodigoContingencia(result.getString(index_cdgo_cntngnca));
			contingenciasVO.setDescripcionContingencia(result.getString(index_dscrpcn_cntngnca));
			contingenciasVO.setAplicaMaternidad(result.getString(index_aplca_mtrndd));
			
			lContingencias.add(contingenciasVO);		
		}		
	}
	
	public List<ContingenciasVO> getlContingencias() {
		return lContingencias;
	}
}
