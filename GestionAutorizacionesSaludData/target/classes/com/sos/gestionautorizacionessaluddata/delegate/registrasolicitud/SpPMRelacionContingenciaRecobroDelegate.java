package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;


/**
 * Class SpPMRelacionContingenciaRecobro
 * Clase Delegate que consulta los recobros asociados al diagnostico
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/12/2015
 *
 */
public class SpPMRelacionContingenciaRecobroDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los recobros asociadas a la contingencia */
	private List<ContingenciaRecobroVO> lRelContingenciaRecobros;
	
	private int consecutivoCodigoContingencia;
	
	public SpPMRelacionContingenciaRecobroDelegate(int consecutivoCodigoContingencia){
		this.consecutivoCodigoContingencia = consecutivoCodigoContingencia;	
	}	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_RELACION_CONTINGENCIA_RECOBROS"));
		
		stmt.setInt(1, this.consecutivoCodigoContingencia);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_rcbro = result.findColumn("cnsctvo_cdgo_rcbro");
		int index_dscrpcn_rcbro      = result.findColumn("dscrpcn_rcbro");
		int index_slcta_prgrmcn      = result.findColumn("slcta_prgrmcn");
		int index_aplca_pos          = result.findColumn("aplca_pos");
		
		lRelContingenciaRecobros = new ArrayList<ContingenciaRecobroVO>();
		
		while(result.next()){
			ContingenciaRecobroVO contingenciaRecobroVO = new ContingenciaRecobroVO();
			contingenciaRecobroVO.setConsecutivoCodigoRecobro(result.getInt(index_cnsctvo_cdgo_rcbro));
			contingenciaRecobroVO.setDescripcionRecobro(result.getString(index_dscrpcn_rcbro));
			contingenciaRecobroVO.setSolicitaProgramacion(result.getString(index_slcta_prgrmcn));
			contingenciaRecobroVO.setAplicaPos(result.getString(index_aplca_pos));
			
			lRelContingenciaRecobros.add(contingenciaRecobroVO);			
		}	
	}	
	
	public List<ContingenciaRecobroVO> getlRelContingenciaRecobros() {
		return lRelContingenciaRecobros;
	}
}
