package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASConsultaTiposDocumentoSoporteDelegate
 * Clase delegate que permite consultar los documentos soportes
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/02/2016
 */
public class SpASConsultaTiposDocumentoSoporteDelegate implements  SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	private StringBuilder xml;
	private int modeloDocumentoSoporte;
	
	private List<DocumentoSoporteVO> lDocumentosSoporte;

	public SpASConsultaTiposDocumentoSoporteDelegate(StringBuilder xml, int modeloDocumentoSoporte){
		this.xml = xml;
		this.modeloDocumentoSoporte = modeloDocumentoSoporte;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DOCUMENTOS_SOPORTE));	
		
		stmt.setString(1, this.xml.toString());
		stmt.setInt(2, this.modeloDocumentoSoporte);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_dcmnto_sprte = result.findColumn("cnsctvo_cdgo_dcmnto_sprte");
		int index_cdgo_dcmnto_sprte         = result.findColumn("cdgo_dcmnto_sprte");
		int index_dscrpcn_dcmnto_sprte      = result.findColumn("dscrpcn_dcmnto_sprte");		
		int index_tmno_mxmo                 = result.findColumn("tmno_mxmo");
		int index_url_ejmplo                = result.findColumn("url_ejmplo");
		int index_frmts_prmtdo              = result.findColumn("frmts_prmtdo");
		int index_oblgtro                   = result.findColumn("oblgtro");
		int index_cntdd_mxma_sprts_crgr     = result.findColumn("cntdd_mxma_sprts_crgr");
		int index_cnsctvo_cdgo_tpo_imgn     = result.findColumn("cnsctvo_cdgo_tpo_imgn");
		int index_cdgo_cdfccn               = result.findColumn("cdgo_cdfccn");
		
		lDocumentosSoporte = new ArrayList<DocumentoSoporteVO>();
		
		while(result.next()){
			DocumentoSoporteVO documentosSoporteVO = new DocumentoSoporteVO();
			documentosSoporteVO.setConsecutivoDocumento(result.getLong(index_cnsctvo_cdgo_dcmnto_sprte));
			documentosSoporteVO.setCodigoDocumentoSoporte(result.getString(index_cdgo_dcmnto_sprte));
			documentosSoporteVO.setNombreDocumento(result.getString(index_dscrpcn_dcmnto_sprte));
			documentosSoporteVO.setTamanoMaximo(result.getString(index_tmno_mxmo));
			documentosSoporteVO.setFormatosPermitidos(result.getString(index_frmts_prmtdo));
			documentosSoporteVO.setUrl(result.getString(index_url_ejmplo));
			documentosSoporteVO.setObligatorio(result.getString(index_oblgtro));
			documentosSoporteVO.setCantidadMaximaSoportes(result.getInt(index_cntdd_mxma_sprts_crgr));
			documentosSoporteVO.setTempCantidadMaximaSoportes(result.getInt(index_cntdd_mxma_sprts_crgr));
			documentosSoporteVO.setConsecutivoCodigoTipoAnexo(result.getInt(index_cnsctvo_cdgo_tpo_imgn));
			documentosSoporteVO.setCodigoCodificacionPrestacion(result.getString(index_cdgo_cdfccn));
			
			lDocumentosSoporte.add(documentosSoporteVO);
		}		
	}
	
	public List<DocumentoSoporteVO> getlDocumentosSoporte() {
		return lDocumentosSoporte;
	}
}
