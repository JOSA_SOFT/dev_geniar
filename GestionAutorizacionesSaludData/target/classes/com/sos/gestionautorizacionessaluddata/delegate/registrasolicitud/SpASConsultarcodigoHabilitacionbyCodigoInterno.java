package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultaUbicacionPacienteDelegate
 * Clase delegate que permite el codigo de habilitacion
 * al consecutivo de atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/02/2016
 */
public class SpASConsultarcodigoHabilitacionbyCodigoInterno implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar el codigo de habilitación */	
	private List<String> lCodigoHabilitacion;
	
	private String codigoInterno;
	
	public SpASConsultarcodigoHabilitacionbyCodigoInterno(String codigoInterno){
		this.codigoInterno = codigoInterno;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CODIGO_HABILITACION));
		
		stmt.setString(1, this.codigoInterno);
		
		ResultSet result = stmt.executeQuery();
		
		int cdgo_prstdr_mnstro = result.findColumn("cdgo_prstdr_mnstro");
		
		lCodigoHabilitacion = new ArrayList<String>();
		
		while(result.next()){
			lCodigoHabilitacion.add(result.getString(cdgo_prstdr_mnstro));			
		}		
		
	}
	
	public List<String> getlCodigoHabilitacion() {
		return lCodigoHabilitacion;
	}
}
