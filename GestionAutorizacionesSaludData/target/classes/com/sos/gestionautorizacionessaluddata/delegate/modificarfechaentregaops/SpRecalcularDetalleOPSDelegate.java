package com.sos.gestionautorizacionessaluddata.delegate.modificarfechaentregaops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.TotalResumenOpsVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpRecalcularResumenOPSDelegate Clase delegate que permite recalcular el
 * resumen de las OPS de acuerdo a unos criterios seleccionados por el usuario
 * 
 * @author Ing. Germán Pérez
 * @version 08/11/2017
 *
 */
public class SpRecalcularDetalleOPSDelegate implements SQLDelegate {

	private List<TotalResumenOpsVO> lTotalResumenOps;
	private String listaTotalResumenXML;
	private Double valorBase;
	private Integer prioridad;
	private String usuario;

	public SpRecalcularDetalleOPSDelegate(String listaTotalResumenXML, Double valorBase, Integer prioridad,
			String usuario) {
		this.listaTotalResumenXML = listaTotalResumenXML;
		this.valorBase = valorBase;
		this.prioridad = prioridad;
		this.usuario = usuario;

	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.RECALCULAR_DETALLE_OPS));
		int totalOps = 0;
		Double valorOpsConsultadas = Double.valueOf(0);

		int totalOpsMod = 0;
		Double valorOpsMod = Double.valueOf(0);

		// Valida los parametros que se envian al SP
		if (valorBase > 0) {
			stmt.setNull(1, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(1, this.listaTotalResumenXML);
		}
		stmt.setDouble(2, valorBase);
		stmt.setInt(3, this.prioridad);
		stmt.setString(4, usuario);

		ResultSet result = stmt.executeQuery();

		int descripcionAgrupadorPrestacion = result.findColumn(ConstantesData.DESCRIPCION_AGRUPADOR_PRESTACION);
		int consecutivoAgrupadorPrestacion = result.findColumn(ConstantesData.CONSECUTIVO_AGRUPADOR_PRESTACION);
		int cantidadOpsConsultada = result.findColumn(ConstantesData.CANTIDAD_OPS_CONSULTADAS);
		int valorOpsConsultada = result.findColumn(ConstantesData.VALOR_OPS_CONSULTADAS);
		int porcentajeCantidadOpsConsultada = result.findColumn(ConstantesData.PORCENTAJE_CANTIDAD_OPS_CONSULTADA);
		int porcentajeValorOpsConsultada = result.findColumn(ConstantesData.PORCENTAJE_VALOR_OPS_CONSULTADA);
		int porcentajeCantidadOpsModificada = result.findColumn(ConstantesData.PORCENTAJE_CANTIDAD_OPS_MODIFICADA);
		int porcentajeValorOpsModificada = result.findColumn(ConstantesData.PORCENTAJE_VALOR_OPS_MODIFICADA);
		int cantidadOPSModificada = result.findColumn(ConstantesData.CANTIDAD_OPS_MODIFICADA);
		int valorOPSModificada = result.findColumn(ConstantesData.VALOR_OPS_MODIFICADAS);
		lTotalResumenOps = new ArrayList<TotalResumenOpsVO>();

		while (result.next()) {
			TotalResumenOpsVO totalResumenOpsVO = new TotalResumenOpsVO();
			totalResumenOpsVO.setConsecutivoAgrupador(result.getString(consecutivoAgrupadorPrestacion));
			totalResumenOpsVO.setDescripcionAgrupador(result.getString(descripcionAgrupadorPrestacion));
			totalResumenOpsVO.setCantidadOpsConsultadas(result.getInt(cantidadOpsConsultada));
			totalResumenOpsVO.setValorOpsConsultadas(result.getDouble(valorOpsConsultada));
			totalResumenOpsVO.setPorcentajeCantidadOpsConsultadas(result.getFloat(porcentajeCantidadOpsConsultada));
			totalResumenOpsVO.setPorcentajeValorOpsConsultadas(result.getFloat(porcentajeValorOpsConsultada));
			totalResumenOpsVO.setPorcentajeCantidadOpsAModificar(result.getFloat(porcentajeCantidadOpsModificada));
			totalResumenOpsVO.setPorcentajeValorOpsAModificar(result.getFloat(porcentajeValorOpsModificada));

			totalResumenOpsVO.setCantidadOpsAModificar(result.getInt(cantidadOPSModificada));
			totalResumenOpsVO.setValorOpsAModificar(result.getDouble(valorOPSModificada));

			totalResumenOpsVO.setValorOpsConsultadasFormato(totalResumenOpsVO.getValorOpsConsultadas().toString());
			totalResumenOpsVO.setValorOpsAModificarFormato(totalResumenOpsVO.getValorOpsAModificar().toString());
			totalOps += totalResumenOpsVO.getCantidadOpsConsultadas();
			valorOpsConsultadas += totalResumenOpsVO.getValorOpsConsultadas();
			totalOpsMod += totalResumenOpsVO.getCantidadOpsAModificar();
			valorOpsMod += totalResumenOpsVO.getValorOpsAModificar();
			lTotalResumenOps.add(totalResumenOpsVO);
		}
		TotalResumenOpsVO totales = construirTotalesResumen(totalOps, valorOpsConsultadas, totalOpsMod, valorOpsMod);
		lTotalResumenOps.add(totales);
		stmt.close();
	}

	public TotalResumenOpsVO construirTotalesResumen(int totalOps, Double valorOpsConsultadas, int totalOpsMod,
			Double valorOpsMod) {
		TotalResumenOpsVO totalResumenOpsVO = new TotalResumenOpsVO();
		totalResumenOpsVO.setDescripcionAgrupador(ConstantesData.TOTAL_LABEL);
		totalResumenOpsVO.setCantidadOpsConsultadas(totalOps);
		totalResumenOpsVO.setValorOpsConsultadas(valorOpsConsultadas);
		totalResumenOpsVO.setPorcentajeValorOpsConsultadas(Float.valueOf(100));
		totalResumenOpsVO.setPorcentajeCantidadOpsConsultadas(Float.valueOf(100));

		totalResumenOpsVO.setCantidadOpsAModificar(totalOpsMod);
		totalResumenOpsVO.setValorOpsAModificar(valorOpsMod);

		Float porcentajeValorModificar = calcularPorcentajeValorModificar(lTotalResumenOps);
		Float porcentajeCantidadModificar = calcularCantidadModificar(lTotalResumenOps);
		totalResumenOpsVO.setPorcentajeValorOpsAModificar(porcentajeValorModificar);
		totalResumenOpsVO.setPorcentajeCantidadOpsAModificar(porcentajeCantidadModificar);

		String valorOpsConsultadasFormato = Utilidades.formatoNumerico(valorOpsConsultadas);
		String valorOpsAModificarFormato = Utilidades.formatoNumerico(valorOpsMod);

		totalResumenOpsVO.setValorOpsAModificarFormato(valorOpsAModificarFormato);
		totalResumenOpsVO.setValorOpsConsultadasFormato(valorOpsConsultadasFormato);

		return totalResumenOpsVO;
	}

	public List<TotalResumenOpsVO> getlTotalResumenOps() {
		return lTotalResumenOps;
	}

	public Float calcularPorcentajeValorModificar(List<TotalResumenOpsVO> coleccionOpsVO) {
		float valor = ConstantesData.VALOR_CERO;
		for (TotalResumenOpsVO t : coleccionOpsVO) {
			valor += t.getPorcentajeValorOpsAModificar();
		}
		return valor;
	}

	public Float calcularCantidadModificar(List<TotalResumenOpsVO> coleccionOpsVO) {
		float valor = ConstantesData.VALOR_CERO;
		for (TotalResumenOpsVO t : coleccionOpsVO) {
			valor += t.getPorcentajeCantidadOpsAModificar();
		}
		return valor;
	}

}
