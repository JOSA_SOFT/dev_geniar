package com.sos.gestionautorizacionessaluddata.delegate.modificarfechaentregaops;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.modificarfechaentregaops.CriteriosConsultaOps;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.TotalResumenOpsVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpConsultaResumenOPSDelegate Clase delegate que permite consultar el
 * resumen de las OPS de acuerdo a unos criterios seleccionados por el usuario
 * 
 * @author Ing. Germán Pérez
 * @version 28/09/2017
 *
 */

public class SpConsultaResumenOPSDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar los planes de salud */
	private List<TotalResumenOpsVO> lTotalResumenOps;
	private CriteriosConsultaOps criterios;

	public SpConsultaResumenOPSDelegate(CriteriosConsultaOps criteriosConsultaOps) {
		criterios = criteriosConsultaOps;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_RESUMEN_OPS));
		int totalOps = 0;
		Double valorOpsConsultadas = Double.valueOf(0);
		if (criterios != null) {
			// Valida los parametros que se envian al SP

			validarCombosMultiples(stmt);

			validacionFechas(stmt);
			if (this.criterios.getIncapacidad() == null) {
				stmt.setNull(3, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(3, this.criterios.getIncapacidad());
			}

			if (this.criterios.getPrestacion() == null) {
				stmt.setNull(11, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(11, this.criterios.getPrestacion());
			}

			if (this.criterios.getEntregaInmediata() == null) {
				stmt.setNull(12, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(12, this.criterios.getEntregaInmediata());
			}

			stmt.setString(13, this.criterios.getUsuario());

			ResultSet result = stmt.executeQuery();

			lTotalResumenOps = new ArrayList<TotalResumenOpsVO>();

			while (result.next()) {
				int consecutivoAgrupadorPrestacion = result.findColumn(ConstantesData.CONSECUTIVO_AGRUPADOR_PRESTACION);
				int descripcionAgrupadorPrestacion = result.findColumn(ConstantesData.DESCRIPCION_AGRUPADOR_PRESTACION);

				int cantidadOpsConsultada = result.findColumn(ConstantesData.CANTIDAD_OPS_CONSULTADAS);
				int valorOpsConsultada = result.findColumn(ConstantesData.VALOR_OPS_CONSULTADAS);
				int cantidadOPSModificada = result.findColumn(ConstantesData.CANTIDAD_OPS_MODIFICADA);
				int valorOPSModificada = result.findColumn(ConstantesData.VALOR_OPS_MODIFICADAS);
				int porcentajeCantidadOpsConsultada = result
						.findColumn(ConstantesData.PORCENTAJE_CANTIDAD_OPS_CONSULTADA);
				int porcentajeValorOpsConsultada = result.findColumn(ConstantesData.PORCENTAJE_VALOR_OPS_CONSULTADA);
				int porcentajeCantidadOpsModificada = result
						.findColumn(ConstantesData.PORCENTAJE_CANTIDAD_OPS_MODIFICADA);
				int porcentajeValorOpsModificada = result.findColumn(ConstantesData.PORCENTAJE_VALOR_OPS_MODIFICADA);

				TotalResumenOpsVO totalResumenOpsVO = new TotalResumenOpsVO();
				totalResumenOpsVO.setValorOpsConsultadasFormato(totalResumenOpsVO.getValorOpsConsultadas().toString());
				totalResumenOpsVO.setValorOpsAModificarFormato(totalResumenOpsVO.getValorOpsAModificar().toString());
				totalResumenOpsVO.setValorOpsConsultadas(result.getDouble(valorOpsConsultada));
				totalResumenOpsVO.setPorcentajeValorOpsConsultadas(result.getFloat(porcentajeValorOpsConsultada));
				totalResumenOpsVO.setPorcentajeCantidadOpsAModificar(result.getFloat(porcentajeCantidadOpsModificada));
				totalResumenOpsVO.setValorOpsAModificar(result.getDouble(valorOPSModificada));
				totalResumenOpsVO.setCantidadOpsConsultadas(result.getInt(cantidadOpsConsultada));
				totalResumenOpsVO.setDescripcionAgrupador(result.getString(descripcionAgrupadorPrestacion));
				totalResumenOpsVO.setPorcentajeCantidadOpsConsultadas(result.getFloat(porcentajeCantidadOpsConsultada));
				totalResumenOpsVO.setPorcentajeValorOpsAModificar(result.getFloat(porcentajeValorOpsModificada));
				totalResumenOpsVO.setCantidadOpsAModificar(result.getInt(cantidadOPSModificada));
				totalResumenOpsVO.setConsecutivoAgrupador(result.getString(consecutivoAgrupadorPrestacion));
				totalOps++;
				valorOpsConsultadas += totalResumenOpsVO.getValorOpsConsultadas();
				lTotalResumenOps.add(totalResumenOpsVO);
			}
			TotalResumenOpsVO totales = construirTotalesResumen(totalOps, valorOpsConsultadas);
			lTotalResumenOps.add(totales);
			stmt.close();
		}
	}

	private void validarCombosMultiples(PreparedStatement stmt) throws SQLException {
		if (this.criterios.getPlanes() == null) {
			stmt.setNull(1, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(1, this.criterios.getPlanes());
		}

		if (this.criterios.getAfiliados() == null) {
			stmt.setNull(2, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(2, this.criterios.getAfiliados());
		}
		if (this.criterios.getEstados() == null) {
			stmt.setNull(8, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(8, this.criterios.getEstados());
		}

		if (this.criterios.getSedes() == null) {
			stmt.setNull(9, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(9, this.criterios.getSedes());
		}

		if (this.criterios.getGruposEntrega() == null) {
			stmt.setNull(10, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(10, this.criterios.getGruposEntrega());
		}
	}

	private void validacionFechas(PreparedStatement stmt) throws SQLException {
		if (this.criterios.getFechaEntregaIni() == null) {
			stmt.setNull(4, java.sql.Types.DATE);
		} else {
			stmt.setDate(4, new Date(this.criterios.getFechaEntregaIni().getTime()));
		}

		if (this.criterios.getFechaEntregaFin() == null) {
			stmt.setNull(5, java.sql.Types.DATE);
		} else {
			stmt.setDate(5, new Date(this.criterios.getFechaEntregaFin().getTime()));
		}

		if (this.criterios.getFechaCreacionIni() == null) {
			stmt.setNull(6, java.sql.Types.DATE);
		} else {
			stmt.setDate(6, new Date(this.criterios.getFechaCreacionIni().getTime()));
		}

		if (this.criterios.getFechaCreacionFin() == null) {
			stmt.setNull(7, java.sql.Types.DATE);
		} else {
			stmt.setDate(7, new Date(this.criterios.getFechaCreacionFin().getTime()));
		}
	}

	public TotalResumenOpsVO construirTotalesResumen(int totalOps, Double valorOpsConsultadas) {

		TotalResumenOpsVO totalResumenOpsVO = new TotalResumenOpsVO();
		String valorOpsConsultadasFormato = Utilidades.formatoNumerico(valorOpsConsultadas);
		String valorOpsAModificarFormato = Utilidades.formatoNumerico(valorOpsConsultadas);
		totalResumenOpsVO.setDescripcionAgrupador(ConstantesData.TOTAL_LABEL);
		totalResumenOpsVO.setCantidadOpsConsultadas(totalOps);
		totalResumenOpsVO.setValorOpsConsultadas(valorOpsConsultadas);
		totalResumenOpsVO.setPorcentajeValorOpsConsultadas(Float.valueOf(100));
		totalResumenOpsVO.setPorcentajeCantidadOpsConsultadas(Float.valueOf(100));
		totalResumenOpsVO.setCantidadOpsAModificar(totalOps);
		totalResumenOpsVO.setValorOpsAModificar(valorOpsConsultadas);
		totalResumenOpsVO.setPorcentajeValorOpsAModificar(Float.valueOf(100));
		totalResumenOpsVO.setPorcentajeCantidadOpsAModificar(Float.valueOf(100));
		totalResumenOpsVO.setValorOpsAModificarFormato(valorOpsAModificarFormato);
		totalResumenOpsVO.setValorOpsConsultadasFormato(valorOpsConsultadasFormato);

		return totalResumenOpsVO;
	}

	public List<TotalResumenOpsVO> getlTotalResumenOps() {
		return lTotalResumenOps;
	}

}
