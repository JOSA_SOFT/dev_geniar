package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoPoblacionalVO;

/**
 * Class SpConsultaGrupoPoblacionalDelegate
 * Clase Delegate que consulta el grupo poblacional del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/12/2015
 *
 */
public class SpConsultaGrupoPoblacionalDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la informacion del grupo poblacional*/	
	private List<GrupoPoblacionalVO> lGrupoPoblacional;
	private int numeroUnicoIdentificacionAfiliado;
	
	public SpConsultaGrupoPoblacionalDelegate(int numeroUnicoIdentificacionAfiliado){
		this.numeroUnicoIdentificacionAfiliado = numeroUnicoIdentificacionAfiliado;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_GRUPOPOBLACIONAL"));
		
		stmt.setInt(1, this.numeroUnicoIdentificacionAfiliado);
		
		ResultSet result = stmt.executeQuery();
			
		int index_cdgo_grpo_pblcnl    = result.findColumn("cdgo_grpo_pblcnl");
		int index_dscrpcn_grpo_pblcnl = result.findColumn("dscrpcn_grpo_pblcnl");
		int index_inco_vgnca          = result.findColumn("inco_vgnca");
		int index_fn_vgnca            = result.findColumn("fn_vgnca");
		
		lGrupoPoblacional = new ArrayList<GrupoPoblacionalVO>();
		
		while(result.next()){
			GrupoPoblacionalVO grupoPoblacionalVO = new GrupoPoblacionalVO();
			grupoPoblacionalVO.setCodigoGrupoPoblacional(result.getString(index_cdgo_grpo_pblcnl));
			grupoPoblacionalVO.setDescripcionGrupoPoblacional(result.getString(index_dscrpcn_grpo_pblcnl));
			grupoPoblacionalVO.setInicioVigencia(new java.util.Date(result.getDate(index_inco_vgnca).getTime()));
			grupoPoblacionalVO.setFinVigencia(new java.util.Date(result.getDate(index_fn_vgnca).getTime()));
			
			lGrupoPoblacional.add(grupoPoblacionalVO);
		}
	}
	
	public List<GrupoPoblacionalVO> getlGrupoPoblacional() {
		return lGrupoPoblacional;
	}	
}
