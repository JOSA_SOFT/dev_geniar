package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoFamiliarVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpPmTraerGrupoFamiliarDelegate
 * Clase Delegate que consulta el grupo familiar del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/12/2015
 *
 */

public class SpPmTraerGrupoFamiliarDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la informacion del grupo familiar*/	
	private List<GrupoFamiliarVO> lGrupoFamiliar;
	
	private int consecutivoTipoIdentificacion;
	private String numeroIdentificacion;

	public SpPmTraerGrupoFamiliarDelegate(int consecutivoTipoIdentificacion, String numeroIdentificacion){
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_GRUPO_FAMILIAR));
		stmt.setInt(1, this.consecutivoTipoIdentificacion);
		stmt.setString(2, this.numeroIdentificacion);
		
	    ResultSet result = stmt.executeQuery();
		
		int index_dscrpcn_pln               = result.findColumn("dscrpcn_pln");
		int index_cdgo_tpo_idntfccn         = result.findColumn("cdgo_tpo_idntfccn");
		int index_nmro_idntfccn             = result.findColumn("nmro_idntfccn");
		int index_nmbre_Afldo               = result.findColumn("Nmbre_Afldo");
		int index_tpo_afldo                 = result.findColumn("tpo_afldo");
		int index_fcha_ncmnto               = result.findColumn("fcha_ncmnto");		
		int index_dscrpcn_prntsco           = result.findColumn("dscrpcn_prntsco");
		int index_nmro_cntrto               = result.findColumn("nmro_cntrto");
		int index_inco_vgnca_bnfcro         = result.findColumn("inco_vgnca_bnfcro");
		int index_fn_vgnca_bnfcro           = result.findColumn("fn_vgnca_bnfcro");
		int index_estdo                     = result.findColumn("estdo");		
		int index_cnsctvo_cdgo_pln          = result.findColumn("cnsctvo_cdgo_pln");
		int index_nmro_unco_idntfccn_bnfcro = result.findColumn("nmro_unco_idntfccn_bnfcro");
		int index_cnsctvo_cdgo_tpo_idntfccn = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int index_cnsctvo_cdgo_prntsco      = result.findColumn("cnsctvo_cdgo_prntsco");
		int index_cnsctvo_cdgo_tpo_afldo    = result.findColumn("cnsctvo_cdgo_tpo_afldo");
		int index_Orgn                      = result.findColumn("Orgn");
		
		lGrupoFamiliar = new ArrayList<GrupoFamiliarVO>();
		
		
		while(result.next()){
			GrupoFamiliarVO grupoFamiliarVO = new GrupoFamiliarVO();
			AfiliadoVO afiliadoVO = new AfiliadoVO();
			TiposIdentificacionVO identificacionVO = new TiposIdentificacionVO();
			PlanVO planVO  = new PlanVO();
			
			identificacionVO.setConsecutivoTipoIdentificacion(result.getInt(index_cnsctvo_cdgo_tpo_idntfccn));
			identificacionVO.setCodigoTipoIdentificacion(result.getString(index_cdgo_tpo_idntfccn));
			
			planVO.setConsectivoPlan(result.getInt(index_cnsctvo_cdgo_pln));
			planVO.setDescripcionPlan(result.getString(index_dscrpcn_pln));
								
			afiliadoVO.setPlan(planVO);
			afiliadoVO.setTipoIdentificacionAfiliado(identificacionVO);
			afiliadoVO.setNombreCompleto(result.getString(index_nmbre_Afldo));
			afiliadoVO.setNumeroIdentificacion(result.getString(index_nmro_idntfccn));
			afiliadoVO.setNumeroUnicoIdentificacion(result.getInt(index_nmro_unco_idntfccn_bnfcro));
			afiliadoVO.setNumeroContrato(result.getString(index_nmro_cntrto));
			afiliadoVO.setFechaNacimiento(new java.util.Date(result.getDate(index_fcha_ncmnto).getTime()));
			afiliadoVO.setConsecutivoTipoAfiliado(result.getInt(index_cnsctvo_cdgo_tpo_afldo));
			afiliadoVO.setDescripcionTipoAfiliado(result.getString(index_tpo_afldo));
			afiliadoVO.setFechaInicioVigencia(new java.util.Date(result.getDate(index_inco_vgnca_bnfcro).getTime()));
			afiliadoVO.setFechaFinVigencia(new java.util.Date(result.getDate(index_fn_vgnca_bnfcro).getTime()));
			afiliadoVO.setConsecutivoParentesco(result.getInt(index_cnsctvo_cdgo_prntsco));
			afiliadoVO.setDescripcionParentesco(result.getString(index_dscrpcn_prntsco));
			afiliadoVO.setDescripcionEstadoAfiliado(result.getString(index_estdo));
			
			grupoFamiliarVO.setAfiliadoGrupoFamiliar(afiliadoVO);
			grupoFamiliarVO.setOrigen(result.getString(index_Orgn));
			lGrupoFamiliar.add(grupoFamiliarVO);			
		}		
	}
	
	public List<GrupoFamiliarVO> getlGrupoFamiliar() {
		return lGrupoFamiliar;
	}
}
