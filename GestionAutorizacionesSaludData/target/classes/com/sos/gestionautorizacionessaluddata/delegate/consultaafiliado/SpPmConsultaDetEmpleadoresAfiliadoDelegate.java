package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ActividadEconomicaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CargoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.DepartamentoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EntidadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TipoCotizanteVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;


/**
 * Class SpPmConsultaDetEmpleadoresAfiliado
 * Clase Delegate que consulta el o los empleadores del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class SpPmConsultaDetEmpleadoresAfiliadoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la informacion del empleador*/	
	private List<EmpleadorVO> lEmpleador;
	
	private int consecutivoCodigoTipoContrato;
	private String numeroContrato;
	private Date fechaConsulta;

	
	public SpPmConsultaDetEmpleadoresAfiliadoDelegate(int consecutivoCodigoTipoContrato, String numeroContrato, java.util.Date fechaConsulta){
		this.consecutivoCodigoTipoContrato = consecutivoCodigoTipoContrato;
		this.numeroContrato = numeroContrato;
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_EMPLEADOR"));	
		stmt.setInt(1, this.consecutivoCodigoTipoContrato);
		stmt.setString(2, this.numeroContrato);
		stmt.setDate(3, this.fechaConsulta);		
		stmt.setNull(4, java.sql.Types.INTEGER);
		stmt.setNull(5, java.sql.Types.INTEGER);
		stmt.setNull(6, java.sql.Types.VARCHAR);			
		stmt.setNull(7, java.sql.Types.VARCHAR);		
		stmt.setNull(8, java.sql.Types.INTEGER);
		stmt.setNull(9, java.sql.Types.VARCHAR);
		stmt.setNull(10, java.sql.Types.DATE);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_idntfccn  = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int index_cdgo_tpo_idntfccn          = result.findColumn("cdgo_tpo_idntfccn");
		int index_nmro_idntfccn              = result.findColumn("nmro_idntfccn");
		int index_idntfccn_cmplta_empldrn    = result.findColumn("idntfccn_cmplta_empldr");
		int index_nmro_unco_idntfccn_aprtnte = result.findColumn("nmro_unco_idntfccn_aprtnte");
		int index_cnsctvo_scrsl              = result.findColumn("cnsctvo_scrsl");
		int index_cnsctvo_prdcto_scrsl       = result.findColumn("cnsctvo_prdcto_scrsl");		
		int index_prncpl                     = result.findColumn("prncpl");		
		int index_cnsctvo_cdgo_tpo_ctznte    = result.findColumn("cnsctvo_cdgo_tpo_ctznte");
		int index_cdgo_tpo_ctznte            = result.findColumn("cdgo_tpo_ctznte");
		int index_dscrpcn_tpo_ctznte         = result.findColumn("dscrpcn_tpo_ctznte");		
		int index_inco_vgnca_cbrnza          = result.findColumn("inco_vgnca_cbrnza");		
		int index_fn_vgnca_cbrnza            = result.findColumn("fn_vgnca_cbrnza");		
		int index_cnsctvo_cdgo_actvdd_ecnmca = result.findColumn("cnsctvo_cdgo_actvdd_ecnmca");
		int index_cdgo_actvdd_ecnmca         = result.findColumn("cdgo_actvdd_ecnmca");
		int index_dscrpcn_actvdd_ecnmca      = result.findColumn("dscrpcn_actvdd_ecnmca");		
		int index_drccn                      = result.findColumn("drccn");
		int index_tlfno                      = result.findColumn("tlfno");
		int index_cnsctvo_cdgo_arp           = result.findColumn("cnsctvo_cdgo_arp");
		int index_cdgo_entdd                 = result.findColumn("cdgo_entdd");
		int index_dscrpcn_entdd              = result.findColumn("dscrpcn_entdd");		
		int index_cnsctvo_cdgo_crgo_empldo   = result.findColumn("cnsctvo_cdgo_crgo_empldo");
		int index_cdgo_crgo                  = result.findColumn("cdgo_crgo");
		int index_dscrpcn_crgo               = result.findColumn("dscrpcn_crgo");		
		int index_cnsctvo_cdgo_cdd           = result.findColumn("cnsctvo_cdgo_crgo_empldo");
		int index_cdgo_cdd                   = result.findColumn("cdgo_cdd");
		int index_dscrpcn_cdd                = result.findColumn("dscrpcn_cdd");
		int index_cnsctvo_cdgo_dprtmnto      = result.findColumn("cnsctvo_cdgo_dprtmnto");
		int index_cdgo_dprtmnto              = result.findColumn("cdgo_dprtmnto");
		int index_dscrpcn_dprtmnto           = result.findColumn("dscrpcn_dprtmnto");
		int index_eml                        = result.findColumn("eml");
							
		lEmpleador = new ArrayList<EmpleadorVO>();
		
		while(result.next()){
			EmpleadorVO empleadorVO = new EmpleadorVO();
			TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			ActividadEconomicaVO actividadEconomicaVO = new ActividadEconomicaVO();
			TipoCotizanteVO cotizanteVO = new TipoCotizanteVO();
			CargoVO cargoVO = new CargoVO();
			CiudadVO ciudadVO = new CiudadVO();
			DepartamentoVO departamentoVO = new DepartamentoVO();
			EntidadVO entidadVO = new EntidadVO();
			
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(index_cnsctvo_cdgo_tpo_idntfccn));
			tiposIdentificacionVO.setCodigoTipoIdentificacion(result.getString(index_cdgo_tpo_idntfccn));
			empleadorVO.setTiposIdentificacionVO(tiposIdentificacionVO);
			
			actividadEconomicaVO.setConsecutivoActividadEconomica(result.getInt(index_cnsctvo_cdgo_actvdd_ecnmca));
			actividadEconomicaVO.setCodigoEntidad(result.getString(index_cdgo_actvdd_ecnmca));
			actividadEconomicaVO.setDescripcionEntidad(result.getString(index_dscrpcn_actvdd_ecnmca));
			empleadorVO.setActividadEconomicaVO(actividadEconomicaVO);
			
			cargoVO.setConsecutivoCodigoCargoEmpleado(result.getInt(index_cnsctvo_cdgo_crgo_empldo));
			cargoVO.setCodigoCargoEmpleado(result.getString(index_cdgo_crgo));
			cargoVO.setDescripcionCargoEmpleado(result.getString(index_dscrpcn_crgo));			
			empleadorVO.setCargoVO(cargoVO);
			
			entidadVO.setConsecutivoCodigoEntidad(result.getInt(index_cnsctvo_cdgo_arp));
			entidadVO.setCodigoEntidad(result.getString(index_cdgo_entdd));
			entidadVO.setDescripcionEntidad(result.getString(index_dscrpcn_entdd));			
			empleadorVO.setEntidadVO(entidadVO);
			
			cotizanteVO.setConsecutivoCodigoTipoCotizante(result.getInt(index_cnsctvo_cdgo_tpo_ctznte));
			cotizanteVO.setCodigoTipoCotizante(result.getString(index_cdgo_tpo_ctznte));
			cotizanteVO.setDescripcionTipoCotizante(result.getString(index_dscrpcn_tpo_ctznte));			
			empleadorVO.setTipoCotizanteVO(cotizanteVO);
			
			ciudadVO.setConsecutivoCodigoCiudad(result.getInt(index_cnsctvo_cdgo_cdd));
			ciudadVO.setCodigoCiudad(result.getString(index_cdgo_cdd));
			ciudadVO.setDescripcionCiudad(result.getString(index_dscrpcn_cdd));
			empleadorVO.setCiudadVO(ciudadVO);
			
			departamentoVO.setConsecutivoCodigoDepartamento(result.getInt(index_cnsctvo_cdgo_dprtmnto));
			departamentoVO.setCodigoDepartamento(result.getString(index_cdgo_dprtmnto));
			departamentoVO.setDescripcionDepartamento(result.getString(index_dscrpcn_dprtmnto));
			empleadorVO.setDepartamentoVO(departamentoVO);			
			
			empleadorVO.setNumeroIdentificacionEmpleador(result.getString(index_nmro_idntfccn));
			empleadorVO.setIdentificacionCompletaEmpleador(result.getString(index_idntfccn_cmplta_empldrn));
			empleadorVO.setNumeroUnicoIdentificacionAportante(result.getInt(index_nmro_unco_idntfccn_aprtnte));
			empleadorVO.setConsecutivoSucursalAportante(result.getInt(index_cnsctvo_scrsl));
			empleadorVO.setEmpleadorPrincipal(result.getString(index_prncpl));
			empleadorVO.setDireccion(result.getString(index_drccn));
			empleadorVO.setTelefono(result.getString(index_tlfno));
			empleadorVO.setEmlEmpleador(result.getString(index_eml));
			empleadorVO.setInicioVigenciaCobranza(new java.util.Date(result.getDate(index_inco_vgnca_cbrnza).getTime()));
			empleadorVO.setFinVigenciaCobranza(new java.util.Date(result.getDate(index_fn_vgnca_cbrnza).getTime()));
			empleadorVO.setConsecutivoProductoSucursal(result.getInt(index_cnsctvo_prdcto_scrsl));
			
			lEmpleador.add(empleadorVO);
		}		
	}
	
	public List<EmpleadorVO> getlEmpleador() {
		return lEmpleador;
	}
}
