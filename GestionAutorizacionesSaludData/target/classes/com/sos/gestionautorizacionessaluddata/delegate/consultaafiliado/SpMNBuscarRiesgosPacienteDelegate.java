package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgosPacienteVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpMNBuscarRiesgosPacienteDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<RiesgosPacienteVO> lRiesgosPaciente;
	
	private int consecutivoTipoIdentificacion;
	private String numeroIdentificacion;
	private Date fechaConsulta;
	
	
	public SpMNBuscarRiesgosPacienteDelegate(int consecutivoTipoIdentificacion, String numeroIdentificacion, java.util.Date fechaConsulta){
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_RIESGOSAFILIADO"));
		stmt.setNull(1, java.sql.Types.INTEGER);
		stmt.setNull(2, java.sql.Types.INTEGER);
		stmt.setInt(3, this.consecutivoTipoIdentificacion);
		stmt.setString(4, this.numeroIdentificacion);
		stmt.setNull(5, java.sql.Types.DATE);
		stmt.setNull(6, java.sql.Types.DATE);
		stmt.setDate(7, this.fechaConsulta);
		
		ResultSet result = stmt.executeQuery();
		 
		int index_dscrpcn_rsgo_dgnstco     = result.findColumn("dscrpcn_rsgo_dgnstco");
		int index_fcha_mrccn               = result.findColumn("fcha_mrccn");
		int index_cnsctvo_ntfccn_ascdo     = result.findColumn("cnsctvo_ntfccn_ascdo");
		int index_cnsctvo_cdgo_ofcna_ascdo = result.findColumn("cnsctvo_cdgo_ofcna_ascdo");
		int index_fcha_fn_mrccn            = result.findColumn("fcha_fn_mrccn");
		int index_dscrpcn_lbrto            = result.findColumn("dscrpcn_lbrto");
		int index_dscrpcn_lbrto_crto       = result.findColumn("dscrpcn_lbrto_crto");
		int index_fnte_mrccn               = result.findColumn("fnte_mrccn");
		int index_modulo                   = result.findColumn("modulo");
		 
		lRiesgosPaciente = new ArrayList<RiesgosPacienteVO>();
		 
		while(result.next()){
			 RiesgosPacienteVO riesgosPacienteVO = new RiesgosPacienteVO();
			 riesgosPacienteVO.setDescripcionRiesgo(result.getString(index_dscrpcn_rsgo_dgnstco));
			 riesgosPacienteVO.setFechaMarcacion(new java.util.Date(result.getDate(index_fcha_mrccn).getTime()));
			 riesgosPacienteVO.setConsecutivoNotificacionAsociado(result.getInt(index_cnsctvo_ntfccn_ascdo));
			 riesgosPacienteVO.setConsecutivoOficinaAsociado(result.getInt(index_cnsctvo_cdgo_ofcna_ascdo));
			 riesgosPacienteVO.setFechaFinMarcacion(new java.util.Date(result.getDate(index_fcha_fn_mrccn).getTime()));
			 riesgosPacienteVO.setDescripcionLibreto(result.getString(index_dscrpcn_lbrto));
			 riesgosPacienteVO.setDescripcionLibretoCorto(result.getString(index_dscrpcn_lbrto_crto));
			 riesgosPacienteVO.setConsecutivoFuenteMarcacion(result.getInt(index_fnte_mrccn));
			 riesgosPacienteVO.setFuenteMarcacion(result.getString(index_modulo));
			 
			 lRiesgosPaciente.add(riesgosPacienteVO);			 
		}		
	}	
	
	public List<RiesgosPacienteVO> getlRiesgosPaciente() {
		return lRiesgosPaciente;
	}

}
