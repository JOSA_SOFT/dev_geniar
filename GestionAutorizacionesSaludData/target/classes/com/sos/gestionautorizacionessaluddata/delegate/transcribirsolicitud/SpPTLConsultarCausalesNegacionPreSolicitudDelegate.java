package com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpPTLConsultarCausalesNegacionPreSolicitudDelegate
 * Clase Consulta las causales de devolucion para la trnscripcion de solicitud
 * @author Rafael Cano
 * @version 20/09/2016
 *
 */
public class SpPTLConsultarCausalesNegacionPreSolicitudDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las causas de la anulacion */
	private List<MotivoCausaVO> lstMotivoCausaVO;
	
	/** Parametro fecha */
	private Date fechaMotivoCausa;
	
	public SpPTLConsultarCausalesNegacionPreSolicitudDelegate(java.util.Date fechaMotivoCausa ){
		this.fechaMotivoCausa = new java.sql.Date(fechaMotivoCausa.getTime());
	}
		
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_MOTIVOS_DEVOLUCION_TRANSCRIPCION));	
		stmt.setDate(1, fechaMotivoCausa);
		
		ResultSet result = stmt.executeQuery();

		int indice_cnsctvo_cdgo_csa_estdo_prstcn_slctd = result.findColumn("cnsctvo_cdgo_csa_estdo_prstcn_slctd");
		int indice_cdgo_csa_estdo_prstcn_slctd= result.findColumn("cdgo_csa_estdo_prstcn_slctd");
		int indice_dscrpcn_csa_estdo_prstcn_slctd= result.findColumn("dscrpcn_csa_estdo_prstcn_slctd");

		lstMotivoCausaVO = new ArrayList<MotivoCausaVO>();
		MotivoCausaVO vo;
		while(result.next()){
			vo = new MotivoCausaVO();
			
			vo.setConsecutivoCodMotivoCausa(result.getInt(indice_cnsctvo_cdgo_csa_estdo_prstcn_slctd));
			vo.setCodMotivoCausa(result.getInt(indice_cdgo_csa_estdo_prstcn_slctd));
			vo.setDesMotivoCausa(result.getString(indice_dscrpcn_csa_estdo_prstcn_slctd));
			
			lstMotivoCausaVO.add(vo);			
		}	

	}
	
	public List<MotivoCausaVO> getListaMotivosTranscribir() {
		return lstMotivoCausaVO;
	}
	
}
