package com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGestionContratacionParametros
 * Clase Delegate que consultar la informacion de los documentos adjuntos
 * @author ing. Rafael Cano
 * @version 08/07/2016
 *
 */
public class SpINFConsultarSoportesPreSolicitudDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar los soportes                                             */	
	private List<SoporteVO> listaDocumentos;
	private int numeroPreSolicitud;
	
	public SpINFConsultarSoportesPreSolicitudDelegate(int numeroPreSolicitud){
		this.numeroPreSolicitud = numeroPreSolicitud;
	}

	
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_SOPORTES_PRESOLICITUD));	
		stmt.setInt(1, this.numeroPreSolicitud);

		
		ResultSet result = stmt.executeQuery();
		
		int indice_nombreSoporte = result.findColumn("nmbre_sprte");
		int indice_rutaFisica = result.findColumn("rta_fsca");
		int indice_consCodigoDocumentoSoporte = result.findColumn("cnsctvo_cdgo_dcmnto_sprte"); 

		listaDocumentos= new ArrayList<SoporteVO>();
				
		while(result.next()){
			SoporteVO documento= new SoporteVO();
			DocumentoSoporteVO documentoSoporteVO= new DocumentoSoporteVO();
			documento.setConsecutivoSoporte(result.getLong(indice_consCodigoDocumentoSoporte));
			documento.setNombreSoporte(result.getString(indice_nombreSoporte));
			documento.setRutaFisica(result.getString(indice_rutaFisica));
			documento.setConsecutivoCodigoDocumentoSoporte(result.getLong(indice_consCodigoDocumentoSoporte));
			
			documentoSoporteVO.setConsecutivoDocumento(result.getLong(indice_consCodigoDocumentoSoporte));
			documentoSoporteVO.setCodigoDocumentoSoporte(result.getString(indice_consCodigoDocumentoSoporte));
			documentoSoporteVO.setNombreDocumento(result.getString(indice_nombreSoporte));
			
			documento.setDocumentoSoporteVO(documentoSoporteVO);
			listaDocumentos.add(documento);
			
		}

	}
	
	public List<SoporteVO> getListaDocumentos() {
		return listaDocumentos;
	}
	
}