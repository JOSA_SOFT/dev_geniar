package com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.DevolucionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpASGuardarInformacionAdicionarPrestacionDelegate
 * Clase guarda la informacion de una nueva prestacion
 * @author Rafael Cano
 * @version 12/07/2016
 *
 */
public class SpAsActualizarDatosDevolucionPresolicitudDelegate implements SQLDelegate{
	
	DevolucionVO devolucion;
	
	public SpAsActualizarDatosDevolucionPresolicitudDelegate(DevolucionVO devolucion){
		this.devolucion = devolucion;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement call = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_DEVOLUCION));
		call.setInt(1, devolucion.getConsPresolicitud());
		call.setInt(2, devolucion.getConsMotivo());
		call.setString(3, Utilidades.validarNullCadena(devolucion.getUsuario()));
		call.setString(4,Utilidades.validarNullCadena(devolucion.getObservacion()));
		call.registerOutParameter(5, java.sql.Types.INTEGER);
		call.registerOutParameter(6, java.sql.Types.VARCHAR);

		call.executeUpdate();

		Integer codResultado = call.getInt(5);
		String msgResultado = call.getString(6);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
