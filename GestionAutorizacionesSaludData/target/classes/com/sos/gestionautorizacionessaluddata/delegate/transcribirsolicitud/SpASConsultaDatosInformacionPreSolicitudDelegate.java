package com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGestionContratacionParametros
 * Clase Delegate que consultar la informacion de la presolicitud
 * @author ing. Rafael Cano
 * @version 08/07/2016
 *
 */
public class SpASConsultaDatosInformacionPreSolicitudDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar la informacion del empleador*/	
	private List<AfiliadoVO> listaAfiliado;
	private int numeroPreSolicitud;
	private String numeroInstancia;
	
	public SpASConsultaDatosInformacionPreSolicitudDelegate(int numeroPreSolicitud, String numeroInstancia){
		this.numeroPreSolicitud = numeroPreSolicitud;
		this.numeroInstancia = numeroInstancia;
	}

	
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESOLICITUD));	
		stmt.setInt(1, this.numeroPreSolicitud);
		stmt.setString(2, this.numeroInstancia);
		
		ResultSet result = stmt.executeQuery();

		int indice_numeroIdentificacion = result.findColumn("nmro_idntfccn");
		int indice_descripcionPlan= result.findColumn("dscrpcn_pln");
		int indice_consPlan = result.findColumn("cnsctvo_cdgo_pln");
		int indice_consCodigoTipoIdentificacion = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int indice_codigoTipoIdentificacion = result.findColumn("cdgo_tpo_idntfccn");
		
		listaAfiliado= new ArrayList<AfiliadoVO>();
				
		AfiliadoVO afiliadoVO = new AfiliadoVO();
		PlanVO planVO = new PlanVO();
		TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
		
		while(result.next()){
			
			tiposIdentificacionVO.setCodigoTipoIdentificacion(result.getString(indice_codigoTipoIdentificacion).trim());
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(indice_consCodigoTipoIdentificacion));
			planVO.setDescripcionPlan(result.getString(indice_descripcionPlan));
			planVO.setConsectivoPlan(result.getInt(indice_consPlan));
			afiliadoVO.setNumeroIdentificacion(result.getString(indice_numeroIdentificacion));
			afiliadoVO.setPlan(planVO);
			afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
			
			listaAfiliado.add(afiliadoVO);
		}

	}
	
	public List<AfiliadoVO> getListaAfiliados() {
		return listaAfiliado;
	}
	
}