package com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGestionContratacionParametros
 * Clase Delegate que consultar la informacion de la presolicitud
 * @author ing. Rafael Cano
 * @version 27/07/2016
 *
 */
public class SpASConsultaTiposDocumentoSoportePresolicitudDelegate implements SQLDelegate{

	private List<SelectItem> listaTipoDocumento;
	private int consTipoDocumento;
	
	public SpASConsultaTiposDocumentoSoportePresolicitudDelegate(int consTipoDocumento){
		this.consTipoDocumento = consTipoDocumento;
	}

	
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_TIPO_DOCUMENTO_SOPORTE));	
		stmt.setInt(1, this.consTipoDocumento);
		
		ResultSet result = stmt.executeQuery();

		int indice_consTipoDocumento = result.findColumn("cnsctvo_cdgo_dcmnto_sprte");
		int indice_descripcionTipoDocumento= result.findColumn("dscrpcn_dcmnto_sprte");
		
		listaTipoDocumento= new ArrayList<SelectItem>();
		
		while(result.next()){
		
			SelectItem item = new SelectItem();
			item.setValue(result.getInt(indice_consTipoDocumento));
			item.setLabel(result.getString(indice_descripcionTipoDocumento));
			
			listaTipoDocumento.add(item);
		}

	}
	
	public List<SelectItem> getListaTipoDocumentos() {
		return listaTipoDocumento;
	}
	
}