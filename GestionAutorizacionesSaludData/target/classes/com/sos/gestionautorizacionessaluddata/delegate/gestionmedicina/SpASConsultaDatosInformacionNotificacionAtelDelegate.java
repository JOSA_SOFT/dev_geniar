package com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.NotificacionesAtelVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultarDatosConsecutivosNotificaciolAtelDelegate
 * Clase Delegate que consulta las Notificaciones ATEL de la pantalla Medicina del trabajo
 * @author ing. Rafael Cano
 * @version 15/02/2016
 *
 */

public class SpASConsultaDatosInformacionNotificacionAtelDelegate implements SQLDelegate{
	
	private int numeroSolicitud;
	private List<NotificacionesAtelVO> listaNotificaciones;
	
	public SpASConsultaDatosInformacionNotificacionAtelDelegate(int numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_NOTIFICACIONES_ATEL_BPM"));	
		stmt.setInt(1, this.numeroSolicitud);
		
		ResultSet result = stmt.executeQuery();
		
		int indice_consTipo = result.findColumn("cnsctvo_cdgo_tpo_dgnstco");
		int indice_tipo = result.findColumn("dscrpcn_tpo_dgnstco_sld");
		int indice_consDiagnostico = result.findColumn("cnsctvo_cdgo_dgnstco");
		int indice_diagnostico = result.findColumn("dscrpcn_dgnstco");
		int indice_consEstado = result.findColumn("cnsctvo_cdgo_estdo_ntfccn");
		int indice_estado = result.findColumn("dscrpcn_estdo_ntfccn");
		int indice_numeroNotificacion = result.findColumn("nmro_Ntfccn");
		int indice_atel = result.findColumn("atl");
		int indice_tipoIdentificacion = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int indice_identificacion = result.findColumn("nmro_idntfccn");
		int indice_numeroUnicoIdentif = result.findColumn("nui_afldo");
		int indice_consecutivoNotif = result.findColumn("cnsctvo_ntfccn");
		
		listaNotificaciones = new ArrayList<NotificacionesAtelVO>();
		
		while(result.next()){
			NotificacionesAtelVO notificacionesAtelDTO = new NotificacionesAtelVO();
			
			notificacionesAtelDTO.setConsTipo(result.getString(indice_consTipo));
			notificacionesAtelDTO.setTipo(result.getString(indice_tipo));
			notificacionesAtelDTO.setConsDiagnostico(result.getString(indice_consDiagnostico));
			notificacionesAtelDTO.setDiagnostico(result.getString(indice_diagnostico));
			notificacionesAtelDTO.setConsEstado(result.getString(indice_consEstado));
			notificacionesAtelDTO.setEstado(result.getString(indice_estado));
			notificacionesAtelDTO.setNumeroNotificacion(result.getString(indice_numeroNotificacion));
			notificacionesAtelDTO.setAtel(result.getString(indice_atel));
			notificacionesAtelDTO.setTipoIdentificacion(result.getString(indice_tipoIdentificacion));
			notificacionesAtelDTO.setIdentificacion(result.getString(indice_identificacion));
			notificacionesAtelDTO.setNumeroUnicoIdentif(result.getString(indice_numeroUnicoIdentif));
			notificacionesAtelDTO.setConsecutivoNotif(result.getString(indice_consecutivoNotif));
		
			listaNotificaciones.add(notificacionesAtelDTO);
		}

	}
	
	public List<NotificacionesAtelVO> getListaNotificaciones() {
		return listaNotificaciones;
	}
}
