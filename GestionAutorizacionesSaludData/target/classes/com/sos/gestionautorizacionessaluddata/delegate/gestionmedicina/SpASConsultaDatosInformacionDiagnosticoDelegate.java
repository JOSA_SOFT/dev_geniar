package com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultarDatosConsecutivosDiagnosticoDelegate
 * Clase Delegate que consultar los Diagnosticos
 * @author ing. Rafael Cano
 * @version 15/02/2016
 *
 */

public class SpASConsultaDatosInformacionDiagnosticoDelegate implements SQLDelegate{
	
	private int numeroSolicitud;
	private List<DiagnosticoDTO> listaDiagnostico;
	
	public SpASConsultaDatosInformacionDiagnosticoDelegate(int numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_DIAGNOSTICOS_BPM"));	
		stmt.setInt(1, this.numeroSolicitud);
		
		ResultSet result = stmt.executeQuery();
		  
		int indice_principal = result.findColumn("dscrpcn_tpo_dgnstco_sld");
		int indice_codigo = result.findColumn("cdgo_dgnstco");
		int indice_descripcion = result.findColumn("dscrpcn_dgnstco");
		int indice_asignadoSOS = result.findColumn("asgnd_ss");
		
		listaDiagnostico = new ArrayList<DiagnosticoDTO>();
		
		while(result.next()){
			DiagnosticoDTO diagnosticosDTO = new DiagnosticoDTO();
			
			diagnosticosDTO.setPrincipal(result.getString(indice_principal));
			diagnosticosDTO.setCodigo(result.getString(indice_codigo));
			diagnosticosDTO.setDescripcion(result.getString(indice_descripcion));
			diagnosticosDTO.setAsignadoSOS(result.getString(indice_asignadoSOS));
		
			listaDiagnostico.add(diagnosticosDTO);
		}

	}
	
	public List<DiagnosticoDTO> getListaDiagnosticos() {
		return listaDiagnostico;
	}
}
