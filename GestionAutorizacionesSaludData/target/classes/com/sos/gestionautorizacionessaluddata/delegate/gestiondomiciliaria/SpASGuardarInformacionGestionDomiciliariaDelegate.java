package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarInformacionGestionDomiciliariaDelegate
 * Clase guarda la informacion correspondiente a la gestion domiciliaria
 * @author Julian Hernandez
 * @version 22/03/2016
 *
 */
public class SpASGuardarInformacionGestionDomiciliariaDelegate implements SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	StringBuilder xml;
	
	public SpASGuardarInformacionGestionDomiciliariaDelegate(StringBuilder xml){
		this.xml = xml;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_GESTION_DOMICILIARIA));
		statement.setString(1, this.xml.toString());
		statement.registerOutParameter(2, java.sql.Types.INTEGER);
		statement.registerOutParameter(3, java.sql.Types.VARCHAR);

		statement.executeUpdate();

		Integer resultado = statement.getInt(2);
		String  mensaje = statement.getString(3);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}

}
