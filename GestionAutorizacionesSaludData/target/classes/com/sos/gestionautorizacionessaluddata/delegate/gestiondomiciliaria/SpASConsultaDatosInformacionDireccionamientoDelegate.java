package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;

/**
 * Class SpASGestionContratacionParametros
 * Clase Sp para consulta del direccionamiento
 * @author ing. Rafael Cano
 * @version 24/02/2016
 *
 */

public class SpASConsultaDatosInformacionDireccionamientoDelegate implements SQLDelegate {
	private int numeroSolicitud;
	private int consPrestacion;
	private DireccionamientoVO direccionamientoDTO;

	public SpASConsultaDatosInformacionDireccionamientoDelegate(int numeroSolicitud, int consPrestacion){
		this.numeroSolicitud = numeroSolicitud;
		this.consPrestacion = consPrestacion;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_DIRECCIONAMIENTO"));	
		stmt.setInt(1, this.numeroSolicitud);
		stmt.setInt(2, this.consPrestacion);


		ResultSet result = stmt.executeQuery();

		int consTipoIdentificacion = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int codigoTipoIdentificacion = result.findColumn("cdgo_tpo_idntfccn");
		int numeroIdentificacion = result.findColumn("nmro_idntfccn");
		int codigoInterno = result.findColumn("cdgo_intrno");
		int sucursal = result.findColumn("nmbre_scrsl");
		int fechaEsperado = result.findColumn("fcha_estmda_entrga");		

		while(result.next()){

			direccionamientoDTO = new DireccionamientoVO();
			direccionamientoDTO.setConsTipoIdentificacion(result.getInt(consTipoIdentificacion));
			direccionamientoDTO.setCodigoTipoIdentificacion(result.getString(codigoTipoIdentificacion));
			direccionamientoDTO.setNumeroIdentificacion(result.getString(numeroIdentificacion));
			direccionamientoDTO.setCodigoInterno(result.getString(codigoInterno));
			direccionamientoDTO.setSucursal(result.getString(sucursal));
			direccionamientoDTO.setFechaEsperado(result.getDate(fechaEsperado));
			break;
		}
	}

	public DireccionamientoVO getDireccionamiento() {
		return direccionamientoDTO;
	}
}
