package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarInformacionCambioFechaDomiDelegate
 * Clase guarda el cambio de fecha direccionamiento de la prestacion
 * @author Julian Hernandez
 * @version 22/03/2016
 *
 */
public class SpASGuardarInformacionCambioFechaDomiDelegate implements SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	StringBuilder xml;
	
	public SpASGuardarInformacionCambioFechaDomiDelegate(StringBuilder xml){
		this.xml = xml;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  callableStatement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_CAMBIO_FECHA_DIRECCIONAMIENTO_PRESTACION));

		callableStatement.setString(1, this.xml.toString());
		callableStatement.registerOutParameter(2, java.sql.Types.INTEGER);
		callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);

		callableStatement.executeUpdate();

		Integer codResultado = callableStatement.getInt(2);
		String msgResultado = callableStatement.getString(3);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
