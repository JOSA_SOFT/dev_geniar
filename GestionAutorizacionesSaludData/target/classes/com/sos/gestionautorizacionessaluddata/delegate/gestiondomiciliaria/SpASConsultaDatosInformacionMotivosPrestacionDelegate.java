package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.MotivoPrestacionVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionMotivosPrestacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<MotivoPrestacionVO> lstMotivoPrestacionVO;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null; 
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_MOTIVOS_PRESTACION")); 
		
		ResultSet result = stmt.executeQuery();
		 
		int cnsctvo_cdgo_mtvo_prstcn     	= result.findColumn("cnsctvo_cdgo_mtvo_prstcn"); 
		int cdgo_mtvo_prstcn   				= result.findColumn("cdgo_mtvo_prstcn");
		int dscrpcn_mtvo_prstcn     		= result.findColumn("dscrpcn_mtvo_prstcn");
		 
		lstMotivoPrestacionVO = new ArrayList<MotivoPrestacionVO>();
		MotivoPrestacionVO obj;
		while(result.next()){ 
			obj = new MotivoPrestacionVO();
			obj.setConsecutivoCodMotivoPrestacion(result.getInt(cnsctvo_cdgo_mtvo_prstcn));
			obj.setCodMotivoPrestacion(result.getString(cdgo_mtvo_prstcn));
			obj.setDesMotivoPrestacion(result.getString(dscrpcn_mtvo_prstcn));
			 
			lstMotivoPrestacionVO.add(obj);			 
		}		
	}	 
	
	public List<MotivoPrestacionVO> getLstMotivoPrestacionVO() {
		return lstMotivoPrestacionVO;
	}

}
