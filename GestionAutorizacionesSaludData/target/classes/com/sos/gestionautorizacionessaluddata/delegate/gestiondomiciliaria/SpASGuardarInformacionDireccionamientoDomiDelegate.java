package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarInformacionCambioFechaDomiDelegate
 * Clase guarda el cambio de fecha direccionamiento de la prestacion
 * @author Julian Hernandez
 * @version 22/03/2016
 *
 */
public class SpASGuardarInformacionDireccionamientoDomiDelegate implements SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	StringBuilder xml;
	
	public SpASGuardarInformacionDireccionamientoDomiDelegate(StringBuilder xml){
		this.xml = xml;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {

		CallableStatement  cStatement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_DIRECCIONAMIENTO_PRESTACION));

		cStatement.setString(1, this.xml.toString());
		cStatement.registerOutParameter(2, java.sql.Types.INTEGER);
		cStatement.registerOutParameter(3, java.sql.Types.VARCHAR);

		cStatement.executeUpdate();

		Integer codResultado = cStatement.getInt(2);
		String msgResultado = cStatement.getString(3);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}

	}

}
