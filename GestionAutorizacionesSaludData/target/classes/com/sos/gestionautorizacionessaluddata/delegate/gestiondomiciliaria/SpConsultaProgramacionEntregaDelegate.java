package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpConsultaProgramacionEntregaDelegate
 * Clase Delegate que consulta la programacion de entrega de la prestacion
 * @author Julian Hernandez
 * @version 31/03/2016
 *
 */
public class SpConsultaProgramacionEntregaDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar la programacion*/	
	private List<ProgramacionEntregaVO> lstProgramacionEntregaVO;
	
	private Integer nui;
	private Integer consecutivoTipoPrestacion;
	private Integer consecutivoPrestacion;
	
	public SpConsultaProgramacionEntregaDelegate(Integer nui, Integer consecutivoTipoPrestacion, Integer consecutivoPrestacion){
		this.nui = nui;
		this.consecutivoTipoPrestacion = consecutivoTipoPrestacion;
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		CallableStatement  stmt = conn.prepareCall(ConsultaSp.getString("CONSULTA_PROGRAMACION_ENTREGA"));
		stmt.setInt(1,  nui); 
		stmt.setDate(2, null);
		stmt.setDate(3, null);
		stmt.setNull(4, java.sql.Types.INTEGER);
		stmt.setNull(5, java.sql.Types.INTEGER);
		stmt.setInt(6, consecutivoTipoPrestacion);
		stmt.setInt(7, consecutivoPrestacion);
		stmt.registerOutParameter(8, java.sql.Types.VARCHAR);
		stmt.setString(9, ConstantesData.S);
		
		ResultSet result = stmt.executeQuery();
		
		int cnsctvo_cdgo_det_prgrmcn_fcha_evnto     = result.findColumn("cnsctvo_cdgo_det_prgrmcn_fcha_evnto");
		int fcha_entrga  							= result.findColumn("fcha_entrga");
		int cntdd     								= result.findColumn("cntdd");
		int prvdr  									= result.findColumn("prvdr");
		int dscrpcn_prstcn     						= result.findColumn("dscrpcn_prstcn");
		 
		lstProgramacionEntregaVO = new ArrayList<ProgramacionEntregaVO>();
		ProgramacionEntregaVO obj;
		while(result.next()){ 
			obj = new ProgramacionEntregaVO();
			obj.setConsCodProgramacionFecEvento(result.getInt(cnsctvo_cdgo_det_prgrmcn_fcha_evnto));
			obj.setFechaEntrega(result.getDate(fcha_entrga));
			obj.setCantidad(result.getInt(cntdd));
			obj.setProveedor(result.getString(prvdr));
			obj.setPrestacion(result.getString(dscrpcn_prstcn));
			lstProgramacionEntregaVO.add(obj);			 
		}		
	}	 
	
	public List<ProgramacionEntregaVO> getLstProgramacionEntregaVO() {
		return lstProgramacionEntregaVO;
	}

}
