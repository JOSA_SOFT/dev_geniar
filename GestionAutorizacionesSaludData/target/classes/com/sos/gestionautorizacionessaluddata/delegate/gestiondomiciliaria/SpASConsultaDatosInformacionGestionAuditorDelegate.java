package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.GestionesPrestacionVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionGestionAuditorDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<GestionesPrestacionVO> lstGestionesPrestacionVO;
	
	private Integer consecutivoPrestacion;
	
	public SpASConsultaDatosInformacionGestionAuditorDelegate(Integer consecutivoPrestacion){
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_GESTIONES_PRESTACION"));
		stmt.setInt(1, this.consecutivoPrestacion);
		
		ResultSet result = stmt.executeQuery();
		 
		int usuario     		= result.findColumn("usro_lgn_adtr");
		int tipoAuditoria   	= result.findColumn("dscrpcn_tpo_adtra");
		int estado     			= result.findColumn("dscrpcn_estdo_no_cnfrmdd");
		int observacion   		= result.findColumn("obsrvcn_gstn_adtr");
		int causales     		= result.findColumn("dscrpcn_no_cnfrmdd_vldcn");
		 
		lstGestionesPrestacionVO = new ArrayList<GestionesPrestacionVO>();
		GestionesPrestacionVO obj;
		while(result.next()){ 
			obj = new GestionesPrestacionVO();
			obj.setDescUsuario(result.getString(usuario));
			obj.setDescTipoAuditoria(result.getString(tipoAuditoria));
			obj.setDescEstado(result.getString(estado));
			obj.setObservacion(result.getString(observacion));
			obj.setDescCausales(result.getString(causales));
			 
			lstGestionesPrestacionVO.add(obj);			 
		}		
	}	 
	
	public List<GestionesPrestacionVO> getLstGestionesPrestacionVO() {
		return lstGestionesPrestacionVO;
	}

}
