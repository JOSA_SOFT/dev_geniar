package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;

/**
 * Class SpASConsultarDatosAlternativasAuditorDelegate
 * Clase Sp para consulta de las alternativas
 * @author ing. Rafael Cano
 * @version 02/03/2016
 *
 */

public class SpASConsultaDatosInformacionAlternativasAuditorDelegate implements SQLDelegate {
	private List<AlternativasVO> listaAlternativas;
	private int prestacion;
	
	public SpASConsultaDatosInformacionAlternativasAuditorDelegate(int prestacion){
		this.prestacion = prestacion;	
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_ALTERNATIVAS"));
		stmt.setInt(1, this.prestacion);
		
		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_rsltdo_altrntva_gstn");
		int indice_descripcion = result.findColumn("altrntva");

		listaAlternativas = new ArrayList<AlternativasVO>();
		AlternativasVO alternativa;
		while(result.next()){
			alternativa = new AlternativasVO();
			alternativa.setCodigoAlternativa(result.getString(indice_consecutivo));
			alternativa.setAlternativa(result.getString(indice_descripcion));
			
			listaAlternativas.add(alternativa);
		}

	}


	public List<AlternativasVO> getListaAlternativas() {
		return listaAlternativas;
	}

}
