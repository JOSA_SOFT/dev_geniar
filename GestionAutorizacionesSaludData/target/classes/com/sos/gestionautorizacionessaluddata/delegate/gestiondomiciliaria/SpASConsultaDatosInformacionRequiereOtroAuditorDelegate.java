package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.GrupoAuditorVO;


/**
 * Class SpASConsultaDatosInformacionRequiereOtroAuditorDelegate
 * Clase Delegate que consulta los grupos auditores
 * @author Julian Hernandez
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionRequiereOtroAuditorDelegate implements SQLDelegate{

	private List<GrupoAuditorVO> lstGrupoAuditorVO;

	private Integer numeroSolitud;
	private Integer consegutivoGrupoAuditor;
	
	public SpASConsultaDatosInformacionRequiereOtroAuditorDelegate(Integer numeroSolitud, Integer consegutivoGrupoAuditor){
		this.numeroSolitud = numeroSolitud;
		this.consegutivoGrupoAuditor = consegutivoGrupoAuditor;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_GRUPOS_AUDITORES"));
		stmt.setInt(1,  this.numeroSolitud);
		stmt.setInt(2,  this.consegutivoGrupoAuditor); 

		ResultSet result = stmt.executeQuery();

		int cnsctvo_cdgo_tpo_adtr_rsgo_afldo  		= result.findColumn("cnsctvo_cdgo_tpo_adtr_rsgo_afldo");
		int dscrpcn_grpo_adtr_prcso     			= result.findColumn("dscrpcn_grpo_adtr_prcso");

		lstGrupoAuditorVO = new ArrayList<GrupoAuditorVO>();
		GrupoAuditorVO obj;
		while(result.next()){ 
			obj = new GrupoAuditorVO();
			obj.setCodigoGrupoAuditor(result.getInt(cnsctvo_cdgo_tpo_adtr_rsgo_afldo));
			obj.setDescGrupoAuditor(result.getString(dscrpcn_grpo_adtr_prcso));
			lstGrupoAuditorVO.add(obj);
		}		
	}	 

	public List<GrupoAuditorVO> getLstGrupoAuditorVO() {
		return lstGrupoAuditorVO;
	}

}
