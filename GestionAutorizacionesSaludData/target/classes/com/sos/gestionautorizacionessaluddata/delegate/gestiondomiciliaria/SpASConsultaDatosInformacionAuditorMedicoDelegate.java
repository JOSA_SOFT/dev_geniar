package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.UsuarioVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultaDatosInformacionAuditorMedicoDelegate
 * Clase Delegate para consultar los medicos auditores
 * @author Julian Hernandez
 * @version 11/03/2016
 *
 */
public class SpASConsultaDatosInformacionAuditorMedicoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<UsuarioVO> lstMedicosAuditoresVO;
	
	private String strLogin;

	
	public SpASConsultaDatosInformacionAuditorMedicoDelegate(String strLogin){
		this.strLogin = strLogin;
		
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_MEDICO_AUDITOR));
		stmt.setString(1, this.strLogin);
				
		ResultSet result = stmt.executeQuery();
		 
		int nmbre     		= result.findColumn("nmbre_usro");
		int lgn_usro   		= result.findColumn("lgn_usro");
		int dscrpcn_usro    = result.findColumn("dscrpcn_usro");
		int cnsctvo_adtr    = result.findColumn("cnsctvo_adtr");
		 
		lstMedicosAuditoresVO = new ArrayList<UsuarioVO>();
		
		while(result.next()){ 
			UsuarioVO usuarioVO = new UsuarioVO();
			usuarioVO.setLogin(result.getString(lgn_usro));
			usuarioVO.setDescripcion(result.getString(dscrpcn_usro));
			usuarioVO.setNombre(result.getString(nmbre));
			usuarioVO.setConsecutivo(result.getInt(cnsctvo_adtr));
			 
			lstMedicosAuditoresVO.add(usuarioVO);			 
		}		
	}	 
	
	public List<UsuarioVO> getLstMedicosAuditoresVO() {
		return lstMedicosAuditoresVO;
	}

}
