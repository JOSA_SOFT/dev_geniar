package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarInformacionGestionAuditoriaIntegralDelegate
 * Clase guarda la informacion correspondiente a la gestion auditoria integral
 * @author Julian Hernandez
 * @version 22/03/2016
 *
 */
public class SpASGuardarInformacionGestionAuditoriaIntegralDelegate implements SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	StringBuilder xml;
	
	public SpASGuardarInformacionGestionAuditoriaIntegralDelegate(StringBuilder xml){
		this.xml = xml;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement call = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_GESTION_AUDITORIA_INTEGRAL));
		call.setString(1, this.xml.toString());
		call.registerOutParameter(2, java.sql.Types.INTEGER);
		call.registerOutParameter(3, java.sql.Types.VARCHAR);

		call.executeUpdate();

		Integer codResultado = call.getInt(2);
		String msgResultado = call.getString(3);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
