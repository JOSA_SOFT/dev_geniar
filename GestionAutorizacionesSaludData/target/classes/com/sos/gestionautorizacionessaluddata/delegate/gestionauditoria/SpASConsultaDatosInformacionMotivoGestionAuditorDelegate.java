package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.MotivoPrestacionVO;


/**
 * Class SpASConsultaDatosInformacionMotivoGestionAuditorDelegate
 * Clase Delegate que conuslta los motivos de direccionamiento
 * @author Rafael Cano
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionMotivoGestionAuditorDelegate implements SQLDelegate{

	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<MotivoPrestacionVO> lstMotivoPrestacionVO;
	
	private Integer consecutivoTipoMotivo;
	
	public SpASConsultaDatosInformacionMotivoGestionAuditorDelegate(Integer consecutivoTipoMotivo){
		this.consecutivoTipoMotivo = consecutivoTipoMotivo;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null; 
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_LISTA_MOTIVOS_DIRECCIONAMIENTO")); 
		stmt.setInt(1, this.consecutivoTipoMotivo);
		
		ResultSet result = stmt.executeQuery();
		 
		int cnsctvo_cdgo_mtvo_gstn     	= result.findColumn("cnsctvo_cdgo_mtvo_gstn"); 
		int cdgo_mtvo_gstn   			= result.findColumn("cdgo_mtvo_gstn");
		int dscrpcn_mtvo_gstn     		= result.findColumn("dscrpcn_mtvo_gstn");
		 
		lstMotivoPrestacionVO = new ArrayList<MotivoPrestacionVO>();
		MotivoPrestacionVO obj;
		while(result.next()){ 
			obj = new MotivoPrestacionVO();
			obj.setConsecutivoCodMotivoPrestacion(result.getInt(cnsctvo_cdgo_mtvo_gstn));
			obj.setCodMotivoPrestacion(result.getString(cdgo_mtvo_gstn));
			obj.setDesMotivoPrestacion(result.getString(dscrpcn_mtvo_gstn));
			 
			lstMotivoPrestacionVO.add(obj);			 
		}		
	}	 
	
	public List<MotivoPrestacionVO> getLstMotivoPrestacionVO() {
		return lstMotivoPrestacionVO;
	}

}
