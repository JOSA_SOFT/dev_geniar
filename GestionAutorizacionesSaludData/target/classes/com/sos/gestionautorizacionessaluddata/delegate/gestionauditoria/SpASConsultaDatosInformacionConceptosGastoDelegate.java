package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;


/**
 * Class SpASConsultaDatosInformacionConceptosGastoDelegate
 * Clase Delegate que consulta los conceptos de gastos
 * @author Julian Hernandez
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionConceptosGastoDelegate implements SQLDelegate{

	/** Lista que se utiliza almacenar los conceptos de gastos*/	
	private List<ConceptosGastoVO> lstConceptosGastoVO;

	private Integer consecutivoPrestacion;
	
	public SpASConsultaDatosInformacionConceptosGastoDelegate(Integer consecutivoPrestacion){
		this.consecutivoPrestacion = consecutivoPrestacion;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_CONCEPTO_GASTOS"));
		stmt.setInt(1, this.consecutivoPrestacion);

		ResultSet result = stmt.executeQuery();

		int cdgo_cncpto_gsto     = result.findColumn("cdgo_cncpto_gsto");
		int dscrpcn_cncpto_gsto  = result.findColumn("dscrpcn_cncpto_gsto");

		lstConceptosGastoVO = new ArrayList<ConceptosGastoVO>();
		ConceptosGastoVO obj;
		while(result.next()){ 
			obj = new ConceptosGastoVO();
			obj.setConsecutivoCodigoServicioSolicitado(result.getInt(cdgo_cncpto_gsto));
			obj.setDescripcionConceptoGasto(result.getString(dscrpcn_cncpto_gsto));
			
			lstConceptosGastoVO.add(obj);			 
		}		
	}	 

	public List<ConceptosGastoVO> getLstConceptosGastoVO() {
		return lstConceptosGastoVO;
	}

}
