package com.sos.gestionautorizacionessaluddata.delegate.gestioninconsistencias;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;



/**
 * Class SpASGestionContratacionParametros
 * Clase Delegate que consulta el afiliado y la solicitud
 * @author ing. Rafael Cano
 * @version 08/02/2016
 *
 */
public class SpASConsultaDatosSolicitudServicioDelegate implements SQLDelegate{



	private int numeroSolicitud;

	private List<Object> afiliadoYSolicitud = new ArrayList<Object>();

	public SpASConsultaDatosSolicitudServicioDelegate(int numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_AFILIADO));	
		stmt.setInt(1, this.numeroSolicitud);


		ResultSet result = stmt.executeQuery();

		int indice_tipoIdentificacion = result.findColumn("cdgo_tpo_idntfccn");
		int indice_identificacion = result.findColumn("nmro_idntfccn_afldo");
		int indice_nombre = result.findColumn("nmbre");
		int indice_genero = result.findColumn("cdgo_sxo");
		int indice_edad = result.findColumn("edd");
		int indice_tipoAfiliado = result.findColumn("dscrpcn");		
		int indice_estado = result.findColumn("estdo");		
		int indice_semanasCotizacion = result.findColumn("smns_ctzds_antrr_eps");
		int indice_semanasSOS = result.findColumn("smns_ctzds");
		int indice_planAfiliado = result.findColumn("dscrpcn_pln");
		int indice_consecutivoPlanAfiliado = result.findColumn("cnsctvo_cdgo_pln");
		int indice_descPlanComplementario = result.findColumn("dscrpcn_pln_pc");
		int indice_ciudad = result.findColumn("dscrpcn_cdd");
		int indice_descDerecho = result.findColumn("dscrpcn_drcho");
		int indice_altoRiesgo = result.findColumn("alt_rsgo");
		int indice_recienNacido = result.findColumn("rcn_ncdo");
		int indice_nro = result.findColumn("nmro_hjo_afldo");
		int indice_generoRecNac = result.findColumn("cdgo_sxo_rcn_ncdo");
		int indice_fechaConsulta = result.findColumn("fcha_crcn");
		int cnsctvo_cdgo_tpo_idntfccn = result.findColumn("cnsctvo_cdgo_tpo_idntfccn_afldo");
		int fcha_ncmnto_rcn_ncdo = result.findColumn("fcha_ncmnto_rcn_ncdo");
		int nmro_vldcn_espcl = result.findColumn("nmro_vldcn_espcl");
		int nmro_unco_idntfccnr = result.findColumn("nmro_unco_idntfccnr");
		int cnsctvo_cdgo_sde = result.findColumn("sde_afldo");
		int dscrpcn_sde = result.findColumn("dscrpcn_sde");
		
		int indice_numeroSolicitud  = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_fechaCreacion = result.findColumn("fcha_crcn");
		int indice_fechaSolicitud = result.findColumn("fcha_slctd");
		int indice_numeroRadicado = result.findColumn("nmro_slctd_atrzcn_ss");
		int indice_ubicacionPaciente = result.findColumn("dscrpcn_tpo_ubccn_pcnte");
		int indice_prioridad = result.findColumn("dscrpcn_prrdd_atncn");
		int indice_auditorSolicita = result.findColumn("adtr_slctnte");
		int indice_origenSolicita = result.findColumn("cdgo_mdo_cntcto");
		int cnsctvo_cdgo_frma_atncn = result.findColumn("cnsctvo_cdgo_frma_atncn");
		int indice_origenAtencion = result.findColumn("dscrpcn_orgn_atncn");
		int indice_claseAtencionSol = result.findColumn("dscrpcn_clse_atncn");
		int indice_justificacionClinica = result.findColumn("jstfccn_clnca");
		int spra_tpe_st = result.findColumn("spra_tpe_st");
		int dscrpcn_rcbro = result.findColumn("dscrpcn_rcbro");
		int nmro_slctd_prvdr = result.findColumn("nmro_slctd_prvdr");
		int cnsctvo_cdgo_rcbro = result.findColumn("cnsctvo_cdgo_rcbro");
		int obsrvcn_adcnl = result.findColumn("obsrvcn_adcnl");
		int indice_cdgo_tpo_idntfccn = result.findColumn("cdgo_tpo_idntfccn");
		int indce_obsrvcns_rqre_otra_gstn_adtr = result.findColumn("obsrvcns_rqre_otra_gstn_adtr");
		
		AfiliadoVO afiliadoVO = new AfiliadoVO();
		SolicitudBpmVO solicitudVO = new SolicitudBpmVO();

		while(result.next()){

			GeneroVO generoVO = new GeneroVO();
			generoVO.setDescripcionGenero(result.getString(indice_genero));

			PlanVO planVO = new PlanVO();
			planVO.setDescripcionPlan(result.getString(indice_planAfiliado));
			planVO.setConsectivoPlan(result.getInt(indice_consecutivoPlanAfiliado));

			PlanVO planComplementario = new PlanVO();
			planComplementario.setDescripcionPlan(result.getString(indice_descPlanComplementario));

			CiudadVO ciudadVO = new CiudadVO();
			ciudadVO.setDescripcionCiudad(result.getString(indice_ciudad));

			TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			tiposIdentificacionVO.setDescripcionTipoIdentificacion(result.getString(indice_tipoIdentificacion));
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(cnsctvo_cdgo_tpo_idntfccn));
			tiposIdentificacionVO.setCodigoTipoIdentificacion(result.getString(indice_cdgo_tpo_idntfccn));
			
			/**Se Agrega Afiliado**/
			afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
			afiliadoVO.setNumeroIdentificacion(result.getString(indice_identificacion));
			afiliadoVO.setNombreCompleto(result.getString(indice_nombre));
			afiliadoVO.setGenero(generoVO);
			afiliadoVO.setEdadAnos(result.getInt(indice_edad));
			afiliadoVO.setDescripcionTipoAfiliado(result.getString(indice_tipoAfiliado));
			afiliadoVO.setDescripcionEstadoAfiliado(Utilidades.homologarEstado(result.getString(indice_estado)));
			afiliadoVO.setSemanasCotizadas(result.getInt(indice_semanasCotizacion));
			afiliadoVO.setSemanasAfiliacionPOSSOS(result.getInt(indice_semanasSOS));
			afiliadoVO.setPlan(planVO);
			afiliadoVO.setPlanComplementario(planComplementario);
			afiliadoVO.setCiudadResidencia(ciudadVO);
			afiliadoVO.setDescripcionEstadoDerecho(result.getString(indice_descDerecho));
			afiliadoVO.setAltoRiesgo(ConstantesData.SI.equals(result.getString(indice_altoRiesgo))?true:false);
			afiliadoVO.setRecienNacido(	ConstantesData.RECIEN_NACIDO.equals(result.getString(indice_recienNacido)) ||
										ConstantesData.RECIEN_NACIDO_NUMERO.equals(result.getString(indice_recienNacido))?ConstantesData.SI:ConstantesData.NO);
			afiliadoVO.setNro(result.getString(indice_nro));
			afiliadoVO.setGeneroRecNac(result.getString(indice_generoRecNac));
			afiliadoVO.setFechaConsulta(result.getDate(indice_fechaConsulta));
			
			afiliadoVO.setFechaRecienNacido(result.getDate(fcha_ncmnto_rcn_ncdo));
			afiliadoVO.setFechaRecienNacido(validaDate(result.getDate(fcha_ncmnto_rcn_ncdo)));
			
			afiliadoVO.setValEspecial(result.getString(nmro_vldcn_espcl));
			afiliadoVO.setNumeroUnicoIdentificacion(result.getInt(nmro_unco_idntfccnr));
			afiliadoVO.setConsecutivoCodigoSede(result.getInt(cnsctvo_cdgo_sde));
			afiliadoVO.setDescripcionSede(result.getString(dscrpcn_sde));
			
			/**Se Agrega Afiliado**/
			solicitudVO.setNumeroSolicitud(result.getInt(indice_numeroSolicitud));
			solicitudVO.setFechaCreacion(result.getDate(indice_fechaCreacion));
			solicitudVO.setFechaSolicitud(result.getDate(indice_fechaSolicitud));
			solicitudVO.setNumeroRadicado(result.getString(indice_numeroRadicado));
			solicitudVO.setUbicacionPaciente(result.getString(indice_ubicacionPaciente));
			solicitudVO.setPrioridad(result.getString(indice_prioridad));
			solicitudVO.setAuditorSolicita(result.getString(indice_auditorSolicita));
			solicitudVO.setOrigenSolicita(result.getString(indice_origenSolicita));
			solicitudVO.setOrigenAtencion(result.getString(indice_origenAtencion));
			solicitudVO.setClaseAtencion(result.getString(indice_claseAtencionSol));
			solicitudVO.setJustificacionClinica(result.getString(indice_justificacionClinica));
			solicitudVO.setCodigoOrigenAtencion(result.getInt(cnsctvo_cdgo_frma_atncn));
			solicitudVO.setSuperaTopeSoat(Utilidades.convertirStringToSINO(result.getString(spra_tpe_st)));
			solicitudVO.setRecobro(result.getString(dscrpcn_rcbro));
			solicitudVO.setNumeroSolicitudProovedor(result.getString(nmro_slctd_prvdr)!=null && !"".equals(result.getString(nmro_slctd_prvdr).trim())?Integer.parseInt(result.getString(nmro_slctd_prvdr).trim()):null);
			solicitudVO.setConsRecobro(result.getInt(cnsctvo_cdgo_rcbro));
			solicitudVO.setObsSolicitud(result.getString(obsrvcn_adcnl));
			solicitudVO.setObservacionesGestionAuditor(result.getString(indce_obsrvcns_rqre_otra_gstn_adtr)); 
			
			break;
		}		
		afiliadoYSolicitud.add(afiliadoVO);
		afiliadoYSolicitud.add(solicitudVO);

	}
	
	//setea los parametros fecha
	public Date validaDate(java.util.Date fecha) throws SQLException{
		
		Date fechaRecienNacido = null;		
		if(fecha != null){
			fechaRecienNacido = new java.util.Date(fecha.getTime());
		}
		return fechaRecienNacido;		
	}


	public List<Object> getListAfiliadoYSolicitud() {
		return afiliadoYSolicitud;
	}
}