package com.sos.gestionautorizacionessaluddata.delegate.gestioninconsistencias;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;


/**
 * Class SpASGestionContratacionParametros
 * Clase Delegate que consultar las inconsistencias
 * @author ing. Rafael Cano
 * @version 09/02/2016
 *
 */
public class SpASConsultaDatosInformacionInconsistenciasDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar la informacion del empleador*/	
	private List<SelectItem> listaIncosistencias;
	private int numeroSolicitud;
	private int estado;
	
	public SpASConsultaDatosInformacionInconsistenciasDelegate(int numeroSolicitud, int estado){
		this.numeroSolicitud = numeroSolicitud;
		this.estado = estado;
	}

	
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_INCONSISTENCIAS"));	
		stmt.setInt(1, this.numeroSolicitud);
		stmt.setInt(2, this.estado);

		
		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivoInconsistencia = result.findColumn("cnsctvo_dt_prcso_vldcn_ctc");
		int indice_descripcionIncosistencia = result.findColumn("infrmcn_adcnl_no_cnfrmdd");

		listaIncosistencias= new ArrayList<SelectItem>();
				
		while(result.next()){
			SelectItem itemIncosistencia= new SelectItem();
			itemIncosistencia.setValue(result.getInt(indice_consecutivoInconsistencia));
			itemIncosistencia.setDescription(result.getString(indice_descripcionIncosistencia));
			
			listaIncosistencias.add(itemIncosistencia);
		}

	}
	
	public List<SelectItem> getListaInconsistencias() {
		return listaIncosistencias;
	}
	
}