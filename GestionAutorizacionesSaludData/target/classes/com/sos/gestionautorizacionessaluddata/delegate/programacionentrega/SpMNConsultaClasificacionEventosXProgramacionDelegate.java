package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ClasificacionEventoXProgramacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpMNConsultaClasificacionEventosXProgramacionDelegate
 * Clase delegate que Consulta la clasificacion de eventos 
 * @author Julian Hernandez
 * @version 05/07/2016
 */
public class SpMNConsultaClasificacionEventosXProgramacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar Consulta la clasificacion de eventos */
	private List<ClasificacionEventoXProgramacionVO> lstClasificacionEventoXProgramacionVO;		
	
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION));
		
		stmt.setNull(1, java.sql.Types.DATE);
		stmt.setString(2, ConstantesData.S);
		
		lstClasificacionEventoXProgramacionVO = new ArrayList<ClasificacionEventoXProgramacionVO>();
		
		ResultSet result = stmt.executeQuery();
		
		int dscrpcn_clsfccn_evnto         	= result.findColumn("dscrpcn_clsfccn_evnto");		
		int cnsctvo_cdgo_clsfccn_evnto  	= result.findColumn("cnsctvo_cdgo_clsfccn_evnto");
		int cdgo_clsfccn_evnto           	= result.findColumn("cdgo_clsfccn_evnto");
		
		ClasificacionEventoXProgramacionVO vo;
		while(result.next()){
			vo = new ClasificacionEventoXProgramacionVO();
			vo.setDesClasificacionEvento(result.getString(dscrpcn_clsfccn_evnto));
			vo.setConsCodClasificacionEvento(result.getInt(cnsctvo_cdgo_clsfccn_evnto));
			vo.setCodClasificacionEvento(result.getString(cdgo_clsfccn_evnto));
			
			lstClasificacionEventoXProgramacionVO.add(vo);
		}		
	}
	
	/**
	 * respuesta
	 * @return
	 */
	public List<ClasificacionEventoXProgramacionVO> getlstClasificacionEventoXProgramacionVO() {
		return lstClasificacionEventoXProgramacionVO;
	}
}
