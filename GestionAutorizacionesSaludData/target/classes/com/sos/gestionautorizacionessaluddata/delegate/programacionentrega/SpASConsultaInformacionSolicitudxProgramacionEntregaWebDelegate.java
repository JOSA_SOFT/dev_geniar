package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate
 * Clase Delegate que consulta Informacion Solicitud x Programacion
 * @author Julian Hernandez
 * @version 06/07/2016
 *
 */
public class SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate implements SQLDelegate{
	
	/** lista vo resultado **/
	private List<AutorizacionServicioVO> lstAutorizacionServicioVO;
	/** cons Solicitud **/
	private Integer consSolicitud;
	/** cons Prestacion **/
	private Integer consPrestacion;
	
	/**
	 * 
	 * @param consSolicitud
	 * @param consPrestacion
	 */
	public SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate(Integer consSolicitud, Integer consPrestacion){
		this.consSolicitud = consSolicitud;
		this.consPrestacion = consPrestacion;
	}

	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		CallableStatement  stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_SOLICITUD_PROGRAMACION_ENTEGA));
		stmt.setInt(1,  consSolicitud); 
		stmt.setInt(2,  consPrestacion); 
		
		ResultSet result = stmt.executeQuery();
		
		int fcha_utlzcn_hsta  					= result.findColumn("fcha_utlzcn_hsta");
		int cnsctvo_srvco_slctdo     			= result.findColumn("cnsctvo_srvco_slctdo");
		int cnsctvo_cdgo_srvco_slctdo     		= result.findColumn("cnsctvo_cdgo_srvco_slctdo");
		int cnsctvo_slctd_atrzcn_srvco 			= result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int cnsctvo_cdgo_estdo_srvco_slctdo     = result.findColumn("cnsctvo_cdgo_estdo_srvco_slctdo");
		int cdgo_srvco_slctdo  					= result.findColumn("cdgo_srvco_slctdo");
		int nmro_slctd_atrzcn_ss     			= result.findColumn("nmro_slctd_atrzcn_ss");
		int nmro_unco_ops               	 	= result.findColumn("nmro_unco_ops");
		int cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = result.findColumn("cnsctvo_cdgo_estdo_cncpto_srvco_slctdo");
		int vlr_cncpto_cro						= result.findColumn("vlr_cncpto_cro");
		
		AutorizacionServicioVO vo;
		lstAutorizacionServicioVO = new ArrayList<AutorizacionServicioVO>();
		while(result.next()){ 
			vo = new AutorizacionServicioVO();
			vo.setFechaVencimiento(Utilidades.resultStamentDate(result.getDate(fcha_utlzcn_hsta)));
			vo.setConsecutivoServicioSolicitado(result.getInt(cnsctvo_srvco_slctdo));
			vo.setConsEstadoPrestacion(result.getInt(cnsctvo_cdgo_estdo_srvco_slctdo));
			vo.setCodServicioSolicitado(result.getString(cdgo_srvco_slctdo));
			vo.setConsecutivoCodServicioSolicitado(result.getInt(cnsctvo_cdgo_srvco_slctdo));
			vo.setNumRadicadoSolicitud(result.getString(nmro_slctd_atrzcn_ss));
			vo.setNumeroUnicoAutorizacion(result.getInt(nmro_unco_ops));
			vo.setConsecutivoSolicitud(result.getInt(cnsctvo_slctd_atrzcn_srvco));
			vo.setConsecutivoCodigoEstadoOPS(result.getInt(cnsctvo_cdgo_estdo_cncpto_srvco_slctdo));
			
			String v_vlr_cncpto_cro = result.getString(vlr_cncpto_cro)!=null?result.getString(vlr_cncpto_cro):null;
			vo.setValorConceptoCero(ConstantesData.S.equals(v_vlr_cncpto_cro)?true:false);
			lstAutorizacionServicioVO.add(vo);
		}
	}	 
	
	/**
	 * 
	 * @return
	 */
	public List<AutorizacionServicioVO> getlstAutorizacionServicioVO() {
		return lstAutorizacionServicioVO;
	}

}
