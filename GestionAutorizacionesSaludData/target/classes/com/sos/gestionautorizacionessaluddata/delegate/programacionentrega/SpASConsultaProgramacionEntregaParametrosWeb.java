package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ParametrosConsultaProgramacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpConsultaProgramacionEntregaDelegate
 * Clase Delegate que consulta la programacion de entrega BUSQUEDA
 * @author Julian Hernandez
 * @version 06/07/2016
 *
 */
public class SpASConsultaProgramacionEntregaParametrosWeb implements SQLDelegate{

	/** Lista que se utiliza almacenar la programacion*/	
	private List<ProgramacionEntregaVO> lstProgramacionEntregaVO;
	/** Lista que se utiliza almacenar la programacion*/	
	private Integer nui;
	/** fecha Desde*/	
	private Date fechaDesde;
	/** fecha Hasta*/	
	private Date fechaHasta;
	/** consecutivo Codigo Clasificacion Evento*/	
	private Integer consCodClasificacionEvento;
	/** consecutivo Codigo Estado Entrega*/	
	private Integer consCodEstadoEntrega;
	/** consecutivo Codigo Tipo Prestacion*/	
	private Integer consCodTipoPrestacion;
	/** consecutivo Codigo Prestacion*/	
	private Integer consCodPrestacion;
	/** numero de acta*/	
	private String acta;
	/** numero de Autorizacion*/	
	private String numeroAutorizacion;
	/**  parametrosConsulta*/
	ParametrosConsultaProgramacionVO parametrosConsulta;

	/**
	 * 
	 * @param nui
	 * @param fechaDesde
	 * @param fechaHasta
	 * @param consCodClasificacionEvento
	 * @param consCodEstadoEntrega
	 * @param consCodTipoPrestacion
	 * @param consCodPrestacion
	 */
	public SpASConsultaProgramacionEntregaParametrosWeb(Integer nui,  Date fechaDesde, Date fechaHasta, 
			Integer consCodClasificacionEvento, Integer consCodEstadoEntrega, Integer consCodTipoPrestacion, Integer consCodPrestacion){
		this.nui = nui;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.consCodClasificacionEvento = consCodClasificacionEvento;
		this.consCodEstadoEntrega = consCodEstadoEntrega;
		this.consCodTipoPrestacion = consCodTipoPrestacion;
		this.consCodPrestacion = consCodPrestacion;
	}

	
	
	public SpASConsultaProgramacionEntregaParametrosWeb( ParametrosConsultaProgramacionVO parametrosConsulta) {
		this.nui 						= parametrosConsulta.getNumeroUnicoIdentificacion();
		this.fechaDesde 				= parametrosConsulta.getFechaDesde();
		this.fechaHasta 				= parametrosConsulta.getFechaHasta();
		this.consCodClasificacionEvento = parametrosConsulta.getConsCodClasificacionEvento();
		this.consCodEstadoEntrega 		= parametrosConsulta.getConsCodEstadoEntrega();
		this.consCodTipoPrestacion 		= parametrosConsulta.getTipoPrestacionSeleccionado();
		this.consCodPrestacion 			= parametrosConsulta.getConsecutivoPrestacion();
		this.acta 						= parametrosConsulta.getActa();
		this.numeroAutorizacion 		= parametrosConsulta.getNumeroNotificacion();
	}



	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {

		CallableStatement  stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_PROGRAMACION_ENTREGA_PARAMETROS));
		stmt.setInt(1,  nui); 
		
		stmt.setDate(2, new java.sql.Date(fechaDesde.getTime()));
		stmt.setDate(3, new java.sql.Date(fechaHasta.getTime()));
		
		if (consCodClasificacionEvento != null) {
			stmt.setInt(4, consCodClasificacionEvento);
		}else {
			stmt.setNull(4, java.sql.Types.INTEGER);
		}

		if (consCodEstadoEntrega != null) {
			stmt.setInt(5, consCodEstadoEntrega);
		}else {
			stmt.setNull(5, java.sql.Types.INTEGER);
		}

		if (consCodTipoPrestacion != null) {
			stmt.setInt(6, consCodTipoPrestacion);
		}else {
			stmt.setNull(6, java.sql.Types.INTEGER);
		}

		if (consCodPrestacion != null) {
			stmt.setInt(7, consCodPrestacion);
		}else {
			stmt.setNull(7, java.sql.Types.INTEGER);
		}

		if (acta != null && acta.equals(ConstantesData.CADENA_VACIA)==false) {
			stmt.setString(8, acta);
		}else {
			stmt.setNull(8, java.sql.Types.VARCHAR);
		}
		
		if (numeroAutorizacion != null ) {
			stmt.setString(9, numeroAutorizacion);
		}else {
			stmt.setNull(9, java.sql.Types.VARCHAR);
		}
		
		ResultSet result = stmt.executeQuery();

		int fcha_entrga  					= result.findColumn("fcha_entrga");
		int dscrpcn_prstcn     				= result.findColumn("dscrpcn_prstcn");
		int cntdd  							= result.findColumn("cntdd");
		int dscrpcn_estdo_entrga     		= result.findColumn("dscrpcn_estdo_entrga");
		int prvdr  							= result.findColumn("prvdr");
		int dscrpcn_clsfccn_evnto     		= result.findColumn("dscrpcn_clsfccn_evnto");
		int nmro_autrzcn  					= result.findColumn("nmro_autrzcn");
		int usro_mdfccn     				= result.findColumn("usro_mdfccn");
		int cnsctvo_prstcn     				= result.findColumn("cnsctvo_prstcn");
		int cnsctvo_cdgo_estds_entrga     	= result.findColumn("cnsctvo_cdgo_estds_entrga");
		int cnsctvo_cdgo_det_prgrmcn_fcha_evnto = result.findColumn("cnsctvo_cdgo_det_prgrmcn_fcha_evnto");
		int cnsctvo_det_prgrmcn_fcha 			= result.findColumn("cnsctvo_det_prgrmcn_fcha");
		int fcha_fnlzcn						= result.findColumn("fcha_fnlzcn");
		int indce_acta						= result.findColumn("acta");
		int cnsctvo_cdgo_ntfccn				= result.findColumn("cnsctvo_cdgo_ntfccn");
		int cdgo_ofcna						= result.findColumn("cdgo_ofcna");

		lstProgramacionEntregaVO = new ArrayList<ProgramacionEntregaVO>();
		ProgramacionEntregaVO obj;
		int contador = 1;
		while(result.next()){ 
			obj = new ProgramacionEntregaVO();
			obj.setConsProgramacionFecEvento(result.getInt(cnsctvo_det_prgrmcn_fcha));
			obj.setConsCodProgramacionFecEvento(result.getInt(cnsctvo_cdgo_det_prgrmcn_fcha_evnto));
			obj.setFechaEntrega(Utilidades.resultStamentDate(result.getDate(fcha_entrga)));
			obj.setPrestacion(result.getString(dscrpcn_prstcn));
			obj.setCantidad(result.getInt(cntdd));
			obj.setEstadoEntrega(result.getString(dscrpcn_estdo_entrga));
			obj.setProveedor(result.getString(prvdr));
			obj.setEvento(result.getString(dscrpcn_clsfccn_evnto));
			obj.setNumAutorizacion(result.getString(nmro_autrzcn));
			obj.setUsrUltimaMod(result.getString(usro_mdfccn));
			obj.setFechaFinalizacion(Utilidades.resultStamentDate(result.getDate(fcha_fnlzcn)));
			obj.setConsPrestacion(result.getInt(cnsctvo_prstcn));
			obj.setConsEstadoProgramacion(result.getInt(cnsctvo_cdgo_estds_entrga));
			obj.setActa(result.getString(indce_acta));
			obj.setNumeroNotificacion(result.getInt(cnsctvo_cdgo_ntfccn)); 
			obj.setCodigoOficina(result.getString(cdgo_ofcna));
			
			obj.setPar(contador%2==0);
			contador++;
			lstProgramacionEntregaVO.add(obj);			 
		}
	}	 

	/**
	 * 
	 * @return
	 */
	public List<ProgramacionEntregaVO> getLstProgramacionEntregaVO() {
		return lstProgramacionEntregaVO;
	}

}
