package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.EstadoNotificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpMNConsultaEstadoNotificacionDelegate
 * Clase delegate que Devuelve los estados que se le pueden asignar a un evento
 * @author Julian Hernandez
 * @version 05/07/2016
 */
public class SpMNConsultaEstadoNotificacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los estados que se le pueden asignar a un evento*/
	private List<EstadoNotificacionVO> lstEstadoNotificacionVO;		
	
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_ESTADOS_NOTIFICACION));
		
		stmt.setInt(1, ConstantesData.CONS_COD_CLASIFICACION_EVENTO_DEFAULT);	
		stmt.setNull(2, java.sql.Types.DATE);
		stmt.setString(3, ConstantesData.S);
		
		lstEstadoNotificacionVO = new ArrayList<EstadoNotificacionVO>();
		
		ResultSet result = stmt.executeQuery();
		
		
		int dscrpcn         			= result.findColumn("dscrpcn");		
		int cnsctvo_cdgo_estdo_ntfccn  	= result.findColumn("cnsctvo_cdgo_estdo_ntfccn");
		int cdgo_estdo_ntfccn           = result.findColumn("cdgo_estdo_ntfccn");
		
		EstadoNotificacionVO vo;
		while(result.next()){
			vo = new EstadoNotificacionVO();
			vo.setDescripcion(result.getString(dscrpcn));
			vo.setConsCodEstadoNotificacion(result.getInt(cnsctvo_cdgo_estdo_ntfccn));
			vo.setCodEstadoNotificacion(result.getString(cdgo_estdo_ntfccn));
			
			lstEstadoNotificacionVO.add(vo);
		}		
	}
	
	/**
	 * respuesta
	 * @return
	 */
	public List<EstadoNotificacionVO> getlstEstadoNotificacionVO() {
		return lstEstadoNotificacionVO;
	}
}
