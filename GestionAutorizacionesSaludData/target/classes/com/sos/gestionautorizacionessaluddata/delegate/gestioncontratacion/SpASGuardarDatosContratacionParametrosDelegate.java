package com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion;


/**
 * Class SpASGuardarDatosContratacionParametrosDelegate
 * Clase Sp que Guarda la gestion de la prestacion en la pantalla de contratacion
 * @author ing. Rafael Cano
 * @version 08/03/2016
 *
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;




import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASGuardarDatosContratacionParametrosDelegate implements SQLDelegate{
	
	private int numeroSolicitud;
	private int codigoTipoPrestacion;
	private int consecutivoTipoPrestacion;
	private int consecutivoPrestacion;
	private String consecutivoPrestador;
	private String usuario;
	private String observaciones;
	private String nombrePrestacion;
	
	public SpASGuardarDatosContratacionParametrosDelegate(int codigoTipoPrestacion, int numeroSolicitud, int consecutivoTipoPrestacion, int consecutivoPrestacion, String consecutivoPrestador, String usuario, String observaciones, String nombrePrestacion){
		this.numeroSolicitud = numeroSolicitud;
		this.codigoTipoPrestacion = codigoTipoPrestacion;
		this.consecutivoTipoPrestacion = consecutivoTipoPrestacion;
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.consecutivoPrestador = consecutivoPrestador;
		this.usuario = usuario;
		this.observaciones = observaciones;
		this.nombrePrestacion = nombrePrestacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.GUARDAR_GESTION_CONTRATACION));	
		stmt.setInt(1, this.codigoTipoPrestacion);
		stmt.setInt(2, this.numeroSolicitud);
		stmt.setInt(3, this.consecutivoTipoPrestacion);
		stmt.setInt(4, this.consecutivoPrestacion);
		stmt.setString(5, this.consecutivoPrestador);
		stmt.setString(6, this.usuario);  
		stmt.setString(7, this.observaciones);
		stmt.setString(8, this.nombrePrestacion);
		
		stmt.executeUpdate();
	}	
}
