package com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EntidadVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASGestionContratacionParametros
 * Clase Sp para consultar los Empleadores
 * @author ing. Rafael Cano
 * @version 10/02/2016
 *
 */

public class SpASConsultaDatosInformacionEmpleadorDelegate  implements SQLDelegate{
	
	private int numeroSolicitud;
	private List<EmpleadorVO> listaEmpleadores;
	
	public SpASConsultaDatosInformacionEmpleadorDelegate(int numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_EMPLEADORES"));	
		stmt.setInt(1, this.numeroSolicitud);

		
		ResultSet result = stmt.executeQuery();
		  
		int indice_tipoIdentificacion = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int indice_descTipoIdentificacion = result.findColumn("cdgo_tpo_idntfccn");
		int indice_numeroIdentificacion = result.findColumn("nmro_idntfccn");
		int indice_razonSocial = result.findColumn("rzn_scl");
		int indice_arp = result.findColumn("dscrpcn_entdd");		
		
		listaEmpleadores = new ArrayList<EmpleadorVO>();
		
		while(result.next()){
			EmpleadorVO empleadorVO = new EmpleadorVO();
			EntidadVO entidadVO = new EntidadVO();
			TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(indice_tipoIdentificacion));
			tiposIdentificacionVO.setCodigoTipoIdentificacion(result.getString(indice_descTipoIdentificacion));
			empleadorVO.setTiposIdentificacionVO(tiposIdentificacionVO);
			empleadorVO.setNumeroIdentificacionEmpleador(result.getString(indice_numeroIdentificacion));
			empleadorVO.setRazonSocialEmpleador(result.getString(indice_razonSocial));
			entidadVO.setDescripcionEntidad(result.getString(indice_arp));
			empleadorVO.setEntidadVO(entidadVO);
			
			listaEmpleadores.add(empleadorVO);
		}

	}
	
	public List<EmpleadorVO> getListaEmpeladores() {
		return listaEmpleadores;
	}
}
