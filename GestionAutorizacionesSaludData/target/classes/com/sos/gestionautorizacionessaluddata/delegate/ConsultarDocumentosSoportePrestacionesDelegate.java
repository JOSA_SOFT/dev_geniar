package com.sos.gestionautorizacionessaluddata.delegate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que permite consultar los documentos de soporte para una prestacion.
 *
 * @author camoreno
 * @version 1.0 06/11/2015
 */
public class ConsultarDocumentosSoportePrestacionesDelegate implements SQLDelegate {

	/** Documentos consultados. */
	private List<DocumentoSoporteVO> resultado;
	
	/** Identificador de soporte pre solicitud. */
	private Integer consecutivoPrestacion;

	/**
	 * Instantiates a new sp ptl consultar documentos soporte delegate.
	 *
	 * @param consecutivoPrestacion
	 *            the consecutivo doc
	 */
	public ConsultarDocumentosSoportePrestacionesDelegate(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	/**
	 * Ejecutar data.
	 *
	 * @param conn
	 *            the conn
	 * @throws SQLException
	 *             the SQL exception
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {

		PreparedStatement stmt = conn
				.prepareStatement("");
		stmt.setInt(1, consecutivoPrestacion);

		ResultSet rs = stmt.executeQuery();

		int indexCnsctvoCdgoDcmntoSprte = rs
				.findColumn("cnsctvo_cdgo_dcmnto_sprte");
		int indexCdgoDcmntoSprte = rs.findColumn("cdgo_dcmnto_sprte");
		int indexDscrpcnDcmntoSprte = rs.findColumn("dscrpcn_dcmnto_sprte");
		int indexFchaCrcn = rs.findColumn("fcha_crcn");
		int indexUsroCrcn = rs.findColumn("usro_crcn");
		int indexVsbleUsro = rs.findColumn("vsble_usro");
		int indexFrmtsPrmtdo = rs.findColumn("frmts_prmtdo");
		int indexTmnoMaximo = rs.findColumn("tmno_mxmo");
		int indexOblgtro = rs.findColumn("oblgtro");
		int indexUrlEjmplo = rs.findColumn("url_ejmplo");
		int indexCntddMxmaSprtsCrgr = rs
				.findColumn("cntdd_mxma_sprts_crgr");

		resultado = new ArrayList<DocumentoSoporteVO>();

		while (rs.next()) {

			DocumentoSoporteVO documentoSoporteVO = new DocumentoSoporteVO();
			documentoSoporteVO.setConsecutivoDocumento(rs.getLong(indexCnsctvoCdgoDcmntoSprte));
			documentoSoporteVO.setCodigoDocumentoSoporte(rs
					.getString(indexCdgoDcmntoSprte));
			documentoSoporteVO.setNombreDocumento(rs
					.getString(indexDscrpcnDcmntoSprte));
			documentoSoporteVO.setFechaCreacion(rs.getDate(indexFchaCrcn));
			documentoSoporteVO
					.setUsuarioCreacion(rs.getString(indexUsroCrcn));
			documentoSoporteVO
					.setVisibleUsuario(rs.getString(indexVsbleUsro));
			documentoSoporteVO.setFormatosPermitidos(rs
					.getString(indexFrmtsPrmtdo));
			documentoSoporteVO.setTamanoMaximo(rs.getString(indexTmnoMaximo));
			documentoSoporteVO.setObligatorio(rs.getString(indexOblgtro));
			documentoSoporteVO.setUrl(rs.getString(indexUrlEjmplo));
			documentoSoporteVO.setCantidadMaximaSoportes(rs
					.getInt(indexCntddMxmaSprtsCrgr));
			documentoSoporteVO.setTempCantidadMaximaSoportes(rs
					.getInt(indexCntddMxmaSprtsCrgr));

			resultado.add(documentoSoporteVO);
		}
	}

	/**
	 * Este metodo permite retornar el resultado de la consulta.
	 *
	 * @return the resultado
	 */
	public List<DocumentoSoporteVO> getResultado() {
		return resultado;
	}

}
