package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPCupsXRiesgo implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<CupsXRiesgoDTO> lDto;

	private Integer cnsctvoCdfccn;

	public SpBDPCupsXRiesgo(Integer cnsctvoCdfccn) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CUPSXRIESGO));

		stmt.setInt(1, cnsctvoCdfccn);
		ResultSet result = stmt.executeQuery();

		int indiceCodigoRiesgo = result.findColumn("cdgo_rsgo_dgnstco");
		int indiceDescripcionRiesgo = result.findColumn("dscrpcn_rsgo_dgnstco");

		lDto = new ArrayList<CupsXRiesgoDTO>();

		while (result.next()) {

			CupsXRiesgoDTO dto = new CupsXRiesgoDTO();
			dto.setCodigoRiesgo(result.getString(indiceCodigoRiesgo));
			dto.setDescripcionRiesgo(result.getString(indiceDescripcionRiesgo));

			lDto.add(dto);
		}
	}

	public List<CupsXRiesgoDTO> getlDto() {
		return lDto;
	}
}
