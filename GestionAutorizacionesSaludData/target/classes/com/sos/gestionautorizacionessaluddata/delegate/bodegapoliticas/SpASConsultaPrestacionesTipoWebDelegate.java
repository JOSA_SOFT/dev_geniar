package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASConsultaPrestacionesSolicitudWebDelegate Clase delegate que permite
 * consultar las prestaciones relacionadas a una solicitud
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpASConsultaPrestacionesTipoWebDelegate implements SQLDelegate {

	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<PrestacionDTO> lPrestacionesDTO;
	private Integer tipoPrestacion;
	private String codigoPrestacion;
	private String descripcionPrestacion;
	private Integer cupsNorma;

	public SpASConsultaPrestacionesTipoWebDelegate(Integer tipoPrestacion, String codigoPrestacion,
			String descripcionPrestacion, Integer cupsNorma) {
		super();
		this.tipoPrestacion = tipoPrestacion;
		this.codigoPrestacion = codigoPrestacion;
		this.descripcionPrestacion = descripcionPrestacion;
		this.cupsNorma = cupsNorma;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_X_TIPO_PRESTACION));

		if (codigoPrestacion == null){
			stmt.setNull(1, Types.VARCHAR);
		} else {
			stmt.setString(1, codigoPrestacion);
		}
		
		if (descripcionPrestacion == null){
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, descripcionPrestacion);
		}
		
		if (tipoPrestacion == null){
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, this.tipoPrestacion);
		}
		
		if (cupsNorma == null){
			stmt.setNull(4, Types.INTEGER);
		} else {
			stmt.setInt(4, this.cupsNorma);
		}
		
		ResultSet result = stmt.executeQuery();

		int indiceCdgoCdfccn = result.findColumn(ConstantesData.CODIGO_CODIFICACION);
		int indiceDscrpcnPrsntcnMdcmnto = result.findColumn(ConstantesData.DESCRIPCION_CODIFICACION);
		int indiceCnsctvoCdfccn = result.findColumn(ConstantesData.CONSECUTIVO_CODIFICACION);

		lPrestacionesDTO = new ArrayList<PrestacionDTO>();

		while (result.next()) {
			PrestacionDTO prestacionDTO = new PrestacionDTO();
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString(indiceCdgoCdfccn));
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(indiceDscrpcnPrsntcnMdcmnto));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(indiceCnsctvoCdfccn));
			lPrestacionesDTO.add(prestacionDTO);
		}
		stmt.close();
	}

	public List<PrestacionDTO> getlPrestacionesDTO() {
		return lPrestacionesDTO;
	}
}
