package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPConveniosPrestacion implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<ConvenioPrestacionDTO> lPrestacionesDTO;
	private Integer cnsctvoCdgoTpoCdfccn;
	private Integer cnsctvoCdgoPln;
	private Integer cnsctvoCdfccn;
	private Integer cnsctvoCdgoCdd;
	private String cdgoIntrno;

	public SpBDPConveniosPrestacion(Integer cnsctvoCdgoTpoCdfccn, Integer cnsctvoCdgoPln, Integer cnsctvoCdfccn,
			Integer cnsctvoCdgoCdd, String cdgoIntrno) {
		this.cnsctvoCdgoTpoCdfccn = cnsctvoCdgoTpoCdfccn;
		this.cnsctvoCdgoPln = cnsctvoCdgoPln;
		this.cnsctvoCdfccn = cnsctvoCdfccn;
		this.cnsctvoCdgoCdd = cnsctvoCdgoCdd;
		this.cdgoIntrno = cdgoIntrno;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CONVENIO_PRESTACION));

		if (cnsctvoCdgoTpoCdfccn == null)
			stmt.setNull(1, Types.INTEGER);
		else
			stmt.setInt(1, cnsctvoCdgoTpoCdfccn);
		if (cnsctvoCdgoPln == null)
			stmt.setNull(2, Types.INTEGER);
		else
			stmt.setInt(2, cnsctvoCdgoPln);

		if (cnsctvoCdfccn == null)
			stmt.setNull(3, Types.INTEGER);
		else
			stmt.setInt(3, cnsctvoCdfccn);
		if (cnsctvoCdgoCdd == null)
			stmt.setNull(4, Types.INTEGER);
		else
			stmt.setInt(4, cnsctvoCdgoCdd);
		if (cdgoIntrno == null)
			stmt.setNull(5, Types.VARCHAR);
		else
			stmt.setString(5, cdgoIntrno);
		ResultSet result = stmt.executeQuery();

		int indiceIpsCodigo = result.findColumn(ConstantesData.IPS_CODIGO);
		int indiceIpsDescripcion = result.findColumn(ConstantesData.IPS_DESCRIPCION);
		int indiceCiudadDesc = result.findColumn(ConstantesData.CIUDAD_DESCRIPCION);
		int indicePlanDesc = result.findColumn(ConstantesData.PLAN_DESCRIPCION);
		int indiceValor = result.findColumn(ConstantesData.VALOR);
		int indiceTipoAtencion = result.findColumn(ConstantesData.TIPO_ATENCION);
		int indiceformulaLiquidacion = result.findColumn(ConstantesData.FORMULA_LIQUIDACION);
		int indiceAccesoDirecto = result.findColumn(ConstantesData.ACCESO_DIRECTO);
		int indiceDireccion = result.findColumn(ConstantesData.DIRECCION);
		int indiceTelefono = result.findColumn(ConstantesData.TELEFONO);
		int indiceInicioVigencia = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceFinVigencia = result.findColumn(ConstantesData.FIN_VIGENCIA);

		lPrestacionesDTO = new ArrayList<ConvenioPrestacionDTO>();

		while (result.next()) {

			ConvenioPrestacionDTO presentacionDTO = new ConvenioPrestacionDTO();
			presentacionDTO.setIpsCodigo(result.getString(indiceIpsCodigo));
			presentacionDTO.setIpsDescripcion(result.getString(indiceIpsDescripcion));
			presentacionDTO.setCiudadDesc(result.getString(indiceCiudadDesc));
			presentacionDTO.setPlanDesc(result.getString(indicePlanDesc));
			presentacionDTO.setValor(result.getDouble(indiceValor));
			presentacionDTO.setTipoAtencion(result.getString(indiceTipoAtencion));
			presentacionDTO.setFormulaLiquidacion(result.getString(indiceformulaLiquidacion));
			presentacionDTO.setAccesoDirecto(result.getString(indiceAccesoDirecto));
			presentacionDTO.setDireccion(result.getString(indiceDireccion));
			presentacionDTO.setTelefono(result.getString(indiceTelefono));
			presentacionDTO.setInicioVigencia(result.getDate(indiceInicioVigencia));
			presentacionDTO.setFinVigencia(result.getDate(indiceFinVigencia));
			lPrestacionesDTO.add(presentacionDTO);
		}
	}

	public List<ConvenioPrestacionDTO> getlPrestacionesDTO() {
		return lPrestacionesDTO;
	}
}
