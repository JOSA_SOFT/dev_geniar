package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PoliticasAuthPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPPoliticaAuthPrestacionWebDelegate las autorizaciones de
 * prestaciones
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpBDPPoliticaAuthPrestacionWebDelegate implements SQLDelegate {

	private List<PoliticasAuthPrestacionDTO> datosBasicos;
	private Integer codPrestacion;

	public SpBDPPoliticaAuthPrestacionWebDelegate(Integer codPrestacion) {

		this.codPrestacion = codPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_POLIT_AUTH));

		stmt.setInt(1, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indiceDscTipoAuthPres = result.findColumn(ConstantesData.DESCRIPCION_TIPO_AUTH_PRES);
		int indiceDescAgrupador = result.findColumn(ConstantesData.DESCRIPCION_AGRUPADOR);
		int indiceDescPolitica = result.findColumn(ConstantesData.DESCRIPCION_POLITICA);
		datosBasicos = new ArrayList<PoliticasAuthPrestacionDTO>();
		while (result.next()) {

			PoliticasAuthPrestacionDTO datos = new PoliticasAuthPrestacionDTO();

			datos.setTipoPolitica(result.getString(indiceDscTipoAuthPres));
			datos.setAgrupadorPolitica(result.getString(indiceDescAgrupador));
			datos.setDescripcionPolitica(result.getString(indiceDescPolitica));
			if (datos.getDescripcionPolitica().length() > ConstantesData.FIN_SUBSTRING_DESCRIPCION_LARGA)
				datos.setShortDescripcionPolitica(datos.getDescripcionPolitica()
						.substring(ConstantesData.INICIO_SUBSTRING, ConstantesData.FIN_SUBSTRING_DESCRIPCION_LARGA));
			datosBasicos.add(datos);
		}
	}

	public List<PoliticasAuthPrestacionDTO> getDatosBasicos() {
		return datosBasicos;
	}
}
