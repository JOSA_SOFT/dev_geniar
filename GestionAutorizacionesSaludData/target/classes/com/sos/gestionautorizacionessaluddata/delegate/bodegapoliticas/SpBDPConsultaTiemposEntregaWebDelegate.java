package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.TiempoEntregaDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPConsultaTiemposEntregaWebDelegate consulta los tiempos de entrega
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpBDPConsultaTiemposEntregaWebDelegate implements SQLDelegate {

	private List<TiempoEntregaDTO> datosBasicos;

	private Integer codPrestacion;

	public SpBDPConsultaTiemposEntregaWebDelegate(Integer codPrestacion) {

		this.codPrestacion = codPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_TIEMPO_ENTREGA));

		stmt.setInt(1, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indicePlan = result.findColumn(ConstantesData.CODIGO_PLAN);
		int indiceItemDePresupuesto = result.findColumn(ConstantesData.ITEM_PRESUPUESTO);
		int indiceCodigoGrupo = result.findColumn(ConstantesData.CODIGO_GRUPO);
		int indiceDescripcionGrupo = result.findColumn(ConstantesData.DESCRIPCION_GRUPO);
		int indiceTiempodeEntrega = result.findColumn(ConstantesData.TIEMPO_ENTREGA);
		datosBasicos = new ArrayList<TiempoEntregaDTO>();
		while (result.next()) {

			TiempoEntregaDTO datos = new TiempoEntregaDTO();

			datos.setPlan(result.getString(indicePlan));
			datos.setItemDePresupuesto(result.getString(indiceItemDePresupuesto));
			datos.setCodigoGrupo(result.getString(indiceCodigoGrupo));
			datos.setDescripcionGrupo(result.getString(indiceDescripcionGrupo));
			datos.setTiempodeEntrega(result.getInt(indiceTiempodeEntrega));
			datosBasicos.add(datos);
		}
	}

	public List<TiempoEntregaDTO> getDatosBasicos() {
		return datosBasicos;
	}
}
