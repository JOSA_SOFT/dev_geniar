package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.MarcasPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPMarcasPrestacionWebDelegate consultalas marcas de la prestacion
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpBDPMarcasPrestacionWebDelegate implements SQLDelegate {

	private List<MarcasPrestacionDTO> datosBasicos;
	private Integer codPrestacion;

	public SpBDPMarcasPrestacionWebDelegate(Integer codPrestacion) {

		this.codPrestacion = codPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_MARCAS_PRESTACION));

		stmt.setInt(1, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indiceDescripcionMarca = result.findColumn(ConstantesData.DESCRIPCION_TIPO_MARCA);
		int indiceValor = result.findColumn(ConstantesData.VALOR);
		int indiceInicioVigencia = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceFinVigencia = result.findColumn(ConstantesData.FINVIGENCIA);
		datosBasicos = new ArrayList<MarcasPrestacionDTO>();
		while (result.next()) {

			MarcasPrestacionDTO datos = new MarcasPrestacionDTO();

			datos.setDescripcionMarca(result.getString(indiceDescripcionMarca));
			datos.setValor(result.getString(indiceValor));
			datos.setInicioVigencia(result.getDate(indiceInicioVigencia));
			datos.setFinVigencia(result.getDate(indiceFinVigencia));
			datosBasicos.add(datos);
		}
	}

	public List<MarcasPrestacionDTO> getDatosBasicos() {
		return datosBasicos;
	}
}
