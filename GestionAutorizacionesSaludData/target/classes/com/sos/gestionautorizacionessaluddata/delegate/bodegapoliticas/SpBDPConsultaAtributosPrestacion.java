package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.AtributosPrestacionDTO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPConsultaAtributosPrestacion consulta los atributos de la 
 * prestación
 * 
 * @author Daniel Ramirez
 * @version 04/08/2017
 */
public class SpBDPConsultaAtributosPrestacion implements SQLDelegate {

	/**
	 * objeto que se utiliza para almacenar el resultado
	 */
	private List<AtributosPrestacionDTO> datoList;
	private Integer codPrestacion;
	
	public SpBDPConsultaAtributosPrestacion(Integer codPrestacion) {
		this.codPrestacion = codPrestacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_ATRIBUTOS_PRESTACION));

		stmt.setInt(1, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indiceDescripcion = result.findColumn(ConstantesData.DESCRIPCION_TIPO_MARCA_PRESTACION);
		int indiceFechaInicio = result.findColumn(ConstantesData.INICIO_VIGENCIA_MARCA_PRESTACION);
		int indiceFechaFin = result.findColumn(ConstantesData.FIN_VIGENCIA_MARCA_PRESTACION);

		datoList = new ArrayList<AtributosPrestacionDTO>();
		while (result.next()) {

			AtributosPrestacionDTO datos = new AtributosPrestacionDTO();
			datos.setDescripcion(result.getString(indiceDescripcion));
			datos.setInicioVigencia(result.getDate(indiceFechaInicio));
			datos.setFinVigencia(result.getDate(indiceFechaFin));
			
			datoList.add(datos);
		}
		
		stmt.close();
	}

	public List<AtributosPrestacionDTO> getDatos() {
		return datoList;
	}

}
