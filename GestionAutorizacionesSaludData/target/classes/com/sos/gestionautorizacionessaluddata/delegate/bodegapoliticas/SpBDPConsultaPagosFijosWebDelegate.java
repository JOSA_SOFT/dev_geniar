package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PagoFijoDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPConsultaPagosFijosWebDelegate implements SQLDelegate{

	private List<PagoFijoDTO> lDto;
	
	private Integer consecutivoPrestacion;
	private Integer consecutivoCiudad;
	private Integer consecutivoCodigoPlan;
	private String codigoInternoPrestador;
		
	public SpBDPConsultaPagosFijosWebDelegate(Integer consecutivoPrestacion, Integer consecutivoCiudad, Integer consecutivoCodigoPlan,
			String codigoInternoPrestador) {
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.consecutivoCiudad = consecutivoCiudad;
		this.consecutivoCodigoPlan = consecutivoCodigoPlan;
		this.codigoInternoPrestador = codigoInternoPrestador;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PAGOS_FIJOS));

		if (this.consecutivoPrestacion == null) {
			stmt.setNull(1, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(1, consecutivoPrestacion);
		}
		
		if (this.consecutivoCiudad == null) {
			stmt.setNull(2, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(2, consecutivoCiudad);
		}
		
		if (this.consecutivoCodigoPlan == null) {
			stmt.setNull(3, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(3, consecutivoCodigoPlan);
		}
		
		if (this.codigoInternoPrestador == null) {
			stmt.setNull(4, java.sql.Types.VARCHAR);
		} else {
			stmt.setString(4, codigoInternoPrestador);
		}

		ResultSet result = stmt.executeQuery();

		int consecutivoCodigoTipoModulo = result.findColumn(ConstantesData.CONSECUTIVO_CODIGO_TIPO_MODELO);
		int descripcionTipoModulo       = result.findColumn(ConstantesData.DESCRIPCION_TIPO_MODELO);
		int descripcionCiudad           = result.findColumn(ConstantesData.DESCRIPCION_CIUDAD);
		int codigoDiagnostico           = result.findColumn(ConstantesData.CODIGO_DIAGNOSTICO);
		int descripcionDiagnostico      = result.findColumn(ConstantesData.DESCRIPCION_DIAGNOSTICO);
		int inicioVigencia              = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int finVigencia                 = result.findColumn(ConstantesData.FIN_VIGENCIA);
		

		lDto = new ArrayList<PagoFijoDTO>();

		while (result.next()) {

			PagoFijoDTO dto = new PagoFijoDTO();
			dto.setCodigoTipoPago(result.getInt(consecutivoCodigoTipoModulo));
			dto.setDescrTipoPago(result.getString(descripcionTipoModulo));
			dto.setDescrCiudad(result.getString(descripcionCiudad));
			dto.setCodigoDiagnostico(result.getInt(codigoDiagnostico));
			dto.setDescrDiagnostico(result.getString(descripcionDiagnostico));
			dto.setInicioVigencia(result.getDate(inicioVigencia));
			dto.setFinVigencia(result.getDate(finVigencia));
			
			lDto.add(dto);
		}
		
		stmt.close();
	}
	
	public List<PagoFijoDTO> getlDto() {
		return lDto;
	}

	public void setlDto(List<PagoFijoDTO> lDto) {
		this.lDto = lDto;
	}
}