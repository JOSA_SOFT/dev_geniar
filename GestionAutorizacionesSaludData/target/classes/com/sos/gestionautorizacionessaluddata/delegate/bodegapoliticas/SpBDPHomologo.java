package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.HomologacionDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPHomologo implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<HomologacionDTO> lDto;

	private Integer cnsctvoCdfccn;
	private Integer cnsctvoCdd;
	private String cdgoInterno;

	public SpBDPHomologo(Integer cnsctvoCdfccn, Integer cnsctvoCdd, String cdgoInterno) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
		this.cnsctvoCdd = cnsctvoCdd;
		this.cdgoInterno = cdgoInterno;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_HOMOLOGO));

		stmt.setInt(1, cnsctvoCdfccn);
		if (cnsctvoCdd == null)
			stmt.setNull(2, java.sql.Types.INTEGER);
		else
			stmt.setInt(2, cnsctvoCdd);

		if (cdgoInterno == null)
			stmt.setNull(3, java.sql.Types.VARCHAR);
		else
			stmt.setString(3, cdgoInterno);

		ResultSet result = stmt.executeQuery();

		int indiceCodigoServicio = result.findColumn("cdgo_srvco");
		int indiceDescripcionServicio = result.findColumn("dscrpcn_srvco");
		int indiceCdgoCdfccn = result.findColumn("cdgo_cdfccn");
		int indiceDscrpcnCdfccn = result.findColumn("dscrpcn_cdfccn");
		int indiceCodigoInterno = result.findColumn("cdgo_intrno");
		int indiceNmbreScrsl = result.findColumn("nmbre_scrsl");

		lDto = new ArrayList<HomologacionDTO>();

		while (result.next()) {

			HomologacionDTO dto = new HomologacionDTO();
			dto.setCodigoServicio(result.getString(indiceCodigoServicio));
			dto.setDescripcionServicio(result.getString(indiceDescripcionServicio));
			dto.setCdgoCdfccn(result.getString(indiceCdgoCdfccn));
			dto.setDscrpcnCdfccn(result.getString(indiceDscrpcnCdfccn));
			dto.setCdgoIntrno(result.getString(indiceCodigoInterno));
			dto.setNmbreScrsl(result.getString(indiceNmbreScrsl));
			lDto.add(dto);
		}
	}

	public List<HomologacionDTO> getlDto() {
		return lDto;
	}
}
