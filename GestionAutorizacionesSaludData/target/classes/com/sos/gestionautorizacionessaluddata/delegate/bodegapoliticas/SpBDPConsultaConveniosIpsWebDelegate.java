package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioIpsDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPConsultaConveniosIpsWebDelegate implements SQLDelegate{
	
	/**
	 * Lista que se utiliza para almacenar los convenios relacionados con el prestador 
	 */
	private List<ConvenioIpsDTO> lDto;
	
	private String descPrestacion;
	private Integer tipoPrestacion;
	private Integer cupsNorma;	
	private String codigoInternoPrestador;
	private Integer consecutivoCodigoCiudad;	
	
	public SpBDPConsultaConveniosIpsWebDelegate(String descPrestacion, Integer tipoPrestacion, Integer cupsNorma,
			String codigoInternoPrestador, Integer consecutivoCodigoCiudad) {
		this.descPrestacion = descPrestacion;
		this.tipoPrestacion = tipoPrestacion;
		this.cupsNorma = cupsNorma;
		this.codigoInternoPrestador = codigoInternoPrestador;
		this.consecutivoCodigoCiudad = consecutivoCodigoCiudad;		
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CONVENIOS_IPS));

		if (this.descPrestacion == null) {
			stmt.setNull(1, java.sql.Types.LONGVARCHAR);
		} else {
			stmt.setString(1, this.descPrestacion);			
		}
		
		if (this.tipoPrestacion == null) {
			stmt.setNull(2, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(2, this.tipoPrestacion);			
		}
		
		if (this.cupsNorma == null) {
			stmt.setNull(3, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(3, this.cupsNorma);			
		}
		
		if (this.codigoInternoPrestador == null) {
			stmt.setNull(4, java.sql.Types.VARCHAR);
		} else {
			stmt.setString(4, this.codigoInternoPrestador);		
		}		
		
		if (this.consecutivoCodigoCiudad == null) {
			stmt.setNull(5, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(5, this.consecutivoCodigoCiudad);			
		}		
		
		ResultSet result = stmt.executeQuery();

		int consecutivoCodificacion      = result.findColumn(ConstantesData.CONSECUTIVO_CODIFICACION);
		int codigoCodificacion           = result.findColumn(ConstantesData.CODIGO_CODIFICACION);
		int descripcionCodificacion      = result.findColumn(ConstantesData.DESCRIPCION_CODIFICACION);
		int ipsCodigo                    = result.findColumn(ConstantesData.IPS_CODIGO);
		int ipsDescripcion               = result.findColumn(ConstantesData.IPS_DESCRIPCION);
		int planDescripcion              = result.findColumn(ConstantesData.PLAN_DESCRIPCION);
		int valor                        = result.findColumn(ConstantesData.VALOR);
		int tipoAtencion                 = result.findColumn(ConstantesData.TIPO_ATENCION);
		int formulaLiquidacion           = result.findColumn(ConstantesData.FORMULA_LIQUIDACION);
		int accesoDirecto                = result.findColumn(ConstantesData.ACCESO_DIRECTO);
		int direccion                    = result.findColumn(ConstantesData.DIRECCION);
		int telefono                     = result.findColumn(ConstantesData.TELEFONO);
		int inicioVigencia               = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int finVigencia                  = result.findColumn(ConstantesData.FIN_VIGENCIA);

		lDto = new ArrayList<ConvenioIpsDTO>();

		while (result.next()) {
			ConvenioIpsDTO dto = new ConvenioIpsDTO();
			dto.setConsecutivoPrestacion(result.getInt(consecutivoCodificacion));
			dto.setDescripcionPrestacion(result.getInt(codigoCodificacion) + "/" + result.getString(descripcionCodificacion));
			dto.setNombreIps(result.getInt(ipsCodigo) + "/" + result.getString(ipsDescripcion));
			dto.setDescripcionPlan(result.getString(planDescripcion));
			dto.setValorConvenio(result.getInt(valor));
			dto.setTipoAtencionIps(result.getString(tipoAtencion));
			dto.setFormulaLiquidacionIps(result.getString(formulaLiquidacion));
			dto.setAccesoDirectoIps(result.getString(accesoDirecto));
			dto.setDireccionTelefonoIps(result.getString(direccion) + result.getString(telefono));
			dto.setInicioVigenciaMarcaPrestacion(result.getDate(inicioVigencia));
			dto.setFinVigenciaMarcaPrestacion(result.getDate(finVigencia));
			
			lDto.add(dto);
		}
		
		stmt.close();
	}

	public List<ConvenioIpsDTO> getlDto() {
		return lDto;
	}
	
	public void setlDto(List<ConvenioIpsDTO> lDto) {
		this.lDto = lDto;
	}
}
