package com.sos.gestionautorizacionessaluddata.consultas;

import java.util.ResourceBundle;

/**
 * Class ConsultaSp
 * Clase ConsultaSp que consulta las propiedades de un archivo properties
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class ConsultaSp {

	/** Constante BUNDLE_NAME. */
	private static final String BUNDLE_NAME = "properties.consultasp";//$NON-NLS-1$

	/** Constante RESOURCE_BUNDLE. */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	/**
	 * Instancia nueva de sql.
	 */
	private ConsultaSp() {
	}

	/**
	 * Obtiene string.
	 * 
	 * @param key
	 *            the key
	 * @return string
	 */
	public static String getString(String key) {
		return RESOURCE_BUNDLE.getString(key);

	}
}
