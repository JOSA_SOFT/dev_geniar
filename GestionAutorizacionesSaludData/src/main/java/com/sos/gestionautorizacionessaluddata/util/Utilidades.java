package com.sos.gestionautorizacionessaluddata.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class Utilidades implements Serializable {
	private static final long serialVersionUID = -7936319458741738063L;

	private Utilidades() {
		/* clase utilizaría constructor privado para evitar instancias */
	}

	public static BigInteger convertirNumeroToBigInteger(Integer numero) {
		BigInteger conversionNumero = null;

		if (numero == null) {
			conversionNumero = null;
		} else {
			conversionNumero = BigInteger.valueOf(numero);
		}

		return conversionNumero;
	}

	public static BigDecimal convertirNumeroToBigDecimal(Integer numero) {
		BigDecimal conversionNumero = new BigDecimal(0);

		if (numero == null) {
			conversionNumero = null;
		} else {
			conversionNumero = BigDecimal.valueOf(numero);
		}

		return conversionNumero;
	}

	/**
	 * Metodo que convierte (1/0) en (SI/NO)
	 * 
	 * @param numero
	 * @return
	 */
	public static String convertirNumeroToSINO(Object numero) {
		String resultado = "";

		if (numero != null) {
			if (((Integer) (numero)).equals(ConstantesData.CODIGO_SI)) {
				resultado = ConstantesData.SI;
			} else if (((Integer) (numero)).equals(ConstantesData.CODIGO_NO)) {
				resultado = ConstantesData.NO;
			}
		}

		return resultado;
	}

	/**
	 * Metodo que convierte (S/N) en (SI/NO)
	 * 
	 * @param numero
	 * @return
	 */
	public static String convertirStringToSINO(Object string) {
		String resultado = "";

		if (string != null) {
			if (string.toString().trim().equals(ConstantesData.S)) {
				resultado = ConstantesData.SI;
			} else if (string.toString().trim().equals(ConstantesData.N)) {
				resultado = ConstantesData.NO;
			}
		}

		return resultado;
	}

	/**
	 * Metodo que valdia el Null para numeros
	 * 
	 * @param numero
	 * @return
	 */
	public static int validarNullNumero(Object entrada) {
		if (entrada == null) {
			return 0;
		} else {
			return Integer.parseInt(entrada.toString());
		}

	}

	/**
	 * Metodo que valdia el Null
	 * 
	 * @param numero
	 * @return
	 */
	public static String validarNullCadena(Object entrada) {
		if (entrada == null) {
			return "";
		} else {
			return entrada.toString();
		}

	}

	/**
	 * Metodo que homologa el estado del afiliado
	 * 
	 * @param cadena
	 * @return
	 */
	public static String homologarEstado(String entrada) {
		if (entrada.equals(ConstantesData.DESCIPCION_ESTADO)) {
			return ConstantesData.HOMOLOGACION_ESTADO_ACTIVO;
		} else {
			return ConstantesData.HOMOLOGACION_ESTADO_INACTIVO;
		}

	}

	/**
	 * Metodo que homologa el estado del afiliado
	 * 
	 * @param cadena
	 * @return
	 */
	public static boolean enlaceSeleccionar(Integer estado) {
		boolean seleccionar = true;
		if (estado.equals(ConstantesData.COD_ESTADO_AUDITORIA) || estado.equals(ConstantesData.COD_ESTADO_APROBADO)
				|| estado.equals(ConstantesData.COD_ESTADO_INGRESADO)) {
			seleccionar = false;
		}
		return seleccionar;
	}

	/**
	 * realiza la validacion de las fechas
	 * 
	 * @param fecha
	 * @return
	 * @throws SQLException
	 */
	public static java.util.Date resultStamentDate(Date fecha) throws SQLException {
		if (fecha == null) {
			return null;
		} else {
			return new java.util.Date(fecha.getTime());
		}
	}

	public static String formatoNumerico(Double valor) {
		DecimalFormat formatea = new DecimalFormat(ConstantesData.FORMATO_DECIMAL);
		Double val = Double.parseDouble(String.valueOf(ConstantesData.VALOR_CERO));
		if (valor != null)
			val = valor;
		return formatea.format(val);
	}
}
