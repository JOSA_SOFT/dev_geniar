/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.AfiliadoNombresVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * @author GENIAR
 *
 */
public class SpRCConsultaAfiliadoIDDelegate implements SQLDelegate {

	private int consecutivoTipoID;
	private String numeroIdentificacion;
	private AfiliadoNombresVO afiliado;
	
	/**
	 * Constructor de la clase
	 * @param consecutivoTipoID
	 * @param numeroIdentificacion
	 */
	public SpRCConsultaAfiliadoIDDelegate(int consecutivoTipoID,
			String numeroIdentificacion) {
		super();
		this.consecutivoTipoID = consecutivoTipoID;
		this.numeroIdentificacion = numeroIdentificacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.BUSCADOR_AFILIADOS_ID));
		
		stmt.setInt(1, this.consecutivoTipoID);
		stmt.setString(2, this.numeroIdentificacion);
		
		ResultSet rs = stmt.executeQuery();
		
		if(rs.next()){
			afiliado = new AfiliadoNombresVO();
			afiliado.setPrimerNombreInfoAfiliado(rs.getString("prmr_nmbre").trim());
			afiliado.setPrimerApellidoInfoAfiliado(rs.getString("prmr_aplldo").trim());
			String segundoNombre = rs.getString("sgndo_nmbre")==null? "":rs.getString("sgndo_nmbre").trim();
			String segundoApellido = rs.getString("sgndo_aplldo")==null? "":rs.getString("sgndo_aplldo").trim();
			afiliado.setSegundoNombreInfoAfiliado(segundoNombre);
			afiliado.setSegundoApellidoInfoAfiliado(segundoApellido);
			afiliado.setNumeroUnicoIdentificacionInfoAfiliado(rs.getInt("nmro_unco_idntfccn_afldo"));
		}
	}

	public AfiliadoNombresVO getAfiliado() {
		return afiliado;
	}

}
