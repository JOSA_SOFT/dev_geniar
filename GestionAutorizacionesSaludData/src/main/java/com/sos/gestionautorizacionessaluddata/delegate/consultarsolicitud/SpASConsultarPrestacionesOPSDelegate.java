package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultarPrestacionesOPSDelegate
 * Clase delegate que permite consultar las prestaciones relacionadas a una OPS
 * @author Julian Hernandez
 * @version 03/05/2016
 */
public class SpASConsultarPrestacionesOPSDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las prestaciones relacionadas a una solicitud  */
	private List<PrestacionDTO> lstPrestacionesDTO;	
	
	/** numero de solicitud **/
	private Integer numeroSolicitud;
	/** numero unico de autorizacion **/
	private Integer numeroUnicoAutorizacion;
	
	public SpASConsultarPrestacionesOPSDelegate(Integer numeroSolicitud, Integer numeroUnicoAutorizacion){
		this.numeroSolicitud = numeroSolicitud;
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
	}	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_X_OPS));	
		
		stmt.setInt(1, this.numeroSolicitud);	
		stmt.setInt(2, this.numeroUnicoAutorizacion);
		
		ResultSet result = stmt.executeQuery();
		
		int cnsctvo_cdgo_tpo_srvco  = result.findColumn("cnsctvo_cdgo_tpo_srvco");
		int cdgo_tpo_srvco          = result.findColumn("cdgo_tpo_srvco");
		int dscrpcn_tpo_srvco     	= result.findColumn("dscrpcn_tpo_srvco");
		int cnsctvo_cdfccn          = result.findColumn("cnsctvo_cdfccn");
		int cdgo_cdfccn             = result.findColumn("cdgo_cdfccn");
		int dscrpcn_cdfccn          = result.findColumn("dscrpcn_cdfccn");
		int cntdd         			= result.findColumn("cntdd");	
		int cnsctvo_cdgo_cdd_rsdnca_afldo          	= result.findColumn("cnsctvo_cdgo_cdd_rsdnca_afldo");
		int cnsctvo_cdgo_pln         				= result.findColumn("cnsctvo_cdgo_pln");
		
		lstPrestacionesDTO = new ArrayList<PrestacionDTO>();
		PrestacionDTO prestacionDTO;
		TipoCodificacionVO tipoCodificacionVO;
		while(result.next()){
			prestacionDTO = new PrestacionDTO();
			tipoCodificacionVO = new TipoCodificacionVO();
			
			tipoCodificacionVO.setCodigoTipoCodificacion(result.getString(cdgo_tpo_srvco));
			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(result.getInt(cnsctvo_cdgo_tpo_srvco));
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(dscrpcn_tpo_srvco));
			
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(dscrpcn_cdfccn));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(cnsctvo_cdfccn));
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString(cdgo_cdfccn));
			prestacionDTO.setCantidad(result.getInt(cntdd));
			prestacionDTO.setConsecutivoCiudad(result.getInt(cnsctvo_cdgo_cdd_rsdnca_afldo));
			prestacionDTO.setConsecutivoPlan(result.getInt(cnsctvo_cdgo_pln));
			lstPrestacionesDTO.add(prestacionDTO);			
		}		
	}
	
	public List<PrestacionDTO> getlstPrestacionesDTO() {
		return lstPrestacionesDTO;
	}
}
