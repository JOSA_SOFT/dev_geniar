package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASObtenerOficinasDelegate implements SQLDelegate, Serializable {
	private static final long serialVersionUID = 5755364631439911083L;

	private String codigoOficina;
	private String descripcionOficina;
	private Integer tipoOficina;
	private Integer consecutivoSede;
	private Date fechaConsulta;
	private List<OficinasVO> result;

	public SpASObtenerOficinasDelegate(String codigoOficina, String descripcionOficina, Integer tipoOficina,
			Integer consecutivoSede, Date fechaConsulta) {
		super();
		this.codigoOficina = codigoOficina;
		this.descripcionOficina = descripcionOficina;
		this.tipoOficina = tipoOficina;
		this.consecutivoSede = consecutivoSede;
		this.fechaConsulta = fechaConsulta;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_OFICINA_CAJA));
		if (codigoOficina != null) {
			stmt.setString(1, codigoOficina);
		} else {
			stmt.setNull(1, Types.CHAR);
		}

		if (descripcionOficina != null) {
			stmt.setString(2, descripcionOficina);
		} else {
			stmt.setNull(2, Types.CHAR);
		}

		if (tipoOficina != null) {
			stmt.setInt(3, tipoOficina);
		} else {
			stmt.setNull(3, Types.INTEGER);
		}

		if (consecutivoSede != null) {
			stmt.setInt(4, consecutivoSede);
		} else {
			stmt.setNull(4, Types.INTEGER);
		}

		if (fechaConsulta != null) {
			stmt.setDate(5, new java.sql.Date(fechaConsulta.getTime()));
		} else {
			stmt.setNull(5, Types.DATE);
		}

		ResultSet rs = stmt.executeQuery();

		result = new ArrayList<OficinasVO>();

		while (rs.next()) {
			OficinasVO ofi = new OficinasVO();
			ofi.setCodigoOficina(rs.getString("cdgo_ofcna"));
			ofi.setConsecutivoCodigoOficina(rs.getInt("cnsctvo_cdgo_ofcna"));
			ofi.setDescripcionOficina(rs.getString("dscrpcn_ofcna"));
			ofi.setConsecutivoCodigoSede(rs.getInt("cnsctvo_cdgo_sde"));
			ofi.setCodigoSede(rs.getString("cdgo_sde"));
			ofi.setDescripcionSede(rs.getString("dscrpcn_sde"));
			ofi.setConsecutivoTipoOficina(rs.getInt("cnsctvo_cdgo_tpo_ofcna"));
			result.add(ofi);
		}
	}

	public List<OficinasVO> getResult() {
		return result;
	}
}
