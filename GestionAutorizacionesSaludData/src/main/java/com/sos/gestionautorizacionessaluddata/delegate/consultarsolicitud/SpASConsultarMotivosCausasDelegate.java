package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultarMotivosCausasDelegate
 * Clase Delegate que permite consultar los motivos/causas
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpASConsultarMotivosCausasDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar las causas de la anulacion */
	private List<MotivoCausaVO> lstMotivoCausaVO;
	
	/** Parametro tipo causa */
	private Integer consecutivoCodTipoMotivoCausa;
	
	public SpASConsultarMotivosCausasDelegate(Integer consecutivoCodTipoMotivoCausa){
		this.consecutivoCodTipoMotivoCausa = consecutivoCodTipoMotivoCausa;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_MOTIVO_CAUSA));	
		stmt.setInt(1, consecutivoCodTipoMotivoCausa);
		
		ResultSet result = stmt.executeQuery();
		
		int cnsctvo_cdgo_csa_nvdd   	= result.findColumn("cnsctvo_cdgo_csa_nvdd");
		int cdgo_csa_nvdd 				= result.findColumn("cdgo_csa_nvdd");
		int dscrpcn_cdgo_csa_nvdd       = result.findColumn("dscrpcn_cdgo_csa_nvdd");
		int rqre_nmro_atrzcn_ips       = result.findColumn("rqre_nmro_atrzcn_ips");
		
		lstMotivoCausaVO = new ArrayList<MotivoCausaVO>();
		MotivoCausaVO vo;
		while(result.next()){
			vo = new MotivoCausaVO();
			
			vo.setConsecutivoCodMotivoCausa(result.getInt(cnsctvo_cdgo_csa_nvdd));
			vo.setCodMotivoCausa(result.getInt(cdgo_csa_nvdd));
			vo.setDesMotivoCausa(result.getString(dscrpcn_cdgo_csa_nvdd));
			String requiereNumeroAutorizacionIps = result.getString(rqre_nmro_atrzcn_ips);
			vo.setRequiereNumeroAutorizacionIps(ConstantesData.S.equals(requiereNumeroAutorizacionIps)?true:false);
			lstMotivoCausaVO.add(vo);			
		}	
		
	}
	
	public List<MotivoCausaVO> getlstMotivoCausaVO() {
		return lstMotivoCausaVO;
	}
}
