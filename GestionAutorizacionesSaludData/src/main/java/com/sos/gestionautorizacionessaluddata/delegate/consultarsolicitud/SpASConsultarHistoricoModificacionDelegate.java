package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoModificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultarHistoricoModificacionDelegate
 * Clase Delegate que permite consultar el historico de modificaciones
 * @author Giovanny González
 * SETI S.A.S
 * @version 22/02/2017
 *
 */

public class SpASConsultarHistoricoModificacionDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar el historico de modificaciones */
	private List<HistoricoModificacionVO> lHistoricoModificacion;
	
	private int consecutivoSolicitud;
	
	public SpASConsultarHistoricoModificacionDelegate(int consecutivoSolicitud){
		this.consecutivoSolicitud = consecutivoSolicitud;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_HISTORICO_MODIFICACION));	
		stmt.setInt(1, consecutivoSolicitud);
		
		ResultSet result = stmt.executeQuery();
		
		int index_dscrpcn_elmnto_mdfccn  = result.findColumn("dscrpcn_elmnto_mdfccn");
		int index_nmro_atrzcn 			 = result.findColumn("nmro_atrzcn");
		int index_dscrpcn_srvco_slctdo   = result.findColumn("dscrpcn_srvco_slctdo");
		int index_tpo_mdfccn		     = result.findColumn("tpo_mdfccn");
		int index_fcha_mdfccn		     = result.findColumn("fcha_mdfccn");
		int index_vlr_antrr		   	     = result.findColumn("vlr_antrr");
		int index_vlr_nvo		         = result.findColumn("vlr_nvo");
		int index_nmro_atrzcn_antrr		 = result.findColumn("nmro_atrzcn_antrr");
		int index_usro_mdfccn		     = result.findColumn("usro_mdfccn");
		int index_dscrpcn_csl_mdfccn	 = result.findColumn("dscrpcn_csl_mdfccn");
		int index_obsrvcns				 = result.findColumn("obsrvcns");
		
		lHistoricoModificacion = new ArrayList<HistoricoModificacionVO>();
		
		while(result.next()){
			HistoricoModificacionVO historicoModificacionVO = new HistoricoModificacionVO();
			
			historicoModificacionVO.setElementoModificacion(result.getString(index_dscrpcn_elmnto_mdfccn));
			historicoModificacionVO.setNumeroAutorizacion(result.getInt(index_nmro_atrzcn));
			historicoModificacionVO.setDescripcionPrestacion(result.getString(index_dscrpcn_srvco_slctdo));
			historicoModificacionVO.setTipoModificacion(result.getString(index_tpo_mdfccn));
			historicoModificacionVO.setFechaModificacion(new java.util.Date(result.getDate(index_fcha_mdfccn).getTime()));
			historicoModificacionVO.setValorAnterior(result.getString(index_vlr_antrr));
			historicoModificacionVO.setValorNuevo(result.getString(index_vlr_nvo));
			historicoModificacionVO.setNumAutorizacionAnterior(result.getInt(index_nmro_atrzcn_antrr));
			historicoModificacionVO.setUsuarioModificacion(result.getString(index_usro_mdfccn));
			historicoModificacionVO.setCausalModificacion(result.getString(index_dscrpcn_csl_mdfccn));
			historicoModificacionVO.setObservacionModificacion(result.getString(index_obsrvcns));
			
			lHistoricoModificacion.add(historicoModificacionVO);			
		}		
	}
	
	public List<HistoricoModificacionVO> getlHistoricoModificacion() {
		return lHistoricoModificacion;
	}
}
