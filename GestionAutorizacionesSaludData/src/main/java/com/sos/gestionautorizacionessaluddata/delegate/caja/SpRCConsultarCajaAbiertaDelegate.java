package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta el registro que indica que la caja sigue abierta
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarCajaAbiertaDelegate implements SQLDelegate {

	private String usroLogin;
	private List<MovimientoCajaVO> listMovimientosCja;

	/**
	 * Recibe el login del usuario
	 * 
	 * @param usroCrcn
	 */
	public SpRCConsultarCajaAbiertaDelegate(String usroCrcn) {
		super();
		this.usroLogin = usroCrcn;
		this.listMovimientosCja = new ArrayList<MovimientoCajaVO>();
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultarCajaAbierta
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_CAJA_ABIERTA));
		stmt.setString(1, this.usroLogin);

		ResultSet result = stmt.executeQuery();

		int cnsctvoMvmntoCja = result.findColumn("cnsctvo_mvmnto_cja");
		int bse = result.findColumn("bse");
		int fchaAprtra = result.findColumn("fcha_aprtra");
		int fchaCrre = result.findColumn("fcha_crre");
		int cnsctvoCdgoOfcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int cnsctvoCdgoEstdo = result.findColumn("cnsctvo_cdgo_estdo");
		int drccnIp = result.findColumn("drccn_ip");
		int fltnteCrre = result.findColumn("fltnte_crre");
		int sbrnteCrre = result.findColumn("sbrnte_crre");
		int ttlEfctvo = result.findColumn("ttl_efctvo");
		int ttlCja = result.findColumn("ttl_cja");
		int ttlNtaCrdto = result.findColumn("ttl_nta_crdto");
		int cnsctvoCrreOfcna = result.findColumn("cnsctvo_crre_ofcna");
		int fchaCrcn = result.findColumn("fcha_crcn");
		int usroCrcn = result.findColumn("usro_crcn");
		int fchaUltmaMdfccn = result.findColumn("fcha_ultma_mdfccn");
		int usroUltmaMdfccn = result.findColumn("usro_ultma_mdfccn");

		if (result.next()) {
			MovimientoCajaVO movimientosCajaVO = new MovimientoCajaVO();
			movimientosCajaVO.setCnsctvoMvmntoCja(result
					.getInt(cnsctvoMvmntoCja));
			movimientosCajaVO.setBse(result.getDouble(bse));
			movimientosCajaVO.setFchaAprtra(new Date(result.getDate(fchaAprtra)
					.getTime()));
			if (result.getDate(fchaCrre) != null)
				movimientosCajaVO.setFchaCrre(new Date(result.getDate(fchaCrre)
						.getTime()));
			movimientosCajaVO.setCnsctvoCdgoOfcna(result
					.getInt(cnsctvoCdgoOfcna));
			movimientosCajaVO.setCnsctvoCdgoEstdo(result
					.getInt(cnsctvoCdgoEstdo));
			movimientosCajaVO.setDrccnIp(result.getString(drccnIp));
			movimientosCajaVO.setFltnteCrre(result.getDouble(fltnteCrre));
			movimientosCajaVO.setSbrnteCrre(result.getDouble(sbrnteCrre));
			movimientosCajaVO.setTtlEfctvo(result.getDouble(ttlEfctvo));
			movimientosCajaVO.setTtlCja(result.getDouble(ttlCja));
			movimientosCajaVO.setTtlNtaCrdto(result.getDouble(ttlNtaCrdto));
			movimientosCajaVO.setCnsctvoCrreOfcna(result
					.getInt(cnsctvoCrreOfcna));
			movimientosCajaVO.setFchaCrcn(new Date(result.getDate(fchaCrcn)
					.getTime()));
			movimientosCajaVO.setUsroCrcn(result.getString(usroCrcn));
			movimientosCajaVO.setFchaUltmaMdfccn(new Date(result.getDate(
					fchaUltmaMdfccn).getTime()));
			movimientosCajaVO.setUsroUltmaMdfccn(result
					.getString(usroUltmaMdfccn));

			this.listMovimientosCja.add(movimientosCajaVO);
		}
	}

	public List<MovimientoCajaVO> getListMovimientosCja() {
		return listMovimientosCja;
	}
}