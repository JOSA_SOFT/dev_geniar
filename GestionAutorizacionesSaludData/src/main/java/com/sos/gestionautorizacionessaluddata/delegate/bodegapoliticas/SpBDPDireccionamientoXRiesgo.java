package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPDireccionamientoXRiesgo implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<DireccionamientoXRiesgoDTO> lDto;

	private Integer cnsctvoCdfccn;
	private Integer cnsctvoCdad;

	public SpBDPDireccionamientoXRiesgo(Integer cnsctvoCdfccn, Integer cnsctvoCdad) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
		this.cnsctvoCdad = cnsctvoCdad;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DIRECCIONAMIENTO_X_RIESGO));

		stmt.setInt(2, cnsctvoCdfccn);
		stmt.setInt(1, cnsctvoCdad);
		ResultSet result = stmt.executeQuery();
		int indiceCdgoIntrno = result.findColumn(ConstantesData.CODIGO_INTERNO);
		int indiceNmbreScrsl = result.findColumn(ConstantesData.NOMBRE_SUCURSAL);
		int indiceDscrpcnSde = result.findColumn(ConstantesData.DESCRIPCION_CIUDAD);
		int indiceDscrpcnPln = result.findColumn(ConstantesData.DESCRIPCION_PLAN);
		int indiceCdgoRsgoDgnstco = result.findColumn(ConstantesData.CODIGO_RIESGO_DIAGNOSTICO);
		int indiceDscrpcnRsgoDgnstco = result.findColumn(ConstantesData.DESCRIPCION_RIESGO_DIAGNOSTICO);
		int indiceEddMnma = result.findColumn(ConstantesData.EDAD_MINIMA);
		int indiceEddMxma = result.findColumn(ConstantesData.EDAD_MAXIMA);
		int indiceSfcncaIps = result.findColumn(ConstantesData.SFCNCA_IPS);
		int indiceNvlPrrdd = result.findColumn(ConstantesData.NIVEL_PRIORIDAD);
		int indiceIncoVgnca = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceFnVgnca = result.findColumn(ConstantesData.FIN_VIGENCIA);

		lDto = new ArrayList<DireccionamientoXRiesgoDTO>();

		while (result.next()) {

			DireccionamientoXRiesgoDTO dto = new DireccionamientoXRiesgoDTO();
			dto.setCdgoIntrnoRiesgo(result.getString(indiceCdgoIntrno));
			dto.setNmbreScrslRiesgo(result.getString(indiceNmbreScrsl));
			dto.setDscrpcnSdeRiesgo(result.getString(indiceDscrpcnSde));
			dto.setDscrpcnPlnRiesgo(result.getString(indiceDscrpcnPln));
			dto.setCdgoRsgoDgnstcoRiesgo(result.getString(indiceCdgoRsgoDgnstco));
			dto.setDscrpcnRsgoDgnstcoRiesgo(result.getString(indiceDscrpcnRsgoDgnstco));
			dto.setEddMnmaRiesgo(result.getInt(indiceEddMnma));
			dto.setEddMxmaRiesgo(result.getInt(indiceEddMxma));
			dto.setSfcncaIpsRiesgo(result.getString(indiceSfcncaIps));
			dto.setNvlPrrddRiesgo(result.getString(indiceNvlPrrdd));
			dto.setIncoVgncaRiesgo(result.getDate(indiceIncoVgnca));
			dto.setFnVgncaRiesgo(result.getDate(indiceFnVgnca));
			lDto.add(dto);
		}
	}

	public List<DireccionamientoXRiesgoDTO> getlDto() {
		return lDto;
	}
}
