package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoFormatosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASConsultarHistoricoDescargasDelegate
 * Clase Delegate que permite consultar el historico de descargas de los formatos
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/03/2016
 *
 */

public class SpASConsultarHistoricoDescargasDelegate implements SQLDelegate{

	/** Lista que se utiliza para almacenar el historico de formatos */
	private List<HistoricoFormatosVO> lHistoricoFormatos;
	
	private int consecutivoSolicitud;
	
	public SpASConsultarHistoricoDescargasDelegate(int consecutivoSolicitud){
		this.consecutivoSolicitud = consecutivoSolicitud;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_HISTORICO_DESCARGAS));	
		stmt.setInt(1, consecutivoSolicitud);
		
		ResultSet result = stmt.executeQuery();
		
		int index_fcha_dscrga_frmto    = result.findColumn("fcha_dscrga_frmto");
		int index_dscrpcn_srvco_slctdo	   = result.findColumn("dscrpcn_srvco_slctdo");
		int index_frmto_dscrgdo        = result.findColumn("frmto_dscrgdo");
		int index_usro_dscrga_frmto    = result.findColumn("usro_dscrga_frmto");
		int index_dscrpcn_mdo_cntcto   = result.findColumn("dscrpcn_mdo_cntcto");
		int cnsctvo_srvco_slctdo	   = result.findColumn("cnsctvo_srvco_slctdo");
		int index_dscrpcn_prstcn	   = result.findColumn("dscrpcn_prstcn");
		
		lHistoricoFormatos = new ArrayList<HistoricoFormatosVO>();
		
		while(result.next()){
			HistoricoFormatosVO historicoFormatosVO = new HistoricoFormatosVO();
			
			historicoFormatosVO.setFechaDescargaFormato(new java.util.Date(result.getDate(index_fcha_dscrga_frmto).getTime()));
			historicoFormatosVO.setDescripcionServicioSolicitado(result.getString(index_dscrpcn_srvco_slctdo));
			historicoFormatosVO.setFormatoDescarga(result.getString(index_frmto_dscrgdo));
			historicoFormatosVO.setUsuarioFormatoDescarga(result.getString(index_usro_dscrga_frmto));
			historicoFormatosVO.setDescripcionMedioContactoDescarga(result.getString(index_dscrpcn_mdo_cntcto));
			historicoFormatosVO.setConsecutivoPrestacion(result.getInt(cnsctvo_srvco_slctdo));
			historicoFormatosVO.setDescripcionPrestacion(result.getString(index_dscrpcn_prstcn));
			lHistoricoFormatos.add(historicoFormatosVO);			
		}		
	}
	
	public List<HistoricoFormatosVO> getlHistoricoFormatos() {
		return lHistoricoFormatos;
	}
}
