package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpMNConsultaPrestadorCodigoInternoDelegate
 * Clase delegate que permite consultar la informacion del prestador
 * @author Julian Hernandez
 * @version 03/05/2016
 */
public class SpMNConsultaPrestadorCodigoInternoDelegate implements SQLDelegate{
	
	/** informacion del prestador  */
	private PrestadorVO prestadorVO;	
	
	/** numero interno prestador **/
	private String numeroInternoPrestador;
	
	/** numero interno prestador **/
	private String tipoPrestador;
	
	public SpMNConsultaPrestadorCodigoInternoDelegate(String numeroInternoPrestador, String tipoPrestador){
		this.numeroInternoPrestador = numeroInternoPrestador;
		this.tipoPrestador = tipoPrestador;
	}	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTADOR_X_COD_INTERNO));	
		
		stmt.setString(1, this.numeroInternoPrestador);
		stmt.setString(2, this.tipoPrestador);
		stmt.setInt(3, ConstantesData.SW_PRESTADOR_COD_INTERNO);
		
		ResultSet result = stmt.executeQuery();
		
		int nmro_idntfccn  				= result.findColumn("nmro_idntfccn");
		int Cdgo_Tpo_Idntfccn  			= result.findColumn("Cdgo_Tpo_Idntfccn");
		int nmro_unco_idntfccn_prstdr  	= result.findColumn("nmro_unco_idntfccn_prstdr");
		int nmbre_scrsl  				= result.findColumn("nmbre_scrsl");
		int nmbre_prstdr  				= result.findColumn("nmbre_prstdr");
		int cdgo_intrno  				= result.findColumn("cdgo_intrno");
		int cdgo_cdd  					= result.findColumn("cdgo_cdd");
		int dscrpcn_cdd  				= result.findColumn("dscrpcn_cdd");
		int cnsctvo_cdgo_tpo_idntfccn  	= result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		
		TiposIdentificacionVO tiposIdentificacionVO;
		CiudadVO ciudadVO;
		while(result.next()){
			prestadorVO = new PrestadorVO();
			tiposIdentificacionVO = new TiposIdentificacionVO(result.getInt(cnsctvo_cdgo_tpo_idntfccn), result.getString(Cdgo_Tpo_Idntfccn), null);
			ciudadVO = new CiudadVO();
			
			ciudadVO.setCodigoCiudad(result.getString(cdgo_cdd));
			ciudadVO.setDescripcionCiudad(result.getString(dscrpcn_cdd));
			
			prestadorVO.setTiposIdentificacionPrestadorVO(tiposIdentificacionVO);
			prestadorVO.setNumeroIdentificacionPrestador(result.getString(nmro_idntfccn));
			prestadorVO.setNombreSucursal(result.getString(nmbre_scrsl));
			prestadorVO.setRazonSocial(result.getString(nmbre_prstdr));
			prestadorVO.setNumeroIdentificacionPrestador(result.getString(nmro_unco_idntfccn_prstdr));
			prestadorVO.setCodigoInterno(result.getString(nmbre_prstdr));
			prestadorVO.setCiudadVO(ciudadVO);
			prestadorVO.setCodigoInterno(result.getString(cdgo_intrno));
			
			break;
					
		}		
	}
	
	public PrestadorVO getPrestadorVO() {
		return prestadorVO;
	}
}
