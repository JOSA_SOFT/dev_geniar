package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;


/**
 * Class SpASConsultarPrestadorDireccionamientoDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author Julian Hernandez
 * @version 28/12/2015
 *
 */
public class SpASConsultarPrestadorDireccionamientoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<PrestadorVO> lstPrestadoresDireccionamiento;
	/** Consecutivo prestador */	
	private Integer conscodigoPrestacion;
	/** Consecutivo plan*/	
	private Integer consecutivoCodPlan;
	/** Consecutivo ciudad*/	
	private Integer consecutivoCiudad;
	/** Consecutivo de la solicitud*/
	private Integer idSolicitud;
	
	/**
	 * Constructor
	 * @param conscodigoPrestacion
	 * @param consecutivoCodPlan
	 * @param consecutivoCiudad
	 */
	public SpASConsultarPrestadorDireccionamientoDelegate(Integer conscodigoPrestacion, Integer consecutivoCodPlan, Integer consecutivoCiudad, Integer idSolicitud){
		this.conscodigoPrestacion = conscodigoPrestacion;
		this.consecutivoCodPlan = consecutivoCodPlan;
		this.consecutivoCiudad = consecutivoCiudad;
		this.idSolicitud = idSolicitud;
	}
	
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_PRESTADORES_DIRECCIONAMIENTO"));
		stmt.setInt(1, this.conscodigoPrestacion);
		stmt.setInt(2, this.consecutivoCodPlan);
		stmt.setInt(3, this.consecutivoCiudad);
		stmt.setInt(4, this.idSolicitud);
		
		ResultSet result = stmt.executeQuery();
		 
		int cdgo_intrno     	= result.findColumn("cdgo_intrno");
		int nmbre_scrsl   		= result.findColumn("nmbre_scrsl");
		int drccn     			= result.findColumn("drccn");
		int vlr_trfdo   		= result.findColumn("vlr_trfdo");
		
		int rzn_scl   			= result.findColumn("rzn_scl");
		int cdgo_tpo_idntfccn   = result.findColumn("cdgo_tpo_idntfccn");
		int nmro_idntfccn   	= result.findColumn("nmro_idntfccn");
		 
		lstPrestadoresDireccionamiento = new ArrayList<PrestadorVO>();
		PrestadorVO obj;
		TiposIdentificacionVO tiposIdentificacionVO;
		
		while(result.next()){ 
			
			obj = new PrestadorVO();
			tiposIdentificacionVO = new TiposIdentificacionVO(null, result.getString(cdgo_tpo_idntfccn), null);
			obj.setTiposIdentificacionPrestadorVO(tiposIdentificacionVO);
			obj.setCodigoInterno(result.getString(cdgo_intrno));
			obj.setNombreSucursal(result.getString(nmbre_scrsl));
			obj.setDireccionPrestador(result.getString(drccn));
			obj.setValor(result.getInt(vlr_trfdo));
			obj.setRazonSocial(result.getString(rzn_scl));
			obj.setNumeroIdentificacionPrestador(result.getString(nmro_idntfccn));
			
			lstPrestadoresDireccionamiento.add(obj);			 
		}		
	}	 
	
	/**
	 * Get resultado
	 * @return
	 */
	public List<PrestadorVO> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

}
