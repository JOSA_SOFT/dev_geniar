package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASconsultaDatosSolicitudModificarFechaEntregaDelegate
 * Clase delegate que permite consultar las prestaciones Aprobadas de una solicitud
 * @author Giovanny Gonzalez
 * @version 26/10/2016
 */
public class SpASconsultaDatosSolicitudModificarFechaEntregaDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las prestaciones relacionadas a una solicitud  */
	private List<PrestacionDTO> lPrestacionesDTO;	
	private Integer numeroSolicitud;
	private String 	numeroRadicado;
	
	public SpASconsultaDatosSolicitudModificarFechaEntregaDelegate(Integer numeroSolicitud,String numeroRadicado){
		this.numeroSolicitud = numeroSolicitud;
		this.numeroRadicado = numeroRadicado;
	}	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_FECHA_ENTREGA));	
		
		stmt.setInt(1, this.numeroSolicitud);	
		stmt.setString(2, this.numeroRadicado);
		ResultSet result = stmt.executeQuery();
		
		int indice_dscrpcn_tpo_cdfccn		= result.findColumn("dscrpcn_tpo_cdfccn");
		int indice_cdgo_cdfccn          	= result.findColumn("cdgo_cdfccn");
		int indice_dscrpcn_cdfccn     		= result.findColumn("dscrpcn_cdfccn");
		int indice_fcha_estmda_entrga   	= result.findColumn("fcha_estmda_entrga");
		int indice_cvo_cgo_tpo_srv_sldo   	= result.findColumn("cnsctvo_cdgo_tpo_srvco_slctdo");
		int indice_cvo_cdgo_srvco_slctdo   	= result.findColumn("cnsctvo_cdgo_srvco_slctdo");
		int indice_cnsctvo_srvco_slctdo 	= result.findColumn("cnsctvo_srvco_slctdo");
		
		lPrestacionesDTO = new ArrayList<PrestacionDTO>();
		
		while(result.next()){
			PrestacionDTO prestacionDTO = new PrestacionDTO();
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
	
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(indice_dscrpcn_tpo_cdfccn));
			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(result.getInt(indice_cvo_cgo_tpo_srv_sldo));
			
			if (result.getDate(indice_fcha_estmda_entrga)!=null) {
				prestacionDTO.setFechaEstimadaEntrega(new java.util.Date(result.getDate(indice_fcha_estmda_entrga).getTime()));
			}			
			
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString(indice_cdgo_cdfccn));
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(indice_dscrpcn_cdfccn));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(indice_cvo_cdgo_srvco_slctdo));
			prestacionDTO.setConsecutivoServicioSolicitado(result.getInt(indice_cnsctvo_srvco_slctdo));
			lPrestacionesDTO.add(prestacionDTO);			
		}		
	}
	
	public List<PrestacionDTO> getlPrestacionesDTO() {
		return lPrestacionesDTO;
	}
}

