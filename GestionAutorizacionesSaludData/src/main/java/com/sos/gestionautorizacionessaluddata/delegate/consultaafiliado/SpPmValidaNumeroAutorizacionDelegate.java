package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;


/**
 * Class SpPmValidaNumeroAutorizacionDelegate
 * Clase Delegate que consulta las validaciones de derecho del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/01/2016
 *
 */
public class SpPmValidaNumeroAutorizacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar las validaciones de derecho*/
	private List<ValidacionEspecialVO> lValidacionEspecial;
	
	private String codigoOficina;
	private long numeroValidacion;
	private int consecutivoCodigoTipoIdentificacion;
	private String numeroIdentificacion;
	
	public SpPmValidaNumeroAutorizacionDelegate(String codigoOficina, long numeroValidacion, int consecutivoCodigoTipoIdentificacion, String numeroIdentificacion){
		this.codigoOficina = codigoOficina;
		this.numeroValidacion = numeroValidacion;
		this.consecutivoCodigoTipoIdentificacion = consecutivoCodigoTipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_VALIDACION_DERECHOS"));
		stmt.setString(1, this.codigoOficina);
		stmt.setLong(2, this.numeroValidacion);
		stmt.setInt(3, this.consecutivoCodigoTipoIdentificacion);
		stmt.setString(4, this.numeroIdentificacion);
		

	    ResultSet result = stmt.executeQuery();
		
		int index_fcha_vldcn          = result.findColumn("fcha_vldcn");
		int index_existe              = result.findColumn("existe");
		int index_cnsctvo_estdo_drcho = result.findColumn("cnsctvo_estdo_drcho");
		int index_fcha_mxma_cmprbnte  = result.findColumn("fcha_mxma_cmprbnte");
		int index_cnsctvo_cdgo_ofcna  = result.findColumn("cnsctvo_cdgo_ofcna");
		
		lValidacionEspecial = new ArrayList<ValidacionEspecialVO>();
		
		while(result.next()){
			OficinasVO oficinasVO = new OficinasVO();
			ValidacionEspecialVO validacionEspecialVO = new ValidacionEspecialVO();
			
			oficinasVO.setCodigoOficina(this.codigoOficina);
			oficinasVO.setConsecutivoCodigoOficina(result.getInt(index_cnsctvo_cdgo_ofcna));
			
			validacionEspecialVO.setFechaValidacion(new java.util.Date(result.getDate(index_fcha_vldcn).getTime()));
			validacionEspecialVO.setMarcaValidacionExiste(result.getBoolean(index_existe));
			validacionEspecialVO.setConsecutivoCodigoEstadoDerecho(result.getInt(index_cnsctvo_estdo_drcho));
			validacionEspecialVO.setFechaMaximaComprobante(new java.util.Date(result.getDate(index_fcha_mxma_cmprbnte).getTime()));
			
			validacionEspecialVO.setOficinasVO(oficinasVO);
			
			lValidacionEspecial.add(validacionEspecialVO);
		}		
	}	
	
	public List<ValidacionEspecialVO> getlValidacionEspecial() {
		return lValidacionEspecial;
	}	
}
