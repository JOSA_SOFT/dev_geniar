package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.CausasVO;

/**
 * Class SpASConsultarDatosCausasEstadosDelegate
 * Clase Sp para consulta de las causas
 * @author ing. Rafael Cano
 * @version 01/03/2016
 *
 */

public class SpASConsultaDatosInformacionCausasEstadosDelegate implements SQLDelegate {
	private List<CausasVO> listaCausa;
	private int parametroNegocio;
	private int prestacion;
	
	public SpASConsultaDatosInformacionCausasEstadosDelegate(int parametroNegocio, int prestacion){
		this.parametroNegocio = parametroNegocio;
		this.prestacion = prestacion;	
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_CAUSAS"));
		stmt.setInt(1, this.parametroNegocio);
		stmt.setInt(2, this.prestacion);
		
		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_no_cnfrmdd_vldcn");
		int indice_causa = result.findColumn("dscrpcn_no_cnfrmdd_vldcn");
		int indice_estado = result.findColumn("estdo");

		listaCausa = new ArrayList<CausasVO>();
				
		while(result.next()){
			CausasVO causa = new CausasVO();
			causa.setCodigocausa(result.getString(indice_consecutivo));
			causa.setCausa(result.getString(indice_causa));
			causa.setEstadoCausa(result.getInt(indice_estado)==0?false:true);
			
			listaCausa.add(causa);
		}

	}


	public List<CausasVO> getListaCausas() {
		return listaCausa;
	}

}
