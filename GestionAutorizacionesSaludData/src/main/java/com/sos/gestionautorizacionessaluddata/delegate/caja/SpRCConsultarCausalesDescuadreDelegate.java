package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta causales descuadre
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarCausalesDescuadreDelegate implements SQLDelegate {

	private List<CausalDescuadreVO> listCausales;

	/**
	 * Constructor
	 */
	public SpRCConsultarCausalesDescuadreDelegate() {
		super();
		listCausales = new ArrayList<CausalDescuadreVO>();
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultarCausalesDescuadre
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_CAUSALES_DESCUADRE));
		ResultSet result = stmt.executeQuery();

		int cnsctvoCdgoCslDscdre = result.findColumn("cnsctvo_cdgo_csl_dscdre");
		int cdgoCslDscdre = result.findColumn("cdgo_csl_dscdre");
		int dscrpcnCslDscdre = result.findColumn("dscrpcn_csl_dscdre");
		int fchaCrcn = result.findColumn("fcha_crcn");
		int usroCrcn = result.findColumn("usro_crcn");
		int vsbleUsro = result.findColumn("vsble_usro");

		while (result.next()) {
			CausalDescuadreVO causalDescuadreVO = new CausalDescuadreVO();
			causalDescuadreVO.setCnsctvoCdgoCslDscdre(result
					.getInt(cnsctvoCdgoCslDscdre));
			causalDescuadreVO.setCdgoCslDscdre(result.getString(cdgoCslDscdre));
			causalDescuadreVO.setDscrpcnCslDscdre(result
					.getString(dscrpcnCslDscdre));
			causalDescuadreVO.setFchaCrcn(new Date(result.getDate(fchaCrcn)
					.getTime()));
			causalDescuadreVO.setUsroCrcn(result.getString(usroCrcn));
			causalDescuadreVO.setVsbleUsro(result.getString(vsbleUsro));
			listCausales.add(causalDescuadreVO);
		}

	}

	public List<CausalDescuadreVO> getListCausales() {
		return listCausales;
	}

}