package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpAsConsultaSalarioMinimoLVDelegate
 * Clase Delegate que consulta el valor del salario minimo
 * @author ing. Julian Hernandez
 * @version 13/01/2017
 *
 */
public class SpAsConsultaSalarioMinimoLVDelegate implements SQLDelegate{
	
	/** Valor salario minimo */
	private Integer valorSalarioMLV;

	/**
	 * 
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
			
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_SALARIO_MINIMO));
		
		ResultSet result = stmt.executeQuery();
		
		int vlr_slro_mnmo = result.findColumn("vlr_slro_mnmo");
		valorSalarioMLV = 0;
		while(result.next()){
			valorSalarioMLV = result.getInt(vlr_slro_mnmo);
		}		
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getvalorSalarioMLV() {
		return valorSalarioMLV;
	}
}
