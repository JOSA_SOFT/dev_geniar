package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpConsultaProgramacionEntregaDelegate
 * Clase Delegate que consulta la programacion de entrega BUSQUEDA POR DEFECTO
 * @author Julian Hernandez
 * @version 06/07/2016
 *
 */
public class SpASConsultaProgramacionEntregaWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar la programacion*/	
	private List<ProgramacionEntregaVO> lstProgramacionEntregaVO;
	/** numero unico de identifiacion*/	
	private Integer nui;
	
	/**
	 * 
	 * @param nui
	 */
	public SpASConsultaProgramacionEntregaWebDelegate(Integer nui){
		this.nui = nui;
	}

	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		CallableStatement  stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.CONSULTA_PROGRAMACION_ENTREGA_DEFAULT));
		stmt.setInt(1,  nui); 
		
		ResultSet result = stmt.executeQuery();
		
		int indce_fcha_entrga  							= result.findColumn("fcha_entrga");
		int indce_dscrpcn_prstcn     					= result.findColumn("dscrpcn_prstcn");
		int indce_cnsctvo_prstcn     					= result.findColumn("cnsctvo_prstcn");
		int indce_dscrpcn_estdo_entrga     				= result.findColumn("dscrpcn_estdo_entrga");
		int indce_prvdr  								= result.findColumn("prvdr");
		int indce_nmro_autrzcn  						= result.findColumn("nmro_autrzcn");
		int indce_cntdd  								= result.findColumn("cntdd");
		int indce_usro_mdfccn     						= result.findColumn("usro_mdfccn");
		int indce_cnsctvo_cdgo_estds_entrga     		= result.findColumn("cnsctvo_cdgo_estds_entrga");
		int indce_cnsctvo_cdgo_det_prgrmcn_fcha_evnto 	= result.findColumn("cnsctvo_cdgo_det_prgrmcn_fcha_evnto");
		int indce_dscrpcn_clsfccn_evnto     			= result.findColumn("dscrpcn_clsfccn_evnto");
		int indce_cnsctvo_det_prgrmcn_fcha 				= result.findColumn("cnsctvo_det_prgrmcn_fcha");
		int indce_fcha_fnlzcn							= result.findColumn("fcha_fnlzcn");
		int indce_acta									= result.findColumn("acta");
		int indce_cnsctvo_cdgo_ntfccn					= result.findColumn("cnsctvo_cdgo_ntfccn");
		int indce_cdgo_ofcna							= result.findColumn("cdgo_ofcna");
		int indce_vr_gnrr                               = result.findColumn("vr_gnrr");
		  
		lstProgramacionEntregaVO = new ArrayList<ProgramacionEntregaVO>();
		ProgramacionEntregaVO obj_dflt;
		int contador = 1;
		while(result.next()){ 
			obj_dflt = new ProgramacionEntregaVO();
			obj_dflt.setConsProgramacionFecEvento(result.getInt(indce_cnsctvo_det_prgrmcn_fcha));
			obj_dflt.setConsCodProgramacionFecEvento(result.getInt(indce_cnsctvo_cdgo_det_prgrmcn_fcha_evnto));
			obj_dflt.setConsPrestacion(result.getInt(indce_cnsctvo_prstcn));
			obj_dflt.setPrestacion(result.getString(indce_dscrpcn_prstcn));
			obj_dflt.setCantidad(result.getInt(indce_cntdd));
			obj_dflt.setEstadoEntrega(result.getString(indce_dscrpcn_estdo_entrga));
			obj_dflt.setProveedor(result.getString(indce_prvdr));
			obj_dflt.setNumAutorizacion(result.getString(indce_nmro_autrzcn));
			obj_dflt.setFechaEntrega(result.getDate(indce_fcha_entrga));
			obj_dflt.setUsrUltimaMod(result.getString(indce_usro_mdfccn));
			obj_dflt.setFechaFinalizacion(Utilidades.resultStamentDate(result.getDate(indce_fcha_fnlzcn)));
			obj_dflt.setEvento(result.getString(indce_dscrpcn_clsfccn_evnto));
			obj_dflt.setConsEstadoProgramacion(result.getInt(indce_cnsctvo_cdgo_estds_entrga));
			
			obj_dflt.setActa(result.getString(indce_acta));
			obj_dflt.setNumeroNotificacion(result.getInt(indce_cnsctvo_cdgo_ntfccn)); 
			obj_dflt.setCodigoOficina(result.getString(indce_cdgo_ofcna));
			obj_dflt.setPar(contador%2==0);
			obj_dflt.setVerGenerar(result.getInt(indce_vr_gnrr) == ConstantesData.VER_GENERAR ? true: false);			
			contador++;
			lstProgramacionEntregaVO.add(obj_dflt);			 
		}
		
	}	 
	
	/**
	 * 
	 * @return
	 */
	public List<ProgramacionEntregaVO> getLstProgramacionEntregaVO() {
		return lstProgramacionEntregaVO;
	}

}
