package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpPMbuscalateralidadesDelegate
 * Clase Delegate que consulta la lateralidad
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class SpPMBuscalateralidadesDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar la lateralidad */	
	private List<LateralidadesVO> lLateralidad;
	
	private Date fechaConsulta;

	public SpPMBuscalateralidadesDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_LATERALIDADES"));
		stmt.setDate(1, this.fechaConsulta);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_ltrldd = result.findColumn("cnsctvo_cdgo_ltrldd");
		int index_cdgo_ltrldd         = result.findColumn("cdgo_ltrldd");
		int index_dscrpcn_ltrldd      = result.findColumn("dscrpcn_ltrldd");
		
		lLateralidad = new ArrayList<LateralidadesVO>();
		
		while(result.next()){
			LateralidadesVO lateralidadesVO = new LateralidadesVO();
			lateralidadesVO.setConsecutivoLaterialidad(result.getInt(index_cnsctvo_cdgo_ltrldd));
			lateralidadesVO.setCodigoLaterialidad(result.getString(index_cdgo_ltrldd));
			lateralidadesVO.setDescripcionLateralidad(result.getString(index_dscrpcn_ltrldd));
			
			lLateralidad.add(lateralidadesVO);
		}
		
	}
	
	public List<LateralidadesVO> getlLateralidad() {
		return lLateralidad;
	}
}
