package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.SQLDelegate;
import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;

/**
 * Class SpTraerRecobro
 * Clase Sp para consulta del recobro
 * @author ing. Rafael Cano
 * @version 04/03/2016
 *
 */

public class SpTraerRecobroDelegate implements SQLDelegate {
	private List<SelectItem> listaRecobros;
	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_RECOBROS"));

		ResultSet result = stmt.executeQuery();		   
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_rcbro");
		int indice_descripcion = result.findColumn("dscrpcn_rcbro");
		
		listaRecobros = new ArrayList<SelectItem>();
				
		while(result.next()){
			SelectItem item = new SelectItem();
			item.setValue(result.getInt(indice_consecutivo));
			item.setLabel(result.getString(indice_descripcion));
			
			listaRecobros.add(item);
		}
	}

	public List<SelectItem> getListaRecobros() {
		return listaRecobros;
	}

}
