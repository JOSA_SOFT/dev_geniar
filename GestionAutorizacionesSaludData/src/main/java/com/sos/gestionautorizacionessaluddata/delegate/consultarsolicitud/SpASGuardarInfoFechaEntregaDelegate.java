package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;


	/**
		 * Class SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate
		 * Clase delegate que permite generar los numero unicos de ops
		 * asociados a una solicitud
		 * @author Giovanny González
		 * @version 02/11/2016
	*/
	public class SpASGuardarInfoFechaEntregaDelegate implements SQLDelegate{
		
		/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
		StringBuilder xml;
						
		public SpASGuardarInfoFechaEntregaDelegate(StringBuilder xml){
			this.xml = xml;
		}			

		@Override
		public void ejecutarData(Connection conn) throws SQLException {

			CallableStatement stmt = null;		
			stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_FECHA_MODIFICADA));	
			stmt.setString(1, this.xml.toString());
			stmt.registerOutParameter(2, java.sql.Types.INTEGER);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
							
			stmt.execute();
				
			Integer resultado = stmt.getInt(2);
			String  mensaje = stmt.getString(3);
				
			if (resultado < 0) {
				throw new DataAccessException(mensaje);
			}
					
		}			
		
	}
