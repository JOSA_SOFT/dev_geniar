package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.HistoricoPrestacionVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionHistoricoPrestacionDelegate implements SQLDelegate{

	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<HistoricoPrestacionVO> lstHistoricoPrestacionVO;

	private Integer consecutivoTipoDoc;
	private String numDocumento;
	
	public SpASConsultaDatosInformacionHistoricoPrestacionDelegate(Integer consecutivoTipoDoc, String numDocumento){
		this.consecutivoTipoDoc = consecutivoTipoDoc;
		this.numDocumento = numDocumento;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_HISTORICOS_PRESTACION"));
		stmt.setInt(1, this.consecutivoTipoDoc);
		stmt.setString(2, this.numDocumento);


		ResultSet result = stmt.executeQuery();

		int fcha_slctd     				= result.findColumn("fcha_slctd");
		int dscrpcn_estdo_srvco_slctdo  = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int nmbre_scrsl     			= result.findColumn("nmbre_scrsl");
		int cntdd_slctda     			= result.findColumn("cntdd_slctda");
		int dscrpcn_tpo_cdfccn   		= result.findColumn("dscrpcn_tpo_cdfccn");
		int dscrpcn_cdfccn     			= result.findColumn("dscrpcn_cdfccn");
		int indcdr_no_ps     			= result.findColumn("indcdr_no_ps");

		lstHistoricoPrestacionVO = new ArrayList<HistoricoPrestacionVO>();
		HistoricoPrestacionVO obj;
		while(result.next()){ 
			obj = new HistoricoPrestacionVO();
			obj.setFecha(result.getDate(fcha_slctd));
			obj.setDescripcionEstado(result.getString(dscrpcn_estdo_srvco_slctdo));
			obj.setDescripcionPrestador(result.getString(nmbre_scrsl));
			obj.setCantidadHistorico(result.getInt(cntdd_slctda));
			obj.setDescripcionTipoPrestacion(result.getString(dscrpcn_tpo_cdfccn));
			obj.setDescripcionPrestacion(result.getString(dscrpcn_cdfccn));
			obj.setDescPos(result.getString(indcdr_no_ps));
			
			lstHistoricoPrestacionVO.add(obj);
		}		
	}	 

	public List<HistoricoPrestacionVO> getLstHistoricoPrestacionVO() {
		return lstHistoricoPrestacionVO;
	}

}
