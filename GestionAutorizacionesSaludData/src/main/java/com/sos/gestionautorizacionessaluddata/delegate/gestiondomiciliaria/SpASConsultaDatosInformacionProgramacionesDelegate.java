package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;

/**
 * Class SpASConsultaDatosInformacionProgramaciones
 * Clase Sp para consulta del Programaciones
 * @author ing. Rafael Cano
 * @version 29/02/2016
 *
 */

public class SpASConsultaDatosInformacionProgramacionesDelegate implements SQLDelegate {
	private int numeroSolicitud;
	private int consPrestacion;
	private DireccionamientoVO direccionamientoDTO;
	
	public SpASConsultaDatosInformacionProgramacionesDelegate(int numeroSolicitud, int consPrestacion){
		this.numeroSolicitud = numeroSolicitud;
		this.consPrestacion = consPrestacion;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_PROGRAMACION"));	
		stmt.setInt(1, this.numeroSolicitud);
		stmt.setInt(2, this.consPrestacion);

		
		ResultSet result = stmt.executeQuery();

		int indice_consTipoIdentificacion = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_codigoTipoIdentificacion = result.findColumn("dscrpcn_tpo_cdfccn");
		int indice_numeroIdentificacion = result.findColumn("cdgo_cdfccn");
		int indice_codigoInterno = result.findColumn("cdgo_intrno");
		int indice_sucursal = result.findColumn("nmbre_scrsl");
		int indice_fechaEsperado = result.findColumn("fcha_rl_entrga");		
		
		result.next();
		
		direccionamientoDTO = new DireccionamientoVO();
		
		direccionamientoDTO.setConsTipoIdentificacion(result.getInt(indice_consTipoIdentificacion));
		direccionamientoDTO.setCodigoTipoIdentificacion(result.getString(indice_codigoTipoIdentificacion));
		direccionamientoDTO.setNumeroIdentificacion(result.getString(indice_numeroIdentificacion));
		direccionamientoDTO.setCodigoInterno(result.getString(indice_codigoInterno));
		direccionamientoDTO.setSucursal(result.getString(indice_sucursal));
		direccionamientoDTO.setFechaEsperado(result.getDate(indice_fechaEsperado));

	}
	
	public DireccionamientoVO getDireccionamiento() {
		return direccionamientoDTO;
	}
}
