package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.ips.modelo.IPSVO;

/**
 * Class SpMNTraerIpsMedicoDelegate
 * Clase Delegate que consulta las ips relacionados con el medico
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/12/2015
 *
 */

public class SpMNTraerIpsMedicoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las ips relacionadas con el medico */
	private List<IPSVO> lIPSMedico;
	
	private int numeroUnicoIdentificacionMedico;
	
	public SpMNTraerIpsMedicoDelegate(int numeroUnicoIdentificacionMedico){
		this.numeroUnicoIdentificacionMedico = numeroUnicoIdentificacionMedico;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_IPSMEDICO"));
			
		stmt.setInt(1, this.numeroUnicoIdentificacionMedico);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cdgo_intrno               = result.findColumn("cdgo_intrno");
		int index_cnsctvo_cdgo_tpo_idntfccn = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");			
		int index_nmro_idntfccn             = result.findColumn("nmro_idntfccn");		
		int index_nmbre_scrsl               = result.findColumn("nmbre_scrsl");
		int index_rzn_scl                   = result.findColumn("rzn_scl");
		int index_drccn                     = result.findColumn("drccn");
		int index_tlfno                     = result.findColumn("tlfno");	
		int index_dscrpcn_cdd               = result.findColumn("dscrpcn_cdd");	
		
		lIPSMedico = new ArrayList<IPSVO>();
		
		while(result.next()){
			IPSVO ipsMedicoVO = new IPSVO();
			ipsMedicoVO.setCodigoInterno(result.getString(index_cdgo_intrno));
			ipsMedicoVO.setConsecCodigoTipoId(result.getLong(index_cnsctvo_cdgo_tpo_idntfccn));			
			ipsMedicoVO.setNumeroIdentificacion(result.getString(index_nmro_idntfccn));
			ipsMedicoVO.setSucursal(result.getString(index_nmbre_scrsl));
			ipsMedicoVO.setRazonSocial(result.getString(index_rzn_scl));
			ipsMedicoVO.setDireccion(result.getString(index_drccn));
			ipsMedicoVO.setTelefono(result.getString(index_tlfno));			
			ipsMedicoVO.setDescripcionCiudad(result.getString(index_dscrpcn_cdd));			
			
			lIPSMedico.add(ipsMedicoVO);			
		}		
	}
	
	public List<IPSVO> getlIPSMedico() {
		return lIPSMedico;
	}
}
