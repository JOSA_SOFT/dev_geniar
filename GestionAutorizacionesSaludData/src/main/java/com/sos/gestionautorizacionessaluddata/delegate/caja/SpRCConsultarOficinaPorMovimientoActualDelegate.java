package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta Oficina por movimiento de caja con fecha actual
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarOficinaPorMovimientoActualDelegate implements
		SQLDelegate {

	private String userLogin;
	private OficinasVO oficinasVO;

	/**
	 * Consulta Oficina por movimiento de caja con fecha actual
	 * 
	 * @param userLogin
	 */
	public SpRCConsultarOficinaPorMovimientoActualDelegate(String userLogin) {
		super();
		this.userLogin = userLogin;
	}

	/**
	 * Ejecuta el procedimiento
	 * BDCna.cja.spRCConsultarOficinaPorMovimientoActual
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_OFICINA_MOVIMIENTO_ACTUAL));
		stmt.setString(1, this.userLogin);

		ResultSet result = stmt.executeQuery();
		int cnsctvoCdgoOfc = result.findColumn("cnsctvo_cdgo_ofcna");
		int cdgoofcna = result.findColumn("cdgo_ofcna");
		int dscrpcnofcna = result.findColumn("dscrpcn_ofcna");

		if (result.next()) {
			OficinasVO oficinasVOResult = new OficinasVO();
			oficinasVOResult.setConsecutivoCodigoOficina(result
					.getInt(cnsctvoCdgoOfc));
			oficinasVOResult.setCodigoOficina(result.getString(cdgoofcna));
			oficinasVOResult.setDescripcionOficina(result
					.getString(dscrpcnofcna));
			this.oficinasVO = oficinasVOResult;
		}
	}

	public OficinasVO getOficinasVO() {
		return oficinasVO;
	}

}