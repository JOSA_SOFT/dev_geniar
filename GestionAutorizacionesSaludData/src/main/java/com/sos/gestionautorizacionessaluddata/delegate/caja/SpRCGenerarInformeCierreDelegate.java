package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeGeneralReciboVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que se encarga de consumir el SP de generar reporte de cierre de caja
 * @author GENIAR
 *
 */
public class SpRCGenerarInformeCierreDelegate implements SQLDelegate {

	private Date fechaInicial;
	private Date fechaFinal;
	private Integer consecutivoOficina;
	private String usuario;
	private Integer consecutivoSede;
	private List<InformeGeneralReciboVO> resultado;
	
	/**
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param consecutivoOficina
	 * @param usuario
	 * @param consecutivoSede
	 */
	public SpRCGenerarInformeCierreDelegate(Date fechaInicial, Date fechaFinal,
			Integer consecutivoOficina, String usuario, Integer consecutivoSede) {
		super();
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.consecutivoOficina = consecutivoOficina;
		this.usuario = usuario;
		this.consecutivoSede = consecutivoSede;
	}



	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.GENERAR_INFORME_CIERRE_CAJA));
		stmt.setDate(1, new java.sql.Date(fechaInicial.getTime()));
		stmt.setDate(2, new java.sql.Date(fechaFinal.getTime()));
		
		if (consecutivoOficina == null){
			stmt.setNull(3, Types.INTEGER);
		}else{
			stmt.setInt(3, consecutivoOficina);
		}
		
		if (usuario == null){
			stmt.setNull(4, Types.CHAR);
		}else{
			stmt.setString(4, usuario);
		}
		
		if (consecutivoSede == null){
			stmt.setNull(5, Types.INTEGER);
		}else{
			stmt.setInt(5, consecutivoSede);
		}
		
		ResultSet rs = stmt.executeQuery();
		resultado = new ArrayList<InformeGeneralReciboVO>();
		
		while (rs.next()) {
			InformeGeneralReciboVO informe = new InformeGeneralReciboVO();
			informe.setBase(rs.getDouble("bse"));
			informe.setConsecutivoMovimientoCaja(rs.getInt("cnsctvo_mvmnto_cja"));
			informe.setDescripcionOficina(rs.getString("dscrpcn_ofcna"));
			informe.setFaltanteCierre(rs.getDouble("fltnte_crre"));
			informe.setFechaApertura(rs.getDate("fcha_aprtra"));
			informe.setFechaCierre(rs.getDate("fcha_crre"));
			informe.setNombreAsi(rs.getString("nmbre_asi"));
			informe.setSobranteCierre(rs.getDouble("sbrnte_crre"));
			informe.setTotalCaja(rs.getDouble("ttl_cja"));
			informe.setTotalEfectivo(rs.getDouble("ttl_efctvo"));
			informe.setUsuario(rs.getString("usro"));
			resultado.add(informe);
		}
	}

	public List<InformeGeneralReciboVO> getResultado() {
		return resultado;
	}

}
