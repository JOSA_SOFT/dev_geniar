package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.AntecedentesPrestacionVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaDatosInformacionAntescedentesPrestacionDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<AntecedentesPrestacionVO> lstAntecedentesPrestacionVO;
	
	private Integer consecutivoSolicitud;
	private Integer consecutivoPrestacion;
	
	public SpASConsultaDatosInformacionAntescedentesPrestacionDelegate(Integer consecutivoSolicitud, Integer consecutivoPrestacion){
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;
	}


	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_ANTECEDENTES_PRESTACION"));
		stmt.setInt(1, this.consecutivoSolicitud);
		stmt.setInt(2, this.consecutivoPrestacion);
		ResultSet result = stmt.executeQuery();
		 
		int FechaSolicitud     	= result.findColumn("fcha_slctd");
		int estado   			= result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int prestador     		= result.findColumn("nmbre_scrsl");
		int cantidad     		= result.findColumn("cntdd_slctda");
		 
		lstAntecedentesPrestacionVO = new ArrayList<AntecedentesPrestacionVO>();
		AntecedentesPrestacionVO obj;
		while(result.next()){ 
			obj = new AntecedentesPrestacionVO();
			obj.setFecha(result.getDate(FechaSolicitud));
			obj.setDescEstado(result.getString(estado));
			obj.setDescPrestador(result.getString(prestador));
			obj.setCantidad(result.getInt(cantidad));
			lstAntecedentesPrestacionVO.add(obj);			 
		}		
	}	 
	
	public List<AntecedentesPrestacionVO> getLstAntecedentesPrestacionVO() {
		return lstAntecedentesPrestacionVO;
	}

}
