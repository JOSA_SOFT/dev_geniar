package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.servicios.PrestacionFechaEntregaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASConsultaPrestacionFechaEntregaWebDelegate
 * Clase delegate que permite consultar la fecha de entrega de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/04/2016
 */
public class SpASConsultaPrestacionFechaEntregaWebDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar el detalle de la solicitud */
	private List<PrestacionFechaEntregaVO> lPrestacionFechaEntregaVO;	

	private Integer numeroSolicitud;
	
	
	public SpASConsultaPrestacionFechaEntregaWebDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_FECHA_ENTREGA_X_SOLICITUD));	
		
		stmt.setInt(1, this.numeroSolicitud);		
		ResultSet result = stmt.executeQuery();
		
		int indice_cnsctvo_srvco_slctdo = result.findColumn("cnsctvo_srvco_slctdo");
		int indice_dscrpcn_srvco_slctdo = result.findColumn("dscrpcn_srvco_slctdo");
		int indice_fcha_estmda_entrga   = result.findColumn("fcha_estmda_entrga");
		
		lPrestacionFechaEntregaVO = new ArrayList<PrestacionFechaEntregaVO>();
		
		while(result.next()){
			 PrestacionFechaEntregaVO entregaVO = new PrestacionFechaEntregaVO();
			 PrestacionDTO prestacionDTO = new PrestacionDTO();
			 
			 prestacionDTO.setConsecutivoServicioSolicitado(result.getInt(indice_cnsctvo_srvco_slctdo));
			 prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(indice_dscrpcn_srvco_slctdo));
			 
			 entregaVO.setPrestacionDTO(prestacionDTO);
			 entregaVO.setFechaEstimadaEntrega(result.getDate(indice_fcha_estmda_entrga) != null ? new java.util.Date(result.getDate(indice_fcha_estmda_entrga).getTime()) : null);
			 
			 lPrestacionFechaEntregaVO.add(entregaVO);
		}		
	}
	
	public List<PrestacionFechaEntregaVO> getlPrestacionFechaEntregaVO() {
		return lPrestacionFechaEntregaVO;
	}
}
