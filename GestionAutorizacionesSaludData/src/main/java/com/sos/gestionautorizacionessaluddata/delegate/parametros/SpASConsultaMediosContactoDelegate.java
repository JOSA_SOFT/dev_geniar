package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.MedioContactoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultaMediosContactoDelegate
 * Clase Delegate que consulta los medio de contacto de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 08/02/2016
 *
 */
public class SpASConsultaMediosContactoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los medios de contacto */
	private List<MedioContactoVO> lMediosContacto;
		
	private Date fechaConsulta;
	private String visibleUsuario = "S";
	private String codigoMedioContacto;
	private int    consecutivoCodigoMedioContacto;
	
	public SpASConsultaMediosContactoDelegate(java.util.Date fechaConsulta, String codigoMedioContacto, int consecutivoCodigoMedioContacto){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoMedioContacto = codigoMedioContacto;
		this.consecutivoCodigoMedioContacto = consecutivoCodigoMedioContacto;
	}
	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_MEDIOS_CONTACTO"));
		
		if(this.consecutivoCodigoMedioContacto == 0){
			stmt.setNull(1, java.sql.Types.INTEGER);
		}else{
			stmt.setInt(1, this.consecutivoCodigoMedioContacto);
		}
		
		if(this.codigoMedioContacto == null || ConstantesData.CADENA_VACIA.equals(this.codigoMedioContacto)){
			stmt.setNull(2, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(2, this.codigoMedioContacto);
		}
		
		stmt.setDate(3, this.fechaConsulta);
		stmt.setString(4, this.visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_mdo_cntcto  = result.findColumn("cnsctvo_cdgo_mdo_cntcto");
		int index_cdgo_mdo_cntcto          = result.findColumn("cdgo_mdo_cntcto");
		int index_dscrpcn_mdo_cntcto       = result.findColumn("dscrpcn_mdo_cntcto");
		
		lMediosContacto = new ArrayList<MedioContactoVO>();
		
		while(result.next()){
			MedioContactoVO medioContactoVO = new MedioContactoVO();
			medioContactoVO.setConsecutivoMedioContacto(result.getInt(index_cnsctvo_cdgo_mdo_cntcto));
			medioContactoVO.setCodigoMedioContacto(result.getString(index_cdgo_mdo_cntcto));
			medioContactoVO.setDescripcionMedioContacto(result.getString(index_dscrpcn_mdo_cntcto));
			
			lMediosContacto.add(medioContactoVO);
		}
	}
	
	public List<MedioContactoVO> getlMediosContacto() {
		return lMediosContacto;
	}

}
