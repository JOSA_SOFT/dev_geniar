package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConvenioCajaVO;


/**
 * Class SpPmInformacionConvenioCajaDelegate
 * Clase Delegate que consulta la informacion de la cuota moderadora y copago
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class SpPmInformacionConvenioCajaDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar la informacion  de la cuota moderadora y copago*/	
	private List<ConvenioCajaVO> lConvenioCaja;
	
	private int consecutivoCodigoPlan;
	private int consecutivoRangoSalarial;
	private int consecutivoTipoAfiliado;
	private int consecutivoProductoSucursal;
	private Date fechaConsulta;
	
	public SpPmInformacionConvenioCajaDelegate (int consecutivoCodigoPlan, int consecutivoRangoSalarial, int consecutivoTipoAfiliado, int consecutivoProductoSucursal, java.util.Date fechaConsulta){
		this.consecutivoCodigoPlan = consecutivoCodigoPlan;
		this.consecutivoRangoSalarial = consecutivoRangoSalarial;
		this.consecutivoTipoAfiliado = consecutivoTipoAfiliado;
		this.consecutivoProductoSucursal = consecutivoProductoSucursal;
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_INFORMACION_CONVENIO_CAJA"));	
		
		stmt.setInt(1, this.consecutivoCodigoPlan);
		stmt.setInt(2, this.consecutivoRangoSalarial);
		stmt.setInt(3, this.consecutivoTipoAfiliado);
		stmt.setInt(4, this.consecutivoProductoSucursal);		
		stmt.setDate(5, this.fechaConsulta);
		
		ResultSet result = stmt.executeQuery();
		 
		int index_txto_cpgo             = result.findColumn("txto_cpgo");
		int index_txto_cta_mdrdra       = result.findColumn("txto_cta_mdrdra");
		int index_txto_cpgo_cta_mdrdra  = result.findColumn("txto_cpgo_cta_mdrdra");
		
		lConvenioCaja = new ArrayList<ConvenioCajaVO>();
		
		while(result.next()){
			ConvenioCajaVO capitacionVO = new ConvenioCajaVO();
			capitacionVO.setTextoCopago(result.getString(index_txto_cpgo));
			capitacionVO.setTextoCuotaModeradora(result.getString(index_txto_cta_mdrdra));
			capitacionVO.setTextoCopagoTextoCuotaModeradora(result.getString(index_txto_cpgo_cta_mdrdra));
			
			lConvenioCaja.add(capitacionVO);
		}		
	}
	
	public List<ConvenioCajaVO> getlConvenioCaja() {
		return lConvenioCaja;
	}
}
