package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.FundamentoLegalVO;

/**
 * Class SpASConsultarDatosFundamentoLegalDelegate
 * Clase Sp para consulta de los fundamentos legales
 * @author ing. Rafael Cano
 * @version 02/03/2016
 *
 */

public class SpASConsultaDatosInformacionFundamentoLegalDelegate implements SQLDelegate {
	private List<FundamentoLegalVO> listaFundamentos;
	private int numeroSolicitud;
	private int consecutivoPrestacion;
	
	public SpASConsultaDatosInformacionFundamentoLegalDelegate(int numeroSolicitud, int consecutivoPrestacion){
		this.numeroSolicitud = numeroSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_FUNDAMENTOS_LEGALES"));
		stmt.setInt(1, this.numeroSolicitud);
		stmt.setInt(2, this.consecutivoPrestacion);
		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_fndmnto_lgl");
		int indice_fundamento_legal = result.findColumn("dscrpcn_fndmnto_lgl");
		int indice_estado = result.findColumn("estdo");

		listaFundamentos = new ArrayList<FundamentoLegalVO>();
				
		while(result.next()){
			FundamentoLegalVO fundamentoLegal = new FundamentoLegalVO();
			fundamentoLegal.setCodigoFudamento(result.getString(indice_consecutivo));
			fundamentoLegal.setFundamentoLegal(result.getString(indice_fundamento_legal));
			fundamentoLegal.setEstadoFudamento((result.getInt(indice_estado)==0)?false:true);
			
			listaFundamentos.add(fundamentoLegal);
		}

	}


	public List<FundamentoLegalVO> getListaFundamentos() {
		return listaFundamentos;
	}

}
