package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * DELEGATE OBTENER LAS NOTAS CRÉDITO DEL GRUPO FAMILIAR
 * 
 * @author JOSE OLMEDO SOTO AGUIRRE
 *
 */
public class SpRCGuardarReciboCajaDelegate implements SQLDelegate {
	private int nuiAfiliado;
	private List<DocumentoCajaVO> notas;
	private List<InformacionOpsValorVO> opsSeleccionada;
	private ReciboCajaVO recibo;
	private String usuario;
	private int codCaja = 0;
	private int codDocumento = 0;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {

		int consecutivoDetalle = 0;
		try {
			consecutivoDetalle = guardarMotivoDetalle(conn);
			guardarDocumentoCaja(conn, consecutivoDetalle);

			guardarReciboCaja(conn, codDocumento);

			if (notas != null && !notas.isEmpty()) {
				guardarNotasCredito(conn);
			}

		} catch (Exception e) {

			throw new SQLException(e);
		}
	}

	/**
	 * Se encarga de guardar el soporte de caja
	 * 
	 * @param conn
	 *            Conexión
	 * @param codDocumento
	 *            Codigo de documento caja
	 * @throws SQLException
	 */
	private void guardarReciboCaja(Connection conn, int codDocumento)
			throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.GUARDAR_SOPORTE_CAJA));

		for(InformacionOpsValorVO ops : opsSeleccionada){
			stmt.setInt(1, codDocumento);
			stmt.setInt(2, ops.getCodeOPS());
			stmt.setDate(3, new java.sql.Date(new Date().getTime()));
			stmt.setString(4, usuario);
			stmt.execute();
		}
	}

	/**
	 * Guarda el documento caja
	 * 
	 * @param conn
	 *            conexion
	 * @param consecutivoDetalle
	 *            Consecutivo del movimiento detalle
	 * @return codigo de caja almacenado
	 * @throws Exception
	 */
	private int guardarDocumentoCaja(Connection conn, int consecutivoDetalle)
			throws SQLException {
		PreparedStatement stmt;

		stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.GUARDAR_DOCUMENTO_CAJA));

		stmt.setInt(1, nuiAfiliado);
		stmt.setString(2, null);
		stmt.setDouble(3, recibo.getTotalPagar());
		stmt.setInt(4, ConstantesData.CODIGO_TIPO_DOC_RECIBO_CAJA);
		stmt.setInt(5, ConstantesData.CODIGO_ESTADO_GENERADO_CAJA);
		stmt.setString(6, null);
		stmt.setInt(7, consecutivoDetalle);
		stmt.setString(8, recibo.getObservaciones());
		stmt.setString(9, usuario);

		ResultSet resultS = stmt.executeQuery();

		if (resultS.next()) {
			codCaja = resultS.getInt("numeroDocumento");
			codDocumento = resultS.getInt("cnsctvo_cdgo_dcmnto_cja");
		}

		if (codDocumento == 0)
			throw new SQLException(ConstantesData.MENSAJE_ERROR_NO_GUARDA_DOC);

		return codDocumento;

	}

	/**
	 * Guarda el motivo de detalle
	 * 
	 * @param conn
	 *            Conexion
	 * @return Código del motivo de detalle guardado
	 * @throws Exception
	 */
	private int guardarMotivoDetalle(Connection conn) throws SQLException {
		int consecutivoDetalle = 0;
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.INSERTAR_DETALLE_MOV));
		stmt.setDouble(1, recibo.getTotalPagar());
		stmt.setDouble(2, recibo.getValorEfectivo());
		stmt.setDate(3, new java.sql.Date(new Date().getTime()));
		stmt.setString(4, usuario);
		stmt.setDate(5, new java.sql.Date(new Date().getTime()));
		stmt.setString(6, usuario);

		ResultSet resultS = stmt.executeQuery();
		if (resultS.next())
			consecutivoDetalle = resultS.getInt(1);

		if (consecutivoDetalle == 0)
			throw new SQLException(ConstantesData.MSG_ERROR_NO_GUARDA_DETALLE);

		return consecutivoDetalle;
	}

	/**
	 * Almacena las notas credito seleccionadas y cruzadas con las OPS
	 * 
	 * @param conn
	 *            Conexiones
	 * @throws Exception
	 */
	private void guardarNotasCredito(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		for (DocumentoCajaVO nota : notas) {
			recibo.setTotalPagar(nota.getValorAfiliad());
			recibo.setValorEfectivo(0);

			int codMotivo = guardarMotivoDetalle(conn);
			stmt = conn.prepareStatement(ConsultaSp
					.getString(ConstantesData.ACTUALIZAR_CAJA));
			stmt.setInt(1, nota.getPkDocCaja());
			stmt.setInt(2, codMotivo);

			stmt.execute();

			stmt = conn.prepareStatement(ConsultaSp
					.getString(ConstantesData.INSERTAR_DOC_RELACIONADO));

			stmt.setInt(1, codDocumento);
			stmt.setInt(2, nota.getPkDocCaja());
			stmt.setString(3, usuario); 

			stmt.execute();

		}
	}

	public void setNuiAfiliado(int nuiAfiliado) {
		this.nuiAfiliado = nuiAfiliado;
	}

	public int getNuiAfiliado() {
		return nuiAfiliado;
	}

	public void setNotas(List<DocumentoCajaVO> notas) {
		this.notas = notas;
	}

	public List<DocumentoCajaVO> getNotas() {
		return notas;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setRecibo(ReciboCajaVO recibo) {
		this.recibo = recibo;
	}

	public ReciboCajaVO getRecibo() {
		return recibo;
	}

	public void setCodCaja(int codCaja) {
		this.codCaja = codCaja;
	}

	public int getCodCaja() {
		return codCaja;
	}

	public List<InformacionOpsValorVO> getOpsSeleccionada() {
		return opsSeleccionada;
	}

	public void setOpsSeleccionada(List<InformacionOpsValorVO> opsSeleccionada) {
		this.opsSeleccionada = opsSeleccionada;
	}
}
