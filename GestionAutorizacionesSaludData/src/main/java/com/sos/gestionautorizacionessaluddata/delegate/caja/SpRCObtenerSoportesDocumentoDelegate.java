package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * 
 * @author GENIAR
 *
 */
public class SpRCObtenerSoportesDocumentoDelegate implements SQLDelegate {
	private Integer consecutivoCodigoDocumentoCaja;
	private OPSCajaVO ops;
	private List<InformacionOpsValorVO> listaOps;
	
	/**
	 * 
	 * @param consecutivoCodigoDocumentoCaja
	 */
	public SpRCObtenerSoportesDocumentoDelegate(
			Integer consecutivoCodigoDocumentoCaja) {
		super();
		this.consecutivoCodigoDocumentoCaja = consecutivoCodigoDocumentoCaja;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_OPS_DOCUMENTO_CAJA));

		ops = new OPSCajaVO();
		
		

		stmt.setInt(1, this.consecutivoCodigoDocumentoCaja);

		ResultSet resultS = stmt.executeQuery();

		listaOps = new ArrayList<InformacionOpsValorVO>();
		int i = 0;
		List<ConceptoPagoOpsVO> conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
		while (resultS.next()) {
			InformacionOpsValorVO retornoResult = new InformacionOpsValorVO();
			ConceptoPagoOpsVO conceptoPago = new ConceptoPagoOpsVO();
			retornoResult.setValueOPS(resultS.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			retornoResult.setCodeOPS(resultS.getInt("nmro_unco_ops"));
			
			conceptoPago.setNombreConceptoPago(resultS.getString("dscrpcn_cncpto_pgo"));
			conceptoPago.setValorConceptoPago(resultS.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			conceptoPago.setConsecutivoCodigoConcepto(resultS.getInt("cnsctvo_cdgo_cncpto_pgo"));

			if (i == 0) {
				this.adicionar(conceptoPago, conceptosPago,
						retornoResult);
				i++;
			} else {
				if (listaOps.get(i - 1) != null
						&& listaOps.get(i - 1).getCodeOPS() == retornoResult
								.getCodeOPS()) {
					this.modificar(listaOps.get(i - 1), conceptoPago);
				} else {
					conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
					this.adicionar(conceptoPago, conceptosPago,
							retornoResult);
					i++;
				}
			}
		}
		ops.setOps(listaOps);
	}

	private void adicionar(ConceptoPagoOpsVO concepto,
			List<ConceptoPagoOpsVO> conceptos,
			InformacionOpsValorVO retornoResult) {
		if (concepto.getConsecutivoCodigoConcepto() == ConstantesData.CONSECUTIVO_CONCEPTO_CUOTA_MODERADORA) {
			retornoResult.setTotalCuotaModeradora(concepto
					.getValorConceptoPago());
		} else {
			retornoResult.setTotalCopago(concepto.getValorConceptoPago());
		}
		conceptos.add(concepto);
		retornoResult.setListaConceptos(conceptos);
		listaOps.add(retornoResult);
	}

	private void modificar(InformacionOpsValorVO ops,
			ConceptoPagoOpsVO concepto) {
		ops.getListaConceptos().add(concepto);
		ops.setValueOPS(ops.getValueOPS()
				+ concepto.getValorConceptoPago());
		if (concepto.getConsecutivoCodigoConcepto() == ConstantesData.CONSECUTIVO_CONCEPTO_CUOTA_MODERADORA) {
			ops.setTotalCuotaModeradora(ops.getTotalCuotaModeradora()
					+ concepto.getValorConceptoPago());
		} else {
			ops.setTotalCopago(ops.getTotalCopago()
					+ concepto.getValorConceptoPago());
		}
	}

	public OPSCajaVO getOps() {
		return ops;
	}

}
