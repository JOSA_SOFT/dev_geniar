package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;


/**
 * Class SpASConsultaUbicacionPacienteDelegate
 * Clase delegate que permite consultar la ubicacion del paciente de acuerdo 
 * al consecutivo de atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/02/2016
 */
public class SpASConsultaUbicacionPacienteDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la ubicacion del paciente */	
	private List<UbicacionPacienteVO> lUbicacionPaciente;
	
	private String visibleUsuario = "S";
	private Date fechaConsulta;
	private Integer consecutivoClaseAtencion;
	
	public SpASConsultaUbicacionPacienteDelegate(java.util.Date fechaConsulta, Integer consecutivoClaseAtencion){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());	
		this.consecutivoClaseAtencion = consecutivoClaseAtencion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_UBICACION_PACIENTE_CLASE"));
		
		stmt.setInt(1, this.consecutivoClaseAtencion);
		stmt.setDate(2, this.fechaConsulta);		
		stmt.setString(3, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_ubccn_pcnte = result.findColumn("cnsctvo_cdgo_tpo_ubccn_pcnte");
		int index_cdgo_tpo_ubccn_pcnte         = result.findColumn("cdgo_tpo_ubccn_pcnte");
		int index_dscrpcn_tpo_ubccn_pcnte      = result.findColumn("dscrpcn_tpo_ubccn_pcnte");
		
		
		lUbicacionPaciente = new ArrayList<UbicacionPacienteVO>();
		
		while(result.next()){
			UbicacionPacienteVO ubicacionPacienteVO = new UbicacionPacienteVO();
			ubicacionPacienteVO.setConsecutivoUbicacionPaciente(result.getInt(index_cnsctvo_cdgo_tpo_ubccn_pcnte));
			ubicacionPacienteVO.setCodigoUbicacionPaciente(result.getString(index_cdgo_tpo_ubccn_pcnte));
			ubicacionPacienteVO.setDescripcionUbicacionPaciente(result.getString(index_dscrpcn_tpo_ubccn_pcnte));
			
			lUbicacionPaciente.add(ubicacionPacienteVO);			
		}		
		
	}
	
	public List<UbicacionPacienteVO> getlUbicacionPaciente() {
		return lUbicacionPaciente;
	}
}
