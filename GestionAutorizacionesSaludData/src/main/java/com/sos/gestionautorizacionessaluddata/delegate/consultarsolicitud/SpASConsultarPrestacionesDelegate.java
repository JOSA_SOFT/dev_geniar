package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASConsultarPrestacionesDelegate implements SQLDelegate {
	List<PrestacionDTO> lstPrestacionesDTO;
	private Integer consecutivoSolicitud;
	
	
	
	
	public SpASConsultarPrestacionesDelegate(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}




	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_PRESTACIONES));	
		
		stmt.setInt(1, this.consecutivoSolicitud);	
		
		ResultSet result = stmt.executeQuery();
		
		int cnsctvo_cdgo_tpo_srvco = result.findColumn("cnsctvo_cdgo_tpo_srvco");
		int cdgo_tpo_srvco = result.findColumn("cdgo_tpo_srvco");
		int dscrpcn_tpo_srvco  = result.findColumn("dscrpcn_tpo_srvco");
		int cnsctvo_cdfccn  = result.findColumn("cnsctvo_cdfccn");
		int cdgo_cdfccn = result.findColumn("cdgo_cdfccn");
		int dscrpcn_cdfccn = result.findColumn("dscrpcn_cdfccn");
		int cntdd  = result.findColumn("cntdd");	
		int cnsctvo_cdgo_cdd_rsdnca_afldo = result.findColumn("cnsctvo_cdgo_cdd_rsdnca_afldo");
		int cnsctvo_cdgo_pln = result.findColumn("cnsctvo_cdgo_pln");
		int	cnsctvo_srvco_slctdo = result.findColumn("cnsctvo_srvco_slctdo");			
	
		lstPrestacionesDTO = new ArrayList<PrestacionDTO>();
		PrestacionDTO prestacionDTO;
		TipoCodificacionVO tipoCodificacionVO;
		while(result.next()){
			prestacionDTO = new PrestacionDTO();
			tipoCodificacionVO = new TipoCodificacionVO();
			
			tipoCodificacionVO.setCodigoTipoCodificacion(result.getString(cdgo_tpo_srvco));
			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(result.getInt(cnsctvo_cdgo_tpo_srvco));
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(dscrpcn_tpo_srvco));			
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);			
			prestacionDTO.setDescripcionCodificacionPrestacion(result.getString(dscrpcn_cdfccn));
			prestacionDTO.setConsecutivoCodificacionPrestacion(result.getInt(cnsctvo_cdfccn));
			prestacionDTO.setCodigoCodificacionPrestacion(result.getString(cdgo_cdfccn));
			prestacionDTO.setCantidad(result.getInt(cntdd));
			prestacionDTO.setConsecutivoCiudad(result.getInt(cnsctvo_cdgo_cdd_rsdnca_afldo));
			prestacionDTO.setConsecutivoPlan(result.getInt(cnsctvo_cdgo_pln));
			prestacionDTO.setConsecutivoServicioSolicitado(result.getInt(cnsctvo_srvco_slctdo));
			lstPrestacionesDTO.add(prestacionDTO);			
		}		
	}
	
	public List<PrestacionDTO> getlstPrestacionesDTO() {
		return lstPrestacionesDTO;
	}
}
