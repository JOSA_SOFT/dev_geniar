package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * 
 * @author GENIAR
 *
 */
public class SpRCConsultaDatosContactoDelegate implements SQLDelegate {

	private int consecutivoContacto;
	private DatosContactoVO contacto;
	
	/**
	 * 
	 * @param consecutivoContacto
	 */
	public SpRCConsultaDatosContactoDelegate(int consecutivoContacto) {
		super();
		this.consecutivoContacto = consecutivoContacto;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_DATOS_CONTACTO_DOCUMENTO));
		
		stmt.setInt(1, this.consecutivoContacto);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			contacto = new DatosContactoVO();
			contacto.setConsecutivoBanco(rs.getInt("cnsctvo_cdgo_bnco"));
			contacto.setConsecutivoMetodoDevolucion(rs.getInt("cnsctvo_cdgo_mtdo_dvlcn_dnro"));
			contacto.setCorreoElectronico(rs.getString("eml"));
			contacto.setNumeroCuenta(rs.getString("nmro_cnta"));
			contacto.setTelefono(rs.getString("nmro_tlfno"));
			contacto.setTipoCuenta(rs.getString("tpo_cnta"));
			contacto.setTitular(rs.getString("ttlr"));
		}
	}

	public DatosContactoVO getContacto() {
		return contacto;
	}
}
