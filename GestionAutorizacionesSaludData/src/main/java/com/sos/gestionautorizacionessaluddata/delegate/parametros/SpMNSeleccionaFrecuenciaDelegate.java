package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpMNSeleccionaFrecuenciaDelegate
 * Clase delegate que permite consultar la frecuencia del medicamento
 * @author ing. Victor Hugo Gil Ramos
 * @version 18/12/2015
 */

public class SpMNSeleccionaFrecuenciaDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la frecuencia del medicamento */
	private List<FrecuenciaVO> lFrecuencia;
	
	private String visibleUsuario = "N";
	private Date fechaConsulta;
	
	public SpMNSeleccionaFrecuenciaDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_FRECUENCIA_MED"));
		stmt.setDate(1, this.fechaConsulta);		
		stmt.setString(2, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_frcnca = result.findColumn("cnsctvo_cdgo_frcnca");
		int index_cdgo_frcnca        = result.findColumn("cdgo_frcnca");
		int index_dscrpcn_frcnca      = result.findColumn("dscrpcn_frcnca");
		
		lFrecuencia = new ArrayList<FrecuenciaVO>();
		
		while(result.next()){
			FrecuenciaVO frecuenciaVO = new FrecuenciaVO();
			frecuenciaVO.setConsecutivoFrecuencia(result.getInt(index_cnsctvo_cdgo_frcnca));
			frecuenciaVO.setCodigoFrecuencia(result.getString(index_cdgo_frcnca));
			frecuenciaVO.setDescripcionFrecuencia(result.getString(index_dscrpcn_frcnca));
			
			lFrecuencia.add(frecuenciaVO);
		}		
	}
	
	public List<FrecuenciaVO> getlFrecuencia() {
		return lFrecuencia;
	}
}
