package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesExentosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Se encarga de ejecutar el sp spRCObtenerCausalesExoneracion
 * @author GENIAR
 */
public class SpRCObtenerCausalesExoneracionDelegate implements SQLDelegate {

	private int consecutivoSolicitud;
	private List<CausalesExentosVO> listaCausales;
	
	/**
	 * Constructor de la clase
	 * @param consecutivoSolicitud
	 */
	public SpRCObtenerCausalesExoneracionDelegate(int consecutivoSolicitud) {
		super();
		this.consecutivoSolicitud = consecutivoSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_EXONERACIONES_PAGO));
		listaCausales = new ArrayList<CausalesExentosVO>();

		stmt.setInt(1, consecutivoSolicitud);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			CausalesExentosVO obj = new CausalesExentosVO();
			obj.setConsecutivoCausa(rs.getInt("cnsctvo_cdgo_csa_no_cbro_cta_rcprcn"));
			obj.setDescripcion(rs.getString("dscrpcn_csa_exnrcn_cta_rcprcn"));
			listaCausales.add(obj);
		}
	}

	public List<CausalesExentosVO> getListaCausales() {
		return listaCausales;
	}
}
