package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

public class SpAsValidaEstadoPrestReliquidaDelegate implements SQLDelegate{
	/** Lista que se utiliza para almacenar el detalle de la solicitud */


	private Integer numeroSolicitud;
	private Integer totalRegistros;
	
	
	public SpAsValidaEstadoPrestReliquidaDelegate(Integer numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_RELIQUIDAR));	
		
		stmt.setInt(1, this.numeroSolicitud);	
		ResultSet result = stmt.executeQuery();
		
		int indice_total_registros  = result.findColumn("total_registros");
		while(result.next()){
			totalRegistros=result.getInt(indice_total_registros);
		}	
	}
	
	public Integer getTotalRegistros() {
		return totalRegistros;
	}
}
