package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXPlanXCargoDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPCupsXPlanXCargo implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<CupsXPlanXCargoDTO> lDto;

	private Integer cnsctvoCdfccn;

	public SpBDPCupsXPlanXCargo(Integer cnsctvoCdfccn) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_CUPSXPLANXCARGO));

		stmt.setString(1, cnsctvoCdfccn.toString());
		ResultSet result = stmt.executeQuery();

		int indiceDescPlan = result.findColumn("dscrpcn_pln");
		int indiceDescCargo = result.findColumn("dscrpcn_crgo_ss");

		lDto = new ArrayList<CupsXPlanXCargoDTO>();

		while (result.next()) {

			CupsXPlanXCargoDTO dto = new CupsXPlanXCargoDTO();
			dto.setDescPlan(result.getString(indiceDescPlan));
			dto.setDescCargo(result.getString(indiceDescCargo));

			lDto.add(dto);
		}
	}

	public List<CupsXPlanXCargoDTO> getlDto() {
		return lDto;
	}
}
