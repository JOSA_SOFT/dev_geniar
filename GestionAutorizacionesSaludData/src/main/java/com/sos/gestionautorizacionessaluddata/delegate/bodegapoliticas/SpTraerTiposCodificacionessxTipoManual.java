package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpTraerTipoCodificacionDelegate Clase Delegate que consulta los tipos
 * de codificaciones
 * 
 * @author Jose Soto
 * @version 15/03/2017
 *
 */
public class SpTraerTiposCodificacionessxTipoManual implements SQLDelegate {

	/** Lista que se utiliza para almacenar los tipos de codificacion */
	private List<TipoCodificacionVO> lTipoCodificacion;
	private List<SelectItem> lTipoCodificacionBPM;
	private int tipoABuscar;

	public SpTraerTiposCodificacionessxTipoManual(int tipoABuscar) {
		this.tipoABuscar = tipoABuscar;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_TIPOS_CODIFICACION));

		stmt.setInt(1, tipoABuscar);

		ResultSet result = stmt.executeQuery();

		int indexCnsctvoCdgoTpoCdfccn = result.findColumn("cnsctvo_cdgo_tpo_cdfccn");
		int indexCdgoTpoCdfccn = result.findColumn("cdgo_tpo_cdfccn");
		int indexDscrpcTpoCdfccn = result.findColumn("dscrpcn_tpo_cdfccn");

		lTipoCodificacion = new ArrayList<TipoCodificacionVO>();
		lTipoCodificacionBPM = new ArrayList<SelectItem>();

		while (result.next()) {
			int consecutivoCodigoTipoCodificacion = result.getInt(indexCnsctvoCdgoTpoCdfccn);
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();

			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(consecutivoCodigoTipoCodificacion);
			tipoCodificacionVO.setCodigoTipoCodificacion(result.getString(indexCdgoTpoCdfccn));
			tipoCodificacionVO.setDescripcionTipoCodificacion(result.getString(indexDscrpcTpoCdfccn));
			lTipoCodificacion.add(tipoCodificacionVO);

			lTipoCodificacionBPM
					.add(generarItem(consecutivoCodigoTipoCodificacion, result.getString(indexDscrpcTpoCdfccn)));

		}
	}

	public SelectItem generarItem(int consecutivoTipoPrestacion, String descripcionTipoPrestacion) {
		SelectItem item = new SelectItem();
		item.setValue(consecutivoTipoPrestacion);
		item.setLabel(descripcionTipoPrestacion);
		return item;
	}

	public List<TipoCodificacionVO> getlTipoCodificacion() {
		return lTipoCodificacion;
	}

	public List<SelectItem> getlTipoCodificacionBPM() {
		return lTipoCodificacionBPM;
	}
}
