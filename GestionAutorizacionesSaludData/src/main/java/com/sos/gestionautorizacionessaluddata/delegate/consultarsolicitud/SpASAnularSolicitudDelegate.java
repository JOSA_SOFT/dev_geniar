package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASAnularSolicitudDelegate
 * Clase Anula solicitud
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpASAnularSolicitudDelegate implements SQLDelegate{
	
	/**  Consecutivo solicitud	**/
	private Integer consecutivoSolicitud;
	/**  Consecutivo del servicio  **/
	private Integer consecutivoServicio;
	/** numero unico de autorizacion	**/
	private Integer numeroUnicoAutorizacion;
	/** consecutivo codigo causa	**/
	private Integer consecutivoCodMotivoCausa;
	/** observacion	**/
	private String observacion;
	/** usuario	**/
	private String usuario;
	
	/**
	 * 
	 * @param consecutivoSolicitud
	 * @param numeroUnicoAutorizacion
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 */
	public SpASAnularSolicitudDelegate(Integer consecutivoSolicitud, Integer consecutivoServicio, Integer numeroUnicoAutorizacion, Integer consecutivoCodMotivoCausa,
			String observacion, String usuario){
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoServicio = consecutivoServicio;
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
		this.observacion = observacion;
		this.usuario = usuario;
	}
	
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.ANULAR_SOLICITUD));
		
		if (this.consecutivoSolicitud!=null) {
			statement.setInt(1, this.consecutivoSolicitud);
		}else {
			statement.setNull(1, java.sql.Types.INTEGER);
		}
		
		if (this.consecutivoServicio!=null) {
			statement.setInt(2, this.consecutivoServicio);
		}else {
			statement.setNull(2, java.sql.Types.INTEGER);
		}
		
		if (this.numeroUnicoAutorizacion!=null) {
			statement.setInt(3, this.numeroUnicoAutorizacion);
		}else {
			statement.setNull(3, java.sql.Types.INTEGER);
		}
			
		statement.setInt(4, this.consecutivoCodMotivoCausa);
		statement.setString(5, this.observacion);
		statement.setString(6, this.usuario);
		
		statement.registerOutParameter(7, java.sql.Types.INTEGER);
		statement.registerOutParameter(8, java.sql.Types.VARCHAR);

		statement.executeUpdate();

		Integer resultado = statement.getInt(7);
		String  mensaje = statement.getString(8);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}

}
