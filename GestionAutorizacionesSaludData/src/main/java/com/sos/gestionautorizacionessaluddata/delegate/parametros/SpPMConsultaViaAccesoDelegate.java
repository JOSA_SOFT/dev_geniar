package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpPMConsultaViaAccesoDelegate
 * Clase Delegate que consulta las vias de acesso de la prestacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */
public class SpPMConsultaViaAccesoDelegate implements SQLDelegate{
	
	private List<ViaAccesoVO> lViaAcceso;

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_VIA_ACCESO"));
						
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_va_accso = result.findColumn("cnsctvo_cdgo_va_accso");
		int index_dscrpcn_va_accso = result.findColumn("dscrpcn_va_accso");
		
		
		lViaAcceso = new ArrayList<ViaAccesoVO>();
				
		while(result.next()){
			
			ViaAccesoVO accesoVO = new ViaAccesoVO();
			accesoVO.setConsecutivoViaAcceso(result.getInt(index_cnsctvo_cdgo_va_accso));
			accesoVO.setDescripcionViaAcceso(result.getString(index_dscrpcn_va_accso));
			
			lViaAcceso.add(accesoVO);		
		}						
	}
	
	public List<ViaAccesoVO> getlViaAcceso() {
		return lViaAcceso;
	}
}
