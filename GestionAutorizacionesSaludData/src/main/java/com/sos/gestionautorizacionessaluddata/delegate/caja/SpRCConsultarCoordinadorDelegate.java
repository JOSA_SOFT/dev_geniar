package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.CoordinadorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta el coordinador dado el login
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarCoordinadorDelegate implements SQLDelegate {

	private String loginUser;
	private CoordinadorVO coordinadorVO;

	/**
	 * Recibe el login user para buscar al coordinador
	 * 
	 * @param loginUser
	 */
	public SpRCConsultarCoordinadorDelegate(String loginUser) {
		super();
		this.loginUser = loginUser;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultarCoordinador
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_COORDINADOR));
		stmt.setString(1, this.loginUser);
		ResultSet result = stmt.executeQuery();

		int cnsctvoCdgoCrdndrAsi = result.findColumn("cnsctvo_cdgo_crdndr_asi");
		int cdgoCrdndrAsi = result.findColumn("cdgo_crdndr_asi");
		int cnsctvoCdgoOfcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int dscrpcnOfcna = result.findColumn("dscrpcn_ofcna");
		int lgnUsro = result.findColumn("lgn_usro");

		if (result.next()) {
			CoordinadorVO coordinadorVOTemp = new CoordinadorVO();
			coordinadorVOTemp.setCnsctvoCdgoCrdndrAsi(result
					.getInt(cnsctvoCdgoCrdndrAsi));
			coordinadorVOTemp.setCdgoCrdndrAsi(result.getString(cdgoCrdndrAsi));
			coordinadorVOTemp.setCnsctvoCdgoOfcna(result
					.getInt(cnsctvoCdgoOfcna));
			coordinadorVOTemp.setDscrpcnOfcna(result.getString(dscrpcnOfcna));
			coordinadorVOTemp.setLgnUsro(result.getString(lgnUsro));
			this.coordinadorVO = coordinadorVOTemp;
		}
	}

	public CoordinadorVO getCoordinadorVO() {
		return coordinadorVO;
	}
}