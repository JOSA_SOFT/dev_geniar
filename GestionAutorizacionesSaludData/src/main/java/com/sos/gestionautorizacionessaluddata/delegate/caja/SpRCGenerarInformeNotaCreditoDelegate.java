package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeNotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clae que se encarga de consumir el sp spRCGenerarInformeNotaCredito
 * 
 * @author GENIAR
 */
public class SpRCGenerarInformeNotaCreditoDelegate implements SQLDelegate {

	private Date fechaInicial;
	private Date fechaFinal;
	private Integer nuiAfiliado;
	private Integer estadoDocumento;
	private List<InformeNotasCreditoVO> resultado;

	/**
	 * Constructor de la clase
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param nuiAfiliado
	 * @param estadoDocumento
	 */
	public SpRCGenerarInformeNotaCreditoDelegate(Date fechaInicial, Date fechaFinal,
			Integer nuiAfiliado, Integer estadoDocumento) {
		super();
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.nuiAfiliado = nuiAfiliado;
		this.estadoDocumento = estadoDocumento;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.GENERAR_INFORME_NOTAS_CREDITO));
		
		stmt.setDate(1, new java.sql.Date(fechaInicial.getTime()));
		stmt.setDate(2, new java.sql.Date(fechaFinal.getTime()));
		
		if (nuiAfiliado == null){
			stmt.setNull(3, Types.INTEGER);
		}else{
			stmt.setInt(3, nuiAfiliado);
		}
		
		if (estadoDocumento == null){
			stmt.setNull(4, Types.INTEGER);
		}else{
			stmt.setInt(4, estadoDocumento);
		}
		
		ResultSet rs = stmt.executeQuery();
		
		resultado = new ArrayList<InformeNotasCreditoVO>();
		
		while (rs.next()) {
			InformeNotasCreditoVO obj = new InformeNotasCreditoVO();
			obj.setConsecutivoCodigoDocumento(rs.getInt("cnsctvo_cdgo_dcmnto_cja"));
			obj.setNumeroDocumento(rs.getInt("nmro_dcmnto"));
			obj.setFechaDocumento(rs.getDate("fcha_crcn"));
			obj.setNombreCompletoAfiliado(rs.getString("nmbre_cmplto_afldo"));
			obj.setEstadoDocumento(rs.getString("dscrpcn_estdo_dcmnto_cja"));
			obj.setNuiAfililado(rs.getInt("nmro_unco_idntfccn_afldo"));
			obj.setNumeroIdentificacionAfiliado(rs.getString("nmro_idntfccn_afldo"));
			obj.setTipoIdentificacionAfiliado(rs.getString("tpo_idntfccn_afldo"));
			obj.setValorDocumento(rs.getDouble("vlr_dcmnto_cja"));
			obj.setConsecutivoEstadoDocumento(rs.getInt("cnsctvo_cdgo_estdo_dcmnto_cja"));
			obj.setConsecutivoCodigoTipoDocumentoAfiliado(rs.getInt("cnsctvo_cdgo_cdgo_tpo_dcmnto"));
			obj.setConsecutivoContacto(rs.getInt("cnsctvo_dto_cntcto"));
			obj.setObservacion(rs.getString("obsrvcn"));
			resultado.add(obj);
		}
	}

	public List<InformeNotasCreditoVO> getResultado() {
		return resultado;
	}

}
