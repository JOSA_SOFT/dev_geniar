package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ServiciosCapitadosVO;


/**
 * Class SpPmValidaServiciosCapitadosCajaDelegate
 * Clase Delegate que consulta la informacion de los servicios capitados del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/01/2016
 *
 */
public class SpPmValidaServiciosCapitadosCajaDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los servicios capitados del afiliado*/
	private List<ServiciosCapitadosVO> lServiciosCapitados;
	
	private long codigoConvenio;
	private int  consecutivoCodigoCiudad;
	private int  numeroUnicoIdentificacionAfiliado;
	private int  consecutivoCodigoTipoContrato;
	private Date fechaConsulta;
	
	public SpPmValidaServiciosCapitadosCajaDelegate(long codigoConvenio, int consecutivoCodigoCiudad, int numeroUnicoIdentificacionAfiliado, int consecutivoCodigoTipoContrato, java.util.Date fechaConsulta){
		this.codigoConvenio = codigoConvenio;
		this.consecutivoCodigoCiudad = consecutivoCodigoCiudad;
		this.numeroUnicoIdentificacionAfiliado = numeroUnicoIdentificacionAfiliado;
		this.consecutivoCodigoTipoContrato = consecutivoCodigoTipoContrato;
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_VALIDA_SERVICIOS_CAPITADOS"));	
		
		stmt.setLong(1, this.codigoConvenio);
		stmt.setInt(2, this.consecutivoCodigoCiudad);
		stmt.setInt(3, this.numeroUnicoIdentificacionAfiliado);
		stmt.setInt(4, this.consecutivoCodigoTipoContrato);		
		stmt.setDate(5, this.fechaConsulta);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cdgo_itm_cptcn    = result.findColumn("cdgo_itm_cptcn");
		int index_dscrpcn_itm_cptcn = result.findColumn("dscrpcn_itm_cptcn");
		int index_accion            = result.findColumn("accion");
		
		lServiciosCapitados = new ArrayList<ServiciosCapitadosVO>();
		
		while(result.next()){
			ServiciosCapitadosVO serviciosCapitadosVO = new ServiciosCapitadosVO();
			serviciosCapitadosVO.setCodigoItemCapitacion(result.getString(index_cdgo_itm_cptcn));
			serviciosCapitadosVO.setDescripcionItemCapitacion(result.getString(index_dscrpcn_itm_cptcn));
			serviciosCapitadosVO.setAccionItemCapitacion(result.getString(index_accion));
			
			lServiciosCapitados.add(serviciosCapitadosVO);
		}		
	}
	
	public List<ServiciosCapitadosVO> getlServiciosCapitados() {
		return lServiciosCapitados;
	}
}
