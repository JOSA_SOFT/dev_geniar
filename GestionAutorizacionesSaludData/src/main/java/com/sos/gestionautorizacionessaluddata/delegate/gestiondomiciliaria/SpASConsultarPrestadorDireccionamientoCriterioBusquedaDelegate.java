package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASConsultarPrestadorDireccionamientoDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author Julian Hernandez
 * @version 28/12/2015
 *
 */
public class SpASConsultarPrestadorDireccionamientoCriterioBusquedaDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<PrestadorVO> lstPrestadoresDireccionamiento;
	
	/** Consecutivo prestador */	
	private Integer conscodigoPrestacion;
	
	/** Consecutivo plan*/	
	private Integer consecutivoCodPlan;
	
	/** Consecutivo ciudad*/	
	private Integer consecutivoCiudad;
	
	/** Codigo Interno*/
	private String codigoInterno;
	
	/** NIT */
	private String nit;
	
	/**sucursal*/
	private String sucursal;
	
	/** Consecutivo de la solicitud*/	
	private Integer idSolicitud;
	
	
	/**
	 * Constructor
	 * @param conscodigoPrestacion
	 * @param consecutivoCodPlan
	 * @param consecutivoCiudad
	 */
	public SpASConsultarPrestadorDireccionamientoCriterioBusquedaDelegate(Integer conscodigoPrestacion, Integer consecutivoCodPlan, Integer consecutivoCiudad, String codigoInterno, String sucursal, String nit, Integer idSolicitud){
		this.conscodigoPrestacion = conscodigoPrestacion;
		this.consecutivoCodPlan = consecutivoCodPlan;
		this.consecutivoCiudad = consecutivoCiudad;
		this.codigoInterno = codigoInterno;
		this.nit = nit;
		this.sucursal = sucursal;
		this.idSolicitud = idSolicitud;
	}
	
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {		
		PrestadorVO prestador;
		TiposIdentificacionVO tiposIdentificacionVO;
		
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_PRESTADORES_DIRECCIONAMIENTO_CRITERIO"));
		stmt.setInt(1, this.conscodigoPrestacion);
		stmt.setInt(2, this.consecutivoCodPlan);
		stmt.setInt(3, this.consecutivoCiudad);
		
		if(codigoInterno != null && codigoInterno.trim().equals(ConstantesData.CADENA_VACIA)==false){
			stmt.setString(4, codigoInterno);
		}else{
			stmt.setString(4, null);
		}
		
		if(nit != null && nit.trim().equals(ConstantesData.CADENA_VACIA)==false){
			stmt.setString(5, nit);
		}else{
			stmt.setString(5, null);
		}
		
		if(sucursal != null && sucursal.trim().equals(ConstantesData.CADENA_VACIA)==false){
			stmt.setString(6, sucursal);
		}else{
			stmt.setString(6, null);
		}

		stmt.setInt(7, this.idSolicitud);
		
		ResultSet result = stmt.executeQuery();
		 
		int cdgo_intrnoPrestacion     	= result.findColumn("cdgo_intrno");
		int nmbre_scrslPrestacion   	= result.findColumn("nmbre_scrsl");
		int drccnPrestacion     		= result.findColumn("drccn");
		int vlr_trfdoPrestacion   		= result.findColumn("vlr_trfdo");		
		int rzn_sclPrestacion   		= result.findColumn("rzn_scl");
		int cdgo_tpo_idntfccnPrestacion = result.findColumn("cdgo_tpo_idntfccn");
		int nmro_idntfccnPrestacion   	= result.findColumn("nmro_idntfccn");
		 
		lstPrestadoresDireccionamiento = new ArrayList<PrestadorVO>();

		
		while(result.next()){ 
			
			prestador = new PrestadorVO();
			tiposIdentificacionVO = new TiposIdentificacionVO(null, result.getString(cdgo_tpo_idntfccnPrestacion), null);
			
			prestador.setTiposIdentificacionPrestadorVO(tiposIdentificacionVO);
			prestador.setCodigoInterno(result.getString(cdgo_intrnoPrestacion));
			prestador.setNombreSucursal(result.getString(nmbre_scrslPrestacion));
			prestador.setDireccionPrestador(result.getString(drccnPrestacion));
			prestador.setValor(result.getInt(vlr_trfdoPrestacion));
			prestador.setRazonSocial(result.getString(rzn_sclPrestacion));
			prestador.setNumeroIdentificacionPrestador(result.getString(nmro_idntfccnPrestacion));
			
			lstPrestadoresDireccionamiento.add(prestador);			 
		}		
	}	 
	
	/**
	 * Get resultado
	 * @return
	 */
	public List<PrestadorVO> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

}
