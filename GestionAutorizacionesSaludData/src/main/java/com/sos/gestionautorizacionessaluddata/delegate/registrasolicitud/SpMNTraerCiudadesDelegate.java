package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpMNTraerCiudadesDelegate
 * Clase Delegate que consulta las ciudades por codigo o descripcion
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpMNTraerCiudadesDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar las ciudades */
	private List<CiudadVO> lCiudad;
	
	private String codigoCiudad;
	private String descripcionCiudad;
	private Date fechaConsulta;
	private String visibleUsuario = "S";
	
	public SpMNTraerCiudadesDelegate(java.util.Date fechaConsulta, String codigoCiudad, String descripcionCiudad){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoCiudad = codigoCiudad;
		this.descripcionCiudad = descripcionCiudad;		
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_CIUDADES"));
		
		if(this.codigoCiudad == null || ConstantesData.CADENA_VACIA.equals(this.codigoCiudad)){
		    stmt.setNull(1, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(1, this.codigoCiudad);
		}
		
		if(this.descripcionCiudad == null || ConstantesData.CADENA_VACIA.equals(this.descripcionCiudad)){
		    stmt.setNull(2, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(2, this.descripcionCiudad);
		}
		
		stmt.setDate(3, this.fechaConsulta);
		stmt.setString(4, visibleUsuario);
		stmt.setNull(5, java.sql.Types.INTEGER);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_cdd  = result.findColumn("cnsctvo_cdgo_cdd");
		int index_cdgo_cdd          = result.findColumn("cdgo_cdd");
		int index_dscrpcn_cdd       = result.findColumn("dscrpcn_cdd");
		int index_cnsctvo_cdgo_sde  = result.findColumn("cnsctvo_cdgo_sde");
		int index_cdgo_sde          = result.findColumn("cdgo_sde");
		int index_dscrpcn_sde       = result.findColumn("dscrpcn_sde");
		
		lCiudad = new ArrayList<CiudadVO>();
		
		while(result.next()){
			CiudadVO ciudadVO = new CiudadVO();
			ciudadVO.setConsecutivoCodigoCiudad(result.getInt(index_cnsctvo_cdgo_cdd));
			ciudadVO.setCodigoCiudad(result.getString(index_cdgo_cdd));
			ciudadVO.setDescripcionCiudad(result.getString(index_dscrpcn_cdd));
			ciudadVO.setConsecutivoCodigoSede(result.getInt(index_cnsctvo_cdgo_sde));
			ciudadVO.setCodigoSede(result.getString(index_cdgo_sde));
			ciudadVO.setDescripcionSede(result.getString(index_dscrpcn_sde));
			
			lCiudad.add(ciudadVO);
		}			
	}
	
	public List<CiudadVO> getlCiudad() {
		return lCiudad;
	}
}
