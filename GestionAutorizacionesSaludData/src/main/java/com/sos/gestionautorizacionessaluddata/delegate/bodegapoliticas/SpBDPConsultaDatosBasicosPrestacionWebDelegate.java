package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosBasicosBodegaPoliticaDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPConsultaDatosBasicosPrestacionWebDelegate Consulta los datos
 * basicos de la prestacion
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpBDPConsultaDatosBasicosPrestacionWebDelegate implements SQLDelegate {

	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private DatosBasicosBodegaPoliticaDTO datosBasicos;
	private Integer tipoPrestacion;
	private Integer codPrestacion;

	public SpBDPConsultaDatosBasicosPrestacionWebDelegate(Integer tipoPrestacion, Integer codPrestacion) {
		this.tipoPrestacion = tipoPrestacion;
		this.codPrestacion = codPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DATOS_BASICOS_PRESTACION));

		stmt.setInt(1, this.tipoPrestacion);
		stmt.setInt(2, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indiceCdgoInicioVigencia = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceCdgoFinVigencia = result.findColumn(ConstantesData.FIN_VIGENCIA);
		int indiceCdgoGenero = result.findColumn(ConstantesData.GENERO);
		int indiceCdgoNivel = result.findColumn(ConstantesData.NIVEL);
		int indiceCdgoCoberturaPos = result.findColumn(ConstantesData.COBERTURA_POS);
		int indiceCdgoCoberturaNoPos = result.findColumn(ConstantesData.COBERTURA_NO_POS);
		int indiceCdgoCoberturaPosCond = result.findColumn(ConstantesData.COBERTURA_POS_COND);
		int indiceCdgoCoberturaAutoF = result.findColumn(ConstantesData.COBERTURA_AUTO_F);

		while (result.next()) {
			datosBasicos = new DatosBasicosBodegaPoliticaDTO();
			datosBasicos.setInicioVigencia(result.getDate(indiceCdgoInicioVigencia));
			datosBasicos.setFinVigencia(result.getDate(indiceCdgoFinVigencia));
			datosBasicos.setGenero(result.getString(indiceCdgoGenero));
			datosBasicos.setNivel(result.getString(indiceCdgoNivel));

			datosBasicos
					.setCoberturaPos(result.getString(indiceCdgoCoberturaPos).equals(ConstantesData.S) ? true : false);
			datosBasicos.setCoberturaNoPos(
					result.getString(indiceCdgoCoberturaNoPos).equals(ConstantesData.S) ? true : false);
			datosBasicos.setCoberturaPosCond(
					result.getString(indiceCdgoCoberturaPosCond).equals(ConstantesData.S) ? true : false);
			datosBasicos.setCoberturaAutoF(
					result.getString(indiceCdgoCoberturaAutoF).equals(ConstantesData.S) ? true : false);
		}
	}

	public DatosBasicosBodegaPoliticaDTO getDatosBasicos() {
		return datosBasicos;
	}
}
