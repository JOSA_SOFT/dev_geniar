package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgoAfiliadoVO;


/**
 * Class SpMNBuscarRiesgosPacienteDelegate
 * Clase Delegate que consulta los riesgos asociados al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaRiesgosAfiliadoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza almacenar los riesgoos del afiliado*/	
	private List<RiesgoAfiliadoVO> lstRiesgoAfiliadoVO;
			
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_RIESGOSAFILIADO_DOM"));
		stmt.setDate(1,  new java.sql.Date(new Date().getTime())); 
		stmt.setString(2, null);
		stmt.setString(3, null);
		
		ResultSet result = stmt.executeQuery();
		 
		int cnsctvo_cdgo_chrte     	= result.findColumn("cnsctvo_cdgo_chrte");
		int cdgo_chrte  			= result.findColumn("cdgo_chrte");
		int dscrpcn_chrte     		= result.findColumn("dscrpcn_chrte");
		 
		lstRiesgoAfiliadoVO = new ArrayList<RiesgoAfiliadoVO>();
		RiesgoAfiliadoVO obj;
		while(result.next()){ 
			obj = new RiesgoAfiliadoVO();
			obj.setDescRiesgoDiagnostico(result.getString(dscrpcn_chrte));
			obj.setConsecutivoCodRiesgoDiagnostico(result.getInt(cnsctvo_cdgo_chrte));
			obj.setCodRiesgoDiagnostico(result.getInt(cdgo_chrte));
			 
			 lstRiesgoAfiliadoVO.add(obj);			 
		}		
	}	 
	
	public List<RiesgoAfiliadoVO> getLstRiesgoAfiliadoVO() {
		return lstRiesgoAfiliadoVO;
	}

}
