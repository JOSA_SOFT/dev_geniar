package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.GestionAutorizacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import java.sql.ResultSet;

import co.eps.sos.dataccess.SQLDelegate;

/**
* Class SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate
* Clase delegate que permite consultar la gestión de autorización realizada sobre las prestaciones devueltas
* @author ing. Jorge Rodriguez De León
* @version 13/12/2016
*/
public class SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate implements SQLDelegate{
	private Integer numeroSolicitud;
	private Integer consecutivoPrestacion;
	private GestionAutorizacionVO gestionAutorizacionVO;
	
	
	public SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate(Integer numeroSolicitud, Integer consecutivoPrestacion) {
		this.numeroSolicitud = numeroSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_GESTION_AUTORIZA_PRESTACIONES));
		
		stmt.setInt(1, this.numeroSolicitud);	
		stmt.setInt(2, this.consecutivoPrestacion);
		
		ResultSet result = stmt.executeQuery();
		
		int indice_nmro_atrzcn_prstcn_ips   = result.findColumn("nmro_atrzcn_prstcn_ips");
		int indice_cnsctvo_cdgo_csa_nvdd    = result.findColumn("cnsctvo_cdgo_csa_nvdd");
		int indice_obsrvcns     			= result.findColumn("obsrvcns");
		int indice_cdgo_intrno              = result.findColumn("cdgo_intrno");
		
		
		gestionAutorizacionVO = new GestionAutorizacionVO();
		if(result.next()){
			gestionAutorizacionVO.setCodigoInterno(result.getString(indice_cdgo_intrno));
			gestionAutorizacionVO.setConsecutivoCodigoCausaNovedad(result.getInt(indice_cnsctvo_cdgo_csa_nvdd));
			gestionAutorizacionVO.setNumeroAtorizacionPrestacionIPS(result.getString(indice_nmro_atrzcn_prstcn_ips));
			gestionAutorizacionVO.setObservaciones(result.getString(indice_obsrvcns));
			
		}

	}
	public GestionAutorizacionVO getGestionAutorizacionVO() {
		return gestionAutorizacionVO;
	}
	
	

	
	
	

}
