package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate
 * Clase Sp de Creación Solicitudes Con Programación de Entrega
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate implements SQLDelegate{
	
	/** Consecutivo programacion **/
	private Integer consCodProgramacionFecEvento;
	/** Usuario**/
	private String usuario;
	/** Cons Solicitud**/
	private Integer consSolicitud;
	
	/**
	 * @param consCodProgramacionFecEvento
	 * @param usuario
	 */
	public SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate(Integer consCodProgramacionFecEvento, String usuario){
		this.consCodProgramacionFecEvento = consCodProgramacionFecEvento;
		this.usuario = usuario;
	}
		
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA));
		statement.setInt(1, this.consCodProgramacionFecEvento);
		statement.setString(2, this.usuario);
		statement.registerOutParameter(3, java.sql.Types.INTEGER);
		statement.registerOutParameter(4, java.sql.Types.INTEGER);
		statement.registerOutParameter(5, java.sql.Types.VARCHAR);
		statement.executeUpdate();

		Integer resultado = statement.getInt(4);
		String  mensaje = statement.getString(5);
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
		consSolicitud = statement.getInt(3);
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getSolicitud() {
		return consSolicitud;
	}

}
