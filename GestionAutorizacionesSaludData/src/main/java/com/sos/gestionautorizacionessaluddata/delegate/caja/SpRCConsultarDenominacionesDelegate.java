package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DenominacionVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Consulta denominaciones
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarDenominacionesDelegate implements SQLDelegate {

	private List<DenominacionVO> listDenominaciones;
	private Integer consecutivoTipoDenominacion;

	/**
	 * Recibe el consecutivo del tipo denominacion a consultar
	 * 
	 * @param consecutivoTipoDenominacion
	 */
	public SpRCConsultarDenominacionesDelegate(Integer consecutivoTipoDenominacion) {
		super();
		this.listDenominaciones = new ArrayList<DenominacionVO>();
		this.consecutivoTipoDenominacion = consecutivoTipoDenominacion;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCConsultarDenominaciones
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_DENOMINACIONES));

		stmt.setInt(1, this.consecutivoTipoDenominacion);
		ResultSet result = stmt.executeQuery();

		int dscrpcnDnmncn = result.findColumn("dscrpcn_dnmncn");

		while (result.next()) {
			DenominacionVO denominacionVO = new DenominacionVO();
			denominacionVO.setDenominacion(Double.parseDouble(result
					.getString(dscrpcnDnmncn)));
			listDenominaciones.add(denominacionVO);
		}
	}

	public List<DenominacionVO> getListDenominaciones() {
		return listDenominaciones;
	}
}