package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASValidarUsuarioAnulaSolicitudyOpsDelegate
 * Clase delegate que permite validar si el usuario puede o no anular y mod solicitidues
 * @author Julian Hernandez
 * @version 03/05/2016
 */
public class SpASValidarUsuarioAnulaSolicitudyOpsDelegate implements SQLDelegate{
	
	/** Permiso o no de anular  */
	private boolean permisoAnularSolicitudOPS;	
	
	/** usuario **/
	private String usuario;
	
	public SpASValidarUsuarioAnulaSolicitudyOpsDelegate(String usuario){
		this.usuario = usuario;
	}	

	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PERMISO_ANULAR_SOLICITUD_OPS));	
		
		stmt.setString(1, this.usuario);	
		
		ResultSet result = stmt.executeQuery();
		
		int atrzdo         			= result.findColumn("atrzdo");		
		
		permisoAnularSolicitudOPS = false;
		while(result.next()){
			if (ConstantesData.S.equals(result.getString(atrzdo))) {
				permisoAnularSolicitudOPS = true;
				break;
			}
			
		}		
	}
	
	public boolean getPermisoAnularSolicitudOPS() {
		return permisoAnularSolicitudOPS;
	}
}
