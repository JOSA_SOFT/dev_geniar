package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;

/**
 * Class SpASGestionContratacionParametros
 * Clase Sp para consulta del direccionamiento
 * @author ing. Rafael Cano
 * @version 24/02/2016
 *
 */

public class SpASConsultaDatosInformacionEstadosGestionAuditorDelegate implements SQLDelegate {
	private List<SelectItem> listaEstados;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_ESTADOS"));

		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_estdo_srvco_slctdo");
		int indice_estado = result.findColumn("dscrpcn_estdo_srvco_slctdo");

		listaEstados = new ArrayList<SelectItem>();
				
		while(result.next()){
			SelectItem item = new SelectItem();
			item.setValue(result.getInt(indice_consecutivo));
			item.setLabel(result.getString(indice_estado));
			
			listaEstados.add(item);
		}
	}

	public List<SelectItem> getListaEstados() {
		return listaEstados;
	}

}
