package com.sos.gestionautorizacionessaluddata.delegate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.eps.sos.dataccess.SQLDelegate;
import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.ModuloVO;
import com.sos.gestionautorizacionessaluddata.model.ServicioVO;


/**
 * Class MenuDelegate
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class MenuDelegate implements SQLDelegate {
	/** Usuario para el cual se cargará el menu */
	String usr;
	/** Modulos a lops cuales tiene acceso el usuario y los cuales se pintaran en el menú  */
	private List<ModuloVO> lModulos;
	private String codigoModulo;

	public MenuDelegate(String usr,String codigoModulo){
		this.usr = usr;
		this.codigoModulo = codigoModulo;
		lModulos = new ArrayList<ModuloVO>();
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_MENU"));
		stmt.setString(1, usr);
		stmt.setString(2, codigoModulo);

		Map<String, ModuloVO> modulos = new HashMap<String, ModuloVO>();
		ResultSet result = stmt.executeQuery();
		while(result.next()){
			String codModulo  =  result.getString(3);
			ModuloVO modulo = modulos.get(codModulo);
			if(modulo == null){
				modulo = new ModuloVO();
				modulo.setNombre(result.getString(4));
				modulo.setServicios(new ArrayList<ServicioVO>());
				modulo.setConsecutivoCodigoPerfil(result.getInt(5));
				modulos.put(codModulo, modulo);
				lModulos.add(modulo);
			}
			ServicioVO servicio = new ServicioVO();
			servicio.setLabel(result.getString(2));
			servicio.setName(result.getString(1));
			servicio.setAction(result.getString(1));
			servicio.setDeshabilitado(false);
			modulo.getServicios().add(servicio);
		}
	}

	
	/**
	 * Obtiene los modulos que se pintaran en el menú.
	 * 
	 * @param 
	 * @return List
	 */
	public List<ModuloVO> getModulos() {
		return lModulos;
	}

}
