package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarModificacionesOPSDelegate
 * Clase Guarda la modificacion de la ops direccionamiento
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpASGuardarModificacionesOPSDelegate implements SQLDelegate{
	
	/** Consecutivo solicitud **/
	private Integer consecutivoSolicitud;
	/** Numero unico de autorizacion**/
	private Integer numUnicoAutorizacion;
	/** Codigo interno prestador**/
	private String codInternoPrestador;
	/** Consecutivo causa**/
	private Integer consecutivoCodMotivoCausa;
	/** Observacion**/
	private String observacion;
	/** Usuario**/
	private String usuario;
	
	private ResultSet result;
	
	/**
	 * Construccion
	 * @param consecutivoSolicitud
	 * @param numUnicoAutorizacion
	 * @param codInternoPrestador
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 */
	public SpASGuardarModificacionesOPSDelegate(Integer consecutivoSolicitud, Integer numUnicoAutorizacion, String codInternoPrestador, Integer consecutivoCodMotivoCausa,
			String observacion, String usuario){
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.numUnicoAutorizacion = numUnicoAutorizacion;
		this.codInternoPrestador = codInternoPrestador.trim();
		this.consecutivoCodMotivoCausa = consecutivoCodMotivoCausa;
		this.observacion = observacion;
		this.usuario = usuario;
	}
		
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_MOD_OPS));
		statement.setInt(1, this.consecutivoSolicitud);
		statement.setInt(2, this.numUnicoAutorizacion);
		statement.setString(3, this.codInternoPrestador);
		statement.setString(4, this.usuario);
		statement.setInt(5, this.consecutivoCodMotivoCausa);
		statement.setString(6, this.observacion);
		
		statement.registerOutParameter(7, java.sql.Types.INTEGER);
		statement.registerOutParameter(8, java.sql.Types.VARCHAR);

		result = statement.executeQuery();

		Integer resultado = statement.getInt(7);
		String  mensaje = statement.getString(8);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}
	
	public boolean getRetornoResultado() throws SQLException {
		boolean tieneResultado = false;
		if(result != null)
			tieneResultado = result.next();
		return tieneResultado;
	}

}
