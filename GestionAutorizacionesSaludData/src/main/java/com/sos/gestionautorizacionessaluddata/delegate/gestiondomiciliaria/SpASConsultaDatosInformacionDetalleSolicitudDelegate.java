package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.EspecialidadVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASConsultarDetalleSolicitudDelegate
 * Clase Delegate que consultar el detalle de la solicitud
 * @author Julian Hernandez
 * @version 11/03/2016
 *
 */

public class SpASConsultaDatosInformacionDetalleSolicitudDelegate implements SQLDelegate{
	
	private int numeroSolicitud;
	private SolicitudBpmVO solicitud;
	
	public SpASConsultaDatosInformacionDetalleSolicitudDelegate(int numeroSolicitud){
		this.numeroSolicitud = numeroSolicitud;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_DETALLE_BPM));	
		stmt.setInt(1, this.numeroSolicitud);
		
		ResultSet result = stmt.executeQuery();
		
		int cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = result.findColumn("cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte");
		int cdgo_tpo_idntfccn = result.findColumn("cdgo_tpo_idntfccn");
		int nmro_idntfccn_mdco_trtnte = result.findColumn("nmro_idntfccn_mdco_trtnte");
		int rgstro_mdco_trtnte = result.findColumn("rgstro_mdco_trtnte");
		int prmr_nmbre_afldo = result.findColumn("prmr_nmbre_afldo");
		int sgndo_nmbre_afldo = result.findColumn("sgndo_nmbre_afldo");
		int prmr_aplldo = result.findColumn("prmr_aplldo");
		int sgndo_aplldo = result.findColumn("sgndo_aplldo");
		int dscrpcn_espcldd = result.findColumn("dscrpcn_espcldd");
		
		int cnsctvo_cdgo_tpo_idntfccn_ips_slctnte = result.findColumn("cnsctvo_cdgo_tpo_idntfccn_ips_slctnte");
		int cdgo_tpo_idntfccn_ips = result.findColumn("cdgo_tpo_idntfccn_ips");
		int nmro_idntfccn_ips_slctnte = result.findColumn("nmro_idntfccn_ips_slctnte");
		int cdgo_intrno = result.findColumn("cdgo_intrno");
		int nmbre_scrsl = result.findColumn("nmbre_scrsl");
		int rzn_scl = result.findColumn("rzn_scl");
		int dscrpcn_cdd = result.findColumn("dscrpcn_cdd");
		
		solicitud = new SolicitudBpmVO();
		MedicoVO medico = new MedicoVO();
		PrestadorVO prestador = new PrestadorVO();
		EspecialidadVO especialidad = new EspecialidadVO();
		CiudadVO ciudad = new CiudadVO();
		
		while(result.next()){
			
			medico.setTiposIdentificacionMedicoVO(new TiposIdentificacionVO(result.getInt(cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte), 
						result.getString(cdgo_tpo_idntfccn).trim(), result.getString(nmro_idntfccn_mdco_trtnte).trim()));
			medico.setRegistroMedico(result.getString(rgstro_mdco_trtnte));
			medico.setPrimerNombreMedico(result.getString(prmr_nmbre_afldo));
			medico.setSegundoNombreMedico(result.getString(sgndo_nmbre_afldo));
			medico.setPrimerApellidoMedico(result.getString(prmr_aplldo));
			medico.setSegundoApellidoMedico(result.getString(sgndo_aplldo));
			
			especialidad.setDescripcionEspecialidad(result.getString(dscrpcn_espcldd));
			medico.setEspecialidadMedicoVO(especialidad);
		
			prestador.setTiposIdentificacionPrestadorVO(new TiposIdentificacionVO(result.getInt(cnsctvo_cdgo_tpo_idntfccn_ips_slctnte), 
						result.getString(cdgo_tpo_idntfccn_ips).trim(), result.getString(nmro_idntfccn_ips_slctnte).trim() ));
			prestador.setCodigoInterno(result.getString(cdgo_intrno));
			prestador.setNombreSucursal(result.getString(nmbre_scrsl));
			prestador.setRazonSocial(result.getString(rzn_scl));
			
			ciudad.setDescripcionCiudad(result.getString(dscrpcn_cdd));
			prestador.setCiudadVO(ciudad);
			
			solicitud.setMedicoSolicitante(medico);
			solicitud.setPrestadorSolicitante(prestador);
		}

	}
	
	public SolicitudBpmVO getDetalleSolicitudBpmVO() {
		return solicitud;
	}
}
