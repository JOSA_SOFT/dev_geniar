
package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PrestacionPorPlanesDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpBDPConsultaPrestacionPorPlanesWebDelegateConsulta la prestacion por
 * planes
 * 
 * @author Jose Soto
 * @version 15/03/2017
 */
public class SpBDPConsultaPrestacionPorPlanesWebDelegate implements SQLDelegate {

	/**
	 * objeto que se utiliza para almacenar el resultado
	 */
	private List<PrestacionPorPlanesDTO> datoList;
	private Integer tipoPrestacion;
	private Integer codPrestacion;

	public SpBDPConsultaPrestacionPorPlanesWebDelegate(Integer tipoPrestacion, Integer codPrestacion) {
		this.tipoPrestacion = tipoPrestacion;
		this.codPrestacion = codPrestacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACION_POR_PLANES));

		stmt.setInt(1, this.tipoPrestacion);
		stmt.setInt(2, this.codPrestacion);
		ResultSet result = stmt.executeQuery();

		int indicePlan = result.findColumn(ConstantesData.CODIGO_PLAN);
		int indiceCoberturaPorPlan = result.findColumn(ConstantesData.COBERTURA_POR_PLAN);
		int indiceSemanasDeCarencia = result.findColumn(ConstantesData.SEMANAS_CARENCIA);
		int indiceCantidadMaximaPermitida = result.findColumn(ConstantesData.CANTIDAD_MAXIMA_PERMITIDA);
		int indiceFrecuenciaEntrega = result.findColumn(ConstantesData.FRECUENCIA_ENTREGA);
		int indiceTopeMaximoporOrden = result.findColumn(ConstantesData.TOPE_MAXIMO_POR_ORDEN);
		int indiceVigenciaAutorizacionMedica = result.findColumn(ConstantesData.VIGENCIA_AUTORIZACION_MEDICA);
		int indiceEdadMinima = result.findColumn(ConstantesData.EDAD_MINIMA);
		int indiceEdadMaxima = result.findColumn(ConstantesData.EDAD_MAXIMA);
		int indiceInicioVigencia = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceFinVigencia = result.findColumn(ConstantesData.INICIO_VIGENCIA);

		datoList = new ArrayList<PrestacionPorPlanesDTO>();
		while (result.next()) {

			PrestacionPorPlanesDTO datos = new PrestacionPorPlanesDTO();
			datos.setPlan(result.getString(indicePlan));
			datos.setCoberturaPorPlan(result.getString(indiceCoberturaPorPlan));
			datos.setSemanasDeCarencia(result.getInt(indiceSemanasDeCarencia));
			datos.setCantidadMaximaPermitida(result.getString(indiceCantidadMaximaPermitida));
			datos.setFrecuenciaEntrega(result.getInt(indiceFrecuenciaEntrega));
			datos.setTopeMaximoporOrden(result.getInt(indiceTopeMaximoporOrden));
			datos.setVigenciaAutorizacionMedica(result.getString(indiceVigenciaAutorizacionMedica));
			datos.setEdadMinima(result.getInt(indiceEdadMinima));
			datos.setEdadMaxima(result.getInt(indiceEdadMaxima));
			datos.setInicioVigencia(result.getDate(indiceInicioVigencia));
			datos.setFinVigencia(result.getDate(indiceFinVigencia));
			datoList.add(datos);
		}

	}

	public List<PrestacionPorPlanesDTO> getDatos() {
		return datoList;
	}
}
