package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoServicioVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpPMTraerTipoServicioSolicitado
 * Clase Delegate que consulta los tipos de servicio de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpPMTraerTipoServicioSolicitadoDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los tipos de servicio de la atencion */	
	private List<TipoServicioVO> lTipoServicio;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_TIPOSSERVICIOS"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_srvco_slctdo = result.findColumn("cnsctvo_cdgo_tpo_srvco_slctdo");
		int index_dscrpcn_tpo_srvco_slctdo      = result.findColumn("dscrpcn_tpo_srvco_slctdo");
		
		lTipoServicio = new ArrayList<TipoServicioVO>();
		
		while(result.next()){
			TipoServicioVO tipoServicioVO  = new TipoServicioVO();
			tipoServicioVO.setConsecutivoTipoServicio(result.getInt(index_cnsctvo_cdgo_tpo_srvco_slctdo));
			tipoServicioVO.setDescripcionTipoServicio(result.getString(index_dscrpcn_tpo_srvco_slctdo));
			
			lTipoServicio.add(tipoServicioVO);
		}		
	}
	
	public List<TipoServicioVO> getlTipoServicio() {
		return lTipoServicio;
	}
}
