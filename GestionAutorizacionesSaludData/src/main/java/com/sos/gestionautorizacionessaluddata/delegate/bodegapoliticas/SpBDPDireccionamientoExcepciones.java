package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoExcepcionesDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPDireccionamientoExcepciones implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<DireccionamientoExcepcionesDTO> lDto;

	private Integer cnsctvoCdfccn;

	public SpBDPDireccionamientoExcepciones(Integer cnsctvoCdfccn) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DIRECCIONAMIENTO_EXCEPCION));

		stmt.setInt(1, cnsctvoCdfccn);
		ResultSet result = stmt.executeQuery();

		int indiceCdgoIntrno = result.findColumn("cdgo_intrno");
		int indiceNmbreScrsl = result.findColumn("nmbre_scrsl");
		int indiceDscrpcSde = result.findColumn("dscrpcn_sde");
		int indiceDscrpcnZna = result.findColumn("dscrpcn_zna");
		int indiceEddMnma = result.findColumn("edd_mnma");
		int indiceEddMxma = result.findColumn("edd_mxma");
		int indiceCnsctvoCdgoDgnstco = result.findColumn("cnsctvo_cdgo_dgnstco");
		int indiceDscrpcnDgnstco = result.findColumn("dscrpcn_dgnstco");
		int indiceIncoVgnca = result.findColumn("inco_vgnca");
		int indiceFnVgnca = result.findColumn("fn_vgnca");

		lDto = new ArrayList<DireccionamientoExcepcionesDTO>();

		while (result.next()) {

			DireccionamientoExcepcionesDTO dto = new DireccionamientoExcepcionesDTO();
			dto.setCdgoIntrno(result.getString(indiceCdgoIntrno));
			dto.setNmbreScrsl(result.getString(indiceNmbreScrsl));
			dto.setDscrpcnSde(result.getString(indiceDscrpcSde));
			dto.setDscrpcnZna(result.getString(indiceDscrpcnZna));
			dto.setEddMnma(result.getInt(indiceEddMnma));
			dto.setEddMxma(result.getInt(indiceEddMxma));
			dto.setCnsctvoCdgoDgnstco(result.getInt(indiceCnsctvoCdgoDgnstco));
			dto.setDscrpcnDgnstco(result.getString(indiceDscrpcnDgnstco));
			dto.setIncoVgnca(result.getDate(indiceIncoVgnca));
			dto.setFnVgnca(result.getDate(indiceFnVgnca));
			lDto.add(dto);
		}
	}

	public List<DireccionamientoExcepcionesDTO> getlDto() {
		return lDto;
	}
}
