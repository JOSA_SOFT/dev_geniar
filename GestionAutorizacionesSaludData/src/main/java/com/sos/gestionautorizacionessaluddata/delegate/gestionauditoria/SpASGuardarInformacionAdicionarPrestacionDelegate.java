package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpASGuardarInformacionAdicionarPrestacionDelegate
 * Clase guarda la informacion de una nueva prestacion
 * @author Rafael Cano
 * @version 18/05/2016
 *
 */
public class SpASGuardarInformacionAdicionarPrestacionDelegate implements SQLDelegate{
	
	PrestacionesAprobadasVO prestacionNueva;
	int numeroSolicitud;
	String userBpm;
	
	public SpASGuardarInformacionAdicionarPrestacionDelegate(PrestacionesAprobadasVO prestacionNueva, int numeroSolicitud, String userBpm){
		this.prestacionNueva = prestacionNueva;
		this.numeroSolicitud = numeroSolicitud;
		this.userBpm = userBpm;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement call = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_ADICIONAR_PRESTACION));
		call.setInt(1, Utilidades.validarNullNumero(prestacionNueva.getConsTipoPrestacion()));
		call.setString(2, Utilidades.validarNullCadena(prestacionNueva.getCodigoPrestacion()));
		call.setString(3, Utilidades.validarNullCadena(prestacionNueva.getLateralidad()));
		call.setString(4,Utilidades.validarNullCadena(prestacionNueva.getViaAcceso()));
		call.setString(5, Utilidades.validarNullCadena(prestacionNueva.getCantidad()));
		call.setString(6, Utilidades.validarNullCadena(prestacionNueva.getCantidadDosis()));
		call.setInt(7, Utilidades.validarNullNumero(prestacionNueva.getConsecutivoCodigoDosis()));
		call.setString(8, Utilidades.validarNullCadena(prestacionNueva.getFrecuencia()));
		call.setInt(9, Utilidades.validarNullNumero(prestacionNueva.getConsecutivoFrecuencia()));
		call.setInt(10, Utilidades.validarNullNumero(prestacionNueva.getConsecutivoMotivo()));
		call.setString(11, Utilidades.validarNullCadena(prestacionNueva.getObservaciones()));
		call.setInt(12, Utilidades.validarNullNumero(numeroSolicitud));
		call.setString(13, Utilidades.validarNullCadena(userBpm));
		call.registerOutParameter(14, java.sql.Types.INTEGER);
		call.registerOutParameter(15, java.sql.Types.VARCHAR);

		call.executeUpdate();

		Integer codResultado = call.getInt(14);
		String msgResultado = call.getString(15);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
