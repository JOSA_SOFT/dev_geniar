package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.GrupoEntregaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpTraerGruposEntregaDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar los grupos de entrega */
	private List<GrupoEntregaVO> lGruposEntrega;

	private Date fechaActual;

	public SpTraerGruposEntregaDelegate(java.util.Date fechaActual) {
		this.fechaActual = new Date(fechaActual.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaParametrosSp.getString(ConstantesData.CONSULTA_GRUPOS_ENTREGA));

		if (this.fechaActual == null) {
			stmt.setNull(1, java.sql.Types.DATE);
		} else {
			stmt.setDate(1, this.fechaActual);
		}

		ResultSet result = stmt.executeQuery();

		int consecutivoCodigoGrupoEntrega = result.findColumn(ConstantesData.CONSECUTIVO_CODIGO_GRUPO_ENTREGA);
		int codigoGrupoEntrega = result.findColumn(ConstantesData.CODIGO_GRUPO_ENTREGA);
		int descripcionGrupoEntrega = result.findColumn(ConstantesData.DESCRIPCION_GRUPO_ENTREGA);

		lGruposEntrega = new ArrayList<GrupoEntregaVO>();

		while (result.next()) {
			GrupoEntregaVO grupoEntregaVO = new GrupoEntregaVO();
			grupoEntregaVO.setConsecutivoCodigoGrupoEntrega(result.getInt(consecutivoCodigoGrupoEntrega));
			grupoEntregaVO.setCodigoGrupoEntrega(result.getString(codigoGrupoEntrega));
			grupoEntregaVO.setDescripcionGrupoEntrega(result.getString(descripcionGrupoEntrega));

			lGruposEntrega.add(grupoEntregaVO);
		}

		stmt.close();
	}

	public List<GrupoEntregaVO> getlGruposEntrega() {
		return lGruposEntrega;
	}

}
