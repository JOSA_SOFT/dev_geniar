package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;


/**
 * Class SpASGuardarInformacionModificacionRecobroDelegate
 * Clase que permite ejecutar el guardado de la modificacion de recobro
 * @author Carlos Andres Lopez Ramirez
 * @version	2017/04/20
 */
public class SpASGuardarInformacionModificacionRecobroDelegate implements SQLDelegate {
	
	private Integer numeroSolicitud; 
	private Integer consecutivoContingenciaRecobro; 
	private Integer consecutivoCodigoContingencia; 
	private String observaciones; 
	private String usuario;
	private int resultado;
	
	public SpASGuardarInformacionModificacionRecobroDelegate(Integer numeroSolicitud, Integer consecutivoContingenciaRecobro, 
			 												 Integer consecutivoCodigoContingencia, String observaciones, String usuario) {
		this.numeroSolicitud = numeroSolicitud;
		this.consecutivoCodigoContingencia = consecutivoCodigoContingencia;
		this.consecutivoContingenciaRecobro = consecutivoContingenciaRecobro;
		this.observaciones = observaciones;
		this.usuario = usuario;
	}
	
	
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_MODIFICACION_RECOBRO));
		
		statement.setInt(1, this.numeroSolicitud);
		statement.setInt(2, this.consecutivoContingenciaRecobro);
		statement.setString(3, this.usuario);
		statement.setInt(4, this.consecutivoCodigoContingencia);
		statement.setString(5, this.observaciones);

		statement.registerOutParameter(6, java.sql.Types.INTEGER);
		statement.registerOutParameter(7, java.sql.Types.VARCHAR);
		
		statement.executeUpdate();
		
		resultado = statement.getInt(6);
		String  mensaje = statement.getString(7);
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
		
	}

	
	
	public int getResultado() {
		return resultado;
	}



	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

}
