package com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarInformacionGestionDomiciliariaDelegate
 * Clase guarda la informacion correspondiente a la gestion domiciliaria
 * @author Julian Hernandez
 * @version 22/03/2016
 *
 */
public class SpASAsociarProgramacionEntregaDelegate implements SQLDelegate{
	
	
	private Integer numeroPrestacion;
	private Integer consecutivoProgramacion;
	private Integer consTipoIdentificacionAfiliado;
	private String numeroIdentificacionAfiliado;
	private String usuario;
	
	public SpASAsociarProgramacionEntregaDelegate(Integer numeroPrestacion, Integer consecutivoProgramacion, Integer consTipoIdentificacionAfiliado, String numeroIdentificacionAfiliado, String  usuario){
		this.numeroPrestacion = numeroPrestacion;
		this.consecutivoProgramacion = consecutivoProgramacion;
		this.consTipoIdentificacionAfiliado = consTipoIdentificacionAfiliado;
		this.numeroIdentificacionAfiliado = numeroIdentificacionAfiliado;
		this.usuario = usuario;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  callableStatement = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_ASOCIAR_PROGRAMACION));
		callableStatement.setInt(1, this.numeroPrestacion);
		callableStatement.setInt(2, this.consecutivoProgramacion);
		callableStatement.setInt(3, this.consTipoIdentificacionAfiliado);
		callableStatement.setString(4, this.numeroIdentificacionAfiliado);
		callableStatement.setString(5, this.usuario);
		callableStatement.registerOutParameter(6, java.sql.Types.INTEGER);
		callableStatement.registerOutParameter(7, java.sql.Types.VARCHAR);

		callableStatement.executeUpdate();

		Integer codResultado = callableStatement.getInt(6);
		String msgResultado = callableStatement.getString(7);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
