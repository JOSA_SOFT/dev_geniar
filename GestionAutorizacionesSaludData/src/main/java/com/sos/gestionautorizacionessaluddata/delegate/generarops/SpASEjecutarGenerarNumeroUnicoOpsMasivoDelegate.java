package com.sos.gestionautorizacionessaluddata.delegate.generarops;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate
 * Clase delegate que permite generar los numero unicos de ops
 * asociados a una solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/04/2016
 */
public class SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate implements SQLDelegate{
	
	/** Objeto que se utiliza para almacenar el resultado del procedimiento almacenado */
	private ServiceErrorVO serviceError;
	
	private Integer consecutivoSolicitud;
	private String usuarioSession;
	
	public SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate(Integer consecutivoSolicitud, String usuarioSession){
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.usuarioSession = usuarioSession;
	}
	

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		CallableStatement stmt = conn.prepareCall(ConsultaSp.getString(ConstantesData.EJECUTA_NUMERO_UNICO_OPS));
		
		stmt.setInt(1, this.consecutivoSolicitud);
		stmt.setString(2, this.usuarioSession);
		stmt.registerOutParameter(3, Types.VARCHAR);
		stmt.registerOutParameter(4, Types.VARCHAR);
		
		stmt.executeUpdate();
		
		serviceError = new ServiceErrorVO();
		serviceError.setCodigoError(stmt.getString(4));
		serviceError.setMensajeError(stmt.getString(3));					
	}
	
	public ServiceErrorVO getServiceError() {
		return serviceError;
	}
}
