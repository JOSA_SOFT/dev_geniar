package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.OrigenAtencionVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpPMTraerOrigenAtencionDelegate
 * Clase Delegate que consulta el Origen de la Atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */
public class SpPMTraerOrigenAtencionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar el origen de la atencion */	
	private List<OrigenAtencionVO> lOrigenAtencion;
	private Date fechaConsulta;
	private String visibleUsuario = "N";
	
	public SpPMTraerOrigenAtencionDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_ORIGENATENCION"));
		stmt.setNull(1, java.sql.Types.VARCHAR);
		stmt.setNull(2, java.sql.Types.VARCHAR);
		stmt.setDate(3, this.fechaConsulta);
		stmt.setString(4, visibleUsuario);
				
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_orgn_atncn = result.findColumn("cnsctvo_cdgo_orgn_atncn");
		int index_cdgo_orgn_atncn         = result.findColumn("cdgo_orgn_atncn");
		int index_dscrpcn_orgn_atncn      = result.findColumn("dscrpcn_orgn_atncn");
		
		lOrigenAtencion = new ArrayList<OrigenAtencionVO>();
				
		while(result.next()){
			OrigenAtencionVO atencionVO = new OrigenAtencionVO();
			atencionVO.setConsecutivoOrigenAtencion(result.getInt(index_cnsctvo_cdgo_orgn_atncn));
			atencionVO.setCodigoOrigenAtencion(result.getString(index_cdgo_orgn_atncn));
			atencionVO.setDescripcionOrigenAtencion(result.getString(index_dscrpcn_orgn_atncn));
			
			lOrigenAtencion.add(atencionVO);			
		}			
	}
	
	public List<OrigenAtencionVO> getlOrigenAtencion() {
		return lOrigenAtencion;
	}
}
