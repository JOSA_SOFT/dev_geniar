package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosAdicionalesCumsDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPDatosAdicionalesCums implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private DatosAdicionalesCumsDTO dto;

	private Integer cnsctvoCdfccn;

	public SpBDPDatosAdicionalesCums(Integer cnsctvoCdfccn) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DATOS_CUMS));

		stmt.setInt(1, cnsctvoCdfccn);
		ResultSet result = stmt.executeQuery();
		
		int indiceCdgoPrsntcn = result.findColumn(ConstantesData.CODIGO_PRESENTACION);
		int indiceDscrpcnPrsntcn = result.findColumn(ConstantesData.DESCRIPCION_PRESENTACION);
		int indiceCntddPrsntcn = result.findColumn(ConstantesData.CANTIDAD_PRESENTACION);
		int indiceDscrpcnFrmaEntrga = result.findColumn(ConstantesData.DESCRIPCION_FORMA_ENTREGA);
		int indiceObsrvcnsPrsntcn = result.findColumn(ConstantesData.OBSERVACIONES_PRESENTACION);
		int indiceCdgoFrmaFrmctca = result.findColumn(ConstantesData.CODIGO_FORMA_FRMCTCA);
		int indiceDscrpcnFrmaFrmctca = result.findColumn(ConstantesData.DESCRIPCION_FORMA_FRMCTCA);
		int indiceCntddFrccnda = result.findColumn(ConstantesData.CANTIDAD_FRCCNDA);
		int indiceIndccnsInvma = result.findColumn(ConstantesData.INDCCNS_INVMA);
		int indiceCntrndccnsInvma = result.findColumn(ConstantesData.CNTRNDCCNS_INVIMA);
		int indiceCdgoVaAdmnstrcn = result.findColumn(ConstantesData.CODIGO_VA_ADMINISTRACION);
		int indiceDscrpcnVaAdmnstrcn = result.findColumn(ConstantesData.DESCRIPCION_VA_ADMINISTRACION);
		int indiceCdgoMdloLqdcnCncpto = result.findColumn(ConstantesData.CODIGO_MODULO_LIQUIDACION_CONCEPTO);
		int indiceDscrpcnMdloLqdcnCncpto = result.findColumn(ConstantesData.DESCRIPCION_MODULO_LIQUIDACION_CONCEPTO);

		if (result.next()) {

			dto = new DatosAdicionalesCumsDTO();
			dto.setCdgoPrsntcn(result.getString(indiceCdgoPrsntcn));
			dto.setDscrpcnPrsntcn(result.getString(indiceDscrpcnPrsntcn));
			dto.setCntddPrsntcn(result.getString(indiceCntddPrsntcn));
			dto.setDscrpcnFrmaEntrga(result.getString(indiceDscrpcnFrmaEntrga));
			dto.setObsrvcnsPrsntcn(result.getString(indiceObsrvcnsPrsntcn));
			dto.setCdgoFrmaFrmctca(result.getString(indiceCdgoFrmaFrmctca));
			dto.setDscrpcnFrmaFrmctca(result.getString(indiceDscrpcnFrmaFrmctca));
			dto.setCntddFrccnda(result.getString(indiceCntddFrccnda));
			dto.setIndccnsInvma(result.getString(indiceIndccnsInvma));
			dto.setCntrndccnsInvma(result.getString(indiceCntrndccnsInvma));
			dto.setCdgoVaAdmnstrcn(result.getString(indiceCdgoVaAdmnstrcn));
			dto.setDscrpcnVaAdmnstrcn(result.getString(indiceDscrpcnVaAdmnstrcn));
			dto.setCdgoMdloLqdcnCncpto(result.getString(indiceCdgoMdloLqdcnCncpto));
			dto.setDscrpcnMdloLqdcnCncpto(result.getString(indiceDscrpcnMdloLqdcnCncpto));

		}
	}

	public DatosAdicionalesCumsDTO getlDto() {
		return dto;
	}
}
