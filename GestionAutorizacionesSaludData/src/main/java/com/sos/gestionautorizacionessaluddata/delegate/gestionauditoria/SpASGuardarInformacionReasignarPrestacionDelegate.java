package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpASGuardarInformacionReasignarPrestacionDelegate
 * Clase guarda la reasignación de las prestaciones entre auditores
 * @author Jorge Rodriguez
 * @version 08/03/2017
 *
 */
public class SpASGuardarInformacionReasignarPrestacionDelegate implements SQLDelegate{
	
	/** Observacion**/
	private String observacion;
	/** Consecutivo prestacion**/
	private Integer consecutivoPrestacion;
	/** Usuario**/
	private String usuario;	
	/** descGrupoAuditor**/
	private String descGrupoAuditor;
	
	
	public SpASGuardarInformacionReasignarPrestacionDelegate(String observacion, Integer consecutivoPrestacion, String usuario, String descGrupoAuditor){
		this.observacion = observacion;
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.usuario = usuario;
		this.descGrupoAuditor = descGrupoAuditor;
	}
		
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement call = conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_REASIGNACION_PRESTACION));
		call.setInt(1, Utilidades.validarNullNumero(consecutivoPrestacion));
		call.setString(2, Utilidades.validarNullCadena(descGrupoAuditor));
		call.setString(3, Utilidades.validarNullCadena(observacion));
		call.setString(4, Utilidades.validarNullCadena(usuario));

		call.registerOutParameter(5, java.sql.Types.INTEGER);
		call.registerOutParameter(6, java.sql.Types.VARCHAR);

		call.executeUpdate();

		Integer codResultado = call.getInt(5);
		String msgResultado = call.getString(6);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
	}

}
