package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.NotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * DELEGATE OBTENER LAS NOTAS CREDITO DEL GRUPO FAMILIAR
 * 
 * @author JOSE OLMEDO SOTO AGUIRRE
 *
 */
public class SpRCObtenerNotasCreditosGrupoFamiliarDelegate implements SQLDelegate {
	private int nuiAfiliado;
	private NotasCreditoVO notasCredito;
	
	public SpRCObtenerNotasCreditosGrupoFamiliarDelegate(int nuiAfiliado) {
		super();
		this.nuiAfiliado = nuiAfiliado;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement stmt = conn.prepareCall(ConsultaSp
				.getString(ConstantesData.OBTENER_NOTAS_CREDITO));
				
		notasCredito = new NotasCreditoVO();
		List<DocumentoCajaVO> notas = new ArrayList<DocumentoCajaVO>(10);
		stmt.setInt(1, nuiAfiliado);
		stmt.registerOutParameter(2, Types.VARCHAR);
		
		ResultSet resultS = stmt.executeQuery();

		while (resultS.next()) {
			DocumentoCajaVO nota = new DocumentoCajaVO();
			nota.setFechaGeneracion(new Date(resultS.getDate(1).getTime()));
			nota.setIdentificacionAfiliado(resultS.getString(2));
			nota.setNombreAfiliado(resultS.getString(3));
			nota.setValorAfiliad(resultS.getDouble(4));
			nota.setNumNotaCredito(resultS.getInt(5));
			nota.setPkDocCaja(resultS.getInt(6));
			notas.add(nota);
		}
		
		notasCredito.setNotas(notas);
		notasCredito.setEntregaDinero(stmt.getString(2));
	}

	public NotasCreditoVO getNotasCredito() {
		return notasCredito;
	}
	
	
}
