package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpTraerTiposIdentificacionDelegate
 * Clase delegate que permite consultar los tipos de identificacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/12/2015
 *
 */

public class SpTraerTiposIdentificacionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar los tipos de identificacion */	
	private List<TiposIdentificacionVO> lTiposIdentificacion;
	
	private String visibleUsuario = "N";
	private Date fechaConsulta;
	
	
	public SpTraerTiposIdentificacionDelegate(java.util.Date fechaConsulta){
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
	}
		
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_TIPOS_IDENTIFICACION"));
		stmt.setDate(1, this.fechaConsulta);
		stmt.setNull(2, java.sql.Types.VARCHAR);
		stmt.setNull(3, java.sql.Types.VARCHAR);
		stmt.setNull(4, java.sql.Types.VARCHAR);
		stmt.setString(5, visibleUsuario);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_tpo_idntfccn = result.findColumn("cnsctvo_cdgo_tpo_idntfccn");
		int index_cdgo_tpo_idntfccn         = result.findColumn("cdgo_tpo_idntfccn");
		int index_dscrpcn_tpo_idntfccn      = result.findColumn("dscrpcn_tpo_idntfccn");
		
		lTiposIdentificacion = new ArrayList<TiposIdentificacionVO>();		
		
		while(result.next()){
			
			TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			
			tiposIdentificacionVO.setConsecutivoTipoIdentificacion(result.getInt(index_cnsctvo_cdgo_tpo_idntfccn));
			tiposIdentificacionVO.setCodigoTipoIdentificacion(result.getString(index_cdgo_tpo_idntfccn));
			tiposIdentificacionVO.setDescripcionTipoIdentificacion(result.getString(index_dscrpcn_tpo_idntfccn));
			
			lTiposIdentificacion.add(tiposIdentificacionVO);				
		}		
	}
	
	public List<TiposIdentificacionVO> getlTiposIdentificacion() {
		return lTiposIdentificacion;
	}
}
