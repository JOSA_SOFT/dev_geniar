package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Consulta un usuario del sistema
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCConsultarUsuarioWebDelegate implements SQLDelegate {

	private String usroCrcn;
	private Integer cnsctvoCdgoMdlo;
	private UsuarioWebVO usuarioWebVO;

	/**
	 * Recibe el usuarioLogin y el consecutivo del modulo para buscar el usuario
	 * web
	 * 
	 * @param usroCrcn
	 * @param cnsctvoCdgoMdlo
	 */
	public SpRCConsultarUsuarioWebDelegate(String usroCrcn, Integer cnsctvoCdgoMdlo) {
		super();
		this.usroCrcn = usroCrcn;
		this.cnsctvoCdgoMdlo = cnsctvoCdgoMdlo;
	}

	/**
	 * Ejecuta el procedimiento bdSeguridad.dbo.spVwConsultarUsuarioWeb
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTAR_USUARIO_WEB));
		stmt.setString(1, this.usroCrcn);
		stmt.setInt(2, this.cnsctvoCdgoMdlo);

		ResultSet result = stmt.executeQuery();

		int prmrNmbreUsro = result.findColumn("prmr_nmbre_usro");
		int sgndoNmbreUsro = result.findColumn("sgndo_nmbre_usro");
		int prmrAplldoUsro = result.findColumn("prmr_aplldo_usro");
		int sgndoAplldoUsro = result.findColumn("sgndo_aplldo_usro");
		int lgnUsro = result.findColumn("lgn_usro");
		int psswrdUsro = result.findColumn("psswrd_usro");
		int dscrpcnUsro = result.findColumn("dscrpcn_usro");
		int cnsctvoCdgoOfcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int eml = result.findColumn("eml");
		int cnsctvoCdgoTpoIdntfccnPrstdr = result
				.findColumn("cnsctvo_cdgo_tpo_idntfccn_prstdr");
		int nmroIdntfccnPrstdr = result.findColumn("nmro_idntfccn_prstdr");
		int cdgoIntrno = result.findColumn("cdgo_intrno");

		if (result.next()) {
			UsuarioWebVO usuarioWebVOTemp = new UsuarioWebVO();
			usuarioWebVOTemp.setPrmrNmbreUsro(result.getString(prmrNmbreUsro));
			usuarioWebVOTemp
					.setSgndoNmbreUsro(result.getString(sgndoNmbreUsro));
			usuarioWebVOTemp
					.setPrmrAplldoUsro(result.getString(prmrAplldoUsro));
			usuarioWebVOTemp.setSgndoAplldoUsro(result
					.getString(sgndoAplldoUsro));
			usuarioWebVOTemp.setLgnUsro(result.getString(lgnUsro));
			usuarioWebVOTemp.setPsswrdUsro(result.getString(psswrdUsro));
			usuarioWebVOTemp.setDscrpcnUsro(result.getString(dscrpcnUsro));
			usuarioWebVOTemp.setCnsctvoCdgoOfcna(result
					.getInt(cnsctvoCdgoOfcna));
			usuarioWebVOTemp.setEml(result.getString(eml));
			usuarioWebVOTemp.setCnsctvoCdgoTpoIdntfccnPrstdr(result
					.getInt(cnsctvoCdgoTpoIdntfccnPrstdr));
			usuarioWebVOTemp.setNmroIdntfccnPrstdr(result
					.getString(nmroIdntfccnPrstdr));
			usuarioWebVOTemp.setCdgoIntrno(result.getString(cdgoIntrno));
			this.usuarioWebVO = usuarioWebVOTemp;
		}

	}

	public UsuarioWebVO getUsuarioWebVO() {
		return usuarioWebVO;
	}
}