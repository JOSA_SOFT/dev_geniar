/**
 * 
 */
package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroReporteCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteCajaDetalladoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que se encarga de generar el reporte del sp
 * bdcna.cja.spRCConsultaInformesDetalle
 * 
 * @author GENIAR
 */
public class SpRCConsultaInformesDetalleDelegate implements SQLDelegate {

	ParametroReporteCierreCajaVO entrada;
	List<ReporteCajaDetalladoVO> resultado;

	/**
	 * Constructor de la clase
	 * 
	 * @param entrada
	 */
	public SpRCConsultaInformesDetalleDelegate(ParametroReporteCierreCajaVO entrada) {
		this.entrada = entrada;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.REPORTE_CAJA_DETALLADO));

		stmt.setDate(1, new Date(entrada.getFechaInicial().getTime()));
		
		if (entrada.getNui() == null){
			stmt.setNull(2, Types.INTEGER);
		}else{
			stmt.setInt(2, entrada.getNui());
		}
		
		if (entrada.getConsecutivoCodigoOficina() == null){
			stmt.setNull(3, Types.INTEGER);
		}else{
			stmt.setInt(3, entrada.getConsecutivoCodigoOficina());
		}
		
		if (entrada.getLogin() == null){
			stmt.setNull(4, Types.CHAR);
		}else{
			stmt.setString(4, entrada.getLogin());
		}
		
		if (entrada.getNumeroOps() == null){
			stmt.setNull(5, Types.INTEGER);
		}else{
			stmt.setInt(5, entrada.getNumeroOps());
		}
		
		if (entrada.getNumeroRecibo() == null){
			stmt.setNull(6, Types.INTEGER);
		}else{
			stmt.setInt(6, entrada.getNumeroRecibo());
		}
		
		if (entrada.getConsecutivoCodigoSede() == null){
			stmt.setNull(7, Types.INTEGER);
		}else{
			stmt.setInt(7, entrada.getConsecutivoCodigoSede());
		}
		
		ResultSet rs = stmt.executeQuery();

		resultado = new ArrayList<ReporteCajaDetalladoVO>();

		while (rs.next()) {
			ReporteCajaDetalladoVO reporte = new ReporteCajaDetalladoVO();
			reporte.setDescripcionOficina(rs.getString("dscrpcn_ofcna"));
			reporte.setDescripcionSede(rs.getString("dscrpcn_sde"));
			reporte.setFechaGeneracion(rs.getDate("fcha_gnrcn"));
			reporte.setNumeroRecibo(rs.getInt("nmro_rcbo"));
			reporte.setValorTotalRecibo(rs.getDouble("vlr_ttl_rcbo"));
			reporte.setNumeroOPS(rs.getString("nmro_unco_ops"));
			reporte.setAfiliado(rs.getString("dscrpcn_afldo"));
			reporte.setUsuario(rs.getString("usro"));
			resultado.add(reporte);
		}
	}

	public List<ReporteCajaDetalladoVO> getResultado() {
		return resultado;
	}

}
