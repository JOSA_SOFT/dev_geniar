package com.sos.gestionautorizacionessaluddata.delegate.programacionentrega;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpAsActualizarEstadoProgramacionEntregaDelegate
 * Clase Sp para actualizar Estado Programacion Entrega
 * @author Julian Hernandez
 * @version 29/04/2016
 *
 */
public class SpAsActualizarEstadoProgramacionEntregaDelegate implements SQLDelegate{
	
	/** Consecutivo consSolicitud **/
	private Integer consSolicitud;
	
	/** Consecutivo programacion **/
	private Integer consCodProgramacionFecEvento;
	
	/** Usuario**/
	private String usuario;
	
	/** Estado entregado **/
	private Integer estadoEntregado;
	
	/**
	 * @param consCodProgramacionFecEvento
	 * @param usuario
	 */
	public SpAsActualizarEstadoProgramacionEntregaDelegate(Integer consSolicitud, Integer consCodProgramacionFecEvento, String usuario, Integer estadoEntregado){
		this.consSolicitud = consSolicitud;
		this.consCodProgramacionFecEvento = consCodProgramacionFecEvento;
		this.usuario = usuario;
		this.estadoEntregado = estadoEntregado;
	}
		
	/*
	 * (non-Javadoc)
	 * @see co.eps.sos.dataccess.SQLDelegate#ejecutarData(java.sql.Connection)
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.ACTUALIZAR_ESTADO_PROGRAMACION_ENTREGA));
		statement.setInt(1, this.consSolicitud);
		statement.setInt(2, this.consCodProgramacionFecEvento);
		statement.setString(3, this.usuario);
		statement.setInt(4, this.estadoEntregado);
		statement.registerOutParameter(5, java.sql.Types.INTEGER);
		statement.registerOutParameter(6, java.sql.Types.VARCHAR);
		
		statement.executeUpdate();

		Integer resultado = statement.getInt(5);
		String  mensaje = statement.getString(6);		
		
		if (resultado < 0) {
			throw new DataAccessException(mensaje);
		}
	}
	
}
