package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;
import co.eps.sos.dataccess.exception.DataAccessException;

public class SpAsGuardarInformacionRecobroXPrestacionDelegate implements SQLDelegate {

	Integer consecutivoPrestacion; 
	Integer numSolicitud; 
	Integer consecutivoRecobro;
	String observacion; 
	String usuario; 
	Integer consecutivoContingencia;
	
	public SpAsGuardarInformacionRecobroXPrestacionDelegate(
			Integer consecutivoPrestacion, Integer numSolicitud,
			Integer consecutivoRecobro, String observacion, String usuario,
			Integer consecutivoContingencia) {
		this.consecutivoPrestacion = consecutivoPrestacion;
		this.numSolicitud = numSolicitud;
		this.consecutivoRecobro = consecutivoRecobro;
		this.observacion = observacion;
		this.usuario = usuario;
		this.consecutivoContingencia = consecutivoContingencia;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement statement= conn.prepareCall(ConsultaSp.getString(ConstantesData.GUARDAR_RECOBRO_X_PRESTACION));
		
		statement.setInt(1, numSolicitud);
		statement.setInt(2, consecutivoPrestacion);
		statement.setInt(3, consecutivoRecobro);
		statement.setString(4, observacion);
		statement.setInt(5, consecutivoContingencia);
		statement.setString(6, usuario);
		statement.registerOutParameter(7, java.sql.Types.INTEGER);
		statement.registerOutParameter(8, java.sql.Types.VARCHAR);
		
		statement.executeUpdate();
		
		Integer codResultado = statement.getInt(7);
		String msgResultado = statement.getString(8);
		
		if (codResultado<0) {
			throw new DataAccessException(msgResultado);
		}
		
		
	}

}
