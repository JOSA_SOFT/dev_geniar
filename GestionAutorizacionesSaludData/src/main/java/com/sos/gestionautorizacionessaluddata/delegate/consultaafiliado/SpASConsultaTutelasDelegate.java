package com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TutelasAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;

/**
 * Class SpSAConsultaTutelasDelegate
 * Clase Delegate que consulta las tutelas asociadas al afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class SpASConsultaTutelasDelegate implements SQLDelegate {
	
	/** Lista que se utiliza almacenar las tutelas del afiliado*/	
	private List<TutelasAfiliadoVO> lTutelasAfiliado;
	
	private int consecutivoTipoIdentificacion;
	private String numeroIdentificacion;
	
	public SpASConsultaTutelasDelegate(int consecutivoTipoIdentificacion, String numeroIdentificacion){
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
        PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_TUTELAS_AFILIADO"));
		
		stmt.setInt(1, this.consecutivoTipoIdentificacion);
		stmt.setString(2, this.numeroIdentificacion);
		
		 ResultSet result = stmt.executeQuery();
		 
		 int index_cnsctvo_ntfccn                = result.findColumn("cnsctvo_ntfccn");
		 int index_cnsctvo_cdgo_ofcna            = result.findColumn("cnsctvo_cdgo_ofcna");
		 int index_dscrpcn_ttla                  = result.findColumn("dscrpcn_ttla");
		 int index_cnsctvo_cdgo_estdo_ttla       = result.findColumn("cnsctvo_cdgo_estdo_ttla");
		 int index_dscrpcn_estdo_ttla            = result.findColumn("dscrpcn_estdo_ttla");
		 int index_cnsctvo_cdgo_estdo_fllo       = result.findColumn("cnsctvo_cdgo_estdo_fllo");
		 int index_dscrpcn_estdo_fllo            = result.findColumn("dscrpcn_estdo_fllo");
		 int index_rslve                         = result.findColumn("rslve");
		 int index_cnsctvo_cdgo_dgnstco          = result.findColumn("cnsctvo_cdgo_dgnstco");
		 int index_dscrpcn_dgnstco               = result.findColumn("dscrpcn_dgnstco");
		 int index_cnsctvo_cdgo_csa_estdo_ntfccn = result.findColumn("cnsctvo_cdgo_csa_estdo_ntfccn");
		 int index_fcha_ntfccn                   = result.findColumn("fcha_ntfccn");
		
		 lTutelasAfiliado = new ArrayList<TutelasAfiliadoVO>();
		 
		 while(result.next()){
			 TutelasAfiliadoVO tutelasAfiliadoVO = new TutelasAfiliadoVO();
			 DiagnosticosVO diagnosticosVO = new DiagnosticosVO();
			 
			 diagnosticosVO.setConsecutivoCodigoDiagnostico(result.getInt(index_cnsctvo_cdgo_dgnstco));
			 diagnosticosVO.setDescripcionDiagnostico(result.getString(index_dscrpcn_dgnstco));
			
			 tutelasAfiliadoVO.setConsecutivoNotificacion(result.getInt(index_cnsctvo_ntfccn));
			 tutelasAfiliadoVO.setConsecutivoCodigoOficina(result.getInt(index_cnsctvo_cdgo_ofcna));
			 tutelasAfiliadoVO.setDescripcionTutela(result.getString(index_dscrpcn_ttla));
			 tutelasAfiliadoVO.setConsecutivoEstadoTutela(result.getInt(index_cnsctvo_cdgo_estdo_ttla));
			 tutelasAfiliadoVO.setDescripcionEstadoTutela(result.getString(index_dscrpcn_estdo_ttla));
			 tutelasAfiliadoVO.setConsecutivoEstadoFallo(result.getInt(index_cnsctvo_cdgo_estdo_fllo));
			 tutelasAfiliadoVO.setDescripcionEstadoFallo(result.getString(index_dscrpcn_estdo_fllo));
			 tutelasAfiliadoVO.setResuelveTutela(result.getString(index_rslve));			 
			 tutelasAfiliadoVO.setDiagnosticoVO(diagnosticosVO);
			 tutelasAfiliadoVO.setConsecutivoCodigoCausaEstadoNotificacion(result.getInt(index_cnsctvo_cdgo_csa_estdo_ntfccn));
			 tutelasAfiliadoVO.setFechaNotificacion(new java.util.Date(result.getDate(index_fcha_ntfccn).getTime()));			 
			 
			 lTutelasAfiliado.add(tutelasAfiliadoVO);			 
		 }		
	}
	
	public List<TutelasAfiliadoVO> getlTutelasAfiliado() {
		return lTutelasAfiliado;
	}
}
