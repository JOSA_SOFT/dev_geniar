package com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpConsultaMedicamentosDelegate
 * Clase Delegate que permite consultar los medicamentos
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/12/2015
 *
 */
public class SpConsultaMedicamentosDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar los medicamentos */
	private List<MedicamentosVO> lMedicamentos;
	
	private String codigoCodificacionMedicamento;
	private String descripcionCodificacionMedicamento;
	private Date fechaConsulta;
	
	public SpConsultaMedicamentosDelegate(java.util.Date fechaConsulta, String codigoCodificacionMedicamento, String descripcionCodificacionMedicamento){
		
		this.fechaConsulta = new java.sql.Date(fechaConsulta.getTime());
		this.codigoCodificacionMedicamento = codigoCodificacionMedicamento;
		this.descripcionCodificacionMedicamento = descripcionCodificacionMedicamento;				
	}
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_MEDICAMENTOS"));	
		stmt.setNull(1, java.sql.Types.VARCHAR);
		stmt.setNull(2, java.sql.Types.VARCHAR);
		stmt.setNull(3, java.sql.Types.VARCHAR);
		stmt.setDate(4, this.fechaConsulta);
		stmt.setNull(5, java.sql.Types.VARCHAR);
		stmt.setNull(6, java.sql.Types.VARCHAR);
		stmt.setNull(7, java.sql.Types.VARCHAR);
		
		if(this.codigoCodificacionMedicamento == null || ConstantesData.CADENA_VACIA.equals(this.codigoCodificacionMedicamento)){
		    stmt.setNull(8, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(8, this.codigoCodificacionMedicamento);
		}
		
		stmt.setNull(9, java.sql.Types.VARCHAR);
		
		if(this.descripcionCodificacionMedicamento == null || ConstantesData.CADENA_VACIA.equals(this.descripcionCodificacionMedicamento)){
		    stmt.setNull(10, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(10, this.descripcionCodificacionMedicamento);
		}
		
		ResultSet result = stmt.executeQuery();
		
		int index_cdgo_cdfccn              = result.findColumn("cdgo_cdfccn");
		int index_dscrpcn_cdfccn           = result.findColumn("dscrpcn_cdfccn");
		int index_CodMedPrestador          = result.findColumn("CodMedPrestador");		
		int index_DescMedPrestador         = result.findColumn("DescMedPrestador");
		int index_rgstro_invma             = result.findColumn("rgstro_invma");
		int index_cdgo_prsntcn             = result.findColumn("cdgo_prsntcn");
		int index_dscrpcn_prsntcn          = result.findColumn("dscrpcn_prsntcn");
		int index_cntdd_prsntcn            = result.findColumn("cntdd_prsntcn");
		int index_cdgo_cncntrcn            = result.findColumn("cdgo_cncntrcn");
		int index_dscrpcn_cncntrcn         = result.findColumn("dscrpcn_cncntrcn_sn_cntdd");		
		int index_cntdd_cncntrcn           = result.findColumn("cntdd_cncntrcn");
		int index_cdgo_frma_frmctca        = result.findColumn("cdgo_frma_frmctca");
		int index_dscrpcn_frma_frmctca     = result.findColumn("dscrpcn_frma_frmctca");
		int index_cdgo_va_admnstrcn        = result.findColumn("cdgo_va_admnstrcn");		
		int index_dscrpcn_va_admnstrcn     = result.findColumn("dscrpcn_va_admnstrcn");
		int index_cdgo_sbgrpo_trptco       = result.findColumn("cdgo_sbgrpo_trptco");
		int index_dscrpcn_clse_frmcotrptca = result.findColumn("dscrpcn_clse_frmcotrptca");
		int index_indcdr_no_ps             = result.findColumn("indcdr_no_ps"); 
		int index_cnsctvo_cdgo_itm_prspsto = result.findColumn("cnsctvo_cdgo_itm_prspsto");
		int index_cnsctvo_cdfccn           = result.findColumn("cnsctvo_cdfccn");
		
		lMedicamentos = new ArrayList<MedicamentosVO>();
		
		while(result.next()){
			MedicamentosVO medicamentosVO = new MedicamentosVO();
			ConcentracionVO concentracionVO = new ConcentracionVO();
			PresentacionVO presentacionVO = new PresentacionVO();
			DosisVO dosisVO = new DosisVO();
			FrecuenciaVO frecuenciaVO = new FrecuenciaVO();
			
			medicamentosVO.setConsecutivoCodificacionMedicamento(result.getInt(index_cnsctvo_cdfccn));
			medicamentosVO.setCodigoCodificacionMedicamento(result.getString(index_cdgo_cdfccn));
			medicamentosVO.setDescripcionCodificacionMedicamento(result.getString(index_dscrpcn_cdfccn));
			medicamentosVO.setCodigoMedicamentoPrestador(result.getString(index_CodMedPrestador));
			medicamentosVO.setDescripcionMedicamentoPrestador(result.getString(index_DescMedPrestador));
			medicamentosVO.setRegistroInvima(result.getString(index_rgstro_invma));
			
			concentracionVO.setCodigoConcentracion(result.getString(index_cdgo_cncntrcn));
			concentracionVO.setDescripcionConcentracion(result.getString(index_dscrpcn_cncntrcn));
			concentracionVO.setCantidadConcentracion(result.getString(index_cntdd_cncntrcn));
			
			presentacionVO.setCodigoPresentacion(result.getString(index_cdgo_prsntcn));
			presentacionVO.setDescripcionPresentacion(result.getString(index_dscrpcn_prsntcn));
			presentacionVO.setCantidadPresentacion(result.getString(index_cntdd_prsntcn));
					
			medicamentosVO.setCodigoFormaFarmaceutica(result.getString(index_cdgo_frma_frmctca));
			medicamentosVO.setDescripcionFormaFarmaceutica(result.getString(index_dscrpcn_frma_frmctca));
			medicamentosVO.setCodigoViaAdministracion(result.getString(index_cdgo_va_admnstrcn));
			medicamentosVO.setDescripcionViaAdministracion(result.getString(index_dscrpcn_va_admnstrcn));
			medicamentosVO.setCodigoSubgrupoTerapeutico(result.getString(index_cdgo_sbgrpo_trptco));
			medicamentosVO.setDescripcionClaseFarmacologico(result.getString(index_dscrpcn_clse_frmcotrptca));
			String noPos = result.getString(index_indcdr_no_ps)!=null?result.getString(index_indcdr_no_ps).trim():null;
			medicamentosVO.setIndicadorNoPOS("N".equals(noPos)?"S":"N");
			medicamentosVO.setConsecutivoItemPresupuesto(result.getInt(index_cnsctvo_cdgo_itm_prspsto));
			
			medicamentosVO.setConcentracionVO(concentracionVO);
			medicamentosVO.setPresentacionVO(presentacionVO);
			medicamentosVO.setDosisVO(dosisVO);
			medicamentosVO.setFrecuenciaVO(frecuenciaVO);
			
			lMedicamentos.add(medicamentosVO);
		}		
	}
	
	public List<MedicamentosVO> getlMedicamentos() {
		return lMedicamentos;
	}
}
