package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASConsultarExistenciaReciboCajaDelegate implements SQLDelegate {

	private Integer consecutivoSolicitud;
	private Integer consecutivoPrestacion;
	private Integer cantidadRecibos;
	
	public SpASConsultarExistenciaReciboCajaDelegate(Integer consecutivoSolicitud, Integer consecutivoPrestacion) {
		this.consecutivoSolicitud = consecutivoSolicitud;
		this.consecutivoPrestacion = consecutivoPrestacion;		
		this.cantidadRecibos = ConstantesData.VALOR_CERO;
	}
	
	/**
	 * 
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		CallableStatement  statement = conn.prepareCall(ConsultaSp.getString(ConstantesData.VALIDAR_EXISTENCIA_RECIBOS_CAJA));
		statement.setInt(1, consecutivoSolicitud);
		statement.setInt(2, consecutivoPrestacion);
		statement.registerOutParameter(3, java.sql.Types.INTEGER);
		
		statement.executeUpdate();
		
		cantidadRecibos = statement.getInt(3);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getCantidadRecibos() {
		return cantidadRecibos;
	}

}
