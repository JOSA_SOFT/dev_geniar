package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Cierra la caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCCierreCajaDelegate implements SQLDelegate {

	private MovimientoCajaVO movimientoCajaVO;
	private String causalesXML;

	/**
	 * Recibe los parametros para registrar el cierre de caja
	 * 
	 * @param movimientoCajaVO
	 * @param causalesXML
	 */
	public SpRCCierreCajaDelegate(MovimientoCajaVO movimientoCajaVO,
			String causalesXML) {
		super();
		this.movimientoCajaVO = movimientoCajaVO;
		this.causalesXML = causalesXML;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCCierreCajaspRCCierreCaja
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.GUARGAR_CIERRE_CAJA));

		stmt.setInt(1, movimientoCajaVO.getCnsctvoMvmntoCja());
		stmt.setString(2, movimientoCajaVO.getUsroUltmaMdfccn());
		stmt.setDouble(3, movimientoCajaVO.getTtlEfctvo());
		stmt.setDouble(4, movimientoCajaVO.getTtlNtaCrdto());
		stmt.setDouble(5, movimientoCajaVO.getTtlCja());
		stmt.setDouble(6, movimientoCajaVO.getSbrnteCrre());
		stmt.setDouble(7, movimientoCajaVO.getFltnteCrre());
		if (this.causalesXML == null || this.causalesXML.isEmpty()) {
			stmt.setNull(8, Types.VARCHAR);
		} else {
			stmt.setString(8, this.causalesXML);
		}

		stmt.execute();
	}

}