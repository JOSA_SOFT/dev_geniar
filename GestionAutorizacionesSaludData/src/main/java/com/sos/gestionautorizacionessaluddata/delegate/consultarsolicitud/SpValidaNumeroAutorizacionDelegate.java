package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;


/**
 * Class SpValidaNumeroAutorizacionDelegate
 * Clase delegate que permite validar las validaciones especiales del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 15/03/2016
 */
public class SpValidaNumeroAutorizacionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar las prestaciones pis */
	private List<ValidacionEspecialVO> lValidacionEspecialVO;

	/*Identifica el conswecutivo de identificacion del afialiado */
	private int consecutivoTipoIdentificacion;
	
	/*Identifica el numero de identificacion del afialiado */
	private String numeroIdentificacion;
	
	public SpValidaNumeroAutorizacionDelegate(int consecutivoTipoIdentificacion, String numeroIdentificacion){
		
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_VALIDACION_ESPECIAL"));	
		
		stmt.setInt(1, this.consecutivoTipoIdentificacion);
		stmt.setString(2, this.numeroIdentificacion);
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_ofcna = result.findColumn("cnsctvo_cdgo_ofcna");
		int index_nmro_vrfccn        = result.findColumn("nmro_vrfccn");
		int index_cdgo_usro          = result.findColumn("cdgo_usro");
		int index_fcha_vldcn         = result.findColumn("fcha_vldcn");
		int cdgo_ofcna         		= result.findColumn("cdgo_ofcna");
		
		lValidacionEspecialVO = new ArrayList<ValidacionEspecialVO>();
		
		while(result.next()){
			ValidacionEspecialVO validacionEspecialVO = new ValidacionEspecialVO();
			OficinasVO oficinasVO = new OficinasVO();
			
			oficinasVO.setConsecutivoCodigoOficina(result.getInt(index_cnsctvo_cdgo_ofcna));
			oficinasVO.setCodigoOficina(result.getString(cdgo_ofcna));
			validacionEspecialVO.setOficinasVO(oficinasVO);
			
			validacionEspecialVO.setNumeroValidacion(Utilidades.convertirNumeroToBigInteger(result.getInt(index_nmro_vrfccn)));
			validacionEspecialVO.setUsroValidacion(result.getString(index_cdgo_usro));
			validacionEspecialVO.setFechaValidacion(result.getDate(index_fcha_vldcn) != null ? new java.util.Date(result.getDate(index_fcha_vldcn).getTime()): null);
			
			lValidacionEspecialVO.add(validacionEspecialVO);
		}		
	}
	
	public List<ValidacionEspecialVO> getlValidacionEspecialVO() {
		return lValidacionEspecialVO;
	}
}
