package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DetalleSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Class SpASConsultaDetalleSolicitudWebDelegate Clase delegate que permite
 * consultar el detalle de la informacion de la solicitud
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 25/03/2016
 */
public class SpASConsultaDetalleSolicitudWebDelegate implements SQLDelegate {

	/** Lista que se utiliza para almacenar el detalle de la solicitud */
	private List<DetalleSolicitudVO> lDetalleSolicitud;

	private String numeroRadicacionSolicitud;

	private NumberFormat decimalFormat = NumberFormat.getNumberInstance(new Locale("es"));

	public SpASConsultaDetalleSolicitudWebDelegate(String numeroRadicacionSolicitud) {
		this.numeroRadicacionSolicitud = numeroRadicacionSolicitud;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DETALLE_SOLICITUD));

		stmt.setString(1, this.numeroRadicacionSolicitud);
		ResultSet result = stmt.executeQuery();

		int indice_cdgo_mdo_cntcto = result.findColumn("cdgo_mdo_cntcto");
		int indice_fcha_crcn = result.findColumn("fcha_crcn");
		int indice_nmro_slctd_prvdr = result.findColumn("nmro_slctd_prvdr");
		int indice_nmro_idntfccn_afldo = result.findColumn("nmro_idntfccn_afldo");
		int indice_nmbre = result.findColumn("nmbre");
		int indice_dscrpcn_pln = result.findColumn("dscrpcn_pln");
		int indice_edd = result.findColumn("edd");
		int indice_edd_mss = result.findColumn("edd_mss");
		int indice_edd_ds = result.findColumn("edd_ds");
		int indice_rcn_ncdo = result.findColumn("rcn_ncdo");
		int indice_fcha_ncmnto_rcn_ncdo = result.findColumn("fcha_ncmnto_rcn_ncdo");
		int indice_cdgo_sxo_rcn_ncdo = result.findColumn("cdgo_sxo_rcn_ncdo");
		int indice_prto_mltple = result.findColumn("prto_mltple");
		int indice_nmro_hjo_afldo = result.findColumn("nmro_hjo_afldo");
		int indice_fcha_slctd_prfsnal = result.findColumn("fcha_slctd_prfsnal");
		int indice_dscrpcn_tpo_ubccn_pcnte = result.findColumn("dscrpcn_tpo_ubccn_pcnte");
		int indice_dscrpcn_orgn_atncn = result.findColumn("dscrpcn_orgn_atncn");
		int indice_dscrpcn_prrdd_atncn = result.findColumn("dscrpcn_prrdd_atncn");
		int indice_dscrpcn_rcbro = result.findColumn("dscrpcn_rcbro");
		int indice_jstfccn_clnca = result.findColumn("jstfccn_clnca");
		int indice_spra_tpe_st = result.findColumn("spra_tpe_st");
		int indice_obsrvcn_adcnl = result.findColumn("obsrvcn_adcnl");
		int indice_fcha_ingrso_hsptlzcn = result.findColumn("fcha_ingrso_hsptlzcn");
		int indice_fcha_egrso_hsptlzcn = result.findColumn("fcha_egrso_hsptlzcn");
		int indice_crte_cnta = result.findColumn("crte_cnta");
		int indice_idntfccion_prstdor = result.findColumn("idntfccion_prstdor");
		int indice_cdgo_intrno = result.findColumn("cdgo_intrno");
		int indice_nmbre_scrsl = result.findColumn("nmbre_scrsl");
		int indice_rzn_scl = result.findColumn("rzn_scl");
		int indice_dscrpcn_cdd = result.findColumn("dscrpcn_cdd");
		int indice_eml = result.findColumn("eml");
		int indice_cnsctvo_prslctd = result.findColumn("cnsctvo_prslctd");

		lDetalleSolicitud = new ArrayList<DetalleSolicitudVO>();
		while (result.next()) {
			AfiliadoVO afiliadoVO = new AfiliadoVO();
			HospitalizacionVO hospitalizacionVO = new HospitalizacionVO();
			PrestadorVO prestadorVO = new PrestadorVO();
			CiudadVO ciudadVO = new CiudadVO();
			DetalleSolicitudVO detalleSolicitudVO = new DetalleSolicitudVO();

			afiliadoVO.setNumeroIdentificacion(result.getString(indice_nmro_idntfccn_afldo));
			afiliadoVO.setNombreCompleto(result.getString(indice_nmbre));
			afiliadoVO.setEdadAnos(result.getInt(indice_edd));
			afiliadoVO.setEdadMeses(result.getInt(indice_edd_mss));
			afiliadoVO.setEdadDias(result.getInt(indice_edd_ds));
			afiliadoVO.setEmail(result.getString(indice_eml));
			afiliadoVO.setConsecutivoPresolicitud(result.getInt(indice_cnsctvo_prslctd));

			if (ConstantesData.RECIEN_NACIDO_CADENA.equals(result.getString(indice_rcn_ncdo))) {
				afiliadoVO.setRecienNacido(result.getString(indice_rcn_ncdo));
				afiliadoVO.setNro(result.getString(indice_nmro_hjo_afldo));
				afiliadoVO.setFechaRecienNacido(new java.util.Date(result.getDate(indice_fcha_ncmnto_rcn_ncdo).getTime()));
				afiliadoVO.setGeneroRecNac(result.getString(indice_cdgo_sxo_rcn_ncdo));
				afiliadoVO.setPartoMultiple(result.getString(indice_prto_mltple));
			}

			hospitalizacionVO.setFechaIngreso(result.getDate(indice_fcha_ingrso_hsptlzcn) != null ? new java.util.Date(result.getDate(indice_fcha_ingrso_hsptlzcn).getTime()) : null);
			hospitalizacionVO.setFechaEgreso(result.getDate(indice_fcha_egrso_hsptlzcn) != null ? new java.util.Date(result.getDate(indice_fcha_egrso_hsptlzcn).getTime()) : null);
			hospitalizacionVO.setCorteCuenta(result.getString(indice_crte_cnta) != null ? decimalFormat.format(Long.parseLong(result.getString(indice_crte_cnta))) : null);

			ciudadVO.setDescripcionCiudad(result.getString(indice_dscrpcn_cdd));

			prestadorVO.setCodigoInterno(result.getString(indice_cdgo_intrno));
			prestadorVO.setNumeroIdentificacionPrestador(result.getString(indice_idntfccion_prstdor));
			prestadorVO.setRazonSocial(result.getString(indice_rzn_scl));
			prestadorVO.setNombreSucursal(result.getString(indice_nmbre_scrsl));
			prestadorVO.setCiudadVO(ciudadVO);

			detalleSolicitudVO.setNumeroSolicitudProveedor(result.getString(indice_nmro_slctd_prvdr));
			detalleSolicitudVO.setFechaCreacion(new java.util.Date(result.getDate(indice_fcha_crcn).getTime()));
			detalleSolicitudVO.setFechaSolicitud(new java.util.Date(result.getDate(indice_fcha_slctd_prfsnal).getTime()));
			detalleSolicitudVO.setDescripcionMedioContacto(result.getString(indice_cdgo_mdo_cntcto));
			detalleSolicitudVO.setDescripcionPlan(result.getString(indice_dscrpcn_pln));
			detalleSolicitudVO.setOrigenAtencion(result.getString(indice_dscrpcn_orgn_atncn));
			detalleSolicitudVO.setDescripcionUbicacionPaciente(result.getString(indice_dscrpcn_tpo_ubccn_pcnte));
			detalleSolicitudVO.setDescripcionPrioridadAtencion(result.getString(indice_dscrpcn_prrdd_atncn));
			detalleSolicitudVO.setDescripcionRecobro(result.getString(indice_dscrpcn_rcbro));
			detalleSolicitudVO.setJustificacionClinica(result.getString(indice_jstfccn_clnca));
			detalleSolicitudVO.setSuperaTopeSoat(result.getString(indice_spra_tpe_st));
			detalleSolicitudVO.setObsSolcitud(result.getString(indice_obsrvcn_adcnl));

			detalleSolicitudVO.setAfiliadoVO(afiliadoVO);
			detalleSolicitudVO.setHospitalizacionVO(hospitalizacionVO);
			detalleSolicitudVO.setPrestadorSolicitante(prestadorVO);

			lDetalleSolicitud.add(detalleSolicitudVO);
		}
	}

	public List<DetalleSolicitudVO> getlDetalleSolicitud() {
		return lDetalleSolicitud;
	}
}
