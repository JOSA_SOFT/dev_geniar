package com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Class SpASGuardarMarcasMedicinaTrabajoDelegate
 * Clase Delegate que guarda las marcas ATEL de Medicina del trabajo
 * @author ing. Rafael Cano
 * @version 17/03/2016
 *
 */

public class SpASGuardarMarcasMedicinaTrabajoDelegate implements SQLDelegate{
	
	/** Contiene el formato xml de los datos, esto permite hacer carga masiva en base de datos  */
	StringBuilder xml;
	
	public SpASGuardarMarcasMedicinaTrabajoDelegate(StringBuilder xml){
		this.xml = xml;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.GUARDAR_GESTION_MEDICINA));	
		stmt.setString(1, this.xml.toString());
		
		stmt.executeUpdate();
	}

}
