package com.sos.gestionautorizacionessaluddata.delegate.parametros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaParametrosSp;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpTraerClaseHabitacion
 * Clase Delegate que consulta la clase de habitacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */

public class SpTraerClaseHabitacionDelegate implements SQLDelegate {
	
	/** Lista que se utiliza para almacenar la clase de habitacion */
	private List<ClaseHabitacionVO> lClaseHabitacionVO;
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		stmt = conn.prepareStatement(ConsultaParametrosSp.getString("CONSULTA_CLASEHABITACION"));
		
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_cdgo_clse_hbtcn = result.findColumn("cnsctvo_cdgo_clse_hbtcn");
		int index_cdgo_clse_hbtcn         = result.findColumn("cdgo_clse_hbtcn");
		int index_dscrpcn_clse_hbtcn      = result.findColumn("dscrpcn_clse_hbtcn");
		
		lClaseHabitacionVO = new ArrayList<ClaseHabitacionVO>();
		
		while(result.next()){
			ClaseHabitacionVO claseHabitacionVO = new ClaseHabitacionVO();
			claseHabitacionVO.setConsecutivoClaseHabitacion(result.getInt(index_cnsctvo_cdgo_clse_hbtcn));
			claseHabitacionVO.setCodigoClaseHabitacion(result.getString(index_cdgo_clse_hbtcn));
			claseHabitacionVO.setDescripcionClaseHabitacion(result.getString(index_dscrpcn_clse_hbtcn));
			
			lClaseHabitacionVO.add(claseHabitacionVO);
		}		
	}
	
	public List<ClaseHabitacionVO> getlClaseHabitacionVO() {
		return lClaseHabitacionVO;
	}
}
