package com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;


/**
 * Class SpAsConsultarCausalesDeNoCobroDelegate
 * Clase para consultar las causales de no cobro visibles en pantalla.
 * @author ing. Carlos Andres Lopez Ramirez
 * @version 13/06/2017
 *
 */
public class SpAsConsultarCausalesDeNoCobroDelegate implements SQLDelegate {
	
	private List<SelectItem> lstCausasnoCobroCuota;
	
	public SpAsConsultarCausalesDeNoCobroDelegate() {
		super();
	}

	/**
	 * 
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		
		PreparedStatement stmt = null;		
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTAR_CAUSALES_NO_COBRO_CUOTA));
		
		ResultSet result = stmt.executeQuery();
		
		int indice_consecutivo = result.findColumn("cnsctvo_cdgo_csa_no_cbro_cta_rcprcn");
		int indice_descripcion = result.findColumn("dscrpcn_csa_no_cbro_cta_rcprcn");
		
		lstCausasnoCobroCuota = new ArrayList<SelectItem>();
		
		while(result.next()){
			SelectItem item = new SelectItem();
			item.setValue(result.getInt(indice_consecutivo));
			item.setLabel(result.getString(indice_descripcion));			
			lstCausasnoCobroCuota.add(item);
		}
		
	}
	
	/**
	 * Retorna las lista de causales de no cobro.
	 * @return
	 */
	public List<SelectItem> getCausalesNoCobroCuota() {
		return lstCausasnoCobroCuota;
	}

}
