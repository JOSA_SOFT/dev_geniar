package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.PrestacionPisVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpMNConsultaPrestacionPisDelegate
 * Clase delegate que permite consultar las prestaciones PIS
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/03/2016
 */
public class SpMNConsultaPrestacionPisDelegate implements SQLDelegate{
	
	/** Lista que se utiliza para almacenar las prestaciones pis */
	private List<PrestacionPisVO> lPrestacionesPis;
		
	private String codigoCodificacionPrestacionPis;
	private String descripcionCodificacionPrestacionPis;	
	
	public SpMNConsultaPrestacionPisDelegate(String codigoCodificacionPrestacionPis, String descripcionCodificacionPrestacionPis){
		this.codigoCodificacionPrestacionPis = codigoCodificacionPrestacionPis;
		this.descripcionCodificacionPrestacionPis = descripcionCodificacionPrestacionPis;
	}	
	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString("CONSULTA_PRESTACIONES_PIS"));	
		
		stmt.setNull(1, java.sql.Types.INTEGER);
		
		if(this.codigoCodificacionPrestacionPis == null || ConstantesData.CADENA_VACIA.equals(this.codigoCodificacionPrestacionPis)){
		    stmt.setNull(2, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(2, this.codigoCodificacionPrestacionPis);
		}
		
		if(this.descripcionCodificacionPrestacionPis == null || ConstantesData.CADENA_VACIA.equals(this.descripcionCodificacionPrestacionPis)){
		    stmt.setNull(3, java.sql.Types.VARCHAR);
		}else{
			stmt.setString(3, this.descripcionCodificacionPrestacionPis);
		}
		stmt.setInt(4,ConstantesData.TIPO_PRESTACION);
		ResultSet result = stmt.executeQuery();
		
		int index_cnsctvo_prstcn_pis       = result.findColumn("cnsctvo_prstcn_pis");
		int index_cdgo_cdfccn              = result.findColumn("cdgo_cdfccn");
		int index_dscrpcn_prstcn_cmplta    = result.findColumn("dscrpcn_prstcn_cmplta");
		int index_cnsctvo_cdgo_itm_prspsto = result.findColumn("cnsctvo_cdgo_itm_prspsto");
		
		lPrestacionesPis = new ArrayList<PrestacionPisVO>();
		
		while(result.next()){
			PrestacionPisVO prestacionPisVO = new PrestacionPisVO();
			prestacionPisVO.setConsecutivoPrestacionPis(result.getInt(index_cnsctvo_prstcn_pis));
			prestacionPisVO.setCodigoPrestacionPis(result.getString(index_cdgo_cdfccn));
			prestacionPisVO.setDescripcionPrestacionPis(result.getString(index_dscrpcn_prstcn_cmplta));
			prestacionPisVO.setConsecutivoItemPresupuesto(result.getInt(index_cnsctvo_cdgo_itm_prspsto));
			
			lPrestacionesPis.add(prestacionPisVO);
		}		
	}

	
	public List<PrestacionPisVO> getlPrestacionesPis() {
		return lPrestacionesPis;
	}	
}
