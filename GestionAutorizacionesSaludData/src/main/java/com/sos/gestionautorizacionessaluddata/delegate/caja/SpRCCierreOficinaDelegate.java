package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.CierreOficinaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

/**
 * Cierra la oficina
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class SpRCCierreOficinaDelegate implements SQLDelegate {

	private CierreOficinaVO cierreOficinaVO;
	private String xmlMovimientosCerrados;
	private String xmlMovimientosCierreAdmin;

	private Integer consecutivoCierreOficina;

	/**
	 * Recibe oficina y listados diferentes a movimientos cerrados y en cierre
	 * administrativo
	 * 
	 * @param cierreOficinaVO
	 * @param xmlMovimientosCerrados
	 * @param xmlMovimientosCierreAdmin
	 */
	public SpRCCierreOficinaDelegate(CierreOficinaVO cierreOficinaVO,
			String xmlMovimientosCerrados, String xmlMovimientosCierreAdmin) {
		super();
		this.cierreOficinaVO = cierreOficinaVO;
		this.xmlMovimientosCerrados = xmlMovimientosCerrados;
		this.xmlMovimientosCierreAdmin = xmlMovimientosCierreAdmin;
	}

	/**
	 * Ejecuta el procedimiento BDCna.cja.spRCCierreOficina
	 */
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CIERRE_OFICINA));
		
		stmt.setDouble(1, this.cierreOficinaVO.getTtlEfctvoCja());
		stmt.setDouble(2, this.cierreOficinaVO.getTtlRcbdoOfcna());
		stmt.setDouble(3, this.cierreOficinaVO.getBseOfcna());
		stmt.setDouble(4, this.cierreOficinaVO.getFltnteOfcna());
		stmt.setDouble(5, this.cierreOficinaVO.getSbrnteOfcna());
		stmt.setString(6, this.cierreOficinaVO.getUsroCrcn());
		stmt.setInt(7, this.cierreOficinaVO.getCnsctvoCdgoCrdndrAsi());
		
		if (this.xmlMovimientosCierreAdmin == null
				|| this.xmlMovimientosCierreAdmin.isEmpty()) {
			stmt.setNull(8, Types.VARCHAR);
		} else {
			stmt.setString(8, this.xmlMovimientosCierreAdmin);
		}
		if (this.xmlMovimientosCerrados == null
				|| this.xmlMovimientosCerrados.isEmpty()) {
			stmt.setNull(9, Types.VARCHAR);
		} else {
			stmt.setString(9, this.xmlMovimientosCerrados);
		}

		ResultSet result = stmt.executeQuery();

		int cnsctvoCrreOfc = result.findColumn("cnsctvo_crre_ofcna");
		if (result.next()) {
			this.consecutivoCierreOficina = result.getInt(cnsctvoCrreOfc);
		}
	}

	public Integer getCnsctvoCrreOfcna() {
		return consecutivoCierreOficina;
	}

}
