package com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion;


/**
 * Class SpASConsultarDatosConsecutivoPrestacionesDelegate
 * Clase Sp que consulta las prestaciones aprobadas
 * @author ing. Rafael Cano
 * @version 11/02/2016
 *
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaluddata.util.Utilidades;

import co.eps.sos.dataccess.SQLDelegate;

public class SpASConsultaDatosInformacionPrestacionSolicitudDelegate implements SQLDelegate{
	
	private int numeroSolicitud;
	private String estado;
	private List<PrestacionesAprobadasVO> listaPrestacionesAprobadas;
	
	public SpASConsultaDatosInformacionPrestacionSolicitudDelegate(int numeroSolicitud, String estado){
		this.numeroSolicitud = numeroSolicitud;
		this.estado = estado;
	}

	
	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_PRESTACIONES_APROBADAS));	
		
		stmt.setInt(1, this.numeroSolicitud);
		stmt.setString(2, this.estado);
		
		ResultSet result = stmt.executeQuery();
		  
		int indice_numeroSolicitud = result.findColumn("cnsctvo_slctd_atrzcn_srvco");
		int indice_tipoPrestacion = result.findColumn("dscrpcn_tpo_cdfccn");
		int indice_codigoPrestacion = result.findColumn("cdgo_cdfccn");
		int indice_conscodigoPrestacion = result.findColumn("cnsctvo_cdgo_srvco_slctdo");
		int indice_descripcion = result.findColumn("dscrpcn_cdfccn");
		int indice_planAfiliado = result.findColumn("cnsctvo_cdgo_pln");
		int indice_descPlanAfiliado = result.findColumn("pln_afldo");  
		int indice_observaciones = result.findColumn("obsrvcn_adcnl");  
		int indice_lateralidad = result.findColumn("cdgo_ltrldd");  
		int indice_descLateralidad = result.findColumn("dscrpcn_ltrldd");  
		int indice_viaAcceso = result.findColumn("cnsctvo_cdgo_va_accso");  
		int indice_descViaAcceso = result.findColumn("dscrpcn_cdgo_va_accso");  
		int indice_direccionamiento = result.findColumn("cdgo_intrno");  
		int indice_descDireccionamiento = result.findColumn("nmbre_scrsl");  
		int indice_usuarioGestiona = result.findColumn("usro_ultma_mdfccn");  
		int indice_fechaGestion = result.findColumn("fcha_ultma_mdfccn");
		int indice_consTipoPrestacion = result.findColumn("cnsctvo_cdgo_tpo_cdfccn");
		int indice_estadoPrestacion = result.findColumn("dscrpcn_estdo_srvco_slctdo");
		int indice_atel = result.findColumn("atl");
		int indice_requiereAuditoria = result.findColumn("rqur_adtra");
		int indice_cantidad = result.findColumn("cntdd_slctda");
		int indice_consPosologiaCUMS = result.findColumn("dss");
		int indice_posologiaCUMS = result.findColumn("dscrpcn_dss");
		int indice_cada = result.findColumn("prdcdd_dss");
		int indice_frecuencia = result.findColumn("dscrpcn_frcnca");
		int indice_recobro = result.findColumn("dscrpcn_chrte");
		int indice_conscodigoPrestacionGSA = result.findColumn("cnsctvo_srvco_slctdo");
		int cnsctvo_cdgo_cdd_rsdnca_afldo = result.findColumn("cnsctvo_cdgo_cdd_rsdnca_afldo");
		int cnsctvo_cdgo_prsntcn_dss = result.findColumn("cnsctvo_cdgo_prsntcn_dss");
		int cnsctvo_cdgo_undd_prdcdd_dss = result.findColumn("cnsctvo_cdgo_undd_prdcdd_dss");
		int cnsctvo_cdgo_estdo_srvco_slctdo = result.findColumn("cnsctvo_cdgo_estdo_srvco_slctdo");
		int cnsctvo_cdgo_ltrldd = result.findColumn("cnsctvo_cdgo_ltrldd");
		int indice_rqre_otra_gstn_adtr = result.findColumn("rqre_otra_gstn_adtr");
		int indice_cnsctvo_cdgo_rcbro = result.findColumn("cnsctvo_cdgo_rcbro");
		int indice_cnsctvo_csa_no_cbro_cta = result.findColumn("cnsctvo_cdgo_csa_no_cbro_cta_rcprcn");
		
		listaPrestacionesAprobadas = new ArrayList<PrestacionesAprobadasVO>();
		
		while(result.next()){
			PrestacionesAprobadasVO prestacionesAprobadasDTO = new PrestacionesAprobadasVO();
			
			prestacionesAprobadasDTO.setNumeroSolicitud(result.getString(indice_numeroSolicitud));
			prestacionesAprobadasDTO.setTipoPrestacion(result.getString(indice_tipoPrestacion));
			prestacionesAprobadasDTO.setCodigoPrestacion(result.getString(indice_codigoPrestacion));
			prestacionesAprobadasDTO.setConscodigoPrestacion(result.getInt(indice_conscodigoPrestacion));
			prestacionesAprobadasDTO.setDescripcion(result.getString(indice_descripcion));
			prestacionesAprobadasDTO.setPlanAfiliado(result.getString(indice_planAfiliado));
			prestacionesAprobadasDTO.setDescPlanAfiliado(result.getString(indice_descPlanAfiliado));
			prestacionesAprobadasDTO.setObservaciones(result.getString(indice_observaciones));
			prestacionesAprobadasDTO.setLateralidad(result.getString(indice_lateralidad));
			prestacionesAprobadasDTO.setDescLateralidad(result.getString(indice_descLateralidad));
			prestacionesAprobadasDTO.setViaAcceso(result.getString(indice_viaAcceso));
			prestacionesAprobadasDTO.setDescViaAcceso(result.getString(indice_descViaAcceso));
			prestacionesAprobadasDTO.setDireccionamiento(result.getString(indice_direccionamiento));
			prestacionesAprobadasDTO.setDescDireccionamiento(result.getString(indice_descDireccionamiento));
			prestacionesAprobadasDTO.setUsuarioGestiona(result.getString(indice_usuarioGestiona));
			prestacionesAprobadasDTO.setFechaGestion(result.getDate(indice_fechaGestion));
			prestacionesAprobadasDTO.setCodigoTipoPrestacion(result.getString(indice_consTipoPrestacion));
			prestacionesAprobadasDTO.setEstadoPrestacion(result.getString(indice_estadoPrestacion));
			prestacionesAprobadasDTO.setAtel(result.getString(indice_atel));
			prestacionesAprobadasDTO.setAtelCheck(ConstantesData.CODIGO_SI.equals(result.getString(indice_atel))?true:false);
			prestacionesAprobadasDTO.setRequiereAuditoria(result.getString(indice_requiereAuditoria));
			prestacionesAprobadasDTO.setCantidad(result.getString(indice_cantidad));
			prestacionesAprobadasDTO.setConsPosologiaCUMS(result.getString(indice_consPosologiaCUMS));
			prestacionesAprobadasDTO.setPosologiaCUMS(result.getString(indice_posologiaCUMS));
			prestacionesAprobadasDTO.setCada(result.getString(indice_cada));
			prestacionesAprobadasDTO.setFrecuencia(result.getString(indice_frecuencia));
			prestacionesAprobadasDTO.setRecobro(result.getString(indice_recobro));
			prestacionesAprobadasDTO.setConscodigoPrestacionGSA(result.getString(indice_conscodigoPrestacionGSA));
			prestacionesAprobadasDTO.setConsecutivoCiudad(result.getInt(cnsctvo_cdgo_cdd_rsdnca_afldo));
			prestacionesAprobadasDTO.setConsecutivoCodigoDosis(result.getInt(cnsctvo_cdgo_prsntcn_dss));
			prestacionesAprobadasDTO.setConsecutivoFrecuencia(result.getInt(cnsctvo_cdgo_undd_prdcdd_dss));
			prestacionesAprobadasDTO.setConsEstadoPrestacion(result.getInt(cnsctvo_cdgo_estdo_srvco_slctdo));
			prestacionesAprobadasDTO.setConsLateralidad(result.getInt(cnsctvo_cdgo_ltrldd));
			prestacionesAprobadasDTO.setVerDetalle(Utilidades.enlaceSeleccionar(result.getInt(cnsctvo_cdgo_estdo_srvco_slctdo)));
			prestacionesAprobadasDTO.setRequiereOtraGestionAuditoria(result.getString(indice_rqre_otra_gstn_adtr));
			prestacionesAprobadasDTO.setConsecutivoRecobro(result.getInt(indice_cnsctvo_cdgo_rcbro));	
			prestacionesAprobadasDTO.setConsecutivoCausaNoCobroCuota(result.getInt(indice_cnsctvo_csa_no_cbro_cta));
			listaPrestacionesAprobadas.add(prestacionesAprobadasDTO);
		}

	}
	
	public List<PrestacionesAprobadasVO> getListaPrestacionesAprobadas() {
		return listaPrestacionesAprobadas;
	}
}
