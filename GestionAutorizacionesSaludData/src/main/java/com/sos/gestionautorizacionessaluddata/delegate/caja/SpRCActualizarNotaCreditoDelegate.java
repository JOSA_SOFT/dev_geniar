package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * 
 * @author GENIAR
 *
 */
public class SpRCActualizarNotaCreditoDelegate implements SQLDelegate {

	private DocumentoCajaVO documento;
	
	/**
	 * 
	 * @param documento
	 */
	public SpRCActualizarNotaCreditoDelegate(DocumentoCajaVO documento) {
		super();
		this.documento = documento;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.ACTUALIZAR_NOTA_CREDITO_CAJA));
		
		stmt.setInt(1, documento.getPkDocCaja());
		stmt.setInt(2, documento.getEstadoDocumento());
		stmt.setString(3, documento.getObservacion());
		stmt.setString(4, documento.getUsuarioCreacion());
		
		if (documento.getDatoContacto() == null || documento.getDatoContacto() <= 0){
			stmt.setNull(5, Types.INTEGER);
		}else{
			stmt.setInt(5, documento.getDatoContacto());
		}
		stmt.execute();
	}
}
