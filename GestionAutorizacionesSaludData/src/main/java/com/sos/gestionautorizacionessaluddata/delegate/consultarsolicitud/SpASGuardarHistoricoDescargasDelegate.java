package com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import co.eps.sos.dataccess.SQLDelegate;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.GuardarHistoricoVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;


/**
 * Class SpASGuardarHistoricoDescargasDelegate
 * Clase delegate que permite guardar en el historico de descargas
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/03/2016
 */
public class SpASGuardarHistoricoDescargasDelegate implements SQLDelegate {
	
	private GuardarHistoricoVO guardarHistoricoVO;
	
	public SpASGuardarHistoricoDescargasDelegate(GuardarHistoricoVO guardarHistoricoVO){
		this.guardarHistoricoVO = guardarHistoricoVO;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = null;		
		stmt = conn.prepareStatement(ConsultaSp.getString(ConstantesData.GUARDAR_HISTORICO_DESCARGAS));	
		
		stmt.setInt(1, guardarHistoricoVO.getConsecutivoSolicitud());//consecutivo solicitud
		stmt.setString(2, guardarHistoricoVO.getUsuarioDescarga());  //usuario que realiza la descarga
		stmt.setDate(3, new java.sql.Date(guardarHistoricoVO.getFechaDescarga().getTime())); //fecha en la que se realiza la descarga
		stmt.setString(4, guardarHistoricoVO.getCodigoMedioContacto()); //codigo del medio de contacto '01 - EPS'
		
		if(guardarHistoricoVO.getNumeroUnicoAutorizacion() == null){ //se envia el numero unico ops
			stmt.setNull(5, java.sql.Types.INTEGER);
		}else{
			stmt.setInt(5, guardarHistoricoVO.getNumeroUnicoAutorizacion());
		}
		
		stmt.setString(6, guardarHistoricoVO.getXmlServicios().toString());		
		stmt.executeUpdate();		
	}
}
