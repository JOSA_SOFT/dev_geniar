package com.sos.gestionautorizacionessaluddata.delegate.caja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

/**
 * Clase que consulta los conceptos de PAGO de una OPS
 * 
 * @author GENIAR <jorge.garcia@geniar.net>
 */
public class SpRCObtenerConceptoPagoOPSDelegate implements SQLDelegate {

	private int numeroUnicoOps;
	private List<ConceptoPagoOpsVO> listaConceptosGasto;

	/**
	 * Constructor de la clase, recibe el numero de autorizacion de la solicitud
	 * 
	 * @param numeroSolicitud
	 */
	public SpRCObtenerConceptoPagoOPSDelegate(int numeroUnicoOps) {
		this.numeroUnicoOps = numeroUnicoOps;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(ConsultaSp
				.getString(ConstantesData.CONSULTA_CONCEPTOS_PAGO_OPS));
		
		listaConceptosGasto = new ArrayList<ConceptoPagoOpsVO>();
		
		stmt.setInt(1, this.numeroUnicoOps);
		ResultSet resultS = stmt.executeQuery();

		while (resultS.next()) {
			ConceptoPagoOpsVO concepto = new ConceptoPagoOpsVO();
			concepto.setValorConceptoPago(resultS.getDouble("vlr_lqdcn_cta_rcprcn_srvco_ops"));
			concepto.setNombreConceptoPago(resultS.getString("dscrpcn_cncpto_pgo"));
			listaConceptosGasto.add(concepto);
		}		

	}

	public List<ConceptoPagoOpsVO> getListaConceptosGasto() {
		return listaConceptosGasto;
	}

}
