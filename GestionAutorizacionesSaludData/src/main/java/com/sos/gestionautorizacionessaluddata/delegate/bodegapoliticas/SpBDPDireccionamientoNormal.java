package com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.consultas.ConsultaSp;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoNormalDTO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;

import co.eps.sos.dataccess.SQLDelegate;

public class SpBDPDireccionamientoNormal implements SQLDelegate {
	/**
	 * Lista que se utiliza para almacenar las prestaciones relacionadas a una
	 * solicitud
	 */
	private List<DireccionamientoNormalDTO> lDto;

	private Integer cnsctvoCdfccn;
	private Integer cnsctvoCdad;

	public SpBDPDireccionamientoNormal(Integer cnsctvoCdfccn, Integer cnsctvoCdad) {
		this.cnsctvoCdfccn = cnsctvoCdfccn;
		this.cnsctvoCdad = cnsctvoCdad;
	}

	@Override
	public void ejecutarData(Connection conn) throws SQLException {
		PreparedStatement stmt = conn
				.prepareStatement(ConsultaSp.getString(ConstantesData.CONSULTA_DIRECCIONAMIENTO_NORMAL));

		stmt.setInt(2, cnsctvoCdfccn);
		stmt.setInt(1, cnsctvoCdad);
		ResultSet result = stmt.executeQuery();

		int indiceCdgoIntrno = result.findColumn(ConstantesData.CODIGO_INTERNO);
		int indiceNmbreScrsl = result.findColumn(ConstantesData.NOMBRE_SUCURSAL);
		int indiceDscrpcnSde = result.findColumn(ConstantesData.DESCRIPCION_CIUDAD);
		int indiceDscrpcnZna = result.findColumn(ConstantesData.DESCRIPCION_ZONA);
		int indiceDscrpcnPln = result.findColumn(ConstantesData.DESCRIPCION_PLAN);
		int indiceEddMnma = result.findColumn(ConstantesData.EDAD_MINIMA);
		int indiceEddMxma = result.findColumn(ConstantesData.EDAD_MAXIMA);
		int indiceSfcncaIps = result.findColumn(ConstantesData.SFCNCA_IPS);
		int indiceNvlPrrdd = result.findColumn(ConstantesData.NIVEL_PRIORIDAD);
		int indiceIncoVgnca = result.findColumn(ConstantesData.INICIO_VIGENCIA);
		int indiceFnVgnca = result.findColumn(ConstantesData.FIN_VIGENCIA);

		lDto = new ArrayList<DireccionamientoNormalDTO>();

		while (result.next()) {

			DireccionamientoNormalDTO dto = new DireccionamientoNormalDTO();
			dto.setCdgoIntrnoNormal(result.getString(indiceCdgoIntrno));
			dto.setNmbreScrslNormal(result.getString(indiceNmbreScrsl));
			dto.setDscrpcnSdeNormal(result.getString(indiceDscrpcnSde));
			dto.setDscrpcnZnaNormal(result.getString(indiceDscrpcnZna));
			dto.setDscrpcnPlnNormal(result.getString(indiceDscrpcnPln));
			dto.setEddMnmaNormal(result.getInt(indiceEddMnma));
			dto.setEddMxmaNormal(result.getInt(indiceEddMxma));
			dto.setSfcncaIpsNormal(result.getString(indiceSfcncaIps));
			dto.setNvlPrrddNormal(result.getString(indiceNvlPrrdd));
			dto.setIncoVgncaNormal(result.getDate(indiceIncoVgnca));
			dto.setFnVgncaNormal(result.getDate(indiceFnVgnca));
			lDto.add(dto);
		}
	}

	public List<DireccionamientoNormalDTO> getlDto() {
		return lDto;
	}
}
