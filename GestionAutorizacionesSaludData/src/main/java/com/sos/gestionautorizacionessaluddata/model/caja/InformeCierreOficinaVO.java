package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsula la informacion del informe de cierre
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class InformeCierreOficinaVO implements Serializable {
	private static final long serialVersionUID = -8579070541434919707L;
	private Double totalBaseTabla;
	private Double totalFaltanteTabla;
	private Double totalSobranteTabla;
	private Double totalEfectivoCajaTabla;
	private Double totalCierreOficina;
	private CierreOficinaVO cierreOficinaVO;
	private CoordinadorVO coordinadorVO;

	public Double getTotalBaseTabla() {
		return totalBaseTabla;
	}

	public void setTotalBaseTabla(Double totalBaseTabla) {
		this.totalBaseTabla = totalBaseTabla;
	}

	public Double getTotalFaltanteTabla() {
		return totalFaltanteTabla;
	}

	public void setTotalFaltanteTabla(Double totalFaltanteTabla) {
		this.totalFaltanteTabla = totalFaltanteTabla;
	}

	public Double getTotalSobranteTabla() {
		return totalSobranteTabla;
	}

	public void setTotalSobranteTabla(Double totalSobranteTabla) {
		this.totalSobranteTabla = totalSobranteTabla;
	}

	public Double getTotalEfectivoCajaTabla() {
		return totalEfectivoCajaTabla;
	}

	public void setTotalEfectivoCajaTabla(Double totalEfectivoCajaTabla) {
		this.totalEfectivoCajaTabla = totalEfectivoCajaTabla;
	}

	public Double getTotalCierreOficina() {
		return totalCierreOficina;
	}

	public void setTotalCierreOficina(Double totalCierreOficina) {
		this.totalCierreOficina = totalCierreOficina;
	}

	public CierreOficinaVO getCierreOficinaVO() {
		return cierreOficinaVO;
	}

	public void setCierreOficinaVO(CierreOficinaVO cierreOficinaVO) {
		this.cierreOficinaVO = cierreOficinaVO;
	}

	public CoordinadorVO getCoordinadorVO() {
		return coordinadorVO;
	}

	public void setCoordinadorVO(CoordinadorVO coordinadorVO) {
		this.coordinadorVO = coordinadorVO;
	}
}