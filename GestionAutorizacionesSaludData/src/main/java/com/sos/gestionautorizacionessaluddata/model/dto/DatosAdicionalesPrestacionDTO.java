package com.sos.gestionautorizacionessaluddata.model.dto;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;


/**
 * Class PrestacionDTO
 * Clase DTO con la informacion adiconal de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/07/2017
 *
 */

public class DatosAdicionalesPrestacionDTO implements Serializable{

	private static final long serialVersionUID = -7092403696070066806L;
	
	private TipoCodificacionVO tipoCodificacionVO;	
	private String codigoCodificacionPrestacion;
	private String descripcionCodificacionPrestacion;	
	private String descripcionEstadoPrestacion;
	private String nombreUsuarioAutoriza;
	private String nombreUsuarioAudita;	
	private String requiereOtraGestionAuditor;
	private String justificacionAuditor;
	private String codigoInternoDireccionamiento;
	private String prestadorDireccionamiento;
	private String nivelAtencion;	
	
	public TipoCodificacionVO getTipoCodificacionVO() {
		return tipoCodificacionVO;
	}
	public void setTipoCodificacionVO(TipoCodificacionVO tipoCodificacionVO) {
		this.tipoCodificacionVO = tipoCodificacionVO;
	}
	public String getCodigoCodificacionPrestacion() {
		return codigoCodificacionPrestacion;
	}
	public void setCodigoCodificacionPrestacion(String codigoCodificacionPrestacion) {
		this.codigoCodificacionPrestacion = codigoCodificacionPrestacion;
	}
	public String getDescripcionCodificacionPrestacion() {
		return descripcionCodificacionPrestacion;
	}
	public void setDescripcionCodificacionPrestacion(
			String descripcionCodificacionPrestacion) {
		this.descripcionCodificacionPrestacion = descripcionCodificacionPrestacion;
	}
	public String getDescripcionEstadoPrestacion() {
		return descripcionEstadoPrestacion;
	}
	public void setDescripcionEstadoPrestacion(String descripcionEstadoPrestacion) {
		this.descripcionEstadoPrestacion = descripcionEstadoPrestacion;
	}
	public String getNombreUsuarioAutoriza() {
		return nombreUsuarioAutoriza;
	}
	public void setNombreUsuarioAutoriza(String nombreUsuarioAutoriza) {
		this.nombreUsuarioAutoriza = nombreUsuarioAutoriza;
	}
	public String getNombreUsuarioAudita() {
		return nombreUsuarioAudita;
	}
	public void setNombreUsuarioAudita(String nombreUsuarioAudita) {
		this.nombreUsuarioAudita = nombreUsuarioAudita;
	}
	public String getRequiereOtraGestionAuditor() {
		return requiereOtraGestionAuditor;
	}
	public void setRequiereOtraGestionAuditor(String requiereOtraGestionAuditor) {
		this.requiereOtraGestionAuditor = requiereOtraGestionAuditor;
	}
	public String getJustificacionAuditor() {
		return justificacionAuditor;
	}
	public void setJustificacionAuditor(String justificacionAuditor) {
		this.justificacionAuditor = justificacionAuditor;
	}	
	public String getCodigoInternoDireccionamiento() {
		return codigoInternoDireccionamiento;
	}
	public void setCodigoInternoDireccionamiento(
			String codigoInternoDireccionamiento) {
		this.codigoInternoDireccionamiento = codigoInternoDireccionamiento;
	}
	public String getPrestadorDireccionamiento() {
		return prestadorDireccionamiento;
	}
	public void setPrestadorDireccionamiento(String prestadorDireccionamiento) {
		this.prestadorDireccionamiento = prestadorDireccionamiento;
	}
	public String getNivelAtencion() {
		return nivelAtencion;
	}
	public void setNivelAtencion(String nivelAtencion) {
		this.nivelAtencion = nivelAtencion;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

