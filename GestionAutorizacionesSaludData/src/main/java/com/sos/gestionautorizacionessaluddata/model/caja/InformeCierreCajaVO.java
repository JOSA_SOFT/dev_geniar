package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Encapsula la informacion del informe del cierre
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class InformeCierreCajaVO implements Serializable {
	private static final long serialVersionUID = -3193386423807315998L;
	private String userLogin;
	private Date fecha;
	private String oficinaNombre;
	private String responsable;

	private Double base;

	private Double totalEfectivoRecaudado;
	private Double totalNotasCredito;
	private Double totalRecaudado;

	private Double totalCaja;
	private Double faltante;
	private Double sobrante;
	
	private Date fechaApertura;

	private List<DenominacionVO> listMonedas;
	private List<DenominacionVO> listBilletes;
	private List<CausalDescuadreVO> listCausalesDescuadre;

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getOficinaNombre() {
		return oficinaNombre;
	}

	public void setOficinaNombre(String oficinaNombre) {
		this.oficinaNombre = oficinaNombre;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Double getBase() {
		return base;
	}

	public void setBase(Double base) {
		this.base = base;
	}

	public Double getTotalEfectivoRecaudado() {
		return totalEfectivoRecaudado;
	}

	public void setTotalEfectivoRecaudado(Double totalEfectivoRecaudado) {
		this.totalEfectivoRecaudado = totalEfectivoRecaudado;
	}

	public Double getTotalNotasCredito() {
		return totalNotasCredito;
	}

	public void setTotalNotasCredito(Double totalNotasCredito) {
		this.totalNotasCredito = totalNotasCredito;
	}

	public Double getTotalRecaudado() {
		return totalRecaudado;
	}

	public void setTotalRecaudado(Double totalRecaudado) {
		this.totalRecaudado = totalRecaudado;
	}

	public Double getTotalCaja() {
		return totalCaja;
	}

	public void setTotalCaja(Double totalCaja) {
		this.totalCaja = totalCaja;
	}

	public Double getFaltante() {
		return faltante;
	}

	public void setFaltante(Double faltante) {
		this.faltante = faltante;
	}

	public Double getSobrante() {
		return sobrante;
	}

	public void setSobrante(Double sobrante) {
		this.sobrante = sobrante;
	}

	public List<DenominacionVO> getListMonedas() {
		return listMonedas;
	}

	public void setListMonedas(List<DenominacionVO> listMonedas) {
		this.listMonedas = listMonedas;
	}

	public List<DenominacionVO> getListBilletes() {
		return listBilletes;
	}

	public void setListBilletes(List<DenominacionVO> listBilletes) {
		this.listBilletes = listBilletes;
	}

	public List<CausalDescuadreVO> getListCausalesDescuadre() {
		return listCausalesDescuadre;
	}

	public void setListCausalesDescuadre(
			List<CausalDescuadreVO> listCausalesDescuadre) {
		this.listCausalesDescuadre = listCausalesDescuadre;
	}

	public Date getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
}