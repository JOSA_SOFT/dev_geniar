package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class DevolucionVO
 * Clase VO para guardar las Devoluciones
 * @author ing. Rafael Cano
 * @version 12/07/2016
 *
 */

public class DevolucionVO implements Serializable {
	private static final long serialVersionUID = -8940914147427996506L;
	private int consPresolicitud;
	private int consMotivo;
	private String usuario;
	private String observacion;
	
	public int getConsPresolicitud() {
		return consPresolicitud;
	}
	public void setConsPresolicitud(int consPresolicitud) {
		this.consPresolicitud = consPresolicitud;
	}
	public int getConsMotivo() {
		return consMotivo;
	}
	public void setConsMotivo(int consMotivo) {
		this.consMotivo = consMotivo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
}