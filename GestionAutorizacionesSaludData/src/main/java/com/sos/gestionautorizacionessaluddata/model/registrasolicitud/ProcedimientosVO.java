package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class ProcedimientosVO
 * Clase VO para los procedimientos
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/12/2015
 *
 */
public class ProcedimientosVO implements Serializable {
	private static final long serialVersionUID = 4098187957333350786L;

	/*Identifica el consecutivo del procedimiento*/
	private Integer consecutivoCodificacionProcedimiento;
	
	/*Identifica el codigo del procedimiento*/
	private String codigoCodificacionProcedimiento;
	
	/*Identifica la descricion del procedimiento*/
	private String descripcionCodificacionProcedimiento;
	
	/*Identifica el codigo del servicio*/
	private String codigoServicio;
	
	/*Identifica la descripcion del servicio*/
	private String descripcionServicio;
	
	/*Identifica el consecutivo de la lateralidad*/
	private Integer consecutivoLateralidad;
	
	/*Identifica el consecutivo de la clase via acceso*/
	private Integer consecutivoViaAcceso;
	
	/*Identifica la cantidad de CUP*/
	private Integer cantidadProcedimiento;
	
	/*Identifica si el medicamento es no pos*/
	private String indicadorNoPOS;
	
	/*Identifica el consecutivo del item de presupuesto*/
	private int consecutivoItemPresupuesto;
	
	private String estanciaProcedimiento;
	
	/*Identifica si aplica lateralidad*/
	private String aplicaLaterialidad;

	
	public ProcedimientosVO() {
		super();
	}
	
	public ProcedimientosVO(String codigoCodificacionProcedimiento, String descripcionCodificacionProcedimiento) {
		super();
		this.codigoCodificacionProcedimiento = codigoCodificacionProcedimiento;
		this.descripcionCodificacionProcedimiento = descripcionCodificacionProcedimiento;
	}
	
	public Integer getConsecutivoCodificacionProcedimiento() {
		return consecutivoCodificacionProcedimiento;
	}

	public void setConsecutivoCodificacionProcedimiento(
			Integer consecutivoCodificacionProcedimiento) {
		this.consecutivoCodificacionProcedimiento = consecutivoCodificacionProcedimiento;
	}	

	public String getCodigoCodificacionProcedimiento() {
		return codigoCodificacionProcedimiento;
	}

	public void setCodigoCodificacionProcedimiento(
			String codigoCodificacionProcedimiento) {
		this.codigoCodificacionProcedimiento = codigoCodificacionProcedimiento;
	}

	public String getDescripcionCodificacionProcedimiento() {
		return descripcionCodificacionProcedimiento;
	}

	public void setDescripcionCodificacionProcedimiento(
			String descripcionCodificacionProcedimiento) {
		this.descripcionCodificacionProcedimiento = descripcionCodificacionProcedimiento;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getDescripcionServicio() {
		return descripcionServicio;
	}

	public void setDescripcionServicio(String descripcionServicio) {
		this.descripcionServicio = descripcionServicio;
	}

	/**
	 * @return the consecutivoLaterialidad
	 */
	public Integer getConsecutivoLaterialidad() {
		return consecutivoLateralidad;
	}

	/**
	 * @param consecutivoLaterialidad the consecutivoLaterialidad to set
	 */
	public void setConsecutivoLaterialidad(Integer consecutivoLateralidad) {
		this.consecutivoLateralidad = consecutivoLateralidad;
	}

	/**
	 * @return the cantidadProcedimiento
	 */
	public Integer getCantidadProcedimiento() {
		return cantidadProcedimiento;
	}

	/**
	 * @param cantidadProcedimiento the cantidadProcedimiento to set
	 */
	public void setCantidadProcedimiento(Integer cantidadProcedimiento) {
		this.cantidadProcedimiento = cantidadProcedimiento;
	}
	
	public String getIndicadorNoPOS() {
		return indicadorNoPOS;
	}

	public void setIndicadorNoPOS(String indicadorNoPOS) {
		this.indicadorNoPOS = indicadorNoPOS;
	}
		
	public int getConsecutivoItemPresupuesto() {
		return consecutivoItemPresupuesto;
	}

	public void setConsecutivoItemPresupuesto(int consecutivoItemPresupuesto) {
		this.consecutivoItemPresupuesto = consecutivoItemPresupuesto;
	}
	
	public String getEstanciaProcedimiento() {
		return estanciaProcedimiento;
	}

	public void setEstanciaProcedimiento(String estanciaProcedimiento) {
		this.estanciaProcedimiento = estanciaProcedimiento;
	}
	
	public String getAplicaLaterialidad() {
		return aplicaLaterialidad;
	}
	
	public void setAplicaLaterialidad(String aplicaLaterialidad) {
		this.aplicaLaterialidad = aplicaLaterialidad;
	}

	public Integer getConsecutivoViaAcceso() {
		return consecutivoViaAcceso;
	}

	public void setConsecutivoViaAcceso(Integer consecutivoViaAcceso) {
		this.consecutivoViaAcceso = consecutivoViaAcceso;
	}
	
}
