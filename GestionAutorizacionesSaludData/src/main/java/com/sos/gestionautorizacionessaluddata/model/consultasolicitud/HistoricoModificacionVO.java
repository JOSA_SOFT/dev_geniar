package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;

public class HistoricoModificacionVO implements Serializable {
	private static final long serialVersionUID = -4727343810114833492L;

	/*Identifica la descricion del elemento modificado*/
	private String elementoModificacion;
	
	/*Identifica el número Autorización de la OPS*/	
	private Integer numeroAutorizacion;
	
	/*Identifica la descricion de la Prestación*/
	private String descripcionPrestacion;	
		
	/*Identifica el tipo de Modificación*/
	private String tipoModificacion;
	
	/*Identifica la fecha de modificación */
	private Date fechaModificacion;

	/*Identifica el valor antes de la modificación*/	
	private String valorAnterior;

	/*Identifica el valor después de la modificación*/	
	private String valorNuevo;

	/*Identifica el número Autorización de la OPS antes de la modificación*/	
	private Integer numAutorizacionAnterior;
		
	/*Identifica el usuario que realiza la modificación*/
	private String usuarioModificacion;
	
	/*Identifica la causal de la modificación*/
	private String causalModificacion;
	
	/*Identifica las observaciones de la modificación*/
	private String observacionModificacion;

	public String getElementoModificacion() {
		return elementoModificacion;
	}

	public void setElementoModificacion(String elementoModificacion) {
		this.elementoModificacion = elementoModificacion;
	}

	public Integer getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(Integer numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	public String getTipoModificacion() {
		return tipoModificacion;
	}

	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(String valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public String getValorNuevo() {
		return valorNuevo;
	}

	public void setValorNuevo(String valorNuevo) {
		this.valorNuevo = valorNuevo;
	}

	public Integer getNumAutorizacionAnterior() {
		return numAutorizacionAnterior;
	}

	public void setNumAutorizacionAnterior(Integer numAutorizacionAnterior) {
		this.numAutorizacionAnterior = numAutorizacionAnterior;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getCausalModificacion() {
		return causalModificacion;
	}

	public void setCausalModificacion(String causalModificacion) {
		this.causalModificacion = causalModificacion;
	}

	public String getObservacionModificacion() {
		return observacionModificacion;
	}

	public void setObservacionModificacion(String observacionModificacion) {
		this.observacionModificacion = observacionModificacion;
	}
		
}
