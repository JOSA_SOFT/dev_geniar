package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class ContingenciaRecobroVO
 * Clase VO para la relacion de la contingencia con los recobros
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/12/2015
 *
 */
public class ContingenciaRecobroVO implements Serializable{
	private static final long serialVersionUID = -3556662125802823290L;

	/*Identifica el consecutivo codigo de recobro.*/
	private Integer consecutivoCodigoRecobro;
	
	/*Identifica la descripciion del recobro.*/
	private String descripcionRecobro;
	
	/*Identifica si el recobro necesita programacion.*/
	private String solicitaProgramacion;
	
	/*Identifica si el recobro aplicar para plan pos o no.*/
	private String aplicaPos;	
	
	public Integer getConsecutivoCodigoRecobro() {
		return consecutivoCodigoRecobro;
	}
	public void setConsecutivoCodigoRecobro(Integer consecutivoCodigoRecobro) {
		this.consecutivoCodigoRecobro = consecutivoCodigoRecobro;
	}
	public String getDescripcionRecobro() {
		return descripcionRecobro;
	}
	public void setDescripcionRecobro(String descripcionRecobro) {
		this.descripcionRecobro = descripcionRecobro;
	}
	public String getSolicitaProgramacion() {
		return solicitaProgramacion;
	}
	public void setSolicitaProgramacion(String solicitaProgramacion) {
		this.solicitaProgramacion = solicitaProgramacion;
	}
	public String getAplicaPos() {
		return aplicaPos;
	}
	public void setAplicaPos(String aplicaPos) {
		this.aplicaPos = aplicaPos;
	}
}
