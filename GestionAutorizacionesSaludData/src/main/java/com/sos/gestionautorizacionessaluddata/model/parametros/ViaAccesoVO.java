package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ViaAccesoVO
 * Clase VO para las Vias de Aceeso de la Prestacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */
public class ViaAccesoVO implements Serializable {
	private static final long serialVersionUID = -1006187977179097892L;
	private Integer consecutivoViaAcceso;
	private String descripcionViaAcceso;
	
	public Integer getConsecutivoViaAcceso() {
		return consecutivoViaAcceso;
	}
	public void setConsecutivoViaAcceso(Integer consecutivoViaAcceso) {
		this.consecutivoViaAcceso = consecutivoViaAcceso;
	}
	public String getDescripcionViaAcceso() {
		return descripcionViaAcceso;
	}
	public void setDescripcionViaAcceso(String descripcionViaAcceso) {
		this.descripcionViaAcceso = descripcionViaAcceso;
	}
}
