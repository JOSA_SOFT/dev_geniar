package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;

/**
 * VO sobre los datos del recibo de caja a imprimir.
 * 
 * @author JOSE OLMEDO SOTO AGUIRRE - GENIAR S.A.S
 *
 */
public class ReciboCajaVO implements Serializable {
	private static final long serialVersionUID = 7255798101296723488L;
	private String numeroReciboCaja;
	private InformacionOpsValorVO opsCanceladas;
	private String observaciones;

	private String estado;
	private String usuario;
	private String fechaCreacion;
	private double totalPagar;
	private double sumatoriaNotasCredito = 0;
	private double sumatoriaValorOps = 0;
	private double saldoFavor = 0;
	private double valorEfectivo = 0;
	private double valorRecibido = 0;
	private double valorDevolver = 0;
	private DatosSolicitudVO solicitudSeleccionada;

	public String getNumeroReciboCaja() {
		return numeroReciboCaja;
	}

	public void setNumeroReciboCaja(String numeroReciboCaja) {
		this.numeroReciboCaja = numeroReciboCaja;
	}

	public InformacionOpsValorVO getOpsCanceladas() {
		return opsCanceladas;
	}

	public void setOpsCanceladas(InformacionOpsValorVO opsCanceladas) {
		this.opsCanceladas = opsCanceladas;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public double getTotalPagar() {
		return totalPagar;
	}

	public void setTotalPagar(double totalPagar) {
		this.totalPagar = totalPagar;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setSumatoriaValorOps(double sumatoriaValorOps) {
		this.sumatoriaValorOps = sumatoriaValorOps;
	}

	public double getSumatoriaValorOps() {
		return sumatoriaValorOps;
	}

	public void setSumatoriaNotasCredito(double sumatoriaNotasCredito) {
		this.sumatoriaNotasCredito = sumatoriaNotasCredito;
	}

	public double getSumatoriaNotasCredito() {
		return sumatoriaNotasCredito;
	}

	public void setValorEfectivo(double valorEfectivo) {
		this.valorEfectivo = valorEfectivo;
	}

	public double getValorEfectivo() {
		return valorEfectivo;
	}

	public void setSaldoFavor(double saldoFavor) {
		this.saldoFavor = saldoFavor;
	}

	public double getSaldoFavor() {
		return saldoFavor;
	}

	public void setValorRecibido(double valorRecibido) {
		this.valorRecibido = valorRecibido;

	}

	public double getValorRecibido() {
		return valorRecibido;
	}

	public void setValorDevolver(double valorDevolver) {
		this.valorDevolver = valorDevolver;
	}

	public double getValorDevolver() {
		return valorDevolver;
	}

	public void setSolicitudSeleccionada(DatosSolicitudVO solicitudSeleccionada) {
		this.solicitudSeleccionada = solicitudSeleccionada;
	}

	public DatosSolicitudVO getSolicitudSeleccionada() {
		return solicitudSeleccionada;
	}
}
