package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;

/**
 * Class DatosSolicitudVO
 * Clase VO que contiene los datos de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/03/2016
 *
 */
public class ConsultaSolicitudVO implements Serializable {
	private static final long serialVersionUID = 2651382275405837512L;

	/*Identifica el numero de solicitud*/
	private String numeroSolicitudProveedor;
	
	/*Identifica el numero de radicado de la solicitud*/
	private String numeroRadicadoSolicitud;
	
	/*Identifica el numero de autorizacion del servicios*/
	private Integer numeroAutorizacionOPS;
	
	/*Identifica la fecha desde de consulta*/
	private Date fechaDesde;
	
	/*Identifica la fecha hasta de consulta*/
	private Date fechaHasta;
	
	/*Identifica el consecutivo del plan*/
	private Integer consecutivoPlan;
	
	/*Identifica el consecutivo del servicio solicitado PIS, CUMS, CUOS*/
	private Integer consecutivoServicioSolicitado;
	
	/*Identifica el consecutivo del tipo de identificacion del prestador */
	private Integer consecutivoTipoIdentificacionPrestador;
	
	/*Identifica el numero de identificacion del prestador */
	private String numeroIdentificacionPrestador; 
	
	/*Identifica el codigo interno del prestador */
	private String codigoInternoPrestador;	
	
	/*Identifica el numero unico de identificacion de afiliado*/
	private Integer numeroUnicoIdentificacionAfiliado;
	

	public String getNumeroSolicitudProveedor() {
		return numeroSolicitudProveedor;
	}

	public void setNumeroSolicitudProveedor(String numeroSolicitudProveedor) {
		this.numeroSolicitudProveedor = numeroSolicitudProveedor;
	}	

	public String getNumeroRadicadoSolicitud() {
		return numeroRadicadoSolicitud;
	}

	public void setNumeroRadicadoSolicitud(String numeroRadicadoSolicitud) {
		this.numeroRadicadoSolicitud = numeroRadicadoSolicitud;
	}

	public Integer getNumeroAutorizacionOPS() {
		return numeroAutorizacionOPS;
	}

	public void setNumeroAutorizacionOPS(Integer numeroAutorizacionOPS) {
		this.numeroAutorizacionOPS = numeroAutorizacionOPS;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Integer getConsecutivoPlan() {
		return consecutivoPlan;
	}

	public void setConsecutivoPlan(Integer consecutivoPlan) {
		this.consecutivoPlan = consecutivoPlan;
	}

	public Integer getConsecutivoServicioSolicitado() {
		return consecutivoServicioSolicitado;
	}

	public void setConsecutivoServicioSolicitado(
			Integer consecutivoServicioSolicitado) {
		this.consecutivoServicioSolicitado = consecutivoServicioSolicitado;
	}

	public Integer getConsecutivoTipoIdentificacionPrestador() {
		return consecutivoTipoIdentificacionPrestador;
	}

	public void setConsecutivoTipoIdentificacionPrestador(
			Integer consecutivoTipoIdentificacionPrestador) {
		this.consecutivoTipoIdentificacionPrestador = consecutivoTipoIdentificacionPrestador;
	}

	public String getNumeroIdentificacionPrestador() {
		return numeroIdentificacionPrestador;
	}

	public void setNumeroIdentificacionPrestador(
			String numeroIdentificacionPrestador) {
		this.numeroIdentificacionPrestador = numeroIdentificacionPrestador;
	}

	public String getCodigoInternoPrestador() {
		return codigoInternoPrestador;
	}

	public void setCodigoInternoPrestador(String codigoInternoPrestador) {
		this.codigoInternoPrestador = codigoInternoPrestador;
	}
	
	public Integer getNumeroUnicoIdentificacionAfiliado() {
		return numeroUnicoIdentificacionAfiliado;
	}

	public void setNumeroUnicoIdentificacionAfiliado(
			Integer numeroUnicoIdentificacionAfiliado) {
		this.numeroUnicoIdentificacionAfiliado = numeroUnicoIdentificacionAfiliado;
	}

}
