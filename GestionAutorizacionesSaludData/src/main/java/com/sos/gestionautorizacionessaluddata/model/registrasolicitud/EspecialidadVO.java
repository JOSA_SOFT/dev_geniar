package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class EspecialidadVO
 * Clase VO para las especialidades de los medicos
 * @author ing. Victor Hugo Gil Ramos
 * @version 06/01/2016
 *
 */
public class EspecialidadVO implements Serializable{
	private static final long serialVersionUID = -797987103025283210L;

	/*Identifica el consecutivo de la especialidad del medico*/
	private Integer consecutivoCodigoEspecialidad;
	
	/*Identifica el codigo de la especialidad del medico*/
	private String codigoEspecialidad;
	
	/*Identifica la descripcion de la especialidad del medico*/
	private String descripcionEspecialidad;
	
	public Integer getConsecutivoCodigoEspecialidad() {
		return consecutivoCodigoEspecialidad;
	}

	public void setConsecutivoCodigoEspecialidad(Integer consecutivoCodigoEspecialidad) {
		this.consecutivoCodigoEspecialidad = consecutivoCodigoEspecialidad;
	}

	public String getCodigoEspecialidad() {
		return codigoEspecialidad;
	}

	public void setCodigoEspecialidad(String codigoEspecialidad) {
		this.codigoEspecialidad = codigoEspecialidad;
	}

	public String getDescripcionEspecialidad() {
		return descripcionEspecialidad;
	}

	public void setDescripcionEspecialidad(String descripcionEspecialidad) {
		this.descripcionEspecialidad = descripcionEspecialidad;
	}
}
