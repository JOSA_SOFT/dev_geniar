package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;

/**
 * Class PrestacionPisVO
 * Clase VO para las prestaciones pis
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/03/2016
 *
 */
public class PrestacionPisVO implements Serializable {
	private static final long serialVersionUID = -2083511130170319872L;

	/*Identifica el consecutivo de la prestacion pis*/
	private Integer consecutivoPrestacionPis;
	
	/*Identifica el consecutivo de la prestacion pis*/
	private String codigoPrestacionPis;
		
	/*Identifica la descripcion de la prestacion pis*/
	private String descripcionPrestacionPis;
		
	/*Identifica el consecutivo del item de presupuesto*/
	private int consecutivoItemPresupuesto;
	
	public PrestacionPisVO() {
		super();		
	}

	public PrestacionPisVO(String codigoPrestacionPis, String descripcionPrestacionPis) {
		super();
		this.codigoPrestacionPis = codigoPrestacionPis;
		this.descripcionPrestacionPis = descripcionPrestacionPis;
	}

	public Integer getConsecutivoPrestacionPis() {
		return consecutivoPrestacionPis;
	}

	public void setConsecutivoPrestacionPis(Integer consecutivoPrestacionPis) {
		this.consecutivoPrestacionPis = consecutivoPrestacionPis;
	}

	public String getCodigoPrestacionPis() {
		return codigoPrestacionPis;
	}

	public void setCodigoPrestacionPis(String codigoPrestacionPis) {
		this.codigoPrestacionPis = codigoPrestacionPis;
	}

	public String getDescripcionPrestacionPis() {
		return descripcionPrestacionPis;
	}

	public void setDescripcionPrestacionPis(String descripcionPrestacionPis) {
		this.descripcionPrestacionPis = descripcionPrestacionPis;
	}

	public int getConsecutivoItemPresupuesto() {
		return consecutivoItemPresupuesto;
	}

	public void setConsecutivoItemPresupuesto(int consecutivoItemPresupuesto) {
		this.consecutivoItemPresupuesto = consecutivoItemPresupuesto;
	}
}
