package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * encarga de almacenar la informacion de contacto.
 * @author GENIAR
 *
 */
public class DatosContactoVO implements Serializable {
	private static final long serialVersionUID = 7371449705335668797L;
	private int consecutivoMetodoDevolucion;
	private String numeroCuenta;
	private int consecutivoBanco;
	private String tipoCuenta;
	private String titular;
	private String telefono;
	private String correoElectronico;
	private Integer consecutivoDatoContacto;
	
	public int getConsecutivoMetodoDevolucion() {
		return consecutivoMetodoDevolucion;
	}
	public void setConsecutivoMetodoDevolucion(int consecutivoMetodoDevolucion) {
		this.consecutivoMetodoDevolucion = consecutivoMetodoDevolucion;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public int getConsecutivoBanco() {
		return consecutivoBanco;
	}
	public void setConsecutivoBanco(int consecutivoBanco) {
		this.consecutivoBanco = consecutivoBanco;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public Integer getConsecutivoDatoContacto() {
		return consecutivoDatoContacto;
	}
	public void setConsecutivoDatoContacto(Integer consecutivoDatoContacto) {
		this.consecutivoDatoContacto = consecutivoDatoContacto;
	}
}
