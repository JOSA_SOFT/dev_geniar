package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;


/**
 * Class RecienNacidoVO
 * Clase VO del recien nacido
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class RecienNacidoVO implements Serializable{
	private static final long serialVersionUID = -9061579351820408342L;

	/*Identifica si es recien nacido*/
	private String recienNacido;	

	private Date fechaNacimientoRecienNacido;
	
	/*Identifica el objeto del genero del recien nacido*/
	private GeneroVO generoVO;
	
	/*Identifica si es es parto multiple*/
	private String partoMultiple;
	
	/*Identifica el numero de hijos*/
	private Integer numeroHijos;	
	
	
	public String getRecienNacido() {
		return recienNacido;
	}

	public void setRecienNacido(String recienNacido) {
		this.recienNacido = recienNacido;
	}

	public Date getFechaNacimientoRecienNacido() {
		return fechaNacimientoRecienNacido;
	}

	public void setFechaNacimientoRecienNacido(Date fechaNacimientoRecienNacido) {
		this.fechaNacimientoRecienNacido = fechaNacimientoRecienNacido;
	}

	public GeneroVO getGeneroVO() {
		return generoVO;
	}

	public void setGeneroVO(GeneroVO generoVO) {
		this.generoVO = generoVO;
	}

	public String getPartoMultiple() {
		return partoMultiple;
	}

	public void setPartoMultiple(String partoMultiple) {
		this.partoMultiple = partoMultiple;
	}

	public Integer getNumeroHijos() {
		return numeroHijos;
	}

	public void setNumeroHijos(Integer numeroHijos) {
		this.numeroHijos = numeroHijos;
	}
}
