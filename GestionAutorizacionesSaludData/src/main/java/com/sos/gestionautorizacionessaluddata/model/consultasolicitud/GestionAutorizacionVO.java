package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;

/**
 * Class GestionAutorizacionVO
 * Clase VO que contiene la gestión de la autorización de las prestaciones
 * @author ing. Jorge Rodriguez De León
 * @version 13/12/2016
 *
 */
public class GestionAutorizacionVO implements Serializable {
	private static final long serialVersionUID = 5605985680730044639L;

	/*contiene el numero de autorización de la prestación asignado por la IPS*/
	private String 	numeroAtorizacionPrestacionIPS;
	
	/*Consecutivo de la causa por la cual se autoriza la prestación*/
	private int		consecutivoCodigoCausaNovedad;
	
	/*Observaciones ingresedas por el usuario autorizador*/
	private String 	observaciones;
	
	/*Codigo interno del prestador al cual se le asigna la prestación*/
	private String	codigoInterno;

	public String getNumeroAtorizacionPrestacionIPS() {
		return numeroAtorizacionPrestacionIPS;
	}

	public void setNumeroAtorizacionPrestacionIPS(
			String numeroAtorizacionPrestacionIPS) {
		this.numeroAtorizacionPrestacionIPS = numeroAtorizacionPrestacionIPS;
	}

	public int getConsecutivoCodigoCausaNovedad() {
		return consecutivoCodigoCausaNovedad;
	}

	public void setConsecutivoCodigoCausaNovedad(int consecutivoCodigoCausaNovedad) {
		this.consecutivoCodigoCausaNovedad = consecutivoCodigoCausaNovedad;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	

}
