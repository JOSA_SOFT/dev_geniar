package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;


/**
 * Class MedicoVO
 * Clase VO del medico
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class MedicoVO implements Serializable{
	private static final long serialVersionUID = -7730667059790056653L;

	/*Identifica el tipo de identificacion del medico */
	private TiposIdentificacionVO tiposIdentificacionMedicoVO;
	
	/*Identifica el numero de identificacion del medico */
	private String numeroIdentificacionMedico;
	
	/*Identifica el numero de unico de identificacion del medico */
	private String numeroUnicoIdentificacionMedico;
	
	/*Identifica el registro medico */
	private String registroMedico;
	
	/*Identifica el primer nombre del medico */
	private String primerNombreMedico;
	
	/*Identifica el segundo nombre del medico */
	private String segundoNombreMedico;
	
	/*Identifica el primer apellido del medico */
	private String primerApellidoMedico;
	
	/*Identifica el segundo apellido del medico */
	private String segundoApellidoMedico;
	
	/*Identifica si el medico es adscrito */
	private String medicoAdscrito;
	
	/*Identifica la especialidad del medico */
	private EspecialidadVO especialidadMedicoVO;
	
	public TiposIdentificacionVO getTiposIdentificacionMedicoVO() {
		return tiposIdentificacionMedicoVO;
	}

	public void setTiposIdentificacionMedicoVO(
			TiposIdentificacionVO tiposIdentificacionMedicoVO) {
		this.tiposIdentificacionMedicoVO = tiposIdentificacionMedicoVO;
	}

	public String getNumeroIdentificacionMedico() {
		return numeroIdentificacionMedico;
	}

	public void setNumeroIdentificacionMedico(String numeroIdentificacionMedico) {
		this.numeroIdentificacionMedico = numeroIdentificacionMedico;
	}

	public String getNumeroUnicoIdentificacionMedico() {
		return numeroUnicoIdentificacionMedico;
	}

	public void setNumeroUnicoIdentificacionMedico(
			String numeroUnicoIdentificacionMedico) {
		this.numeroUnicoIdentificacionMedico = numeroUnicoIdentificacionMedico;
	}

	public String getRegistroMedico() {
		return registroMedico;
	}

	public void setRegistroMedico(String registroMedico) {
		this.registroMedico = registroMedico;
	}

	public String getPrimerNombreMedico() {
		return primerNombreMedico;
	}

	public void setPrimerNombreMedico(String primerNombreMedico) {
		this.primerNombreMedico = primerNombreMedico;
	}

	public String getSegundoNombreMedico() {
		return segundoNombreMedico;
	}

	public void setSegundoNombreMedico(String segundoNombreMedico) {
		this.segundoNombreMedico = segundoNombreMedico;
	}

	public String getPrimerApellidoMedico() {
		return primerApellidoMedico;
	}

	public void setPrimerApellidoMedico(String primerApellidoMedico) {
		this.primerApellidoMedico = primerApellidoMedico;
	}

	public String getSegundoApellidoMedico() {
		return segundoApellidoMedico;
	}

	public void setSegundoApellidoMedico(String segundoApellidoMedico) {
		this.segundoApellidoMedico = segundoApellidoMedico;
	}

	public String getMedicoAdscrito() {
		return medicoAdscrito;
	}

	public void setMedicoAdscrito(String medicoAdscrito) {
		this.medicoAdscrito = medicoAdscrito;
	}

	public EspecialidadVO getEspecialidadMedicoVO() {
		return especialidadMedicoVO;
	}

	public void setEspecialidadMedicoVO(EspecialidadVO especialidadMedicoVO) {
		this.especialidadMedicoVO = especialidadMedicoVO;
	}
}
