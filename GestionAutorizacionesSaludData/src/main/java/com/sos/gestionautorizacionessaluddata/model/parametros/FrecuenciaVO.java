package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class FrecuenciaVO
 * Clase VO de la frecuencia del medicamento
 * @author ing. Victor Hugo Gil Ramos
 * @version 18/12/2015
 *
 */
public class FrecuenciaVO implements Serializable{
	private static final long serialVersionUID = 3741961536179819595L;

	/*Consecutivo de la frecuencia*/
	private Integer consecutivoFrecuencia;
	
	/*Codigo de la frecuencia*/
	private String codigoFrecuencia;
	
	/*Descripcion de la frecuencia*/
	private String descripcionFrecuencia;
	
	public Integer getConsecutivoFrecuencia() {
		return consecutivoFrecuencia;
	}
	public void setConsecutivoFrecuencia(Integer consecutivoFrecuencia) {
		this.consecutivoFrecuencia = consecutivoFrecuencia;
	}
	public String getCodigoFrecuencia() {
		return codigoFrecuencia;
	}
	public void setCodigoFrecuencia(String codigoFrecuencia) {
		this.codigoFrecuencia = codigoFrecuencia;
	}
	public String getDescripcionFrecuencia() {
		return descripcionFrecuencia;
	}
	public void setDescripcionFrecuencia(String descripcionFrecuencia) {
		this.descripcionFrecuencia = descripcionFrecuencia;
	}
}
