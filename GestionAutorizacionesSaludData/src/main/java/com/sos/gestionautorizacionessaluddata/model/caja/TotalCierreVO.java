package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsula los totales del cierre
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class TotalCierreVO implements Serializable {
	private static final long serialVersionUID = -6266645952575757211L;
	private Double sumNotaCredito;
	private Double sumReciboCaja;

	/**
	 * Constructor
	 */
	public TotalCierreVO() {
		super();
		sumNotaCredito = 0.0;
		sumReciboCaja = 0.0;
	}

	public Double getSumNotaCredito() {
		return sumNotaCredito;
	}

	public void setSumNotaCredito(Double sumNotaCredito) {
		this.sumNotaCredito = sumNotaCredito;
	}

	public Double getSumReciboCaja() {
		return sumReciboCaja;
	}

	public void setSumReciboCaja(Double sumReciboCaja) {
		this.sumReciboCaja = sumReciboCaja;
	}

}
