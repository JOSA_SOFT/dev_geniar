package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Encapsula la informacion del reporte de nota credito
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class ReporteNotaCreditoVO implements Serializable {
	private static final long serialVersionUID = 5004403852049542879L;
	private Integer numeroNotaCredito;
	private String tipoEidentificacion;
	private String nombre;
	private Date fecha;
	private String oficina;
	private String valorLetras;
	private String tipoCuenta;
	private String numeroCuenta;
	private String nombreBanco;
	private String nombreUsuarioAtencion;
	private List<InformeOPSVO> listOPS;
	private Double sumaTotalOPSs;

	public Integer getNumeroNotaCredito() {
		return numeroNotaCredito;
	}

	public void setNumeroNotaCredito(Integer numeroNotaCredito) {
		this.numeroNotaCredito = numeroNotaCredito;
	}

	public String getTipoEidentificacion() {
		return tipoEidentificacion;
	}

	public void setTipoEidentificacion(String tipoEidentificacion) {
		this.tipoEidentificacion = tipoEidentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getValorLetras() {
		return valorLetras;
	}

	public void setValorLetras(String valorLetras) {
		this.valorLetras = valorLetras;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public String getNombreUsuarioAtencion() {
		return nombreUsuarioAtencion;
	}

	public void setNombreUsuarioAtencion(String nombreUsuarioAtencion) {
		this.nombreUsuarioAtencion = nombreUsuarioAtencion;
	}

	public List<InformeOPSVO> getListOPS() {
		return listOPS;
	}

	public void setListOPS(List<InformeOPSVO> listOPS) {
		this.listOPS = listOPS;
	}

	public Double getSumaTotalOPSs() {
		return sumaTotalOPSs;
	}

	public void setSumaTotalOPSs(Double sumaTotalOPSs) {
		this.sumaTotalOPSs = sumaTotalOPSs;
	}
}