package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;


/**
 * Class InconsistenciasResponseServiceVO
 * Clase VO del resultado de las inconsistencias de la malla
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/02/2016
 *
 */
public class InconsistenciasResponseServiceVO implements Serializable {
	
	private static final long serialVersionUID = 2458627300147318508L;

	private BigInteger consecutivoPrestacion;
	
	private String codigoPrestacion;
	
	private String descripcionPrestacion;
	
	/** consecutivo visual de la tabla*/
	private Integer numero;	
	
	/** consecutivo de la regla que se valido*/
	private Integer consecutivoRegla;	

	/** descripcion de la regla que se valido*/
	private String  descripcionRegla;
	
	/** mensaje del servicio*/
	private String mensaje;
	
	/** informacion adicional */
	private String  informacionAdicional;
	
	/** consecutivo del tipo de validacion */
	private Integer consecutivoTipoValidacion;
	
	/** descripcion del tipo de validacion */
	private String  descripcionTipoValidacion;
	
	/** informacion de lsita de las marcas de la inconsistencias */
	private List<MarcaInconsistenciaVO> lMarcaInconsistencia;
	
	/** informacion del estado de la solicitud */
	private EstadoVO estadoVO;	
	
	/** informacion si esta resuelta la inconsistencia */
	private boolean resuelta;
	
	/** informacion si esta confirmada la inconsistencia */
	private boolean confirmada;
	
	/** identifica el tipo de respuesta de la gestion */
	private String tipoRespuesta;
	

	public Integer getConsecutivoRegla() {
		return consecutivoRegla;
	}

	public void setConsecutivoRegla(Integer consecutivoRegla) {
		this.consecutivoRegla = consecutivoRegla;
	}

	public String getDescripcionRegla() {
		return descripcionRegla;
	}

	public void setDescripcionRegla(String descripcionRegla) {
		this.descripcionRegla = descripcionRegla;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getInformacionAdicional() {
		return informacionAdicional;
	}

	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	public Integer getConsecutivoTipoValidacion() {
		return consecutivoTipoValidacion;
	}

	public void setConsecutivoTipoValidacion(Integer consecutivoTipoValidacion) {
		this.consecutivoTipoValidacion = consecutivoTipoValidacion;
	}

	public String getDescripcionTipoValidacion() {
		return descripcionTipoValidacion;
	}

	public void setDescripcionTipoValidacion(String descripcionTipoValidacion) {
		this.descripcionTipoValidacion = descripcionTipoValidacion;
	}
	
	public EstadoVO getEstadoVO() {
		return estadoVO;
	}

	public void setEstadoVO(EstadoVO estadoVO) {
		this.estadoVO = estadoVO;
	}
	
	public List<MarcaInconsistenciaVO> getlMarcaInconsistencia() {
		return lMarcaInconsistencia;
	}

	public void setlMarcaInconsistencia(
			List<MarcaInconsistenciaVO> lMarcaInconsistencia) {
		this.lMarcaInconsistencia = lMarcaInconsistencia;
	}

	public boolean isResuelta() {
		return resuelta;
	}

	public void setResuelta(boolean resuelta) {
		this.resuelta = resuelta;
	}

	public boolean isConfirmada() {
		return confirmada;
	}

	public void setConfirmada(boolean confirmada) {
		this.confirmada = confirmada;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	public BigInteger getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	public void setConsecutivoPrestacion(BigInteger consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}
	
	public String getTipoRespuesta() {
		return tipoRespuesta;
	}

	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}

}
