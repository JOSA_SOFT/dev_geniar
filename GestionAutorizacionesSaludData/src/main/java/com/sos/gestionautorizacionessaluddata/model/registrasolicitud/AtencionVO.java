package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OrigenAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PrioridadAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoServicioVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;


/**
 * Class AtencionVO
 * Clase VO de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class AtencionVO implements Serializable{
	
	private static final long serialVersionUID = 4085322614588415714L;

	/*Identifica la fecha de solicitud profesional */
	private Date fechaSolicitudProfesional;
	
	/*Identifica el numero de solicitud SOS */
	private Integer numeroSolicitud;
	
	/*Identifica la clase de atencion */
	private ClaseAtencionVO claseAtencionVO;
	
	/*Identifica el tipo de servicio */
	private TipoServicioVO tipoServicioVO;
	
	/*Identifica la prioridad de la atencion */
	private PrioridadAtencionVO prioridadAtencionVO;
	
	/*Identifica el origen de la atencion */
	private OrigenAtencionVO origenAtencionVO;
	
	/*Identifica si la atencion es domiciliaria */
	private String domi;
	
	/*Identifica si la atencion supera el tope soat */
	private String superaTopeSoat;
	
	/*Identifica la justificacion clinica de la atencion */
	private String justificacionClinica;
	
	/*Identifica la ubicacion del paciente */
	private UbicacionPacienteVO ubicacionPacienteVO;
	
	/*Identifica la guia del atencion */
	private String manejoIntegralGuia;
	
	public Date getFechaSolicitudProfesional() {
		return fechaSolicitudProfesional;
	}

	public void setFechaSolicitudProfesional(Date fechaSolicitudProfesional) {
		this.fechaSolicitudProfesional = fechaSolicitudProfesional;
	}

	public Integer getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(Integer numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public ClaseAtencionVO getClaseAtencionVO() {
		return claseAtencionVO;
	}

	public void setClaseAtencionVO(ClaseAtencionVO claseAtencionVO) {
		this.claseAtencionVO = claseAtencionVO;
	}

	public TipoServicioVO getTipoServicioVO() {
		return tipoServicioVO;
	}

	public void setTipoServicioVO(TipoServicioVO tipoServicioVO) {
		this.tipoServicioVO = tipoServicioVO;
	}

	public PrioridadAtencionVO getPrioridadAtencionVO() {
		return prioridadAtencionVO;
	}

	public void setPrioridadAtencionVO(PrioridadAtencionVO prioridadAtencionVO) {
		this.prioridadAtencionVO = prioridadAtencionVO;
	}

	public OrigenAtencionVO getOrigenAtencionVO() {
		return origenAtencionVO;
	}

	public void setOrigenAtencionVO(OrigenAtencionVO origenAtencionVO) {
		this.origenAtencionVO = origenAtencionVO;
	}

	public String getDomi() {
		return domi;
	}

	public void setDomi(String domi) {
		this.domi = domi;
	}

	public String getSuperaTopeSoat() {
		return superaTopeSoat;
	}

	public void setSuperaTopeSoat(String superaTopeSoat) {
		this.superaTopeSoat = superaTopeSoat;
	}

	public String getJustificacionClinica() {
		return justificacionClinica;
	}

	public void setJustificacionClinica(String justificacionClinica) {
		this.justificacionClinica = justificacionClinica;
	}

	public UbicacionPacienteVO getUbicacionPacienteVO() {
		return ubicacionPacienteVO;
	}

	public void setUbicacionPacienteVO(UbicacionPacienteVO ubicacionPacienteVO) {
		this.ubicacionPacienteVO = ubicacionPacienteVO;
	}

	public String getManejoIntegralGuia() {
		return manejoIntegralGuia;
	}

	public void setManejoIntegralGuia(String manejoIntegralGuia) {
		this.manejoIntegralGuia = manejoIntegralGuia;
	}
}
