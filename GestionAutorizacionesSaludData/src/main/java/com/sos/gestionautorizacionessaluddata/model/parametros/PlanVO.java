package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class PlanVO
 * Clase VO de planes
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/12/2015
 *
 */

public class PlanVO implements Serializable{
	private static final long serialVersionUID = 7783490903523865228L;

	/*Consecutivo del plan*/
	private Integer consectivoPlan;
	
	/*Codigo del plan*/
	private String codigoPlan;
	
	/*Descripcion del plan*/
	private String descripcionPlan;
	
	public Integer getConsectivoPlan() {
		return consectivoPlan;
	}
	public void setConsectivoPlan(Integer consectivoPlan) {
		this.consectivoPlan = consectivoPlan;
	}
	public String getCodigoPlan() {
		return codigoPlan;
	}
	public void setCodigoPlan(String codigoPlan) {
		this.codigoPlan = codigoPlan;
	}
	public String getDescripcionPlan() {
		return descripcionPlan;
	}
	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}
}
