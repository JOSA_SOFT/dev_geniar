package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class TiposDiagnosticosVO
 * Clase VO con los tipos de diagnosticos
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 *
 */

public class TiposDiagnosticosVO implements Serializable{
	private static final long serialVersionUID = -5886310016709865405L;

	/*Consecutivo del tipo de diagnostico*/
	private Integer consecutivoTipoDiagnostico;
	
	/*Codigo del tipo de diagnostico*/
	private String codigoTipoDiagnostico;
	
	/*Descripcion del tipo de diagnostico*/
	private String descripcionTipoDiagnostico;
	
	public Integer getConsecutivoTipoDiagnostico() {
		return consecutivoTipoDiagnostico;
	}

	public void setConsecutivoTipoDiagnostico(Integer consecutivoTipoDiagnostico) {
		this.consecutivoTipoDiagnostico = consecutivoTipoDiagnostico;
	}

	public String getCodigoTipoDiagnostico() {
		return codigoTipoDiagnostico;
	}

	public void setCodigoTipoDiagnostico(String codigoTipoDiagnostico) {
		this.codigoTipoDiagnostico = codigoTipoDiagnostico;
	}

	public String getDescripcionTipoDiagnostico() {
		return descripcionTipoDiagnostico;
	}

	public void setDescripcionTipoDiagnostico(String descripcionTipoDiagnostico) {
		this.descripcionTipoDiagnostico = descripcionTipoDiagnostico;
	}
}
