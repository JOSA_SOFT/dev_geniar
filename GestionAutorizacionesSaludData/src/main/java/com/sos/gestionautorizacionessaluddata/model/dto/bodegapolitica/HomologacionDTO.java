package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;

public class HomologacionDTO implements Serializable {
	private static final long serialVersionUID = -4693782708597036595L;
	private String codigoServicio;
	private String descripcionServicio;
	private String cdgoCdfccn;
	private String dscrpcnCdfccn;
	private String cdgoIntrno;
	private String nmbreScrsl;

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getDescripcionServicio() {
		return descripcionServicio;
	}

	public void setDescripcionServicio(String descripcionServicio) {
		this.descripcionServicio = descripcionServicio;
	}

	public String getCdgoCdfccn() {
		return cdgoCdfccn;
	}

	public void setCdgoCdfccn(String cdgoCdfccn) {
		this.cdgoCdfccn = cdgoCdfccn;
	}

	public String getDscrpcnCdfccn() {
		return dscrpcnCdfccn;
	}

	public void setDscrpcnCdfccn(String dscrpcnCdfccn) {
		this.dscrpcnCdfccn = dscrpcnCdfccn;
	}

	public void setCdgoIntrno(String cdgoIntrno) {
		this.cdgoIntrno = cdgoIntrno;
	}

	public String getCdgoIntrno() {
		return cdgoIntrno;
	}

	public String getNmbreScrsl() {
		return nmbreScrsl;
	}

	public void setNmbreScrsl(String nmbreScrsl) {
		this.nmbreScrsl = nmbreScrsl;
	}

}
