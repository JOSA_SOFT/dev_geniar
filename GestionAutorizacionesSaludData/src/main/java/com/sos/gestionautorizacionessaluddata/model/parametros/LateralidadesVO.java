package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class LateralidadesVO
 * Clase VO para lateralidades
 * @author ing. Victor Hugo Gil Ramos
 * @version 18/12/2015
 *
 */
public class LateralidadesVO  implements Serializable{
	private static final long serialVersionUID = 3848645177745195339L;

	/*Consecutivo de la lateralidad*/
	private Integer consecutivoLaterialidad;
	
	/*codigo de la lateralidad*/
	private String codigoLaterialidad;
    
	/*descripcion de la lateralidad*/
	private String descripcionLateralidad;
      
      
    public Integer getConsecutivoLaterialidad() {
		return consecutivoLaterialidad;
	}
	public void setConsecutivoLaterialidad(Integer consecutivoLaterialidad) {
		this.consecutivoLaterialidad = consecutivoLaterialidad;
	}
	public String getCodigoLaterialidad() {
		return codigoLaterialidad;
	}
	public void setCodigoLaterialidad(String codigoLaterialidad) {
		this.codigoLaterialidad = codigoLaterialidad;
	}
	public String getDescripcionLateralidad() {
		return descripcionLateralidad;
	}
	public void setDescripcionLateralidad(String descripcionLateralidad) {
		this.descripcionLateralidad = descripcionLateralidad;
	}
}
