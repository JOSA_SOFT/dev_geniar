package com.sos.gestionautorizacionessaluddata.model.dto;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciasVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;


/**
 * Class DiagnosticoDTO
 * Clase VO que se utiliza para mostrar los diagnosticos en jtable
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/02/2016
 *
 */
public class DiagnosticoDTO implements Serializable{
	private static final long serialVersionUID = -4258612271183107193L;
	private DiagnosticosVO diagnosticosVO;
	private TiposDiagnosticosVO tiposDiagnosticosVO;
	private ContingenciasVO contingenciasVO;
	private ContingenciaRecobroVO recobroVO;
	
	private String principal;
	private String codigo;
	private String asignadoSOS;
	private String descripcion;
	
		
	public DiagnosticosVO getDiagnosticosVO() {
		return diagnosticosVO;
	}
	
	public void setDiagnosticosVO(DiagnosticosVO diagnosticosVO) {
		this.diagnosticosVO = diagnosticosVO;
	}
	
	public TiposDiagnosticosVO getTiposDiagnosticosVO() {
		return tiposDiagnosticosVO;
	}

	public void setTiposDiagnosticosVO(TiposDiagnosticosVO tiposDiagnosticosVO) {
		this.tiposDiagnosticosVO = tiposDiagnosticosVO;
	}	
	
	public ContingenciasVO getContingenciasVO() {
		return contingenciasVO;
	}

	public void setContingenciasVO(ContingenciasVO contingenciasVO) {
		this.contingenciasVO = contingenciasVO;
	}

	public ContingenciaRecobroVO getRecobroVO() {
		return recobroVO;
	}

	public void setRecobroVO(ContingenciaRecobroVO recobroVO) {
		this.recobroVO = recobroVO;
	}
	
	public String getPrincipal() {
		return principal.toUpperCase();
	}
	
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getAsignadoSOS() {
		return asignadoSOS.toUpperCase();
	}
	
	public void setAsignadoSOS(String asignadoSOS) {
		this.asignadoSOS = asignadoSOS;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
}
