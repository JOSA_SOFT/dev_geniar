package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;
import java.util.Date;


/**
 * Class ProgramacionEntregaVO
 * Clase VO para almacenar la programacion entrega
 * @author Julian Hernandez
 * @version 29/03/2016
 *
 */

public class ProgramacionEntregaVO implements Serializable{
	private static final long serialVersionUID = 8436805705458982397L;
	private Integer consCodProgramacionFecEvento;
	private Integer consProgramacionFecEvento;
	private Integer numero;
	private Date fechaEntrega;
	private Integer cantidad;
	private String proveedor;
	private String prestacion;
	private Integer consProgramacionPrestacion;
	private String estadoEntrega;
	private String evento;
	private String numAutorizacion;
	private String usrUltimaMod;
	private Integer consEstadoPrestacion;
	private Date fechaVencimientoAutorizacion;
	private Integer consEstadoOPS;
	private Integer consPrestacion;
	private Integer consSolicitud;
	private String numRadicadoSolicitud;
	private String codigoPrestacion;
	private boolean verGenerar;
	private Integer consEstadoProgramacion;
	private Date fechaFinalizacion;
	private Integer consecutivoServicioSolicitado;
	private Integer consecutivoCodServicioSolicitado;
	private String codServicioSolicitado;
	private boolean valorConceptoCero;
	private String acta;
	private Integer numeroNotificacion;
	private String	codigoOficina;
	
	/**
	 * identifica si el dato de la solicitud es par para diferenciarse de la
	 * proxima programacion
	 */
	private boolean par;
	
	/**
	 * @return the consCodProgramacionFecEvento
	 */
	public Integer getConsCodProgramacionFecEvento() {
		return consCodProgramacionFecEvento;
	}
	/**
	 * @param consCodProgramacionFecEvento the consCodProgramacionFecEvento to set
	 */
	public void setConsCodProgramacionFecEvento(Integer consCodProgramacionFecEvento) {
		this.consCodProgramacionFecEvento = consCodProgramacionFecEvento;
	}
	/**
	 * @return the numero
	 */
	public Integer getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	/**
	 * @return the fechaEntrega
	 */
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	/**
	 * @return the cantidad
	 */
	public Integer getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the proveedor
	 */
	public String getProveedor() {
		return proveedor;
	}
	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	/**
	 * @return the prestacion
	 */
	public String getPrestacion() {
		return prestacion;
	}
	/**
	 * @param prestacion the prestacion to set
	 */
	public void setPrestacion(String prestacion) {
		this.prestacion = prestacion;
	}
	/**
	 * @return the consProgramacionPrestacion
	 */
	public Integer getConsProgramacionPrestacion() {
		return consProgramacionPrestacion;
	}
	/**
	 * @param consProgramacionPrestacion the consProgramacionPrestacion to set
	 */
	public void setConsProgramacionPrestacion(Integer consProgramacionPrestacion) {
		this.consProgramacionPrestacion = consProgramacionPrestacion;
	}
	/**
	 * @return the estadoEntrega
	 */
	public String getEstadoEntrega() {
		return estadoEntrega;
	}
	/**
	 * @param estadoEntrega the estadoEntrega to set
	 */
	public void setEstadoEntrega(String estadoEntrega) {
		this.estadoEntrega = estadoEntrega;
	}
	/**
	 * @return the evento
	 */
	public String getEvento() {
		return evento;
	}
	/**
	 * @param evento the evento to set
	 */
	public void setEvento(String evento) {
		this.evento = evento;
	}
	/**
	 * @return the numAutorizacion
	 */
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	/**
	 * @param numAutorizacion the numAutorizacion to set
	 */
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	/**
	 * @return the usrUltimaMod
	 */
	public String getUsrUltimaMod() {
		return usrUltimaMod;
	}
	/**
	 * @param usrUltimaMod the usrUltimaMod to set
	 */
	public void setUsrUltimaMod(String usrUltimaMod) {
		this.usrUltimaMod = usrUltimaMod;
	}
	/**
	 * @return the par
	 */
	public boolean isPar() {
		return par;
	}
	/**
	 * @param par the par to set
	 */
	public void setPar(boolean par) {
		this.par = par;
	}
	/**
	 * @return the consEstadoPrestacion
	 */
	public Integer getConsEstadoPrestacion() {
		return consEstadoPrestacion;
	}
	/**
	 * @param consEstadoPrestacion the consEstadoPrestacion to set
	 */
	public void setConsEstadoPrestacion(Integer consEstadoPrestacion) {
		this.consEstadoPrestacion = consEstadoPrestacion;
	}
	/**
	 * @return the fechaVencimientoAutorizacion
	 */
	public Date getFechaVencimientoAutorizacion() {
		return fechaVencimientoAutorizacion;
	}
	/**
	 * @param fechaVencimientoAutorizacion the fechaVencimientoAutorizacion to set
	 */
	public void setFechaVencimientoAutorizacion(Date fechaVencimientoAutorizacion) {
		this.fechaVencimientoAutorizacion = fechaVencimientoAutorizacion;
	}
	/**
	 * @return the consPrestacion
	 */
	public Integer getConsPrestacion() {
		return consPrestacion;
	}
	/**
	 * @param consPrestacion the consPrestacion to set
	 */
	public void setConsPrestacion(Integer consPrestacion) {
		this.consPrestacion = consPrestacion;
	}
	/**
	 * @return the consSolicitud
	 */
	public Integer getConsSolicitud() {
		return consSolicitud;
	}
	/**
	 * @param consSolicitud the consSolicitud to set
	 */
	public void setConsSolicitud(Integer consSolicitud) {
		this.consSolicitud = consSolicitud;
	}
	/**
	 * @return the numRadicadoSolicitud
	 */
	public String getNumRadicadoSolicitud() {
		return numRadicadoSolicitud;
	}
	/**
	 * @param numRadicadoSolicitud the numRadicadoSolicitud to set
	 */
	public void setNumRadicadoSolicitud(String numRadicadoSolicitud) {
		this.numRadicadoSolicitud = numRadicadoSolicitud;
	}
	/**
	 * @return the codigoPrestacion
	 */
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}
	/**
	 * @param codigoPrestacion the codigoPrestacion to set
	 */
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}
	/**
	 * @return the verGenerar
	 */
	public boolean isVerGenerar() {
		return verGenerar;
	}
	/**
	 * @param verGenerar the verGenerar to set
	 */
	public void setVerGenerar(boolean verGenerar) {
		this.verGenerar = verGenerar;
	}
	/**
	 * @return the consEstadoProgramacion
	 */
	public Integer getConsEstadoProgramacion() {
		return consEstadoProgramacion;
	}
	/**
	 * @param consEstadoProgramacion the consEstadoProgramacion to set
	 */
	public void setConsEstadoProgramacion(Integer consEstadoProgramacion) {
		this.consEstadoProgramacion = consEstadoProgramacion;
	}
	/**
	 * @return the fechaFinalizacion
	 */
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	/**
	 * @param fechaFinalizacion the fechaFinalizacion to set
	 */
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	/**
	 * @return the consEstadoOPS
	 */
	public Integer getConsEstadoOPS() {
		return consEstadoOPS;
	}
	/**
	 * @param consEstadoOPS the consEstadoOPS to set
	 */
	public void setConsEstadoOPS(Integer consEstadoOPS) {
		this.consEstadoOPS = consEstadoOPS;
	}
	/**
	 * @return the consecutivoServicioSolicitado
	 */
	public Integer getConsecutivoServicioSolicitado() {
		return consecutivoServicioSolicitado;
	}
	/**
	 * @param consecutivoServicioSolicitado the consecutivoServicioSolicitado to set
	 */
	public void setConsecutivoServicioSolicitado(
			Integer consecutivoServicioSolicitado) {
		this.consecutivoServicioSolicitado = consecutivoServicioSolicitado;
	}
	/**
	 * @return the consecutivoCodServicioSolicitado
	 */
	public Integer getConsecutivoCodServicioSolicitado() {
		return consecutivoCodServicioSolicitado;
	}
	/**
	 * @param consecutivoCodServicioSolicitado the consecutivoCodServicioSolicitado to set
	 */
	public void setConsecutivoCodServicioSolicitado(
			Integer consecutivoCodServicioSolicitado) {
		this.consecutivoCodServicioSolicitado = consecutivoCodServicioSolicitado;
	}
	/**
	 * @return the codServicioSolicitado
	 */
	public String getCodServicioSolicitado() {
		return codServicioSolicitado;
	}
	/**
	 * @param codServicioSolicitado the codServicioSolicitado to set
	 */
	public void setCodServicioSolicitado(String codServicioSolicitado) {
		this.codServicioSolicitado = codServicioSolicitado;
	}
	/**
	 * @return the consProgramacionFecEvento
	 */
	public Integer getConsProgramacionFecEvento() {
		return consProgramacionFecEvento;
	}
	/**
	 * @param consProgramacionFecEvento the consProgramacionFecEvento to set
	 */
	public void setConsProgramacionFecEvento(Integer consProgramacionFecEvento) {
		this.consProgramacionFecEvento = consProgramacionFecEvento;
	}
	
	/**
	 * @return the valorConceptoCero
	 */
	public boolean isValorConceptoCero() {
		return valorConceptoCero;
	}
	
	/**
	 * @param valorConceptoCero the valorConceptoCero to set
	 */
	public void setValorConceptoCero(boolean valorConceptoCero) {
		this.valorConceptoCero = valorConceptoCero;
	}
	
	public String getActa() {
		return acta;
	}
	
	public void setActa(String acta) {
		this.acta = acta;
	}
	
	public Integer getNumeroNotificacion() {
		return numeroNotificacion;
	}
	
	public void setNumeroNotificacion(Integer numeroNotificacion) {
		this.numeroNotificacion = numeroNotificacion;
	}
	
	public String getCodigoOficina() {
		return codigoOficina;
	}
	
	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}
	
	
	
}
	
