package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;
import java.util.List;

/**
 * Class ModuloVO
 * 
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class ModuloVO implements Serializable {
	private static final long serialVersionUID = 8985422693808986891L;

	/** Nombre del modulo, aparecera en el titulo del menu */
	private String nombre;

	/** Listado de servicios del m�dulo, son las opciones que tendr� el men� */
	private List<ServicioVO> servicios;

	private int consecutivoCodigoPerfil;

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setServicios(List<ServicioVO> servicios) {
		this.servicios = servicios;
	}

	public List<ServicioVO> getServicios() {
		return servicios;
	}

	public int getConsecutivoCodigoPerfil() {
		return consecutivoCodigoPerfil;
	}

	public void setConsecutivoCodigoPerfil(int consecutivoCodigoPerfil) {
		this.consecutivoCodigoPerfil = consecutivoCodigoPerfil;
	}

}
