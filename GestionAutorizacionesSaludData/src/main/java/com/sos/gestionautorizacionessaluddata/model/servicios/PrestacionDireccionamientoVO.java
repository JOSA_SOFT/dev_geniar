package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;


/**
 * Class PrestacionDireccionamientoVO 
 * Clase que permite para almacenar la informacion de las prestaciones asociadas al direccionamiento
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/03/2016
 *
 */
public class PrestacionDireccionamientoVO implements Serializable{
	private static final long serialVersionUID = -8686177352790861901L;

	/*Identifica el listado de los prestadores asociados al direccionamiento */
	private List<PrestadorVO> lPrestadorVO;
	
	/*Identifica el listado de las prestaciones asociados al direccionamiento */
	private PrestacionDTO prestacionDTO;
	
	public List<PrestadorVO> getlPrestadorVO() {
		return lPrestadorVO;
	}

	public void setlPrestadorVO(List<PrestadorVO> lPrestadorVO) {
		this.lPrestadorVO = lPrestadorVO;
	}

	public PrestacionDTO getPrestacionDTO() {
		return prestacionDTO;
	}

	public void setPrestacionDTO(PrestacionDTO prestacionDTO) {
		this.prestacionDTO = prestacionDTO;
	}
}
