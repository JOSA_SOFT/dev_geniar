package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class ActividadEconomicaVO
 * Clase VO de la actividad economica del empleador
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/01/2016
 *
 */
public class ActividadEconomicaVO implements Serializable {
	private static final long serialVersionUID = 5942030613545006323L;

	/*Identifica el consecutivo de la actividad economica del empleador*/
	private Integer consecutivoActividadEconomica;
	
	/*Identifica el codigo de la actividad economica del empleador*/
	private String codigoEntidad;
	
	/*Identifica la descripcion de la actividad economica del empleador*/
	private String descripcionEntidad;
	
	public Integer getConsecutivoActividadEconomica() {
		return consecutivoActividadEconomica;
	}

	public void setConsecutivoActividadEconomica(Integer consecutivoActividadEconomica) {
		this.consecutivoActividadEconomica = consecutivoActividadEconomica;
	}

	public String getCodigoEntidad() {
		return codigoEntidad;
	}

	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}

	public String getDescripcionEntidad() {
		return descripcionEntidad;
	}

	public void setDescripcionEntidad(String descripcionEntidad) {
		this.descripcionEntidad = descripcionEntidad;
	}
}
