package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;

/**
 * Class SolicitudVO
 * Clase VO de EstadoVO 
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/02/2016
 *
 */
public class EstadoVO implements Serializable {
	private static final long serialVersionUID = 3277938242868670590L;

	/** informacion del consecutivo del estado de la solicitud */
	private Integer consecutivoEstadoSolicitud;	
	
	/** informacion del codigo del estado de la solicitud */
	private String codigoEstadoSolicitud;	
	
	/** informacion de la descripcion del estado de la solicitud */
	private String descripcionEstadoSolicitud;	

	
	public String getCodigoEstadoSolicitud() {
		return codigoEstadoSolicitud;
	}

	public void setCodigoEstadoSolicitud(String codigoEstadoSolicitud) {
		this.codigoEstadoSolicitud = codigoEstadoSolicitud;
	}
	
	public Integer getConsecutivoEstadoSolicitud() {
		return consecutivoEstadoSolicitud;
	}

	public void setConsecutivoEstadoSolicitud(Integer consecutivoEstadoSolicitud) {
		this.consecutivoEstadoSolicitud = consecutivoEstadoSolicitud;
	}

	public String getDescripcionEstadoSolicitud() {
		return descripcionEstadoSolicitud;
	}

	public void setDescripcionEstadoSolicitud(String descripcionEstadoSolicitud) {
		this.descripcionEstadoSolicitud = descripcionEstadoSolicitud;
	}
}
