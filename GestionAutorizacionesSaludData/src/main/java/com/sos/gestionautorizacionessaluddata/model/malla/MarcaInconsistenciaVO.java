package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;

/**
 * Class PrestacionDTO
 * Clase DTO con la informacion de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class MarcaInconsistenciaVO implements Serializable{
	private static final long serialVersionUID = -7683700131085857519L;

	/** informacion del consecutivo de la marca de la inconsistencia */
	private Integer consecutivoMarcaInconsistencia;	
	
	/** informacion del codigo de la marca de la inconsistencia*/
	private String codigoMarcaInconsistencia;	
	
	/** informacion de la descripcion de la marca de la inconsistencia */
	private String descripcionMarcaInconsistencia;	
	
	public Integer getConsecutivoMarcaInconsistencia() {
		return consecutivoMarcaInconsistencia;
	}

	public void setConsecutivoMarcaInconsistencia(
			Integer consecutivoMarcaInconsistencia) {
		this.consecutivoMarcaInconsistencia = consecutivoMarcaInconsistencia;
	}

	public String getCodigoMarcaInconsistencia() {
		return codigoMarcaInconsistencia;
	}

	public void setCodigoMarcaInconsistencia(String codigoMarcaInconsistencia) {
		this.codigoMarcaInconsistencia = codigoMarcaInconsistencia;
	}

	public String getDescripcionMarcaInconsistencia() {
		return descripcionMarcaInconsistencia;
	}

	public void setDescripcionMarcaInconsistencia(
			String descripcionMarcaInconsistencia) {
		this.descripcionMarcaInconsistencia = descripcionMarcaInconsistencia;
	}
}
