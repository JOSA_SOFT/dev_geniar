package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class ServiciosCapitadosVO
 * Clase VO para los convenios de capitacion del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/01/2016
 *
 */
public class ServiciosCapitadosVO implements Serializable {
	private static final long serialVersionUID = 8744856909780375723L;

	/*Identifica el codigo del item de capitacion.*/
	private String codigoItemCapitacion;
	
	/*Identifica la descripcion del item de capitacion.*/
	private String descripcionItemCapitacion;
	
	/*Identifica la accion del item de capitacion.*/
	private String accionItemCapitacion;
	
	public String getCodigoItemCapitacion() {
		return codigoItemCapitacion;
	}

	public void setCodigoItemCapitacion(String codigoItemCapitacion) {
		this.codigoItemCapitacion = codigoItemCapitacion;
	}

	public String getDescripcionItemCapitacion() {
		return descripcionItemCapitacion;
	}

	public void setDescripcionItemCapitacion(String descripcionItemCapitacion) {
		this.descripcionItemCapitacion = descripcionItemCapitacion;
	}

	public String getAccionItemCapitacion() {
		return accionItemCapitacion;
	}

	public void setAccionItemCapitacion(String accionItemCapitacion) {
		this.accionItemCapitacion = accionItemCapitacion;
	}
}
