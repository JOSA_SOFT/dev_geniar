package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;

import com.sos.util.export.annotations.SOSExportField;



/**
 * Class UsuarioVO
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class UsuarioVO implements Serializable {
	private static final long serialVersionUID = 8684403908865071128L;
	private String login;
	private String descripcion;
	private Integer consecutivo;
	private String nombre;
	
	
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	@SOSExportField(posicion=1,titulo="Login")
	public String getLogin() {
		return login;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@SOSExportField(posicion=2,titulo="Descripcion")
	public String getDescripcion() {
		return descripcion;
	}
	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}
	
	@SOSExportField(posicion=3,titulo="Consecutivo")
	public Integer getConsecutivo() {
		return consecutivo;
	}
	
}
