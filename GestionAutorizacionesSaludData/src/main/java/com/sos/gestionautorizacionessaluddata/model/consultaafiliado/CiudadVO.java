package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class CiudadVO
 * Clase VO de las ciudades
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/12/2015
 *
 */

public class CiudadVO implements Serializable {
	private static final long serialVersionUID = -6393330801841554961L;

	/*Identifica el consecutivo codigo de la ciudad*/
	private Integer consecutivoCodigoCiudad;
	
	/*Identifica el codigo de la ciudad*/
	private String codigoCiudad;
	
	/*Identifica la descripcion de la ciudad*/
	private String descripcionCiudad;
	
	/*Identifica el consecutivo de la sede*/
	private Integer consecutivoCodigoSede;
	
	/*Identifica el codigo de la sede*/
	private String codigoSede;
	
	/*Identifica la descripcion de la sede*/
	private String descripcionSede;
	
	
	public Integer getConsecutivoCodigoCiudad() {
		return consecutivoCodigoCiudad;
	}

	public void setConsecutivoCodigoCiudad(Integer consecutivoCodigoCiudad) {
		this.consecutivoCodigoCiudad = consecutivoCodigoCiudad;
	}

	public String getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public String getDescripcionCiudad() {
		return descripcionCiudad;
	}

	public void setDescripcionCiudad(String descripcionCiudad) {
		this.descripcionCiudad = descripcionCiudad;
	}
	
	public Integer getConsecutivoCodigoSede() {
		return consecutivoCodigoSede;
	}

	public void setConsecutivoCodigoSede(Integer consecutivoCodigoSede) {
		this.consecutivoCodigoSede = consecutivoCodigoSede;
	}

	public String getCodigoSede() {
		return codigoSede;
	}

	public void setCodigoSede(String codigoSede) {
		this.codigoSede = codigoSede;
	}

	public String getDescripcionSede() {
		return descripcionSede;
	}

	public void setDescripcionSede(String descripcionSede) {
		this.descripcionSede = descripcionSede;
	}
}
