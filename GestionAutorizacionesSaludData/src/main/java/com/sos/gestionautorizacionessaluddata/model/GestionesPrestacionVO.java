package com.sos.gestionautorizacionessaluddata.model;

import java.io.Serializable;

/**
 * Clase VO para los riesgos del afiliado
 * @author Julian Hernandez Gomez
 * @version 10/03/2016
 *
 */
public class GestionesPrestacionVO implements Serializable{
	private static final long serialVersionUID = 2005477586970530208L;

	/** **/
	private String descUsuario;
	
	/** **/
	private String descTipoAuditoria;
	
	/** **/
	private String descEstado;
	
	/** **/
	private String descCausales;
	
	/** **/
	private String observacion;

	/**
	 * @return the descUsuario
	 */
	public String getDescUsuario() {
		return descUsuario;
	}

	/**
	 * @param descUsuario the descUsuario to set
	 */
	public void setDescUsuario(String descUsuario) {
		this.descUsuario = descUsuario;
	}

	/**
	 * @return the descTipoAuditoria
	 */
	public String getDescTipoAuditoria() {
		return descTipoAuditoria;
	}

	/**
	 * @param descTipoAuditoria the descTipoAuditoria to set
	 */
	public void setDescTipoAuditoria(String descTipoAuditoria) {
		this.descTipoAuditoria = descTipoAuditoria;
	}

	/**
	 * @return the descEstado
	 */
	public String getDescEstado() {
		return descEstado;
	}

	/**
	 * @param descEstado the descEstado to set
	 */
	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	/**
	 * @return the descCausales
	 */
	public String getDescCausales() {
		return descCausales;
	}

	/**
	 * @param descCausales the descCausales to set
	 */
	public void setDescCausales(String descCausales) {
		this.descCausales = descCausales;
	}

	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return observacion;
	}

	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	
}
