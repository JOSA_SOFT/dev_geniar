package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ParametrosGeneralesVO
 * Clase VO de los parametros generales
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/01/2016
 *
 */
public class ParametrosGeneralesVO implements Serializable{
	private static final long serialVersionUID = 1140261594170421065L;

	/*Identifica el consecutivo codigo del parametro general */
	private int consecutivoCodigoParametroGeneral;
	
	/*Identifica el valor del parametro general */
	private Long valorParametroGeneral; 
	
	public int getConsecutivoCodigoParametroGeneral() {
		return consecutivoCodigoParametroGeneral;
	}

	public void setConsecutivoCodigoParametroGeneral(
			int consecutivoCodigoParametroGeneral) {
		this.consecutivoCodigoParametroGeneral = consecutivoCodigoParametroGeneral;
	}

	public Long getValorParametroGeneral() {
		return valorParametroGeneral;
	}

	public void setValorParametroGeneral(Long valorParametroGeneral) {
		this.valorParametroGeneral = valorParametroGeneral;
	}
}
