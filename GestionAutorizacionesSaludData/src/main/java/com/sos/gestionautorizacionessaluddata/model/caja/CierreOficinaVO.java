package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Encapsula la informacion del cierre de oficina
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class CierreOficinaVO extends InformacionRegistroVO implements Serializable{
	private static final long serialVersionUID = -4343482528229652857L;
	private Integer cnsctvoCrreOfcna;
	private Double ttlEfctvoCja;
	private Double ttlRcbdoOfcna;
	private Double bseOfcna;
	private Double fltnteOfcna;
	private Double sbrnteOfcna;
	private Integer cnsctvoCdgoCrdndrAsi;

	public Integer getCnsctvoCrreOfcna() {
		return cnsctvoCrreOfcna;
	}

	public void setCnsctvoCrreOfcna(Integer cnsctvoCrreOfcna) {
		this.cnsctvoCrreOfcna = cnsctvoCrreOfcna;
	}

	public Double getTtlEfctvoCja() {
		return ttlEfctvoCja;
	}

	public void setTtlEfctvoCja(Double ttlEfctvoCja) {
		this.ttlEfctvoCja = ttlEfctvoCja;
	}

	public Double getTtlRcbdoOfcna() {
		return ttlRcbdoOfcna;
	}

	public void setTtlRcbdoOfcna(Double ttlRcbdoOfcna) {
		this.ttlRcbdoOfcna = ttlRcbdoOfcna;
	}

	public Double getBseOfcna() {
		return bseOfcna;
	}

	public void setBseOfcna(Double bseOfcna) {
		this.bseOfcna = bseOfcna;
	}

	public Double getFltnteOfcna() {
		return fltnteOfcna;
	}

	public void setFltnteOfcna(Double fltnteOfcna) {
		this.fltnteOfcna = fltnteOfcna;
	}

	public Double getSbrnteOfcna() {
		return sbrnteOfcna;
	}

	public void setSbrnteOfcna(Double sbrnteOfcna) {
		this.sbrnteOfcna = sbrnteOfcna;
	}

	public Integer getCnsctvoCdgoCrdndrAsi() {
		return cnsctvoCdgoCrdndrAsi;
	}

	public void setCnsctvoCdgoCrdndrAsi(Integer cnsctvoCdgoCrdndrAsi) {
		this.cnsctvoCdgoCrdndrAsi = cnsctvoCdgoCrdndrAsi;
	}
}