package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.Date;


/**
 * Class GrupoFamiliarVO
 * Clase VO del grupo familiar del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/12/2015
 *
 */
public class GrupoFamiliarVO implements Serializable {
	private static final long serialVersionUID = -4346229610025597694L;

	/*Identifica el grupo familiar consultado.*/
	private AfiliadoVO afiliadoGrupoFamiliar;
	
	/*Identifica la fecha de validacion de la informacion*/
	private Date fechaValidacion = new Date(System.currentTimeMillis());
	
	/*Identifica el origen de la informacion*/
	private String origen;
		
	public AfiliadoVO getAfiliadoGrupoFamiliar() {
		return afiliadoGrupoFamiliar;
	}
	public void setAfiliadoGrupoFamiliar(AfiliadoVO afiliadoGrupoFamiliar) {
		this.afiliadoGrupoFamiliar = afiliadoGrupoFamiliar;
	}
	public Date getFechaValidacion() {
		return fechaValidacion;
	}
	public void setFechaValidacion(Date fechaValidacion) {
		this.fechaValidacion = fechaValidacion;
	}
	
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
}
