package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;

/**
 * Class HistoricoFormatosVO
 * Clase VO para guardar el historico de descargas
 * @author ing. Victor Hugo Gil Ramos
 * @version 11/03/2016
 *
 */
public class GuardarHistoricoVO implements Serializable{
	private static final long serialVersionUID = 4990994098107716661L;

	/** Identifica el xml con la informacion a registrar*/
	private StringBuilder xmlServicios;
	
	/** Identifica el consecutivo de la solicitud*/
	private Integer consecutivoSolicitud;
	
	/** Identifica la fecha de descarga*/
	private Date fechaDescarga;
	
	/** Identifica el usuario de descarga*/
	private String usuarioDescarga;
	
	/** Identifica el medio de contacto*/
	private String codigoMedioContacto; 
	
	/** Identifica el numero unico de autorizacion*/
	private Integer numeroUnicoAutorizacion;
	
	
	public StringBuilder getXmlServicios() {
		return xmlServicios;
	}
	
	public void setXmlServicios(StringBuilder xmlServicios) {
		this.xmlServicios = xmlServicios;
	}
	
	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}
	
	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}
	
	public Date getFechaDescarga() {
		return fechaDescarga;
	}

	public void setFechaDescarga(Date fechaDescarga) {
		this.fechaDescarga = fechaDescarga;
	}
	
	public String getUsuarioDescarga() {
		return usuarioDescarga;
	}
	
	public void setUsuarioDescarga(String usuarioDescarga) {
		this.usuarioDescarga = usuarioDescarga;
	}
	
	public String getCodigoMedioContacto() {
		return codigoMedioContacto;
	}
	
	public void setCodigoMedioContacto(String codigoMedioContacto) {
		this.codigoMedioContacto = codigoMedioContacto;
	}
	
	public Integer getNumeroUnicoAutorizacion() {
		return numeroUnicoAutorizacion;
	}

	public void setNumeroUnicoAutorizacion(Integer numeroUnicoAutorizacion) {
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
	}
}
