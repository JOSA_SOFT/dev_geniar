package com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;



/**
 * Class SolicitudBpmVO
 * Clase VO de la Solicitud
 * @author ing. Rafael Cano
 * @version 23/12/2015
 *
 */

public class SolicitudBpmVO implements Serializable{
	private static final long serialVersionUID = 5374508078821481120L;

	/* Consecutivo de la solicitud  */
	private Integer numeroSolicitud;
	
	/* Fecha de Creación  */
	private Date fechaCreacion;
	
	/*Identifica la fecha de solicitud profesional*/
	private Date fechaSolicitud;
	
	/*Identifica el numero de radicado*/
	private String numeroRadicado;
	
	/*Identifica la Ubicacion del afiliado*/
	private String ubicacionPaciente;
	
	/*Identifica la prioridad de la solicitud*/
	private String prioridad;
	
	/*Identifica el auditor de la solicitud */
	private String auditorSolicita;
	
	/*Identifica el Origen de la solicitud */
	private String origenSolicita;
	
	/*Identifica el Origen de la atención */
	private Integer codigoOrigenAtencion;
	
	/*Identifica el Origen de la atención */
	private String origenAtencion;
	
	/*Identifica la clase de atención */
	private String claseAtencion;
	
	/*Identifica la justificacion clinica */
	private String justificacionClinica;

	/*Identifica la observacion de la solicitud*/
	private String obsSolicitud;
	
	private String recobro;
	private String superaTopeSoat;
	private int consRecobro;
	
	/*Identifica el objeto con la informacion de los soportes*/
	private List<SoporteVO> lSoporteVO;
	
	/* Consecutivo de la solicitud del Proovedor  */
	private Integer numeroSolicitudProovedor;
	
	
	/** **/
	private MedicoVO medicoSolicitante;
	
	/*Identifica la informacion del prestador*/
	private PrestadorVO prestadorSolicitante;
	
	/** **/
	private List<DiagnosticoDTO> lstDiagnosticos;

	private String observacionesGestionAuditor;
	/**
	 * @return the numeroSolicitud
	 */
	public Integer getNumeroSolicitud() {
		return numeroSolicitud;
	}

	/**
	 * @param numeroSolicitud the numeroSolicitud to set
	 */
	public void setNumeroSolicitud(Integer numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the fechaSolicitud
	 */
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	/**
	 * @param fechaSolicitud the fechaSolicitud to set
	 */
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/**
	 * @return the numeroRadicado
	 */
	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	/**
	 * @param numeroRadicado the numeroRadicado to set
	 */
	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}

	/**
	 * @return the ubicacionPaciente
	 */
	public String getUbicacionPaciente() {
		return ubicacionPaciente;
	}

	/**
	 * @param ubicacionPaciente the ubicacionPaciente to set
	 */
	public void setUbicacionPaciente(String ubicacionPaciente) {
		this.ubicacionPaciente = ubicacionPaciente;
	}

	/**
	 * @return the prioridad
	 */
	public String getPrioridad() {
		return prioridad;
	}

	/**
	 * @param prioridad the prioridad to set
	 */
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	/**
	 * @return the auditorSolicita
	 */
	public String getAuditorSolicita() {
		return auditorSolicita;
	}

	/**
	 * @param auditorSolicita the auditorSolicita to set
	 */
	public void setAuditorSolicita(String auditorSolicita) {
		this.auditorSolicita = auditorSolicita;
	}

	/**
	 * @return the origenSolicita
	 */
	public String getOrigenSolicita() {
		return origenSolicita;
	}

	/**
	 * @param origenSolicita the origenSolicita to set
	 */
	public void setOrigenSolicita(String origenSolicita) {
		this.origenSolicita = origenSolicita;
	}

	/**
	 * @return the origenAtencion
	 */
	public String getOrigenAtencion() {
		return origenAtencion;
	}

	/**
	 * @param origenAtencion the origenAtencion to set
	 */
	public void setOrigenAtencion(String origenAtencion) {
		this.origenAtencion = origenAtencion;
	}

	/**
	 * @return the claseAtencion
	 */
	public String getClaseAtencion() {
		return claseAtencion;
	}

	/**
	 * @param claseAtencion the claseAtencion to set
	 */
	public void setClaseAtencion(String claseAtencion) {
		this.claseAtencion = claseAtencion;
	}

	/**
	 * @return the justificacionClinica
	 */
	public String getJustificacionClinica() {
		return justificacionClinica;
	}

	/**
	 * @param justificacionClinica the justificacionClinica to set
	 */
	public void setJustificacionClinica(String justificacionClinica) {
		this.justificacionClinica = justificacionClinica;
	}

	/**
	 * @return the obsSolcitud
	 */
	public String getObsSolcitud() {
		return obsSolicitud;
	}

	/**
	 * @param obsSolcitud the obsSolcitud to set
	 */
	public void setObsSolcitud(String obsSolcitud) {
		this.obsSolicitud = obsSolcitud;
	}

	/**
	 * @return the medicoSolicitante
	 */
	public MedicoVO getMedicoSolicitante() {
		return medicoSolicitante;
	}

	/**
	 * @param medicoSolicitante the medicoSolicitante to set
	 */
	public void setMedicoSolicitante(MedicoVO medicoSolicitante) {
		this.medicoSolicitante = medicoSolicitante;
	}

	/**
	 * @return the prestadorSolicitante
	 */
	public PrestadorVO getPrestadorSolicitante() {
		return prestadorSolicitante;
	}

	/**
	 * @param prestadorSolicitante the prestadorSolicitante to set
	 */
	public void setPrestadorSolicitante(PrestadorVO prestadorSolicitante) {
		this.prestadorSolicitante = prestadorSolicitante;
	}

	/**
	 * @return the lstDiagnosticos
	 */
	public List<DiagnosticoDTO> getLstDiagnosticos() {
		return lstDiagnosticos;
	}

	/**
	 * @param lstDiagnosticos the lstDiagnosticos to set
	 */
	public void setLstDiagnosticos(List<DiagnosticoDTO> lstDiagnosticos) {
		this.lstDiagnosticos = lstDiagnosticos;
	}

	/**
	 * @return the obsSolicitud
	 */
	public String getObsSolicitud() {
		return obsSolicitud;
	}

	/**
	 * @param obsSolicitud the obsSolicitud to set
	 */
	public void setObsSolicitud(String obsSolicitud) {
		this.obsSolicitud = obsSolicitud;
	}

	/**
	 * @return the recobro
	 */
	public String getRecobro() {
		return recobro;
	}

	/**
	 * @param recobro the recobro to set
	 */
	public void setRecobro(String recobro) {
		this.recobro = recobro;
	}

	/**
	 * @return the superaTopeSoat
	 */
	public String getSuperaTopeSoat() {
		return superaTopeSoat;
	}

	/**
	 * @param superaTopeSoat the superaTopeSoat to set
	 */
	public void setSuperaTopeSoat(String superaTopeSoat) {
		this.superaTopeSoat = superaTopeSoat;
	}

	/**
	 * @return the codigoOrigenAtencion
	 */
	public Integer getCodigoOrigenAtencion() {
		return codigoOrigenAtencion;
	}

	/**
	 * @param codigoOrigenAtencion the codigoOrigenAtencion to set
	 */
	public void setCodigoOrigenAtencion(Integer codigoOrigenAtencion) {
		this.codigoOrigenAtencion = codigoOrigenAtencion;
	}

	/**
	 * @return the  solicitudProveedor 
	 */
	public Integer getNumeroSolicitudProovedor() {
		return numeroSolicitudProovedor;
	}

	/**
	 * @param numeroSolicitudProovedor the numeroSolicitudProovedor to set
	 */
	public void setNumeroSolicitudProovedor(Integer numeroSolicitudProovedor) {
		this.numeroSolicitudProovedor = numeroSolicitudProovedor;
	}

	public int getConsRecobro() {
		return consRecobro;
	}

	public void setConsRecobro(int consRecobro) {
		this.consRecobro = consRecobro;
	}

	public List<SoporteVO> getlSoporteVO() {
		return lSoporteVO;
	}

	public void setlSoporteVO(List<SoporteVO> lSoporteVO) {
		this.lSoporteVO = lSoporteVO;
	}

	public String getObservacionesGestionAuditor() {
		return observacionesGestionAuditor;
	}

	public void setObservacionesGestionAuditor(String observacionesGestionAuditor) {
		this.observacionesGestionAuditor = observacionesGestionAuditor;
	}
	
	
	
}