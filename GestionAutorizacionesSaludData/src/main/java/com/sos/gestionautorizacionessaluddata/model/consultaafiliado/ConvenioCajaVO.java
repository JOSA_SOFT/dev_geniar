package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class ConvenioCajaVO
 * Clase VO para la informacion de cuota moderadora y copago del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class ConvenioCajaVO implements Serializable {
	private static final long serialVersionUID = -7532138475748070441L;

	/*Identifica el texto del copago.*/
	private String textoCopago;
	
	/*Identifica el texto de la cuota moderadora.*/
	private String textoCuotaModeradora;
	
	/*Identifica el texto concatenado del copago cuota moderadora*/
	private String textoCopagoTextoCuotaModeradora;
	
	public String getTextoCopago() {
		return textoCopago;
	}

	public void setTextoCopago(String textoCopago) {
		this.textoCopago = textoCopago;
	}

	public String getTextoCuotaModeradora() {
		return textoCuotaModeradora;
	}

	public void setTextoCuotaModeradora(String textoCuotaModeradora) {
		this.textoCuotaModeradora = textoCuotaModeradora;
	}

	public String getTextoCopagoTextoCuotaModeradora() {
		return textoCopagoTextoCuotaModeradora;
	}

	public void setTextoCopagoTextoCuotaModeradora(
			String textoCopago_textoCuotaModeradora) {
		this.textoCopagoTextoCuotaModeradora = textoCopago_textoCuotaModeradora;
	}
}
