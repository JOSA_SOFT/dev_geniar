package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class ClaseAtencionVO
 * Clase VO de la clase de Atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class ClaseAtencionVO implements Serializable{	
	private static final long serialVersionUID = 7202215998102214259L;

	/*Consecutivo de la clase de atencion */
	private Integer consecutivoClaseAtencion;
	
	/*codigo de la clase de atencion */
	private String codigoClaseAtencion;
	
	/*descripcion de la clase de atencion */
	private String descripcionClaseAtencion;
	
	/*Consecutivo de la forma de atencion */
	private Integer consecutivoFormaAtencion;
	
	/*codigo de la forma de atencion */
	private String codigoFormaAtencion;
	
	public Integer getConsecutivoClaseAtencion() {
		return consecutivoClaseAtencion;
	}
	public void setConsecutivoClaseAtencion(Integer consecutivoClaseAtencion) {
		this.consecutivoClaseAtencion = consecutivoClaseAtencion;
	}
	
	public String getCodigoClaseAtencion() {
		return codigoClaseAtencion;
	}
	public void setCodigoClaseAtencion(String codigoClaseAtencion) {
		this.codigoClaseAtencion = codigoClaseAtencion;
	}
	
	public String getDescripcionClaseAtencion() {
		return descripcionClaseAtencion;
	}
	public void setDescripcionClaseAtencion(String descripcionClaseAtencion) {
		this.descripcionClaseAtencion = descripcionClaseAtencion;
	}
	
	public Integer getConsecutivoFormaAtencion() {
		return consecutivoFormaAtencion;
	}
	public void setConsecutivoFormaAtencion(Integer consecutivoFormaAtencion) {
		this.consecutivoFormaAtencion = consecutivoFormaAtencion;
	}
		
	public String getCodigoFormaAtencion() {
		return codigoFormaAtencion;
	}
	public void setCodigoFormaAtencion(String codigoFormaAtencion) {
		this.codigoFormaAtencion = codigoFormaAtencion;
	}
}
