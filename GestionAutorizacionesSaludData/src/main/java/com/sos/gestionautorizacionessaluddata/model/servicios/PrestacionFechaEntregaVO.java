package com.sos.gestionautorizacionessaluddata.model.servicios;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;

/**
 * Class PrestacionFechaEntregaVO 
 * Clase que permite para almacenar la informacion de las prestaciones asociadas a la fecha de entrega
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/03/2016
 *
 */
public class PrestacionFechaEntregaVO implements Serializable{
	private static final long serialVersionUID = 189133043738797220L;

	/*Identifica el consecutivo del grupo de entrega de la prestacion */
	private String consecutivoGrupoEntrega;
	
	/*Identifica el grupo de entrega de la prestacion */
	private String grupoEntrega;
		
	/*Identifica la fecha de solicitud de la prestacion */
	private Date fechaSolicitud;
	
	/*Identifica la fecha de estimada de la entrega de la prestacion */
	private Date fechaEstimadaEntrega;	
	
	/*Identifica el listado de las prestaciones asociados a la fecha de entrega*/
	private PrestacionDTO prestacionDTO;
		
	public String getConsecutivoGrupoEntrega() {
		return consecutivoGrupoEntrega;
	}

	public void setConsecutivoGrupoEntrega(String consecutivoGrupoEntrega) {
		this.consecutivoGrupoEntrega = consecutivoGrupoEntrega;
	}

	public String getGrupoEntrega() {
		return grupoEntrega;
	}

	public void setGrupoEntrega(String grupoEntrega) {
		this.grupoEntrega = grupoEntrega;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Date getFechaEstimadaEntrega() {
		return fechaEstimadaEntrega;
	}

	public void setFechaEstimadaEntrega(Date fechaEstimadaEntrega) {
		this.fechaEstimadaEntrega = fechaEstimadaEntrega;
	}

	public PrestacionDTO getPrestacionDTO() {
		return prestacionDTO;
	}

	public void setPrestacionDTO(PrestacionDTO prestacionDTO) {
		this.prestacionDTO = prestacionDTO;
	}
}
