package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;


/**
 * Class ValidacionEspecialVO
 * Clase VO para la informacion de la validacion especial
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/01/2016
 *
 */
public class ValidacionEspecialVO implements Serializable {
	private static final long serialVersionUID = -8862052390824031832L;

	/*Identifica la fecha de validacion del afiliado*/
	private Date fechaValidacion;	

	/*Identifica si la validacion especial del afialido se valida */
	private boolean marcaValidacionExiste;
	
	/*Identifica el consecutvo del estado de derecho del afialiado */
	private Integer consecutivoCodigoEstadoDerecho;
	
	/*Identifica el numero de validacion de derecho del afialiado */
	private BigInteger numeroValidacion;
	
	/*Identifica la maxima fecha que aplica la validacion de derecho del afiliado */
	private Date fechaMaximaComprobante;
	
	/*Identifica la oficina donde se genera la validacion especial del afiliado*/
	private OficinasVO oficinasVO;
	
	/*Identifica el usuario que realiza la validacion especial del afiliado*/
	private String usroValidacion;	
	
	/*Identifica la informacion del afiliado a quien le realizan la validacion especial*/
	private AfiliadoVO afiliadoVO;
	
	/** identifica el numero unico de OPS */
	private Integer numeroUnicoAutorizacion;
	
	/* Identifica el Consecutivo de la solicitud  */
	private Integer consecutivoSolicitud;
	

	public Date getFechaValidacion() {
		return fechaValidacion;
	}

	public void setFechaValidacion(Date fechaValidacion) {
		this.fechaValidacion = fechaValidacion;
	}

	public boolean isMarcaValidacionExiste() {
		return marcaValidacionExiste;
	}

	public void setMarcaValidacionExiste(boolean marcaValidacionExiste) {
		this.marcaValidacionExiste = marcaValidacionExiste;
	}

	public Integer getConsecutivoCodigoEstadoDerecho() {
		return consecutivoCodigoEstadoDerecho;
	}

	public void setConsecutivoCodigoEstadoDerecho(
			Integer consecutivoCodigoEstadoDerecho) {
		this.consecutivoCodigoEstadoDerecho = consecutivoCodigoEstadoDerecho;
	}

	public Date getFechaMaximaComprobante() {
		return fechaMaximaComprobante;
	}

	public void setFechaMaximaComprobante(Date fechaMaximaComprobante) {
		this.fechaMaximaComprobante = fechaMaximaComprobante;
	}

	public OficinasVO getOficinasVO() {
		return oficinasVO;
	}

	public void setOficinasVO(OficinasVO oficinasVO) {
		this.oficinasVO = oficinasVO;
	}

	public BigInteger getNumeroValidacion() {
		return numeroValidacion;
	}

	public void setNumeroValidacion(BigInteger numeroValidacion) {
		this.numeroValidacion = numeroValidacion;
	}
	
	public String getUsroValidacion() {
		return usroValidacion;
	}

	public void setUsroValidacion(String usroValidacion) {
		this.usroValidacion = usroValidacion;
	}
	
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public Integer getNumeroUnicoAutorizacion() {
		return numeroUnicoAutorizacion;
	}

	public void setNumeroUnicoAutorizacion(Integer numeroUnicoAutorizacion) {
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
	}

	public Integer getConsecutivoSolicitud() {
		return consecutivoSolicitud;
	}

	public void setConsecutivoSolicitud(Integer consecutivoSolicitud) {
		this.consecutivoSolicitud = consecutivoSolicitud;
	}
}
