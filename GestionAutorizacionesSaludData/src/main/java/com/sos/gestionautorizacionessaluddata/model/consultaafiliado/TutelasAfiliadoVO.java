package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.Date;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;


/**
 * Class TutelasAfiliadoVO
 * Clase VO para las tutelas de los afiliados
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class TutelasAfiliadoVO implements Serializable {
	private static final long serialVersionUID = -6735150280482232598L;

	/*Identifica el consecutivo de la notificacion de la tutela*/
	private Integer consecutivoNotificacion;
	
	/*Identifica el consecutivo de la oficina*/
	private Integer consecutivoCodigoOficina;
	
	/*Identifica la descripcion de la tutela*/
	private String descripcionTutela;
	
	/*Identifica el consecutivo del estado de la tutela*/
	private Integer consecutivoEstadoTutela;
	
	/*Identifica la descripcion del estado de la tutela*/
	private String descripcionEstadoTutela;
	
	/*Identifica el consecutivo fallo de la tutela*/
	private Integer consecutivoEstadoFallo;
	
	/*Identifica la descripcion fallo de la tutela*/
	private String descripcionEstadoFallo;
	
	/*Identifica el resuelve de la tutela*/
	private String resuelveTutela;
	
	/*Identifica el objeto Diagnostico asociado a la tutela*/
	private DiagnosticosVO diagnosticoVO;
	
	/*Identifica la fecha de notificacion de la tutela*/
	private Date fechaNotificacion;
		
	/*Identifica el consecutivo causa del estado de la notificacion de la tutela*/
	private Integer consecutivoCodigoCausaEstadoNotificacion;
	
	
	public Integer getConsecutivoNotificacion() {
		return consecutivoNotificacion;
	}
	public void setConsecutivoNotificacion(Integer consecutivoNotificacion) {
		this.consecutivoNotificacion = consecutivoNotificacion;
	}
	public Integer getConsecutivoCodigoOficina() {
		return consecutivoCodigoOficina;
	}
	public void setConsecutivoCodigoOficina(Integer consecutivoCodigoOficina) {
		this.consecutivoCodigoOficina = consecutivoCodigoOficina;
	}
	public String getDescripcionTutela() {
		return descripcionTutela;
	}
	public void setDescripcionTutela(String descripcionTutela) {
		this.descripcionTutela = descripcionTutela;
	}
	public Integer getConsecutivoEstadoTutela() {
		return consecutivoEstadoTutela;
	}
	public void setConsecutivoEstadoTutela(Integer consecutivoEstadoTutela) {
		this.consecutivoEstadoTutela = consecutivoEstadoTutela;
	}
	public String getDescripcionEstadoTutela() {
		return descripcionEstadoTutela;
	}
	public void setDescripcionEstadoTutela(String descripcionEstadoTutela) {
		this.descripcionEstadoTutela = descripcionEstadoTutela;
	}
	public Integer getConsecutivoEstadoFallo() {
		return consecutivoEstadoFallo;
	}
	public void setConsecutivoEstadoFallo(Integer consecutivoEstadoFallo) {
		this.consecutivoEstadoFallo = consecutivoEstadoFallo;
	}
	public String getDescripcionEstadoFallo() {
		return descripcionEstadoFallo;
	}
	public void setDescripcionEstadoFallo(String descripcionEstadoFallo) {
		this.descripcionEstadoFallo = descripcionEstadoFallo;
	}
	public String getResuelveTutela() {
		return resuelveTutela;
	}
	public void setResuelveTutela(String resuelveTutela) {
		this.resuelveTutela = resuelveTutela;
	}
	public DiagnosticosVO getDiagnosticoVO() {
		return diagnosticoVO;
	}
	public void setDiagnosticoVO(DiagnosticosVO diagnosticoVO) {
		this.diagnosticoVO = diagnosticoVO;
	}
	public Integer getConsecutivoCodigoCausaEstadoNotificacion() {
		return consecutivoCodigoCausaEstadoNotificacion;
	}
	public void setConsecutivoCodigoCausaEstadoNotificacion(
			Integer consecutivoCodigoCausaEstadoNotificacion) {
		this.consecutivoCodigoCausaEstadoNotificacion = consecutivoCodigoCausaEstadoNotificacion;
	}	
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
}
