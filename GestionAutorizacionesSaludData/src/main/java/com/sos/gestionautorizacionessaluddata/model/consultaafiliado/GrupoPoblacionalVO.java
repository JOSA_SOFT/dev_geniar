package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.Date;

/**
 * Class GrupoPoblacionalVO
 * Clase VO del grupo poblacional del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/12/2015
 *
 */
public class GrupoPoblacionalVO implements Serializable {
	private static final long serialVersionUID = -3284379333688009287L;

	/*Identifica el codigo del grupo poblacional del afiliado.*/
	private String codigoGrupoPoblacional;
	
	/*Identifica la descripcion del grupo poblacional del afiliado.*/
	private String descripcionGrupoPoblacional;
	
	/*Identifica el inicio de vigencia del grupo poblacional del afiliado.*/
	private Date inicioVigencia;
	
	/*Identifica el fin de vigencia del grupo poblacional del afiliado.*/
	private Date finVigencia;
	
	public String getCodigoGrupoPoblacional() {
		return codigoGrupoPoblacional;
	}

	public void setCodigoGrupoPoblacional(String codigoGrupoPoblacional) {
		this.codigoGrupoPoblacional = codigoGrupoPoblacional;
	}

	public String getDescripcionGrupoPoblacional() {
		return descripcionGrupoPoblacional;
	}

	public void setDescripcionGrupoPoblacional(String descripcionGrupoPoblacional) {
		this.descripcionGrupoPoblacional = descripcionGrupoPoblacional;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * @return the finVigencia
	 */
	public Date getFinVigencia() {
		return finVigencia;
	}

	/**
	 * @param finVigencia the finVigencia to set
	 */
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

}
