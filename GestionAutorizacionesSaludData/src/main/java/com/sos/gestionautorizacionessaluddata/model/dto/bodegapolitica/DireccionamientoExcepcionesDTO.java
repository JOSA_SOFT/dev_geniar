package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;
import java.util.Date;

public class DireccionamientoExcepcionesDTO implements Serializable{
	private static final long serialVersionUID = -3690426733422112032L;
	private String cdgoIntrno;
	private String nmbreScrsl;
	private String dscrpcnSde;
	private String dscrpcnZna;
	private int eddMnma;
	private int eddMxma;
	private int cnsctvoCdgoDgnstco;
	private String dscrpcnDgnstco;
	private Date incoVgnca;
	private Date fnVgnca;
	private String dscrpcnCdd;

	public String getCdgoIntrno() {
		return cdgoIntrno;
	}

	public void setCdgoIntrno(String cdgoIntrno) {
		this.cdgoIntrno = cdgoIntrno;
	}

	public String getNmbreScrsl() {
		return nmbreScrsl;
	}

	public void setNmbreScrsl(String nmbreScrsl) {
		this.nmbreScrsl = nmbreScrsl;
	}

	public String getDscrpcnSde() {
		return dscrpcnSde;
	}

	public void setDscrpcnSde(String dscrpcnSde) {
		this.dscrpcnSde = dscrpcnSde;
	}

	public String getDscrpcnZna() {
		return dscrpcnZna;
	}

	public void setDscrpcnZna(String dscrpcnZna) {
		this.dscrpcnZna = dscrpcnZna;
	}

	public int getEddMnma() {
		return eddMnma;
	}

	public void setEddMnma(int eddMnma) {
		this.eddMnma = eddMnma;
	}

	public int getEddMxma() {
		return eddMxma;
	}

	public void setEddMxma(int eddMxma) {
		this.eddMxma = eddMxma;
	}

	public int getCnsctvoCdgoDgnstco() {
		return cnsctvoCdgoDgnstco;
	}

	public void setCnsctvoCdgoDgnstco(int cnsctvoCdgoDgnstco) {
		this.cnsctvoCdgoDgnstco = cnsctvoCdgoDgnstco;
	}

	public String getDscrpcnDgnstco() {
		return dscrpcnDgnstco;
	}

	public void setDscrpcnDgnstco(String dscrpcnDgnstco) {
		this.dscrpcnDgnstco = dscrpcnDgnstco;
	}

	public Date getIncoVgnca() {
		return incoVgnca;
	}

	public void setIncoVgnca(Date incoVgnca) {
		this.incoVgnca = incoVgnca;
	}

	public Date getFnVgnca() {
		return fnVgnca;
	}

	public void setFnVgnca(Date fnVgnca) {
		this.fnVgnca = fnVgnca;
	}

	public String getDscrpcnCdd() {
		return dscrpcnCdd;
	}

	public void setDscrpcnCdd(String dscrpcnCdd) {
		this.dscrpcnCdd = dscrpcnCdd;
	}
}
