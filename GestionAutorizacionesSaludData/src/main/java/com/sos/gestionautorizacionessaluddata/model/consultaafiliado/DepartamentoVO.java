package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class DepartamentoVO
 * Clase VO de los departamentos
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class DepartamentoVO implements Serializable {
	private static final long serialVersionUID = 4765203590803305848L;

	/*Consecutivo del departamento*/
	private Integer consecutivoCodigoDepartamento;
	
	/*Codigo del departamento*/
	private String codigoDepartamento;
	
	/*Descripcion del departamento*/
	private String descripcionDepartamento;
	
	public Integer getConsecutivoCodigoDepartamento() {
		return consecutivoCodigoDepartamento;
	}

	public void setConsecutivoCodigoDepartamento(Integer consecutivoCodigoDepartamento) {
		this.consecutivoCodigoDepartamento = consecutivoCodigoDepartamento;
	}

	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}

	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	public String getDescripcionDepartamento() {
		return descripcionDepartamento;
	}

	public void setDescripcionDepartamento(String descripcionDepartamento) {
		this.descripcionDepartamento = descripcionDepartamento;
	}
}
