package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * Almacena los dato de concepto de pago de una OPS
 * @author GENIAR
 */
public class ConceptoPagoOpsVO  implements Serializable {
	private static final long serialVersionUID = 2113635759499388416L;
	private Double valorConceptoPago;
	private String nombreConceptoPago;
	private int consecutivoCodigoConcepto;
	
	public Double getValorConceptoPago() {
		return valorConceptoPago;
	}
	public void setValorConceptoPago(Double valorConceptoPago) {
		this.valorConceptoPago = valorConceptoPago;
	}
	public String getNombreConceptoPago() {
		return nombreConceptoPago;
	}
	public void setNombreConceptoPago(String nombreConceptoPago) {
		this.nombreConceptoPago = nombreConceptoPago;
	}
	public int getConsecutivoCodigoConcepto() {
		return consecutivoCodigoConcepto;
	}
	public void setConsecutivoCodigoConcepto(int consecutivoCodigoConcepto) {
		this.consecutivoCodigoConcepto = consecutivoCodigoConcepto;
	}
}
