package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class PrestacionesAprobadasDTO
 * Clase DTO para almacenar la consulta de Prestaciones Aprobadas
 * @author ing. Rafael Cano
 * @version 11/02/2016
 *
 */

public class GestionPrestacionVO implements Serializable {
	private static final long serialVersionUID = 5623910051464444320L;
	private int consTipoPrestacion;
	private String consPrestacion;
	private String consTipoPrestador;
	private String observaciones;
	
	public int getConsTipoPrestacion() {
		return consTipoPrestacion;
	}
	public void setConsTipoPrestacion(int consTipoPrestacion) {
		this.consTipoPrestacion = consTipoPrestacion;
	}
	public String getConsPrestacion() {
		return consPrestacion;
	}
	public void setConsPrestacion(String consPrestacion) {
		this.consPrestacion = consPrestacion;
	}
	public String getConsTipoPrestador() {
		return consTipoPrestador;
	}
	public void setConsTipoPrestador(String consTipoPrestador) {
		this.consTipoPrestador = consTipoPrestador;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
}
