package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.List;

/**
 * Se encarga de almacenar la informacion de las notas credito
 * @author geniarjag
 *
 */
public class NotasCreditoVO implements Serializable {
	private static final long serialVersionUID = -649157049966825360L;
	private List<DocumentoCajaVO> notas;
	private String entregaDinero;

	public List<DocumentoCajaVO> getNotas() {
		return notas;
	}

	public void setNotas(List<DocumentoCajaVO> notas) {
		this.notas = notas;
	}

	public String getEntregaDinero() {
		return entregaDinero;
	}

	public void setEntregaDinero(String entregaDinero) {
		this.entregaDinero = entregaDinero;
	}

}
