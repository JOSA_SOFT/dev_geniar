package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;

public class CupsXPlanXCargoDTO implements Serializable{
	private static final long serialVersionUID = -3067493222478622265L;
	private String descCargo;
	private String descPlan;

	public String getDescCargo() {
		return descCargo;
	}

	public void setDescCargo(String descCargo) {
		this.descCargo = descCargo;
	}

	public String getDescPlan() {
		return descPlan;
	}

	public void setDescPlan(String descPlan) {
		this.descPlan = descPlan;
	}
}
