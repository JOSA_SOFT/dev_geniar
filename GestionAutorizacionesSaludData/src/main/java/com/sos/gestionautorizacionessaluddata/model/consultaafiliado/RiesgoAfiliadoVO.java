package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Clase VO para los riesgos del afiliado
 * @author Julian Hernandez Gomez
 * @version 10/03/2016
 *
 */
public class RiesgoAfiliadoVO implements Serializable {
	private static final long serialVersionUID = -717672798955655754L;

	/** **/
	private String descRiesgoDiagnostico;
	
	/** **/
	private Integer consecutivoCodRiesgoDiagnostico;
	
	/** **/
	private Integer codRiesgoDiagnostico;
	
	/** **/
	private Integer generaNotificacion;
	
	/** **/
	private Integer tiempoExpiracion;

	/** **/
	private Integer consecutivoCodLaboratorio;

	public String getDescRiesgoDiagnostico() {
		return descRiesgoDiagnostico;
	}

	public void setDescRiesgoDiagnostico(String descRiesgoDiagnostico) {
		this.descRiesgoDiagnostico = descRiesgoDiagnostico;
	}

	public Integer getConsecutivoCodRiesgoDiagnostico() {
		return consecutivoCodRiesgoDiagnostico;
	}

	public void setConsecutivoCodRiesgoDiagnostico(
			Integer consecutivoCodRiesgoDiagnostico) {
		this.consecutivoCodRiesgoDiagnostico = consecutivoCodRiesgoDiagnostico;
	}

	public Integer getCodRiesgoDiagnostico() {
		return codRiesgoDiagnostico;
	}

	public void setCodRiesgoDiagnostico(Integer codRiesgoDiagnostico) {
		this.codRiesgoDiagnostico = codRiesgoDiagnostico;
	}

	public Integer getGeneraNotificacion() {
		return generaNotificacion;
	}

	public void setGeneraNotificacion(Integer generaNotificacion) {
		this.generaNotificacion = generaNotificacion;
	}

	public Integer getTiempoExpiracion() {
		return tiempoExpiracion;
	}

	public void setTiempoExpiracion(Integer tiempoExpiracion) {
		this.tiempoExpiracion = tiempoExpiracion;
	}

	public Integer getConsecutivoCodLaboratorio() {
		return consecutivoCodLaboratorio;
	}

	public void setConsecutivoCodLaboratorio(Integer consecutivoCodLaboratorio) {
		this.consecutivoCodLaboratorio = consecutivoCodLaboratorio;
	}
	
	
	
	
}
