package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;


/**
 * Class ConceptosGastoVO
 * Clase VO que contiene los conceptos de gasto
 * @author ing. Victor Hugo Gil Ramos
 * @version 05/04/2016
 *
 */
public class ConceptosGastoVO implements Serializable{
	private static final long serialVersionUID = 5775501947632216375L;

	/*Identifica la consecutivo codigo del servicio*/
	private Integer consecutivoCodigoServicioSolicitado;
	
	/*Identifica la descripcion del servicio*/
	private String descripcionServicioSolicitado;

	/*Identifica la descripcion del servicio*/
	private String descripcionConceptoGasto;
	
	/*Identifica el numero unico de autorizacion de la ops*/
	private Integer numeroUnicoAutorizacionOps;
	
	/*Identifica la descripcion de la razon social del prestador*/
	private String razonSocial;
	
	/*Identifica el valor de la prestacion */
	private Integer valorPrestacion; 
	
	/*Identifica el valor de la prestacion */
	private Integer valorAcumulado; 
	
	/*Identifica el valor anterior de la prestacion */
	private Integer valorAnterior; 
	
	/*Identifica la fecha de impresion */
	private Date fechaImpresion;
	
	/*Identifica la fecha de utilizacion desde */
	private Date fechaUtilizacionDesde;
	
	/*Identifica la fecha de utilizacion hasta */
	private Date fechaUtilizacionHasta;
	
	/*Identifica el numero de radicacion cuenta */
	private Integer numeroRadicacionCuentas;
	
	/*Identifica el consecutivo oficina cuenta */
	private Integer consecutivoOficinaCuenta;
	
	/*Identifica el numero de identificacion del prestador */
	private String numeroIdentificacionPrestador;
	
	/*Identifica el codigo interno del prestador */
	private String codigoInterno;
	
	/*Identifica el estado del concepto de gasto */
	private String descripcionEstadoConcepto;
	
	/*Identifica la descripcion de la causa */
	private String descripcionCausaNovedad;
	
	/*Identifica el usuario de ultima modificacion */
	private String usuarioUltimaModificacion;
	
	/*Identifica el consecutivo la factura */
	private Integer numerofactura;
	
	/*Identifica la fecha de radicacion */
	private Date fechaRadicacion;
	
	/*Identifica si puede ver el enlace la anulacion */
	private boolean verAnular;
	/*Identifica si puede ver el enlace modificar ops */
	private boolean verModificar;
	
	/*Identifica la consecutivo estado*/
	private Integer consecutivoEstadoServicioSolicitado;
	
	
	public Integer getConsecutivoCodigoServicioSolicitado() {
		return consecutivoCodigoServicioSolicitado;
	}

	public void setConsecutivoCodigoServicioSolicitado(
			Integer consecutivoCodigoServicioSolicitado) {
		this.consecutivoCodigoServicioSolicitado = consecutivoCodigoServicioSolicitado;
	}

	public String getDescripcionServicioSolicitado() {
		return descripcionServicioSolicitado;
	}

	public void setDescripcionServicioSolicitado(
			String descripcionServicioSolicitado) {
		this.descripcionServicioSolicitado = descripcionServicioSolicitado;
	}

	public String getDescripcionConceptoGasto() {
		return descripcionConceptoGasto;
	}

	public void setDescripcionConceptoGasto(String descripcionConceptoGasto) {
		this.descripcionConceptoGasto = descripcionConceptoGasto;
	}

	public Integer getNumeroUnicoAutorizacionOps() {
		return numeroUnicoAutorizacionOps;
	}

	public void setNumeroUnicoAutorizacionOps(Integer numeroUnicoAutorizacionOps) {
		this.numeroUnicoAutorizacionOps = numeroUnicoAutorizacionOps;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public float getValorPrestacion() {
		return valorPrestacion;
	}

	public void setValorPrestacion(Integer valorPrestacion) {
		this.valorPrestacion = valorPrestacion;
	}

	public float getValorAcumulado() {
		return valorAcumulado;
	}

	public void setValorAcumulado(Integer valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}	
	
	public Integer getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(Integer valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public Date getFechaImpresion() {
		return fechaImpresion;
	}

	public void setFechaImpresion(Date fechaImpresion) {
		this.fechaImpresion = fechaImpresion;
	}

	public Date getFechaUtilizacionDesde() {
		return fechaUtilizacionDesde;
	}

	public void setFechaUtilizacionDesde(Date fechaUtilizacionDesde) {
		this.fechaUtilizacionDesde = fechaUtilizacionDesde;
	}

	public Date getFechaUtilizacionHasta() {
		return fechaUtilizacionHasta;
	}

	public void setFechaUtilizacionHasta(Date fechaUtilizacionHasta) {
		this.fechaUtilizacionHasta = fechaUtilizacionHasta;
	}
	
	public Integer getNumeroRadicacionCuentas() {
		return numeroRadicacionCuentas;
	}

	public void setNumeroRadicacionCuentas(Integer numeroRadicacionCuentas) {
		this.numeroRadicacionCuentas = numeroRadicacionCuentas;
	}

	public Integer getConsecutivoOficinaCuenta() {
		return consecutivoOficinaCuenta;
	}

	public void setConsecutivoOficinaCuenta(Integer consecutivoOficinaCuenta) {
		this.consecutivoOficinaCuenta = consecutivoOficinaCuenta;
	}

	/**
	 * @return the numeroIdentificacionPrestador
	 */
	public String getNumeroIdentificacionPrestador() {
		return numeroIdentificacionPrestador;
	}

	/**
	 * @param numeroIdentificacionPrestador the numeroIdentificacionPrestador to set
	 */
	public void setNumeroIdentificacionPrestador(
			String numeroIdentificacionPrestador) {
		this.numeroIdentificacionPrestador = numeroIdentificacionPrestador;
	}

	/**
	 * @return the codigoInterno
	 */
	public String getCodigoInterno() {
		return codigoInterno;
	}

	/**
	 * @param codigoInterno the codigoInterno to set
	 */
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	
	public String getDescripcionEstadoConcepto() {
		return descripcionEstadoConcepto;
	}

	public void setDescripcionEstadoConcepto(String descripcionEstadoConcepto) {
		this.descripcionEstadoConcepto = descripcionEstadoConcepto;
	}

	public String getDescripcionCausaNovedad() {
		return descripcionCausaNovedad;
	}

	public void setDescripcionCausaNovedad(String descripcionCausaNovedad) {
		this.descripcionCausaNovedad = descripcionCausaNovedad;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	/**
	 * @return the numerofactura
	 */
	public Integer getNumerofactura() {
		return numerofactura;
	}

	/**
	 * @param numerofactura the numerofactura to set
	 */
	public void setNumerofactura(Integer numerofactura) {
		this.numerofactura = numerofactura;
	}

	/**
	 * @return the fechaRadicacion
	 */
	public Date getFechaRadicacion() {
		return fechaRadicacion;
	}

	/**
	 * @param fechaRadicacion the fechaRadicacion to set
	 */
	public void setFechaRadicacion(Date fechaRadicacion) {
		this.fechaRadicacion = fechaRadicacion;
	}

	/**
	 * @return the verAnular
	 */
	public boolean isVerAnular() {
		return verAnular;
	}

	/**
	 * @param verAnular the verAnular to set
	 */
	public void setVerAnular(boolean verAnular) {
		this.verAnular = verAnular;
	}

	/**
	 * @return the consecutivoEstadoServicioSolicitado
	 */
	public Integer getConsecutivoEstadoServicioSolicitado() {
		return consecutivoEstadoServicioSolicitado;
	}

	/**
	 * @param consecutivoEstadoServicioSolicitado the consecutivoEstadoServicioSolicitado to set
	 */
	public void setConsecutivoEstadoServicioSolicitado(
			Integer consecutivoEstadoServicioSolicitado) {
		this.consecutivoEstadoServicioSolicitado = consecutivoEstadoServicioSolicitado;
	}

	/**
	 * @return the verModificar
	 */
	public boolean isVerModificar() {
		return verModificar;
	}

	/**
	 * @param verModificar the verModificar to set
	 */
	public void setVerModificar(boolean verModificar) {
		this.verModificar = verModificar;
	}
	
	
	
}
