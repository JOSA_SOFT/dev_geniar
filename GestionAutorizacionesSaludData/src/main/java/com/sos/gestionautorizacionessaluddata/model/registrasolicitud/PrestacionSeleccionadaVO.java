package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

/**
 * Class PrestacionSeleccionadaVO
 * Clase VO que almacena las prestaciones seleccionadas
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/01/2016
 *
 */
public class PrestacionSeleccionadaVO implements Serializable{
	private static final long serialVersionUID = 6675146639631931242L;

	/*Identifica el codigo de la prestacion seleccionada*/
	private String codigoPrestacionSeleccionada;
	
	/*Identifica el consecutivo del item de presupuesto seleccionado*/
	private int consecutivoItemPresupuestoSeleccionado;
	
	public String getCodigoPrestacionSeleccionada() {
		return codigoPrestacionSeleccionada;
	}

	public void setCodigoPrestacionSeleccionada(String codigoPrestacionSeleccionada) {
		this.codigoPrestacionSeleccionada = codigoPrestacionSeleccionada;
	}

	public int getConsecutivoItemPresupuestoSeleccionado() {
		return consecutivoItemPresupuestoSeleccionado;
	}

	public void setConsecutivoItemPresupuestoSeleccionado(
			int consecutivoItemPresupuestoSeleccionado) {
		this.consecutivoItemPresupuestoSeleccionado = consecutivoItemPresupuestoSeleccionado;
	}
}
