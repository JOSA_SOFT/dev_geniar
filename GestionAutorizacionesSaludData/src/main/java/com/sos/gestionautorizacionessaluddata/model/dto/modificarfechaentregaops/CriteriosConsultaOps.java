package com.sos.gestionautorizacionessaluddata.model.dto.modificarfechaentregaops;

import java.io.Serializable;
import java.util.Date;

public class CriteriosConsultaOps implements Serializable {
	private static final long serialVersionUID = 3809373264988799090L;

	private String planes;
	private String afiliados;
	private Integer incapacidad;
	private Date fechaEntregaIni;
	private Date fechaEntregaFin;
	private Date fechaCreacionIni;
	private Date fechaCreacionFin;
	private String estados;
	private String sedes;
	private String gruposEntrega;
	private Integer prestacion;
	private Integer entregaInmediata;
	private String usuario;

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getPlanes() {
		return planes;
	}

	public void setPlanes(String planes) {
		this.planes = planes;
	}

	public String getAfiliados() {
		return afiliados;
	}

	public void setAfiliados(String afiliados) {
		this.afiliados = afiliados;
	}

	public Integer getIncapacidad() {
		return incapacidad;
	}

	public void setIncapacidad(Integer incapacidad) {
		this.incapacidad = incapacidad;
	}

	public String getEstados() {
		return estados;
	}

	public void setEstados(String estados) {
		this.estados = estados;
	}

	public String getSedes() {
		return sedes;
	}

	public void setSedes(String sedes) {
		this.sedes = sedes;
	}

	public String getGruposEntrega() {
		return gruposEntrega;
	}

	public void setGruposEntrega(String gruposEntrega) {
		this.gruposEntrega = gruposEntrega;
	}

	public Integer getPrestacion() {
		return prestacion;
	}

	public void setPrestacion(Integer prestacion) {
		this.prestacion = prestacion;
	}

	public Integer getEntregaInmediata() {
		return entregaInmediata;
	}

	public void setEntregaInmediata(Integer entregaInmediata) {
		this.entregaInmediata = entregaInmediata;
	}

	public Date getFechaEntregaIni() {
		return fechaEntregaIni;
	}

	public void setFechaEntregaIni(Date fechaEntregaIni) {
		this.fechaEntregaIni = fechaEntregaIni;
	}

	public Date getFechaEntregaFin() {
		return fechaEntregaFin;
	}

	public void setFechaEntregaFin(Date fechaEntregaFin) {
		this.fechaEntregaFin = fechaEntregaFin;
	}

	public Date getFechaCreacionIni() {
		return fechaCreacionIni;
	}

	public void setFechaCreacionIni(Date fechaCreacionIni) {
		this.fechaCreacionIni = fechaCreacionIni;
	}

	public Date getFechaCreacionFin() {
		return fechaCreacionFin;
	}

	public void setFechaCreacionFin(Date fechaCreacionFin) {
		this.fechaCreacionFin = fechaCreacionFin;
	}
}
