package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;


/**
 * Class OficinasVO
 * Clase VO de las oficinas
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/12/2015
 *
 */
public class OficinasVO implements Serializable {
	private static final long serialVersionUID = -1477322051315948096L;

	/*Consecutivo codigo oficina*/
	private Integer consecutivoCodigoOficina;
	
	/*Consecutivo codigo sede*/
	private Integer consecutivoCodigoSede;
	
	/*Codigo oficina*/
	private String codigoOficina;
	
	/*Codigo sede*/
	private String codigoSede;
	
	/*Descripcion oficina*/
	private String descripcionOficina;	
	
	private String descripcionSede;
	
	private Integer consecutivoTipoOficina;
	
	public Integer getConsecutivoCodigoOficina() {
		return consecutivoCodigoOficina;
	}
	public void setConsecutivoCodigoOficina(Integer consecutivoCodigoOficina) {
		this.consecutivoCodigoOficina = consecutivoCodigoOficina;
	}
	public Integer getConsecutivoCodigoSede() {
		return consecutivoCodigoSede;
	}
	public void setConsecutivoCodigoSede(Integer consecutivoCodigoSede) {
		this.consecutivoCodigoSede = consecutivoCodigoSede;
	}
	public String getCodigoOficina() {
		return codigoOficina;
	}
	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}
	public String getCodigoSede() {
		return codigoSede;
	}
	public void setCodigoSede(String codigoSede) {
		this.codigoSede = codigoSede;
	}
	public String getDescripcionOficina() {
		return descripcionOficina;
	}
	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}
	public String getDescripcionSede() {
		return descripcionSede;
	}
	public void setDescripcionSede(String descripcionSede) {
		this.descripcionSede = descripcionSede;
	}
	public Integer getConsecutivoTipoOficina() {
		return consecutivoTipoOficina;
	}
	public void setConsecutivoTipoOficina(Integer consecutivoTipoOficina) {
		this.consecutivoTipoOficina = consecutivoTipoOficina;
	}
}
