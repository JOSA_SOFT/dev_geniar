package com.sos.gestionautorizacionessaluddata.model.malla;

import java.io.Serializable;
import java.util.List;


/**
 * Class ServiceErrorVO
 * Clase VO de los mensajes de error
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class ServiceErrorVO implements Serializable{
	private static final long serialVersionUID = -8225252270128833212L;

	/** Codigo del error del servicio */
	private String codigoError;
	
	/** Mensaje de error del servicio */
	private String mensajeError;
	
	/** Estador del error del servicio */
	private String estadoTerminacion;
	
	/** Mensaje del estado del error presentado */
	private String mensajeEstadoTerminacion;
	
	/** Identifica el listado de advertencias */
	private List<AdvertenciaVO> lAdvertenciaVO;

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getEstadoTerminacion() {
		return estadoTerminacion;
	}

	public void setEstadoTerminacion(String estadoTerminacion) {
		this.estadoTerminacion = estadoTerminacion;
	}

	public String getMensajeEstadoTerminacion() {
		return mensajeEstadoTerminacion;
	}

	public void setMensajeEstadoTerminacion(String mensajeEstadoTerminacion) {
		this.mensajeEstadoTerminacion = mensajeEstadoTerminacion;
	}
	
	
	public List<AdvertenciaVO> getlAdvertenciaVO() {
		return lAdvertenciaVO;
	}

	public void setlAdvertenciaVO(List<AdvertenciaVO> lAdvertenciaVO) {
		this.lAdvertenciaVO = lAdvertenciaVO;
	}
}
