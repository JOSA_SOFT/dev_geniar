package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;
import java.util.Date;

/**
 * Class RiesgosPacienteVO
 * Clase VO para los riesgos del paciente
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class RiesgosPacienteVO implements Serializable {
	private static final long serialVersionUID = -5758141763847976252L;

	/*Identifica la descripcion del riesgo.*/
	private String descripcionRiesgo;
		
	/*Identifica la fecha de la marcacion del riesgo.*/
	private Date fechaMarcacion;
	
	/*Identifica el consecutivo de la fuente de marcacion.*/
	private Integer consecutivoFuenteMarcacion;
	
	/*Identifica la fuente de marcacion.*/
	private String fuenteMarcacion;
	
	/*Identifica el consecutivo de la notificacion.*/
	private Integer consecutivoNotificacionAsociado;
	
	/*Identifica el consecutivo de la oficina asociado a la notificacion.*/
	private Integer consecutivoOficinaAsociado;
	
	/*Identifica la fecha fin de la marcacion del riesgo.*/
	private Date fechaFinMarcacion;
	
	/*Identifica la descripcion del libreto.*/
	private String descripcionLibreto;
	
	/*Identifica la descripcion del libreto  corto.*/
	private String descripcionLibretoCorto;
	
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}
	public Date getFechaMarcacion() {
		return fechaMarcacion;
	}
	public void setFechaMarcacion(Date fechaMarcacion) {
		this.fechaMarcacion = fechaMarcacion;
	}
	public Integer getConsecutivoFuenteMarcacion() {
		return consecutivoFuenteMarcacion;
	}
	public void setConsecutivoFuenteMarcacion(Integer consecutivoFuenteMarcacion) {
		this.consecutivoFuenteMarcacion = consecutivoFuenteMarcacion;
	}
	public String getFuenteMarcacion() {
		return fuenteMarcacion;
	}
	public void setFuenteMarcacion(String fuenteMarcacion) {
		this.fuenteMarcacion = fuenteMarcacion;
	}
	public Integer getConsecutivoNotificacionAsociado() {
		return consecutivoNotificacionAsociado;
	}
	public void setConsecutivoNotificacionAsociado(
			Integer consecutivoNotificacionAsociado) {
		this.consecutivoNotificacionAsociado = consecutivoNotificacionAsociado;
	}
	public Integer getConsecutivoOficinaAsociado() {
		return consecutivoOficinaAsociado;
	}
	public void setConsecutivoOficinaAsociado(Integer consecutivoOficinaAsociado) {
		this.consecutivoOficinaAsociado = consecutivoOficinaAsociado;
	}
	public Date getFechaFinMarcacion() {
		return fechaFinMarcacion;
	}
	public void setFechaFinMarcacion(Date fechaFinMarcacion) {
		this.fechaFinMarcacion = fechaFinMarcacion;
	}
	public String getDescripcionLibreto() {
		return descripcionLibreto;
	}
	public void setDescripcionLibreto(String descripcionLibreto) {
		this.descripcionLibreto = descripcionLibreto;
	}
	public String getDescripcionLibretoCorto() {
		return descripcionLibretoCorto;
	}
	public void setDescripcionLibretoCorto(String descripcionLibretoCorto) {
		this.descripcionLibretoCorto = descripcionLibretoCorto;
	}	
}
