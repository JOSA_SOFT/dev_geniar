package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class TiposIdentificacionVO
 * Clase VO de los tipos de identificacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/12/2015
 *
 */

public class TiposIdentificacionVO implements Serializable {
	private static final long serialVersionUID = -5931641222705682601L;

	/*Consecutivo del tipo de identificacion*/
	private Integer consecutivoTipoIdentificacion;
	
	/*codigo tipo de identificacion*/
	private String codigoTipoIdentificacion;
	
	/*descripcion del tipo de identificacion*/
	private String descripcionTipoIdentificacion;
	
	
	
	public TiposIdentificacionVO() {
		super();
	}
	public TiposIdentificacionVO(Integer consecutivoTipoIdentificacion,
			String codigoTipoIdentificacion,
			String descripcionTipoIdentificacion) {
		super();
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
		this.codigoTipoIdentificacion = codigoTipoIdentificacion;
		this.descripcionTipoIdentificacion = descripcionTipoIdentificacion;
	}
	public Integer getConsecutivoTipoIdentificacion() {
		return consecutivoTipoIdentificacion;
	}
	public void setConsecutivoTipoIdentificacion(Integer consecutivoTipoIdentificacion) {
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
	}
	public String getCodigoTipoIdentificacion() {
		return codigoTipoIdentificacion;
	}
	public void setCodigoTipoIdentificacion(String codigoTipoIdentificacion) {
		this.codigoTipoIdentificacion = codigoTipoIdentificacion;
	}
	public String getDescripcionTipoIdentificacion() {
		return descripcionTipoIdentificacion;
	}
	public void setDescripcionTipoIdentificacion(
			String descripcionTipoIdentificacion) {
		this.descripcionTipoIdentificacion = descripcionTipoIdentificacion;
	}
}
