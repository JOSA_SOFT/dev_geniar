package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;

/**
 * VO que tiene los datos del afililado.
 * @author GENIAR
 *
 */
public class AfiliadoNombresVO implements Serializable{
	private static final long serialVersionUID = -6864347169316612036L;
	
	private String primerNombreInfoAfiliado;
	private String segundoNombreInfoAfiliado;
	private String primerApellidoInfoAfiliado;
	private String segundoApellidoInfoAfiliado;
	private Integer numeroUnicoIdentificacionInfoAfiliado;
	
	public String getNombreCompleto(){
		return this.primerNombreInfoAfiliado + " " + this.primerApellidoInfoAfiliado + " " + this.segundoNombreInfoAfiliado + " " + this.segundoApellidoInfoAfiliado;
	}

	public String getPrimerNombreInfoAfiliado() {
		return primerNombreInfoAfiliado;
	}

	public void setPrimerNombreInfoAfiliado(String primerNombreInfoAfiliado) {
		this.primerNombreInfoAfiliado = primerNombreInfoAfiliado;
	}

	public String getSegundoNombreInfoAfiliado() {
		return segundoNombreInfoAfiliado;
	}

	public void setSegundoNombreInfoAfiliado(String segundoNombreInfoAfiliado) {
		this.segundoNombreInfoAfiliado = segundoNombreInfoAfiliado;
	}

	public String getPrimerApellidoInfoAfiliado() {
		return primerApellidoInfoAfiliado;
	}

	public void setPrimerApellidoInfoAfiliado(String primerApellidoInfoAfiliado) {
		this.primerApellidoInfoAfiliado = primerApellidoInfoAfiliado;
	}

	public String getSegundoApellidoInfoAfiliado() {
		return segundoApellidoInfoAfiliado;
	}

	public void setSegundoApellidoInfoAfiliado(String segundoApellidoInfoAfiliado) {
		this.segundoApellidoInfoAfiliado = segundoApellidoInfoAfiliado;
	}

	public Integer getNumeroUnicoIdentificacionInfoAfiliado() {
		return numeroUnicoIdentificacionInfoAfiliado;
	}

	public void setNumeroUnicoIdentificacionInfoAfiliado(
			Integer numeroUnicoIdentificacionInfoAfiliado) {
		this.numeroUnicoIdentificacionInfoAfiliado = numeroUnicoIdentificacionInfoAfiliado;
	}
}
