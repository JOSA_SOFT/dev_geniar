package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class EstadoMegaVO
 * Clase VO de EstadoMegaVO 
 * @author ing. German Perez - Geniar S.A.S
 * @version 22/09/2017
 *
 */

public class EstadoMegaVO implements Serializable {
	private static final long serialVersionUID = 3277938242868670600L;

	/** informacion del consecutivo del estado de mega */
	private Integer consecutivoCodigoEstadoMega;	
	
	/** informacion del codigo del estado de mega */
	private String codigoEstadoMega;	
	
	/** informacion de la descripcion del estado de mega */
	private String descripcionEstadoMega;	
	
	/** informacion del codigo del estado de la atencion */
	private String codigoEstadoAtencion;	
	
	/** informacion de la descripcion del estado de la atencion */
	private String descripcionEstadoAtencion;

	public Integer getConsecutivoCodigoEstadoMega() {
		return consecutivoCodigoEstadoMega;
	}

	public void setConsecutivoCodigoEstadoMega(Integer consecutivoCodigoEstadoMega) {
		this.consecutivoCodigoEstadoMega = consecutivoCodigoEstadoMega;
	}

	public String getCodigoEstadoMega() {
		return codigoEstadoMega;
	}

	public void setCodigoEstadoMega(String codigoEstadoMega) {
		this.codigoEstadoMega = codigoEstadoMega;
	}

	public String getDescripcionEstadoMega() {
		return descripcionEstadoMega;
	}

	public void setDescripcionEstadoMega(String descripcionEstadoMega) {
		this.descripcionEstadoMega = descripcionEstadoMega;
	}

	public String getCodigoEstadoAtencion() {
		return codigoEstadoAtencion;
	}

	public void setCodigoEstadoAtencion(String codigoEstadoAtencion) {
		this.codigoEstadoAtencion = codigoEstadoAtencion;
	}

	public String getDescripcionEstadoAtencion() {
		return descripcionEstadoAtencion;
	}

	public void setDescripcionEstadoAtencion(String descripcionEstadoAtencion) {
		this.descripcionEstadoAtencion = descripcionEstadoAtencion;
	}	
}
