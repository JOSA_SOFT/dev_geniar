package com.sos.gestionautorizacionessaluddata.model.consultaafiliado;

import java.io.Serializable;

/**
 * Class TipoCotizanteVO
 * Clase VO del tipo de cotizante del empleado
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/01/2016
 *
 */
public class TipoCotizanteVO implements Serializable {
	private static final long serialVersionUID = 2315806595043362186L;

	/*Identifica el consecutivo codigo tipo cotizante.*/
	private Integer consecutivoCodigoTipoCotizante;
	
	/*Identifica el codigo del tipo cotizante.*/
	private String codigoTipoCotizante;
	
	/*Identifica la descripcion del tipo cotizante.*/
	private String descripcionTipoCotizante;
	
	public Integer getConsecutivoCodigoTipoCotizante() {
		return consecutivoCodigoTipoCotizante;
	}

	public void setConsecutivoCodigoTipoCotizante(Integer consecutivoCodigoTipoCotizante) {
		this.consecutivoCodigoTipoCotizante = consecutivoCodigoTipoCotizante;
	}

	public String getCodigoTipoCotizante() {
		return codigoTipoCotizante;
	}

	public void setCodigoTipoCotizante(String codigoTipoCotizante) {
		this.codigoTipoCotizante = codigoTipoCotizante;
	}

	public String getDescripcionTipoCotizante() {
		return descripcionTipoCotizante;
	}

	public void setDescripcionTipoCotizante(String descripcionTipoCotizante) {
		this.descripcionTipoCotizante = descripcionTipoCotizante;
	}
}
