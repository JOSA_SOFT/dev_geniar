package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Clase que encapsula los datos de la propiedades que se consulta por medio del PropertyQuery
 * @author waguilera
 *
 */
public class PropiedadVO implements Serializable{
	private static final long serialVersionUID = 8785104197807340956L;

	private String aplicacion;
	
	private String clasificacion;
	
	private String propiedad;
	
	private String valor;

	
	/**
	 * Constructor vacio de la clase
	 */
	public PropiedadVO() {
		super();
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
