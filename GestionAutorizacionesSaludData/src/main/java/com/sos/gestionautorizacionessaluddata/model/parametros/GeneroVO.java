package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class GeneroVo
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/11/2015
 *
 */

public class GeneroVO implements Serializable {
	private static final long serialVersionUID = 90294445936173061L;

	/* Consecutivo del genero  */
	private Integer consecutivoGenero;
	
	/* Codigo del genero  */
	private String codigoGenero;
	
	/* Descripcion del genero  */
	private String descripcionGenero;
	
	
	public Integer getConsecutivoGenero() {
		return consecutivoGenero;
	}
	public void setConsecutivoGenero(Integer consecutivoGenero) {
		this.consecutivoGenero = consecutivoGenero;
	}
	public String getDescripcionGenero() {
		return descripcionGenero;
	}
	public void setDescripcionGenero(String descripcionGenero) {
		this.descripcionGenero = descripcionGenero;
	}
	public String getCodigoGenero() {
		return codigoGenero;
	}
	public void setCodigoGenero(String codigoGenero) {
		this.codigoGenero = codigoGenero;
	}
}
