package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * VO DE LAS NOTAS CREDITOS
 * 
 * @author JOSE SOTO GENIAR
 *
 */
public class DocumentoCajaVO  implements Serializable {
	private static final long serialVersionUID = -3127770045889391101L;
	private boolean selected;
	private Integer numNotaCredito;
	private Date fechaGeneracion;
	private String identificacionAfiliado;
	private String nombreAfiliado;
	private Double valorAfiliad;

	private Integer nuiAfiliado;
	private Integer conceptoGeneracion;
	private Integer tipoDocumento;
	private Integer estadoDocumento;
	private Integer datoContacto;
	private Integer detalleMovimientoCaja;
	private String observacion;
	private String usuarioCreacion;
	private Integer numeroDocumento;
	private int pkDocCaja;

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected
	 *            the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the numNotaCredito
	 */
	public int getNumNotaCredito() {
		return numNotaCredito;
	}

	/**
	 * @param numNotaCredito
	 *            the numNotaCredito to set
	 */
	public void setNumNotaCredito(int numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}

	/**
	 * @return the fechaGeneracion
	 */
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	/**
	 * @param fechaGeneracion
	 *            the fechaGeneracion to set
	 */
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	/**
	 * @return the identificacionAfiliado
	 */
	public String getIdentificacionAfiliado() {
		return identificacionAfiliado;
	}

	/**
	 * @param identificacionAfiliado
	 *            the identificacionAfiliado to set
	 */
	public void setIdentificacionAfiliado(String identificacionAfiliado) {
		this.identificacionAfiliado = identificacionAfiliado;
	}

	/**
	 * @return the nombreAfiliado
	 */
	public String getNombreAfiliado() {
		return nombreAfiliado;
	}

	/**
	 * @param nombreAfiliado
	 *            the nombreAfiliado to set
	 */
	public void setNombreAfiliado(String nombreAfiliado) {
		this.nombreAfiliado = nombreAfiliado;
	}

	/**
	 * @return the valorAfiliad
	 */
	public double getValorAfiliad() {
		return valorAfiliad;
	}

	/**
	 * @param valorAfiliad
	 *            the valorAfiliad to set
	 */
	public void setValorAfiliad(double valorAfiliad) {
		this.valorAfiliad = valorAfiliad;
	}

	public int getNuiAfiliado() {
		return nuiAfiliado;
	}

	public void setNuiAfiliado(int nuiAfiliado) {
		this.nuiAfiliado = nuiAfiliado;
	}

	public Integer getConceptoGeneracion() {
		return conceptoGeneracion;
	}

	public void setConceptoGeneracion(Integer conceptoGeneracion) {
		this.conceptoGeneracion = conceptoGeneracion;
	}

	public int getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(int tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public int getEstadoDocumento() {
		return estadoDocumento;
	}

	public void setEstadoDocumento(int estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public Integer getDatoContacto() {
		return datoContacto;
	}

	public void setDatoContacto(Integer datoContacto) {
		this.datoContacto = datoContacto;
	}

	public Integer getDetalleMovimientoCaja() {
		return detalleMovimientoCaja;
	}

	public void setDetalleMovimientoCaja(Integer detalleMovimientoCaja) {
		this.detalleMovimientoCaja = detalleMovimientoCaja;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public int getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(int numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public void setPkDocCaja(int pkDocCaja) {
		this.pkDocCaja = pkDocCaja;
	}

	public int getPkDocCaja() {
		return pkDocCaja;
	}

}