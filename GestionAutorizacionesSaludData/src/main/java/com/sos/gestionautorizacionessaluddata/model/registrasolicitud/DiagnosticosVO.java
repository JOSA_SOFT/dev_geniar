package com.sos.gestionautorizacionessaluddata.model.registrasolicitud;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;

/**
 * Class DiagnosticoVO
 * Clase VO para los diagnosticos
 * @author ing. Victor Hugo Gil Ramos
 * @version 28/12/2015
 *
 */
public class DiagnosticosVO implements Serializable{
	private static final long serialVersionUID = -4209381213213841327L;

	/*Identifica el consecutivo del diagnostico.*/
	private Integer consecutivoCodigoDiagnostico;
	
	/*Identifica el codigo del diagnostico.*/
	private String codigoDiagnostico;
	
	/*Identifica la descripcion del diagnostico.*/
	private String descripcionDiagnostico;
	
	/*Identifica el consecutivo codigo de la contingencia.*/
	private Integer consecutivoCodigoContingencia;
	
	/*Identifica el consecutivo codigo Morbilidad.*/
	private Integer consecutivoCodigoMorbilidad;
	
	/*Identifica el codigo Morbilidad.*/
	private String codigoMorbilidad;
	
	/*Identifica el numero de dias promedio.*/
	private Integer numeroDiasPromedio;
	
	/*Identifica el consecutivo codigo recobro*/
	private Integer consecutivoCodigoRecobro;
	
	/*Identifica el genero al que aplica el diagnostico*/
	private GeneroVO generoVO;
		
	public Integer getConsecutivoCodigoDiagnostico() {
		return consecutivoCodigoDiagnostico;
	}
	public void setConsecutivoCodigoDiagnostico(Integer consecutivoCodigoDiagnostico) {
		this.consecutivoCodigoDiagnostico = consecutivoCodigoDiagnostico;
	}
	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}
	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}
	public String getDescripcionDiagnostico() {
		return descripcionDiagnostico;
	}
	public void setDescripcionDiagnostico(String descripcionDiagnostico) {
		this.descripcionDiagnostico = descripcionDiagnostico;
	}
	public Integer getConsecutivoCodigoContingencia() {
		return consecutivoCodigoContingencia;
	}
	public void setConsecutivoCodigoContingencia(Integer consecutivoCodigoContingencia) {
		this.consecutivoCodigoContingencia = consecutivoCodigoContingencia;
	}
	public Integer getConsecutivoCodigoMorbilidad() {
		return consecutivoCodigoMorbilidad;
	}
	public void setConsecutivoCodigoMorbilidad(Integer consecutivoCodigoMorbilidad) {
		this.consecutivoCodigoMorbilidad = consecutivoCodigoMorbilidad;
	}
	public String getCodigoMorbilidad() {
		return codigoMorbilidad;
	}
	public void setCodigoMorbilidad(String codigoMorbilidad) {
		this.codigoMorbilidad = codigoMorbilidad;
	}
	public Integer getNumeroDiasPromedio() {
		return numeroDiasPromedio;
	}
	public void setNumeroDiasPromedio(Integer numeroDiasPromedio) {
		this.numeroDiasPromedio = numeroDiasPromedio;
	}
	public Integer getConsecutivoCodigoRecobro() {
		return consecutivoCodigoRecobro;
	}
	public void setConsecutivoCodigoRecobro(Integer consecutivoCodigoRecobro) {
		this.consecutivoCodigoRecobro = consecutivoCodigoRecobro;
	}
	public GeneroVO getGeneroVO() {
		return generoVO;
	}
	public void setGeneroVO(GeneroVO generoVO) {
		this.generoVO = generoVO;
	}
}
