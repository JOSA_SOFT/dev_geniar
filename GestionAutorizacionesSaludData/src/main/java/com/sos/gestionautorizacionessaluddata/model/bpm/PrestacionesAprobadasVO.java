package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;
import java.util.Date;


/**
 * Class PrestacionesAprobadasDTO
 * Clase DTO para almacenar la consulta de Prestaciones Aprobadas
 * @author ing. Rafael Cano
 * @version 11/02/2016
 *
 */

public class PrestacionesAprobadasVO implements Serializable {
	private static final long serialVersionUID = 2586272436711140976L;
	private String numeroSolicitud;
	private Integer conscodigoPrestacion;
	private Integer consTipoPrestacion;
	private String codigoTipoPrestacion;
	private String tipoPrestacion;
	private String codigoPrestacion;
	private String descripcion;
	private String planAfiliado;
	private String descPlanAfiliado; 
	private String observaciones;
	private String lateralidad;
	private String descLateralidad; 
	private String viaAcceso;
	private String descViaAcceso; 
	private String direccionamiento;
	private String descDireccionamiento; 
	private String usuarioGestiona;
	private Date fechaGestion;
	private Integer consEstadoPrestacion;
	private String estadoPrestacion;
	private String atel;
	private String requiereAuditoria;
	private String cantidad;
	private String consPosologiaCUMS;
	private String posologiaCUMS;
	private String cada;
	private String frecuencia;
	private String recobro;
	private String conscodigoPrestacionGSA;
	private Integer cantidadDosis;
	private Integer consecutivoCodigoDosis;
	private Integer consecutivoFrecuencia;
	private Integer consecutivoMotivo;
	private boolean atelCheck;
	private boolean verDetalle;
	private String consecutivoPrestador;
	private Integer consecutivoCiudad;
	private String requiereCotizacion;
	private Integer consLateralidad;
	private String requiereOtraGestionAuditoria;
	private Integer consecutivoRecobro;	
	private Integer consecutivoCausaNoCobroCuota;

	
	/**
	 * @return the consecutivoPrestador
	 */
	public String getConsecutivoPrestador() {
		return consecutivoPrestador;
	}

	/**
	 * @param consecutivoPrestador the consecutivoPrestador to set
	 */
	public void setConsecutivoPrestador(String consecutivoPrestador) {
		this.consecutivoPrestador = consecutivoPrestador;
	}

	/**
	 * @return the numeroSolicitud
	 */
	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	/**
	 * @param numeroSolicitud the numeroSolicitud to set
	 */
	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	/**
	 * @return the conscodigoPrestacion
	 */
	public Integer getConscodigoPrestacion() {
		return conscodigoPrestacion;
	}

	/**
	 * @param conscodigoPrestacion the conscodigoPrestacion to set
	 */
	public void setConscodigoPrestacion(Integer conscodigoPrestacion) {
		this.conscodigoPrestacion = conscodigoPrestacion;
	}

	/**
	 * @return the codigoTipoPrestacion
	 */
	public String getCodigoTipoPrestacion() {
		return codigoTipoPrestacion;
	}

	/**
	 * @param codigoTipoPrestacion the codigoTipoPrestacion to set
	 */
	public void setCodigoTipoPrestacion(String codigoTipoPrestacion) {
		this.codigoTipoPrestacion = codigoTipoPrestacion;
	}

	/**
	 * @return the tipoPrestacion
	 */
	public String getTipoPrestacion() {
		return tipoPrestacion;
	}

	/**
	 * @param tipoPrestacion the tipoPrestacion to set
	 */
	public void setTipoPrestacion(String tipoPrestacion) {
		this.tipoPrestacion = tipoPrestacion;
	}

	/**
	 * @return the codigoPrestacion
	 */
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	/**
	 * @param codigoPrestacion the codigoPrestacion to set
	 */
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the planAfiliado
	 */
	public String getPlanAfiliado() {
		return planAfiliado;
	}

	/**
	 * @param planAfiliado the planAfiliado to set
	 */
	public void setPlanAfiliado(String planAfiliado) {
		this.planAfiliado = planAfiliado;
	}

	/**
	 * @return the descPlanAfiliado
	 */
	public String getDescPlanAfiliado() {
		return descPlanAfiliado;
	}

	/**
	 * @param descPlanAfiliado the descPlanAfiliado to set
	 */
	public void setDescPlanAfiliado(String descPlanAfiliado) {
		this.descPlanAfiliado = descPlanAfiliado;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the lateralidad
	 */
	public String getLateralidad() {
		return lateralidad;
	}

	/**
	 * @param lateralidad the lateralidad to set
	 */
	public void setLateralidad(String lateralidad) {
		this.lateralidad = lateralidad;
	}

	/**
	 * @return the descLateralidad
	 */
	public String getDescLateralidad() {
		return descLateralidad;
	}

	/**
	 * @param descLateralidad the descLateralidad to set
	 */
	public void setDescLateralidad(String descLateralidad) {
		this.descLateralidad = descLateralidad;
	}

	/**
	 * @return the viaAcceso
	 */
	public String getViaAcceso() {
		return viaAcceso;
	}

	/**
	 * @param viaAcceso the viaAcceso to set
	 */
	public void setViaAcceso(String viaAcceso) {
		this.viaAcceso = viaAcceso;
	}

	/**
	 * @return the descViaAcceso
	 */
	public String getDescViaAcceso() {
		return descViaAcceso;
	}

	/**
	 * @param descViaAcceso the descViaAcceso to set
	 */
	public void setDescViaAcceso(String descViaAcceso) {
		this.descViaAcceso = descViaAcceso;
	}

	/**
	 * @return the direccionamiento
	 */
	public String getDireccionamiento() {
		return direccionamiento;
	}

	/**
	 * @param direccionamiento the direccionamiento to set
	 */
	public void setDireccionamiento(String direccionamiento) {
		this.direccionamiento = direccionamiento;
	}

	/**
	 * @return the descDireccionamiento
	 */
	public String getDescDireccionamiento() {
		return descDireccionamiento;
	}

	/**
	 * @param descDireccionamiento the descDireccionamiento to set
	 */
	public void setDescDireccionamiento(String descDireccionamiento) {
		this.descDireccionamiento = descDireccionamiento;
	}

	/**
	 * @return the usuarioGestiona
	 */
	public String getUsuarioGestiona() {
		return usuarioGestiona;
	}

	/**
	 * @param usuarioGestiona the usuarioGestiona to set
	 */
	public void setUsuarioGestiona(String usuarioGestiona) {
		this.usuarioGestiona = usuarioGestiona;
	}

	/**
	 * @return the fechaGestion
	 */
	public Date getFechaGestion() {
		return fechaGestion;
	}

	/**
	 * @param fechaGestion the fechaGestion to set
	 */
	public void setFechaGestion(Date fechaGestion) {
		this.fechaGestion = fechaGestion;
	}

	/**
	 * @return the estadoPrestacion
	 */
	public String getEstadoPrestacion() {
		return estadoPrestacion;
	}


	/**
	 * @param estadoPrestacion the estadoPrestacion to set
	 */
	public void setEstadoPrestacion(String estadoPrestacion) {
		this.estadoPrestacion = estadoPrestacion;
	}

	/**
	 * @return the atel
	 */
	public String getAtel() {
		return atel;
	}

	/**
	 * @param atel the atel to set
	 */
	public void setAtel(String atel) {
		this.atel = atel;
	}

	/**
	 * @return the requiereAuditoria
	 */
	public String getRequiereAuditoria() {
		return requiereAuditoria;
	}

	/**
	 * @param requiereAuditoria the requiereAuditoria to set
	 */
	public void setRequiereAuditoria(String requiereAuditoria) {
		this.requiereAuditoria = requiereAuditoria;
	}

	/**
	 * @return the cantidad
	 */
	public String getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the consPosologiaCUMS
	 */
	public String getConsPosologiaCUMS() {
		return consPosologiaCUMS;
	}

	/**
	 * @param consPosologiaCUMS the consPosologiaCUMS to set
	 */
	public void setConsPosologiaCUMS(String consPosologiaCUMS) {
		this.consPosologiaCUMS = consPosologiaCUMS;
	}

	/**
	 * @return the posologiaCUMS
	 */
	public String getPosologiaCUMS() {
		return posologiaCUMS;
	}

	/**
	 * @param posologiaCUMS the posologiaCUMS to set
	 */
	public void setPosologiaCUMS(String posologiaCUMS) {
		this.posologiaCUMS = posologiaCUMS;
	}

	/**
	 * @return the cada
	 */
	public String getCada() {
		return cada;
	}

	/**
	 * @param cada the cada to set
	 */
	public void setCada(String cada) {
		this.cada = cada;
	}

	/**
	 * @return the frecuencia
	 */
	public String getFrecuencia() {
		return frecuencia;
	}

	/**
	 * @param frecuencia the frecuencia to set
	 */
	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	/**
	 * @return the recobro
	 */
	public String getRecobro() {
		return recobro;
	}

	/**
	 * @param recobro the recobro to set
	 */
	public void setRecobro(String recobro) {
		this.recobro = recobro;
	}

	/**
	 * @return the conscodigoPrestacionGSA
	 */
	public String getConscodigoPrestacionGSA() {
		return conscodigoPrestacionGSA;
	}

	/**
	 * @param conscodigoPrestacionGSA the conscodigoPrestacionGSA to set
	 */
	public void setConscodigoPrestacionGSA(String conscodigoPrestacionGSA) {
		this.conscodigoPrestacionGSA = conscodigoPrestacionGSA;
	}

	/**
	 * @return the cantidadDosis
	 */
	public Integer getCantidadDosis() {
		return cantidadDosis;
	}

	/**
	 * @param cantidadDosis the cantidadDosis to set
	 */
	public void setCantidadDosis(Integer cantidadDosis) {
		this.cantidadDosis = cantidadDosis;
	}

	/**
	 * @return the consecutivoCodigoDosis
	 */
	public Integer getConsecutivoCodigoDosis() {
		return consecutivoCodigoDosis;
	}

	/**
	 * @param consecutivoCodigoDosis the consecutivoCodigoDosis to set
	 */
	public void setConsecutivoCodigoDosis(Integer consecutivoCodigoDosis) {
		this.consecutivoCodigoDosis = consecutivoCodigoDosis;
	}

	/**
	 * @return the consecutivoFrecuencia
	 */
	public Integer getConsecutivoFrecuencia() {
		return consecutivoFrecuencia;
	}

	/**
	 * @param consecutivoFrecuencia the consecutivoFrecuencia to set
	 */
	public void setConsecutivoFrecuencia(Integer consecutivoFrecuencia) {
		this.consecutivoFrecuencia = consecutivoFrecuencia;
	}

	/**
	 * @return the consecutivoMotivo
	 */
	public Integer getConsecutivoMotivo() {
		return consecutivoMotivo;
	}

	/**
	 * @param consecutivoMotivo the consecutivoMotivo to set
	 */
	public void setConsecutivoMotivo(Integer consecutivoMotivo) {
		this.consecutivoMotivo = consecutivoMotivo;
	}

	/**
	 * @return the verDetalle
	 */
	public boolean isVerDetalle() {
		return verDetalle;
	}

	/**
	 * @param verDetalle the verDetalle to set
	 */
	public void setVerDetalle(boolean verDetalle) {
		this.verDetalle = verDetalle;
	}

	/**
	 * @param atelCheck
	 */
	public boolean getAtelCheck() {
		return atelCheck;
	}

	/**
	 * @param atelCheck the atelCheck to set
	 */
	public void setAtelCheck(boolean atelCheck) {
		this.atelCheck = atelCheck;
	}

	/**
	 * @return the consecutivoCiudad
	 */
	public Integer getConsecutivoCiudad() {
		return consecutivoCiudad;
	}

	/**
	 * @param consecutivoCiudad the consecutivoCiudad to set
	 */
	public void setConsecutivoCiudad(Integer consecutivoCiudad) {
		this.consecutivoCiudad = consecutivoCiudad;
	}

	/**
	 * @return the requiereCotizacion
	 */
	public String getRequiereCotizacion() {
		return requiereCotizacion;
	}

	/**
	 * @param requiereCotizacion the requiereCotizacion to set
	 */
	public void setRequiereCotizacion(String requiereCotizacion) {
		this.requiereCotizacion = requiereCotizacion;
	}

	public Integer getConsTipoPrestacion() {
		return consTipoPrestacion;
	}

	public void setConsTipoPrestacion(Integer consTipoPrestacion) {
		this.consTipoPrestacion = consTipoPrestacion;
	}

	public Integer getConsEstadoPrestacion() {
		return consEstadoPrestacion;
	}

	public void setConsEstadoPrestacion(Integer consEstadoPrestacion) {
		this.consEstadoPrestacion = consEstadoPrestacion;
	}

	public Integer getConsLateralidad() {
		return consLateralidad;
	}

	public void setConsLateralidad(Integer consLateralidad) {
		this.consLateralidad = consLateralidad;
	}

	public String getRequiereOtraGestionAuditoria() {
		return requiereOtraGestionAuditoria;
	}

	public void setRequiereOtraGestionAuditoria(String requiereOtraGestionAuditoria) {
		this.requiereOtraGestionAuditoria = requiereOtraGestionAuditoria;
	}
	
	public Integer getConsecutivoRecobro() {
		return consecutivoRecobro;
	}

	public void setConsecutivoRecobro(Integer consecutivoRecobro) {
		this.consecutivoRecobro = consecutivoRecobro;
	}

	public Integer getConsecutivoCausaNoCobroCuota() {
		return consecutivoCausaNoCobroCuota;
	}

	public void setConsecutivoCausaNoCobroCuota(Integer consecutivoCausaNoCobroCuota) {
		this.consecutivoCausaNoCobroCuota = consecutivoCausaNoCobroCuota;
	}
	
}
