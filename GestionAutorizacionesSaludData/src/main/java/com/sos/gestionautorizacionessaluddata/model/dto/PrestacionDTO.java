package com.sos.gestionautorizacionessaluddata.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.EstadoVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.malla.MarcaInconsistenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;

/**
 * Class PrestacionDTO Clase DTO con la informacion de las prestaciones
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/02/2016
 *
 */
public class PrestacionDTO implements Serializable {
	private static final long serialVersionUID = -6591598309605224989L;
	private TipoCodificacionVO tipoCodificacionVO;
	private String codigoCodificacionPrestacion;
	private String descripcionCodificacionPrestacion;
	private Integer cantidad;
	private String indicadorNoPOS;
	private DosisVO dosisVO;
	private int consecutivoItemPresupuesto;
	private ConcentracionVO concentracionVO;
	private PresentacionVO presentacionVO;
	private MedicamentosVO medicamentosVO;
	private ProcedimientosVO procedimientosVO;
	private String marcaCalidad;
	private String estadoPrestacion;
	private Date fechaEstimadaEntrega;
	private Date fechaEntregaModificada;
	private String numeroRadicacionSolicitud;

	private Integer consecutivoEstado;

	private Integer consecutivoServicioSolicitado;

	private Integer consecutivoCiudad;
	private Integer consecutivoPlan;

	private boolean validacionConceptoCero;

	/* Identifica la lateralidad del procedimiento */
	private LateralidadesVO lateralidadesVO;

	/* Identifica la via de acceso del procedimiento */
	private ViaAccesoVO viaAccesoVO;

	/* Identifica el consecutivo de la prestacion */
	private Integer consecutivoCodificacionPrestacion;

	/** informacion del estado de la solicitud */
	private EstadoVO estadoVO;

	/** informacion de lsita de las marcas de la inconsistencias */
	private List<MarcaInconsistenciaVO> lMarcaInconsistencia;

	/** informacion de inconsistencas que retorna el servcio de malla */
	private List<InconsistenciasResponseServiceVO> lInconsistenciasResponseServiceVO;

	private boolean habilitaDescarga;

	private boolean habilitaDatosAutorizacion;
	private boolean habilitaGestionDatosAutorizacion;

	/** Informacion del recobro */
	private int consecutivoRecobro;
	private String descripcionRecobro;
	private String codigoRecobro;

	private boolean habilitarCambiarRecobro;

	private boolean noCobroCuotaRecuperacion;
	private Integer consecutivoCausaNoCobroCuota;
	private boolean habilitarAnulacionPrestacion;
	private boolean cambiarDireccionamiento;
	
	private String descripcionCausaNoCobro;

	/**
	 * @return the isHabilitaGestionDatosAutorizacion
	 */
	public boolean isHabilitaGestionDatosAutorizacion() {
		return habilitaGestionDatosAutorizacion;
	}

	/**
	 * @param habilitaGestionDatosAutorizacion
	 *            the habilitaGestionDatosAutorizacion to set
	 */
	public void setHabilitaGestionDatosAutorizacion(
			boolean habilitaGestionDatosAutorizacion) {
		this.habilitaGestionDatosAutorizacion = habilitaGestionDatosAutorizacion;
	}

	/**
	 * @return the isHabilitaDatosAutorizacion
	 */
	public boolean isHabilitaDatosAutorizacion() {
		return habilitaDatosAutorizacion;
	}

	/**
	 * @param habilitaDatosAutorizacion
	 *            the habilitaDatosAutorizacion to set
	 */
	public void setHabilitaDatosAutorizacion(boolean habilitaDatosAutorizacion) {
		this.habilitaDatosAutorizacion = habilitaDatosAutorizacion;
	}

	/**
	 * @return the habilitaDescarga
	 */
	public boolean isHabilitaDescarga() {
		return habilitaDescarga;
	}

	/**
	 * @param habilitaDescarga
	 *            the habilitaDescarga to set
	 */
	public void setHabilitaDescarga(boolean habilitaDescarga) {
		this.habilitaDescarga = habilitaDescarga;
	}

	public TipoCodificacionVO getTipoCodificacionVO() {
		return tipoCodificacionVO;
	}

	public void setTipoCodificacionVO(TipoCodificacionVO tipoCodificacionVO) {
		this.tipoCodificacionVO = tipoCodificacionVO;
	}

	public String getCodigoCodificacionPrestacion() {
		return codigoCodificacionPrestacion;
	}

	public void setCodigoCodificacionPrestacion(
			String codigoCodificacionPrestacion) {
		this.codigoCodificacionPrestacion = codigoCodificacionPrestacion;
	}

	public String getDescripcionCodificacionPrestacion() {
		return descripcionCodificacionPrestacion;
	}

	public void setDescripcionCodificacionPrestacion(
			String descripcionCodificacionPrestacion) {
		this.descripcionCodificacionPrestacion = descripcionCodificacionPrestacion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getIndicadorNoPOS() {
		return indicadorNoPOS;
	}

	public void setIndicadorNoPOS(String indicadorNoPOS) {
		this.indicadorNoPOS = indicadorNoPOS;
	}

	public DosisVO getDosisVO() {
		return dosisVO;
	}

	public void setDosisVO(DosisVO dosisVO) {
		this.dosisVO = dosisVO;
	}

	public ConcentracionVO getConcentracionVO() {
		return concentracionVO;
	}

	public void setConcentracionVO(ConcentracionVO concentracionVO) {
		this.concentracionVO = concentracionVO;
	}

	public PresentacionVO getPresentacionVO() {
		return presentacionVO;
	}

	public void setPresentacionVO(PresentacionVO presentacionVO) {
		this.presentacionVO = presentacionVO;
	}

	public MedicamentosVO getMedicamentosVO() {
		return medicamentosVO;
	}

	public void setMedicamentosVO(MedicamentosVO medicamentosVO) {
		this.medicamentosVO = medicamentosVO;
	}

	public ProcedimientosVO getProcedimientosVO() {
		return procedimientosVO;
	}

	public void setProcedimientosVO(ProcedimientosVO procedimientosVO) {
		this.procedimientosVO = procedimientosVO;
	}

	public LateralidadesVO getLateralidadesVO() {
		return lateralidadesVO;
	}

	public void setLateralidadesVO(LateralidadesVO lateralidadesVO) {
		this.lateralidadesVO = lateralidadesVO;
	}

	public ViaAccesoVO getViaAccesoVO() {
		return viaAccesoVO;
	}

	public void setViaAccesoVO(ViaAccesoVO viaAccesoVO) {
		this.viaAccesoVO = viaAccesoVO;
	}

	public int getConsecutivoItemPresupuesto() {
		return consecutivoItemPresupuesto;
	}

	public void setConsecutivoItemPresupuesto(int consecutivoItemPresupuesto) {
		this.consecutivoItemPresupuesto = consecutivoItemPresupuesto;
	}

	public Integer getConsecutivoCodificacionPrestacion() {
		return consecutivoCodificacionPrestacion;
	}

	public void setConsecutivoCodificacionPrestacion(
			Integer consecutivoCodificacionPrestacion) {
		this.consecutivoCodificacionPrestacion = consecutivoCodificacionPrestacion;
	}

	public EstadoVO getEstadoVO() {
		return estadoVO;
	}

	public void setEstadoVO(EstadoVO estadoVO) {
		this.estadoVO = estadoVO;
	}

	public List<MarcaInconsistenciaVO> getlMarcaInconsistencia() {
		return lMarcaInconsistencia;
	}

	public void setlMarcaInconsistencia(
			List<MarcaInconsistenciaVO> lMarcaInconsistencia) {
		this.lMarcaInconsistencia = lMarcaInconsistencia;
	}

	public String getMarcaCalidad() {
		return marcaCalidad;
	}

	public void setMarcaCalidad(String marcaCalidad) {
		this.marcaCalidad = marcaCalidad;
	}

	public String getEstadoPrestacion() {
		return estadoPrestacion;
	}

	public void setEstadoPrestacion(String estadoPrestacion) {
		this.estadoPrestacion = estadoPrestacion;
	}

	/**
	 * @return the lInconsistenciasResponseServiceVO
	 */
	public List<InconsistenciasResponseServiceVO> getlInconsistenciasResponseServiceVO() {
		return lInconsistenciasResponseServiceVO;
	}

	/**
	 * @param lInconsistenciasResponseServiceVO
	 *            the lInconsistenciasResponseServiceVO to set
	 */
	public void setlInconsistenciasResponseServiceVO(
			List<InconsistenciasResponseServiceVO> lInconsistenciasResponseServiceVO) {
		this.lInconsistenciasResponseServiceVO = lInconsistenciasResponseServiceVO;
	}

	/**
	 * @return the fechaEstimadaEntrega
	 */
	public Date getFechaEstimadaEntrega() {
		return fechaEstimadaEntrega;
	}

	/**
	 * @param fechaEstimadaEntrega
	 *            the fechaEstimadaEntrega to set
	 */
	public void setFechaEstimadaEntrega(Date fechaEstimadaEntrega) {
		this.fechaEstimadaEntrega = fechaEstimadaEntrega;
	}

	/**
	 * @return the fechaEntregaModificada
	 */
	public Date getFechaEntregaModificada() {
		return fechaEntregaModificada;
	}

	/**
	 * @param fechaEntregaModificada
	 *            the fechaEntregaModificada to set
	 */
	public void setFechaEntregaModificada(Date fechaEntregaModificada) {
		this.fechaEntregaModificada = fechaEntregaModificada;
	}

	/**
	 * @return the numeroRadicacionSolicitud
	 */
	public String getNumeroRadicacionSolicitud() {
		return numeroRadicacionSolicitud;
	}

	/**
	 * @param numeroRadicacionSolicitud
	 *            the numeroRadicacionSolicitud to set
	 */
	public void setNumeroRadicacionSolicitud(String numeroRadicacionSolicitud) {
		this.numeroRadicacionSolicitud = numeroRadicacionSolicitud;
	}

	public Integer getConsecutivoServicioSolicitado() {
		return consecutivoServicioSolicitado;
	}

	public void setConsecutivoServicioSolicitado(
			Integer consecutivoServicioSolicitado) {
		this.consecutivoServicioSolicitado = consecutivoServicioSolicitado;
	}

	/**
	 * @return the consecutivoCiudad
	 */
	public Integer getConsecutivoCiudad() {
		return consecutivoCiudad;
	}

	/**
	 * @param consecutivoCiudad
	 *            the consecutivoCiudad to set
	 */
	public void setConsecutivoCiudad(Integer consecutivoCiudad) {
		this.consecutivoCiudad = consecutivoCiudad;
	}

	/**
	 * @return the consecutivoPlan
	 */
	public Integer getConsecutivoPlan() {
		return consecutivoPlan;
	}

	/**
	 * @param consecutivoPlan
	 *            the consecutivoPlan to set
	 */
	public void setConsecutivoPlan(Integer consecutivoPlan) {
		this.consecutivoPlan = consecutivoPlan;
	}

	/**
	 * @return the consecutivoEstado
	 */
	public Integer getConsecutivoEstado() {
		return consecutivoEstado;
	}

	/**
	 * @param consecutivoEstado
	 *            the consecutivoEstado to set
	 */
	public void setConsecutivoEstado(Integer consecutivoEstado) {
		this.consecutivoEstado = consecutivoEstado;
	}

	public boolean isValidacionConceptoCero() {
		return validacionConceptoCero;
	}

	public void setValidacionConceptoCero(boolean validacionConceptoCero) {
		this.validacionConceptoCero = validacionConceptoCero;
	}

	public int getConsecutivoRecobro() {
		return consecutivoRecobro;
	}

	public void setConsecutivoRecobro(int consecutivoRecobro) {
		this.consecutivoRecobro = consecutivoRecobro;
	}

	public String getDescripcionRecobro() {
		return descripcionRecobro;
	}

	public void setDescripcionRecobro(String descripcionRecobro) {
		this.descripcionRecobro = descripcionRecobro;
	}

	public String getCodigoRecobro() {
		return codigoRecobro;
	}

	public void setCodigoRecobro(String codigoRecobro) {
		this.codigoRecobro = codigoRecobro;
	}

	public boolean isHabilitarCambiarRecobro() {
		return habilitarCambiarRecobro;
	}

	public void setHabilitarCambiarRecobro(boolean habilitarCambiarRecobro) {
		this.habilitarCambiarRecobro = habilitarCambiarRecobro;
	}

	public boolean isNoCobroCuotaRecuperacion() {
		return noCobroCuotaRecuperacion;
	}

	public void setNoCobroCuotaRecuperacion(boolean noCobroCuotaRecuperacion) {
		this.noCobroCuotaRecuperacion = noCobroCuotaRecuperacion;
	}

	public Integer getConsecutivoCausaNoCobroCuota() {
		return consecutivoCausaNoCobroCuota;
	}

	public void setConsecutivoCausaNoCobroCuota(
			Integer consecutivoCausaNoCobroCuota) {
		this.consecutivoCausaNoCobroCuota = consecutivoCausaNoCobroCuota;
	}

	public boolean isHabilitarAnulacionPrestacion() {
		return habilitarAnulacionPrestacion;
	}

	public void setHabilitarAnulacionPrestacion(
			boolean habilitarAnulacionPrestacion) {
		this.habilitarAnulacionPrestacion = habilitarAnulacionPrestacion;
	}

	public boolean isCambiarDireccionamiento() {
		return cambiarDireccionamiento;
	}

	public void setCambiarDireccionamiento(boolean cambiarDireccionamiento) {
		this.cambiarDireccionamiento = cambiarDireccionamiento;
	}

	public String getDescripcionCausaNoCobro() {
		return descripcionCausaNoCobro;
	}

	public void setDescripcionCausaNoCobro(String descripcionCausaNoCobro) {
		this.descripcionCausaNoCobro = descripcionCausaNoCobro;
	}

}