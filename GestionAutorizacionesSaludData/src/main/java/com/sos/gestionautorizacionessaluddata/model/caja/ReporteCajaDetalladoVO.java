package com.sos.gestionautorizacionessaluddata.model.caja;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que almacena el resultado del reporte caja detallado
 * @author GENIAR
 *
 */
public class ReporteCajaDetalladoVO implements Serializable {
	private static final long serialVersionUID = 1626102877120806603L;
	private Date fechaGeneracion;
	private int numeroRecibo;
	private Double valorTotalRecibo;
	private String descripcionOficina;
	private String descripcionSede;
	private String numeroOPS;
	private String usuario;
	private String afiliado;
	
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public int getNumeroRecibo() {
		return numeroRecibo;
	}
	public void setNumeroRecibo(int numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	public Double getValorTotalRecibo() {
		return valorTotalRecibo;
	}
	public void setValorTotalRecibo(Double valorTotalRecibo) {
		this.valorTotalRecibo = valorTotalRecibo;
	}
	public String getDescripcionOficina() {
		return descripcionOficina;
	}
	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}
	public String getDescripcionSede() {
		return descripcionSede;
	}
	public void setDescripcionSede(String descripcionSede) {
		this.descripcionSede = descripcionSede;
	}
	public String getNumeroOPS() {
		return numeroOPS;
	}
	public void setNumeroOPS(String numeroOPS) {
		this.numeroOPS = numeroOPS;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getAfiliado() {
		return afiliado;
	}
	public void setAfiliado(String afiliado) {
		this.afiliado = afiliado;
	}
}
