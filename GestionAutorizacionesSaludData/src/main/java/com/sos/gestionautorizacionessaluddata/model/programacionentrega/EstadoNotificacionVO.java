package com.sos.gestionautorizacionessaluddata.model.programacionentrega;

import java.io.Serializable;

/**
 * Class EstadoNotificacionVO
 * Clase VO de la estados notificacion
 * @author Julian Hernandez
 * @version 05/07/2016
 *
 */
public class EstadoNotificacionVO implements Serializable{
	private static final long serialVersionUID = 4094700577857718400L;

	/**Identifica la descripcion **/
	private String descripcion;
	
	/** Identifica el consecutivo **/
	private Integer consCodEstadoNotificacion;
	
	/** Identifica el codigo **/
	private String codEstadoNotificacion;

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the consCodEstadoNotificacion
	 */
	public Integer getConsCodEstadoNotificacion() {
		return consCodEstadoNotificacion;
	}

	/**
	 * @param consCodEstadoNotificacion the consCodEstadoNotificacion to set
	 */
	public void setConsCodEstadoNotificacion(Integer consCodEstadoNotificacion) {
		this.consCodEstadoNotificacion = consCodEstadoNotificacion;
	}

	/**
	 * @return the codEstadoNotificacion
	 */
	public String getCodEstadoNotificacion() {
		return codEstadoNotificacion;
	}

	/**
	 * @param codEstadoNotificacion the codEstadoNotificacion to set
	 */
	public void setCodEstadoNotificacion(String codEstadoNotificacion) {
		this.codEstadoNotificacion = codEstadoNotificacion;
	}
	
	
	
}
