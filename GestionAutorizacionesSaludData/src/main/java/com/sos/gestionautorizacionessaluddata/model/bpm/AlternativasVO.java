package com.sos.gestionautorizacionessaluddata.model.bpm;

import java.io.Serializable;

/**
 * Class CausasDTO Clase DTO para almacenar la consulta de las alternativas
 * 
 * @author ing. Rafael Cano
 * @version 02/03/2016
 *
 */

public class AlternativasVO implements Serializable {
	private static final long serialVersionUID = 5960210605260648947L;
	private String alternativa;
	private String codigoAlternativa;

	public AlternativasVO() {
		super();
	}

	public AlternativasVO(String alternativa, String codigoAlternativa) {
		super();
		this.alternativa = alternativa;
		this.codigoAlternativa = codigoAlternativa;
	}

	public String getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}

	public String getCodigoAlternativa() {
		return codigoAlternativa;
	}

	public void setCodigoAlternativa(String codigoAlternativa) {
		this.codigoAlternativa = codigoAlternativa;
	}
}