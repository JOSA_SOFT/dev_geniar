package com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica;

import java.io.Serializable;

/**
 * DTO de la información tiempos de entrega
 * 
 * @author geniarjos
 * @since 14/03/2017
 */
public class TiempoEntregaDTO implements Serializable{
	private static final long serialVersionUID = 7216805962474583570L;
	private String plan;
	private String itemDePresupuesto;
	private String codigoGrupo;
	private String descripcionGrupo;
	private Integer tiempodeEntrega;

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getItemDePresupuesto() {
		return itemDePresupuesto;
	}

	public void setItemDePresupuesto(String itemDePresupuesto) {
		this.itemDePresupuesto = itemDePresupuesto;
	}

	public String getCodigoGrupo() {
		return codigoGrupo;
	}

	public void setCodigoGrupo(String codigoGrupo) {
		this.codigoGrupo = codigoGrupo;
	}

	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}

	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}

	public Integer getTiempodeEntrega() {
		return tiempodeEntrega;
	}

	public void setTiempodeEntrega(Integer tiempodeEntrega) {
		this.tiempodeEntrega = tiempodeEntrega;
	}

}
