package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;

/**
 * Class DatosSolicitudVO Clase VO que contiene los datos de la solicitud
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 19/03/2016
 *
 */
public class DatosSolicitudVO extends SolicitudBpmVO implements Serializable {
	private static final long serialVersionUID = 5609674905174289915L;

	/** identifica la informacion del afiliado */
	private AfiliadoVO afiliadoVO;

	/**
	 * identifica la concatenacion del tipo de documento del afiliado y el
	 * nombre
	 */
	private String identificacionNombreAfiliado;

	/** identifica las prestaciones */
	private PrestacionDTO prestacionDTO;

	/** identifica el listado de prestaciones */
	private List<PrestacionDTO> lPrestaciones;

	/** identifica la fecha de vencimiento gestion */
	private Date fechaVencimientoGestion;
	
	/**
	 * identifica si el dato de la solicitud es par para diferenciarse de la
	 * proxima solicitud
	 */
	private boolean par;

	/** identifica el nombre de la tarea */
	private String nombreTarea;

	/** identifica el usuario de la tarea */
	private String usuarioTarea;

	/** identifica el nombre del grupo de riesgo */
	private String nombreGrupoRiesgo;

	/** numero de prestaciones */
	private int numeroPrestaciones;
	
	/** identifica el numero unico de OPS */
	private Integer numeroUnicoAutorizacion;	
	
	/** identifica el estado de la OPS */
	private Integer consecutivoEstadoAutorizacion;
	
	/** identifica el estado de la OPS */
	private String descripcionEstadoAutorizacion;
	
	/** identifica la fecha de vencimiento de la ops */
	private Date fechaVencimientoAutorizacion;
	
	/** identifica la cantidad de descargas */
	private int cantidadDescarga;
	
	/** identifica el id de la instancia del proceso BPM */
	private Integer idInstanciaProcesoBPM;
	
	/** identifica el id de la consecutivoEstadoSolicitud */
	private Integer consecutivoEstadoSolicitud;

	
	private String seleccionada;
	
	public void setNumeroPrestaciones(int numeroPrestaciones) {
		this.numeroPrestaciones = numeroPrestaciones;
	}

	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public String getIdentificacionNombreAfiliado() {
		return identificacionNombreAfiliado;
	}

	public void setIdentificacionNombreAfiliado(String identificacionNombreAfiliado) {
		this.identificacionNombreAfiliado = identificacionNombreAfiliado;
	}

	public List<PrestacionDTO> getlPrestaciones() {
		return lPrestaciones;
	}

	public void setlPrestaciones(List<PrestacionDTO> lPrestaciones) {
		this.lPrestaciones = lPrestaciones;
	}

	public PrestacionDTO getPrestacionDTO() {
		return prestacionDTO;
	}

	public void setPrestacionDTO(PrestacionDTO prestacionDTO) {
		this.prestacionDTO = prestacionDTO;
	}

	public Date getFechaVencimientoGestion() {
		return fechaVencimientoGestion;
	}

	public void setFechaVencimientoGestion(Date fechaVencimientoGestion) {
		this.fechaVencimientoGestion = fechaVencimientoGestion;
	}

	public boolean isPar() {
		return par;
	}

	public void setPar(boolean par) {
		this.par = par;
	}

	public String getNombreTarea() {
		return nombreTarea;
	}

	public void setNombreTarea(String nombreTarea) {
		this.nombreTarea = nombreTarea;
	}

	public String getUsuarioTarea() {
		return usuarioTarea;
	}

	public void setUsuarioTarea(String usuarioTarea) {
		this.usuarioTarea = usuarioTarea;
	}

	public String getNombreGrupoRiesgo() {
		return nombreGrupoRiesgo;
	}

	public void setNombreGrupoRiesgo(String nombreGrupoRiesgo) {
		this.nombreGrupoRiesgo = nombreGrupoRiesgo;
	}

	/**
	 * @return the numeroPrestaciones
	 */
	public int getNumeroPrestaciones() {
		if (lPrestaciones != null) {
			numeroPrestaciones = lPrestaciones.size();
			numeroPrestaciones++;
			return numeroPrestaciones;
		}
		return numeroPrestaciones;
	}
	
	public Integer getNumeroUnicoAutorizacion() {
		return numeroUnicoAutorizacion;
	}

	public void setNumeroUnicoAutorizacion(Integer numeroUnicoAutorizacion) {
		this.numeroUnicoAutorizacion = numeroUnicoAutorizacion;
	}

	public Integer getConsecutivoEstadoAutorizacion() {
		return consecutivoEstadoAutorizacion;
	}

	public void setConsecutivoEstadoAutorizacion(
			Integer consecutivoEstadoAutorizacion) {
		this.consecutivoEstadoAutorizacion = consecutivoEstadoAutorizacion;
	}

	public String getDescripcionEstadoAutorizacion() {
		return descripcionEstadoAutorizacion;
	}

	public void setDescripcionEstadoAutorizacion(
			String descripcionEstadoAutorizacion) {
		this.descripcionEstadoAutorizacion = descripcionEstadoAutorizacion;
	}
	
	public Date getFechaVencimientoAutorizacion() {
		return fechaVencimientoAutorizacion;
	}

	public void setFechaVencimientoAutorizacion(Date fechaVencimientoAutorizacion) {
		this.fechaVencimientoAutorizacion = fechaVencimientoAutorizacion;
	}
	
	public int getCantidadDescarga() {
		return cantidadDescarga;
	}

	public void setCantidadDescarga(int cantidadDescarga) {
		this.cantidadDescarga = cantidadDescarga;
	}

	/**
	 * @return the idInstanciaProcesoBPM
	 */
	public Integer getIdInstanciaProcesoBPM() {
		return idInstanciaProcesoBPM;
	}

	/**
	 * @param idInstanciaProcesoBPM the idInstanciaProcesoBPM to set
	 */
	public void setIdInstanciaProcesoBPM(Integer idInstanciaProcesoBPM) {
		this.idInstanciaProcesoBPM = idInstanciaProcesoBPM;
	}

	/**
	 * @return the consecutivoEstadoSolicitud
	 */
	public Integer getConsecutivoEstadoSolicitud() {
		return consecutivoEstadoSolicitud;
	}

	/**
	 * @param consecutivoEstadoSolicitud the consecutivoEstadoSolicitud to set
	 */
	public void setConsecutivoEstadoSolicitud(Integer consecutivoEstadoSolicitud) {
		this.consecutivoEstadoSolicitud = consecutivoEstadoSolicitud;
	}

	public String getSeleccionada() {
		return seleccionada;
	}

	public void setSeleccionada(String seleccionada) {
		this.seleccionada = seleccionada;
	}
	
}
