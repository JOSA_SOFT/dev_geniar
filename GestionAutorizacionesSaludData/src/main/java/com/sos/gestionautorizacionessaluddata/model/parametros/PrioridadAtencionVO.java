package com.sos.gestionautorizacionessaluddata.model.parametros;

import java.io.Serializable;

/**
 * Class PrioridadAtencionVO
 * Clase VO de la prioridad de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/12/2015
 */
public class PrioridadAtencionVO implements Serializable{
	private static final long serialVersionUID = 810398091078346896L;

	/*Consecutivo de la prioridad de la atencion*/
	private Integer consecutivoPrioridadAtencion;
	
	/*descripcion de la prioridad de la atencion*/
	private String descripcionPrioridadAtencion;
	
	public Integer getConsecutivoPrioridadAtencion() {
		return consecutivoPrioridadAtencion;
	}

	public void setConsecutivoPrioridadAtencion(Integer consecutivoPrioridadAtencion) {
		this.consecutivoPrioridadAtencion = consecutivoPrioridadAtencion;
	}

	public String getDescripcionPrioridadAtencion() {
		return descripcionPrioridadAtencion;
	}

	public void setDescripcionPrioridadAtencion(String descripcionPrioridadAtencion) {
		this.descripcionPrioridadAtencion = descripcionPrioridadAtencion;
	}	
}
