package com.sos.gestionautorizacionessaluddata.model.consultasolicitud;

import java.io.Serializable;

import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;

/**
 * Class DetalleSolicitudVO
 * Clase VO que contiene el detalle de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 19/03/2016
 *
 */
public class DetalleSolicitudVO extends SolicitudBpmVO implements Serializable{
	private static final long serialVersionUID = 5374981028476375525L;

	/*Identifica la descripcion del medio de contacto*/
	private String descripcionMedioContacto;
	
	/** identifica el numero de solicitud del proveedor*/
	private String numeroSolicitudProveedor;	

	/** identifica la informacion del afiliado*/
	private AfiliadoVO afiliadoVO;	
	
	/*Descripcion del plan*/
	private String descripcionPlan;
	
	/*Descripcion de la ubicacion paciente*/
	private String descripcionUbicacionPaciente;
	
	/*descripcion de la prioridad de la atencion*/
	private String descripcionPrioridadAtencion;
	
	/*Identifica la descripcion del recobro.*/
	private String descripcionRecobro;
	
	/*Identifica si se supera el tope SOAT.*/
	private String superaTopeSoat;
	
	/*Identifica la informacion de la hospitalizacion*/
	private HospitalizacionVO hospitalizacionVO;
	
	public String getDescripcionMedioContacto() {
		return descripcionMedioContacto;
	}

	public void setDescripcionMedioContacto(String descripcionMedioContacto) {
		this.descripcionMedioContacto = descripcionMedioContacto;
	}
	
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public String getDescripcionPlan() {
		return descripcionPlan;
	}

	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}

	public String getDescripcionUbicacionPaciente() {
		return descripcionUbicacionPaciente;
	}

	public void setDescripcionUbicacionPaciente(String descripcionUbicacionPaciente) {
		this.descripcionUbicacionPaciente = descripcionUbicacionPaciente;
	}

	public String getDescripcionPrioridadAtencion() {
		return descripcionPrioridadAtencion;
	}

	public void setDescripcionPrioridadAtencion(String descripcionPrioridadAtencion) {
		this.descripcionPrioridadAtencion = descripcionPrioridadAtencion;
	}

	public String getDescripcionRecobro() {
		return descripcionRecobro;
	}

	public void setDescripcionRecobro(String descripcionRecobro) {
		this.descripcionRecobro = descripcionRecobro;
	}

	public String getSuperaTopeSoat() {
		return superaTopeSoat;
	}

	public void setSuperaTopeSoat(String superaTopeSoat) {
		this.superaTopeSoat = superaTopeSoat;
	}

	public HospitalizacionVO getHospitalizacionVO() {
		return hospitalizacionVO;
	}

	public void setHospitalizacionVO(HospitalizacionVO hospitalizacionVO) {
		this.hospitalizacionVO = hospitalizacionVO;
	}
	
	public String getNumeroSolicitudProveedor() {
		return numeroSolicitudProveedor;
	}

	public void setNumeroSolicitudProveedor(String numeroSolicitudProveedor) {
		this.numeroSolicitudProveedor = numeroSolicitudProveedor;
	}

}
