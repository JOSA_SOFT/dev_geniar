use BDAfiliacionValidador

Go
GRANT SELECT ON bdAfiliacionValidador.dbo.tbPlanes to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbPlanes_Vigencias to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposIdentificacion to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbDepartamentos to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbDepartamentos_vigencias to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbContratosValidador to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbCiudades to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbCiudades_vigencias to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbOficinas to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbOficinas_Vigencias to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbParentescos to autsalud_rol
GRANT SELECT ON bdafiliacionvalidador.dbo.tbParentescos_Vigencias to autsalud_rol
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposPlan TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposPlan_Vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbBeneficiariosValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbContratosValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.TbMatrizDerechosValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbCausasDerechoValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbEstadosDerechoValidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbSexos TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbSexos_Vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposAfiliado  TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposAfiliado_Vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbIpsPrimarias_vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbmatrizcapitacionvalidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbEscenarios_procesovalidador TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbModeloConveniosCapitacion TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbdetModeloConveniosCapitacion TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbMarcasProductosxSucursal TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbMarcasEspeciales TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbMarcasEspeciales_Vigencias TO autSalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposContrato TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposContrato_Vigencias TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbMarcasProductosxSucursal TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbCobranzasValidador TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbAportanteValidador TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbTiposUnidades TO autsalud_rol;
GRANT SELECT ON bdAfiliacionValidador.dbo.tbEapb TO autsalud_rol;

Go

USE bdSisalud
Go

GRANT SELECT ON bdSisalud.dbo.tbMaestroincapacidades TO autsalud_rol;
GRANT SELECT ON bdSisalud.dbo.tbdetalleincapacidad TO autsalud_rol;
GRANT SELECT ON bdSisalud.dbo.tbReglas_Vigencias TO autsalud_rol
go

USE bdServicioCliente
Go

GRANT SELECT ON bdServicioCliente.dbo.tbContactos TO autsalud_rol