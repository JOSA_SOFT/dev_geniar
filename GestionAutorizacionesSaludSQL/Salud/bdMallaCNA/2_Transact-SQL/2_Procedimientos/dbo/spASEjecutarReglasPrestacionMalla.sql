USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASEjecutarReglasPrestacionMalla]    Script Date: 04/05/2017 11:00:55 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASEjecutarReglasPrestacionMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga ejecutar las reglas de Malla de Gestión de D\>
  					 <\D Solicitudes     D\>    
* Observaciones    : <\O      O\>    
* Parametros       : <\P P\>    
* Variables        : <\V       V\>    
* Fecha Creacion   : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramorez AM\>    
* Descripcion        : <\D Se agrega llamado al sp spAsValidarExepcionesFrecuenciaEntrega despues de 
						   ejecutar las reglas. D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/03/21 FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASEjecutarReglasPrestacionMalla]  @cnsctvo_prcso udtConsecutivo,
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@cnsctvo_cdgo_mlla_vldcn udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 

  BEGIN

    /*obtener las reglas de la malla */
    DECLARE @ldfechaActual datetime,
            @cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
            @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo,
            @jrrqa_rgla_vldcn udtConsecutivo,
            @nmbre_sp_vldcn udtNombreObjeto,
            @prmtro_sp_vldcn char(10),
            @instruccionSQL nvarchar(4000),
            @mensajeError varchar(1000),
            @dscrpcn_rgla varchar(500),
            @cnsctvo_lg udtConsecutivo,
            @max_cnsctvo udtConsecutivo,
            @min_cnsctvo udtConsecutivo,
            @negacion char(1),
            @procesoReglaValidacion numeric(1)

    SET @ldfechaActual = GETDATE()
    SET @procesoReglaValidacion = 1
    SET @negacion = 'N'

    /*creacion de tablas temporales*/
    DECLARE @tbMVReglasValidacion TABLE (
      cnsctvo udtConsecutivo IDENTITY (1, 1),
      cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
      cdgo_rgla_vldcn udtCodigo,
      dscrpcn_rgla_vldcn udtDescripcion,
      cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo,
      jrrqa_rgla_vldcn udtConsecutivo,
      cnsctvo_cdgo_cncpto_vlddo udtConsecutivo,
      nmbre_sp_vldcn udtNombreObjeto,
      prmtro_sp_vldcn char(10)
    )

    INSERT @tbMVReglasValidacion (cnsctvo_cdgo_rgla_vldcn,
    cdgo_rgla_vldcn,
    dscrpcn_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    jrrqa_rgla_vldcn,
    cnsctvo_cdgo_cncpto_vlddo,
    nmbre_sp_vldcn,
    prmtro_sp_vldcn)
      SELECT
        r.cnsctvo_cdgo_rgla_vldcn,
        r.cdgo_rgla_vldcn,
        r.dscrpcn_rgla_vldcn,
        r.cnsctvo_cdgo_no_cnfrmdd_vldcn,
        r.jrrqa_rgla_vldcn,
        r.cnsctvo_cdgo_cncpto_vlddo,
        r.nmbre_sp_vldcn,
        r.prmtro_sp_vldcn
      FROM bdMallaCNA.dbo.tbMallasValidacion_Vigencias m
      INNER JOIN bdMallaCNA.dbo.tbGruposReglasValidacion_Vigencias g WITH(NOLOCK)
        ON m.cnsctvo_cdgo_mlla_vldcn = g.cnsctvo_cdgo_mlla_vldcn
      INNER JOIN bdMallaCNA.dbo.tbReglasValidacion_Vigencias r WITH(NOLOCK)
        ON r.cnsctvo_cdgo_grpo_rgla_vldcn = g.cnsctvo_cdgo_grpo_rgla_vldcn
      WHERE m.cnsctvo_cdgo_mlla_vldcn = @cnsctvo_cdgo_mlla_vldcn
      AND @ldfechaActual BETWEEN m.inco_vgnca AND m.fn_vgnca
      AND @ldfechaActual BETWEEN g.inco_vgnca AND g.fn_vgnca
      AND @ldfechaActual BETWEEN r.inco_vgnca AND r.fn_vgnca

    SELECT
      @max_cnsctvo = MAX(cnsctvo)
    FROM @tbMVReglasValidacion
    SET @min_cnsctvo = 1

    IF @es_btch = @negacion
    BEGIN

      WHILE (@min_cnsctvo <= @max_cnsctvo)
      BEGIN

        SELECT
          @cnsctvo_cdgo_rgla_vldcn = cnsctvo_cdgo_rgla_vldcn,
          @cnsctvo_cdgo_no_cnfrmdd_vldcn = cnsctvo_cdgo_no_cnfrmdd_vldcn,
          @jrrqa_rgla_vldcn = jrrqa_rgla_vldcn,
          @nmbre_sp_vldcn = nmbre_sp_vldcn,
          @prmtro_sp_vldcn = ISNULL(prmtro_sp_vldcn, 'null')
        FROM @tbMVReglasValidacion
        WHERE cnsctvo = @min_cnsctvo

        /*ejecutar reglas de validacion*/

        SET @instruccionSQL = 'Exec dbo.' + LTRIM(RTRIM(@nmbre_sp_vldcn)) + ' ' +
        '''' + LTRIM(RTRIM(@prmtro_sp_vldcn)) + '''' + ',' +
        LTRIM(RTRIM(CONVERT(char, @cnsctvo_cdgo_rgla_vldcn))) + ',' +
        LTRIM(RTRIM(CONVERT(char, @cnsctvo_cdgo_no_cnfrmdd_vldcn)))
        EXECUTE sp_executesql @instruccionSQL

        SET @min_cnsctvo = @min_cnsctvo + 1

      END

	  Exec bdMAllaCNA.dbo.spAsValidarExcepcionesFrecuenciaEntrega
    END
    ELSE
    BEGIN

      WHILE (@min_cnsctvo <= @max_cnsctvo)
      BEGIN

        SELECT
          @cnsctvo_cdgo_rgla_vldcn = cnsctvo_cdgo_rgla_vldcn,
          @cnsctvo_cdgo_no_cnfrmdd_vldcn = cnsctvo_cdgo_no_cnfrmdd_vldcn,
          @jrrqa_rgla_vldcn = jrrqa_rgla_vldcn,
          @nmbre_sp_vldcn = nmbre_sp_vldcn,
          @prmtro_sp_vldcn = ISNULL(prmtro_sp_vldcn, 'null')
        FROM @tbMVReglasValidacion
        WHERE cnsctvo = @min_cnsctvo

      /*ejecutar reglas de validacion*/
      BEGIN TRY
        SET @dscrpcn_rgla = 'Regla Malla Validacion : ' + @nmbre_sp_vldcn
        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso	  @cnsctvo_prcso,
																  @cnsctvo_cdgo_tpo_prcso,
																  @dscrpcn_rgla,
																  @procesoReglaValidacion,
																  @cnsctvo_lg OUTPUT

        SET @instruccionSQL = 'Exec dbo.' + LTRIM(RTRIM(@nmbre_sp_vldcn)) + ' ' +
        '''' + LTRIM(RTRIM(@prmtro_sp_vldcn)) + '''' + ',' +
        LTRIM(RTRIM(CONVERT(char, @cnsctvo_cdgo_rgla_vldcn))) + ',' +
        LTRIM(RTRIM(CONVERT(char, @cnsctvo_cdgo_no_cnfrmdd_vldcn)))
        --print @instruccionSQL
        EXECUTE sp_executesql @instruccionSQL
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
      END TRY
      BEGIN CATCH
        SET @mensajeError = @nmbre_sp_vldcn + ' : ' + ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

        SET @min_cnsctvo = @min_cnsctvo + 1
      END

    END

  END
