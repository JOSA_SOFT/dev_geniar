USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCalcularResultadoMalla]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCalcularResultadoMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de calcular el resultado de las validaciones con D\>
					 <\D base en el peso de las reglas. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCalcularResultadoMalla] 
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 

  DECLARE @afirmacion char(1)
  SET @afirmacion = 'S'

  BEGIN

    /*tabla tempoaral para calcular la mayor jerarquia para la regla con cual se debe reportar la no conformidad*/
    DECLARE @tbCalculoJeraquiaReglaAsignada TABLE (
      llve_prmra_rgstro_vlddo udtConsecutivo,
      llve_prmra_cncpto_prncpl_vlddo udtConsecutivo,
      cnsctvo_cdgo_grpo_rgla_vldcn udtConsecutivo,
      jrqia_rgla_asgndo udtConsecutivo
    )

    /*tabla tempoaral para calcular los estados de la solicitud*/
    DECLARE @tbCalculoJerarquiaEstadoSolicitudAsignada TABLE (
      llve_prmra_rgstro_vlddo udtConsecutivo,
      llve_prmra_cncpto_prncpl_vlddo udtConsecutivo,
      jrrqa_estdo_no_cnfrmdd_asgndo udtConsecutivo
    )

    INSERT #tbCalculoSolicitudNoConformidades (llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    cnsctvo_cdgo_rgla_vldcn,
    jrrqa_rgla_vldcn,
    cnsctvo_cdgo_grpo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    cnsctvo_cdgo_estdo_no_cnfrmdd,
    jrrqa_estdo_no_cnfrmdd)
      SELECT DISTINCT --Se realiza el distinct dado que es posible que por datos se generen dos veces la misma no conformidad para una prestación.
        r.llve_prmra_rgstro_vlddo,
        r.llve_prmra_cncpto_prncpl_vlddo,
        r.cnsctvo_cdgo_rgla_vldcn,
        x.jrrqa_rgla_vldcn,
        x.cnsctvo_cdgo_grpo_rgla_vldcn,
        r.cnsctvo_cdgo_no_cnfrmdd_vldcn,
        d.cnsctvo_cdgo_estdo_no_cnfrmdd,
        e.jrrqa_estdo_no_cnfrmdd
      FROM #tbResultadosValidaciones r WITH (NOLOCK)
      INNER JOIN bdMallaCNA.dbo.tbReglasValidacion_Vigencias x WITH (NOLOCK)
        ON r.cnsctvo_cdgo_rgla_vldcn = x.cnsctvo_cdgo_rgla_vldcn
      INNER JOIN bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias d WITH (NOLOCK)
        ON r.cnsctvo_cdgo_no_cnfrmdd_vldcn = d.cnsctvo_cdgo_no_cnfrmdd_vldcn
      INNER JOIN bdMallaCNA.dbo.tbEstadosNoConformidades_vigencias e WITH (NOLOCK)
        ON e.cnsctvo_cdgo_estdo_no_cnfrmdd = d.cnsctvo_cdgo_estdo_no_cnfrmdd;

    /*calculo de jerarquia de regla*/
    INSERT @tbCalculoJeraquiaReglaAsignada (llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    cnsctvo_cdgo_grpo_rgla_vldcn,
    jrqia_rgla_asgndo)
      SELECT
        llve_prmra_rgstro_vlddo,
        llve_prmra_cncpto_prncpl_vlddo,
        cnsctvo_cdgo_grpo_rgla_vldcn,
        MIN(jrrqa_rgla_vldcn) AS jrqia_rgla_asgndo
      FROM #tbCalculoSolicitudNoConformidades
      GROUP BY llve_prmra_rgstro_vlddo,
               llve_prmra_cncpto_prncpl_vlddo,
               cnsctvo_cdgo_grpo_rgla_vldcn

    /**/
    UPDATE #tbCalculoSolicitudNoConformidades
    SET jrqia_rgla_slccnda_no_cnfrmdd = @afirmacion
    FROM #tbCalculoSolicitudNoConformidades d1
    INNER JOIN @tbCalculoJeraquiaReglaAsignada d2
      ON d1.llve_prmra_rgstro_vlddo = d2.llve_prmra_rgstro_vlddo
      AND d1.llve_prmra_cncpto_prncpl_vlddo = d2.llve_prmra_cncpto_prncpl_vlddo
      AND d1.cnsctvo_cdgo_grpo_rgla_vldcn = d2.cnsctvo_cdgo_grpo_rgla_vldcn
    WHERE d1.jrrqa_rgla_vldcn = d2.jrqia_rgla_asgndo

    /*calculo jerrquia por estado*/
    INSERT @tbCalculoJerarquiaEstadoSolicitudAsignada (llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    jrrqa_estdo_no_cnfrmdd_asgndo)
      SELECT
        llve_prmra_rgstro_vlddo,
        llve_prmra_cncpto_prncpl_vlddo,
        MIN(jrrqa_estdo_no_cnfrmdd) AS jrrqa_estdo_no_cnfrmdd_asgndo
      FROM #tbCalculoSolicitudNoConformidades
      WHERE jrqia_rgla_slccnda_no_cnfrmdd = @afirmacion /*No conformidades marcadas */
      GROUP BY llve_prmra_rgstro_vlddo,
               llve_prmra_cncpto_prncpl_vlddo

    /* Actualización de estados reglas que apliquen para ser retornadas acorde a su jerarquía.*/
    UPDATE #tbCalculoSolicitudNoConformidades
    SET jrqia_estdo_slccnda_no_cnfrmdd = @afirmacion
    FROM #tbCalculoSolicitudNoConformidades d1
    INNER JOIN @tbCalculoJerarquiaEstadoSolicitudAsignada d2
      ON d1.llve_prmra_rgstro_vlddo = d2.llve_prmra_rgstro_vlddo
      AND d1.llve_prmra_cncpto_prncpl_vlddo = d2.llve_prmra_cncpto_prncpl_vlddo
    WHERE d1.jrrqa_estdo_no_cnfrmdd = d2.jrrqa_estdo_no_cnfrmdd_asgndo

  END

GO
