USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASGuardarResultadoMalla]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASGuardarResultadoMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de guardar el resultado de las validaciones con D\>
				  <\D base en el peso de las reglas. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASGuardarResultadoMalla] @cnsctvo_prcso udtConsecutivo,
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 
  BEGIN

    DECLARE @cnsctvo_lg int,
            @mensajeError varchar(1000),
            @usuario udtUsuario,
            @fecha datetime,
            @negacion char(1),
            @afirmacion char(1),
            @valorConstante0 numeric(1),
            @procesoGuardar numeric(1)

    SET @fecha = GETDATE()
    SET @usuario = SUBSTRING(System_User, CHARINDEX('\', System_User) + 1, LEN(System_User))
    SET @negacion = 'N'
    SET @afirmacion = 'S'
    SET @procesoGuardar = 1
    SET @valorConstante0 = 0

    IF @es_btch = @negacion

    BEGIN
		
	  DELETE		RPM
	  FROM			bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion RPM
	  INNER JOIN	#tbResultadosValidaciones RV
	  ON			RPM.llve_prmra_rgstro_vlddo = RV.llve_prmra_rgstro_vlddo;

      --Se almacena toda la información del resultado de la malla de validación.
      INSERT bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion (cnsctvo_prcso,
      llve_prmra_rgstro_vlddo,
      cnsctvo_cdgo_rgla_vldcn,
      cnsctvo_cdgo_no_cnfrmdd_vldcn,
      fcha_crcn,
      usro_crcn,
      fcha_ultma_mdfccn,
      usro_ultma_mdfccn,
      llve_prmra_cncpto_prncpl_vlddo,
      infrmcn_adcnl_no_cnfrmdd)
        SELECT DISTINCT
          @cnsctvo_prcso,
          d1.llve_prmra_rgstro_vlddo,
          d1.cnsctvo_cdgo_rgla_vldcn,
          d1.cnsctvo_cdgo_no_cnfrmdd_vldcn,
          @fecha,
          @usuario,
          @fecha,
          @usuario,
          ISNULL(d1.llve_prmra_cncpto_prncpl_vlddo, @valorConstante0),
          ISNULL(d1.infrmcn_adcnl_no_cnfrmdd, '')
        FROM #tbResultadosValidaciones d1
    END

    ELSE
    BEGIN TRY
      EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                        @cnsctvo_cdgo_tpo_prcso,
                                                        'Guardar Datos Resultado Malla Validación.',
                                                        @procesoGuardar,
                                                        @cnsctvo_lg OUTPUT

      /*guarda los datos del resultado del proceso de malla*/

      INSERT bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion (cnsctvo_prcso,
      llve_prmra_rgstro_vlddo,
      cnsctvo_cdgo_rgla_vldcn,
      cnsctvo_cdgo_no_cnfrmdd_vldcn,
      fcha_crcn,
      usro_crcn,
      fcha_ultma_mdfccn,
      usro_ultma_mdfccn,
      llve_prmra_cncpto_prncpl_vlddo,
      infrmcn_adcnl_no_cnfrmdd)
        SELECT
          @cnsctvo_prcso,
          d1.llve_prmra_rgstro_vlddo,
          d1.cnsctvo_cdgo_rgla_vldcn,
          d1.cnsctvo_cdgo_no_cnfrmdd_vldcn,
          @fecha,
          @usuario,
          @fecha,
          @usuario,
          ISNULL(d1.llve_prmra_cncpto_prncpl_vlddo, @valorConstante0),
          ISNULL(d1.infrmcn_adcnl_no_cnfrmdd, '')
        FROM #tbResultadosValidaciones d1
        INNER JOIN #tbCalculoSolicitudNoConformidades d2
          ON d1.llve_prmra_rgstro_vlddo = d2.llve_prmra_rgstro_vlddo
          AND d1.cnsctvo_cdgo_rgla_vldcn = d2.cnsctvo_cdgo_rgla_vldcn
        WHERE d2.jrqia_rgla_slccnda_no_cnfrmdd = @afirmacion /*marca conformidad a guardar*/

      EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

    END TRY
    BEGIN CATCH
      SET @mensajeError = ERROR_MESSAGE();
      EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                   @mensajeError
      RAISERROR (@mensajeError, 16, 2) WITH SETERROR

    END CATCH

  END

GO
