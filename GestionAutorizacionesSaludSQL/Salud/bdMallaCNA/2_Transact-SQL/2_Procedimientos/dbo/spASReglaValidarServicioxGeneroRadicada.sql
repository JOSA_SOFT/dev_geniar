USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarServicioxGeneroRadicada]    Script Date: 01/03/2017 17:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarServicioxGeneroRadicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si el sexo del afiliado no corresponde D\>
					 <\D al definido para la prestaciony la solicitud fue radicada en SOS D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*--------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D
                           Se realiza modificacion del procedimiento para que no tenga en cuenta el plan del
						   afiliado, y la validacion se realice para todos los planes      
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarServicioxGeneroRadicada] 
	@prmtro_sp_vldcn               varchar(30)   ,
	@cnsctvo_cdgo_rgla_vldcn       udtConsecutivo,
	@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS

BEGIN
  SET NOCOUNT ON 

  DECLARE @consecutvioSexoAmbos        udtConsecutivo,
          @negacion                    udtLogico,
          @consecutivoMedioContactoASI udtConsecutivo,
          @conscutivoTipoCups          udtConsecutivo,
		  @infrmcn_adcnl_no_cnfrmdd    Varchar(250)

  Set @consecutivoMedioContactoASI = 1;
  Set @consecutvioSexoAmbos        = 3
  Set @negacion                    = 'N'
  Set @conscutivoTipoCups          = 4
  Set @infrmcn_adcnl_no_cnfrmdd    = 'El servicio solicitado no corresponde con el género del afiliado, se debe validar el servicio ingresado.'

	 
  Insert
  Into       #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                       llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								      )
  Select     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  From       #tmpDatosPrestacionesxSolicitud c
  Inner Join #tmpDatosAfiliado a
  On         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join bdSisalud.dbo.tbCupsServicios m WITH (NOLOCK)
  On         m.cnsctvo_prstcn = c.cnsctvo_cdfccn
  Where      c.fcha_prstcn BETWEEN m.inco_vgnca AND m.fn_vgnca
  And        m.cnsctvo_cdgo_sxo             <> @consecutvioSexoAmbos
  And        m.cnsctvo_cdgo_sxo             != a.cnsctvo_cdgo_sxo
  And        c.cnsctvo_cdgo_tpo_cdfccn       = @conscutivoTipoCups --CUPS en [tbTipoCodificacion_Vigencias
  And        c.cnfrmcn_cldd                  = @negacion
  And        c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI;
End
