USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarVigenciaSolicitud]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarVigenciaSolicitud
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si la diferencia de dias entre la D\>
					 <\D fecha de solcitud y la fecha de ingreso al sistema es mayor al valor definido D\> 
					 <\D para vencimiento prestaciones.  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarVigenciaSolicitud] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  BEGIN
    DECLARE @fecha date,
            @consecutivoConsultaMedico numeric(1),
            @consecutivoConsultaMedicoEspe numeric(1),
            @consecutivoAyudaDiagnos numeric(1),
            @consecutivoAyudaDiagnosEspe numeric(1),
            @consecutivoParametroDiasConsultas numeric(2),
            @consecutivoParametroDiasAyudasDiagnosticas numeric(2)

    SET @fecha = GETDATE()
    SET @consecutivoConsultaMedico = 1
    SET @consecutivoConsultaMedicoEspe = 2
    SET @consecutivoAyudaDiagnos = 3
    SET @consecutivoAyudaDiagnosEspe = 4
    SET @consecutivoParametroDiasConsultas = 77
    SET @consecutivoParametroDiasAyudasDiagnosticas = 76

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación supera los días de vigencia no puede ser radicada.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN [bdSisalud].[dbo].tbCupsServicios s WITH (NOLOCK)
        ON (S.cnsctvo_prstcn = c.cnsctvo_cdfccn)
      INNER JOIN [bdSisalud].[dbo].tbModeloLiquidacionxConceptos l WITH (NOLOCK)
        ON (l.cnsctvo_cdgo_mdlo_lqdcn_cncpto = s.cnsctvo_cdgo_mdlo_lqdcn_cncpto
        AND l.cnsctvo_cdgo_mdlo_lqdcn_cncpto IN
        (@consecutivoConsultaMedico, 	  --'CONSULTA MEDICO GENERAL',
        @consecutivoConsultaMedicoEspe) --'CONSULTA MEDICO ESPECIALISTA') 
        AND
        c.fcha_prstcn BETWEEN s.inco_vgnca AND s.fn_vgnca)
      INNER JOIN [bdsisalud].[dbo].[tbParametrosGenerales_vigencias] v WITH (NOLOCK)
        ON (v.cnsctvo_cdgo_prmtro_gnrl = @consecutivoParametroDiasConsultas
        AND c.fcha_prstcn BETWEEN v.inco_vgnca AND v.fn_vgnca)
      WHERE (v.vlr_prmtro_nmrco < c.ds_dfrnca
      AND v.cnsctvo_cdgo_prmtro_gnrl = @consecutivoParametroDiasConsultas)
      UNION
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación supera los días de vigencia no puede ser radicada.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN [bdSisalud].[dbo].tbCupsServicios s WITH (NOLOCK)
        ON (S.cnsctvo_prstcn = c.cnsctvo_cdfccn)
      INNER JOIN [bdSisalud].[dbo].tbAgrupadoresPrestaciones d WITH (NOLOCK)
        ON (d.cnsctvo_cdgo_agrpdr_prstcn = s.cnsctvo_agrpdr_prstcn
        AND d.cnsctvo_cdgo_agrpdr_prstcn IN
        (@consecutivoAyudaDiagnos, --'AYUDAS DIAGNOSTICAS BASICAS'
        @consecutivoAyudaDiagnosEspe)) --'AYUDAS DIAGNOSTICAS ESPECIALIZADAS'
      INNER JOIN [bdsisalud].[dbo].[tbParametrosGenerales_vigencias] v WITH (NOLOCK)
        ON (v.cnsctvo_cdgo_prmtro_gnrl = @consecutivoParametroDiasAyudasDiagnosticas
        AND c.fcha_prstcn BETWEEN v.inco_vgnca AND v.fn_vgnca)
      WHERE (v.vlr_prmtro_nmrco < c.ds_dfrnca
      AND v.cnsctvo_cdgo_prmtro_gnrl = @consecutivoParametroDiasAyudasDiagnosticas);

  END

GO
