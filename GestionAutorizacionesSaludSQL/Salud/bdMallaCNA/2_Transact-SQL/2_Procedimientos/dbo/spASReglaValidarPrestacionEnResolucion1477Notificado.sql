USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionEnResolucion1477Notificado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionEnResolucion1477Notificado
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestación está asociada a D\>
					 <\D un diagnóstico incluido en la resolución 1477 o al CIE – 10 (Con nomenclatura D\>
					 <\D S, M o T) y el afiliado tiene notificacion de "ATEL" en estado D\>
					 <\D CONFIRMADO D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 20/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionEnResolucion1477Notificado] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoClasifATEP numeric(3),
          @descripcionConfirmado udtdescripcion

  SET @descripcionConfirmado = 'CONFIRMADO'
  SET @consecutivoClasifATEP = 27

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El servicio solicitado es de origen laboral, se debe gestionar con la ARL respectiva.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN bdSisalud.dbo.tbMNRiesgosxDiagnosticos m WITH (NOLOCK)
        ON (c.cnsctvo_cdgo_dgnstco = m.cnsctvo_cdgo_dgnstco)
      INNER JOIN bdCNA.sol.tbCNAPrestacionesXRiesgo_vigencias P WITH (NOLOCK)
        ON (p.cnsctvo_cdgo_rsgo_dgnstco = m.cnsctvo_cdgo_rsgo_dgnstco)
      INNER JOIN bdSisalud.dbo.tbafiliadosMarcados r WITH (NOLOCK)
        ON (r.nmro_unco_idntfccn = a.nmro_unco_idntfccn)
	  INNER JOIN bdSIsalud.dbo.tbEstadosNotificacion_vigencias NV WITH(NOLOCK)
	    ON NV.cnsctvo_cdgo_estdo_ntfccn = r.cnsctvo_cdgo_clsfccn_evnto
		AND NV.dscrpcn_estdo_ntfccn = @descripcionConfirmado
		AND c.fcha_prstcn BETWEEN NV.inco_vgnca AND NV.fn_vgnca
      WHERE r.cnsctvo_cdgo_clsfccn_evnto = @consecutivoClasifATEP
    ;
  END

GO
