USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudSolicitudes]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudSolicitudes
* Desarrollado por : <\A Jois Lombana Sánchez  A\>    
* Descripcion      : <\D Procedimiento que se encarga de actualizar en la temporal de solicitudes  D\>
  					 <\D para realizar el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 26/11/2@Valorcero15  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCargarDatosSolicitudSolicitudes] 
AS
DECLARE @Valorcero as Int
DECLARE @TipoSol as Int
DECLARE @ValorVacio as Char(1)
SELECT	@Valorcero = 0
		,@TipoSol = 2 -- Valor de la tabla bdGestionSolicitudesSalud.dbo.tbtipossolicitud
		,@ValorVacio = ''
BEGIN
	SET NOCOUNT ON 	
	/*::::::::::::::::COMPLETAR SOLICITUDES PROCESO MALLA::::::::::::::::::::::::::::::*/
	-- Insertar el consecutivo de solicitud recibida para actualizar la información
	Insert into	#Slctds 
	Select		 SOP.cnsctvo_slctd_atrzcn_srvco			--cnsctvo_slctd_srvco_sld_rcbda
				,SOP.cnsctvo_cdgo_orgn_atncn			--Origen_atencion
				,SOP.cnsctvo_cdgo_tpo_srvco_slctdo		--Tipo_Servicio
				,SOP.cnsctvo_cdgo_prrdd_atncn			--Prioridad_atencion
				,SOP.cnsctvo_cdgo_tpo_ubccn_pcnte		--Ubicacion_Paciente
				,SOP.cnsctvo_cdgo_tpo_slctd				--Tipo_Solicitud
				,SOP.cnsctvo_cdgo_mdo_cntcto_slctd		--Medio_Contacto
				,SOP.cnsctvo_cdgo_tpo_trnsccn_srvco_sld --Tipo_Transaccion
				,SOP.cnsctvo_cdgo_clse_atncn			--Clase_Atencion
				,SOP.cnsctvo_cdgo_frma_atncn			--Forma_atencion
				,SOP.fcha_slctd							--Fecha_sol
				,SOO.cdgo_orgn_atncn					--Cod_Origen_atencion
				,SOO.cdgo_tpo_srvco_slctdo				--Cod_Tipo_servicio
				,SOO.cdgo_prrdd_atncn					--Cod_Prioridad_atencion
				,SOO.cdgo_tpo_ubccn_pcnte				--Cod_Tipo_Ubicacion
				,SOO.cdgo_mdo_cntcto_slctd				--Cod_Medio_Contacto
				,SOO.cdgo_tpo_trnsccn_srvco_sld			--Cod_Tipo_Transaccion
				,SOO.cdgo_clse_atncn					--Cod_Clase_Atencion
				,SOO.cdgo_frma_atncn					--Cod_Forma_atencion
	 From		#tmpNmroSlctds											TSO WITH (NOLOCK)
	 INNER JOIN	BdCNA.gsa.tbASSolicitudesAutorizacionServicios			SOP WITH (NOLOCK)
	 on			TSO.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_atrzcn_srvco
	 inner join	BdCNA.gsa.tbASSolicitudesAutorizacionServiciosOriginal	SOO WITH (NOLOCK)
	 on			SOP.cnsctvo_slctd_atrzcn_srvco = SOO.cnsctvo_slctd_atrzcn_srvco
	
	-- Calcular cnsctvo_cdgo_orgn_atncn
	IF EXISTS (	SELECT		TSO.Origen_atencion
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Origen_atencion = @Valorcero
				AND			TSO.Cod_Origen_atencion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Origen_Atencion = Isnull(OAT.cnsctvo_cdgo_orgn_atncn,@Valorcero)
		FROM		#Slctds										SOP WITH (NOLOCK)
		Inner Join	bdSisalud.dbo.tbPMOrigenAtencion_Vigencias	OAT WITH (NOLOCK)
		on			SOP.Cod_Origen_atencion = OAT.cdgo_orgn_atncn
		WHERE		SOP.Fecha_sol Between OAT.inco_vgnca and OAT.fn_vgnca		
		And			SOP.Origen_atencion = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_tpo_srvco_slctdo
	IF EXISTS (	SELECT		TSO.Tipo_Servicio
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Tipo_Servicio = @Valorcero
				AND			TSO.Cod_Tipo_servicio<>@ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Tipo_Servicio = Isnull(TSE.cnsctvo_cdgo_tpo_srvco_slctdo,@Valorcero)
		FROM		#Slctds										SOP WITH (NOLOCK)
		inner Join	bdSisalud.dbo.tbPMTipoServicioSolicitado_Vigencias	TSE WITH (NOLOCK)
		on			SOP.Cod_Tipo_servicio = TSE.cdgo_tpo_srvco_slctdo
		WHERE		SOP.Fecha_sol Between TSE.inco_vgnca and TSE.fn_vgnca			
		And			SOP.Tipo_Servicio = @Valorcero						
	END
														
	-- Calcular cnsctvo_cdgo_prrdd_atncn
	IF EXISTS (	SELECT		TSO.Prioridad_atencion
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Prioridad_atencion = @Valorcero
				AND			TSO.Cod_Prioridad_atencion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Prioridad_Atencion = Isnull(PRA.cnsctvo_cdgo_prrdd_atncn,@Valorcero)
		FROM		#Slctds											SOP WITH (NOLOCK)
		inner Join	bdSisalud.dbo.tbPMPrioridadAtencion_Vigencias	PRA WITH (NOLOCK)
		on			SOP.Cod_Prioridad_atencion = PRA.cdgo_prrdd_atncn
		WHERE		SOP.Fecha_sol Between PRA.inco_vgnca and PRA.fn_vgnca				
		And			SOP.Prioridad_atencion = @Valorcero		
	END	

	-- Calcular cnsctvo_cdgo_tpo_ubccn_pcnte
	IF EXISTS (	SELECT		TSO.Ubicacion_Paciente
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Ubicacion_Paciente = @Valorcero
				AND			TSO.Cod_Tipo_Ubicacion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Ubicacion_Paciente = Isnull(UPA.cnsctvo_cdgo_tpo_ubccn_pcnte,@Valorcero)
		FROM		#Slctds										SOP WITH (NOLOCK)
		inner Join	bdSisalud.dbo.tbPMTipoUbicacionPaciente_Vigencias	UPA WITH (NOLOCK)
		on			SOP.Cod_Tipo_Ubicacion = UPA.cdgo_tpo_ubccn_pcnte
		WHERE		SOP.Fecha_sol Between UPA.inco_vgnca and UPA.fn_vgnca					
		And			SOP.Ubicacion_Paciente = @Valorcero					
	END

	-- Calcular cnsctvo_cdgo_tpo_slctd
	IF EXISTS (	SELECT		TSO.Tipo_Solicitud
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Tipo_Solicitud = @Valorcero)
	BEGIN 
		UPDATE		#Slctds
		SET			Tipo_Solicitud = @TipoSol 
		WHERE		Tipo_Solicitud = @Valorcero
	END	

	-- Calcular cnsctvo_cdgo_mdo_cntcto_slctd
	IF EXISTS (	SELECT		TSO.Medio_Contacto
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Medio_Contacto = @Valorcero
				AND			TSO.Cod_Medio_Contacto <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Medio_Contacto = Isnull(MCO.cnsctvo_cdgo_mdo_cntcto,@Valorcero)
		FROM		#Slctds								SOP WITH (NOLOCK)
		inner Join	BdCNA.prm.tbASMediosContacto_Vigencias		MCO WITH (NOLOCK)
		on			SOP.Cod_Medio_Contacto = MCO.cdgo_mdo_cntcto
		WHERE		SOP.Fecha_sol Between MCO.inco_vgnca and MCO.fn_vgnca				
		And			SOP.Medio_Contacto = @Valorcero					
	END
										
	-- Calcular cnsctvo_cdgo_tpo_trnsccn_srvco_sld
	IF EXISTS (	SELECT		TSO.Tipo_Transaccion
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Tipo_Transaccion = @Valorcero
				AND			TSO.Cod_Tipo_Transaccion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Tipo_Transaccion = Isnull(TSS.cnsctvo_cdgo_tpo_trnsccn_srvco_sld,@Valorcero)
		FROM		#Slctds																		SOP WITH (NOLOCK)
		inner Join	bdGestionSolicitudesSalud.dbo.tbTiposTransaccionServiciosSalud_vigencias	TSS WITH (NOLOCK) 
		on			SOP.Cod_Tipo_Transaccion = TSS.cdgo_tpo_trnsccn_srvco_sld
		WHERE		SOP.Fecha_sol Between TSS.inco_vgnca and TSS.fn_vgnca				
		And			SOP.Tipo_Transaccion = @Valorcero				
	END

	-- Calcular cnsctvo_cdgo_clse_atncn
	IF EXISTS (	SELECT		TSO.Clase_Atencion
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Clase_Atencion = @Valorcero
				AND			TSO.Cod_Clase_Atencion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Clase_Atencion = Isnull(CLA.cnsctvo_cdgo_clse_atncn,@Valorcero)
		FROM		#Slctds										SOP WITH (NOLOCK)
		inner Join	bdSisalud.dbo.tbClasesAtencion_Vigencias	CLA WITH (NOLOCK) 
		on			SOP.Cod_Clase_Atencion	 = CLA.cdgo_clse_atncn
		WHERE		SOP.Fecha_sol Between CLA.inco_vgnca and CLA.fn_vgnca				
		And			SOP.Clase_Atencion = @Valorcero				
	END
		
	-- Calcular cnsctvo_cdgo_frma_atncn
	IF EXISTS (	SELECT		TSO.Forma_Atencion
				FROM		#Slctds TSO WITH (NOLOCK)
				WHERE		TSO.Forma_Atencion = @Valorcero
				AND			TSO.Cod_Forma_atencion <> @ValorVacio)
	BEGIN 
		UPDATE		#Slctds
		SET			Forma_Atencion = Isnull(FRA.cnsctvo_cdgo_frma_atncn,@Valorcero)
		FROM		#Slctds								SOP WITH (NOLOCK)
		inner Join	bdSisalud.dbo.tbFormasAtencion_Vigencias	FRA WITH (NOLOCK)
		on			SOP.Cod_Forma_atencion = FRA.cdgo_frma_atncn
		WHERE		SOP.Fecha_sol Between FRA.inco_vgnca and FRA.fn_vgnca				
		And			SOP.Forma_Atencion = @Valorcero				
	END
		
END


GO
