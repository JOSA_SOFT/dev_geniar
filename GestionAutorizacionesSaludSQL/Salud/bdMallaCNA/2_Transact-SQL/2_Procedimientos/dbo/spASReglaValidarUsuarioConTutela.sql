USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarUsuarioConTutela]    Script Date: 26/12/2016 12:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarUsuarioConTutela
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si usuario con tutela  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D   
                           Se actualiza el procedimiento adicionando en la consulta la relacion con la
						   tabla de tutelas
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 26/12/2016  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarUsuarioConTutela] 
   @prmtro_sp_vldcn varchar(30),
   @cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
   @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS

BEGIN
  SET NOCOUNT ON 

  DECLARE @estadoNotificado          udtConsecutivo,
          @estadoConfirmado          udtConsecutivo,
          @consecutivoTutela         udtConsecutivo,
		  @infrmcn_adcnl_no_cnfrmdd  udtObservacion

  SET @estadoNotificado = 14
  SET @estadoConfirmado = 15
  SET @consecutivoTutela = 8
  Set @infrmcn_adcnl_no_cnfrmdd = 'Usuario con tutela.'
  
  Insert 
  Into   #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                   llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
							      )
  Select     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  From       #tmpDatosPrestacionesxSolicitud c
  Inner Join #tmpDatosAfiliado a
  ON         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join bdSisalud.dbo.tbafiliadosMarcados m WITH (NOLOCK)
  ON         m.nmro_unco_idntfccn = a.nmro_unco_idntfccn
  Inner Join bdSisalud.dbo.tbtutela b WITH (NOLOCK)
  On         b.cnsctvo_ntfccn     = m.cnsctvo_ntfccn And
	         b.cnsctvo_cdgo_ofcna = m.cnsctvo_cdgo_ofcna
  Where      m.cnsctvo_cdgo_clsfccn_evnto = @consecutivoTutela --Tutela
  And        m.cnsctvo_cdgo_estdo_ntfccn In (@estadoNotificado, @estadoConfirmado);
 End
