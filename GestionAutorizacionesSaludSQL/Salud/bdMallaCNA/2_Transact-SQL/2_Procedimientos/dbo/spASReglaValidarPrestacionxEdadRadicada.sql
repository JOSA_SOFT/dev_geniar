USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionxEdadRadicada]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionxEdadRadicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la edad del afiliado es mayor  D\>
					 <\D a la edad maxima permitida para la prestacion y la solicitud fue radicada en  D\>
					 <\D SOS D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionxEdadRadicada] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @negacion char(1),
          @consecutivoPlanPos numeric(1),
          @consecutivoMedioContactoASI numeric(1)

  SET @consecutivoMedioContactoASI = 1;
  SET @negacion = 'N'
  SET @consecutivoPlanPos = 1

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'La edad del afiliado supera la edad máxima permitida para la prestación del servicio, se debe validar el servicio ingresado.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN bdSisalud.dbo.tbCupsServicios m WITH (NOLOCK)
        ON (c.cnsctvo_cdfccn = m.cnsctvo_prstcn
        AND c.fcha_prstcn BETWEEN m.inco_vgnca AND m.fn_vgnca)
      WHERE m.edd_mxma < a.edd_afldo
      AND c.cnfrmcn_cldd = @negacion
      AND c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI;
  END

GO
