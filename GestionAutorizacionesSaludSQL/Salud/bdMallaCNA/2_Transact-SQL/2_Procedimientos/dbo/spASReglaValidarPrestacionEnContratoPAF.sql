USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionEnContratoPAF]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionEnContratoPAF
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si existe un PAF contratado para la D\>
					 <\D IPS primaria en la ciudad de residencia del afiiliado. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionEnContratoPAF] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @afirmacion char(1),
          @negacion char(1)
  SELECT
    @afirmacion = 'S',
    @negacion = 'N'
  BEGIN

    --Se ejecuta el SP de PAF.
    EXEC bdContratacion.dbo.spValidaPrestacionesConvenioPAFMasivo

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación asociada al PAF, se presta en la IPS primaria afilado.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosSolicitudes paf
        ON (paf.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
        AND paf.cnsctvo_cmpo_vldcn = cnsctvo_srvco_slctdo_rcbdo)
      WHERE paf.rsltdo_vldcn = @afirmacion

    --Se garantiza que se actualice a no el resultado para una próxima ejecución.
    UPDATE #tmpDatosSolicitudes
    SET rsltdo_vldcn = 'N'

  END

GO
