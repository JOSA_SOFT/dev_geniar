USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASEjecutarMallaGestionSolicitudes]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEjecutarMallaGestionSolicitudes
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento expuesto que inicia la ejecución de la malla     D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion       : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spASEjecutarMallaGestionSolicitudes] @cdgs_slctds NVARCHAR(MAX),
@usro varchar(50),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(2000) OUTPUT,
@msje_rspsta NVARCHAR(MAX) OUTPUT
AS
  SET NOCOUNT ON 

  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  DECLARE @cnsctvo_cdgo_mlla_vldcn udtConsecutivo,
          @negacion char(1),
          @codigoError char(5),
          @rspsta XML,
		  @xml XML,
		  @tipoProceso udtCodigo,
          @tipoTransaccionCrear udtLogico,
          @tipoTransaccionActualizar udtLogico,
          @estadoControl char(1),
		  @estadoOK CHAR(2) = 'OK',
		  @mnsje_ejccn VARCHAR(50) = 'Hay solicitudes que se encuetran en ejecución.',
		  @xstate int,
		  @trancount int;

  SET @cnsctvo_cdgo_mlla_vldcn = 1; --Consecutivo que ejecuta la malla de Gestión de solicitudes.
  SET @negacion = 'N';
  SET @tipoProceso = 'MA'
  SET @tipoTransaccionCrear = 'C'
  SET @tipoTransaccionActualizar = 'A'

  BEGIN
    BEGIN TRY
	  
	  SET @xml = CONVERT(XML,@cdgs_slctds);

	  EXEC dbo.spASObtenerDatosBasicos @xml

      EXEC dbo.spASMallaGestionSolicitudes @usro,
                                           @cnsctvo_cdgo_mlla_vldcn,
                                           @negacion,
										   @estdo_ejccn OUTPUT,
										   @msje_errr OUTPUT,
                                           @rspsta OUTPUT
		
      SET @msje_rspsta = CONVERT(NVARCHAR(MAX),@rspsta);

	--Control de proceso de ejecución de malla para solicitudes
	EXEC bdCNA.gsa.[spASControlProcesoSolicitud] @tipoProceso,
													@tipoTransaccionCrear,
													@usro,
													@msje_errr,
													@estadoControl OUTPUT
    END TRY
    BEGIN CATCH
      SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();
	  SET @estdo_ejccn= 'ET'

    END CATCH

	IF (OBJECT_ID('#tmpNmroSlctds') IS NOT NULL)
		DROP TABLE #tmpNmroSlctds
  END

GO
