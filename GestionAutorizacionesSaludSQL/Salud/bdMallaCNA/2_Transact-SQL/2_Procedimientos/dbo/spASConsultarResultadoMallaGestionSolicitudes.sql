USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASConsultarResultadoMallaGestionSolicitudes]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarResultadoMallaGestionSolicitudes
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento expuesto que consulta el resultado de la ejecución de la malla     D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spASConsultarResultadoMallaGestionSolicitudes] @cdgs_slctds NVARCHAR(MAX),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(2000) OUTPUT,
@msje_rspsta NVARCHAR(MAX) OUTPUT
AS
  SET NOCOUNT ON 
	BEGIN
	BEGIN TRY

  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpDatosSolicitud (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd_ss varchar(16),
    nmro_slctd_prvdr varchar(15),
    cnsctvo_prcso udtConsecutivo,
    dscrpcn_estdo_slctd udtDescripcion,
    cnsctvo_estdo_slctd udtConsecutivo
  )

  --Temporal con los datos básicos de la solicitud para control de ejecución.
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(17),
    nmro_slctd_prvdr varchar(17),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  CREATE TABLE #tbResultadosValidaciones (
    id int IDENTITY (1, 1),
    cnsctvo_prcso int,
    cnsctvo_cdgo_rgla_vldcn int,
    cnsctvo_cdgo_no_cnfrmdd_vldcn int,
    llve_prmra_rgstro_vlddo int,  --- consecutivo solicitud
    infrmcn_adcnl_no_cnfrmdd varchar(250),
    llve_prmra_cncpto_prncpl_vlddo int
  )

  CREATE TABLE #tbCalculoSolicitudNoConformidades (
    llve_prmra_rgstro_vlddo udtConsecutivo,
    llve_prmra_cncpto_prncpl_vlddo udtConsecutivo,
    cnsctvo_cdgo_grpo_rgla_vldcn udtConsecutivo,
    cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
    cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo,
    cnsctvo_cdgo_estdo_no_cnfrmdd udtConsecutivo,
    jrrqa_rgla_vldcn udtconsecutivo,
    jrrqa_estdo_no_cnfrmdd udtConsecutivo,
    jrqia_estdo_slccnda_no_cnfrmdd udtLogico DEFAULT 'N',
    jrqia_rgla_slccnda_no_cnfrmdd udtLogico DEFAULT 'N'
  )

  CREATE TABLE #tmpDatosPrestacionesxSolicitud (
    id int IDENTITY (1, 1),
    cnsctvo_srvco_slctdo_rcbdo udtConsecutivo,
    cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo,
    cnsctvo_cdfccn udtConsecutivo,
    cdgo_cdfccn varchar(15),
    cnsctvo_cdgo_tpo_cdfccn udtConsecutivo,
    cdgo_tpo_cdfccn udtCodigo,
    dscrpcn_estdo_prstcn varchar(250),
    cnsctvo_estdo_prstcn udtConsecutivo
  );

  DECLARE @negacion char(1),
          @codigoExito char(2),
          @mensajeExito char(30),
          @codigoError char(5),
          @rspsta xml,
		  @xml XML,
		  @tipoProceso udtCodigo = 'MA',
		  @en_prcso udtLogico

	  SET @negacion = 'N';
	  SET @codigoExito = 'OK';
	  SET @codigoError = 'ET';
	  SET @mensajeExito = 'Ejecución Exitosa';

	  SET @xml = CONVERT(XML, @cdgs_slctds)

	  EXEC dbo.spASCargarInformacionBasicaResultado @xml

      EXEC dbo.spASConsultarResultadoMalla @negacion,
                                           @rspsta OUTPUT

      SET @estdo_ejccn = @codigoExito
      SET @msje_errr = @mensajeExito;
      SET @msje_rspsta = CONVERT(NVARCHAR(MAX),@rspsta);

    END TRY
    BEGIN CATCH
      SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();
      SET @estdo_ejccn = @codigoError;
      SET @msje_rspsta = NULL
    END CATCH

	DROP TABLE #tmpDatosPrestacionesxSolicitud
    DROP TABLE #tbCalculoSolicitudNoConformidades
    DROP TABLE #tbResultadosValidaciones
    DROP TABLE #tmpDatosSolicitud
  END

GO
