USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASObtenerDatosGestionSolicitudMalla]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASObtenerDatosGestionSolicitudMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de calcular y cargar toda la información en las
						   tablas temporales para le ejecución del proceso de ejecutar malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 27/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASObtenerDatosGestionSolicitudMalla] 
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 
  BEGIN

    EXEC bdMallaCNA.dbo.spASCargarDatosSolicitud @cnsctvo_cdgo_tpo_prcso,
                                                 @es_btch
  END

GO
