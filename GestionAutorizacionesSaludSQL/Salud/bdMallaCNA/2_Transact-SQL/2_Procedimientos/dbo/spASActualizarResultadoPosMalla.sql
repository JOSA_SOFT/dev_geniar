USE [bdMallaCNA]
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASActualizarResultadoPosMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de actualizar el estado de las prestaciones con D\>
					 <\D base en el peso de los estados. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASActualizarResultadoPosMalla] @cnsctvo_prcso udtConsecutivo,
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 
  BEGIN
	
	CREATE TABLE #tmpestadosactualizar (
      cnsctvo_slctd_atrzcn_srvco udtconsecutivo DEFAULT 0,      -- Identificador de la solicitud
      cnsctvo_srvco_slctdo udtconsecutivo DEFAULT 0,      -- Identificador del servicio solicitado o prestación
      nmro_unco_ops udtconsecutivo DEFAULT 0,      -- Identificador de la OPS
      cnsctvo_cdgo_estdo udtconsecutivo DEFAULT 0       -- Identificador del estado a asignar
    );

    DECLARE @negacion char(1),
            @consecutivoCero numeric(1)

    SET @negacion = 'N'
    SET @consecutivoCero = 0

    IF @es_btch = @negacion
    BEGIN

	BEGIN TRY
      --Se consulta por prestación, la menor jerarquía que haya generado una no conformidad, y en caso de que no haya generado,
      --se actualiza el estado a aprobada de la prestación correspondiente.
      WITH tbMin
      AS (SELECT
        llve_prmra_cncpto_prncpl_vlddo,
        MIN(jrrqa_estdo_no_cnfrmdd) AS jrrqa_estdo_no_cnfrmdd
      FROM #tbCalculoSolicitudNoConformidades
      GROUP BY llve_prmra_cncpto_prncpl_vlddo)
      UPDATE #tmpDatosPrestacionesxSolicitud
      SET cnsctvo_estdo_prstcn = ncv.cnsctvo_cdgo_estdo_no_cnfrmdd,
          dscrpcn_estdo_prstcn = ncv.dscrpcn_estdo_no_cnfrmdd
      FROM #tmpDatosPrestacionesxSolicitud s
      INNER JOIN tbMin ce
        ON s.cnsctvo_srvco_slctdo_rcbdo = ce.llve_prmra_cncpto_prncpl_vlddo
      INNER JOIN bdMallaCNA.dbo.tbEstadosNoConformidades_vigencias ncv  WITH(NOLOCK)
        ON ncv.jrrqa_estdo_no_cnfrmdd = ce.jrrqa_estdo_no_cnfrmdd
		AND s.fcha_prstcn BETWEEN ncv.inco_vgnca AND ncv.fn_vgnca;

      --Se consulta por solicitud, la menor jerarquía que haya generado una no conformidad en alguna prestación, y en caso 
      --de que no haya generado, se actualiza el estado a aprobada de la solicitud correspondiente.
      WITH tbMin
      AS (SELECT
        llve_prmra_rgstro_vlddo,
        MIN(jrrqa_estdo_no_cnfrmdd) AS jrrqa_estdo_no_cnfrmdd
      FROM #tbCalculoSolicitudNoConformidades
      GROUP BY llve_prmra_rgstro_vlddo)
      UPDATE #tmpDatosSolicitud
      SET cnsctvo_estdo_slctd = ncv.cnsctvo_cdgo_estdo_no_cnfrmdd,
          dscrpcn_estdo_slctd = ncv.dscrpcn_estdo_no_cnfrmdd
      FROM #tmpDatosSolicitud s
      INNER JOIN tbMin ce
        ON s.cnsctvo_slctd_srvco_sld_rcbda = ce.llve_prmra_rgstro_vlddo
      INNER JOIN bdMallaCNA.dbo.tbEstadosNoConformidades_vigencias ncv  WITH(NOLOCK)
        ON ncv.jrrqa_estdo_no_cnfrmdd = ce.jrrqa_estdo_no_cnfrmdd
		AND s.fcha_slctd BETWEEN ncv.inco_vgnca AND ncv.fn_vgnca

      --Se actualiza el consecutivo del proceso.
      UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
      SET cnsctvo_prcso = @cnsctvo_prcso
      FROM bdCNA.gsa.tbASSolicitudesAutorizacionServicios sl
      INNER JOIN #tmpDatosSolicitud sol
        ON sl.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_srvco_sld_rcbda
	  
      --Se actualiza el estado de las prestaciones en la temporal, con los valores correspondientes homologados
      UPDATE sol
      SET cnsctvo_estdo_prstcn_mga = st.cnsctvo_cdgo_estdo_srvco_slctdo
      FROM #tmpDatosPrestacionesxSolicitud	sol
      LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] st WITH(NOLOCK)
        ON st.cnsctvo_cdgo_estdo_no_cnfrmdd = sol.cnsctvo_estdo_prstcn
		AND sol.fcha_prstcn BETWEEN st.inco_vgnca AND st.fn_vgnca
	
	--Temporal para procesamiento de actualizaciones
    INSERT INTO #tmpestadosactualizar (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, nmro_unco_ops, cnsctvo_cdgo_estdo)
      SELECT	0, DPS.cnsctvo_srvco_slctdo_rcbdo, 0, DPS.cnsctvo_estdo_prstcn_mga
        FROM	#tmpDatosPrestacionesxSolicitud		DPS
	
	--Ejecutar SP de actualización de estados
	EXEC bdcna.gsa.spasactualizarestadogestionsolicitudes

	  --Se consulta el estado de la solicitud definitivo en bdCNA, realizando la homologación.
      UPDATE sol
      SET cnsctvo_estdo_slctd = enc.cnsctvo_cdgo_estdo_no_cnfrmdd,
		  dscrpcn_estdo_slctd = enc.dscrpcn_estdo_no_cnfrmdd
      FROM #tmpDatosSolicitud sol 
      INNER JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios sl
        ON (sl.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_srvco_sld_rcbda)
	  LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] st WITH(NOLOCK)
        ON st.cnsctvo_cdgo_estdo_srvco_slctdo = sl.cnsctvo_cdgo_estdo_slctd
		AND sl.fcha_slctd BETWEEN st.inco_vgnca AND st.fn_vgnca
	  LEFT JOIN bdMallaCNA.[dbo].[tbEstadosNoConformidades_Vigencias] enc WITH(NOLOCK)
        ON enc.cnsctvo_cdgo_estdo_no_cnfrmdd = st.cnsctvo_cdgo_estdo_no_cnfrmdd
		AND sol.fcha_slctd BETWEEN enc.inco_vgnca AND enc.fn_vgnca
	
		DROP TABLE #tmpestadosactualizar
	END TRY
	BEGIN CATCH
		DROP TABLE #tmpestadosactualizar;
		THROW
	END CATCH
    END

  END
GO
