USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASRetornarResultadoMalla]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG     : spASRetornarResultadoMalla
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de retornar el resultado de la ejecución D\>
    			  <\D de las reglas de negocio y el cálculo obtenido. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2015  FC\>  
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez De León AM\>
* Descripcion			 : <\DM	Se adiciona condición para que devuelva las inconsistencias
								mnsje_no_cnfrmdd_ips si el medio contacto IPS de lo contrario
								devuelve mnsje_no_cnfrmdd .DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE dbo.spASRetornarResultadoMalla 
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1),
@msje_rspsta xml OUTPUT
AS
BEGIN
  SET NOCOUNT ON 

  DECLARE @negacion char(1),
		  @vacio	char(1),
		  @cnsctvo_cdgo_mdo_cntcto_slctd	udtConsecutivo,
		  @mdo_cntcto_IPS					udtConsecutivo;
		
  SET @negacion = 'N';
  SET @vacio = '';
  SET @mdo_cntcto_IPS = 2;

  

	Select top 1 @cnsctvo_cdgo_mdo_cntcto_slctd = SAS.cnsctvo_cdgo_mdo_cntcto_slctd From #tmpDatosSolicitud DS
	Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios SAS 
	On SAS.cnsctvo_slctd_atrzcn_srvco =  DS.cnsctvo_slctd_srvco_sld_rcbda;


    IF @es_btch = @negacion
    BEGIN
      SET @msje_rspsta = (SELECT
        slctd.cnsctvo_slctd_srvco_sld_rcbda AS "id_slctd",
        slctd.nmro_slctd_ss AS "nmro_slctd_ss",
        slctd.nmro_slctd_prvdr AS "nmro_slctd_prvdr",
        slctd.dscrpcn_estdo_slctd AS "estdo_slctd",
        slctd.cnsctvo_estdo_slctd AS "cnsctvo_estdo_slctd",
        (SELECT
          prstcn.cnsctvo_cdfccn AS "cnsctvo_cdfccn",
          prstcn.cdgo_cdfccn AS "cdgo_cdfccn",
          isnull(prstcn.dscrpcn_estdo_prstcn, @vacio) AS "estdo_prstcn",
          isnull(prstcn.cnsctvo_estdo_prstcn, @vacio) AS "cnsctvo_estdo_prstcn",
          '' AS "tpo_prstcn",
          prstcn.cnsctvo_cdgo_tpo_cdfccn AS "cnsctvo_tpo_prstcn",
          (SELECT
            incnstnca.dscrpcn_rgla_vldcn AS "dscrpcn_rgla_vldcn",
            incnstnca.cnsctvo_cdgo_rgla_vldcn AS "cnsctvo_cdgo_rgla",
            incnstnca.infrmcn_adcnl_no_cnfrmdd AS "infrmcn_adcnl",
            incnstnca.mnsje_no_cnfrmdd AS "mnsje_no_cnfrmdd",
            incnstnca.cnsctvo_cdgo_estdo_no_cnfrmdd AS "cnsctvo_cdgo_estdo_no_cnfrmdd",
            incnstnca.dscrpcn_estdo_no_cnfrmdd AS "dscrpcn_estdo_no_cnfrmdd",
            incnstnca.cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn AS "cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn",
            incnstnca.dscrpcn_tpo_no_cnfrmdd_vldcn AS "dscrpcn_tpo_no_cnfrmdd_vldcn",
            (SELECT
              mrca.cnsctvo_cdgo_mrca_prcso,
              mrca.dscrpcn_mrca_prcso
            FROM bdMallaCNA.dbo.tbMarcasProcesosxNoConformidadesValidacion mv WITH (NOLOCK)
            INNER JOIN bdMallaCNA.dbo.tbMarcasProcesos mrca
              ON (mv.cnsctvo_cdgo_mrca_prcso = mrca.cnsctvo_cdgo_mrca_prcso)
            WHERE mv.cnsctvo_cdgo_no_cnfrmdd_vldcn = incnstnca.cnsctvo_cdgo_no_cnfrmdd_vldcn
            FOR xml AUTO, TYPE, ELEMENTS)
            mrcs
          FROM (SELECT DISTINCT --Se realiza el distinct dado que es posible que por datos se generen dos veces la misma no conformidad para una prestación.
            vi.dscrpcn_rgla_vldcn,
            incnstncs.cnsctvo_cdgo_rgla_vldcn,
            f.infrmcn_adcnl_no_cnfrmdd,
            CASE 
				WHEN @cnsctvo_cdgo_mdo_cntcto_slctd = @mdo_cntcto_IPS THEN d.mnsje_no_cnfrmdd_ips
            ELSE 
				d.mnsje_no_cnfrmdd
			END AS mnsje_no_cnfrmdd,
			d.cnsctvo_cdgo_no_cnfrmdd_vldcn,
            e.cnsctvo_cdgo_estdo_no_cnfrmdd,
            e.dscrpcn_estdo_no_cnfrmdd,
            nc.cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn,
            nc.dscrpcn_tpo_no_cnfrmdd_vldcn
          FROM #tbCalculoSolicitudNoConformidades AS incnstncs
          LEFT JOIN #tbResultadosValidaciones f
            ON f.llve_prmra_rgstro_vlddo = incnstncs.llve_prmra_rgstro_vlddo
            AND f.llve_prmra_cncpto_prncpl_vlddo = incnstncs.llve_prmra_cncpto_prncpl_vlddo
            AND incnstncs.cnsctvo_cdgo_rgla_vldcn = f.cnsctvo_cdgo_rgla_vldcn
          LEFT JOIN bdMallaCNA.dbo.tbReglasValidacion_Vigencias vi
            ON vi.cnsctvo_cdgo_rgla_vldcn = incnstncs.cnsctvo_cdgo_rgla_vldcn
          LEFT JOIN bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias d
            ON vi.cnsctvo_cdgo_no_cnfrmdd_vldcn = d.cnsctvo_cdgo_no_cnfrmdd_vldcn
          LEFT JOIN bdMallaCNA.dbo.tbEstadosNoConformidades_vigencias e
            ON e.cnsctvo_cdgo_estdo_no_cnfrmdd = d.cnsctvo_cdgo_estdo_no_cnfrmdd
          LEFT JOIN bdMallaCNA.dbo.tbTiposNoConformidadesValidacion_vigencias nc
            ON nc.cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn = d.cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn
          WHERE incnstncs.llve_prmra_rgstro_vlddo = prstcn.cnsctvo_slctd_srvco_sld_rcbda
          AND incnstncs.llve_prmra_cncpto_prncpl_vlddo = prstcn.cnsctvo_srvco_slctdo_rcbdo) incnstnca
          FOR xml AUTO, TYPE, ELEMENTS)
          incnstncs,

          (SELECT DISTINCT
            mrca.cnsctvo_cdgo_mrca_prcso,
            mrca.dscrpcn_mrca_prcso
          FROM #tbCalculoSolicitudNoConformidades nc
          INNER JOIN bdMallaCNA.dbo.tbMarcasProcesosxNoConformidadesValidacion mv WITH (NOLOCK)
            ON (mv.cnsctvo_cdgo_no_cnfrmdd_vldcn = nc.cnsctvo_cdgo_no_cnfrmdd_vldcn)
          INNER JOIN bdMallaCNA.dbo.tbMarcasProcesos mrca
            ON (mv.cnsctvo_cdgo_mrca_prcso = mrca.cnsctvo_cdgo_mrca_prcso)
          WHERE nc.llve_prmra_rgstro_vlddo = slctd.cnsctvo_slctd_srvco_sld_rcbda
          AND nc.llve_prmra_cncpto_prncpl_vlddo = prstcn.cnsctvo_srvco_slctdo_rcbdo
          FOR xml AUTO, TYPE, ELEMENTS)
          mrcs

        FROM #tmpDatosPrestacionesxSolicitud AS prstcn
        WHERE prstcn.cnsctvo_slctd_srvco_sld_rcbda = slctd.cnsctvo_slctd_srvco_sld_rcbda
        FOR xml AUTO, TYPE, ELEMENTS)
      FROM #tmpDatosSolicitud AS slctd
      FOR xml AUTO, TYPE, ELEMENTS, ROOT ('rsltdo_mlla_vldcn'))
    END
  END

GO
