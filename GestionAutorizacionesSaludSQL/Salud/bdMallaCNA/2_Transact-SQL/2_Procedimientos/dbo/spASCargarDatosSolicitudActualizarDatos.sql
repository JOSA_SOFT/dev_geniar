USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudActualizarDatos]    Script Date: 7/10/2017 9:55:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudActualizarDatos
* Desarrollado por : <\A Jois Lombana Sánchez  A\>    
* Descripcion      : <\D Procedimiento que se encarga de actualizar en las tablas la información calculada D\>
  					 <\D para realizar el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 27/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando el prestador y el modelo de capitacion en la tabla de
						   servicios cuando se identifica que una solicitud esta capitada.
						D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-03-02 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez García AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando el prestador y el modelo de capitacion en la tabla de
						   servicios cuando se identifica que una solicitud esta capitada.
						D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-07-06 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spASCargarDatosSolicitudActualizarDatos] 
	@cnsctvo_cdgo_tpo_prcso UdtConsecutivo
AS
BEGIN
	SET NOCOUNT ON 
	
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbSolicitudesAutorizacionServicios::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	UPDATE		BdCNA.gsa.tbASSolicitudesAutorizacionServicios 
	SET			cnsctvo_cdgo_orgn_atncn				= TSO.Origen_atencion		
				,cnsctvo_cdgo_tpo_srvco_slctdo		= TSO.Tipo_Servicio			
				,cnsctvo_cdgo_prrdd_atncn			= TSO.Prioridad_atencion	
				,cnsctvo_cdgo_tpo_ubccn_pcnte		= TSO.Ubicacion_Paciente	
				,cnsctvo_cdgo_tpo_slctd				= TSO.Tipo_Solicitud		
				,cnsctvo_cdgo_mdo_cntcto_slctd		= TSO.Medio_Contacto
				,cnsctvo_cdgo_tpo_trnsccn_srvco_sld = TSO.Tipo_Transaccion
				,cnsctvo_cdgo_frma_atncn			= TSO.Forma_atencion	
				,cnsctvo_prcso_vldcn				= @cnsctvo_cdgo_tpo_prcso				
	FROM		#Slctds TSO
	inner join	BdCNA.gsa.tbASSolicitudesAutorizacionServicios	SOP WITH (NOLOCK)
	on			SOP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_srvco_sld_rcbda
	
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbInformacionAfiliadoSolicitudAutorizacionServicios:::::::::::::::::::::::::::::::::::::::::::::::*/
	UPDATE		BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios 
	SET			cnsctvo_cdgo_tpo_idntfccn_afldo = AFI.Tipo_Identificacion
				,cnsctvo_cdgo_dprtmnto_afldo	= AFI.Departamento
				,cnsctvo_cdgo_cdd_rsdnca_afldo	= AFI.Ciudad
				,cnsctvo_cdgo_cbrtra_sld		= AFI.Cobertura
				,cnsctvo_cdgo_tpo_pln			= AFI.Tipo_Plan	
				,cnsctvo_cdgo_pln				= AFI.Codigo_Plan
				,nmro_unco_idntfccn_afldo		= AFI.NUI
				,nmro_cntrto					= AFI.Numero_contrato
				,cnsctvo_cdgo_estdo_pln			= AFI.Estado_Plan
				,cnsctvo_cdgo_sxo				= AFI.Sexo
				,cnsctvo_cdgo_tpo_vnclcn_afldo	= AFI.Vinculacion
				,cnsctvo_cdgo_tpo_cntrto		= AFI.Tipo_contrato
				,cnsctvo_cdgo_rngo_slrl			= AFI.Rango_Salarial
				,cnsctvo_bnfcro_cntrto			= AFI.Beneficiario
				,cnsctvo_cdgo_estdo_drcho		= AFI.Estado_Derecho
				,cdgo_ips_prmra					= AFI.Ips
				,cnsctvo_cdgo_sde_ips_prmra		= AFI.Sede_Ips
				,cnsctvo_cdgo_sxo_rcn_ncdo		= AFI.Sexo_recien_nacido
				,cnsctvo_cdgo_chrte				= AFI.Cohorte
				,smns_ctzds						= AFI.Semanas_Cotizadas	
				,edd_afldo_ans					= AFI.Edad_Anual
				,edd_afldo_mss					= AFI.Edad_Meses
				,edd_afldo_ds					= AFI.Edad_Dias
	FROM		#Aflds AFI 
	inner join	BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios	AFP WITH (NOLOCK)
	on			AFP.cnsctvo_slctd_atrzcn_srvco = AFI.cnsctvo_slctd_srvco_sld_rcbda
			
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbIPSSolicitudesAutorizacionServicios ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/				
	UPDATE		BdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios	
	SET			cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	= IPS.Tipo_Iden_Soli
				,cnsctvo_cdgo_dprtmnto_prstdr			= IPS.Departamento
				,cnsctvo_cdgo_cdd_prstdr				= IPS.Ciudad
				,nmro_unco_idntfccn_prstdr				= IPS.NIUP
	FROM		#IPSSl IPS 
	inner join	BdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios IPP WITH (NOLOCK)
	on			IPP.cnsctvo_slctd_atrzcn_srvco = IPS.cnsctvo_slctd_srvco_sld_rcbda
	
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbInformacionHospitalariaSolicitudAutorizacionServicios ::::::::::::::::::::::::::::::::::::::::::::::::*/		
	UPDATE		BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios	
	SET			cnsctvo_cdgo_clse_hbtcn		 = THO.Clase_Habitacion
				,cnsctvo_cdgo_srvco_hsptlzcn = THO.Servicio_Hospitalizacion
	FROM		#Hsptlra THO 
	inner join	BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios	IHO WITH (NOLOCK)
	on			IHO.cnsctvo_slctd_atrzcn_srvco = THO.cnsctvo_slctd_srvco_sld_rcbda
	
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbDiagnosticosSolicitudAutorizacionServicios ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/		
	UPDATE		BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios
	SET			cnsctvo_cdgo_tpo_dgnstco = DIG.Tipo_Diagnostico
				,cnsctvo_cdgo_rcbro		 = DIG.Recobro
				,cnsctvo_cdgo_dgnstco	 = DIG.Codigo_Diagnostico
				,cnsctvo_cdgo_cntngnca   = DIG.Contingencia
	FROM		#Dgnstcs DIG 
	inner join	BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios DIA WITH (NOLOCK)
	on			DIA.cnsctvo_slctd_atrzcn_srvco = DIG.cnsctvo_slctd_srvco_sld_rcbda
	AND			DIA.cnsctvo_dgnstco_slctd_atrzcn_srvco = DIG.cnsctvo_dgnstco_slctd_atrzcn_srvco

	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbMedicoTratanteSolicitudAutorizacionServicios ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/		
	UPDATE		BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios 
	SET			cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		= MED.Tipo_Afiliacion
				,cnsctvo_cdgo_espcldd_mdco_trtnte		= MED.Especialidad
				,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	= MED.Tipo_identificacion
	FROM		#Mdcs MED 
	inner join	BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios MEI WITH (NOLOCK)
	on			MEI.cnsctvo_slctd_atrzcn_srvco = MED.cnsctvo_slctd_srvco_sld_rcbda

	
	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbServiciosSolicitados ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/		
	UPDATE		BdCNA.gsa.tbASServiciosSolicitados
	SET			cnsctvo_cdgo_srvco_slctdo			  = SER.Servicio_Solicitado       ,
				cnsctvo_cdgo_tpo_srvco				  = SER.Tipo				      ,
				cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo = SER.Unidad_Tiempo		      ,
				cnsctvo_cdgo_tpo_atrzcn				  = SER.Tipo_Autorizacion	      ,
				cnsctvo_cdgo_prstcn_prstdr			  = SER.Prestacion_Prestador      ,
				crgo_drccnmnto						  = SER.Cargo_Direccionamiento    ,
				indcdr_no_ps						  = SER.Indicador_no_Pos          ,
				prstcn_vgnte						  = SER.Prestacion_Vigente        ,
				prstcn_cptda						  = SER.Prestacion_Capitada       ,
				prgrmcn_entrga						  = SER.Programa_Entrega	      ,
				cnsctvo_cdgo_tpo_mdlo_cpta            = SER.cnsctvo_cdgo_tpo_mdlo_cpta,
				cdgo_intrno_cpta                      = SER.cdgo_intrno_cpta,
				nmro_unco_prstdr_cptdo                = SER.nmro_unco_prstdr_cptdo
	FROM		#Srvcs SER 			
	Inner Join	BdCNA.gsa.tbASServiciosSolicitados SSP WITH (NOLOCK)
	On			SSP.cnsctvo_slctd_atrzcn_srvco = SER.cnsctvo_slctd_srvco_sld_rcbda And	
	            SSP.cnsctvo_cdgo_srvco_slctdo  = SER.Servicio_solicitado

	/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbMedicamentosSolicitadosRecibidos ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/	
	UPDATE		BdCNA.gsa.tbASMedicamentosSolicitados
	SET			cnsctvo_cdgo_prsntcn_dss			= MED.Presentacion_Dosis	
				,cnsctvo_cdgo_undd_prdcdd_dss		= MED.Unidad_Periodicidad_Dosis
				,cnsctvo_cdgo_frma_frmctca			= MED.Forma_Farmaceutica	
				,cnsctvo_cdgo_grpo_trptco			= MED.Grupo					
				,cnsctvo_cdgo_prsntcn				= MED.Prestacion			
				,cnsctvo_cdgo_va_admnstrcn_mdcmnto	= MED.Administracion		
				,cnsctvo_cdgo_undd_cncntrcn_dss		= MED.Concentracion_Unidad
				,fcha_vncmnto_invma					= MED.Fecha_Venc_Invima
				,mstra_mdca							= MED.Muestra_medica
				,uso_cndcndo						= MED.Uso_Condicionado
				,mss_mxmo_rnvcn						= Meses_Max_Renovacion
	FROM		#Mdcmnts MED 
	Inner Join	BdCNA.gsa.tbASServiciosSolicitados SER WITH (NOLOCK)
	On			SER.cnsctvo_slctd_atrzcn_srvco = MED.cnsctvo_slctd_srvco_sld_rcbda 
	Inner Join	BdCNA.gsa.tbASMedicamentosSolicitados	MSP WITH (NOLOCK)
	On			MSP.cnsctvo_srvco_slctdo = MED.cnsctvo_srvco_slctdo_rcbdo
	
		/*::::::::::::::::::::::::::::::::::::: ACTUALIZAR BdCNA.gsa.tbProcedimientosInsumosSolicitados ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/	
	UPDATE		BdCNA.gsa.tbASProcedimientosInsumosSolicitados
	SET			 cnsctvo_cdgo_ltrldd	= INS.Lateralidad
				,cnsctvo_cdgo_va_accso  = INS.Via_Acceso
				,cntdd_mxma				= INS.Cantidad_Max_Permitida
				,undd_cntdd				= INS.Unidad_cantidad
				,frcnca_entrga			= INS.Frecuencia_Entrega
				,accso_drcto			= INS.Acceso_Directo
	FROM		#Insms INS 
	Inner Join	BdCNA.gsa.tbASServiciosSolicitados SER WITH (NOLOCK)
	On			SER.cnsctvo_slctd_atrzcn_srvco = INS.cnsctvo_slctd_srvco_sld_rcbda 
	Inner Join	BdCNA.gsa.tbASProcedimientosInsumosSolicitados	PIP WITH (NOLOCK)
	On			PIP.cnsctvo_srvco_slctdo = INS.cnsctvo_srvco_slctdo_rcbdo	
END
