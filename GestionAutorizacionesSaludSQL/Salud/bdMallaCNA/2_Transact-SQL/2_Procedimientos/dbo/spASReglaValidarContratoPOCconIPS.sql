USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarContratoPOCconIPS]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarContratoPOCconIPS
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el afiliado que realiza la D\>
					 <\D solicitud se encuentra vigente a la fecha de solicitud en un contrato POC  D\>
					 <\D vigente a la fecha de solicitud y que en este mismo POC se encuentren  D\>
					 <\D asociados al plan, diagnostico, prestacion y tipo de atención de la solicitud. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarContratoPOCconIPS] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  DECLARE @afirmacion char(1),
          @negacion char(1)
  SELECT
    @afirmacion = 'S',
    @negacion = 'N'
  BEGIN

    --Se ejecuta el SP de POC.
    EXEC bdContratacion.dbo.SpValidaPrestacionesConvenioPOCMasivo

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Usuario en costo prospectivo.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosSolicitudes paf
        ON (paf.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
        AND paf.cnsctvo_cmpo_vldcn = cnsctvo_srvco_slctdo_rcbdo)
      WHERE paf.rsltdo_vldcn = @afirmacion

    --Se garantiza que se actualice a no el resultado para una próxima ejecución.
    UPDATE #tmpDatosSolicitudes
    SET rsltdo_vldcn = 'N'

  END

GO
