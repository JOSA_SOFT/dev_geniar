USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarCantidadMaximaxPrestacionxSolicitudNoRadicada]    Script Date: 19/09/2016 1:21:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarCantidadMaximaxPrestacionxSolicitudNoRadicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar la Cantidad maxima permitda por D\>
				  <\D prestacion en la solicitud, no fue radicada en SOS D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jorge Rodriguez AM\>    
* Descripcion        : <\D  Se actualiza la regla para que valide la cantidad máximo permitida y 
							no la cantidad máxima													D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  04-10-2016 FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].spASReglaValidarCantidadMaximaxPrestacionxSolicitudNoRadicada @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoMedioContactoASI numeric(1),
          @consecutivocups numeric(1),
          @negacion char(1)

  SET @consecutivoMedioContactoASI = 1;
  SET @consecutivocups = 4
  SET @negacion = 'N'

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'La cantidad ingresada supera la cantidad máxima permitida de ' + CAST(c.cntdd_mxma_prmtda_x_slctd AS varchar) + ' servicios a pedir en una solicitud'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      WHERE c.cnfrmcn_cldd = @negacion
      AND c.cnsctvo_cdgo_mdo_cntcto_slctd <> @consecutivoMedioContactoASI -- Diferente a radicada en SOS (ventanilla)
      AND c.cnsctvo_cdgo_tpo_cdfccn = @consecutivocups --CUPS en [tbTipoCodificacion_Vigencias
      AND c.cntdd_slctda > c.cntdd_mxma_prmtda_x_slctd -- Determinar en la vida o al año.
  END

GO
