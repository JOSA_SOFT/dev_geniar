USE [bdMallaCNA]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('dbo.spAsValidarExcepcionesFrecuenciaEntrega') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE dbo.spAsValidarExcepcionesFrecuenciaEntrega AS SELECT 1;'
END

GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spAsValidarExepcionesFrecuenciaEntrega
* Desarrollado por : <\A Ing. Carlos Andres lopez Ramirez A\>    
* Descripcion      : <\D Proceso que se encarga de realizar validaciones de prestaciones con agrupadores
					     de tutela, cancer, vih y ambulancias y retira el resultado de validacion de 
						 frecuencia de entrega, reglas 22, 27, 28, 29 30, 31, 32 y 33, para que en estos
						 casos la prestacion no sea devuelta.  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2017/03/18 FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER Procedure [dbo].[spAsValidarExcepcionesFrecuenciaEntrega]
As
Begin
	Declare @cnsctvo_cdgo_rgla_vldcn22			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn27			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn28			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn29			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn30			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn31			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn32			UdtConsecutivo,
			@cnsctvo_cdgo_rgla_vldcn33			UdtConsecutivo,
			@fecha_actual						Date,
			@cnsctvo_cdgo_agrpdr_prstcn			udtConsecutivo,
			@cnsctvo_cdgo_agrpdr_prstcn_n		udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto			udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto_c		udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto_v		udtConsecutivo;


	Create Table  #tmpFrecuenciaEntrega
	(
		cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo, 
	    cnsctvo_srvco_slctdo_rcbdo     udtConsecutivo,
		nmro_unco_idntfccn             Varchar(23)   ,
		fcha_slctd_orgn                Datetime
	);

	Create Table  #tmpEstadosFrecuenciaEntrega
	(
		cnsctvo_cdgo_estdo_ntfccn		udtConsecutivo
	);

	Create Table #tmpPrestacionesExepcionFrecuenciaEntrega
	(
		cnsctvo_srvco_slctdo_rcbdo		UdtConsecutivo
	);

	Set @cnsctvo_cdgo_rgla_vldcn22 = 22; -- spASReglaValidarPrestacionYaSolicitada
	Set @cnsctvo_cdgo_rgla_vldcn27 = 27; -- spASReglaValidarCantidadMaximaxPrestacionConfirmada
	Set @cnsctvo_cdgo_rgla_vldcn28 = 28; -- spASReglaValidarCantidadMaximaxPrestacionRadicada
	Set @cnsctvo_cdgo_rgla_vldcn29 = 29; -- spASReglaValidarCantidadMaximaxPrestacionNoRadicada
	Set @cnsctvo_cdgo_rgla_vldcn30 = 30; -- spASReglaValidarCantidadMaximaxPrestacionxSolicitudConfirmacion
	Set @cnsctvo_cdgo_rgla_vldcn31 = 31; -- spASReglaValidarCantidadMaximaxPrestacionxSolicitudRadicada
	Set @cnsctvo_cdgo_rgla_vldcn32 = 32; -- spASReglaValidarCantidadMaximaxPrestacionxSolicitudNoRadicada
	Set @cnsctvo_cdgo_rgla_vldcn33 = 33; -- spASReglaValidarFrecuenciaEntrega
	Set @cnsctvo_cdgo_agrpdr_prstcn   = 297 /*-- agrupador para ambulancias */
	Set @cnsctvo_cdgo_agrpdr_prstcn_n = 294 /*-- agrupador para tutela-cancer-vih */
	Set @cnsctvo_cdgo_clsfccn_evnto   = 8   /*-- Notificacion de tutela */
	Set @cnsctvo_cdgo_clsfccn_evnto_c = 13  /*-- Notificacion de cancer */
	Set @cnsctvo_cdgo_clsfccn_evnto_v = 16  /*-- Notificacion de vih */

	Insert
	Into		#tmpEstadosFrecuenciaEntrega(cnsctvo_cdgo_estdo_ntfccn)
	Select		cnsctvo_cdgo_estdo_ntfccn_sld  
	From		tbASEstadosNotificacionFrecuenciaEntrega_Vigencias With(NoLock)
	Where		@fecha_actual Between inco_vgnca And fn_vgnca;
	 

	Insert
	Into       #tmpFrecuenciaEntrega(cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_srvco_slctdo_rcbdo,
	                                nmro_unco_idntfccn           , fcha_slctd_orgn 
								    )
	Select     c.cnsctvo_slctd_srvco_sld_rcbda, c.cnsctvo_srvco_slctdo_rcbdo,
			   a.nmro_unco_idntfccn           , MAX(aco.fcha_slctd_orgn) 
	From       #tmpDatosPrestacionesxSolicitud c
	Inner Join #tmpDatosAfiliado a
	On         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda	  
	Inner Join #tmpAtencionConceptosOPS aco
	On         aco.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda And 
	           aco.cnsctvo_srvco_slctdo_rcbdo    = c.cnsctvo_srvco_slctdo_rcbdo
	Where      aco.fcha_slctd_orgn IS NOT NULL
	Group By   c.cnsctvo_slctd_srvco_sld_rcbda, c.cnsctvo_srvco_slctdo_rcbdo, a.nmro_unco_idntfccn;

	Insert Into #tmpPrestacionesExepcionFrecuenciaEntrega
	(
				cnsctvo_srvco_slctdo_rcbdo
	)
	Select		a.llve_prmra_rgstro_vlddo
	From		#tbResultadosValidaciones a
	Inner Join	#tmpDatosPrestacionesxSolicitud b
	On			b.cnsctvo_slctd_srvco_sld_rcbda = a.llve_prmra_rgstro_vlddo
	Inner Join	bdSisalud.dbo.tbDetAgrupadoresPrestaciones_Vigencias c With(NoLock)
	On			c.cnsctvo_cdfccn = b.cnsctvo_cdfccn
	Where		c.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn /*-- agrupador para ambulancias */
	And			@fecha_actual Between c.inco_vgnca And c.fn_vgnca;

	/*Se elimina de la tabla temporal si el afiliado tiene una notificacion de tutela */
	Insert Into #tmpPrestacionesExepcionFrecuenciaEntrega
	(
				cnsctvo_srvco_slctdo_rcbdo
	)
	Select		a.llve_prmra_rgstro_vlddo
	From		#tbResultadosValidaciones a
	Inner Join	#tmpDatosPrestacionesxSolicitud b
	On			b.cnsctvo_slctd_srvco_sld_rcbda = a.llve_prmra_rgstro_vlddo
	Inner Join	#tmpDatosAfiliado d
	On			d.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdSisalud.dbo.tbAfiliadosMarcados c With(NoLock)
	On			c.nmro_unco_idntfccn = d.nmro_unco_idntfccn
	Where		c.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto; /*-- 8- Notificacion de Tutela */
	
	/*Se elimina de la tabla temporal lo relacionado con el agrupador de ambulancias */
	Insert Into #tmpPrestacionesExepcionFrecuenciaEntrega
	(
				cnsctvo_srvco_slctdo_rcbdo
	)
	Select		a.llve_prmra_rgstro_vlddo
	From		#tbResultadosValidaciones a
	Inner Join	#tmpDatosPrestacionesxSolicitud b
	On			b.cnsctvo_slctd_srvco_sld_rcbda = a.llve_prmra_rgstro_vlddo
	Inner Join	#tmpDatosAfiliado d
	On			d.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdSisalud.dbo.tbAfiliadosMarcados c With(NoLock)
	On			c.nmro_unco_idntfccn = d.nmro_unco_idntfccn	
	Inner Join	#tmpEstadosFrecuenciaEntrega f
	On			f.cnsctvo_cdgo_estdo_ntfccn = c.cnsctvo_cdgo_estdo_ntfccn
	Inner Join	bdSisalud.dbo.tbDetAgrupadoresPrestaciones_Vigencias g With(NoLock)
	On			g.cnsctvo_cdfccn = b.cnsctvo_cdfccn		
	Where		g.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn_n /*-- agrupador para tutela-cancer-vih */
	And			c.cnsctvo_cdgo_clsfccn_evnto In (@cnsctvo_cdgo_clsfccn_evnto_c, @cnsctvo_cdgo_clsfccn_evnto_v) -- 13-cancer - 16-vih
	And			@fecha_actual Between g.inco_vgnca And g.fn_vgnca;

	--
	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn22;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn27;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn28;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn29;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn30;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn31;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn32;

	Delete		rv
	From		#tbResultadosValidaciones rv
	Inner Join	#tmpPrestacionesExepcionFrecuenciaEntrega pefe
	On			pefe.cnsctvo_srvco_slctdo_rcbdo = rv.llve_prmra_cncpto_prncpl_vlddo
	Where		cnsctvo_cdgo_rgla_vldcn = @cnsctvo_cdgo_rgla_vldcn33;

	--
	Drop Table #tmpEstadosFrecuenciaEntrega;
	Drop Table #tmpFrecuenciaEntrega;
	Drop Table #tmpPrestacionesExepcionFrecuenciaEntrega;

End