USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASObtenerDatosBasicos]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASObtenerDatosBasicos
* Desarrollado por   : <\A Jhon W. Olarte Vélez A\>    
* Descripcion        : <\D Procedimiento que llena las tablas temporales de información básica  D\>
					   <\D de la solicitud														D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASObtenerDatosBasicos] @cdgs_slctds XML
AS
BEGIN
SET NOCOUNT ON 
	--Se inserta la información que llega para ser procesada para Direccionamiento
  INSERT INTO #tmpNmroSlctds (cnsctvo_slctd_srvco_sld_rcbda,
  nmro_slctd,
  nmro_slctd_prvdr)
    SELECT
      pref.value('(cnsctvo_slctd/text())[1]', 'udtConsecutivo') AS cnsctvo,
      pref.value('(cdgo_slctd/text())[1]', 'varchar(16)') AS cdgo,
      pref.value('(cdgo_slctd_prstdr/text())[1]', 'varchar(15)') AS cdgo_prvdr
    FROM @cdgs_slctds.nodes('/cdgs_slctds//idnt_slctds') AS xml_slctd (Pref);

  --Se valida que la información que ingresa, si se encuentre en la base de datos, para así ser procesada.
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_srvco_sld_rcbda)
  WHERE s.cnsctvo_slctd_srvco_sld_rcbda IS NOT NULL

  --Cuando solo llega el número solicitud
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_atrzcn_ss = s.nmro_slctd)
  WHERE s.nmro_slctd IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

  --Cuando solo llega el número solicitud prestador	
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_prvdr = s.nmro_slctd_prvdr)
  WHERE s.nmro_slctd_prvdr IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL
END

GO
