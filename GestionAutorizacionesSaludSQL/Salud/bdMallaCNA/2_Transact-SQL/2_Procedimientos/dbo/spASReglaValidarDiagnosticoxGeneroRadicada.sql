USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarDiagnosticoxGeneroRadicada]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarDiagnosticoxGeneroRadicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si el sexo del afiliado no corresponde D\>
				  <\D al definido para el diagnostico y la solicitud no fue radicada en SOS D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarDiagnosticoxGeneroRadicada] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutvioSexoAmbos numeric(1),
          @negacion char(1),
          @consecutivoMedioContactoASI numeric(1)
  SET @consecutivoMedioContactoASI = 1;
  SET @consecutvioSexoAmbos = 3
  SET @negacion = 'N'

  BEGIN
    DECLARE @fecha date;
    SET @fecha = GETDATE();

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El diagnostico, ingresado no corresponde con el genero del afiliado, se debe validar el diagnostico ingresado.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN bdSisalud.dbo.tbDiagnosticos_Vigencias m WITH (NOLOCK)
        ON (c.cnsctvo_cdgo_dgnstco = m.cnsctvo_cdgo_dgnstco
        AND c.fcha_prstcn BETWEEN m.inco_vgnca AND m.fn_vgnca)
      WHERE m.cnsctvo_cdgo_sxo <> @consecutvioSexoAmbos
      AND m.cnsctvo_cdgo_sxo != a.cnsctvo_cdgo_sxo
      AND c.cnfrmcn_cldd = @negacion
      AND c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI;
  END

GO
