USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarMarcaDomi]    Script Date: 06/04/2017 11:56:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarMarcaDomi
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Valida si a nivel de solicitud esta marcado como DOMI o se valida en 
						cupsxplanesxcargo, y el cargo tenga asociada la marca  DOMI    D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 16/11/2016  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                             Se modifica procedimiento para que no consulte el grupo de domi por descripcion si no por
							 consecutivo
					    DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/04/2017 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarMarcaDomi] 
	@prmtro_sp_vldcn varchar(30),
	@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
	@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON 

  DECLARE @vlr_uno                  Int           ,
		  @vlr_si                   Char(1)       ,
		  @vlr_si_1                 Char(1)       ,
		  @cnsctvo_cdgo_fljo_crgo   udtConsecutivo,
		  @l_fecha	                Datetime      ,
		  @infrmcn_adcnl_no_cnfrmdd Varchar(250)   

  Create 
  Table  #tmpDatosPrestacionesxSolicitudDOMI(cnsctvo_cdgo_rgla_dmi				udtConsecutivo,
											 cnsctvo_cdgo_no_cnfrmdd_dmi		udtConsecutivo,
											 cnsctvo_slctd_srvco_sld_rcbda_dmi	udtConsecutivo,
											 cnsctvo_srvco_slctdo_rcbdo_dmi		udtConsecutivo,
											 flg_mrca_dmi_slctd_srvco			Int default 0,
											 cnsctvo_cdfccn						udtConsecutivo,
											 cnsctvo_cdgo_pln					udtConsecutivo
											);
  
  SET @vlr_uno		            = 1;
  SET @vlr_si		            = 'S';
  SET @vlr_si_1		            = '1';
  Set @cnsctvo_cdgo_fljo_crgo   = 1  ; --DOMI
  Set @l_fecha                  = getDate();
  Set @infrmcn_adcnl_no_cnfrmdd = 'prestacion para validacion de domi.'
  

  Insert 
  Into   #tmpDatosPrestacionesxSolicitudDOMI(cnsctvo_cdgo_rgla_dmi            , cnsctvo_cdgo_no_cnfrmdd_dmi   ,
	                                         cnsctvo_slctd_srvco_sld_rcbda_dmi, cnsctvo_srvco_slctdo_rcbdo_dmi,
	                                         cnsctvo_cdfccn
											)
  SELECT @cnsctvo_cdgo_rgla_vldcn       , @cnsctvo_cdgo_no_cnfrmdd_vldcn,
	     c.cnsctvo_slctd_srvco_sld_rcbda, c.cnsctvo_srvco_slctdo_rcbdo  , 
	     cnsctvo_cdfccn
  FROM   #tmpDatosPrestacionesxSolicitud c

  --Valida si existe la marca Domi a nivel de formulario, de lo contrario se valida a nivel de prestación
  IF Exists(Select 'X' from  #tmpDatosPrestacionesxSolicitudDOMI dps
			Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
			ON sas.cnsctvo_slctd_atrzcn_srvco = dps.cnsctvo_slctd_srvco_sld_rcbda_dmi
			Where sas.dmi in (@vlr_si_1, @vlr_si) 
		   )
	   Begin
		  UPDATE     dps
		  SET        flg_mrca_dmi_slctd_srvco = @vlr_uno 
		  FROM       #tmpDatosPrestacionesxSolicitudDOMI dps
		  Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
		  ON         sas.cnsctvo_slctd_atrzcn_srvco = dps.cnsctvo_slctd_srvco_sld_rcbda_dmi
		  Where      sas.dmi in (@vlr_si_1, @vlr_si);
	   End
  Else
	  Begin
		  Update     dps
		  Set        cnsctvo_cdgo_pln  = ias.cnsctvo_cdgo_pln
		  From       #tmpDatosPrestacionesxSolicitudDOMI dps
		  Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK)
		  On         ias.cnsctvo_slctd_atrzcn_srvco = dps.cnsctvo_slctd_srvco_sld_rcbda_dmi
  
		  UPDATE     dps
		  SET        flg_mrca_dmi_slctd_srvco = @vlr_uno
		  From       #tmpDatosPrestacionesxSolicitudDOMI dps
		  Inner Join bdSisalud.dbo.tbCupsxPlanxCargo_Vigencias cpc WITH (NOLOCK) 
		  On         cpc.cnsctvo_cdfccn   = dps.cnsctvo_cdfccn   And 
		             cpc.cnsctvo_cdgo_pln = dps.cnsctvo_cdgo_pln
		  Inner Join bdSisalud.dbo.tbCargosSOS_Vigencias cv WITH (NOLOCK) 
		  On         cv.cnsctvo_cdgo_crgo_ss = cpc.cnsctvo_cdgo_crgo
		  Inner Join bdcna.prm.tbASFlujosCargos_Vigencias fcv WITH (NOLOCK) 
		  On         fcv.cnsctvo_cdgo_fljo_crgo = cv.cnsctvo_cdgo_fljo_crgo
		  Where      fcv.cnsctvo_cdgo_fljo_crgo = @cnsctvo_cdgo_fljo_crgo
		  And        @l_fecha BETWEEN cpc.inco_vgnca AND cpc.fn_vgnca
		  And	     @l_fecha BETWEEN cv.inco_vgnca AND cv.fn_vgnca
		  And	     @l_fecha BETWEEN fcv.inco_vgnca AND fcv.fn_vgnca  
	  End	  
	  
  INSERT 
  INTO  #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn, cnsctvo_cdgo_no_cnfrmdd_vldcn ,
                                  llve_prmra_rgstro_vlddo, llve_prmra_cncpto_prncpl_vlddo,
                                  infrmcn_adcnl_no_cnfrmdd
								 )
  SELECT @cnsctvo_cdgo_rgla_vldcn           , @cnsctvo_cdgo_no_cnfrmdd_vldcn  ,
         c.cnsctvo_slctd_srvco_sld_rcbda_dmi, c.cnsctvo_srvco_slctdo_rcbdo_dmi,
         @infrmcn_adcnl_no_cnfrmdd
  FROM   #tmpDatosPrestacionesxSolicitudDOMI c
  WHERE  flg_mrca_dmi_slctd_srvco = @vlr_uno

  DROP TABLE #tmpDatosPrestacionesxSolicitudDOMI;
	 
END
