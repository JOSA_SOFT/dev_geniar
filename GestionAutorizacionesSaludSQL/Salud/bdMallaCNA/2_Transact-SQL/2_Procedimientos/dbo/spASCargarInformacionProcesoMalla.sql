USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarInformacionProcesoMalla]    Script Date: 7/10/2017 9:51:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarInformacionProcesoMalla
* Desarrollado por : <\A Jois Lombana A\>    
* Descripcion      : <\D 
                         Procedimiento Orquestador que se encarga de cargar la información que se encuentre de la
  					     solicitud para realizar el procesamiento de la malla. 
					 D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 30/11/2015  FC\>    
*--------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando en la tabla temporal de servicios los campos
						   asociados a la marca de capitacion
                       D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  02/03/2017  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez García  sisjvg01- 2017-07-06 AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando en las tablas temporales 
						   #Srvcs el Campo nmro_unco_prstdr_cptdo,
						   #IPSSl el Campo Cdgo_intrno_slctnte

                       D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-07-06  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------
*/

ALTER PROCEDURE [dbo].[spASCargarInformacionProcesoMalla] 
	@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
	@es_btch                char(1)
AS
  SET NOCOUNT ON 
  BEGIN
	/*:::::::::::::::Creación de tablas Temporales::::::::::::::::::*/
	--TABLA Solicitudes
	Create Table #Slctds(	 cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,Origen_atencion				udtConsecutivo
							,Tipo_Servicio					udtConsecutivo
							,Prioridad_atencion				udtConsecutivo
							,Ubicacion_Paciente				udtConsecutivo
							,Tipo_Solicitud					udtConsecutivo
							,Medio_Contacto					udtConsecutivo
							,Tipo_Transaccion				udtConsecutivo
							,Clase_atencion					udtConsecutivo
							,Forma_atencion					udtConsecutivo
							,Fecha_sol						Datetime
							,Cod_Origen_atencion			udtCodigo
							,Cod_Tipo_servicio				udtCodigo
							,Cod_Prioridad_atencion			udtCodigo
							,Cod_Tipo_Ubicacion				udtCodigo
							,Cod_Medio_Contacto				udtCodigo
							,Cod_Tipo_Transaccion			udtCodigo
							,Cod_Clase_Atencion				char(3)
							,Cod_Forma_atencion				udtCodigo)
			
	--TABLA Afiliado
	Create Table #Aflds	(	cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,Tipo_Identificacion			udtConsecutivo
							,Departamento					udtConsecutivo
							,Ciudad							udtConsecutivo
							,Cobertura						udtConsecutivo
							,Tipo_Plan						udtConsecutivo
							,Codigo_Plan					udtConsecutivo
							,Estado_Plan					udtConsecutivo
							,Sexo							udtConsecutivo
							,Vinculacion					udtConsecutivo
							,Tipo_contrato					udtConsecutivo
							,Rango_Salarial					udtConsecutivo
							,Beneficiario					udtConsecutivo
							,Estado_Derecho					udtConsecutivo
							,Ips							udtCodigoIps 
							,Sede_Ips						udtConsecutivo
							,Sexo_recien_nacido				udtConsecutivo
							,Cohorte						udtConsecutivo
							,Numero_Contrato				udtNumeroFormulario
							,NUI							udtConsecutivo
							,Num_identificacion				udtNumeroIdentificacionLargo
							,Semanas_Cotizadas				udtConsecutivo
							,Edad_Anual						udtConsecutivo
							,Edad_Meses						udtConsecutivo
							,Edad_Dias						udtConsecutivo
							,Cod_Tipo_Identif				udtCodigo
							,Cod_Departamento				Char(3)
							,Cod_ciudad						udtCiudad
							,Cod_Cobertura					Varchar(5)
							,Cod_Tipo_plan					udtCodigo 
							,Cod_plan						udtCodigo
							,Cod_estado_plan				udtCodigo
							,Cod_Sexo						udtCodigo
							,Cod_Vinculacion				udtCodigo
							,Cod_Sexo_recien_ncdo			udtCodigo
							,Fecha_Nacimiento				Datetime)
						
	--TABLA IPS Solicitudes
	Create Table #IPSSl(	cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,Tipo_Iden_Soli					udtConsecutivo
							,Departamento					udtConsecutivo
							,Ciudad							udtConsecutivo
							,Cod_Tipo_Identificacion		udtCodigo
							,Cod_Departamento				Char(3)
							,Cod_Ciudad						udtCiudad
							,Adscrito						udtLogico
							,NIUP							udtConsecutivo
							,Numero_identificacion			udtNumeroIdentificacionLargo
							,Cdgo_intrno_slctnte			UdtCodigoIps)

	-- TABLA Informacion Hospitalaria
	Create Table #Hsptlra	(cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,Clase_Habitacion				udtConsecutivo
							,Servicio_Hospitalizacion		udtConsecutivo
							,Cod_clase_Habitacion			udtCodigo
							,Cod_servicio_Hosp				udtCodigo)

	-- TABLA Diagnosticos
	Create Table #Dgnstcs(		cnsctvo_dgnstco_slctd_atrzcn_srvco udtConsecutivo
								,cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
								,Tipo_Diagnostico				udtConsecutivo
								,Recobro						udtConsecutivo
								,Contingencia					udtConsecutivo		
								,Codigo_Diagnostico				udtConsecutivo
								,Cod_Tipo_Diag					char(3)
								,Cod_Diagnostico				udtCodigoDiagnostico
								,Cod_Recobro					Char(3)
								,Cod_Contingencia				Char(3))
	
	-- TABLA Medicos
	Create Table #Mdcs (	cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,Tipo_Afiliacion				udtConsecutivo
							,Especialidad					udtConsecutivo
							,Tipo_identificacion			udtConsecutivo
							,Cod_Tipo_Afiliacion			udtCodigo
							,Cod_esp_Med_trat				udtCodigo
							,Cod_Tipo_Iden					udtCodigo)

	-- TABLA Servicios
	Create Table #Srvcs (cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo,
						 cnsctvo_srvco_slctdo_rcbdo		udtConsecutivo,
						 Servicio_Solicitado			udtConsecutivo,
						 Tipo							udtConsecutivo,
						 Unidad_Tiempo					udtConsecutivo,
						 Tipo_Autorizacion				udtConsecutivo,
						 Prestacion_Prestador			udtConsecutivo,
						 Cargo_direccionamiento			udtConsecutivo,
						 Indicador_no_Pos				udtLogico     ,
						 Prestacion_Vigente				udtLogico     ,
						 Prestacion_Capitada			udtLogico     default 'N',
						 Programa_Entrega				udtLogico     ,
						 Cod_Tipo_serv					udtCodigo     ,
						 Cod_prstcn_prstdr				udtCodigo     ,
						 Cod_Servicio_sol				char(15)      ,
						 Cod_Unidad_Tiempo_tratam		udtCodigo     ,
						 Cod_Tipo_autorizacion			udtCodigo     ,
						 Cod_Servicio					Char(15)      ,
						 cnsctvo_cdgo_tpo_mdlo_cpta     udtConsecutivo,
						 cdgo_intrno_cpta               udtCodigoIPS,
						 nmro_unco_prstdr_cptdo			UdtConsecutivo,
						)  
	
	-- TABLA Medicamentos
	Create Table #Mdcmnts (cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
								,cnsctvo_srvco_slctdo_rcbdo		udtConsecutivo
								,Presentacion_Dosis				udtConsecutivo
								,Unidad_Periodicidad_Dosis		udtConsecutivo
								,Forma_Farmaceutica				udtConsecutivo
								,Grupo							udtConsecutivo
								,Prestacion						udtConsecutivo
								,Administracion					udtConsecutivo
								,Concentracion_Unidad			udtConsecutivo
								,Fecha_Venc_Invima				Datetime
								,Muestra_medica					udtLogico
								,Uso_condicionado				udtLogico
								,Meses_Max_Renovacion			numeric(13,3)
								,Cod_Presentacion_dosis			udtCodigo
								,Cod_Unidad_Periodicidad		udtCodigo
								,Cod_forma_farmaceutica			char(4)
								,Cod_grupo_terapeutico			char(3)
								,Cod_Presentacion				char(4)
								,Cod_Via_administracion			udtCodigo
								,Cod_Unidad_concentracion		Char(10))

	-- TABLA Insumos 
	Create Table #Insms (	cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo
							,cnsctvo_srvco_slctdo_rcbdo		udtConsecutivo
							,Lateralidad					udtConsecutivo
							,Via_acceso						udtConsecutivo
							,Unidad_cantidad				udtlogico
							,Frecuencia_Entrega				numeric(7, 3)
							,Cantidad_Max_Permitida			numeric(3, 0)
							,Cod_Lateralidad				udtCodigo
							,Cod_Via_acceso					udtCodigo
							,Acceso_Directo					udtLogico)

	-- Actualizar campos de tabla solicitudes PRE MALLA temporal
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudSolicitudes
	
	-- Actualizar Campos de tabla Afiliados temporal PRE MALLA
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudAfiliado

	-- Actualizar Campos de tablas IPS, Informacion Hospitalaria, Diagnosticos y Medicos temporales PRE MALLA
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudIpsHospitalizacionDiagnosticoMedico
	
	-- Actualizar Campos de tablas Prestaciones y/o servicios y Medicamentos temporales PRE MALLA
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudServiciosMedicamentos 

	-- Actualizar Campos de tabla Insumos temporal PRE MALLA
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudServiciosInsumos 
	
	-- Actualizar todas las tablas Reales de PRE MALLA Proceso
	Exec bdMallaCNA.dbo.spASCargarDatosSolicitudActualizarDatos @cnsctvo_cdgo_tpo_prcso

	DROP TABLE #Slctds
	DROP TABLE #Aflds
	DROP TABLE #IPSSl
	DROP TABLE #Hsptlra
	DROP TABLE #Dgnstcs
	DROP TABLE #Mdcs
	DROP TABLE #Srvcs
	DROP TABLE #Mdcmnts
	DROP TABLE #Insms	
END
