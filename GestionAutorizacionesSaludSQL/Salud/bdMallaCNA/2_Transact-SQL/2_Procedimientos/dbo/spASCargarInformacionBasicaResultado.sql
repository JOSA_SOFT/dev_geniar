USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarInformacionBasicaResultado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCargarInformacionBasicaResultado
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite cargar la información básica				  D\>
					   <\D de la solicitud para la consulta de resultados de la malla.			  D\>    
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>
						<\P cnsctvo_cdgo_mlla_vldcn	P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASCargarInformacionBasicaResultado] @cdgs_slctds xml
AS
BEGIN

  DECLARE @tmpCodigosSolicitud TABLE (
    xml_slctd xml
  );

   DECLARE @fcha datetime = GETDATE()
--Se inserta la información que llega para ser procesada en la malla en la temporal.
    INSERT INTO @tmpCodigosSolicitud
      VALUES (@cdgs_slctds);
    INSERT INTO #tmpDatosSolicitud (cnsctvo_slctd_srvco_sld_rcbda,
    nmro_slctd_ss,
    nmro_slctd_prvdr)
      SELECT
        pref.value('(cnsctvo_slctd/text())[1]', 'udtConsecutivo') AS cnsctvo,
        pref.value('(cdgo_slctd/text())[1]', 'varchar(16)') AS cdgo,
        pref.value('(cdgo_slctd_prstdr/text())[1]', 'varchar(16)') AS cdgo_prvdr
      FROM @tmpCodigosSolicitud
      CROSS APPLY xml_slctd.nodes('/cdgs_slctds/idnt_slctds') AS xml_slctd (Pref);

    --Se valida que la información que ingresa, si se encuentre en la base de datos, para así procesar la malla.
    UPDATE #tmpDatosSolicitud
    SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco,
        cnsctvo_prcso = a.cnsctvo_prcso,
        cnsctvo_estdo_slctd = v.cnsctvo_cdgo_estdo_no_cnfrmdd,
        dscrpcn_estdo_slctd = nc.dscrpcn_estdo_no_cnfrmdd,
		nmro_slctd_ss = a.nmro_slctd_atrzcn_ss,
        nmro_slctd_prvdr = a.nmro_slctd_prvdr
    FROM #tmpDatosSolicitud s WITH (NOLOCK)
    LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
      ON a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_srvco_sld_rcbda
    LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] v WITH (NOLOCK)
      ON v.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_slctd
	  AND @fcha BETWEEN v.inco_vgnca AND v.fn_vgnca
	LEFT JOIN bdMallaCNA.[dbo].[tbEstadosNoConformidades_Vigencias] NC WITH(NOLOCK)
	  ON NC.cnsctvo_cdgo_estdo_no_cnfrmdd = v.cnsctvo_cdgo_estdo_no_cnfrmdd
      AND @fcha BETWEEN NC.inco_vgnca AND NC.fn_vgnca
    WHERE s.cnsctvo_slctd_srvco_sld_rcbda IS NOT NULL

    --Cuando solo llega el número solicitud
    UPDATE #tmpDatosSolicitud
    SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco,
        cnsctvo_prcso = a.cnsctvo_prcso,
		cnsctvo_estdo_slctd = v.cnsctvo_cdgo_estdo_no_cnfrmdd,
        dscrpcn_estdo_slctd = nc.dscrpcn_estdo_no_cnfrmdd,
		nmro_slctd_ss = a.nmro_slctd_atrzcn_ss,
        nmro_slctd_prvdr = a.nmro_slctd_prvdr
    FROM #tmpDatosSolicitud s WITH (NOLOCK)
    LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
      ON (a.nmro_slctd_atrzcn_ss = s.nmro_slctd_ss)
	LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] v WITH (NOLOCK)
      ON v.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_slctd
	  AND @fcha BETWEEN v.inco_vgnca AND v.fn_vgnca
	LEFT JOIN bdMallaCNA.[dbo].[tbEstadosNoConformidades_Vigencias] NC WITH(NOLOCK)
	  ON NC.cnsctvo_cdgo_estdo_no_cnfrmdd = v.cnsctvo_cdgo_estdo_no_cnfrmdd
      AND @fcha BETWEEN NC.inco_vgnca AND NC.fn_vgnca
    WHERE s.nmro_slctd_ss IS NOT NULL 
		and  s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

    --Cuando solo llega el número solicitud prestador	
    UPDATE #tmpDatosSolicitud
    SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco,
        cnsctvo_prcso = a.cnsctvo_prcso,
		cnsctvo_estdo_slctd = v.cnsctvo_cdgo_estdo_no_cnfrmdd,
        dscrpcn_estdo_slctd = nc.dscrpcn_estdo_no_cnfrmdd
    FROM #tmpDatosSolicitud s WITH (NOLOCK)
    LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
      ON (a.nmro_slctd_prvdr = s.nmro_slctd_prvdr)
	 LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] v WITH (NOLOCK)
      ON v.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_slctd
	  AND @fcha BETWEEN v.inco_vgnca AND v.fn_vgnca
	LEFT JOIN bdMallaCNA.[dbo].[tbEstadosNoConformidades_Vigencias] NC WITH(NOLOCK)
	  ON NC.cnsctvo_cdgo_estdo_no_cnfrmdd = v.cnsctvo_cdgo_estdo_no_cnfrmdd
      AND @fcha BETWEEN NC.inco_vgnca AND NC.fn_vgnca
    WHERE s.nmro_slctd_prvdr IS NOT NULL
		AND  s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

	--Insert para utilización de SP de control de ejecución.
	INSERT INTO #tmpNmroSlctds (cnsctvo_slctd_srvco_sld_rcbda,
  nmro_slctd,
  nmro_slctd_prvdr)
  SELECT TMP.cnsctvo_slctd_srvco_sld_rcbda,
  TMP.nmro_slctd_ss,
  TMP.nmro_slctd_prvdr
  FROM #tmpDatosSolicitud TMP

END

GO
