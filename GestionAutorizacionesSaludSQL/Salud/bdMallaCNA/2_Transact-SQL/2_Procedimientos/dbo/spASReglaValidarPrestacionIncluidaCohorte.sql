USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionIncluidaCohorte]    Script Date: 03/02/2017 10:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionIncluidaCohorte
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el afiliado presenta un posible D\> 
					 <\D ingreso a cohorte (Prestación). D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 15/10/2015  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D Se completa mensaje, se agrega la referencia de prestacion D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/01/20 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                           Se modifica el procedimiento adicionando la validacion de las vigencias sobre la tabla
						   tbCohortes_vigencias  
						D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/02/03 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionIncluidaCohorte] 
  @prmtro_sp_vldcn varchar(30),
  @cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
  @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @tipoPlanPos                Numeric(1)    ,
          @fcha_actl                  Datetime      ,
		  @infrmcn_adcnl_no_cnfrmdd   udtDescripcion,
		  @infrmcn_adcnl_no_cnfrmdd_1 udtDescripcion,
		  @lcEspacio                  Char(2)

  SET @tipoPlanPos                = 1
  Set @fcha_actl                  = getDate()
  Set @infrmcn_adcnl_no_cnfrmdd   = 'Afiliado con posible ingreso a cohorte:'
  Set @infrmcn_adcnl_no_cnfrmdd_1 = 'Prestacion con posible riesgo.'
  Set @lcEspacio                  = ' '

  BEGIN

    INSERT 
	INTO   #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                      llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
									 )
    SELECT     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
               c.cnsctvo_srvco_slctdo_rcbdo, CONCAT(@infrmcn_adcnl_no_cnfrmdd, @lcEspacio, cv.dscrpcn_chrte, @lcEspacio, @infrmcn_adcnl_no_cnfrmdd_1)
    FROM       #tmpDatosPrestacionesxSolicitud c
    INNER JOIN #tmpDatosAfiliado a
    ON         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
    INNER JOIN BDRiesgosSalud.dbo.tbPrestacionesxCohortes v WITH (NOLOCK)
    ON         v.cnsctvo_cdfccn = c.cnsctvo_cdfccn
	INNER JOIN bdRiesgosSalud.dbo.tbCohortes_vigencias cv WITH (NOLOCK)
    ON         cv.cnsctvo_cdgo_chrte = v.cnsctvo_cdgo_chrte
	LEFT JOIN  bdRiesgosSalud.dbo.tbReporteCohortesxAfiliado ch WITH (NOLOCK)
    ON         ch.cnsctvo_cdgo_chrte       = v.cnsctvo_cdgo_chrte AND 
	           ch.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
    WHERE      ch.nmro_unco_idntfccn_afldo IS NULL
    AND        a.cnsctvo_cdgo_tpo_pln = @tipoPlanPos --Corresponde al tipo de plan POS
	And        @fcha_actl Between cv.inco_vgnca And cv.fn_vgnca
	And        @fcha_actl Between v.inco_vgnca And v.fn_vgnca

  END
