USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASMallaGestionSolicitudes]    Script Date: 06/03/2017 18:02:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASMallaGestionSolicitudes
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite la orquestación de la ejecución de la malla de 
						   gestión de solicitudes     
					   D\>    
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM  
                            Se modifica procedimiento para enviar el usuario al procedmiento
							spASCargarInformacionProcesoMalla
                       DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 06/03/2017   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASMallaGestionSolicitudes] 
    @usro varchar(50),
	@cnsctvo_cdgo_mlla_vldcn udtConsecutivo,
	@es_btch char(1),
	@estdo_ejccn varchar(5) OUTPUT,
	@msje_errr varchar(2000) OUTPUT,
	@msje_rspsta xml OUTPUT
AS
  SET NOCOUNT ON 
  --  Creación de tablas temporales para la ejecución de la gestión de solicitudes y ejecución de reglas.

  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpDatosSolicitud (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_cdgo_tpo_trnsccn_srvco_sld int,
    cnsctvo_estdo_slctd int,
    dscrpcn_estdo_slctd varchar(30),
    nmro_slctd_ss_rmplzo varchar(17),
    cnsctvo_cdgo_estdo_slctd int,
    fcha_crcn datetime,
    fcha_slctd datetime,
    nmro_slctd_pdre varchar(17),
    nmro_slctd_prvdr varchar(17),
    nmro_slctd_ss varchar(17),
    tne_sprte_mdcmnto char(1) DEFAULT 'N', -- sirve para validar si la solicitud tiene documentos de soporte - SI = 'S' , NO = NULO O 'N'
    es_nmro_slctd_prvdr_exstnte_ssttcn char(1) DEFAULT 'N',             -- con este campo podemos saber si el numero esta duplicado de tipo transaccion sustentacion - SI = 'S' , NO = NULO O 'N'
    cndd_vcs_estdo_dvlto int,                 -- indica cuantas solicitudes estan estado devulucion   
    cndd_ds_trnscrrds_sstntcn int DEFAULT 0,
    tne_no_cnfrmdad char(1) DEFAULT 'N', -- sirve para validar si la solicitud tiene una no conformidad asociadad - SI = 'S' , NO = NULO O 'N'
    cndd_vcs_slctds_vldds int,				-- sirve validar si dentro del solicitudes validadas  hay duplicadas 
    aplca_rgla char(1) DEFAULT 'N', -- Marca el registro que debe aplicar la regla 
    prstdr_exstnt char(1) DEFAULT 'N', -- sirve para validar si el prestador existente - SI = 'S' , NO = NULO O 'N'
	orgn_atncn int
  )

  --Temporal con los datos del afiliado
  CREATE TABLE #tmpDatosAfiliado (
    id int IDENTITY (1, 1),
    cnsctvo_afldo_slctd_rcbda int,
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_cdgo_tpo_idntfccn_afldo int,
    cdgo_tpo_idntfccn_afldo char(3),
    nmro_idntfccn_afldo varchar(23),
    nmro_unco_idntfccn varchar(23), -- Número único de identificación del afiliado.
    cnsctvo_cdgo_pln int,
    cdgo_pln char(11),
    dscrpcn_pln varchar(150),
    cnsctvo_cdgo_pln_pc int,
    dscrpcn_pln_pc varchar(150),
    cdgo_pln_pc char(11),
    cnsctvo_cdgo_tpo_pln int,
    cnsctvo_cdgo_pln_tmp int,
    afldo_exstnt char(1) DEFAULT 'N', -- sirve para validar si el afiliado es existente - SI = 'S' , NO = NULO O 'N'
    cnsctvo_cdgo_sxo int,
    cdgo_sxo char(1),
    edd_afldo int, --edad del afiliado en años
    edd_mss int, --edad en meses del afiliado.
    smns_ctzds int, -- Cantidad de semanas cotizadas del afiliado.
    ips_prmria int, -- Ips Primaria del afiliado.
    cnsctvo_sde_ips_prmria int, -- Sede Ips Primaria del afiliado.
    cnsctvo_rngo_slrl int, -- Consecutivo rango salarial.
    cdgo_intrno char(8),
    cnsctvo_estdo_drcho_pos int,
    prmr_nmbre_afldo varchar(30),
    prmr_aplldo_afldo varchar(50),
    rcn_ncdo char(1) DEFAULT 'N',
    prto_mltple char(1),
    cnsctvo_cdgo_cdd_afldo int,
    cdgo_cdd_afldo char(10),
    drccn_afldo varchar(500),
    fcha_ncmnto datetime,
    nro_hjs int,
    cnsctvo_cdgo_chrte int,
    cdgo_chrte int,
    cnsctvo_tpo_cntrto int,
    nmro_cntrto int,
    cnsctvo_bnfcro int,
    fcha_slctd datetime,
	estdo_drcho	INT,
	estdo_drcho_actvo	CHAR(1)
  )

  --Temporal que contiene el cruce de las prestaciones por solicitud
  CREATE TABLE #tmpDatosPrestacionesxSolicitud (
    id int IDENTITY (1, 1),
    cnsctvo_srvco_slctdo_rcbdo int,
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_estdo_prstcn int,
    dscrpcn_estdo_prstcn varchar(30),
	cnsctvo_estdo_prstcn_mga	INT,

    cnsctvo_cdgo_tpo_srvco int,
    cdgo_tpo_srvco varchar(15),
    cnsctvo_cdgo_orgn_atncn int,-- Consecutivo del origen de la atención.
    cdgo_orgn_atncn varchar(15),-- código del origen de la atención.
    cnsctvo_cdgo_prrdd_atncn int, -- Consecutivo prioridad de la atención.
    cdgo_prrdd_atncn varchar(15), -- Código de la prioridad de la atención.

    cnsctvo_cdgo_clse_atncn int, -- Consecutivo clase de la atención.
    cdgo_clse_atncn varchar(15), -- Código de la clase de la atención.
    jstfccn_clnca varchar(2000), -- Justificación clínica de la solicitud
    cnsctvo_cdgo_mdo_cntcto_slctd int, -- Consecutivo código del medio de contacto.
    cdgo_mdo_cntcto_slctd varchar(15), --Código del medio de contacto

    cnsctvo_cdgo_tpo_dgnstco int, -- Consecutivo del tipo de diagnóstico, 1 -> principal
    cdgo_tpo_dgnstco int, -- Consecutivo del tipo de diagnóstico, 1 -> principal
    cnsctvo_cdgo_dgnstco int, -- Consecutivo del diagnostico principal relacionado en la solicitud.
    cdgo_dgnstco varchar(15), -- Código del diagnostico principal				

    cnsctvo_cdgo_srvco_hsptlzcn int, --Consecutivo código del servicio de hospitalización.
    cdgo_srvco_hsptlzcn varchar(15),
    fcha_ingrso_hsptlzcn datetime, --Fecha hospitalización desde.
    fcha_egrso_hsptlzcn datetime, --Fecha hospitalización hastas.
    cma varchar(10),
    cnsctvo_cdgo_clse_hbtcn int, --Consecutivo clase de habitación.
    cdgo_clse_hbtcn varchar(15),

    cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte int,
    cdgo_tpo_idntfccn_mdco_trtnte char(3),
    nmro_idntfccn_mdco_trtnte varchar(23),
    prmr_nmbre_mdco_trtnte varchar(30),
    sgndo_nmbre_mdco_trtnte varchar(30),
    prmr_aplldo_mdco_trtnte varchar(50),
    sgndo_aplldo_mdco_trtnte varchar(50),
    rgstro_mdco_trtnte varchar(17),
    cnsctvo_cdgo_espcldd_mdco_trtnte int,
    cdgo_espcldd_mdco_trtnte varchar(15),
    mdco_adscrto char(1),
    nmro_unco_idntfccn_mdco int,

    prstdr_adscrto char(1),
    cnsctvo_cdgo_tpo_idntfccn_prstdr int,
    cdgo_tpo_idntfccn_prstdr varchar(15),
    nmro_idntfccn_prstdr varchar(23),
    nmro_unco_idntfccn_prstdr int,
    nmbre_prstdr varchar(150),
    cnsctvo_cdgo_cdd_prstdr int,
    cdgo_cdd_prstdr varchar(15),
    cdgo_intrno_ips char(8),

    cnsctvo_cdgo_tpo_cdfccn int,
    cdgo_tpo_cdfccn varchar(15),
    cnsctvo_cdfccn int,
    cdgo_cdfccn varchar(15),
    dscrpcn_cdfccn varchar(150),
    cntdd_slctda int,	--Cantidad solicitada en la prestación
    frcnca_entrga numeric(7, 3), -- Frecuencia de entrega para el procedimiento. (CUPS)
    mdda_cntdd char(1), --Indica si la cantidad es en la vida o en el año.

    cdgo_srvco_rz_ps varchar(15),
    smns_crnca int,
    nmro_slctd_prvdr varchar(17), --Número de solicitud de proveedor para validar información histórica.
    indcdr_no_ps char(1), --sirve para validar si la prestación es NO POS
    fcha_prstcn datetime,
    fcha_crcn datetime,

    srvco_exstnt char(1) DEFAULT 'S',  -- sirve para validar si el servicio es existente - SI = 'S' , NO = NULO O 'N'
    tne_cnvno int,                  -- sirve para validar si el servicio tiene convenio   - SI = 1   , NO = -1 
    ctdd_ds_slctd int,                   -- sirve para indicar cuantos dias hay desde que se presto el servicio hasta que se genero la solicitud
    tne_rz_ps char(1) DEFAULT 'N',  -- sirve para validar si el servicio tiene raiz POS - SI = 'S' , NO = NULO O 'N'
    prstcn_vgnte char(1) DEFAULT 'N',  -- Sirve para validar si la prestación se encuenta vigente.
    tpe_mxmo_st char(1),  -- Sirve para validar si la solicitud está marcada con tope máximod e SOAT.
    cnfrmcn_cldd char(1) DEFAULT 'N', -- Sirve para validar si la solicitud tiene la marca de confirmación de calidad (inconsistencia).
    cntdd_mxma int,  -- Indica la cantidad máxima para la prestación.
    cntdd_mxma_prmtda_x_slctd int,  -- Indica la cantidad máxima permitida para la solicitud.

    dss float,
    cnsctvo_cdgo_prsntcn_dss int,
    cdgo_prsntcn_dss varchar(30),
    cnsctvo_cdgo_va_accso int,
    cdgo_va_accso varchar(30),
    prdcdd_dss float,
    cnsctvo_cdgo_undd_prdcdd_dss varchar(30),
    cdgo_undd_prdcdd_dss varchar(30),
    cncntrcn_dss char(20),
    cnsctvo_cdgo_undd_cncntrcn_dss varchar(30),
    cdgo_undd_cncntrcn_dss varchar(30),
    prsntcn char(20),
    cnsctvo_cdgo_prsntcn int,
    cdgo_prsntcn varchar(30),
    cnsctvo_cdgo_ltrldd int,
    cdgo_ltrldd varchar(30),

    fcha_vncmnto_invma date, -- Fecha de vencimiento invima, solo aplica para medicamentos
    mss_mxmo_rnvcn udtValorDecimales, -- Meses máximo para renovación del medicamento.
    mstra_mdca char(1) DEFAULT 'N', -- Indica si el medicamento es una muestra médica
    mdcmnto_uso_cndcndo char(1) DEFAULT 'N', -- Indica si el medicamente es de uso condicionado.
    prstcn_accso_drcto char(1) DEFAULT 'N', -- Indica si el medicamente es de acceso directo
    prstcn_cptda char(1), --Indica si la prestación se encuentra capitada, solo aplica a POS.
    prgrmcn_entrga char(1), --Indica si la prestación tiene fecha programada de entrega.
    crgo_drccnmnto udtConsecutivo, --Indica si la prestación tiene un cargo para ser direccionado.

    ds_dfrnca int, --Días diferencia entre la fecha de prestación y fecha de creación
    fcha_anno_antrr datetime, --Fecha de solicitud menos un año
	orgn_atncn INT
  )

  CREATE TABLE #tbResultadosValidaciones (
    id int IDENTITY (1, 1),
    cnsctvo_prcso int,
    cnsctvo_cdgo_rgla_vldcn int,
    cnsctvo_cdgo_no_cnfrmdd_vldcn int,
    llve_prmra_rgstro_vlddo int,  --- consecutivo solicitud
    infrmcn_adcnl_no_cnfrmdd varchar(250),
    llve_prmra_cncpto_prncpl_vlddo int
  )

  CREATE TABLE #tmpDatosDocumentosAnexos (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_prcso int,
    cnsctvo_cdgo_mdlo_dcmnto_sprte udtConsecutivo,
    cnsctvo_cdgo_dcmnto_sprte udtConsecutivo
  )

  Create Table #tempDocPrestacionesLoc(
	cnsctvo_cdgo_dcmnto_sprte      udtConsecutivo,
	cdgo_dcmnto_sprte              udtCodigo     ,
	dscrpcn_dcmnto_sprte           udtDescripcion,
	tmno_mxmo                      Int           ,
	url_ejmplo                     udtDescripcion,
	frmts_prmtdo                   Varchar(50)   ,
	oblgtro                        udtLogico     ,
	cntdd_mxma_sprts_crgr          udtConsecutivo,
	cnsctvo_cdgo_tpo_imgn          udtConsecutivo,
	cdgo_cdfccn                    Char(11)
	)

  CREATE TABLE #tbCalculoSolicitudNoConformidades (
    llve_prmra_rgstro_vlddo udtConsecutivo,
    llve_prmra_cncpto_prncpl_vlddo udtConsecutivo,
    cnsctvo_cdgo_grpo_rgla_vldcn udtConsecutivo,
    cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
    cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo,
    cnsctvo_cdgo_estdo_no_cnfrmdd udtConsecutivo,
    jrrqa_rgla_vldcn udtconsecutivo,
    jrrqa_estdo_no_cnfrmdd udtConsecutivo,
    jrqia_estdo_slccnda_no_cnfrmdd udtLogico DEFAULT 'N',
    jrqia_rgla_slccnda_no_cnfrmdd udtLogico DEFAULT 'N'
  )

  --Tabla temporal para la ejecución del SP de PAF y POC
  CREATE TABLE #tmpDatosSolicitudes (
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_cmpo_vldcn int,
    fcha datetime,
    nmro_unco_idntfcn_afldo int,
    cnsctvo_cdgo_dgnstco int,
    cnsctvo_cdgo_pln int,
    edd int,
    cnsctvo_cdgo_tpo_cdfccn int,
    cnsctvo_cdfccn int,
    cnsctvo_cdgo_frma_atncn int,
    cnsctvo_cdgo_cdd_rsdnca int,
    cdgo_intrno char(8),
    cnsctvo_cdgo_estdo_drcho int,
    rsltdo_vldcn char(1) DEFAULT 'N'
  )

  --Tabla para almacenar la información necesaria de atención y conceptos.
  CREATE TABLE #tmpAtencionConceptosOPS(
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_srvco_slctdo_rcbdo int,
	nmro_unco_idntfccn int,
    cntdd_prstcn decimal(3,0),
	fcha_slctd_orgn dateTime
  )

  DECLARE @cnsctvo_prcso udtConsecutivo = 0,
          @cnsctvo_lg udtConsecutivo,
          @cnsctvo_cdgo_tpo_prcso udtConsecutivo,
          @cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo,
          @mensajeError varchar(1000),
          @usuario udtUsuario,
          @estdo_prcso_ok numeric(1),
          @rspsta_mlla xml,
          @negacion char(1),
          @procesoInicioMalla numeric(2),
          @procesoPremalla numeric(2),
          @procesoCarga numeric(2),
          @procesoEjecutarReglas numeric(2),
          @procesoGuardar numeric(2),
          @procesoCalcularResultado numeric(2),
          @procesoPostMalla numeric(2),
          @indiceSolicitud numeric(3),
          @errorSolicitud varchar(100),
          @errorseverity int,
          @errorstate int,
		  @estadoEN Char(2),
		  @estadoOK Char(2),
		  @estadoET Char(2),
		  @mensajeExito VARCHAR(30);
  BEGIN

    SET @cnsctvo_cdgo_tpo_prcso = 15 --Tipo proceso para procesamiento en batch
    SET @cnsctvo_cdgo_tpo_prcso_lna = 16 -- Tipo proceso para procesamiento en Línea
    SET @estdo_prcso_ok = 2
    SET @negacion = 'N'
    SET @procesoInicioMalla = 57
    SET @procesoPremalla = 58
    SET @procesoCarga = 59
    SET @procesoEjecutarReglas = 60
    SET @procesoGuardar = 61
    SET @procesoCalcularResultado = 62
    SET @procesoPostMalla = 63
    SET @indiceSolicitud = 0
    SET @errorSolicitud = 'Una o más solicitudes no existen.'
	SET @estadoEN = 'EN'
	SET @estadoOK = 'OK'
	SET @estadoET = 'ET'
	SET @mensajeExito = 'Ejecución Exitosa';

    SELECT
      @indiceSolicitud = COUNT(id)
    FROM #tmpNmroSlctds so
    WHERE ISNULL(so.cnsctvo_slctd_srvco_sld_rcbda, '0') = 0;

    IF @indiceSolicitud = 0
    BEGIN
      IF @es_btch = @negacion BEGIN
      --Se controla la transacción sólo para poder actualizar el control del proceso.
      BEGIN TRY
        --Ejecución malla en línea.
		
		--Se procesa la pre-malla
		EXEC bdMallaCNA.dbo.spASCargarInformacionProcesoMalla	@cnsctvo_cdgo_tpo_prcso_lna,
																@es_btch,
																@usro

        --Se carga la información.
        EXEC bdMallaCNA.dbo.spASObtenerDatosGestionSolicitudMalla @cnsctvo_cdgo_tpo_prcso_lna,
                                                                  @es_btch

        --Se ejecutan las reglas de negocio.
        EXEC bdMallaCNA.dbo.spASEjecutarReglasPrestacionMalla	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																@cnsctvo_cdgo_mlla_vldcn,
																@es_btch

        --Se Guarda el resultado de la ejecución de las reglas.
        EXEC bdMallaCNA.dbo.spASGuardarResultadoMalla	@cnsctvo_prcso,
														@cnsctvo_cdgo_tpo_prcso,
														@es_btch

        --Se calcula el resultado de la malla con base en las reglas.											
        EXEC bdMallaCNA.dbo.spASCalcularResultadoMalla @cnsctvo_cdgo_tpo_prcso,
                                                       @es_btch

        --Se determina el estado de las prestaciones, y se realiza la actualización.
        EXEC bdMallaCNA.dbo.spASActualizarResultadoPosMalla @cnsctvo_prcso,
															@cnsctvo_cdgo_tpo_prcso,
                                                            @es_btch

        --Se retorna el resultado de la ejecución de la malla.
        EXEC bdMallaCNA.dbo.spASRetornarResultadoMalla @cnsctvo_cdgo_tpo_prcso,
                                                       @es_btch,
                                                       @msje_rspsta OUTPUT

		SELECT @estdo_ejccn = @estadoOK,
			   @msje_errr = @mensajeExito;

      END TRY
     BEGIN CATCH
        SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
        'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
        'Message:' + ERROR_MESSAGE() + CHAR(13) +
        'Procedure:' + ERROR_PROCEDURE();
		SET @estdo_ejccn = @estadoET;
      END CATCH
	  
	  END
      ELSE
      BEGIN

        SET @usuario = SUBSTRING(System_User, CHARINDEX('\', System_User) + 1, LEN(System_User))

      /*PASO 1  Crear Proceso */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraProceso @cnsctvo_cdgo_tpo_prcso,
                                                 @usuario,
                                                 @cnsctvo_prcso OUTPUT
        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Inicia Proceso de Malla de Gestión de Solicitudes En Línea.',
                                                          @procesoInicioMalla,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
		RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      /*PASO 2  Proceso Pre-Malla Mover información del modelo de datos Original al modelo del proceso */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Ejecución de pre-malla.',
                                                          @procesoPremalla,
                                                          @cnsctvo_lg OUTPUT
        --EXEC bdCna.dbo.spASCargarInformacionProcesoMalla @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso, @cdgs_slctd 'Inicia Proceso de Pre Malla Gestión Solicitudes.', 2,@cnsctvo_lg Output
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      /*PASO 3  Cargar información en tablas temporales para su procesamiento posterior */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Carga de Información en Temporales Malla Gestión Solicitudes.',
                                                          @procesoCarga,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdMallaCNA.dbo.spASObtenerDatosGestionSolicitudMalla @cnsctvo_cdgo_tpo_prcso_lna,
                                                                  @es_btch

        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      /*PASO 4  Ejecución de reglas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Ejecución de Reglas Malla Gestión Solicitudes.',
                                                          @procesoEjecutarReglas,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdMallaCNA.dbo.spASEjecutarReglasPrestacionMalla	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																@cnsctvo_cdgo_mlla_vldcn,
																@es_btch
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      /*PASO 5  Guardar resultado ejecución malla */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Guardar Resultado Reglas Malla Gestión Solicitudes.',
                                                          @procesoGuardar,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdMallaCNA.dbo.spASGuardarResultadoMalla	@cnsctvo_prcso,
														@cnsctvo_cdgo_tpo_prcso,
														@es_btch
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      /*PASO 6 Calcular resultado de la malla con base en las reglas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Calcular Resultado Reglas Malla Gestión Solicitudes.',
                                                          @procesoCalcularResultado,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdMallaCNA.dbo.spASCalcularResultadoMalla @cnsctvo_cdgo_tpo_prcso,
                                                       @es_btch
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH


      /*PASO 7 Calcular estado de las prestacione (Post-malla), y actualizar prestaciones. */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso,
                                                          'Calcular Resultado Reglas Malla Gestión Solicitudes.',
                                                          @procesoPostMalla,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdMallaCNA.dbo.spASActualizarResultadoPosMalla @cnsctvo_prcso,
															@cnsctvo_cdgo_tpo_prcso,
                                                            @es_btch
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @mensajeError = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @mensajeError
        RAISERROR (@mensajeError, 16, 2) WITH SETERROR
      END CATCH

      END
    END
    ELSE
    BEGIN
      SELECT @msje_errr = @errorSolicitud,
			 @estdo_ejccn = @estadoEN
    END

    DROP TABLE #tbCalculoSolicitudNoConformidades
    DROP TABLE #tmpDatosDocumentosAnexos
    DROP TABLE #tbResultadosValidaciones
    DROP TABLE #tmpDatosPrestacionesxSolicitud
    DROP TABLE #tmpDatosAfiliado
    DROP TABLE #tmpDatosSolicitud
    DROP TABLE #tmpDatosSolicitudes
	DROP TABLE #tmpAtencionConceptosOPS
  END
