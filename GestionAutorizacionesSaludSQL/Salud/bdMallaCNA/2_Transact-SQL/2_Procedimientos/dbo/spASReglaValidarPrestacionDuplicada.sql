USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionDuplicada]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionDuplicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si hay prestaciones duplicadas para D\>
				  <\D una misma solicitud.  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 16/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionDuplicada] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @constante1 numeric(1);
  SET @constante1 = 1;

  BEGIN

    WITH SUB ( cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_Cdfccn, cnsctvo_srvco_slctdo_rcbdo,fila)
    AS (SELECT
      cnsctvo_slctd_srvco_sld_rcbda,
      cnsctvo_Cdfccn,
	  cnsctvo_srvco_slctdo_rcbdo,
	  ROW_NUMBER() OVER (PARTITION BY cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_cdfccn ORDER BY cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_cdfccn) AS FILA
    FROM #tmpDatosPrestacionesxSolicitud)
    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        s.cnsctvo_slctd_srvco_sld_rcbda,
        s.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación repetida en la solicitud.'
      FROM SUB s
      WHERE s.fila > @constante1;

  END

GO
