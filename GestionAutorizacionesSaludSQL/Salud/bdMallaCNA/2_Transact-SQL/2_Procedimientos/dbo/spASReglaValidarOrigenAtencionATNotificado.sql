USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarOrigenAtencionATNotificado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarOrigenAtencionATNotificado
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el origen de atencion de la D\>
					 <\D solicitud es ACCIDENTE DE TRANSITO y el afiliado tiene una notificacion en D\>
					 <\D estado "Notificado" o "Confirmado"  para el mismo diagnostico principal que D\>
					 <\D llega en la solicitud D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 20/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarOrigenAtencionATNotificado] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoAT numeric(1),
          @consecutivoNotificado numeric(2),
          @consecutivoConfirmado numeric(2)

  SET @consecutivoAT = 2
  SET @consecutivoNotificado = 23
  SET @consecutivoConfirmado = 24

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación identificada como accidente de tránsito (SOAT).'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN bdSisalud.DBO.tbafiliadosMarcados m WITH (NOLOCK)
        ON (a.nmro_unco_idntfccn = m.nmro_unco_idntfccn
        AND c.cnsctvo_cdgo_dgnstco = m.cnsctvo_cdgo_dgnstco)
      WHERE m.cnsctvo_cdgo_clsfccn_evnto = @consecutivoAT --"ACCIDENTE TRANSITO" en tbClasificacionEventosNotificacion
      AND m.cnsctvo_cdgo_estdo_ntfccn IN (@consecutivoNotificado, @consecutivoConfirmado) -- Notificado y confirmado
    ;
  END

GO
