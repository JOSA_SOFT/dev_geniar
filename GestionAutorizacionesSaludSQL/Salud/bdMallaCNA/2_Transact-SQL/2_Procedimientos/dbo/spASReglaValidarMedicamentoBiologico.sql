USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarMedicamentoBiologico]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarMedicamentoBiologico
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestación es medicamento y  D\>
				  <\D tiene marca de biológico a la fecha de solicitud. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarMedicamentoBiologico] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 
  BEGIN
    DECLARE @consecutivoCums numeric(1),
            @consecutivoMarcaBiologico numeric(2);
    SET @consecutivoCums = 5
    SET @consecutivoMarcaBiologico = 10

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El medicamento es un medicamento biológico.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN bdSisalud.dbo.tbMarcasPrestacion d WITH (NOLOCK)
        ON (c.cnsctvo_cdfccn = d.cnsctvo_cdfccn
		AND c.fcha_prstcn BETWEEN d.inco_vgnca_mrca_prstcn AND d.fn_vgnca_mrca_prstcn)
      INNER JOIN bdSisalud.dbo.tbTiposMarcasPrestacion_vigencias tm WITH (NOLOCK)
        ON (tm.cnsctvo_cdgo_tpo_mrca_prstcn = d.cnsctvo_cdgo_tpo_mrca_prstcn
        AND c.fcha_prstcn BETWEEN tm.inco_vgnca AND tm.fn_vgnca
		AND tm.cnsctvo_cdgo_tpo_mrca_prstcn = @consecutivoMarcaBiologico)
      WHERE c.cnsctvo_cdgo_tpo_cdfccn = @consecutivoCums --Medicamentos
  END

GO
