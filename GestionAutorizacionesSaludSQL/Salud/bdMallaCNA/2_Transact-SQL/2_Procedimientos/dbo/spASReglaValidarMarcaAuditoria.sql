USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarMarcaAuditoria]    Script Date: 6/30/2017 3:52:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarMarcaAuditoria
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Valida en cupsxplanesxcargo si el cargo tiene asociada la marca  AUDITORIA    D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2016   FC\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jorge Rodriguez AM\>    
* Descripcion        : <\D   Se modifica filtro para que aplique las reglas de auditoria a CUPS solo si la prestación 
							 está en la tabla CupsxPlanxCargo. Para los CUMS y CUOS se debe enviar a auditoria.	D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                             Se modifica procedimiento para que no consulte el grupo auditor por descripcion si no por
							 consecutivo
					    DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/04/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                             Se modifica procedimiento para que valide todos los auditores que se encuentran
							 parametrizados en tbASFlujosCargos_Vigencias
					    DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 30/06/2017 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarMarcaAuditoria] 
   @prmtro_sp_vldcn varchar(30),
   @cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
   @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON 

  DECLARE	@vlr_uno                   Int             ,
			--@cnsctvo_cdgo_fljo_crgo    udtConsecutivo  ,
			@l_fecha                   Datetime        ,
			@vlr_cups                  Int             ,
			@infrmcn_adcnl_no_cnfrmdd  udtObservacion

  Set @vlr_uno		            = 1;
  Set @l_fecha                  = getDate();
  Set @vlr_cups					= 4;
  Set @infrmcn_adcnl_no_cnfrmdd = 'Prestacion para validacion de auditoria.'
    
  
  Create 
  Table  #tmpDatosPrestacionesxSolicitudAUDITORIA(cnsctvo_cdgo_rgla				udtConsecutivo,
												  cnsctvo_cdgo_no_cnfrmdd		udtConsecutivo,
											      cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo,
											      cnsctvo_srvco_slctdo_rcbdo	udtConsecutivo,
											      flg_mrca_dmi_slctd_srvco		int default 0,
											      cnsctvo_cdfccn				udtConsecutivo,
											      cnsctvo_cdgo_pln				udtConsecutivo,
											      cnsctvo_cdgo_tpo_cdfccn		udtConsecutivo
                                                 );


  Insert 
  Into   #tmpDatosPrestacionesxSolicitudAUDITORIA(cnsctvo_cdgo_rgla         , cnsctvo_cdgo_no_cnfrmdd, cnsctvo_slctd_srvco_sld_rcbda,
	                                              cnsctvo_srvco_slctdo_rcbdo, cnsctvo_cdfccn         , cnsctvo_cdgo_tpo_cdfccn
                                                 )
  SELECT     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, cnsctvo_cdfccn                , cnsctvo_cdgo_tpo_cdfccn
  FROM       #tmpDatosPrestacionesxSolicitud c

  UPDATE     dps
  SET        cnsctvo_cdgo_pln  = ias.cnsctvo_cdgo_pln
  FROM       #tmpDatosPrestacionesxSolicitudAUDITORIA dps
  Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias  WITH (NOLOCK)
  On         ias.cnsctvo_slctd_atrzcn_srvco = dps.cnsctvo_slctd_srvco_sld_rcbda
  

  UPDATE     dps
  SET        flg_mrca_dmi_slctd_srvco = @vlr_uno
  From       #tmpDatosPrestacionesxSolicitudAUDITORIA dps 
  Inner Join bdSisalud.dbo.tbCupsxPlanxCargo_Vigencias cpc WITH (NOLOCK)	
  On         cpc.cnsctvo_cdfccn   = dps.cnsctvo_cdfccn And 
             cpc.cnsctvo_cdgo_pln = dps.cnsctvo_cdgo_pln
  Inner Join bdSisalud.dbo.tbCargosSOS_Vigencias cv		 WITH (NOLOCK)		
  On         cv.cnsctvo_cdgo_crgo_ss = cpc.cnsctvo_cdgo_crgo
  Inner Join bdcna.prm.tbASFlujosCargos_Vigencias fcv WITH (NOLOCK)	
  On         fcv.cnsctvo_cdgo_fljo_crgo = cv.cnsctvo_cdgo_fljo_crgo   
  Where      @l_fecha BETWEEN cpc.inco_vgnca AND cpc.fn_vgnca
  And	     @l_fecha BETWEEN cv.inco_vgnca AND cv.fn_vgnca
  And	     @l_fecha BETWEEN fcv.inco_vgnca AND fcv.fn_vgnca


  --Se guardan en la temporal los registros de las prestaciones CUMS y CUOS
  INSERT 
  INTO   #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                    llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								   )
  SELECT @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
         c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  FROM   #tmpDatosPrestacionesxSolicitudAUDITORIA c
  WHERE  cnsctvo_cdgo_tpo_cdfccn  <> @vlr_cups
  

  --Se guardan en la temporal los registros de las prestaciones CUPS solo si estos estan el la tabla tbCupsxPlanxCargo_Vigencias	 	
  INSERT 
  INTO   #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                    llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								   )
  SELECT @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
         c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  FROM   #tmpDatosPrestacionesxSolicitudAUDITORIA c
  WHERE  flg_mrca_dmi_slctd_srvco = @vlr_uno
  And    cnsctvo_cdgo_tpo_cdfccn  = @vlr_cups
  
  DROP TABLE #tmpDatosPrestacionesxSolicitudAUDITORIA;
	
 END
