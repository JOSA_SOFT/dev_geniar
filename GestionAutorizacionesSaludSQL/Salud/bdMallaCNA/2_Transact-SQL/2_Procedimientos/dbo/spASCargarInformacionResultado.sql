USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarInformacionResultado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarInformacionResultado
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de cargar el resultado de la ejecución D\>
    			     <\D de las reglas de negocio y el cálculo obtenido. D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCargarInformacionResultado] @cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT ON 

  DECLARE @negacion char(1),
          @fecha datetime
  SELECT
    @negacion = 'N',
    @fecha = GETDATE()

  BEGIN

    IF @es_btch = @negacion
    BEGIN

      --Se carga la información del resultado de las validaciones.
      INSERT INTO #tbResultadosValidaciones (cnsctvo_prcso,
      cnsctvo_cdgo_rgla_vldcn,
      cnsctvo_cdgo_no_cnfrmdd_vldcn,
      llve_prmra_rgstro_vlddo,  --- consecutivo solicitud
      infrmcn_adcnl_no_cnfrmdd,
      llve_prmra_cncpto_prncpl_vlddo)
        SELECT
          p.cnsctvo_prcso,
          p.cnsctvo_cdgo_rgla_vldcn,
          p.cnsctvo_cdgo_no_cnfrmdd_vldcn,
          p.llve_prmra_rgstro_vlddo,
          p.infrmcn_adcnl_no_cnfrmdd,
          p.llve_prmra_cncpto_prncpl_vlddo
        FROM #tmpDatosSolicitud n
        INNER JOIN bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion p WITH (NOLOCK)
          ON (n.cnsctvo_prcso = p.cnsctvo_prcso
          AND p.llve_prmra_rgstro_vlddo = n.cnsctvo_slctd_srvco_sld_rcbda);

      --Se carga la información necesaria de las solicitudes para retornar la respuesta.
      INSERT INTO #tmpDatosPrestacionesxSolicitud (cnsctvo_srvco_slctdo_rcbdo,
      cnsctvo_cdfccn,
      cdgo_cdfccn,
      dscrpcn_estdo_prstcn,
      cnsctvo_estdo_prstcn,
      cnsctvo_cdgo_tpo_cdfccn,
      cnsctvo_slctd_srvco_sld_rcbda)
        SELECT
          b.cnsctvo_srvco_slctdo,
          b.cnsctvo_cdgo_srvco_slctdo,
          bo.cdgo_srvco_slctdo,
          en.dscrpcn_estdo_no_cnfrmdd,
          en.cnsctvo_cdgo_estdo_no_cnfrmdd,
          b.cnsctvo_cdgo_tpo_srvco,
          n.cnsctvo_slctd_srvco_sld_rcbda
        FROM #tmpDatosSolicitud n
        INNER JOIN bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)
          ON (n.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)
        LEFT JOIN bdCNA.gsa.tbASServiciosSolicitadosOriginal bo WITH (NOLOCK)
          ON (bo.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
          AND bo.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo)
        LEFT JOIN bdCNA.[prm].[tbASEstadosServiciosSolicitados_Vigencias] e WITH (NOLOCK)
          ON (e.cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_srvco_slctdo
          AND b.fcha_prstcn_srvco_slctdo BETWEEN e.inco_vgnca AND e.fn_vgnca)
        LEFT JOIN bdMallaCNA.dbo.tbEstadosNoConformidades_vigencias en WITH (NOLOCK)
          ON (en.cnsctvo_cdgo_estdo_no_cnfrmdd = e.cnsctvo_cdgo_estdo_no_cnfrmdd
          AND b.fcha_prstcn_srvco_slctdo BETWEEN e.inco_vgnca AND e.fn_vgnca)
      ;
    END
  END

GO
