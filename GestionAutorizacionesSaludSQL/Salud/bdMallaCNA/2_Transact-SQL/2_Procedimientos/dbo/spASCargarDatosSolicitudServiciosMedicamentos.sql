USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudServiciosMedicamentos]    Script Date: 7/10/2017 9:53:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudServiciosMedicamentos
* Desarrollado por : <\A Jois Lombana Sánchez  A\>    
* Descripcion      : <\D 
                         Procedimiento que se encarga de actualizar en la temporal de Servicios para realizar 
						 el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 26/11/21  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez G. - sisjvg01 AM\>    
* Descripcion        : <\D  
                           Se ajusta Sp Para Incluir al Modelo Capita los PGP (Pago Global Prospectivo)  
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-02-07 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando el prestador y el modelo de capitacion 
						   cuando se identifica que una solicitud esta capitada.
						   Adicionalmente se optimiza el procedimiento
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-03-02 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D  
                           Se retiran cruces de las tablas tbModeloCapitacionCiudades y tbDetModeloCapitacionCiudades
						   segun lo definido por Angela Sandoval
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-04-03 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D
                           Se agregan consultas para validar los parametros de las prestaciones de POS condicionado.
						   Se agrega validacion de genero, edad, diagnostico y prestacion.
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-05-16 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez García - sisjvg01 - AM\>    
* Descripcion        : <\D
                           Se Modifica para incluir la nueva validación de Convenios Pagos Fijos
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-07-06 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spASCargarDatosSolicitudServiciosMedicamentos] 

As
	
Begin
	Set NoCount On

	Declare @Valorcero    Int         ,
	        @ValorVacio   Varchar(1)  ,
	        @ValorS       varchar(1)  ,
	        @ValorN       varchar(1)  ,
	        @ValorA       varchar(1)  ,
	        @SinEntregar  int         ,
	        @Reprogramado Int         ,
	        @FormaFar     Varchar(6)  ,
	        @TServicio    Varchar(1)  ,
	        @Descripcion  varchar(100),
	        @Grupo        int         ,
	        @Variable4      int        ,
	 	    @TServicio_Med int        ,
	        @TServicio_Pro int        ,
	        @Valortres     int        ,
			@capita        Int        ,
			@capita_pgp    Int		  ,
			@Variable3	   Int		  ,
			@Variable2	   Int		  ,
			@Variable1	   Int		  ;

	Set	@Valorcero     = 0
	Set	@ValorVacio    = ''
	Set	@ValorS        = 'S'
	Set	@ValorN        = 'N'
	Set	@ValorA        = 'A'
	Set	@SinEntregar   = 151
	Set	@Reprogramado  = 152
	Set	@FormaFar      = '100052'
	Set	@TServicio     = '5' 
	Set	@TServicio_pro = 4 -- Procedimiento
	Set	@TServicio_Med = 5 -- Medicamento			
	Set	@Descripcion   = 'Meses maximos para el tramite de renovacion'
	Set	@Grupo         = 1
	Set	@Variable4      = 4
	Set @capita        = 4
	Set @capita_pgp    = 10
	Set @Valortres     = 3
	Set @Variable3	   = 3;
	Set @Variable2	   = 2;
	Set @Variable1	   = 1;

	Create Table #tbPrestacionCapitada
	(
		cnsctvo_slctd_srvco_sld_rcbda udtconsecutivo, 
		cnsctvo_cdgo_srvco_slctdo     udtconsecutivo, 
		prstcn_cptda                  udtlogico     ,
		cnsctvo_cdgo_tpo_mdlo_cpta    udtconsecutivo, 
		nmro_unco_prstdr_cptdo        UdtConsecutivo,
 	)

	Create Table  #tbProgramacionEntrega 
	(
		cnsctvo_slctd_srvco_sld_rcbda udtconsecutivo
	)

	Create Table #tempTbDefinicionGrupoDiagnostico 
	(
		cnsctvo_cdgo_dfncn_grpo			udtConsecutivo, 
		cnsctvo_slctd_srvco_sld_rcbda	udtConsecutivo,
		Cod_Servicio					Char(11),
		Cod_Sexo						Char(1),
		Edad_Anual						Int
	)

	--sisjvg01 - 2017-07-06 - Creación Temporal Validación Pagos Fijos
	Create table #TmpValidarConveniosPagosFijos
	(
		
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		fcha_cnslta						Datetime,
		nmro_unco_afldo					UdtConsecutivo,
		cnsctvo_cdgo_pln				UdtConsecutivo,
		cnsctvo_prstcn					UdtConsecutivo,
		cdgo_intrno_slctnte				UdtCodigoIps,
		cdgo_intrno_dstntrio			UdtCodigoIps,
		cnsctvo_cdgo_sde				UdtConsecutivo,
		cnsctvo_cdgo_clsfccn_atncn		UdtConsecutivo,
	)

	--sisjvg01 - 2017-07-06 - Creación Temporal Validación Pagos Fijos
	Create Table #TmpConveniosPagoFijos 
	(  
		cdgo_cdfccn						char(11),  
		dscrpcn_cdfccn					UdtDescripcion,
		nmbre_scrsl						UdtDescripcion,
		dscrpcn_mdlo_cnvno				UdtDescripcion,
		dscrpcn_pln						UdtDescripcion,
		nmro_unco_prstdr                UdtConsecutivo,
		cnsctvo_cdgo_tpo_mdlo			UdtConsecutivo,				
		nmro_unco_afldo					UdtConsecutivo,
		cnvndacn						int default 0,
		------------------------------------------------
		cdgo_intrno_dstntrio            UdtCodigoIps,
		cnsctvo_cdgo_mdlo_cnvno         UdtConsecutivo default null,
		cnsctvo_prstcn					UdtConsecutivo,
		nmbre_scrsl1                    UdtDescripcion,
	)  

	/*::::::::::::::::COMPLETAR SERVICIOS SOLICITADOS::::::::::::::::::::::::::::::*/	
	-- Insertar informacion de servicios Temporal

	Insert 
	Into   #Srvcs(cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_srvco_slctdo_rcbdo, Servicio_Solicitado,
	              Tipo                         , Unidad_Tiempo             , Tipo_Autorizacion  ,
				  Prestacion_Prestador         , Cargo_direccionamiento    , Indicador_no_Pos   ,
				  Prestacion_Vigente           , Prestacion_Capitada       , Programa_Entrega   ,
				  Cod_Tipo_serv                , Cod_prstcn_prstdr         , Cod_Servicio_sol   ,
				  Cod_Unidad_Tiempo_tratam     , Cod_Tipo_autorizacion     , Cod_Servicio
	             )
	Select			 TSV.cnsctvo_slctd_atrzcn_srvco				--cnsctvo_slctd_srvco_sld_rcbda
					,TSV.cnsctvo_srvco_slctdo					--cnsctvo_srvco_slctdo_rcbdo					
					,TSV.cnsctvo_cdgo_srvco_slctdo				--Servicio_Solicitado
					,TSV.cnsctvo_cdgo_tpo_srvco					--Tipo 
					,TSV.cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	--Unidad_Tiempo 
					,TSV.cnsctvo_cdgo_tpo_atrzcn				--Tipo_Autorizacion
					,TSV.cnsctvo_cdgo_prstcn_prstdr				--Prestacion_Prestador 
					,TSV.crgo_drccnmnto							--Cargo_direccionamiento
					,TSV.indcdr_no_ps							--Indicador_no_Pos
					,TSV.prstcn_vgnte							--Prestacion_Vigente
					,TSV.prstcn_cptda							--Prestacion_Capitada
					,TSV.prgrmcn_entrga							--Programa_Entrega
					,TSE.cdgo_tpo_srvco							--Cod_Tipo_serv	
					,TSE.cdgo_prstcn_prstdr						--Cod_prstcn_prstdr
					,TSE.cdgo_srvco_slctdo						--Cod_Servicio_sol
					,TSE.cdgo_undd_tmpo_trtmnto_slctdo			--Cod_Unidad_Tiempo_tratam
					,TSE.cdgo_tpo_atrzcn						--Cod_Tipo_autorizacion
					,LTRIM(RTRIM(TSE.cdgo_srvco_slctdo))		--Cod_Servicio
	From			#tmpNmroSlctds								TSO WITH (NOLOCK)
	Inner Join		BdCNA.gsa.tbASServiciosSolicitados			TSV WITH (NOLOCK)
	On				TSO.cnsctvo_slctd_srvco_sld_rcbda = TSV.cnsctvo_slctd_atrzcn_srvco
	Inner Join   	BdCNA.gsa.tbASServiciosSolicitadosOriginal	TSE WITH (NOLOCK)
	On				TSV.cnsctvo_slctd_atrzcn_srvco    = TSE.cnsctvo_slctd_atrzcn_srvco And 
	                TSV.cnsctvo_srvco_slctdo          = TSE.cnsctvo_srvco_slctdo

	-- Insertar informacion de Validar Convenios Pagos Fijos Temporal -sisjvg01- 2017-07-06
	Insert		#TmpValidarConveniosPagosFijos
	(
				cnsctvo_slctd_atrzcn_srvco,					cnsctvo_srvco_slctdo,				fcha_cnslta,
				nmro_unco_afldo,							cnsctvo_cdgo_pln,					cnsctvo_prstcn,
				cdgo_intrno_slctnte
	)
	Select 
				SOP.cnsctvo_slctd_srvco_sld_rcbda,			SER.cnsctvo_srvco_slctdo_rcbdo,		SOP.Fecha_sol,
			    TAF.NUI,									TAF.Codigo_Plan,					SER.Servicio_Solicitado,
				IPS.Cdgo_intrno_slctnte
	From		#Slctds SOP
	Inner Join	#Srvcs  SER
	On			SER.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	#Aflds TAF
	On			TAF.cnsctvo_slctd_srvco_sld_rcbda= SOP.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join  #IPSSl IPS
	On          IPS.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda

	
	-- Calcular cnsctvo_cdgo_tpo_srvco
	IF EXISTS (	Select	Tipo
				From	#Srvcs
				Where	Tipo = @Valorcero
				AND		Cod_Tipo_serv <> @ValorVacio)
		BEGIN 
			UPDATE		#Srvcs
			SET			Tipo = Isnull(TSE.cnsctvo_cdgo_tpo_cdfccn, @Valorcero)
			From		#Srvcs  SER 
			Inner Join	#Slctds SOP	
			On			SER.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbTipoCodificacion_Vigencias TSE WITH (NOLOCK)
			On			SER.Cod_Tipo_serv = TSE.cdgo_tpo_cdfccn			
			Where		SER.Tipo = @Valorcero		
			And         SOP.Fecha_sol Between TSE.inco_vgnca And TSE.fn_vgnca
		END

	-- Calcular cnsctvo_cdgo_prstcn_prstdr
	IF EXISTS (	Select	Prestacion_Prestador
				From	#Srvcs
				Where	Prestacion_Prestador = @Valorcero
				AND		Cod_prstcn_prstdr   <> @ValorVacio)
		BEGIN 
			Update		#Srvcs
			Set			Prestacion_Prestador = Isnull(PPR.cnsctvo_cdgo_prstcn_prstdr,@Valorcero)
			From		#Srvcs	SER
			Inner Join	#Slctds	SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbPrestacionesPrestador PPR WITH (NOLOCK)
			On			PPR.cdgo_prstcn_prstdr = SER.Cod_prstcn_prstdr
			Where		SER.Prestacion_Prestador = @Valorcero		
		END
	
	-- Calcular cnsctvo_cdgo_srvco_slctdo Primera Opcion
	IF EXISTS (	Select	Servicio_Solicitado
				From	#Srvcs 
				Where	Servicio_Solicitado = @Valorcero 
				AND		Cod_Servicio_sol   <> @ValorVacio)
		BEGIN
			Update		#Srvcs
			Set			Servicio_Solicitado = ISNULL(COD.cnsctvo_cdfccn,@Valorcero)
			From		#Srvcs  SER
			Inner Join	#Slctds	SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbTipoCodificacion_vigencias TCV	WITH (NOLOCK)
			On			TCV.cdgo_tpo_cdfccn = SER.Cod_Tipo_serv			
			Inner Join	bdSisalud.dbo.tbCodificaciones COD WITH (NOLOCK)
			On			SER.Cod_Servicio_sol = COD.cdgo_cdfccn
			And			SER.Tipo             = TCV.cnsctvo_cdgo_tpo_cdfccn
			Where		SER.Servicio_Solicitado = @Valorcero		
			And         SOP.Fecha_sol BETWEEN TCV.inco_vgnca AND TCV.fn_vgnca
		END
		
	-- Calcular cnsctvo_cdgo_srvco_slctdo Segunda y última Opción
	IF EXISTS (	Select	Servicio_Solicitado
				From	#Srvcs
				Where	Servicio_Solicitado = @Valorcero)
		BEGIN
			Update		#Srvcs
			Set			Servicio_Solicitado = ISNULL(HPR.cnsctvo_cdfccn,@Valorcero)
			From		#Srvcs  SER 
			Inner Join	#Slctds	SOP	
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#Aflds TAF
			On			TAF.cnsctvo_slctd_srvco_sld_rcbda= SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbHomologacionPrestaciones HPR WITH (NOLOCK)
			On			HPR.cdgo_intrno                = TAF.Ips                  And 
						HPR.cnsctvo_cdgo_prstcn_prstdr = SER.Prestacion_Prestador
			Where		SER.Servicio_Solicitado = @Valorcero		
		END		
	
	-- Calcular cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo
	IF EXISTS (	Select	Unidad_Tiempo
				From	#Srvcs 
				Where	Unidad_Tiempo = @Valorcero
				AND		Cod_Unidad_Tiempo_tratam <> @ValorVacio)
		BEGIN 
			Update		#Srvcs
			Set			Unidad_Tiempo = Isnull(TIE.cnsctvo_cdgo_frcnca,@Valorcero)
			From		#Srvcs  SER 
			Inner Join	#Slctds SOP	
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbMNFrecuencia_Vigencias	TIE WITH (NOLOCK)
			On			SER.Cod_Unidad_Tiempo_tratam = TIE.cdgo_frcnca
			Where		SOP.Fecha_sol Between TIE.inco_vgnca And TIE.fn_vgnca
			And			SER.Unidad_Tiempo = @Valorcero								
		END

	-- Calcular cnsctvo_cdgo_tpo_atrzcn
	IF EXISTS (	Select	Tipo_Autorizacion
				From	#Srvcs  
				Where	Tipo_Autorizacion      = @Valorcero
				And		Cod_Tipo_autorizacion <> @ValorVacio)
		BEGIN 
			Update		#Srvcs
			Set			Tipo_Autorizacion = Isnull(AUT.cnsctvo_cdgo_tpo_atrzcn,@Valorcero)
			From		#Srvcs	SER WITH (NOLOCK)
			Inner Join	#Slctds	SOP	WITH (NOLOCK)
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbTiposAutorizacion_Vigencias	AUT WITH (NOLOCK)
			On			AUT.cdgo_tpo_atrzcn = SER.Cod_Tipo_autorizacion
			Where		SOP.Fecha_sol Between AUT.inco_vgnca And AUT.fn_vgnca
			And			SER.Tipo_Autorizacion = @Valorcero					
		END
	
	-- Calcular crgo_drccnmnto
	IF EXISTS (	Select	Cargo_Direccionamiento
				From	#Srvcs
				Where	Cargo_Direccionamiento = @Valorcero)
		BEGIN
			Update		#Srvcs
			Set			Cargo_Direccionamiento = ISNULL(cnsctvo_cdgo_crgo,@Valorcero)
			From		bdSisalud.dbo.tbCupsxPlanxCargo	CUP WITH(NOLOCK)
			Inner Join	#Srvcs TSE
			On			TSE.Servicio_Solicitado = CUP.cnsctvo_cdfccn

		END

	-- Calcular indcdr_no_ps
	IF EXISTS (	Select	Indicador_no_Pos
				From	#Srvcs
				Where	Indicador_no_Pos = @ValorVacio)
		BEGIN
			Update		#Srvcs
			Set			Indicador_no_Pos = ISNULL(CUM.indcdr_no_ps,@ValorVacio)
			From		#Srvcs	SER
			Inner Join	#Slctds	SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdSisalud.dbo.tbcums CUM WITH (NOLOCK)
			On			CUM.cnsctvo_cms = SER.Servicio_Solicitado
			Where		SOP.Fecha_sol Between CUM.inco_vgnca And CUM.fn_vgnca
			And			SER.Indicador_no_Pos = @ValorVacio
			And			SER.Tipo             = @TServicio_Med
		END

	IF EXISTS (	Select	Indicador_no_Pos
				From	#Srvcs
				Where   Indicador_no_Pos = @ValorVacio)
		BEGIN
			Update		#Srvcs
			Set			Indicador_no_Pos = ISNULL(CUP.no_pos,@ValorVacio)
			From		#Srvcs	SER 
			Inner Join	#Slctds	SOP	
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join  bdSisalud.dbo.tbCupsServicios CUP WITH (NOLOCK)
			On			CUP.cnsctvo_prstcn = SER.Servicio_Solicitado
			Where		SOP.Fecha_sol Between CUP.inco_vgnca And CUP.fn_vgnca
			And			SER.Indicador_no_Pos = @ValorVacio
			And			SER.Tipo             = @TServicio_Pro
		END

	-- Calcular prstcn_vgnte CUPS
	IF EXISTS (	Select	Prestacion_Vigente
				From	#Srvcs
				Where	Prestacion_Vigente = @ValorVacio
				AND		Tipo               = @TServicio_Med)
		BEGIN
			Update		#Srvcs
			Set			Prestacion_Vigente = CASE WHEN ISNULL(CUM.cnsctvo_cms, '') = @ValorVacio THEN @ValorN
												                                                 ELSE @ValorS
											 END
			From		#Srvcs	SER
			Inner Join	#Slctds	SOP	
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Left Join	bdSisalud.dbo.tbcums_vigencias	CUM WITH (NOLOCK)
			On			CUM.cnsctvo_cms = SER.Servicio_Solicitado
			Where    	SOP.Fecha_sol Between CUM.inco_vgnca And CUM.fn_vgnca
			And		    SER.Prestacion_Vigente = @ValorVacio
			And			SER.Tipo               = @TServicio_Med
		END

	-- Calcular prstcn_vgnte CUPS
	IF EXISTS (	Select	Prestacion_Vigente
				From	#Srvcs
				Where	Prestacion_Vigente = @ValorVacio
				AND		Tipo               = @TServicio_pro)
		BEGIN
			Update		#Srvcs
			Set			Prestacion_Vigente = CASE WHEN ISNULL(CUP.cnsctvo_prstcn, '') = @ValorVacio THEN @ValorN
												                                                    ELSE @ValorS
											 END
			From		#Srvcs	SER
			Inner Join	#Slctds SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Left Join	bdSisalud.dbo.tbCupsServicios_vigencias	CUP WITH (NOLOCK)
			On			CUP.cnsctvo_prstcn = SER.Servicio_Solicitado
			Where		SOP.Fecha_sol Between CUP.inco_vgnca And CUP.fn_vgnca
			And		    SER.Prestacion_Vigente = @ValorVacio
			And			SER.Tipo               = @TServicio_pro
		END
	
	-- sisjvg01 - 2017-07-06
	-- Calcular si la prestación esta en convenios Pagos Fijos - prstcn_cptda CAMPO QUE SIEMPRE SE DEBE CALCULAR
	Begin
		-- Validamos si la solicitud se encuentra en un convenio cápita y/o pago fijo
		Insert	#TmpConveniosPagoFijos 
		(  
				cdgo_cdfccn,  					dscrpcn_cdfccn,						nmbre_scrsl,
				dscrpcn_mdlo_cnvno,				dscrpcn_pln,						nmro_unco_prstdr,
				cnsctvo_prstcn,					cnsctvo_cdgo_tpo_mdlo,				nmro_unco_afldo	
		)
		Exec bdSiSalud.dbo.spValidarConveniosPagosFijos null,null,null,null,null,null,null,null

		Insert		#tbPrestacionCapitada
		(
					cnsctvo_slctd_srvco_sld_rcbda,				cnsctvo_cdgo_srvco_slctdo,			  prstcn_cptda,
					cnsctvo_cdgo_tpo_mdlo_cpta,					nmro_unco_prstdr_cptdo
        )
		Select		
					t1.cnsctvo_slctd_atrzcn_srvco,				t1.cnsctvo_prstcn,						@ValorS, 
					t.cnsctvo_cdgo_tpo_mdlo,					t.nmro_unco_prstdr
		From		#TmpConveniosPagoFijos t
		Inner Join  #TmpValidarConveniosPagosFijos t1
		On          t1.nmro_unco_afldo = t.nmro_unco_afldo
		And         t1.cnsctvo_prstcn = t.cnsctvo_prstcn

		Update		#Srvcs
		Set			Prestacion_Capitada         = PRC.prstcn_cptda              ,
				    cnsctvo_cdgo_tpo_mdlo_cpta  = PRC.cnsctvo_cdgo_tpo_mdlo_cpta,
					nmro_unco_prstdr_cptdo      = PRC.nmro_unco_prstdr_cptdo
		From		#tbPrestacionCapitada PRC
		Inner Join	#Srvcs				  SER	
		On			PRC.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda 
		AND		    PRC.cnsctvo_cdgo_srvco_slctdo     = SER.Servicio_Solicitado

	End

/*	-- sisjvg01 - 2017-02-17
	-- Calcular si la prestación esta capitada en convenios - prstcn_cptda CAMPO QUE SIEMPRE SE DEBE CALCULAR	
	BEGIN
		If Exists ( Select 1 From #TmpAfiliadosCapitados)
			Begin
				-- Validar Capita Y PGP

				Insert 
				Into        #tbPrestacionCapitada(cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_cdgo_srvco_slctdo, prstcn_cptda,
				                                  cnsctvo_cdgo_tpo_mdlo_cpta   , cdgo_intrno_cpta
												 )
				Select		SER.cnsctvo_slctd_srvco_sld_rcbda, SER.Servicio_Solicitado, CASE WHEN Count(MCV.cnsctvo_cdgo_tpo_cntrto) > @Valorcero THEN @ValorS 
				                                                                                                                                   ELSE @ValorN 
																						END,
                            a.cnsctvo_cdgo_tpo_mdlo          , a.cdgo_intrno
				From		#Srvcs  SER 
				Inner Join	#Slctds	SOP 
				On          SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
				Inner Join	#Aflds TAF 
				On	        TAF.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
				Inner Join  #TmpAfiliadosCapitados TAC
				On          TAC.cnsctvo_slctd_atrzcn_srvco = TAF.cnsctvo_slctd_srvco_sld_rcbda AND 
				            TAC.nmro_unco_idntfccn_afldo   = TAF.nui
				Inner Join	BDAfiliacionValidador.dbo.tbmatrizcapitacionvalidador MCV WITH (NOLOCK) 
				On	        MCV.nmro_unco_idntfccn = TAF.nui
--				Inner Join	bdafiliacionvalidador.dbo.tbEscenarios_procesovalidador EPV WITH (NOLOCK)   
--              On	        EPV.cdgo_cnvno       = MCV.cdgo_cnvno       AND 
--                          EPV.cnsctvo_cdgo_cdd = TAC.cnsctvo_cdgo_cdd AND	
--                          SOP.Fecha_sol BETWEEN EPV.fcha_dsde AND EPV.fcha_hsta
				Inner Join	bdSisalud.dbo.tbModeloConveniosCapitacion MCA WITH (NOLOCK)	
				On	        MCA.cdgo_mdlo_cnvno_cptcn = MCV.cdgo_cnvno																										
				Inner Join	bdSisalud.dbo.tbdetModeloConveniosCapitacion MCB WITH (NOLOCK) 
				On          MCB.cnsctvo_cdgo_mdlo_cnvno_cptcn = MCA.cnsctvo_cdgo_mdlo_cnvno_cptcn																										
				Inner Join	bdSisalud.dbo.tbDetModeloConveniosCapitacionPrestaciones MCC WITH (NOLOCK)  
				On          MCC.cnsctvo_cdgo_mdlo_cnvno_cptcn_prstcn = MCB.cnsctvo_cdgo_mdlo_cnvno_cptcn_prstcn  And  
				            MCC.cnsctvo_prstcn  					 = SER.Servicio_solicitado
				
				Inner Join  bdsisalud.dbo.tbAsociacionModeloActividad a WITH (NOLOCK)	
				On          a.cnsctvo_mdlo_cnvno_pln =  MCA.cnsctvo_cdgo_mdlo_cnvno_cptcn 
				--Where		SER.Prestacion_Capitada = @ValorVacio
				Where         a.cnsctvo_cdgo_tpo_mdlo In (@capita, @capita_pgp)  -- tipo modelo capitación 4. Capita - 10 CapitaPGP
				And         SOP.Fecha_sol Between MCV.fcha_dsde And MCV.fcha_hsta  
				And         SOP.Fecha_sol Between MCA.inco_vgnca And MCA.fn_vgnca 
				And	        SOP.Fecha_sol Between MCB.inco_vgnca And MCB.fn_vgnca
				And         SOP.Fecha_sol Between MCC.inco_vgnca And MCC.fn_vgnca
				And         SOP.Fecha_sol Between a.inco_vgnca   And a.fn_vgnca
				GROUP BY	SER.cnsctvo_slctd_srvco_sld_rcbda, SER.Servicio_Solicitado, 
				            a.cnsctvo_cdgo_tpo_mdlo          , a.cdgo_intrno

				-- fin Validación Capitacion CAPITA Y PGP - sisjvg01- 2017-02-08

				Update		#Srvcs
				Set			Prestacion_Capitada        = PRC.prstcn_cptda              ,
				            cnsctvo_cdgo_tpo_mdlo_cpta = PRC.cnsctvo_cdgo_tpo_mdlo_cpta,
							cdgo_intrno_cpta           = PRC.cdgo_intrno_cpta
				From		#tbPrestacionCapitada PRC
				Inner Join	#Srvcs				  SER	
				On			PRC.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda AND
				            PRC.cnsctvo_cdgo_srvco_slctdo     = SER.Servicio_Solicitado
			End		
	END
*/	
	-- Calcular prgrmcn_entrga
	IF EXISTS (	Select	Programa_Entrega
				From	#Srvcs
				Where	Programa_Entrega = @ValorVacio)
		Begin 
		    Insert 
			Into        #tbProgramacionEntrega(cnsctvo_slctd_srvco_sld_rcbda)
			Select		SER.cnsctvo_slctd_srvco_sld_rcbda
			From		#Srvcs  SER 
			Inner Join	#Slctds	SOP 
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#Aflds	TAF
			On			TAF.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join  bdSISalud.dbo.tbActuaNotificacion actua With(Nolock)
			On          actua.nmro_unco_idntfccn = TAF.NUI
			Inner Join  bdSISalud.dbo.tbProgramacionPrestacion a   With(Nolock)  
			On	        a.cnsctvo_cdgo_ntfccn = actua.cnsctvo_ntfccn     And 
			            a.cnsctvo_cdgo_ofcna  = actua.cnsctvo_cdgo_ofcna And 
						a.cnsctvo_prstcn      = SER.Servicio_Solicitado
			Inner Join  bdSISalud.dbo.tbAfiliadosMarcados afi  With(Nolock)  
			On          afi.cnsctvo_ntfccn     = actua.cnsctvo_ntfccn And 
			            afi.cnsctvo_cdgo_ofcna = actua.cnsctvo_cdgo_ofcna --esta tabla tambien se puede relacionar con tbActuaNotificacion
			Inner Join  bdSISalud.dbo.tbDetProgramacionFechaEvento b With(Nolock) 
			On          b.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn            
			Inner Join  bdsisalud.dbo.tbDetProgramacionProveedores e With(Nolock) 
			On          e.cnsctvo_prgrmcn_prstcn   = b.cnsctvo_prgrmcn_prstcn   And 
			            e.cnsctvo_det_prgrmcn_fcha = b.cnsctvo_det_prgrmcn_fcha
			Where       e.estdo = @ValorA
			--Posibles filtros requeridos en MEGA_PIU_080_Consulta de Programaciones de Entrega,       
			And         b.fcha_entrga >= SOP.Fecha_sol -- tenga una fecha de entrega vigente
			And         b.cnsctvo_cdgo_estds_entrga In (@SinEntregar, @reprogramado) 

		    IF EXISTS (Select cnsctvo_slctd_srvco_sld_rcbda From #tbProgramacionEntrega) --esta en estado sin entregar o reprogramado
				BEGIN
					UPDATE	#Srvcs
					SET		Programa_Entrega = @ValorS
					Where	Programa_Entrega = @ValorVacio
				END
			ELSE
				BEGIN
					UPDATE	#Srvcs
					SET		Programa_Entrega = @ValorN
					Where	Programa_Entrega = @ValorVacio
				END
		End
	
			
	/*::::::::::::::::COMPLETAR MEDICAMENTOS::::::::::::::::::::::::::::::*/
	-- Insertar informacion medicamentos temporal
	Insert into #Mdcmnts
	Select		TSV.cnsctvo_slctd_atrzcn_srvco			--cnsctvo_slctd_srvco_sld_rcbda
				,MEP.cnsctvo_srvco_slctdo				--cnsctvo_srvco_slctdo_rcbdo
				,MEP.cnsctvo_cdgo_prsntcn_dss			--Presentacion_Dosis 
				,MEP.cnsctvo_cdgo_undd_prdcdd_dss		--Unidad_Periodicidad_Dosis 
				,MEP.cnsctvo_cdgo_frma_frmctca			--Forma_Farmaceutica 
				,MEP.cnsctvo_cdgo_grpo_trptco			--Grupo 
				,MEP.cnsctvo_cdgo_prsntcn				--Prestacion 
				,MEP.cnsctvo_cdgo_va_admnstrcn_mdcmnto	--Administracion 
				,MEP.cnsctvo_cdgo_undd_cncntrcn_dss		--Concentracion_Unidad 
				,MEP.fcha_vncmnto_invma					--Fecha_Venc_Invima
				,MEP.mstra_mdca							--Muestra_Medica
				,MEP.uso_cndcndo						--Uso_condicionado
				,MEP.mss_mxmo_rnvcn						--Meses_Max_Renovacion
				,MEO.cdgo_prsntcn_dss					--Cod_Presentacion_dosis
				,MEO.cdgo_undd_prdcdd_dss				--Cod_Unidad_Periodicidad
				,MEO.cdgo_frma_frmctca					--Cod_forma_farmaceutica
				,MEO.cdgo_grpo_trptco					--Cod_grupo_terapeutico
				,MEO.cdgo_prsntcn						--Cod_Presentacion
				,MEO.cdgo_va_admnstrcn_mdcmnto			--Cod_Via_administracion
				,MEO.cdgo_undd_cncntrcn_dss				--Cod_Unidad_concentracion
	From		#tmpNmroSlctds	TSO 
	Inner Join	BdCNA.gsa.tbASServiciosSolicitados TSV WITH (NOLOCK)
	On			TSO.cnsctvo_slctd_srvco_sld_rcbda = TSV.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BdCNA.gsa.tbASMedicamentosSolicitados MEP WITH (NOLOCK)
	On			TSV.cnsctvo_srvco_slctdo = MEP.cnsctvo_srvco_slctdo
	Inner Join	BdCNA.gsa.tbASMedicamentosSolicitadosOriginal MEO WITH (NOLOCK)
	On			MEP.cnsctvo_mdcmnto_slctdo = MEO.cnsctvo_mdcmnto_slctdo
	
	
	-- Calcular cnsctvo_cdgo_prsntcn_dss			
	IF EXISTS (	Select	Presentacion_Dosis
				From	#Mdcmnts 
				Where	Presentacion_Dosis = @Valorcero
				AND		Cod_Presentacion_dosis <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Presentacion_Dosis = Isnull(PDO.cnsctvo_cdgo_prsntcn,@Valorcero)
			From		#Mdcmnts MEP 
			Inner Join	#Slctds	 SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda		
			Inner Join	bdSisalud.dbo.tbpresentacion_vigencias	PDO WITH (NOLOCK)
			On			PDO.cdgo_prsntcn = MEP.Cod_Presentacion_dosis
			Where		SOP.fecha_sol Between PDO.inco_vgnca And PDO.fn_vgnca
			And			MEP.Presentacion_Dosis = @Valorcero	
		END

	-- Calcular cnsctvo_cdgo_undd_prdcdd_dss
	IF EXISTS (	Select	Unidad_Periodicidad_Dosis
				From	#Mdcmnts
				Where	Unidad_Periodicidad_Dosis = @Valorcero
				AND		Cod_Presentacion_dosis <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Unidad_Periodicidad_Dosis = Isnull(FRE.cnsctvo_cdgo_frcnca,@Valorcero)
			From		#Mdcmnts MEP 
			Inner Join	#Slctds	 SOP 
			On		    SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda		
			Inner Join	bdSisalud.dbo.tbMNFrecuencia_Vigencias	FRE WITH (NOLOCK)
			On			FRE.cdgo_frcnca = MEP.Cod_Presentacion_dosis
			Where		SOP.fecha_sol Between FRE.inco_vgnca And FRE.fn_vgnca
			And			MEP.Unidad_Periodicidad_Dosis = @Valorcero				
		END

	-- Calcular cnsctvo_cdgo_frma_frmctca
	IF EXISTS (	Select	Forma_Farmaceutica
				From	#Mdcmnts
				Where	Forma_Farmaceutica = @Valorcero
				AND		Cod_forma_farmaceutica <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Forma_Farmaceutica = Isnull(FFA.cnsctvo_cdgo_frma_frmctca,@Valorcero)
			From		#Mdcmnts MEP 
			Inner Join	#Slctds	 SOP 
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda		
			Inner Join	bdSisalud.dbo.tbFormaFarmaceutica_Vigencias	FFA WITH (NOLOCK)
			On			FFA.cdgo_frma_frmctca = MEP.Cod_forma_farmaceutica
			Where		SOP.fecha_sol Between FFA.inco_vgnca And FFA.fn_vgnca
			And			MEP.Forma_Farmaceutica = @Valorcero					
		END

	-- Calcular cnsctvo_cdgo_grpo_trptco			
	IF EXISTS (	Select	Grupo
				From	#Mdcmnts
				Where	Grupo = @Valorcero
				AND		Cod_grupo_terapeutico <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Grupo = Isnull(GTP.cnsctvo_cdgo_grpo_trptco,@Valorcero)
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP 
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda			
			Inner Join	bdSisalud.dbo.tbGruposTerapeutico_Vigencias	GTP WITH (NOLOCK)
			On			MEP.Cod_grupo_terapeutico = GTP.cdgo_grpo_trptco
			Where		SOP.fecha_sol Between GTP.inco_vgnca And GTP.fn_vgnca
			And			MEP.Grupo = @Valorcero		
		END

	-- Calcular cnsctvo_cdgo_prsntcn				
	IF EXISTS (	Select	Prestacion
				From	#Mdcmnts 
				Where	Prestacion = @Valorcero
				AND		Cod_Presentacion <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Prestacion = Isnull(PRE.cnsctvo_cdgo_prsntcn,@Valorcero)
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda		
			Inner Join	bdSisalud.dbo.tbPresentacion_Vigencias	PRE WITH (NOLOCK)
			On			PRE.cdgo_prsntcn =  MEP.Cod_Presentacion
			Where		SOP.fecha_sol Between PRE.inco_vgnca And PRE.fn_vgnca
			And			MEP.Prestacion = @Valorcero
		END

	-- Calcular cnsctvo_cdgo_va_admnstrcn_mdcmnto	
	IF EXISTS (	Select	Administracion
				From	#Mdcmnts
				Where	Administracion = @Valorcero
				AND		Cod_Via_administracion <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Administracion = Isnull(VAM.cnsctvo_cdgo_va_admnstrcn,@Valorcero)
			From		#Mdcmnts MEP 
			Inner Join	#Slctds	 SOP 
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda	
			Inner Join	bdSisalud.dbo.tbMNViasAdministracion_Vigencias VAM WITH (NOLOCK)
			On			VAM.cdgo_va_admnstrcn = MEP.Cod_Via_administracion
			Where		SOP.fecha_sol Between VAM.inco_vgnca And VAM.fn_vgnca
			And			MEP.Administracion = @Valorcero	
		END

	-- Calcular cnsctvo_cdgo_undd_cncntrcn_dss
	IF EXISTS (	Select	Concentracion_Unidad
				From	#Mdcmnts 
				Where	Concentracion_Unidad = @Valorcero
				AND		Cod_Unidad_concentracion <> @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Concentracion_Unidad = Isnull(CNT.cnsctvo_cdgo_cncntrcn,@Valorcero)
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda		
			Inner Join	bdSisalud.dbo.tbConcentracion_Vigencias	CNT WITH (NOLOCK)
			On			CNT.cdgo_cncntrcn = MEP.Cod_Unidad_concentracion
			Where		SOP.fecha_sol Between CNT.inco_vgnca And CNT.fn_vgnca
			And			MEP.Concentracion_Unidad = @Valorcero
		END

	-- Calcular fcha_vncmnto_invma
	IF EXISTS (	Select	Fecha_Venc_Invima
				From	#Mdcmnts
				Where	Fecha_Venc_Invima is NULL)
		BEGIN 
			Update		#Mdcmnts
			Set			Fecha_Venc_Invima = CUM.fcha_vncmnto_invma
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP 
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join  #Srvcs SER WITH(NOLOCK)
			On			SER.cnsctvo_srvco_slctdo_rcbdo    = MEP.cnsctvo_srvco_slctdo_rcbdo And
			            SER.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join 	bdSisalud.dbo.tbcums CUM WITH (NOLOCK)
			On			CUM.cnsctvo_cms = SER.Servicio_Solicitado
			Where		SOP.Fecha_sol Between CUM.inco_vgnca And CUM.fn_vgnca
			And			MEP.Fecha_Venc_Invima Is NULL
		END

	-- Calcular mstra_mdca
	IF EXISTS (	Select	Muestra_medica
				From	#Mdcmnts
				Where	Muestra_medica = @ValorVacio)
		BEGIN 
			Update		#Mdcmnts
			Set			Muestra_medica = CASE WHEN MEP.Forma_Farmaceutica = @FormaFar THEN @ValorS ELSE @ValorN END 
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join  #Srvcs SER
			On			SER.cnsctvo_srvco_slctdo_rcbdo    = MEP.cnsctvo_srvco_slctdo_rcbdo    And 
			            SER.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join 	bdSisalud.dbo.tbcums CUM WITH (NOLOCK)
			On			CUM.cnsctvo_cms = SER.Servicio_Solicitado
			Where		SOP.Fecha_sol Between CUM.inco_vgnca And CUM.fn_vgnca
			And			MEP.Muestra_medica = @ValorVacio
		END

	-- Calcular uso_cndcndo
	IF EXISTS (	Select		TMD.Uso_Condicionado
				From		#Mdcmnts	TMD
				Inner Join  #Srvcs		SER
				On			TMD.cnsctvo_srvco_slctdo_rcbdo = SER.cnsctvo_srvco_slctdo_rcbdo
				Where		TMD.Uso_Condicionado = @ValorVacio
				AND			SER.Tipo = @TServicio)
		BEGIN 

			-- Valida el diagnostico
			Insert Into #tempTbDefinicionGrupoDiagnostico
			(			cnsctvo_cdgo_dfncn_grpo,		cnsctvo_slctd_srvco_sld_rcbda)
			Select		DGR.cnsctvo_cdgo_dfncn_grpo,	sop.cnsctvo_slctd_srvco_sld_rcbda
			From		bdSisalud.dbo.tbPVDetGrupos				GRU WITH (NOLOCK)
			Inner Join	bdSisalud.dbo.tbPVDefinicionGrupos		DGR WITH (NOLOCK)
			On			DGR.cnsctvo_cdgo_dtlle_grpo = GRU.cnsctvo_cdgo_dtlle_grpo
			Inner Join	bdSisalud.dbo.tbPVDetDefinicionGrupos	DDG WITH (NOLOCK)
			On			DDG.cnsctvo_cdgo_dfncn_grpo = DGR.cnsctvo_cdgo_dfncn_grpo
			Inner Join	bdSisalud.dbo.tbPVRangosVariables		RVA WITH (NOLOCK)
			On			DDG.cnsctvo_cdgo_rngo_vrble = RVA.cnsctvo_cdgo_rngo_vrble 
			Inner Join	#Dgnstcs	SER 
			On			RVA.vlr_1  = SER.Cod_Diagnostico
			Inner Join	#Slctds							SOP 
			On			SER.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
			Where		GRU.cnsctvo_cdgo_grpo  = @Grupo 		
			And			SOP.Fecha_sol Between GRU.inco_vgnca and GRU.fn_vgnca
			And			SOP.Fecha_sol Between DGR.inco_vgnca and DGR.fn_vgnca
			And			RVA.cnsctvo_cdgo_vrble = @Variable3

			-- Valida la edad
			Update		dgd
			Set			Edad_Anual = afl.Edad_Anual
			From		bdSisalud.dbo.tbPVDetGrupos b	with (nolock)			
			Inner Join	bdSisalud.dbo.tbPVDefinicionGrupos e with (nolock)		
			On			e.cnsctvo_cdgo_dtlle_grpo	= b.cnsctvo_cdgo_dtlle_grpo
			Inner Join	bdSisalud.dbo.tbPVDetDefinicionGrupos f	with (nolock)	
			On			f.cnsctvo_cdgo_dfncn_grpo	= e.cnsctvo_cdgo_dfncn_grpo
			Inner Join	bdSisalud.dbo.tbPVRangosVariables g	with (nolock)		
			On			f.cnsctvo_cdgo_rngo_vrble	= g.cnsctvo_cdgo_rngo_vrble	
			Inner Join	#tempTbDefinicionGrupoDiagnostico dgd
			On			dgd.cnsctvo_cdgo_dfncn_grpo = e.cnsctvo_cdgo_dfncn_grpo
			Inner Join	#Aflds afl
			On			afl.cnsctvo_slctd_srvco_sld_rcbda = dgd.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#Slctds							SOP 
			On			sop.cnsctvo_slctd_srvco_sld_rcbda = afl.cnsctvo_slctd_srvco_sld_rcbda
			Where		b.cnsctvo_cdgo_grpo		= @Grupo
			And			SOP.Fecha_sol Between b.inco_vgnca And b.fn_vgnca
			And			SOP.Fecha_sol Between e.inco_vgnca And e.fn_vgnca
			And			afl.Edad_Anual Between g.vlr_1 And g.vlr_2
			And			g.cnsctvo_cdgo_vrble	= @Variable1

			-- Valida el genero
			Update		dgd
			Set			Cod_Sexo = afl.Cod_Sexo
			From		bdSisalud.dbo.tbPVDetGrupos b	with (nolock)			
			Inner Join	bdSisalud.dbo.tbPVDefinicionGrupos e with (nolock)		
			On			e.cnsctvo_cdgo_dtlle_grpo	= b.cnsctvo_cdgo_dtlle_grpo
			Inner Join	bdSisalud.dbo.tbPVDetDefinicionGrupos f	with (nolock)	
			On			f.cnsctvo_cdgo_dfncn_grpo	= e.cnsctvo_cdgo_dfncn_grpo
			Inner Join	bdSisalud.dbo.tbPVRangosVariables g	with (nolock)		
			On			f.cnsctvo_cdgo_rngo_vrble	= g.cnsctvo_cdgo_rngo_vrble
			Inner Join	#tempTbDefinicionGrupoDiagnostico dgd
			On			dgd.cnsctvo_cdgo_dfncn_grpo = e.cnsctvo_cdgo_dfncn_grpo
			Inner Join	#Aflds afl
			On			afl.cnsctvo_slctd_srvco_sld_rcbda = dgd.cnsctvo_slctd_srvco_sld_rcbda
			And			afl.Cod_Sexo = g.vlr_1
			Inner Join	#Slctds							SOP 
			On			sop.cnsctvo_slctd_srvco_sld_rcbda = afl.cnsctvo_slctd_srvco_sld_rcbda
			Where		b.cnsctvo_cdgo_grpo		= @Grupo
			And			SOP.Fecha_sol Between b.inco_vgnca And b.fn_vgnca
			And			SOP.Fecha_sol Between e.inco_vgnca And e.fn_vgnca
			And			g.cnsctvo_cdgo_vrble	= @Variable2
			And			dgd.Edad_Anual Is Not Null
			
			-- Valida el servicio
			Update		dgs
			Set			Cod_Servicio = SER.Cod_Servicio
			From		bdSisalud.dbo.tbPVDetGrupos				GRU WITH (NOLOCK)
			Inner Join	bdSisalud.dbo.tbPVDefinicionGrupos		DGR WITH (NOLOCK)
			On			DGR.cnsctvo_cdgo_dtlle_grpo = GRU.cnsctvo_cdgo_dtlle_grpo
			Inner Join	bdSisalud.dbo.tbPVDetDefinicionGrupos	DDG WITH (NOLOCK)
			On			DDG.cnsctvo_cdgo_dfncn_grpo = DGR.cnsctvo_cdgo_dfncn_grpo
			Inner Join	bdSisalud.dbo.tbPVRangosVariables		RVA WITH (NOLOCK)
			On			DDG.cnsctvo_cdgo_rngo_vrble = RVA.cnsctvo_cdgo_rngo_vrble 
			inner join	#Srvcs								SER 
			on			RVA.vlr_1  = SER.Cod_Servicio
			inner join	#Mdcmnts							MEP 
			on			SER.cnsctvo_srvco_slctdo_rcbdo = MEP.cnsctvo_srvco_slctdo_rcbdo
			Inner Join	#Slctds							SOP 
			on			MEP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#tempTbDefinicionGrupoDiagnostico dgs
			On			dgs.cnsctvo_cdgo_dfncn_grpo = DGR.cnsctvo_cdgo_dfncn_grpo
			And			dgs.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Where		GRU.cnsctvo_cdgo_grpo  = @Grupo
			And			SOP.Fecha_sol Between GRU.inco_vgnca and GRU.fn_vgnca
			And			SOP.Fecha_sol Between DGR.inco_vgnca and DGR.fn_vgnca
			And			RVA.cnsctvo_cdgo_vrble = @Variable4
			And			dgs.cod_sexo Is Not Null
			And			dgs.Edad_Anual Is Not Null

			-- Marca el servicio si cumple con las condiciones anteriores
			Update		MEP
			Set			Uso_Condicionado = @ValorS
			From		bdSisalud.dbo.tbPVRangosVariables RVA WITH (NOLOCK)
			Inner Join	#Srvcs SER 
			On			RVA.vlr_1  = SER.Cod_Servicio
			Inner Join	#Mdcmnts MEP
			On			SER.cnsctvo_srvco_slctdo_rcbdo = MEP.cnsctvo_srvco_slctdo_rcbdo
			Inner Join	#tempTbDefinicionGrupoDiagnostico dgd
			On			dgd.Cod_Servicio = ser.Cod_Servicio
			And			dgd.cnsctvo_slctd_srvco_sld_rcbda = mep.cnsctvo_slctd_srvco_sld_rcbda
			Where		Uso_Condicionado = @ValorVacio
			And			dgd.Cod_Servicio Is Not Null
			And			dgd.cod_sexo Is Not Null
			And			dgd.Edad_Anual Is Not Null

			Update		#Mdcmnts
			Set			Uso_Condicionado = @ValorN
			Where		Uso_Condicionado = @ValorVacio			
			
		END

	--Calcular mss_mxmo_rnvcn
	IF EXISTS (	Select	Meses_Max_Renovacion
				From	#Mdcmnts 
				Where	Meses_Max_Renovacion = @Valorcero)
		BEGIN
			Update		#Mdcmnts
			Set			Meses_Max_Renovacion = ISNULL(SSV.vlr_prmtro_nmrco,@Valorcero)
			From		#Mdcmnts MEP
			Inner Join	#Slctds	 SOP
			On			SOP.cnsctvo_slctd_srvco_sld_rcbda = MEP.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdGestionSolicitudesSalud.dbo.tbParametrosGeneralesSolicitudesSalud_vigencias SSV WITH (NOLOCK)
			On			SSV.dscrpcn_prmtro_gnrl_slctd_sld = @Descripcion
			Where		SOP.fecha_sol Between SSV.inco_vgnca And SSV.fn_vgnca
			And			MEP.Meses_Max_Renovacion = @Valorcero
		END	

	Drop Table #tempTbDefinicionGrupoDiagnostico
	Drop Table #TmpValidarConveniosPagosFijos
	Drop Table #TmpConveniosPagoFijos
	Drop Table #tbPrestacionCapitada
END
