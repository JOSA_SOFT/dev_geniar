USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionNoPOS]    Script Date: 16/05/2017 04:36:53 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionNoPOS
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestación es de tipo NO POS D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 15/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega cruce con la tabla BdCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios
						   para validar el tipo de plan D\> 
* Nuevas Variables   : <\VM @cnsctvo_cdgo_tpo_pln1: almacena el consecutivo del tipo de plan POS = 1 VM\>    
* Fecha Modificacion : <\FM 24/01/2017 FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega validacion par que no valide las prestaciones marcadas como pos condicionado D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 16/05/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER Procedure [dbo].[spASReglaValidarPrestacionNoPOS]
		@prmtro_sp_vldcn				Varchar(30),
		@cnsctvo_cdgo_rgla_vldcn		udtConsecutivo,
		@cnsctvo_cdgo_no_cnfrmdd_vldcn	udtConsecutivo
As
Begin

		 Set NoCount On 

	  Declare	@afirmacion				Char(1),
				@ngcn					Char(1),
				@cnsctvo_cdgo_tpo_pln1	UdtConsecutivo;
			
	  Set @afirmacion = 'S';
	  Set @ngcn = 'N';
	  Set @cnsctvo_cdgo_tpo_pln1 = 1;

		Insert Into #tbResultadosValidaciones (
					cnsctvo_cdgo_rgla_vldcn, 			cnsctvo_cdgo_no_cnfrmdd_vldcn,
					llve_prmra_rgstro_vlddo,			llve_prmra_cncpto_prncpl_vlddo,
					infrmcn_adcnl_no_cnfrmdd
		)
		Select
					@cnsctvo_cdgo_rgla_vldcn,			@cnsctvo_cdgo_no_cnfrmdd_vldcn,
					c.cnsctvo_slctd_srvco_sld_rcbda,	c.cnsctvo_srvco_slctdo_rcbdo,
					'La prestación es de tipo No POS.'
		From		#tmpDatosPrestacionesxSolicitud c
		Inner Join	bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas
		On			c.cnsctvo_slctd_srvco_sld_rcbda = iasas.cnsctvo_slctd_atrzcn_srvco
		Where		c.indcdr_no_ps = @afirmacion
		And			iasas.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_pln1
		And			c.mdcmnto_uso_cndcndo = @ngcn;

  End
