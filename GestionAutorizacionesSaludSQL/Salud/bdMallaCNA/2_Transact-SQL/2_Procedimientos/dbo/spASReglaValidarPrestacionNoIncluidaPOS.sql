USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionNoIncluidaPOS]    Script Date: 31/01/2017 15:40:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionNoIncluidaPOS
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el plan del afiliado es POS y D\>
					 <\D la prestacion no tiene cobertura para dicho plan. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D
                            Se ajusta el procedimiento para que la validacion de la malla se realice teniendo en cuenta las 
							vigencias de la prestacion o la cobertura							  
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  21/12/2016 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionNoIncluidaPOS] 
    @prmtro_sp_vldcn               Varchar(30),
	@cnsctvo_cdgo_rgla_vldcn       udtConsecutivo,
	@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS

BEGIN
  SET NOCOUNT ON 

  DECLARE @consecutivoTipoPlanPOS   Numeric(1),
          @negacion                 Char(1)   ,
          @afirmacion               Char(1)   ,
		  @consecutivoTipoCUPS      Int       ,
		  @consecutivoTipoCUMS      Int       ,
		  @infrmcn_adcnl_no_cnfrmdd udtObservacion
  
  
  SET @consecutivoTipoPlanPOS = 1
  SET @negacion = 'N'
  SET @afirmacion = 'S'
  SET @consecutivoTipoCUPS = 4
  SET @consecutivoTipoCUMS = 5
  Set @infrmcn_adcnl_no_cnfrmdd = 'El servicio solicitado no tiene cobertura para el plan solicitado, debe ser revisado por la mesa de ayuda'

  INSERT 
  INTO   #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                   llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								  )      
  SELECT     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  FROM       #tmpDatosPrestacionesxSolicitud c
  INNER JOIN #tmpDatosAfiliado a
  ON         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  LEFT JOIN  bdSisalud.dbo.tbcupsserviciosxplanes p WITH (NOLOCK)
  ON         p.cnsctvo_prstcn = c.cnsctvo_cdfccn AND 
             p.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
  WHERE      a.cnsctvo_cdgo_tpo_pln = @consecutivoTipoPlanPOS --Corresponde al tipo de plan POS
  AND        c.cnsctvo_cdgo_tpo_cdfccn = @consecutivoTipoCUPS
  AND NOT    (p.cnsctvo_prstcn IS NOT NULL  AND ((c.fcha_prstcn BETWEEN p.inco_vgnca AND p.fn_vgnca)  OR p.cbrtra = @afirmacion))
  UNION ALL
  SELECT     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  FROM       #tmpDatosPrestacionesxSolicitud c
  INNER JOIN #tmpDatosAfiliado a
  ON         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  LEFT JOIN  bdSisalud.dbo.tbCumsxPlanes p WITH (NOLOCK)
  ON         p.cnsctvo_cdgo_cms = c.cnsctvo_cdfccn AND 
             p.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
  WHERE      a.cnsctvo_cdgo_tpo_pln    = @consecutivoTipoPlanPOS --Corresponde al tipo de plan POS
  AND        c.cnsctvo_cdgo_tpo_cdfccn = @consecutivoTipoCUMS
  AND NOT    (p.cnsctvo_cdgo_cms IS NOT NULL AND ((c.fcha_prstcn BETWEEN p.inco_vgnca AND p.fn_vgnca) OR p.cbrtra = @afirmacion));
END

