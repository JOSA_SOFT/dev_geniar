USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarContratoPACVigente]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarContratoPACVigente
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el contrato PAC del afiliado se D\>
					 <\D encuentra vigente a la fecha de solicitud  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 15/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Luis Fernando Benavides  AM\>    
* Descripcion        : <\D  Utiliza campo: nmro_cntrto de la tabla temporal: #tmpDatosAfiliado para  D\> 
*					   <\D	realizar validacion	y creo tabla temporal para optimizar consulta		 D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 20/04/2016  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarContratoPACVigente] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @planFamiliar numeric(1),
          @planBienestarPlus numeric(1),
          @planSelectivo numeric(1),
          @planExcelencia numeric(1),
          @planQuimbaya numeric(1),
          @planBienestar numeric(1)

  SET @planFamiliar = 2;
  SET @planBienestarPlus = 3;
  SET @planSelectivo = 4;
  SET @planExcelencia = 5;
  SET @planQuimbaya = 6;
  SET @planBienestar = 8;

  BEGIN

  	CREATE TABLE #tmpContratos (
	cnsctvo_slctd_srvco_sld_rcbda	udtconsecutivo,
	nmro_unco_idntfccn_afldo		int,
	cnsctvo_cdgo_pln				udtconsecutivo,
	nmro_cntrto						char(15)
	)
	
	INSERT INTO #tmpContratos(
		   cnsctvo_slctd_srvco_sld_rcbda   ,nmro_unco_idntfccn_afldo	, cnsctvo_cdgo_pln		, nmro_cntrto)
	SELECT a.cnsctvo_slctd_srvco_sld_rcbda, a.nmro_unco_idntfccn		, a.cnsctvo_cdgo_pln_pc , vl.nmro_cntrto
							FROM BdAfiliacionValidador.dbo.tbContratosValidador vl WITH (NOLOCK)
							INNER JOIN #tmpDatosAfiliado a 
							ON (vl.nmro_cntrto = a.nmro_cntrto)
							INNER JOIN #tmpDatosPrestacionesxSolicitud c
							ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
							WHERE a.cnsctvo_cdgo_pln_pc IN (@planFamiliar,
							@planBienestarPlus,
							@planSelectivo,
							@planExcelencia,
							@planQuimbaya,
							@planBienestar)
							AND c.fcha_prstcn BETWEEN vl.inco_vgnca_cntrto AND vl.fn_vgnca_cntrto
							group by a.cnsctvo_slctd_srvco_sld_rcbda, a.nmro_unco_idntfccn , a.cnsctvo_cdgo_pln_pc, vl.nmro_cntrto;

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Afiliado no cuenta con contrato PAC vigente.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      LEFT JOIN #tmpContratos tmp
	  	ON (tmp.nmro_cntrto = a.nmro_cntrto)
	 WHERE tmp.cnsctvo_slctd_srvco_sld_rcbda IS NULL
		AND a.cnsctvo_cdgo_pln_pc IN (@planFamiliar,
							@planBienestarPlus,
							@planSelectivo,
							@planExcelencia,
							@planQuimbaya,
							@planBienestar);

	DROP TABLE #tmpContratos

  END

GO
