USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionPAC]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionPAC
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el plan del afiliado es PAC y la  D\>
					 <\D prestacion no esta cubierta para dicho plan. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionPAC] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoTipoPlanPAC numeric(1),
          @afirmacion char(1)
  SET @consecutivoTipoPlanPAC = 2
  SET @afirmacion = 'S'

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación no cubierta en el plan.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      LEFT JOIN bdSisalud.dbo.tbcupsserviciosxplanes p WITH (NOLOCK)
        ON (p.cnsctvo_prstcn = c.cnsctvo_cdfccn
        AND p.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln_pc)
      WHERE a.cnsctvo_cdgo_tpo_pln = @consecutivoTipoPlanPAC --Corresponde al tipo de plan PAC
      AND NOT (p.cnsctvo_prstcn IS NOT NULL
      AND p.cnsctvo_cdgo_pln IS NOT NULL
      AND (c.fcha_prstcn BETWEEN P.inco_vgnca AND P.fn_vgnca)
      AND p.cbrtra = @afirmacion);

  END

GO
