USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarMedicamentoPOSCondicionado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarMedicamentoPOSCondicionado
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestacion es de tipo No Pos D\>
					 <\D y este no es un POS condicionado. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarMedicamentoPOSCondicionado] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoTipoCUMS numeric(1),
          @afirmacion char(1),
          @consecutivoTipoPlanPOS numeric(1)
  SET @consecutivoTipoCUMS = 5
  SET @afirmacion = 'S'
  SET @consecutivoTipoPlanPOS = 1

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El medicamento solicitado debe ser gestionado como un No POS, porque no cumple con los requisitos para ser un POS.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      WHERE c.cnsctvo_cdgo_tpo_cdfccn = @consecutivoTipoCUMS
      AND --Medicamentos
      c.mdcmnto_uso_cndcndo = @afirmacion
      AND a.cnsctvo_cdgo_tpo_pln = @consecutivoTipoPlanPOS --POS
    ;
  END

GO
