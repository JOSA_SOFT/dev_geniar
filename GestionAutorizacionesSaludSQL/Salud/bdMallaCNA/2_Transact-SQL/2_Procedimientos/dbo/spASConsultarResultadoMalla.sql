USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASConsultarResultadoMalla]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarResultadoMalla
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite la orquestación de la consulta del resultado  D\>
					   <\D de la malla de gestión de solicitudes     D\>    
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>
						<\P cnsctvo_cdgo_mlla_vldcn	P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASConsultarResultadoMalla] @es_btch char(1),
@msje_rspsta xml OUTPUT
AS
  SET NOCOUNT ON 
  --  Creación de tablas temporales para la ejecución de la gestión de solicitudes y ejecución de reglas.

  
  DECLARE @cnsctvo_prcso udtConsecutivo,
          @cnsctvo_lg udtConsecutivo,
          @cnsctvo_cdgo_tpo_prcso udtConsecutivo,
          @cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo,
          @mensajeError varchar(1000),
          @msje_errr varchar(1000),
          @rspsta_mlla xml,
          @negacion char(1),
          @indiceSolicitud numeric(3),
          @errorSolicitud varchar(100),
          @tipoProceso udtCodigo,
          @fcha datetime

  BEGIN

    SET @cnsctvo_cdgo_tpo_prcso = 15 --Tipo proceso para procesamiento en batch
    SET @cnsctvo_cdgo_tpo_prcso_lna = 16 -- Tipo proceso para procesamiento en Línea
    SET @negacion = 'N'
    SET @indiceSolicitud = 0
    SET @errorSolicitud = 'Una o más solicitudes no existen.'
    SET @tipoProceso = 'MA'
    SET @fcha = GETDATE()

    SELECT
      @indiceSolicitud = COUNT(id)
    FROM #tmpDatosSolicitud so
    WHERE ISNULL(so.cnsctvo_slctd_srvco_sld_rcbda, '0') = 0;

    IF @indiceSolicitud = 0
    BEGIN
      IF @es_btch = @negacion
      BEGIN
        --Ejecución malla en línea.

        --Se carga la información necesaria para retornar el resultado de la malla.
        EXEC bdMallaCNA.dbo.spASCargarInformacionResultado @cnsctvo_cdgo_tpo_prcso_lna,
                                                           @es_btch

        --Se calcula el resultado de la malla con base en las reglas.											
        EXEC bdMallaCNA.dbo.spASCalcularResultadoMalla @cnsctvo_cdgo_tpo_prcso_lna,
                                                       @es_btch
        --Se retorna el resultado de la ejecución de la malla.
        EXEC bdMallaCNA.dbo.spASRetornarResultadoMalla @cnsctvo_cdgo_tpo_prcso_lna,
                                                       @es_btch,
                                                       @msje_rspsta OUTPUT
      END

    END
    ELSE
    BEGIN
      RAISERROR (@errorSolicitud, 16, 2)
    END
  END

GO
