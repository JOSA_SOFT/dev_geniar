USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionAccesoDirecto]    Script Date: 09/03/2017 13:58:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionAccesoDirecto
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar  si la prestación cumple con las D\>
					 <\D condiciones para ser de acceso directo. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionAccesoDirecto] 
	@prmtro_sp_vldcn varchar(30),
	@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
	@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  BEGIN

     return 

     DECLARE @consecutivoTipoCUPS numeric(1),
             @afirmacion char(1),
             @consecutivoTipoPlanPOS numeric(1)

     SET @consecutivoTipoCUPS = 4
     SET @afirmacion = 'S'
     SET @consecutivoTipoPlanPOS = 1

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        (CASE
          WHEN a.cnsctvo_cdgo_tpo_pln = @consecutivoTipoPlanPOS THEN 'El servicio solicitado no requiere autorización es de acceso directo, debe dirigirse a su IPS primaria'
          ELSE 'El servicio solicitado no requiere autorización es de acceso directo, consulte el directorio médico'
        END)
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      WHERE c.cnsctvo_cdgo_tpo_cdfccn = @consecutivoTipoCUPS
      AND --Medicamentos
      c.prstcn_accso_drcto = @afirmacion
    
  END
