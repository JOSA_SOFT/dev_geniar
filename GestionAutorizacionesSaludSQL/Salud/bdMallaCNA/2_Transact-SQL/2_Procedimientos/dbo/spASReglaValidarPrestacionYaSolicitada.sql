USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionYaSolicitada]    Script Date: 25/01/2017 16:39:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionYaSolicitada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestación ya fue solicitada D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 15/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Carlos Andres Lopez Ramirez - qvisionclr  AM\>    
* Descripcion        : <\D Se agrega tablas temporales para obtener las prestaciones para la validacion del estado D\> 
* Nuevas Variables   : <\VM @estadoAnuladoMega VM\>    
* Fecha Modificacion : <\FM 20/01/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER Procedure [dbo].[spASReglaValidarPrestacionYaSolicitada] 
			@prmtro_sp_vldcn varchar(30),
			@cnsctvo_cdgo_rgla_vldcn			udtConsecutivo,
			@cnsctvo_cdgo_no_cnfrmdd_vldcn		udtConsecutivo
As
Begin

		Set NoCount On 

		Declare @fecha								date,
				@anno								int,
				@fechaInicial						date,
				@estadoAnulada						UdtConsecutivo,
				@estadoNoAutorizada					UdtConsecutivo,
				@estadoNoTramitada					UdtConsecutivo,
				@estadoVencidaCobro					UdtConsecutivo,
				@estadoVencidaPorNoTramite			UdtConsecutivo,
				@estadoNoAutorizadaMega				UdtConsecutivo,
				@estadoDevueltaMega					UdtConsecutivo,
				@estadoVencidaPorNoTramiteMega		UdtConsecutivo,
				@estadoAnuladoMega					UdtConsecutivo;
				

		
		Create Table #tempIdentificacion
		(
			nmro_unco_idntfccn_afldo		UdtConsecutivo
		)

		Create Table #tempPrestacionesBeneficiario
		(
				cnsctvo_slctd_srvco_sld_rcbda	UdtConsecutivo, 
				cnsctvo_srvco_slctdo_rcbdo		UdtConsecutivo,
				cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo,
				cnsctvo_cdgo_pln				UdtConsecutivo,
				fcha_crcn						Date
		)

		Set @fecha = GETDATE();
		Set @anno = YEAR(@fecha);
		Set @fechaInicial = CAST(CAST(@anno AS varchar) + '-01-01' AS date);
		Set @estadoAnulada = 3;
		Set @estadoNoAutorizada = 2;
		Set @estadoNoTramitada = 4;
		Set @estadoVencidaCobro = 11;
		Set @estadoVencidaPorNoTramite = 12;
		Set @estadoNoAutorizadaMega = 8;
		Set @estadoDevueltaMega = 6;
		Set @estadoVencidaPorNoTramiteMega = 10;
		Set @estadoAnuladoMega = 14;

		-- Saca el numero unico del beneficiario
		Insert Into #tempIdentificacion
		(
					nmro_unco_idntfccn_afldo
		)
		Select 
					iasas.nmro_unco_idntfccn_afldo
		From		#tmpDatosAfiliado da 
		Inner Join	BdCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas With(NoLock)
		On			iasas.cnsctvo_slctd_atrzcn_srvco = da.cnsctvo_slctd_srvco_sld_rcbda

		-- Consuta si existe la misma prestacion en estado activo
		Insert Into #tempPrestacionesBeneficiario
		(
					cnsctvo_slctd_srvco_sld_rcbda,		cnsctvo_srvco_slctdo_rcbdo, 
					cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_pln, fcha_crcn
		)
		Select		iasas.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_srvco_slctdo,	
					ss.cnsctvo_cdgo_srvco_slctdo,		iasas.cnsctvo_cdgo_pln, ss.fcha_crcn
		From		#tempIdentificacion i		
		Inner Join	BdCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas With(NoLock)
		On			iasas.nmro_unco_idntfccn_afldo = i.nmro_unco_idntfccn_afldo
		Inner Join	BdCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
		Inner Join	#tmpDatosPrestacionesxSolicitud dps
		ON			ss.cnsctvo_cdgo_srvco_slctdo = dps.cnsctvo_cdfccn
		Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoNoAutorizadaMega
		And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoDevueltaMega
		And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoVencidaPorNoTramiteMega
		And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoAnuladoMega;


		Insert Into #tbResultadosValidaciones (
					cnsctvo_cdgo_rgla_vldcn,			cnsctvo_cdgo_no_cnfrmdd_vldcn,
					llve_prmra_rgstro_vlddo,			llve_prmra_cncpto_prncpl_vlddo,
					infrmcn_adcnl_no_cnfrmdd
		)
		Select
					@cnsctvo_cdgo_rgla_vldcn,			@cnsctvo_cdgo_no_cnfrmdd_vldcn,
					c.cnsctvo_slctd_srvco_sld_rcbda,	c.cnsctvo_srvco_slctdo_rcbdo,
					'La prestacion ya ha sido procesada.'
		From		#tmpDatosPrestacionesxSolicitud c
		Inner Join	bdSisalud.dbo.tbSolicitudOPS op With(NoLock)
		On			op.nmro_slctd = c.nmro_slctd_prvdr
		And			c.cdgo_intrno_ips = op.cdgo_intrno
		And			op.fcha Between @fechaInicial And c.fcha_prstcn
		Where		op.cnsctvo_cdgo_estdo_slctd != @estadoAnulada --Anulada
		And			op.cnsctvo_cdgo_estdo_slctd != @estadoNoAutorizada --No autorizada
		And			op.cnsctvo_cdgo_estdo_slctd != @estadoNoTramitada --No tramitada
		And			op.cnsctvo_cdgo_estdo_slctd != @estadoVencidaCobro --Vencida por no cobro
		And			op.cnsctvo_cdgo_estdo_slctd != @estadoVencidaPorNoTramite --Vencida por no trámite
		Union
		Select		@cnsctvo_cdgo_rgla_vldcn,
					@cnsctvo_cdgo_no_cnfrmdd_vldcn,
					dps.cnsctvo_slctd_srvco_sld_rcbda, 
					dps.cnsctvo_srvco_slctdo_rcbdo,
					'La prestacion ya ha sido procesada.'
		From		#tmpDatosAfiliado da 
		Inner Join	#tmpDatosPrestacionesxSolicitud dps
		ON			dps.cnsctvo_slctd_srvco_sld_rcbda = da.cnsctvo_slctd_srvco_sld_rcbda
		Inner Join	#tempPrestacionesBeneficiario pb
		On			dps.cnsctvo_cdfccn = pb.cnsctvo_cdgo_srvco_slctdo
		Inner Join	bdSisalud.dbo.tbCupsServiciosxPlanes csp With(NoLock)
		On			csp.cnsctvo_prstcn   = pb.cnsctvo_cdgo_srvco_slctdo
		And			csp.cnsctvo_cdgo_pln = pb.cnsctvo_cdgo_pln
		Where		pb.cnsctvo_slctd_srvco_sld_rcbda != dps.cnsctvo_slctd_srvco_sld_rcbda
		And			DateDiff(Day, pb.fcha_crcn, dps.fcha_crcn) < csp.frcnca_entrga

		Drop Table #tempIdentificacion
		Drop Table #tempPrestacionesBeneficiario
  END
