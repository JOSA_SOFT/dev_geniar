USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarCantidadMaximaxPrestacionConfirmada]    Script Date: 19/09/2016 1:21:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarCantidadMaximaxPrestacionConfirmada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la cantidad máxima permitida para D\>
					 <\D la prestación en la vida o el año, con confirmacion de inconsistencia D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].spASReglaValidarCantidadMaximaxPrestacionConfirmada @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoMedioContactoASI numeric(1),
          @consecutivocups numeric(1),
          @consecutivoPlanPos numeric(1),
          @afirmacion char(1),
          @valorVida char(1),
          @valorAnno char(1),
		  @cnstnteCero int = 0

  SET @consecutivoMedioContactoASI = 1;
  SET @consecutivocups = 4
  SET @consecutivoPlanPos = 1
  SET @afirmacion = 'S'
  SET @valorVida = 'V'
  SET @valorAnno = 'A'

  BEGIN

    WITH temp
    AS (SELECT
      c.cnsctvo_slctd_srvco_sld_rcbda,
      c.cnsctvo_srvco_slctdo_rcbdo,
      SUM(ISNULL(aco.cntdd_prstcn, @cnstnteCero)) AS cantidad_entregada
    FROM #tmpDatosPrestacionesxSolicitud c
    INNER JOIN #tmpDatosAfiliado a
      ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
	INNER JOIN #tmpAtencionConceptosOPS aco
	  ON aco.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
	  AND aco.cnsctvo_srvco_slctdo_rcbdo = c.cnsctvo_srvco_slctdo_rcbdo
    WHERE a.cnsctvo_cdgo_tpo_pln = @consecutivoPlanPos --Corresponde al tipo de plan POS AND
    AND c.cnfrmcn_cldd = @afirmacion
    AND c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI -- Ventanilla
    AND c.cnsctvo_cdgo_tpo_cdfccn = @consecutivocups --CUPS
    AND c.mdda_cntdd = @valorVida
    GROUP BY c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo
    UNION ALL

    SELECT
      c.cnsctvo_slctd_srvco_sld_rcbda,
      c.cnsctvo_srvco_slctdo_rcbdo,
      SUM(ISNULL(cntdd_prstcn,@cnstnteCero)) AS cantidad_entregada
    FROM #tmpDatosPrestacionesxSolicitud c
    INNER JOIN #tmpDatosAfiliado a
      ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
	INNER JOIN #tmpAtencionConceptosOPS aco
	  ON aco.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
	  AND aco.cnsctvo_srvco_slctdo_rcbdo = c.cnsctvo_srvco_slctdo_rcbdo
    WHERE a.cnsctvo_cdgo_tpo_pln = @consecutivoPlanPos --Corresponde al tipo de plan POS AND
    AND c.cnfrmcn_cldd = @afirmacion
    AND c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI -- Ventanilla
    AND c.cnsctvo_cdgo_tpo_cdfccn = @consecutivocups --CUPS
    AND c.mdda_cntdd = @valorAnno
    AND aco.fcha_slctd_orgn BETWEEN c.fcha_anno_antrr AND c.fcha_prstcn
    GROUP BY c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo)
    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'La cantidad máxima permitida a solicitar de ' + CAST(c.cntdd_mxma AS varchar) + ' servicio(s) en ' +
        (CASE
          WHEN c.mdda_cntdd = @valorVida THEN 'la vida'
          WHEN c.mdda_cntdd = @valorAnno THEN 'el año'
        END)
        + ' ya fue superada.'
      FROM #tmpDatosPrestacionesxSolicitud c
      LEFT JOIN temp a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
        AND c.cnsctvo_srvco_slctdo_rcbdo = a.cnsctvo_srvco_slctdo_rcbdo)
      WHERE (a.cantidad_entregada + c.cntdd_slctda) > c.cntdd_mxma;
  END

GO
