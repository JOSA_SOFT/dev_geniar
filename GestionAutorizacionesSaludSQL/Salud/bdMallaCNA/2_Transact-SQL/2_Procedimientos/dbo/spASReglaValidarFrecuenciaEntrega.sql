USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarFrecuenciaEntrega]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarFrecuenciaEntrega
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si existe una solicitud para la misma D\>
					 <\D prestación y esta prestacion no cumple con la frecuencia de entrega definida D\>
					 <\D para la prestacion y el estado de la prestacion es diferente a ciertos estados D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarFrecuenciaEntrega] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @consecutivoMedioContactoASI numeric(1),
          @consecutivocups numeric(1),
          @consecutivoPlanPos numeric(1),
          @afirmacion char(1),
          @valorVida char(1),
          @valorAnno char(1)

  SET @consecutivoMedioContactoASI = 1;
  SET @consecutivocups = 4
  SET @consecutivoPlanPos = 1
  SET @afirmacion = 'S'
  SET @valorVida = 'V'
  SET @valorAnno = 'A'

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El servicio  solicitado supera la frecuencia de entrega con respecto a la ultima vez  que fue entregado.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN (SELECT
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        a.nmro_unco_idntfccn,
        MAX(aco.fcha_slctd_orgn) fcha
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      INNER JOIN #tmpAtencionConceptosOPS aco
	  ON aco.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
	  AND aco.cnsctvo_srvco_slctdo_rcbdo = c.cnsctvo_srvco_slctdo_rcbdo
	  WHERE aco.fcha_slctd_orgn IS NOT NULL
      GROUP BY c.cnsctvo_slctd_srvco_sld_rcbda,
               c.cnsctvo_srvco_slctdo_rcbdo,
               a.nmro_unco_idntfccn) sq
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = sq.cnsctvo_slctd_srvco_sld_rcbda
        AND c.cnsctvo_srvco_slctdo_rcbdo = sq.cnsctvo_srvco_slctdo_rcbdo
        AND a.nmro_unco_idntfccn = sq.nmro_unco_idntfccn)
      WHERE c.cnsctvo_cdgo_tpo_cdfccn = @consecutivocups --CUPS
      AND c.frcnca_entrga > DATEDIFF(DAY, sq.fcha, c.fcha_prstcn)
      AND a.cnsctvo_cdgo_tpo_pln = @consecutivoPlanPos; --Corresponde al tipo de plan POS AND
  END

GO
