USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarDocumentosSoporte]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarDocumentosSoporte
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si la prestación tiene los documentos D\>
					 <\D soporte esperados según la parametrizacion. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 21/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarDocumentosSoporte] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 
  BEGIN

    DECLARE @fecha date;
    SET @fecha = GETDATE();

    WITH tAnexos
    AS (SELECT DISTINCT
      doc.cnsctvo_cdgo_dcmnto_sprte,
      doc.dscrpcn_dcmnto_sprte,
      doc.cdgo_cdfccn
    FROM #tempDocPrestacionesLoc doc
	WHERE doc.cdgo_cdfccn <> ''
	UNION ALL  
	SELECT DISTINCT
      doc.cnsctvo_cdgo_dcmnto_sprte,
      doc.dscrpcn_dcmnto_sprte,
      sol.cdgo_cdfccn
    FROM #tempDocPrestacionesLoc doc, #tmpDatosPrestacionesxSolicitud sol
	WHERE doc.cdgo_cdfccn = '')
    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT DISTINCT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'El servicio solicitado no cuenta con los documentos soportes necesarios.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN tAnexos ta WITH (NOLOCK)
        ON (ta.cdgo_cdfccn = c.cdgo_cdfccn)
      LEFT JOIN #tmpDatosDocumentosAnexos a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
        AND ta.cnsctvo_cdgo_dcmnto_sprte = a.cnsctvo_cdgo_dcmnto_sprte)
      WHERE a.cnsctvo_cdgo_dcmnto_sprte IS NULL
  END

GO
