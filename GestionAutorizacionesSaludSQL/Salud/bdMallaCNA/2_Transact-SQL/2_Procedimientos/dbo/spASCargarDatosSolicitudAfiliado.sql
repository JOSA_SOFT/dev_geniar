USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudAfiliado]    Script Date: 17/05/2017 03:10:36 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudAfiliado
* Desarrollado por : <\A Jois Lombana Sánchez  A\>    
* DescripciOn      : <\D Procedimiento que se encarga de actualizar en la temporal de Afiliados  D\>
  					 <\D para realizar el procesamiento de la malla. D\>    
* ObservaciOnes    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha CreaciOn   : <\FC 26/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIfICACIOn    
*-------------------------------------------------------------------------------------------------------
* ModIficado Por     : <\AM Ing. Carlos Andres lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega update con cruce a tbAfiliadosMarcados para recuperar el cnsctvo_cdgo_chrte
						   Se agregan tablas temporales #tempTbClasificacionEventosNotificacion y
						   #tempTbEstadosNotificacion para recuperar parametros de validacion. D\> 
* Nuevas Variables   : <\VM @cnsctvo_cdgo_clsfccn_evnto13
							@cnsctvo_cdgo_clsfccn_evnto16
							@cnsctvo_cdgo_estdo_ntfccn13
							@cnsctvo_cdgo_estdo_ntfccn14
							@cnsctvo_cdgo_estdo_ntfccn87
							@cnsctvo_cdgo_estdo_ntfccn88
							@cnsctvo_cdgo_estdo_ntfccn89
							@cnsctvo_cdgo_estdo_ntfccn119
							@cnsctvo_cdgo_estdo_ntfccn120
							@cnsctvo_cdgo_estdo_ntfccn156
							@cnsctvo_cdgo_estdo_ntfccn167
							@cnsctvo_cdgo_estdo_ntfccn168  VM\>    
* Fecha ModIficaciOn : <\FM 2017/04/21 FM\>    
*-----------------------------------------------------------------------------------------------------

* DATOS DE MODIfICACIOn    
*-------------------------------------------------------------------------------------------------------
* ModIficado Por     : <\AM Ing. Carlos Andres lopez Ramirez AM\>    
* Descripcion        : <\D Se optimiza consulta para recuperar los datos del afiliado de tbBeneficiariosValidador
						   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha ModIficaciOn : <\FM 2017/05/17 FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER Procedure [dbo].[spASCargarDatosSolicitudAfiliado] 
As
Begin
		Set NoCount On 
		
		Declare @Valorcero						Int,
				@ValorVacio						Varchar(1),
				@fechaActual					DateTime,
				@cnsctvo_cdgo_clsfccn_evnto13	udtCOnsecutivo,
				@cnsctvo_cdgo_clsfccn_evnto16	udtCOnsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn13	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn14	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn87	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn88	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn89	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn119	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn120	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn156	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn167	udtConsecutivo,
				@cnsctvo_cdgo_estdo_ntfccn168	udtConsecutivo,
				@estdo							Char(1);

				
		/*::::::::::::::::COMPLETAR AFILIADO PROCESO MALLA::::::::::::::::::::::::::::::*/	
		Create Table #Bnfcrs 
		(
			maximafechabeneficiario DateTime, 
			cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo
		)

		Create Table #tempTbClasificacionEventosNotificacion
		(
			cnsctvo_cdgo_clsfccn_evnto			udtConsecutivo,
			dscrpcn_clsfccn_evnto				udtDescripcion
		)

		Create Table #tempTbEstadosNotificacion
		(
			cnsctvo_cdgo_estdo_ntfccn			udtConsecutivo,
			dscrpcn_estdo_ntfccn				udtDescripcion
		)

		Create Table #tempDatosBeneficiario
		(
			nmro_unco_idntfccn_afldo			udtConsecutivo,
			nmro_cntrto							udtNumeroFormulario,
			cnsctvo_cdgo_tpo_cntrto				udtConsecutivo,
			cnsctvo_bnfcro						udtConsecutivo,
			cdgo_intrno							udtCodigoIps,
			smns_ctzds							udtConsecutivo,
			cnsctvo_cdgo_rngo_slrl				udtConsecutivo,
			nmro_idntfccn						Varchar(20),
			cnsctvo_cdgo_tpo_idntfccn			udtConsecutivo,
			mxma_fcha_bnfcro					DateTime,
			cnsctvo_slctd_atrzcn_srvco			udtConsecutivo
		)
		

		Set	@Valorcero = 0;
		Set @ValorVacio = '';
		Set @fechaActual = Getdate();
		Set @cnsctvo_cdgo_clsfccn_evnto13 = 13;
		Set @cnsctvo_cdgo_clsfccn_evnto16 = 16;
		Set @cnsctvo_cdgo_estdo_ntfccn13 = 13;	
		Set @cnsctvo_cdgo_estdo_ntfccn14 = 14;
		Set @cnsctvo_cdgo_estdo_ntfccn87 = 87;
		Set @cnsctvo_cdgo_estdo_ntfccn88 = 88;
		Set @cnsctvo_cdgo_estdo_ntfccn89 = 89;
		Set @cnsctvo_cdgo_estdo_ntfccn119 = 119;
		Set @cnsctvo_cdgo_estdo_ntfccn120 = 120;
		Set @cnsctvo_cdgo_estdo_ntfccn156 = 156;
		Set @cnsctvo_cdgo_estdo_ntfccn167 = 167;
		Set @cnsctvo_cdgo_estdo_ntfccn168 = 168;
		Set @estdo = 'A';

		Insert into #tempTbClasificacionEventosNotificacion
		(			cnsctvo_cdgo_clsfccn_evnto,		dscrpcn_clsfccn_evnto)
		Select		cnsctvo_cdgo_clsfccn_evnto,		dscrpcn_clsfccn_evnto
		From		bdSisalud.dbo.tbClasificacionEventosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_clsfccn_evnto  = @cnsctvo_cdgo_clsfccn_evnto13

		Insert into #tempTbClasificacionEventosNotificacion
		(			cnsctvo_cdgo_clsfccn_evnto,		dscrpcn_clsfccn_evnto)
		Select		cnsctvo_cdgo_clsfccn_evnto,		dscrpcn_clsfccn_evnto 
		From		bdSisalud.dbo.tbClasificacionEventosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_clsfccn_evnto  = @cnsctvo_cdgo_clsfccn_evnto16

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn13

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn14

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn119

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn120

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn87

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn88

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn156

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn167

		Insert Into	#tempTbEstadosNotificacion
		(			cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn)
		Select		cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn
		From		bdSisalud.dbo.tbEstadosNotificacion With(NoLock)
		Where		cnsctvo_cdgo_estdo_ntfccn = @cnsctvo_cdgo_estdo_ntfccn168

		-- Insertar cOnsecutivo de solicitud recibida
		Insert Into		#Aflds
		Select 			 AFP.cnsctvo_slctd_atrzcn_srvco			--cnsctvo_slctd_srvco_sld_rcbda
						,AFP.cnsctvo_cdgo_tpo_idntfccn_afldo	--Tipo_IdentIficaciOn
						,AFP.cnsctvo_cdgo_dprtmnto_afldo		--Departamento
						,AFP.cnsctvo_cdgo_cdd_rsdnca_afldo		--Ciudad
						,AFP.cnsctvo_cdgo_cbrtra_sld			--Cobertura
						,AFP.cnsctvo_cdgo_tpo_pln				--Tipo_Plan
						,AFP.cnsctvo_cdgo_pln					--Codigo_Plan
						,AFP.cnsctvo_cdgo_estdo_pln				--Estado_Plan
						,AFP.cnsctvo_cdgo_sxo					--Sexo
						,AFP.cnsctvo_cdgo_tpo_vnclcn_afldo		--VinculaciOn
						,AFP.cnsctvo_cdgo_tpo_cntrto			--tipo_contrato
						,AFP.cnsctvo_cdgo_rngo_slrl				--Rango_Salarial
						,AFP.cnsctvo_bnfcro_cntrto				--Beneficiario
						,AFP.cnsctvo_cdgo_estdo_drcho			--Estado_Derecho
						,AFP.cdgo_ips_prmra						--Ips
						,AFP.cnsctvo_cdgo_sde_ips_prmra			--Sede_Ips
						,AFP.cnsctvo_cdgo_sxo_rcn_ncdo			--Sexo_recien_nacido
						,AFP.cnsctvo_cdgo_chrte					--Cohorte
						,AFP.nmro_cntrto						--numero_contrato
						,AFP.nmro_unco_idntfccn_afldo			--NUI
						,AFP.nmro_idntfccn_afldo				--Num_identIficaciOn
						,AFP.smns_ctzds							--Semanas_Cotizadas
						,AFP.edd_afldo_ans						--Edad_Anual
						,AFP.edd_afldo_mss						--Edad_Meses		
						,AFP.edd_afldo_ds						--Edad_Dias
						,AFO.cdgo_tpo_idntfccn_afldo			--Cod_Tipo_IdentIf
						,AFO.cdgo_dprtmnto_afldo				--Cod_Departamento
						,AFO.cdgo_cdd_rsdnca_afldo				--Cod_ciudad
						,AFO.cdgo_cbrtra_sld					--Cod_Cobertura
						,AFO.cdgo_tpo_pln						--Cod_Tipo_plan
						,AFO.cdgo_pln							--Cod_plan
						,AFO.cdgo_estdo_pln						--Cod_estado_plan
						,AFO.cdgo_sxo							--Cod_Sexo
						,AFO.cdgo_tpo_vnclcn_afldo				--Cod_VinculaciOn
						,AFO.cdgo_sxo_rcn_ncdo					--Cod_Sexo_recien_ncdo
						,AFO.fcha_ncmnto_afldo					--Fecha_Nacimiento
		From			#tmpNmroSlctds TSO With (NoLock)
		Inner Join		BdCNA.gsa.tbASInformaciOnAfiliadoSolicitudAutorizaciOnServicios AFP With (NoLock)
		On				TSO.cnsctvo_slctd_srvco_sld_rcbda = AFP.cnsctvo_slctd_atrzcn_srvco
		Inner Join		BdCNA.gsa.tbASInformaciOnAfiliadoSolicitudAutorizaciOnServiciosOriginal	AFO With (NoLock)
		On				AFP.cnsctvo_slctd_atrzcn_srvco = AFO.cnsctvo_slctd_atrzcn_srvco
		And				AFP.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = AFO.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
		

		-- Carga Informacion Del Beneficiario
		Insert Into #tempDatosBeneficiario
		(
					nmro_unco_idntfccn_afldo,		cnsctvo_slctd_atrzcn_srvco,				nmro_cntrto,
					cnsctvo_cdgo_tpo_cntrto,		cnsctvo_bnfcro,							cdgo_intrno,	
					smns_ctzds,						mxma_fcha_bnfcro
		)
		Select 		nmro_unco_idntfccn_afldo,		sop.cnsctvo_slctd_srvco_sld_rcbda,		bv.nmro_cntrto,
					bv.cnsctvo_cdgo_tpo_cntrto,		bv.cnsctvo_bnfcro,						bv.cdgo_intrno,
					bv.smns_ctzds,					max(VBV.fn_vgnca_estdo_bnfcro) 
		From		#Aflds a	
		Inner Join	#Slctds sop 
		On			sop.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
		Inner Join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv	With (NoLock)
		On			bv.cnsctvo_cdgo_tpo_idntfccn = a.tipo_identificacion
		And			bv.nmro_idntfccn = a.num_identificacion
		Inner Join	BDAfiliaciOnValidador.dbo.tbVigenciasBeneficiariosValidador	VBV	With (NoLock)
		On			vbv.cnsctvo_cdgo_tpo_cntrto		=	bv.cnsctvo_cdgo_tpo_cntrto
		And			vbv.nmro_cntrto					=	bv.nmro_cntrto
		And			vbv.cnsctvo_bnfcro				=	bv.cnsctvo_bnfcro
		Where		bv.estdo = @estdo
		And			sop.Fecha_sol Between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro
		Group By	nmro_unco_idntfccn_afldo,		sop.cnsctvo_slctd_srvco_sld_rcbda,		bv.nmro_cntrto,
					bv.cnsctvo_cdgo_tpo_cntrto,		bv.cnsctvo_bnfcro,						bv.cdgo_intrno,
					bv.smns_ctzds

	

		Update		db
		Set			nmro_idntfccn = a.num_identificacion,
					cnsctvo_cdgo_tpo_idntfccn = a.tipo_identificacion
		From		#tempDatosBeneficiario db
		Inner Join	#Aflds a
		On			a.cnsctvo_slctd_srvco_sld_rcbda = db.cnsctvo_slctd_atrzcn_srvco
			

		-- Calcular cnsctvo_cdgo_tpo_idntfccn_afldo
		If Exists (	Select 	taf.Tipo_IdentIficaciOn
					From	#Aflds taf
					Where	taf.Tipo_IdentIficaciOn = @Valorcero
					And		taf.Cod_Tipo_IdentIf <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Tipo_IdentIficaciOn = IsNull(TID.cnsctvo_cdgo_tpo_idntfccn,@Valorcero)
			From		#Aflds taf 
			Inner Join	#Slctds sop
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbTiposIdentIficaciOn_Vigencias	TID With (NoLock)
			On			taf.Cod_Tipo_IdentIf = TID.cdgo_tpo_idntfccn		
			Where		sop.Fecha_sol Between TID.inco_vgnca And TID.fn_vgnca			
			And			taf.Tipo_IdentIficaciOn = @Valorcero
		End
		
		-- Calcular cnsctvo_cdgo_dprtmnto_afldo
		If Exists (	Select 	taf.Departamento
					From	#Aflds taf 
					Where	taf.Departamento = @Valorcero
					And		taf.Cod_Departamento <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Departamento = IsNull(DEP.cnsctvo_cdgo_dprtmnto,@Valorcero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbDepartamentos_Vigencias	DEP With (NoLock)
			On			taf.Cod_Departamento = DEP.cdgo_dprtmnto	
			Where		sop.Fecha_sol Between DEP.inco_vgnca And DEP.fn_vgnca			
			And			taf.Departamento = @Valorcero
		End	
		
		-- Calcular cnsctvo_cdgo_cdd_rsdnca_afldo
		If Exists (	Select 	taf.Ciudad
					From	#Aflds taf 
					Where	taf.Ciudad = @Valorcero
					And		taf.Cod_ciudad <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Ciudad = IsNull(CIU.cnsctvo_cdgo_cdd,@Valorcero)
			From		#Aflds taf
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbCiudades_Vigencias	CIU With (NoLock)
			On			taf.Cod_ciudad = CIU.cdgo_cdd
			Where		sop.Fecha_sol Between CIU.inco_vgnca And CIU.fn_vgnca				
			And			taf.Ciudad = @Valorcero		
		End

		-- Calcular cnsctvo_cdgo_cbrtra_sld
		If Exists (	Select 	taf.Cobertura
					From	#Aflds taf  
					Where	taf.Cobertura = @Valorcero
					And		taf.Cod_Cobertura <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Cobertura = IsNull(COB.cnsctvo_cdgo_cbrtra_sld,@Valorcero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdSisalud.dbo.tbPMCoberturasSalud_Vigencias	COB With (NoLock)
			On			taf.Cod_Cobertura = COB.cdgo_cbrtra_sld
			Where		sop.Fecha_sol Between COB.inco_vgnca And COB.fn_vgnca					
			And			taf.Cobertura = @Valorcero		
		End			
	
		-- Calcular cnsctvo_cdgo_pln
		If Exists (	Select 	taf.Codigo_Plan
					From	#Aflds taf 
					Where	taf.Codigo_Plan = @Valorcero
					And		taf.Cod_plan <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Codigo_Plan = IsNull(PLA.cnsctvo_cdgo_pln,@Valorcero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbPlanes_Vigencias	PLA With (NoLock)
			On			taf.Cod_plan = PLA.cdgo_pln
			Where		sop.Fecha_sol Between PLA.inco_vgnca And PLA.fn_vgnca					
			And			taf.Codigo_Plan = @Valorcero	
		End	

		-- Calcular cnsctvo_cdgo_tpo_pln
		If Exists (	Select 	taf.Tipo_Plan
					From	#Aflds taf  
					Where	taf.Tipo_Plan = @Valorcero
					)
		Begin
			Update		#Aflds
			Set			Tipo_Plan = IsNull(PLA.cnsctvo_cdgo_tpo_pln,@Valorcero)
			From		#Aflds taf
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda	
			Inner Join	bdAfiliaciOnValidador.dbo.tbPlanes_Vigencias	PLA With (NoLock)
			On			taf.Codigo_Plan = PLA.cnsctvo_cdgo_pln											
			Where		sop.Fecha_sol Between PLA.inco_vgnca And PLA.fn_vgnca					
			And			taf.Tipo_Plan = @Valorcero	
		End		

		-- Calcular nmro_unco_idntfccn_afldo
		If Exists ( Select 	taf.NUI
					From	#Aflds taf 
					Where	taf.NUI = @Valorcero)
		Begin
						
			Update		#Aflds
			Set			nui = IsNull(db.nmro_unco_idntfccn_afldo,@Valorcero)
			From		#Aflds taf 
			Inner Join	#tempDatosBeneficiario	db	
			On			taf.Num_identIficaciOn = db.nmro_idntfccn
			And			taf.Tipo_IdentIficaciOn = db.cnsctvo_cdgo_tpo_idntfccn			
			And			taf.cnsctvo_slctd_srvco_sld_rcbda = db.cnsctvo_slctd_atrzcn_srvco 			
			Where		taf.nui = @Valorcero		

		End

		-- Calcular nmro_cntrto	
		If Exists (	Select 	taf.numero_contrato
					From	#Aflds taf 
					Where	taf.numero_contrato = @ValorVacio)
		Begin	
			Update		#Aflds
			Set			numero_contrato = IsNull(db.nmro_cntrto,@ValorVacio) --
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	#tempDatosBeneficiario db
			On			taf.NUI = db.nmro_unco_idntfccn_afldo
			Where		taf.numero_contrato = @ValorVacio	
		End

		-- Calcular cnsctvo_cdgo_tpo_cntrto 
		If Exists (	Select 	taf.tipo_contrato
					From	#Aflds taf 
					Where	taf.tipo_contrato = @ValorCero)
		Begin
			Update		#Aflds
			Set			tipo_contrato = IsNull(db.cnsctvo_cdgo_tpo_cntrto,@ValorCero) --
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	#tempDatosBeneficiario db 
			On			taf.NUI = db.nmro_unco_idntfccn_afldo
			Where		taf.tipo_contrato = @ValorCero
		End		
		
		-- cnsctvo_bnfcro_cntrto 
		If Exists (	Select 	taf.Beneficiario
					From	#Aflds taf 
					Where	taf.Beneficiario = @ValorCero)
		Begin
			Update		#Aflds
			Set			beneficiario = IsNull(db.cnsctvo_bnfcro, @ValorCero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#tempDatosBeneficiario db
			On			taf.nui = db.nmro_unco_idntfccn_afldo
			Where		taf.Beneficiario = @ValorCero
		End		
		
		-- Calcular cnsctvo_cdgo_sxo
		If Exists (	Select 	taf.Sexo
					From	#Aflds taf
					Where	taf.Sexo = @ValorCero
					And		taf.Cod_Sexo <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Sexo = IsNull(SEX.cnsctvo_cdgo_sxo,@ValorCero)
			From		#Aflds taf
			Inner Join	#Slctds sop
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbSexos_Vigencias	SEX With (NoLock)
			On			taf.Cod_Sexo = SEX.cdgo_sxo
			Where		sop.Fecha_sol Between SEX.inco_vgnca And SEX.fn_vgnca						
			And			taf.Sexo = @ValorCero
		End					

		-- Calcular cnsctvo_cdgo_tpo_vnclcn_afldo
		If Exists (	Select 	taf.VinculaciOn
					From	#Aflds taf 
					Where	taf.VinculaciOn = @ValorCero
					And		taf.Cod_VinculaciOn <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			VinculaciOn = IsNull(TVI.cnsctvo_cdgo_tpo_afldo,@ValorCero)
			From		#Aflds taf
			Inner Join	#Slctds sop
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnvalidador.dbo.tbTiposAfiliado_Vigencias	TVI With (NoLock)
			On			taf.Cod_VinculaciOn = TVI.cdgo_tpo_afldo
			Where		sop.Fecha_sol Between TVI.inco_vgnca And TVI.fn_vgnca							
			And			taf.VinculaciOn = @ValorCero
		End		
		
		-- Calcular cnsctvo_cdgo_rngo_slrl 
		If Exists (	Select 	taf.Rango_Salarial
					From	#Aflds taf With(NoLock) 
					Where	taf.Rango_Salarial = @ValorCero)
		Begin
			Update		#Aflds
			Set			Rango_Salarial = IsNull(COn.cnsctvo_cdgo_rngo_slrl,@ValorCero) --
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#tempDatosBeneficiario db 
			On			db.nmro_unco_idntfccn_afldo = taf.nui
			Left Join	bdAfiliaciOnValidador.dbo.tbContratosValidador		con	With (NoLock)	
			On			con.cnsctvo_cdgo_tpo_cntrto	= taf.tipo_contrato
			And			con.nmro_cntrto	= taf.numero_contrato
			And			con.cnsctvo_cdgo_pln = taf.Codigo_Plan
			Where		sop.Fecha_sol Between con.inco_vgnca_cntrto And con.fn_vgnca_cntrto
			And			taf.Rango_Salarial = @ValorCero
		End							
		
		-- Calcular cnsctvo_cdgo_estdo_drcho CAMPO QUE SIEMPRE SE DEBE CALCULAR
		Begin
			Update		#Aflds
			Set			Estado_derecho = IsNull(EDE.cnsctvo_cdgo_estdo_drcho,@ValorCero)
						,Estado_Plan = IsNull(EDE.cnsctvo_cdgo_estdo_drcho,@ValorCero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdAfiliaciOnValidador.dbo.TbMatrizDerechosValidador		EDE	With(NoLock) 
			On 			EDE.cnsctvo_cdgo_tpo_cntrto = taf.tipo_contrato	
			And			EDE.nmro_cntrto = taf.numero_contrato 
			And			EDE.cnsctvo_bnfcro = taf.Beneficiario
			And			sop.Fecha_sol Between EDE.inco_vgnca_estdo_drcho And EDE.fn_vgnca_estdo_drcho
			Inner Join	bdAfiliaciOnValidador.dbo.tbCausasDerechoValidador		CDV	With(NoLock) 
			On			CDV.cnsctvo_cdgo_estdo_drcho = EDE.cnsctvo_cdgo_estdo_drcho 
			And			CDV.cnsctvo_cdgo_csa_drcho = EDE.cnsctvo_cdgo_csa_drcho	 
			Where		taf.Estado_derecho = @ValorCero
		End
		
		-- Calcular cdgo_ips_prmra = Codigo Interno  CAMPO QUE SIEMPRE SE DEBE CALCULAR
		Begin
			Update		#Aflds
			Set			Ips = IsNull(db.cdgo_intrno,@ValorVacio) --
			From		#Aflds taf 
			Inner Join	#Slctds sop
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	#tempDatosBeneficiario db 
			On			taf.NUI = db.nmro_unco_idntfccn_afldo
			Where		taf.Ips = @ValorVacio
		End
											
		-- Calcular cnsctvo_cdgo_sde_ips_prmra
		If Exists (	Select 	taf.Sede_Ips
					From	#Aflds taf With(NoLock) 
					Where	taf.Sede_Ips = @ValorCero)
		Begin
			Update		#Aflds
			Set			Sede_Ips = IsNull(IPS.cnsctvo_cdgo_sde_ips,@ValorCero)
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Inner Join	bdAfiliaciOnValidador.dbo.tbIpsPrimarias_vigencias	IPS With (NoLock)
			On			taf.Ips = IPS.cdgo_intrno
			Where		sop.Fecha_sol Between IPS.inco_vgnca And IPS.fn_vgnca
			And			taf.Sede_Ips = @ValorCero
		End

		-- Calcular cnsctvo_cdgo_sxo_rcn_ncdo
		If Exists (	Select 	taf.Sexo_recien_nacido
					From	#Aflds taf 
					Where	taf.Sexo_recien_nacido = @ValorCero
					And		taf.Cod_Sexo_recien_ncdo <> @ValorVacio)
		Begin
			Update		#Aflds
			Set			Sexo_recien_nacido = IsNull(SRN.cnsctvo_cdgo_sxo,@ValorCero)
			From		#Aflds taf
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	bdAfiliaciOnValidador.dbo.tbsexos_Vigencias	SRN With (NoLock)
			On			taf.Cod_Sexo_recien_ncdo = SRN.cdgo_sxo
			Where		sop.Fecha_sol Between SRN.inco_vgnca And SRN.fn_vgnca					
			And			taf.Sexo_recien_nacido = @ValorCero	
		End
		
		-- Calcular cnsctvo_cdgo_chrte
		If Exists (	Select 	taf.Cohorte
					From	#Aflds taf 
					Where	taf.Cohorte = @ValorCero)
		Begin
			Update			#Aflds
			Set				Cohorte =IsNull(CAF.cnsctvo_cdgo_chrte,@ValorCero)
			From			#Aflds taf 
			Inner Join		bdRiesgosSalud.dbo.tbReporteCohortesxAfiliado CAF With(NoLock) 
			On				taf.NUI = CAF.nmro_unco_idntfccn_afldo
			Inner Join		bdRiesgosSalud.dbo.tbcohortes_vigencias CVI With(NoLock) 
			On				CVI.cnsctvo_cdgo_chrte = CAF.cnsctvo_cdgo_chrte
			Where			CVI.prrzcn = ( Select  MIN(IsNull(prrzcn,@ValorCero)) 
											From  bdRiesgosSalud.dbo.tbcohortes_vigencias VIG With(NoLock)
											Where VIG.cnsctvo_cdgo_chrte = CVI.cnsctvo_cdgo_chrte
										  )
			And				taf.Cohorte = @ValorCero	

			Update		afld
			Set			cohorte = IsNull(am.cnsctvo_cdgo_clsfccn_evnto,@ValorCero)
			From		#Aflds afld
			Inner Join	bdSisalud.dbo.tbAfiliadosMarcados am With(NoLock)
			On			afld.nui = am.nmro_unco_idntfccn
			Inner Join	#tempTbClasificacionEventosNotificacion cen
			On			am.cnsctvo_cdgo_clsfccn_evnto = cen.cnsctvo_cdgo_clsfccn_evnto
			Inner Join	#tempTbEstadosNotificacion en
			On			am.cnsctvo_cdgo_estdo_ntfccn =	en.cnsctvo_cdgo_estdo_ntfccn		

		End	
		
		--Calcular smns_ctzds
		If Exists (	Select 	taf.Semanas_Cotizadas
					From	#Aflds taf 
					Where	taf.Semanas_Cotizadas = @ValorCero)
		Begin	
			Update		#Aflds
			Set			Semanas_Cotizadas = IsNull(db.smns_ctzds,@ValorCero)--
			From		#Aflds taf 
			Inner Join	#Slctds sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda												
			Inner Join	#tempDatosBeneficiario db 
			On			taf.NUI = db.nmro_unco_idntfccn_afldo
			Where		taf.Semanas_Cotizadas = @ValorCero	
		End

		-- Calcular edd_afldo_anns
		If Exists (	Select 	taf.Edad_Anual
					From	#Aflds taf 
					Where	taf.Edad_Anual = @ValorCero)
		Begin	
			Update		#Aflds
			Set			Edad_Anual = IsNull(BDAfiliaciOnValidador.dbo.fnCalculaAnosOptimizada(taf.Fecha_Nacimiento,@fechaActual,1),@ValorCero)
			From		#Aflds		taf 
			Inner Join	#Slctds	sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Where		taf.Edad_Anual = @ValorCero
		End

		-- Calcular edd_afldo_mss
		If Exists (	Select 	taf.Edad_Meses
					From	#Aflds taf 
					Where	taf.Edad_Meses = @ValorCero)
		Begin	
			Update		#Aflds
			Set			Edad_Meses = IsNull(BDAfiliaciOnValidador.dbo.fnCalculaAnosOptimizada(taf.Fecha_Nacimiento,@fechaActual,2),@ValorCero)
			From		#Aflds		taf
			Inner Join	#Slctds	sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Where		taf.Edad_Meses = @ValorCero
		End
		
		-- Calcular edd_afldo_ds
		If Exists (	Select 	taf.Edad_Dias
					From	#Aflds taf 
					Where	taf.Edad_Dias = @ValorCero)
		Begin	
			Update		#Aflds
			Set			Edad_Dias = IsNull(BDAfiliaciOnValidador.dbo.fnCalculaAnosOptimizada(taf.Fecha_Nacimiento,@fechaActual,3),@ValorCero)
			From		#Aflds		taf 
			Inner Join	#Slctds	sop 
			On			taf.cnsctvo_slctd_srvco_sld_rcbda = sop.cnsctvo_slctd_srvco_sld_rcbda
			Where		taf.Edad_Dias = @ValorCero
		End		
			
		Drop Table #Bnfcrs;
		Drop Table #tempTbClasificacionEventosNotificacion;
		Drop Table #tempTbEstadosNotificacion;
		Drop Table #tempDatosBeneficiario

End



