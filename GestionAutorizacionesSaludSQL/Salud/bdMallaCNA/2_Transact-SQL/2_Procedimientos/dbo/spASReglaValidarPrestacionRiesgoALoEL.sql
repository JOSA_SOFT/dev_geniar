USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionRiesgoALoEL]    Script Date: 07/06/2017 15:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionRiesgoALoEL
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si la prestación esta asociada  al D\>
					 <\D riesgo AL o EL  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 20/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM
                            Se realiza modificacion en el procedimiento para que valide
							la vigencias de los riesgos   
                         DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 07/06/2017  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionRiesgoALoEL] 
  @prmtro_sp_vldcn varchar(30),
  @cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
  @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON 

  Declare @consecutivoRiesgoAT      udtConsecutivo,    
          @infrmcn_adcnl_no_cnfrmdd udtObservacion,
		  @fecha_actual             Datetime      ,      
		  @cnsctvo_cdgo_tpo_pln     udtConsecutivo
  
  Set @consecutivoRiesgoAT      = 210
  Set @infrmcn_adcnl_no_cnfrmdd = 'Prestacion identificada como  Enfermedad Laboral se direciona a medicina del trabajo.'
  Set @fecha_actual             = getDate()
  Set @cnsctvo_cdgo_tpo_pln     = 1

  If Exists (Select top 1 'x' From #tmpDatosAfiliado a Where a.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_pln)
     Begin
	    Insert
		Into       #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                             llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
										    )
        Select     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
                   c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
        From       #tmpDatosPrestacionesxSolicitud c
        Inner Join #tmpDatosAfiliado a
        ON         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
        Inner Join bdCNA.sol.tbCNAPrestacionesxRiesgo_Vigencias m With(NoLock)
        On         m.cnsctvo_prstcn = c.cnsctvo_cdfccn
		Inner Join bdSiSalud.dbo.tbMNRiesgosxDiagnosticos_Vigencias v With(NoLock)
        On         v.cnsctvo_cdgo_rsgo_dgnstco = m.cnsctvo_cdgo_rsgo_dgnstco
        Where      v.cnsctvo_cdgo_rsgo_dgnstco = @consecutivoRiesgoAT --p.dscrpcn_rsgo_dgnstco = 'EP'
		And        @fecha_actual Between m.inco_vgnca And m.fn_vgnca
		And        @fecha_actual Between v.inco_vgnca And v.fn_vgnca
        --And        a.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_pln; --Corresponde al tipo de plan POS AND
	 End   
END
