USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarDiagnosticoRiesgoAccidenteTransito]    Script Date: 07/06/2017 13:49:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarDiagnosticoRiesgoAccidenteTransito
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D 
                         Procedimiento que se encarga de validar si el diagnostico esta asociado  
					     al riesgo Accidente de Transito.  
					  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 20/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM
                            Se realiza modificacion en el procedimiento para que valide
							la vigencias de los riesgos   
                         DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 07/06/2017  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarDiagnosticoRiesgoAccidenteTransito] 
   @prmtro_sp_vldcn               Varchar(30)   ,
   @cnsctvo_cdgo_rgla_vldcn       udtConsecutivo,
   @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON 

  Declare @consecutivoRiesgoAT      udtConsecutivo,
          @infrmcn_adcnl_no_cnfrmdd udtObservacion,
		  @fecha_actual             Datetime

  Set @consecutivoRiesgoAT      = 9
  Set @infrmcn_adcnl_no_cnfrmdd = 'Diagnostico Identificado como accidente de tránsito (SOAT).'
  Set @fecha_actual             = getDate()

  Insert 
  Into       #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                       llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								      )
  Select     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  From       #tmpDatosPrestacionesxSolicitud c
  Inner Join #tmpDatosAfiliado a
  On         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join bdSiSalud.dbo.tbMNRiesgosxDiagnosticos_Vigencias m With(NoLock)
  On         m.cnsctvo_cdgo_dgnstco = c.cnsctvo_cdgo_dgnstco   
  Where      m.cnsctvo_cdgo_rsgo_dgnstco = @consecutivoRiesgoAT --p.dscrpcn_rsgo_dgnstco = 'ACCIDENTE DE TRANSITO'
  And        @fecha_actual Between m.inco_vgnca And m.fn_vgnca;
END


