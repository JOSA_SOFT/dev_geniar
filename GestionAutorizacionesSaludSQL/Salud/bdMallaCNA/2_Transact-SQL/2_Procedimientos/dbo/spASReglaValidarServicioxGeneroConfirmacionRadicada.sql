USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarServicioxGeneroConfirmacionRadicada]    Script Date: 01/03/2017 17:06:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarServicioxGeneroConfirmacionRadicada
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar Si el sexo del afiliado no D\>
					 <\D corresponde al definido para la prestacion, la solicitud fue radicada en SOS D\>
					 <\D y tiene la marca de Confirmacion inconsistenca en Si D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*--------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D
                           Se realiza modificacion del procedimiento para que no tenga en cuenta el plan del
						   afiliado, y la validacion se realice para todos los planes      
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarServicioxGeneroConfirmacionRadicada] 
   @prmtro_sp_vldcn               varchar(30),
   @cnsctvo_cdgo_rgla_vldcn       udtConsecutivo,
   @cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
 
BEGIN
  SET NOCOUNT ON 

  DECLARE @consecutvioSexoAmbos        udtConsecutivo,
          @afirmacion                  udtLogico     ,
          @consecutivoMedioContactoASI udtConsecutivo,
          @conscutivoTipoCups          udtConsecutivo,
		  @infrmcn_adcnl_no_cnfrmdd    varchar(250)

  SET @consecutivoMedioContactoASI = 1;
  SET @consecutvioSexoAmbos        = 3
  SET @afirmacion                  = 'S'
  SET @conscutivoTipoCups          = 4
  Set @infrmcn_adcnl_no_cnfrmdd    = 'El servicio solicitado no corresponde con el género del afiliado, debe solicitarse al médico tratante su ajuste.'

  Insert 
  Into   #tbResultadosValidaciones(cnsctvo_cdgo_rgla_vldcn       , cnsctvo_cdgo_no_cnfrmdd_vldcn, llve_prmra_rgstro_vlddo,
                                   llve_prmra_cncpto_prncpl_vlddo, infrmcn_adcnl_no_cnfrmdd
								  )
  Select     @cnsctvo_cdgo_rgla_vldcn    , @cnsctvo_cdgo_no_cnfrmdd_vldcn, c.cnsctvo_slctd_srvco_sld_rcbda,
             c.cnsctvo_srvco_slctdo_rcbdo, @infrmcn_adcnl_no_cnfrmdd
  From       #tmpDatosPrestacionesxSolicitud c
  Inner Join #tmpDatosAfiliado a
  On         a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join bdSisalud.dbo.tbCupsServicios m WITH (NOLOCK)
  On         m.cnsctvo_prstcn = c.cnsctvo_cdfccn
  Where      c.fcha_prstcn BETWEEN m.inco_vgnca AND m.fn_vgnca
  And        m.cnsctvo_cdgo_sxo             <> @consecutvioSexoAmbos
  And        m.cnsctvo_cdgo_sxo             != a.cnsctvo_cdgo_sxo
  And        c.cnsctvo_cdgo_tpo_cdfccn       = @conscutivoTipoCups --CUPS en [tbTipoCodificacion_Vigencias
  And        c.cnfrmcn_cldd                  = @afirmacion
  And        c.cnsctvo_cdgo_mdo_cntcto_slctd = @consecutivoMedioContactoASI;
  END
