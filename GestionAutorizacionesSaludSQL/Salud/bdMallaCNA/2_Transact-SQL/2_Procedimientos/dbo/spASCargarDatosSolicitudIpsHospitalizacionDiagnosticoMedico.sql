USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudIpsHospitalizacionDiagnosticoMedico]    Script Date: 7/10/2017 9:54:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudIpsHospitalizacionDiagnosticoMedico
* Desarrollado por : <\A Jois Lombana Sánchez  A\>    
* Descripcion      : <\D Procedimiento que se encarga de actualizar en la temporal de IPS, Inf Hospitalaria,  D\>
  					 <\D Diagnosticos y médicos para realizar el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 26/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García  sisjvg01- 2017-07-06 AM\>    
* Descripcion        : <\D    Se modifica el procedimiento adicionando en la tabla temporal #IPSSl el Campo Cdgo_intrno_slctnte D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-07-06  FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCargarDatosSolicitudIpsHospitalizacionDiagnosticoMedico] 
AS
DECLARE @Valorcero as Int
DECLARE @ValorVacio as Varchar(1)
DECLARE @ValorS as varchar(1)
DECLARE @ValorN as varchar(1)
SELECT	@Valorcero = 0
		,@ValorVacio = ''
		,@ValorS = 'S'
		,@ValorN = 'N'
BEGIN
	SET NOCOUNT ON 
	/*::::::::::::::::COMPLETAR IPS SOLICITUDES::::::::::::::::::::::::::::::*/	
	--Insertar información de tabla temporal de IPS
	Insert into	#IPSSl
	(
				cnsctvo_slctd_srvco_sld_rcbda,				Tipo_Iden_Soli,						Departamento,
				Ciudad,										Cod_Tipo_Identificacion,			Cod_Departamento,
				Cod_Ciudad,									Adscrito,							NIUP,
				Numero_identificacion,						Cdgo_intrno_slctnte
	)

	Select		 ISP.cnsctvo_slctd_atrzcn_srvco				--cnsctvo_slctd_srvco_sld_rcbda
				,ISP.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	--Tipo_Iden_Soli
				,ISP.cnsctvo_cdgo_dprtmnto_prstdr			--Departamento
				,ISP.cnsctvo_cdgo_cdd_prstdr				--Ciudad
				,ISO.cdgo_tpo_idntfccn_ips_slctnte			--Cod_Tipo_Identificacion
				,ISO.cdgo_dprtmnto_prstdr					--Cod_Departamento
				,ISO.cdgo_cdd_prstdr						--Cod_Ciudad
				,ISP.adscrto								--Adscrito
				,ISO.nmro_unco_idntfccn_prstdr				--NIUP
				,ISO.nmro_idntfccn_ips_slctnte				--Numero_identificacion
				,ISP.cdgo_intrno							--Código Interno Solicitante
	From		#tmpNmroSlctds												TSO WITH (NOLOCK)
	INNER JOIN	BdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios			ISP WITH (NOLOCK)
	on			TSO.cnsctvo_slctd_srvco_sld_rcbda = ISP.cnsctvo_slctd_atrzcn_srvco
	inner join	BdCNA.gsa.tbASIPSSolicitudesAutorizacionServiciosOriginal	ISO WITH (NOLOCK)
	on			ISP.cnsctvo_slctd_atrzcn_srvco = ISO.cnsctvo_slctd_atrzcn_srvco
	AND			ISP.cnsctvo_ips_slctd_atrzcn_srvco = ISO.cnsctvo_ips_slctd_atrzcn_srvco

	-- Calcular cnsctvo_cdgo_tpo_idntfccn_ips_slctnte
	IF EXISTS (	SELECT	TIP.Tipo_Iden_Soli
				FROM	#IPSSl TIP	WITH(NOLOCK) 
				WHERE	TIP.Tipo_Iden_Soli = @Valorcero
				AND		TIP.Cod_Tipo_Identificacion <> @ValorVacio)
	BEGIN
		UPDATE		#IPSSl
		SET			Tipo_Iden_Soli = Isnull(TID.cnsctvo_cdgo_tpo_idntfccn,@Valorcero)
		FROM		#IPSSl														TIP WITH (NOLOCK)
		inner join	#Slctds												SOP WITH (NOLOCK)
		on			TIP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias	TID WITH (NOLOCK)
		on			TIP.Cod_Tipo_Identificacion = TID.cdgo_tpo_idntfccn
		WHERE		SOP.Fecha_sol Between TID.inco_vgnca And TID.fn_vgnca
		And			TIP.Tipo_Iden_Soli = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_dprtmnto_prstdr
	IF EXISTS (	SELECT	TIP.Departamento
				FROM	#IPSSl TIP	WITH(NOLOCK) 
				WHERE	TIP.Departamento = @Valorcero
				AND		TIP.Cod_Departamento <> @ValorVacio)
	BEGIN
		UPDATE		#IPSSl
		SET			Departamento = Isnull(DEP.cnsctvo_cdgo_dprtmnto,@Valorcero)
		FROM		#IPSSl													TIP WITH (NOLOCK)
		inner join	#Slctds											SOP WITH (NOLOCK)
		on			TIP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdAfiliacionValidador.dbo.tbDepartamentos_Vigencias		DEP WITH (NOLOCK)
		on			TIP.Cod_Departamento = DEP.cdgo_dprtmnto
		WHERE		SOP.Fecha_sol Between DEP.inco_vgnca And DEP.fn_vgnca
		And			TIP.Departamento = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_cdd_prstdr
	IF EXISTS (	SELECT	TIP.Ciudad
				FROM	#IPSSl TIP	WITH(NOLOCK) 
				WHERE	TIP.Ciudad = @Valorcero
				AND		TIP.Cod_Ciudad <> @ValorVacio)
	BEGIN
		UPDATE		#IPSSl
		SET			Ciudad = Isnull(CIU.cnsctvo_cdgo_cdd,@Valorcero)
		FROM		#IPSSl													TIP WITH (NOLOCK)
		inner join	#Slctds											SOP WITH (NOLOCK)
		on			TIP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdAfiliacionValidador.dbo.tbCiudades_Vigencias							CIU WITH (NOLOCK)
		on			TIP.Cod_Ciudad = CIU.cdgo_cdd
		WHERE		SOP.Fecha_sol Between  CIU.inco_vgnca And CIU.fn_vgnca
		And			TIP.Ciudad = @Valorcero		
	END
	
	-- Calcular nmro_unco_idntfccn_prstdr
	IF EXISTS (	SELECT	TIP.NIUP
				FROM	#IPSSl TIP	WITH(NOLOCK) 
				WHERE	TIP.NIUP = @Valorcero
				)
	BEGIN
		IF EXISTS (	SELECT	TIP.Adscrito 
					FROM	#IPSSl TIP	WITH(NOLOCK) 
					WHERE	TIP.NIUP = @Valorcero
					AND		TIP.Adscrito = @ValorS)
		BEGIN
			UPDATE		#IPSSl
			SET			NIUP = Isnull(TBF.nmro_unco_idntfccn_prstdr,@Valorcero)
			FROM		#IPSSl											TIP WITH (NOLOCK)
			Inner join	bdSisalud.dbo.TbPrestadores						TBF WITH(NOLOCK) 
			ON			TBF.cnsctvo_cdgo_tpo_idntfccn = TIP.Tipo_Iden_Soli
			AND			TBF.nmro_idntfccn = TIP.Numero_identificacion
			WHERE		TIP.NIUP = @Valorcero
			AND			TIP.Adscrito = @ValorS	
		END
		ELSE
		BEGIN
			UPDATE		#IPSSl
			SET			NIUP = Isnull(TBF.nmro_idntfccn,@Valorcero)
			FROM		#IPSSl											TIP WITH (NOLOCK)
			Inner Join	bdSisalud.dbo.TbPrestadores						TBF WITH(NOLOCK) 
			ON			TBF.cnsctvo_cdgo_tpo_idntfccn = TIP.Tipo_Iden_Soli
			AND			TBF.nmro_idntfccn = TIP.Numero_identificacion 
			WHERE		TIP.NIUP = @Valorcero
			AND			TIP.Adscrito = @ValorN
		END
	END

	/*::::::::::::::::COMPLETAR INFORMACIÓN HOSPITALARIA::::::::::::::::::::::::::::::*/
	-- Insertar Información en temporal de Hospitalaria
	Insert into #Hsptlra
	Select		IHP.cnsctvo_slctd_atrzcn_srvco		--cnsctvo_slctd_srvco_sld_rcbda
				,IHP.cnsctvo_cdgo_clse_hbtcn		--Clase_Habitacion
				,IHP.cnsctvo_cdgo_srvco_hsptlzcn	--Servicio_Hospitalizacion
				,IHO.cdgo_clse_hbtcn				--Cod_clase_Habitacion
				,IHO.cdgo_srvco_hsptlzcn			--Cod_servicio_Hosp
	From		#tmpNmroSlctds																TSO WITH (NOLOCK)
	INNER JOIN	BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios			IHP WITH (NOLOCK)
	on			TSO.cnsctvo_slctd_srvco_sld_rcbda = IHP.cnsctvo_slctd_atrzcn_srvco
	inner join	BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal	IHO WITH (NOLOCK)
	on			IHP.cnsctvo_slctd_atrzcn_srvco = IHO.cnsctvo_slctd_atrzcn_srvco
	AND			IHP.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco = IHO.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
	
	-- Calcular cnsctvo_cdgo_clse_hbtcn
	IF EXISTS (	SELECT	THO.Clase_Habitacion
				FROM	#Hsptlra THO WITH(NOLOCK) 
				WHERE	THO.Clase_Habitacion = @Valorcero
				AND		THO.Cod_clase_Habitacion <> @ValorVacio)
	BEGIN
			UPDATE		#Hsptlra
			SET			Clase_Habitacion = Isnull(CHB.cnsctvo_cdgo_clse_hbtcn,@Valorcero)
			FROM		#Hsptlra								THO WITH (NOLOCK)
			inner join	#Slctds								SOP WITH (NOLOCK)
			on			THO.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
			inner Join	bdSisalud.dbo.tbClasesHabitacion_Vigencias	CHB WITH (NOLOCK)
			on			THO.Cod_clase_Habitacion = CHB.cdgo_clse_hbtcn
			WHERE		SOP.fecha_Sol Between CHB.inco_vgnca And CHB.fn_vgnca
			and			THO.Clase_Habitacion = @Valorcero	
	END
	
	-- Calcular cnsctvo_cdgo_srvco_hsptlzcn
	IF EXISTS (	SELECT	THO.Servicio_Hospitalizacion
				FROM	#Hsptlra THO WITH(NOLOCK) 
				WHERE	THO.Servicio_Hospitalizacion = @Valorcero
				AND		THO.Cod_servicio_Hosp <> @ValorVacio)
	BEGIN
		UPDATE		#Hsptlra
		SET			Servicio_Hospitalizacion = Isnull(SHO.cnsctvo_cdgo_srvco_hsptlzcn,@Valorcero)
		FROM		#Hsptlra										THO WITH (NOLOCK)
		inner join	#Slctds										SOP WITH (NOLOCK)
		on			THO.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbServiciosHospitalizacion_Vigencias	SHO WITH (NOLOCK)
		on			THO.Cod_servicio_Hosp = SHO.cdgo_srvco_hsptlzcn
		WHERE		SOP.fecha_Sol Between SHO.inco_vgnca And SHO.fn_vgnca
		and			THO.Servicio_Hospitalizacion = @Valorcero
	END
		
	/*::::::::::::::::COMPLETAR TABLA DE DIAGNOSTICOS::::::::::::::::::::::::::::::*/	
	-- Insertar información en temporal de diagnosticos
	Insert into	#Dgnstcs
	Select		 DGP.cnsctvo_dgnstco_slctd_atrzcn_srvco --cnsctvo_dgnstco_slctd_atrzcn_srvco
				,DGP.cnsctvo_slctd_atrzcn_srvco	--cnsctvo_slctd_srvco_sld_rcbda
				,DGP.cnsctvo_cdgo_tpo_dgnstco	--Tipo_Diagnostico
				,DGP.cnsctvo_cdgo_rcbro			--Recobro
				,DGP.cnsctvo_cdgo_cntngnca		--Contingencia
				,DGP.cnsctvo_cdgo_dgnstco		--Codigo_Diagnostico
				,DGO.cdgo_tpo_dgnstco			--Cod_Tipo_Diag
				,DGO.cdgo_dgnstco				--Cod_Diagnostico
				,DGO.cdgo_rcbro					--Cod_Recobro
				,DGO.cdgo_cntngnca				--Cod_Contingencia
	From		#tmpNmroSlctds															TSO WITH (NOLOCK)
	INNER JOIN	BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios				DGP WITH (NOLOCK)
	on			TSO.cnsctvo_slctd_srvco_sld_rcbda = DGP.cnsctvo_slctd_atrzcn_srvco
	inner join	BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal		DGO WITH (NOLOCK)
	on			DGP.cnsctvo_slctd_atrzcn_srvco = DGO.cnsctvo_slctd_atrzcn_srvco
	AND			DGO.cnsctvo_dgnstco_slctd_atrzcn_srvco = DGP.cnsctvo_dgnstco_slctd_atrzcn_srvco
	
	-- Calcular cnsctvo_cdgo_tpo_dgnstco
	IF EXISTS (	SELECT	TDG.Tipo_Diagnostico
				FROM	#Dgnstcs TDG WITH(NOLOCK) 
				WHERE	TDG.Tipo_Diagnostico = @Valorcero
				AND		TDG.Cod_Tipo_Diag <> @ValorVacio)
	BEGIN
		UPDATE		#Dgnstcs
		SET			Tipo_Diagnostico = Isnull(TDV.cnsctvo_cdgo_tpo_dgnstco,@Valorcero)
		FROM		#Dgnstcs								TDG WITH(NOLOCK)
		Inner Join	#Slctds								SOP WITH(NOLOCK)
		on			TDG.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbTipoDiagnostico_Vigencias	TDV WITH (NOLOCK)
		on			TDG.Cod_Tipo_Diag = TDV.cdgo_tpo_dgnstco
		WHERE		SOP.fecha_sol Between TDV.inco_vgnca And TDV.fn_vgnca
		And			TDG.Tipo_Diagnostico = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_dgnstco
	IF EXISTS (	SELECT	TDG.Codigo_Diagnostico
				FROM	#Dgnstcs TDG WITH(NOLOCK) 
				WHERE	TDG.Codigo_Diagnostico = @Valorcero
				AND		TDG.Cod_Diagnostico <> @ValorVacio)
	BEGIN
		UPDATE		#Dgnstcs
		SET			Codigo_Diagnostico = Isnull(DIG.cnsctvo_cdgo_dgnstco,@Valorcero)
		FROM		#Dgnstcs								TDG WITH(NOLOCK)
		Inner Join	#Slctds								SOP WITH(NOLOCK)
		on			TDG.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbDiagnosticos_Vigencias		DIG WITH (NOLOCK)
		on			TDG.Cod_Diagnostico = DIG.cdgo_dgnstco
		WHERE		SOP.fecha_sol Between DIG.inco_vgnca And DIG.fn_vgnca
		And			TDG.Codigo_Diagnostico = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_rcbro
	IF EXISTS (	SELECT		TDI.Recobro
				FROM		#Dgnstcs TDI WITH (NOLOCK)
				WHERE		TDI.Recobro = @Valorcero
				AND			TDI.Cod_Recobro <> @ValorVacio)
	BEGIN 
		UPDATE		#Dgnstcs
		SET			Recobro = Isnull(REC.cnsctvo_cdgo_rcbro,@Valorcero)
		FROM		#Dgnstcs							DIA WITH (NOLOCK)
		Inner Join	#Slctds								SOP WITH(NOLOCK)
		on			DIA.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbRecobros_Vigencias	REC WITH (NOLOCK)
		on			DIA.Cod_Recobro = REC.cdgo_rcbro
		WHERE		SOP.Fecha_sol Between REC.inco_vgnca and REC.fn_vgnca				
		And			DIA.Recobro = @Valorcero				
	END
	
	--Calcular cnsctvo_cdgo_cntngnca
	IF EXISTS (	SELECT		TDI.Contingencia
				FROM		#Dgnstcs TDI WITH (NOLOCK)
				WHERE		TDI.Contingencia = @Valorcero
				AND			TDI.Cod_Contingencia <> @ValorVacio)
	BEGIN
		UPDATE		#Dgnstcs
		SET			Contingencia = Isnull(CVI.cnsctvo_cdgo_cntngnca,@Valorcero)
		FROM		#Dgnstcs								DIA WITH (NOLOCK)
		Inner Join	#Slctds									SOP WITH(NOLOCK)
		on			DIA.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		Inner Join	Bdsisalud.dbo.tbContingencias_Vigencias CVI WITH (NOLOCK)
		on			DIA.Cod_Contingencia = CVI.cdgo_cntngnca
		WHERE		SOP.Fecha_sol Between CVI.inco_vgnca and CVI.fn_vgnca				
		And			DIA.Contingencia = @Valorcero			
	END
	/*::::::::::::::::COMPLETAR MEDICO TRATANTE::::::::::::::::::::::::::::::*/
	-- Insertar información de tabla temporal Medico
	Insert into		#Mdcs
	Select			MTP.cnsctvo_slctd_atrzcn_srvco				--cnsctvo_slctd_srvco_sld_rcbda
					,MTP.cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		--Tipo_Afiliacion
					,MTP.cnsctvo_cdgo_espcldd_mdco_trtnte		--Especialidad
					,MTP.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	--Tipo_identificacion
					,MTO.cdgo_tpo_aflcn_mdco_trtnte				--Cod_Tipo_Afiliacion
					,MTO.cdgo_espcldd_mdco_trtnte				--Cod_esp_Med_trat
					,MTO.cdgo_tpo_idntfccn_mdco_trtnte			--Cod_Tipo_Iden
	From			#tmpNmroSlctds														TSO WITH (NOLOCK)
	INNER JOIN		BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios			MTP WITH (NOLOCK)
	on				TSO.cnsctvo_slctd_srvco_sld_rcbda = MTP.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN		BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal	MTO WITH (NOLOCK)
	on				MTP.cnsctvo_slctd_atrzcn_srvco = MTO.cnsctvo_slctd_atrzcn_srvco
	AND				MTP.cnsctvo_mdco_trtnte_slctd_atrzcn_srvco = MTO.cnsctvo_mdco_trtnte_slctd_atrzcn_srvco
	
	-- Calcular cnsctvo_cdgo_tpo_aflcn_mdco_trtnte
	IF EXISTS (	SELECT	TME.Tipo_Afiliacion
				FROM	#Mdcs TME WITH(NOLOCK) 
				WHERE	TME.Tipo_Afiliacion = @Valorcero
				AND		TME.Cod_Tipo_Afiliacion <> @ValorVacio)
	BEGIN
		UPDATE		#Mdcs
		SET			Tipo_Afiliacion = Isnull(TAM.cnsctvo_cdgo_tpo_aflcn_mdco,@Valorcero)
		FROM		#Mdcs															MTP WITH (NOLOCK)
		inner join	#Slctds														SOP WITH (NOLOCK)
		on			MTP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdGestionSolicitudesSalud.dbo.tbTiposAfiliacionMedico_Vigencias		TAM WITH (NOLOCK)
		on			MTP.Cod_Tipo_Afiliacion = TAM.cdgo_tpo_aflcn_mdco
		WHERE		SOP.fecha_sol Between TAM.inco_vgnca And TAM.fn_vgnca
		And			MTP.Tipo_Afiliacion = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_espcldd_mdco_trtnte
	IF EXISTS (	SELECT	TME.Especialidad
				FROM	#Mdcs TME WITH(NOLOCK) 
				WHERE	TME.Especialidad = @Valorcero
				AND		TME.Cod_esp_Med_trat <> @ValorVacio)
	BEGIN
		UPDATE		#Mdcs
		SET			Especialidad = Isnull(ESP.cnsctvo_cdgo_espcldd,@Valorcero)
		FROM		#Mdcs															MTP WITH (NOLOCK)
		inner join	#Slctds														SOP WITH (NOLOCK)
		on			MTP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbEspecialidades_Vigencias							ESP WITH (NOLOCK)
		on			MTP.Cod_esp_Med_trat = ESP.cdgo_espcldd
		WHERE		SOP.fecha_sol Between ESP.inco_vgnca And ESP.fn_vgnca
		And			MTP.Especialidad = @Valorcero
	END
	
	-- Calcular cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte
	IF EXISTS (	SELECT	TME.Tipo_identificacion
				FROM	#Mdcs TME WITH(NOLOCK) 
				WHERE	TME.Tipo_identificacion = @Valorcero
				AND		TME.Cod_Tipo_Iden <> @ValorVacio)
	BEGIN 
		UPDATE		#Mdcs
		SET			Tipo_identificacion = Isnull(TID.cnsctvo_cdgo_tpo_idntfccn,@Valorcero)
		FROM		#Mdcs															MTP WITH (NOLOCK)
		inner join	#Slctds														SOP WITH (NOLOCK)
		on			MTP.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias			TID WITH (NOLOCK)
		on			MTP.Cod_Tipo_Iden = TID.cdgo_tpo_idntfccn
		WHERE		SOP.fecha_sol Between TID.inco_vgnca And TID.fn_vgnca
		And			MTP.Tipo_identificacion = @Valorcero
	END		
END




