USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitudServiciosInsumos]    Script Date: 7/6/2017 3:12:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitudServiciosInsumos
* Desarrollado por : <\A Jois Lombana S�nchez  A\>    
* Descripcion      : <\D Procedimiento que se encarga de actualizar en la temporal de Insumos  D\>
  					 <\D para realizar el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 26/11/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D	Se ajusta procedimiento para siempre obtener la cantidad maxima y la 
							frecuencia de entrega de tbCupsServiciosxPlanes D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/07/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCargarDatosSolicitudServiciosInsumos] 
AS
DECLARE @Valorcero as Int
DECLARE @ValorVacio as Varchar(1)
DECLARE @Procedimiento as Varchar(1)
SELECT	@Valorcero = 0
		,@ValorVacio = ''
		,@Procedimiento = '4'--CUPS

BEGIN
	SET NOCOUNT ON 
	/*::::::::::::::::COMPLETAR TABLA INSUMOS::::::::::::::::::::::::::::::*/	
	-- Insertar Informacion de insumos temporal
	Insert into #Insms
	Select		TSV.cnsctvo_slctd_atrzcn_srvco			--cnsctvo_slctd_srvco_sld_rcbda
				,INP.cnsctvo_srvco_slctdo				--cnsctvo_srvco_slctdo_rcbdo
				,INP.cnsctvo_cdgo_ltrldd				--Lateralidad
				,INP.cnsctvo_cdgo_va_accso				--Via_Acceso
				,INP.undd_cntdd							--Unidad_cantidad
				,INP.frcnca_entrga						--Frecuencia_Entrega
				,INP.cntdd_mxma							--Cantidad_Max_Permitida
				,INO.cdgo_ltrldd						--Cod_Lateralidad
				,INO.cdgo_va_accso						--Cod_Via_acceso
				,INP.accso_drcto						--Acceso_Directo
	From		#tmpNmroSlctds											TSO WITH (NOLOCK)
	INNER JOIN	BdCNA.gsa.tbASServiciosSolicitados						TSV WITH (NOLOCK)
	on			TSO.cnsctvo_slctd_srvco_sld_rcbda = TSV.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BdCNA.gsa.tbASProcedimientosInsumosSolicitados			INP WITH (NOLOCK)
	on			TSV.cnsctvo_srvco_slctdo = INP.cnsctvo_srvco_slctdo
	inner join	BdCNA.gsa.tbASProcedimientosInsumosSolicitadosOriginal	INO WITH (NOLOCK)
	on			INP.cnsctvo_prcdmnto_insmo_slctdo = INO.cnsctvo_prcdmnto_insmo_slctdo
		
	-- Calcular cnsctvo_cdgo_ltrldd
	IF EXISTS (	SELECT	TIN.Lateralidad
				FROM	#Insms TIN WITH(NOLOCK) 
				WHERE	TIN.Lateralidad = @Valorcero
				AND		TIN.Cod_Lateralidad <> @ValorVacio)
	BEGIN 
		UPDATE		#Insms
		SET			Lateralidad = Isnull(LAT.cnsctvo_cdgo_ltrldd,@Valorcero)
		FROM		#Insms									INS WITH (NOLOCK)
		inner join	#Slctds									SOP WITH (NOLOCK)
		on			INS.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner Join	bdSisalud.dbo.tbLateralidades_Vigencias	LAT WITH (NOLOCK)
		on			INS.Cod_Lateralidad = LAT.cdgo_ltrldd
		WHERE		SOP.fecha_sol Between LAT.inco_vgnca And LAT.fn_vgnca
		and			INS.Lateralidad = @Valorcero
	END
    
	-- Calcular cntdd_mxma_prmtda y frcnca_entrga
	UPDATE		#Insms
	SET			Cantidad_Max_Permitida = SPL.cntdd_mxma_prmtda,
				Frecuencia_Entrega = SPL.frcnca_entrga
	FROM		#Insms									INS WITH (NOLOCK)
	Inner join	#Srvcs									SER WITH (NOLOCK)
	on			SER.cnsctvo_slctd_srvco_sld_rcbda = INS.cnsctvo_slctd_srvco_sld_rcbda
	AND			SER.cnsctvo_srvco_slctdo_rcbdo = INS.cnsctvo_srvco_slctdo_rcbdo
	Inner join	#Aflds									TAF	WITH (NOLOCK)
	on			SER.cnsctvo_slctd_srvco_sld_rcbda = TAF.cnsctvo_slctd_srvco_sld_rcbda
	Inner join	bdSisalud.dbo.tbCupsServiciosxPlanes	SPL	WITH(NOLOCK) 
	on			SPL.cnsctvo_prstcn = SER.Servicio_Solicitado
	AND			TAF.Codigo_Plan = SPL.cnsctvo_cdgo_pln
	WHERE		SER.Tipo= @Procedimiento
		
	--cnsctvo_cdgo_va_accso
	IF EXISTS (	SELECT	TIN.Via_Acceso
				FROM	#Insms TIN WITH(NOLOCK) 
				WHERE	TIN.Via_Acceso = @Valorcero
				AND		TIN.Cod_Via_acceso <> @ValorVacio)
	BEGIN 
		UPDATE		#Insms
		SET			Via_Acceso = Isnull(VAC.cnsctvo_cdgo_va_accso,@Valorcero)
		FROM		#Insms									INS WITH (NOLOCK)
		inner join	#Slctds									SOP WITH (NOLOCK)
		on			INS.cnsctvo_slctd_srvco_sld_rcbda = SOP.cnsctvo_slctd_srvco_sld_rcbda
		inner join	bdSisalud.dbo.tbViasAccesoQx_Vigencias	VAC	WITH(NOLOCK) 
		on			INS.Cod_Via_acceso = VAC.cdgo_va_accso
		WHERE		SOP.Fecha_Sol BETWEEN VAC.inco_vgnca and VAC.fn_vgnca
		AND			INS.Via_Acceso = @Valorcero
	END

	-- Calcular undd_cntdd
	IF EXISTS (	SELECT	TIN.Unidad_cantidad
				FROM	#Insms TIN WITH(NOLOCK) 
				WHERE	TIN.Unidad_cantidad = @ValorVacio)
	BEGIN
		UPDATE		#Insms
		Set			Unidad_Cantidad = ISNULL(PLA.mdda_cntdd,@ValorVacio)
		FROM		#Insms									INS WITH (NOLOCK)
		inner join	#Srvcs									SER WITH (NOLOCK)
		on			SER.cnsctvo_slctd_srvco_sld_rcbda = INS.cnsctvo_slctd_srvco_sld_rcbda
		AND			SER.cnsctvo_srvco_slctdo_rcbdo = INS.cnsctvo_srvco_slctdo_rcbdo
		inner join	#Aflds														TAF	WITH (NOLOCK)
		on			SER.cnsctvo_slctd_srvco_sld_rcbda = TAF.cnsctvo_slctd_srvco_sld_rcbda
		inner join	bdSisalud.dbo.tbCupsServiciosxPlanes	PLA WITH (NOLOCK)		
		ON			SER.Servicio_Solicitado = PLA.cnsctvo_prstcn
		AND			TAF.Codigo_Plan = PLA.cnsctvo_cdgo_pln
		Where		SER.Tipo = @Procedimiento
		and			INS.Unidad_cantidad = @ValorVacio
	END

	--Calcular Acceso_Directo
	IF EXISTS (	SELECT	TIN.Acceso_Directo
				FROM	#Insms TIN WITH(NOLOCK) 
				WHERE	TIN.Acceso_Directo = @ValorVacio)
	BEGIN
		UPDATE		#Insms
		Set			Acceso_Directo = ISNULL(PLA.accso_drcto,@ValorVacio)
		FROM		#Insms									INS WITH (NOLOCK)
		inner join	#Srvcs									SER WITH (NOLOCK)
		on			SER.cnsctvo_slctd_srvco_sld_rcbda = INS.cnsctvo_slctd_srvco_sld_rcbda
		AND			SER.cnsctvo_srvco_slctdo_rcbdo = INS.cnsctvo_srvco_slctdo_rcbdo
		inner join	#Aflds														TAF	WITH (NOLOCK)
		on			SER.cnsctvo_slctd_srvco_sld_rcbda = TAF.cnsctvo_slctd_srvco_sld_rcbda
		inner join	bdSisalud.dbo.tbCupsServiciosxPlanes	PLA WITH (NOLOCK)		
		ON			SER.Servicio_Solicitado = PLA.cnsctvo_prstcn
		AND			TAF.Codigo_Plan = PLA.cnsctvo_cdgo_pln
		Where		SER.Tipo = @Procedimiento
		and			INS.Acceso_Directo = @ValorVacio
	END
	

END


