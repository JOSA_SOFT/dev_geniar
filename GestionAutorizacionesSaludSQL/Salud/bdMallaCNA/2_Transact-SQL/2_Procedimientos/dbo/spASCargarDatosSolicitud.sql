USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASCargarDatosSolicitud]    Script Date: 07/07/2017 10:34:08 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASCargarDatosSolicitud
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de cargar la información que se encuentre de la D\>
  					 <\D solicitud para realizar el procesamiento de la malla. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 27/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Rodriguez  AM\>    
* Descripcion        : <\D  Se actualiza la cantidad máxima permitida para cada una de las prestaciones D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 05-10-2016  FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega carga de datos para llenar la tabla temporal #tmpAtencionConceptosOPS
						   para que cargue la informacion de Mega cuando las solicitudes aun no se 
						   encuentren en Sipres.
						   Se cambia consulta de llenado de la tabla temporal #tmpAtencionConceptosOPS
						   se cambia la tabla tbConceptosOps por tbProcedimientos y el campo cntdd_prstcn
						   por cntdd_prstcn_rl D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 17-03-2017  FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se modifica referencia para validacion de los estados en el llenado de la
						   tabla temporal #tmpAtencionConceptosOPS para validar con la tabla tbAtencionOPS  
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 05-04-2017  FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D	Se agrega cruce faltante en consulta a tbCupsServiciosxPlanes para recuperar el 
							valor del campo cntdd_mxma_prmtda_x_slctd de la tabla temporal #tmpDatosPrestacionesxSolicitud
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06-07-2017  FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D	Se ajusta procedimientos para cargar correctamente los campos cntdd_mxma
							y cntdd_mxma_prmtda_x_slctd
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 07-07-2017  FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [dbo].[spASCargarDatosSolicitud] 
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
  SET NOCOUNT On 
  BEGIN

    DECLARE @indice									udtConsecutivo,
            @numeroRegistros						udtConsecutivo,
            @lntipoidentificacion					udtConsecutivo,
            @lnnumeroidentificacion					udtNumeroIdentificacionLargo,
            @lnplan									udtConsecutivo,
            @afirmacion								char(1),
            @negacion								char(1),
            @cnsctvo_cdgo_tpo_plnPAC				int,
            @cnsctvo_cdgo_tpo_plnPOS				int,
            @mnsje									varchar(80),
            @cdgoCdfccn								char(30),
            @consecutivoCodificacion				udtConsecutivo,
            @consecutivoCodigoTipoContrato			int,
            @consecutivoTipoCodificacion			udtConsecutivo,
            @cnsctvo_tpo_cdfccn_cups				int,
            @cnsctvo_tpo_cdfccn_cums				int,
            @consecutivoCero						int,
            @consecutivoTipoDiagnosticoPrincipal	int,
			@estadoAprobada							int,
			@descEstadoAprobada						varchar(10),
			@estadoAnulada							numeric(1),
			@estadoNoAutorizada						numeric(1),
			@estadoNoTramitada						numeric(1),
			@estadoVencidaCobro						numeric(2),
			@estadoVencidaPorNoTramite				numeric(2),
			@inDocumentosSoporte					XML,
			@consecutivoModeloDocumento				udtConsecutivo,
			@estadoAnuladoMega						UdtConsecutivo,
			@estadoNoAutorizadoMega					UdtConsecutivo,
			@estadoDevueltoMega						UdtConsecutivo,
			@estadoVencidaPorNoTramiteMega			UdtConsecutivo,
			@vlr_cro								Int;
	
	SET @consecutivoModeloDocumento = 4
    SET @cnsctvo_tpo_cdfccn_cups = 4
    SET @cnsctvo_tpo_cdfccn_cums = 5
    SET @indice = 1
    SET @numeroRegistros = 0
    SET @afirmacion = 'S'
    SET @negacion = 'N'
    SET @cnsctvo_cdgo_tpo_plnPAC = 2
    SET @cnsctvo_cdgo_tpo_plnPOS = 1
    SET @consecutivoCero = 0
	SET @consecutivoTipoDiagnosticoPrincipal= 1
	SET @estadoAprobada = 4
	SET @descEstadoAprobada ='APROBADA'
	SET @estadoAnulada = 3;
	SET @estadoNoAutorizada = 2;
	SET @estadoNoTramitada = 4;
	SET @estadoVencidaCobro = 11;
	SET @estadoVencidaPorNoTramite = 12;
	Set	@estadoAnuladoMega = 14;
	Set	@estadoNoAutorizadoMega = 8;
	Set @estadoDevueltoMega	= 6;
	Set @estadoVencidaPorNoTramiteMega = 10;
	Set  @vlr_cro = 0;

	
    --Se inserta la información asociada a la solicitud
    Insert Into #tmpDatosSolicitud 
	(			
					cnsctvo_slctd_srvco_sld_rcbda,
					nmro_slctd_prvdr,
					nmro_slctd_ss_rmplzo,
					cnsctvo_cdgo_estdo_slctd,
					fcha_crcn,
					fcha_slctd,
					cnsctvo_cdgo_tpo_trnsccn_srvco_sld,
					nmro_slctd_pdre,
					nmro_slctd_ss,
					cnsctvo_estdo_slctd,
					dscrpcn_estdo_slctd,
					orgn_atncn
	)
      Select		a.cnsctvo_slctd_atrzcn_srvco,
					a.nmro_slctd_prvdr,
					a.nmro_slctd_ss_rmplzo,
					a.cnsctvo_cdgo_estdo_slctd,
					a.fcha_crcn,
					a.fcha_slctd,
					a.cnsctvo_cdgo_tpo_trnsccn_srvco_sld,
					a.nmro_slctd_pdre,
					a.nmro_slctd_atrzcn_ss,
					@estadoAprobada,
					@descEstadoAprobada,
					a.cnsctvo_cdgo_orgn_atncn
      From			bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
      Inner Join	#tmpNmroSlctds s WITH (NOLOCK)
      On			(a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_srvco_sld_rcbda);

    --Se inserta la información en la tabla de prestacions por solicitud
    Insert Into #tmpDatosPrestacionesxSolicitud 
	(	
					cnsctvo_srvco_slctdo_rcbdo,
					cnsctvo_slctd_srvco_sld_rcbda,
					fcha_prstcn,
					fcha_crcn,
					cnfrmcn_cldd,
					ds_dfrnca,
					fcha_anno_antrr,
					cnsctvo_estdo_prstcn,
					dscrpcn_estdo_prstcn,
					orgn_atncn
	)
      Select		b.cnsctvo_srvco_slctdo,
					a.cnsctvo_slctd_srvco_sld_rcbda,
					a.fcha_slctd,
					b.fcha_crcn,
					ISNULL(b.cnfrmcn_cldd, @negacion),
					DATEDIFF(DAY, a.fcha_slctd,b.fcha_crcn),
					DATEADD(YEAR, -1, a.fcha_slctd),
					@estadoAprobada,
					@descEstadoAprobada,
					a.orgn_atncn
      From			#tmpDatosSolicitud a
      Inner Join	bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)
      On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)
      Left Join		bdCNA.gsa.tbASServiciosSolicitadosOriginal bo WITH (NOLOCK)
      On			(bo.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
      And			bo.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo);

    --Se inserta la información del afiliado.
    Insert Into #tmpDatosAfiliado (
					cnsctvo_afldo_slctd_rcbda,
					cnsctvo_slctd_srvco_sld_rcbda,
					cnsctvo_cdgo_tpo_idntfccn_afldo,
					cdgo_tpo_idntfccn_afldo,
					nmro_idntfccn_afldo,
					cnsctvo_cdgo_pln,
					cdgo_pln,
					cnsctvo_cdgo_pln_pc,
					prmr_nmbre_afldo,
					prmr_aplldo_afldo,
					cnsctvo_cdgo_sxo,
					cdgo_sxo,
					rcn_ncdo,
					cnsctvo_cdgo_cdd_afldo,
					cdgo_cdd_afldo,
					drccn_afldo,
					fcha_ncmnto,
					prto_mltple,
					nro_hjs,
					ips_prmria,
					cnsctvo_sde_ips_prmria,
					cnsctvo_estdo_drcho_pos,
					cnsctvo_bnfcro,
					nmro_cntrto,
					cnsctvo_tpo_cntrto,
					edd_afldo,
					edd_mss,
					smns_ctzds,
					cnsctvo_rngo_slrl,
					cnsctvo_cdgo_chrte,
					fcha_slctd,
					afldo_exstnt,
					nmro_unco_idntfccn,
					cnsctvo_cdgo_tpo_pln
	)
    Select			b.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,
					a.cnsctvo_slctd_srvco_sld_rcbda,
					b.cnsctvo_cdgo_tpo_idntfccn_afldo,
					c.cdgo_tpo_idntfccn_afldo,
					b.nmro_idntfccn_afldo,
					CASE
					  WHEN b.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_plnPOS THEN b.cnsctvo_cdgo_pln
					END,
					c.cdgo_pln,
					CASE
					  WHEN b.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_plnPAC THEN b.cnsctvo_cdgo_pln
					END,
					c.prmr_nmbre_afldo,
					c.prmr_aplldo_afldo,
					b.cnsctvo_cdgo_sxo,
					c.cdgo_sxo,
					b.rcn_ncdo,
					b.cnsctvo_cdgo_cdd_rsdnca_afldo,
					c.cdgo_cdd_rsdnca_afldo,
					c.drccn_afldo,
					c.fcha_ncmnto_afldo,
					b.prto_mltple,
					b.nmro_hjo_afldo,
					b.cdgo_ips_prmra,
					b.cnsctvo_cdgo_sde_ips_prmra,
					b.cnsctvo_cdgo_estdo_drcho,
					b.cnsctvo_bnfcro_cntrto,
					b.nmro_cntrto,
					b.cnsctvo_cdgo_tpo_cntrto,
					b.edd_afldo_ans,
					b.edd_afldo_mss,
					b.smns_ctzds,
					b.cnsctvo_cdgo_rngo_slrl,
					b.cnsctvo_cdgo_chrte,
					a.fcha_slctd,
					(CASE
					  WHEN b.nmro_unco_idntfccn_afldo = @consecutivoCero THEN @negacion
					  ELSE @afirmacion
					END
					) AS existente,
					b.nmro_unco_idntfccn_afldo,
					b.cnsctvo_cdgo_tpo_pln
      From			#tmpDatosSolicitud a WITH (NOLOCK)
      Inner Join	bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
      On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)
      Left Join		bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
      On			(a.cnsctvo_slctd_srvco_sld_rcbda = c.cnsctvo_slctd_atrzcn_srvco);

    --Se actualiza la información encabezado de la solicitud
    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cnsctvo_cdgo_tpo_srvco = b.cnsctvo_cdgo_tpo_srvco_slctdo,
				cdgo_tpo_srvco = c.cdgo_tpo_srvco_slctdo,
				cnsctvo_cdgo_orgn_atncn = b.cnsctvo_cdgo_orgn_atncn,
				cdgo_orgn_atncn = c.cdgo_orgn_atncn,
				cnsctvo_cdgo_prrdd_atncn = b.cnsctvo_cdgo_prrdd_atncn,
				cdgo_prrdd_atncn = c.cdgo_prrdd_atncn,
				cnsctvo_cdgo_clse_atncn = b.cnsctvo_cdgo_frma_atncn,
				cdgo_clse_atncn = C.cdgo_frma_atncn,
				jstfccn_clnca = b.jstfccn_clnca,
				tpe_mxmo_st = b.spra_tpe_st,
				cnsctvo_cdgo_mdo_cntcto_slctd = b.cnsctvo_cdgo_mdo_cntcto_slctd,
				cdgo_mdo_cntcto_slctd = c.cdgo_mdo_cntcto_slctd
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH (NOLOCK)
    On			a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco
    Left Join	bdCNA.gsa.tbASSolicitudesAutorizacionServiciosOriginal c WITH (NOLOCK)
    On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco;

    --Se actualiza la información correspondiente al médico
    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = b.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
				cdgo_tpo_idntfccn_mdco_trtnte = c.cdgo_tpo_idntfccn_mdco_trtnte,
				nmro_idntfccn_mdco_trtnte = b.nmro_idntfccn_mdco_trtnte,
				prmr_nmbre_mdco_trtnte = c.prmr_nmbre_mdco_trtnte,
				sgndo_nmbre_mdco_trtnte = c.sgndo_nmbre_mdco_trtnte,
				prmr_aplldo_mdco_trtnte = c.prmr_aplldo_mdco_trtnte,
				sgndo_aplldo_mdco_trtnte = c.sgndo_aplldo_mdco_trtnte,
				rgstro_mdco_trtnte = b.rgstro_mdco_trtnte,
				cnsctvo_cdgo_espcldd_mdco_trtnte = b.cnsctvo_cdgo_espcldd_mdco_trtnte,
				cdgo_espcldd_mdco_trtnte = c.cdgo_espcldd_mdco_trtnte,
				mdco_adscrto = b.adscrto,
				nmro_unco_idntfccn_mdco = b.nmro_unco_idntfccn_mdco
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios b WITH (NOLOCK)
    On			a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco
    Left Join	bdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
    On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco;

    --Se actualiza la información de los diagnosticos.
    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cnsctvo_cdgo_dgnstco = b.cnsctvo_cdgo_dgnstco,
				cdgo_dgnstco = c.cdgo_dgnstco,
				cnsctvo_cdgo_tpo_dgnstco = b.cnsctvo_cdgo_tpo_dgnstco
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios b WITH (NOLOCK)
    On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco
    And			b.cnsctvo_cdgo_tpo_dgnstco = @consecutivoTipoDiagnosticoPrincipal) --Se consulta sólo el diagnostico principal
    Left Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
    On			(c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco);

    --Se actualiza la información hospitalaria.

    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cnsctvo_cdgo_srvco_hsptlzcn = b.cnsctvo_cdgo_srvco_hsptlzcn,
				cdgo_srvco_hsptlzcn = c.cdgo_srvco_hsptlzcn,
				fcha_ingrso_hsptlzcn = b.fcha_ingrso_hsptlzcn,
				fcha_egrso_hsptlzcn = b.fcha_egrso_hsptlzcn,
				cma = b.cma,
				cnsctvo_cdgo_clse_hbtcn = b.cnsctvo_cdgo_clse_hbtcn,
				cdgo_clse_hbtcn = c.cdgo_clse_hbtcn
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios b WITH (NOLOCK)
    On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)
    Left Join	bdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
    On			(c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco);

    --Se actualiza la información del prestador.

    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			prstdr_adscrto = b.adscrto,
				cnsctvo_cdgo_tpo_idntfccn_prstdr = b.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte,
				cdgo_tpo_idntfccn_prstdr = c.cdgo_tpo_idntfccn_ips_slctnte,
				nmro_idntfccn_prstdr = b.nmro_idntfccn_ips_slctnte,
				cdgo_intrno_ips = b.cdgo_intrno,
				nmbre_prstdr = c.nmbre_ips,
				cnsctvo_cdgo_cdd_prstdr = b.cnsctvo_cdgo_cdd_prstdr,
				cdgo_cdd_prstdr = c.cdgo_cdd_prstdr,
				nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios b WITH (NOLOCK)
    On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)
    Left Join	bdCNA.gsa.tbASIPSSolicitudesAutorizacionServiciosOriginal c WITH (NOLOCK)
    On			(c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco);

    --Se actualiza la información relacionada al encabezado de las prestaciones.

    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cnsctvo_cdgo_tpo_cdfccn = b.cnsctvo_cdgo_tpo_srvco,
				cdgo_tpo_cdfccn = c.cdgo_tpo_srvco,
				cnsctvo_cdfccn = b.cnsctvo_cdgo_srvco_slctdo,
				cdgo_cdfccn = c.cdgo_srvco_slctdo,
				dscrpcn_cdfccn = b.dscrpcn_srvco_slctdo,
				cntdd_slctda = b.cntdd_slctda,
				prstcn_cptda = b.prstcn_cptda,
				prgrmcn_entrga = b.prgrmcn_entrga,
				crgo_drccnmnto = b.crgo_drccnmnto,
				indcdr_no_ps = b.indcdr_no_ps,
				prstcn_vgnte = b.prstcn_vgnte
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)
    On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco
    And			a.cnsctvo_srvco_slctdo_rcbdo = b.cnsctvo_srvco_slctdo)
    Left Join	bdCNA.gsa.tbASServiciosSolicitadosOriginal c WITH (NOLOCK)
    On			(c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
    And			c.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo)


	--Se actualiza la cantidad máxima permitida para cada una de las prestaciones
	UPDATE		#tmpDatosPrestacionesxSolicitud
	SET			cntdd_mxma = SPL.cntdd_mxma
	From		#tmpDatosPrestacionesxSolicitud a
	Inner Join	#tmpDatosAfiliado b
	On			b.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdSisalud.dbo.tbCupsServiciosxPlanes	SPL	WITH(NOLOCK) 
	On			SPL.cnsctvo_prstcn = a.cnsctvo_cdfccn
	And			spl.cnsctvo_cdgo_pln = b.cnsctvo_cdgo_pln_pc


	UPDATE		#tmpDatosPrestacionesxSolicitud
	SET			cntdd_mxma = SPL.cntdd_mxma
	From		#tmpDatosPrestacionesxSolicitud a
	Inner Join	#tmpDatosAfiliado b
	On			b.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdSisalud.dbo.tbCupsServiciosxPlanes	SPL	WITH(NOLOCK) 
	On			SPL.cnsctvo_prstcn = a.cnsctvo_cdfccn
	And			spl.cnsctvo_cdgo_pln = b.cnsctvo_cdgo_pln


    --Se actualiza la información relacionada a los medicamentos para las prestaciones.

    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			fcha_vncmnto_invma = e.fcha_vncmnto_invma,
				mstra_mdca = e.mstra_mdca,
				mss_mxmo_rnvcn = e.mss_mxmo_rnvcn,
				mdcmnto_uso_cndcndo = e.uso_cndcndo,
				cnsctvo_cdgo_va_accso = e.cnsctvo_cdgo_va_admnstrcn_mdcmnto,
				cdgo_va_accso = ee.cdgo_va_admnstrcn_mdcmnto,
				dss = e.dss,
				cnsctvo_cdgo_prsntcn_dss = e.cnsctvo_cdgo_prsntcn_dss,
				cdgo_prsntcn_dss = ee.cdgo_prsntcn_dss,
				prdcdd_dss = e.prdcdd_dss,
				cnsctvo_cdgo_undd_prdcdd_dss = e.cnsctvo_cdgo_undd_prdcdd_dss,
				cdgo_undd_prdcdd_dss = ee.cdgo_undd_prdcdd_dss,
				cncntrcn_dss = e.cncntrcn_dss,
				cnsctvo_cdgo_undd_cncntrcn_dss = e.cnsctvo_cdgo_undd_cncntrcn_dss,
				cdgo_undd_cncntrcn_dss = ee.cdgo_undd_cncntrcn_dss,
				prsntcn = e.prsntcn_dss,
				cnsctvo_cdgo_prsntcn = e.cnsctvo_cdgo_prsntcn,
				cdgo_prsntcn = ee.cdgo_prsntcn
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASMedicamentosSolicitados e WITH (NOLOCK)
    On			(e.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo_rcbdo
    And			a.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cums) -- CUMS = 5
    Left Join	bdCNA.gsa.tbASMedicamentosSolicitadosOriginal ee WITH (NOLOCK)
    On			(a.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cums	-- CUMS = 5
    And			ee.cnsctvo_mdcmnto_slctdo = e.cnsctvo_mdcmnto_slctdo);

    --Se actualiza la información relacionada a los procedimientos para las prestaciones.
    UPDATE		#tmpDatosPrestacionesxSolicitud
    SET			cntdd_mxma_prmtda_x_slctd = d.cntdd_mxma,
				prstcn_accso_drcto = d.accso_drcto,
				frcnca_entrga = d.frcnca_entrga,
				mdda_cntdd = d.undd_cntdd,
				cnsctvo_cdgo_ltrldd = d.cnsctvo_cdgo_ltrldd,
				cdgo_ltrldd = dd.cdgo_ltrldd
    From		#tmpDatosPrestacionesxSolicitud a
    Left Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados d WITH (NOLOCK)
    On			(d.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo_rcbdo
    And			a.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cups) -- CUPS = 4
    Left Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitadosOriginal dd WITH (NOLOCK)
    On			(a.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cups  -- CUPS = 4
    And			dd.cnsctvo_prcdmnto_insmo_slctdo = d.cnsctvo_prcdmnto_insmo_slctdo);

    --Se actualizan los documentos adjuntados para las prestaciones.
	SET @inDocumentosSoporte = (Select	prestacion.cdgo_cdfccn codigoprestacion,
										(CASE
										WHEN DPS.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cums 
											THEN TBC.cnsctvo_cdgo_itm_prspsto
										WHEN DPS.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cups 
											THEN TCS.cnsctvo_cdgo_itm_prspsto
										END) itempresupuesto
								From		#tmpDatosPrestacionesxSolicitud DPS
								Inner Join	bdSisalud.dbo.tbCodificaciones prestacion WITH(NOLOCK)
								On			prestacion.cnsctvo_cdfccn = DPS.cnsctvo_cdfccn
								Left Join	bdSisalud.dbo.tbCums			  TBC WITH(NOLOCK)
								On			prestacion.cnsctvo_cdfccn = TBC.cnsctvo_cms
								And			DPS.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cums
								Left Join	bdSisalud.dbo.tbCupsServicios	TCS WITH(NOLOCK)
								On			TCS.cnsctvo_prstcn = prestacion.cnsctvo_cdfccn
								And			DPS.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_tpo_cdfccn_cups
								FOR XML AUTO, TYPE, ELEMENTS, ROOT('tiposDocumento'))
								--Se realiza la construcción del XML que corresponde al parámetro de entrada.

	Insert Into #tempDocPrestacionesLoc (
			cnsctvo_cdgo_dcmnto_sprte,
			cdgo_dcmnto_sprte,
			dscrpcn_dcmnto_sprte,
			tmno_mxmo,
			url_ejmplo,
			frmts_prmtdo,
			oblgtro,
			cntdd_mxma_sprts_crgr,
			cnsctvo_cdgo_tpo_imgn,
			cdgo_cdfccn
	)
	EXEC bdCNA.gsa.spASConsultaTiposDocumentoSoporte @inDocumentosSoporte, @consecutivoModeloDocumento

	Insert Into #tmpDatosDocumentosAnexos (
				cnsctvo_slctd_srvco_sld_rcbda,
				cnsctvo_cdgo_dcmnto_sprte
	)
	Select		a.cnsctvo_slctd_srvco_sld_rcbda,
				b.cnsctvo_cdgo_dcmnto_sprte
    From		#tmpDatosPrestacionesxSolicitud a
    Inner Join	bdCNA.gsa.tbASDocumentosAnexosServicios b WITH (NOLOCK)
    On			(a.cnsctvo_slctd_srvco_sld_rcbda = b.cnsctvo_slctd_atrzcn_srvco)

    Insert Into #tmpDatosSolicitudes (
			cnsctvo_slctd_srvco_sld_rcbda,
			cnsctvo_cmpo_vldcn,
			fcha,
			nmro_unco_idntfcn_afldo,
			cnsctvo_cdgo_dgnstco,
			cnsctvo_cdgo_pln,
			edd,
			cnsctvo_cdgo_tpo_cdfccn,
			cnsctvo_cdfccn,
			cnsctvo_cdgo_frma_atncn,
			cnsctvo_cdgo_cdd_rsdnca,
			cdgo_intrno,
			cnsctvo_cdgo_estdo_drcho
	)
    Select		c.cnsctvo_slctd_srvco_sld_rcbda,
				c.cnsctvo_srvco_slctdo_rcbdo,
				c.fcha_prstcn,
				a.nmro_unco_idntfccn,
				c.cnsctvo_cdgo_dgnstco,
				(CASE
				  WHEN a.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_plnPOS THEN a.cnsctvo_cdgo_pln
				  WHEN a.cnsctvo_cdgo_tpo_pln = @cnsctvo_cdgo_tpo_plnPAC THEN a.cnsctvo_cdgo_pln_pc
				END),
				a.edd_afldo,
				c.cnsctvo_cdgo_tpo_cdfccn,
				c.cnsctvo_cdfccn,
				c.cnsctvo_cdgo_clse_atncn,
				a.cnsctvo_cdgo_cdd_afldo,
				a.ips_prmria,
				a.cnsctvo_estdo_drcho_pos
     From		#tmpDatosPrestacionesxSolicitud c
     Inner Join #tmpDatosAfiliado a
     On			(c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)

	--Se carga la información necesaria de las tablas de Atención OPS y Conceptos, para solo ser consultada una vez.
	
	Insert Into #tmpAtencionConceptosOPS
	Select
				c.cnsctvo_slctd_srvco_sld_rcbda,
				c.cnsctvo_srvco_slctdo_rcbdo,
				a.nmro_unco_idntfccn,
				co.cntdd_prstcn_rl AS cantidad_entregada,
				op.fcha_slctd_orgn
	From		#tmpDatosPrestacionesxSolicitud c
    Inner Join	#tmpDatosAfiliado a
    On			(c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
    Inner Join	bdSisalud.dbo.tbAtencionOPS op WITH (NOLOCK)
    On			(op.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn)
    Inner Join	bdSisalud.dbo.tbProcedimientos co WITH (NOLOCK)
    On			(op.cnsctvo_cdgo_ofcna = co.cnsctvo_cdgo_ofcna
    And			op.nuam = co.nuam
    And			co.cnsctvo_prstcn = c.cnsctvo_cdfccn
    And			op.cnsctvo_cdgo_estdo NOT IN (	@estadoAnulada, --Anulada
												@estadoNoAutorizada, --No autorizada
												@estadoNoTramitada, --No tramitada
												@estadoVencidaCobro, --Vencida por no cobro
												@estadoVencidaPorNoTramite)) --Vencida por no trámite



	-- Se verifica la solicitud, si no esta en sipres, se carga la informacion de mega.
	Insert Into #tmpAtencionConceptosOPS
	Select		c.cnsctvo_slctd_srvco_sld_rcbda,
				c.cnsctvo_srvco_slctdo_rcbdo,
				a.nmro_unco_idntfccn,
				ss.cntdd_atrzda As cantidad_entregada,
				sas.fcha_slctd As fcha_slctd_orgn
	From		#tmpDatosPrestacionesxSolicitud c
	Inner Join	#tmpDatosAfiliado a
	On			c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas With(NoLock)
	On			iasas.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
	Inner Join	bdCNA.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
	On			sas.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
	And			sas.cnsctvo_slctd_atrzcn_srvco != a.cnsctvo_slctd_srvco_sld_rcbda
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
	On			ss.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco
	ANd			ss.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdfccn
	Left Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams WITH (NOLOCK)
	On			dams.cnsctvo_slctd_mga = ss.cnsctvo_slctd_atrzcn_srvco
	And			dams.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo
	Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoAnuladoMega
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoNoAutorizadoMega
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoDevueltoMega
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @estadoVencidaPorNoTramiteMega
	And			dams.cnsctvo_prstcn Is Null;

	-- Si la cantidad autorizada es 0 se obtiene la cantidad solicitada.
	Update		#tmpAtencionConceptosOPS
	Set			cntdd_prstcn = ss.cntdd_slctda
	From		#tmpAtencionConceptosOPS ao
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = ao.cnsctvo_slctd_srvco_sld_rcbda
	ANd			ss.cnsctvo_srvco_slctdo = ao.cnsctvo_srvco_slctdo_rcbdo	
	Where		ao.cntdd_prstcn = @vlr_cro

	
	--Actualización de información del estado de derecho.
	UPDATE		EDA
	SET			estdo_drcho = b.cnsctvo_cdgo_estdo_drcho,
				estdo_drcho_actvo = b.actvo
	From		#tmpDatosAfiliado	EDA
	Inner Join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv 
	On			EDA.nmro_unco_idntfccn = bv.nmro_unco_idntfccn_afldo
	And			EDA.nmro_cntrto	= bv.nmro_cntrto
	And			EDA.cnsctvo_bnfcro	= bv.cnsctvo_bnfcro
	Inner Join	BDAfiliacionValidador.dbo.tbMatrizDerechosValidador b 
	On			bv.cnsctvo_cdgo_tpo_cntrto=b.cnsctvo_cdgo_tpo_cntrto 
	And			bv.nmro_cntrto=b.nmro_cntrto 
	And			bv.nmro_unco_idntfccn_afldo=b.nmro_unco_idntfccn_bnfcro
    Where		EDA.fcha_slctd BETWEEN b.inco_vgnca_estdo_drcho And b.fn_vgnca_estdo_drcho
	And			EDA.fcha_slctd between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro

  END
