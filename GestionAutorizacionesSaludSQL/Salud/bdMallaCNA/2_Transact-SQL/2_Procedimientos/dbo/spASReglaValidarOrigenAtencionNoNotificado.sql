USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarOrigenAtencionNoNotificado]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarOrigenAtencionNoNotificado
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si el origen de atencion de la D\>
					 <\D Solicitud es AL o EL y el afiliado NO  tiene una notificacion en estado D\>
					 <\D "Notificado" o "Confirmado" para el mismo diagnostico principal que llega  D\>
					 <\D en la solicitud D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 19/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarOrigenAtencionNoNotificado] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

DECLARE @consecutivoAt numeric(1),
          @consecutivoEP numeric(1),
          @consecutivoNotificadoAt numeric(1),
          @consecutivoConfirmadoAt numeric(1),
		  @consecutivoNotificadoEp numeric(2),
          @consecutivoConfirmadoEp numeric(2),
		  @consecutivoOrigenAtencionAT numeric(1) = 1,
		  @consecutivoOrigenAtencionEP numeric(1) = 5,
		  @consecutivoOrigenAtencionATYAT numeric(1) = 6,
		  @consecutivoOrigenAtencionATYEC numeric(1) = 7

  SET @consecutivoAt = 4
  SET @consecutivoEP = 5
  SET @consecutivoNotificadoAt = 6
  SET @consecutivoConfirmadoAt = 7
  SET @consecutivoNotificadoEp = 10
  SET @consecutivoConfirmadoEp = 11

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación de probable origen laboral se canalizará al área de medicina del trabajo para su respectiva verificación.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      LEFT JOIN bdSisalud.DBO.tbafiliadosMarcados m WITH (NOLOCK)
        ON (a.nmro_unco_idntfccn = m.nmro_unco_idntfccn
        AND c.cnsctvo_cdgo_dgnstco = m.cnsctvo_cdgo_dgnstco
		AND m.cnsctvo_cdgo_clsfccn_evnto = @consecutivoEP --"ENFERMEDAD PROFESIONAL"
		AND m.cnsctvo_cdgo_estdo_ntfccn IN (@consecutivoNotificadoEp, @consecutivoConfirmadoEp) )-- Notificado y confirmado 
      WHERE c.orgn_atncn IN (@consecutivoOrigenAtencionAT,@consecutivoOrigenAtencionEP,@consecutivoOrigenAtencionATYAT, @consecutivoOrigenAtencionATYEC )
	  AND  m.nmro_unco_idntfccn IS NULL
	  UNION ALL
	   SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'Prestación de probable origen laboral se canalizará al área de medicina del trabajo para su respectiva verificación.'
      FROM #tmpDatosPrestacionesxSolicitud c
      INNER JOIN #tmpDatosAfiliado a
        ON (c.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_srvco_sld_rcbda)
      LEFT JOIN bdSisalud.DBO.tbafiliadosMarcados m WITH (NOLOCK)
        ON (a.nmro_unco_idntfccn = m.nmro_unco_idntfccn
        AND c.cnsctvo_cdgo_dgnstco = m.cnsctvo_cdgo_dgnstco
		AND m.cnsctvo_cdgo_clsfccn_evnto = @consecutivoAt --"ENFERMEDAD PROFESIONAL"
		AND m.cnsctvo_cdgo_estdo_ntfccn IN (@consecutivoNotificadoAt, @consecutivoConfirmadoAt) )-- Notificado y confirmado 
      WHERE c.orgn_atncn IN (@consecutivoOrigenAtencionAT,@consecutivoOrigenAtencionEP,@consecutivoOrigenAtencionATYAT, @consecutivoOrigenAtencionATYEC )
	  AND  m.nmro_unco_idntfccn IS NULL
    ;
  END

GO
