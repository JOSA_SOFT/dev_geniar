USE [bdMallaCNA]
GO
/****** Object:  StoredProcedure [dbo].[spASReglaValidarPrestacionxDescripcion]    Script Date: 19/09/2016 1:46:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASReglaValidarPrestacionxDescripcion
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga de validar si se debe de realizar la busqueda de D\>
					 <\D la prestación por descripción. D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 15/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[spASReglaValidarPrestacionxDescripcion] @prmtro_sp_vldcn varchar(30),
@cnsctvo_cdgo_rgla_vldcn udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
AS
  SET NOCOUNT ON 

  DECLARE @valorCero char(1),
		  @valorVacio char(1)

  SET @valorCero = '0'
  SET @valorVacio = ''

  BEGIN

    INSERT INTO #tbResultadosValidaciones (cnsctvo_cdgo_rgla_vldcn,
    cnsctvo_cdgo_no_cnfrmdd_vldcn,
    llve_prmra_rgstro_vlddo,
    llve_prmra_cncpto_prncpl_vlddo,
    infrmcn_adcnl_no_cnfrmdd)
      SELECT
        @cnsctvo_cdgo_rgla_vldcn,
        @cnsctvo_cdgo_no_cnfrmdd_vldcn,
        c.cnsctvo_slctd_srvco_sld_rcbda,
        c.cnsctvo_srvco_slctdo_rcbdo,
        'No se ingreso tipo y codgo de prestacion, se debe realizar la busqueda por descripcion.'
      FROM #tmpDatosPrestacionesxSolicitud c
      WHERE c.cdgo_cdfccn =  @valorVacio
      AND c.cnsctvo_cdfccn = @valorCero
      AND c.dscrpcn_cdfccn IS NOT NULL;

  END

GO
