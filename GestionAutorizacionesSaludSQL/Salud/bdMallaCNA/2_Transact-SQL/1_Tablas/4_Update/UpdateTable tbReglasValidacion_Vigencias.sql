Use [bdMallaCNA]

Go

Declare @lnEstdoVacio udtConsecutivo, --Vacio
        @lnEstdoAD    udtConsecutivo, --Acceso Directo
		@lnEstdoADA   udtConsecutivo, --Acceso Directo ARL
		@lnEstdoInc   udtConsecutivo, --Presenta Inconsistencia
		@lnEstdoPac   udtConsecutivo  --Si el paciente cuenta con POS en SOS se entrega la prestacion por el POS

Set  @lnEstdoVacio = 0
Set  @lnEstdoAD    = 1
Set  @lnEstdoADA   = 2
Set  @lnEstdoInc   = 3
Set  @lnEstdoPac   = 4


Update rv
Set    rv.cnsctvo_cdgo_estdo_srvco_mstrr_afldo = @lnEstdoVacio
From bdMallaCNA.dbo.tbReglasValidacion_Vigencias  rv
Where  rv.cnsctvo_cdgo_rgla_vldcn In (0,16,17,19,20,23,24,26,28,31,34,37,40,42,45,47,50,52,53,54,55,56,57,66,67,69,71,80,81,99)


Update  rv
Set    rv.cnsctvo_cdgo_estdo_srvco_mstrr_afldo = @lnEstdoAD
From bdMallaCNA.dbo.tbReglasValidacion_Vigencias rv
Where rv.cnsctvo_cdgo_rgla_vldcn In (48, 62, 65, 68)


Update  rv
Set    cnsctvo_cdgo_estdo_srvco_mstrr_afldo = @lnEstdoADA
From bdMallaCNA.dbo.tbReglasValidacion_Vigencias  rv
Where cnsctvo_cdgo_rgla_vldcn = 58


Update rv
Set    cnsctvo_cdgo_estdo_srvco_mstrr_afldo = @lnEstdoInc
From bdMallaCNA.dbo.tbReglasValidacion_Vigencias  rv
Where  cnsctvo_cdgo_rgla_vldcn In (6,7,8,9,10,11,12,13,14,15,18,21,22,25,27,29,30,32,33,35,36,38,39,41,43,44,46,49,51,59,60,61,63,70)


Update  rv
Set    cnsctvo_cdgo_estdo_srvco_mstrr_afldo = @lnEstdoPac
From bdMallaCNA.dbo.tbReglasValidacion_Vigencias rv
Where cnsctvo_cdgo_rgla_vldcn = 64

GO
