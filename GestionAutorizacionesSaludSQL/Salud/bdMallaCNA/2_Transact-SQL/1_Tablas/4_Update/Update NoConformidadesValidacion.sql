use[bdMallaCNA]
GO

update bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias
set mnsje_no_cnfrmdd_ips = null

UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El Afiliado no se encuentra parametrizado' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 6
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado no cuenta con contrato PAC vigente' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 7
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado  no cuenta  con contrato POS  vigente' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 8
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado con notificacion de mortalidad' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 9
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado  RETIRADO de la EPS Servicio Occidental de Salud' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 10
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Usuario en proceso, sin derecho a servicios' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 11
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Usuario en estado suspendido PAC por suspensi�n en pos sin derecho a servicios' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 12
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado  SUSPENDIDO en la EPS Servicio Occidental de Salud' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 13
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Afiliado  SUSPENDIDO por mora' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 14
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'Usuario en   PROTECCI�N LABORAL. La solicitud est� siendo gestionada por auditor�a. Recibir� una nueva notificaci�n pr�ximamente' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 15
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio de salud  no se encuentra vigente, debe ser ajustado, para autorizaci�n' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 18
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La orden m�dica donde se solicit� el servicio se encuentra vencida' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 25
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La cantidad solicitada supera la cantidad m�xima permitida, en el a�o o en la vida' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 27
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La cantidad solicitada supera la cantidad m�xima permitida, en el a�o o en la vida' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 29
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La cantidad solicitada supera la cantidad m�xima permitida, en el a�o o en la vida' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 30
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La cantidad solicitada supera la cantidad m�xima permitida, en el a�o o en la vida' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 32
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio  solicitado supera la frecuencia de entrega con respecto a la �ltima vez  que fue entregada' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 33
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El diagnostico ingresado no corresponde al genero del afiliado. Se sugiere ajuste' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn =  35
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El diagnostico ingresado no corresponde al genero del afiliado. Se sugiere ajuste' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn =  36
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El diagnostico ingresado no corresponde al genero del afiliado. Se sugiere ajuste' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn =  38
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El diagnostico ingresado no corresponde al genero del afiliado. Se sugiere ajuste' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn =  39
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La edad del afiliado supera la edad m�xima permitida para la prestaci�n del servicio' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 41
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La edad del afiliado supera la edad m�xima permitida para la prestaci�n del servicio' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 43
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La edad del afiliado supera la edad m�xima permitida para la prestaci�n del servicio' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 44
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'La edad del afiliado supera la edad m�xima permitida para la prestaci�n del servicio' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 46
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado no requiere autorizaci�n, est� incluido en los contratos convenidos con la IPS' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 48
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado es de origen laboral, se debe gestionar con la ARL respectiva' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 58
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El medicamento solicitado tiene el registro invima vencido' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 59
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El medicamento solicitado es una muestra m�dica, seg�n el registro invima indicado.   Se sugiere validar y reformular' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 60
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El medicamento solicitado debe ser gestionado como un No POS, porque no cumple con los requisitos para ser un POS' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 61
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado no requiere autorizaci�n, la prestaci�n es de acceso directo y puede brindar la atenci�n sin restricciones' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 62
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado  debe ser gestionado como un No POS, porque no cumple con los requisitos para ser un POS' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 63
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado  debe ser gestionado como un No POS, porque no cumple con los requisitos para ser un POS' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 65
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado no requiere autorizaci�n, est� incluido en los contratos convenidos con la IPS' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 68
UPDATE bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias set mnsje_no_cnfrmdd_ips = 'El servicio solicitado  no cuenta con los documentos soportes necesarios' WHERE cnsctvo_vgnca_no_cnfrmdd_vldcn = 70

GO