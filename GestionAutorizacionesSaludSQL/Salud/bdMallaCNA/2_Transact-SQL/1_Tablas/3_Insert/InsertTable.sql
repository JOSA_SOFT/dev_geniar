Use [bdMallaCNA]

Go

Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime

Set     @fcha_actl  = GETDATE()
Set     @usro       = 'sismigra01'
Set     @obsrvcns   = '.'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'


INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado(cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, fch_crcn, usro_crcn, vsble_usro) VALUES(0,'00','Vacio',@fcha_actl, @usro, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado(cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, fch_crcn, usro_crcn, vsble_usro) VALUES(1,'01','Acceso Directo',@fcha_actl, @usro, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado(cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, fch_crcn, usro_crcn, vsble_usro) VALUES(2,'02','Acceso Directo ARL',@fcha_actl, @usro, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado(cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, fch_crcn, usro_crcn, vsble_usro) VALUES(3,'03','Presenta Inconsistencia',@fcha_actl, @usro, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado(cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, fch_crcn, usro_crcn, vsble_usro) VALUES(4,'04','Si El Paciente Cuenta Con POS En SOS Se Entrega La Prestacion Por El POS',@fcha_actl, @usro, @vsble_usro)

INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias(cnsctvo_vgnca_estdo_srvco_mstrr_afldo, cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, inco_vgnca, fn_vgnca, fch_crcn, usro_crcn, obsrvcns, vsble_usro) VALUES (0, 0,'00','Vacio', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias(cnsctvo_vgnca_estdo_srvco_mstrr_afldo, cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, inco_vgnca, fn_vgnca, fch_crcn, usro_crcn, obsrvcns, vsble_usro) VALUES (1, 1,'01','Acceso Directo', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias(cnsctvo_vgnca_estdo_srvco_mstrr_afldo, cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, inco_vgnca, fn_vgnca, fch_crcn, usro_crcn, obsrvcns, vsble_usro) VALUES (2, 2,'02','Acceso Directo ARL', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias(cnsctvo_vgnca_estdo_srvco_mstrr_afldo, cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, inco_vgnca, fn_vgnca, fch_crcn, usro_crcn, obsrvcns, vsble_usro) VALUES (3, 3,'03','Presenta Inconsistencia', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)
INSERT INTO bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias(cnsctvo_vgnca_estdo_srvco_mstrr_afldo, cnsctvo_cdgo_estdo_srvco_mstrr_afldo, cdgo_estdo_srvco_mstrr_afldo, dscrpcn_estdo_srvco_mstrr_afldo, inco_vgnca, fn_vgnca, fch_crcn, usro_crcn, obsrvcns, vsble_usro) VALUES (4, 4,'04','Si El Paciente Cuenta Con POS En SOS Se Entrega La Prestacion Por El POS', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)

Go