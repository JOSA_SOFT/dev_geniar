USE [bdMallaCNA]
GO
Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime

Set     @fcha_actl  = GETDATE()
Set     @usro       = 'sismigra01'
Set     @obsrvcns   = '.'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'

INSERT [dbo].[tbNoConformidadesValidacion] ([cnsctvo_cdgo_no_cnfrmdd_vldcn], [cdgo_no_cnfrmdd_vldcn], [dscrpcn_no_cnfrmdd_vldcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (80, N'80', N'prestacion para validacion de domi.', @fcha_actl, @usro, @vsble_usro)
INSERT [dbo].[tbNoConformidadesValidacion] ([cnsctvo_cdgo_no_cnfrmdd_vldcn], [cdgo_no_cnfrmdd_vldcn], [dscrpcn_no_cnfrmdd_vldcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (81, N'81', N'Prestacion para validacion de auditoria.', @fcha_actl, @usro,@vsble_usro)



INSERT [dbo].[tbNoConformidadesValidacion_Vigencias] ([cnsctvo_vgnca_no_cnfrmdd_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn], [cdgo_no_cnfrmdd_vldcn], [dscrpcn_no_cnfrmdd_vldcn], [mnsje_no_cnfrmdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_estdo_no_cnfrmdd], [cnsctvo_cdgo_tpo_excpcn_no_cnfrmdd_vldcn], [cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn_excpcn]) VALUES (80, 80, N'80', N'prestacion para validacion de domi.', N'prestacion para validacion de domi.', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns,@vsble_usro, 2, 0, 2, NULL)
INSERT [dbo].[tbNoConformidadesValidacion_Vigencias] ([cnsctvo_vgnca_no_cnfrmdd_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn], [cdgo_no_cnfrmdd_vldcn], [dscrpcn_no_cnfrmdd_vldcn], [mnsje_no_cnfrmdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_estdo_no_cnfrmdd], [cnsctvo_cdgo_tpo_excpcn_no_cnfrmdd_vldcn], [cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn_excpcn]) VALUES (81, 81, N'81', N'Prestacion para validacion de auditoria.', N'Prestacion para validacion de auditoria.', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns,@vsble_usro, 2, 0, 2, NULL)


INSERT [dbo].[tbReglasValidacion] ([cnsctvo_cdgo_rgla_vldcn], [cdgo_rgla_vldcn], [dscrpcn_rgla_vldcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (80, N'80', N'prestacion para validacion de domi.', @fcha_actl, @usro, @vsble_usro)
INSERT [dbo].[tbReglasValidacion] ([cnsctvo_cdgo_rgla_vldcn], [cdgo_rgla_vldcn], [dscrpcn_rgla_vldcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (81, N'81', N'Prestacion para validacion de auditoria.',  @fcha_actl, @usro, @vsble_usro)



INSERT [dbo].[tbReglasValidacion_Vigencias] ([cnsctvo_vgnca_rgla_vldcn], [cnsctvo_cdgo_rgla_vldcn], [cdgo_rgla_vldcn], [dscrpcn_rgla_vldcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_fltro_vldcn], [cnsctvo_cdgo_grpo_rgla_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn], [jrrqa_rgla_vldcn], [cnsctvo_cdgo_cncpto_vlddo], [nmbre_sp_vldcn], [prmtro_sp_vldcn]) VALUES (80, 80, N'80', N'prestacion para validacion de domi.', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro, 0, 5, 80, 23, 0, N'spASReglaValidarMarcaDomi', NULL)
INSERT [dbo].[tbReglasValidacion_Vigencias] ([cnsctvo_vgnca_rgla_vldcn], [cnsctvo_cdgo_rgla_vldcn], [cdgo_rgla_vldcn], [dscrpcn_rgla_vldcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_fltro_vldcn], [cnsctvo_cdgo_grpo_rgla_vldcn], [cnsctvo_cdgo_no_cnfrmdd_vldcn], [jrrqa_rgla_vldcn], [cnsctvo_cdgo_cncpto_vlddo], [nmbre_sp_vldcn], [prmtro_sp_vldcn]) VALUES (81, 81, N'81', N'Prestacion para validacion de auditoria.',@fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro, 0, 5, 81, 23, 0, N'spASReglaValidarMarcaAuditoria', NULL)



INSERT [dbo].[tbMarcasProcesosxNoConformidadesValidacion] ([cnsctvo_cdgo_mrca_prcso_x_no_cnfrmdd_vldcn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_mrca_prcso], [cnsctvo_cdgo_no_cnfrmdd_vldcn]) VALUES (34, @fcha_actl, @usro, 6, 80)
INSERT [dbo].[tbMarcasProcesosxNoConformidadesValidacion] ([cnsctvo_cdgo_mrca_prcso_x_no_cnfrmdd_vldcn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_mrca_prcso], [cnsctvo_cdgo_no_cnfrmdd_vldcn]) VALUES (35, @fcha_actl, @usro, 9, 81)


INSERT [dbo].[tbMarcasProcesosxNoConformidadesValidacion_Vigencias] ([cnsctvo_vgnca_mrca_prcso_x_no_cnfrmdd_vldcn], [cnsctvo_cdgo_mrca_prcso_x_no_cnfrmdd_vldcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_mrca_prcso], [cnsctvo_cdgo_no_cnfrmdd_vldcn]) VALUES (34, 34, @fcha_actl, @fn_vgncia, @fcha_actl, @usro, 6, 80)
INSERT [dbo].[tbMarcasProcesosxNoConformidadesValidacion_Vigencias] ([cnsctvo_vgnca_mrca_prcso_x_no_cnfrmdd_vldcn], [cnsctvo_cdgo_mrca_prcso_x_no_cnfrmdd_vldcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_mrca_prcso], [cnsctvo_cdgo_no_cnfrmdd_vldcn]) VALUES (35, 35, @fcha_actl, @fn_vgncia, @fcha_actl, @usro, 9, 81)
GO
