USE [bdMallaCNA]
GO

CREATE TABLE [dbo].[tbASEstadosServicioMostrarAfiliado](
	[cnsctvo_cdgo_estdo_srvco_mstrr_afldo] [dbo].[udtConsecutivo] NOT NULL,
	[cdgo_estdo_srvco_mstrr_afldo] [dbo].[udtCodigo] NOT NULL,
	[dscrpcn_estdo_srvco_mstrr_afldo] [dbo].[udtDescripcion] NOT NULL,
	[fch_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[vsble_usro] [dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbASEstadosServicioMostrarAfiliado] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_cdgo_estdo_srvco_mstrr_afldo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_INDEXES]
) ON [FG_DATA]

GO

CREATE TABLE [dbo].[tbASEstadosServicioMostrarAfiliado_Vigencias](
	[cnsctvo_vgnca_estdo_srvco_mstrr_afldo] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_estdo_srvco_mstrr_afldo] [dbo].[udtConsecutivo] NOT NULL,
	[cdgo_estdo_srvco_mstrr_afldo] [dbo].[udtCodigo] NOT NULL,
	[dscrpcn_estdo_srvco_mstrr_afldo] [dbo].[udtDescripcion] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fch_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[obsrvcns] [dbo].[udtObservacion] NOT NULL,
	[vsble_usro] [dbo].[udtLogico] NOT NULL,
	[tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbASEstadosServicioMostrarAfiliado_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_estdo_srvco_mstrr_afldo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_INDEXES]
) ON [FG_DATA]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbASEstadosServicioMostrarAfiliado_Vigencias]  WITH NOCHECK ADD  CONSTRAINT [FK_tbASEstadosServicioMostrarAfiliado_Vigencias_tbASEstadosServicioMostrarAfiliado] FOREIGN KEY([cnsctvo_cdgo_estdo_srvco_mstrr_afldo])
REFERENCES [dbo].[tbASEstadosServicioMostrarAfiliado] ([cnsctvo_cdgo_estdo_srvco_mstrr_afldo])
GO

ALTER TABLE [dbo].[tbASEstadosServicioMostrarAfiliado_Vigencias] NOCHECK CONSTRAINT [FK_tbASEstadosServicioMostrarAfiliado_Vigencias_tbASEstadosServicioMostrarAfiliado]
GO
