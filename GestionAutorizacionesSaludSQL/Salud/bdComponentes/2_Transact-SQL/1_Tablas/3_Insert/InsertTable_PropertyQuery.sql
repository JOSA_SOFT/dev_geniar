USE [bdComponentes]
GO
declare @fcha_crcn Datetime

Set  @fcha_crcn = GETDATE()

INSERT [propiedades].[tbAplicaciones] ([cnsctvo_cdgo_aplccn], [cdgo_aplccn], [dscrpcn_aplccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (29, N'29        ', N'AuthenticationService', @fcha_crcn, N'sisczg01                      ', N'S')
INSERT [propiedades].[tbAplicaciones_Vigencias] ([cnsctvo_vgnca_aplccn], [cnsctvo_cdgo_aplccn], [cdgo_aplccn], [dscrpcn_aplccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (29, 29, N'29        ', N'AuthenticationService', @fcha_crcn, CAST(N'9999-12-31 00:00:00.000' AS DateTime), @fcha_crcn, N'sisczg01                      ', N'Observacin', N'S')

INSERT [propiedades].[tbClasificaciones] ([cnsctvo_cdgo_clsfccn], [cdgo_clsfccn], [dscrpcn_clsfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (53, N'53        ', N'Rutas', @fcha_crcn, N'sisczg01                      ', N'S')
INSERT [propiedades].[tbClasificaciones_Vigencias] ([cnsctvo_vgnca_clsfccn], [cnsctvo_cdgo_clsfccn], [cdgo_clsfccn], [dscrpcn_clsfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_aplccn], [jndi], [nmbre_tbla]) VALUES (52, 53, N'53        ', N'Rutas', @fcha_crcn, CAST(N'9999-12-31 00:00:00.000' AS DateTime), @fcha_crcn, N'sisczg01                      ', N'Observaci', N'S', 29, N'jdbc/propertyQuery', N'bdComponentes.propiedades.tbPropiedadesAplicacion_vigencias')

INSERT [propiedades].[tbPropiedadesAplicacion] ([cnsctvo_cdgo_prpdd_aplccn], [cdgo_prpdd_aplccn], [dscrpcn_prpdd_aplccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (141, N'141       ', N'RutaServicioAutenticacion', @fcha_crcn, N'sisczg01                      ', N'S')
INSERT [propiedades].[tbPropiedadesAplicacion_Vigencias] ([cnsctvo_vgnca_prpdd_aplccn], [cnsctvo_cdgo_prpdd_aplccn], [cdgo_prpdd_aplccn], [clve], [dscrpcn_prpdd_aplccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [cnsctvo_cdgo_clsfccn], [vlr]) VALUES (145, 141, N'141       ', N'RutaServicioAutenticacion', N'RutaServicioAutenticacion', @fcha_crcn, CAST(N'9999-12-31 00:00:00.000' AS DateTime), @fcha_crcn, N'sisczg01                      ', N'Observaciones', N'S', 53, N'http://empresarialeps.sos.com.co/AuthenticationService/services/SOSAuthenticationService/SOSAuthenticationService.wsdl')

go

