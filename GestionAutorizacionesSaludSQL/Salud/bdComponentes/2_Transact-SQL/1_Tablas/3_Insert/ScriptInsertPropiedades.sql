/****** Script for SelectTopNRows command from SSMS  ******/
																		-- Pruebas			Producción
USE bdComponentes
GO

Declare @cnsctvo_cdgo_aplccn        udtConsecutivo,
        @cnsctvo_vgnca_aplccn       udtConsecutivo,
		@cnsctvo_cdgo_clsfccn       udtConsecutivo, 
		@cnsctvo_vgnca_clsfccn      udtConsecutivo,  
		@cnsctvo_cdgo_prpdd_aplccn  udtConsecutivo,
		@cnsctvo_vgnca_prpdd_aplccn udtConsecutivo,
		@fcha_crcn                  Datetime,
		@fn_vgnca                   Datetime,
		@usro_crcn                  udtUsuario,
		@vsble_usro                 udtLogico,
		@obsrvcns                   udtObservacion
		

Set @fcha_crcn = getDate()
Set @usro_crcn = 'geniarjag'
Set @fn_vgnca  = '99991231'
Set @vsble_usro = 'S'
Set @obsrvcns   = 'CAJAS - GestionAutorizacionesSalud'


Select @cnsctvo_cdgo_aplccn = cnsctvo_cdgo_aplccn
From   bdComponentes.propiedades.tbAplicaciones 
Where  dscrpcn_aplccn = 'GestionAutorizacionesSalud'
  

Select @cnsctvo_cdgo_clsfccn = MAX(cnsctvo_cdgo_clsfccn) + 1 From propiedades.tbClasificaciones
Select @cnsctvo_vgnca_clsfccn = MAX(cnsctvo_vgnca_clsfccn) + 1 From propiedades.tbClasificaciones_Vigencias

--SELECT * FROM bdComponentes.propiedades.tbClasificaciones
INSERT 
INTO   propiedades.tbClasificaciones(cnsctvo_cdgo_clsfccn, cdgo_clsfccn, dscrpcn_clsfccn, 
                                     fcha_crcn           , usro_crcn   , vsble_usro
									)
VALUES (@cnsctvo_cdgo_clsfccn, Cast(@cnsctvo_cdgo_clsfccn As Char(10)), 'URL_REPORTE_CAJA', @fcha_crcn, @usro_crcn, @vsble_usro)-- desarrollo
           

--SELECT * FROM bdComponentes.propiedades.tbClasificaciones_Vigencias
INSERT 
INTO   propiedades.tbClasificaciones_Vigencias(cnsctvo_vgnca_clsfccn, cnsctvo_cdgo_clsfccn, cdgo_clsfccn,
                                               dscrpcn_clsfccn      , inco_vgnca          , fn_vgnca    , 
											   fcha_crcn            , usro_crcn           , obsrvcns    , 
											   vsble_usro           , cnsctvo_cdgo_aplccn , jndi        ,
											   nmbre_tbla
											  )
VALUES(@cnsctvo_vgnca_clsfccn, @cnsctvo_cdgo_clsfccn, Cast(@cnsctvo_cdgo_clsfccn As Char(10)),'URL_REPORTE_CAJA', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_aplccn, 'jdbc/propertyQuery', 'bdComponentes.propiedades.tbPropiedadesAplicacion_vigencias')
	  


----adicionar las propiades correspondiente----

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.CIERRE.CAJAS

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.CIERRE.CAJAS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.CIERRE.CAJAS', 'RUTA.JASPER.REPORTE.CIERRE.CAJAS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/reporteCierreCaja.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.IMAGEN.LOGO.SOS
Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.IMAGEN.LOGO.SOS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.IMAGEN.LOGO.SOS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.IMAGEN.LOGO.SOS', 'RUTA.IMAGEN.LOGO.SOS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/imagenes/SOSLogo.png')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.INFORME.CIERRE.OFICINAS
Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.INFORME.CIERRE.OFICINAS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.INFORME.CIERRE.OFICINAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.INFORME.CIERRE.OFICINAS', 'RUTA.JASPER.INFORME.CIERRE.OFICINAS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/informeCierreOficina.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.INFORME.CIERRE.CAJAS

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.INFORME.CIERRE.CAJAS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.INFORME.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.INFORME.CIERRE.CAJAS', 'RUTA.JASPER.INFORME.CIERRE.CAJAS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/informeCierreCaja.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS', 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/detalleCierreCaja.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.RECIBOS

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.RECIBOS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.RECIBOS', 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.RECIBOS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/detalleCierreCaja_recibosCaja.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.NOTAS

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.NOTAS', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.NOTAS', 'RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS.NOTAS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/detalleCierreCaja_notasCredito.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.NOTA.CREDITO

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.NOTA.CREDITO', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.NOTA.CREDITO', 'RUTA.JASPER.REPORTE.NOTA.CREDITO', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/reporteNotaCredito.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.RECIBO.CAJA

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.RECIBO.CAJA', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.RECIBO.CAJA', 'RUTA.JASPER.REPORTE.RECIBO.CAJA', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/reciboCaja.jasper')

--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion --RUTA.JASPER.REPORTE.CIERRE.CAJA.DETALLADO

Select @cnsctvo_cdgo_prpdd_aplccn = MAX(cnsctvo_cdgo_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion
Select @cnsctvo_vgnca_prpdd_aplccn = MAX(cnsctvo_vgnca_prpdd_aplccn) + 1 From propiedades.tbPropiedadesAplicacion_Vigencias

INSERT 
INTO   propiedades.tbPropiedadesAplicacion(cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn, dscrpcn_prpdd_aplccn,
                                           fcha_crcn                , usro_crcn        , vsble_usro
										  )
VALUES (@cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)),'RUTA.JASPER.REPORTE.CIERRE.CAJA.DETALLADO', @fcha_crcn, @usro_crcn, @vsble_usro)


--SELECT * FROM bdComponentes.propiedades.tbPropiedadesAplicacion_Vigencias --RUTA.JASPER.REPORTE.DETALLE.CIERRE.CAJAS
INSERT 
INTO   propiedades.tbPropiedadesAplicacion_Vigencias(cnsctvo_vgnca_prpdd_aplccn, cnsctvo_cdgo_prpdd_aplccn, cdgo_prpdd_aplccn   ,
                                                     clve                      , dscrpcn_prpdd_aplccn     , inco_vgnca          ,
                                                     fn_vgnca                  , fcha_crcn                , usro_crcn           ,
                                                     obsrvcns                  , vsble_usro               , cnsctvo_cdgo_clsfccn,
                                                     vlr
													)
VALUES(@cnsctvo_vgnca_prpdd_aplccn, @cnsctvo_cdgo_prpdd_aplccn, Cast(@cnsctvo_cdgo_prpdd_aplccn As Char(10)), 'RUTA.JASPER.REPORTE.CIERRE.CAJA.DETALLADO', 'RUTA.JASPER.REPORTE.CIERRE.CAJA.DETALLADO', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 'Ruta Recursos Externos de Aplicaciones', @vsble_usro, @cnsctvo_cdgo_clsfccn, '/plantillas/reporteDetalladoCajas.jasper')









