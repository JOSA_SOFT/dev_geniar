USE bdBodegasPoliticas 
GO
 GRANT EXECUTE on bdp.spBDPConsultaDatosBasicosPrestacion	to [autsalud_rol]	
 GRANT EXECUTE on bdp.spBDPConsultaPrestacionPorPlanes		to [autsalud_rol]
 GRANT EXECUTE on bdp.spBDPConsultaTiemposEntrega			to [autsalud_rol]	
 GRANT EXECUTE on bdp.spBDPMarcasPrestacion					to [autsalud_rol]
 GRANT EXECUTE on bdp.spBDPPoliticaAuthPrestacion			to [autsalud_rol]	
  
  GRANT EXECUTE on bdp.spBDPConveniosPrestacion			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPDireccionamientoExcepciones			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPDireccionamientoNormal			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPDireccionamientoXRiesgo			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPHomologo			to [autsalud_rol]
  GRANT EXECUTE on bdp.spBDPMarcasPrestacion			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPPoliticaAuthPrestacion			to [autsalud_rol]		 
  GRANT EXECUTE on bdp.spBDPCupsXRiesgo			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPCupsXPlanXCargo			to [autsalud_rol]	
  GRANT EXECUTE on bdp.spBDPDatosAdicionalesCums			to [autsalud_rol]	

 