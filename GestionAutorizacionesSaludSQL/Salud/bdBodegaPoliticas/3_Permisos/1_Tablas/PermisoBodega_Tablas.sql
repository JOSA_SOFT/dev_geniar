USE bdBodegasPoliticas 
GO
 GRANT SELECT on  [prm].[tbBPPoliticasAutorizacionPrestaciones]					to [autsalud_rol]
 GRANT SELECT on  [prm].[tbBPPoliticasAutorizacionPrestaciones_Vigencias]       to [autsalud_rol]
 GRANT SELECT on  [bdp].[tbBPPoliticasAutorizacionPrestacionesxPrestacion]      to [autsalud_rol]
 GRANT SELECT on  [prm].[tbBPTiposPoliticasAutorizacionPrestaciones]            to [autsalud_rol]
 GRANT SELECT on  [prm].[tbBPTiposPoliticasAutorizacionPrestaciones_Vigencias]  to [autsalud_rol]

 