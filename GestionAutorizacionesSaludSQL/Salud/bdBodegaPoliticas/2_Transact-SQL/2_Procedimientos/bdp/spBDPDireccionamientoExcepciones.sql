USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPDireccionamientoExcepciones') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPDireccionamientoExcepciones AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPDireccionamientoExcepciones
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar los direccionamientos excepciones.
* Observaciones : <\O O\>    
* Parametros  :
				
				<\P	@cnsctvo_cdfccn 	P\>


* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPDireccionamientoExcepciones 00087
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPDireccionamientoExcepciones
	@cnsctvo_cdfccn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @fcha_actl datetime;	
	
	SET @fcha_actl = GETDATE()

	
	SELECT DISTINCT  a.cdgo_intrno,
		b.nmbre_scrsl,
		c.dscrpcn_sde,	
		fv.dscrpcn_zna,
		a.edd_mnma,
		a.edd_mxma,
		a.cnsctvo_cdgo_dgnstco,
		ev.dscrpcn_dgnstco,
		a.inco_vgnca,
		a.fn_vgnca,
		cv.dscrpcn_cdd 
	FROM bdCNA.gsa.tbASExcepcionesDiagnosticosDireccionesPrestador a  WITH(NOLOCK)
	INNER JOIN bdSisalud.dbo.tbDireccionesPrestador b  WITH(NOLOCK) ON (a.cdgo_intrno = b.cdgo_intrno)
	INNER JOIN  BDAfiliacionValidador.dbo.tbSedes_vigencias c  WITH(NOLOCK) ON (b.cnsctvo_cdgo_sde = c.cnsctvo_cdgo_sde)
	INNER JOIN bdsisalud.dbo.tbdiagnosticos_vigencias ev  WITH(NOLOCK) ON a.cnsctvo_cdgo_dgnstco = ev.cnsctvo_cdgo_dgnstco
    INNER JOIN bdafiliacionvalidador.dbo.tbciudades_vigencias cv WITH(NOLOCK) ON b.cnsctvo_cdgo_cdd = cv.cnsctvo_cdgo_cdd
	INNER JOIN bdafiliacionvalidador.dbo.tbzonas_vigencias fv  WITH(NOLOCK) ON b.cnsctvo_cdgo_zna = fv.cnsctvo_cdgo_zna
	WHERE a.cnsctvo_cdfccn = @cnsctvo_cdfccn 		
	AND @fcha_actl BETWEEN a.inco_vgnca AND a.fn_vgnca
	AND @fcha_actl BETWEEN b.inco_vgnca AND b.fn_vgnca
	AND @fcha_actl BETWEEN ev.inco_vgnca AND ev.fn_vgnca
	AND @fcha_actl BETWEEN c.inco_vgnca AND c.fn_vgnca
	AND @fcha_actl BETWEEN cv.inco_vgnca AND cv.fn_vgnca
	AND @fcha_actl BETWEEN fv.inco_vgnca AND fv.fn_vgnca
END
GO
