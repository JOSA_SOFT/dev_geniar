USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPCupsXPlanXCargo') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPCupsXPlanXCargo AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPCupsXPlanXCargo
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar los cups por plan por cargo
* Observaciones : <\O O\>    
* Parametros  :
				<\P @id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/04/04 FC\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Jose Olmedo Soto AM\>    
* Descripcion        : <\DM Ajuste de consecutivo en parametro DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------

* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPCupsXPlanXCargo 244103
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPCupsXPlanXCargo
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fecha_actual	datetime,
			@cdgo_cps		udtCodigo
	
	SET @fecha_actual = GETDATE()
	SET @cdgo_cps = 4
	
	Select     dv.dscrpcn_pln, cv.dscrpcn_crgo_ss
	From       bdsisalud.dbo.tbCupsxPlanxCargo_vigencias a With(NoLock)
	Inner Join bdsisalud.dbo.tbcargossos_vigencias cv   With(NoLock) 
	On         cv.cnsctvo_cdgo_crgo_ss = a.cnsctvo_cdgo_crgo
	Inner Join bdsisalud.dbo.tbcodificaciones c With(NoLock) 
	On         c.cnsctvo_cdfccn = a.cnsctvo_cdfccn 
	Inner Join BDAfiliacionValidador.dbo.tbplanes_vigencias dv With(NoLock) 
	On         dv.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Where      c.cnsctvo_cdgo_tpo_cdfccn = @cdgo_cps
	And        c.cnsctvo_cdfccn          = @id_cnsctvo_prstcn
	And        @fecha_actual between a.inco_vgnca AND a.fn_vgnca
	And        @fecha_actual between cv.inco_vgnca AND cv.fn_vgnca 
	And        @fecha_actual between dv.inco_vgnca AND dv.fn_vgnca 
END
GO
