USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPConsultaTiemposEntrega]    Script Date: 04/05/2017 8:25:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spBDPConsultaTiemposEntrega
* Desarrollado por : José Soto 
* Descripcion      : Proceso encargado de consultar los tiempos de entrega
* Observaciones    : <\O O\>    
* Parametros       : <\P id_cnsctvo_prstcn P\>
* Variables        : <\V V\>
* Fecha Creacion   : <\FC 2017/03/16 FC\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion optimizando la busqueda
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/04 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Angela Sandoval AM\>    
* Descripcion        : <\DM  Se realiza modificacion la busqueda no puede ser por prestacion sino por item de presupuesto
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/27 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
--EXEC bdp.spBDPConsultaTiemposEntrega @id_cnsctvo_prstcn = 23070

ALTER PROCEDURE [bdp].[spBDPConsultaTiemposEntrega]
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fecha_actual             Datetime      ,
	        @cnsctvo_cdgo_itm_prspsto Int           ,
			@lnValorCero              Int           ,
			@dscrpcn_itm_prspsto      udtDescripcion,
			@lcVacio                  udtLogico     ,
			@lcS                      udtLogico     

	Create 
	Table  #TablaTempRetornoPrestacionPlanes(cdgo_pln    udtDescripcion,
		                                     itm_prspsto udtDescripcion,
		                                     cdgo_grpo   Char(4)       ,
		                                     dscrpn_grpo udtDescripcion,
		                                     tmpo_entrga Int
	                                        )

	SET @fecha_actual        = getDate()
	Set @lnValorCero         = 0
	Set @dscrpcn_itm_prspsto = 'VACIO'
	Set @lcVacio             = ''
	set @lcS                 = 'S'


	Select @cnsctvo_cdgo_itm_prspsto = cnsctvo_cdgo_itm_prspsto 
	From   bdsisalud.dbo.tbcupsservicios WITH(NOLOCK) 
	Where  cnsctvo_prstcn = @id_cnsctvo_prstcn
	And    @fecha_actual Between inco_vgnca AND fn_vgnca
	
	Insert 
	Into      #TablaTempRetornoPrestacionPlanes(cdgo_pln   , itm_prspsto, cdgo_grpo, 
	                                            dscrpn_grpo, tmpo_entrga
											   )
	Select     p.dscrpcn_pln     , Case When c.dscrpcn_itm_prspsto = @dscrpcn_itm_prspsto Then @lcVacio Else c.dscrpcn_itm_prspsto End, 
	           d.cdgo_grpo_entrga, d.dscrpcn_grpo_entrga, a.vlr_ds_entrga 
	From       bdCNA.prm.tbASFechaEntrega_Vigencias a With(NoLock)
	Inner Join BDAfiliacionValidador.dbo.tbPlanes_vigencias p With(NoLock) 
	On         p.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	left outer Join bdsisalud.dbo.tbitemspresupuesto c  With(NoLock) 
	On         c.cnsctvo_cdgo_itm_prspsto = a.cnsctvo_cdgo_itm_prspsto
	and        c.cnsctvo_cdgo_itm_prspsto = @cnsctvo_cdgo_itm_prspsto 
	Inner Join bdcna.prm.tbASGruposEntrega_Vigencias d With(NoLock) 
	On         d.cnsctvo_cdgo_grpo_entrga = a.cnsctvo_cdgo_grpo_entrga 
	and        d.vsble_usro =@lcS
	WHERE      a.vlr_ds_entrga      > @lnValorCero 
	And        @fecha_actual between a.inco_vgnca AND a.fn_vgnca
	And        @fecha_actual between p.inco_vgnca AND p.fn_vgnca
	And        @fecha_actual between d.inco_vgnca AND d.fn_vgnca
	And        (a.cnsctvo_cdgo_itm_prspsto = @cnsctvo_cdgo_itm_prspsto  Or  
	            a.cnsctvo_cdgo_itm_prspsto = @lnValorCero
			   )
	Order By  p.cnsctvo_cdgo_pln, c.dscrpcn_itm_prspsto, d.dscrpcn_grpo_entrga
	 		 
	Select cdgo_pln   , itm_prspsto, cdgo_grpo, 
	       dscrpn_grpo, tmpo_entrga               
	From   #TablaTempRetornoPrestacionPlanes

	DROP TABLE #TablaTempRetornoPrestacionPlanes
END
