USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPMarcasPrestacion]    Script Date: 12/07/2017 7:51:56 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG : spBDPMarcasPrestacion
* Desarrollado por : José Soto 
* Descripcion      : Proceso encargado de consultar las marcas de prestación
* Observaciones    : <\O O\>    
* Parametros       : <\P id_cnsctvo_tpo_prtcn P\>
				     <\P id_cnsctvo_prstcn P\>
* Variables        : <\V V\>
* Fecha Creacion   : <\FC 2017/03/16 FC\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Angela SAndoval AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion al agrupador poniendolo en mayuscula y agregandole el 
							tipo de agrupador en el mismo campo
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/12 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
-- EXEC bdp.spBDPMarcasPrestacion @id_cnsctvo_prstcn = 23070

ALTER PROCEDURE [bdp].[spBDPMarcasPrestacion]
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN

	Set Nocount On;
	
	Declare @fecha_actual Datetime,
	        @vlr          Varchar(max);

	Create 
	Table  #TablaTempRetornoMarcaPrestacion(dsc_tpo_mrca udtDescripcion,
		                                    vlr		     Varchar(max)  , 
		                                    inco_vgnca   Datetime      ,
		                                    fnVgnca		 Datetime
	                                       )

	Set @fecha_actual = getDate()
	Set @vlr          = Null
		
	Insert 
	Into       #TablaTempRetornoMarcaPrestacion(dsc_tpo_mrca, vlr, inco_vgnca, fnVgnca)
	Select     upper(t.dscrpcn_tpo_mrca_prstcn), m.vlr_char, m.inco_vgnca_mrca_prstcn, m.fn_vgnca_mrca_prstcn
	From       bdSisalud.dbo.tbMarcasPrestacion m  With(NoLock)
	Inner Join bdSisalud.dbo.tbTiposMarcasPrestacion_Vigencias t With(NoLock) 
	On         t.cnsctvo_cdgo_tpo_mrca_prstcn = m.cnsctvo_cdgo_tpo_mrca_prstcn
	Where      m.cnsctvo_cdfccn = @id_cnsctvo_prstcn 
	And        @fecha_actual Between m.inco_vgnca_mrca_prstcn And m.fn_vgnca_mrca_prstcn
	And        @fecha_actual Between t.inco_vgnca And t.fn_vgnca
		 
-- Ajustado por Angela Sandoval para mostrar el tipo - el agrupador		
	Insert 
	Into       #TablaTempRetornoMarcaPrestacion(dsc_tpo_mrca, vlr, inco_vgnca, fnVgnca)		 
	Select     ltrim(rtrim(upper(dscrpcn_tpo_agrpdr_prstcn)))+' - '+ ltrim(rtrim(dscrpcn_agrpdr_prstcn)), @vlr, a.inco_vgnca, a.fn_vgnca 
	--Select     ltrim(rtrim(upper(dscrpcn_tpo_agrpdr_prstcn)))+' - '+ ltrim(rtrim(dscrpcn_agrpdr_prstcn)), a.inco_vgnca, a.fn_vgnca 
	From       bdsisalud.dbo.tbDetAgrupadoresPrestaciones_Vigencias a With(NoLock) 
    Inner Join bdSisalud.dbo.tbAgrupadoresPrestaciones_vigencias b With(NoLock)
	On         b.cnsctvo_cdgo_agrpdr_prstcn  = a.cnsctvo_cdgo_agrpdr_prstcn
	inner Join bdSisalud.dbo.tbTiposAgrupadoresPrestaciones c on
	           c.cnsctvo_cdgo_tpo_agrpdr_prstcn=b.cnsctvo_cdgo_tpo_agrpdr_prstcn
    Where      a.cnsctvo_cdfccn = @id_cnsctvo_prstcn
    And        @fecha_actual Between a.inco_vgnca And a.fn_vgnca
	And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca


	Select Distinct dsc_tpo_mrca, vlr, inco_vgnca, fnVgnca               
	From   #TablaTempRetornoMarcaPrestacion
	order by dsc_tpo_mrca
	
	Drop Table #TablaTempRetornoMarcaPrestacion
END
