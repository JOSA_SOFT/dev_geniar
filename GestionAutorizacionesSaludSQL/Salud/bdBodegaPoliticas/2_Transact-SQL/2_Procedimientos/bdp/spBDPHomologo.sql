USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPHomologo') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPHomologo AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPHomologo
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar las homologaciones.
* Observaciones : <\O O\>    
* Parametros  :
				
				<\P	@lccdgo_srvco 	P\>


* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Jose Olmedo Soto AM\>    
* Descripcion        : <\DM Ajuste de consecutivo en parametro DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPHomologo @lccdgo_srvco	= 871121
    EJ\>
*-----------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPHomologo
	@lccdgo_srvco udtConsecutivo,
	@cnsctvo_cdd udtConsecutivo,
	@cdgo_intrno udtCodigoIps
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @ldFechaConsulta						datetime,
			@lncnsctvo_cdgo_clsfccn_prstcn_prstdr	int,
			@vco									char(1)

	Create Table #tmpProcedimientos(
		cdgo_srvco			Char(11),
		dscrpcn_srvco		udtDescripcion,
		cdgo_cdfccn			Char(11),
		dscrpcn_cdfccn		udtDescripcion, 
		cdgo_intrno		    udtCodigoIPS,
		nmbre_scrsl			varchar(150),
		cnsctvo_cdgo_cdd    udtConsecutivo
    )
	  
	SET @ldFechaConsulta = getdate()
	SET @lncnsctvo_cdgo_clsfccn_prstcn_prstdr = 3
	SET @vco = '';

	Insert 
	Into       #tmpProcedimientos(cdgo_cdfccn   , dscrpcn_cdfccn, cdgo_srvco, 
	                              cdgo_intrno, nmbre_scrsl,cnsctvo_cdgo_cdd
								 )
    Select     c.cdgo_cdfccn   , c.dscrpcn_cdfccn, p1.cdgo_prstcn_prstdr, 
	           p1.cdgo_intrno, nmbre_scrsl,gv.cnsctvo_cdgo_cdd
    From       BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
    Inner Join BDIpsParametros.dbo.tbDetalle_PrestacionesPrestador h2 With(NoLock) 
	On         p1.cnsctvo_cdgo_prstcn_prstdr = h2.cnsctvo_cdgo_prstcn_prstdr
    Inner Join BDIpsParametros.dbo.tbHomologacionPrestaciones h1 With(NoLock) 
	On         h1.cdgo_intrno                = p1.cdgo_intrno And 
			   h1.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr 
    Inner Join BDIpsParametros.dbo.tbDetalle_HomologacionPrestaciones h3 With(NoLock) 
	On         h3.cnsctvo_cdgo_hmlgcn_prstcn = h1.cnsctvo_cdgo_hmlgcn_prstcn 
    Inner Join bdsisalud.dbo.tbCodificaciones c With(NoLock) 
	On         c.cnsctvo_cdfccn = h1.cnsctvo_cdfccn
    Inner Join bdsisalud.dbo.tbcupsServicios g With(NoLock) 
	On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn       
	Inner Join bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock) 
	On         dp.cdgo_intrno = p1.cdgo_intrno 
	Inner Join bdafiliacionvalidador.dbo.tbciudades_vigencias gv With(NoLock) 
	On         gv.cnsctvo_cdgo_cdd = dp.cnsctvo_cdgo_cdd
    WHERE      c.cnsctvo_cdfccn                      = @lccdgo_srvco
    And        h1.cnsctvo_cdgo_clsfccn_prstcn_prstdr = @lncnsctvo_cdgo_clsfccn_prstcn_prstdr
	And        @ldFechaConsulta Between h2.inco_vgnca And h2.fn_vgnca
    And        @ldFechaConsulta Between h3.inco_vgnca And h3.fn_vgnca   
	And        @ldFechaConsulta Between g.inco_vgnca  And g.fn_vgnca  
	And        @ldFechaConsulta Between dp.inco_vgnca And dp.fn_vgnca 


	IF @cnsctvo_cdd IS NOT NULL
	BEGIN	
		DELETE #tmpProcedimientos 
		WHERE cnsctvo_cdgo_cdd <> @cnsctvo_cdd
	END	

	IF @cdgo_intrno IS NOT NULL AND @cdgo_intrno<>@vco
	BEGIN
		DELETE #tmpProcedimientos 
		WHERE cdgo_intrno <> @cdgo_intrno
	END


	--actualiza la descripcion del servicio en la tabla temporal
	UPDATE      #tmpProcedimientos
	SET         dscrpcn_srvco = b.dscrpcn_cdfccn
	FROM        #tmpProcedimientos a  
	INNER JOIN  bdsisalud.dbo.tbCodificaciones b With(NoLock) 
	ON          b.cdgo_cdfccn = a.cdgo_srvco

	--actualiza la descripcion del servicio en la tabla temporal 
	UPDATE      #tmpProcedimientos
	SET         dscrpcn_srvco = dscrpcn_cdfccn
	FROM        #tmpProcedimientos a
	INNER JOIN  BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
	ON          p1.cdgo_prstcn_prstdr = a.cdgo_srvco
	WHERE       a.dscrpcn_srvco Is Null

	Select Distinct  cdgo_cdfccn  , dscrpcn_cdfccn, cdgo_srvco, 
	                 dscrpcn_srvco, cdgo_intrno, nmbre_scrsl
	From #tmpProcedimientos
	order by nmbre_scrsl

	Drop Table #tmpProcedimientos
END
GO
