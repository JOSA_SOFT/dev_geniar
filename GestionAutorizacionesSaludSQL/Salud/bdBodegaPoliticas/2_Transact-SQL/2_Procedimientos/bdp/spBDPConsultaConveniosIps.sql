USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPConsultaConveniosIps]    Script Date: 4/10/2017 10:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPConsultaConveniosIps') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPConsultaConveniosIps AS SELECT 1;'
END
GO


/*----------------------------------------------------------------------------------------
* Metodo o PRG     : spBDPConsultaConveniosIps
* Desarrollado por : <\A Daniel Ramirez   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar los convenios por IPS, prestación y ciudad
                     D\>    
* Observaciones  :   <\O O\>    
* Parametros     :   <\P 
						 @lcDescrServicio  descripcion del servicio
						 @lnTipoServicio   tipo del servicio
						 @lbCupsNorma	   cups norma
						 @cdgoInterno	   codigo interno,
						 @cnsctvoCdgoCdd   codigo de la ciudad 
					 P\>    
* Variables      :   <\V V\>    
* Fecha Creacion :   <\FC 2017/08/08 FC\>
* Ejemplos: 
    <\EJ
       Exec [bdBodegasPoliticas].[bdp].[spBDPConsultaConveniosIps]  '<prestaciones><prestacion value="SATURACION" /></prestaciones>', 4, 0, '00001', null
	   Exec [bdBodegasPoliticas].[bdp].[spBDPConsultaConveniosIps]  '<prestaciones><prestacion value="oral" /></prestaciones>', 5, 0, '00001', null
	   Exec [bdBodegasPoliticas].[bdp].[spBDPConsultaConveniosIps]  '<prestaciones><prestacion value="sencillo" /><prestacion value="plus" /></prestaciones>', 9, 0, '92338', null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------*/ 

ALTER procedure bdp.spBDPConsultaConveniosIps
	@lcDescrServicio  xml,
	@lnTipoServicio   udtConsecutivo,
	@lbCupsNorma	  Int,
	@cdgoInterno	  udtCodigoIps,
	@cnsctvoCdgoCdd   udtConsecutivo
AS
Begin
	SET NOCOUNT ON;

	Declare @fecha_actual	              Datetime,
			@lcVacio                      udtLogico,
			@lcPorcentaje                 udtLogico,
			@cnsctvo_tpo_cdfccn_cups      Int,
	        @cnsctvo_tpo_cdfccn_cums      Int,
	        @cnsctvo_tpo_cdfccn_pis       Int,
			@esCupsNorma			      Int,
			@cnsctvo_cdgo_tpo_mrca_prstcn Int,
			@cnsctvo_cdgo_vlr_mrca_prstcn Int,
			
			@cnst_vco 					  udtLogico,
			@cnst_s 					  udtLogico,
			@cnst_n 					  udtLogico

	create table #TempDescFiltro (
		dscrpcn_prstcn udtDescripcion
	);
	
	Create table #TablaTempServicios (
		ips_cdgo 						udtConsecutivo,
		ips_dscrpcn 					udtDescripcion,
		vlr 							float,
		drccn 							udtDescripcion,
		tlfno 							udtDescripcion,
		inco_vgnca 						datetime,
		fn_vgnca 						datetime,
		cnsctvo_cdgo_tpo_cdfccn 		udtConsecutivo,
		cnsctvo_prstncn 				udtConsecutivo,
		cnsctvo_mdlo_cnvno_pln 			udtConsecutivo,
		cnsctvo_cdgo_cdd				udtConsecutivo,
		cnsctvo_cdgo_pln				udtConsecutivo,
		cdgo_intrno						udtCodigoIps,
		cnsctvo_cdgo_frma_lqdcn     	udtConsecutivo,
		cnsctvo_cdgo_clsfccn_atncn		udtConsecutivo	
	)
	
	Create Table #TablaTempConvenioPrestacion(
		cnsctvo_cdfccn					udtConsecutivo, --id de la prestacion  
		cdgo_cdfccn						char(11),  --Codigo de la prestacion
		dscrpcn_cdfccn					udtDescripcion, --Descripcion de la prestacion
		ips_cdgo 						udtConsecutivo,
		ips_dscrpcn 					udtDescripcion,
		cdd_dsc 						udtDescripcion,
		pln_dsc 						udtDescripcion,
		vlr 							float,
		tpo_atncn 						udtDescripcion,
		frmla_lqdcn 					udtDescripcion,
		accso_drcto 					char(1),
		drccn 							udtDescripcion,
		tlfno 							udtDescripcion,
		inco_vgnca 						datetime,
		fn_vgnca 						datetime,
		cnsctvo_cdgo_tpo_cdfccn 		udtConsecutivo,
		cnsctvo_prstncn 				udtConsecutivo,
		cnsctvo_mdlo_cnvno_pln 			udtConsecutivo,
		cnsctvo_cdgo_cdd				udtConsecutivo,
		cnsctvo_cdgo_pln				udtConsecutivo,
		cdgo_intrno						udtCodigoIps,
		cnsctvo_cdgo_frma_lqdcn     	udtConsecutivo,
		cnsctvo_cdgo_clsfccn_atncn		udtConsecutivo,
		cnsctvo_cdgo_tpo_mrca_prstcn	Int,
		cnsctvo_cdgo_vlr_mrca_prstcn	Int		
	)

	Create Table #TablaTempConvenioPrestacionFiltered(
		cnsctvo_cdfccn					udtConsecutivo, --id de la prestacion  
		cdgo_cdfccn						char(11),  --Codigo de la prestacion
		dscrpcn_cdfccn					udtDescripcion, --Descripcion de la prestacion
		ips_cdgo 						udtConsecutivo,
		ips_dscrpcn 					udtDescripcion,
		cdd_dsc 						udtDescripcion,
		pln_dsc 						udtDescripcion,
		vlr 							float,
		tpo_atncn 						udtDescripcion,
		frmla_lqdcn 					udtDescripcion,
		accso_drcto 					char(1),
		drccn 							udtDescripcion,
		tlfno 							udtDescripcion,
		inco_vgnca 						datetime,
		fn_vgnca 						datetime,
		cnsctvo_cdgo_tpo_cdfccn 		udtConsecutivo,
		cnsctvo_prstncn 				udtConsecutivo,
		cnsctvo_mdlo_cnvno_pln 			udtConsecutivo,
		cnsctvo_cdgo_cdd				udtConsecutivo,
		cnsctvo_cdgo_pln				udtConsecutivo,
		cdgo_intrno						udtCodigoIps,
		cnsctvo_cdgo_frma_lqdcn     	udtConsecutivo,
		cnsctvo_cdgo_clsfccn_atncn		udtConsecutivo,
		cnsctvo_cdgo_tpo_mrca_prstcn	Int,
		cnsctvo_cdgo_vlr_mrca_prstcn	Int		
	)
	
	Set @cnsctvo_tpo_cdfccn_cups      = 4;
	Set @cnsctvo_tpo_cdfccn_cums 	  = 5;
	Set @cnsctvo_tpo_cdfccn_pis  	  = 9;
	Set @lcVacio                 	  = ''; 
	Set @lcPorcentaje            	  = '%';
	Set @fecha_actual                 = getDate();
	Set @esCupsNorma			      = 1;
	Set @cnsctvo_cdgo_tpo_mrca_prstcn = 17;
	Set @cnsctvo_cdgo_vlr_mrca_prstcn = 3;
	
	SET @cnst_s ='S';
	SET @cnst_n = 'N';
	SET @cnst_vco = '';		
	


	
	--Creo una tabla para filtrar las descripciones
	Insert Into #TempDescFiltro (dscrpcn_prstcn)
	Select prestaciones.prestacion.value('@value', 'udtDescripcion')
	From   @lcDescrServicio.nodes('//prestacion') prestaciones(prestacion);
	
	
	-- Tabla temporal que contiene los tipos de servicios
	Insert Into #TablaTempServicios (
				ips_cdgo              , ips_dscrpcn            , vlr                    ,
				drccn                 , tlfno                  , inco_vgnca             ,
				fn_vgnca              , cnsctvo_cdgo_tpo_cdfccn, cnsctvo_prstncn        ,
				cnsctvo_mdlo_cnvno_pln, cnsctvo_cdgo_cdd       , cnsctvo_cdgo_pln       ,
				cdgo_intrno           , cnsctvo_cdgo_frma_lqdcn, cnsctvo_cdgo_clsfccn_atncn
	)
	Select Distinct
			   f.cdgo_intrno           , f.nmbre_scrsl            , c.vlr_trfdo             ,
			   f.drccn                 , f.tlfno                  , c.inco_vgnca            ,
			   c.fn_vgnca              , c.cnsctvo_cdgo_tpo_cdfccn, c.cnsctvo_prstncn       ,
			   e.cnsctvo_mdlo_cnvno_pln, f.cnsctvo_cdgo_cdd       , e.cnsctvo_cdgo_pln      ,
			   e.cdgo_intrno           , c.cnsctvo_cdgo_frma_lqdcn, c.cnsctvo_cdgo_tpo_atncn
	FROM       bdcontratacion.dbo.tblsdetlistapreciosxsucursalxplan c WITH(nolock)
    INNER JOIN bdcontratacion.dbo.tblslistapreciosxsucursalxplan d WITH(nolock) 
	ON         c.cnsctvo_cdgo_lsta_prco = d.cnsctvo_cdgo_lsta_prco 
    INNER JOIN bdsisalud.dbo.tbasociacionmodeloactividad e WITH(nolock) 
	ON         e.cnsctvo_asccn_mdlo_actvdd = d.cnsctvo_asccn_mdlo_actvdd 
	INNER JOIN bdsisalud.dbo.tbdireccionesprestador f  WITH(nolock)
	ON         e.cdgo_intrno = f.cdgo_intrno
	WHERE	   @fecha_actual between c.inco_vgnca and c.fn_vgnca
	AND        @fecha_actual between d.inco_vgnca and d.fn_vgnca
	AND        @fecha_actual between e.inco_vgnca and e.fn_vgnca
	AND        @fecha_actual between f.inco_vgnca and f.fn_vgnca
	AND        f.cdgo_intrno = @cdgoInterno 

	If @lnTipoServicio = @cnsctvo_tpo_cdfccn_cups 
		Begin
			INSERT INTO #tablatempconvenioprestacion (
				cnsctvo_cdfccn            , cdgo_cdfccn                 , dscrpcn_cdfccn         ,
				ips_cdgo                  , ips_dscrpcn                 , vlr                    ,
				accso_drcto               , drccn                       , tlfno                  ,
				inco_vgnca                , fn_vgnca                    , cnsctvo_cdgo_tpo_cdfccn,
				cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      , cnsctvo_cdgo_cdd       ,
				cnsctvo_cdgo_pln          , cdgo_intrno                 , cnsctvo_cdgo_frma_lqdcn,
				cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn, cnsctvo_cdgo_vlr_mrca_prstcn) 
			SELECT DISTINCT 
					   b.cnsctvo_cdfccn            , b.cdgo_cdfccn                 , b.dscrpcn_cdfccn         , 
					   c.ips_cdgo                  , c.ips_dscrpcn                 , c.vlr                    , 
					   @cnst_vco accso_drcto       , c.drccn                       , c.tlfno                  , 
					   c.inco_vgnca                , c.fn_vgnca                    , c.cnsctvo_cdgo_tpo_cdfccn, 
					   c.cnsctvo_prstncn           , c.cnsctvo_mdlo_cnvno_pln      , c.cnsctvo_cdgo_cdd       , 
					   c.cnsctvo_cdgo_pln          , c.cdgo_intrno                 , c.cnsctvo_cdgo_frma_lqdcn, 
					   c.cnsctvo_cdgo_clsfccn_atncn, d.cnsctvo_cdgo_tpo_mrca_prstcn, d.cnsctvo_cdgo_vlr_mrca_prstcn
			FROM       bdsisalud.dbo.tbcupsservicios a WITH(nolock) 
		    INNER JOIN bdsisalud.dbo.tbcodificaciones b WITH(nolock) 
			ON         a.cnsctvo_prstcn = b.cnsctvo_cdfccn 
		    INNER JOIN #TablaTempServicios c
			ON         a.cnsctvo_prstcn = c.cnsctvo_prstncn
		    INNER JOIN bdsisalud.dbo.tbMarcasPrestacion d WITH(nolock) 
			ON         d.cnsctvo_cdfccn = a.cnsctvo_prstcn
			WHERE	   @fecha_actual between a.inco_vgnca and a.fn_vgnca
			AND        @fecha_actual between d.inco_vgnca_mrca_prstcn and d.fn_vgnca_mrca_prstcn
		
			if( @lbCupsNorma = @esCupsNorma )
				Begin
					DELETE #tablatempconvenioprestacion
					WHERE  cnsctvo_cdgo_tpo_mrca_prstcn != @cnsctvo_cdgo_tpo_mrca_prstcn 
					AND cnsctvo_cdgo_vlr_mrca_prstcn != @cnsctvo_cdgo_vlr_mrca_prstcn 
				End
		End
	
	
	if @lnTipoServicio = @cnsctvo_tpo_cdfccn_cums 
		Begin
			INSERT INTO #tablatempconvenioprestacion (
				cnsctvo_cdfccn            , cdgo_cdfccn                 , dscrpcn_cdfccn         , 
				ips_cdgo                  , ips_dscrpcn                 , vlr                    , 
				accso_drcto               , drccn                       , tlfno                  , 
				inco_vgnca                , fn_vgnca                    , cnsctvo_cdgo_tpo_cdfccn, 
				cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      , cnsctvo_cdgo_cdd       , 
				cnsctvo_cdgo_pln          , cdgo_intrno                 , cnsctvo_cdgo_frma_lqdcn, 
				cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn, cnsctvo_cdgo_vlr_mrca_prstcn) 
			SELECT DISTINCT
					   b.cnsctvo_cdfccn            , b.cdgo_cdfccn           , b.dscrpcn_cdfccn         , 
					   c.ips_cdgo                  , c.ips_dscrpcn           , c.vlr                    , 
					   @cnst_vco accso_drcto       , c.drccn                 , c.tlfno                  , 
				   	   c.inco_vgnca            	   , c.fn_vgnca              , c.cnsctvo_cdgo_tpo_cdfccn, 
					   c.cnsctvo_prstncn           , c.cnsctvo_mdlo_cnvno_pln, c.cnsctvo_cdgo_cdd       , 
					   c.cnsctvo_cdgo_pln          , c.cdgo_intrno           , c.cnsctvo_cdgo_frma_lqdcn, 
					   c.cnsctvo_cdgo_clsfccn_atncn, null                    , null
			FROM       bdsisalud.dbo.tbcums_vigencias a WITH(nolock) 
			INNER JOIN bdsisalud.dbo.tbcodificaciones b WITH(nolock) 
			ON         a.cnsctvo_cms = b.cnsctvo_cdfccn 
			INNER JOIN #TablaTempServicios c
			ON         a.cnsctvo_cms = c.cnsctvo_prstncn
			WHERE	   @fecha_actual between a.inco_vgnca and a.fn_vgnca
		End
	
	if @lnTipoServicio =  @cnsctvo_tpo_cdfccn_pis
		Begin
			INSERT INTO #tablatempconvenioprestacion (
				cnsctvo_cdfccn            , cdgo_cdfccn                 , dscrpcn_cdfccn         , 
				ips_cdgo                  , ips_dscrpcn                 , vlr                    , 
				accso_drcto               , drccn                       , tlfno                  , 
				inco_vgnca                , fn_vgnca                    , cnsctvo_cdgo_tpo_cdfccn, 
				cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      , cnsctvo_cdgo_cdd       , 
				cnsctvo_cdgo_pln          , cdgo_intrno                 , cnsctvo_cdgo_frma_lqdcn, 
				cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn, cnsctvo_cdgo_vlr_mrca_prstcn) 
			SELECT DISTINCT
					   b.cnsctvo_cdfccn            , b.cdgo_cdfccn           , b.dscrpcn_cdfccn         , 
					   c.ips_cdgo                  , c.ips_dscrpcn           , c.vlr                    ,  
					   @cnst_vco accso_drcto       , c.drccn                 , c.tlfno                  , 
				       c.inco_vgnca                , c.fn_vgnca              , c.cnsctvo_cdgo_tpo_cdfccn, 
				       c.cnsctvo_prstncn           , c.cnsctvo_mdlo_cnvno_pln, c.cnsctvo_cdgo_cdd       , 
				       c.cnsctvo_cdgo_pln          , c.cdgo_intrno           , c.cnsctvo_cdgo_frma_lqdcn, 
				       c.cnsctvo_cdgo_clsfccn_atncn, null                    , null 
			FROM       bdsisalud.dbo.tbprestacionpis_vigencias a WITH(nolock) 
		    INNER JOIN bdsisalud.dbo.tbcodificaciones b WITH(nolock) 
			ON         a.cnsctvo_prstcn_pis = b.cnsctvo_cdfccn 
		    INNER JOIN #TablaTempServicios c
			ON         a.cnsctvo_prstcn_pis = c.cnsctvo_prstncn
		    WHERE	   @fecha_actual between a.inco_vgnca and a.fn_vgnca
			
		End
	
	-- Si el consecutivo de la ciudad no es nulo
	If @cnsctvoCdgoCdd Is Not Null
	Begin
		-- Filtro las descripciones y el codigo de la ciudad
		INSERT INTO #TablaTempConvenioPrestacionFiltered (
			cnsctvo_cdfccn              , cdgo_cdfccn               , dscrpcn_cdfccn              ,
			ips_cdgo                    , ips_dscrpcn               , cdd_dsc                     ,
			pln_dsc                     , vlr                       , tpo_atncn                   ,
			frmla_lqdcn                 , accso_drcto               , drccn                       ,
			tlfno                       , inco_vgnca                , fn_vgnca                    ,
			cnsctvo_cdgo_tpo_cdfccn     , cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      ,
			cnsctvo_cdgo_cdd            , cnsctvo_cdgo_pln          , cdgo_intrno                 ,
			cnsctvo_cdgo_frma_lqdcn     , cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn,
			cnsctvo_cdgo_vlr_mrca_prstcn)
		SELECT     cnsctvo_cdfccn              , cdgo_cdfccn               , dscrpcn_cdfccn,
				   ips_cdgo                    , ips_dscrpcn               , cdd_dsc,
				   pln_dsc                     , vlr                       , tpo_atncn,
				   frmla_lqdcn                 , accso_drcto               , drccn,
				   tlfno                       , inco_vgnca                , fn_vgnca,
				   cnsctvo_cdgo_tpo_cdfccn     , cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln, 
				   cnsctvo_cdgo_cdd            , cnsctvo_cdgo_pln          , cdgo_intrno,
				   cnsctvo_cdgo_frma_lqdcn     , cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn,
				   cnsctvo_cdgo_vlr_mrca_prstcn
		FROM       #tablatempconvenioprestacion a
		INNER JOIN #TempDescFiltro b
		ON         a.dscrpcn_cdfccn like @lcPorcentaje + b.dscrpcn_prstcn + @lcPorcentaje
		WHERE      cnsctvo_cdgo_cdd = @cnsctvoCdgoCdd
	End
	Else	
	Begin
		-- Filtro las descripciones
		INSERT INTO #TablaTempConvenioPrestacionFiltered (
			cnsctvo_cdfccn              , cdgo_cdfccn               , dscrpcn_cdfccn              ,
			ips_cdgo                    , ips_dscrpcn               , cdd_dsc                     ,
			pln_dsc                     , vlr                       , tpo_atncn                   ,
			frmla_lqdcn                 , accso_drcto               , drccn                       ,
			tlfno                       , inco_vgnca                , fn_vgnca                    ,
			cnsctvo_cdgo_tpo_cdfccn     , cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      ,
			cnsctvo_cdgo_cdd            , cnsctvo_cdgo_pln          , cdgo_intrno                 ,
			cnsctvo_cdgo_frma_lqdcn     , cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn,
			cnsctvo_cdgo_vlr_mrca_prstcn)
		SELECT     cnsctvo_cdfccn              , cdgo_cdfccn               , dscrpcn_cdfccn,
				   ips_cdgo                    , ips_dscrpcn               , cdd_dsc,
				   pln_dsc                     , vlr                       , tpo_atncn,
				   frmla_lqdcn                 , accso_drcto               , drccn,
				   tlfno                       , inco_vgnca                , fn_vgnca,
				   cnsctvo_cdgo_tpo_cdfccn     , cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln, 
				   cnsctvo_cdgo_cdd            , cnsctvo_cdgo_pln          , cdgo_intrno,
				   cnsctvo_cdgo_frma_lqdcn     , cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn,
				   cnsctvo_cdgo_vlr_mrca_prstcn
		FROM       #tablatempconvenioprestacion a
		INNER JOIN #TempDescFiltro b
		ON         a.dscrpcn_cdfccn like @lcPorcentaje + b.dscrpcn_prstcn + @lcPorcentaje
	End
	
	--Actualizo las descricpiones
	--Ciudad
	UPDATE d
	SET d.cdd_dsc =  cv.dscrpcn_cdd								
	FROM #TablaTempConvenioPrestacionFiltered d
	INNER JOIN bdafiliacionvalidador.dbo.tbciudades_vigencias cv WITH(NOLOCK) ON d.cnsctvo_cdgo_cdd = cv.cnsctvo_cdgo_cdd
	WHERE   @fecha_actual between cv.inco_vgnca and cv.fn_vgnca
	
	--Plan
	UPDATE d
	SET d.pln_dsc = pl_v.dscrpcn_pln									
	FROM #TablaTempConvenioPrestacionFiltered d
	INNER JOIN BDAfiliacionValidador.dbo.tbPlanes_vigencias pl_v  WITH(NOLOCK) on d.cnsctvo_cdgo_pln = pl_v.cnsctvo_cdgo_pln
	WHERE  @fecha_actual between pl_v.inco_vgnca and pl_v.fn_vgnca	
	
	--Tipo de atención
	UPDATE d
	SET d.tpo_atncn = i.dscrpcn_clsfccn_atncn							
	FROM #TablaTempConvenioPrestacionFiltered d
	INNER JOIN bdSisalud.dbo.tbClasificacionAtencion_vigencias i  WITH(NOLOCK) ON i.cnsctvo_cdgo_clsfccn_atncn = d.cnsctvo_cdgo_clsfccn_atncn
		and 	 @fecha_actual between i.inco_vgnca and i.fn_vgnca	
	
	--Formula
	UPDATE d
	SET d.frmla_lqdcn =  j.dscrpcn_frma_lqdcn_prstcn								
	FROM #TablaTempConvenioPrestacionFiltered d
	INNER JOIN bdSisalud.dbo.tbFormaLiquidacionPrestacion_vigencias j WITH(NOLOCK) ON j.cnsctvo_cdgo_frma_lqdcn_prstcn= d.cnsctvo_cdgo_frma_lqdcn
		and  		@fecha_actual between j.inco_vgnca and j.fn_vgnca
	
	--Acceso directo
	UPDATE     d
	SET        d.accso_drcto = l.accso_drcto									
	FROM       #TablaTempConvenioPrestacionFiltered d
	INNER JOIN bdContratacion.dbo.tbModeloConveniosPrestacionesxPlan k WITH(NOLOCK) --Convenios de Prestaciones X plan
	ON         d.cnsctvo_mdlo_cnvno_pln = k.cnsctvo_mdlo_cnvno_prstcn_pln
	INNER JOIN bdContratacion.dbo.tbDetModeloConveniosPrestaciones l WITH(NOLOCK) -- Detalle de los Convenios de Prestaciones
	ON         k.cnsctvo_cdgo_mdlo_cnvno_prstcn = l.cnsctvo_cdgo_mdlo_cnvno_prstcn
	INNER JOIN bdsisaLUD.dbo.tbDetEquivalencias de WITH(NOLOCK)--Las euivalencia
	ON         l.cnsctvo_det_eqvlnca = de.cnsctvo_det_eqvlnca
	INNER JOIN bdsisalud.dbo.tbCodificaciones co WITH(NOLOCK)-- Las prestaciones 
	ON         (de.cnsctvo_cdfccn_b = co.cnsctvo_cdfccn AND co.cnsctvo_cdgo_tpo_cdfccn =d.cnsctvo_cdgo_tpo_cdfccn AND co.cnsctvo_cdfccn=d.cnsctvo_prstncn)
	WHERE      @fecha_actual  between k.inco_vgnca and k.fn_vgnca
	AND        @fecha_actual between l.inco_vgnca and l.fn_vgnca		
	AND        @fecha_actual between de.inco_vgnca and de.fn_vgnca	
		
	UPDATE #TablaTempConvenioPrestacionFiltered				  
	SET accso_drcto = @cnst_n
	WHERE accso_drcto <> @cnst_s

	UPDATE #TablaTempConvenioPrestacionFiltered				  
	SET accso_drcto = @cnst_n
	WHERE  accso_drcto is null

	select cnsctvo_cdfccn              , cdgo_cdfccn               , dscrpcn_cdfccn              ,
		   ips_cdgo                    , ips_dscrpcn               , cdd_dsc                     ,
		   pln_dsc                     , vlr                       , tpo_atncn                   ,
		   frmla_lqdcn                 , accso_drcto               , drccn                       ,
		   tlfno                       , inco_vgnca                , fn_vgnca                    ,
		   cnsctvo_cdgo_tpo_cdfccn     , cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln      ,
		   cnsctvo_cdgo_cdd            , cnsctvo_cdgo_pln          , cdgo_intrno                 ,
		   cnsctvo_cdgo_frma_lqdcn     , cnsctvo_cdgo_clsfccn_atncn, cnsctvo_cdgo_tpo_mrca_prstcn,
		   cnsctvo_cdgo_vlr_mrca_prstcn		
	from   #TablaTempConvenioPrestacionFiltered

	Drop table #TablaTempConvenioPrestacionFiltered
	Drop table #tablatempconvenioprestacion
	Drop Table #TempDescFiltro
	Drop table #TablaTempServicios
End

