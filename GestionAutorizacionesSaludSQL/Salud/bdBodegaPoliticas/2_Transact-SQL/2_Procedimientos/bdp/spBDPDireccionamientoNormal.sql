USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPDireccionamientoNormal]    Script Date: 09/05/2017 8:37:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spBDPDireccionamientoNormal
* Desarrollado por : José Soto 
* Descripcion      : Proceso encargado de consultar los direccionamientos normales.
* Observaciones    : <\O O\>    
* Parametros  :
				<\P	@cdgo_intrno 	P\>
				<\P	@cnsctvo_cdfccn	P\>
* Variables   : <\V V\>
* Fecha Creacion   : <\FC 2017/03/16 FC\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento cambiando el parametro de entrada al procedmiento y la 
							consulta. Para que tome el consecutivo de la ciudad en vez del codigo interno, consultando
							todos lo direccionamientos de la prestacion en una ciudade determinada
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/28 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Angela Sandoval
                            La zona de la IPS no siembre esta parametrizada,  y siempre debe aparecer
							Se adiciona el inner con la lista de precios tbLSDetListaPreciosxSucursalxPlan
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/28 FM\>    

*---------------------------------------------------------------------------------------------------------------------------------*/

---Exec  [bdp].[spBDPDireccionamientoNormal]  8112, 94542
-- select * from bdsisalud..tbcodificaciones where cdgo_cdfccn='00048811-07'
-- select * from bdafiliacionvalidador..tbciudades where dscrpcn_cdd like '%cali%'

ALTER PROCEDURE [bdp].[spBDPDireccionamientoNormal]
	@cnsctvo_cdgo_cdd udtConsecutivo,
	@cnsctvo_cdfccn	  udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;	

	DECLARE @fcha_actl datetime ,
	        @lcVacio   udtLogico	

	SET @fcha_actl = GETDATE()
	Set @lcVacio   = ''

	Select     a.cdgo_intrno                               , b.nmbre_scrsl , gv.dscrpcn_cdd,
			   IsNull(fv.dscrpcn_zna, @lcVacio) dscrpcn_zna, ev.dscrpcn_pln, a.edd_mnma    ,
			   a.edd_mxma                                  , a.sfcnca_ips  , a.nvl_prrdd   ,
			   a.inco_vgnca                                , a.fn_vgnca
	From       bdCNA.sol.tbCNADireccionamientoPrestadorDestino a With(NoLock)
    Inner Join bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan DPS WITH (NOLOCK)
    ON         a.cnsctvo_cdgo_dt_lsta_prco = DPS.cnsctvo_cdgo_dt_lsta_prco
	Inner Join bdSisalud.dbo.tbDireccionesPrestador	b With(NoLock) 
	On         b. cdgo_intrno = a.cdgo_intrno
	Inner Join bdafiliacionvalidador.dbo.tbplanes_vigencias ev With(NoLock) 
	On         ev.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	left outer Join bdafiliacionvalidador.dbo.tbzonas_vigencias fv  With(NoLock) 
	On         fv.cnsctvo_cdgo_zna = b.cnsctvo_cdgo_zna
	And        @fcha_actl Between fv.inco_vgnca And fv.fn_vgnca
	Inner Join bdafiliacionvalidador.dbo.tbciudades_vigencias gv With(NoLock) 
	On         gv.cnsctvo_cdgo_cdd = b.cnsctvo_cdgo_cdd
	Where      b.cnsctvo_cdgo_cdd = @cnsctvo_cdgo_cdd
	And        a.cnsctvo_cdfccn = @cnsctvo_cdfccn 
	And        @fcha_actl Between a.inco_vgnca  And a.fn_vgnca
	And        @fcha_actl Between b.inco_vgnca  And b.fn_vgnca
	And        @fcha_actl Between ev.inco_vgnca And ev.fn_vgnca
	And        @fcha_actl Between gv.inco_vgnca And gv.fn_vgnca
END