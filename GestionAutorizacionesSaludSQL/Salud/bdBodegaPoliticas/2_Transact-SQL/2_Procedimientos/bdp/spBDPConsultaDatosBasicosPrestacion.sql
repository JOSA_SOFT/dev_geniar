USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPConsultaDatosBasicosPrestacion]    Script Date: 09/05/2017 8:38:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPConsultaDatosBasicosPrestacion
* Desarrollado por : José Soto 
* Descripcion  : Proceso encargado de consultar los datos básicos de la prestación según el consecutivo de la prestación.  
* Observaciones : <\O O\>    
* Parametros  : <\P id_cnsctvo_tpo_prtcn P\>
				<\P id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPConsultaDatosBasicosPrestacion @id_cnsctvo_tpo_prtcn = 05 , @id_cnsctvo_prstcn = 23070
    EJ\>
*-----------------------------------------------------------------------------------------
* Modificado por : Angela Sandoval
* Descripcion  : Las variables que no existen de los medicamentos e insumos se les coloca que 'N.A.'No aplican
* Observaciones : <\O O\>    
*-----------------------------------------------------------------------------------------*/

ALTER PROCEDURE [bdp].[spBDPConsultaDatosBasicosPrestacion]
	@id_cnsctvo_tpo_prtcn udtConsecutivo,
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
	 @fecha_actual	datetime,
	 @cdgo_cups		INT,
	 @cdgo_cums		INT,
	 @cdgo_cuos		INT,
	 @cdgo_cgo		INT, -- Codigo de cargo
	 @cnst_n		char(1),
	 @cnst_s		char(1),
 	 @cnst_NA		char(4)
	
	Create Table #TablaTempRetornoDatoBasicoPrestacion(
		inco_vgnca		datetime,
		fn_vgnca		datetime,
		gnro			varchar(200),
		nvl				varchar(200),
		cbrtra_ps		char(1),
		cbrtra_no_ps	char(1),
		cbrtra_ps_cnd	char(1),
		cbrtra_ato_f	char(1)
	)


	--Códigos de tipos de prestaciones para determinar tabla a consultar.
	SET @cdgo_cups = 04;
	SET @cdgo_cums = 05;
	SET @cdgo_cuos = 09;
	SET @cdgo_cgo = 31;
	SET @fecha_actual = GETDATE()
	SET @cnst_n = 'N'
	SET @cnst_s = 'S'
	SET @cnst_NA= 'N.A.'
	
	
	
	IF (@id_cnsctvo_tpo_prtcn = @cdgo_cups)
	BEGIN
		INSERT INTO #TablaTempRetornoDatoBasicoPrestacion (
			inco_vgnca, fn_vgnca, gnro, nvl, cbrtra_ps, cbrtra_no_ps, cbrtra_ps_cnd, cbrtra_ato_f)
		SELECT cupS.inco_vgnca, cupS.fn_vgnca,   sexo.dscrpcn_sxo, cupS.cnsctvo_cdgo_nvl_cmpljdd,
				(CASE cupS.no_pos WHEN @cnst_n THEN @cnst_s WHEN @cnst_s THEN @cnst_n END) cbrtra_Ps,
				cupS.no_pos cbrtra_No_Ps, @cnst_n cbrtra_Ps_Cnd, @cnst_n cbrtra_AtF 
		FROM bdSisalud.dbo.tbcupsservicios as cupS  WITH (NOLOCK) 
		INNER JOIN  bdSisalud.dbo.tbSexoSalud_Vigencias sexo  WITH (NOLOCK) on (cups.cnsctvo_cdgo_sxo = sexo.cnsctvo_cdgo_sxo)
		WHERE cupS.cnsctvo_prstcn =  @id_cnsctvo_prstcn
		and @fecha_actual BETWEEN sexo.inco_vgnca and sexo.fn_vgnca
		and @fecha_actual BETWEEN cupS.inco_vgnca and cupS.fn_vgnca

		IF exists(select cnsctvo_vgnca_cps_pln_crgo
				  from bdSisalud.dbo.tbCupsxPlanxCargo_Vigencias v  WITH (NOLOCK)
				  WHERE v.cnsctvo_cdgo_crgo = @cdgo_cgo 
				  and cnsctvo_cdfccn =   @id_cnsctvo_prstcn
				  and @fecha_actual BETWEEN v.inco_vgnca and v.fn_vgnca)
		BEGIN
			UPDATE #TablaTempRetornoDatoBasicoPrestacion
			SET cbrtra_ato_f = @cnst_s
		END		 
	END 
	ELSE 
		IF(@id_cnsctvo_tpo_prtcn = @cdgo_cums)
		BEGIN
			INSERT INTO #TablaTempRetornoDatoBasicoPrestacion (
				inco_vgnca, fn_vgnca, gnro, nvl, cbrtra_ps, cbrtra_no_ps, cbrtra_ps_cnd, cbrtra_ato_f)
			Select cums.inco_vgnca, cums.fn_vgnca, @cnst_NA, @cnst_NA,
				(CASE cums.indcdr_no_ps WHEN @cnst_n THEN @cnst_s WHEN @cnst_s THEN @cnst_n END) cbrtra_Ps,
				cums.indcdr_no_ps cbrtra_No_Ps, @cnst_n cbrtra_Ps_Cnd, @cnst_n cbrtra_AtF 
			FROM bdSisalud.dbo.tbcums as cums  WITH (NOLOCK)
			INNER JOIN bdSisalud.dbo.tbCums_vigencias as cumsv  WITH (NOLOCK) On cums.cnsctvo_cms = cumsv.cnsctvo_cms
			WHERE cums.cnsctvo_cms =  @id_cnsctvo_prstcn
			AND @fecha_actual BETWEEN cumsv.inco_vgnca and cumsv.fn_vgnca

		END 
		ELSE 
		IF(@id_cnsctvo_tpo_prtcn = @cdgo_cuos)
		BEGIN
			INSERT INTO #TablaTempRetornoDatoBasicoPrestacion (
				inco_vgnca, fn_vgnca, gnro, nvl, cbrtra_ps, cbrtra_no_ps, cbrtra_ps_cnd, cbrtra_ato_f)
			Select cous.inco_vgnca, cous.fn_vgnca,   @cnst_NA, @cnst_NA,
				(CASE cous.no_pos WHEN @cnst_n THEN @cnst_s WHEN @cnst_s THEN @cnst_n END) cbrtra_Ps,
				cous.no_pos    cbrtra_No_Ps, @cnst_n cbrtra_Ps_Cnd, @cnst_n cbrtra_AtF 
			FROM bdSisalud.dbo.tbPrestacionPis as cous  WITH (NOLOCK) --TODO verificar en produccion ya que no tiene llave primaria en desarrollo
			WHERE cous.cnsctvo_prstcn_pis =   @id_cnsctvo_prstcn
		END

	SELECT  inco_vgnca,
			fn_vgnca,				
			gnro,
			nvl,
			cbrtra_ps,
			cbrtra_no_ps,
			cbrtra_ps_cnd,
			cbrtra_ato_f
	FROM #TablaTempRetornoDatoBasicoPrestacion

	DROP TABLE #TablaTempRetornoDatoBasicoPrestacion
END
