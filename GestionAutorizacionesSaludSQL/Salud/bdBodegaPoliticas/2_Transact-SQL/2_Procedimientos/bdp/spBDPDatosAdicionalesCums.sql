USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPDatosAdicionalesCums') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPDatosAdicionalesCums AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPDatosAdicionalesCums
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar los datos adicionales de los cums
* Observaciones : <\O O\>    
* Parametros  :
				<\P id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/04/04 FC\>
*------------------------------------------------------------------------------------------------------------------------------------------ 
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jose Olmedo Soto Aguirre AM\>    
* Descripcion        : <\DM ajusta consulta por consecutivo y no por codigo. DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/26 FM\>   
*------------------------------------------------------------------------------------------------------------------------------------------ 
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza la optimizacion del procedimiento y adicionalmente se ajusta la consulta
                       DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/26 FM\>   
*------------------------------------------------------------------------------------------------------------------------------------------ 
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPDatosAdicionalesCums @id_cnsctvo_prstcn = 23070
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPDatosAdicionalesCums
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fecha_actual	datetime,
			@csctvo_cms		udtConsecutivo

	Create
	Table   #tmpDatosAdicionalesCums(cnsctvo_cms                    udtConsecutivo,
	                                 cnsctvo_prsntcn	            udtConsecutivo,
									 cnsctvo_cdgo_frma_entrga       udtConsecutivo,
									 cnsctvo_frma_frmctca           udtConsecutivo,
									 cnsctvo_cdgo_va_admnstrcn      udtConsecutivo, 
									 cnsctvo_cdgo_mdlo_lqdcn_cncpto udtConsecutivo,
	                                 cdgo_prsntcn	                udtCodigo     ,
                                     dscrpcn_prsntcn                udtDescripcion, 
									 cntdd_prsntcn                  Varchar(15)   ,
	                                 dscrpcn_frma_entrga            udtDescripcion,
	                                 obsrvcns_prsntcn               udtDescripcion,
									 cdgo_frma_frmctca              Char(4)       ,
						             dscrpcn_frma_frmctca           udtDescripcion,
	                                 cntdd_frccnda                  Int           ,
									 indccns_invma                  Varchar(300)  ,
									 cntrndccns_invma               Varchar(300)  ,
									 cdgo_va_admnstrcn	            udtCodigo     ,
                                     dscrpcn_va_admnstrcn           udtDescripcion,
									 cdgo_mdlo_lqdcn_cncpto	        udtCodigo     ,
                                     dscrpcn_mdlo_lqdcn_cncpto      udtDescripcion
	                               )	
	SET @fecha_actual = GETDATE()
	SET @csctvo_cms = 5

	Insert 
	Into      #tmpDatosAdicionalesCums(cnsctvo_cms         , cnsctvo_prsntcn	       , cnsctvo_cdgo_frma_entrga      ,
									   cnsctvo_frma_frmctca, cnsctvo_cdgo_va_admnstrcn, cnsctvo_cdgo_mdlo_lqdcn_cncpto,
									   cntdd_prsntcn       , obsrvcns_prsntcn         , cntdd_frccnda                 , 
									   indccns_invma       , cntrndccns_invma         
								      )
	Select     c.cnsctvo_cms         , c.cnsctvo_prsntcn	       , c.cnsctvo_cdgo_frma_entrga      ,
			   c.cnsctvo_frma_frmctca, c.cnsctvo_cdgo_va_admnstrcn , c.cnsctvo_cdgo_mdlo_lqdcn_cncpto,
		       c.cntdd_prsntcn       , c.obsrvcns_prsntcn          , c.cntdd_frccnda                 , 
	           c.indccns_invma       , c.cntrndccns_invma 
	From       bdSisalud.dbo.tbCums c With(NoLock)			
	Inner Join bdsisalud.dbo.tbcodificaciones co With(NoLock)	
	On         co.cnsctvo_cdfccn = c.cnsctvo_cms 
	Where      co.cnsctvo_cdgo_tpo_cdfccn = @csctvo_cms
	And        co.cnsctvo_cdfccn          = @id_cnsctvo_prstcn


	/*Se actuliza la presentacion del medicamento */
	Update     a
	Set        cdgo_prsntcn    = pv.cdgo_prsntcn   , 
	           dscrpcn_prsntcn = pv.dscrpcn_prsntcn
	From       #tmpDatosAdicionalesCums a
	Inner Join bdSisalud.dbo.tbPresentaciones_Vigencias pv With(NoLock) 
	On         pv.cnsctvo_cdgo_prsntcn = a.cnsctvo_prsntcn	
	Where      @fecha_actual Between pv.inco_vgnca And pv.fn_vgnca


	/*Se actuliza la descripcion de la forma de entrega del medicamento */
	Update     a
	Set        dscrpcn_frma_entrga = fev.dscrpcn_frma_entrga
	From       #tmpDatosAdicionalesCums a
	Inner Join bdSisalud.dbo.tbFormaEntrega_Vigencias fev With(NoLock)  
	On         fev.cnsctvo_cdgo_frma_entrga = a.cnsctvo_cdgo_frma_entrga
	Where      @fecha_actual Between fev.inco_vgnca  And fev.fn_vgnca


	/*Se actuliza la forma farmaceutica del medicamento */
	Update     a
	Set        cdgo_frma_frmctca    = ff.cdgo_frma_frmctca   ,   
		       dscrpcn_frma_frmctca = ff.dscrpcn_frma_frmctca
	From       #tmpDatosAdicionalesCums a
	Inner Join bdSisalud.dbo.tbFormaFarmaceutica_Vigencias ff With(NoLock)  
	On         ff.cnsctvo_cdgo_frma_frmctca = a.cnsctvo_frma_frmctca
	Where      @fecha_actual Between ff.inco_vgnca And ff.fn_vgnca


	/*Se actuliza la via de administraciondel medicamento */
	Update     a
	Set        cdgo_va_admnstrcn    = vav.cdgo_va_admnstrcn   , 
	           dscrpcn_va_admnstrcn = vav.dscrpcn_va_admnstrcn
	From       #tmpDatosAdicionalesCums a
	Inner Join bdsisalud.dbo.tbMNViasAdministracion_Vigencias vav WITH(NOLOCK) 
	On         vav.cnsctvo_cdgo_va_admnstrcn = a.cnsctvo_cdgo_va_admnstrcn
	Where      @fecha_actual Between vav.inco_vgnca And vav.fn_vgnca


	/*Se actuliza el modelo de liquidacion de conceptos*/
	Update     a
	Set        cdgo_mdlo_lqdcn_cncpto    = mlqc.cdgo_mdlo_lqdcn_cncpto, 
	           dscrpcn_mdlo_lqdcn_cncpto = mlqc.dscrpcn_mdlo_lqdcn_cncpto
	From       #tmpDatosAdicionalesCums a
	Inner Join bdSisalud.dbo.tbModeloLiquidacionxConceptos mlqc WITH(NOLOCK) 
	On         mlqc.cnsctvo_cdgo_mdlo_lqdcn_cncpto = a.cnsctvo_cdgo_mdlo_lqdcn_cncpto
	Where      @fecha_actual Between mlqc.inco_vgnca And mlqc.fn_vgnca


	Select cdgo_prsntcn          , dscrpcn_prsntcn          , cntdd_prsntcn       ,   
		   dscrpcn_frma_entrga   , obsrvcns_prsntcn         , cdgo_frma_frmctca   ,  
		   dscrpcn_frma_frmctca  , cntdd_frccnda            , indccns_invma       ,
		   cntrndccns_invma      , cdgo_va_admnstrcn        , dscrpcn_va_admnstrcn,
		   cdgo_mdlo_lqdcn_cncpto, dscrpcn_mdlo_lqdcn_cncpto
	From   #tmpDatosAdicionalesCums

	Drop Table #tmpDatosAdicionalesCums
END
GO
