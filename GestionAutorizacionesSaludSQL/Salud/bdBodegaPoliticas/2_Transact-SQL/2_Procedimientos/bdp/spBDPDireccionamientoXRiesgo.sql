USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPDireccionamientoXRiesgo]    Script Date: 28/04/2017 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPDireccionamientoXRiesgo
* Desarrollado por : José Soto 
* Descripcion  : Proceso encargado de consultar los direccionamientos por riesgo.
* Observaciones : <\O O\>    
* Parametros  :
				<\P	@cdgo_intrno 	P\>
				<\P	@cnsctvo_cdfccn	P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento cambiando el parametro de entrada al procedmiento y la 
							consulta. Para que tome el consecutivo de la ciudad en vez del codigo interno, consultando
							todos lo direccionamientos de la prestacion en una ciudade determinada
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/28 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
-- EXEC bdp.spBDPDireccionamientoXRiesgo 8112,	28775
-- EXEC bdp.spBDPDireccionamientoXRiesgo 8112, 115816 

ALTER PROCEDURE [bdp].[spBDPDireccionamientoXRiesgo]
	@cnsctvo_cdgo_cdd udtConsecutivo, 
	@cnsctvo_cdfccn   udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;	
	DECLARE @fcha_actl datetime;	
	
	SET @fcha_actl = GETDATE()

	Select     a.cdgo_intrno , b.nmbre_scrsl       , fv.dscrpcn_cdd         ,
		       ev.dscrpcn_pln, cv.cdgo_rsgo_dgnstco, cv.dscrpcn_rsgo_dgnstco,
		       a.edd_mnma    , a.edd_mxma          , a.sfcnca_ips           ,
		       a.nvl_prrdd   , a.inco_vgnca        , a.fn_vgnca
	From       bdCNA.sol.tbCNADireccionamientoPrestadorDestino a With(NoLock) 
	Inner Join bdSisalud.dbo.tbDireccionesPrestador b With(NoLock) 
	On         b.cdgo_intrno = a.cdgo_intrno
	Inner Join bdsisalud.dbo.tbMNRiesgosDiagnosticos_vigencias cv With(NoLock)
	On         cv.cnsctvo_cdgo_rsgo_dgnstco = a.cnsctvo_cdgo_rsgo_dgnstco
	Inner Join bdafiliacionvalidador.dbo.tbplanes_vigencias ev With(NoLock) 
	On         ev.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Inner Join bdafiliacionvalidador.dbo.tbciudades_vigencias fv With(NoLock) 
	On         fv.cnsctvo_cdgo_cdd = b.cnsctvo_cdgo_cdd 	
	Where      b.cnsctvo_cdgo_cdd = @cnsctvo_cdgo_cdd
	And        a.cnsctvo_cdfccn = @cnsctvo_cdfccn 
	And        @fcha_actl Between a.inco_vgnca  And a.fn_vgnca
	And        @fcha_actl Between b.inco_vgnca  And b.fn_vgnca
	And        @fcha_actl Between cv.inco_vgnca And cv.fn_vgnca
	And        @fcha_actl Between ev.inco_vgnca And ev.fn_vgnca
	And        @fcha_actl Between fv.inco_vgnca And fv.fn_vgnca
END
