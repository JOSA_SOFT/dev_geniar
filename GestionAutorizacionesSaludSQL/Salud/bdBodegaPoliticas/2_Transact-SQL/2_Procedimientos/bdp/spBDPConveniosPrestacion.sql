USE [bdBodegasPoliticas]
GO
/****** Object:  StoredProcedure [bdp].[spBDPConveniosPrestacion]    Script Date: 08/08/2017 11:38:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPConveniosPrestacion
* Desarrollado por : José Soto 
* Descripcion  : Proceso encargado de consultar los convenios de la prestacion
* Observaciones : <\O O\>    
* Parametros  : <\P @cnsctvo_cdgo_tpo_cdfccn P\>
				<\P @cnsctvo_cdgo_pln  		P\>
				<\P	@cnsctvo_cdfccn			P\>
				<\P	@consecutivoCiudad 		P\>
				<\P	@cdgo_intrno 	P\>

* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que realice la consulta del direccionamiento
							para los paquetes
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/08/08 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
--EXEC bdp.spBDPConveniosPrestacion 23520, 4, null, 1, null
ALTER PROCEDURE [bdp].[spBDPConveniosPrestacion]
	@cnsctvo_cdgo_tpo_cdfccn 		udtConsecutivo,
	@cnsctvo_cdgo_pln  				udtConsecutivo, 
	@cnsctvo_cdfccn					udtConsecutivo,
	@cnsctvo_cdgo_cdd 				udtConsecutivo,
	@cdgo_intrno 					udtCodigoIps
AS
BEGIN
	SET NOCOUNT ON;

	Declare @fcha_actl                  Datetime      ,
			@cnst_s                     udtLogico     ,
			@cnst_n                     udtLogico     ,
			@cnst_vco                   udtLogico     ,
			@cdgo_intrno_gnrco	        udtCodigoIPS  ,
			@cnsctvo_cdgo_tpo_mdlo_cums udtConsecutivo,
			@cnsctvo_cdgo_tpo_mdlo_cups udtConsecutivo,
			@cnsctvo_cdgo_tpo_mdlo_otro udtConsecutivo
	
	Create 
	Table #TablaTempConvenioPrestacion(ips_cdgo 			      udtConsecutivo,
		                               ips_dscrpcn 				  udtDescripcion,
		                               cdd_dsc 					  udtDescripcion,
		                               pln_dsc 					  udtDescripcion,
		                               vlr 						  Float         ,
		                               tpo_atncn 				  udtDescripcion,
		                               frmla_lqdcn 				  Varchar(max)  ,
		                               accso_drcto 				  udtLogico     ,
		                               drccn 					  Varchar(max)  ,
		                               tlfno 					  Varchar(max)  ,
		                               inco_vgnca 				  Datetime      ,
		                               fn_vgnca 				  Datetime      ,
		                               cnsctvo_cdgo_tpo_cdfccn 	  udtConsecutivo,
		                               cnsctvo_prstncn 			  udtConsecutivo,
		                               cnsctvo_mdlo_cnvno_pln 	  udtConsecutivo,
		                               cnsctvo_cdgo_cdd			  udtConsecutivo,
		                               cnsctvo_cdgo_pln			  udtConsecutivo,
		                               cdgo_intrno				  udtCodigoIps  ,
		                               cnsctvo_cdgo_frma_lqdcn    udtConsecutivo,
		                               cnsctvo_cdgo_clsfccn_atncn udtConsecutivo
	                                  )
	Create
	Table  #tbTmpModelos(id_mdlo               udtConsecutivo Identity(1,1),
	                     cnsctvo_cdgo_tpo_mdlo udtConsecutivo              ,
						 dscrpcn_mdlo          udtDescripcion
	                    )	

	Set @fcha_actl                  = getDate();
	Set @cnst_s                     = 'S'      ;
	Set @cnst_n                     = 'N'      ;
	Set @cnst_vco                   = ''       ;
	Set @cdgo_intrno_gnrco          = '5999'   ;
	Set @cnsctvo_cdgo_tpo_mdlo_cums = 5; --Modelo de medicamentos
	Set @cnsctvo_cdgo_tpo_mdlo_cups = 1; --Modelo de prestacion
	Set @cnsctvo_cdgo_tpo_mdlo_otro = 7; --Modelo de otros servicios

	--Se registran los modelos 
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_cums)
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_cups)
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_otro)
	
	Insert 
	Into   #TablaTempConvenioPrestacion (ips_cdgo                  , ips_dscrpcn           , vlr                    ,  
									     accso_drcto               , drccn                 , tlfno                  , 
										 inco_vgnca                , fn_vgnca              , cnsctvo_cdgo_tpo_cdfccn, 
										 cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln, cnsctvo_cdgo_cdd       , 
										 cnsctvo_cdgo_pln          , cdgo_intrno           , cnsctvo_cdgo_frma_lqdcn, 
										 cnsctvo_cdgo_clsfccn_atncn
										)
	Select      Distinct e.cdgo_intrno           , e.nmbre_scrsl           , a.vlr_trfdo               ,  
				         @cnst_vco               , e.drccn                 , e.tlfno                   , 
					     a.inco_vgnca            , a.fn_vgnca              , a.cnsctvo_cdgo_tpo_cdfccn , 
					     a.cnsctvo_prstncn       , d.cnsctvo_mdlo_cnvno_pln, e.cnsctvo_cdgo_cdd        , 
					     d.cnsctvo_cdgo_pln      , d.cdgo_intrno           , a.cnsctvo_cdgo_frma_lqdcn ,
					     a.cnsctvo_cdgo_tpo_atncn							
	From       bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan a With(NoLock) 
	Inner Join bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan c With(NoLock) 
	On         c.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco
	Inner Join bdSisalud.dbo.tbAsociacionModeloActividad d With(NoLock) 
	On         d.cnsctvo_asccn_mdlo_actvdd = c.cnsctvo_asccn_mdlo_actvdd
	Inner Join bdSisalud.dbo.tbdireccionesPrestador e With(NoLock) 
	On         e.cdgo_intrno = d.cdgo_intrno
	WHERE      a.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
	And        a.cnsctvo_prstncn         = @cnsctvo_cdfccn
	And        @fcha_actl Between d.inco_vgnca And d.fn_vgnca
	And        @fcha_actl Between c.inco_vgnca And c.fn_vgnca
	And        @fcha_actl Between a.inco_vgnca And a.fn_vgnca
	And        @fcha_actl Between e.inco_vgnca And e.fn_vgnca  
	
			
	--Direccionamiento Paquete
	Insert		
	Into        #TablaTempConvenioPrestacion(ips_cdgo                  , ips_dscrpcn           , vlr                    ,  
									         accso_drcto               , drccn                 , tlfno                  , 
										     inco_vgnca                , fn_vgnca              , cnsctvo_cdgo_tpo_cdfccn, 
										     cnsctvo_prstncn           , cnsctvo_mdlo_cnvno_pln, cnsctvo_cdgo_cdd       , 
										     cnsctvo_cdgo_pln          , cdgo_intrno           , cnsctvo_cdgo_frma_lqdcn, 
										     cnsctvo_cdgo_clsfccn_atncn
										    )
	Select      Distinct p.cdgo_intrno           , p.nmbre_scrsl           , d.vlr_trfdo              ,
	                     @cnst_vco               , p.drccn                 , p.tlfno                  ,
						 d.inco_vgnca            , d.fn_vgnca              , d.cnsctvo_cdgo_tpo_cdfccn,
						 d.cnsctvo_prstncn       , a.cnsctvo_mdlo_cnvno_pln, p.cnsctvo_cdgo_cdd       ,
						 a.cnsctvo_cdgo_pln      , a.cdgo_intrno           , d.cnsctvo_cdgo_frma_lqdcn,       
				         d.cnsctvo_cdgo_tpo_atncn
	From		bdContratacion.dbo.tbPaqueteDetalle b With(NoLock) 
	Inner Join	bdSisalud.dbo.tbDetTarifas c With(NoLock) 
	On          c.cnsctvo_rgstro = b.cnsctvo_pqte
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d With(NoLock)  
	On          d.cnsctvo_prstncn = c.cnsctvo_det_trfa 
	Inner Join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan l With(NoLock)
	On          l.cnsctvo_cdgo_lsta_prco = d.cnsctvo_cdgo_lsta_prco   
	Inner Join	bdSisalud.dbo.tbAsociacionModeloActividad a With(NoLock)	
	On          a.cnsctvo_asccn_mdlo_actvdd = l.cnsctvo_asccn_mdlo_actvdd	
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador p With(NoLock)
	On			p.cdgo_intrno = a.cdgo_intrno
	Inner Join  #tbTmpModelos m
	On          m.cnsctvo_cdgo_tpo_mdlo = a.cnsctvo_cdgo_tpo_mdlo
	Where		d.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
	And         b.cnsctvo_prstcn          = @cnsctvo_cdfccn
	And			p.cdgo_intrno            != @cdgo_intrno_gnrco	
	And			@fcha_actl BetWeen c.inco_vgnca And c.fn_vgnca
	And			@fcha_actl BetWeen d.inco_vgnca And d.fn_vgnca
	And			@fcha_actl BetWeen l.inco_vgnca And l.fn_vgnca
    And 		@fcha_actl BetWeen a.inco_vgnca And a.fn_vgnca 
	And 		@fcha_actl BetWeen p.inco_vgnca And p.fn_vgnca 
	
	
	If @cnsctvo_cdgo_cdd Is Not Null
	   Begin	
			Delete #TablaTempConvenioPrestacion 
			Where  cnsctvo_cdgo_cdd <> @cnsctvo_cdgo_cdd
	   End
	
	If @cnsctvo_cdgo_pln Is Not Null
	   Begin
		  Delete #TablaTempConvenioPrestacion 
	 	  Where  cnsctvo_cdgo_pln <> @cnsctvo_cdgo_pln
	   End

	If @cdgo_intrno Is Not Null
	   Begin
		  Delete #TablaTempConvenioPrestacion 
		  Where cdgo_intrno <> @cdgo_intrno
	   End

	Update     d
	Set        d.cdd_dsc =  cv.dscrpcn_cdd								
	From       #TablaTempConvenioPrestacion d
	Inner Join bdAfiliacionValidador.dbo.tbciudades_vigencias cv With(NoLock)	 
	On         cv.cnsctvo_cdgo_cdd = d.cnsctvo_cdgo_cdd
	Where      @fcha_actl BetWeen cv.inco_vgnca and cv.fn_vgnca

	Update     d
	Set        d.pln_dsc = pl_v.dscrpcn_pln									
	From       #TablaTempConvenioPrestacion d
	Inner Join bdAfiliacionValidador.dbo.tbPlanes_vigencias pl_v With(NoLock) 
	On         pl_v.cnsctvo_cdgo_pln = d.cnsctvo_cdgo_pln
	Where      @fcha_actl Between pl_v.inco_vgnca and pl_v.fn_vgnca	

	Update     d
	Set        d.tpo_atncn = i.dscrpcn_clsfccn_atncn							
	From       #TablaTempConvenioPrestacion d
	Inner Join bdSisalud.dbo.tbClasificacionAtencion_Vigencias i With(NoLock) 
	On         i.cnsctvo_cdgo_clsfccn_atncn = d.cnsctvo_cdgo_clsfccn_atncn	
	Where      @fcha_actl Between i.inco_vgnca And i.fn_vgnca
	
	Update     d
	Set        d.frmla_lqdcn =  j.dscrpcn_frma_lqdcn_prstcn								
	From       #TablaTempConvenioPrestacion d
	Inner Join bdSisalud.dbo.tbFormaLiquidacionPrestacion_Vigencias j With(NoLock)  
	On         j. cnsctvo_cdgo_frma_lqdcn_prstcn= d.cnsctvo_cdgo_frma_lqdcn		
	Where      @fcha_actl Between j.inco_vgnca And j.fn_vgnca
	

	Update     d
	Set        d.accso_drcto = l.accso_drcto									
	From       #TablaTempConvenioPrestacion d
	Inner Join bdContratacion.dbo.tbModeloConveniosPrestacionesxPlan k With(NoLock)  --Convenios de Prestaciones X plan
	On         k.cnsctvo_mdlo_cnvno_prstcn_pln = d.cnsctvo_mdlo_cnvno_pln
	Inner Join bdContratacion.dbo.tbDetModeloConveniosPrestaciones l With(NoLock)  -- Detalle de los Convenios de Prestaciones
	On         l.cnsctvo_cdgo_mdlo_cnvno_prstcn = k.cnsctvo_cdgo_mdlo_cnvno_prstcn
	Inner Join bdSisalud.dbo.tbDetEquivalencias de With(NoLock) --Las euivalencia
	On         de.cnsctvo_det_eqvlnca = l.cnsctvo_det_eqvlnca
	Inner Join bdSisalud.dbo.tbCodificaciones co With(NoLock) -- Las prestaciones 
	ON         co.cnsctvo_cdfccn          = de.cnsctvo_cdfccn_b       And 
	           co.cnsctvo_cdgo_tpo_cdfccn = d.cnsctvo_cdgo_tpo_cdfccn And 
			   co.cnsctvo_cdfccn          = d.cnsctvo_prstncn
	Where      @fcha_actl Between k.inco_vgnca  And k.fn_vgnca
	And        @fcha_actl Between l.inco_vgnca  And l.fn_vgnca		
	And        @fcha_actl Between de.inco_vgnca And de.fn_vgnca	
		
	Update #TablaTempConvenioPrestacion				  
	Set    accso_drcto = @cnst_n
	Where  accso_drcto <> @cnst_s Or accso_drcto Is Null
				   	 
	Select   ips_cdgo   , ips_dscrpcn, cdd_dsc  ,
		     pln_dsc    , vlr        , tpo_atncn,
		     frmla_lqdcn, accso_drcto, drccn    ,
		     tlfno      , inco_vgnca , fn_vgnca 	
	From     #TablaTempConvenioPrestacion a
    Where    @fcha_actl Between a.inco_vgnca And a.fn_vgnca
	Order By ips_dscrpcn
		
	Drop Table #TablaTempConvenioPrestacion
	Drop Table #tbTmpModelos
End
