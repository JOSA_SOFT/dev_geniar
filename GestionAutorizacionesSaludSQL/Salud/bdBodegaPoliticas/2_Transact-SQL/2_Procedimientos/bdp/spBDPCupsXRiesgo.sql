USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPCupsXRiesgo') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPCupsXRiesgo AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPCupsXRiesgo
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar los cups por riesgo
* Observaciones : <\O O\>    
* Parametros  :
				<\P id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/04/04 FC\>
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPCupsXRiesgo @id_cnsctvo_prstcn = 23070
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPCupsXRiesgo
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fecha_actual datetime;

	SET @fecha_actual = GETDATE()

	SELECT rd.cdgo_rsgo_dgnstco, rd.dscrpcn_rsgo_dgnstco
	FROM bdSisalud.dbo.tbMNRiesgosDiagnosticos rd WITH(NOLOCK)
	INNER JOIN bdSisalud.dbo.tbMNRiesgosDiagnosticos_Vigencias rdv WITH(NOLOCK) ON rd.cnsctvo_cdgo_rsgo_dgnstco = rdv.cnsctvo_cdgo_rsgo_dgnstco
	INNER JOIN bdCNA.sol.tbCNAPrestacionesxRiesgo_Vigencias prv WITH(NOLOCK) ON rd.cnsctvo_cdgo_rsgo_dgnstco = prv.cnsctvo_cdgo_rsgo_dgnstco
	WHERE @fecha_actual between prv.inco_vgnca and prv.fn_vgnca 
	AND @fecha_actual between  rdv.inco_vgnca and rdv.fn_vgnca 
	AND prv.cnsctvo_prstcn = @id_cnsctvo_prstcn 		 
END
GO
