USE [bdBodegasPoliticas]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPConsultaPrestacionPorPlanes') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPConsultaPrestacionPorPlanes AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPConsultaPrestacionPorPlanes
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar los datos de prestaciones por planes.  
* Observaciones : <\O O\>    
* Parametros  : <\P id_cnsctvo_tpo_prtcn P\>
				<\P id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Jose Olmedo Soto AM\>    
* Descripcion        : <\DM Ajuste en consulta para validacion de vigencias. DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/27 FM\>   
*-----------------------------------------------------------------------------------------------------------------------

* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPConsultaPrestacionPorPlanes @id_cnsctvo_tpo_prtcn = 05 , @id_cnsctvo_prstcn = 23070
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPConsultaPrestacionPorPlanes
	@id_cnsctvo_tpo_prtcn udtConsecutivo,
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE  @fecha_actual	datetime,
			 @cdgo_cups		INT,
			 @cdgo_cums		INT,
			 @cnst_n		char(1),
			 @spco			char(1),
			 @ds_vldz       numeric,
			 @cnsctvo_cdgo_cncpto_gsto udtConsecutivo

	--C�digos de tipos de prestaciones para determinar tabla a consultar.
	SET @cdgo_cups = 04;
	SET @cdgo_cums = 05;
	SET @cnst_n = null
	SET @spco = ' '
	SET @cnsctvo_cdgo_cncpto_gsto = 44

	SET @fecha_actual = GETDATE()
	
	Create Table #TablaTempRetornoPrestacionPlanes(
		cdgo_pln			varchar(200),
		cbrtra_pr_pln		char(1),
		smns_crnca			int,
		ctdd_mxma_prmtda	varchar(10),
		frcnca_entrga		int,
		tpe_mxm_pr_ordn		int ,
		vngcna_atrzcn_mdca	numeric,
		edd_mnma			int ,
		edd_mxma			int,
		inco_vgnca			datetime,
		fn_vgnca			datetime
	)
	
	IF (@id_cnsctvo_tpo_prtcn = @cdgo_cups)
	BEGIN
		INSERT INTO #TablaTempRetornoPrestacionPlanes (
			cdgo_pln, cbrtra_pr_pln, smns_crnca, ctdd_mxma_prmtda, frcnca_entrga, tpe_mxm_pr_ordn,
			vngcna_atrzcn_mdca, edd_mnma, edd_mxma, inco_vgnca, fn_vgnca)
		 SELECT 
			planes_v.dscrpcn_pln cdgo_pln, csxp.cbrtra cbrtra_pr_pln, csxp.smns_crnca	smns_crnca,
			concat(csxp.cntdd_mxma_prmtda ,@spco,csxp.mdda_cntdd)	ctdd_mxma_prmtda, csxp.frcnca_entrga frcnca_entrga,
			csxp.tpe_mxmo_ordn	tpe_mxm_pr_ordn, csxp.ctdd_ds_mxmo vngcna_atrzcn_mdca,		
			csxp.edd_mnma edd_mnma, csxp.edd_mxma edd_mxma, csxp.inco_vgnca	inco_vgnca, csxp.fn_vgnca	fn_vgnca     
		FROM bdSisalud.dbo.tbCupsServiciosxPlanes csxp with(nolock)
		INNER JOIN  BDAfiliacionValidador.dbo.tbPlanes_vigencias planes_v  WITH(NOLOCK) on(csxp.cnsctvo_cdgo_pln = planes_v.cnsctvo_cdgo_pln)
		where csxp.cnsctvo_prstcn  =   @id_cnsctvo_prstcn
		AND @fecha_actual BETWEEN csxp.inco_vgnca AND csxp.fn_vgnca	 
		AND @fecha_actual BETWEEN planes_v.inco_vgnca AND planes_v.fn_vgnca	 
	END 
	ELSE 
	IF(@id_cnsctvo_tpo_prtcn = @cdgo_cums)
	BEGIN
		INSERT INTO #TablaTempRetornoPrestacionPlanes (
			cdgo_pln, cbrtra_pr_pln, smns_crnca, ctdd_mxma_prmtda, frcnca_entrga, tpe_mxm_pr_ordn,
			 edd_mnma, edd_mxma, inco_vgnca, fn_vgnca)
		select planes_v.dscrpcn_pln cdgo_pln, csxp.cbrtra	cbrtra_pr_pln, csxp.smns_crnca	smns_crnca,
			concat(csxp.cntdd_mxma ,@spco,  csxp.mdda_cntdd)	ctdd_mxma_prmtda, @cnst_n	frcnca_entrga,
			@cnst_n	tpe_mxm_pr_ordn, @cnst_n	edd_mnma, @cnst_n	edd_mxma,
			csxp.inco_vgnca	inco_vgnca, csxp.fn_vgnca	fn_vgnca     
		FROM bdSisalud.dbo.tbCumsxPlanes csxp with(nolock)
		INNER JOIN  BDAfiliacionValidador.dbo.tbPlanes_vigencias planes_v  WITH(NOLOCK) on(csxp.cnsctvo_cdgo_pln = planes_v.cnsctvo_cdgo_pln)
		where csxp.cnsctvo_cdgo_cms = @id_cnsctvo_prstcn
		AND @fecha_actual BETWEEN planes_v.inco_vgnca AND planes_v.fn_vgnca
		AND @fecha_actual BETWEEN csxp.inco_vgnca AND csxp.fn_vgnca	

		select @ds_vldz = ds_vldz
		from bdsisalud.dbo.tbConceptosGasto_Vigencias  with(nolock)
		where  cnsctvo_cdgo_cncpto_gsto=@cnsctvo_cdgo_cncpto_gsto

		--El valor se replica para todos los registros.
		UPDATE #TablaTempRetornoPrestacionPlanes
		SET vngcna_atrzcn_mdca = @ds_vldz 

	END 

	SELECT  cdgo_pln,
			cbrtra_pr_pln,
			smns_crnca,
			ctdd_mxma_prmtda,
			frcnca_entrga,
			tpe_mxm_pr_ordn,
			vngcna_atrzcn_mdca,		
			edd_mnma,
			edd_mxma,
			inco_vgnca,
			fn_vgnca               
	FROM #TablaTempRetornoPrestacionPlanes

	DROP TABLE #TablaTempRetornoPrestacionPlanes
END
