USE bdBodegasPoliticas
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('bdp.spBDPPoliticaAuthPrestacion') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE bdp.spBDPPoliticaAuthPrestacion AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spBDPPoliticaAuthPrestacion
* Desarrollado por : Jos� Soto 
* Descripcion  : Proceso encargado de consultar las politicas de autorizacion de prestaciones
* Observaciones : <\O O\>    
* Parametros  : <\P id_cnsctvo_tpo_prtcn P\>
				<\P id_cnsctvo_prstcn P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/16 FC\>
* Ejemplo: 
    <\EJ
       EXEC bdp.spBDPPoliticaAuthPrestacion @id_cnsctvo_prstcn = 23070
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE bdp.spBDPPoliticaAuthPrestacion
	@id_cnsctvo_prstcn udtConsecutivo
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @fecha_actual datetime;
	
	CREATE TABLE #TablaTempRetornoAuthPresta(
		dsc_tpo_ath_prs udtDescripcion,
		dsc_agrpdr		udtDescripcion ,
		dsc_pltca		varchar(max)
	)

	SET @fecha_actual = GETDATE()

	INSERT INTO #TablaTempRetornoAuthPresta (dsc_tpo_ath_prs,dsc_agrpdr,dsc_pltca)
	SELECT	tppap.dscrpcn_tpo_pltca_atrzcn_prstcn,pap.dscrpcn_pltca_atrzcn_prstcn,papv.vlr_pltca
	FROM prm.tbBPPoliticasAutorizacionPrestaciones_Vigencias papv  WITH(NOLOCK)
	INNER JOIN prm.tbBPTiposPoliticasAutorizacionPrestaciones_vigencias tppap  WITH(NOLOCK) ON papv.cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn = tppap.cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn
	INNER JOIN prm.tbBPPoliticasAutorizacionPrestaciones pap  WITH(NOLOCK) ON papv.cnsctvo_cdgo_pltca_atrzcn_prstcn  = pap.cnsctvo_cdgo_pltca_atrzcn_prstcn 
	INNER JOIN prm.tbBPPoliticasAutorizacionPrestaciones_vigencias ppapv WITH(NOLOCK) ON papv.cnsctvo_cdgo_pltca_atrzcn_prstcn  = ppapv.cnsctvo_cdgo_pltca_atrzcn_prstcn
	INNER JOIN bdp.tbBPPoliticasAutorizacionPrestacionesxPrestacion  papp WITH(NOLOCK) ON pap.cnsctvo_cdgo_pltca_atrzcn_prstcn  = papp.cnsctvo_cdgo_pltca_atrzcn_prstcn
	WHERE papp. cnsctvo_cdfccn  =@id_cnsctvo_prstcn
	AND @fecha_actual BETWEEN papv.inco_vgnca AND papv.fn_vgnca
	AND @fecha_actual BETWEEN ppapv.inco_vgnca AND ppapv.fn_vgnca
	AND @fecha_actual BETWEEN tppap.inco_vgnca AND tppap.fn_vgnca
		 		 
		 
	SELECT  dsc_tpo_ath_prs, dsc_agrpdr, dsc_pltca              
	FROM #TablaTempRetornoAuthPresta

	DROP TABLE #TablaTempRetornoAuthPresta
END
GO
