USE  bdBodegasPoliticas
GO

CREATE TABLE prm.tbBPPoliticasAutorizacionPrestaciones(
  cnsctvo_cdgo_pltca_atrzcn_prstcn udtConsecutivo not null,
  cdgo_pltca_atrzcn_prstcn udtCodigo not null,
  dscrpcn_pltca_atrzcn_prstcn udtDescripcion not null,
  fcha_crcn datetime not null,
  usro_crcn udtUsuario not null,
  vsble_usro udtLogico not null
)
GO

CREATE TABLE bdp.tbBPPoliticasAutorizacionPrestacionesxPrestacion(
  cnsctvo_cdgo_pltca_atrzcn_prstcn_x_prstcn udtConsecutivo not null,
  cnsctvo_cdgo_pltca_atrzcn_prstcn udtConsecutivo  not null,
  cnsctvo_cdfccn udtConsecutivo not null,
  inco_vgnca datetime  not null,
  fn_vgnca datetime  not null,
  fcha_crcn datetime  not null,
  usro_crcn udtUsuario  not null
)
GO

CREATE TABLE prm.tbBPTiposPoliticasAutorizacionPrestaciones (
  cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn udtConsecutivo not null,
  cdgo_tpo_pltca_atrzcn_prstcn udtCodigo not null,
  dscrpcn_tpo_pltca_atrzcn_prstcn udtDescripcion not null,
  fcha_crcn datetime not null,
  usro_crcn udtUsuario not null,
  vsble_usro udtLogico not null
)
GO

CREATE TABLE prm.tbBPPoliticasAutorizacionPrestaciones_Vigencias (
	cnsctvo_cdgo_vgnca_pltca_atrzcn_prstcn udtConsecutivo not null,
	cnsctvo_cdgo_pltca_atrzcn_prstcn udtConsecutivo not null,
	cdgo_pltca_atrzcn_prstcn udtCodigo not null,
	dscrpcn_pltca_atrzcn_srvco udtDescripcion not null,
	inco_vgnca datetime not null,
	fn_vgnca datetime not null,
	fcha_crcn datetime not null,
	usro_crcn udtUsuario not null,
	obsrvcns udtObservacion not null,
	vsble_usro udtLogico not null,
	vlr_pltca varchar(max) not null,
	cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn udtConsecutivo not null,
	tmstmp timestamp
)
GO

CREATE TABLE prm.tbBPTiposPoliticasAutorizacionPrestaciones_Vigencias (
  cnsctvo_vgnca_cdgo_tpo_pltca_atrzcn_prstcn udtConsecutivo not null,
  cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn udtConsecutivo not null,
  cdgo_tpo_pltca_atrzcn_prstcn udtCodigo not null,
  dscrpcn_tpo_pltca_atrzcn_prstcn udtDescripcion not null,
  inco_vgnca datetime not null,
  fn_vgnca datetime not null,
  fcha_crcn datetime not null,
  usro_crcn udtUsuario not null,
  obsrvcns udtObservacion not null,
  vsble_usro udtLogico not null,
  tme_stmp timestamp not null
)
GO

ALTER TABLE prm.tbBPPoliticasAutorizacionPrestaciones ADD CONSTRAINT PK_tbBPPoliticasAutorizacionPrestaciones PRIMARY KEY (cnsctvo_cdgo_pltca_atrzcn_prstcn);
GO
ALTER TABLE bdp.tbBPPoliticasAutorizacionPrestacionesxPrestacion ADD CONSTRAINT PK_tbBPPoliticasAutorizacionPrestacionesxPrestacion PRIMARY KEY (cnsctvo_cdgo_pltca_atrzcn_prstcn_x_prstcn);
GO
ALTER TABLE prm.tbBPTiposPoliticasAutorizacionPrestaciones ADD CONSTRAINT PK_cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn PRIMARY KEY (cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn);
GO
ALTER TABLE prm.tbBPPoliticasAutorizacionPrestaciones_Vigencias ADD CONSTRAINT PK_cnsctvo_cdgo_vgnca_pltca_atrzcn_prstcn PRIMARY KEY (cnsctvo_cdgo_vgnca_pltca_atrzcn_prstcn);
GO
ALTER TABLE prm.tbBPTiposPoliticasAutorizacionPrestaciones_Vigencias ADD CONSTRAINT PK_tbBPTiposPoliticasAutorizacionPrestaciones_Vigencias PRIMARY KEY (cnsctvo_vgnca_cdgo_tpo_pltca_atrzcn_prstcn);
GO
ALTER TABLE bdp.tbBPPoliticasAutorizacionPrestacionesxPrestacion ADD FOREIGN KEY (cnsctvo_cdgo_pltca_atrzcn_prstcn) REFERENCES prm.tbBPPoliticasAutorizacionPrestaciones (cnsctvo_cdgo_pltca_atrzcn_prstcn);
GO
ALTER TABLE prm.tbBPPoliticasAutorizacionPrestaciones_Vigencias   ADD FOREIGN KEY (cnsctvo_cdgo_pltca_atrzcn_prstcn) REFERENCES prm.tbBPPoliticasAutorizacionPrestaciones(cnsctvo_cdgo_pltca_atrzcn_prstcn);
GO
ALTER TABLE prm.tbBPPoliticasAutorizacionPrestaciones_Vigencias  ADD FOREIGN KEY (cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn ) REFERENCES prm.tbBPTiposPoliticasAutorizacionPrestaciones (cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn );
GO
ALTER TABLE prm.tbBPTiposPoliticasAutorizacionPrestaciones_Vigencias ADD FOREIGN KEY (cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn) REFERENCES prm.tbBPTiposPoliticasAutorizacionPrestaciones(cnsctvo_cdgo_tpo_pltca_atrzcn_prstcn);
GO	