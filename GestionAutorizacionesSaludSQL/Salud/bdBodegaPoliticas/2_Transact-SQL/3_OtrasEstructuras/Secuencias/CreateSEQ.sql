USE [bdCNA]
GO

/****** Object:  Sequence [cja].[seqRCRecibosCaja]    Script Date: 22/03/2017 8:30:55 ******/
CREATE SEQUENCE [cja].[seqRCRecibosCaja] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 NO CACHE 
GO


/****** Object:  Sequence [cja].[seqRCNotasCredito]    Script Date: 22/03/2017 8:30:46 ******/
CREATE SEQUENCE [cja].[seqRCNotasCredito] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 NO CACHE 
GO

