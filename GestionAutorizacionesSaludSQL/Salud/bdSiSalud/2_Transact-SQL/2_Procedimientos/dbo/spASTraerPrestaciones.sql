USE [bdSisalud]
GO
/****** Object:  StoredProcedure [dbo].[spASTraerPrestaciones]    Script Date: 12/05/2017 10:40:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG     : spASTraerPrestaciones  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar por codigo o descripcion las prestaciones 
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2017/04/27 FC\>    
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                            Se modifica procedimiento para en la consulta de los medicamentos se adicione la tabla 
							tbMNATC_Vigencias
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/12 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/
--Exec [dbo].[spASTraerPrestaciones] '684000', NULL, 4
--Exec [dbo].[spASTraerPrestaciones] NULL, 'acet', 5

ALTER procedure [dbo].[spASTraerPrestaciones] 
	@lcCodigoServicio Char(11)       =	NULL,        
	@lcDescripcion	  udtDescripcion =	NULL,
	@lnTipoServicio   udtConsecutivo     
AS
Begin
	SET NOCOUNT ON;

	Declare @fecha_actual	          Datetime,
	        @cnsctvo_tpo_cdfccn_cups Int      ,
	        @cnsctvo_tpo_cdfccn_cums Int      ,
	        @cnsctvo_tpo_cdfccn_pis  Int      ,
			@lcVacio                 udtLogico,
			@lcPorcentaje            udtLogico,
			@separator               Char(3)

	Create 
	Table  #tempCodificaciones(cnsctvo_cdfccn	       udtConsecutivo, 
							   cdgo_cdfccn	           Char(11)      ,
							   dscrpcn_cdfccn	       Varchar(500),
							   cnsctvo_cdgo_tpo_cdfccn udtConsecutivo
							  )

	Set @cnsctvo_tpo_cdfccn_cups = 4        ;
	Set @cnsctvo_tpo_cdfccn_cums = 5        ;
	Set @cnsctvo_tpo_cdfccn_pis  = 9        ;
	Set @lcVacio                 = ''       ; 
	Set @lcPorcentaje            = '%'      ;
	Set @fecha_actual            = getDate();
	Set @separator               = ' - '

	/*Se consultan los procedimientos */
	If @lnTipoServicio = @cnsctvo_tpo_cdfccn_cups 
	   Begin
	      If @lcCodigoServicio != @lcVacio Or @lcCodigoServicio IS Not Null
		    Begin
			   Insert
			   Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 
			   Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, a.dscrpcn_cdfccn, a.cnsctvo_cdgo_tpo_cdfccn
			   From       tbCodificaciones a With(NoLock)
			   Inner Join tbCupsServicios b With(NoLock)
			   On         b.cnsctvo_prstcn = a.cnsctvo_cdfccn
			   Where      a.cdgo_cdfccn = @lcCodigoServicio
			   And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca
			End
		  Else
		    Begin
			   Insert
			   Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 			  
			   Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, a.dscrpcn_cdfccn, a.cnsctvo_cdgo_tpo_cdfccn
			   From       tbCodificaciones a With(NoLock)
			   Inner Join tbCupsServicios b With(NoLock)
			   On         b.cnsctvo_prstcn = a.cnsctvo_cdfccn
			   Where      a.dscrpcn_cdfccn Like @lcPorcentaje + @lcDescripcion + @lcPorcentaje
			   And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca
			End
	   End
   
    /*Se consultan los medicamentos */
    If @lnTipoServicio = @cnsctvo_tpo_cdfccn_cums 
       Begin
	     If @lcCodigoServicio != @lcVacio Or @lcCodigoServicio IS Not Null
            Begin
              Insert
              Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 
              Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, a.dscrpcn_cdfccn, a.cnsctvo_cdgo_tpo_cdfccn
              From       tbCodificaciones a With(NoLock)
              Inner Join tbCums b With(NoLock)
              On         b.cnsctvo_cms = a.cnsctvo_cdfccn
              Where      a.cdgo_cdfccn = @lcCodigoServicio
              And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca		     
            End
         Else
           Begin
              Insert
              Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 
              Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, CONCAT(a.dscrpcn_cdfccn, @separator, Upper(c.dscrpcn_atc)) , a.cnsctvo_cdgo_tpo_cdfccn
              From       tbCodificaciones a With(NoLock)
              Inner Join tbCums b With(NoLock)
              On         b.cnsctvo_cms = a.cnsctvo_cdfccn
			  Inner Join tbMNATC_Vigencias  c With(NoLock)
			  On         c.cnsctvo_cdgo_atc = b.cnsctvo_atc
              Where      (a.dscrpcn_cdfccn Like @lcPorcentaje + @lcDescripcion + @lcPorcentaje OR  c.dscrpcn_atc Like @lcPorcentaje + @lcDescripcion + @lcPorcentaje)
              And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca
			  And        @fecha_actual Between c.inco_vgnca And c.fn_vgnca				  
		   End
	   End

   /*Se consultan los insumos */
   If @lnTipoServicio = @cnsctvo_tpo_cdfccn_pis 
      Begin
	     If @lcCodigoServicio != @lcVacio Or @lcCodigoServicio IS Not Null
            Begin
              Insert
              Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 
              Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, a.dscrpcn_cdfccn, a.cnsctvo_cdgo_tpo_cdfccn
              From       tbCodificaciones a With(NoLock)
              Inner Join tbPrestacionPis b With(NoLock)
              On         b.cnsctvo_prstcn_pis = a.cnsctvo_cdfccn
              Where      a.cdgo_cdfccn = @lcCodigoServicio
              And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca		
            End
         Else
            Begin
			   Insert
               Into       #tempCodificaciones(cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn, cnsctvo_cdgo_tpo_cdfccn) 
               Select     a.cnsctvo_cdfccn, a.cdgo_cdfccn, a.dscrpcn_cdfccn, a.cnsctvo_cdgo_tpo_cdfccn
               From       tbCodificaciones a With(NoLock)
               Inner Join tbPrestacionPis b With(NoLock)
               On         b.cnsctvo_prstcn_pis = a.cnsctvo_cdfccn
               Where      a.dscrpcn_cdfccn Like @lcPorcentaje + @lcDescripcion + @lcPorcentaje
               And        @fecha_actual Between b.inco_vgnca And b.fn_vgnca		
            End             
	  End

	Select   cnsctvo_cdfccn, cdgo_cdfccn, dscrpcn_cdfccn
	From     #tempCodificaciones
	Order By dscrpcn_cdfccn

	Drop Table #tempCodificaciones
End