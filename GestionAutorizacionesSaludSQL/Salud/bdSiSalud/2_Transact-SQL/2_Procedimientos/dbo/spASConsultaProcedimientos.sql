USE [bdSisalud]
GO
/****** Object:  StoredProcedure [dbo].[spASConsultaProcedimientos]    Script Date: 19/05/2017 14:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------------------------------------------------- 
* Metodo o PRG     : spConsultaProcedimientos  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar por codigo o descripcion los procedimientos 
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2014/12/09 FC\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Adiciona campo no_pos DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/01/18  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Adiciona en el resultado de la consulta el campo: cnsctvo_cdgo_itm_prspsto DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/01/20  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Adiciona en el resultado de la consulta los campos: cnsctvo_cdfccn, estnca, ltrldd DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/02/16  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta el procedimiento para que retorne informacion de los procedimientos independientemente 
							de que esten convenidos con el prestador que es consultado
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/12/14  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta el procedimiento para que retorne informacion de los procedimientos independientemente 
							de que esten convenidos con el prestador que es consultado
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/12/14  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta el procedimiento para que marca el resultaddo de la estancia siempre como S
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/03/16  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\DM 
                            Se agrega insert a la tabla temporal para que al momento de sacar las prestaciones por homologacion, 
							obtenga tambien las prestaciones no homologadas.
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/03/27  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se modifica el procedimiento adicionando en la busqueda el tipo de codificacion 4 (Servicios)
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/19  FM\>   
*-----------------------------------------------------------------------------------------------------------------------------------*/  

/* 
 exec dbo.spASConsultaProcedimientos null, null, null, '2016-12-14', null, null, null, null, '684000', null, null
 exec dbo.spASConsultaProcedimientos null, null, null, '2016-12-14', null, null, null, null, null, null, 'TUMOR'
 exec dbo.spASConsultaProcedimientos null, null, null, getdate(), null, null, null, null, null, null, 'HABITACION'
 exec dbo.spASConsultaProcedimientos null, null, '760010003704', '20161015', null, null, null, null, null, null,'implan'
 exec dbo.spASConsultaProcedimientos null, null, '768340003732', '20161015', null, null, null, null, '890302-A18' , null,null
 exec dbo.spASConsultaProcedimientos null, null, '760010254101', '20161122', null, null, null, null, 534000, null,'null'
 exec dbo.spASConsultaProcedimientos null, null, '768340003732', '20161015', null, null, null, null, '881201' , null,null
 exec dbo.spASConsultaProcedimientos null, null, null, '20161015', null, null, null, null, '881201' , null,null
*/  

ALTER procedure  [dbo].[spASConsultaProcedimientos]     
    @lcTipoIdentPrestador    Char(3)                     ,
	@lcNumeroIdentPrestador  udtNumeroIdentificacionLargo, 
	@lcCodigoHabilitacion    Char(23)                    ,
	@ldFechaConsulta         Datetime                    ,
	@lcCodigoPlan            udtCodigo                   ,
	@lnTipoModelo            Int                         , -- Tipo de modelo  4: MODELO DE PROCEDIMIENTOS
	@lnConsecutivoPlan       udtConsecutivo              ,
	@lncodigoCobertura       udtCodigo                   ,
	@lcCodigoServicio        Char(11)       = NUll       , 
	@lcHomologacion          udtLogico      = NUll       ,
	@lcDescripcion           udtDescripcion = NUll    	
	  
AS  

SET NOCOUNT ON 
	
Begin

  Declare @ldFechaActual                        Datetime      ,
          @cnsctvo_cdgo_tpo_idntfccn            udtConsecutivo,
          @nmro_unco_idntfccn_prstdr            udtConsecutivo,
          @cdgo_intrno                          udtCodigoIps  ,
          @lbConvenio                           udtLogico     ,
          @lncnsctvo_cdgo_clsfccn_prstcn_prstdr udtConsecutivo,
		  @mrca_estnca							Int           ,
		  @cnsctvo_agrpdr_prstcn                udtConsecutivo,
		  @cnt_rgstrs                           Int           ,
		  @cnsctvo_cdgo_tpo_cdfccn	            udtConsecutivo

  Create 
  Table #tmpProcedimientos(codigoServicio			Char(11)             ,
                           descripcionServicio		udtDescripcion       ,
                           cdgo_cdfccn				Char(11)             ,
                           dscrpcn_cdfccn			udtDescripcion       , 
						   no_pos					udtCodigo            ,
						   cnsctvo_cdgo_itm_prspsto udtConsecutivo       ,
						   estnca					udtLogico DEFAULT 'S',
						   cnsctvo_cdfccn			udtconsecutivo       ,
						   ltrldd					udtLogico            ,
						   cnsctvo_agrpdr_prstcn    udtconsecutivo
                          )

  Set @ldFechaActual                        = getDate()
  Set @lncnsctvo_cdgo_clsfccn_prstcn_prstdr = 3
  Set @cnsctvo_agrpdr_prstcn                = 7
  Set @mrca_estnca                          = 8
  Set @cnsctvo_cdgo_tpo_cdfccn              = 4
  
  If @lcCodigoHabilitacion   Is Not Null Or 
     @lcNumeroIdentPrestador Is Not Null  -- condicion puesta por angela  para qe busque los homologos el 25-10-2016
		Set @lbConvenio  = 'S'
  Else 
  		Set @lbConvenio  = 'N'
		

  If @lbConvenio = 'S'
     Begin
	    /*Consulta el consecutivo tipo de identificacion*/
        Select @cnsctvo_cdgo_tpo_idntfccn = cnsctvo_cdgo_tpo_idntfccn
        From   bdAfiliacionValidador.dbo.tbtiposIdentificacion_vigencias With(NoLock)
        Where  cdgo_tpo_idntfccn = @lcTipoIdentPrestador
        And    @ldFechaActual Between inco_vgnca And fn_vgnca
                                 
        /*Consulta el NUI del prestador*/
        Select @nmro_unco_idntfccn_prstdr = nmro_unco_idntfccn_prstdr
        From   tbPrestadores With(NoLock) 
        Where  cnsctvo_cdgo_tpo_idntfccn = @cnsctvo_cdgo_tpo_idntfccn
        And    nmro_idntfccn             = @lcNumeroIdentPrestador
         
        /*Consulta el codigo interno*/
		-- actualizado por angela cuando no se ingresa todos los datos de la IPS igual se puede calcular el nui y se comentarea en el where, esta variable es unica, no estaba 25-10-2016
        Select @cdgo_intrno  = cdgo_intrno
        From   tbDireccionesPrestador With(NoLock)
        Where  cdgo_prstdr_mnstro        = @lcCodigoHabilitacion
        And    nmro_unco_idntfccn_prstdr = @nmro_unco_idntfccn_prstdr

		If @nmro_unco_idntfccn_prstdr IS Null
		  Begin 
		        -- actualizado por angela cuando no se ingresa todos los datos de la IPS igual se puede calcular el nui y se comentarea en el where, esta variable es unica, no estaba 25-10-2016
				Select @cdgo_intrno               = cdgo_intrno              , 
				       @nmro_unco_idntfccn_prstdr = nmro_unco_idntfccn_prstdr
				From   tbDireccionesPrestador With(NoLock)
				Where  cdgo_prstdr_mnstro  = @lcCodigoHabilitacion
		  End 

        If (@lcCodigoServicio != '' Or @lcCodigoServicio IS Not Null)
           Begin

			  Insert 
			  Into       #tmpProcedimientos(cdgo_cdfccn, dscrpcn_cdfccn          , codigoServicio, 
			                                no_pos     , cnsctvo_cdgo_itm_prspsto, cnsctvo_cdfccn, 
											ltrldd     , cnsctvo_agrpdr_prstcn
			                               )
              Select     c.cdgo_cdfccn, c.dscrpcn_cdfccn          , p1.cdgo_prstcn_prstdr, 
			             g.no_pos     , g.cnsctvo_cdgo_itm_prspsto, c.cnsctvo_cdfccn     , 
						 g.ltrldd     , g.cnsctvo_agrpdr_prstcn
              From       BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
              Inner Join BDIpsParametros.dbo.tbDetalle_PrestacionesPrestador h2 With(NoLock)
              On         h2.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr
              Inner Join BDIpsParametros.dbo.tbHomologacionPrestaciones h1 With(NoLock)
              On         h1.cdgo_intrno                = p1.cdgo_intrno  And 
                         h1.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr 
              Inner Join BDIpsParametros.dbo.tbDetalle_HomologacionPrestaciones h3 With(NoLock) 
              On         h3.cnsctvo_cdgo_hmlgcn_prstcn = h1.cnsctvo_cdgo_hmlgcn_prstcn 
              Inner Join tbCodificaciones c With(NoLock) 
              On         c.cnsctvo_cdfccn = h1.cnsctvo_cdfccn
              Inner Join tbcupsServicios g With(NoLock)
              On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn       
              Where 	 p1.cdgo_prstcn_prstdr                 = @lcCodigoServicio
              And        p1.cdgo_intrno                        = @cdgo_intrno
              And        h1.cnsctvo_cdgo_clsfccn_prstcn_prstdr = @lncnsctvo_cdgo_clsfccn_prstcn_prstdr 
			  And        c.cnsctvo_cdgo_tpo_cdfccn             = @cnsctvo_cdgo_tpo_cdfccn 		
              And        @ldFechaConsulta Between h2.inco_vgnca And h2.fn_vgnca
              And        @ldFechaConsulta Between h3.inco_vgnca And h3.fn_vgnca      

			  --actualiza la descripcion del servicio en la tabla temporal
			  Update      #tmpProcedimientos
			  Set         descripcionServicio = b.dscrpcn_cdfccn
			  From        #tmpProcedimientos a  
			  Inner Join  tbCodificaciones b With(NoLock) 
			  On          b.cnsctvo_cdfccn = a.cnsctvo_cdfccn
			 
			  --actualiza la descripcion del servicio en la tabla temporal 
			  Update      #tmpProcedimientos
			  Set         descripcionServicio = dscrpcn_cdfccn
			  From        #tmpProcedimientos a
			  Inner Join  BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
			  On          p1.cdgo_prstcn_prstdr = a.codigoServicio
			  Where       a.descripcionServicio Is Null

			  Select @cnt_rgstrs = Count(1) From #tmpProcedimientos
			  If @cnt_rgstrs = 0
			     Begin
				    Insert 
				    Into   #tmpProcedimientos(cdgo_cdfccn        , dscrpcn_cdfccn, codigoServicio          , 
					                          descripcionServicio, no_pos        , cnsctvo_cdgo_itm_prspsto, 
											  cnsctvo_cdfccn     , ltrldd        , cnsctvo_agrpdr_prstcn
                                              )
				    Select     c.cdgo_cdfccn                          , c.dscrpcn_cdfccn, c.cdgo_cdfccn As cdgo_prstcn_prstdr, 
					           c.dscrpcn_cdfccn As descripcionServicio, g.no_pos        , g.cnsctvo_cdgo_itm_prspsto         , 
							   c.cnsctvo_cdfccn                       , g.ltrldd        , g.cnsctvo_agrpdr_prstcn
				    From       tbCodificaciones c With(NoLock) 
				    Inner Join tbcupsServicios g With(NoLock)       
				    On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn   
				    Where      c.cdgo_cdfccn             = @lcCodigoServicio
					And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
				    And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	 
			     End
           End
        Else
           Begin

		      Insert 
			  Into       #tmpProcedimientos(cdgo_cdfccn, dscrpcn_cdfccn          , codigoServicio ,  
			                                no_pos     , cnsctvo_cdgo_itm_prspsto, cnsctvo_cdfccn, 
											ltrldd     , cnsctvo_agrpdr_prstcn
			                               )
	          Select     c.cdgo_cdfccn, c.dscrpcn_cdfccn          , p1.cdgo_prstcn_prstdr, 
			             g.no_pos     , g.cnsctvo_cdgo_itm_prspsto, c.cnsctvo_cdfccn     , 
						 g.ltrldd     , g.cnsctvo_agrpdr_prstcn
              From       BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
              Inner Join BDIpsParametros.dbo.tbDetalle_PrestacionesPrestador h2 With(NoLock)
              On         h2.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr
              Inner Join BDIpsParametros.dbo.tbHomologacionPrestaciones h1 With(NoLock)
              On         h1.cdgo_intrno                = p1.cdgo_intrno  And 
                         h1.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr 
              Inner Join BDIpsParametros.dbo.tbDetalle_HomologacionPrestaciones h3 With(NoLock) 
              On         h3.cnsctvo_cdgo_hmlgcn_prstcn = h1.cnsctvo_cdgo_hmlgcn_prstcn 
              Inner Join tbCodificaciones c With(NoLock) 
              On         c.cnsctvo_cdfccn = h1.cnsctvo_cdfccn
              Inner Join tbcupsServicios g With(NoLock)
              On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn       
              Where 	 p1.dscrpcn_prstcn_prstdr              Like '%' + @lcDescripcion + '%'
              And        p1.cdgo_intrno                        = @cdgo_intrno
              And        h1.cnsctvo_cdgo_clsfccn_prstcn_prstdr = @lncnsctvo_cdgo_clsfccn_prstcn_prstdr
			  And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
              And        @ldFechaConsulta Between h2.inco_vgnca And h2.fn_vgnca
              And        @ldFechaConsulta Between h3.inco_vgnca And h3.fn_vgnca
			  
			  Insert 
			  Into       #tmpProcedimientos(cdgo_cdfccn, dscrpcn_cdfccn          , codigoServicio ,  
			                                no_pos     , cnsctvo_cdgo_itm_prspsto, cnsctvo_cdfccn, 
											ltrldd     , cnsctvo_agrpdr_prstcn
			                               )
			  Select     c.cdgo_cdfccn          , c.dscrpcn_cdfccn         , c.cdgo_cdfccn As cdgo_prstcn_prstdr,
					     g.no_pos			   , g.cnsctvo_cdgo_itm_prspsto, c.cnsctvo_cdfccn,
						 g.ltrldd			   , g.cnsctvo_agrpdr_prstcn
              From       tbCodificaciones c With(NoLock)
              Inner Join tbcupsServicios g With(NoLock)
              On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn
			  Left Join	 #tmpProcedimientos p
			  On		 p.cnsctvo_cdfccn = c.cnsctvo_cdfccn
              Where      c.dscrpcn_cdfccn Like '%' + @lcDescripcion + '%'
			  And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn        
              And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca
			  And		 p.cnsctvo_cdfccn Is Null
			  
			  --actualiza la descripcion del servicio en la tabla temporal
			  Update      #tmpProcedimientos
			  Set         descripcionServicio = b.dscrpcn_cdfccn
			  From        #tmpProcedimientos a
			  Inner Join  tbCodificaciones b With(NoLock) 
			  On          b.cnsctvo_cdfccn = a.cnsctvo_cdfccn  

			   --actualiza la descripcion del servicio en la tabla temporal 
			  Update      #tmpProcedimientos
			  Set         descripcionServicio = dscrpcn_cdfccn
			  From        #tmpProcedimientos a
			  Inner Join  BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
			  On          p1.cdgo_prstcn_prstdr = a.codigoServicio
			  Where       a.descripcionServicio Is Null		
			  
			  Select @cnt_rgstrs = Count(1) From #tmpProcedimientos
			  If @cnt_rgstrs = 0
			    Begin
				    Insert 
                    Into   #tmpProcedimientos(cdgo_cdfccn        , dscrpcn_cdfccn, codigoServicio          , 
					                          descripcionServicio, no_pos        , cnsctvo_cdgo_itm_prspsto, 
											  cnsctvo_cdfccn     , ltrldd        , cnsctvo_agrpdr_prstcn
                                             )
                    Select     c.cdgo_cdfccn                          , c.dscrpcn_cdfccn, c.cdgo_cdfccn As cdgo_prstcn_prstdr, 
					           c.dscrpcn_cdfccn As descripcionServicio, g.no_pos        , g.cnsctvo_cdgo_itm_prspsto         , 
							   c.cnsctvo_cdfccn                       , g.ltrldd        , g.cnsctvo_agrpdr_prstcn
                    From       tbCodificaciones c With(NoLock) 
                    Inner Join tbcupsServicios g With(NoLock)       
                    On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn   
                    Where      c.dscrpcn_cdfccn Like '%' + @lcDescripcion + '%'
					And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn 
                    And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	
		        End		       
           End
     End
  Else
    Begin
	  If (@lcCodigoServicio != '' Or @lcCodigoServicio Is Not Null)
	    Begin
           Insert 
           Into   #tmpProcedimientos(cdgo_cdfccn        , dscrpcn_cdfccn, codigoServicio          , 
		                             descripcionServicio, no_pos        , cnsctvo_cdgo_itm_prspsto, 
									 cnsctvo_cdfccn     , ltrldd        , cnsctvo_agrpdr_prstcn
                                    )
	       Select     c.cdgo_cdfccn                          , c.dscrpcn_cdfccn, c.cdgo_cdfccn As cdgo_prstcn_prstdr, 
		              c.dscrpcn_cdfccn As descripcionServicio, g.no_pos        , g.cnsctvo_cdgo_itm_prspsto         ,
					  c.cnsctvo_cdfccn                       , g.ltrldd        , g.cnsctvo_agrpdr_prstcn
	       From       tbCodificaciones c With(NoLock) 
           Inner Join tbcupsServicios g With(NoLock)       
	       On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn   
	       Where      c.cdgo_cdfccn             = @lcCodigoServicio
		   And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn 
	       And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	 
        End
	  Else
	    Begin
		   Insert 
           Into   #tmpProcedimientos(cdgo_cdfccn        , dscrpcn_cdfccn, codigoServicio          , 
		                             descripcionServicio, no_pos        , cnsctvo_cdgo_itm_prspsto, 
									 cnsctvo_cdfccn     , ltrldd        , cnsctvo_agrpdr_prstcn
                                    )
	       Select     c.cdgo_cdfccn                          , c.dscrpcn_cdfccn, c.cdgo_cdfccn As cdgo_prstcn_prstdr, 
		              c.dscrpcn_cdfccn As descripcionServicio, g.no_pos        , g.cnsctvo_cdgo_itm_prspsto         , 
					  c.cnsctvo_cdfccn                       , g.ltrldd        , g.cnsctvo_agrpdr_prstcn
	       From       tbCodificaciones c With(NoLock) 
           Inner Join tbcupsServicios g With(NoLock)       
	       On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn   
	       Where      c.dscrpcn_cdfccn Like '%' + @lcDescripcion + '%'
		   And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn 
	       And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	
		End	   
    End

	Select cdgo_cdfccn        , dscrpcn_cdfccn, codigoServicio,
	       descripcionServicio, no_pos        , cnsctvo_cdgo_itm_prspsto, 
		   cnsctvo_cdfccn     , estnca        , ltrldd
	From   #tmpProcedimientos	

	Drop table #tmpProcedimientos
End
