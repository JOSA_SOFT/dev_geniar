USE [bdSisalud]
GO
/****** Object:  StoredProcedure [dbo].[spASConsultaMedicamentos]    Script Date: 19/05/2017 15:06:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spConsultaMedicamentos  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar por codigo o descripcion los medicamentos que esten o no 
						 convenidos						 					 
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2014/11/26 FC\>    
*    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                            Se adiciona al procedimiento la consulta del grupo terapeutico al que pertenece
                            los medicamentos enviados
                       DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2015/01/08  FM\>    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                            Se modifica el procedimiento de acuerdo a lo especificado en el control de cambio 
							CTC_FCC_002_HomologacionMedicamentosVitalesNoDisponiblesWSConsultaMedicamentos el cual 
							indica que para el caso en que no se encuentre el medicamento con el código consultado  
							se realice una segunda búsqueda en las tablas de homologación del prestador

							Se debe validar si el medicamento está marcado como VITAL NO DISPONIBLE
                       DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2015/01/08  FM\>    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Darling Liliana Dorado AM\>    
* Descripcion        : <\DM a la descripcion de la concentracion se adiciona la cantidad porque no se esta retornando DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2015/03/24  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Se requiere la descripcion de la concentracion y la cantidad en campos diferentes, se agregan	DM/>
*					   <\DM	campos en el resultado de la consulta: cntdd_cncntrcn y dscrpcn_cncntrcn_sn_cntdd tambien:		DM/>
*					   <\DM	cntdd_prsntcn y indcdr_no_ps
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2015/12/07  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Adiciona en el resultado de la consulta el campo: cnsctvo_cdgo_itm_prspsto DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/01/20  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Adiciona en el resultado de la consulta los campos: cnsctvo_cdfccn DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/02/16  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Luis Fernando Benavides AM\>    
* Descripcion        : <\DM Se adiciona condicion para que se realice la consulta de los servicios homologados DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/10/25  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta el procedimiento para que retorne informacion de los medicamentos independientemente de que esten
							convenidos con el prestador que es consultado
                       DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/12/19  FM\>   
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se modifica el procedimiento adicionando en la busqueda el tipo de codificacion 4 (Servicios)
                        DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/19  FM\>   
*----------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

/* 
 exec dbo.spASConsultaMedicamentos null, null, null, '2015-01-06', null, null, null, '00000VND-29', null, null
 exec dbo.spASConsultaMedicamentos null, null, null, '2015-01-06', null, null, null, null, null, 'PROGLYCEM'
 exec dbo.spASConsultaMedicamentos null, null, '760010003704', '20161015', null, 5, 1, null, null,'VACUNA'
 exec dbo.spASConsultaMedicamentos null, null, '768340003732', '20161015', null, 5, 1, '19991775-01', null, null
 exec dbo.spASConsultaMedicamentos 'NI', 890303208, '760010003711', '20161101', null, 5, 1, '19991775-01', null, null
*/  
ALTER procedure  [dbo].[spASConsultaMedicamentos]     
    @lcTipoIdentPrestador    Char(3)                     ,
	@lcNumeroIdentPrestador  udtNumeroIdentificacionLargo, 
	@lcCodigoHabilitacion    Char(23)                    ,
	@ldFechaConsulta         Datetime                    ,
	@lcCodigoPlan            udtCodigo                   ,
	@lnTipoModelo            Int                         , -- Tipo de modelo  5: MODELO DE MEDICAMENTOS
	@lnConsecutivoPlan       udtConsecutivo              ,
	@lcCodigoServicio        Char(11)       = NUll       , 
	@lcHomologacion          udtLogico      = NUll       ,
	@lcDescripcion           udtDescripcion = NUll    	
	  
AS  

SET NOCOUNT ON 
	
Begin
   Declare @ldFechaActual                 Datetime      ,
           @cnsctvo_cdgo_tpo_idntfccn     udtConsecutivo,
		   @nmro_unco_idntfccn_prstdr     udtConsecutivo,
		   @cdgo_intrno                   udtCodigoIps  , 
		   @liCantidad                    Int		    ,
		   @cnsctvo_cdgo_tpo_mrca_prstcn  udtConsecutivo,
		   @lbConvenio                    udtLogico     ,
		   @lcCodigoVitalNoDisponible     Char(3)       ,
		   @cnt_rgstrs                    Int           ,
		   @cnsctvo_cdgo_tpo_cdfccn       udtConsecutivo

   Create 
   Table #tmpMedicamentos(cdgo_cdfccn					Char(11)      ,       
                          dscrpcn_cdfccn				udtDescripcion,       
                          cnsctvo_cdgo_frma_frmctca	    udtConsecutivo,                                           
                          dscrpcn_frma_frmctca			udtDescripcion,                                           
                          cntdd_cncntrcn				Varchar(15)   ,                                    
                          cnsctvo_cdgo_cncntrcn		    udtConsecutivo,                                                        
                          dscrpcn_cncntrcn				udtDescripcion,                                            
                          rgstro_invma					Varchar(27)   ,                                       
                          dscrpcn_mdcmnto_gnrco		    udtDescripcion,
						  cdgo_sbgrpo_trptco            Char(3)       ,                                          
                          cnsctvo_cdgo_clse_frmcotrptca udtConsecutivo,                                          
                          dscrpcn_clse_frmcotrptca		udtDescripcion,                                          
                          indccns_invma				    Char(300)     ,                                      
                          cnsctvo_atc					udtConsecutivo,                                      
                          mltcmpnnte					Char(1)       ,                                    
                          fcha_vncmnto_invma			Datetime      ,                                
                          cdgo_atc						Char(8)       ,
                          cntrndccns_invma				Varchar(300)  ,
                          cmpnnte						Varchar(255)  ,
                          cmrcl						    Char(1)       ,
                          indcdr_no_ps					Char(1)       ,
                          cnsctvo_lbrtro				udtConsecutivo,
                          dscrpcn_lbrtro				udtDescripcion,
                          cnsctvo_cdfccn				udtConsecutivo,
                          cnsctvo_prsntcn               udtConsecutivo,
                          cdgo_prsntcn	                udtCodigo     ,
                          dscrpcn_prsntcn               udtDescripcion,
                          cdgo_cncntrcn	                Char(4)       ,
                          cdgo_frma_frmctca             Char(4)       ,
                          cnsctvo_cdgo_va_admnstrcn     udtConsecutivo,
                          cdgo_va_admnstrcn	            udtCodigo     ,
                          dscrpcn_va_admnstrcn	        udtDescripcion,						  
						  vitalNoDisponible             udtLogico Default 'N',
						  dscrpcn_cncntrcn_sn_cntdd		udtDescripcion,
						  cntdd_prsntcn					Varchar(15),
						  cnsctvo_cdgo_itm_prspsto		udtConsecutivo
                         )                                    

   Set @ldFechaActual                = getDate()
   Set @cnsctvo_cdgo_tpo_mrca_prstcn = 5
   Set @lcCodigoVitalNoDisponible    = 'vnd'
   Set @cnsctvo_cdgo_tpo_cdfccn      = 5

   If @lcCodigoHabilitacion   Is Not Null Or 
      @lcNumeroIdentPrestador Is Not Null  -- condicion puesta para qe busque los homologos el 25-10-2016
		Set @lbConvenio  = 'S'
  Else 
  		Set @lbConvenio  = 'N'
		   
   If @lbConvenio = 'N'
      Begin
		   If (@lcCodigoServicio != '' Or @lcCodigoServicio Is Not Null)
			 Begin
				Insert 
				Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
									   cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
									   fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
									   dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
									   cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
									   cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
									   cntdd_prsntcn			, cnsctvo_cdgo_itm_prspsto
									  )	
				Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn           , g.cnsctvo_cncntrcn         ,
						   g.cnsctvo_atc         , g.rgstro_invma            , g.mltcmpnnte               ,
						   g.fcha_vncmnto_invma  , g.indccns_invma           , f.cdgo_cdfccn              , 
						   f.dscrpcn_cdfccn      , g.cntrndccns_invma        , g.cmpnnte                  ,
						   g.cmrcl               , g.indcdr_no_ps            , g.cnsctvo_lbrtro           ,	
						   f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn         , g.cnsctvo_cdgo_va_admnstrcn,
						   g.cntdd_prsntcn		  , g.cnsctvo_cdgo_itm_prspsto
				From       tbcodificaciones f With(NoLock)
				Inner Join tbcums g With(NoLock)
				On         g.cnsctvo_cms = f.cnsctvo_cdfccn
				Where      f.cdgo_cdfccn             = @lcCodigoServicio
				And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
				And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca				
			 End
		   Else
			 Begin
			   Insert 
			   Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
									  cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
									  fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
									  dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
									  cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
									  cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
									  cntdd_prsntcn			   , cnsctvo_cdgo_itm_prspsto
									 )	
			   Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn  , g.cnsctvo_cncntrcn         ,
					      g.cnsctvo_atc         , g.rgstro_invma    , g.mltcmpnnte               ,
					      g.fcha_vncmnto_invma  , g.indccns_invma   , f.cdgo_cdfccn              , 
					      f.dscrpcn_cdfccn      , g.cntrndccns_invma, g.cmpnnte                  ,
					      g.cmrcl               , g.indcdr_no_ps    , g.cnsctvo_lbrtro           ,	
					      f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn , g.cnsctvo_cdgo_va_admnstrcn,
					      g.cntdd_prsntcn       , g.cnsctvo_cdgo_itm_prspsto
			   From       tbcodificaciones f With(NoLock)
			   Inner Join tbcums g With(NoLock)
			   On         g.cnsctvo_cms = f.cnsctvo_cdfccn
			   Where      g.dscrpcn_cms    Like  '%' + @lcDescripcion + '%'
			   And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
			   And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca			   
			 End	
		End     
   Else    
     Begin
         /*Consulta el consecutivo tipo de identificacion*/
         Select @cnsctvo_cdgo_tpo_idntfccn = cnsctvo_cdgo_tpo_idntfccn
         From   bdAfiliacionValidador.dbo.tbtiposIdentificacion_vigencias With(NoLock)
         Where  cdgo_tpo_idntfccn = @lcTipoIdentPrestador
         And    @ldFechaActual Between inco_vgnca And fn_vgnca
                                 
         /*Consulta el NUI del prestador*/
         Select @nmro_unco_idntfccn_prstdr = nmro_unco_idntfccn_prstdr
         From   tbPrestadores With(NoLock)
         Where  cnsctvo_cdgo_tpo_idntfccn = @cnsctvo_cdgo_tpo_idntfccn
         And    nmro_idntfccn             = @lcNumeroIdentPrestador
         
         /*Consulta el codigo interno*/
         Select @cdgo_intrno  = cdgo_intrno
         From   tbDireccionesPrestador With(NoLock)
         Where  cdgo_prstdr_mnstro        = @lcCodigoHabilitacion
         And    nmro_unco_idntfccn_prstdr = @nmro_unco_idntfccn_prstdr
		 
		 If @nmro_unco_idntfccn_prstdr IS Null
		  Begin 
		        /*actualizado por cuando no se ingresa todos los datos de la IPS igual se puede calcular el nui y 
				  se comentarea en el where, esta variable es unica, no estaba 25-10-2016
				*/
				Select @cdgo_intrno               = cdgo_intrno              , 
				       @nmro_unco_idntfccn_prstdr = nmro_unco_idntfccn_prstdr
				From   tbDireccionesPrestador With(NoLock)
				Where  cdgo_prstdr_mnstro  = @lcCodigoHabilitacion
		  End 

         If (@lcCodigoServicio != '' Or @lcCodigoServicio IS Not Null)
            Begin			   
               Insert 
               Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
                                      cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
                                      fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
                                      dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
                                      cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
                                      cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
									  cntdd_prsntcn			   , cnsctvo_cdgo_itm_prspsto
                                     )	
               Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn          , g.cnsctvo_cncntrcn         ,
                          g.cnsctvo_atc         , g.rgstro_invma            , g.mltcmpnnte               ,
                          g.fcha_vncmnto_invma  , g.indccns_invma           , f.cdgo_cdfccn              , 
                          f.dscrpcn_cdfccn      , g.cntrndccns_invma        , g.cmpnnte                  ,
                          g.cmrcl               , g.indcdr_no_ps            , g.cnsctvo_lbrtro           ,	
                          f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn         , g.cnsctvo_cdgo_va_admnstrcn,
					      g.cntdd_prsntcn       , g.cnsctvo_cdgo_itm_prspsto
               From       tbAsociacionModeloActividad b        With(NoLock)
               Inner Join bdContratacion.dbo.tbModeloConveniosMedicamentos c With(NoLock)
               On         c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = b.cnsctvo_mdlo_cnvno_pln
               Inner Join bdContratacion.dbo.tbDetModeloConveniosMedicamentos d	With(NoLock)
               On         d.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto
               Inner Join tbtipomodelo e With(NoLock)													
               On         e.cnsctvo_cdgo_tpo_mdlo = b.cnsctvo_cdgo_tpo_mdlo
               Inner Join tbcodificaciones f With(NoLock)
               On         f.cnsctvo_cdfccn = d.cnsctvo_cdgo_mdcmnto
               Inner Join tbcums g With(NoLock)
               On         g.cnsctvo_cms = f.cnsctvo_cdfccn
               Where      f.cdgo_cdfccn             = @lcCodigoServicio
	           And        b.cdgo_intrno             = @cdgo_intrno
               And        b.cnsctvo_cdgo_tpo_mdlo   = @lnTipoModelo -- Tipo de modelo  5: MODELO DE MEDICAMENTOS
               And        b.cnsctvo_cdgo_pln        = @lnConsecutivoPlan
			   And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
               And        @ldFechaConsulta Between b.inco_vgnca And b.fn_vgnca 
               And        @ldFechaConsulta Between c.inco_vgnca And c.fn_vgnca
               And        @ldFechaConsulta Between d.inco_vgnca And d.fn_vgnca

			   Select @cnt_rgstrs = Count(1) From #tmpMedicamentos
			   If @cnt_rgstrs = 0
			     Begin
				     Insert 
				     Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
		                                    cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
	                                        fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
	                                        dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
	                                        cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
	                                        cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
	                                        cntdd_prsntcn			, cnsctvo_cdgo_itm_prspsto
                                           )	
	                 Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn          , g.cnsctvo_cncntrcn         ,
	                            g.cnsctvo_atc         , g.rgstro_invma            , g.mltcmpnnte               ,
                                g.fcha_vncmnto_invma  , g.indccns_invma           , f.cdgo_cdfccn              , 
                                f.dscrpcn_cdfccn      , g.cntrndccns_invma        , g.cmpnnte                  ,
                                g.cmrcl               , g.indcdr_no_ps            , g.cnsctvo_lbrtro           ,	
                                f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn         , g.cnsctvo_cdgo_va_admnstrcn,
                                g.cntdd_prsntcn		  , g.cnsctvo_cdgo_itm_prspsto
                     From       tbcodificaciones f With(NoLock)
                     Inner Join tbcums g With(NoLock)
                     On         g.cnsctvo_cms = f.cnsctvo_cdfccn
                     Where      f.cdgo_cdfccn             = @lcCodigoServicio
					 And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
                     And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	
				 End
            End
         Else
           Begin
	         Insert 
             Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
                                    cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
                                    fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
                                    dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
                                    cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
                                    cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
									cntdd_prsntcn			 , cnsctvo_cdgo_itm_prspsto
                                   )	
             Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn          , g.cnsctvo_cncntrcn         ,
                        g.cnsctvo_atc         , g.rgstro_invma            , g.mltcmpnnte               ,
                        g.fcha_vncmnto_invma  , g.indccns_invma           , f.cdgo_cdfccn              , 
                        f.dscrpcn_cdfccn      , g.cntrndccns_invma        , g.cmpnnte                  ,
                        g.cmrcl               , g.indcdr_no_ps            , g.cnsctvo_lbrtro           ,	
                        f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn         , g.cnsctvo_cdgo_va_admnstrcn,
					    g.cntdd_prsntcn       , g.cnsctvo_cdgo_itm_prspsto
             From       tbAsociacionModeloActividad b        With(NoLock)
             Inner Join bdContratacion.dbo.tbModeloConveniosMedicamentos c With(NoLock)
             On         c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = b.cnsctvo_mdlo_cnvno_pln
             Inner Join bdContratacion.dbo.tbDetModeloConveniosMedicamentos d	With(NoLock)
             On         d.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto
             Inner Join tbtipomodelo e With(NoLock)													
             On         e.cnsctvo_cdgo_tpo_mdlo = b.cnsctvo_cdgo_tpo_mdlo
             Inner Join tbcodificaciones f With(NoLock)
             On         f.cnsctvo_cdfccn = d.cnsctvo_cdgo_mdcmnto
             Inner Join tbcums g With(NoLock)
             On         g.cnsctvo_cms = f.cnsctvo_cdfccn
             Where      g.dscrpcn_cms Like  '%' + @lcDescripcion + '%'
	         And        b.cdgo_intrno             = @cdgo_intrno
             And        b.cnsctvo_cdgo_tpo_mdlo   = @lnTipoModelo -- Tipo de modelo  5: MODELO DE MEDICAMENTOS
             And        b.cnsctvo_cdgo_pln        = @lnConsecutivoPlan
			 And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
             And        @ldFechaConsulta Between b.inco_vgnca And b.fn_vgnca 
             And        @ldFechaConsulta Between c.inco_vgnca And c.fn_vgnca
             And        @ldFechaConsulta Between d.inco_vgnca And d.fn_vgnca

			 Select @cnt_rgstrs = Count(1) From #tmpMedicamentos
			   If @cnt_rgstrs = 0
		         Begin
				    Insert 
			        Into  #tmpMedicamentos(cnsctvo_cdgo_frma_frmctca, cntdd_cncntrcn          , cnsctvo_cdgo_cncntrcn    ,
			                               cnsctvo_atc              , rgstro_invma            , mltcmpnnte               ,
			                               fcha_vncmnto_invma       , indccns_invma           , cdgo_cdfccn              ,			
		                                   dscrpcn_cdfccn           , cntrndccns_invma        , cmpnnte                  , 
                                           cmrcl                    , indcdr_no_ps            , cnsctvo_lbrtro           , 
			                               cnsctvo_cdfccn           , cnsctvo_prsntcn         , cnsctvo_cdgo_va_admnstrcn,
		                                   cntdd_prsntcn			   , cnsctvo_cdgo_itm_prspsto
		                                  )	
	                Select     g.cnsctvo_frma_frmctca, g.cntdd_cncntrcn  , g.cnsctvo_cncntrcn         ,
		                       g.cnsctvo_atc         , g.rgstro_invma    , g.mltcmpnnte               ,
	                           g.fcha_vncmnto_invma  , g.indccns_invma   , f.cdgo_cdfccn              , 
		                       f.dscrpcn_cdfccn      , g.cntrndccns_invma, g.cmpnnte                  ,
		                       g.cmrcl               , g.indcdr_no_ps    , g.cnsctvo_lbrtro           ,	
		                       f.cnsctvo_cdfccn      , g.cnsctvo_prsntcn , g.cnsctvo_cdgo_va_admnstrcn,
	                           g.cntdd_prsntcn       , g.cnsctvo_cdgo_itm_prspsto
			        From       tbcodificaciones f With(NoLock)
		            Inner Join tbcums g With(NoLock)
	                On         g.cnsctvo_cms = f.cnsctvo_cdfccn
	                Where      g.dscrpcn_cms Like  '%' + @lcDescripcion + '%'
					And        f.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn
			        And        @ldFechaConsulta Between g.inco_vgnca And g.fn_vgnca	
				 End
           End	   
	  End
                 
   /*Se actualiza el codigo y descripcion de la forma farmaceutica de los medicamentos */
   Update     #tmpMedicamentos 
   Set        cdgo_frma_frmctca    = b.cdgo_frma_frmctca   ,
              dscrpcn_frma_frmctca = b.dscrpcn_frma_frmctca                                                      
   From       #tmpMedicamentos a 
   Inner Join tbFormaFarmaceutica b With(NoLock)
   On         b.cnsctvo_cdgo_frma_frmctca = a.cnsctvo_cdgo_frma_frmctca 
                                      
   /*Se actualiza el codigo y descripcion de la concentracion de los medicamentos */                         
   Update     #tmpMedicamentos  
   Set        cdgo_cncntrcn		= b.cdgo_cncntrcn   ,
              dscrpcn_cncntrcn	= b.dscrpcn_cncntrcn                                                       
   From       #tmpMedicamentos a 
   Inner Join tbconcentracion_Vigencias b With(NoLock)
   On         b.cnsctvo_cdgo_cncntrcn = a.cnsctvo_cdgo_cncntrcn

   /*Se actualiza el codigo y descripcion de la presentacion de los medicamentos */   
   Update     #tmpMedicamentos  
   Set        cdgo_prsntcn    = b.cdgo_prsntcn   ,
              dscrpcn_prsntcn = b.dscrpcn_prsntcn                                                       
   From       #tmpMedicamentos a 
   Inner Join tbPresentaciones_vigencias b With(NoLock)
   On         b.cnsctvo_cdgo_prsntcn = a.cnsctvo_prsntcn

    /*Se actualiza el codigo y descripcion de la via de administracion de los medicamentos */   
   Update     #tmpMedicamentos  
   Set        cdgo_va_admnstrcn    = b.cdgo_va_admnstrcn   ,
              dscrpcn_va_admnstrcn = b.dscrpcn_va_admnstrcn                                                       
   From       #tmpMedicamentos a 
   Inner Join tbMNViasAdministracion_vigencias b With(NoLock)
   On         b.cnsctvo_cdgo_va_admnstrcn = a.cnsctvo_cdgo_va_admnstrcn


    /*Se actualiza la descripcion del grupo terapeutico*/
   Update     #tmpMedicamentos 
   Set        cdgo_sbgrpo_trptco            = c.cdgo_sbgrpo_trptco                 , -- Codigo grupo terapeutico
              dscrpcn_clse_frmcotrptca      = Rtrim(Ltrim(c.dscrpcn_sbgrpo_trptco)), -- Descripcion grupo terapeutico              
			  dscrpcn_mdcmnto_gnrco         = b.dscrpcn_atc                        ,                                     
              cnsctvo_cdgo_clse_frmcotrptca = a.cnsctvo_atc          			               
   From       #tmpMedicamentos a 
   Inner Join tbMNATC_Vigencias b With(NoLock)
   On         b.cnsctvo_cdgo_atc = a.cnsctvo_atc
   Inner Join tbSubgrupoQuimico_vigencias e With(NoLock)              
   On         e.cnsctvo_cdgo_sbgrpo_qmco = b.cnsctvo_cdgo_sbgrpo_qmco               
   Inner Join tbSubgrupoFarmacologico_Vigencias f With(NoLock)               
   On         f.cnsctvo_cdgo_sbgrpo_frmclgco = e.cnsctvo_cdgo_sbgrpo_frmclgco
   Inner Join tbSubgrupoTerapeutico_vigencias c With(NoLock)  
   On         c.cnsctvo_cdgo_sbgrpo_trptco = f.cnsctvo_cdgo_sbgrpo_trptco
   Where      @ldFechaActual Between e.inco_vgnca and e.fn_vgnca              
   And        @ldFechaActual Between b.inco_vgnca and b.fn_vgnca              
   And        @ldFechaActual Between c.inco_vgnca and c.fn_vgnca              
   And        @ldFechaActual Between f.inco_vgnca and f.fn_vgnca 


   Update     #tmpMedicamentos 
   Set        cdgo_atc= b.cdgo_atc                                                     
   From       #tmpMedicamentos a 
   Inner Join tbMNATC_Vigencias b With(NoLock)  
   On         b.cnsctvo_cdgo_atc = a.cnsctvo_atc   

   /*Se actualiza la descripcion del laboratorio de los medicamentos */   
   Update     #tmpMedicamentos 
   Set        dscrpcn_lbrtro  = b.dscrpcn_lbrtro
   From       #tmpMedicamentos a 
   Inner Join tblaboratorios b With(NoLock) 
   On         b.cnsctvo_cdgo_lbrtro = a.cnsctvo_lbrtro

   --07/12/2015 sislbr01 se requiere la descripcion de la concentracion sin la cantidad en otro campo
   Update     #tmpMedicamentos  Set  dscrpcn_cncntrcn_sn_cntdd = dscrpcn_cncntrcn 

   --24/03/2015 sisddm01 se adiciona a la descripcion de la concentracion la cantidad para que sea mostrada en pantalla
   Update     #tmpMedicamentos  Set  dscrpcn_cncntrcn = Substring(Ltrim(Rtrim(cntdd_cncntrcn))+' '+Ltrim(Rtrim(dscrpcn_cncntrcn)),0,150)

   Select  Distinct cdgo_cdfccn				, dscrpcn_cdfccn    , cdgo_cdfccn As CodMedPrestador, dscrpcn_cdfccn  As DescMedPrestador,
           rgstro_invma				, cdgo_prsntcn      , dscrpcn_prsntcn               , cdgo_cncntrcn                      ,
		   dscrpcn_cncntrcn			, cdgo_frma_frmctca , dscrpcn_frma_frmctca          , cdgo_va_admnstrcn                  , 
		   dscrpcn_va_admnstrcn		, cdgo_sbgrpo_trptco, dscrpcn_clse_frmcotrptca		, cntdd_cncntrcn					 ,
		   dscrpcn_cncntrcn_sn_cntdd, cntdd_prsntcn		, indcdr_no_ps					, cnsctvo_cdgo_itm_prspsto			 ,
		   cnsctvo_cdfccn
   From    #tmpMedicamentos					

   --24/03/2015 sisddm01 se adiciona borrado de la tabla
   drop table #tmpMedicamentos 

End
