USE [BDSisalud]
GO
/****** Object:  StoredProcedure [dbo].[spConsultarPrestadoresParaDireccionamiento]    Script Date: 9/1/2017 10:29:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spConsultarPrestadoresParaDireccionamiento
* Desarrollado por   : <\A Ing. Juan Carlos Vásquez G. A\>    
* Descripcion        : <\D Procedimiento Consultas de Prestadores Para Direccionamiento D\>
          				    
* Observaciones      : <\O      O\>    
* Parametros         : <\P   P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 2016/03/16  FC\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Luis Fernando Benavides  AM\>    
* Descripcion        : <\D  Adiciona resultado de consulta razon social, consecutivo tipo identificacion D\> 
*					   <\D  y numero de identificacion	D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/05/03 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez G  AM\>    
* Descripcion        : <\D  Adiciona consulta prestadores que no se encuentra direccionados D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/06/15 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez G  AM\>    
* Descripcion        : <\DM  
                            Adiciona filtro para excluir ips genérica	y concatenar nombre de la sucursal 
							y dirección  
					   DM\>
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/07/07 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Juan David Reina y Ing. Carlos Andrés López Ramírez  AM\>    
* Descripcion        : <\DM 
                            Se anexa consulta para obtener la sede de la ciudad y se modifican las 
							consultas de los prestadores para el direccionamiento de forma que estos 
							se obtengan a partir de la sede y no de la ciudad  
						DM\> 
* Nuevas Variables   : <\VM @cnsctvo_cdgo_sde VM\>    
* Fecha Modificacion : <\FM 2016/11/22 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que en el direccionamiento desde el 
							BPM se muestre los prestadores relacionados con las sedes de cali  
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/03/27 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que reciba como parametro el consecutivo de 
							la solicitud, con el fin de consultar en el detalle de la lista de precios 
							la forma de atencion
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/17 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento adicionando la validacion de la forma de atencion = 3
							con el fin de que muestre los direccionamientos correspondientes
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/04/17 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que consulte correctamente los convenios
							basado en el procedimiento de la bodega de mega
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/09 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que realice la consulta del direccionamiento
							de la sede cali, con convenio y paquetes
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/21 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\DM 
                            Se modifica sp para asignar a la variable @fechaActual el valor de la fecha sin hora.
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/09/01 FM\>    
*--------------------------------------------------------------------------------------------------------------------------------*/
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 88128, 1, 8112, 133354
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 113034, 8, 3543, 113034
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 118885, 1, 3543, 37996

ALTER PROCEDURE [dbo].[spConsultarPrestadoresParaDireccionamiento] 

/*01*/	@cnsctvo_cdfccn			    udtConsecutivo,
/*02*/	@cnsctvo_cdgo_pln		    udtConsecutivo,
/*03*/	@cnsctvo_cdgo_cdd	        udtConsecutivo,
/*04*/	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo

AS

Begin
	Set NoCount On

	Declare @fechaActual		         Datetime      ,
			@cdgo_intrno_gnrco	         udtCodigoIPS  ,
			@cnsctvo_cdgo_sde	         udtConsecutivo,
			@separador                   Char(3)       ,
			@lcVacio                     udtLogico     ,
			@cnsctvo_sde_cli             udtConsecutivo,
			@cnsctvo_cdgo_frma_atncn     udtConsecutivo,
			@cnsctvo_cdgo_tpo_mdlo_cums  udtConsecutivo,
			@cnsctvo_cdgo_tpo_mdlo_cups  udtConsecutivo,
			@cnsctvo_cdgo_tpo_mdlo_otro  udtConsecutivo,
			@cnsctvo_cdgo_frma_atncn_tds udtConsecutivo,
			@cnsctvo_sde_bgta            udtConsecutivo,
		    @cnsctvo_sde_mdlln           udtConsecutivo,
			@fcha_actl					 Datetime;
			 
	Create
	Table  #tbTmpModelos(id_mdlo               udtConsecutivo Identity(1,1),
	                     cnsctvo_cdgo_tpo_mdlo udtConsecutivo              ,
						 dscrpcn_mdlo          udtDescripcion
	                    )	
	
	Create 
	Table  #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn				udtConsecutivo              ,
										     cdgo_cdfccn				Char(11)                    ,
										     dscrpcn_cdfccn				udtDescripcion              ,
										     cdgo_intrno				udtCodigoIPS                ,		
										     nmbre_scrsl				Varchar(250)                ,
										     drccn						udtDireccion                ,
										     vlr_trfdo					Float                       ,
										     cnsctvo_cdgo_cdd			udtConsecutivo              ,
										     nmro_unco_idntfccn_prstdr	udtConsecutivo              ,
										     rzn_scl					Varchar(200)                ,
										     cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo              ,
										     cdgo_tpo_idntfccn			Char(3)                     ,
										     dscrpcn_tpo_idntfccn		udtDescripcion              ,
										     nmro_idntfccn				UdtNumeroIdentificacionLargo
	                                        )

	Set @fcha_actl					 = getDate();
	Set @separador                   = ' - '    ;
	Set @cdgo_intrno_gnrco           = '5999'   ;
	Set @fechaActual	             = Convert(Date, @fcha_actl);
	Set @lcVacio                     = ' '      ;
	Set @cnsctvo_sde_cli             = 2; --Sede Cali
	Set @cnsctvo_cdgo_tpo_mdlo_cums  = 5; --Modelo de medicamentos
	Set @cnsctvo_cdgo_tpo_mdlo_cups  = 1; --Modelo de prestacion
	Set @cnsctvo_cdgo_tpo_mdlo_otro  = 7; --Modelo de otros servicios
	Set @cnsctvo_cdgo_frma_atncn_tds = 3;
	Set @cnsctvo_sde_bgta            = 11; --sede Bogota
    Set @cnsctvo_sde_mdlln           = 17;  --sede Medellin

	--Se registran los modelos 
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_cums)
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_cups)
	Insert Into #tbTmpModelos(cnsctvo_cdgo_tpo_mdlo) Values (@cnsctvo_cdgo_tpo_mdlo_otro)
		
	Select @cnsctvo_cdgo_sde = cnsctvo_sde_inflnca 
	From   bdafiliacionvalidador.dbo.tbciudades_vigencias With(NoLock)	
	Where  cnsctvo_cdgo_cdd = @cnsctvo_cdgo_cdd
	And    @fechaactual Between inco_vgnca And fn_vgnca

	Select @cnsctvo_cdgo_frma_atncn = cnsctvo_cdgo_frma_atncn
	From   bdCNA.gsa.tbASSolicitudesAutorizacionServicios With(NoLock)
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

	--Direccionamiento MEGA
	Insert 
	Into        #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
				                                  vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
				                                  drccn
	                                             )
	Select 		Distinct d.cnsctvo_cdfccn, d.cdgo_intrno      , Concat(d1.nmbre_scrsl, @separador, d1.drccn),
				         d2.vlr_trfdo    , d1.cnsctvo_cdgo_cdd, d1.nmro_unco_idntfccn_prstdr         ,
				         d1.drccn
	From		bdCNA.sol.tbCNADireccionamientoPrestadorDestino d With(NoLock)
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador d1	With(NoLock)
	On			d1.cdgo_intrno = d.cdgo_intrno	
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d2 With(NoLock)	
	On			d2.cnsctvo_cdgo_dt_lsta_prco = d.cnsctvo_cdgo_dt_lsta_prco
	Where		d.cnsctvo_cdfccn          = @cnsctvo_cdfccn
	And			d.cnsctvo_cdgo_pln        = @cnsctvo_cdgo_pln
	And			d.cdgo_intrno            != @cdgo_intrno_gnrco
	And			d1.cnsctvo_cdgo_sde       = @cnsctvo_cdgo_sde 
	And         (
	              d2.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or
	              d2.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
			    )
	And			@fechaactual BetWeen d.inco_vgnca  And d.fn_vgnca
	And			@fechaactual BetWeen d1.inco_vgnca And d1.fn_vgnca
	And			@fechaactual BetWeen d2.inco_vgnca And d2.fn_vgnca
		
	--Direccionamiento Convenio
	Insert		
	Into       #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
												 vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
												 drccn
	                                            )
	Select     Distinct a.cnsctvo_prstncn, d.cdgo_intrno     , Concat(e.nmbre_scrsl, @separador, e.drccn),
                        a.vlr_trfdo      , e.cnsctvo_cdgo_cdd, e.nmro_unco_idntfccn_prstdr               ,
                        e.drccn      					
	From       bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan a With(NoLock) 
	Inner Join bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan c With(NoLock) 
	On         c.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco 
	Inner Join bdSisalud.dbo.tbAsociacionModeloActividad d With(NoLock) 
	On         d.cnsctvo_asccn_mdlo_actvdd = c.cnsctvo_asccn_mdlo_actvdd
	Inner Join bdSisalud.dbo.tbdireccionesPrestador e With(NoLock) 
	On         e.cdgo_intrno = d.cdgo_intrno	
	Inner Join #tbTmpModelos m
	On         m.cnsctvo_cdgo_tpo_mdlo = d.cnsctvo_cdgo_tpo_mdlo
	Where      a.cnsctvo_prstncn  = @cnsctvo_cdfccn
	And        d.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
	And        e.cnsctvo_cdgo_sde = @cnsctvo_cdgo_sde 
	And        e.cdgo_intrno     != @cdgo_intrno_gnrco	
	And        (
	            a.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or
	            a.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
			   )
	And        @fechaactual Between d.inco_vgnca And d.fn_vgnca 
	And        @fechaactual Between c.inco_vgnca And c.fn_vgnca
	And        @fechaactual Between a.inco_vgnca And a.fn_vgnca 
	And	       @fechaactual Between e.inco_vgnca And e.fn_vgnca         
	
	--Direccionamiento Paquete
	Insert		
	Into        #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
												  vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
												  drccn
	                                             )
	Select      Distinct d.cnsctvo_prstncn, a.cdgo_intrno     , Concat(p.nmbre_scrsl, '', p.drccn),
				         d.vlr_trfdo      , p.cnsctvo_cdgo_cdd, p.nmro_unco_idntfccn_prstdr               ,
				         p.drccn       
	From		bdContratacion.dbo.tbPaqueteDetalle b With(NoLock) 
	Inner Join	bdSisalud.dbo.tbDetTarifas c With(NoLock) 
	On          c.cnsctvo_rgstro = b.cnsctvo_pqte
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d With(NoLock)  
	On          d.cnsctvo_prstncn = c.cnsctvo_det_trfa 
	Inner Join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan l With(NoLock)
	On          l.cnsctvo_cdgo_lsta_prco = d.cnsctvo_cdgo_lsta_prco   
	Inner Join	bdSisalud.dbo.tbAsociacionModeloActividad a With(NoLock)	
	On          a.cnsctvo_asccn_mdlo_actvdd = l.cnsctvo_asccn_mdlo_actvdd	
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador p With(NoLock)
	On			p.cdgo_intrno = a.cdgo_intrno
	Inner Join  #tbTmpModelos m
	On          m.cnsctvo_cdgo_tpo_mdlo = a.cnsctvo_cdgo_tpo_mdlo
	Where		b.cnsctvo_prstcn    = @cnsctvo_cdfccn
	And			a.cnsctvo_cdgo_pln  = @cnsctvo_cdgo_pln
	And			p.cnsctvo_cdgo_sde  = @cnsctvo_cdgo_sde 
	And			p.cdgo_intrno      != @cdgo_intrno_gnrco	
    And         (
	              d.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or
	              d.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
			    )
	And			@fechaactual BetWeen c.inco_vgnca And c.fn_vgnca
	And			@fechaactual BetWeen d.inco_vgnca And d.fn_vgnca
	And			@fechaactual BetWeen l.inco_vgnca And l.fn_vgnca
    And 		@fechaactual BetWeen a.inco_vgnca And a.fn_vgnca 
	And 		@fechaactual BetWeen p.inco_vgnca And p.fn_vgnca 


	If (@cnsctvo_cdgo_sde <> @cnsctvo_sde_bgta) And (@cnsctvo_cdgo_sde <> @cnsctvo_sde_mdlln)
		Begin
		   If @cnsctvo_cdgo_sde <> @cnsctvo_sde_cli
			  Begin
				Insert 
				Into        #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
															  vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
															  drccn
															 )
				Select 		Distinct d.cnsctvo_cdfccn, d.cdgo_intrno      , Concat(d1.nmbre_scrsl, @separador, d1.drccn),
									 d2.vlr_trfdo    , d1.cnsctvo_cdgo_cdd, d1.nmro_unco_idntfccn_prstdr                ,
									 d1.drccn
				From		bdcna.sol.tbCNADireccionamientoPrestadorDestino d With(NoLock)
				Inner Join	bdSiSalud.dbo.tbDireccionesPrestador d1	With(NoLock)
				On			d1.cdgo_intrno = d.cdgo_intrno
				Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d2 With(NoLock)	
				On			d2.cnsctvo_cdgo_dt_lsta_prco = d.cnsctvo_cdgo_dt_lsta_prco
				Where		d.cnsctvo_cdfccn    = @cnsctvo_cdfccn
				And			d.cnsctvo_cdgo_pln  = @cnsctvo_cdgo_pln
				And			d1.cnsctvo_cdgo_sde = @cnsctvo_sde_cli 
				And			d.cdgo_intrno      != @cdgo_intrno_gnrco		  
				And         (
							 d2.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or 
							 d2.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
							)
				And			@fechaactual BetWeen d.inco_vgnca  And d.fn_vgnca
				And			@fechaactual BetWeen d1.inco_vgnca And d1.fn_vgnca
				And			@fechaactual BetWeen d2.inco_vgnca And d2.fn_vgnca

				---direccionamiento convenio Cali
				Insert		
				Into       #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
															 vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
															 drccn
															)
				Select     Distinct a.cnsctvo_prstncn, d.cdgo_intrno     , Concat(e.nmbre_scrsl, @separador, e.drccn),
									a.vlr_trfdo      , e.cnsctvo_cdgo_cdd, e.nmro_unco_idntfccn_prstdr               ,
									e.drccn      					
				From       bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan a With(NoLock) 
				Inner Join bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan c With(NoLock) 
				On         c.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco 
				Inner Join bdSisalud.dbo.tbAsociacionModeloActividad d With(NoLock) 
				On         d.cnsctvo_asccn_mdlo_actvdd = c.cnsctvo_asccn_mdlo_actvdd
				Inner Join bdSisalud.dbo.tbdireccionesPrestador e With(NoLock) 
				On         e.cdgo_intrno = d.cdgo_intrno	
				Inner Join #tbTmpModelos m
				On         m.cnsctvo_cdgo_tpo_mdlo = d.cnsctvo_cdgo_tpo_mdlo
				Where      a.cnsctvo_prstncn  = @cnsctvo_cdfccn
				And        d.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
				And        e.cnsctvo_cdgo_sde = @cnsctvo_sde_cli 
				And        e.cdgo_intrno     != @cdgo_intrno_gnrco	   
				And        (
							 a.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or
							 a.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
						   )
				And        @fechaactual Between d.inco_vgnca And d.fn_vgnca 
				And        @fechaactual Between c.inco_vgnca And c.fn_vgnca
				And        @fechaactual Between a.inco_vgnca And a.fn_vgnca 
				And	       @fechaactual Between e.inco_vgnca And e.fn_vgnca		
		
				--Direccionamiento Paquete
				Insert		
				Into        #tbTmpPrestadoresDireccionamiento(cnsctvo_cdfccn, cdgo_intrno     , nmbre_scrsl              ,		
															  vlr_trfdo     , cnsctvo_cdgo_cdd,	nmro_unco_idntfccn_prstdr,
															  drccn
															 )
				Select      Distinct d.cnsctvo_prstncn, a.cdgo_intrno     , Concat(p.nmbre_scrsl, '', p.drccn),
									 d.vlr_trfdo      , p.cnsctvo_cdgo_cdd, p.nmro_unco_idntfccn_prstdr               ,
									 p.drccn       
				From		bdContratacion.dbo.tbPaqueteDetalle b With(NoLock) 
				Inner Join	bdSisalud.dbo.tbDetTarifas c With(NoLock) 
				On          c.cnsctvo_rgstro = b.cnsctvo_pqte
				Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d With(NoLock)  
				On          d.cnsctvo_prstncn = c.cnsctvo_det_trfa 
				Inner Join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan l With(NoLock)
				On          l.cnsctvo_cdgo_lsta_prco = d.cnsctvo_cdgo_lsta_prco   
				Inner Join	bdSisalud.dbo.tbAsociacionModeloActividad a With(NoLock)	
				On          a.cnsctvo_asccn_mdlo_actvdd = l.cnsctvo_asccn_mdlo_actvdd	
				Inner Join	bdSisalud.dbo.tbDireccionesPrestador p With(NoLock)
				On			p.cdgo_intrno = a.cdgo_intrno
				Inner Join  #tbTmpModelos m
				On          m.cnsctvo_cdgo_tpo_mdlo = a.cnsctvo_cdgo_tpo_mdlo
				Where		b.cnsctvo_prstcn    = @cnsctvo_cdfccn
				And			a.cnsctvo_cdgo_pln  = @cnsctvo_cdgo_pln
				And			p.cnsctvo_cdgo_sde  = @cnsctvo_sde_cli 
				And			p.cdgo_intrno      != @cdgo_intrno_gnrco	
				And         (
							 d.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn    Or
							 d.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_frma_atncn_tds
							)
				And			@fechaactual BetWeen c.inco_vgnca And c.fn_vgnca
				And			@fechaactual BetWeen d.inco_vgnca And d.fn_vgnca
				And			@fechaactual BetWeen l.inco_vgnca And l.fn_vgnca
				And 		@fechaactual BetWeen a.inco_vgnca And a.fn_vgnca 
				And 		@fechaactual BetWeen p.inco_vgnca And p.fn_vgnca 	         	
			  End		   
		End	

	--/* Ips */
	Update		#tbTmpPrestadoresDireccionamiento 
	Set			rzn_scl						= b.rzn_scl, 
				cnsctvo_cdgo_tpo_idntfccn	= a.cnsctvo_cdgo_tpo_idntfccn, 
				nmro_idntfccn				= a.nmro_idntfccn
	From		#tbTmpPrestadoresDireccionamiento c
	Inner Join	dbo.tbprestadores a With(NoLock)
	On			c.nmro_unco_idntfccn_prstdr = a.nmro_unco_idntfccn_prstdr
	Inner Join	dbo.tbips b With(NoLock)
	On			a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

	/* Medicos */
	Update		#tbTmpPrestadoresDireccionamiento
	Set			rzn_scl						= CONCAT(LTRIM(RTRIM(b.prmr_nmbre_afldo)), @lcVacio, LTRIM(RTRIM(b.sgndo_nmbre_afldo)), @lcVacio,
												     LTRIM(RTRIM(b.prmr_aplldo))     , @lcVacio, LTRIM(RTRIM(b.sgndo_aplldo))
													), 
				cnsctvo_cdgo_tpo_idntfccn	= a.cnsctvo_cdgo_tpo_idntfccn, 
				nmro_idntfccn				= a.nmro_idntfccn
	From		#tbTmpPrestadoresDireccionamiento c
	Inner Join	dbo.tbprestadores a With(NoLock)
	On			c.nmro_unco_idntfccn_prstdr = a.nmro_unco_idntfccn_prstdr
	Inner Join	dbo.tbmedicos b With(NoLock)
	On			a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

	/* Tipo Identificacion */
	Update		#tbTmpPrestadoresDireccionamiento
	Set			dscrpcn_tpo_idntfccn	= d.dscrpcn_tpo_idntfccn,
				cdgo_tpo_idntfccn		= d.cdgo_tpo_idntfccn
	From		#tbTmpPrestadoresDireccionamiento c 
	Inner join	BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias d With(NoLock)
	On			c.cnsctvo_cdgo_tpo_idntfccn = d.cnsctvo_cdgo_tpo_idntfccn
	Where		@fechaactual between d.inco_vgnca and d.fn_vgnca

	-- mostrar resultado consulta
	Select 	    Distinct cdgo_intrno         , nmbre_scrsl  , drccn            ,	
				         vlr_trfdo	         , rzn_scl 	    , cdgo_tpo_idntfccn,
				         dscrpcn_tpo_idntfccn, nmro_idntfccn
	From		#tbTmpPrestadoresDireccionamiento
	Order By	vlr_trfdo
		
	Drop Table #tbTmpPrestadoresDireccionamiento
	Drop Table #tbTmpModelos
End
