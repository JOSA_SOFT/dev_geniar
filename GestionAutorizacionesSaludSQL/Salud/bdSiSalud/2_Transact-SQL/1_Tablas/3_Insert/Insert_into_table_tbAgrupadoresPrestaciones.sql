USE bdSiSalud
GO

DECLARE @fecha_actual							datetime,
		@cnsctvo_cdgo_agrpdr_prstcn				udtconsecutivo,
		@cnsctvo_vgnca_agrpdr_prstcn			udtConsecutivo,
		@cnsctvo_cdgo_tpo_agrpdr_prstcn			udtConsecutivo,
		@cnsctvo_vgnca_tpo_agrpdr_prstcn		udtConsecutivo,
		@uno									int
SET     @fecha_actual = getDate()
SET		@uno		  =	1

Select @cnsctvo_cdgo_tpo_agrpdr_prstcn = Max(cnsctvo_cdgo_tpo_agrpdr_prstcn) +@uno
From  dbo.tbTiposAgrupadoresPrestaciones with(nolock)

Select @cnsctvo_vgnca_tpo_agrpdr_prstcn = Max(cnsctvo_vgnca_tpo_agrpdr_prstcn)  +@uno
From  dbo.tbTiposAgrupadoresPrestaciones_Vigencias with(nolock)

Select @cnsctvo_cdgo_agrpdr_prstcn = Max(cnsctvo_cdgo_agrpdr_prstcn)  +@uno
From  dbo.tbAgrupadoresPrestaciones with(nolock)

Select @cnsctvo_vgnca_agrpdr_prstcn = Max(cnsctvo_vgnca_agrpdr_prstcn)  +@uno
From  dbo.tbAgrupadoresPrestaciones_Vigencias with(nolock)



INSERT INTO dbo.tbTiposAgrupadoresPrestaciones (
	cnsctvo_cdgo_tpo_agrpdr_prstcn, cdgo_tpo_agrpdr_prstcn, dscrpcn_tpo_agrpdr_prstcn,
	fcha_crcn                     , usro_crcn             , vsble_usro
	)
VALUES (
	@cnsctvo_cdgo_tpo_agrpdr_prstcn           , @cnsctvo_cdgo_tpo_agrpdr_prstcn      , 'Entrega Inmediata Deuda',
	@fecha_actual, 'siswtc01', 'S'
)

INSERT INTO dbo.tbTiposAgrupadoresPrestaciones_Vigencias (
	cnsctvo_vgnca_tpo_agrpdr_prstcn, cnsctvo_cdgo_tpo_agrpdr_prstcn, cdgo_tpo_agrpdr_prstcn,
	dscrpcn_tpo_agrpdr_prstcn      , inco_vgnca                    , fn_vgnca              ,
	fcha_crcn                      , usro_crcn                     , obsrvcns              ,
	vsble_usro
	)
VALUES (
	@cnsctvo_vgnca_tpo_agrpdr_prstcn                       , @cnsctvo_cdgo_tpo_agrpdr_prstcn                       , @cnsctvo_cdgo_tpo_agrpdr_prstcn                     ,
	'Entrega Inmediata Deuda', '2017-09-01 00:00:00.000', '9999-12-31 00:00:00.000',
	@fecha_actual            , 'siswtc01'               , 'Tipo de agrupador para las prestaciones sobre las cuales se requiere hacer entrega inmediata de ops',
	'S'
)


INSERT INTO dbo.tbAgrupadoresPrestaciones (
	cnsctvo_cdgo_agrpdr_prstcn, cdgo_agrpdr_prstcn, dscrpcn_agrpdr_prstcn,
	fcha_crcn                 , usro_crcn         , vsble_usro
	)
VALUES (
	@cnsctvo_cdgo_agrpdr_prstcn          , '372'      , 'Entrega Inmediata Deuda',
	@fecha_actual, 'user1'     , 'S'
)

INSERT INTO dbo.tbAgrupadoresPrestaciones_Vigencias (
	cnsctvo_vgnca_agrpdr_prstcn, cnsctvo_cdgo_agrpdr_prstcn    , cdgo_agrpdr_prstcn,
	dscrpcn_agrpdr_prstcn      , inco_vgnca                    , fn_vgnca          ,
	fcha_crcn                  , usro_crcn                     , obsrvcns          ,
	vsble_usro                 , cnsctvo_cdgo_tpo_agrpdr_prstcn
	)
VALUES (
	@cnsctvo_vgnca_agrpdr_prstcn                      , @cnsctvo_cdgo_agrpdr_prstcn                      , '372'                     ,
	'Entrega Inmediata Deuda', '2017-09-01 00:00:00.000', '9999-12-31 00:00:00.000',
	@fecha_actual            , 'user1'                  , 'Agrupador para las prestaciones sobre las cuales se requiere hacer entrega inmediata de ops',
	'S'                      , @cnsctvo_cdgo_tpo_agrpdr_prstcn
)