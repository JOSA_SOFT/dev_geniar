USE [bdSisalud]
GO

--32
--Select * from tbSolicitudOPSCausasEstadosPrestacion
--Select * from tbSolicitudOPSCausasEstadosPrestacion_Vigencias 


--28
--Select * from tbSolicitudOPSCausasxEstadosPrestacion
--Select * from tbSolicitudOPSCausasxEstadosPrestacion_Vigencias



Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime ,		
		@brrdo      udtLogico


Set     @fcha_actl  = GETDATE()
Set     @usro       = 'qvisionvgr'
Set     @obsrvcns   = '.'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'
Set     @brrdo      = 'N'

Alter Table bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion DISABLE trigger all
Alter Table bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion_Vigencias  DISABLE trigger all

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(31,
	        '31',
	        'Documentos soportes de la solicitud no legibles',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(31,
	        31,
			'31',
			'Documentos soportes de la solicitud no legibles',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(32,
	        '32',
	        'Solicitud generada por prestador no adscrito',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(32,
	        32,
			'32',
			'Solicitud generada por prestador no adscrito',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(33,
	        '33',
	        'Afiliado sin derecho a servicios',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(33,
	        33,
			'33',
			'Afiliado sin derecho a servicios',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(34,
	        '34',
	        'Comfandi: comuniquese  a la linea 6841000 a pedir su cita.',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(34,
	        34,
			'34',
			'Comfandi: comuniquese  a la linea 6841000 a pedir su cita.',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(35,
	        '35',
	        'La orden medica se encuentra vencida comuniquese con su medico tratante.',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(35,
	        35,
			'35',
			'La orden medica se encuentra vencida comuniquese con su medico tratante.',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

----------------------------------------------------------------------------------------------------------------
INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(36,
	        '36',
	        'PAF Comfandi Comuniquese con Clinica Tequendama al tel 6846464 - 6846400  o Clinica Amiga al tel 3336999 - 6841000',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(36,
	        36,
			'36',
			'PAF Comfandi Comuniquese con Clinica Tequendama al tel 6846464 - 6846400  o Clinica Amiga al tel 3336999 - 6841000',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(37,
	        '37',
	        'PAF Versalles Comuniquese  al tel 6875900 - 4184444 Citas Medicas Ext 100 - 200 - 400',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(37,
	        37,
			'37',
			'PAF Versalles Comuniquese  al tel 6875900 - 4184444 Citas Medicas Ext 100 - 200 - 400',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )


INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(38,
	        '38',
	        'PAF Salud Florida Comuniquese  al tel 2642462 - 2642466 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(38,
	        38,
			'38',
			'PAF Salud Florida Comuniquese  al tel 2642462 - 2642466 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(39,
	        '39',
	        'PGP RNM y TAC Comuniquese con  Clinica Amiga al tel 3336999 - 6841000',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(39,
	        39,
			'39',
			'PGP RNM y TAC Comuniquese con  Clinica Amiga al tel 3336999 - 6841000',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(40,
	        '40',
	        'PGP Sigma Comuniquese  al tel 4854890 - 6682525 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(40,
	        40,
			'40',
			'PGP Sigma Comuniquese  al tel 4854890 - 6682525 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(41,
	        '41',
	        'PGP Clinica Vision Comuniquese al tel 4450902- 4450901 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(41,
	        41,
			'41',
			'PGP Clinica Vision Comuniquese al tel 4450902- 4450901 a pedir su cita.',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(42,
	        '42',
	        'PGP Reuma Comuniquese a Comfandi Torres al tel 6841000 6841000',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(42,
	        42,
			'42',
			'PGP Reuma Comuniquese a Comfandi Torres al tel 6841000 6841000',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(43,
	        '43',
	        'PGP 2000 Comuniquese a la Clinica Amiga al tel 3336999 - 6841000 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(43,
	        43,
			'43',
			'PGP 2000 Comuniquese a la Clinica Amiga al tel 3336999 - 6841000 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )


INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(44,
	        '44',
	        'PGP Clinicos Comuniquese  al tel  486 6678 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(44,
	        44,
			'44',
			'PGP Clinicos Comuniquese  al tel  486 6678 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(45,
	        '45',
	        'PGP Salud Mental Comuniquese con Ciclo Vital Colombia al tel 5522800-3116342513',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(45,
	        45,
			'45',
			'PGP Salud Mental Comuniquese con Ciclo Vital Colombia al tel 5522800-3116342513',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

		  
INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(46,
	        '46',
	        'UPI osteomuscular Norte Comuniquese a IPS Centro de Rehabilitacion del sur al tel  3777320 -5519800 - 5141687 - 37771282- 3174494978',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(46,
	        46,
			'46',
			'UPI osteomuscular Norte Comuniquese a IPS Centro de Rehabilitacion del sur al tel  3777320 -5519800 - 5141687 - 37771282- 3174494978',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )
-----------------------------------nuevos-------------------------------------------------------------------------------------------------------

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(47,
	        '47',
	        'Soportes incompletos, pendiente historia clinica',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(47,
	        47,
			'47',
			'Soportes incompletos, pendiente historia clinica',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(48,
	        '48',
	        'Soportes incompletos, pendiente solicitud medica',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(48,
	        48,
			'48',
			'Soportes incompletos, pendiente solicitud medica',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )


INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(49,
	        '49',
	        'Centro especialista amiga, Comuniquese al tel  3336999 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(49,
	        49,
			'49',
			'Centro especialista amiga, Comuniquese al tel  3336999 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )


INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(50,
	        '50',
	        'Clinica amiga, comuniquese al tel 3336999 a pedir su cita',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(50,
	        50,
			'50',
			'Clinica amiga, comuniquese al tel 3336999 a pedir su cita',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )


INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(51,
	        '51',
	        'UPI osteomuscular SUR Comuniquese a IPS Promover  5534405-3176550770',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(51,
	        51,
			'51',
			'UPI osteomuscular SUR Comuniquese a IPS Promover  5534405-3176550770',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(52,
	        '52',
	        'Orden NO POS no puede ser ingresadas por esta via, por favor tramitar en el SIAU',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(52,
	        52,
			'52',
			'Orden NO POS no puede ser ingresadas por esta via, por favor tramitar en el SIAU',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(53,
	        '53',
	        'Este tipo de servicio no se gestionan por el portal',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(53,
	        53,
			'53',
			'Este tipo de servicio no se gestionan por el portal',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(54,
	        '54',
	        'Los soportes no corresponden al usuario inscrito',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(54,
	        54,
			'54',
			'Los soportes no corresponden al usuario inscrito',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(55,
	        '55',
	        'Soportes incompletos favor anexar orden medica e historia clinica',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(55,
	        55,
			'55',
			'Soportes incompletos favor anexar orden medica e historia clinica',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(56,
	        '56',
	        'Favor tramitar una sola orden medica segun fecha y medico tratante solicitante',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(56,
	        56,
			'56',
			'Favor tramitar una sola orden medica segun fecha y medico tratante solicitante',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion]
           ([cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[vsble_usro])
     VALUES(57,
	        '57',
	        'Soportes incompletos favor anexar orden medica e historia clinica',
			@fcha_actl,
			@usro,
			@vsble_usro
		   )

INSERT INTO [dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd]
           ,[cdgo_csa_estdo_prstcn_slctd]
           ,[dscrpcn_csa_estdo_prstcn_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[obsrvcns]
           ,[vsble_usro]
           ,[rspta_eps]
           ,[rspta_usro])
     VALUES(57,
	        57,
			'57',
			'Soportes incompletos favor anexar orden medica e historia clinica',
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			@obsrvcns,
			@vsble_usro,
			NULL,
			NULL
		  )

Alter Table bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion  ENABLE trigger all
Alter Table bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion_Vigencias  ENABLE trigger all


