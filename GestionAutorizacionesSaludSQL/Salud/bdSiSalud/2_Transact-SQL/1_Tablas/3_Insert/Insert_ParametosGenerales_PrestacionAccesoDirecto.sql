use bdSiSalud

Go

INSERT [dbo].[tbParametrosGenerales] ([cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (114, N'114 ', N'Consecutivo del perfil coordinador ',getDate(), N'sismigra01', N'S')
GO

INSERT [dbo].[tbParametrosGenerales_Vigencias] ([cnsctvo_vgnca_prmtro_gnrl], [cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [tpo_dto_prmtro], [vlr_prmtro_nmrco], [vlr_prmtro_crctr], [vlr_prmtro_fcha], [cnsctvo_cdgo_tpo_undd]) VALUES (114, 114, N'114 ', N'Consecutivo del perfil coordinador', getDate(), CAST(0x002D247F00000000 AS DateTime), getDate(), N'sismigra01', N'consecutivo del perfil coordinador ', N'S', N'N', 246, NULL, NULL, NULL)
GO

