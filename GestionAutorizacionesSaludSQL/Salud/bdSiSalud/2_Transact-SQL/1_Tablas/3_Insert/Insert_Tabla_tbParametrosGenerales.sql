USE [bdSisalud]
GO

-- 1. Poblamos tbParametrosGenerales

USE [bdSisalud]
go
INSERT INTO [dbo].[tbParametrosGenerales](cnsctvo_cdgo_prmtro_gnrl,cdgo_prmtro_gnrl,dscrpcn_prmtro_gnrl,fcha_crcn,usro_crcn,vsble_usro) values (110,'110','Cantidad de D�as Siguientes Para OPS Virtual',getdate(),'sisjvg01','S')


-- 2. Poblamos tbParametrosGenerales_Vigencias

INSERT INTO [dbo].[tbParametrosGenerales_Vigencias](cnsctvo_vgnca_prmtro_gnrl,cnsctvo_cdgo_prmtro_gnrl,cdgo_prmtro_gnrl,dscrpcn_prmtro_gnrl,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro,tpo_dto_prmtro,vlr_prmtro_nmrco) values (110,110,'110','Cantidad de D�as Siguientes Para OPS Virtual',getdate(),'99991231',getdate(),'sisjvg01','Define cantidad d�as a tomar posterior a la fecha actual para Prog. Entrga OPS Virtual ','S','N',5)
