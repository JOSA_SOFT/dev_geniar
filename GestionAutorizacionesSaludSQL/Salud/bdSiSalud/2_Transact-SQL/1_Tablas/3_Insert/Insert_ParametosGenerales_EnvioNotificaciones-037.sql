use bdSiSalud

Go

Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime
		
Set     @fcha_actl  = GETDATE()
Set     @usro       = 'sismigra01'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'
Set     @obsrvcns   = '.'		

INSERT [dbo].[tbParametrosGenerales] ([cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (111, N'111 ', N'Correo electr�nico utilizado en el tag bcc de las plantillas del servicio de notificaciones', @fcha_actl, @usro, @vsble_usro)

INSERT [dbo].[tbParametrosGenerales_Vigencias] ([cnsctvo_vgnca_prmtro_gnrl], [cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [tpo_dto_prmtro], [vlr_prmtro_nmrco], [vlr_prmtro_crctr], [vlr_prmtro_fcha], [cnsctvo_cdgo_tpo_undd]) VALUES (113, 111, N'111 ', N'Correo electr�nico utilizado en el tag bcc de las plantillas del servicio de notificaciones', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Correo electr�nico utilizado en el tag bcc de las plantillas del servicio de notificaciones', @vsble_usro, N'C', NULL, 'asandoval@sos.com.co', NULL, NULL)

INSERT [dbo].[tbParametrosGenerales] ([cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (112, N'112 ', N'L�nea nacional de atenci�n al cliente', @fcha_actl, @usro, @vsble_usro)

INSERT [dbo].[tbParametrosGenerales_Vigencias] ([cnsctvo_vgnca_prmtro_gnrl], [cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [tpo_dto_prmtro], [vlr_prmtro_nmrco], [vlr_prmtro_crctr], [vlr_prmtro_fcha], [cnsctvo_cdgo_tpo_undd]) VALUES (114, 112, N'112 ', N'L�nea nacional de atenci�n al cliente', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'L�nea nacional de atenci�n al cliente', @vsble_usro, N'C', NULL, '01 8000 938 777', NULL, NULL)

GO

--Select * from [tbParametrosGenerales] order by [cnsctvo_cdgo_prmtro_gnrl]
--Select * from [tbParametrosGenerales_Vigencias] order by [cnsctvo_cdgo_prmtro_gnrl]