USE [bdSisalud]
GO

Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime ,		
		@brrdo      udtLogico


Set     @fcha_actl  = GETDATE()
Set     @usro       = 'qvisionvgr'
Set     @obsrvcns   = '.'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'
Set     @brrdo      = 'N'


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(30,
	        @fcha_actl,
			@usro,
			6,
			31)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(30,
	        30,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			31)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(31,
	        @fcha_actl,
			@usro,
			6,
			32)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(31,
	        31,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			32)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(32,
	        @fcha_actl,
			@usro,
			6,
			33)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(32,
	        32,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			33)
			
INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(33,
	        @fcha_actl,
			@usro,
			6,
			34)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(33,
	        33,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			34)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(34,
	        @fcha_actl,
			@usro,
			6,
			35)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(34,
	        34,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			35)

---

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(35,
	        @fcha_actl,
			@usro,
			6,
			36)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(35,
	        35,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			36)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(36,
	        @fcha_actl,
			@usro,
			6,
			37)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(36,
	        36,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			37)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(37,
	        @fcha_actl,
			@usro,
			6,
			38)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(37,
	        37,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			38)
			

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(38,
	        @fcha_actl,
			@usro,
			6,
			39)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(38,
	        38,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			39)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(39,
	        @fcha_actl,
			@usro,
			6,
			40)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(39,
	        39,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			40)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(40,
	        @fcha_actl,
			@usro,
			6,
			41)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(40,
	        40,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			41)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(41,
	        @fcha_actl,
			@usro,
			6,
			42)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(41,
	        41,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			42)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(42,
	        @fcha_actl,
			@usro,
			6,
			43)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(42,
	        42,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			43)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(43,
	        @fcha_actl,
			@usro,
			6,
			44)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(43,
	        43,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			44)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(44,
	        @fcha_actl,
			@usro,
			6,
			45)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(44,
	        44,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			45)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(45,
	        @fcha_actl,
			@usro,
			6,
			46)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(45,
	        45,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			46)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(46,
	        @fcha_actl,
			@usro,
			6,
			47)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(46,
	        46,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			47)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(47,
	        @fcha_actl,
			@usro,
			6,
			48)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(47,
	        47,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			48)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(48,
	        @fcha_actl,
			@usro,
			6,
			49)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(48,
	        48,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			49)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(49,
	        @fcha_actl,
			@usro,
			6,
			50)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(49,
	        49,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			50)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(50,
	        @fcha_actl,
			@usro,
			6,
			51)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(50,
	        50,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			51)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(51,
	        @fcha_actl,
			@usro,
			6,
			52)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(51,
	        51,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			52)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(52,
	        @fcha_actl,
			@usro,
			6,
			53)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(52,
	        52,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			53)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(53,
	        @fcha_actl,
			@usro,
			6,
			54)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(53,
	        53,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			54)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(54,
	        @fcha_actl,
			@usro,
			6,
			55)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(54,
	        54,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			55)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(55,
	        @fcha_actl,
			@usro,
			6,
			56)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(55,
	        55,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			56)

INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion]
           ([cnsctvo_csa_estdo_slctd]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(56,
	        @fcha_actl,
			@usro,
			6,
			57)


INSERT INTO [dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
           ([cnsctvo_vgnca_csa_estdo_slctd]
           ,[cnsctvo_csa_estdo_slctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[cnsctvo_cdgo_estdo_prstcn_slctd]
           ,[cnsctvo_cdgo_csa_estdo_prstcn_slctd])
     VALUES(56,
	        56,
			@fcha_actl,
			@fn_vgncia,
			@fcha_actl,
			@usro,
			6,
			57)