use bdSisalud
go


Declare @fcha_crcn    Datetime      ,
        @usro_crcn    udtUsuario    ,
		@vsble_usro   udtLogico     ,
		@fn_vgca      Datetime      ,
		@obsrvcns     udtObservacion,
		@adtr_mdcmnts Int


Set @fcha_crcn    = getDate()
Set @usro_crcn    = 'sismigra01'
Set @vsble_usro   = 'S'
Set @fn_vgca      = '9999-12-31'
Set @obsrvcns     = '.'
Set @adtr_mdcmnts = 4

Alter Table bdSisalud.dbo.tbCargosSOS  DISABLE trigger all
Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  DISABLE trigger all

INSERT INTO [dbo].[tbCargosSOS] ([cnsctvo_cdgo_crgo_ss], [cdgo_crgo_ss], [dscrpcn_crgo_ss], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (33, N'50', N'AUDITOR MEDICAMENTOS', @fcha_crcn, @usro_crcn, @vsble_usro)
INSERT INTO [dbo].[tbCargosSOS_Vigencias] ([cnsctvo_vgnca_crgo_ss], [cnsctvo_cdgo_crgo_ss], [cdgo_crgo_ss], [dscrpcn_crgo_ss], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [aplca_drccnmnto], [nvl_jrrq], [cnsctvo_cdgo_fljo_crgo]) VALUES (33, 33, N'50', N'AUDITOR MEDICAMENTOS', @fcha_crcn, @fn_vgca, @fcha_crcn, @usro_crcn, @obsrvcns, @vsble_usro, N'S', 1, @adtr_mdcmnts)

Alter Table bdSisalud.dbo.tbCargosSOS  ENABLE trigger all
Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  ENABLE trigger all
go
