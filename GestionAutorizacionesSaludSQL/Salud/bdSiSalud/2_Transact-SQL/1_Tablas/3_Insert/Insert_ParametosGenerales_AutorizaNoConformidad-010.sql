use bdSiSalud

Go

INSERT [dbo].[tbParametrosGenerales] ([cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (109, N'110 ', N'Numero de dias permitidos para cambiar el estado de la prestación devuelta a aprobada desde la radicación de la solicitud',getDate(), N'sismigra01', N'S')
GO

INSERT [dbo].[tbParametrosGenerales_Vigencias] ([cnsctvo_vgnca_prmtro_gnrl], [cnsctvo_cdgo_prmtro_gnrl], [cdgo_prmtro_gnrl], [dscrpcn_prmtro_gnrl], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [tpo_dto_prmtro], [vlr_prmtro_nmrco], [vlr_prmtro_crctr], [vlr_prmtro_fcha], [cnsctvo_cdgo_tpo_undd]) VALUES (112, 110, N'110 ', N'Numero de dias permitidos para cambiar el estado de la prestación devuelta a aprobada desde la radicación de la solicitud', getDate(), CAST(0x002D247F00000000 AS DateTime), getDate(), N'sismigra01', N'Numero de dias permitidos para cambiar el estado de la prestación devuelta a aprobada desde la radicación de la solicitud', N'S', N'N', 5, NULL, NULL, NULL)
GO

