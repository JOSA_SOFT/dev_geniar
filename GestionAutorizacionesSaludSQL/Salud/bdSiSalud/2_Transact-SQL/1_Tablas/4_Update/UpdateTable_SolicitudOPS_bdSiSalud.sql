use bdSisalud

Go

Update [bdSisalud].[dbo].[tbSolicitudOPSCausasEstadosPrestacion_Vigencias] 
Set    fn_vgnca = getDate()
Where  cnsctvo_cdgo_csa_estdo_prstcn_slctd In (26, 27, 28)

go

Update [bdSisalud].[dbo].[tbSolicitudOPSCausasxEstadosPrestacion_Vigencias]
Set    fn_vgnca = getDate()
Where  cnsctvo_csa_estdo_slctd In (22, 23, 24)

