use bdSiSalud
go

Alter Table bdSisalud.dbo.tbCargosSOS  DISABLE trigger all
Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  DISABLE trigger all

go

Update tbCargosSOS_Vigencias
Set    cnsctvo_cdgo_fljo_crgo = 5
Where  cnsctvo_cdgo_crgo_ss = 19

Update tbCargosSOS_Vigencias
Set    cnsctvo_cdgo_fljo_crgo = 6
Where  cnsctvo_cdgo_crgo_ss = 28

go

Alter Table bdSisalud.dbo.tbCargosSOS  ENABLE trigger all
Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  ENABLE trigger all

go

--Select cnsctvo_cdgo_fljo_crgo, * from bdSiSalud.dbo.tbCargosSOS_Vigencias Where cnsctvo_cdgo_crgo_ss IN (33, 28, 19)



