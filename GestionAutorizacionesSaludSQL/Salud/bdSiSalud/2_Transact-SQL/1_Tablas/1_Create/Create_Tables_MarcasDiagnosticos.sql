USE [BDSisalud]
GO

/****** Object:  Table [dbo].[tbTiposMarcasDiagnostico]    Script Date: 29/08/2017 11:39:11 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbTiposMarcasDiagnostico](
	[cnsctvo_cdgo_tpo_mrca_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cdgo_tpo_mrca_dgnstco] [dbo].[udtCodigo] NOT NULL,
	[dscrpcn_tpo_mrca_dgnstco] [dbo].[udtDescripcion] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[vsble_usro] [dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbMarcasDiagnostico] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_cdgo_tpo_mrca_dgnstco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_DATA]
) ON [FG_DATA]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave primaria de la tabla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'cnsctvo_cdgo_tpo_mrca_dgnstco'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo del cnsctvo_cdgo_mrca_dgnstco' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'cdgo_tpo_mrca_dgnstco'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripcion del cnsctvo_cdgo_mrca_dgnstco' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'dscrpcn_tpo_mrca_dgnstco'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creacion del registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'fcha_crcn'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario de creacion del registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'usro_crcn'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica si el registro es visible para seleccion al usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico', @level2type=N'COLUMN',@level2name=N'vsble_usro'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla que administra los parametros correspondientes a Cnsctvo_cdgo_mrca_dgnstco' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbTiposMarcasDiagnostico'
GO


CREATE TABLE [dbo].[tbTiposMarcasDiagnostico_Vigencias](
	[cnsctvo_vgnca_tpo_mrca_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_tpo_mrca_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cdgo_tpo_mrca_dgnstco] [dbo].[udtCodigo] NOT NULL,
	[dscrpcn_tpo_mrca_dgnstco] [dbo].[udtDescripcion] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[obsrvcns] [dbo].[udtObservacion] NOT NULL,
	[vsble_usro] [dbo].[udtLogico] NOT NULL,
	[tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbMarcasDiagnosticoVigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_tpo_mrca_dgnstco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_DATA]
) ON [FG_DATA]

GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnostico_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbMarcasDiagnostico_Vigencias_tbMarcasDiagnostico] FOREIGN KEY([cnsctvo_cdgo_tpo_mrca_dgnstco])
REFERENCES [dbo].[tbTiposMarcasDiagnostico] ([cnsctvo_cdgo_tpo_mrca_dgnstco])
GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnostico_Vigencias] CHECK CONSTRAINT [FK_tbMarcasDiagnostico_Vigencias_tbMarcasDiagnostico]
GO



CREATE TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos](
	[cnsctvo_cdgo_tpo_mrca_dgnstco_pr_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[cnsctvo_cdgo_tpo_mrca_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
 CONSTRAINT [PK_tbMarcasDiagnosticoxDiagnosticos] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_cdgo_tpo_mrca_dgnstco_pr_dgnstco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_DATA]
) ON [FG_DATA]

GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos]  WITH CHECK ADD  CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_tbDiagnosticos] FOREIGN KEY([cnsctvo_cdgo_dgnstco])
REFERENCES [dbo].[tbDiagnosticos] ([cnsctvo_cdgo_dgnstco])
GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos] CHECK CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_tbDiagnosticos]
GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos]  WITH CHECK ADD  CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_tbMarcasDiagnostico] FOREIGN KEY([cnsctvo_cdgo_tpo_mrca_dgnstco])
REFERENCES [dbo].[tbTiposMarcasDiagnostico] ([cnsctvo_cdgo_tpo_mrca_dgnstco])
GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos] CHECK CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_tbMarcasDiagnostico]
GO



USE [BDSisalud]
GO

/****** Object:  Table [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias]    Script Date: 29/08/2017 11:42:07 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias](
	[cnsctvo_vgnca_tpo_mrca_dgnstco_pr_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_tpo_mrca_dgnstco_pr_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[cnsctvo_tpo_mrca_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbMarcasDiagnosticoxDiagnosticos_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_tpo_mrca_dgnstco_pr_dgnstco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [FG_DATA]
) ON [FG_DATA]

GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_Vigencias_tbMarcasDiagnosticoxDiagnosticos] FOREIGN KEY([cnsctvo_cdgo_tpo_mrca_dgnstco_pr_dgnstco])
REFERENCES [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos] ([cnsctvo_cdgo_tpo_mrca_dgnstco_pr_dgnstco])
GO

ALTER TABLE [dbo].[tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias] CHECK CONSTRAINT [FK_tbMarcasDiagnosticoxDiagnosticos_Vigencias_tbMarcasDiagnosticoxDiagnosticos]
GO



