USE bdSisalud
GO

/****** Object:  Table dbo.tbprocedimientos    Script Date: 27/01/2017 8:21:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.tbASDatosAdicionalesMegaSipres(
	cnsctvo_cdgo_ofcna dbo.udtConsecutivo NOT NULL,
	nuam numeric(18, 0) NOT NULL,
	cnsctvo_prstcn dbo.udtConsecutivo NOT NULL,
	fcha_estmda_entrga_fnl datetime,  
	cnsctvo_cdgo_grpo_entrga udtConsecutivo, 
	cnsctvo_cdgo_mdlo_cra udtConsecutivo, 
	cnsctvo_slctd_mga udtConsecutivo, 
	fcha_crcn datetime NOT NULL,
	usro_crcn udtUsuario NOT NULL,
	fcha_ultma_mdfccn datetime NOT NULL,
	usro_ultma_mdfccn udtUsuario NOT NULL,
	
 CONSTRAINT PK_tbASDatosAdicionalesMegaSipres PRIMARY KEY CLUSTERED 
(
	cnsctvo_cdgo_ofcna ASC,
	nuam ASC,
	cnsctvo_prstcn ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON FG_DATA03
) ON FG_DATA03

GO


ALTER TABLE [dbo].[tbASDatosAdicionalesMegaSipres]  WITH CHECK ADD  CONSTRAINT [FK_tbASDatosAdicionalesMegaSipres_tbprocedimientos] FOREIGN KEY([cnsctvo_cdgo_ofcna], [nuam], [cnsctvo_prstcn])
REFERENCES [dbo].[tbprocedimientos] ([cnsctvo_cdgo_ofcna], [nuam], [cnsctvo_prstcn])
GO

ALTER TABLE [dbo].[tbASDatosAdicionalesMegaSipres] CHECK CONSTRAINT [FK_tbASDatosAdicionalesMegaSipres_tbprocedimientos]
GO



