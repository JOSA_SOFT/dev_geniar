USE[bdSisalud]
Go

GRANT SELECT ON OBJECT::dbo.tbSolicitudOPSCausasxEstadosPrestacion_Vigencias TO autSalud_rol;
GRANT SELECT ON OBJECT::dbo.tbSolicitudOPSCausasEstadosPrestacion_Vigencias TO autSalud_rol;
GRANT SELECT ON OBJECT::dbo.tbIncidenciasdetprogramacionfechaevento TO autSalud_rol;
GRANT SELECT ON OBJECT::dbo.tbBancos_vigencias TO autSalud_rol;
GRANT SELECT ON OBJECT::dbo.tbBancos TO autSalud_rol;