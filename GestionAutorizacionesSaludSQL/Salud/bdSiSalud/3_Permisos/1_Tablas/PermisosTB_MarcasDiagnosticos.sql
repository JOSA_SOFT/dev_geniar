Use bdSisalud
Go

	Grant Select On dbo.tbTiposMarcasDiagnostico To autsalud_rol
	Grant Update On dbo.tbTiposMarcasDiagnostico To autsalud_rol
	Grant Insert On dbo.tbTiposMarcasDiagnostico To autsalud_rol

	Grant Select On dbo.tbTiposMarcasDiagnostico_Vigencias To autsalud_rol
	Grant Update On dbo.tbTiposMarcasDiagnostico_Vigencias To autsalud_rol
	Grant Insert On dbo.tbTiposMarcasDiagnostico_Vigencias To autsalud_rol

	Grant Select On dbo.tbTiposMarcasDiagnosticoxDiagnosticos To autsalud_rol
	Grant Update On dbo.tbTiposMarcasDiagnosticoxDiagnosticos To autsalud_rol
	Grant Insert On dbo.tbTiposMarcasDiagnosticoxDiagnosticos To autsalud_rol

	Grant Select On dbo.tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias To autsalud_rol
	Grant Update On dbo.tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias To autsalud_rol
	Grant Insert On dbo.tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias To autsalud_rol