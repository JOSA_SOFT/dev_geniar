USE bdProcesosSalud
GO

BEGIN TRANSACTION 

DECLARE @fechaActual datetime = getdate()-1,
		@fechaFinal datetime = '9999-12-31 00:00:00.000',
		@usuario udtUsuario = 'geniarjag',
		@consTipoProceso int,
		@consTipoRegistroLogEvento int,
		@consTipoRegistroLogEvento_vigencia int

BEGIN TRY
	SELECT @consTipoProceso = MIN(cnsctvo_cdgo_tpo_prcso)
	FROM dbo.tbTiposProceso
	WHERE dscrpcn_tpo_prcso = 'Proceso Autom�tico Creaci�n Solicitudes MEGA a partir de 3047 Masivo'

	SELECT @consTipoRegistroLogEvento = max(ISNULL(cnsctvo_cdgo_tpo_rgstro_lg_evnto,0))+1
	FROM dbo.tbTiposRegistroLogEvento

	SELECT @consTipoRegistroLogEvento_vigencia = max(ISNULL(cnsctvo_vgnca_tpo_rgstro_lg_evnto,0))+1
	FROM dbo.tbTiposRegistroLogEvento_Vigencias

	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 13: Llenar tabla datos migracion mega-3047',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 13: Llenar tabla datos migracion mega-3047',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SELECT 'Todo salio bien'

	COMMIT
END TRY
BEGIN CATCH
	SELECT 'Algo fallo: '+ERROR_MESSAGE()+' En la linea '+cast(ERROR_LINE() as varchar)
	ROLLBACK
END CATCH
