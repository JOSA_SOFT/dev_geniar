USE bdProcesosSalud
GO

BEGIN TRANSACTION 

DECLARE @fechaActual datetime = getdate()-1,
		@fechaFinal datetime = '9999-12-31 00:00:00.000',
		@usuario udtUsuario = 'geniarjag',
		@consTipoProceso int,
		@consTipoProcesoVigencia int,
		@consTipoRegistroLogEvento int,
		@consTipoRegistroLogEvento_vigencia int
		
BEGIN TRY

	SELECT @consTipoProceso = max(ISNULL(cnsctvo_cdgo_tpo_prcso,0))+1
	FROM dbo.tbTiposProceso

	SELECT @consTipoProcesoVigencia = max(ISNULL(cnsctvo_vgnca_tpo_prcso,0))+1
	FROM dbo.tbTiposProceso_Vigencias

	SELECT @consTipoRegistroLogEvento = max(ISNULL(cnsctvo_cdgo_tpo_rgstro_lg_evnto,0))+1
	FROM dbo.tbTiposRegistroLogEvento

	SELECT @consTipoRegistroLogEvento_vigencia = max(ISNULL(cnsctvo_vgnca_tpo_rgstro_lg_evnto,0))+1
	FROM dbo.tbTiposRegistroLogEvento_Vigencias

	--Select @consTipoProceso, @consTipoProcesoVigencia, @consTipoRegistroLogEvento, @consTipoRegistroLogEvento_vigencia

	INSERT INTO dbo.tbTiposProceso (cnsctvo_cdgo_tpo_prcso, cdgo_tpo_prcso, dscrpcn_tpo_prcso, fcha_crcn, usro_crcn, vsble_usro)
	VALUES (@consTipoProceso,@consTipoProceso,'Proceso Autom�tico Creaci�n Solicitudes MEGA a partir de 3047 Masivo',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposProceso_Vigencias (cnsctvo_vgnca_tpo_prcso, cnsctvo_cdgo_tpo_prcso, cdgo_tpo_prcso, dscrpcn_tpo_prcso, inco_vgnca ,fn_vgnca, fcha_crcn, usro_crcn, vsble_usro)
	VALUES (@consTipoProcesoVigencia,@consTipoProceso,@consTipoProceso,'Proceso Autom�tico Creaci�n Solicitudes MEGA a partir de 3047 Masivo',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 1: Poblar tablas temporales para migracion de 3047 a MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 1: Poblar tablas temporales para migracion de 3047 a MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 2: Validacion Solicitud 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 2: Validacion Solicitud 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 3: Guardar Solicitud 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 3: Guardar Solicitud 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 4. Grabar Solicitud Afiliados 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 4. Grabar Solicitud Afiliados 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 5: Grabar Solicitud Prestador 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 5: Grabar Solicitud Prestador 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 6: Grabar Solicitud M�dico 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 6: Grabar Solicitud M�dico 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 7: Grabar Solicitud Diagn�stico 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 7: Grabar Solicitud Diagn�stico 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 8: Grabar Solicitud Prestaciones 3047 en MEGA',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 8: Grabar Solicitud Prestaciones 3047 en MEGA',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 9: Actualizar estados MEGA de acuerdo a 3047',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 9: Actualizar estados MEGA de acuerdo a 3047',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 10: Actualizar fecha de entrega de acuerdo a 3047',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 10: Actualizar fecha de entrega de acuerdo a 3047',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1

	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 11: Ejecutar direccionamiento prestador',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 11: Ejecutar direccionamiento prestador',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	SET @consTipoRegistroLogEvento = @consTipoRegistroLogEvento + 1
	SET @consTipoRegistroLogEvento_vigencia = @consTipoRegistroLogEvento_vigencia + 1
	
	INSERT INTO dbo.tbTiposRegistroLogEvento(cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,fcha_crcn,usro_crcn,vsble_usro)
	VALUES (@consTipoRegistroLogEvento,@consTipoRegistroLogEvento,'Paso 12: Generar numero de radicado solicitud',@fechaActual,@usuario,'S')

	INSERT INTO dbo.tbTiposRegistroLogEvento_Vigencias(cnsctvo_vgnca_tpo_rgstro_lg_evnto,cnsctvo_cdgo_tpo_rgstro_lg_evnto,cdgo_tpo_rgstro_lg_evnto,dscrpcn_tpo_rgstro_lg_evnto,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,vsble_usro,cnsctvo_cdgo_tpo_prcso)
	VALUES (@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento_vigencia,@consTipoRegistroLogEvento_vigencia,'Paso 12: Generar numero de radicado solicitud',@fechaActual,@fechaFinal,@fechaActual,@usuario,'S',@consTipoProceso)

	COMMIT

	SELECT 'Todo salio bien'
END TRY
BEGIN CATCH
	SELECT 'Algo fallo: '+ERROR_MESSAGE()+' En la linea '+cast(ERROR_LINE() as varchar)
	ROLLBACK
END CATCH
