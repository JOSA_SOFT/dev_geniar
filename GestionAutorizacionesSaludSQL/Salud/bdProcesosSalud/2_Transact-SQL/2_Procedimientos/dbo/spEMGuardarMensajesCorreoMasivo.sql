USE [bdProcesosSalud]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('dbo.spEMGuardarMensajesCorreoMasivo') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE dbo.spEMGuardarMensajesCorreoMasivo AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Procedimiento			: spEMGuardarMensajesCorreoMasivo
* Desarrollado por		: <\A Luis Fernando Benavides				\A>
* Descripción			: <\D Envio de correos masivo tipo servicio	\D>
* Observaciones			: <\O O\>
* Parámetros			: <\P @mnsje_crreo: XML con datos del mensaje de correo P\>
* Variables				: <\V V\>
* Fecha Creación		: <\FC 2015/12/14 FC\>
*-----------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------
* Modificado Por         : <\AM Luis Fernando Benavides AM\>
* Descripcion            : <\DM Captura resultado XML en variable para que pueda ser recuperado en un Insert into DM\>
* Nuevos Parametros      : <\PM @rsltdo: variable donde queda el resultado en formato XML PM\>
* Nuevas Variables       : <\VM VM\>
* Fecha Modificacion     : <\FM 2016/03/23 FM\>
*-----------------------------------------------------------------------------------------
* Modificado Por         : <\AM Luis Fernando Benavides AM\>
* Descripcion            : <\DM Agregua atributo @htmlTbody para la etiqueta parametro, permite pasar filas a tabla DM\>
* Nuevos Parametros      : <\PM PM\>
* Nuevas Variables       : <\VM VM\>
* Fecha Modificacion     : <\FM 2016/04/29 FM\>
*-----------------------------------------------------------------------------------------
* Modificado Por         : <\AM Luis Fernando Benavides AM\>
* Descripcion            : <\DM Implementa log para las inconsistencias de logica de negocio DM\>
* Nuevos Parametros      : <\PM PM\>
* Nuevas Variables       : <\VM VM\>
* Fecha Modificacion     : <\FM 2016/10/05 FM\>
*-----------------------------------------------------------------------------------------*/
/*
Declare @mnsje_crreo XML
Set	@mnsje_crreo = '
<mensajeCorreo>
  <id>1</id>
  <correo>lbenavides@sos.com.co</correo>
  <co>lbenavides@sos.com.co</co>
  <bcc>ogranados@sos.com.co</bcc>
  <plantilla>1</plantilla>
  <parametros>
	<parametro>
		<nombre>nmbre_afldo</nombre>
		<valor>Luis Fernando Benavides</valor>
	</parametro>
	<parametro>
		<nombre>tpo_idntfccn</nombre>
		<valor>CC</valor>
	</parametro>
	<parametro>
		<nombre>nmro_idntfccn</nombre>
		<valor>16536990</valor>
	</parametro>
	<parametro>
		<nombre>dscrpcn_pln</nombre>
		<valor>BIENESTAR</valor>
	</parametro>
	<parametro>
		<nombre>rngo_slrl</nombre>
		<valor>1</valor>
	</parametro>
	<parametro>
		<nombre>drcho</nombre>
		<valor>DERECHO A TODOS LOS SERVICIOS</valor>
	</parametro>
	<parametro>
		<nombre>ips_afldo</nombre>
		<valor>COMFANDI IPS PASOANCHO</valor>
	</parametro>
  </parametros>
  <fecha>
    <dia>20</dia>
    <hora>00</hora>
    <minutos>00</minutos>
    <mes>12</mes>
    <segundos>00</segundos>
    <ano>2015</ano>
  </fecha>
  <usuario>geniarwva</usuario>
</mensajeCorreo>
'
Exec dbo.spEMGuardarMensajesCorreoMasivo @mnsje_crreo
*/
Alter Procedure dbo.spEMGuardarMensajesCorreoMasivo
@mnsje_crreo	XML,
@rsltdo			XML = NULL OUTPUT
AS

Begin
	Set NoCount On

	Declare	@usr_ejcn_prcso						UdtUsuario		= Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fecha_actual						DateTime		= getdate(),
			@cnsctvo_lg							Int, 		
			@mensajeError 						Varchar(1000)	= '',
			@cnsctvo_prcso						udtConsecutivo, 
			@cnsctvo_cdgo_tpo_prcso				udtConsecutivo  = 9, /*Proceso envio correos*/
			@cnsctvo_estdo_exto					udtConsecutivo	= 2,
			@cnsctvo_estdo_err					udtConsecutivo	= 3,
			@id									int, 
			@nombre								varchar(max), 
			@valor								varchar(max),
			@total								int, 
			@cntdr								int,
			@cnsctvo_cdgo_tpo_rgstro_lg_evnto	udtConsecutivo = 35, /*Extraccion y Poblar Tablas Temporales Datos*/
			@cnsctvo_cdgo_estdo_evnto_prcso		udtConsecutivo = 1,  /*En proceso*/
			@ps1								int = 1,			 /*Paso 1*/
			@ps2								int = 2,			 /*Paso 2*/
			@cnsctvo_estdo						udtConsecutivo = 3,
			@cnsctvo_cdgo_tpo_incnsstnca_prcso	udtConsecutivo = 1

	Exec spRegistraProceso @cnsctvo_cdgo_tpo_prcso, @usr_ejcn_prcso,@cnsctvo_prcso Output

	Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso
		,@ps1
		,'Extraccion, Poblar Tablas Temporales Datos y Validar'
		,@cnsctvo_cdgo_tpo_rgstro_lg_evnto
		,@cnsctvo_cdgo_estdo_evnto_prcso
		,@cnsctvo_lg Output

	/*Crear tablas temporales*/
    Create Table #mensajecorreo
    (
		id							int             NULL
        ,asnto_crro					varchar(250)    NULL
		,crpo_crro					varchar(max)    NULL
		,crro						varchar(50)     NULL
		,crro_co					varchar(max)    NULL
		,crro_bcc					varchar(max)    NULL
		,cnsctvo_cdgo_plntlla_mnsje	int             NULL
		,fcha_crcn					varchar(30)     NULL
		,usro_crcn					varchar(50)     NULL
    )     

    Create Table #parametros
	(
		cnsctvo						int identity(1,1)   NOT NULL
		,id							int                 NULL
		,nmbre						varchar(max)        NULL
		,vlr						varchar(max)        NULL
	)

	Create Table #detalle
	(
		id							int             NULL
		,cdgo_mnsje					udtcodigo       NOT NULL
		,dscrpcn_mnsje				udtdescripcion  NOT NULL
	)

	/*Poblar tablas temporales*/
    Insert #mensajecorreo(
		id
		,crro
		,crro_co
		,crro_bcc
		,cnsctvo_cdgo_plntlla_mnsje
		,fcha_crcn
		,usro_crcn
		)
	Select 
		colx.value('id[1]', 'varchar(50)')									as id
		,colx.value('correo[1]', 'varchar(50)')								as crro
		,colx.value('co[1]', 'varchar(max)')								as crro_co
		,colx.value('bcc[1]', 'varchar(max)')								as crro_bcc
		,colx.value('plantilla[1]', 'varchar')								as cnsctvo_cdgo_plntlla_mnsje
		,concat(colx.value('(fecha/ano)[1]', 'varchar(max)')			, 
		colx.value('(fecha/mes)[1]', 'varchar(max)')					,
		colx.value('(fecha/dia)[1]', 'varchar(max)')					, ' ' ,
		colx.value('(fecha/hora)[1]', 'varchar(max)')				, ':' , 
		colx.value('(fecha/minutos)[1]', 'varchar(max)')				, ':' ,
		colx.value('(fecha/segundos)[1]', 'varchar(max)'))			as fcha_crcn
		,colx.value('usuario[1]', 'char(30)')								as usro_crcn
    From  @mnsje_crreo.nodes('mensajeCorreo') as TABX(COLX)

	/*20161003 sislbr01 OJO solo para pruebas*/
    Update	#mensajecorreo
	Set		crro		= 'kchaparro@greensqa.com'
			,crro_co	= 'lbenavides@sos.com.co'
			,crro_bcc	= 'asandoval@sos.com.co';

    Insert #parametros(
		id
		,nmbre
		,vlr
		)
	Select 
		colx.value('../../id[1]','varchar(50)')		as id
		,colx.value('nombre[1]', 'varchar(max)') as nmbre
		,CASE WHEN colx.value('@htmlTbody[1]','int') = 1
		THEN 
			cast(colx.query('valor/tbody')  as varchar(max))
		ELSE
			colx.value('valor[1]','varchar(max)')
		END as vlr
    From  @mnsje_crreo.nodes('mensajeCorreo/parametros/parametro') as TABX(COLX)

	/*Validar datos obligatorios*/
	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'01'
			,'El campo id esta vacio'
	From	#mensajecorreo
	Where	isnull(id,'') = ''

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'02'
			,'El campo correo esta vacio'
	From	#mensajecorreo
	Where	isnull(crro,'') = ''

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'03'
			,'El campo plantilla esta vacio'
	From	#mensajecorreo
	Where	isnull(cnsctvo_cdgo_plntlla_mnsje,'') = ''

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'04'
			,'El campo fecha esta vacio'
	From	#mensajecorreo
	Where	isnull(fcha_crcn,'') = ''

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'05'
			,'El campo usuario esta vacio'
	From	#mensajecorreo
	Where	isnull(usro_crcn,'') = ''

	/*Validar inconsistenicas en los datos*/
	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'06'
			,'El campo id esta repetido'
	From	#mensajecorreo
	Where   isnull(id,'') <> '' 
	Group by id
	Having  Count(id) > 1

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'07'
			,'El campo fecha no es valida'
	From	#mensajecorreo
	Where	isdate(fcha_crcn) = 0

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	a.id
			,'08'
			,'La plantilla no existe'
	From	#mensajecorreo a Left join tbPlantillaMensajes_Vigencias b with(nolock)
	On		a.cnsctvo_cdgo_plntlla_mnsje = b.cnsctvo_cdgo_plntlla_mnsje
	And		@fecha_actual between b.inco_vgnca and b.fn_vgnca
	Where	(b.cnsctvo_cdgo_plntlla_mnsje is null)

	Insert into #detalle(
			id
			,cdgo_mnsje
			,dscrpcn_mnsje)
	Select	id
			,'09'
			,CONCAT('Esta vacio el valor del parametro: ',a.nmbre)
	From	#parametros a 
	Where	(a.vlr is null)

	/*Validar si existen inconsistencias en datos*/
	If Exists (Select 1 from #detalle)
	Begin
		/*Mostrar inconsistencias conocidas*/
		SET @rsltdo = (
		Select 
		/*cabecera*/
			(
			Select	a.cnsctvo_cdgo_estdo_prcso as estado, a.dscrpcn_estdo_prcso as descripcion
			From	tbEstadosProceso_Vigencias a with(nolock)
			Where	a.cnsctvo_cdgo_estdo_prcso = @cnsctvo_estdo_err
			And		@fecha_actual between a.inco_vgnca and a.fn_vgnca
			FOR XML PATH('cabecera'), TYPE
			),
		/*detalle*/
			(Select 
				(
				Select	a.id as [@id], a.cdgo_mnsje as codigo, a.dscrpcn_mnsje as descripcion
				From	#detalle a ORDER BY a.id, a.cdgo_mnsje
				FOR XML PATH('mensajeCorreo'), TYPE 
			)
			 FOR XML PATH('detalle'), TYPE)
		FOR XML PATH('resultado')
		)

		/*Log*/
		--Set	 @mensajeError						= @rsltdo
		Set	 @mensajeError						= CONVERT(VARCHAR(1000), @rsltdo)
		Set	 @cnsctvo_estdo						= 3 /*Error*/
		Set	 @cnsctvo_cdgo_estdo_evnto_prcso	= 3 /*Incorrecto*/ 
		Set	 @cnsctvo_cdgo_tpo_incnsstnca_prcso = 2 /*Error Logica de negocio*/

	End 
	Else 
	Begin
		/*Log Evento*/
		Set	 @cnsctvo_cdgo_estdo_evnto_prcso	= 2 /*Correcto*/
		Exec spActualizaEstadoLogEventoProceso	@cnsctvo_lg,	@cnsctvo_cdgo_estdo_evnto_prcso

		Set  @cnsctvo_cdgo_tpo_rgstro_lg_evnto	= 48 /*Guardar Envio Mensaje de Correo*/
		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso
		,@ps2
		,'Guardar Envio Mensaje de Correo'
		,@cnsctvo_cdgo_tpo_rgstro_lg_evnto
		,@cnsctvo_cdgo_estdo_evnto_prcso
		,@cnsctvo_lg Output

		/*Crear indices para mejorar rendimiento*/
		--Create Nonclustered Index IDX_#mensajecorreo_id On #mensajecorreo(id)      
		--Create Nonclustered Index IDX_#parametros_id	On #parametros(id)

		/*Actualizar asunto y cuerpo tabla temporal*/
		Update		a
		Set			asnto_crro	= b.asnto_plntlla
					,crpo_crro	= b.crpo_plntlla_mnsje
		From		#mensajecorreo a
		Inner join	tbPlantillaMensajes_Vigencias b with(nolock)
		On			a.cnsctvo_cdgo_plntlla_mnsje = b.cnsctvo_cdgo_plntlla_mnsje
		Where		@fecha_actual between b.inco_vgnca and b.fn_vgnca

		/*Actualizar parametros cuerpo tabla temporal*/
		Select	@total = count(1) From #parametros
		Set		@cntdr = 1

		WHILE @cntdr <= @total
		BEGIN
			Select @id = id, @nombre = nmbre, @valor = vlr FROM #parametros WHERE cnsctvo = @cntdr
		
			UPDATE	#mensajecorreo
			SET		crpo_crro = replace(crpo_crro,'{$'+rtrim(ltrim(@nombre))+'}',@valor)
			WHERE	id = @id

			Set @cntdr = @cntdr + 1
		END 

		/*Guardar los mensajes de correo en tabla fisica*/
		Begin try
			Insert Into tbMensajesCorreo(
				asnto_crro
				,crpo_crro
				,crro
				,crro_co
				,crro_bcc
				,cnsctvo_cdgo_plntlla_mnsje
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn
				)
			Select
				asnto_crro
				,crpo_crro
				,crro
				,crro_co
				,crro_bcc
				,cnsctvo_cdgo_plntlla_mnsje
				,fcha_crcn
				,usro_crcn
				,fcha_crcn
				,usro_crcn
			From #mensajeCorreo

			SET @rsltdo = (
			Select	a.cnsctvo_cdgo_estdo_prcso as estado, a.dscrpcn_estdo_prcso as descripcion
			From	tbEstadosProceso_Vigencias a with(nolock)
			Where	a.cnsctvo_cdgo_estdo_prcso = @cnsctvo_estdo_exto
			And		@fecha_actual between a.inco_vgnca and a.fn_vgnca
			FOR XML PATH('cabecera'), ROOT('resultado')
			)

			/*Log*/
			Set	 @cnsctvo_estdo						= 2 /*Exitoso*/
			Set	 @cnsctvo_cdgo_estdo_evnto_prcso	= 2 /*Correcto*/ 

		End try 
		Begin catch
			/*Mostrar inconsistencias no conocidas al insertar*/
			Set @mensajeError = concat(ERROR_PROCEDURE(),' ',ERROR_MESSAGE(),' Linea: ',cast(ERROR_LINE() as varchar(5)))

			SET @rsltdo = (
			Select	0 as estado, @mensajeError as descripcion
			FOR XML PATH('cabecera'), ROOT('resultado')
			)

			/*Log*/
			--Set	 @mensajeError						= @rsltdo
			Set	 @mensajeError						= CONVERT(VARCHAR(1000), @rsltdo)
			Set	 @cnsctvo_estdo						= 3 /*Error*/
			Set	 @cnsctvo_cdgo_estdo_evnto_prcso	= 3 /*Incorrecto*/ 
			Set	 @cnsctvo_cdgo_tpo_incnsstnca_prcso = 1 /*Error en base de datos*/
		End catch
	End 

	/*Log Inconsistencia*/
	IF @cnsctvo_estdo = 3 
	Begin
		Exec spRegistrarInconsistenciaProceso @cnsctvo_cdgo_tpo_incnsstnca_prcso, @mensajeError, @cnsctvo_lg
	End 
	/*Log Evento*/
	Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,	@cnsctvo_cdgo_estdo_evnto_prcso
	/*Log Proceso*/
	Exec spActualizaEstadoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso, @cnsctvo_estdo

	SELECT @rsltdo.query('/')
	AS [xmlarchivo];

	/*Borrar indices temporales*/
	--DROP INDEX IDX_#mensajecorreo_id	ON #mensajecorreo
	--DROP INDEX IDX_#parametros_id		ON #parametros

	/*Borrar tablas temporales*/
	drop table #mensajecorreo
	drop table #parametros
	drop table #detalle

End
GO

GRANT EXECUTE ON dbo.spEMGuardarMensajesCorreoMasivo TO autsalud_rol
GO 


