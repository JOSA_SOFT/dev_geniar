USE [bdProcesosSalud]
GO
/****** Object:  StoredProcedure [dbo].[spEMControladorRegistrarMensajeOPS]    Script Date: 06/04/2017 4:39:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*-------------------------------------------------------------------------------------------------------
* Metodo o PRG 	         :  		spEMControladorRegistrarMensajeOPS
* Desarrollado por		 :  <\A		Ing. Warner Fernando Valencia - SEIT Consulting					A\>
* Descripcion			 :  <\D		Permite controlar el proceso de envio de correo de los servicios autorizados del proyecto MEGA
*																									D\>
* Observaciones		     :  <\O																		O\>
* Parametros			 :  <\P   																	P\>		
* Fecha Creacion		 :  <\FC	2015/07/11														FC\>
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Maria Liliana Prieto Rincon	AM\>
* Descripcion			 : <\DM Se adicionan 4 columnas nuevas a #tmpServiciosAutorizados cta_rcprcn, nmro_entrga, ultma_entrga, nmro_ttl_entrgs 	 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2016/08/30 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Julian Hidalgo	AM\>
* Descripcion			 : <\DM Se agrega los campos de nuam y cnsctvo_cdgo_ofcna en la tabla
								#tbPrestadorMensaje para ser utilizados mas adelante dentro de todo el componente de envio de correo 	 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017/03/13 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Maria Liliana Prieto	AM\>
* Descripcion			 : <\DM Se amplia el tamaño del campo drccn_prstdr de 150 a 300 caracteres, para concatenar la direccion con la ciudad del prestador 	 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017/04/06 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
*/

--EXEC spEMControladorRegistrarMensajeOPS  
ALTER PROCEDURE[dbo].[spEMControladorRegistrarMensajeOPS] 

AS

Set Nocount on;
BEGIN -- Inicio del procedimiento

	   Declare  @fecha_actual				DateTime,
				@lnErrorProceso				Int = 0,
				@usr_ejcn_prcso				UdtUsuario,
				@cnsctvo_lg					Int,				
				@mensajeError 				Varchar(1000)='',
				@cnsctvo_prcso				udtConsecutivo,
				@cnsctvo_cdgo_tpo_prcso		udtConsecutivo,
				@cnsctvo_estdo_exto			udtConsecutivo=2,
				@cnsctvo_estdo_err			udtConsecutivo=3,
				@cantRegistros				udtConsecutivo
				
 CREATE TABLE #tmpServiciosAutorizados(
				cnsctvo_prcso					udtConsecutivo,
				cnsctvo_atncn_ops				udtConsecutivo,
				nuam							numeric	,
				cnsctvo_cdgo_ofcna				udtConsecutivo,
				nmro_unco_ops					udtConsecutivo,
				cdgo_prstdr						char(8),
				nmro_unco_idntfccn_prstdr		udtConsecutivo,
				cnsctvo_gnrco					udtConsecutivo,
				cnsctvo_prstcn					udtConsecutivo,
				fcha_imprsn						datetime,
				fcha_utlzcn_dsde				datetime,
				fcha_utlzcn_hsta				datetime,
				fcha_utlzcn_hsta_txto			varchar(10),
				nmro_unco_idntfccn_afldo		udtConsecutivo,
				nmro_idntfccn					varchar(23),
				cnsctvo_cdgo_tpo_idntfccn		udtConsecutivo,
				cnsctvo_cdgo_pln				udtConsecutivo,
				cdgo_intrno						char(8),
				prmr_aplldo						varchar(50),
				sgndo_aplldo					varchar(50),
				prmr_nmbre						varchar(30),
				sgndo_nmbre						varchar(30),
				dscrpcn_ofcna					varchar(150),
				cdgo_tpo_idntfccn				varchar(3),
				dscrpcn_pln						varchar(150),
				cnsctvo_cdgo_rngo_slr			udtConsecutivo,
				dscrpcn_rngo_slrl					varchar(150),
				cnsctvo_cdgo_estdo_drcho		udtConsecutivo,
				dscrpcn_estdo_drcho				varchar(150),
				nbmre_ips_prmra					varchar(150),
				nmbre_prstdr					varchar(250),
				drccn_prstdr					varchar(300),
				tlfno_prstdr					varchar(150),
				cdgo_cdfccn						varchar(11),
				dscrpcn_cdfccn					varchar(150),
				eml								varchar(50),
				cnsctvo_bnfcro					udtConsecutivo,	
				fcha_inco_vgnca					datetime,
				fcha_fn_vgnca					datetime,
				cnsctvo_cdgo_tpo_cntrto			udtConsecutivo,
				nmro_cntrto						udtNumeroFormulario,
				vldo							int default 0, /* 0 Indica correcto;  1 no pasa la validacion  */
				cta_rcprcn						float		null, 
				nmro_entrga						Int			null,	 
				ultma_entrga					udtLogico	null,
				nmro_ttl_entrgs					Int			null
	)		
	
	
	
	--drop table #tbPrestadorMensaje
create table #tbPrestadorMensaje(
	cnsctvo_prstdr      int identity(1,1),
	cdgo_prstdr                   char(8),
	nmro_unco_idntfccn_afldo          int,
	nuam				          numeric,
	cnsctvo_cdgo_ofcna	   udtConsecutivo,
	crro_mnsje		          varchar(50),
	crro_mnsje_co	         varchar(max),
	crro_mnsje_bcc	         varchar(max),
	asnto_mnsje	    	     varchar(max),
	cbcra_mnsje		         varchar(max),
	dtlle_mnsje		         varchar(max),
	pntlla_mnsje	         varchar(max)
)

		
		
		/*variables del proceso*/
		Set	@fecha_actual = GetDate();
		set @cnsctvo_cdgo_tpo_prcso= 8 /*Enviar Correo Servicios Autorizados Afiliados - MEGA  */
		set @usr_ejcn_prcso='geniarwva' --SYSTEM_USER



Begin Try
	   
	      
	   --Registra proceso
		Exec spRegistraProceso @cnsctvo_cdgo_tpo_prcso, @usr_ejcn_prcso,@cnsctvo_prcso Output
				 
		Begin Try
						
			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Consultar Autorizaciones para Crear Mensajes de Correo',1, 1,@cnsctvo_lg Output
			Exec spEMConsultarOPSAutorizadas	@cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso	  
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		End Try
		Begin Catch
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR
		End Catch

		/*Validamos si hay datos para envio de correo */
		Select @cantRegistros= count(1) from #tmpServiciosAutorizados

		If (@cantRegistros <= 0)
		Begin

			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'No hay Autorizaciones para Crear Mensajes de Correo',1, 1,@cnsctvo_lg Output
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto 
			return 
		End


		 Begin Try
			
			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos Autorizaciones para Mensajes de Correo', 2,1,@cnsctvo_lg Output								
			EXEC spEMCalcularDatosOPSAutorizadas @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso						
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		End Try
		Begin Catch
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR						
		End Catch

		 Begin Try
			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Validar Datos Autorizaciones para Mensajes de Correo', 3,1,@cnsctvo_lg Output								
			EXEC spEMValidarDatosOPSAutorizadas @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso						
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto
		End Try
		Begin Catch
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR						
		End Catch


	
		Begin Try			
			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos Mensaje de Correo', 4,1,@cnsctvo_lg Output								
			EXEC spEMCalcularDatosOPSMensajeCorreo @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso						
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		End Try
		Begin Catch
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR						
		End Catch


		Begin Try
			
			Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Marcar OPS con Mensaje de Correo ', 5,1,@cnsctvo_lg Output								
			EXEC spEMActualizarOPSMensajeGenerado @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso						
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		End Try
		Begin Catch
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR						
		End Catch


End Try
Begin Catch
		Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
		Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						
End Catch
		
END-- Fin del procedimiento
go
sp_recompile 'spEMControladorRegistrarMensajeOPS'
go
