USE [bdProcesosSalud]
GO
/****** Object:  StoredProcedure [dbo].[spEMCalcularDatosOPSMensajeCorreo]    Script Date: 07/04/2017 1:43:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*-------------------------------------------------------------------------------------------------------
* Metodo o PRG 	         :  		spEMCalcularDatosOPSMensajeCorreo
* Desarrollado por		 :  <\A		Ing. Warner Fernando Valencia - SEIT Consulting					A\>
* Descripcion			 :  <\D		Crea los Mensajes de Correo
*																									D\>
* Observaciones		     :  <\O																		O\>
* Parametros			 :  <\P   																	P\>		
* Fecha Creacion		 :  <\FC	2015/07/11														FC\>
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM    Ing. Julian Hidalgo AM\>
* Descripcion			 : <\DM    Se Insertan los campos nuam y cnsctvo_cdgo_ofcna en la tabla #tbPrestadorMensaje 
							 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017-03-13 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
*/

--EXEC spEMCalcularDatosOPSMensajeCorreo  
ALTER PROCEDURE[dbo].[spEMCalcularDatosOPSMensajeCorreo] 
	@cnsctvo_prcso				udtconsecutivo, 
	@cnsctvo_cdgo_tpo_prcso		udtconsecutivo

AS

Set Nocount on;


BEGIN -- Inicio del procedimiento

	Declare @cnsctvo_lg			udtConsecutivo,
			@cnsctvo_estdo_exto	udtConsecutivo=2,
			@cnsctvo_estdo_err	udtConsecutivo=3,
			@mensajeError		varchar(max),
			@asnto				varchar(max),
			@crpo_plntlla_mnsje	varchar(max),
			@usr_ejcn_prcso		udtusuario,
			@cnsctvo_plntlla	udtConsecutivo,
			@fcha_prcso			datetime

				
	set @usr_ejcn_prcso='procesoenviocorreo' --SYSTEM_USER
	set @cnsctvo_plntlla=1
	set @fcha_prcso=getdate()


	Begin Try
		
		
		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Obtener Plantilla Para Mensaje de Correo', 4,1,@cnsctvo_lg Output


		
		Select @crpo_plntlla_mnsje	= crpo_plntlla_mnsje,
				@asnto				= asnto_plntlla
		from tbPlantillaMensajes_vigencias 
		where cnsctvo_cdgo_plntlla_mnsje =@cnsctvo_plntlla
		and inco_vgnca <= @fcha_prcso
		and fn_vgnca >= @fcha_prcso
		
		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto


		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Prestador del Mensaje', 4,1,@cnsctvo_lg Output		
		Insert #tbPrestadorMensaje(
			cdgo_prstdr,
			nmro_unco_idntfccn_afldo,
			nuam,
			cnsctvo_cdgo_ofcna,
			asnto_mnsje,
			pntlla_mnsje
			
		)
		select	cdgo_prstdr,
				nmro_unco_idntfccn_afldo,
				nuam,
			cnsctvo_cdgo_ofcna,
				@asnto,
				@crpo_plntlla_mnsje				
		from #tmpServiciosAutorizados
		where vldo=0 /*indica que cumplio con las validaciones*/
		group by cdgo_prstdr,
				nmro_unco_idntfccn_afldo,
				nuam,
				cnsctvo_cdgo_ofcna

		
		
		update #tbPrestadorMensaje
		set crro_mnsje= d2.eml
		from #tbPrestadorMensaje d1 inner join #tmpServiciosAutorizados d2
		on d1.nmro_unco_idntfccn_afldo = d2.nmro_unco_idntfccn_afldo


		Exec spEMCalcularMensajeCorreo @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso

		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto	
		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Guardar Mensaje Correo', 4,1,@cnsctvo_lg Output
		
		Insert tbMensajesCorreo(
			asnto_crro,
			crpo_crro,
			crro,
			crro_co,
			crro_bcc,
			cnsctvo_cdgo_plntlla_mnsje,
			fcha_crcn,
			usro_crcn,
			usro_ultma_mdfccn,
			fcha_ultma_mdfccn
		)
		Select 	substring(ltrim(rtrim(asnto_mnsje)),1,250),
				pntlla_mnsje,
				substring(ltrim(rtrim(crro_mnsje)),1,50),
				crro_mnsje_bcc,
				'mprieto@sos.com.co',--crro_mnsje_co,
				@cnsctvo_plntlla,
				@fcha_prcso,
				@usr_ejcn_prcso,
				@usr_ejcn_prcso,
				@fcha_prcso
		from #tbPrestadorMensaje

		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto



	End Try
	Begin Catch
			set @mensajeError=''
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
	End Catch

END
go
sp_recompile 'spEMCalcularDatosOPSMensajeCorreo'
go