USE [bdProcesosSalud]
GO
/****** Object:  StoredProcedure [dbo].[spEMCalcularDatosOPSAutorizadas]    Script Date: 12/04/2017 10:31:35 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*-------------------------------------------------------------------------------------------------------
* Metodo o PRG 	         :  		spEMCalcularDatosOPSAutorizadas
* Desarrollado por		 :  <\A		Ing. Warner Fernando Valencia - SEIT Consulting					A\>
* Descripcion			 :  <\D		Calcula los datos de los Afiliados, prestador y prestaciones
*																									D\>
* Observaciones		     :  <\O																		O\>
* Parametros			 :  <\P   																	P\>		
* Fecha Creacion		 :  <\FC	2015/07/11														FC\>
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM  Ing.Juan Carlos VÃ¡squez GAM\>
* Descripcion			 : <\DM  Se ajusta sp para recuperar el e-mail 
								del afiliado de BDAfiliacionValidador.dbo.tbBeneficiariosValidador							 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2016/08/22  FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM  Ing. Maria Liliana Prieto AM\>
* Descripcion			 : <\DM  Se ajusta sp para recuperar el e-mail 
								del afiliado de bdSeguridad.dbo.tbRegistroUsuariosWeb					 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017/04/04 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Maria Liliana Prieto	AM\>
* Descripcion			 : <\DM Se amplia el tamaño del campo drccn_prstdr de 150 a 300 caracteres, para concatenar la direccion con la ciudad del prestador 	 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017/04/06 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Maria Liliana Prieto	AM\>
* Descripcion			 : <\DM se valida que este vigente a la fecha del proceso y se busca por el plan 	 DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 2017/04/12 FM\>		
*-----------------------------------------------------------------------------------------------------------------------------------------
*/
--EXEC spEMCalcularDatosOPSAutorizadas  
ALTER PROCEDURE[dbo].[spEMCalcularDatosOPSAutorizadas] 
	@cnsctvo_prcso				udtconsecutivo, 
	@cnsctvo_cdgo_tpo_prcso		udtconsecutivo

AS

Set Nocount on;
BEGIN -- Inicio del procedimiento

	Declare @mrcaEnvioCorreo	udtConsecutivo,
			@valido				udtLogico,
			@cnsctvo_lg			udtConsecutivo,
			@cnsctvo_estdo_exto	udtConsecutivo=2,
			@cnsctvo_estdo_err	udtConsecutivo=3,
			@mensajeError		varchar(max),
			@fechaproceso datetime
		
				
	
	Create table #tbCalculoEstadoDerecho(
		nmro_cntrto					udtNumeroFormulario,
		cnsctvo_cdgo_tpo_cntrto		udtConsecutivo,
		cnsctvo_bnfcro				udtConsecutivo,	
		cnsctvo_cdgo_estdo_drcho	 udtConsecutivo,	
		fcha_inco_vgnca				datetime
	)
				
	set @mrcaEnvioCorreo=3
	set @valido='S'
	set @fechaproceso=getdate()

--SPPMConsultarServiciosMarcadosAutorizados

	Begin Try
		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos de Autorizaciones Marcadas - Atencion', 2,1,@cnsctvo_lg Output

		/*calcular Afiliado*/
		update #tmpServiciosAutorizados
		set prmr_aplldo=d2.prmr_aplldo,
			sgndo_aplldo= d2.sgndo_aplldo,
			prmr_nmbre= d2.prmr_nmbre,
			sgndo_nmbre= d2.sgndo_nmbre,
			nmro_idntfccn= d2.nmro_idntfccn,
			cnsctvo_cdgo_rngo_slr=d2.cnsctvo_cdgo_rngo_slrl,
			cnsctvo_cdgo_tpo_idntfccn = d2.cnsctvo_cdgo_tpo_idntfccn,
			cdgo_intrno=d2.cdgo_ips,--eml = 'ataborda@sos.com.co'
			cnsctvo_bnfcro= d2.nmro_bnfcro,
			fcha_inco_vgnca= d2.fcha_inco_vgnca,
			fcha_fn_vgnca= d2.fcha_fn_vgnca
		from #tmpServiciosAutorizados d1 inner join bdsisalud.dbo.tbactua d2 with(nolock)
		on d1.nuam = d2.nuam
		and d1.cnsctvo_cdgo_ofcna = d2.cnsctvo_cdgo_ofcna



		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto
		
		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos de Autorizaciones Marcadas - Beneficiarios ', 2,1,@cnsctvo_lg Output

						
		/*calcular el campo email */
		--sismpr01 - 20170412- se valida que este vigente a la fecha del proceso y se busca por el plan
		update #tmpServiciosAutorizados
		set nmro_cntrto				= d2.nmro_cntrto,
			cnsctvo_cdgo_tpo_cntrto = d2.cnsctvo_cdgo_tpo_cntrto,
			eml						= d2.eml
		from	#tmpServiciosAutorizados d1 
					inner join BDAfiliacionValidador.dbo.tbBeneficiariosValidador d2 with(nolock)
						on		d1.nmro_unco_idntfccn_afldo	= d2.nmro_unco_idntfccn_afldo
					inner join BDAfiliacionValidador.dbo.tbcontratosvalidador cv with(nolock)
						on		cv.cnsctvo_cdgo_tpo_cntrto	= d2.cnsctvo_cdgo_tpo_cntrto
						and		cv.nmro_cntrto				= d2.nmro_cntrto	
		where	d1.cnsctvo_bnfcro		= d2.cnsctvo_bnfcro
		and		d1.cnsctvo_cdgo_pln		= cv.cnsctvo_cdgo_pln
		and		@fechaproceso			Between d2.inco_vgnca_bnfcro and d2.fn_vgnca_bnfcro


		update	d1
		set		eml = d2.eml
		from	#tmpServiciosAutorizados d1 
					inner join bdSeguridad.dbo.tbRegistroUsuariosWeb d2 with(nolock)
						on    d1.nmro_unco_idntfccn_afldo= d2.nmro_unco_idntfccn_usro
		where	isnull(ltrim(rtriM(d2.eml)) ,'') != ''


		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto


		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos de Autorizaciones Marcadas - Presolicitud ', 2,1,@cnsctvo_lg Output

		/*calcular el correo de la solicitud, en el cual queda registrado el login del usuario que crea la solicitud de acuerdo a Orientacion A.Taborda */
		update #tmpServiciosAutorizados
		set eml = ltrim(rtrim(d3.usro_crcn))
		from #tmpServiciosAutorizados d1 inner join bdcna.dbo.tbPrestacionesAutorizadasPreSolicitud d2
		on d1.nuam = d2.nuam 
		and d1.cnsctvo_cdgo_ofcna= d2.cnsctvo_cdgo_ofcna
		and d1.cnsctvo_prstcn= d2.cnsctvo_cdfccn
		inner join bdcna.dbo.tbPresolicitudes d3
		on d3.cnsctvo_prslctd= d2.cnsctvo_prslctd

		
		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos de Autorizaciones Marcadas - Estado Derecho ', 2,1,@cnsctvo_lg Output

		Insert #tbCalculoEstadoDerecho(
			nmro_cntrto,		
			cnsctvo_cdgo_tpo_cntrto,
			cnsctvo_bnfcro,			
			cnsctvo_cdgo_estdo_drcho,
			fcha_inco_vgnca		
		)
		Select Distinct 
			  nmro_cntrto,		
			 cnsctvo_cdgo_tpo_cntrto,
			 cnsctvo_bnfcro,			
			 cnsctvo_cdgo_estdo_drcho,
			 fcha_inco_vgnca	
		from #tmpServiciosAutorizados

		/*calcular el estado de derecho*/
		Update		#tbCalculoEstadoDerecho
		Set			cnsctvo_cdgo_estdo_drcho	= m.cnsctvo_cdgo_estdo_drcho					
		From		#tbCalculoEstadoDerecho t Inner Join	BDAfiliacionValidador.dbo.TbMatrizDerechosValidador	m	With(NoLock)	
		On	m.cnsctvo_cdgo_tpo_cntrto	= t.cnsctvo_cdgo_tpo_cntrto
		And	m.nmro_cntrto				= t.nmro_cntrto 
		And	m.cnsctvo_bnfcro			= t.cnsctvo_bnfcro
		where @fechaproceso between m.inco_vgnca_estdo_drcho  and m.fn_vgnca_estdo_drcho
		
		Update		#tbCalculoEstadoDerecho
		Set			cnsctvo_cdgo_estdo_drcho	= m.cnsctvo_cdgo_estdo_drcho					
		From		#tbCalculoEstadoDerecho t Inner Join	BDAfiliacionValidador.dbo.TbMatrizDerechosValidador_at	m	With(NoLock)	
		On	m.cnsctvo_cdgo_tpo_cntrto	= t.cnsctvo_cdgo_tpo_cntrto
		And	m.nmro_cntrto				= t.nmro_cntrto 
		And	m.cnsctvo_bnfcro			= t.cnsctvo_bnfcro
		Where t.cnsctvo_cdgo_estdo_drcho is null
		And @fechaproceso between m.inco_vgnca_estdo_drcho  and m.fn_vgnca_estdo_drcho

	
		Update		#tmpServiciosAutorizados
		Set			cnsctvo_cdgo_estdo_drcho	= m.cnsctvo_cdgo_estdo_drcho					
		From		#tmpServiciosAutorizados t Inner Join	#tbCalculoEstadoDerecho	m	With(NoLock)	
		On	m.cnsctvo_cdgo_tpo_cntrto	= t.cnsctvo_cdgo_tpo_cntrto
		And	m.nmro_cntrto				= t.nmro_cntrto 
		And	m.cnsctvo_bnfcro			= t.cnsctvo_bnfcro
		and m.fcha_inco_vgnca			= t.fcha_inco_vgnca	
		

		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto

		Exec spRegistraLogEventoProceso @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso,1,'Calcular Datos de Autorizaciones Marcadas - Datos Basicos ', 2,1,@cnsctvo_lg Output


		update #tmpServiciosAutorizados
		set dscrpcn_pln = d2.dscrpcn_pln
		from #tmpServiciosAutorizados d1 inner join BDAfiliacionValidador.dbo.tbplanes d2 with(nolock)
		on d1.cnsctvo_cdgo_pln = d2.cnsctvo_cdgo_pln

		update #tmpServiciosAutorizados
		set dscrpcn_rngo_slrl= d2.dscrpcn_rngo_slrl
		from #tmpServiciosAutorizados d1 inner join BDAfiliacionValidador.dbo.tbRangosSalariales  d2 with(nolock)
		on d1.cnsctvo_cdgo_rngo_slr= d2.cnsctvo_cdgo_rngo_slrl

		update #tmpServiciosAutorizados
		set dscrpcn_ofcna=d2.dscrpcn_ofcna
		from #tmpServiciosAutorizados d1 inner join BDAfiliacionValidador.dbo.tboficinas  d2 with(nolock)
		on d1.cnsctvo_cdgo_ofcna = d2.cnsctvo_cdgo_ofcna

		update #tmpServiciosAutorizados
		set cdgo_tpo_idntfccn=d2.cdgo_tpo_idntfccn
		from #tmpServiciosAutorizados d1 inner join BDAfiliacionValidador.dbo.tbtiposIdentificacion  d2 with(nolock)
		on d1.cnsctvo_cdgo_tpo_idntfccn = d2.cnsctvo_cdgo_tpo_idntfccn

		/*ojo debe calcular el estado de derecho*/
		update #tmpServiciosAutorizados
		set dscrpcn_estdo_drcho=d2.dscrpcn_drcho
		from #tmpServiciosAutorizados d1 inner join BDAfiliacionValidador.dbo.tbEstadosDerechoValidador  d2 with(nolock)
		on d1.cnsctvo_cdgo_estdo_drcho = d2.cnsctvo_cdgo_estdo_drcho

		update #tmpServiciosAutorizados
		set nbmre_ips_prmra=d2.nmbre_scrsl
		from #tmpServiciosAutorizados d1 inner join bdSisalud.dbo.tbIPSPrimarias_vigencias  d2 with(nolock)
		on d1.cdgo_intrno = d2.cdgo_intrno

		/*calcula datos prestador*/
		update #tmpServiciosAutorizados
		set nmbre_prstdr=d2.nmbre_scrsl,
			drccn_prstdr=ltrim(rtrim(d2.drccn)) + space(1) + ltrim(rtrim(c.dscrpcn_cdd)),
			tlfno_prstdr=d2.tlfno
		from #tmpServiciosAutorizados d1	
				inner join bdSisalud.dbo.tbDireccionesPrestador  d2		on	d1.cdgo_prstdr		= d2.cdgo_intrno
				inner join bdafiliacionvalidador..tbciudades	 c		on	d2.cnsctvo_cdgo_cdd = c.cnsctvo_cdgo_cdd
		
		
		/*calcular los servicios*/
		update #tmpServiciosAutorizados
		set cdgo_cdfccn= d2.cdgo_cdfccn,
			dscrpcn_cdfccn= d2.dscrpcn_cdfccn
		from #tmpServiciosAutorizados d1 inner join bdSisalud.dbo.tbCodificaciones  d2 with(nolock)
		on d1.cnsctvo_prstcn = d2.cnsctvo_cdfccn

		/*calcular fecha utilizacion hay ops que no tiene fecha de utilizacion  */
		/*--20150815 se desactiva de acuedo a consulta angela Sandoval las ops siempre tiene fcha hasta y queda en la tabla tbconceptosOPS
		update #tmpServiciosAutorizados
		set fcha_utlzcn_hsta = fcha_imprsn 
		where fcha_utlzcn_hsta is null
		*/

		update #tmpServiciosAutorizados
		set fcha_utlzcn_hsta_txto = convert(char(10),fcha_utlzcn_hsta,120) 
	
		
			

		Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_exto
	End Try

	Begin Catch
			set @mensajeError=''
			Set @mensajeError = @mensajeError + ERROR_PROCEDURE() + ' '+ ERROR_MESSAGE() +  ' Linea: '+ cast(ERROR_LINE() as varchar(5)) ;
			Exec spRegistrarInconsistenciaProceso 1,@mensajeError,@cnsctvo_lg
			Exec spActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
			RAISERROR (@mensajeError, 16, 2) With SETERROR	
	End Catch

END
go
sp_recompile 'spEMCalcularDatosOPSAutorizadas'
go