USE bdProcesosSalud
GO

GRANT SELECT, INSERT, UPDATE ON tbTiposProceso TO autsalud_rol
GRANT SELECT, INSERT, UPDATE ON tbTiposProceso_Vigencias TO autsalud_rol
GRANT SELECT, INSERT, UPDATE ON tbTiposRegistroLogEvento TO autsalud_rol
GRANT SELECT, INSERT, UPDATE ON tbLogEventosProceso TO autsalud_rol
GRANT SELECT, INSERT, UPDATE ON tbInconsistenciasxProceso TO autsalud_rol
GRANT SELECT, INSERT, UPDATE ON tbProcesos TO autsalud_rol
