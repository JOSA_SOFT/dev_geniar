--SCRIPT PARA AGREGAR ITEM AL MENU

USE [BDSeguridad]

DECLARE @consecutivoServicio int
DECLARE @codigoServicio CHAR(2)
DECLARE @nombreServicio VARCHAR(150)
DECLARE @descripcionServicio VARCHAR(150)
DECLARE @consecutivoModulo int
DECLARE @consecutivoPerfil int

Select @consecutivoModulo = cnsctvo_cdgo_mdlo
From tbModulos 
Where cdgo_mdlo = '4000'

SET @consecutivoPerfil = 236
	
	
-- ######## Consulta Radicaciones con Glosas ##########
Set @nombreServicio = 'Consultar Bodega Politica'
Set @descripcionServicio = 'Consulta de Bodegas de Políticas'

Select @consecutivoServicio = (Select Max(cnsctvo_cdgo_srvco) As consecutivo  From tbServicios) + 1
Set  @codigoServicio = cast(@consecutivoServicio As char(4))

INSERT INTO tbServicios
           (cnsctvo_cdgo_srvco
           ,cdgo_srvco
           ,nmbre_srvco
           ,dscrpcn_srvco
           ,inco_vgnca
		   ,fn_vgncia
           ,brrdo
           ,fcha_crcn
           ,usro_crcn
           ,obsrvcns
           ,vsble_usro
           ,cnsctvo_cdgo_mdlo)
     VALUES
           (@consecutivoServicio
           ,@consecutivoServicio
           ,@nombreServicio
           ,@descripcionServicio
           ,'2014-10-21T00:00:00.000'
           ,'9999-12-31T00:00:00.000'
           ,'N'
           ,Getdate()
           ,'geniarjag '
           ,''
           ,'S'
           ,@consecutivoModulo)

INSERT INTO tbServiciosxPerfil
           (cnsctvo_cdgo_srvco
           ,cnsctvo_cdgo_prfl
           ,brrdo
           ,inco_vgnca
           ,fn_vgnca
           ,fcha_crcn
           ,usro_crcn
           ,obsrvcns
           ,vsble_usro)
     VALUES
           (@consecutivoServicio
           ,@consecutivoPerfil
           ,'N'
           ,'2014-10-21T00:00:00.000'
           ,'9999-12-31T00:00:00.000'
           ,getdate()
           ,'geniarjag '
           ,''
           ,'S')
		   
SET @consecutivoPerfil = 246	   
INSERT INTO tbServiciosxPerfil
           (cnsctvo_cdgo_srvco
           ,cnsctvo_cdgo_prfl
           ,brrdo
           ,inco_vgnca
           ,fn_vgnca
           ,fcha_crcn
           ,usro_crcn
           ,obsrvcns
           ,vsble_usro)
     VALUES
           (@consecutivoServicio
           ,@consecutivoPerfil
           ,'N'
           ,'2014-10-21T00:00:00.000'
           ,'9999-12-31T00:00:00.000'
           ,getdate()
           ,'geniarjag '
           ,''
           ,'S')	
