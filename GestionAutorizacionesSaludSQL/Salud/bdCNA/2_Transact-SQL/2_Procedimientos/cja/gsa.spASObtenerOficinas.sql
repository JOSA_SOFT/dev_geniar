USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASObtenerOficinas') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASObtenerOficinas AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASObtenerOficinas
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Se encarga de consultar las oficinas de acuerdo al tipo D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/03/09 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASObtenerOficinas
		EXEC gsa.spASObtenerOficinas null, null, 4, null, null
		EXEC gsa.spASObtenerOficinas '01', null, 4, null, null
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASObtenerOficinas(
	@codigoOficina char(5)				= NULL,
	@descripcionOficina udtDescripcion		= NULL,
	@consecutivoTipoOficina udtConsecutivo	= NULL,
	@consecutivoSede udtConsecutivo			= NULL,
	@fechaConsulta datetime					= NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @descripcionAuxiliar udtDescripcion

	CREATE TABLE #oficinas(
		cdgo_ofcna				char(5), 
		dscrpcn_ofcna			udtDescripcion,	
		cnsctvo_cdgo_ofcna		udtConsecutivo,	
		cnsctvo_cdgo_sde		udtConsecutivo,	
		cdgo_sde				char(3),
		dscrpcn_sde				udtDescripcion,
		cnsctvo_cdgo_tpo_ofcna	udtConsecutivo
	)

	SET @fechaConsulta = ISNULL(@fechaConsulta, getdate())
	SET @descripcionAuxiliar = CONCAT('%',LTRIM(RTRIM(@descripcionOficina)),'%')
	
	INSERT INTO #oficinas(cdgo_ofcna, dscrpcn_ofcna, cnsctvo_cdgo_ofcna, cnsctvo_cdgo_sde, cnsctvo_cdgo_tpo_ofcna)
	SELECT a.cdgo_ofcna, a.dscrpcn_ofcna, a.cnsctvo_cdgo_ofcna, a.cnsctvo_cdgo_sde,	a.cnsctvo_cdgo_tpo_ofcna
	FROM BDAfiliacionValidador.dbo.tbOficinas_Vigencias a with(nolock)
	INNER JOIN BDAfiliacionValidador.dbo.tbOficinas b with(nolock) ON a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	WHERE @fechaConsulta between a.inco_vgnca and a.fn_vgnca

    IF @codigoOficina IS NOT NULL 
	BEGIN
		DELETE
		FROM #oficinas
		WHERE cdgo_ofcna != @codigoOficina
	END 

	IF @descripcionOficina IS NOT NULL 
	BEGIN
		DELETE
		FROM #oficinas
		WHERE dscrpcn_ofcna NOT LIKE @descripcionAuxiliar
	END

	IF @consecutivoTipoOficina IS NOT NULL 
	BEGIN
		DELETE
		FROM #oficinas
		WHERE cnsctvo_cdgo_tpo_ofcna != @consecutivoTipoOficina
	END

	IF @consecutivoSede IS NOT NULL 
	BEGIN
		DELETE
		FROM #oficinas
		WHERE cnsctvo_cdgo_sde != @consecutivoSede
	END
	
	UPDATE a
	SET cdgo_sde = c.cdgo_sde, a.dscrpcn_sde = c.dscrpcn_sde
	FROM #oficinas a
	INNER JOIN BDAfiliacionValidador.dbo.tbSedes b with(nolock) ON a.cnsctvo_cdgo_sde = b.cnsctvo_cdgo_sde
	INNER JOIN BDAfiliacionValidador.dbo.tbSedes_Vigencias c with(nolock) ON c.cnsctvo_cdgo_sde = b.cnsctvo_cdgo_sde
	WHERE @fechaConsulta between c.inco_vgnca and c.fn_vgnca


	SELECT 
		cdgo_ofcna,
		dscrpcn_ofcna,
		cnsctvo_cdgo_ofcna,
		cnsctvo_cdgo_sde,
		cdgo_sde,
		dscrpcn_sde,
		cnsctvo_cdgo_tpo_ofcna
	FROM #oficinas
END
GO
