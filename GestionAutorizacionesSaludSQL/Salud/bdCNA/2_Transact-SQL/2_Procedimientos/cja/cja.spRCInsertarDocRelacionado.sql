USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCInsertarDocRelacionado]    Script Date: 31/10/2016 9:00:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCInsertarDocRelacionado') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCInsertarDocRelacionado AS SELECT 1;'
END
GO
/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCInsertarDocRelacionado
* Desarrollado por : <\A Ing. Jose Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se actualiza el estado a "Cobrado" de los documentos caja.
					 D\>     
	 
* Parametros       : <\P 
						@cnsctvo_cdgo_dcmnto Codigo del documento
						@cnsctvo_cdgo_dcmnto_cja_rlc C�digo de la nota cr�dito
						@usr Usuario conectado.
						
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCInsertarDocRelacionado]
	@cnsctvo_cdgo_dcmnto udtConsecutivo,
	@cnsctvo_cdgo_dcmnto_cja_rlc udtConsecutivo,
	@usr udtUsuario
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @fechaActual datetime

	SET @fechaActual = getdate()

	INSERT INTO [cja].[tbRCDocumentosRelacionados](cnsctvo_cdgo_dcmnto_cja, 
												   cnsctvo_cdgo_dcmnto_cja_rlcndo, 
												   fcha_crcn, usro_crcn, 
												   fcha_ultma_mdfccn, 
												   usro_ultma_mdfccn) 
	values(@cnsctvo_cdgo_dcmnto,@cnsctvo_cdgo_dcmnto_cja_rlc,@fechaActual,@usr,@fechaActual,@usr)
  
END
GO

