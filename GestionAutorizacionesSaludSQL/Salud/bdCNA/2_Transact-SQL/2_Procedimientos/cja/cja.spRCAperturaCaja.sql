USE BDCna
GO
/****** Object:  StoredProcedure cja.spRCAperturaCaja    Script Date: 01/11/2016 09:52:33 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.spRCAperturaCaja') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCAperturaCaja AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCAperturaCaja
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
            Se realiza la apertura de la caja
           D\>     
   
* Parametros       : <\P 
            @fecha fecha y hora actual
            @usro_crcn login de usuario en sesion
            @bse base ingresada
            @cnsctvo_cdgo_ofcna Consecutivo codigo de la oficina seleccionada
            @drccn_ip Direccion IP del equipo desde donde se conectan
            @cnsctvo_cdgo_estdo consecutivo codigo del estado Abierto
           P\>     
* Variables        : <\V                      V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCAperturaCaja
  @bse numeric,
  @usro_crcn udtUsuario,
  @cnsctvo_cdgo_ofcna int,
  @cnsctvo_cdgo_estdo int,
  @drccn_ip varchar(30),
  @fecha datetime = null
AS
BEGIN
  SET NOCOUNT ON

  SET @fecha = ISNULL(@fecha,getdate())

  INSERT INTO cja.tbRCMovimientosCaja
           (bse
           ,fcha_aprtra
           ,fcha_crre
           ,cnsctvo_cdgo_ofcna
           ,cnsctvo_cdgo_estdo
           ,drccn_ip
           ,fltnte_crre
           ,sbrnte_crre
           ,ttl_efctvo
           ,ttl_cja
           ,ttl_nta_crdto
           ,cnsctvo_crre_ofcna
           ,fcha_crcn
           ,usro_crcn
           ,fcha_ultma_mdfccn
           ,usro_ultma_mdfccn)
     VALUES
           (@bse
           ,@fecha
           ,null
           ,@cnsctvo_cdgo_ofcna
           ,@cnsctvo_cdgo_estdo
           ,@drccn_ip
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,@fecha
           ,@usro_crcn
           ,@fecha
           ,@usro_crcn) 


	UPDATE bdSeguridad.dbo.tbusuarios 
	SET cnsctvo_cdgo_ofcna=@cnsctvo_cdgo_ofcna
	WHERE lgn_usro=@usro_crcn

	UPDATE bdSeguridad.dbo.tbusuariosweb 
	SET cnsctvo_cdgo_ofcna=@cnsctvo_cdgo_ofcna
	WHERE  lgn_usro=@usro_crcn

END
GO

