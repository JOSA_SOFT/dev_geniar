USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerMetodoDevolucion') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerMetodoDevolucion AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCObtenerMetodoDevolucion
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Obtiene los metodos de devolucion D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/12/15 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCObtenerMetodoDevolucion 
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCObtenerMetodoDevolucion]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FECHAACTUAL DATETIME
	SET @FECHAACTUAL =getdate()
	
	SELECT a.cnsctvo_cdgo_mtdo_dvlcn_dnro, a.dscrpcn_mtdo_dvlcn_dnro
	FROM cja.tbRCMetodosDevolucionDinero a
	INNER JOIN cja.tbRCMetodosDevolucionDinero_Vigencias b On a.cnsctvo_cdgo_mtdo_dvlcn_dnro = b.cnsctvo_cdgo_mtdo_dvlcn_dnro
	WHERE @FECHAACTUAL between b.inco_vgnca and b.fn_vgnca
END
GO

