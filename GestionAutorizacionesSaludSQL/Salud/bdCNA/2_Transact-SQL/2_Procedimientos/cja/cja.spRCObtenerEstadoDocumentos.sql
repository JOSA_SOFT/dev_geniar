USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerEstadoDocumentos') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerEstadoDocumentos AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCObtenerEstadoDocumentos
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Obtiene los estados de los documentos de acuerdo a tipo de documento. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/01/06 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        declare @fechaActual datetime = getdate(),
				@visible udtLogico = 'S',
				@tipoDocumento udtConsecutivo = 2
		EXEC cja.spRCObtenerEstadoDocumentos '3',null,@fechaActual,@visible,@tipoDocumento
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCObtenerEstadoDocumentos(
	@codigo udtCodigo,
	@descripcion udtDescripcion,
	@fechaReferencia datetime,
	@visibleUsuario udtLogico,
	@tipoDocumento udtConsecutivo
)
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #estados(
		cnsctvo_cdgo_estdo_dcmnto_cja udtConsecutivo,
		cdgo_estdo_dcmnto_cja udtCodigo, 
		dscrpcn_estdo_dcmnto_cja udtDescripcion
	)

	INSERT INTO #estados (cnsctvo_cdgo_estdo_dcmnto_cja, cdgo_estdo_dcmnto_cja, dscrpcn_estdo_dcmnto_cja)
	SELECT a.cnsctvo_cdgo_estdo_dcmnto_cja, a.cdgo_estdo_dcmnto_cja, a.dscrpcn_estdo_dcmnto_cja
	FROM cja.tbRCEstadosDocumentosCaja a WITH(NOLOCK)
	INNER JOIN cja.tbRCEstadosDocumentosCaja_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_estdo_dcmnto_cja = b.cnsctvo_cdgo_estdo_dcmnto_cja
	WHERE @fechaReferencia between b.inco_vgnca and b.fn_vgnca
	AND b.vsble_usro = @visibleUsuario
	AND b.cnsctvo_cdgo_tpo_dcmnto_cja = @tipoDocumento

	IF @codigo IS NOT NULL
	BEGIN
		DELETE FROM #estados
		WHERE cdgo_estdo_dcmnto_cja NOT LIKE CONCAT(@codigo,'%')
	END 

	IF @descripcion IS NOT NULL
	BEGIN
		DELETE FROM #estados
		WHERE dscrpcn_estdo_dcmnto_cja NOT LIKE CONCAT(@descripcion,'%')
	END

	SELECT cnsctvo_cdgo_estdo_dcmnto_cja,cdgo_estdo_dcmnto_cja,dscrpcn_estdo_dcmnto_cja
	FROM #estados

	DROP TABLE #estados
END
GO
