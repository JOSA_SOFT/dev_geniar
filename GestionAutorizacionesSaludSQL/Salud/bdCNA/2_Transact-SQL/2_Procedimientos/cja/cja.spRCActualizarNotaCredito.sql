USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCActualizarNotaCredito') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCActualizarNotaCredito AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCActualizarNotaCredito
* Desarrollado por : <\A Campo para documentar el autor del sp A\>
* Descripcion  : <\D Campo para documentar la descripcion D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC yyyy/mm/dd FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCActualizarNotaCredito ...
    EJ\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta sp para que si esta cobrando una nota credito afecte la caja. DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/05/12 FM\>   
*-----------------------------------------------------------------------------------------------------------------------

*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCActualizarNotaCredito(
	@cnsctvo_cdgo_dcmnto udtConsecutivo,
	@cnsctvo_cdgo_estdo_dcmnto udtConsecutivo,
	@obsrvcn udtObservacion,
	@usro_mdfccn udtUsuario,
	@cnsctvo_cdgo_cntcto udtConsecutivo = null
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @fechaActual datetime,
			@estadoCobrado udtConsecutivo,
			@valorDocumento numeric(18,0),
			@detalleMovimiento udtConsecutivo,
			@valorRecibido numeric(18,0)
	
	SET @fechaActual = getdate()
	SET @estadoCobrado = 3
	SET @valorRecibido = 0
	SET @detalleMovimiento = null

	IF @cnsctvo_cdgo_estdo_dcmnto = @estadoCobrado
	BEGIN
		SELECT @valorDocumento = vlr_dcmnto_cja
		FROM cja.tbRCDocumentosCaja WITH(NOLOCK)
		WHERE cnsctvo_cdgo_dcmnto_cja = @cnsctvo_cdgo_dcmnto

		EXEC cja.spRCGuardarDetMovimientoCaja @valorDocumento, @valorRecibido, @fechaActual, @usro_mdfccn, @fechaActual, @usro_mdfccn, @detalleMovimiento output
	END
	
	UPDATE cja.tbRCDocumentosCaja
	SET cnsctvo_cdgo_estdo_dcmnto_cja = @cnsctvo_cdgo_estdo_dcmnto,
		obsrvcn = @obsrvcn,
		usro_ultma_mdfccn = @usro_mdfccn,
		fcha_ultma_mdfccn = @fechaActual,
		cnsctvo_dtlle_mvmnto_cja = @detalleMovimiento
	WHERE cnsctvo_cdgo_dcmnto_cja = @cnsctvo_cdgo_dcmnto

	IF @cnsctvo_cdgo_cntcto IS NOT NULL 
	BEGIN
		UPDATE cja.tbRCDocumentosCaja
		SET cnsctvo_dto_cntcto = @cnsctvo_cdgo_cntcto
		WHERE cnsctvo_cdgo_dcmnto_cja = @cnsctvo_cdgo_dcmnto
	END
END
GO
