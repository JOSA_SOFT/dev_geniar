USE [bdCNA]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarCoordinador]    Script Date: 17/05/2017 11:10:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarCoordinador
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
    			         Procedimiento que permite consultar la informacion del coordinador
					 D\>     
* Parametros       : <\P 
						@lgn_usro login de usuario en sesion
					 P\>     
* Variables        : <\V  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D
                            Se realiza modificacion en el procedimiento para que realice la validacion del usuario con 
							el lgn_usro y no con el usuario de creacion
                        D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 17/05/2017  FM\>    
*------------------------------------------------------------------------------------------------------------------------------*/

--exec [cja].[spRCConsultarCoordinador] 'sisaps01'

ALTER PROCEDURE [cja].[spRCConsultarCoordinador]
	@lgn_usro udtUsuario
AS
BEGIN
    SET NOCOUNT ON
	 
	DECLARE @fechaActual date
	
	SET @fechaActual = getDate()
		
	Select     a.cnsctvo_cdgo_crdndr_asi, a.cdgo_crdndr_asi, a.cnsctvo_cdgo_ofcna,
		       b.dscrpcn_ofcna          , a.lgn_usro
    From       cja.tbRCCoordinadorAsixOficina_Vigencias a With(NoLock)
	Inner Join bdAfiliacionValidador.dbo.tbOficinas b With(NoLock) 
	On         b.cnsctvo_cdgo_ofcna =a.cnsctvo_cdgo_ofcna
    Where	   a.lgn_usro = @lgn_usro
	And        @fechaActual Between a.inco_vgnca And a.fn_vgnca 

END

