USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultaOPSValorNotasCredito]    Script Date: 14/11/2016 05:56:59 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaOPSValorNotasCredito') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaOPSValorNotasCredito AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaOPSValorNotasCredito
* Desarrollado por : <\A JORGE - GENIAR A\>
* Descripcion  : <\D Consulta grupo de OPS y su valor D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P  @nmro_dcmnto numero de documento P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/14 FC\>
* Ejemplo: 
    <\EJ
        EXEC BDCna.cja.spRCConsultaOPSValorNotasCredito 32
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCConsultaOPSValorNotasCredito]
	@nmro_dcmnto udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @tpo_dcmnto udtConsecutivo	

	CREATE TABLE #ops_detalle(
		cnsctvo_cdgo_dcmnto_cja udtConsecutivo null,
		nmro_unco_ops udtConsecutivo null,
		vlr_dcmnto_cja numeric(18,0) null
	)

	SET @tpo_dcmnto = 2

	INSERT INTO #ops_detalle (cnsctvo_cdgo_dcmnto_cja,nmro_unco_ops,vlr_dcmnto_cja) 
	SELECT a.cnsctvo_cdgo_dcmnto_cja, b.nmro_unco_ops, a.vlr_dcmnto_cja
	FROM cja.tbRCDocumentosCaja a WITH(NOLOCK)
	INNER JOIN cja.tbRCSoportesDocumentosCaja b WITH(NOLOCK) ON a.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja
	WHERE a.nmro_dcmnto = @nmro_dcmnto
	AND a.cnsctvo_cdgo_tpo_dcmnto_cja = @tpo_dcmnto


	SELECT 
		cnsctvo_cdgo_dcmnto_cja,vlr_dcmnto_cja,
		STUFF((
		SELECT ', ' + CAST(nmro_unco_ops AS VARCHAR(MAX)) 
		FROM #ops_detalle 
		WHERE (cnsctvo_cdgo_dcmnto_cja = Results.cnsctvo_cdgo_dcmnto_cja) 
		FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
		,1,2,'') AS nmro_unco_ops_grupo
	FROM #ops_detalle Results
	GROUP BY cnsctvo_cdgo_dcmnto_cja,vlr_dcmnto_cja

	DROP TABLE #ops_detalle
END
GO

