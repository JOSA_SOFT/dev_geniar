USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarMovimientosCajaPorCierrerOficina]    Script Date: 10/11/2016 02:02:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCConsultarMovimientosCajaPorCierrerOficina]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCConsultarMovimientosCajaPorCierrerOficina] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarMovimientosCajaPorCierrerOficina
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
            Consulta los movimientos caja por el consecutivo
            de cierre oficina
           D\>     
   
* Parametros       : <\P 
            @cnsctvo_crre_ofcna consecutivo cierre oficina
           P\>     
* Variables        : <\V                      V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCConsultarMovimientosCajaPorCierrerOficina
  @cnsctvo_crre_ofcna udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON

  SELECT a.cnsctvo_mvmnto_cja
      ,a.bse]
      ,a.fcha_aprtra]
      ,a.fcha_crre]
      ,a.cnsctvo_cdgo_ofcna]
      ,a.cnsctvo_cdgo_estdo]
      ,b.dscrpcn_estdo]
      ,a.drccn_ip]
      ,a.fltnte_crre]
      ,a.sbrnte_crre]
      ,a.ttl_efctvo]
      ,a.ttl_cja]
      ,a.ttl_nta_crdto]
      ,a.cnsctvo_crre_ofcna]
      ,a.fcha_crcn]
      ,a.usro_crcn]
      ,a.fcha_ultma_mdfccn]
      ,a.usro_ultma_mdfccn]
  FROM cja.tbRCMovimientosCaja a WITH(NOLOCK)
     inner join cja.tbRCEstadosMovimientosCaja b WITH(NOLOCK) on a.cnsctvo_cdgo_estdo = b.cnsctvo_cdgo_estdo
  WHERE a.cnsctvo_crre_ofcna = @cnsctvo_crre_ofcna
  ORDER BY a.fcha_aprtra DESC

END
GO

