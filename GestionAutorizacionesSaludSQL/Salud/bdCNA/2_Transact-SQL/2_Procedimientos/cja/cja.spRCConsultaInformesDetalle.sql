USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaInformesDetalle') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaInformesDetalle AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaInformesDetalle
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D sp que genera el reporte detallado de las cajas D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC yyyy/mm/dd FC\>
* Ejemplo: 
    <\EJ
        DECLARE 
			@fcha_incl datetime,
			@nui udtConsecutivo,
			@cnsctvo_cdgo_ofcna udtUsuario,
			@lgn udtUsuario,
			@nmro_ops udtConsecutivo,
			@nro_rcbo udtConsecutivo,
			@cnsctvo_cdgo_sde udtConsecutivo

		SET @nui = null
		SET @cnsctvo_cdgo_ofcna = null
		SET @lgn = null
		SET @fcha_incl = getdate()
		SET @nmro_ops = null
		SET @nro_rcbo = null

        EXEC cja.spRCConsultaInformesDetalle 
			@fcha_incl,
			@nui,
			@cnsctvo_cdgo_ofcna,
			@lgn,
			@nmro_ops,
			@nro_rcbo,
			@cnsctvo_cdgo_sde

    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCConsultaInformesDetalle](
	@fcha_incl datetime,
	@nui udtConsecutivo = null,
	@cnsctvo_cdgo_ofcna udtConsecutivo = null,
	@lgn udtUsuario = null,
	@nmro_ops udtConsecutivo = null,
	@nro_rcbo udtConsecutivo = null,
	@cnsctvo_cdgo_sde udtConsecutivo = null
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @fechaCero datetime,
		    @fechaFin datetime,
			@TIPO_DOCUMENTO_RECIBO_CAJA udtConsecutivo

	CREATE TABLE #informacionMovimiento(
		cnsctvo_cdgo_ofcna udtConsecutivo NULL,
		fcha_gnrcn datetime NULL,
		nmro_rcbo udtConsecutivo NULL,
		nmro_unco_ops udtConsecutivo NULL,
		vlr_ttl_rcbo numeric(18,0) NULL,
		dscrpcn_ofcna udtDescripcion NULL,
		dscrpcn_sde udtDescripcion NULL,
		nmro_unco_idntfccn_afldo udtConsecutivo NULL,
		usro udtUsuario NULL,
		cnsctvo_cdgo_sde udtConsecutivo NULL,
		dscrpcn_afldo varchar(500)
	)

	SET @fechaCero = CAST(convert(varchar(10),@fcha_incl,112) AS date)
	SET @fechaFin = DATEADD(s,-1,(@fechaCero+1))
	SET @TIPO_DOCUMENTO_RECIBO_CAJA = 1

	INSERT INTO #informacionMovimiento (cnsctvo_cdgo_ofcna, fcha_gnrcn, nmro_rcbo, nmro_unco_ops, vlr_ttl_rcbo, nmro_unco_idntfccn_afldo, usro, cnsctvo_cdgo_sde, dscrpcn_ofcna)
	SELECT a.cnsctvo_cdgo_ofcna, c.fcha_crcn, c.nmro_dcmnto, d.nmro_unco_ops, b.vlr_rcbdo, c.nmro_unco_idntfccn_afldo, c.usro_ultma_mdfccn, e.cnsctvo_cdgo_sde, e.dscrpcn_ofcna
	FROM cja.tbRCMovimientosCaja a WITH(NOLOCK)
		INNER JOIN cja.tbRCDetMovimientosCaja b WITH(NOLOCK) On a.cnsctvo_mvmnto_cja = b.cnsctvo_mvmnto_cja
		INNER JOIN cja.tbRCDocumentosCaja c WITH(NOLOCK) On b.cnsctvo_dtlle_mvmnto_cja = c.cnsctvo_dtlle_mvmnto_cja
		INNER JOIN cja.tbRCSoportesDocumentosCaja d WITH(NOLOCK) On c.cnsctvo_cdgo_dcmnto_cja = d.cnsctvo_dcmnto_cja
		INNER JOIN BDAfiliacionValidador.dbo.tbOficinas_Vigencias e WITH(NOLOCK) On e.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	WHERE a.fcha_crre between @fechaCero and @fechaFin
	AND c.cnsctvo_cdgo_tpo_dcmnto_cja = @TIPO_DOCUMENTO_RECIBO_CAJA
	
	IF @nmro_ops IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE nmro_unco_ops != @nmro_ops
	END

	IF @nui IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE nmro_unco_idntfccn_afldo != @nui
	END

	IF @nro_rcbo IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE nmro_rcbo != @nro_rcbo
	END

	IF @lgn IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE usro != @lgn
	END

	IF @cnsctvo_cdgo_ofcna IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE cnsctvo_cdgo_ofcna != @cnsctvo_cdgo_ofcna
	END	

	IF @cnsctvo_cdgo_sde IS NOT NULL
	BEGIN
		DELETE 
		FROM #informacionMovimiento
		WHERE cnsctvo_cdgo_sde != @cnsctvo_cdgo_sde
	END	

	UPDATE a
	SET a.dscrpcn_sde = c.dscrpcn_sde
	FROM #informacionMovimiento a 
	INNER JOIN BDAfiliacionValidador.dbo.tbSedes c WITH(NOLOCK) On c.cnsctvo_cdgo_sde = a.cnsctvo_cdgo_sde

	UPDATE a
	SET a.dscrpcn_afldo = c.cdgo_tpo_idntfccn + ' - ' + b.nmro_idntfccn
	FROM #informacionMovimiento a 
	INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b WITH(NOLOCK) On a.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	INNER JOIN BDAfiliacionValidador.dbo.tbTiposIdentificacion c WITH(NOLOCK) On b.cnsctvo_cdgo_tpo_idntfccn = c.cnsctvo_cdgo_tpo_idntfccn

	SELECT 
		cnsctvo_cdgo_ofcna, 
		fcha_gnrcn, nmro_rcbo, 
		vlr_ttl_rcbo, 
		dscrpcn_ofcna, 
		dscrpcn_sde, 
		usro, 
		dscrpcn_afldo, 
		nmro_unco_ops
	FROM #informacionMovimiento Results

	DROP TABLE #informacionMovimiento
END
GO

