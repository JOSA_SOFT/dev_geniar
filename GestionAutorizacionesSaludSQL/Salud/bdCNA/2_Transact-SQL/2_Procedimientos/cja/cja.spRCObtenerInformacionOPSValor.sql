USE [bdCNA]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerInformacionOPSValor]    Script Date: 01/06/2017 12:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------------------   
* Metodo o PRG     :  spRCObtenerInformacionOPSValor
* Desarrollado por : <\A Ing. José Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Método encargado de consultar el número de la OPS y el valor que tiene, 
						según el número de solicitud enviado.
					 D\>     
* Parametros       : <\P 
						@user_name: Nombre de usuario logeado en la aplicación.
						retorna el código de la OPS y el valor de este.
					 P\>     
* Variables        : <\V V\>     
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp para que identifique si hay alguna de las OPS que se encarga de 
                            pagar la IPS 
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp que no tome las cuotas de recuperacion anuladas
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/
/*
 declare @muestraMensajeIPS char(2), @generaReciboCaja  Char(2)
 exec cja.spRCObtenerInformacionOPSValor 2380, @muestraMensajeIPS output, @generaReciboCaja output
 select @muestraMensajeIPS, @generaReciboCaja
*/

ALTER PROCEDURE [cja].[spRCObtenerInformacionOPSValor]
	@numeroSolicitud   udtConsecutivo,
	@muestraMensajeIPS Char(2) Output,
	@generaReciboCaja  Char(2) Output
AS
BEGIN
	SET NOCOUNT ON;

	Declare @codigo_conse_medio_pago       udtConsecutivo,
		    @vlr_cero                      udtConsecutivo,
		    @consecutivo_estado_autorizada udtConsecutivo,
		    @codigo_conse_medio_pago_ips   udtConsecutivo,
		    @no                            Char(2)       ,
		    @si                            Char(2)       ,
		    @fechaActual                   Datetime      ,
		    @recibosGenerados              Int           ,
		    @tipo_documento_caja           udtConsecutivo,
			@estdo_cta_rcprcn_anldo        udtConsecutivo

	Create 
	Table  #infoValorOPS(nmro_unco_ops                  udtConsecutivo, 
		                 vlr_lqdcn_cta_rcprcn_srvco_ops Numeric(18,9) , 
		                 cnsctvo_cdgo_cncpto_pgo        udtConsecutivo, 
		                 dscrpcn_cncpto_pgo             udtDescripcion
	                    )

	Create 
	Table  #infoValorOPSCualquierMedio(nmro_unco_ops                        udtConsecutivo, 
		                               vlr_lqdcn_cta_rcprcn_srvco_ops       Numeric(18,9) , 
		                               cnsctvo_cdgo_cncpto_pgo              udtConsecutivo, 
		                               dscrpcn_cncpto_pgo                   udtDescripcion,
		                               cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn udtConsecutivo
 	                                  )

	Create 
	Table  #pagoIPS(nmro_unco_ops udtConsecutivo)
	
	Set @codigo_conse_medio_pago       = 1 --Cobro EPS 
	Set @vlr_cero                      = 0 --numero cero
	Set @consecutivo_estado_autorizada = 11 --Estado autorizada.
	Set @codigo_conse_medio_pago_ips   = 2
	Set @no                            = 'NO'
	Set @si                            = 'SI'
	Set @fechaActual                   = getdate()
	Set @tipo_documento_caja           = 1
	Set @estdo_cta_rcprcn_anldo        = 4 --Cuota de recuperacion Anulado

	--Procedimientos.
	Insert 
	Into       #infoValorOPSCualquierMedio(nmro_unco_ops     , vlr_lqdcn_cta_rcprcn_srvco_ops      , cnsctvo_cdgo_cncpto_pgo, 
	                                       dscrpcn_cncpto_pgo, cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn
									      )
	Select     Distinct cro.nmro_unco_ops   , cro.vlr_lqdcn_cta_rcprcn_srvco_ops      , cro.cnsctvo_cdgo_cncpto_pgo, 
	                    c.dscrpcn_cncpto_pgo, cro.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn
	FROM       bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps cro With(Nolock) 
	Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(Nolock) 
	On         ss.cnsctvo_slctd_atrzcn_srvco = cro.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(Nolock)
	On         pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado b With(Nolock) 
	On         b.cnsctvo_prcdmnto_insmo_slctdo = PIS.cnsctvo_prcdmnto_insmo_slctdo 
	Inner Join bdsisalud.dbo.tbConceptosPago_Vigencias c With(Nolock)
	On         c.cnsctvo_cdgo_cncpto_pgo = cro.cnsctvo_cdgo_cncpto_pgo
	Where      cro.cnsctvo_slctd_atrzcn_srvco      = @numeroSolicitud 
	And        ss.cnsctvo_cdgo_estdo_srvco_slctdo  = @consecutivo_estado_autorizada
	And        cro.cnsctvo_cdgo_estdo_cta_rcprcn  != @estdo_cta_rcprcn_anldo
	And        cro.nmro_unco_ops                  != @vlr_cero	
	And        @fechaActual Between c.inco_vgnca And c.fn_vgnca

	--Medicamentos.
	Insert 
	Into       #infoValorOPSCualquierMedio(nmro_unco_ops     , vlr_lqdcn_cta_rcprcn_srvco_ops      , cnsctvo_cdgo_cncpto_pgo, 
	                                       dscrpcn_cncpto_pgo, cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn
										  )
	SELECT     Distinct cro.nmro_unco_ops, cro.vlr_lqdcn_cta_rcprcn_srvco_ops      , cro.cnsctvo_cdgo_cncpto_pgo, 
	           c.dscrpcn_cncpto_pgo      , cro.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn
	FROM       bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps cro With(Nolock) 
	Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(Nolock) 
	On         ss.cnsctvo_slctd_atrzcn_srvco = cro.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASMedicamentosSolicitados med With(Nolock) 
	On         med.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado b With(Nolock) 
	On         b.cnsctvo_mdcmnto_slctdo = med.cnsctvo_mdcmnto_slctdo 
	Inner Join bdsisalud.dbo.tbConceptosPago_Vigencias c With(Nolock)  
	On         c.cnsctvo_cdgo_cncpto_pgo = cro.cnsctvo_cdgo_cncpto_pgo
	Where      cro.cnsctvo_slctd_atrzcn_srvco     = @numeroSolicitud
	And        ss.cnsctvo_cdgo_estdo_srvco_slctdo = @consecutivo_estado_autorizada
	And        cro.cnsctvo_cdgo_estdo_cta_rcprcn  <> @estdo_cta_rcprcn_anldo
	And        cro.nmro_unco_ops                  <> @vlr_cero
	And        @fechaActual Between c.inco_vgnca And c.fn_vgnca
	
	Insert 
	Into    #infoValorOPS(nmro_unco_ops          , vlr_lqdcn_cta_rcprcn_srvco_ops, 
	                      cnsctvo_cdgo_cncpto_pgo, dscrpcn_cncpto_pgo
						 )
	Select  nmro_unco_ops, vlr_lqdcn_cta_rcprcn_srvco_ops, cnsctvo_cdgo_cncpto_pgo, dscrpcn_cncpto_pgo
	From    #infoValorOPSCualquierMedio
	Where   cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn = @codigo_conse_medio_pago
	And     vlr_lqdcn_cta_rcprcn_srvco_ops       > @vlr_cero

	Select      @recibosGenerados = count(1)
	From        #infoValorOPS a
	Inner Join  bdCNA.cja.tbRCSoportesDocumentosCaja b With(Nolock) 
	On          b.nmro_unco_ops = a.nmro_unco_ops
	Inner Join  bdCNA.cja.tbRCDocumentosCaja c With(Nolock) 
	On          c.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja
	Where       c.cnsctvo_cdgo_tpo_dcmnto_cja = @tipo_documento_caja

	If Exists (Select nmro_unco_ops
			   From   #infoValorOPSCualquierMedio
			   Where  cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn = @codigo_conse_medio_pago_ips)
		Begin
			Set @muestraMensajeIPS = @si
		End
	Else
		Begin
			Set @muestraMensajeIPS = @no
		End

	If @recibosGenerados > @vlr_cero
		Begin
			Set @generaReciboCaja = @no
		End
	Else
		Begin
			SET @generaReciboCaja = @si
		End

	Select   nmro_unco_ops          , vlr_lqdcn_cta_rcprcn_srvco_ops, 
	         cnsctvo_cdgo_cncpto_pgo, dscrpcn_cncpto_pgo
	From     #infoValorOPS
	Order By nmro_unco_ops

	Drop Table #infoValorOPS
	Drop Table #pagoIPS
	Drop Table #infoValorOPSCualquierMedio
END
