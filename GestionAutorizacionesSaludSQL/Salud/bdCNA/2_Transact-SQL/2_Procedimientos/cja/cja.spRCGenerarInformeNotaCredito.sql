USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCGenerarInformeNotaCredito') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCGenerarInformeNotaCredito AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCGenerarInformeNotaCredito
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Consulta las notas credito generadas D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/01/11 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
		DECLARE @fechaInicial datetime,
				@fechaFinal datetime

		SET @fechaInicial = getdate()-180
		SET @fechaFinal = getdate()+1
        EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, 31803456
		EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, 3180345
		EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, null, 2
		EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, null, 3
		EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, 31803456, 3
		EXEC cja.spRCGenerarInformeNotaCredito @fechaInicial, @fechaFinal, 31803456, 2
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCGenerarInformeNotaCredito (
	@fechaInicial datetime,
	@fechaFinal datetime,
	@nuiAfiliado udtConsecutivo = null,
	@estadoNotaCredito udtConsecutivo = null
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fechaActual datetime,
			@NOTA_CREDITO udtConsecutivo,
			@fechaCero datetime,
			@fechaFin datetime	

	CREATE TABLE #notasCredito(
		cnsctvo_cdgo_dcmnto_cja		udtConsecutivo,
		nmro_dcmnto					int, 
		fcha_crcn					datetime, 
		dscrpcn_estdo_dcmnto_cja	udtDescripcion, 
		nmro_unco_idntfccn_afldo	udtConsecutivo, 
		vlr_dcmnto_cja				numeric(18,0),
		nmbre_cmplto_afldo			udtDescripcion,
		tpo_idntfccn_afldo			udtDescripcion,
		nmro_idntfccn_afldo			udtDescripcion,
		cnsctvo_cdgo_estdo_dcmnto_cja	udtConsecutivo,
		cnsctvo_cdgo_cdgo_tpo_dcmnto	udtConsecutivo,
		cnsctvo_dto_cntcto				udtConsecutivo,
		obsrvcn							udtObservacion
	)

	SET @fechaCero = CAST(convert(varchar(10),@fechaFinal,112) AS date)
	SET @fechaFin = DATEADD(s,-1,(@fechaCero+1))

	SET @fechaActual = getdate()
	SET @NOTA_CREDITO = 2
	
	INSERT INTO #notasCredito (cnsctvo_cdgo_dcmnto_cja, nmro_dcmnto, fcha_crcn, dscrpcn_estdo_dcmnto_cja, 
							   nmro_unco_idntfccn_afldo, vlr_dcmnto_cja, cnsctvo_cdgo_estdo_dcmnto_cja, 
							   cnsctvo_dto_cntcto, obsrvcn)
	SELECT a.cnsctvo_cdgo_dcmnto_cja, a.nmro_dcmnto, a.fcha_crcn, b.dscrpcn_estdo_dcmnto_cja, 
		   a.nmro_unco_idntfccn_afldo, a.vlr_dcmnto_cja, a.cnsctvo_cdgo_estdo_dcmnto_cja, 
		   a.cnsctvo_dto_cntcto, a.obsrvcn
	FROM cja.tbRCDocumentosCaja a WITH(NOLOCK)
		INNER JOIN cja.tbRCEstadosDocumentosCaja b WITH(NOLOCK) On a.cnsctvo_cdgo_estdo_dcmnto_cja = b.cnsctvo_cdgo_estdo_dcmnto_cja
		INNER JOIN cja.tbRCEstadosDocumentosCaja_Vigencias c WITH(NOLOCK) On b.cnsctvo_cdgo_estdo_dcmnto_cja = c.cnsctvo_cdgo_estdo_dcmnto_cja
	WHERE @fechaActual between c.inco_vgnca and c.fn_vgnca
	AND a.fcha_crcn between @fechaInicial and @fechaFin
	AND a.cnsctvo_cdgo_tpo_dcmnto_cja = @NOTA_CREDITO

	IF @nuiAfiliado IS NOT NULL
	BEGIN
		DELETE FROM #notasCredito
		WHERE nmro_unco_idntfccn_afldo != @nuiAfiliado
	END

	IF @estadoNotaCredito IS NOT NULL 
	BEGIN
		DELETE FROM #notasCredito
		WHERE cnsctvo_cdgo_estdo_dcmnto_cja != @estadoNotaCredito
	END

	UPDATE a
	SET nmbre_cmplto_afldo = CONCAT(ltrim(rtrim(b.prmr_nmbre)),' ',ltrim(rtrim(b.sgndo_nmbre)),' ',ltrim(rtrim(b.prmr_aplldo)),' ',ltrim(rtrim(b.sgndo_aplldo))),
		nmro_idntfccn_afldo = b.nmro_idntfccn,
		tpo_idntfccn_afldo = c.cdgo_tpo_idntfccn,
		cnsctvo_cdgo_cdgo_tpo_dcmnto = c.cnsctvo_cdgo_tpo_idntfccn
	FROM #notasCredito a
	INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b WITH(NOLOCK) On a.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	INNER JOIN BDAfiliacionValidador.dbo.tbTiposIdentificacion c WITH(NOLOCK) On b.cnsctvo_cdgo_tpo_idntfccn = c.cnsctvo_cdgo_tpo_idntfccn

	SELECT	cnsctvo_cdgo_dcmnto_cja, 
			nmro_dcmnto, 
			fcha_crcn, 
			dscrpcn_estdo_dcmnto_cja, 
			nmro_unco_idntfccn_afldo, 
			vlr_dcmnto_cja, 
			nmbre_cmplto_afldo, 
			tpo_idntfccn_afldo,
			nmro_idntfccn_afldo,
			cnsctvo_cdgo_estdo_dcmnto_cja,
			cnsctvo_cdgo_cdgo_tpo_dcmnto,
			cnsctvo_dto_cntcto,
			obsrvcn
	FROM #notasCredito
	ORDER BY cnsctvo_cdgo_estdo_dcmnto_cja, fcha_crcn DESC

	DROP TABLE #notasCredito

END
GO
