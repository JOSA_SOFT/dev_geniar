USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCCierreCaja]    Script Date: 21/11/2016 16:44:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCCierreCaja') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCCierreCaja AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCCierreCaja
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Se realiza el cierre de la caja
					 D\>     
	 
* Parametros       : <\P 
						@cnsctvo_mvmnto_cja consecutivo caja
						@usro_ultma_mdfccn ultimo usuario de modificacion
						@ttl_efctvo total efectivo caja
						@ttl_nta_crdto total nota credito
						@ttl_cja total caja
						@sbrnte_crre total sobrante
						@fltnte_crre total faltante
						@causales XML de causales

Ejemplo XML:
Declare @causalesList varchar(max) =
'
	<causales>
		<causal>
			<cnsctvo_mvmnto_cja>28</cnsctvo_mvmnto_cja>
			<cnsctvo_cdgo_csl_dscdre>1</cnsctvo_cdgo_csl_dscdre>
			<vlr_dscdre>1</vlr_dscdre>
			<usro_crcn>user1</usro_crcn>
		</causal>
		<causal>
			<cnsctvo_mvmnto_cja>28</cnsctvo_mvmnto_cja>
			<cnsctvo_cdgo_csl_dscdre>1</cnsctvo_cdgo_csl_dscdre>
			<vlr_dscdre>1</vlr_dscdre>
			<usro_crcn>user1</usro_crcn>
		</causal>
	</causales>
'

exec cja.[spRCCierreCaja] null,null,null,null,null,null,null,@causalesList
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     

*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCCierreCaja]
	@cnsctvo_mvmnto_cja udtConsecutivo,
	@usro_ultma_mdfccn udtUsuario,
	@ttl_efctvo numeric,
	@ttl_nta_crdto numeric,
	@ttl_cja numeric,
	@sbrnte_crre numeric,
	@fltnte_crre numeric,
	@causales xml
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@fechaActual datetime,
		@cnsctvo_cdgo_estdo int,
		@idoc int

	CREATE TABLE #CausalesTemp(cnsctvo_mvmnto_cja int, cnsctvo_cdgo_csl_dscdre udtConsecutivo, vlr_dscdre numeric, fcha_crcn datetime, usro_crcn udtUsuario)
	SET @fechaActual = getDate()
	SET @cnsctvo_cdgo_estdo = 2


		UPDATE [cja].[tbRCMovimientosCaja]
		   SET [fcha_crre] = @fechaActual
			  ,[cnsctvo_cdgo_estdo] = @cnsctvo_cdgo_estdo
			  ,[fltnte_crre] = @fltnte_crre
			  ,[sbrnte_crre] = @sbrnte_crre
			  ,[ttl_efctvo] = @ttl_efctvo
			  ,[ttl_cja] = @ttl_cja
			  ,[ttl_nta_crdto] = @ttl_nta_crdto
			  ,[fcha_ultma_mdfccn] = @fechaActual
			  ,[usro_ultma_mdfccn] = @usro_ultma_mdfccn
		WHERE cnsctvo_mvmnto_cja = @cnsctvo_mvmnto_cja

	IF @causales IS NOT NULL
	BEGIN
		
		INSERT INTO	#CausalesTemp (cnsctvo_mvmnto_cja , cnsctvo_cdgo_csl_dscdre , vlr_dscdre, fcha_crcn , usro_crcn)
		SELECT 
			pref.value('(cnsctvo_mvmnto_cja/text())[1]', 'int') AS cnsctvo_mvmnto_cja,
			pref.value('(cnsctvo_cdgo_csl_dscdre/text())[1]', 'udtConsecutivo') AS cnsctvo_cdgo_csl_dscdre,
			pref.value('(vlr_dscdre/text())[1]', 'numeric') AS vlr_dscdre,
			GETDATE() as fcha_crcn,
			pref.value('(usro_crcn/text())[1]', 'udtUsuario') AS usro_crcn
		FROM @causales.nodes('/causales/causal') AS xml_slctd (Pref);

		INSERT INTO	tbRCMovimientosCajaxCausalesDescuadre ( cnsctvo_mvmnto_cja , cnsctvo_cdgo_csl_dscdre , vlr_dscdre, fcha_crcn , usro_crcn )
			SELECT	cnsctvo_mvmnto_cja , cnsctvo_cdgo_csl_dscdre , vlr_dscdre, GETDATE() as fcha_crcn , usro_crcn 
			FROM	#CausalesTemp
		
	END
		
END
GO

