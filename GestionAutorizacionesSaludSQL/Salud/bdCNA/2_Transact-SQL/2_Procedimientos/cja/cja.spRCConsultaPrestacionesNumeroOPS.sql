USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaPrestacionesNumeroOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaPrestacionesNumeroOPS AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaPrestacionesNumeroOPS
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@gmail.com> A\>
* Descripcion  : <\D Se encarga de consultar las prestaciones asociadas a un numero unico OPS. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/12/20 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCConsultaPrestacionesNumeroOPS 1234
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCConsultaPrestacionesNumeroOPS(
	@nmro_unco_ops udtConsecutivo
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FECHA_ACTUAL datetime

	SET @FECHA_ACTUAL = getdate()

	SELECT distinct tc.dscrpcn_tpo_cdfccn, c.cdgo_cdfccn as cdgo_srvco_slctdo, ss.dscrpcn_srvco_slctdo, f.dscrpcn_estdo_srvco_slctdo
	FROM gsa.TbAsConsolidadoCuotaRecuperacionxOps cro with(nolock) 
	INNER JOIN gsa.tbASServiciosSolicitados ss with(nolock) on cro.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN bdSiSalud.dbo.tbcodificaciones c with(nolock) on ss.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdfccn
	INNER JOIN bdSiSalud.dbo.tbTipoCodificacion_Vigencias tc with(nolock) on tc.cnsctvo_cdgo_tpo_cdfccn = c.cnsctvo_cdgo_tpo_cdfccn
	INNER JOIN gsa.tbASConceptosServicioSolicitado d with(nolock) on cro.nmro_unco_ops = d.nmro_unco_ops and d.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN prm.tbASEstadosServiciosSolicitados e with(nolock) on ss.cnsctvo_cdgo_estdo_srvco_slctdo = e.cnsctvo_cdgo_estdo_srvco_slctdo
	INNER JOIN prm.tbASEstadosServiciosSolicitados_Vigencias f on e.cnsctvo_cdgo_estdo_srvco_slctdo = f.cnsctvo_cdgo_estdo_srvco_slctdo
	WHERE cro.nmro_unco_ops = @nmro_unco_ops
	AND @FECHA_ACTUAL between tc.inco_vgnca AND tc.fn_vgnca 
	AND @FECHA_ACTUAL between f.inco_vgnca AND f.fn_vgnca
END
GO
