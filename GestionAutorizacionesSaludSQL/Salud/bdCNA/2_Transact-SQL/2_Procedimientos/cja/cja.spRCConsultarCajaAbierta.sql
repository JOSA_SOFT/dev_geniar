USE BDCna
GO
/****** Object:  StoredProcedure cja.spRCConsultarCajaAbierta    Script Date: 14/12/2016 9:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultarCajaAbierta') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultarCajaAbierta AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarCajaAbierta
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Se realiza la apertura de la caja
					 D\>     
	 
* Parametros       : <\P 
						@usro_crcn login de usuario en sesion
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCConsultarCajaAbierta
	@usro_crcn udtUsuario
AS
BEGIN
SET NOCOUNT ON
  
  DECLARE
  	@ID_ESTADO_ABIERTO udtConsecutivo,
  	@ID_ESTADO_CIERRE_ADMINISTRATIVO udtConsecutivo
  	SET @ID_ESTADO_ABIERTO = 1
  	SET @ID_ESTADO_CIERRE_ADMINISTRATIVO = 4

	SELECT cnsctvo_mvmnto_cja
      ,bse
      ,fcha_aprtra
      ,fcha_crre
      ,cnsctvo_cdgo_ofcna
      ,cnsctvo_cdgo_estdo
      ,drccn_ip
      ,fltnte_crre
      ,sbrnte_crre
      ,ttl_efctvo
      ,ttl_cja
      ,ttl_nta_crdto
      ,cnsctvo_crre_ofcna
      ,fcha_crcn
      ,usro_crcn
      ,fcha_ultma_mdfccn
      ,usro_ultma_mdfccn
  FROM cja.tbRCMovimientosCaja WITH(NOLOCK)
  WHERE usro_crcn = @usro_crcn 
  AND (cnsctvo_cdgo_estdo = @ID_ESTADO_ABIERTO
	 OR cnsctvo_cdgo_estdo = @ID_ESTADO_CIERRE_ADMINISTRATIVO)

END
GO