USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCBuscadorUsuarioWeb') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCBuscadorUsuarioWeb AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCBuscadorUsuarioWeb
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Buscador de usuarios a partir de nombre o login D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/12/06 FC\>
* Ejemplo: 
    <\EJ
        EXEC cja.spRCBuscadorUsuarioWeb 'user',null
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCBuscadorUsuarioWeb](
	@lgn_usro udtUsuario = null,
	@nombreCompleto varchar(140) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE  @lgn_usro1      Varchar(30),  
			 @nombreCompleto1 varchar(140)

	CREATE TABLE #usuarios(
		prmr_nmbre_usro udtNombre, 
		sgndo_nmbre_usro udtNombre, 
		prmr_aplldo_usro udtApellido, 
		sgndo_aplldo_usro udtApellido, 
		lgn_usro udtUsuario
	)

	CREATE TABLE #datosUsuarios(
		lgn_usro          udtUsuario,
		prmr_nmbre_usro   udtNombre, 
		sgndo_nmbre_usro  udtNombre, 
		prmr_aplldo_usro  udtApellido, 
		sgndo_aplldo_usro udtApellido, 
		nmbre_cmplto      varchar(140)
	)

	SET @lgn_usro1 = ltrim(rtrim(@lgn_usro))              --Este campo pueden llegar desde la aplicacion sin espacios
	SET @nombreCompleto1 = ltrim(rtrim(@nombreCompleto))  --Este campo pueden llegar desde la aplicacion sin espacios

	Insert 
	Into    #datosUsuarios (lgn_usro        , prmr_nmbre_usro  , sgndo_nmbre_usro, 
	                        prmr_aplldo_usro, sgndo_aplldo_usro, nmbre_cmplto
						   )
	Select  ltrim(rtrim(lgn_usro))        ,  ltrim(rtrim(prmr_nmbre_usro)), ltrim(rtrim(isnull(sgndo_nmbre_usro,''))),
	        ltrim(rtrim(prmr_aplldo_usro)),  ltrim(rtrim(isnull(sgndo_aplldo_usro,''))),
	        replace(ltrim(rtrim(prmr_nmbre_usro)) +' '+ ltrim(rtrim(isnull(sgndo_nmbre_usro,'')))  +' '+ 
		    ltrim(rtrim(prmr_aplldo_usro))+' '+ ltrim(rtrim(isnull(sgndo_aplldo_usro,''))),'  ',' ') As nmbre_cmplto
	From    bdseguridad.dbo.tbUsuariosWeb With(Nolock)

	IF @lgn_usro IS NOT NULL AND @nombreCompleto IS NULL
	BEGIN
		INSERT INTO #usuarios (prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro)
		 SELECT prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro
		 FROM   #datosUsuarios
	 	 WHERE  lgn_usro like @lgn_usro1 + '%'
	END

	IF @nombreCompleto IS NOT NULL AND @lgn_usro IS NULL
	BEGIN
		INSERT INTO #usuarios (prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro)
		 SELECT prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro
		 FROM   #datosUsuarios
		 Where  nmbre_cmplto like @nombreCompleto1 +'%'
	END

	IF @nombreCompleto IS NOT NULL AND @lgn_usro IS NOT NULL
	BEGIN
		INSERT INTO #usuarios (prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro)
		SELECT prmr_nmbre_usro, sgndo_nmbre_usro, prmr_aplldo_usro, sgndo_aplldo_usro, lgn_usro
		FROM   #datosUsuarios
		Where  nmbre_cmplto like @nombreCompleto1 +'%'
		And    lgn_usro like @lgn_usro1 + '%'
	END
    
	SELECT prmr_nmbre_usro,
		sgndo_nmbre_usro,
		prmr_aplldo_usro,
		sgndo_aplldo_usro,
		lgn_usro
	FROM #usuarios

	DROP TABLE #usuarios
	DROP TABLE #datosUsuarios
END
GO
