USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarOficina]    Script Date: 05/11/2016 02:38:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.[spRCConsultarOficina]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCConsultarOficina] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarOficina
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Consulta la oficina
					 D\>     
	 
* Parametros       : <\P 
						@cnsctvo_cdgo_ofcna consecutivo oficina
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCConsultarOficina]
	
	@cnsctvo_cdgo_ofcna int
AS
BEGIN
	SET NOCOUNT ON

	SELECT cnsctvo_cdgo_ofcna
      ,cdgo_ofcna
      ,dscrpcn_ofcna
      ,fcha_crcn
      ,usro_crcn
      ,vsble_usro
  FROM BDAfiliacionValidador.dbo.tbOficinas WITH(NOLOCK)
  WHERE cnsctvo_cdgo_ofcna = @cnsctvo_cdgo_ofcna

END
GO

