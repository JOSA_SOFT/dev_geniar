USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerConceptoPagoOPS]    Script Date: 30/10/2016 22:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF object_id('cja.spRCObtenerConceptoPagoOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerConceptoPagoOPS AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCObtenerConceptoPagoOPS
* Desarrollado por : <\A Ing. Jorge Andres Garcia Erazo - Geniar SAS A\>     
* Descripcion      : <\D 
						Metodo encargado de obtener los conceptos de pago de una OPS
					 D\>     
	 
* Parametros       : <\P 
						@cnsctvo_slctd_atrzcn_srvco: Numero de autorizacion de la OPS
					 P\>     
* Variables        : <\V V\>     
* Fecha Creacion   : <\FC 2016/10/30 FC\>     
*     
 EXEC cja.spRCObtenerConceptoPagoOPS 92587102
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCObtenerConceptoPagoOPS] 
	@nmro_unco_ops udtConsecutivo
AS 
BEGIN
	SET NOCOUNT ON
	Select distinct a.vlr_lqdcn_cta_rcprcn_srvco_ops, b.dscrpcn_cncpto_pgo
	From gsa.TbAsConsolidadoCuotaRecuperacionxOps a with(nolock)
	inner join bdsisalud.dbo.tbConceptosPago b with(nolock) On a.cnsctvo_cdgo_cncpto_pgo = b.cnsctvo_cdgo_cncpto_pgo
	where a.nmro_unco_ops = @nmro_unco_ops
END

GO

