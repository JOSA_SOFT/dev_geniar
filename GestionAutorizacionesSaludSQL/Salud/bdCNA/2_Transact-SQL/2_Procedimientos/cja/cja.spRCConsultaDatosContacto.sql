USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaDatosContacto') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaDatosContacto AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaDatosContacto
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Consulta los datos de contacto de un documento. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/01/11 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCConsultaDatosContacto 90
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCConsultaDatosContacto(
	@cnsctvo_dto_cntcto udtConsecutivo
)
AS
BEGIN
	SET NOCOUNT ON
    SELECT nmro_cnta, cnsctvo_cdgo_mtdo_dvlcn_dnro, cnsctvo_cdgo_bnco, tpo_cnta, ttlr, nmro_tlfno, eml
	FROM cja.tbRCDatosContacto WITH(NOLOCK)
	WHERE  cnsctvo_dto_cntcto = @cnsctvo_dto_cntcto
END
GO
