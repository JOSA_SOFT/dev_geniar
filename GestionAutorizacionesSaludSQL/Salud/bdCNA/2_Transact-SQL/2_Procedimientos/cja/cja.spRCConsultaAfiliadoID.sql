USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaAfiliadoID') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaAfiliadoID AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaAfiliadoID
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Consulta un afiliado a partir del tipo y numero de identificacion D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>	
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/12/13 FC\>
* Ejemplo: 
    <\EJ
        EXEC cja.spRCConsultaAfiliadoID 1,'66928201'
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCConsultaAfiliadoID(
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn varchar(20)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @fechaActual datetime
	SET @fechaActual = getdate()
	
	SELECT distinct nmro_unco_idntfccn_afldo,prmr_aplldo,sgndo_aplldo,prmr_nmbre,sgndo_nmbre
	FROM bdafiliacionvalidador.dbo.tbBeneficiariosValidador WITH(NOLOCK)
	WHERE cnsctvo_cdgo_tpo_idntfccn=@cnsctvo_cdgo_tpo_idntfccn 
	AND nmro_idntfccn=@nmro_idntfccn
	AND @fechaActual BETWEEN inco_vgnca_bnfcro AND fn_vgnca_bnfcro
END
GO
