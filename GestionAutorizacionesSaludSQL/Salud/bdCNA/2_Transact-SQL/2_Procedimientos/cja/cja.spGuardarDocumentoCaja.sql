USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCGuardarDocumentoCaja]    Script Date: 04/11/2016 9:10:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCGuardarDocumentoCaja]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCGuardarDocumentoCaja] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCGuardarDocumentoCaja
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Realiza el guardado de un documento de caja, sea nota credito o recibo de caja D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P 
					nmro_unco_idntfccn_afldo		: Nui del afiliado que genera el documento
					cnsctvo_cdgo_cncpto_gnrcn		: Para la notas credito, si es excedente o anulacion de OPS
					vlr_dcmnto_cja					: Valor del documento
					cnsctvo_cdgo_tpo_dcmnto_cja		: Tipo del documento (Nota credito o recibo caja)
					cnsctvo_cdgo_estdo_dcmnto_cja	: Estado del documento
					cnsctvo_dto_cntcto				: Informacion de contacto asociado al documento
					cnsctvo_dtlle_mvmnto_cja		: Relacion del detalle del movimiendo de la caja
					obsrvcn							: Observaciones.
					usro_crcn						: Usuario de aplicacion web que crea el registro
				P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/02 FC\>
* Ejemplo: 
    <\EJ
		declare @ops varchar(max) = '<lista_ops><ops><nmro_unco_ops>12345</nmro_unco_ops></ops><ops><nmro_unco_ops>1234567</nmro_unco_ops></ops></lista_ops>'
		exec cja.spRCGuardarDocumentoCaja 31533885, null, 4000, 1, 1, null, null, 'observacion', 'user1', @ops,null,null	--Recibo caja
		exec cja.spRCGuardarDocumentoCaja 31533885, 2, 4000, 2, 2, null, null, 'observacion', 'user1'		--Nota credito pendiente por cobrar
		exec cja.spRCGuardarDocumentoCaja 31533885, 2, 4000, 2, 3, null, null, 'observacion', 'user1'		--Nota credito pendiente por cobrar

    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCGuardarDocumentoCaja]
	@nmro_unco_idntfccn_afldo		udtConsecutivo,
	@cnsctvo_cdgo_cncpto_gnrcn		udtConsecutivo,
	@vlr_dcmnto_cja					numeric(18,0),
	@cnsctvo_cdgo_tpo_dcmnto_cja	udtConsecutivo,
	@cnsctvo_cdgo_estdo_dcmnto_cja	udtConsecutivo,
	@cnsctvo_dto_cntcto				udtConsecutivo,
	@cnsctvo_dtlle_mvmnto_cja		udtConsecutivo,
	@obsrvcn						udtObservacion,
	@usro_crcn						udtUsuario,
	@listaDocumentos				xml = null,
	@cnsctvo_cdgo_dcmnto			udtObservacion OUTPUT,
	@nmro_dcmnto					udtObservacion OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @secuencia				int,
			@DOCUMENTO_RECIBO_CAJA	int,
			@DOCUMENTO_NOTA_CREDITO int,
			@FECHA_ACTUAL			datetime,
			@identidad				int

	CREATE TABLE #ops(
		nmro_unco_ops int
	)

	SET @DOCUMENTO_RECIBO_CAJA	= 1
	SET @DOCUMENTO_NOTA_CREDITO = 2
	SET @FECHA_ACTUAL = getdate()

	IF @cnsctvo_cdgo_tpo_dcmnto_cja = @DOCUMENTO_RECIBO_CAJA
		SET @secuencia = NEXT VALUE FOR cja.seqRCRecibosCaja

	IF @cnsctvo_cdgo_tpo_dcmnto_cja = @DOCUMENTO_NOTA_CREDITO
		SET @secuencia = NEXT VALUE FOR cja.seqRCNotasCredito

	INSERT INTO cja.tbRCDocumentosCaja (
		nmro_unco_idntfccn_afldo,
		cnsctvo_cdgo_cncpto_gnrcn,
		vlr_dcmnto_cja,
		cnsctvo_cdgo_tpo_dcmnto_cja,
		cnsctvo_cdgo_estdo_dcmnto_cja,
		cnsctvo_dto_cntcto,
		cnsctvo_dtlle_mvmnto_cja,
		nmro_dcmnto,
		obsrvcn,
		fcha_crcn,
		usro_crcn,
		fcha_ultma_mdfccn,
		usro_ultma_mdfccn
	) VALUES (
		@nmro_unco_idntfccn_afldo,
		@cnsctvo_cdgo_cncpto_gnrcn,
		@vlr_dcmnto_cja,
		@cnsctvo_cdgo_tpo_dcmnto_cja,
		@cnsctvo_cdgo_estdo_dcmnto_cja,
		@cnsctvo_dto_cntcto,
		@cnsctvo_dtlle_mvmnto_cja,
		@secuencia,
		@obsrvcn,
		@FECHA_ACTUAL,
		@usro_crcn,
		@FECHA_ACTUAL,
		@usro_crcn
	)

	SELECT @identidad = SCOPE_IDENTITY()
	SET @cnsctvo_cdgo_dcmnto = @identidad
	SET @nmro_dcmnto = @secuencia

	IF @listaDocumentos IS NOT NULL
	BEGIN
		INSERT INTO #ops (nmro_unco_ops)
		SELECT DISTINCT
		  pref.value('(nmro_unco_ops/text())[1]', 'udtConsecutivo') AS nmro_unco_ops
		FROM @listaDocumentos.nodes('/lista_ops//ops') AS xml_slctd (Pref);

		INSERT INTO cja.tbRCSoportesDocumentosCaja (
			cnsctvo_dcmnto_cja,
			nmro_unco_ops,
			fcha_crcn,
			usro_crcn,
			fcha_ultma_mdfccn,
			usro_ultma_mdfccn
		)
		SELECT 
			@cnsctvo_cdgo_dcmnto,
			nmro_unco_ops,
			@FECHA_ACTUAL,
			@usro_crcn,
			@FECHA_ACTUAL,
			@usro_crcn
		FROM #ops
	END

	DROP TABLE #ops
END
GO

