USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarOficinaPorMovimientoActual]    Script Date: 11/11/2016 11:03:48 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultarOficinaPorMovimientoActual') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultarOficinaPorMovimientoActual AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  [spRCConsultarOficinaPorMovimientoActual]
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Consulta la oficina
					 D\>     
	 
* Parametros       : <\P 
						@userLogin login usuario
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCConsultarOficinaPorMovimientoActual]
	@userLogin udtUsuario
AS
BEGIN
DECLARE
	SET NOCOUNT ON
	@fechaFin DateTIME,
	@fechaInicio DATETIME
	
	SET @fechaInicio = CAST(convert(varchar(10),getdate(),112) AS date)
	SELECT @fechaFin = DATEADD(s,-1,(@fechaInicio+1))

	SELECT a.[cnsctvo_cdgo_ofcna]
      ,a.[cdgo_ofcna]
      ,a.[dscrpcn_ofcna]
      ,a.[fcha_crcn]
      ,a.[usro_crcn]
      ,a.[vsble_usro]
  FROM [BDAfiliacionValidador].[dbo].[tbOficinas] a WITH(NOLOCK)
	   inner join [BDCna].[cja].[tbRCMovimientosCaja] b on a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
  WHERE b.[usro_crcn] = @userLogin
  AND   b.[fcha_crcn] between @fechaInicio and @fechaFin 

END
GO

