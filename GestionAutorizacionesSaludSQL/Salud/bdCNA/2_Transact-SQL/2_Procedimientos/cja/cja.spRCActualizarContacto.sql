USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCActualizarContacto') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCActualizarContacto AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCActualizarContacto
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Actualiza los datos de contacto D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/12/18 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCActualizarContacto ...
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCActualizarContacto(
	@cnsctvo_cdgo_mtdo_dvlcn_dnro udtConsecutivo,
	@nmro_cnta varchar(50),
	@cnsctvo_cdgo_bnco udtConsecutivo,
	@tpo_cnta varchar(50),
	@ttlr varchar(150),
	@nmro_tlfno varchar(50),
	@eml varchar(100),
	@usro_mdfcn udtUsuario,
	@cnsctvo_dto_cntcto udtConsecutivo,
	@cnsctvo_dto_cntcto_out udtConsecutivo OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fechaActual datetime

	SET @fechaActual = getdate()

	IF @cnsctvo_dto_cntcto IS NOT NULL
	BEGIN
		UPDATE cja.tbRCDatosContacto
		SET cnsctvo_cdgo_mtdo_dvlcn_dnro = @cnsctvo_cdgo_mtdo_dvlcn_dnro,
			nmro_cnta = @nmro_cnta,
			cnsctvo_cdgo_bnco = @cnsctvo_cdgo_bnco,
			tpo_cnta = @tpo_cnta,
			ttlr = @ttlr,
			nmro_tlfno = @nmro_tlfno,
			eml = @eml,
			usro_ultma_mdfccn = @usro_mdfcn,
			fcha_ultma_mdfccn = @fechaActual
		WHERE cnsctvo_dto_cntcto = @cnsctvo_dto_cntcto
	END
	ELSE
	BEGIN
		INSERT INTO cja.tbRCDatosContacto
			   (cnsctvo_cdgo_mtdo_dvlcn_dnro
			   ,nmro_cnta
			   ,cnsctvo_cdgo_bnco
			   ,tpo_cnta
			   ,ttlr
			   ,nmro_tlfno
			   ,eml
			   ,usro_crcn
			   ,fcha_crcn
			   ,fcha_ultma_mdfccn
			   ,usro_ultma_mdfccn)
		 VALUES
			   (@cnsctvo_cdgo_mtdo_dvlcn_dnro
			   ,@nmro_cnta
			   ,@cnsctvo_cdgo_bnco
			   ,@tpo_cnta
			   ,@ttlr
			   ,@nmro_tlfno
			   ,@eml
			   ,@usro_mdfcn
			   ,@fechaActual
			   ,@fechaActual
			   ,@usro_mdfcn)

		Select @cnsctvo_dto_cntcto_out = SCOPE_IDENTITY()
	END
END
GO
