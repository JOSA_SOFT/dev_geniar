USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarDenominaciones]    Script Date: 06/12/2016 14:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultarDenominaciones') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultarDenominaciones AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarDenominaciones
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Se consulta denominaciones con vigencia y tipo
					 D\>     
	 
* Parametros       : <\P 
						@tipo_denominacion Tipo de denominacion
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCConsultarDenominaciones]
	@tipo_denominacion int
AS
BEGIN 
	DECLARE
	@fechaActual date
	SET @fechaActual = getDate()


	SET NOCOUNT ON
	SELECT	b.dscrpcn_dnmncn
	  FROM [cja].[tbRCDenominaciones_Vigencias] a WITH(NOLOCK)
		   inner join [cja].[tbRCDenominaciones] b WITH(NOLOCK) on a.cnsctvo_cdgo_dnmncn = b.cnsctvo_cdgo_dnmncn
		   inner join [cja].[tbRCTiposDenominacion_Vigencias] c WITH(NOLOCK) on a.cnsctvo_cdgo_tpo_dnmncn = c.cnsctvo_cdgo_tpo_dnmncn
		   inner join [cja].[tbRCTiposDenominacion] d WITH(NOLOCK) on c.cnsctvo_cdgo_tpo_dnmncn = d.cnsctvo_cdgo_tpo_dnmncn
	  WHERE @fechaActual between a.inco_vgnca and a.fn_vgnca
	  AND	@fechaActual between c.inco_vgnca and c.fn_vgnca
	  AND	c.cnsctvo_cdgo_tpo_dnmncn = @tipo_denominacion
 ORDER BY   CAST(b.dscrpcn_dnmncn AS NUMERIC(18))

END
GO

