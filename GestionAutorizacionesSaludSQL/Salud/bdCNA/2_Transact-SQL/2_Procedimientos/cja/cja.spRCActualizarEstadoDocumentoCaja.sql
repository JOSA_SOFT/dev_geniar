USE BDCna
GO
/****** Object:  StoredProcedure cja.spRCActualizarEstadoDocumentoCaja    Script Date: 31/10/2016 9:00:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCActualizarEstadoDocumentoCaja') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCActualizarEstadoDocumentoCaja AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCActualizarEstadoDocumentoCaja
* Desarrollado por : <\A Ing. Jose Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se actualiza el estado a "Cobrado" de los documentos caja.
					 D\>     
	 
* Parametros       : <\P 
						  @cnsctvo_cdgo_dcmnto Consecutivo del documento a modificar
						  @cnsctvo_dtalle   C�digo del detalle de movimiento que se va a asociar al documento.
						
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCActualizarEstadoDocumentoCaja
	  @cnsctvo_cdgo_dcmnto udtConsecutivo	,
	  @cnsctvo_dtalle   udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Estado_Cobrado INTEGER
	SET @Estado_Cobrado = 3

	UPDATE cja.tbRCDocumentosCaja 
	SET cnsctvo_cdgo_estdo_dcmnto_cja = @Estado_Cobrado,
		cnsctvo_dtlle_mvmnto_cja =  @cnsctvo_dtalle
	WHERE cnsctvo_cdgo_dcmnto_cja = @cnsctvo_cdgo_dcmnto

END
GO

