USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaNotasCreditoRecibo') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaNotasCreditoRecibo AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCConsultaNotasCreditoRecibo
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Se encarga de tener las notas credito utilizadas D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/10 FC\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se realiza ajusta a consulta para que al obtener el ultimo movimiento de cierre tenga en cuenta el usuario
							que viene como parametro DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------


* Ejemplo: 
    <\EJ
		DECLARE @fcha datetime
		SET @fcha = getdate()-1
        EXEC cja.spRCConsultaNotasCreditoRecibo 'user1', @fcha
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCConsultaNotasCreditoRecibo](
	@usro udtUsuario,
	@fcha datetime
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ESTADO_CIERRE udtConsecutivo,
		@ESTADO_COBRADA udtConsecutivo,
		@TIPO_NOTA_CREDITO udtConsecutivo,
		@fechaInicio datetime,
		@fechaFin datetime,
		@cnsctvo_mvmnto_cja_actl udtConsecutivo

	CREATE TABLE #informacionMovimiento(
		cnsctvo_dtlle_mvmnto_cja udtConsecutivo NULL,
		cnsctvo_mvmnto_cja udtConsecutivo NULL,
		vlr_dcmnto numeric(18 ,0) NULL,
		vlr_nta_crdto numeric(18 ,0) NULL,
		nmro_dcmnto int NULL,
		nmro_ops varchar(max) NULL,
		cnsctvo_cdgo_dcmnto_cja_rc udtConsecutivo NULL
	)

	CREATE TABLE #valoresNotasCredito(
		cnsctvo_dtlle_mvmnto_cja udtConsecutivo NULL,
		cnsctvo_cdgo_dcmnto_cja_nc udtConsecutivo NULL,
		cnsctvo_cdgo_dcmnto_cja_rc udtConsecutivo NULL,
		vlr_nta_crdto numeric(18 ,0)NULL
	)

	CREATE TABLE #ops_detalle(
		cnsctvo_cdgo_dcmnto_cja_rc udtConsecutivo NULL,
		nmro_unco_ops udtConsecutivo NULL
	)

	CREATE TABLE #ops_detalle_separadas(
		cnsctvo_cdgo_dcmnto_cja udtConsecutivo NULL,
		nmro_unco_ops_grpo varchar(max)NULL
	)

	SET @ESTADO_CIERRE = 2
	SET @ESTADO_COBRADA = 3
	SET @TIPO_NOTA_CREDITO = 2
	SET @fechaInicio = CAST(convert(varchar(10),@fcha,112) AS date)
	SELECT @fechaFin = DATEADD(s,-1,(@fechaInicio+1))

	SELECT @cnsctvo_mvmnto_cja_actl = max(a.cnsctvo_mvmnto_cja)
	FROM cja.tbRCMovimientosCaja a with(nolock)
	WHERE a.fcha_crre between @fechaInicio and @fechaFin

	Insert into #informacionMovimiento (cnsctvo_dtlle_mvmnto_cja, cnsctvo_mvmnto_cja, vlr_dcmnto)
	Select b.cnsctvo_dtlle_mvmnto_cja, b.cnsctvo_mvmnto_cja, b.vlr_dcmnto
	From cja.tbRCMovimientosCaja a with(nolock)
	inner join cja.tbRCDetMovimientosCaja b with(nolock) On a.cnsctvo_mvmnto_cja = b.cnsctvo_mvmnto_cja
	where a.usro_ultma_mdfccn = @usro
	and a.cnsctvo_cdgo_estdo = @ESTADO_CIERRE
	and a.fcha_crre between @fechaInicio and @fechaFin
	and a.cnsctvo_mvmnto_cja = @cnsctvo_mvmnto_cja_actl

	UPDATE a
	SET a.nmro_dcmnto = b.nmro_dcmnto
	FROM #informacionMovimiento a 
	inner join cja.tbRCDocumentosCaja b with(nolock) On a.cnsctvo_dtlle_mvmnto_cja  = b.cnsctvo_dtlle_mvmnto_cja
	where b.cnsctvo_cdgo_tpo_dcmnto_cja = @TIPO_NOTA_CREDITO
	
	--valores de notas credito que fueron cruzadas con recibo de caja.
	INSERT INTO #valoresNotasCredito (cnsctvo_cdgo_dcmnto_cja_nc, vlr_nta_crdto, cnsctvo_cdgo_dcmnto_cja_rc)
	SELECT  d.cnsctvo_cdgo_dcmnto_cja_rlcndo, sum(b.vlr_dcmnto) vlr_nta_crdto, d.cnsctvo_cdgo_dcmnto_cja
	FROM #informacionMovimiento a
	INNER JOIN cja.tbRCDetMovimientosCaja b with(nolock) On a.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
	INNER JOIN cja.tbRCDocumentosCaja c with(nolock) On c.cnsctvo_dtlle_mvmnto_cja  = b.cnsctvo_dtlle_mvmnto_cja
	INNER JOIN cja.tbRCDocumentosRelacionados d with(nolock) On d.cnsctvo_cdgo_dcmnto_cja_rlcndo = c.cnsctvo_cdgo_dcmnto_cja
	WHERE c.cnsctvo_cdgo_estdo_dcmnto_cja = @ESTADO_COBRADA
	AND c.cnsctvo_cdgo_tpo_dcmnto_cja = @TIPO_NOTA_CREDITO
	GROUP BY d.cnsctvo_cdgo_dcmnto_cja_rlcndo, d.cnsctvo_cdgo_dcmnto_cja

	--valores de notas credito que fueron cobradas directamente en la caja.
	INSERT INTO #valoresNotasCredito (cnsctvo_cdgo_dcmnto_cja_nc, vlr_nta_crdto, cnsctvo_cdgo_dcmnto_cja_rc)
	SELECT  d.cnsctvo_dcmnto_cja, sum(b.vlr_dcmnto) vlr_nta_crdto, d.cnsctvo_dcmnto_cja
	FROM #informacionMovimiento a
	INNER JOIN cja.tbRCDetMovimientosCaja b with(nolock) On a.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
	INNER JOIN cja.tbRCDocumentosCaja c with(nolock) On c.cnsctvo_dtlle_mvmnto_cja  = b.cnsctvo_dtlle_mvmnto_cja
	INNER JOIN cja.tbRCSoportesDocumentosCaja d with(nolock) On d.cnsctvo_dcmnto_cja = c.cnsctvo_cdgo_dcmnto_cja
	LEFT JOIN cja.tbRCDocumentosRelacionados e with(nolock) On e.cnsctvo_cdgo_dcmnto_cja_rlcndo = c.cnsctvo_cdgo_dcmnto_cja
	WHERE c.cnsctvo_cdgo_estdo_dcmnto_cja = @ESTADO_COBRADA
	AND c.cnsctvo_cdgo_tpo_dcmnto_cja = @TIPO_NOTA_CREDITO
	AND e.cnsctvo_cdgo_dcmnto_cja_rlcndo IS NULL
	GROUP BY d.cnsctvo_dcmnto_cja, d.cnsctvo_dcmnto_cja

	UPDATE a
	SET a.vlr_nta_crdto = c.vlr_nta_crdto,
		a.cnsctvo_cdgo_dcmnto_cja_rc = c.cnsctvo_cdgo_dcmnto_cja_rc
	FROM #informacionMovimiento a 
	INNER JOIN cja.tbRCDocumentosCaja b with(nolock) On a.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
	INNER JOIN #valoresNotasCredito c On c.cnsctvo_cdgo_dcmnto_cja_nc = b.cnsctvo_cdgo_dcmnto_cja

	INSERT INTO #ops_detalle (cnsctvo_cdgo_dcmnto_cja_rc, nmro_unco_ops)
	Select distinct a.cnsctvo_cdgo_dcmnto_cja_rc, b.nmro_unco_ops
	From #informacionMovimiento a 
	inner join cja.tbRCSoportesDocumentosCaja b with(nolock) On a.cnsctvo_cdgo_dcmnto_cja_rc = b.cnsctvo_dcmnto_cja
	where a.cnsctvo_cdgo_dcmnto_cja_rc is not null

	Insert into #ops_detalle_separadas (cnsctvo_cdgo_dcmnto_cja,nmro_unco_ops_grpo)
	SELECT 
		cnsctvo_cdgo_dcmnto_cja_rc,
		STUFF((
		SELECT ', ' + CAST(nmro_unco_ops AS VARCHAR(MAX)) 
		FROM #ops_detalle 
		WHERE (cnsctvo_cdgo_dcmnto_cja_rc = Results.cnsctvo_cdgo_dcmnto_cja_rc) 
		FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
		,1,2,'') AS nmro_unco_ops_grupo
	FROM #ops_detalle Results
	GROUP BY cnsctvo_cdgo_dcmnto_cja_rc

	UPDATE a
	SET a.nmro_ops = b.nmro_unco_ops_grpo
	FROM #informacionMovimiento a
	INNER JOIN #ops_detalle_separadas b ON a.cnsctvo_cdgo_dcmnto_cja_rc = b.cnsctvo_cdgo_dcmnto_cja
	
	SELECT distinct nmro_dcmnto, vlr_dcmnto, ltrim(rtrim(nmro_ops)) nmro_ops
	FROM #informacionMovimiento
	where nmro_dcmnto is not null


	DROP TABLE #informacionMovimiento
	DROP TABLE #valoresNotasCredito
	DROP TABLE #ops_detalle
	DROP TABLE #ops_detalle_separadas
END
GO

