USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerBancos') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerBancos AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCObtenerBancos
* Desarrollado por : <\A Campo para documentar el autor del sp A\>
* Descripcion  : <\D Campo para documentar la descripcion D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC yyyy/mm/dd FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCObtenerBancos
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCObtenerBancos]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE
		@FECHAACTUAL DATETIME,
		@VSBLE_USRO CHAR(1)

	SET @FECHAACTUAL=getdate()
	SET @VSBLE_USRO = 'S'
	

    SELECT a.cnsctvo_cdgo_bnco, a.dscrpcn_bnco
	FROM bdsisalud.dbo.tbBancos a with(nolock)
	inner join bdsisalud.dbo.tbBancos_vigencias b with(nolock) on a.cnsctvo_cdgo_bnco = b.cnsctvo_cdgo_bnco
	WHERE @FECHAACTUAL between b.inco_vgnca and b.fn_vgnca
	AND b.vsble_usro = @VSBLE_USRO
END
GO

