USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCGuardarDatosContacto') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCGuardarDatosContacto AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCGuardarDatosContacto
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Realiza el guardado de los datos de contacto. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P 
					@cnsctvo_cdgo_mtdo_dvlcn_dnro udtConsecutivo,
					@nmro_cnta varchar(50),
					@cnsctvo_cdgo_bnco udtConsecutivo,
					@tpo_cnta varchar(50),
					@ttlr varchar(150),
					@nmro_tlfno varchar(50),
					@eml varchar(100),
					@usro_crcn udtUsuario
				P\>
* Variables   : <\V 
					@fcha datetime
					@cnsctvo_dto_cntcto udtConsecutivo
				V\>
* Fecha Creacion : <\FC 2016/11/04 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC spRCGuardarDatosContacto 1,'1234',1,'Ahorro','geniar','316','a@a.com','geniarjag'
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCGuardarDatosContacto]
	@cnsctvo_cdgo_mtdo_dvlcn_dnro udtConsecutivo,
	@nmro_cnta varchar(50),
	@cnsctvo_cdgo_bnco udtConsecutivo,
	@tpo_cnta varchar(50),
	@ttlr varchar(150),
	@nmro_tlfno varchar(50),
	@eml varchar(100),
	@usro_crcn udtUsuario
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fcha datetime,
			@cnsctvo_dto_cntcto udtConsecutivo
	SET @fcha = getdate()

	INSERT INTO cja.tbRCDatosContacto
			   (cnsctvo_cdgo_mtdo_dvlcn_dnro
			   ,nmro_cnta
			   ,cnsctvo_cdgo_bnco
			   ,tpo_cnta
			   ,ttlr
			   ,nmro_tlfno
			   ,eml
			   ,usro_crcn
			   ,fcha_crcn
			   ,fcha_ultma_mdfccn
			   ,usro_ultma_mdfccn)
		 VALUES
			   (@cnsctvo_cdgo_mtdo_dvlcn_dnro
			   ,@nmro_cnta
			   ,@cnsctvo_cdgo_bnco
			   ,@tpo_cnta
			   ,@ttlr
			   ,@nmro_tlfno
			   ,@eml
			   ,@usro_crcn
			   ,@fcha
			   ,@fcha
			   ,@usro_crcn)

	Select @cnsctvo_dto_cntcto = SCOPE_IDENTITY()
	Select @cnsctvo_dto_cntcto as cnsctvo_dto_cntcto
END
GO

