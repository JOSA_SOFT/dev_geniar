USE BDCna
GO
/****** Object:  StoredProcedure cja.spRCGuardarDetMovimientoCaja    Script Date: 04/11/2016 8:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.spRCGuardarDetMovimientoCaja') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCGuardarDetMovimientoCaja AS SELECT 1;'
END
GO


/*----------------------------------------------------------------------------------------
* Metodo o PRG : spRCGuardarDetMovimientoCaja
* Desarrollado por : <\A Jos� Olmedo Soto Aguirre <jose.soto@geniar.net> A\>
* Descripcion  : <\D Realiza el guardado del detalle de movimiento de la cajaD\>    
* Observaciones : <\O O\>    
* Parametros  : <\P 
					vlr_dcmnto		: Valor del documento
					vlr_rcbdo					:  Valor registrado en el campo Efectivo
					cha_crcn : Fecha actual
					usro_crcn : usuario en sesion
					fcha_ultma_mdfccn : Fecha actual
					usro_ultma_mdfccn : usuario en sesion
				P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/02 FC\>
* Ejemplo: 
    <\EJ
	
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCGuardarDetMovimientoCaja		
	@vlr_dcmnto			numeric(18,0),
	@vlr_rcbdo			numeric(18,0),
	@fcha_crcn			datetime,
	@usro_crcn			udtUsuario,
	@fcha_ultma_mdfccn datetime,
	@usro_ultma_mdfccn udtUsuario,
	@cnsctvo_cdgo_dtlle_mvmnto_cja udtConsecutivo OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE 
		@cnsctvo_mvmnto_cja  udtConsecutivo,
		@fechaCero datetime,
		@fechaFin datetime,
		@fechaActual datetime,
		@estado_abierto_movimiento udtConsecutivo

	Set @fechaActual = getdate()
	Set @estado_abierto_movimiento = 1

	SELECT @fechaCero = CAST(convert(varchar(10),@fechaActual,112) AS date)
	SELECT @fechaFin = DATEADD(s,-1,(@fechaCero+1))	

	SELECT @cnsctvo_mvmnto_cja  = cnsctvo_mvmnto_cja 
	from  cja.tbRCMovimientosCaja with(nolock)
	WHERE fcha_aprtra between @fechaCero AND @fechaFin 
	AND cnsctvo_cdgo_estdo = @estado_abierto_movimiento
	AND usro_crcn = @usro_crcn
	
	INSERT INTO cja.tbRCDetMovimientosCaja(cnsctvo_mvmnto_cja, vlr_dcmnto , 
												vlr_rcbdo, fcha_crcn, 
												usro_crcn, fcha_ultma_mdfccn, 
												usro_ultma_mdfccn) 
			values(@cnsctvo_mvmnto_cja,@vlr_dcmnto,
					@vlr_rcbdo,@fcha_crcn,
					@usro_crcn,@fcha_ultma_mdfccn,
					@usro_ultma_mdfccn)

	SELECT @cnsctvo_cdgo_dtlle_mvmnto_cja = SCOPE_IDENTITY()
	
END
GO

