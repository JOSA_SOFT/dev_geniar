USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerReciboCajaSolicitud') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerReciboCajaSolicitud AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCObtenerReciboCajaSolicitud
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D  D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P @cnsctvo_slctd_atrzcn_srvco: Consecutivo de la solicitud a consultar P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/07/04 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCObtenerReciboCajaSolicitud 394088
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo  AM\>    
* Descripcion        : <\DM Se ajusta consulta retirando tabla de servicios solicitados que no es necesaria, se coloca distinct DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/10 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCObtenerReciboCajaSolicitud(
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @codigoMedioPagoEPS udtConsecutivo,
			@tipoReciboCaja	udtConsecutivo,
			@tipoNotaCredito	udtConsecutivo,
			@cero				int,
			@totalNotasCredito	numeric(18,0),
			@espacio			varchar(1),
			@fechaActual		datetime

	CREATE TABLE #temp_informacionReciboCaja(
		cnsctvo_cdgo_dcmnto_cja			udtConsecutivo,
		nmro_dcmnto						int,
		nmro_unco_ops					udtConsecutivo, 
		vlr_lqdcn_cta_rcprcn_srvco_ops	float, 
		cnsctvo_cdgo_cncpto_pgo			udtConsecutivo, 
		dscrpcn_cncpto_pgo				udtDescripcion,
		usro_crcn						udtUsuario,
		vlr_nta_crdto					numeric(18,0),
		nmbre_afldo						varchar(140),
		nmro_idntfccn					varchar(20),
		cdgo_tpo_idntfccn				char(3),
		fcha_rcbo						datetime,
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		vlr_rcbo						numeric(18,0),
		vlr_rcbdo						numeric(18,0),
		cnsctvo_cdgo_ofcna				udtConsecutivo,
		nmbre_ofcna						udtDescripcion
	)
	
	SET @codigoMedioPagoEPS = 1 --Cobro EPS 
	SET @tipoReciboCaja = 1
	SET @tipoNotaCredito = 2
	SET @cero = 0
	SET @espacio = ' '
	SET @fechaActual = getdate()

	INSERT INTO #temp_informacionReciboCaja(
		nmro_dcmnto,			cnsctvo_cdgo_dcmnto_cja,	nmro_unco_ops,							vlr_lqdcn_cta_rcprcn_srvco_ops,			cnsctvo_cdgo_cncpto_pgo, 
		dscrpcn_cncpto_pgo,		usro_crcn,					fcha_rcbo,								cnsctvo_slctd_atrzcn_srvco,				vlr_rcbo,	
		vlr_rcbdo,				cnsctvo_cdgo_ofcna)
	SELECT distinct
		e.nmro_dcmnto,			e.cnsctvo_cdgo_dcmnto_cja,	d.nmro_unco_ops,						cro.vlr_lqdcn_cta_rcprcn_srvco_ops,		cro.cnsctvo_cdgo_cncpto_pgo, 
		c.dscrpcn_cncpto_pgo,	e.usro_crcn,				e.fcha_crcn,							@cnsctvo_slctd_atrzcn_srvco,			vlr_dcmnto_cja,
		f.vlr_rcbdo,			g.cnsctvo_cdgo_ofcna
	FROM gsa.TbAsConsolidadoCuotaRecuperacionxOps cro with(nolock) 
		INNER JOIN bdsisalud.dbo.tbConceptosPago_Vigencias c with(nolock) on c.cnsctvo_cdgo_cncpto_pgo = cro.cnsctvo_cdgo_cncpto_pgo
		INNER JOIN cja.tbRCSoportesDocumentosCaja d with(nolock) on cro.nmro_unco_ops = d.nmro_unco_ops
		INNER JOIN cja.tbRCDocumentosCaja e with(nolock) on d.cnsctvo_dcmnto_cja = e.cnsctvo_cdgo_dcmnto_cja
		INNER JOIN cja.tbRCDetMovimientosCaja f with(nolock) on f.cnsctvo_dtlle_mvmnto_cja = e.cnsctvo_dtlle_mvmnto_cja
		INNER JOIN cja.tbRCMovimientosCaja g with(nolock) on f.cnsctvo_mvmnto_cja = g.cnsctvo_mvmnto_cja
	WHERE cro.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	AND cro.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn = @codigoMedioPagoEPS
	AND e.cnsctvo_cdgo_tpo_dcmnto_cja = @tipoReciboCaja
	AND @fechaActual between c.inco_vgnca and c.fn_vgnca

	UPDATE tmp
	SET tmp.nmbre_ofcna = a.dscrpcn_ofcna
	FROM #temp_informacionReciboCaja tmp
	INNER JOIN BDAfiliacionValidador.dbo.tbOficinas_Vigencias a with(nolock) on tmp.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	WHERE @fechaActual between a.inco_vgnca and a.fn_vgnca 
	
	UPDATE tmp
	SET tmp.nmbre_afldo = CONCAT(
								LTRIM(RTRIM(b.prmr_nmbre)),
								@espacio,
								LTRIM(RTRIM(ISNULL(b.sgndo_nmbre,@espacio))),
								@espacio,
								LTRIM(RTRIM(b.prmr_aplldo)),
								@espacio,
								LTRIM(RTRIM(ISNULL(b.sgndo_aplldo,@espacio)))
							),
		nmro_idntfccn = LTRIM(RTRIM(b.nmro_idntfccn)),
		cdgo_tpo_idntfccn = c.cdgo_tpo_idntfccn
	FROM #temp_informacionReciboCaja tmp
	INNER JOIN gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios a with(nolock) on tmp.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b with(nolock) on a.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	INNER JOIN BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias c with(nolock) on c.cnsctvo_cdgo_tpo_idntfccn = b.cnsctvo_cdgo_tpo_idntfccn
	WHERE @fechaActual between c.inco_vgnca and c.fn_vgnca
	AND @fechaActual between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro


	SELECT @totalNotasCredito = SUM(ISNULL(c.vlr_dcmnto_cja,@cero))
	FROM #temp_informacionReciboCaja a
	INNER JOIN cja.tbRCDocumentosRelacionados b with(nolock) on a.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_cdgo_dcmnto_cja
	INNER JOIN cja.tbRCDocumentosCaja c with(nolock) on b.cnsctvo_cdgo_dcmnto_cja_rlcndo = c.cnsctvo_cdgo_dcmnto_cja
	WHERE c.cnsctvo_cdgo_tpo_dcmnto_cja = @tipoNotaCredito

	UPDATE #temp_informacionReciboCaja
	SET vlr_nta_crdto = @totalNotasCredito

	SELECT 
		cnsctvo_cdgo_dcmnto_cja			,
		nmro_dcmnto						,
		nmro_unco_ops					,
		vlr_lqdcn_cta_rcprcn_srvco_ops	,
		cnsctvo_cdgo_cncpto_pgo			,
		dscrpcn_cncpto_pgo				,
		usro_crcn						,
		vlr_nta_crdto					,
		nmbre_afldo						,
		nmro_idntfccn					,
		cdgo_tpo_idntfccn				,
		fcha_rcbo						,
		cnsctvo_slctd_atrzcn_srvco		,
		vlr_rcbo						,
		vlr_rcbdo						,
		cnsctvo_cdgo_ofcna				,
		nmbre_ofcna						
	FROM #temp_informacionReciboCaja
	
	DROP TABLE #temp_informacionReciboCaja
END
GO
