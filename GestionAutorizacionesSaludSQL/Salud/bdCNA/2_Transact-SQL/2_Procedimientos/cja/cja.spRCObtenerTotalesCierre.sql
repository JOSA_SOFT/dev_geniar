USE BDCna
GO
/****** Object:  StoredProcedure cja.spRCObtenerTotalesCierre    Script Date: 02/11/2016 07:40:50 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.spRCObtenerTotalesCierre') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerTotalesCierre AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCObtenerTotalesCierre
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Se calcula la sumatoria de notas credito en estado cobrado
						de acuerdo a un movimiento caja en apertura y tambien la sumatoria
						de recibos caja en estado generado
					 D\>     
	 
* Parametros       : <\P 
						@consecutivoMovimientoCaja Consecutivo movimiento caja en apertura
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta SP para que identifique las notas credito cobradas que afectan a la caja y no tenga
							en cuenta las notas creditos que fueron cruzadas con recibos de caja. DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------


*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCObtenerTotalesCierre
	
	@consecutivoMovimientoCaja int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE
	@ConseDocumentoEstadoCobrado udtConsecutivo,
	@ConseDocumentoEstadoGenerado udtConsecutivo,
	@SumNotasCredito numeric(18),
	@SumRecibosCaja numeric(18),
	@TipoNotaCredito numeric(18),
	@TipoReciboCaja numeric(18)
	
	SET @ConseDocumentoEstadoCobrado = 3
	SET @ConseDocumentoEstadoGenerado = 1

	SET @TipoNotaCredito = 2
	SET @TipoReciboCaja = 1

	SET @SumNotasCredito = 
	(
		SELECT	SUM(a.vlr_dcmnto) AS RESULT
		  FROM cja.tbRCDetMovimientosCaja a WITH(NOLOCK)
			   inner join cja.tbRCDocumentosCaja b WITH(NOLOCK) on a.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
			   left join cja.tbRCDocumentosRelacionados c WITH(NOLOCK) on c.cnsctvo_cdgo_dcmnto_cja_rlcndo = b.cnsctvo_cdgo_dcmnto_cja
		  WHERE a.cnsctvo_mvmnto_cja = @consecutivoMovimientoCaja
		  AND	b.cnsctvo_cdgo_estdo_dcmnto_cja = @ConseDocumentoEstadoCobrado
		  AND	b.cnsctvo_cdgo_tpo_dcmnto_cja = @TipoNotaCredito
		  AND	a.vlr_dcmnto IS NOT NULL
		  AND	c.cnsctvo_dcmnto_rlcndo IS NULL
	)

	SET @SumRecibosCaja = 
	(
		SELECT	SUM(a.vlr_rcbdo) AS RESULT
		  FROM cja.tbRCDetMovimientosCaja a WITH(NOLOCK)
			   inner join cja.tbRCDocumentosCaja b WITH(NOLOCK) on a.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
		  WHERE a.cnsctvo_mvmnto_cja = @consecutivoMovimientoCaja
		  AND	b.cnsctvo_cdgo_estdo_dcmnto_cja = @ConseDocumentoEstadoGenerado
		  AND	b.cnsctvo_cdgo_tpo_dcmnto_cja = @TipoReciboCaja
		  AND	a.vlr_rcbdo IS NOT NULL
	)

	SELECT	ISNULL(@SumNotasCredito,0) AS NOTAS_CREDITO,
			ISNULL(@SumRecibosCaja,0) AS RECIBOS_CAJA
END
GO

