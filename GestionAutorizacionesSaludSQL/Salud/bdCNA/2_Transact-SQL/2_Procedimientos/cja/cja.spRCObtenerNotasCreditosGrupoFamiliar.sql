USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerNotasCreditosGrupoFamiliar]    Script Date: 31/10/2016 9:00:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerNotasCreditosGrupoFamiliar') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerNotasCreditosGrupoFamiliar AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCObtenerNotasCreditosGrupoFamiliar
* Desarrollado por : <\A Ing. Jose Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se consultan las notas cr�ditos seg�n el NUI de cada integrante del grupo famliar
					 D\>     
	 
* Parametros       : <\P 
						@nuiAfiliado NUI del afiliado a consultar
						
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta sp para que identifique si alguna nota credito es de recibos de caja anteriores a la fecha actual, 
							En caso de ser asi asigna a variable de salida N (que quiere decir que no entrega efectivo por ventanilla), 
							de lo contrario queda en S (que quiere decir que puede entregar dinero por ventanilla) DM/>
* Nuevos Parametros  : <\PM @entregaEfectivo char(2) parametro de salida PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/30 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCObtenerNotasCreditosGrupoFamiliar
	@num_afiliado udtConsecutivo,
	@entregaEfectivo char(2) output
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@id_tipo_nota_credito udtConsecutivo,
		@id_estado_pend_cobro udtConsecutivo,
		@fechaActual datetime,
		@fechaCero datetime,
		@fechaFinCero datetime,
		@SI char(2),
		@NO char(2),
		@id_tipo_recibo_caja udtConsecutivo

	-- CREA TABLA TEMPORAL PARA ALMACENAR CONTRATOS ASOCIADOS AL AFILIADO
	CREATE TABLE #CONTRATOS_AFILIADO (
		CONTRATO udtConsecutivo NULL
	)
	
	CREATE TABLE #NUIS_GRUPO_FAMILIAR(
		NUIS udtConsecutivo NULL
	)

	CREATE TABLE #notasCredito(
		fechaCreacion datetime,
		identificacion varchar(50),
		nombres varchar(200),
		valorCaja numeric(18,0),
		notaCredito int,
		cnsctvo_cdgo_dcmnto_cja udtConsecutivo,
		fcha_crcn datetime
	)

	CREATE TABLE #ops(
		nmro_unco_ops numeric(18,0)
	)

	SET @id_tipo_nota_credito = 2
	SET @id_estado_pend_cobro = 2
	SET @fechaActual = getdate()
	SET @fechaCero = CAST(convert(varchar(10),@fechaActual,112) AS date)
	SET @fechaFinCero = DATEADD(s,-1,(@fechaCero+1))
	SET @SI = 'SI'
	SET @NO = 'NO'
	SET @id_tipo_recibo_caja = 1

	INSERT INTO  #CONTRATOS_AFILIADO (CONTRATO)  
	EXEC cja.spRCObtenerContratosGrupoFamiliar @num_afiliado	

	--La tabla #CONTRATOS_AFILIADO se est� usando en el SP cja.spRCObtenerNUIGrupoFamiliar
	INSERT INTO #NUIS_GRUPO_FAMILIAR(NUIS)  
	EXEC cja.spRCObtenerNUIGrupoFamiliar 

	INSERT INTO #notasCredito(fechaCreacion, identificacion,nombres,valorCaja,notaCredito,cnsctvo_cdgo_dcmnto_cja,fcha_crcn)
	SELECT DISTINCT doCaja.fcha_crcn fechaCreacion,	 
		CONCAT(ti.cdgo_tpo_idntfccn,' - ',bv.nmro_idntfccn) as identificacion, 
		CONCAT(LTRIM(RTRIM(bv.prmr_nmbre)),' ',LTRIM(RTRIM(bv.sgndo_nmbre)),' ',LTRIM(RTRIM(bv.prmr_aplldo)),' ',LTRIM(RTRIM(bv.sgndo_aplldo))) as nombres,		
		doCaja.vlr_dcmnto_cja valorCaja,
		doCaja.nmro_dcmnto notaCredito,
		doCaja.cnsctvo_cdgo_dcmnto_cja,
		doCaja.fcha_crcn
	FROM bdAfiliacionValidador.dbo.tbBeneficiariosValidador bv with(nolock) 
	INNER JOIN bdAfiliacionValidador.dbo.tbTiposIdentificacion ti with(nolock) on ti.cnsctvo_cdgo_tpo_idntfccn = bv.cnsctvo_cdgo_tpo_idntfccn
	INNER JOIN cja.tbRCDocumentosCaja doCaja with(nolock) on doCaja.nmro_unco_idntfccn_afldo = bv.nmro_unco_idntfccn_afldo
	INNER JOIN #NUIS_GRUPO_FAMILIAR nuisF on nuisF.NUIS = bv.nmro_unco_idntfccn_afldo
	WHERE doCaja.cnsctvo_cdgo_estdo_dcmnto_cja = @id_estado_pend_cobro 
	AND doCaja.cnsctvo_cdgo_tpo_dcmnto_cja = @id_tipo_nota_credito 
	AND doCaja.fcha_crcn between @fechaCero and @fechaFinCero

	INSERT INTO #ops(nmro_unco_ops)
	SELECT distinct nmro_unco_ops
	FROM #notasCredito a
	INNER JOIN cja.tbRCSoportesDocumentosCaja b with(nolock) on a.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja

	IF EXISTS (SELECT 1
			   FROM #ops a
			   INNER JOIN cja.tbRCSoportesDocumentosCaja b with(nolock) ON a.nmro_unco_ops = b.nmro_unco_ops
			   INNER JOIN cja.tbRCDocumentosCaja c with(nolock) on c.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja
			   WHERE c.cnsctvo_cdgo_tpo_dcmnto_cja = @id_tipo_recibo_caja
			   AND c.fcha_crcn < @fechaCero)
	BEGIN
		SET @entregaEfectivo = @NO
	END
	ELSE
	BEGIN
		SET @entregaEfectivo = @SI
	END

	SELECT fechaCreacion, identificacion, nombres, valorCaja, notaCredito, cnsctvo_cdgo_dcmnto_cja
	FROM #notasCredito
	ORDER BY fcha_crcn ASC

	-- BORRADO DE TABLAS TEMPORALES
	DROP TABLE #CONTRATOS_AFILIADO 
	DROP TABLE #NUIS_GRUPO_FAMILIAR
	DROP TABLE #notasCredito
	DROP TABLE #ops
END
GO

