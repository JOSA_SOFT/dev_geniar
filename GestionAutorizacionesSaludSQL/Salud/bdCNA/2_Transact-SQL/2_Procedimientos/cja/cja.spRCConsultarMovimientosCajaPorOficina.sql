USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarMovimientosCajaPorOficina]    Script Date: 07/11/2016 12:28:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCConsultarMovimientosCajaPorOficina]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCConsultarMovimientosCajaPorOficina] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarMovimientosCajaPorOficina
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
            Consulta los movimientos caja por oficina y que tengan null el campo
            consecutivo cierre oficina.
           D\>     
   
* Parametros       : <\P 
            @cnsctvo_cdgo_ofcna consecutivo codigo oficina
           P\>     
* Variables        : <\V                      V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE cja.spRCConsultarMovimientosCajaPorOficina
  @cnsctvo_cdgo_ofcna udtConsecutivo
AS
BEGIN
  SET NOCOUNT ON

  SELECT a.cnsctvo_mvmnto_cja
      ,a.bse
      ,a.fcha_aprtra
      ,a.fcha_crre
      ,a.cnsctvo_cdgo_ofcna
      ,a.cnsctvo_cdgo_estdo
      ,b.dscrpcn_estdo
      ,a.drccn_ip
      ,a.fltnte_crre
      ,a.sbrnte_crre
      ,a.ttl_efctvo
      ,a.ttl_cja
      ,a.ttl_nta_crdto
      ,a.cnsctvo_crre_ofcna
      ,a.fcha_crcn
      ,a.usro_crcn
      ,a.fcha_ultma_mdfccn
      ,a.usro_ultma_mdfccn
  FROM cja.tbRCMovimientosCaja a WITH(NOLOCK)
     inner join cja.tbRCEstadosMovimientosCaja b WITH(NOLOCK) on a.cnsctvo_cdgo_estdo = b.cnsctvo_cdgo_estdo
  WHERE a.cnsctvo_crre_ofcna IS NULL
  AND   a.cnsctvo_cdgo_ofcna = @cnsctvo_cdgo_ofcna
  ORDER BY a.fcha_aprtra DESC

END
GO

