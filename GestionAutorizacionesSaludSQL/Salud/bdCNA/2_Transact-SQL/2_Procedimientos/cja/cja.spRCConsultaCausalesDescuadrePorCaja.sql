USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCConsultaCausalesDescuadrePorCaja') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCConsultaCausalesDescuadrePorCaja AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCConsultaCausalesDescuadrePorCaja
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Consulta las causales de descuadre asociadas a un movimiento de caja D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/07 FC\>
* Ejemplo: 
    <\EJ
        EXEC cja.spRCConsultaCausalesDescuadrePorCaja 31
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCConsultaCausalesDescuadrePorCaja]
	@cnsctvo_mvmnto_cja udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.vlr_dscdre, a.usro_crcn, b.dscrpcn_csl_dscdre
	FROM cja.tbRCMovimientosCajaxCausalesDescuadre a WITH(NOLOCK)
	INNER JOIN cja.tbRCCausalesDescuadre b On a.cnsctvo_cdgo_csl_dscdre = b.cnsctvo_cdgo_csl_dscdre 
	WHERE a.cnsctvo_mvmnto_cja = @cnsctvo_mvmnto_cja
END
GO

