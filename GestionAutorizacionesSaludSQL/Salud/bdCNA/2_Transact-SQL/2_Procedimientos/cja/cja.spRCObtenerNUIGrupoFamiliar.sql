USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerNUIGrupoFamiliar]    Script Date: 31/10/2016 9:00:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCObtenerNUIGrupoFamiliar]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCObtenerNUIGrupoFamiliar] AS SELECT 1;'
END
GO


/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCObtenerContratosGrupoFamiliar
* Desarrollado por : <\A Ing. Jose Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se consultan los contratos que est�n amarrados al NUI del afiliado
					 D\>     
	 
* Parametros       : <\P 
						@nuiAfiliado NUI del afiliado a consultar
						
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCObtenerNUIGrupoFamiliar]	
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #NUISBENEFICIARIO (NUMERO_ID udtConsecutivo NULL, FECHAINICIO datetime NULL, FECHAFIN datetime NULL);

	INSERT INTO #NUISBENEFICIARIO (NUMERO_ID,FECHAINICIO,FECHAFIN)
	select 
		b.nmro_unco_idntfccn_afldo, b.inco_vgnca_bnfcro , b.fn_vgnca_bnfcro       
	from bdAfiliacionValidador.dbo.tbBeneficiariosValidador b with(nolock)
	inner join #CONTRATOS_AFILIADO c on
		c.CONTRATO = b.nmro_cntrto
	
	SELECT DISTINCT nui.NUMERO_ID
	FROM #NUISBENEFICIARIO nui with(nolock)
	where getDate() BETWEEN  nui.FECHAINICIO and nui.FECHAFIN

	DROP TABLE  #NUISBENEFICIARIO
	
END
GO

