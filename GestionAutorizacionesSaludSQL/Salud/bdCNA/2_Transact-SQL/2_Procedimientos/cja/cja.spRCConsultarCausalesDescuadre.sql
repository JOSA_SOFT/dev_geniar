USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultarCausalesDescuadre]    Script Date: 02/11/2016 04:23:54 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCConsultarCausalesDescuadre]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCConsultarCausalesDescuadre] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCConsultarCausalesDescuadre
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Consulta las causales de descuadre
					 D\>     
	 
* Parametros       : <\P 
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCConsultarCausalesDescuadre]	
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @fechaActual datetime
	SET @fechaActual = getDate()

		SELECT a.cnsctvo_cdgo_csl_dscdre
		  ,a.cdgo_csl_dscdre
		  ,a.dscrpcn_csl_dscdre
		  ,a.fcha_crcn
		  ,a.usro_crcn
		  ,a.vsble_usro
	  FROM	BDCna.cja.tbRCCausalesDescuadre a WITH(NOLOCK)
			inner join BDCna.cja.tbRCCausalesDescuadre_Vigencias b WITH(NOLOCK) on a.cnsctvo_cdgo_csl_dscdre = b.cnsctvo_cdgo_csl_dscdre
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca
END
GO

