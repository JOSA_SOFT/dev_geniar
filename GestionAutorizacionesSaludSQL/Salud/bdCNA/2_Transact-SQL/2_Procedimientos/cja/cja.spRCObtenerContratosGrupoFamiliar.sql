USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerContratosGrupoFamiliar]    Script Date: 31/10/2016 9:00:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerContratosGrupoFamiliar') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerContratosGrupoFamiliar AS SELECT 1;'
END
GO
/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCObtenerContratosGrupoFamiliar
* Desarrollado por : <\A Ing. Jose Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se consultan los contratos que est�n amarrados al NUI del afiliado
					 D\>     
	 
* Parametros       : <\P 
						@nuiAfiliado NUI del afiliado a consultar
						
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     
*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCObtenerContratosGrupoFamiliar]
	@nuiAfiliado udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@FECHAACTUAL DATETIME

	CREATE TABLE #CONTRATOS_BENEFICIARIOS(
	NUM_CONTRATO		udtConsecutivo NULL, 
	inco_vgnca_bnfcro	datetime NULL, 
	fn_vgnca_bnfcro		datetime NULL
	);

	SET @FECHAACTUAL = getDate() 

    insert into #CONTRATOS_BENEFICIARIOS(NUM_CONTRATO,		
										 inco_vgnca_bnfcro	,
										 fn_vgnca_bnfcro)	
	 select 
		nmro_cntrto, inco_vgnca_bnfcro , fn_vgnca_bnfcro       
	from bdAfiliacionValidador.dbo.tbBeneficiariosValidador with(nolock)
	where nmro_unco_idntfccn_afldo=@nuiAfiliado

	select NUM_CONTRATO
	from #CONTRATOS_BENEFICIARIOS with(nolock)
	where @FECHAACTUAL between inco_vgnca_bnfcro and fn_vgnca_bnfcro

	DROP TABLE  #CONTRATOS_BENEFICIARIOS
END
GO

