USE [bdCNA]
GO
/****** Object:  StoredProcedure [cja].[spRCConsultaRecibosCajaUsuario]    Script Date: 01/06/2017 15:18:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------------------ 
* Metodo o PRG     : cja.spRCConsultaRecibosCajaUsuario
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion      : <\D 
                         Obtiene todos los recibos de caja asociados a un usuario en una fecha 
                         Campo para documentar la descripcion 
					  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>
* Variables        : <\V V\>
* Fecha Creacion   : <\FC 2016/11/09 FC\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM 
                            Se realiza ajusta a consulta para que al obtener el ultimo movimiento 
                            de cierre tenga en cuenta el usuario que viene como parametro 
						DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp que no tome las cuotas de recuperacion anuladas
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/

/* Ejemplo: 
    <\EJ
        DECLARE @fcha datetime
		SET @fcha = '2016-11-20 00:00:00.000'
		EXEC cja.spRCConsultaRecibosCajaUsuario 'user1', @fcha
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCConsultaRecibosCajaUsuario]
	@usro udtUsuario,
	@fcha Datetime
AS
BEGIN
	SET NOCOUNT ON;

	Declare @estado_cierre               udtConsecutivo,
		    @estado_cierre_adminitrativo udtConsecutivo,
		    @cuota_moderadora            udtConsecutivo,
		    @tipo_recibo_caja            udtConsecutivo,
		    @estado_cobrada              udtConsecutivo,
		    @tipo_nota_credito           udtConsecutivo,
		    @copago                      udtConsecutivo,
		    @fechaInicio                 Datetime      ,
		    @fechaFin                    Datetime      ,
		    @cnsctvo_mvmnto_cja_actl     udtConsecutivo,
			@estdo_cta_rcprcn_anldo      udtConsecutivo

	Create
	Table  #informacionMovimiento(cnsctvo_dtlle_mvmnto_cja udtConsecutivo,
		                          cnsctvo_mvmnto_cja       udtConsecutivo,
		                          vlr_dcmnto               Numeric(18,0) ,
		                          vlr_rcbdo                Numeric(18,0) ,
		                          vlr_cta_mdrdra           Numeric(18,0) ,
		                          vlr_cpgo                 Numeric(18,0) ,
		                          vlr_nta_crdto            Numeric(18,0) ,
		                          nmro_dcmnto              Int           ,
		                          nmro_ops                 Varchar(Max) 
	                             )

	Create 
	Table  #valoresCuotas(cnsctvo_dtlle_mvmnto_cja udtConsecutivo,
		                  vlr_cta_mdrdra           Numeric(18,0) ,     
		                  vlr_cpgo                 Numeric(18,0) 
	                     )

	Create 
	Table  #valoresNotasCredito(cnsctvo_dtlle_mvmnto_cja   udtConsecutivo,
		                        cnsctvo_cdgo_dcmnto_cja_rc udtConsecutivo,
		                        vlr_nta_crdto              Numeric(18,0) , 
	                           )

	Create 
	Table  #ops_detalle(cnsctvo_dtlle_mvmnto_cja udtConsecutivo,
		                nmro_unco_ops            udtConsecutivo 
	                   )

	Create 
	Table  #ops_detalle_separadas(
		cnsctvo_dtlle_mvmnto_cja udtConsecutivo NULL,
		nmro_unco_ops_grpo varchar(max) NULL
	)

	CREATE TABLE #valoresConcepto(cnsctvo_dtlle_mvmnto_cja udtConsecutivo,
		                          vlr                      Numeric(18,0) 
	                             )

	Set @copago                 = 2
	Set @estado_cierre          = 2
	Set @cuota_moderadora       = 1
	Set @tipo_recibo_caja       = 1
	Set @estado_cobrada         = 3
	Set @tipo_nota_credito      = 2
	Set @estdo_cta_rcprcn_anldo = 4
	Set @fechaInicio            = CAST(Convert(Varchar(10), @fcha,112) AS Date)
	
	
	Select @fechaFin = DATEADD(s,-1,(@fechaInicio+1))

	Select @cnsctvo_mvmnto_cja_actl = Max(cnsctvo_mvmnto_cja)
	From   cja.tbRCMovimientosCaja With(Nolock)
	Where  fcha_crre Between @fechaInicio And @fechaFin
	And    usro_crcn = @usro
	
	Insert 
	Into   #informacionMovimiento (cnsctvo_dtlle_mvmnto_cja, cnsctvo_mvmnto_cja, 
	                               vlr_dcmnto              , vlr_rcbdo
								  )
	Select     b.cnsctvo_dtlle_mvmnto_cja, b.cnsctvo_mvmnto_cja, 
	           b.[vlr_dcmnto ]           , b.vlr_rcbdo
	From       bdCNA.cja.tbRCMovimientosCaja a With(Nolock)
	Inner Join bdCNA.cja.tbRCDetMovimientosCaja b With(Nolock) 
	On         b.cnsctvo_mvmnto_cja = a.cnsctvo_mvmnto_cja
	Where      a.cnsctvo_mvmnto_cja = @cnsctvo_mvmnto_cja_actl
	And        a.usro_ultma_mdfccn = @usro
	And        (
	            a.cnsctvo_cdgo_estdo = @estado_cierre OR 
	            a.cnsctvo_cdgo_estdo = @estado_cierre_adminitrativo
			   )
	And        a.fcha_crre between @fechaInicio and @fechaFin
	

	Insert 
	Into       #valoresConcepto (cnsctvo_dtlle_mvmnto_cja, vlr)
	Select     Distinct a.cnsctvo_dtlle_mvmnto_cja, d.vlr_lqdcn_cta_rcprcn_srvco_ops
	From       #informacionMovimiento a 
	Inner Join bdCNA.cja.tbRCDocumentosCaja b With(Nolock) 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Inner Join bdCNA.cja.tbRCSoportesDocumentosCaja c With(Nolock) 
	On         c.cnsctvo_dcmnto_cja = b.cnsctvo_cdgo_dcmnto_cja
	Inner Join bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps d With(Nolock) 
	On         d.nmro_unco_ops = c.nmro_unco_ops
	Inner Join bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion e With(Nolock) 
	On         e.cnsctvo_slctd_atrzcn_srvco = d.cnsctvo_slctd_atrzcn_srvco
	Where      e.cnsctvo_cdgo_cncpto_pgo        = @cuota_moderadora
	And        b.cnsctvo_cdgo_tpo_dcmnto_cja    = @tipo_recibo_caja
	And        d.cnsctvo_cdgo_estdo_cta_rcprcn != @estdo_cta_rcprcn_anldo  
	And        e.cnsctvo_cdgo_estdo_cta_rcprcn != @estdo_cta_rcprcn_anldo

	Insert 
	Into     #valoresCuotas(cnsctvo_dtlle_mvmnto_cja, vlr_cta_mdrdra)
	Select   cnsctvo_dtlle_mvmnto_cja, sum(vlr)
	From     #valoresConcepto
	Group By cnsctvo_dtlle_mvmnto_cja

	Delete From #valoresConcepto

	Insert 
	Into       #valoresConcepto (cnsctvo_dtlle_mvmnto_cja, vlr)
	Select     Distinct a.cnsctvo_dtlle_mvmnto_cja, d.vlr_lqdcn_cta_rcprcn_srvco_ops
	From       #informacionMovimiento a 
	Inner Join bdCNA.cja.tbRCDocumentosCaja b With(Nolock) 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Inner Join bdCNA.cja.tbRCSoportesDocumentosCaja c With(Nolock) 
	On         c.cnsctvo_dcmnto_cja = b.cnsctvo_cdgo_dcmnto_cja
	Inner Join bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps d With(Nolock) 
	On         d.nmro_unco_ops = c.nmro_unco_ops
	Inner Join bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion e With(Nolock) 
	On         e.cnsctvo_slctd_atrzcn_srvco = d.cnsctvo_slctd_atrzcn_srvco
	Where      e.cnsctvo_cdgo_cncpto_pgo        = @copago
	And        b.cnsctvo_cdgo_tpo_dcmnto_cja    = @tipo_recibo_caja
	And        d.cnsctvo_cdgo_estdo_cta_rcprcn != @estdo_cta_rcprcn_anldo  
	And        e.cnsctvo_cdgo_estdo_cta_rcprcn != @estdo_cta_rcprcn_anldo

	Insert 
	Into     #valoresCuotas(cnsctvo_dtlle_mvmnto_cja, vlr_cpgo)
	Select   cnsctvo_dtlle_mvmnto_cja, Sum(vlr)
	From     #valoresConcepto
	Group By cnsctvo_dtlle_mvmnto_cja

	Delete From #valoresConcepto

	Update     a
	Set        vlr_cta_mdrdra = b.vlr_cta_mdrdra
	From       #informacionMovimiento a 
	Inner Join #valoresCuotas b 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Where      b.vlr_cta_mdrdra Is Not Null

	Update     a
	Set        vlr_cpgo = b.vlr_cpgo
	From       #informacionMovimiento a 
	Inner Join #valoresCuotas b 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Where      b.vlr_cpgo is not null

	Update     a
	Set        nmro_dcmnto = b.nmro_dcmnto
	From       #informacionMovimiento a 
	Inner Join bdCNA.cja.tbRCDocumentosCaja b With(Nolock) 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Where      b.cnsctvo_cdgo_tpo_dcmnto_cja = @tipo_recibo_caja


	Insert 
	Into       #valoresNotasCredito (cnsctvo_cdgo_dcmnto_cja_rc, vlr_nta_crdto)
	Select     d.cnsctvo_cdgo_dcmnto_cja, Sum(b.[vlr_dcmnto ])
	From       #informacionMovimiento a
	Inner Join bdCNA.cja.tbRCDetMovimientosCaja b With(Nolock) 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Inner Join bdCNA.cja.tbRCDocumentosCaja c With(Nolock) 
	On         c.cnsctvo_dtlle_mvmnto_cja = b.cnsctvo_dtlle_mvmnto_cja
	Inner Join bdCNA.cja.tbRCDocumentosRelacionados d With(Nolock) 
	On         d.cnsctvo_cdgo_dcmnto_cja_rlcndo = c.cnsctvo_cdgo_dcmnto_cja
	Where      c.cnsctvo_cdgo_estdo_dcmnto_cja = @estado_cobrada
	And        c.cnsctvo_cdgo_tpo_dcmnto_cja   = @tipo_nota_credito
	Group By   d.cnsctvo_cdgo_dcmnto_cja

	Update     a
	Set        vlr_nta_crdto = c.vlr_nta_crdto
	From       #informacionMovimiento a 
	Inner Join bdCNA.cja.tbRCDocumentosCaja b With(Nolock) 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Inner Join #valoresNotasCredito c 
	On         c.cnsctvo_cdgo_dcmnto_cja_rc = b.cnsctvo_cdgo_dcmnto_cja

	Insert 
	Into       #ops_detalle (cnsctvo_dtlle_mvmnto_cja, nmro_unco_ops)
	Select     a.cnsctvo_dtlle_mvmnto_cja, c.nmro_unco_ops
	From       #informacionMovimiento a 
	Inner Join bdCNA.cja.tbRCDocumentosCaja b With(Nolock) On 
	           b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	Inner Join bdCNA.cja.tbRCSoportesDocumentosCaja c with(nolock) 
	On         c.cnsctvo_dcmnto_cja = b.cnsctvo_cdgo_dcmnto_cja
	Where      a.nmro_dcmnto Is Not Null


	Insert 
	Into     #ops_detalle_separadas (cnsctvo_dtlle_mvmnto_cja, nmro_unco_ops_grpo)
	Select   cnsctvo_dtlle_mvmnto_cja,
		     STUFF((
			      Select ', ' + CAST(nmro_unco_ops AS VARCHAR(MAX)) 
			      From   #ops_detalle 
			      Where (cnsctvo_dtlle_mvmnto_cja = Results.cnsctvo_dtlle_mvmnto_cja) 
			      For XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,2,''
				) AS nmro_unco_ops_grupo
	From     #ops_detalle Results
	Group By cnsctvo_dtlle_mvmnto_cja

	Update     a
	Set        nmro_ops = b.nmro_unco_ops_grpo
	From       #informacionMovimiento a
	Inner Join #ops_detalle_separadas b 
	On         b.cnsctvo_dtlle_mvmnto_cja = a.cnsctvo_dtlle_mvmnto_cja
	
	Select  vlr_dcmnto                        , vlr_rcbdo    , vlr_cta_mdrdra,
		    vlr_cpgo                          , vlr_nta_crdto, nmro_dcmnto   ,
		    Ltrim(Rtrim(nmro_ops)) As nmro_ops
	From    #informacionMovimiento
	Where   nmro_dcmnto is not null

	Drop Table #informacionMovimiento
	Drop Table #valoresCuotas
	Drop Table #valoresNotasCredito
	Drop Table #ops_detalle
	Drop Table #ops_detalle_separadas
	Drop Table #valoresConcepto
END
