USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCValidaAperturaCaja]    Script Date: 16/11/2016 9:47:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCValidaAperturaCaja]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCValidaAperturaCaja] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCValidaAperturaCaja
* Desarrollado por : <\A Ing. José Olmedo Soto Aguirre - Geniar SAS A\>     
* Descripcion      : <\D 
						Se realiza la validación si la caja para el usuario enviado se encuentra abierta, cerrada o abierta en destiempo.
					 D\>     
	 
* Parametros       : <\P 
						@user_name: Nombre de usuario logeado en la aplicación.
						@return: Retorna 0: Si no se encuentra abierta la caja
										 1: Si la caja está abierta
										 2: Si la caja está abierta pero una fecha diferente a la actual.
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Ajuste para que solo tenga en cuenta el perfil de cajas, si es otro perfil arrojara que ya está abierta la caja. DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/20 FM\>   
* Ejemplo: 
    <\EJ
       EXEC cja.spRCValidaAperturaCaja 'user6'
	   EXEC cja.spRCValidaAperturaCaja 'user1'
	   EXEC cja.spRCValidaAperturaCaja 'user19'
	   EXEC cja.spRCValidaAperturaCaja 'user20'
	   EXEC cja.spRCValidaAperturaCaja 'jorgeage'
	   EXEC cja.spRCValidaAperturaCaja 'user9'
    EJ\>
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCValidaAperturaCaja]
	
	@user_name udtUsuario
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE
		@ID_ESTADO_ABIERTO udtConsecutivo ,
		@FECHA_MOVIMIENTO_CAJA datetime,
		@return  int ,
		@Exist_rol int,
		@FECHAACTUAL datetime,
		@ID_ESTADO_CIERRE_ADMINISTRATIVO udtConsecutivo,
		@rolASI udtConsecutivo,
		@validarAperturaCaja int

	SET @FECHAACTUAL = getDate()
	SET @ID_ESTADO_ABIERTO = 1
	SET @return = -1
	SET @ID_ESTADO_CIERRE_ADMINISTRATIVO = 4
	SET @rolASI = 236
	
	SELECT @Exist_rol = count(1) 
	FROM bdSeguridad.dbo.tbLoginsxPerfil with(nolock)
	WHERE lgn_usro = @user_name 
	AND cnsctvo_cdgo_prfl = @rolASI

	IF @Exist_rol > 0
	BEGIN 		
		SET @validarAperturaCaja = 1
	END
	ELSE 
	BEGIN
		SET @validarAperturaCaja = 0
	END
	
	IF @validarAperturaCaja = 1
	BEGIN
		SELECT	@FECHA_MOVIMIENTO_CAJA = max(movimiento.fcha_aprtra)
		FROM cja.tbRCMovimientosCaja movimiento with(nolock)
		WHERE movimiento.usro_crcn = @user_name
		AND (movimiento.cnsctvo_cdgo_estdo = @ID_ESTADO_ABIERTO
			 OR movimiento.cnsctvo_cdgo_estdo = @ID_ESTADO_CIERRE_ADMINISTRATIVO)

		IF @FECHA_MOVIMIENTO_CAJA IS NULL
		BEGIN
			SET @return = 0
		END
		ELSE
		BEGIN
			IF cast(@FECHA_MOVIMIENTO_CAJA As Date) < cast(@FECHAACTUAL As Date)
			BEGIN
				SET @return = 2
			END
			ELSE
			BEGIN
				SET @return = 1
			END
		END
	END
	ELSE
	BEGIN
		SET @return = 1
	END
	
	
	
	SELECT @return AS retorno
END
GO

