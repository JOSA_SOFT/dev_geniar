USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCCierreOficina]    Script Date: 09/11/2016 07:50:21 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCCierreOficina]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCCierreOficina] AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  spRCCierreOficina
* Desarrollado por : <\A Ing. Fabian Caicedo - Geniar SAS A\>     
* Descripcion      : <\D 
						Se realiza el cierre de la caja
					 D\>     
	 
* Parametros       : <\P 
						@ttl_efctvo_cja_ofi total efectivo caja oficina
						@ttl_rcbdo_ofcna total recibido oficina
						@bse_ofcna base oficina
						@fltnte_ofcna faltante oficina
						@sbrnte_ofcna sobrante oficina
						@usro_ofi usuario oficina
						@cnsctvo_cdgo_crdndr_asi_ofi consecutivo coordinador

						@movimientosCajaCierreAdmin lista de movimientos con cierre administrativo
						@movimientosCajaCierre lista de movimientos ya cerrados

Ejemplo XML:
Cierre administrativo

					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/10/27 FC\>     


Declare @admin varchar(max) = 
'<movimientoscja>
	<movimientocja>
		<cnsctvo_mvmnto_cja>12</cnsctvo_mvmnto_cja>
		<usro_ultma_mdfccn>user1</usro_ultma_mdfccn>
		<ttl_efctvo>12</ttl_efctvo>
		<ttl_nta_crdto>12</ttl_nta_crdto>
		<ttl_cja>12</ttl_cja>
		<sbrnte_crre>12</sbrnte_crre>
		<fltnte_crre>12</fltnte_crre>
	</movimientocja>
	<movimientocja>
		<cnsctvo_mvmnto_cja>12</cnsctvo_mvmnto_cja>
		<usro_ultma_mdfccn>user1</usro_ultma_mdfccn>
		<ttl_efctvo>12</ttl_efctvo>
		<ttl_nta_crdto>12</ttl_nta_crdto>
		<ttl_cja>12</ttl_cja>
		<sbrnte_crre>12</sbrnte_crre>
		<fltnte_crre>12</fltnte_crre>
	</movimientocja>
</movimientoscja>'

Declare @cerrados varchar(max) =
'<movimientoscja>
	<movimientocja>
		<cnsctvo_mvmnto_cja>12</cnsctvo_mvmnto_cja>
		<usro_ultma_mdfccn>user1</usro_ultma_mdfccn>
	</movimientocja>
	<movimientocja>
		<cnsctvo_mvmnto_cja>12</cnsctvo_mvmnto_cja>
		<usro_ultma_mdfccn>user1</usro_ultma_mdfccn>
	</movimientocja>
</movimientoscja>'
exec cja.spRCCierreOficina null,null,null,null,null,null,null,@admin,@cerrados


*     
*---------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [cja].[spRCCierreOficina]
	
		@ttl_efctvo_cja_ofi numeric,
        @ttl_rcbdo_ofcna numeric,
		@bse_ofcna numeric,
        @fltnte_ofcna numeric,
        @sbrnte_ofcna numeric,
        @usro_ofi udtUsuario,
        @cnsctvo_cdgo_crdndr_asi_ofi numeric,
		@movimientosCajaCierreAdmin xml,
		@movimientosCajaCierre xml
AS
BEGIN 
	SET NOCOUNT ON;
	DECLARE
		@idCierreOficina int,
		@fechaActual datetime,
		@cns_estado_cierre_admin int,
		@idoc int	
	
	CREATE TABLE #VariableTabla (
		cnsctvo_mvmnto_cja udtConsecutivo NULL,
		bse numeric NULL,
		fcha_aprtra datetime NULL,
		fcha_crre datetime NULL,
		cnsctvo_cdgo_ofcna udtConsecutivo NULL,
		cnsctvo_cdgo_estdo udtConsecutivo NULL,
		drccn_ip varchar NULL,
		fltnte_crre numeric NULL,
		sbrnte_crre numeric NULL,
		ttl_efctvo numeric NULL,
		ttl_cja numeric NULL,
		ttl_nta_crdto numeric NULL,
		cnsctvo_crre_ofcna udtConsecutivo NULL,
		fcha_crcn datetime NULL,
		usro_crcn udtUsuario NULL,
		fcha_ultma_mdfccn datetime NULL,
		usro_ultma_mdfccn udtUsuario NULL
	)

	SET @fechaActual = getDate()
	SET @cns_estado_cierre_admin = 4
	
	INSERT INTO [cja].[tbRCCierreOficina]
           ([ttl_efctvo_cja]
           ,[ttl_rcbdo_ofcna]
           ,[bse_ofcna]
           ,[fltnte_ofcna]
           ,[sbrnte_ofcna]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[fcha_ultma_mdfccn]
           ,[usro_ultma_mdfccn]
           ,[cnsctvo_cdgo_crdndr_asi])
     VALUES
           (@ttl_efctvo_cja_ofi
           ,@ttl_rcbdo_ofcna
           ,@bse_ofcna
           ,@fltnte_ofcna
           ,@sbrnte_ofcna
           ,@fechaActual
           ,@usro_ofi
           ,@fechaActual
           ,@usro_ofi
           ,@cnsctvo_cdgo_crdndr_asi_ofi)
	
	SELECT @idCierreOficina = Scope_Identity()


	IF @movimientosCajaCierreAdmin IS NOT NULL
	BEGIN
		INSERT INTO	#VariableTabla (cnsctvo_mvmnto_cja, usro_ultma_mdfccn, ttl_efctvo, ttl_nta_crdto, ttl_cja, sbrnte_crre, fltnte_crre, fcha_ultma_mdfccn)
		SELECT 
			pref.value('(cnsctvo_mvmnto_cja/text())[1]', 'udtConsecutivo') AS cnsctvo_mvmnto_cja,
			pref.value('(usro_ultma_mdfccn/text())[1]', 'udtUsuario') AS usro_ultma_mdfccn,
			pref.value('(ttl_efctvo/text())[1]', 'numeric') AS ttl_efctvo,
			pref.value('(ttl_nta_crdto/text())[1]', 'numeric') AS ttl_nta_crdto,
			pref.value('(ttl_cja/text())[1]', 'numeric') AS ttl_cja,
			pref.value('(sbrnte_crre/text())[1]', 'numeric') AS sbrnte_crre,
			pref.value('(fltnte_crre/text())[1]', 'numeric') AS sbrnte_crre,
			@fechaActual as fcha_ultma_mdfccn
		FROM @movimientosCajaCierreAdmin.nodes('/movimientoscja//movimientocja') AS xml_slctd (Pref);

		UPDATE a
		SET  a.fcha_crre = b.fcha_crre
			,a.cnsctvo_cdgo_estdo = @cns_estado_cierre_admin
			,a.fltnte_crre = b.fltnte_crre
			,a.sbrnte_crre = b.sbrnte_crre
			,a.ttl_efctvo = b.ttl_efctvo
			,a.ttl_cja = b.ttl_cja
			,a.ttl_nta_crdto = b.ttl_nta_crdto
			,a.cnsctvo_crre_ofcna = @idCierreOficina
			,a.fcha_ultma_mdfccn = b.fcha_ultma_mdfccn
			,a.usro_ultma_mdfccn = b.usro_ultma_mdfccn
		FROM cja.tbRCMovimientosCaja a With(Nolock)
		INNER JOIN #VariableTabla b On a.cnsctvo_mvmnto_cja = b.cnsctvo_mvmnto_cja

		DELETE FROM #VariableTabla


	END


	IF @movimientosCajaCierre IS NOT NULL
	BEGIN
		INSERT INTO	#VariableTabla (cnsctvo_mvmnto_cja, usro_ultma_mdfccn, fcha_ultma_mdfccn)
		SELECT 
			pref.value('(cnsctvo_mvmnto_cja/text())[1]', 'udtConsecutivo') AS cnsctvo_mvmnto_cja,
			pref.value('(usro_ultma_mdfccn/text())[1]', 'udtUsuario') AS usro_ultma_mdfccn,
			@fechaActual as fcha_ultma_mdfccn
		FROM @movimientosCajaCierre.nodes('/movimientoscja//movimientocja') AS xml_slctd (Pref);

		UPDATE a
		SET  a.cnsctvo_crre_ofcna = @idCierreOficina
			,a.fcha_ultma_mdfccn = b.fcha_ultma_mdfccn
			,a.usro_ultma_mdfccn = b.usro_ultma_mdfccn
		FROM cja.tbRCMovimientosCaja a With(Nolock)
		INNER JOIN #VariableTabla b On a.cnsctvo_mvmnto_cja = b.cnsctvo_mvmnto_cja
	END
	DROP TABLE #VariableTabla
	SELECT @idCierreOficina AS cnsctvo_crre_ofcna
END

GO

