USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCGuardarReciboCajaOrquestador]    Script Date: 02/12/2016 16:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.[spRCGuardarReciboCajaOrquestador]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCGuardarReciboCajaOrquestador] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCGuardarReciboCajaOrquestador
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion  : <\D Este sp se encarga de guardar el recibo, las notas credito que se 
					 generen, las notas credito que se empleen para pagar un recibo de 
					 caja y actualizar las notas credito que se usaron en el detalle.
				  D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/18 FC\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta llamado a sp spRCGuardarDocumentoCaja ya que se le agrego nuevo parametro DM/>
* Nuevos Parametros  : <\PM @insrta_dtlle PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------

* Ejemplo: 
    <\EJ        
		DECLARE @nmro_unco_ops varchar(max),
				@nts_crdto varchar(max),
				@nmro_rcbo udtConsecutivo


		SET @nmro_unco_ops = '<lista_ops><ops><nmro_unco_ops>92873894</nmro_unco_ops><vlr_ops>10600.0</vlr_ops></ops></lista_ops>'
		SET @nts_crdto = '<lista_notas><nota><cnsctvo_dcmnto_cja>220</cnsctvo_dcmnto_cja><vlr_nta_crdto>10600.0</vlr_nta_crdto></nota></lista_notas>'

		BEGIN TRANSACTION
			EXEC cja.spRCGuardarReciboCajaOrquestador 0,0,'user1',33513749,'prueba orquestador 2',@nmro_unco_ops,@nts_crdto,@nmro_rcbo OUTPUT
			Select @nmro_rcbo
		ROLLBACK

    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCGuardarReciboCajaOrquestador](
	@vlr_ttl_pgr_rcbo numeric(18,0),
	@vlr_efctvo_rcbo numeric(18,0),
	@usro udtUsuario,
	@nuiAfiliado udtConsecutivo,
	@obsrvcn udtObservacion,
	@lista_nmro_unco_ops xml,
	@lista_nts_crdto xml,
	@nmro_rcbo udtConsecutivo OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @hops int,
			@hnts int,
			@max_ops int,
			@max_nta int,
			@cont_ops int,
			@cont_nta int,
			@fechaActual datetime,
			@nmro_unco_ops udtConsecutivo,
			@cnsctvo_dtlle_mvmnto_cja udtConsecutivo,
			@cnsctvo_cdgo_dcmnto_cja_rcbo udtConsecutivo,
			@CODIGO_TIPO_DOC_RECIBO_CAJA udtConsecutivo,
			@CODIGO_ESTADO_GENERADO_CAJA udtConsecutivo,
			@vlr_nta_crdto numeric(18,0),
			@cnsctvo_cdgo_dcmnto_cja_nta_crdto udtConsecutivo,
			@NO	char(2)

	CREATE TABLE #ops(
		id_ops int identity,
		nmro_unco_ops udtConsecutivo,
		vlr_ops numeric(18,0)
	)

	CREATE TABLE #notasCredito(
		id_nta int identity,
		cnsctvo_dcmnto_cja udtConsecutivo,
		vlr_nta_crdto numeric(18,0)
	)

	SET @fechaActual = getdate()
	SET @CODIGO_TIPO_DOC_RECIBO_CAJA = 1
	SET @CODIGO_ESTADO_GENERADO_CAJA = 1
	SET @cont_ops = 1
	SET @cont_nta = 1
	SET @NO = 'NO'
	
	EXEC sp_xml_preparedocument @hops OUTPUT, @lista_nmro_unco_ops
	
	INSERT INTO	#ops (nmro_unco_ops, vlr_ops)
	SELECT	nmro_unco_ops, vlr_ops udtUsuario
	FROM	OPENXML (@hops, '/lista_ops/ops',2)
	WITH	(nmro_unco_ops udtConsecutivo, vlr_ops numeric(18,0))

	EXEC sp_xml_removedocument @hops

	EXEC cja.spRCGuardarDetMovimientoCaja @vlr_ttl_pgr_rcbo, @vlr_efctvo_rcbo, @fechaActual, @usro, @fechaActual, @usro, @cnsctvo_dtlle_mvmnto_cja OUTPUT

	EXEC cja.spRCGuardarDocumentoCaja @nuiAfiliado, null, @vlr_ttl_pgr_rcbo, 
									  @CODIGO_TIPO_DOC_RECIBO_CAJA, @CODIGO_ESTADO_GENERADO_CAJA, null, 
									  @cnsctvo_dtlle_mvmnto_cja, @obsrvcn, @usro, null, @cnsctvo_cdgo_dcmnto_cja_rcbo OUTPUT, @nmro_rcbo OUTPUT, @NO

	SELECT @max_ops = max(id_ops)
	FROM #ops

	WHILE(@cont_ops <= @max_ops)
	BEGIN
		SELECT @nmro_unco_ops = nmro_unco_ops
		FROM #ops
		WHERE id_ops = @cont_ops

		EXEC cja.spRCGuardarReciboCaja @cnsctvo_cdgo_dcmnto_cja_rcbo, @nmro_unco_ops, @fechaActual, @usro
		SET @cont_ops = @cont_ops + 1
	END

	IF @lista_nts_crdto is not null
	BEGIN
		EXEC sp_xml_preparedocument @hnts OUTPUT, @lista_nts_crdto
		
		INSERT INTO	#notasCredito (cnsctvo_dcmnto_cja, vlr_nta_crdto)
		SELECT	cnsctvo_dcmnto_cja, vlr_nta_crdto
		FROM	OPENXML (@hnts, '/lista_notas/nota',2)
		WITH	(cnsctvo_dcmnto_cja udtConsecutivo, vlr_nta_crdto numeric(18,0))
		
		SELECT @max_nta = max(id_nta)
		FROM #notasCredito

		WHILE(@cont_nta<=@max_nta)
		BEGIN
			SELECT @vlr_nta_crdto = vlr_nta_crdto, @cnsctvo_cdgo_dcmnto_cja_nta_crdto = cnsctvo_dcmnto_cja
			FROM #notasCredito
			WHERE id_nta = @cont_nta
			
			EXEC cja.spRCGuardarDetMovimientoCaja @vlr_nta_crdto, 0, @fechaActual, @usro, @fechaActual, @usro, @cnsctvo_dtlle_mvmnto_cja OUTPUT
			EXEC cja.spRCActualizarEstadoDocumentoCaja @cnsctvo_cdgo_dcmnto_cja_nta_crdto, @cnsctvo_dtlle_mvmnto_cja
			EXEC cja.spRCInsertarDocRelacionado @cnsctvo_cdgo_dcmnto_cja_rcbo, @cnsctvo_cdgo_dcmnto_cja_nta_crdto, @usro

			SET @cont_nta = @cont_nta + 1
		END
		EXEC sp_xml_removedocument @hnts
	END

	DROP TABLE #ops
	DROP TABLE #notasCredito
END
GO

