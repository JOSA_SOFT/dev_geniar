USE BDCna
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCGenerarInformeCierre') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCGenerarInformeCierre AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCGenerarInformeCierre
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@gmail.com> A\>
* Descripcion  : <\D Genera el informe de cierre de caja D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/07 FC\>
* Ejemplo: 
    <\EJ
        DECLARE @fcha_incl datetime,
				@fcha_fnl datetime,
				@cnsctvo_cdgo_ofcna udtConsecutivo,
				@lgn udtUsuario

		SET @fcha_incl = getdate()-4
		SET @fcha_fnl = getdate()+1
		SET @cnsctvo_cdgo_ofcna = 2
		SET @lgn = 'user1'
		EXEC cja.spRCGenerarInformeCierre @fcha_incl, @fcha_fnl, @cnsctvo_cdgo_ofcna, @lgn
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCGenerarInformeCierre](
	@fcha_incl datetime,
	@fcha_fnl datetime,
	@cnsctvo_cdgo_ofcna udtConsecutivo = null,
	@lgn udtUsuario = null,
	@cnsctvo_cdgo_sede udtConsecutivo = null
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @estadoCerradoCaja udtConsecutivo,
			@fechaCero			datetime,
			@fechaFinCero		datetime,
			@fechaFinMax		datetime

	SET @estadoCerradoCaja = 2


	select @fechaCero = CAST(convert(varchar(10),@fcha_incl,112) AS date)
	select @fechaFinCero = CAST(convert(varchar(10),@fcha_fnl,112) AS date)
	SET @fechaFinMax = DATEADD(s,-1,(@fechaFinCero+1))

	CREATE TABLE #reporte(
		fcha_crre datetime NULL,
		fcha_aprtra datetime NULL,
		bse numeric(18 ,0) NULL,
		ttl_efctvo numeric(18 ,0) NULL,
		sbrnte_crre numeric(18 ,0) NULL,
		fltnte_crre numeric(18 ,0) NULL,
		cnsctvo_mvmnto_cja udtConsecutivo NULL,
		ttl_cja numeric(18 ,0) NULL,
		dscrpcn_ofcna udtDescripcion NULL,
		nmbre_asi varchar(90) NULL,
		cnsctvo_cdgo_ofcna udtConsecutivo NULL,
		usro udtUsuario  NULL
	)

	INSERT INTO #reporte (fcha_crre, fcha_aprtra, bse, ttl_efctvo, sbrnte_crre, fltnte_crre, cnsctvo_mvmnto_cja, ttl_cja, cnsctvo_cdgo_ofcna, usro)
	SELECT fcha_crre,
		  fcha_aprtra,
		  bse,
		  ttl_efctvo,
		  sbrnte_crre,
		  fltnte_crre,
		  cnsctvo_mvmnto_cja,
		  ttl_cja,
		  cnsctvo_cdgo_ofcna,
		  usro_crcn
	FROM cja.tbRCMovimientosCaja with(nolock)
	WHERE fcha_crre between @fechaCero and @fechaFinMax
	AND cnsctvo_cdgo_estdo = @estadoCerradoCaja

	IF @cnsctvo_cdgo_ofcna IS NOT NULL
	BEGIN
		DELETE 
		FROM #reporte
		WHERE cnsctvo_cdgo_ofcna != @cnsctvo_cdgo_ofcna
	END

	IF @lgn IS NOT NUll
	BEGIN
		DELETE 
		FROM #reporte
		WHERE usro != @lgn
	END

	IF @cnsctvo_cdgo_sede IS NOT NULL
	BEGIN
		DELETE a
		FROM #reporte a
		INNER JOIN BDAfiliacionValidador.dbo.tbOficinas_Vigencias b ON a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
		WHERE b.cnsctvo_cdgo_sde != @cnsctvo_cdgo_sede
	END

	UPDATE a
	SET a.dscrpcn_ofcna = b.dscrpcn_ofcna
	FROM #reporte a
	INNER JOIN BDAfiliacionValidador.dbo.tbOficinas_Vigencias b WITH(NOLOCK) On a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna

	UPDATE a
	SET a.nmbre_asi = ltrim(rtrim(b.prmr_nmbre_usro)) + ' ' + ltrim(rtrim(b.prmr_aplldo_usro))
	FROM #reporte a
	INNER JOIN bdSeguridad.dbo.tbUsuariosWeb b WITH(NOLOCK) On a.usro = b.lgn_usro

	SELECT 
		fcha_crre, 
		fcha_aprtra,
		bse,
		ttl_efctvo,
		sbrnte_crre,
		fltnte_crre,
		cnsctvo_mvmnto_cja,
		ttl_cja,
		dscrpcn_ofcna,
		nmbre_asi,
		ltrim(rtrim(usro)) as usro
	FROM #reporte

	DROP TABLE #reporte
END
GO

