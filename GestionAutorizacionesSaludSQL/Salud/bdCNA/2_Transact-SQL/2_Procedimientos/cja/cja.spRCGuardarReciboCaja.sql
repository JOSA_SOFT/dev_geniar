USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[[spRCGuardarReciboCaja]]    Script Date: 04/11/2016 8:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('cja.[spRCGuardarReciboCaja]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.[spRCGuardarReciboCaja] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : [spRCGuardarReciboCaja]
* Desarrollado por : <\A Jos� Olmedo Soto Aguirre <jose.soto@geniar.net> A\>
* Descripcion  : <\D Realiza el guardado del recibo de caja generado, con las OPS seleccionadas y relacionadas al documento. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P 
					cnsctvo_dcmnt_cja = Consecutivo Documento Caja
					nmro_unco_ops = numero unico ops seleccionada
					fcha_crcn = Fecha actual
					usro_crcn = usuario en sesion
				P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/11/02 FC\>
* Ejemplo: 
    <\EJ
	
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE [cja].[spRCGuardarReciboCaja] 
	@cnsctvo_dcmnt_cja udtConsecutivo,
	@nmro_unco_ops udtConsecutivo,
	@fcha_crcn datetime,
	@usro_crcn udtUsuario
AS
BEGIN
	SET NOCOUNT ON;

   Insert into bdCNA.cja.tbRCSoportesDocumentosCaja (cnsctvo_dcmnto_cja, nmro_unco_ops, fcha_crcn, usro_crcn, fcha_ultma_mdfccn, usro_ultma_mdfccn)  
   values(@cnsctvo_dcmnt_cja,@nmro_unco_ops,@fcha_crcn,@usro_crcn,@fcha_crcn,@usro_crcn)
   
END
GO

