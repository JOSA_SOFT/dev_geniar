USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('cja.spRCObtenerSoportesDocumento') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE cja.spRCObtenerSoportesDocumento AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : cja.spRCObtenerSoportesDocumento
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Obtiene los documentos soporte de un documento de caja. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/01/11 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC cja.spRCObtenerSoportesDocumento 1361
    EJ\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE cja.spRCObtenerSoportesDocumento(
	@cnsctvo_cdgo_dcmnto_cja udtConsecutivo
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @fechaActual datetime

	SET @fechaActual = getdate()

    SELECT a.nmro_unco_ops, b.vlr_lqdcn_cta_rcprcn_srvco_ops, b.cnsctvo_cdgo_cncpto_pgo, c.dscrpcn_cncpto_pgo
	FROM cja.tbRCSoportesDocumentosCaja a WITH(NOLOCK)
		INNER JOIN gsa.TbAsConsolidadoCuotaRecuperacionxOps b WITH(NOLOCK) On a.nmro_unco_ops = b.nmro_unco_ops
		INNER JOIN bdsisalud.dbo.tbConceptosPago_Vigencias c WITH(NOLOCK) On b.cnsctvo_cdgo_cncpto_pgo = c.cnsctvo_cdgo_cncpto_pgo
	WHERE  cnsctvo_dcmnto_cja = @cnsctvo_cdgo_dcmnto_cja
	AND @fechaActual between c.inco_vgnca and c.fn_vgnca

END
GO
