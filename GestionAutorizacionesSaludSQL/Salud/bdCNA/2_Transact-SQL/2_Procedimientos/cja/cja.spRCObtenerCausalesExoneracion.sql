USE [bdCNA]
GO
/****** Object:  StoredProcedure [cja].[spRCObtenerCausalesExoneracion]    Script Date: 01/06/2017 14:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG     : cja.spRCObtenerCausalesExoneracion
* Desarrollado por : <\A Se encarga de obtener las causales de exoneracion de las OPS. A\>
* Descripcion      : <\D Campo para documentar la descripcion D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>
* Variables        : <\V V\>
* Fecha Creacion   : <\FC 2016/12/20 FC\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp que no tome las cuotas de recuperacion anuladas
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/
--exec cja.spRCObtenerCausalesExoneracion 2380 

ALTER PROCEDURE [cja].[spRCObtenerCausalesExoneracion]
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @no_genera_cobro        udtLogico     ,
			@cotizante              udtConsecutivo,
			@valor_cero             udtConsecutivo,
			@FechaActual            Datetime      ,
			@estdo_cta_rcprcn_anldo udtConsecutivo

	Set @cotizante              = 9
	Set @valor_cero             = 0
	Set @FechaActual            = getdate()
	Set @estdo_cta_rcprcn_anldo = 4 --Cuota de recuperacion Anulado
	
	Select     Distinct cnsctvo_cdgo_csa_no_cbro_cta_rcprcn, b.dscrpcn_csa_exnrcn_cta_rcprcn
	From       BdCna.gsa.TbAsDetCalculoCuotaRecuperacion a With(Nolock)
	Inner Join bdcna.prm.tbAsCausasExoneracionCuotaRecuperacion_Vigencias b With(Nolock)
	On         b.cnsctvo_cdgo_csa_exnrcn_cta_rcprcn   = a.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
	Where      a.cnsctvo_slctd_atrzcn_srvco           = @cnsctvo_slctd_atrzcn_srvco
	And        a.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn != @cotizante --'los cotizantes no sacarles mensaje'
	And        a.nmro_unco_ops                       != @valor_cero
	And        a.cnsctvo_cdgo_estdo_cta_rcprcn       != @estdo_cta_rcprcn_anldo
	And        @FechaActual Between b.inco_vgnca And b.fn_vgnca 

END
