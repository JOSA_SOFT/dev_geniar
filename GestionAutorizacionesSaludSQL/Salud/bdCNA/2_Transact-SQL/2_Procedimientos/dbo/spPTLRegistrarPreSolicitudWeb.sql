USE [bdCNA]
GO
/****** Object:  StoredProcedure [dbo].[spPTLRegistrarPreSolicitudWeb]    Script Date: 24/01/2017 10:59:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*---------------------------------------------------------------------------------------------------------------------------------------
* Procedimiento			:  spPTLRegistrarPreSolicitudWeb
* Desarrollado por		: <\A Carlos Andres Moreno Mayor A\>
* Modificado por        : < Diego Blanco>
* Descripción			: <\D Se encarga de registrar las pre-solicitudes D\>
* Observaciones			: <\O O\>
* Parámetros			: <\P @usro_crcn Usuario de creación,
							  @usro_ultma_mdfccn Usuario ultima modificación,
							  @nmro_unco_idnttfccn_usro Numero unico identificación del usuario,
							  @nmro_unco_idntfccn_afldo Numero unico identificación del afiliado,
							  @indcdr_rcn_ncdo Indicador si es recien nacido,
							  @indcdr_aprbcn_jrmntda Indicador si aprobó la notificación juramentada,
							  @cnsctvo_cdgo_estdo_prstcn_slctd Referencia al Estado de la pre solicitud,
							  @obsrvcns Observaciones de la pre-solicitud,
							  @orgn_prslctd Origen de la pre-solicitud P\>
* Variables				: <\V  	V\>
* Fecha Creación		: <\FC 03/07/2015 2:19 pm FC\>
*------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		: <\AM Ing. Darling Liliana Dorado AM\>    
* Descripcion			: <\DM Solucion error reportado en el Quick 2016-00000-000034311, donde el usuario del sistema
							   se toma como el correo del afiliado logueado, pero este correo puede ser de 100 caracteres,
							   pero el campo de usuario en la tablas esta de 30. Dado esto se ajusta lo siguiente:
							   * Se ajusta el parametro @usro_crcn [dbo].[udtUsuario] por @usro_crcn Varchar(100)
							   DM\>
* Nuevas Variables		: <\VM  VM\>    
* Fecha Modificacion    : <\FM  FM\>   
* Modificado Por		: <\AM  AM\>    
* Descripcion			: <\DM  20160714 DM\>  
*------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		: <\AM Luis Fernando Benavides AM\>    
* Descripcion			: <\DM Integracion con proyecto MEGA, entregable transcripcion de solicitudes. Se invoca al 
							   procedimiento almacenado gsa.spASRegistrarDatosAdicionalesPreSolicitud
							   DM\>
* Nuevas Variables		: <\VM  VM\>    
* Fecha Modificacion    : <\FM  FM\>   
* Modificado Por		: <\AM  AM\>    
* Descripcion			: <\DM  20160817 DM\>  
*---------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spPTLRegistrarPreSolicitudWeb](
	       @cnsctvo_rgstro_usro_wb INT,
           @nmro_unco_idntfccn_afldo INT,
           @indcdr_rcn_ncdo [dbo].[udtLogico],
           @aprbcn_jrmntda [dbo].[udtLogico],
           @cnsctvo_cdgo_estdo_prslctd INT,
           @cnsctvo_cdgo_orgn_prslctd INT,
           @cdgo_tpo_dcmnto char(3),
           @nmro_idntfccn varchar(23),
           @nmbre_cmplto_afldo varchar(100),
           @cnsctvo_cdgo_pln INT,
           @obsrvcns varchar(500),
           @usro_crcn varchar(100),
		   @usro_ultma_mdfccn varchar(100)
)


AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @maximoRegistro int
	DECLARE @maximoRegistro1 int
	DECLARE @cdgo_rsltdo		udtconsecutivo
	DECLARE @mnsje_rsltdo		varchar(2000)
	DECLARE @usro_crcn_aplccn	udtUsuario	= Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

	SELECT @maximoRegistro = isnull(MAX(cnsctvo_prslctd),0) + 1 
	FROM [BDCna].[dbo].[tbPreSolicitudes] With(NoLock)

INSERT INTO [dbo].[tbPreSolicitudes]
           ([cnsctvo_prslctd]
		   ,[cnsctvo_rgstro_usro_wb]
           ,[nmro_unco_idntfccn_afldo]
           ,[indcdr_rcn_ncdo]
           ,[aprbcn_jrmntda]
           ,[cnsctvo_cdgo_estdo_prslctd]
           ,[cnsctvo_cdgo_orgn_prslctd]
           ,[cdgo_tpo_dcmnto]
           ,[nmro_idntfccn]
           ,[nmbre_cmplto_afldo]
           ,[cnsctvo_cdgo_pln]
           ,[obsrvcns]
           ,[fcha_crcn]
           ,[usro_crcn]
		   ,[usro_ultma_mdfccn]
		   ,[fcha_ultma_mdfccn])
	VALUES (@maximoRegistro
	       ,@cnsctvo_rgstro_usro_wb
		   ,@nmro_unco_idntfccn_afldo
		   ,@indcdr_rcn_ncdo
		   ,@aprbcn_jrmntda
		   ,@cnsctvo_cdgo_estdo_prslctd
		   ,@cnsctvo_cdgo_orgn_prslctd
		   ,@cdgo_tpo_dcmnto
		   ,@nmro_idntfccn
		   ,@nmbre_cmplto_afldo
		   ,@cnsctvo_cdgo_pln
		   ,CONCAT ( 'Solicitud #', @maximoRegistro, ' ingresada por la web el ', CONVERT(VARCHAR(10), GETDATE(), 103) )
		   ,getDate()
		   ,@usro_crcn
		   ,@usro_ultma_mdfccn
		   ,getDate())

SELECT [cnsctvo_prslctd]
      ,[cnsctvo_rgstro_usro_wb]
      ,[nmro_unco_idntfccn_afldo]
      ,[indcdr_rcn_ncdo]
      ,[aprbcn_jrmntda]
      ,[cnsctvo_cdgo_estdo_prslctd]
      ,[cnsctvo_cdgo_orgn_prslctd]
      ,[cdgo_tpo_dcmnto]
      ,[nmro_idntfccn]
      ,[nmbre_cmplto_afldo]
      ,[cnsctvo_cdgo_pln]
      ,[obsrvcns]
      ,[fcha_crcn]
      ,[usro_crcn]
      ,[fcha_ultma_mdfccn]
      ,[usro_ultma_mdfccn]
  FROM [BDCna].[dbo].[tbPreSolicitudes] With(NoLock) 
  Where cnsctvo_prslctd = @maximoRegistro

  SELECT @maximoRegistro1 = isnull(MAX(cnsctvo_hstrco_estdo_prslctd),0) + 1 
  FROM [BDCna].[dbo].[tbHistoricoEstadosPreSolicitud] With(NoLock)

  INSERT INTO [BDCna].[dbo].[tbHistoricoEstadosPreSolicitud]
           ([cnsctvo_hstrco_estdo_prslctd]
		   ,[cnsctvo_prslctd]
           ,[cnsctvo_cdgo_estdo_prslctd]
           ,[inco_vgnca]
           ,[fn_vgnca]
           ,[fcha_fn_vldz_rgstro]
           ,[vldo]
           ,[fcha_crcn]
           ,[usro_crcn]
           ,[fcha_ultma_mdfccn]
           ,[usro_ultma_mdfccn])

	values ( @maximoRegistro1
		         ,@maximoRegistro
				 ,@cnsctvo_cdgo_estdo_prslctd
				 ,getDate()
	             ,'9999-12-31 23:59:59'
				 ,'9999-12-31 23:59:59'
				 ,'S'
				 ,Getdate()
				 ,@usro_crcn
				 ,Getdate()
                 ,@usro_ultma_mdfccn)

	/*20160817 sislbr01 Integracion MEGA*/
	Exec BDCna.gsa.spASRegistrarDatosAdicionalesPreSolicitud @maximoRegistro,  @usro_crcn_aplccn, @cdgo_rsltdo  output,  @mnsje_rsltdo output;

END



