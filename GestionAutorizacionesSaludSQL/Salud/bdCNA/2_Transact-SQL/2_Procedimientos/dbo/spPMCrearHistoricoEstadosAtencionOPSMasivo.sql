USE [BDCna]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('dbo.spPMCrearHistoricoEstadosAtencionOPSMasivo') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE dbo.spPMCrearHistoricoEstadosAtencionOPSMasivo AS SELECT 1;'
END
GO

/*---------------------------------------------------------------------------------
* Nombre			: spPMCrearHistoricoEstadosAtencionOPSMasivo 
* Desarrollado por	: <\A Luis Fernando Benavides								A>
* Descripcion		: <\D Se crea el registro Histórico Estados Atención OPS y  D\>
					  <\D se le asigna el estado 13 "Autorizado" Masivamente	D\>
* Observaciones		: <\O En base a: spPMCrearHistoricoEstadosAtencionOPS		O\>
* Parametros		: <\P  P\>
* Variables			: <\V  V\>
* Fecha Creacion	: <\FC 2016/09/26											FC\>
*---------------------------------------------------------------------------------
* DATOS DE MODIFICACION 
*---------------------------------------------------------------------------------
* Modificado Por	: <\AM  AM\>
* Descripcion		: <\DM  DM\>
* Nuevos Parametros	: <\PM	PM\>
* Nuevas Variables	: <\VM  VM\>
* Fecha Modificacion: <\FM  FM\>
-----------------------------------------------------------------------------------*/

/*
CREATE TABLE #serviciosAutorizados_Impresos(	
	id						int identity(1,1), 	cnsctvo_srvco_slctdo	udtconsecutivo,
	nuam					numeric(18,0),		cnsctvo_cdgo_ofcna		int,
	cnsctvo_atncn_ops		int,				usro					udtusuario);

INSERT INTO #serviciosAutorizados_Impresos (nuam,cnsctvo_cdgo_ofcna,usro)
SELECT 1099964,	5,'sislbr01' UNION ALL
SELECT 1099965,	5,'sislbr01' UNION ALL 
SELECT 2897352,	1,'sislbr01' UNION ALL 
SELECT 2897351,	1,'sislbr01'

EXEC spPMCrearHistoricoEstadosAtencionOPSMasivo

DROP TABLE #serviciosAutorizados_Impresos
*/

ALTER PROCEDURE [dbo].[spPMCrearHistoricoEstadosAtencionOPSMasivo]
AS
Begin
    SET NOCOUNT ON

	DECLARE @fechaActual							datetime		= GETDATE(),
			@fechaFin								datetime		= '99991231',
			@lcnsctvo_hstrco_estds_atncn_ops		udtConsecutivo	= 0,
			@lcvldo									udtLogico		= 'S',			
			@lncnsctvo_cdgo_estdo_atncn				udtConsecutivo	= 13 /*Estado "Autorizado"*/

	/* Obtengo el consecutivo de atención OPS */
	UPDATE		b
	SET			b.cnsctvo_atncn_ops = a.cnsctvo_atncn_ops
	FROM		BDSiSalud.dbo.tbAtencionOPS a with(nolock) 
	INNER JOIN	#serviciosAutorizados_Impresos b with(nolock)
	ON 			a.nuam = b.nuam AND	a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna

	/* Obtengo ultimo consecutivo tabla historico estados atencion OPS */
	SELECT @lcnsctvo_hstrco_estds_atncn_ops = MAX(cnsctvo_hstrco_estds_atncn_ops) FROM dbo.tbHistoricoEstadosAtencionOPS with(nolock)

	INSERT	tbHistoricoEstadosAtencionOPS(
			cnsctvo_hstrco_estds_atncn_ops,
			cnsctvo_atncn_ops,
			cnsctvo_cdgo_estdo_atncn,
			inco_vgnca,
			fn_vgnca,
			fcha_fn_vldz_rgstro,
			vldo,
			fcha_crcn,
			usro_crcn,
			fcha_ultma_mdfccn,
			usro_ultma_mdfccn
			)
	SELECT 
			ROW_NUMBER() OVER(ORDER BY id DESC) + @lcnsctvo_hstrco_estds_atncn_ops,
			cnsctvo_atncn_ops,
			@lncnsctvo_cdgo_estdo_atncn,
			@fechaActual,
			@fechaFin,
			@fechaFin,
			@lcvldo,
			@fechaActual,
			usro,
			@fechaActual,
			usro
	FROM	#serviciosAutorizados_Impresos
End
GO
