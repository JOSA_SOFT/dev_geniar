USE [bdSisalud]
GO
/****** Object:  StoredProcedure [dbo].[spConsultarPrestadoresParaDireccionamiento]    Script Date: 13/01/2017 8:11:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spConsultarPrestadoresParaDireccionamiento
* Desarrollado por   : <\A Ing. Juan Carlos Vásquez G.													A\>    
* Descripcion        : <\D Procedimiento Consultas de Prestadores Para Direccionamiento					D\>
          				    
* Observaciones      : <\O      O\>    
* Parametros         : <\P   P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 2016/03/16  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Luis Fernando Benavides  AM\>    
* Descripcion        : <\D  Adiciona resultado de consulta razon social, consecutivo tipo identificacion D\> 
*					   <\D  y numero de identificacion	D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/05/03 FM\>    
*-----------------------------------------------------------------------------------------------------

*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez G  AM\>    
* Descripcion        : <\D  Adiciona consulta prestadores que no se encuentra direccionados D\> 
*					   <\D																				D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/06/15 FM\>    
*-----------------------------------------------------------------------------------------------------

*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez G  AM\>    
* Descripcion        : <\D  Adiciona filtro para excluir ips genérica	y concatenar nombre de la sucursal 
							y dirección  D\> 
*					   <\D																				D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/07/07 FM\>    
*-----------------------------------------------------------------------------------------------------

*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Juan David Reina y Ing. Carlos Andrés López Ramírez  AM\>    
* Descripcion        : <\D  Se anexa consulta para obtener la sede de la ciudad y se modifican las 
							consultas de los prestadores para el direccionamiento de forma que estos 
							se obtengan a partir de la sede y no de la ciudad  D\> 
*					   <\D																				D\> 
* Nuevas Variables   : <\VM @cnsctvo_cdgo_sde VM\>    
* Fecha Modificacion : <\FM 2016/11/22 FM\>    
*-----------------------------------------------------------------------------------------------------



*/
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 30443,1, 8112
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 30221,1, 8606
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 30462,1, 7360
-- exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento 27600,1, 8112

ALTER PROCEDURE [dbo].[spConsultarPrestadoresParaDireccionamiento] 

/*01*/		@cnsctvo_cdfccn			UdtConsecutivo,
/*02*/		@cnsctvo_cdgo_pln		UdtConsecutivo,
/*03*/		@cnsctvo_cdgo_cdd	    UdtConsecutivo 

AS

Begin
		Set NoCount On

		Declare @fechaactual		datetime = getdate(),
				@cdgo_intrno_gnrco	UdtCodigoIPS = '5999',
				@cnsctvo_cdgo_sde		udtConsecutivo 

		Select @cnsctvo_cdgo_sde=cnsctvo_sde_inflnca from bdafiliacionvalidador.dbo.tbciudades_vigencias sv
		where sv.cnsctvo_cdgo_cdd=@cnsctvo_cdgo_cdd

		Create Table #tbTmpPrestadoresDireccionamiento
		(
		cnsctvo_cdfccn				UdtConsecutivo,
		cdgo_cdfccn					char(11),
		dscrpcn_cdfccn				UdtDescripcion,
		cdgo_intrno					UdtCodigoIPS,		
		nmbre_scrsl					Varchar(250),
		drccn						UdtDireccion,
		vlr_trfdo					Float,
		cnsctvo_cdgo_cdd			UdtConsecutivo,
		nmro_unco_idntfccn_prstdr	UdtConsecutivo,
		rzn_scl						varchar(200),
		cnsctvo_cdgo_tpo_idntfccn	UdtConsecutivo,
		cdgo_tpo_idntfccn			char(3),
		dscrpcn_tpo_idntfccn		varchar(150),
		nmro_idntfccn				UdtNumeroIdentificacionLargo
		)

		Insert #tbTmpPrestadoresDireccionamiento
		(
					cnsctvo_cdfccn,			cdgo_intrno,			nmbre_scrsl,		
					vlr_trfdo,				cnsctvo_cdgo_cdd,		nmro_unco_idntfccn_prstdr,
					drccn
		)
		Select 
					d.cnsctvo_cdfccn,		d.cdgo_intrno,			concat(d1.nmbre_scrsl,' - ',d1.drccn),
					d2.vlr_trfdo,			d1.cnsctvo_cdgo_cdd,	d1.nmro_unco_idntfccn_prstdr,
					d1.drccn
		From		bdcna.sol.tbCNADireccionamientoPrestadorDestino d with(nolock)
		Inner Join	dbo.tbDireccionesPrestador d1	with(nolock)
		On			d1.cdgo_intrno = d.cdgo_intrno
		inner join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d2 with (nolock)	
		on			d2.cnsctvo_cdgo_dt_lsta_prco = d.cnsctvo_cdgo_dt_lsta_prco
		Where		d.cnsctvo_cdfccn = @cnsctvo_cdfccn
		And			d.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
		And			(d1.cnsctvo_cdgo_sde = @cnsctvo_cdgo_sde or d1.cnsctvo_cdgo_sde is null or d1.cnsctvo_cdgo_sde = 0)
		And			@fechaactual BetWeen d.inco_vgnca And d.fn_vgnca
		And			@fechaactual BetWeen d1.inco_vgnca And d1.fn_vgnca
		And			@fechaactual BetWeen d2.inco_vgnca And d2.fn_vgnca
		And			d.cdgo_intrno != @cdgo_intrno_gnrco


		Insert		#tbTmpPrestadoresDireccionamiento
		(
					cnsctvo_cdfccn,			cdgo_intrno,			nmbre_scrsl,		
					vlr_trfdo,				cnsctvo_cdgo_cdd,		nmro_unco_idntfccn_prstdr,
					drccn
		)
		Select 
					c.cnsctvo_prstncn,		a.cdgo_intrno,			concat(d.nmbre_scrsl,' - ',d.drccn),
					c.vlr_trfdo,			d.cnsctvo_cdgo_cdd,		d.nmro_unco_idntfccn_prstdr,
					d.drccn
		from		bdSisalud.dbo.tbAsociacionModeloActividad a with (nolock)			
		inner join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan b with (nolock)	
		on			a.cnsctvo_asccn_mdlo_actvdd = b.cnsctvo_asccn_mdlo_actvdd													
		inner join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan c with (nolock)	
		on			b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
		Inner Join	dbo.tbDireccionesPrestador d	with(nolock)
		On			d.cdgo_intrno = a.cdgo_intrno
		Left Join  #tbTmpPrestadoresDireccionamiento e
		On			e.cnsctvo_cdfccn = c.cnsctvo_prstncn
		And			e.cdgo_intrno = a.cdgo_intrno
		And			e.cnsctvo_cdgo_cdd = d.cnsctvo_cdgo_cdd
		where		@fechaactual between a.inco_vgnca and a.fn_vgnca 
		And			@fechaactual between b.inco_vgnca and b.fn_vgnca
		And			@fechaactual between c.inco_vgnca and c.fn_vgnca
		And			@fechaactual BetWeen d.inco_vgnca And d.fn_vgnca
		And			c.cnsctvo_prstncn = @cnsctvo_cdfccn
		And			a.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
		And			(d.cnsctvo_cdgo_sde = @cnsctvo_cdgo_sde or d.cnsctvo_cdgo_sde is null or d.cnsctvo_cdgo_sde = 0)
		And			e.cdgo_cdfccn is null
		And			d.cdgo_intrno != @cdgo_intrno_gnrco


		/* Ips */
		Update		#tbTmpPrestadoresDireccionamiento 
		Set			rzn_scl						= b.rzn_scl, 
					cnsctvo_cdgo_tpo_idntfccn	= a.cnsctvo_cdgo_tpo_idntfccn, 
					nmro_idntfccn				= a.nmro_idntfccn
		From		#tbTmpPrestadoresDireccionamiento c
		Inner join	dbo.tbprestadores a with(nolock)
		On			c.nmro_unco_idntfccn_prstdr = a.nmro_unco_idntfccn_prstdr
		Inner join	dbo.tbips b with(nolock)
		On			a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

		/* Medicos */
		Update		#tbTmpPrestadoresDireccionamiento
		Set			rzn_scl						= CONCAT(LTRIM(RTRIM(b.prmr_nmbre_afldo)),' ',LTRIM(RTRIM(b.sgndo_nmbre_afldo)),' ',
												  LTRIM(RTRIM(b.prmr_aplldo)),' ',LTRIM(RTRIM(b.sgndo_aplldo))), 
					cnsctvo_cdgo_tpo_idntfccn	= a.cnsctvo_cdgo_tpo_idntfccn, 
					nmro_idntfccn				= a.nmro_idntfccn
		From		#tbTmpPrestadoresDireccionamiento c
		Inner join	dbo.tbprestadores a with(nolock)
		On			c.nmro_unco_idntfccn_prstdr = a.nmro_unco_idntfccn_prstdr
		Inner join	dbo.tbmedicos b with(nolock)
		On			a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

		/* Tipo Identificacion */
		Update		#tbTmpPrestadoresDireccionamiento
		Set			dscrpcn_tpo_idntfccn	= d.dscrpcn_tpo_idntfccn,
					cdgo_tpo_idntfccn		= d.cdgo_tpo_idntfccn
		From		#tbTmpPrestadoresDireccionamiento c 
		Inner join	BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias d with(nolock)
		On			c.cnsctvo_cdgo_tpo_idntfccn = d.cnsctvo_cdgo_tpo_idntfccn
		Where		@fechaactual between d.inco_vgnca and d.fn_vgnca

		-- mostrar resultado consulta
		Select 	distinct	
					cdgo_intrno				,		
					nmbre_scrsl				,
					drccn					,	
					vlr_trfdo				,
					rzn_scl					,
					cdgo_tpo_idntfccn		,
					dscrpcn_tpo_idntfccn,
					nmro_idntfccn
		From		#tbTmpPrestadoresDireccionamiento
		Order By	vlr_trfdo
		
		Drop Table #tbTmpPrestadoresDireccionamiento
End
