USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASProcesoLiquidacion]    Script Date: 22/06/2017 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     		: spASProcesoLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS A\>
* Descripcion			: <\D 
                              Orquesta los SPs encargados de generar la liquidación
							  y/o reliquidación									
						   D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 01/06/2016 FC\>
*-------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM	
                                Se adiciona llamado a SP spASGuardarTrazaIndicadorReLiquidacion 
								para que guarde la traza para mostrar en los indicadores	
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/08/12 FM\>
*-------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para que el proceso de alistamiento se ejecute teniendo en cuenta
								la fecha estimada del dia ejecucion del proceso y el dia anterior
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/04/05 FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza modificacion del procedimiento para que los valores del Detalle de Lista Precios
							 se calculen teniendo en cuenta la lista vigente para el modelo de prestacion y se puedan 
							 tomar los paquetes en la liquidacion
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 06/06/2016		FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza modificacion del procedimiento para que agrupe las prestaciones x item de presupuesto
							 con el fin de que realice la liquidacion correctamente
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 22/06/2016		FM\>
*----------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASProcesoLiquidacion]	
	@fcha_entrga		DATE,
	@es_btch			udtlogico,
	@mensaje			VARCHAR(1000) OUTPUT,
	@codigo_error		VARCHAR(100) OUTPUT	
AS
BEGIN
  SET NOCOUNT ON; 

	DECLARE @usr_lgn						udtusuario,
			@tpo_prcso						udtcodigo	= 'LQ',
            @actlzr							udtlogico	= 'A',
            @estdo_cntrl					udtlogico,
            @cdgo_err						CHAR(5)		= 'ERROR',
            @cdgo_exito						CHAR(2)		= 'OK',
            @no_es_btch						udtlogico	= 'N',
			@si_es_btch						udtlogico	= 'S',
			@cnsctvo_cdgo_tpo_prcso			INT			= 22, --Liquidacion Masiva; bdProcesosSalud.dbo.tbTiposProceso
			@cnsctvo_prcso					INT,
			@cnsctvo_lg						INT,
			@estdo_prcso_ok					NUMERIC(1)  = 2,
			@cnsctvo_prcso_err				INT			= 3,
			@prcso_crga_infrmcn				INT			= 125,
			@prcso_elmnar_cncpts			INT			= 126,
			@prcso_lqdar_cncpts				INT			= 127,
			@prcso_grdar_rsltdo				INT			= 128,
			@prcso_gnrar_ops				INT			= 129,
			@prcso_rtrnar_rstdo				INT			= 130,
			@mrca_rlqdcn					udtlogico	= 'S',	
			@cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo = null,
			@lcUsrioAplcn					UdtUsuario,
			@vlr_incl						INT			= 1,
			@cntdr							INT			= 1,
			@cntdd							INT            ,
			@fcha_entrga_n_ds_ants			DATE           ,
			@fcha_entrga_msvo               DATE           ,
			@rlqdcn							INT			= 1;

	CREATE TABLE #cnslddo_slctud(
			cnsctvo_cnslddo_slctud						udtconsecutivo IDENTITY(1,1),
			cnsctvo_slctd_atrzcn_srvco					udtconsecutivo,
			cnsctvo_srvco_slctdo						udtconsecutivo,
			cnsctvo_cdgo_srvco_slctdo					udtconsecutivo,
			cnsctvo_cdgo_espcldd_mdco_trtnte			udtconsecutivo,
			cdgo_intrno									CHAR(8),
			cnsctvo_cdgo_pln							udtconsecutivo,
			cnsctvo_cdgo_frma_atncn						udtconsecutivo,
			cntdd_slctda								INT, 
			cnsctvo_prcdmnto_insmo_slctdo				UdtConsecutivo, 
			cnsctvo_mdcmnto_slctdo						UdtConsecutivo,
			nmro_unco_ops								UdtConsecutivo,
			fcha_estmda_entrga							DATE,
			mrca_rlqdcn									udtlogico DEFAULT 'N'		  	
	)

    CREATE TABLE #tbConceptoGastosTmp_1 (			
		cnsctvo_srvco_lqdr				udtconsecutivo identity(1,1),
		cnsctvo_slctd_atrzcn_srvco		udtconsecutivo,
		cnsctvo_cdgo_cps				udtconsecutivo,
		cnsctvo_cdgo_frma_atncn			udtconsecutivo,
		bltrl							CHAR(1),
		msma_va							CHAR(1),
		cntdd							INT,
		rmpe_pqte						CHAR(1),
		espcldd							INT,
		vlr_lqdcn						INT				DEFAULT 0,
		agrpdr_srvco_lqdr				udtConsecutivo           ,
		cnsctvo_srvco_slctdo            udtConsecutivo           ,
		cdgo_intrno_prstdr				CHAR(8),
		fcha_lqdcn						DATETIME,
		cnsctvo_cdgo_pln				udtconsecutivo,
		rsltdo_lqdcn					INT				DEFAULT 0,
		cnsctvo_pqte					INT				DEFAULT 0,
		cnsctvo_cdgo_dt_lsta_prco		udtconsecutivo	DEFAULT 0,
		cnsctvo_cdgo_lsta_prco			udtconsecutivo	DEFAULT 0,
		cnsctvo_srvco_lqdrc				udtconsecutivo,
		cnsctvo_cdgo_cncpto_gsto		udtConsecutivo,
		vlr_lqdcn_cncpto				INT				DEFAULT 0, 
		cnsctvo_cdgo_clse_hbtcn			udtconsecutivo  DEFAULT 0,
		rsltdo_lqdcnc					INT				DEFAULT 0, 
		cnsctvo_cdgo_cncpts_dt_lsta_prco udtconsecutivo DEFAULT 0,
		cnsctvo_prcdmnto_insmo_slctdo	udtconsecutivo,
		cnsctvo_mdcmnto_slctdo			udtconsecutivo,
		cnsctvo_ops						udtconsecutivo	DEFAULT 0,
		cnsctvo_cdgo_grpo_imprsn		udtconsecutivo	DEFAULT 0,
		cnsctvo_cdgo_frma_lqdcn_prstcn	udtconsecutivo	DEFAULT 0
	);

    CREATE TABLE #tbserviciosaliquidarorigen (
			cnsctvo_srvco_lqdr				udtconsecutivo,
			cnsctvo_cdgo_cps				udtconsecutivo,
			cnsctvo_cdgo_tpo_atncn			udtconsecutivo,
			bltrl							CHAR(1),
			msma_va							CHAR(1),
			cntdd							INT,
			rmpe_pqte						CHAR(1),
			espcldd							INT,
			vlr_lqdcn						INT,
			agrpdr_srvco_lqdr				INT,
			cdgo_intrno_prstdr				CHAR(8),
			fcha_lqdcn						DATETIME,
			cnsctvo_cdgo_pln				udtconsecutivo,
			rsltdo_lqdcn					INT,
			cnsctvo_pqte					INT,
			cnsctvo_cdgo_dt_lsta_prco		udtconsecutivo  DEFAULT 0,
			cnsctvo_cdgo_lsta_prco			udtconsecutivo  DEFAULT 0
    );

     CREATE TABLE #tbconceptosserviciosaliquidarorigen (
			cnsctvo_cncpto_lqdr				 udtconsecutivo,
			cnsctvo_srvco_lqdr				 udtconsecutivo,
			cnsctvo_cdgo_cncpto_gsto		 udtconsecutivo,
			vlr_lqdcn_cncpto				 INT,
			cnsctvo_cdgo_clse_hbtcn			 udtconsecutivo,
			rsltdo_lqdcn				 	 INT			DEFAULT 0,
			cnsctvo_cdgo_cncpts_dt_lsta_prco udtconsecutivo DEFAULT 0,
			cnsctvo_prstcn_pqte		         udtConsecutivo DEFAULT 0
    );

    CREATE TABLE #tmpresultadoliquidacion (
			cnsctvo_srvco_lqdr				udtconsecutivo,
			rsltdo_lqdcn					INT,
			mnsj_lqdcn						NVARCHAR(MAX),
			vlr_lqdcn						INT,
			cnsctvo_cdgo_cncpto_gsto		udtconsecutivo,
			vlr_lqdcn_cncpto				INT,
			rsltdo_lqdcnc					INT,
			dscrpcn_cncpto_gsto				NVARCHAR(MAX) 
    );	

	CREATE TABLE #tmpSolicitudAnulacionCuotasRecuperacion(
			cnsctvo_slctd_atrzcn_srvco		 udtconsecutivo,
			cnsctvo_cdgo_srvco_slctdo		 udtconsecutivo,
			nmro_unco_ops					 udtconsecutivo
	)	

	CREATE TABLE #tmpConsolidadoLiquidacionxPrestacion (
			cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
			cnsctvo_cdgo_cps			udtConsecutivo,
			vlr_lqdcn					int
	)

	SET @usr_lgn          = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user));
	SET @lcUsrioAplcn     = @usr_lgn;
	Set @fcha_entrga_msvo = @fcha_entrga;

	BEGIN TRY
	
		BEGIN TRAN																

			IF	@es_btch = @no_es_btch
			BEGIN
				BEGIN TRY						
	
					EXEC gsa.spASCargarInformacionProcesoLiquidacion	
																						
					EXEC gsa.spASMarcaServiciosOnlineReLiquidacion																																																		
																			
					IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.mrca_rlqdcn = @mrca_rlqdcn)					
					BEGIN
						EXEC gsa.spASEliminarLiquidacion  @usr_lgn
					END																														
								
					EXEC gsa.spASAplicarLiquidacion				

				    EXEC gsa.spASGuardarResultadoLiquidacion	@usr_lgn

					--Se adiciona llamado para guardar traza de reliquidación
					EXEC BDcna.gsa.spASGuardarTrazaIndicadorReLiquidacion @usr_lgn,
																		  @rlqdcn,
																		  @mensaje OUTPUT,
																		  @codigo_error OUTPUT

					IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.mrca_rlqdcn = @mrca_rlqdcn AND CS.fcha_estmda_entrga = @fcha_entrga)
					BEGIN		
						 
						SELECT @cntdd = COUNT(1) FROM #tmpnmroslctds;
						WHILE @cntdr <= @cntdd  BEGIN	
																		
							SELECT @cnsctvo_slctd_atrzcn_srvco = ns.cnsctvo_slctd_srvco_sld_rcbda FROM #tmpnmroslctds ns
							WHERE  ns.id = @cntdr;

							EXEC gsa.spASEjecutarAlistamientoSolicitudEntrega	@cnsctvo_slctd_atrzcn_srvco,
																				@lcUsrioAplcn,
																				@mensaje OUTPUT,
																				@codigo_error OUTPUT
							SET @cntdr = @cntdr + @vlr_incl;
						END
						
					END
					
					EXEC gsa.spASRetornarResultadoLiquidacion 	
									
					SET @codigo_error = @cdgo_exito;

				END TRY
				BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					SET @codigo_error = @cdgo_err;
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				END CATCH				
			END
			ELSE IF @es_btch = @si_es_btch	
			BEGIN			  

			  --Se crea el proceso para la ejecución del servicio de Liquidación Masiva.
			  EXEC bdProcesosSalud.dbo.spPRORegistraProceso	@cnsctvo_cdgo_tpo_prcso,
															@usr_lgn,
															@cnsctvo_prcso OUTPUT

			  --Paso 1 Se carga la información del proceso de liquidación y se marcan los servicios que se deben reliquidar
			  BEGIN TRY
				EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																	   @cnsctvo_cdgo_tpo_prcso,
																	   'Paso 1. Cargar información del proceso de liquidación y marcan los servicios que se deben reliquidar',
																	   @prcso_crga_infrmcn,
																	   @cnsctvo_lg OUTPUT																	   				

				EXEC gsa.spASCargarInformacionProcesoLiquidacion

				EXEC gsa.spASMarcaServiciosMasivoReLiquidacion	@fcha_entrga_msvo,
																@mensaje OUTPUT,
																@codigo_error OUTPUT				

				EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

			  END TRY
			  BEGIN CATCH
				SET @mensaje =  'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
								'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
								'Message:'   + ERROR_MESSAGE() + CHAR(13) +
								'Procedure:' + ERROR_PROCEDURE();
				
				SET @codigo_error = @cdgo_err;
				
				EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada	@cnsctvo_lg,
																					@mensaje;
				RAISERROR (@codigo_error, 16, 2) With SETERROR
			  END CATCH

			  --Paso 1.1 Se eliminan los conceptos de gasto asociados a los servicios marcados para reliquidar
			  --y se actualiza la prestación a estado aprobada y el valor de la liquidación		  

			  IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.mrca_rlqdcn = @mrca_rlqdcn)
			  BEGIN
				  BEGIN TRY
					EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																		   @cnsctvo_cdgo_tpo_prcso,
																		   'Paso 1.1 Eliminar los conceptos de gasto para reliquidar y se actualiza la prestación 
																		    a estado aprobada y el valor de la liquidación',
																		   @prcso_elmnar_cncpts,
																		   @cnsctvo_lg OUTPUT
					
					EXEC gsa.spASEliminarLiquidacion	@usr_lgn
					
					EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

				  END TRY
				  BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					
					SET @codigo_error = @cdgo_err;
					
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				  END CATCH
			  END

			  --Paso 2 Se liquidan los conceptos de gasto asociados a las prestaciones
			  BEGIN
				  BEGIN TRY
					EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																		   @cnsctvo_cdgo_tpo_prcso,
																		   'Paso 2 Liquidar conceptos de gasto asociados a las prestaciones',
																		   @prcso_lqdar_cncpts,
																		   @cnsctvo_lg OUTPUT
					EXEC bdcna.gsa.spASAplicarLiquidacion				   
					EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

				  END TRY
				  BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					
					SET @codigo_error = @cdgo_err;
					
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				  END CATCH
			  END

			  --Paso 3 Se guarda el resultado de la liquidación
			  BEGIN
				  BEGIN TRY
					EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																		   @cnsctvo_cdgo_tpo_prcso,
																		   'Paso 3 Guardar resultado de la liquidación',
																		   @prcso_grdar_rsltdo,
																		   @cnsctvo_lg OUTPUT
					
					EXEC bdcna.gsa.spASGuardarResultadoLiquidacion @usr_lgn
					
					EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

				  END TRY
				  BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					
					SET @codigo_error = @cdgo_err;
					
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				  END CATCH
			   END

			  --Paso 3.1 Se genera el número único de OPS solo si es reliquidación o masivo con fecha de entrega
			  --del día siguiente
			  Set @fcha_entrga_n_ds_ants = DATEADD(DAY,-@vlr_incl, @fcha_entrga_msvo); 			
			  

			  IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.fcha_estmda_entrga In(@fcha_entrga_msvo, @fcha_entrga_n_ds_ants))
			  BEGIN
				  BEGIN TRY
					EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																		   @cnsctvo_cdgo_tpo_prcso,
																		   'Paso 3.1 Generar número único de OPS',
																		   @prcso_gnrar_ops,
																		   @cnsctvo_lg OUTPUT
					
					EXEC bdcna.gsa.spASEjecutarAlistamientoSolicitudEntrega @cnsctvo_slctd_atrzcn_srvco,
																		    @lcUsrioAplcn, 	
																		    @mensaje OUTPUT,
																		    @codigo_error OUTPUT
					
					EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

				  END TRY
				  BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					
					SET @codigo_error = @cdgo_err;
					
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				  END CATCH
			  END

			  --Paso 4 Se retorna el resultado de la liquidación
			  BEGIN
				  BEGIN TRY
					
					EXEC bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																		   @cnsctvo_cdgo_tpo_prcso,
																		   'Paso 4 Retornar el resultado de la liquidación',
																		   @prcso_rtrnar_rstdo,
																		   @cnsctvo_lg OUTPUT
					
					EXEC bdcna.gsa.spASRetornarResultadoLiquidacion
					
					EXEC bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

				  END TRY
				  BEGIN CATCH
					SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
									'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
									'Message:'   + ERROR_MESSAGE() + CHAR(13) +
									'Procedure:' + ERROR_PROCEDURE();
					
					SET @codigo_error = @cdgo_err;
					
					EXEC bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																					  @mensaje;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
				  END CATCH
			  END

			  -- Se registra el fin del proceso de liquidación
			  EXEC bdProcesosSalud.dbo.spPROActualizaEstadoProceso	@cnsctvo_prcso, 
																	@cnsctvo_cdgo_tpo_prcso, 
																	@estdo_prcso_ok

			  --Control de proceso de ejecución de liquidación			  
			  EXEC bdcna.gsa.spASControlProcesoSolicitud  @tpo_prcso,
														  @actlzr,
														  @usr_lgn,
														  @mensaje,
														  @estdo_cntrl OUTPUT 	
			  SET @codigo_error = @cdgo_exito;		
														  
			END;
	
			COMMIT
			
	END TRY
	BEGIN CATCH	
		
		ROLLBACK

		IF @es_btch = @si_es_btch	
		BEGIN
			-- Se envía la notificación
			EXEC bdProcesosSalud.dbo.spPROEnviarNotificacion	@cnsctvo_prcso, 
																@cnsctvo_cdgo_tpo_prcso, 
																@cnsctvo_prcso_err
		END			
			
	END CATCH
  
	DROP TABLE #cnslddo_slctud;
	DROP TABLE #tbConceptoGastosTmp_1;
	DROP TABLE #tbserviciosaliquidarorigen;
	DROP TABLE #tbconceptosserviciosaliquidarorigen;
	DROP TABLE #tmpresultadoliquidacion;	
	DROP TABLE #tmpSolicitudAnulacionCuotasRecuperacion;
	DROP TABLE #tmpConsolidadoLiquidacionxPrestacion;

END;

