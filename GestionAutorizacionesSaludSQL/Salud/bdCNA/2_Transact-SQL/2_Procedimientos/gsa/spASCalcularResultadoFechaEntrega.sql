USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCalcularResultadoFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCalcularResultadoFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite calcular la fecha de entrega acorde al grupo   D\>
					   <\D asignado a las prestaciones de fecha esperada de entrega	 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASCalcularResultadoFechaEntrega] @cnsctvo_prcso udtConsecutivo,
@es_btch char(1)
AS

BEGIN
  SET NOCOUNT ON
  DECLARE @fechaActual datetime,
		  @prestacionCums INT,
		  @prestacionCups INT,
		  @valorCero INT

  SELECT @fechaActual = GETDATE(),
		 @prestacionCums = 5,	
		 @prestacionCups = 4,
		 @valorCero = 0
	
  --Se actualiza el ítem de presupuesto para CUPS.
  UPDATE #tmpDatosSolicitudFechaEntrega
  SET cnsctvo_cdgo_itm_prspsto = cs.cnsctvo_cdgo_itm_prspsto
  FROM #tmpDatosSolicitudFechaEntrega tm
  INNER JOIN bdSisalud.dbo.tbCupsServicios cs WITH(NOLOCK)
	ON cs.cnsctvo_prstcn = tm.cnsctvo_cdfccn
  WHERE @fechaActual BETWEEN cs.inco_vgnca AND cs.fn_vgnca
	AND tm.cnsctvo_cdgo_tpo_srvco = @prestacionCups
	
  --Se actualiza el ítem de presupuesto para Cums.
  UPDATE #tmpDatosSolicitudFechaEntrega
  SET cnsctvo_cdgo_itm_prspsto = cs.cnsctvo_cdgo_itm_prspsto
  FROM #tmpDatosSolicitudFechaEntrega tm
  INNER JOIN bdSisalud.dbo.tbCums cs WITH(NOLOCK)
	ON cs.cnsctvo_cms = tm.cnsctvo_cdfccn
  WHERE @fechaActual BETWEEN cs.inco_vgnca AND cs.fn_vgnca
	AND tm.cnsctvo_cdgo_tpo_srvco = @prestacionCums
  
  --Se actualizan los días de más para la entrega cuando aplique la condición ítem presupuesto.
  UPDATE #tmpDatosSolicitudFechaEntrega
  SET fcha_estmda_entrga = bdSisalud.dbo.fnPeCalculaDiasHabiles(@fechaActual, fe.vlr_ds_entrga)
  FROM #tmpDatosSolicitudFechaEntrega tm
  INNER JOIN bdCNA.prm.tbASFechaEntrega_Vigencias fe WITH (NOLOCK)
    ON fe.cnsctvo_cdgo_grpo_entrga = tm.cnsctvo_grpo_entrga
	AND fe.cnsctvo_cdgo_pln = tm.cnsctvo_pln
	AND fe.cnsctvo_cdgo_itm_prspsto = tm.cnsctvo_cdgo_itm_prspsto
	AND fe.cnsctvo_cdgo_itm_prspsto  <> @valorCero
  WHERE @fechaActual BETWEEN fe.inco_vgnca AND fe.fn_vgnca

  --Se actualizan los días de más para la entrega cuando no aplique ítem presupuesto
  --Y no se haya establecido la fecha estimada de entrega.
  UPDATE #tmpDatosSolicitudFechaEntrega
  SET fcha_estmda_entrga = bdSisalud.dbo.fnPeCalculaDiasHabiles(@fechaActual, fe.vlr_ds_entrga)
  FROM #tmpDatosSolicitudFechaEntrega tm
  INNER JOIN bdCNA.prm.tbASFechaEntrega_Vigencias fe WITH (NOLOCK)
    ON fe.cnsctvo_cdgo_grpo_entrga = tm.cnsctvo_grpo_entrga
	AND fe.cnsctvo_cdgo_pln = tm.cnsctvo_pln
	AND fe.cnsctvo_cdgo_itm_prspsto  = @valorCero
  WHERE @fechaActual BETWEEN fe.inco_vgnca AND fe.fn_vgnca
   AND tm.fcha_estmda_entrga IS NULL;

   --Se realiza la agrupación para el grupo de más prioridad, para todas las pretaciones.
   WITH tmp AS (Select TDS.cnsctvo_slctd_srvco_sld_rcbda,RFE.cnsctvo_cdgo_grpo_entrga,GEV.cdgo_grpo_entrga, MIN(RFE.jrrqa) jrrqa
				from #tmpDatosSolicitudFechaEntrega TDS
				INNER JOIN bdCNA.[prm].[tbASReglasFechaEntrega_Vigencias] RFE WITH(NOLOCK)
				ON RFE.cnsctvo_cdgo_grpo_entrga = TDS.cnsctvo_grpo_entrga 
				AND @fechaActual BETWEEN RFE.inco_vgnca AND RFE.fn_vgnca
				INNER JOIN bdCNA.[prm].[tbASGruposEntrega_Vigencias] GEV WITH(NOLOCK)
				ON GEV.cnsctvo_cdgo_grpo_entrga = RFE.cnsctvo_cdgo_grpo_entrga
				AND @fechaActual BETWEEN GEV.inco_vgnca AND GEV.fn_vgnca
				GROUP BY cnsctvo_slctd_srvco_sld_rcbda,RFE.cnsctvo_cdgo_grpo_entrga,GEV.cdgo_grpo_entrga)
	UPDATE up
	SET cnsctvo_grpo_entrga_fnl = tmp.cnsctvo_cdgo_grpo_entrga,
		cdgo_grpo_entrga_fnl = tmp.cdgo_grpo_entrga
	FROM #tmpDatosSolicitudFechaEntrega up 
	INNER JOIN tmp tmp
	ON tmp.cnsctvo_slctd_srvco_sld_rcbda = up.cnsctvo_slctd_srvco_sld_rcbda;

	--Se realiza la agrupación para selecccionar la fecha mayor, pero dentro de las prestaciones que estén asociadas al grupo de menor jerarquía por solicitud.
   WITH tmp AS (Select TDS.cnsctvo_slctd_srvco_sld_rcbda,RFE.cnsctvo_cdgo_grpo_entrga,GEV.cdgo_grpo_entrga, MAX(TDS.fcha_estmda_entrga) fcha_mxma
				from #tmpDatosSolicitudFechaEntrega TDS
				INNER JOIN bdCNA.[prm].[tbASReglasFechaEntrega_Vigencias] RFE WITH(NOLOCK)
				ON RFE.cnsctvo_cdgo_grpo_entrga = TDS.cnsctvo_grpo_entrga 
				AND @fechaActual BETWEEN RFE.inco_vgnca AND RFE.fn_vgnca
				INNER JOIN bdCNA.[prm].[tbASGruposEntrega_Vigencias] GEV WITH(NOLOCK)
				ON GEV.cnsctvo_cdgo_grpo_entrga = RFE.cnsctvo_cdgo_grpo_entrga
				AND @fechaActual BETWEEN GEV.inco_vgnca AND GEV.fn_vgnca
				INNER JOIN #tmpDatosSolicitudFechaEntrega TTDS
				ON TDS.cnsctvo_slctd_srvco_sld_rcbda = TTDS.cnsctvo_slctd_srvco_sld_rcbda
				AND TDS.cnsctvo_grpo_entrga = TTDS.cnsctvo_grpo_entrga_fnl
				GROUP BY TDS.cnsctvo_slctd_srvco_sld_rcbda,RFE.cnsctvo_cdgo_grpo_entrga,GEV.cdgo_grpo_entrga)
	UPDATE up
	SET fcha_estmda_entrga_fnl = tmp.fcha_mxma
	FROM #tmpDatosSolicitudFechaEntrega up 
	INNER JOIN tmp tmp
	ON tmp.cnsctvo_slctd_srvco_sld_rcbda = up.cnsctvo_slctd_srvco_sld_rcbda;
	
END

GO
