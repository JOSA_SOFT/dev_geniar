USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosDetalleIndicadoresBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultarDatosDetalleIndicadoresBPM]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultarDatosDetalleIndicadoresBPM] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosDetalleIndicadoresBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Este SP devuelve el consolidado de registros para
							  los indicadores que se muestran en el dashboard del BPM D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se adicionan consultas al SP para que devuelva el
								consolidado de los indicadores de la fase 3 DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2016/08/12	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosDetalleIndicadoresBPM]
@cnsctvo_indcdr		udtConsecutivo,
@fcha_inco			datetime,
@fcha_fn			datetime

AS
BEGIN

	SET NOCOUNT ON;
	
	Declare @cnsctvo_cdgo_mdo_cntcto_ips	 udtConsecutivo,
			@cnsctvo_indcdr_ips				 udtConsecutivo,
			@cnsctvo_cdgo_mdo_cntcto_afl	 udtConsecutivo,
			@cnsctvo_indcdr_afl				 udtConsecutivo,
			@cnsctvo_cdgo_mdo_cntcto		 udtConsecutivo,
			@cnsctvo_indcdr_exstnca_drcho	 udtConsecutivo,
			@cnsctvo_indcdr_rqre_adtra		 udtConsecutivo,
			@cnsctvo_indcdr_drccnmnto_prstdr udtConsecutivo,
			@cnsctvo_indcdr_prdctvdd		 udtConsecutivo,
			@cnsctvo_indcdr_rlqdcn			 udtConsecutivo,
			@cnsctvo_indcdr_cldd_dgtcn		 udtConsecutivo;  

	create table #TempDatosConsulta (
		id_cnsctvo						int identity(1,1) NOT NULL,
		da_smna							varchar(5),
		fcha_dtlle_rgstro				varchar(10),
		indcdr							udtConsecutivo,
		cnsctvo_cdgo_mdo_cntcto_slctd	udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		fcha_crcn						date
	)

	create table #TempDetIndicadores (
		id_cnsctvo						int identity(1,1) NOT NULL,
		ttl_rgstrs_indicador			int default 0,
		da_smna							varchar(10),
		ttl_rgstrs_gnrl					int default 0,
		fcha_hra						varchar(30)
	)

	create table #TempConsolidadoIndicadores (
		ttl_rgstrs_gnrl	int default 0,
		fcha_hra_gnrl		varchar(30)
	)

	create table #TempDetConsolidadoIndicadores (
		fcha_hra_gnrl		varchar(30)
	)

	Set @cnsctvo_cdgo_mdo_cntcto_ips	= 2;
	Set @cnsctvo_indcdr_ips				= 1;

	Set	@cnsctvo_cdgo_mdo_cntcto_afl	= 4;
	Set @cnsctvo_indcdr_afl				= 2;

	Set @cnsctvo_indcdr_exstnca_drcho	= 4;
	Set @cnsctvo_indcdr_rqre_adtra		= 6;
	Set @cnsctvo_indcdr_drccnmnto_prstdr= 7;
	Set @cnsctvo_indcdr_prdctvdd		= 5;
	Set @cnsctvo_indcdr_rlqdcn			= 9;
	Set @cnsctvo_indcdr_cldd_dgtcn		= 10;



	-- Se obtienen los datos para indicadores Sol ingresadas desde IPS y Afiliado
	if @cnsctvo_indcdr = @cnsctvo_indcdr_ips or @cnsctvo_indcdr = @cnsctvo_indcdr_afl 
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, sas.fcha_crcn
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK) 
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn;	

		if @cnsctvo_indcdr = @cnsctvo_indcdr_ips
		Begin
			Set @cnsctvo_cdgo_mdo_cntcto = @cnsctvo_cdgo_mdo_cntcto_ips;
		End

		if @cnsctvo_indcdr = @cnsctvo_indcdr_afl
		Begin
			Set @cnsctvo_cdgo_mdo_cntcto = @cnsctvo_cdgo_mdo_cntcto_afl;
		End

		Insert into #TempDetIndicadores(ttl_rgstrs_indicador, da_smna, fcha_hra )
		select count(1), da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr
		And cnsctvo_cdgo_mdo_cntcto_slctd = @cnsctvo_cdgo_mdo_cntcto
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn asc
		
		insert into #TempConsolidadoIndicadores ( 	ttl_rgstrs_gnrl, 	fcha_hra_gnrl	) 
		select count(1), fcha_dtlle_rgstro from #TempDatosConsulta
		group by  fcha_dtlle_rgstro

		Update di set ttl_rgstrs_gnrl = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl

	End

	-- Se obtienen los datos para indicadores Sol que no superan la validación de existencias y derecho
	if @cnsctvo_indcdr = @cnsctvo_indcdr_exstnca_drcho
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn;
				
		Insert into #TempDetIndicadores( ttl_rgstrs_gnrl, da_smna, fcha_hra )
		select count(1), da_smna, fcha_dtlle_rgstro  from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn asc


		Insert into #TempDetConsolidadoIndicadores( fcha_hra_gnrl )
		Select  FORMAT(rve.fcha_crcn, 'ddMMyyyy') FECHA_HORA from BDCna.gsa.tbASRegistroValidacionEspecial rve WITH (NOLOCK)
		Inner Join #TempDatosConsulta dc on dc.cnsctvo_slctd_atrzcn_srvco = rve.cnsctvo_slctd_atrzcn_srvco 

		insert into #TempConsolidadoIndicadores ( 	ttl_rgstrs_gnrl, 	fcha_hra_gnrl	)
		Select COUNT(1) , fcha_hra_gnrl from #TempDetConsolidadoIndicadores
		group by fcha_hra_gnrl

		Delete from #TempDetConsolidadoIndicadores;

		Update di set ttl_rgstrs_indicador = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl
	End

	-- Se obtienen los datos para indicadores Sol que no superan la validación de existencias y derecho
	if @cnsctvo_indcdr = @cnsctvo_indcdr_rqre_adtra
	Begin
		insert into #TempDatosConsulta(
				da_smna, 
				fcha_dtlle_rgstro,
				indcdr,
				fcha_crcn
		)
		Select  da_smna,  
				FORMAT(fcha_hra, 'ddMMyyyy') FECHA_HORA, 
				id_indcdr,
				di.fcha_hra
		from BDCna.ind.tbAsDetalleIndicador di  With(NoLock)
		where id_indcdr = @cnsctvo_indcdr
		And di.fcha_hra	between @fcha_inco and  @fcha_fn; 

		Insert into #TempDetIndicadores(ttl_rgstrs_indicador, da_smna, fcha_hra )
		select count(1), da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn asc

		Delete tdc 
		from #TempDatosConsulta tdc;

		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, cnsctvo_slctd_atrzcn_srvco)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn;

		insert into #TempConsolidadoIndicadores ( ttl_rgstrs_gnrl, 	fcha_hra_gnrl	) 
		select count(1), fcha_dtlle_rgstro from #TempDatosConsulta
		group by  fcha_dtlle_rgstro

		Update di set ttl_rgstrs_gnrl = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl

	End

	-- Se obtienen los datos para indicadores que tuvieron cambio de direccionamiento. 
	if @cnsctvo_indcdr = @cnsctvo_indcdr_drccnmnto_prstdr
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn
		from BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
		Where ss.fcha_crcn between  @fcha_inco and  @fcha_fn
		order by ss.fcha_crcn asc
		
		Insert into #TempDetIndicadores(ttl_rgstrs_gnrl, da_smna, fcha_hra )
		select count(1) total, da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr_drccnmnto_prstdr
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn
		

		Insert into #TempDetConsolidadoIndicadores( fcha_hra_gnrl )
		Select  FORMAT(rve.fcha_crcn, 'ddMMyyyy') fcha_dtlle_rgstro
		from BDCna.gsa.tbASHistoricoPrestadorServiciosSolicitados rve WITH (NOLOCK)
		Inner Join #TempDatosConsulta dc on dc.cnsctvo_slctd_atrzcn_srvco = rve.cnsctvo_slctd_atrzcn_srvco

		insert into #TempConsolidadoIndicadores ( 	ttl_rgstrs_gnrl, 	fcha_hra_gnrl	)
		Select COUNT(1) , fcha_hra_gnrl from #TempDetConsolidadoIndicadores
		group by fcha_hra_gnrl

		Delete from #TempDetConsolidadoIndicadores;
		
		Update di set ttl_rgstrs_indicador = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl	
	End

	-- Se obtienen los datos para indicadores para medir la productividad de diligenciamiento
	if @cnsctvo_indcdr = @cnsctvo_indcdr_prdctvdd
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco, sas.fcha_crcn
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn
		And fcha_vncmnto_gstn is null
		order by sas.fcha_crcn asc;

		Insert into #TempDetIndicadores(ttl_rgstrs_gnrl, da_smna, fcha_hra )
		select count(1) total, da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr_prdctvdd
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn

		Delete tdc
		from #TempDatosConsulta tdc;


		insert into #TempDatosConsulta(	da_smna, fcha_dtlle_rgstro,	indcdr,	fcha_crcn)
		Select  da_smna, FORMAT(fcha_hra, 'ddMMyyyy') FECHA_HORA, id_indcdr, di.fcha_hra
		from BDCna.ind.tbAsDetalleIndicador di  With(NoLock)
		where id_indcdr = @cnsctvo_indcdr_prdctvdd
		And di.fcha_hra	between @fcha_inco and  @fcha_fn; 

		insert into #TempConsolidadoIndicadores ( ttl_rgstrs_gnrl, 	fcha_hra_gnrl	) 
		select count(1), fcha_dtlle_rgstro from #TempDatosConsulta
		group by  fcha_dtlle_rgstro

		Update di set ttl_rgstrs_indicador = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl

	End
	
	-- Se obtienen los datos para indicadores para medir las solicitudes que fueron reliquidadas
	if @cnsctvo_indcdr = @cnsctvo_indcdr_rlqdcn
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco, sas.fcha_crcn
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn
		And fcha_vncmnto_gstn is null
		order by sas.fcha_crcn asc;

		Insert into #TempDetIndicadores(ttl_rgstrs_gnrl, da_smna, fcha_hra )
		select count(1) total, da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr_rlqdcn
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn asc

		Delete tdc
		from #TempDatosConsulta tdc; 

		insert into #TempDatosConsulta(	da_smna, fcha_dtlle_rgstro,	indcdr,	fcha_crcn)
		Select  di.da_smna, FORMAT(di.fcha_hra, 'ddMMyyyy') FECHA_HORA, di.id_indcdr, di.fcha_hra
		from BDCna.ind.tbAsDetalleIndicador di  With(NoLock)
		Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock) 
		On sas.cnsctvo_slctd_atrzcn_srvco = di.VLR_NMRCO
		where di.id_indcdr = @cnsctvo_indcdr_rlqdcn
		And di.fcha_hra	between @fcha_inco and  @fcha_fn; 

		insert into #TempConsolidadoIndicadores ( ttl_rgstrs_gnrl, 	fcha_hra_gnrl	) 
		select count(1), fcha_dtlle_rgstro from #TempDatosConsulta
		group by  fcha_dtlle_rgstro

		Update di set ttl_rgstrs_indicador = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl
	End

	-- Se obtienen los datos para indicadores para medir la calidad de la digitación de los usuarios
	if @cnsctvo_indcdr = @cnsctvo_indcdr_cldd_dgtcn
	Begin
		insert into #TempDatosConsulta(
			da_smna, fcha_dtlle_rgstro, cnsctvo_cdgo_mdo_cntcto_slctd, indcdr, cnsctvo_slctd_atrzcn_srvco, fcha_crcn)
		Select  
			DATEPART(dw, fcha_crcn) DIA,	FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			cnsctvo_cdgo_mdo_cntcto_slctd, 	@cnsctvo_indcdr id_indcdr, cnsctvo_slctd_atrzcn_srvco, sas.fcha_crcn
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn
		order by sas.fcha_crcn asc;

		Insert into #TempDetIndicadores(ttl_rgstrs_gnrl, da_smna, fcha_hra )
		select count(1) total, da_smna, fcha_dtlle_rgstro from #TempDatosConsulta
		where indcdr = @cnsctvo_indcdr_cldd_dgtcn
		group by da_smna, fcha_dtlle_rgstro, fcha_crcn
		order by fcha_crcn asc

		Delete tdc from #TempDatosConsulta
		tdc;

		insert into #TempDatosConsulta(	da_smna, fcha_dtlle_rgstro,	indcdr,	fcha_crcn)
		Select  di.da_smna, FORMAT(di.fcha_hra, 'ddMMyyyy') FECHA_HORA, di.id_indcdr, di.fcha_hra
		from BDCna.ind.tbAsDetalleIndicador di  With(NoLock)
		Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock) 
		On sas.cnsctvo_slctd_atrzcn_srvco = di.VLR_NMRCO
		where di.id_indcdr = @cnsctvo_indcdr_cldd_dgtcn
		And di.fcha_hra	between @fcha_inco and  @fcha_fn; 

		insert into #TempConsolidadoIndicadores ( ttl_rgstrs_gnrl, 	fcha_hra_gnrl	) 
		select count(1), fcha_dtlle_rgstro from #TempDatosConsulta
		group by  fcha_dtlle_rgstro

		Update di set ttl_rgstrs_indicador = di2.ttl_rgstrs_gnrl
		from #TempDetIndicadores di 
		Inner join #TempConsolidadoIndicadores di2 on di.fcha_hra = di2.fcha_hra_gnrl
		
	End

	Select ttl_rgstrs_indicador, SUBSTRING (fcha_hra, 1, 4) fcha_hra,
		   ttl_rgstrs_gnrl from #TempDetIndicadores



	Drop table #TempDetIndicadores;
	Drop table #TempDatosConsulta;
	Drop table #TempConsolidadoIndicadores;
	Drop table #TempDetConsolidadoIndicadores
END


GO
