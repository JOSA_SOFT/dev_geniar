USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerDatosNotificacionAfiliadoPresolicitud]    Script Date: 05/10/2016 4:45:12 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASObtenerDatosNotificacionAfiliadoPresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASObtenerDatosNotificacionAfiliadoPresolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASObtenerDatosNotificacionAfiliadoPresolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D Carga la información del afiliado y del proveedor
							  en las tablas temporales para el procesamiento
							  del envío de la notificación de presolicitudes	D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/14										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jorge Rodriguez AM\>
* Descripcion			 : <\DM	Se adiciona logica para que permita poblar los nuevos camposDM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM			2017-01-06							FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASObtenerDatosNotificacionAfiliadoPresolicitud]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@fcha_actl		DATETIME,
			@valorCero		INT,
			@cdgo_prmtro_gnrl			CHAR(3),
			@cdgo_prmtro_gnrl_vgnca		CHAR(3),
			@vsble_usro                 udtLogico,
			@vlr_prmtro_nmrco           Numeric(18),
		    @vlr_prmtro_crctr           Char(200),
		    @vlr_prmtro_fcha            Datetime,
			@tpo_dto_prmtro		        udtLogico,
			@cdgo_prmtro_gnrl_ln		CHAR(3),
			@cdgo_prmtro_gnrl_vgnca_ln	CHAR(3),
			@cdgo_prmtro_gnrl_ac		CHAR(3),
			@cdgo_prmtro_gnrl_vgnca_ac	CHAR(3);;	

	

		SELECT @fcha_actl = GETDATE(),
			   @valorCero = 0,
			   @cdgo_prmtro_gnrl			= 111,
			   @cdgo_prmtro_gnrl_vgnca		= 111,
			   @vsble_usro					= 'S',
			   @tpo_dto_prmtro				= 'C',
			   @cdgo_prmtro_gnrl_ln			= 112,
			   @cdgo_prmtro_gnrl_vgnca_ln	= 112,
			   @cdgo_prmtro_gnrl_ac			= 92,
			   @cdgo_prmtro_gnrl_vgnca_ac	= 92;;	

		Insert Into #tmpAflds ( 
						id_sld_rcbda
						,cdgo_tpo_idntfccn_afldo
						,nmro_idntfccn_afldo
						,cnsctvo_cdgo_pln
						,nmro_unco_idntfccn_afldo
						,eml
						,cnsctvo_slctd_srvco_sld_rcbda
						)
		Select 
						TSO.id_sld_rcbda
						,PS.cdgo_tpo_dcmnto
						,PS.nmro_idntfccn
						,PS.cnsctvo_cdgo_pln
						,PS.nmro_unco_idntfccn_afldo
						,PS.usro_crcn
						,PS.cnsctvo_prslctd
		FROM			#tmpSlctdes				TSO
		INNER JOIN		dbo.tbPreSolicitudes	 PS WITH (NOLOCK)
		ON				TSO.cnsctvo_slctd_srvco_sld_rcbda = PS.cnsctvo_prslctd

		
		--actualiza el codigo del tipo de identificacion del afiliado
		Update     TSO
		Set		   cnsctvo_cdgo_tpo_idntfccn_afldo = t.cnsctvo_cdgo_tpo_idntfccn
		From       #tmpAflds				TSO
		Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
		On         t.cdgo_tpo_idntfccn = TSO.cdgo_tpo_idntfccn_afldo
		Where      @fcha_actl Between t.inco_vgnca And t.fn_vgnca

		--actualiza Datos del cliente
		Update     TSO
		Set        cnsctvo_cdgo_tpo_cntrto	= b.cnsctvo_cdgo_tpo_cntrto,
				   nmbre_afldo				= Concat(Ltrim(Rtrim(b.prmr_nmbre)),' ',
													 Ltrim(Rtrim(b.sgndo_nmbre)),' ',
													 Ltrim(Rtrim(b.prmr_aplldo)),' ',
													 Ltrim(Rtrim(b.sgndo_aplldo))),
				   nmro_cntrto				= b.nmro_cntrto,
				   cnsctvo_cdgo_rngo_slrl	= c.cnsctvo_cdgo_rngo_slrl,
				   cnsctvo_bnfcro_cntrto	= b.cnsctvo_bnfcro
				   	           
		From       #tmpAflds				TSO
		INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b	With(NoLock)
		On		   b.nmro_unco_idntfccn_afldo = TSO.nmro_unco_idntfccn_afldo
		INNER JOIN	BDAfiliacionValidador.dbo.tbContratosValidador				c	With(NoLock)	On	b.nmro_cntrto					=   c.nmro_cntrto
																				And	b.cnsctvo_cdgo_tpo_cntrto		=   c.cnsctvo_cdgo_tpo_cntrto
																				And c.cnsctvo_cdgo_pln				=   TSO.cnsctvo_cdgo_pln
		Inner Join	BDAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador	v	With(NoLock)	On	v.cnsctvo_cdgo_tpo_cntrto		=	b.cnsctvo_cdgo_tpo_cntrto
																				And	v.nmro_cntrto					=	b.nmro_cntrto
																				And	v.cnsctvo_bnfcro				=	b.cnsctvo_bnfcro
									
		Where	@fcha_actl Between v.inco_vgnca_estdo_bnfcro And v.fn_vgnca_estdo_bnfcro
		And		@fcha_actl Between b.inco_vgnca_bnfcro And b.fn_vgnca_bnfcro
		And		@fcha_actl Between c.inco_vgnca_cntrto And c.fn_vgnca_cntrto

		
		Update	TSO
		Set	cdgo_ips_prmra	= a.cdgo_ips,
			sde_afldo		= a.cdgo_sde
		From	#tmpAflds	TSO 
		Inner Join BDAfiliacionValidador.dbo.tbActuarioCapitacionValidador a With(NoLock)
		On	a.nmro_unco_idntfccn		= TSO.nmro_unco_idntfccn_afldo
		And		a.cnsctvo_cdgo_tpo_cntrto	= TSO.cnsctvo_cdgo_tpo_cntrto
		
		Update	TSO
		Set	cnsctvo_cdgo_sde_ips_prmra	= a.cnsctvo_cdgo_sde_ips
		From	#tmpAflds	TSO 
		Inner Join bdSisalud.dbo.tbIPSPrimarias_vigencias a With(NoLock)
		On	a.cdgo_intrno		= TSO.cdgo_ips_prmra
		Where	@fcha_actl between a.inco_vgnca and a.fn_vgnca

		UPDATE		TAF
		SET			TAF.cnsctvo_cdgo_estdo_drcho = ISNULL(EDE.cnsctvo_cdgo_estdo_drcho,@valorCero),
					TAF.dscrpcn_csa_drcho		 = CDV.dscrpcn_csa_drcho
		FROM		#tmpAflds													TAF 
		INNER JOIN	#tmpSlctdes													TSO 
					ON	TAF.id_sld_rcbda = TSO.id_sld_rcbda
		INNER JOIN	bdAfiliacionValidador.dbo.TbMatrizDerechosValidador			EDE	WITH(NOLOCK) 
					ON 	EDE.cnsctvo_cdgo_tpo_cntrto = TAF.cnsctvo_cdgo_tpo_cntrto	
					AND	EDE.nmro_cntrto = TAF.nmro_cntrto 
					AND	EDE.cnsctvo_bnfcro = TAF.cnsctvo_bnfcro_cntrto
		INNER JOIN	bdAfiliacionValidador.dbo.tbCausasDerechoValidador			CDV	WITH(NOLOCK) 
					ON	CDV.cnsctvo_cdgo_estdo_drcho = EDE.cnsctvo_cdgo_estdo_drcho 
					AND	CDV.cnsctvo_cdgo_csa_drcho = EDE.cnsctvo_cdgo_csa_drcho	 
		WHERE		@fcha_actl BETWEEN EDE.inco_vgnca_estdo_drcho AND EDE.fn_vgnca_estdo_drcho
					AND @fcha_actl BETWEEN CDV.inco_vgnca AND CDV.fn_vgnca;
	
		UPDATE		TAF
		SET			dscrpcn_pln = PLA.dscrpcn_pln
		FROM		#tmpAflds											TAF 
		INNER JOIN	#tmpSlctdes											TSO 
					ON	TAF.id_sld_rcbda = TSO.id_sld_rcbda												
		INNER JOIN	bdAfiliacionValidador.dbo.tbPlanes_Vigencias	PLA WITH (NOLOCK)
					ON	TAF.cnsctvo_cdgo_pln = PLA.cnsctvo_cdgo_pln
		WHERE		@fcha_actl BETWEEN PLA.inco_vgnca AND PLA.fn_vgnca	
	
		INSERT INTO		#tmpPrvdor (
						id_sld_rcbda,
						crreo)
		SELECT			TSO.id_sld_rcbda, 
						DP.crro_elctrnco 
		FROM			bdSisalud.dbo.tbDireccionesPrestador DP WITH(NOLOCK) 
		INNER JOIN		#tmpAflds TAF 
						ON	dp.cdgo_intrno = TAF.cdgo_ips_prmra
		INNER JOIN      #tmpSlctdes TSO
						ON TAF.id_sld_rcbda=TSO.id_sld_rcbda
		WHERE 			@fcha_actl BETWEEN DP.inco_vgnca AND DP.fn_vgnca;
		
		--Actualiza la descripcion  de la sede de la IPS
		UPDATE	TAF
		SET		dscrpcn_sde	 =	s.dscrpcn_sde
		FROM	#tmpAflds		TAF
		Inner Join	BDAfiliacionValidador.dbo.tbSedes_Vigencias s With(NoLock) 
		On		s.cnsctvo_cdgo_sde = TAF.sde_afldo
		and		@fcha_actl between s.inco_vgnca and s.fn_vgnca

		--Se obtiene parametro para tag bcc
		Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl, 
													@vsble_usro, 
													@cdgo_prmtro_gnrl_vgnca, 
													@vsble_usro, 
													@tpo_dto_prmtro, 
													@vlr_prmtro_nmrco Output, 
													@vlr_prmtro_crctr Output, 
													@vlr_prmtro_fcha Output;

		UPDATE	TAF
		SET		bcc	 =	RTRIM(LTRIM(@vlr_prmtro_crctr))
		FROM	#tmpAflds		TAF

		--Se obtiene parametro para la linea nacional
		Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl_ln, 
													@vsble_usro, 
													@cdgo_prmtro_gnrl_vgnca_ln, 
													@vsble_usro, 
													@tpo_dto_prmtro, 
													@vlr_prmtro_nmrco Output, 
													@vlr_prmtro_crctr Output, 
													@vlr_prmtro_fcha Output;

		UPDATE	TAF
		SET		lnea_ncnal	 =	RTRIM(LTRIM(@vlr_prmtro_crctr))
		FROM	#tmpAflds		TAF

		--Se obtiene parametro para correo de atención al cliente
		Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl_ac, 
													@vsble_usro, 
													@cdgo_prmtro_gnrl_vgnca_ac, 
													@vsble_usro, 
													@tpo_dto_prmtro, 
													@vlr_prmtro_nmrco Output, 
													@vlr_prmtro_crctr Output, 
													@vlr_prmtro_fcha Output;
		UPDATE	TAF
		SET		crreo_atncn_clnte	 =	RTRIM(LTRIM(@vlr_prmtro_crctr))
		FROM	#tmpAflds		TAF 		

END
GO