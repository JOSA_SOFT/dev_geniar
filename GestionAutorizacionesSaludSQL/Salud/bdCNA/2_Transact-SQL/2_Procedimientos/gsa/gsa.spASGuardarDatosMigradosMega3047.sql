USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASGuardarDatosMigradosMega3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASGuardarDatosMigradosMega3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASGuardarDatosMigradosMega3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Este proceso almacenado se encarga de guardar los datos migrados de 3047 a Mega. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/07/19 FC\>
* Ejemplo: 
    <\EJ
        EXEC gsa.spASGuardarDatosMigradosMega3047
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASGuardarDatosMigradosMega3047
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @estado_aprobada_3047	udtConsecutivo,
			@fechaInicial			datetime,
			@usro_crccn				udtUsuario,
			@fcha_actl				datetime

	SET @estado_aprobada_3047 = 3
	SET @fechaInicial = '20170601'
	SET @fcha_actl = getdate()
	SET @usro_crccn = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

    INSERT INTO gsa.tbASDatosMigracion3047Mega(
		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_slctd_prstcn,	fcha_crcn,	usro_crcn,
		fcha_ultma_mdfccn,				usro_ultma_mdfccn
	)
	SELECT 
		a.cnsctvo_slctd_atrzcn_srvco,	b.cnsctvo_slctd_prstcn,	@fcha_actl,	@usro_crccn,
		@fcha_actl,						@usro_crccn
	FROM #solicitudes_guardadas a
	INNER JOIN bdsisalud.dbo.tbsolicitudopsprestaciones b with(nolock) on a.Identificador_sol = b.cnsctvo_slctd_ops
	WHERE b.cnsctvo_cdgo_estdo_prstcn_slctd = @estado_aprobada_3047
END
GO



