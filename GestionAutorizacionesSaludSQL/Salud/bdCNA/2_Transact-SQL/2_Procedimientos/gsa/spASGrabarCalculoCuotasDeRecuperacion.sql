USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarCalculoCuotasDeRecuperacion]    Script Date: 30/06/2017 07:11:32 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASGrabarCalculoCuotasDeRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D Grabar Tablas Definitivas el Cálculo Cuotas de Recuperación (Copagos-Cuota Moderadora) y consolidados	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P	#tempTbAsDetCalculoCuotaRecuperacion
							#tempTbAsCalculoConsolidadoCuotaRecuperacion
							#tempTbAsDetalleTopesCopago		
							#tempTbAsConsolidadoCuotaRecuperacionxOps	
							#tempTbAsConsolidadoCuotaRecuperacionxOps														P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/25																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se hace redondeo de los valores definitivos DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/23 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se cambia el redondeo segun requerimiento, mantis 11883, se cambi el Round(valor, 0)
							   por Round(valor, -2)  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/25 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se agregan condiciones en el cambio de estado de las ops para que no se modifique el estado
							   de las prestaciones que no tengan numero de ops o que no se halla cumplido la fecha de entrega DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/25 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se agrega un join en el cambio de estado para validar la fecha DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/28 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se agregan validaciones para evitar que se dupliquen registros, en caso que un registro ya 
							   exista para una OPS los valores se actualizan, no se crea un nuevo registro DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/02 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se modifica el insert a la tabla tbAsDetalleTopesCopago que en algunos casos no guardaba.
							   Se agrega update para actualizar el campo cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn en la 
							   tabla temporal #tempTbAsDetalleTopesCopago cuando ya existe el registro, caso de recalculo 
							   de cuota.
							   Se agregan updates para poner los campos cnsctvo_dtlle_mdlo_tps_cpgo y cnsctvo_mdlo en 0
							   cuando estos no se calculan, caso de recalculo.  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/23 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se agregan cruces con el campo cnsctvo_cdgo_cncpto_pago al guardar la asociacion y el 
						       consolidado por OPS.
							   Se agrega campo cnsctvo_cdgo_cncpto_pago a tabla temporal #tempDetalle.
							   Se cambia declaracion de tablas temporales @ a temporales #, y se agregan drops.						   								 
						  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/05 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se modifica procedimiento, se agregan clausulas Merge en el guardado en las tablas reales. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/30 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Carlos Andrés López Ramírez	 AM\>  													
* Descripcion			: <\DM Se retira Merge de guardado en la tabla TbAsDetalleTopesCopago de forma que siempre 
							   guarde el nuevo registro y no actualice  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/30 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASGrabarCalculoCuotasDeRecuperacion]

/*01*/  @lcUsrioAplcn UdtUsuario

AS

Begin
	SET NOCOUNT ON

	Declare @lcUsuario									UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fechaactual								Datetime = getdate(),
			@ValorSI									UdtLogico = 'S',
			@ValorDOS									int = 2,
			@cnsctvo_cdgo_estdo_srvco_slctdo9			int  = 9,  -- Estado servicio '9-Liquidada'
			@cnsctvo_cdgo_estdo_srvco_slctdo11			int  = 11,  -- Estado servicio '11-Autorizada'
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9    int  = 9, -- Estado Conceptos '9-Liquidada'
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11	int	= 11, -- Estado Conceptos '11-Autorizada'
			@ValorCero									int,
			@cnsctvo_cdgo_cncpto_pgo2					udtConsecutivo,
			@vlr_uno									Int;

	

	If  @lcUsrioAplcn is not null
		Begin
			Set @lcUsuario =  @lcUsrioAplcn;
		End

   Create Table #tempDetalle
   (
		cnsctvo_rgstro_dt_cta_rcprcn				int,
		cnsctvo_cdgo_cncpto_pgo						int,
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo
   )

   Create Table #tempConsolidado
   (
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn		int,
		cnsctvo_cdgo_cncpto_pgo						int,
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo
   )

   Create Table #tempConsolidadoOPS
   (
		cnsctvo_rgstro_cnslddo_cta_rcprcn_ops		int,
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo,
		nmro_unco_ops								int
   )

	Set @ValorCero = 0;
	Set @cnsctvo_cdgo_cncpto_pgo2 = 2;
	Set @vlr_uno = 1;
		
	-- Paso 1. Grabamos los detalles del cálculo cuotas de recuperación
	Merge bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion With(RowLock) As Target
	Using(
			Select 		cnsctvo_srvco_slctdo,								cnsctvo_slctd_atrzcn_srvco,			cnsctvo_dtlle_mdlo_tcncs_bnfccn,
						cnsctvo_mdlo,										cntdd_slctda,						cnsctvo_cdgo_cncpto_pgo,
						cdgo_intrno_prtsdr_cbro,							vlr_lqdcn_srvco,					vlr_tcnca_lqdcn_neto,
						vlr_lqdcn_cta_rcprcn_srvco,							gnra_cbro,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
						cnsctvo_cdgo_estdo_cta_rcprcn,						cnsctvo_prcso,						cnsctvo_tpo_prcso,
						@fechaactual fcha_crcn,								@lcUsuario usro_crcn,				@fechaactual fcha_ultma_mdfccn,
						@lcUsuario usro_ultma_mdfccn,						nmro_unco_ops			
			From		#tempTbAsDetCalculoCuotaRecuperacion
			Group By	cnsctvo_srvco_slctdo,								cnsctvo_slctd_atrzcn_srvco,			cnsctvo_dtlle_mdlo_tcncs_bnfccn,
						cnsctvo_mdlo,										cntdd_slctda,						cnsctvo_cdgo_cncpto_pgo,
						cdgo_intrno_prtsdr_cbro,							vlr_lqdcn_srvco,					vlr_tcnca_lqdcn_neto,
						vlr_lqdcn_cta_rcprcn_srvco,							gnra_cbro,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
						cnsctvo_cdgo_estdo_cta_rcprcn,						cnsctvo_prcso,						cnsctvo_tpo_prcso,
						nmro_unco_ops	
	) As Source
	On (
			Target.cnsctvo_slctd_atrzcn_srvco = Source.cnsctvo_slctd_atrzcn_srvco		And
			Target.cnsctvo_srvco_slctdo = Source.cnsctvo_srvco_slctdo					And
			Target.nmro_unco_ops = Source.nmro_unco_ops
	)
	When Matched Then
			Update 
			Set			Target.cnsctvo_dtlle_mdlo_tcncs_bnfccn = Source.cnsctvo_dtlle_mdlo_tcncs_bnfccn,
						Target.cnsctvo_mdlo = Source.cnsctvo_mdlo,							
						Target.cntdd_atrzda = Source.cntdd_slctda,						
						Target.cnsctvo_cdgo_cncpto_pgo = Source.cnsctvo_cdgo_cncpto_pgo,
						Target.cdgo_intrno_prstdr_cbro = Source.cdgo_intrno_prtsdr_cbro,				
						Target.vlr_lqdcn_srvco = Source.vlr_lqdcn_srvco,					
						Target.vlr_tcnca_lqdcn_nto = Source.vlr_tcnca_lqdcn_neto,
						Target.vlr_lqdcn_cta_rcprcn_srvco = Round(Source.vlr_lqdcn_cta_rcprcn_srvco, -2),				
						Target.gnra_cbro = Source.gnra_cbro,							
						Target.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = Source.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
						Target.cnsctvo_cdgo_estdo_cta_rcprcn = Source.cnsctvo_cdgo_estdo_cta_rcprcn,			
						Target.cnsctvo_prcso = Source.cnsctvo_prcso,						
						Target.cnsctvo_tpo_prcso = Source.cnsctvo_tpo_prcso,
						Target.fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,
						Target.usro_ultma_mdfccn = Source.usro_ultma_mdfccn			
		When Not Matched Then
			Insert 
			(
					cnsctvo_srvco_slctdo,					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_dtlle_mdlo_tcncs_bnfccn,
					cnsctvo_mdlo,							cntdd_atrzda,						cnsctvo_cdgo_cncpto_pgo,
					cdgo_intrno_prstdr_cbro,				vlr_lqdcn_srvco,					vlr_tcnca_lqdcn_nto,
					vlr_lqdcn_cta_rcprcn_srvco,				gnra_cbro,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
					cnsctvo_cdgo_estdo_cta_rcprcn,			cnsctvo_prcso,						cnsctvo_tpo_prcso,
					fcha_crcn,								usro_crcn,							fcha_ultma_mdfccn,
					usro_ultma_mdfccn,						nmro_unco_ops
			)
			Values(
					 Source.cnsctvo_srvco_slctdo,								 Source.cnsctvo_slctd_atrzcn_srvco,			 Source.cnsctvo_dtlle_mdlo_tcncs_bnfccn,
					 Source.cnsctvo_mdlo,										 Source.cntdd_slctda,						 Source.cnsctvo_cdgo_cncpto_pgo,
					 Source.cdgo_intrno_prtsdr_cbro,							 Source.vlr_lqdcn_srvco,					 Source.vlr_tcnca_lqdcn_neto,
					 Source.vlr_lqdcn_cta_rcprcn_srvco,							 Source.gnra_cbro,							 Source.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
					 Source.cnsctvo_cdgo_estdo_cta_rcprcn,						 Source.cnsctvo_prcso,						 Source.cnsctvo_tpo_prcso,
					 Source.fcha_crcn,											 Source.usro_crcn,							 Source.fcha_ultma_mdfccn,
					 Source.usro_ultma_mdfccn,									 Source.nmro_unco_ops			
			)
			Output  Inserted.cnsctvo_rgstro_dt_cta_rcprcn, Inserted.cnsctvo_slctd_atrzcn_srvco , Inserted.cnsctvo_cdgo_cncpto_pgo 
			into #tempDetalle (cnsctvo_rgstro_dt_cta_rcprcn, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_cncpto_pgo);



	-- Paso 2. Grabamos el Consolidado del cálculo de cuotas de recuperación


	Merge bdCNA.gsa.tbAsCalculoConsolidadoCuotaRecuperacion	 With(RowLock) AS Target
	Using (
		Select distinct
				cnsctvo_cdgo_estdo_cta_rcprcn,					cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_ttl,
				vlr_lqdcn_cta_rcprcn_ttl_fnl,					cnsctvo_prcso,						cnsctvo_tpo_prcso,
				@fechaactual fcha_crcn,							@lcUsuario usro_crcn,				@fechaactual fcha_ultma_mdfccn,
				@lcUsuario usro_ultma_mdfccn,					cnsctvo_slctd_atrzcn_srvco,			fcha_entrga
		From	#tempTbAsCalculoConsolidadoCuotaRecuperacion
		Group By cnsctvo_cdgo_estdo_cta_rcprcn,					cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_ttl,
				vlr_lqdcn_cta_rcprcn_ttl_fnl,					cnsctvo_prcso,						cnsctvo_tpo_prcso,
				cnsctvo_slctd_atrzcn_srvco,						fcha_entrga
	) As Source
	On (
			Target.cnsctvo_slctd_atrzcn_srvco = Source.cnsctvo_slctd_atrzcn_srvco  And
			Target.cnsctvo_cdgo_cncpto_pgo = Source.cnsctvo_cdgo_cncpto_pgo
	)
	When Matched Then
			Update		
			Set			Target.cnsctvo_cdgo_estdo_cta_rcprcn = Source.cnsctvo_cdgo_estdo_cta_rcprcn,			
						Target.vlr_lqdcn_cta_rcprcn_ttl = Source.vlr_lqdcn_cta_rcprcn_ttl,
						Target.vlr_lqdcn_cta_rcprcn_ttl_fnl =Round(Source.vlr_lqdcn_cta_rcprcn_ttl_fnl, -2),			
						Target.cnsctvo_prcso = Source.cnsctvo_prcso,						
						Target.cnsctvo_tpo_prcso = Source.cnsctvo_tpo_prcso,
						Target.fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,	
						Target.usro_ultma_mdfccn = Source.usro_ultma_mdfccn,						
						Target.fcha_entrga = Source.fcha_entrga
	When Not Matched Then
			Insert 
			(
					cnsctvo_cdgo_estdo_cta_rcprcn,			cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_ttl,
					vlr_lqdcn_cta_rcprcn_ttl_fnl,			cnsctvo_prcso,						cnsctvo_tpo_prcso,
					fcha_crcn,								usro_crcn,							fcha_ultma_mdfccn,	
					usro_ultma_mdfccn,						cnsctvo_slctd_atrzcn_srvco,			fcha_entrga
			)
			Values
			(
				Source.cnsctvo_cdgo_estdo_cta_rcprcn,					Source.cnsctvo_cdgo_cncpto_pgo,			    Source.vlr_lqdcn_cta_rcprcn_ttl,
				Round(Source.vlr_lqdcn_cta_rcprcn_ttl_fnl, -2),			Source.cnsctvo_prcso,						Source.cnsctvo_tpo_prcso,
				Source.fcha_crcn,										Source.usro_crcn,							Source.fcha_ultma_mdfccn,
				Source.usro_ultma_mdfccn,								Source.cnsctvo_slctd_atrzcn_srvco,			Source.fcha_entrga
			);

	Insert 
	Into		#tempConsolidado 
	(
				cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,		cnsctvo_cdgo_cncpto_pgo,	cnsctvo_slctd_atrzcn_srvco
	)
	Select		a.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,		a.cnsctvo_cdgo_cncpto_pgo,	a.cnsctvo_slctd_atrzcn_srvco
	From		gsa.tbAsCalculoConsolidadoCuotaRecuperacion			a	With(NoLock)
	Inner Join	#tempTbAsCalculoConsolidadoCuotaRecuperacion		b	With(NoLock)
	On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	And			a.cnsctvo_cdgo_cncpto_pgo = b.cnsctvo_cdgo_cncpto_pgo

	Update		a
	Set			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = b.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	From		#tempTbAsDetalleTopesCopago a
	Inner Join	#tempConsolidado b
	On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
		

	-- Paso 3. Grabamos la asociación entre el detalle y el consolidado cuotas de recuperación
	
	Merge		gsa.tbAsAsocDetallexConsolidadoCuotaRecuperacion	As		T
	Using		
	(
				Select distinct
					t.cnsctvo_rgstro_dt_cta_rcprcn,			t1.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,			@fechaactual fcha_crcn,
					@lcUsuario usro_crcn,					@fechaactual fcha_ultma_mdfccn,						@lcUsuario usro_ultma_mdfccn
				From		#tempDetalle t 
				Inner Join	#tempConsolidado t1 
				On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
				And			t1.cnsctvo_cdgo_cncpto_pgo = t.cnsctvo_cdgo_cncpto_pgo

	)	As	S
	On			
	(			
				T.cnsctvo_rgstro_dt_cta_rcprcn = S.cnsctvo_rgstro_dt_cta_rcprcn						And
				T.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = S.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	)
	When Not Matched Then
			Insert
			(
						cnsctvo_rgstro_dt_cta_rcprcn,			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,			fcha_crcn,								
						usro_crcn,								fcha_ultma_mdfccn,									usro_ultma_mdfccn
			)
			Values
			(
						S.cnsctvo_rgstro_dt_cta_rcprcn,			S.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,			S.fcha_crcn,								
						S.usro_crcn,							S.fcha_ultma_mdfccn,								S.usro_ultma_mdfccn
			)
	When Matched Then
		Update
		Set
				T.cnsctvo_rgstro_dt_cta_rcprcn = S.cnsctvo_rgstro_dt_cta_rcprcn,			
				T.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = S.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,			
				T.fcha_ultma_mdfccn = @fechaactual,									
				T.usro_ultma_mdfccn = @lcUsuario;

	--Si no tiene el detalle de los topes pone 0
	Update		#tempTbAsDetalleTopesCopago
	Set			cnsctvo_dtlle_mdlo_tps_cpgo = @ValorCero
	Where		cnsctvo_dtlle_mdlo_tps_cpgo Is Null

	Update		#tempTbAsDetalleTopesCopago
	Set			cnsctvo_mdlo = @ValorCero
	Where		cnsctvo_mdlo Is Null

	--Grabamos el detalle de los topes por evento y año
	Insert Into bdCNA.gsa.TbAsDetalleTopesCopago
	(
			cnsctvo_dtlle_mdlo_tps_cpgo,				cnsctvo_mdlo,								cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
			cnsctvo_slctd_atrzcn_srvco,					nmro_unco_idntfccn_afldo,					sldo_antrr_dspnble_evnto,
			sldo_antrr_dspnble_ano,						sldo_dspnble_evnto,							sldo_dspnble_ano,
			cnsctvo_cdgo_estdo_cta_rcprcn,				cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,			cnsctvo_cdgo_tpo_trnsccn,
			fcha_crcn,									usro_crcn,									fcha_ultma_mdfccn,
			usro_ultma_mdfccn
	)
	Select		Distinct
				t.cnsctvo_dtlle_mdlo_tps_cpgo,				t.cnsctvo_mdlo,								t1.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
				t.cnsctvo_slctd_atrzcn_srvco,				t.nmro_unco_idntfccn_afldo,					t.sldo_antrr_dspnble_evnto,
				t.sldo_antrr_dspnble_ano,					t.sldo_dspnble_evnto,						t.sldo_dspnble_ano,
				t.cnsctvo_cdgo_estdo_cta_rcprcn,			t.cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,		t.cnsctvo_cdgo_tpo_trnsccn,
				@fechaactual fcha_crcn,						@lcUsuario usro_crcn,						@fechaactual fcha_ultma_mdfccn,
				@lcUsuario usro_ultma_mdfccn
	From		#tempTbAsDetalleTopesCopago t
	Inner join	#tempConsolidado t1
	On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
	Where		t1.cnsctvo_cdgo_cncpto_pgo = 2
	Group By	t.cnsctvo_dtlle_mdlo_tps_cpgo,				t.cnsctvo_mdlo,								t1.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
				t.cnsctvo_slctd_atrzcn_srvco,				t.nmro_unco_idntfccn_afldo,					t.sldo_antrr_dspnble_evnto,
				t.sldo_antrr_dspnble_ano,					t.sldo_dspnble_evnto,						t.sldo_dspnble_ano,
				t.cnsctvo_cdgo_estdo_cta_rcprcn,			t.cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,		t.cnsctvo_cdgo_tpo_trnsccn;
	
	-- Paso 5. Grabamos el consolidado de cuotas de recuperación x OPS
	Update		t
	Set			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = t1.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps t
	Inner Join  #tempConsolidado t1
	On          t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
	And			t1.cnsctvo_cdgo_cncpto_pgo = t.cnsctvo_cdgo_cncpto_pgo

	Merge bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps With(RowLock)  As Target
	Using(
			Select 
						cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,										cnsctvo_cdgo_cncpto_pgo,
						cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,						cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
						cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,	@fechaactual fcha_crcn,								@lcUsuario usro_crcn,		
						@fechaactual fcha_ultma_mdfccn,			@lcUsuario usro_ultma_mdfccn
			From		#tempTbAsConsolidadoCuotaRecuperacionxOps
			Where		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn is not null
			Group By	cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,										cnsctvo_cdgo_cncpto_pgo,
						cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,						cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
						cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn
	) AS Source
	On (
			Target.cnsctvo_slctd_atrzcn_srvco = Source.cnsctvo_slctd_atrzcn_srvco	And
			Target.nmro_unco_ops = Source.nmro_unco_ops							And
			Target.cnsctvo_cdgo_cncpto_pgo = Source.cnsctvo_cdgo_cncpto_pgo
	)
	When Matched Then
		Update
		Set			Target.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = Source.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
					Target.cnsctvo_cdgo_estdo_cta_rcprcn = Source.cnsctvo_cdgo_estdo_cta_rcprcn,			
					Target.vlr_lqdcn_cta_rcprcn_srvco_ops = Round(Source.vlr_lqdcn_cta_rcprcn_srvco_ops, -2),		
					Target.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn = Source.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,   
					Target.fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,						
					Target.usro_ultma_mdfccn = Source.usro_ultma_mdfccn
	When Not Matched Then
		Insert
		(

				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,						cnsctvo_cdgo_cncpto_pgo,
				cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
				cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,   fcha_crcn,							usro_crcn,							
				fcha_ultma_mdfccn,						usro_ultma_mdfccn
		)
		Values
		(
				Source.cnsctvo_slctd_atrzcn_srvco,				Source.nmro_unco_ops,										Source.cnsctvo_cdgo_cncpto_pgo,
				Source.cnsctvo_cdgo_estdo_cta_rcprcn,			Round(Source.vlr_lqdcn_cta_rcprcn_srvco_ops, -2),			Source.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
				Source.cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,	Source.fcha_crcn,											Source.usro_crcn,		
				Source.fcha_ultma_mdfccn,						Source.usro_ultma_mdfccn
		)
		Output      Inserted.cnsctvo_rgstro_cnslddo_cta_rcprcn_ops, Inserted.cnsctvo_slctd_atrzcn_srvco, inserted.nmro_unco_ops  
		Into #tempConsolidadoOPS (cnsctvo_rgstro_cnslddo_cta_rcprcn_ops,cnsctvo_slctd_atrzcn_srvco,nmro_unco_ops);



    Update      t1
	Set			cnsctvo_rgstro_cnslddo_cta_rcprcn_ops = t2.cnsctvo_rgstro_cnslddo_cta_rcprcn_ops
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps t 
	Inner Join	gsa.tbAsDetCalculoCuotaRecuperacion  t1
	On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
	Inner Join  #tempConsolidadoOPS t2 
	On			t2.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
	And			t2.nmro_unco_ops = t.nmro_unco_ops

	-- Paso 6. Cambiamos el estado de los servicios que se le calcularon cuotas de recuperación de '9-Liquidada a 11-Autorizada'
	Update		ss
	Set			cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo11, /* Estado servicio '11-Autorizada'*/
				fcha_ultma_mdfccn = @fechaactual,
				usro_ultma_mdfccn = @lcUsuario
	From		gsa.tbASServiciosSolicitados ss
	Inner Join	#TempServicioSolicitudes tss
	On			tss.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	And			tss.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 /*Estado servicio '9-Liquidada'*/
	Where       tss.nmro_unco_ops != @ValorCero

	-- Paso 7. Actualizamos tabla fisica el cambio de estado de los conceptos de '9-Liquidada a 11-Autorizada' para procedimientos
	Update		css
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_ultma_mdfccn = @fechaactual
	From		#TempServicioSolicitudes tss  
	Inner Join  gsa.tbASProcedimientosInsumosSolicitados pis with(nolock)
	On			pis.cnsctvo_srvco_slctdo =  tss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css 
	On			css.cnsctvo_prstcn = tss.cnsctvo_cdgo_srvco_slctdo
	And			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9
	Where       tss.nmro_unco_ops != @ValorCero

	-- Paso 8. Actualizamos tabla fisica el cambio de estado de los conceptos de '9-Liquidada a 11-Autorizada' para Medicamentos
	Update		css
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_ultma_mdfccn = @fechaactual
	From		#TempServicioSolicitudes tss  
	Inner Join  gsa.tbASMedicamentosSolicitados ms with(nolock)
	On			ms.cnsctvo_srvco_slctdo =  tss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css 
	On			css.cnsctvo_prstcn = tss.cnsctvo_cdgo_srvco_slctdo
	And			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9
	Where       tss.nmro_unco_ops != @ValorCero

	Drop Table #tempConsolidado
	Drop Table #tempConsolidadoOPS
	Drop Table #tempDetalle

End
