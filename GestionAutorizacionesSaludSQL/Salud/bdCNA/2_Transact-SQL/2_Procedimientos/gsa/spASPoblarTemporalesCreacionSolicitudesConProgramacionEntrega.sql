USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega]    Script Date: 24/07/2017 9:07:08 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------
* Método o PRG		:		gsa.spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega							
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez García										A\>	
* Descripción		: <\D	Poblar tablas temporales del proceso de creación de las solicitudes
							con programación de Entrega											D\>
* Observaciones		: <\O 																		O\>	
* Parámetros		: <\P 	P\>	
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/03/15															FC\>
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por	: <\AM  Luis Fernando Benavides AM\>      
* Descripción		: <\DM	
                            Ajuste para que el consecutivo recobro se obtenga de la tabla:			
*					        bdSisalud.dbo.tbafiliadosMarcados del campo	cnsctvo_cdgo_clsfccn_evnto	
*					        debido a inconsistencia presentada con el nombre de campo en SIPRES		
                      DM\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2016/09/08																FM\>
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por	: <\AM  Luis Fernando Benavides AM\>      
* Descripción		: <\DM	
                            Cambio actualizacion de campo: cnsctvo_cdgo_tpo_vnclcn_afldo apartir  
*					        de la tabla tbBeneficiariosValidador. Estaba actualizando campo con	   
*					        cnsctvo_cdgo_tpo_afldo cambio por este cnsctvo_cdgo_prntsco			    
                      DM\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2016/09/28 FM\>
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM Se modifica el procedimiento adicionando la consulta del parametro general 
                            de los dias para la impresion de la OPS 
					   DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2016-10-21  FM\>    
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01- AM\>    
* Descripcion        : <\DM  Se modifica el procedimiento adicionando nueva funcionalidad para OPS Virtual D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2016-12-20  FM\>    
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                           Se modifica el procedimiento para reemplazar en el campo 
						   cnsctvo_cdgo_orgn_atncn el valor 1 = ACCIDENTE DE TRABAJO por el 
						   valor 4 = ENFERMEDAD GENERAL
                        DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-03-06  FM\>    
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez G. AM\>    
* Descripcion        : <\D   
                           Se modifica el procedimiento para la ejecución masiva 
                           solo tome las programaciones de entrega que apliquen para ops virtual
                        D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-03-31  FM\>    
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez G. AM\>    
* Descripcion        : <\DM  
                             Por OPS Virtual - Se modifica por
							 1. ajuste para tomar la Ips Solicitante en #Tempo_IPS-
							 2. ajuste para enviar la oficina de la programación de entrega 
							 3. se implementa tabla temporal para excluir las prog. entrega ya procesadas
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-05  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Maria Liliana Prieto AM\>    
* Descripcion        : <\DM Quitar los que sean de comfandi y no sean droguerias DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-04-12  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez AM\>    
* Descripcion        : <\DM   Se implementa para el proceso masivo la validación si 
                             una programación de entrega tiene asociado un afiliado sin derechos 
							 no se debe procesar       
					   DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-12  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez AM\>    
* Descripcion        : <\D   Se implementa para el proceso masivo la validación del modelo de convenios y liquidación 
                             con la clasificación de la atención solo 1-Ambulatorio     D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-17  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto AM\>    
* Descripcion        : <\DM  para recuperar el contrato se consulta solo los tipos de contrato pos y pos subsidiado   
							 Quitar las programaciones de entrega de afiliados que tengan planes complementarios vigentes  
							 Se modifica la forma de consultar los dias habiles, se implementa funcion
						DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-17  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez AM\>    
* Descripcion        : <\DM  
                             Se implementa para el proceso masivo la eliminación de programaciones 
							 de afiliados con más de una programación     
						DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-19  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto AM\>    
* Descripcion        : <\DM  La programacion de entrega de afiliados que tengan planes complementarios 
                             vigentes solo aplica para COMFANDI 
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	 2017-04-19  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM   
                            Se modifica el procedimiento actualizando en la informacion adicional de la solcitud
							la cohorte (riesgo) que tiene asociado
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	 2017-04-21  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto AM\>    
* Descripcion        : <\DM   se consulta parametro para validar si se ejecuta o no el paso DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-04-25  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\DM  
                             se modifica proceso para recuperar la informacion del contrato a partir del codigo del plan
							 y evitar la carga de informacion mezclada de los contratos activos del afiliado 
					   DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-05-03  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Juan Carlos Vásquez - sisjvg01 AM\>    
* Descripcion        : <\DM 
                            se modifica proceso para excluir las prestaciones x prestador que no 
                            esten disponibles para entrega D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM	2017-05-16  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM
                           Se modifica procedimiento para que se cargue en la tabla temporal #Tempo_IPS las
						   informacion de la IPS asociada tutelas o en su defecto generico, con el fin de que 
						   pueda guardar la informacion del direccionamiento en las estructuras de MEGA 
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-05-18  FM\>  
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto Rincon AM\>    
* Descripcion        : <\DM
                           Se modifica procedimiento para que se ejecute automaticamente los Lunes y los Miercoles
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-05-24  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto Rincon AM\>    
* Descripcion        : <\DM
                           Se modifica procedimiento para que se ejecute diario, se actualiza la fecha de ejecucion porque 
						   la requieren los informes para su generacion
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-06-05  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Darlin Dorado AM\>    
* Descripcion        : <\DM
                           Se modifica procedimiento eliminar las programaciones de entrega que tienen una prestación
						   que no corresponde con el acta de CTC
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-06-05  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Darlin Dorado AM\>    
* Descripcion        : <\DM
                           Se modifica para incluir en el proceso masivo los afiliados beneficiarios
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-06-09  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto Rincon AM\>    
* Descripcion        : <\DM
                           Se modifica procedimiento para que se ejecute los dias Domingo, Martes y Jueves 
                        DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-06-29  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Maria Liliana Prieto Rincon AM\>    
* Descripcion        : <\DM  Se modifica procedimiento para que se ejecute los dias Domingo, Lunes, Martes, Miercoles y Jueves   DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-07-17  FM\> 
*-------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01- AM\>    
* Descripcion        : <\DM  Se modifica procedimiento para validar que el plan de la programación sea igual al vigente de la contratación   DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM	2017-07-24  FM\> 
*-------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega] null,'sismpr01'
ALTER PROCEDURE  [gsa].[spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega] 

/*01*/ @cnsctvo_cdgo_det_prgrmcn_fcha_evnto		UdtConsecutivo = null,
/*02*/ @usro_prcso								UdtUsuario

AS 

Begin
	SET NOCOUNT ON

	Declare	@fechaactual					datetime    = getdate(),
			@lcUsuario						UdtUsuario  = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fechacalculada					Datetime,
			@estdo							char(1)		= 'A',
			@sn_entrgr						int			= 151, -- Estado Entrega '151-Sin Entregar'
			@rprgrmdo						int			= 152, -- Estado Entrega '152-Reprogramado'
			@ValorCERO						int			= 0,
			@ValorUNO						int			= 1,
			@ValorTRES						int			= 3,
			@ValorCUATRO					int			= 4,
			@cnsctvo_cdgo_clsfccn_evnto8    int         = 8,  -- Evento Tutela
			@cnsctvo_cdgo_clsfccn_evnto11   int         = 11,  -- Evento CTC
			@cnsctvo_cdgo_prmtro_gnrl93		int			= 93 ,-- Parámetro General
			@cnsctvo_cdgo_prmtro_gnrl100	int         = 100, -- Parámetro para obtener los días atras de la programación de entrega
			@diasHistorico					int,
			@dias_prgrmcn_entrga            int,
			@ValorSI						char(1)        = 'S',
			@ValorNO						char(1)        = 'N',
			@cnsctvo_cdgo_estdo_acta56      int            = 56,     -- Estado acta Anulada
			@cnsctvo_cdgo_clsfccn_atncn1    int            = 1,      -- 1-Ambulatorio
			@nmro_unco_idntfccn_mdco100961	UdtConsecutivo = 100961, -- Número Unico Médicos 
			@clse_prstdrM					char(1)		   = 'M',    -- Clase de Prestador 'Médicos'
			@cnsctvo_gnrco_mdco				UdtConsecutivo = 87733, -- Consecutivo Médico Genérico para Mega
			@nmro_idntfccn_mdco_mga			udtNumeroIdentificacionLargo = '99999998', -- Número Unico Identificación Médico Genérico
			@fcha_crte_inco					datetime, --fecha corte inicio liquidación
			@fechacrtelqdccn				Datetime = getdate(),

			--Variable que se utilizan para la consulta del parametro general de los dias para la impresion de la OPS
			@lcCdgo_prmtro_cntdd_mss_frmto  Char(3),   
			@vlr_prmtro_nmrco               Numeric(18,0),
			@vlr_prmtro_crctr               Char(200)    ,
			@vlr_prmtro_fcha                Datetime     ,
			@vsble_usro			            udtLogico    ,
			@tpo_dto_prmtro		            udtLogico    ,

			-- variables para nueva funcionalidad Ops Virtual  - sisjvg01- 2016/12/20

			@cnsctvo_cdgo_tpo_dcmnto97				int = 97,   -- AUTORIZACION REPORTE POR CORREO ELECTRONICO O CELULAR
			@cnsctvo_cdgo_tpo_mrca_prstdr21			int = 21,   -- Ips recauda Mipres
			@cnsctvo_cdgo_tpo_mrca_prstdr14         int = 14,   -- IPS recauda copago y cuota moderadora
			@cnsctvo_cdgo_tps_mrcs_evnts_ntfccn1	int = 1,    -- marca evento '1-aplica OPS Virtual'
			@cntdd_dias_fstvos						int = 0,    -- cantidad de días festivos
			@nui_cmfndi								int = 100419,
			@tpo_ips_drgra							int = 8,
			@tpo_cntrto_PAC							int = 2,
			@vlr_vacio                              udtLogico,
			@cnsctvo_cdgo_clsfccn_evnto_cncr        udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto_vh          udtConsecutivo,
			@cdgo_estdo_ntfccn_ingrsdo              udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo               udtConsecutivo,
			@cdgo_estdo_ntfccn_prbble               udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo1              udtConsecutivo,
			@cdgo_estdo_ntfccn_cnfrmdo              udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo2              udtConsecutivo,
			@cdgo_estdo_ntfccn_cnfrmdo1             udtConsecutivo,
			@cdgo_estdo_ntfccn_prbble1              udtConsecutivo,
			@cdgo_estdo_ntfccn_cn_srvco             udtConsecutivo,
			@cdgo_estdo_ntfccn_sn_srvco             udtConsecutivo,
			@cdgo_prmtro_fcha_ejccn					varchar(5),
			@fcha_ejccn								datetime,
			@cnsctvo_cdgo_tpo_cntrto1				udtConsecutivo, -- 1 POS
			@cnsctvo_cdgo_tpo_cntrto3				udtConsecutivo, -- 3 POSS
			@cdgo_intrno_gnrco                      udtCodigoIPS  ,			
			@cnt                                    Int           ,
			@nmro_unco_idntfccn_ips_gnrca           udtConsecutivo

	Set @cnsctvo_cdgo_clsfccn_evnto_cncr = 13 --cancer
	Set @cnsctvo_cdgo_clsfccn_evnto_vh   = 16 --vih

	--ESTADO NOTIFICACION
	Set @cdgo_estdo_ntfccn_ingrsdo  = 13  -- Ingresado
	Set @cdgo_estdo_ntfccn_ntfcdo   = 14  -- Notificado
	Set @cdgo_estdo_ntfccn_prbble   = 87  -- Probable
	Set @cdgo_estdo_ntfccn_ntfcdo1  = 88  -- Notificado
	Set @cdgo_estdo_ntfccn_cnfrmdo  = 89  -- Confirmado
	Set @cdgo_estdo_ntfccn_ntfcdo2  = 119 -- Notificado	
	Set @cdgo_estdo_ntfccn_cnfrmdo1 = 120 -- Confirmado
	Set @cdgo_estdo_ntfccn_prbble1  = 156 -- Probable
	Set @cdgo_estdo_ntfccn_cn_srvco = 167 -- Con Servicio
	Set @cdgo_estdo_ntfccn_sn_srvco = 168 -- Sin Servicio
	Set @cnsctvo_cdgo_tpo_cntrto1   = 1;
	Set @cnsctvo_cdgo_tpo_cntrto3   = 3;
	Set @cdgo_intrno_gnrco          = '5999';

	--Procedimiento que permite consultar los parametros generales para recuperar la cantidad de días antes de la fecha actual
	-- a tomar para la prog. entrega */
	
	Set @vlr_vacio                       = ''
	Set @lcCdgo_prmtro_cntdd_mss_frmto   = '100'
	Set @vsble_usro                      = 'S'
	Set @tpo_dto_prmtro                  = @vlr_vacio
	

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	Set @fcha_crte_inco		=	DATEADD(dd,DATEDIFF(dd,0,@fechaactual),0)
	Set	@fcha_crte_inco		-=	@vlr_prmtro_nmrco

	--Procedimiento que permite consultar los parametros generales para recuperar la cantidad de días posteriores a la fecha actual
	-- a tomar para la prog. entrega */
	Set @lcCdgo_prmtro_cntdd_mss_frmto  = '110'
	Set @vsble_usro                     = 'S'
	Set @tpo_dto_prmtro                 = @vlr_vacio

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	select	@fechacrtelqdccn = bdsisalud.dbo.fnPeCalculaDiasHabiles(@fechacrtelqdccn,@vlr_prmtro_nmrco)
	select	@fechacrtelqdccn = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))
	


	-- Paso 1. Poblar tabla temporal de las programaciones de Entrega Pendientes ó Reprogramadas 
	if @cnsctvo_cdgo_det_prgrmcn_fcha_evnto is not null -- es por demanda
		Begin
			Set @lcUsuario = @usro_prcso

			INSERT		#tbDetProgramacion 
			(              
						cnsctvo_prgrmcn_prstcn,					cnsctvo_cdgo_ntfccn,					cnsctvo_cdgo_ofcna,
						cnsctvo_prstcn,							cnsctvo_cdgo_clsfccn_cdfccn,			fcha_dsde,
						fcha_hsta,								cnsctvo_cdgo_estds_entrga,				cdgo_intrno,
						nmro_unco_idntfccn_prstdr,				cntdd,									cnsctvo_cdgo_dgnstco,
						cnsctvo_pr_ntfccn,						cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_prgrmcn_fcha_evnto,
						cnsctvo_det_prgrmcn_fcha,				fcha_entrga,							fcha_rl_entrga,
						nmro_acta,								cnsctvo_cdgo_clsfccn_evnto,				nmro_unco_idntfccn_afldo
			)
			SELECT TOP 1 
						a.cnsctvo_prgrmcn_prstcn,				a.cnsctvo_cdgo_ntfccn,					a.cnsctvo_cdgo_ofcna,
						a.cnsctvo_prstcn,						a.cnsctvo_cdgo_clsfccn_cdfccn,			b.fcha_dsde,
						b.fcha_hsta,							b.cnsctvo_cdgo_estds_entrga,			e.cdgo_intrno,
						isnull(e.nmro_unco_idntfccn_prstdr,0),	c.cntdd,								f.cnsctvo_cdgo_dgnstco,
						a.cnsctvo_pr_ntfccn,					c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto,	b.cnsctvo_prgrmcn_fcha_evnto,
						c.cnsctvo_det_prgrmcn_fcha,				isnull(c.fcha_entrga,''),				c.fcha_rl_entrga,
						a.cnsctvo_pr_ntfccn,					f.cnsctvo_cdgo_clsfccn_evnto,			f.nmro_unco_idntfccn
			From		bdsisalud.dbo.tbProgramacionPrestacion a With(Nolock)	
			INNER JOIN	bdsisalud.dbo.tbProgramacionFechaEvento b	With(Nolock)	
			ON			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
			INNER JOIN	bdsisalud.dbo.tbDetProgramacionFechaEvento c	With(Nolock)	
			ON			c.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn  
			And			c.cnsctvo_prgrmcn_fcha_evnto = b.cnsctvo_prgrmcn_fcha_evnto  
			And			(c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo))
			And			c.fcha_entrga between @fcha_crte_inco  and @fechacrtelqdccn 
			INNER JOIN	bdsisalud.dbo.tbCodificaciones d With(Nolock)	
			ON			d.cnsctvo_cdfccn = a.cnsctvo_prstcn
			INNER JOIN	bdsisalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
			ON			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
			AND			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha
			and			c.cnsctvo_prgrmcn_fcha_evnto = e.cnsctvo_prgrmcn_fcha_evnto 
			AND			e.estdo = @estdo
			INNER JOIN	bdsisalud.dbo.tbAfiliadosMarcados f with(nolock)
			ON			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
			And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
			Where		c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = @cnsctvo_cdgo_det_prgrmcn_fcha_evnto
		End
	Else  -- es masivo
		Begin
			--Select  @fcha_crte_inco	 = '20170401'
			--Select	@fechacrtelqdccn = '20170515'
			Set		@cdgo_prmtro_fcha_ejccn          = '11'
			
			-- 2017/06/29 - sismpr01 - si es el dia martes, jueves o domingo y su hora es mayor igual a las 6 pm se debe ejecutar el masivo, se actualiza la fecha de ejecucion porque la requieren los informes para su generacion
			-- 2017/07/17 - sismpr01 - si es el dia Domingo, lunes, martes, miercoles, jueves y su hora es mayor igual a las 6 pm se debe ejecutar el masivo, se actualiza la fecha de ejecucion porque la requieren los informes para su generacion
			If	(datename(WEEKDAY,@fechaActual) in ('Sunday','Monday','Tuesday','Wednesday','Thursday')	And datepart(hh,@fechaActual) >= 18 ) 
			Begin
				update	pg
				set		pg.vlr_prmtro_fcha = convert(varchar(10),@fechaActual,111)
				from	bdcna.dbo.tbParametrosGenerales_Vigencias pg with (nolock)
				where	cdgo_prmtro_gnrl = @cdgo_prmtro_fcha_ejccn
				And		@fechaactual	between inco_vgnca And fn_vgnca
			End

			-- 2017/06/29 - sismpr01 - si es el dia lunes, miercoles o viernes y su hora es menor igual a las 4 am se debe ejecutar el masivo, se actualiza la fecha de ejecucion porque la requieren los informes para su generacion
			--If	(datename(WEEKDAY,@fechaActual) in ('Monday','Wednesday','Friday')	And datepart(hh,@fechaActual) <= 4	)
			-- 2017/07/17 sismpr01 si es el dia viernes y su hora es menor igual a las 4 am se debe ejecutar el masivo, se actualiza la fecha de ejecucion porque la requieren los informes para su generacion
			If	(datename(WEEKDAY,@fechaActual) = 'Friday'	And datepart(hh,@fechaActual) <= 4	)
			Begin
				update	pg
				set		pg.vlr_prmtro_fcha = convert(varchar(10),dateadd(day,-1,@fechaActual),111)
				from	bdcna.dbo.tbParametrosGenerales_Vigencias pg  with (nolock)
				where	cdgo_prmtro_gnrl = @cdgo_prmtro_fcha_ejccn
				And		@fechaactual	between inco_vgnca And fn_vgnca
			End
			
			-- Recuperamos el parámetro fecha de ejecución proceso
	
			Select	@fcha_ejccn	= convert(varchar(10), vlr_prmtro_fcha, 111)
			From	bdcna.dbo.tbParametrosGenerales_Vigencias with (nolock)
			Where	cdgo_prmtro_gnrl = @cdgo_prmtro_fcha_ejccn
			And		@fechaactual between inco_vgnca And fn_vgnca

			If (@fcha_ejccn is null or @fechaactual not between @fcha_ejccn and dateadd(hour,28,@fcha_ejccn))
			Begin
				return
			End	

			INSERT		#tbDetProgramacion 
			(              
						cnsctvo_prgrmcn_prstcn,			cnsctvo_cdgo_ntfccn,					cnsctvo_cdgo_ofcna,
						cnsctvo_prstcn,					cnsctvo_cdgo_clsfccn_cdfccn,			fcha_dsde,
						fcha_hsta,						cnsctvo_cdgo_estds_entrga,				cdgo_intrno,
						nmro_unco_idntfccn_prstdr,		cntdd,									cnsctvo_cdgo_dgnstco,
						cnsctvo_pr_ntfccn,				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_prgrmcn_fcha_evnto,
						cnsctvo_det_prgrmcn_fcha,		fcha_entrga,							fcha_rl_entrga,
						nmro_acta,						cnsctvo_cdgo_clsfccn_evnto,				nmro_unco_idntfccn_afldo
			)
			SELECT DISTINCT
						a.cnsctvo_prgrmcn_prstcn,		a.cnsctvo_cdgo_ntfccn,					a.cnsctvo_cdgo_ofcna,
						a.cnsctvo_prstcn,				a.cnsctvo_cdgo_clsfccn_cdfccn,			b.fcha_dsde,
						b.fcha_hsta,					b.cnsctvo_cdgo_estds_entrga,			e.cdgo_intrno,
						isnull(e.nmro_unco_idntfccn_prstdr,0),	c.cntdd,						f.cnsctvo_cdgo_dgnstco,
						a.cnsctvo_pr_ntfccn,			c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto,	b.cnsctvo_prgrmcn_fcha_evnto,
						c.cnsctvo_det_prgrmcn_fcha,		isnull(c.fcha_entrga,''),				c.fcha_rl_entrga,
						a.cnsctvo_pr_ntfccn,			f.cnsctvo_cdgo_clsfccn_evnto,			f.nmro_unco_idntfccn
			From		BdSiSalud.dbo.tbProgramacionPrestacion a With(Nolock)	
			INNER JOIN	bdsisalud.dbo.tbProgramacionFechaEvento b	With(Nolock)	
			ON			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
			INNER JOIN	BdSiSalud.dbo.tbDetProgramacionFechaEvento c	With(Nolock)	
			ON			c.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn  
			And			c.cnsctvo_prgrmcn_fcha_evnto = b.cnsctvo_prgrmcn_fcha_evnto  
			And			(c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo))
			And			c.fcha_entrga between @fcha_crte_inco  and @fechacrtelqdccn 
			INNER JOIN	BdSiSalud.dbo.tbCodificaciones d With(Nolock)	
			ON			d.cnsctvo_cdfccn = a.cnsctvo_prstcn
			INNER JOIN	BdSiSalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
			ON			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
			AND			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha
			and			c.cnsctvo_prgrmcn_fcha_evnto = e.cnsctvo_prgrmcn_fcha_evnto 
			AND			e.estdo = @estdo
			INNER JOIN	BdSiSalud.dbo.tbAfiliadosMarcados f with(nolock)
			ON			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
			And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
			And			f.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11
			order by    isnull(c.fcha_entrga,'')			


	   End

	-- si el proceso es masivo solo procesara las programaciones que aplique para ops virtual -sisjvg01 -2017-03-31
	if @cnsctvo_cdgo_det_prgrmcn_fcha_evnto is null -- es por masivo
	Begin
			-- sisjvg01- 20170516 - Excluir prestaciones x prestador que no tengan disponibilidad para entrega
			Delete		t
			From		#tbDetProgramacion t
			Inner join  bdcna.prm.tbPrestacionesNoDisponiblesxPrestador pnxp with(nolock)
			On          pnxp.nmro_unco_idntfccn_prstdr = t.nmro_unco_idntfccn_prstdr
			And         pnxp.cnsctvo_cdfccn = t.cnsctvo_prstcn
			And         @fechaactual between pnxp.inco_vgnca and pnxp.fn_vgnca
			
			-- sisjvg01 - 20170419 - Quitar las programaciones de afiliados con más de una programación	
			Select		nmro_unco_idntfccn_afldo,cnsctvo_prstcn
			into		#tmpProgramacionesVarias
			From		#tbDetProgramacion
			Group by	nmro_unco_idntfccn_afldo,cnsctvo_prstcn
			Having		count(1) > @ValorUNO
	
			Delete		t
			From		#tbDetProgramacion t
			Inner join	#tmpProgramacionesVarias t1
			On			t1.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo 
			And         t1.cnsctvo_prstcn = t.cnsctvo_prstcn
			
			-- sismpr01 - 20170412 - Quitar las programaciones de entrega que sean de comfandi y no sean droguerias
			Delete		tdp
			From		#tbDetProgramacion tdp
			inner join	bdsisalud.dbo.tbdireccionesprestador  dp with (nolock)
   		    On			tdp.nmro_unco_idntfccn_prstdr	= dp.nmro_unco_idntfccn_prstdr 
			And			tdp.cdgo_intrno					=  dp.cdgo_intrno
			Where		tdp.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi	
			And			dp.cnsctvo_cdgo_tpo_ips			!=	@tpo_ips_drgra

			-- sismpr01 - 20170417 - Quitar las programaciones de entrega de afiliados que tengan planes complementarios vigentes
			-- sismpr01 - 20170419 - solo aplica para comfandi
			Delete		t
			From		#tbDetProgramacion t
			Inner join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv with (nolock)
			On			bv.nmro_unco_idntfccn_afldo	=	t.nmro_unco_idntfccn_afldo		
			And			t.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi		
			Where		@fechaactual  Between bv.inco_vgnca_bnfcro and bv.fn_vgnca_bnfcro
			And			bv.cnsctvo_cdgo_tpo_cntrto	=	@tpo_cntrto_PAC -- PAC			

			
			-- Recuperamos el plan de los afiliados de la programación de entrega
			Update		t
			Set			cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
			From        #tbDetProgramacion t
			Inner Join	bdSisalud.dbo.tbactuanotificacion a
			ON			a.cnsctvo_ntfccn		= t.cnsctvo_cdgo_ntfccn
			And			a.cnsctvo_cdgo_ofcna	= t.cnsctvo_cdgo_ofcna

			-- Recuperamos el tipo y número de contrato,consecutivo del beneficiario
			-- sismpr01 - 20170417 - se recupera el contrato pos y pos subsidiado
			Update		t
			Set			cnsctvo_bnfcro			= bv.cnsctvo_bnfcro,
						cnsctvo_cdgo_tpo_cntrto	= bv.cnsctvo_cdgo_tpo_cntrto, 
						nmro_cntrto				= bv.nmro_cntrto 
			From		#tbDetProgramacion t
			Inner join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv with (nolock)
			On			bv.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo
			And			bv.cnsctvo_cdgo_tpo_cntrto	in (@cnsctvo_cdgo_tpo_cntrto1,@cnsctvo_cdgo_tpo_cntrto3) --1 POS, 3 POSS
			Where		@fechaactual between bv.inco_vgnca_bnfcro and bv.fn_vgnca_bnfcro
			
			-- Se eliminan las programaciones de entrega que tienen un plan del afiliado diferente al vigente en el contrato
			-- sisjvg01- 2017-07-24 - 
			Delete		t
			From		#tbDetProgramacion t
			Inner Join	BDAfiliacionValidador.dbo.tbContratosValidador cv
			On			cv.cnsctvo_cdgo_tpo_cntrto = t.cnsctvo_cdgo_tpo_cntrto
			And         cv.nmro_cntrto = t.nmro_cntrto
			And			cv.cnsctvo_cdgo_pln <> t.cnsctvo_cdgo_pln
			Where		@fechaactual Between cv.inco_vgnca_cntrto And cv.fn_vgnca_cntrto
			And			cv.cnsctvo_cdgo_tpo_cntrto	in (@cnsctvo_cdgo_tpo_cntrto1,@cnsctvo_cdgo_tpo_cntrto3) --1 POS, 3 POSS
						

			--Se Eliminan las programaciones de entrega para afiliados no vigentes
			Delete		t
			From		#tbDetProgramacion t
			where		t.cnsctvo_cdgo_tpo_cntrto	is Null

			Delete		t
			From		#tbDetProgramacion t
			where		t.nmro_cntrto				is Null

			Delete		t
			From		#tbDetProgramacion t
			where		t.cnsctvo_bnfcro			is Null


			-- sisjvg01- 2017-04-12 - Eliminar las programaciones de entrega para afiliados Sin Derechos
			Delete     t
			From       #tbDetProgramacion t
			Inner join BDAfiliacionValidador.dbo.TbMatrizDerechosValidador bv with (nolock)
			On         bv.nmro_unco_idntfccn_bnfcro	=			t.nmro_unco_idntfccn_afldo
			And        bv.cnsctvo_cdgo_tpo_cntrto	=			t.cnsctvo_cdgo_tpo_cntrto
			And        bv.nmro_cntrto				=			t.nmro_cntrto
			And		   bv.cnsctvo_bnfcro			=			t.cnsctvo_bnfcro
			And        @fechaactual					Between		bv.inco_vgnca_estdo_drcho and bv.fn_vgnca_estdo_drcho
			And		   bv.cnsctvo_cdgo_estdo_drcho	In			(	Select	cnsctvo_cdgo_estdo_drcho
																	From	BDAfiliacionValidador.dbo.tbestadosderechovalidador with(nolock) 
																	Where	drcho  ='N' 
																	And		@fechaactual Between inco_vgnca	And fn_vgnca
																)
																			
			
			-- marcamos las programaciones que apliquen para ops Virtual
			Update      t
			Set         mrca_ops_vrtl = @ValorSI
			From        #tbDetProgramacion t
			Inner Join  BDSisalud.dbo.tbTiposMarcasxEventosNotificacion tmen with(nolock)
			On          tmen.cnsctvo_cdgo_clsfccn_evnto = t.cnsctvo_cdgo_clsfccn_evnto
			inner join  BDSisalud.dbo.tbTiposMarcasXSucursal tms with(nolock)
			On          tms.cdgo_intrno = t.cdgo_intrno
			Where		tmen.cnsctvo_cdgo_tps_mrcs_evnts_ntfccn = @cnsctvo_cdgo_tps_mrcs_evnts_ntfccn1
			and         tms.cnsctvo_cdgo_tpo_mrca_prstdr In(@cnsctvo_cdgo_tpo_mrca_prstdr14,@cnsctvo_cdgo_tpo_mrca_prstdr21)
			and         @fechaactual between tmen.inco_vgnca and tmen.fn_vgnca         
			and         @fechaactual between tms.inco_vgnca and tms.fn_vgnca
			
			-- eliminamos las programaciones que no aplican para ops virtual
			delete 
			from   #tbDetProgramacion 
			where  mrca_ops_vrtl = @ValorNo

			-- eliminamos las programaciones que apliquen para ops virtual
			-- pero que tenga la primer programacion y la fecha de entrega mayor a la actual
			delete 
			from   #tbDetProgramacion 
			where  cnsctvo_det_prgrmcn_fcha = @ValorUNO  
			and    fcha_entrga > @fechaactual

			-- Eliminamos las programaciones con acta anulada -- sisjvg01- 2017-04-03
			delete      t
			from		#tbDetProgramacion t 
			INNER JOIN	BdSiSalud.dbo.tbCTCRecobro r with(nolock)
			On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
			And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
			And			r.nmro_acta = t.nmro_acta
			Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
			And         r.cnsctvo_cdgo_estdo_acta = @cnsctvo_cdgo_estdo_acta56

			-- Eliminamos las programaciones con prestaciones diferentes al acta -- sisjvg01- 2017-06-07
			delete      t
			from		#tbDetProgramacion t 
			INNER JOIN	BdSiSalud.dbo.tbCTCRecobro r with(nolock)
			On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
			And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
			And			r.nmro_acta = t.nmro_acta
			INNER JOIN  BdSiSalud.dbo.tbmedicamentoxctc  m with(nolock)
			On          m.cnsctvo_cdgo_ctc = r.cnsctvo_cdgo_ctc
			And         m.cnsctvo_cdgo_sde = r.cnsctvo_cdgo_sde
			Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
			And         r.cnsctvo_cdgo_estdo_acta = @cnsctvo_cdgo_estdo_acta56
			And         t.cnsctvo_prstcn <> m.cnsctvo_cdgo_mdcmnto

			-- Marcamos las programaciones que tengan convenio vigente y liq. vigentes - sisjvg01- 2017-04-07

			-- Modelo Convenio Cups
			Update      t
			Set         mrca_cnvno = @ValorSI
			from		#tbDetProgramacion t
			Inner join	bdSisalud.dbo.tbAsociacionModeloActividad a with (nolock)			
			On			a.cdgo_intrno = t.cdgo_intrno
			And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln
			And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
			inner join	bdContratacion.dbo.tbModeloConveniosPrestacionesxPlan b With(NoLock)
			on			b.cnsctvo_mdlo_cnvno_prstcn_pln = a.cnsctvo_mdlo_cnvno_pln
			And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca
			inner join	bdContratacion.dbo.tbDetModeloConveniosPrestaciones	c	With(NoLock)	
			on			c.cnsctvo_cdgo_mdlo_cnvno_prstcn = b.cnsctvo_cdgo_mdlo_cnvno_prstcn
			And         c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
			And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca
			inner join	bdSiSalud.dbo.tbDetEquivalencias e	With(NoLock)
			on			e.cnsctvo_det_eqvlnca = c.cnsctvo_det_eqvlnca
			And         e.cnsctvo_cdfccn_b = t.cnsctvo_prstcn 
			And         t.fcha_entrga Between e.inco_vgnca And e.fn_vgnca
			
			-- Modelo Convenio Medicamentos
			Update      t
			Set         mrca_cnvno = @ValorSI
			from		#tbDetProgramacion t
			Inner join	bdSisalud.dbo.tbAsociacionModeloActividad a with (nolock)			
			On			a.cdgo_intrno = t.cdgo_intrno
			And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln
			And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
			inner join	bdContratacion.dbo.tbModeloConveniosMedicamentos b with(nolock)
			On          b.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = a.cnsctvo_mdlo_cnvno_pln
			And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca
			inner join	bdContratacion.dbo.tbDetModeloConveniosMedicamentos	c with(nolock)
			On          c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = b.cnsctvo_cdgo_mdlo_cnvno_mdcmnto
            And         c.cnsctvo_cdgo_mdcmnto = t.cnsctvo_prstcn
			And         c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
			And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca

			-- Modelo Cuos 
			Update      t
			Set         mrca_cnvno = @ValorSI
			from		#tbDetProgramacion t
			Inner join	bdSisalud.dbo.tbAsociacionModeloActividad a with (nolock)			
			On			a.cdgo_intrno = t.cdgo_intrno
			And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln
			And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
			inner join	bdContratacion.dbo.tbmodeloconveniosotrosserviciosxplan b With(NoLock)
			On          b.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs = a.cnsctvo_mdlo_cnvno_pln
			And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca
			inner join	bdContratacion.dbo.tbdetmodeloconveniosotrosservicios	c	With(NoLock)
			On          c.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs = b.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs
			And			c.cnsctvo_prstcn_pis = t.cnsctvo_prstcn
			And         c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
			And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca

			-- Eliminamos las programaciones que no tengan convenio vigente
			delete      t
			from		#tbDetProgramacion t 
			Where       mrca_cnvno = @ValorNO
			
			-- Modelo Liquidación
			Update      t
			Set         mrca_lqdcn = @ValorSI
			from		#tbDetProgramacion t
			Inner join	bdSisalud.dbo.tbAsociacionModeloActividad a with (nolock)			
			On			a.cdgo_intrno = t.cdgo_intrno
			And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln
            And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
			inner join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan b with (nolock)	
			on			a.cnsctvo_asccn_mdlo_actvdd = b.cnsctvo_asccn_mdlo_actvdd
			And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca	
			inner join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan c with (nolock)	
			on			b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
			And         c.cnsctvo_prstncn = t.cnsctvo_prstcn
			And         c.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_clsfccn_atncn1
			And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca
			And         c.vlr_trfdo > @ValorCERO	

			-- Eliminamos las programaciones que no tengan modelo liquidación
			delete      t
			from		#tbDetProgramacion t 
			Where       mrca_lqdcn = @ValorNO
			
	End
		
	-- Paso 2.  Poblar tabla Temporal de los afiliados con base en la programación de entrega 

	Insert		#Tempo_Afiliados 
	(
				id,									cnsctvo_cdgo_tpo_idntfccn_afldo,		nmro_idntfccn_afldo,						
				cnsctvo_cdgo_cdd_rsdnca_afldo,		cnsctvo_cdgo_pln,						cnsctvo_cdgo_sxo,
				nmro_unco_idntfccn_afldo,			cnsctvo_cdgo_sde_ips_prmra,				cnsctvo_cdgo_rngo_slrl,				
				usro_crcn,							prmr_aplldo_afldo,						sgndo_aplldo_afldo,
				prmr_nmbre_afldo,					sgndo_nmbre_afldo,						fcha_ncmnto_afldo,
				cnsctvo_cdgo_cbrtra_sld,			cdgo_cbrtra_sld						
									
	)
	Select distinct    
				t.id_tbla                       , a.cnsctvo_cdgo_tpo_idntfccn      , a.nmro_idntfccn                   ,
				a.cnsctvo_cdgo_cdd              , a.cnsctvo_cdgo_pln               , a.cnsctvo_cdgo_sxo                ,
				a.nmro_unco_idntfccn            , a.cnsctvo_cdgo_sde_usro          , a.cnsctvo_cdgo_rngo               ,
				@lcUsuario                      , isnull(a.prmr_aplldo, @vlr_vacio), isnull(a.sgndo_aplldo, @vlr_vacio),
				isnull(a.prmr_nmbre, @vlr_vacio), isnull(a.sgndo_nmbre, @vlr_vacio), a.fcha_ncmnto                     ,
				@ValorUNO                       , 'RCT'									
	From		BdSiSalud.dbo.tbactuanotificacion a with(nolock)
	Inner Join	#tbDetProgramacion t
	ON			t.cnsctvo_cdgo_ntfccn = a.cnsctvo_ntfccn 
	AND			t.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna	

	Update      a
	Set			cnsctvo_afldo_slctd_orgn = id_tbla
	From		#Tempo_Afiliados a

	Update      a
	Set			edd_afldo_ans = datediff(yy,a.fcha_ncmnto_afldo, @fechaactual),
				edd_afldo_mss = datediff(mm,a.fcha_ncmnto_afldo, @fechaactual),
				edd_afldo_ds = datediff(dd,a.fcha_ncmnto_afldo, @fechaactual)
	From		#Tempo_Afiliados a
	
	Update		a
	Set			cnsctvo_cdgo_tpo_cntrto         = bv.cnsctvo_cdgo_tpo_cntrto        ,	
				nmro_cntrto                     = bv.nmro_cntrto                    ,
				cnsctvo_bnfcro_cntrto           = bv.cnsctvo_bnfcro                 , 
				drccn_afldo                     = bv.drccn_rsdnca                   ,   
				tlfno_afldo                     = tlfno_rsdnca                      ,
				crro_elctrnco_afldo             = eml                               ,
				cdgo_ips_prmra                  = isnull(bv.cdgo_intrno, @vlr_vacio),
				cnsctvo_cdgo_tpo_idntfccn_afldo = bv.cnsctvo_cdgo_tpo_idntfccn      ,
				nmro_idntfccn_afldo             = bv.nmro_idntfccn                  ,
				cnsctvo_cdgo_tpo_vnclcn_afldo   = bv.cnsctvo_cdgo_prntsco /*20160928 sislbr01*/
	From		#Tempo_Afiliados a
	Inner Join  BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv with(nolock)
	On			bv.nmro_unco_idntfccn_afldo  = a.nmro_unco_idntfccn_afldo         
	Where		@fechaactual between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro

	Update      a
	Set         cnsctvo_cdgo_rngo_slrl = cv.cnsctvo_cdgo_rngo_slrl
	From		#Tempo_Afiliados a
	Inner Join  BDAfiliacionValidador.dbo.tbContratosValidador cv with(nolock)
	On          cv.cnsctvo_cdgo_tpo_cntrto = a.cnsctvo_cdgo_tpo_cntrto  And
	            cv.nmro_cntrto             = a.nmro_cntrto
	Where       @fechaactual between cv.inco_vgnca_cntrto and cv.fn_vgnca_cntrto

	Update		a
	Set			cnsctvo_cdgo_dprtmnto_afldo = bf.cnsctvo_cdgo_dprtmnto
	From		#Tempo_Afiliados a
	Inner Join  BDAfiliacionValidador.dbo.tbBeneficiariosFormularioValidador bf with(nolock)
	On			bf.cnsctvo_tpo_idntfccn_bnfcro = a.cnsctvo_cdgo_tpo_idntfccn_afldo And
	            bf.nmro_idntfccn_bnfcro        = a.nmro_idntfccn_afldo
	Where		@fechaactual between bf.inco_vgnca_bnfcro And bf.fn_vgnca_bnfcro

	Update		a
	Set			cdgo_dprtmnto_afldo = dv.cdgo_dprtmnto,						
				dscrpcn_dprtmnto_afldo = dv.dscrpcn_dprtmnto
	From		#Tempo_Afiliados a
	Inner Join  BDAfiliacionValidador.dbo.tbDepartamentos_Vigencias dv with(nolock)
	On			dv.cnsctvo_cdgo_dprtmnto = a.cnsctvo_cdgo_dprtmnto_afldo
	Where		@fechaactual between dv.inco_vgnca And dv.fn_vgnca

	Update		a
	Set			cnsctvo_cdgo_estdo_drcho = mdv.cnsctvo_cdgo_estdo_drcho 
	From		#Tempo_Afiliados a
	Inner Join	bdAfiliacionValidador.dbo.TbMatrizDerechosValidador mdv With(NoLock)
	On			mdv.cnsctvo_cdgo_tpo_cntrto   = a.cnsctvo_cdgo_tpo_cntrto    And
	            mdv.nmro_cntrto               = a.nmro_cntrto                And
				mdv.nmro_unco_idntfccn_bnfcro = a.nmro_unco_idntfccn_afldo
	Where		@fechaactual Between mdv.inco_vgnca_estdo_drcho And mdv.fn_vgnca_estdo_drcho

	Update		a
	Set			cdgo_pln = pv.cdgo_pln,
				cnsctvo_cdgo_tpo_pln = pv.cnsctvo_cdgo_tpo_pln
	From		#Tempo_Afiliados a
	Inner Join	bdAfiliacionValidador.dbo.tbPlanes_Vigencias pv with(nolock)
	On			pv.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Where		@fechaactual Between pv.inco_vgnca And pv.fn_vgnca

	Update		a
	Set			cdgo_tpo_pln = tv.cdgo_tpo_pln
	From		#Tempo_Afiliados a
	Inner Join	bdAfiliacionValidador.dbo.tbTiposPlan_Vigencias tv with(nolock)
	On			tv.cnsctvo_cdgo_tpo_pln = a.cnsctvo_cdgo_tpo_pln
	Where		@fechaactual Between tv.inco_vgnca And tv.fn_vgnca

	Update		a
	Set			dscrpcn_cdd_rsdnca_afldo = cv.dscrpcn_cdd,
				cdgo_cdd_rsdnca_afldo = cv.cdgo_cdd
	From		#Tempo_Afiliados a
	Inner Join	bdAfiliacionValidador.dbo.tbCiudades_Vigencias cv with(nolock)
	On			cv.cnsctvo_cdgo_cdd = a.cnsctvo_cdgo_cdd_rsdnca_afldo
	Where		@fechaactual Between cv.inco_vgnca And cv.fn_vgnca
	
	Update		a
	Set			cdgo_tpo_idntfccn_afldo = cdgo_tpo_idntfccn
	From		#Tempo_Afiliados a
	Inner Join  bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias iv with(nolock)
	On			iv.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn_afldo
	Where		@fechaactual Between iv.inco_vgnca And iv.fn_vgnca

	Update		a
	Set			cdgo_sxo = sv.cdgo_sxo
	From		#Tempo_Afiliados a
	Inner Join  bdAfiliacionValidador.dbo.tbSexos_Vigencias sv with(nolock)
	On			sv.cnsctvo_cdgo_sxo = a.cnsctvo_cdgo_sxo
	Where		@fechaactual Between sv.inco_vgnca And sv.fn_vgnca
	
	Update		a
	Set			cnsctvo_cdgo_sxo_rcn_ncdo = rn.cnsctvo_cdgo_sxo,
				fcha_ncmnto_rcn_ncdo = rn.fcha_ncmnto
	From		#Tempo_Afiliados a
	Inner Join	#tbDetProgramacion d
	On			d.id_tbla = a.id
	Inner Join	bdSisalud.dbo.tbRecienNacido rn with(nolock)
	On			rn.cnsctvo_ntfccn = d.cnsctvo_cdgo_ntfccn
	And			rn.cnsctvo_cdgo_ofcna = d.cnsctvo_cdgo_ofcna

	Update		a
	Set			cdgo_sxo_rcn_ncdo = sv.cdgo_sxo
	From		#Tempo_Afiliados a
	Inner Join  bdAfiliacionValidador.dbo.tbSexos_Vigencias sv with(nolock)
	On			sv.cnsctvo_cdgo_sxo = a.cnsctvo_cdgo_sxo_rcn_ncdo
	Where		@fechaactual Between sv.inco_vgnca And sv.fn_vgnca


	-- Se actualiza el consecutivo de la cohorte para insertar en la tabla temporal
	Update      a
	Set         cnsctvo_cdgo_chrte = ISNULL(b.cnsctvo_cdgo_clsfccn_evnto, @ValorCERO)
	From        #Tempo_Afiliados a
	Inner Join  bdSisalud.dbo.tbafiliadosMarcados b with(nolock)
	On          b.nmro_unco_idntfccn = a.nmro_unco_idntfccn_afldo
	Where       b.cnsctvo_cdgo_clsfccn_evnto In (@cnsctvo_cdgo_clsfccn_evnto_cncr, @cnsctvo_cdgo_clsfccn_evnto_vh) --13 Cancer, 16 vih
	And         b.cnsctvo_cdgo_estdo_ntfccn  In (@cdgo_estdo_ntfccn_ingrsdo , @cdgo_estdo_ntfccn_ntfcdo , @cdgo_estdo_ntfccn_prbble  , --Ingresado   , Notificado, Probable
	                                             @cdgo_estdo_ntfccn_ntfcdo1 , @cdgo_estdo_ntfccn_cnfrmdo, @cdgo_estdo_ntfccn_ntfcdo2 , --Notificado  , Confirmado, Notificado 
												 @cdgo_estdo_ntfccn_cnfrmdo1, @cdgo_estdo_ntfccn_prbble1, @cdgo_estdo_ntfccn_cn_srvco, --Confirmado  , Probable  , Con Servicio
												 @cdgo_estdo_ntfccn_sn_srvco                                                           --Sin Servicio
	                                            )

    -- Paso 3 Poblar tabla Temporal de las IPS con base en la programación de entrega
    Insert Into #Tempo_IPS(id                       , cdgo_intrno, 
	                       nmro_unco_idntfccn_prstdr, usro_crcn
	                      )
	Select      Distinct 
	            t.id_tbla               , r.cdgo_intrno,
				r.nmro_unco_idntfccn_ips, @lcUsuario
	From        #tbDetProgramacion t
	Inner Join  BdSiSalud.dbo.tbCTCRecobro r with(nolock)
	On			r.cnsctvo_ntfccn     = t.cnsctvo_cdgo_ntfccn And
	            r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna  And 
				r.nmro_acta          = t.nmro_acta
	Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
	
	-- Paso 3 Poblar tabla Temporal de las IPS con base tutela	
    Insert Into #Tempo_IPS(id                       , cdgo_intrno, 
	                       nmro_unco_idntfccn_prstdr, usro_crcn
	                      )
	Select      Distinct 
	            t.id_tbla               , a.cdgo_intrno,
				a.nmro_unco_idntfccn_ips, @lcUsuario
	From        #tbDetProgramacion t
	Inner Join  bdSisalud.dbo.tbtutela a with(nolock)
	On			a.cnsctvo_ntfccn     = t.cnsctvo_cdgo_ntfccn And
	            a.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
	Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto8 -- tutela
		
	Select @cnt = Count(1) From #Tempo_IPS

	If @cnt = @ValorCERO
	   Begin
	      Select @nmro_unco_idntfccn_ips_gnrca = nmro_unco_idntfccn_prstdr
          From   bdSisalud.dbo.tbDireccionesPrestador With(NoLock)
          Where  cdgo_intrno = @cdgo_intrno_gnrco
         
		  Insert 
		  Into   #Tempo_IPS(id                       , cdgo_intrno, 
	                        nmro_unco_idntfccn_prstdr, usro_crcn
	                       )
	      Select Distinct 
	             t.id_tbla                    , @cdgo_intrno_gnrco,
				 @nmro_unco_idntfccn_ips_gnrca, @lcUsuario
	      From   #tbDetProgramacion t       
	   End

	Update		i
	Set			cnsctvo_cdgo_tpo_idntfccn_ips_slctnte = isnull(p.cnsctvo_cdgo_tpo_idntfccn,0),
				nmro_idntfccn_ips_slctnte = isnull(p.nmro_idntfccn,''),
				dgto_vrfccn = p.dgto_vrfccn,
				adscrto = p.prstdr_gnrco
	From		#Tempo_IPS i
	Inner Join	BdSiSalud.dbo.tbprestadores p with(nolock)
	On			p.nmro_unco_idntfccn_prstdr = i.nmro_unco_idntfccn_prstdr

	Update		i
	Set			nmbre_ips = dp.nmbre_scrsl
	From		#Tempo_IPS i
	Inner Join	BdSiSalud.dbo.tbDireccionesPrestador dp with(nolock)
	On			dp.cdgo_intrno = i.cdgo_intrno
	And			dp.prncpl = @ValorSI

	Update		i
	Set			nmbre_scrsl = dp.nmbre_scrsl,
				drccn_prstdr = dp.drccn,
				tlfno_prstdr = dp.tlfno,
				cnsctvo_cdgo_cdd_prstdr = dp.cnsctvo_cdgo_cdd,
				usro_crcn = @lcUsuario,
				cdgo_hbltcn = dp.cdgo_prstdr_mnstro
	From		#Tempo_IPS i
	Inner Join	BdSiSalud.dbo.tbDireccionesPrestador dp with(nolock)
	On			dp.cdgo_intrno = i.cdgo_intrno

	Update		i
	Set			cdgo_cdd_prstdr = cv.cdgo_cdd,
				cnsctvo_cdgo_dprtmnto_prstdr = cv.cnsctvo_cdgo_dprtmnto,
				nmro_indctvo_prstdr = cv.indctvo
	From		#Tempo_IPS i
	Inner Join	bdAfiliacionValidador.dbo.tbCiudades_Vigencias cv with(nolock)
	On			cv.cnsctvo_cdgo_cdd = i.cnsctvo_cdgo_cdd_prstdr
	And			@fechaactual Between cv.inco_vgnca And cv.fn_vgnca

	Update		i
	Set			cdgo_dprtmnto_prstdr = dv.cdgo_dprtmnto
	From		#Tempo_IPS i
	Inner Join	bdAfiliacionValidador.dbo.tbDepartamentos_Vigencias dv with(nolock)
	On			dv.cnsctvo_cdgo_dprtmnto = i.cnsctvo_cdgo_dprtmnto_prstdr
	And			@fechaactual Between dv.inco_vgnca And dv.fn_vgnca
	

	-- Paso 4. Insertamos a la tabla temporal los médicos con base en la programación de entrega
	-- Insertamos Los médicos solicitantes por Evento CTC
		Insert		#Tempo_Medicos
		(
					id,						nmro_unco_idntfccn_mdco,				usro_crcn,
					cnsctvo_cdgo_ntfccn,	cnsctvo_cdgo_ofcna,						cnsctvo_cdgo_ctc
		)		
		Select distinct
					t.id_tbla,				isnull(r.nmro_unco_idntfccn_mdco,0),	@lcUsuario,
					t.cnsctvo_cdgo_ntfccn,	t.cnsctvo_cdgo_ofcna,					r.cnsctvo_cdgo_ctc
		From		BdSiSalud.dbo.tbCTCRecobro r with(nolock)
		Inner Join	#tbDetProgramacion t
		On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
		And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
		And			r.nmro_acta = t.nmro_acta
		Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc

		-- Actualizamos la marca lista de chequeo
		Update		t
		Set			mrca_lsta_chk = @ValorSI
		From		#tbDetProgramacion t
		Inner Join	#Tempo_Medicos tm
		On			tm.id = t.id_tbla

		-- Insertamos los médicos solicitantes por Evento Tutelas
		Insert		#Tempo_Medicos
		(
					id,						nmro_unco_idntfccn_mdco,				usro_crcn,
					cnsctvo_cdgo_ntfccn,	cnsctvo_cdgo_ofcna
		)		
		Select distinct
					t.id_tbla,				isnull(r.nmro_unco_idntfccn_mdco,0),	@lcUsuario,
					t.cnsctvo_cdgo_ntfccn,	t.cnsctvo_cdgo_ofcna
		From		BdSiSalud.dbo.tbTutela r with(nolock)
		Inner Join	#tbDetProgramacion t
		On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
		And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
		Left Join  #Tempo_Medicos t1
		On			t1.id = t.id_tbla		
		Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto8 -- tutela
		And			t.mrca_lsta_chk = @ValorNO
		And         t1.id is null

		-- Actualizamos la marca lista de chequeo
		Update		t
		Set			mrca_lsta_chk = @ValorSI
		From		#tbDetProgramacion t
		Inner Join	#Tempo_Medicos tm
		On			tm.id = t.id_tbla
		
		-- Insertamos los médicos solicitantes del Histórico de solicitudes anteriores para
		-- los casos diferentes a eventos CTC y TUTELAS

		-- Recuperamos el parámetro de días para verificar histórico de servicios
		Select 	@diasHistorico	= vlr_prmtro_nmrco
		From	bdsisalud.dbo.tbParametrosGenerales_Vigencias
		Where	cnsctvo_cdgo_prmtro_gnrl = @cnsctvo_cdgo_prmtro_gnrl93
		And		@fechaactual between inco_vgnca and fn_vgnca
		And		vsble_usro = @ValorSI

		Set @fechacalculada = dateadd(day,-@diasHistorico,cast(@fechaactual As Date))
		
		Insert		#Tempo_Medicos
		(
					id,							nmro_unco_idntfccn_mdco,								usro_crcn,
					cnsctvo_cdgo_ntfccn,		cnsctvo_cdgo_ofcna
		)		
		Select distinct
					t.id_tbla,					isnull(sm.nmro_unco_idntfccn_mdco,@ValorCERO),		@lcUsuario,
					t.cnsctvo_cdgo_ntfccn,		t.cnsctvo_cdgo_ofcna
		From        #tbDetProgramacion t
		Inner Join  #Tempo_Afiliados ta
		On			ta.id = t.id_tbla
		Inner Join  gsa.tbASServiciosSolicitados ss with(nolock)
		On			ss.cnsctvo_cdgo_srvco_slctdo = t.cnsctvo_prstcn
		And			ss.fcha_prstcn_srvco_slctdo Between @fechacalculada and @fechaactual
		Inner Join  gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios sa with(nolock)
		On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
		And			sa.nmro_unco_idntfccn_afldo = ta.nmro_unco_idntfccn_afldo
		Inner Join	gsa.tbASMedicoTratanteSolicitudAutorizacionServicios sm with(nolock)
		On			sm.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
		left Join   #Tempo_Medicos t1
		On			t1.id = t.id_tbla		
		Where		t.mrca_lsta_chk = @ValorNO
		And         t.cnsctvo_cdgo_clsfccn_evnto in(@cnsctvo_cdgo_clsfccn_evnto8,@cnsctvo_cdgo_clsfccn_evnto11)  /*tutela-ctc*/
		And			t1.id is null	

		-- Actualizamos la marca lista de chequeo
		Update		t
		Set			mrca_lsta_chk = @ValorSI
		From		#tbDetProgramacion t
		Inner Join	#Tempo_Medicos tm
		On			tm.id = t.id_tbla

		
		-- Insertamos Los Médicos solicitantes Genérico para los casos que no esten asignados en programación de entrega
		Insert		#Tempo_Medicos
		(
					id,							nmro_idntfccn_mdco_trtnte,						nmro_unco_idntfccn_mdco,
					usro_crcn
					
		)	
		Select distinct		
					t.id_tbla,					@nmro_idntfccn_mdco_mga,					@nmro_unco_idntfccn_mdco100961,								
					@lcUsuario
		From        #tbDetProgramacion t
		Left Join   #Tempo_Medicos t1
		On			t1.id = t.id_tbla
		Where		t.mrca_lsta_chk = @ValorNO
		And         t1.id is null

		Update  t1
		Set     nmro_unco_idntfccn_mdco = @nmro_unco_idntfccn_mdco100961,
				nmro_idntfccn_mdco_trtnte = @nmro_idntfccn_mdco_mga
		From    #Tempo_Medicos t1
		Where	nmro_unco_idntfccn_mdco = @ValorCERO


	    Update      t
		Set			prmr_nmbre_mdco_trtnte = ltrim(rtrim(isnull(b.prmr_nmbre,''))),
					sgndo_nmbre_mdco_trtnte	= ltrim(rtrim(isnull(b.sgndo_nmbre,''))),
					prmr_aplldo_mdco_trtnte	= ltrim(rtrim(isnull(b.prmr_aplldo,''))),
					sgndo_aplldo_mdco_trtnte = ltrim(rtrim(isnull(b.sgndo_aplldo,''))),
					cnsctvo_cdgo_espcldd_mdco_trtnte = isnull(b.cnsctvo_cdgo_espcldd,0),
					rgstro_mdco_trtnte = isnull(b.rgstro_mdco,''),
					adscrto = @ValorNO
		From        #Tempo_Medicos t
		Inner Join  bdSisalud.dbo.tbGenericosMedicos b with(nolock)
		On			b.nmro_idntfccn = t.nmro_idntfccn_mdco_trtnte
		where		b.cnsctvo_gnrco = @cnsctvo_gnrco_mdco


		Update		t
		Set			prmr_nmbre_mdco_trtnte = ltrim(rtrim(isnull(m.prmr_nmbre_afldo,''))),
					sgndo_nmbre_mdco_trtnte	= ltrim(rtrim(isnull(m.sgndo_nmbre_afldo,''))),
					prmr_aplldo_mdco_trtnte	= ltrim(rtrim(isnull(m.prmr_aplldo,''))),
					sgndo_aplldo_mdco_trtnte = ltrim(rtrim(isnull(m.sgndo_aplldo,''))),
					cnsctvo_cdgo_espcldd_mdco_trtnte = isnull(m.cnsctvo_cdgo_espcldd,0),
					rgstro_mdco_trtnte = isnull(m.rgstro_mdco,''),
					adscrto = @ValorSI
		From		#Tempo_Medicos t
		Inner Join	BdSiSalud.dbo.tbMedicos m with(nolock)	
		On			m.nmro_unco_idntfccn_prstdr = t.nmro_unco_idntfccn_mdco
		Where		t.nmro_unco_idntfccn_mdco != @nmro_unco_idntfccn_mdco100961

		Update		t
		Set			prmr_nmbre_mdco_trtnte = ltrim(rtrim(isnull(b.prmr_nmbre,''))),
					sgndo_nmbre_mdco_trtnte	= ltrim(rtrim(isnull(b.sgndo_nmbre,''))),
					prmr_aplldo_mdco_trtnte	= ltrim(rtrim(isnull(b.prmr_aplldo,''))),
					sgndo_aplldo_mdco_trtnte = ltrim(rtrim(isnull(b.sgndo_aplldo,''))),
					cnsctvo_cdgo_espcldd_mdco_trtnte = isnull(b.cnsctvo_cdgo_espcldd,0),
					rgstro_mdco_trtnte = isnull(b.rgstro_mdco,''),
					adscrto = @ValorNO
		From		#Tempo_Medicos t
		INNER JOIN  bdsisalud.dbo.tbGenericosDetalleCTC e  with(nolock)
		ON			e.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn 
		AND			e.cnsctvo_cdgo_ofcna  = t.cnsctvo_cdgo_ofcna 
		AND			t.cnsctvo_cdgo_ctc = e.cnsctvo_cdgo_ctc 
		AND			e.clse_prstdr = @clse_prstdrM AND e.estdo = @estdo
		Inner Join  bdSisalud.dbo.tbGenericosMedicos b with(nolock)
		On			e.cnsctvo_gnrco = b.cnsctvo_gnrco                            
		Where		t.nmro_unco_idntfccn_mdco = @nmro_unco_idntfccn_mdco100961 

		Update		t
		Set			cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = isnull(p.cnsctvo_cdgo_tpo_idntfccn,0),
					nmro_idntfccn_mdco_trtnte = isnull(p.nmro_idntfccn,'')
		From		#Tempo_Medicos t
		Inner Join	bdSisalud.dbo.tbprestadores p with(nolock)
		On			p.nmro_unco_idntfccn_prstdr = t.nmro_unco_idntfccn_mdco

		Update		t
		Set			cdgo_espcldd_mdco_trtnte = isnull(e.cdgo_espcldd,'')
		From		#Tempo_Medicos t
		Inner Join	bdSisalud.dbo.tbEspecialidades_Vigencias e with(nolock)
		On			e.cnsctvo_cdgo_espcldd = t.cnsctvo_cdgo_espcldd_mdco_trtnte
		And			@fechaactual Between e.inco_vgnca And e.fn_vgnca

		Update      t
		Set         cdgo_tpo_idntfccn_mdco_trtnte =  substring(isnull(d.cdgo_tpo_idntfccn,''),1,2)
		From		#Tempo_Medicos t
		Inner Join	bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias d  with(nolock)
		On			d.cnsctvo_cdgo_tpo_idntfccn = t.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte
		And			@fechaactual Between d.inco_vgnca And d.fn_vgnca

		
	-- Paso 5. Insertamos a la tabla temporal los diagnósticos con base en la programación de entrega
	
	Insert	#Tempo_Diagnostico
	(
				id,					cnsctvo_cdgo_dgnstco,			cnsctvo_cdgo_cntngnca,				
				usro_crcn,			cnsctvo_cdgo_tpo_dgnstco,		cdgo_tpo_dgnstco,
				cnsctvo_cdgo_rcbro
	)
	Select distinct
				t.id_tbla,			t.cnsctvo_cdgo_dgnstco,			am.cnsctvo_cdgo_cntngnca,			
				@lcUsuario,			@ValorUNO,						'01',
				am.cnsctvo_cdgo_clsfccn_evnto
	From		bdSisalud.dbo.tbafiliadosMarcados am with(nolock)
	Inner Join	#tbDetProgramacion t
	On			am.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
	And			am.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna

	Update		d
	Set			cdgo_dgnstco  = dv.cdgo_dgnstco/*,
				cnsctvo_cdgo_rcbro = iif(dv.cnsctvo_cdgo_rcbro = 0,9,dv.cnsctvo_cdgo_rcbro)*/
	From		#Tempo_Diagnostico d
	Inner Join	bdSisalud.dbo.tbDiagnosticos_Vigencias dv with(nolock)
	On			dv.cnsctvo_cdgo_dgnstco = d.cnsctvo_cdgo_dgnstco
	And			@fechaactual between dv.inco_vgnca And dv.fn_vgnca


	Update		d
	Set			cdgo_rcbro = rv.cdgo_rcbro
	From		#Tempo_Diagnostico d
	Inner Join	bdSisalud.dbo.tbRecobros_Vigencias rv with(nolock)
	On			rv.cnsctvo_cdgo_rcbro = d.cnsctvo_cdgo_rcbro
	And			@fechaactual between rv.inco_vgnca and rv.fn_vgnca

	Update		d
	Set			cdgo_cntngnca = cv.cdgo_cntngnca
	From		#Tempo_Diagnostico d
	Inner Join	bdSisalud.dbo.tbContingencias_Vigencias cv with(nolock)
	On			cv.cnsctvo_cdgo_cntngnca = d.cnsctvo_cdgo_cntngnca
	And			@fechaactual between cv.inco_vgnca and cv.fn_vgnca

	-- Paso 6. Insertamos a la tabla temporal las prestaciones con base en la programación de entrega
	Insert		#Tempo_Prestaciones
	(  
				id,										cnsctvo_cdgo_srvco_slctdo,				cntdd_slctda,
				fcha_prstcn_srvco_slctdo,				vlr_lqdcn_srvco,						usro_crcn,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cdgo_tpo_srvco,							cdgo_undd_tmpo_trtmnto_slctdo,
				cdgo_prstcn_prstdr,						cdgo_tpo_atrzcn,						cdgo_prsntcn_dss,
				cdgo_undd_prdcdd_dss,					cdgo_frma_frmctca,						cdgo_grpo_trptco,
				cdgo_va_admnstrcn_mdcmnto,				cdgo_undd_cncntrcn_dss,					cdgo_ltrldd,
				cdgo_va_accso,							cnsctvo_mdcmnto_slctdo_orgn,			cnsctvo_cdgo_ltrldd,
				cnsctvo_cdgo_va_accso,					cnsctvo_srvco_slctdo_orgn,				cnfrmcn_cldd,
				cnsctvo_cdgo_tpo_atrzcn
	)
	Select distinct 
				t.id_tbla,								t.cnsctvo_prstcn,						t.cntdd,
				@fechaactual,							@ValorCERO,								@lcUsuario,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@ValorCERO,								@ValorCERO,
				@ValorCERO,								@ValorCERO,								@ValorCERO,
				@ValorCERO,								@ValorCERO,								@ValorCERO,
				@ValorCERO,								@ValorCERO,								@ValorCERO,
				@ValorCERO,								@ValorCERO,								@ValorCUATRO,	
				@ValorTRES,								@ValorCERO,								@ValorCERO,
				@ValorCERO
	From		#tbDetProgramacion t

	Update      p
	Set			cnsctvo_cdgo_tpo_srvco = c.cnsctvo_cdgo_tpo_cdfccn,
				dscrpcn_srvco_slctdo = c.dscrpcn_cdfccn,
				cdgo_srvco_slctdo = c.cdgo_cdfccn
	From		#Tempo_Prestaciones p
	Inner Join	bdSisalud.dbo.tbCodificaciones c with(nolock)
	On			c.cnsctvo_cdfccn = p.cnsctvo_cdgo_srvco_slctdo
	
	Update		p
	Set			tmpo_trtmnto_slctdo = pfe.ds_trtmnto_ms,
				dss = pfe.cntdd_dss,
				cnsctvo_cdgo_prsntcn_dss = isnull(pfe.cnsctvo_cdgo_dss,0),
				prdcdd_dss = pfe.ds_trtmnto,
				cnsctvo_cdgo_undd_prdcdd_dss = pfe.cnsctvo_cdgo_prdcdd,
				cnsctvo_cdgo_prsntcn = pfe.cnsctvo_cdgo_prsntcn,
				cnsctvo_cdgo_prstcn_prstdr = t.cnsctvo_prstcn,
				cncntrcn_dss = pfe.cntdd_cda,
				prsntcn_dss	 = pfe.cntdd_prsntcn,
				cnsctvo_cdgo_undd_cncntrcn_dss  = pfe.cnsctvo_cdgo_frcnca_cda,
				cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo = isnull(pfe.cnsctvo_cdgo_frcnca,0)
	From		#Tempo_Prestaciones p
	Inner Join  #tbDetProgramacion t
	On			t.id_tbla = p.id 
	Inner Join  bdSisalud.dbo.tbProgramacionFechaEvento pfe with(nolock)
	On			t.cnsctvo_prgrmcn_fcha_evnto = pfe.cnsctvo_prgrmcn_fcha_evnto
	And			t.cnsctvo_prgrmcn_prstcn = pfe.cnsctvo_prgrmcn_prstcn
	
	Update      p
	Set			cnsctvo_cdgo_frma_frmctca = isnull(cms.cnsctvo_frma_frmctca,0),
				cnsctvo_cdgo_va_admnstrcn_mdcmnto = isnull(cms.cnsctvo_cdgo_va_admnstrcn,0),
				cnsctvo_mdcmnto_slctdo_orgn = isnull(cms.cnsctvo_cms,0),
				cnsctvo_cdgo_undd_cncntrcn_dss = isnull(cms.cnsctvo_cncntrcn,0),
				cncntrcn_dss = isnull(cms.cntdd_cncntrcn,0)
	From		#Tempo_Prestaciones p
	Inner Join  #tbDetProgramacion t
	On			t.id_tbla = p.id 
	Inner Join  bdSisalud.dbo.tbcums cms with(nolock)
	On			cms.cnsctvo_cms = t.cnsctvo_prstcn


	Update		p
	Set			cnsctvo_cdgo_grpo_trptco = 	 c.cnsctvo_cdgo_sbgrpo_trptco
	From		#Tempo_Prestaciones p
	Inner Join  bdSisalud.dbo.tbcums a with(nolock)
	On			a.cnsctvo_cms = p.cnsctvo_cdgo_srvco_slctdo
	Inner Join	bdSisalud.dbo.tbMNATC_Vigencias b With(NoLock)
	On			b.cnsctvo_cdgo_atc = a.cnsctvo_atc
	Inner Join	bdSisalud.dbo.tbSubgrupoQuimico_vigencias e With(NoLock)              
	On			e.cnsctvo_cdgo_sbgrpo_qmco = b.cnsctvo_cdgo_sbgrpo_qmco               
	Inner Join	bdSisalud.dbo.tbSubgrupoFarmacologico_Vigencias f With(NoLock)               
	On			f.cnsctvo_cdgo_sbgrpo_frmclgco = e.cnsctvo_cdgo_sbgrpo_frmclgco
	Inner Join	bdSisalud.dbo.tbSubgrupoTerapeutico_vigencias c With(NoLock)  
	On			c.cnsctvo_cdgo_sbgrpo_trptco = f.cnsctvo_cdgo_sbgrpo_trptco
	Where		@fechaactual Between e.inco_vgnca and e.fn_vgnca              
	And			@fechaactual Between b.inco_vgnca and b.fn_vgnca              
	And			@fechaactual Between c.inco_vgnca and c.fn_vgnca              
	And			@fechaactual Between f.inco_vgnca and f.fn_vgnca

	update		p
	Set			nmro_prstcn = id_fila
	From		#Tempo_Prestaciones p

	
	-- Paso 7 Insertamos a la tabla temporal las solicitudes con base en la programación de entrega
	Insert	#Tempo_Solicitudes
	(  
			id,										cnsctvo_cdgo_orgn_atncn,					cnsctvo_cdgo_tpo_srvco_slctdo,
			cnsctvo_cdgo_prrdd_atncn,				cnsctvo_cdgo_tpo_ubccn_pcnte,				cnsctvo_cdgo_tpo_slctd,
			jstfccn_clnca,							cnsctvo_cdgo_mdo_cntcto_slctd,				cnsctvo_cdgo_tpo_trnsccn_srvco_sld,
			nmro_slctd_prvdr,						nmro_slctd_pdre,							cdgo_eps,
			cnsctvo_cdgo_clse_atncn,				cnsctvo_cdgo_frma_atncn,					fcha_slctd,
			nmro_slctd_atrzcn_ss,					nmro_slctd_ss_rmplzo,						cnsctvo_cdgo_ofcna_atrzcn,
			nuam,									srvco_hsptlzcn,								ga_atncn,
			cnsctvo_prcso_vldcn,					cnsctvo_prcso_mgrcn,						mgrda_gstn,
			spra_tpe_st,							dmi,										obsrvcn_adcnl,
			usro_crcn,								cdgo_orgn_atncn,							cdgo_tpo_srvco_slctdo,
			cdgo_prrdd_atncn,						cdgo_tpo_ubccn_pcnte,						cdgo_clse_atncn,
			cdgo_frma_atncn,						cdgo_mdo_cntcto_slctd,						anio_slctd,
			cnsctvo_cdgo_tpo_orgn_slctd

	)
	Select distinct
			t.id_tbla,								4, /*--ENFERMEDAD GENERAL*/					2,
			2,										@ValorUNO,									@ValorCERO,
			'Generado por programación de entregas',5,											@ValorCERO,
			'',										'',											'EPS018',
			@ValorUNO,								@ValorUNO,									@fechaactual,
			t.id_tbla,								@ValorCERO,									cnsctvo_cdgo_ofcna,
			@ValorCERO,								'',											'',
			@ValorCERO,								@ValorCERO,									@ValorCERO,
			@ValorCERO,								@ValorCERO,									'',
			'proc_progentrega',						13,											'02',
			'02',									@ValorUNO,									'',
			'',										'05',										year(@fechaactual),
			iif(t.mrca_ops_vrtl = 'S',3,2)
	From	#tbDetProgramacion t
End
