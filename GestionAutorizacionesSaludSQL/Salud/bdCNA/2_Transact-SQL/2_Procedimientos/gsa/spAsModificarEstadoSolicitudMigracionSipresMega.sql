USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsModificarEstadoSolicitudMigracionSipresMega]    Script Date: 14/02/2017 11:27:17 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


If(Object_id('gsa.spAsModificarEstadoSolicitudMigracionSipresMega') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsModificarEstadoSolicitudMigracionSipresMega As Select 1';
End
Go

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: tbAsModificarEstadoSolicitudMigracionSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Cambia el estado de las solicitudes de acuerdo a los estados de la migracion
							  D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  Entrada:
								   1- @usro_sstma: usuario del sistema
							  P\>  													
* Variables					: <\V  1- @fcha_actl: Fecha Actual obtenida del sistema.
								   2- @fin_vigencia: Fecha de fin de vigencia utilizada para el historico de 
												     cambio de estado de conceptos
								   3- @vldo_si: Valor por defecto para el cambio de estado que indica la valides del registro
								   4- @cnsctvo_cdgo_mdlo_cra46: codigo que crea mega
								   5- @vlr_cro: Almacena el valor 0
								   6- @cnsctvo_cdgo_estdo8: Codigo de estado que indica que la ops esta impresa en Sipres
								   7- @cnsctvo_cdgo_estdo3: Codigo Sipres que indica que la ops esta anulada.
								   8- @cnsctvo_cdgo_estdo11: Codigo sipres que indica que la ops esta vencida.
								   9- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15: Codigo Mega que indica que la ops esta entregada
								   10- @vlr_uno: Almacena el valor 1
								   11- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14: Codigo Mega que indica que la Ops fue anulada
								   12- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10: Codigo Mega que indica que la ops esta vencida.
								   13- @cdgo_exto: Codigo que indica que el proceso termino de forma exitosa.
								   14- @cdgo_errr: Codigo que indica que el proceso termino con errores.
								   15- @mnsge_exto: mensaje que informa la terminacion correcta del proceso.
								   16- @mnsge_errr: Mensaje que informa que el proceso termino con errores
								   17- @mnmo: Variable utilizada para ciclo de anulacion de cuotas de recuperacion.
								   18- @mxmo: Variable utilizada para ciclo de anulacion de cuotas de recuperacion.
								   19- @nmro_unco_ops: Utilizada en ciclo de anulacin de cuotas de recuperacion, guarda los numero de 
													   ops que se les anulara cuota.
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.spAsMigracionEstadosSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se modifica el orden de ejecucion DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agregan consultas para cambiar los estados de las solicitudes No Autorizadas y
							   No Tramitadas. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se generaliza actualizacion de estado de la solicitud, se retiran condicionales de los estados
							   y se deja un unico update. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/10 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsModificarEstadoSolicitudMigracionSipresMega]
	@usro_sstma UdtUsuario
As
Begin
	Set NoCount On

	Declare @cnsctvo_cdgo_estdo7						UdtConsecutivo,
			@cnsctvo_cdgo_estdo8						UdtConsecutivo,
			@cnsctvo_cdgo_estdo2						UdtConsecutivo,
			@cnsctvo_cdgo_estdo3						UdtConsecutivo,
			@cnsctvo_cdgo_estdo4						UdtConsecutivo,
			@cnsctvo_cdgo_estdo11						UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo12	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo8	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo6	UdtConsecutivo,
			@fcha_actl									Date,
			@vlr_uno									Int,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10	UdtConsecutivo,
			@fn_vgnca									Date;

	-- Tabla temporal para cargar las prestaciones que no cambiaron de estadoo que cambiaron y no fueron anuladas o vencidas
	Create Table #tempServicios
	(
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
		cnsctvo_srvco_slctdo			UdtConsecutivo,
		cnsctvo_cdgo_estdo_srvco_slctdo	UdtConsecutivo
	);

	-- Se carga la informacion para el historico de cambios de estado de la solicitud
	Create Table #tempHistoricoTrazaSolicitud
	(
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo,
		cnsctvo_cdgo_estdo_slctd					UdtConsecutivo,
		cnsctvo_hstrco_estds_slctd_srvco_sld_orgn	UdtConsecutivo,
		inco_vgnca									Date,
		fn_vgnca									Date,
		fcha_fn_vldz_rgstro							Date,
		vldo										Int,
		usro_crcn									UdtUsuario,
		fcha_crcn									Date,
		usro_ultma_mdfccn							UdtUsuario,
		fcha_ultma_mdfccn							Date
	);

	Set @cnsctvo_cdgo_estdo7 = 7;
	Set @cnsctvo_cdgo_estdo8 = 8;
	Set @cnsctvo_cdgo_estdo2 = 2;
	Set @cnsctvo_cdgo_estdo3 = 3;
	Set @cnsctvo_cdgo_estdo4 = 4;
	Set @cnsctvo_cdgo_estdo11 = 11;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15 = 15;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14 = 14;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo12 = 12;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo8 = 8;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo6 = 6;
	Set @fcha_actl = getDate();
	Set @vlr_uno = 1;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10 = 10;
	Set @fn_vgnca = '9999-12-31 00:00:00.000';	
	
	-- Carga las prestaciones que en la migracion el estado se cambio a anulado
	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo 
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo3
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14;			

	--Carga las prestaciones que en la migracion el estado cambio a vencido
	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo  
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo11
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10;

	--Carga las prestaciones que en la migracion el estado cambio a vencido
	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo  
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo2
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo8;

	-- 
	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo  
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo4
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo6;

	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo  
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15;

	Insert Into #tempServicios
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo
	)
	Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_estdo_srvco_slctdo  
	From		gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempPrestaciones em 
	On			sas.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		em.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo7
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo12;

	-- Si hay pretaciones en estado vencido se eliminan de la tabla temporal
	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10;

	-- Si hay prestaciones en estado anulado se eliminan de la tabla temporal
	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14;

	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo6;

	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo8;	

	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15;	

	Delete 
	From		#tempServicios
	Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo12;	

	Insert Into #tempHistoricoTrazaSolicitud
	(
				cnsctvo_slctd_atrzcn_srvco, 		cnsctvo_hstrco_estds_slctd_srvco_sld_orgn,		inco_vgnca,							
				fn_vgnca,							fcha_fn_vldz_rgstro,							vldo,								
				usro_crcn,							fcha_crcn,										usro_ultma_mdfccn,					
				fcha_ultma_mdfccn
	)
	Select Distinct
				em.cnsctvo_slctd_atrzcn_srvco,	em.cnsctvo_cdgo_estdo_srvco_slctdo,				@fcha_actl,
				@fn_vgnca,						@fn_vgnca,										@vlr_uno,
				@usro_sstma,					@fcha_actl,										@usro_sstma,
				@fcha_actl
	From		#tempPrestaciones em
	Inner Join	gsa.tbASHistoricoEstadosSolicitudAutorizacionServicios hsas With(RowLock)
	On			em.cnsctvo_slctd_atrzcn_srvco = hsas.cnsctvo_slctd_atrzcn_srvco
	Left Join	#tempServicios s	
	On			s.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco
	Where		s.cnsctvo_slctd_atrzcn_srvco Is Null 
	And			@fcha_actl Between hsas.inco_vgnca And hsas.fn_vgnca;

	-- Se cambia el fin de vigencia del estado actual vigente
	Update		hsas
	Set			fn_vgnca = @fcha_actl,
				usro_ultma_mdfccn = @usro_sstma,
				fcha_ultma_mdfccn = @fcha_actl
	From		gsa.tbASHistoricoEstadosSolicitudAutorizacionServicios hsas With(RowLock)
	Inner Join	#tempHistoricoTrazaSolicitud hts
	On			hsas.cnsctvo_hstrco_estds_slctd_atrzcn_srvco = hts.cnsctvo_hstrco_estds_slctd_srvco_sld_orgn
	Where		@fcha_actl Between hsas.inco_vgnca And hsas.fn_vgnca;

	-- Se cambia el estado de las solicitudes anuladas
	Update		hs
	Set			cnsctvo_cdgo_estdo_slctd = em.cnsctvo_cdgo_estdo_srvco_slctdo
	From		#tempHistoricoTrazaSolicitud hs
	Inner Join	#tempPrestaciones em
	On			hs.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_atrzcn_srvco


	-- Se guarda el historico en la tabla real
	Insert Into gsa.tbASHistoricoEstadosSolicitudAutorizacionServicios
	(	
				cnsctvo_slctd_atrzcn_srvco, 		cnsctvo_hstrco_estds_slctd_srvco_sld_orgn,		inco_vgnca,							
				fn_vgnca,							fcha_fn_vldz_rgstro,							vldo,								
				usro_crcn,							fcha_crcn,										usro_ultma_mdfccn,					
				fcha_ultma_mdfccn,					cnsctvo_cdgo_estdo_slctd
	)
	Select
				cnsctvo_slctd_atrzcn_srvco, 		cnsctvo_hstrco_estds_slctd_srvco_sld_orgn,		inco_vgnca,							
				fn_vgnca,							fcha_fn_vldz_rgstro,							vldo,								
				usro_crcn,							fcha_crcn,										usro_ultma_mdfccn,					
				fcha_ultma_mdfccn,					cnsctvo_cdgo_estdo_slctd
	From		#tempHistoricoTrazaSolicitud;
		
	
	
	
	Drop Table #tempHistoricoTrazaSolicitud;
	Drop Table #tempServicios;

End

