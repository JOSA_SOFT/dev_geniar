USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaDatosInformacionSolicitud] 83

ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionSolicitud] 
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @ldfcha_consulta DATETIME

	CREATE TABLE #DatosSolicitudAutorizacion(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo, --numeroSolicitud		
		fcha_crcn 						datetime,		--fechaCreacion
		fcha_slctd						datetime,		--fechaSolicitud
		nmro_slctd_atrzcn_ss 			varchar(16),	--numeroRadicado	
		cnsctvo_cdgo_tpo_ubccn_pcnte 	udtConsecutivo,	--ubicacionPaciente	
		cnsctvo_cdgo_prrdd_atncn 		udtConsecutivo,	--prioridad
		adtr_slctnte					udtDescripcion,     --auditor solicitante
		cdgo_mdo_cntcto 				udtDescripcion,		--origenSolicitud
		dscrpcn_orgn_atncn 				udtDescripcion,	--origenAntencion
		dscrpcn_clse_atncn 				udtDescripcion,	--claseAtencionSol
		cnsctvo_cdgo_frma_atncn			udtConsecutivo,
		jstfccn_clnca					varchar(2000),	--justificacionClinica
		spra_tpe_st						udtLogico,
		dscrpcn_rcbro					udtDescripcion,
		nmro_slctd_prvdr				varchar(15)	  ,
		dscrpcn_prrdd_atncn		        udtDescripcion,
		dscrpcn_tpo_ubccn_pcnte         udtDescripcion
	);


	SET	@ldfcha_consulta = GETDATE()

	INSERT INTO #DatosSolicitudAutorizacion(
		cnsctvo_slctd_atrzcn_srvco,		fcha_crcn,					fcha_slctd,			nmro_slctd_atrzcn_ss,
		cnsctvo_cdgo_tpo_ubccn_pcnte,	cnsctvo_cdgo_prrdd_atncn,	adtr_slctnte,		cdgo_mdo_cntcto,
		dscrpcn_orgn_atncn,				dscrpcn_clse_atncn,			jstfccn_clnca,		cnsctvo_cdgo_frma_atncn,
		spra_tpe_st,	nmro_slctd_prvdr)
	SELECT
		ss.cnsctvo_slctd_atrzcn_srvco,	ss.fcha_crcn,				ss.fcha_slctd,		ss.nmro_slctd_atrzcn_ss,
		ss.cnsctvo_cdgo_tpo_ubccn_pcnte,ss.cnsctvo_cdgo_prrdd_atncn,NULL,				mcontac.dscrpcn_mdo_cntcto,
		oat.dscrpcn_orgn_atncn, cat.dscrpcn_clse_atncn			   , ss.jstfccn_clnca,  cat.cnsctvo_cdgo_frma_atncn,
		ss.spra_tpe_st, ltrim(rtrim(ss.nmro_slctd_prvdr))			
	FROM gsa.tbASSolicitudesAutorizacionServicios ss WITH(NOLOCK)
		INNER JOIN BDCna.prm.tbASMediosContacto_Vigencias mcontac WITH(NOLOCK) 
			ON ss.cnsctvo_cdgo_mdo_cntcto_slctd = mcontac.cnsctvo_cdgo_mdo_cntcto
		INNER JOIN bdSisalud.dbo.tbPMOrigenAtencion_Vigencias oat WITH(NOLOCK) 
			ON oat.cnsctvo_cdgo_orgn_atncn = ss.cnsctvo_cdgo_orgn_atncn
		INNER JOIN bdSisalud.dbo.tbClasesAtencion_Vigencias cat	  WITH(NOLOCK) 
			ON cat.cnsctvo_cdgo_clse_atncn = ss.cnsctvo_cdgo_clse_atncn
	WHERE ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	AND @ldfcha_consulta BETWEEN oat.inco_vgnca AND oat.fn_vgnca
	AND @ldfcha_consulta BETWEEN cat.inco_vgnca AND cat.fn_vgnca
	AND @ldfcha_consulta BETWEEN mcontac.inco_vgnca AND mcontac.fn_vgnca;


	UPDATE    dsa Set dscrpcn_rcbro = cv.dscrpcn_clsfccn_evnto
	FROM    #DatosSolicitudAutorizacion dsa
	INNER JOIN gsa.tbASDiagnosticosSolicitudAutorizacionServicios D WITH(NOLOCK) ON d.cnsctvo_slctd_atrzcn_srvco = dsa.cnsctvo_slctd_atrzcn_srvco 
	AND dsa.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	INNER JOIN bdsisalud.dbo.tbClasificacionEventosNotificacion_vigencias CV WITH(NOLOCK) 
	ON d.cnsctvo_cdgo_rcbro = cv.cnsctvo_cdgo_clsfccn_evnto
	Where  @ldfcha_consulta BETWEEN CV.inco_vgnca AND CV.fn_vgnca;


	Update dsa set adtr_slctnte = concat(rtrim(prmr_nmbre_usro), ' ', 
										 rtrim(sgndo_nmbre_usro), ' ',  
										 rtrim(prmr_aplldo_usro),' ', 
										 rtrim(sgndo_aplldo_usro))
	FROM    #DatosSolicitudAutorizacion dsa WITH(NOLOCK)
	Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios ss WITH(NOLOCK) on ss.cnsctvo_slctd_atrzcn_srvco = dsa.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdseguridad..tbusuariosweb u WITH(NOLOCK) on u.lgn_usro = rtrim(ss.usro_ultma_mdfccn) ;

	
	Update     #DatosSolicitudAutorizacion
	Set        dscrpcn_tpo_ubccn_pcnte = LTRIM(RTRIM(tup.dscrpcn_tpo_ubccn_pcnte))
	From       #DatosSolicitudAutorizacion cds
	INNER JOIN bdSisalud.dbo.tbPMTipoUbicacionPaciente_Vigencias tup WITH(NOLOCK) 
	ON         tup.cnsctvo_cdgo_tpo_ubccn_pcnte = cds.cnsctvo_cdgo_tpo_ubccn_pcnte
	Where      @ldfcha_consulta BETWEEN tup.inco_vgnca AND tup.fn_vgnca
	
	Update     #DatosSolicitudAutorizacion
	Set        dscrpcn_prrdd_atncn	= pa.dscrpcn_prrdd_atncn
	From       #DatosSolicitudAutorizacion cds
	INNER JOIN bdSisalud.dbo.tbPMPrioridadAtencion_Vigencias pa	WITH(NOLOCK)   	  
	ON         pa.cnsctvo_cdgo_prrdd_atncn = cds.cnsctvo_cdgo_prrdd_atncn
	WHERE      @ldfcha_consulta  BETWEEN pa.inco_vgnca  AND pa.fn_vgnca; 


	SELECT
		cnsctvo_slctd_atrzcn_srvco,	fcha_crcn              , fcha_slctd     , nmro_slctd_atrzcn_ss,	dscrpcn_tpo_ubccn_pcnte,	
		dscrpcn_prrdd_atncn       ,	adtr_slctnte           , cdgo_mdo_cntcto, dscrpcn_orgn_atncn  ,	dscrpcn_clse_atncn     ,
		jstfccn_clnca             , cnsctvo_cdgo_frma_atncn, spra_tpe_st    , dscrpcn_rcbro       , nmro_slctd_prvdr
	FROM #DatosSolicitudAutorizacion cds


	DROP TABLE #DatosSolicitudAutorizacion

END
GO
