USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios]    Script Date: 10/02/2017 4:04:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spConsultarPrestadoresParaDireccionamientoXcriterios') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spConsultarPrestadoresParaDireccionamientoXcriterios AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spConsultarPrestadoresParaDireccionamientoXcriterios
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS						 A\>
* Descripcion			: <\D 
								SP creado para guardar el registro de la Reasignación de las prestaciones
								entre auditores. Registra las observaciones y el tipo de auditoria													
                          D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2017/03/08 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios] 30443,1, 8112, null, null, null
--exec [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios] 30443,1, 8112, '91600', null, null
--exec [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios] 30443,1, 8112, null, '890300517', null
--exec [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios] 30443,1, 8112, null, null, 'CLINICA BASILIA S.A. - CALLE 5E NO. 42A -53'
AlTER PROCEDURE [gsa].[spConsultarPrestadoresParaDireccionamientoXcriterios] 
/*01*/		@cnsctvo_cdfccn			UdtConsecutivo,
/*02*/		@cnsctvo_cdgo_pln		UdtConsecutivo,
/*03*/		@cnsctvo_cdgo_cdd	    UdtConsecutivo,
			@cdgo_intrno			UdtCodigoIps = null,
			@nit					udtNumeroIdentificacionLargo = null, 
			@scrsl					udtDescripcion = null
AS

BEGIN
	SET NOCOUNT ON;

		Declare @fechaactual			datetime = getdate(),
				@cdgo_intrno_gnrco		UdtCodigoIPS = '5999',
				@cnsctvo_cdgo_sde		udtConsecutivo,
				@lcSentenceSql           Varchar(2000) = NULL,
				@lcOrderSql              Varchar(2000) = NULL,
				@lcWhereSql              Varchar(2000) = NULL 


		Create Table #tbTmpPrestadoresDireccionamiento
		(
		cdgo_intrno					UdtCodigoIPS,
		nmbre_scrsl					Varchar(250),	
		drccn						UdtDireccion,
		vlr_trfdo					Float,
		rzn_scl						varchar(200),
		cdgo_tpo_idntfccn			char(3),
		dscrpcn_tpo_idntfccn		varchar(150),
		nmro_idntfccn				UdtNumeroIdentificacionLargo
	
		)

		Insert into #tbTmpPrestadoresDireccionamiento(					
					cdgo_intrno				,		
					nmbre_scrsl				,
					drccn					,	
					vlr_trfdo				,
					rzn_scl					,
					cdgo_tpo_idntfccn		,
					dscrpcn_tpo_idntfccn,
					nmro_idntfccn)
		exec bdsisalud.dbo.spConsultarPrestadoresParaDireccionamiento @cnsctvo_cdfccn, @cnsctvo_cdgo_pln, @cnsctvo_cdgo_cdd;

		-- mostrar resultado consulta
		SET @lcSentenceSql =
							'Select 	distinct cdgo_intrno	,		
										nmbre_scrsl				,
										drccn					,	
										vlr_trfdo				,
										rzn_scl					,
										cdgo_tpo_idntfccn		,
										dscrpcn_tpo_idntfccn,
										nmro_idntfccn
							From		#tbTmpPrestadoresDireccionamiento p
							where 1=1 ';
		

			IF (@cdgo_intrno != '' OR @cdgo_intrno IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND p.cdgo_intrno = ''' + 
					@cdgo_intrno + '''' + CHAR(13) 
			END

			IF (@nit != '' OR @nit IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND p.nmro_idntfccn = ''' + 
					@nit + '''' + CHAR(13) 
			END

			IF (@scrsl != '' OR @scrsl IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND p.nmbre_scrsl = ''' + 
					@scrsl + '''' + CHAR(13) 
			END
		Set @lcOrderSql = 'Order By	vlr_trfdo'
		SET @lcSentenceSql = @lcSentenceSql + ISNULL(@lcWhereSql,'') + @lcOrderSql
							   
							

		EXEC(@lcSentenceSql)

		drop table #tbTmpPrestadoresDireccionamiento
END
