Use bdCNA
Go

If(Object_id('gsa.spASObtenerDatosAnulacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASObtenerDatosAnulacion As Select 1';
End

Go

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASObtenerDatosAnulacion
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento que recupera la informacion del xml D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 02/10/2017  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

Alter Procedure gsa.spASObtenerDatosAnulacion
	@dts_anlcn_xml		XML
As 
Begin
	Set Nocount On
	
	Insert Into #tempInformacionSolicitud (
		cnsctvo_slctd_atrzcn_srvco,		
		cnsctvo_cdgo_mtvo_csa,
		obsrvcn,						
		usro_anlcn,
		orgn_mdfccn
	)
	SELECT	Pref.value('(numSolicitud/text())[1]', 'udtConsecutivo'),
			Pref.value('(causal/text())[1]', 'udtConsecutivo'),
			Pref.value('(observacion/text())[1]', 'udtObservacion'),
			Pref.value('(usuario/text())[1]', 'udtUsuario'),
			Pref.value('(origenModificacion/text())[1]', 'udtDescripcion')
	FROM @dts_anlcn_xml.nodes('(/anular/solicitud)') AS xml_slctd (Pref);


	Insert Into #tempInformacionServicios (
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo
	)
	SELECT	Pref.value('(numSolicitud/text())[1]', 'udtConsecutivo'),
			Pref.value('(consecutivo/text())[1]', 'udtConsecutivo')
	FROM @dts_anlcn_xml.nodes('(/anular/prestaciones/prestacion)') AS xml_slctd (Pref);

End