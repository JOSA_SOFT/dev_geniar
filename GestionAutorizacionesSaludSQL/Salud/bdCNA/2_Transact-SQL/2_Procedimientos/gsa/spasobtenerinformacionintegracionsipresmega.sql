USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spasobtenerinformacionintegracionsipresmega]    Script Date: 8/9/2017 8:34:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASObtenerInformacionIntegracionSipresMEGA
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento encargado de extraer la informaci�n necesaria del sistema  D\>    
					   <\D de MEGA para realizar la replicaci�n al sistema destino de Sipres		D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P @fcha_inco: Fecha de incicio de busqueda de informaci�n.				P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jonathan Chamizo Quina AM\>    
* Descripcion        : <\D Se adiciona script para validar si existe el SP en la BD. En caso contrario
					 :     crea con la instrucci�n As Select 1										     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-09-20  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento para que no tome las solicitudes y prestaciones
                           con estado 0 y adicionalmente se agrega el campo de estado en la tabla de lo 
						   conceptos							     
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-19  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento para adicionar en la tabla temporal de solicitudes el estado de la atencion, con el estado del concepto o 
                           el estado de la solicitud cuando no se tengan conceptos de gasto asociados
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                           Se modifica el procedimiento quitando el calculo del valor de referencia para que se tome de la tabla de servicios y se inserte
                           el valor del concepto.
						   Se actualiza el numero unico del medico cuando es no adscrito
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D 
                           Se agrega un Update a la tabla temporal #tmpinformacionservicios para el caso 
						   donde el campo vlr_rfrnca es nulo, actualiza el valor a 0
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-11-17  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos V�squez Garc�a - sisjvg01 AM\>    
* Descripcion        : <\D Se modifica el procedimiento para agregar el atributo cnsctvo_cdgo_tpo_orgn_slctd - para el manejo de la nueva funcionalidad
						   de Envio de OPS Virtual correspondiente a solicitudes creadas por proceso autom�tico de programaci�n de entrega 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega Join en el llenado de la tabla temporal #tmpinformacionconceptos para sacar el nmro_unco_idntfccn_prstdr y el cdgo_intrno
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-18  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agrega condicional en la carga de datos de los conceptos para que no 
						   se carguen los conceptos de las prestaciones en estado aprobado en mega,
						   validacion hecha para caso donde quedan conceptos con valor 0
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-25  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se modifica el proceso de agrupacion para que agrupe segun parametros, se agregan tablas temporales para este proceso
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-27  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se modifica el proceso de agrupacion Segun observaciones realizadas, se realiza proceso para que cada una de las prestaciones
						   con agrupador 4 o 5 tengan nuam diferentes y no se agrupen con un unico nuam por solicitud.
						   Se agrega carga de la informacion de la empresa en la tabla temporal #tempInformacionEmpresasxAfiliado.
					   D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-30  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agregan condicionales a consultas para no cargar los datos de las solicitudes en estado anulado 
						   ni las solicitudes en estado devuelto.
					   D\>
* Nuevas Variables   : <\VM 1. @cnsctvo_cdgo_estdo_dvlto_mga
						    2. @cnsctvo_cdgo_estdo_anldo_mga VM\>    
* Fecha Modificacion : <\FM  2017-02-01  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se modifica la condicion de carga de solicitudes en estado anulado, se correge proceso para 
						   cargar las solicitudes anuladas que ya se encuentren en sipres D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-03 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agrega cruce faltante con la tabla tbASInformacionAfiliadoSolicitudAutorizacionServicios 
						   ya que la informacion que cruza con la tabla tbAsInformacionEmpresasxAfiliado se obtiene con
						   el campo cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-08 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agrega proceso para recuperar los consecutivos de las solicitudes que pasan a sipres en estado Ingresado.
						   Se agrega Update para actualizar los estados a migrar a sipres en Ingresado cuando existe almenos una prestacion
						   que migre en estado Ingresado para una solicitud.
						   Se retira condicion de no carga de solicitudes en estado devuelto, y se agrega delete a las prestaciones con
						   este estado que no se hallan migrado con anterioridad.
						D\>

* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-09 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega cruce con la tabla tbASServiciosSolicitados para cargar algunos casos de solicitudes que no cargaban
						   debido que el estado y la fecha de ultima modificacion en la tabla tbAsSolicitudesAutorizacionServicios solo se 
						   modifican cuando todas las prestaciones cambian a un mismo estado.
						   Se cambia orden de ejecucion de las instrucciones de eliminacion de las prestaciones y solicitudes en estados
						   Anulado, Devuelta y No Autorizado que no han sido migradas a Sipres con anterioridad.
						   Se cambia Orden de ejecucuion de condicion de cambio de estado en las tablas temporales de las prestaciones 
						   con solicitudes no liquidadas.
						D\>

* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-10 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega delete a la tabla temporal de conceptos para los conceptos de las prestaciones que
						   migraran a Sipres en estado ingresado.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-20 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agregan tablas temporales #tempPrestacionesAgrupadasDiferenteEstado y #tempAgrupadoresDiferenteEstado
						   La temporal #tempPrestacionesAgrupadasDiferenteEstado se utiliza para obtener el id agrupador, el consecutivo de la solicitud y 
						   el estado de las prestaciones.
						   La tabla temporal #tempAgrupadoresDiferenteEstado se utiliza para obtener las los id agrupador que tiene mas de una 
						   prestacion con diferente estado.
						   Se cambia la asignacion de estados para poner el estado de la prestaciones a las atenciones que tengan todas 
						   las prestaciones con el mismo estado.
						   Se agrega Update para poner el estado de la atencion en Ingresado cuando una atencion tenga dos o mas 
						   prestaciones con diferente estado.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-02-20 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega Update a tabla temporal #tmpinformacionservicios para actualizar la el campo cnsctvo_cdgo_ltrldd con el valor 4 
						   cuando dicho campo tenga el valor 0
						D\>
* Nuevas Variables   : <\VM @cnsctvo_cdgo_ltrldd4 VM\>    
* Fecha Modificacion : <\FM 2017-03-09 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se modifica la carga de informacion en el campo hra_slctd_orgn de la temporal #tmpinformacionsolicitud
						   de forma que cargue la hora sin segundos.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-05 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega sentencia para obtener el cnsctvo_prcdmto_insmo_slctdo de tbASProcedimientosInsumosSolicitados
						   para CUOS y PIS
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-07 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se deja en los Join los condicionales que corresponden a referencias entre tablas, las demas condiciones se
						   dejan en los Where.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-17 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se realizan mejoras en el procedimiento para reducir el tiempo de ejecucion.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-17 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega condicional para evitar la migracion de estados de las solicitudes usadas.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-05-31 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se pone condicional para no permitir la creacion de la tabla temporal #tmpsolicitudes
						   cuando la migracion es a pedido, para este caso la fecha de migracion sera null.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-06-06 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se agrega sentencias para evitar la migracion de prestaciones con agrupador 4, servicios diagnosticos especializados.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-07-11 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se realizan cambios en el proceso de agrupacion de prestaciones por solicitud.
						   Se modifica proceso para no calcular la agrupacion de prestacion a las que ya se les haya calculado el nuam.
						   Se agrega llamado a nuevo procedimiento de agrupacion.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-07-21 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se realizan ajuste para recuperar el campo cnsctvo_cdgo_tpo_orgn_slctd en la 
						   temporal de agrupacion.
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-07-27 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
						   Se realiza ajuste para que las prestaciones no migradas con anterioridad se les asigne el mismo nuam y
						   oficina de las prestaciones ya migradas con el mismo numero de OPS
						D\>
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-08-08 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/


ALTER Procedure [gsa].[spasobtenerinformacionintegracionsipresmega]

				@fcha_inco							DATETIME

As

Begin
		Set NoCount On

		Declare @cnsctvo_mdcmnto					Int,
				@cnsctvo_prstcn						Int,
				@cnsctvo_cuos						Int,
				@cnsctvo_dgnstco_prncpl				Int,
				@ttl								Int,
				@ind1								Int,
				@ind2								Int,
				@cntdd_agrpdr						Int,
				@slctd								Int,
				@cntdd_mxma_prstcn_ops_sprs			Int,
				@afrmcn								CHAR(1),
				@fcha_actl							DATETIME,
				@cnsctvo_tpo_undd_ans				Int,
				@cnsctvo_cdgo_estdo_aprbdo_mga		UdtConsecutivo,
				@cnsctvo_cdgo_estdo_dvlto_mga		UdtConsecutivo,
				@cnsctvo_cdgo_estdo_anldo_mga		UdtConsecutivo,
				@cnsctvo_cdgo_estdo_prcsdo_mga		UdtConsecutivo,
				@cnsctvo_cdgo_estdo_usdo_mga		UdtConsecutivo,
				@cnsctvo_cdgo_estdo_no_atrzdo_mga	UdtConsecutivo,
				@vlr_cro							Int,
				@cnsctvo_cdgo_agrpdr_prstcn			UdtConsecutivo,
				@vlr_ds								Int,
				@cnsctvo_cdgo_estdo_dvlta			UdtConsecutivo,
				@cnsctvo_cdgo_agrpdr_prstcn4		UdtConsecutivo,
				@cnsctvo_cdgo_agrpdr_prstcn5		UdtConsecutivo,
				@id_srvco							Int,
				@cnsctvo_cdgo_estdo_ingrsdo_sprs	UdtConsecutivo,
				@vlr_uno							Int,
				@cnsctvo_cdgo_ltrldd4				UdtConsecutivo,
				@cnsctvo_cdgo_tpo_orgn_slctd		udtConsecutivo;



		Create Table #tempInformacionSolicitudAux
		(
			id                           udtConsecutivo IDENTITY (1, 1),
			cnsctvo_slctd_atrzcn_srvco   udtConsecutivo,
			cnsctvo_cdgo_ofcna           udtConsecutivo,
			nuam                         Numeric(18, 0),
			fcha_slctd                   Datetime      ,
			cdgo_intrno                  Char(8)       ,
			nmro_unco_idntfccn_prstdr    Int           ,
			nmro_unco_idntfccn_mdco      Int           ,
			cnsctvo_cdgo_cntngnca        udtConsecutivo,
			fcha_estmda_entrga           Datetime      ,
			cnsctvo_cdgo_clse_atncn      udtConsecutivo,
			cnsctvo_cdgo_frma_atncn      udtConsecutivo,
			obsrvcn_adcnl                Varchar(250)  ,
			fcha_crcn                    Datetime      ,
			usro_crcn                    udtusuario    ,
			usro_ultma_mdfccn            udtusuario    , 
			fcha_ultma_mdfccn            Datetime      ,
			cnsctvo_cdgo_dgnstco         udtConsecutivo,
			cnsctvo_cdgo_ofcna_atrzcn    udtConsecutivo,
			cnsctvo_cdgo_rcbro           udtConsecutivo,
			cnsctvo_cdgo_tpo_ubccn_pcnte udtConsecutivo,
			nmro_slctd_prvdr             Int           ,
			cnsctvo_cdgo_srvco_hsptlzcn  udtConsecutivo,
			cnsctvo_cdgo_mrbldd          udtConsecutivo,
			cnsctvo_cdgo_tpo_atncn       udtConsecutivo,
			cnsctvo_cdgo_estdo_mega      udtConsecutivo,
			cnsctvo_cdgo_estdo           udtConsecutivo,
			hra_dgta                     Char(10)      ,
			nmro_atncn                   Int           ,
			hra_slctd_orgn               Char(8)       ,
			cnsctvo_cdgo_mdo_cntcto      udtConsecutivo,
			fcha_imprsn                  Datetime      ,
			tpo_prstcn                   Char(1) DEFAULT 'N',
			cnsctvo_cdgo_grpo_entrga     udtConsecutivo,
			nmro_vsts                    Int           ,
			fcha_ingrso_hsptlzcn		 DateTime	   ,
			fcha_egrso_hsptlzcn			 DateTime	   ,
			ds_estnca                    Int Default 0 ,
			crte_cnta					 udtConsecutivo Default 0,
			cnsctvo_cdgo_clse_hbtcn		 udtConsecutivo,
			cnsctvo_cdgo_estdo_atncn     udtConsecutivo,
			cnsctvo_atncn_ops			 UdtConsecutivo,
			cnsctvo_cdgo_tpo_orgn_slctd  UdtConsecutivo,
			cnsctvo_cdgo_agrpdr_prstcn	 UdtConsecutivo
		)

		Create Table #tempServiciosXSolicitud(
			cnsctvo_slctd_atrzcn_srvco UdtConsecutivo,
			cntdd Int
		)

		Create table #tempRelSolicitudServicios
		(
			id							Int Identity(1, 1),
			id_slctd					Int,
			id_srvco					Int,
			cnsctvo_slctd_atrzcn_srvco	UdtConsecutivo,
			cnsctvo_cdgo_agrpdr_prstcn	UdtConsecutivo
		)
				
		Create Table #tmpsolicitudagrupador
		(
			id Int IDENTITY (1, 1),
			cnsctvo_slctd_atrzcn_srvco UdtConsecutivo,
			cnsctvo_cdgo_agrpdr_prstcn UdtConsecutivo,
			cntdd		Int
		)

		If(@fcha_inco Is not Null)
		Begin
			Create Table #tmpsolicitudes 
			(
				cnsctvo_slctd_atrzcn_srvco	UdtConsecutivo,
				cnsctvo_srvco_slctdo		UdtConsecutivo
			)
		End

		Create Table #tempPrestacionesAgrupadasDiferenteEstado
		(
			id_agrpdr						Int,
			cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
			cnsctvo_cdgo_estdo_mga			UdtConsecutivo
		)

		Create Table #tempAgrupadoresDiferenteEstado
		(
			id_agrpdr						Int,
			cnsctvo_cdgo_estdo				UdtConsecutivo			
		)

		Set @cnsctvo_mdcmnto = 5
		Set @cnsctvo_prstcn = 4
		Set @cnsctvo_cuos = 9;
		Set @cnsctvo_dgnstco_prncpl = 1
		Set @cntdd_mxma_prstcn_ops_sprs = 5
		Set @afrmcn = 'S'
		Set @fcha_actl = GETDATE()
		Set @cnsctvo_tpo_undd_ans = 4
		Set @cnsctvo_cdgo_estdo_aprbdo_mga = 7;
		Set @vlr_cro = 0;
		Set @vlr_ds = 2;
		Set @cnsctvo_cdgo_estdo_dvlta = 6;
		Set @cnsctvo_cdgo_agrpdr_prstcn4 = 4;
		Set @cnsctvo_cdgo_agrpdr_prstcn5 = 5;
		Set @cnsctvo_cdgo_estdo_dvlto_mga = 6;
		Set @cnsctvo_cdgo_estdo_anldo_mga = 14;
		Set @cnsctvo_cdgo_estdo_prcsdo_mga = 13;
		Set @cnsctvo_cdgo_estdo_usdo_mga = 16;
		Set @cnsctvo_cdgo_estdo_no_atrzdo_mga = 8;
		Set @cnsctvo_cdgo_estdo_ingrsdo_sprs = 1;
		Set @vlr_uno = 1
		Set @cnsctvo_cdgo_ltrldd4 = 4;
		Set @cnsctvo_cdgo_tpo_orgn_slctd = 3;
	


		--Se buscan las solicitudes que hayan sido modificadas, seg�n la tabla de solicitudes y conceptos, desde el d�a anterior a la hora
		--parametrizada
		if(@fcha_inco Is Not Null)
		Begin

			Insert Into #tmpsolicitudes (cnsctvo_slctd_atrzcn_srvco)
			Select     SSO.cnsctvo_slctd_atrzcn_srvco
			From       bdcna.gsa.tbasconceptosserviciosolicitado CCS With(NoLock)
			Inner Join bdcna.gsa.tbasmedicamentossolicitados MSO With(NoLock)
			On         MSO.cnsctvo_mdcmnto_slctdo = CCS.cnsctvo_mdcmnto_slctdo
			Inner Join bdcna.gsa.tbASServiciosSolicitados SSO With(NoLock)
			On         SSO.cnsctvo_srvco_slctdo = MSO.cnsctvo_srvco_slctdo
			Where      CCS.fcha_ultma_mdfccn >= @fcha_inco
			Union
			Select     SSO.cnsctvo_slctd_atrzcn_srvco
			From       bdcna.gsa.tbasconceptosserviciosolicitado CCS With(NoLock)
			Inner Join bdcna.gsa.tbasprocedimientosinsumossolicitados PIS With(NoLock)
			On         PIS.cnsctvo_prcdmnto_insmo_slctdo = CCS.cnsctvo_prcdmnto_insmo_slctdo
			Inner Join bdcna.gsa.tbASServiciosSolicitados SSO With(NoLock)
			On         sso.cnsctvo_srvco_slctdo = PIS.cnsctvo_srvco_slctdo
			Where      ccs.fcha_ultma_mdfccn >= @fcha_inco
			Union
			Select		ss.cnsctvo_slctd_atrzcn_srvco
			From		bdCNA.gsa.tbASServiciosSolicitados ss
			Where		ss.fcha_ultma_mdfccn >= @fcha_inco
						
		End
			
		--Se carga la informaci�n del encabezado de la solicitud
		Insert 
		Into   #tmpinformacionsolicitud 
		(					
					cnsctvo_slctd_atrzcn_srvco , fcha_slctd                  , cdgo_intrno              ,
					nmro_unco_idntfccn_prstdr  , nmro_unco_idntfccn_mdco     , cnsctvo_cdgo_clse_atncn  , 
					cnsctvo_cdgo_frma_atncn    , obsrvcn_adcnl				 , fcha_crcn                , 
					usro_crcn                  , usro_ultma_mdfccn			 , fcha_ultma_mdfccn        , 
					cnsctvo_cdgo_ofcna_atrzcn  , cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         ,
					cnsctvo_cdgo_srvco_hsptlzcn, cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  ,
					hra_slctd_orgn             , nmro_vsts                   , ds_estnca                ,
					cnsctvo_cdgo_tpo_orgn_slctd, fcha_ingrso_hsptlzcn		 , fcha_egrso_hsptlzcn		,
					crte_cnta				   , cnsctvo_cdgo_clse_hbtcn
		)
		Select Distinct 
					TSA.cnsctvo_slctd_atrzcn_srvco , TSA.fcha_slctd                  , TIS.cdgo_intrno                  ,
					TIS.nmro_unco_idntfccn_prstdr  , TMT.nmro_unco_idntfccn_mdco     , TSA.cnsctvo_cdgo_clse_atncn      , 
					TSA.cnsctvo_cdgo_frma_atncn    , TSA.obsrvcn_adcnl               , TSA.fcha_crcn                    , 
					TSA.usro_crcn                  , TSA.usro_ultma_mdfccn           , TSA.fcha_ultma_mdfccn            , 
					TSA.cnsctvo_cdgo_ofcna_atrzcn  , TSA.cnsctvo_cdgo_tpo_ubccn_pcnte, TSA.nmro_slctd_prvdr             ,
					TIH.cnsctvo_cdgo_srvco_hsptlzcn, TSA.cnsctvo_cdgo_estdo_slctd    , TSA.cnsctvo_cdgo_mdo_cntcto_slctd,
					Concat(Convert(NVARCHAR(2), DATEPART(hh, TSA.fcha_slctd)), ':', CONVERT(NVARCHAR(2), DATEPART(mi, TSA.fcha_slctd))),
					TIH.nmro_vsts                  , ISNULL(TIH.ds_estnca, 0),
					TSA.cnsctvo_cdgo_tpo_orgn_slctd, tih.fcha_ingrso_hsptlzcn		 , tih.fcha_egrso_hsptlzcn			,
					isNull(tih.crte_cnta, 0)	   , tih.cnsctvo_cdgo_clse_hbtcn
		From		bdcna.gsa.tbassolicitudesautorizacionservicios TSA With(NoLock)
		Inner Join	#tmpsolicitudes TSO
		On			TSO.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdcna.gsa.tbasipssolicitudesautorizacionservicios TIS With(NoLock)
		On			TIS.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco		
		Inner Join	bdcna.gsa.tbasmedicotratantesolicitudautorizacionservicios TMT With(NoLock)
		On			TMT.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco
		Left Join	bdcna.gsa.tbasinformacionhospitalariasolicitudautorizacionservicios TIH With(NoLock)
		On			TIH.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco
		Where		TSA.cnsctvo_cdgo_estdo_slctd != @vlr_cro
		And			TSA.cnsctvo_cdgo_estdo_slctd != @cnsctvo_cdgo_estdo_prcsdo_mga

		-- 
		Update		iso
		Set			cnsctvo_cdgo_cntngnca = tds.cnsctvo_cdgo_cntngnca,
					cnsctvo_cdgo_dgnstco = tds.cnsctvo_cdgo_dgnstco,
					cnsctvo_cdgo_rcbro = tds.cnsctvo_cdgo_rcbro
		From		#tmpinformacionsolicitud iso
		Inner Join	bdcna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios tds With(NoLock)
		On			iso.cnsctvo_slctd_atrzcn_srvco = tds.cnsctvo_slctd_atrzcn_srvco
		Inner Join  bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias tdsv With(NoLock)
		On			tdsv.cnsctvo_dgnstco_slctd_atrzcn_srvco = tds.cnsctvo_dgnstco_slctd_atrzcn_srvco
		Where		TDS.cnsctvo_cdgo_tpo_dgnstco = @cnsctvo_dgnstco_prncpl
		And			@fcha_actl Between tdsv.inco_vgnca And tdsv.fn_vgnca

		---Se actualiza el numero unico del medico cuando es no adscrito
		Update    #tmpinformacionsolicitud
		Set       nmro_unco_idntfccn_mdco = 100961
		Where     nmro_unco_idntfccn_mdco = @vlr_cro


		--Se actualiza la fecha estimada de entrega
		Update    tis
		Set       fcha_estmda_entrga       = ISNULL(rsf.fcha_estmda_entrga, ''),
				  cnsctvo_cdgo_grpo_entrga = rsf.cnsctvo_cdgo_grpo_entrga
		From      #tmpinformacionsolicitud TIS
		Left Join bdcna.gsa.tbAsResultadoFechaEntrega RSF With(NoLock)
		On        RSF.cnsctvo_slctd_atrzcn_srvco = TIS.cnsctvo_slctd_atrzcn_srvco

		--Informaci�n del afiliado.
		Insert 
		Into       #tmpinformacionafiliado (cnsctvo_slctd_atrzcn_srvco, nmro_unco_idntfccn_afldo       , cnsctvo_cdgo_pln             ,
											cnsctvo_cdgo_sde_ips_prmra, cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo          ,
											cnsctvo_bnfcro_cntrto     , cnsctvo_cdgo_sxo               , edd_afldo_ans                ,
											smns_ctzds                , cnsctvo_cdgo_tpo_vnclcn_afldo  , cnsctvo_cdgo_rngo_slrl       ,
											fcha_ultma_mdfccn         , cdgo_ips_prmra                 , cnsctvo_cdgo_cdd_rsdnca_afldo,
											cnsctvo_cdgo_tpo_cntrto   , nmro_cntrto                    , cnsctvo_cdgo_tpo_undd
											)
		Select      TSA.cnsctvo_slctd_atrzcn_srvco, TAS.nmro_unco_idntfccn_afldo       , TAS.cnsctvo_cdgo_pln             ,
					TAS.cnsctvo_cdgo_sde_ips_prmra, TAS.cnsctvo_cdgo_tpo_idntfccn_afldo, TAS.nmro_idntfccn_afldo          ,
					TAS.cnsctvo_bnfcro_cntrto     , TAS.cnsctvo_cdgo_sxo               , TAS.edd_afldo_ans                ,
					TAS.smns_ctzds                , TAS.cnsctvo_cdgo_tpo_vnclcn_afldo  , TAS.cnsctvo_cdgo_rngo_slrl       ,
					TAS.fcha_ultma_mdfccn         , TAS.cdgo_ips_prmra                 , TAS.cnsctvo_cdgo_cdd_rsdnca_afldo,
					TAS.cnsctvo_cdgo_tpo_cntrto   , TAS.nmro_cntrto                    , @cnsctvo_tpo_undd_ans
		From		#tmpinformacionsolicitud TSA With(NoLock)
		Inner Join	bdcna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios TAS With(NoLock)
		On			TAS.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco
		Group By	TSA.cnsctvo_slctd_atrzcn_srvco, TAS.nmro_unco_idntfccn_afldo       , TAS.cnsctvo_cdgo_pln             ,
					TAS.cnsctvo_cdgo_sde_ips_prmra, TAS.cnsctvo_cdgo_tpo_idntfccn_afldo, TAS.nmro_idntfccn_afldo          ,
					TAS.cnsctvo_bnfcro_cntrto     , TAS.cnsctvo_cdgo_sxo               , TAS.edd_afldo_ans                ,
					TAS.smns_ctzds                , TAS.cnsctvo_cdgo_tpo_vnclcn_afldo  , TAS.cnsctvo_cdgo_rngo_slrl       ,
					TAS.fcha_ultma_mdfccn         , TAS.cdgo_ips_prmra                 , TAS.cnsctvo_cdgo_cdd_rsdnca_afldo,
					TAS.cnsctvo_cdgo_tpo_cntrto   , TAS.nmro_cntrto
				

		--Informaci�n de servicios
		Insert 
		Into       #tmpinformacionservicios (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo  , fcha_imprsn                      ,
											cnsctvo_cdgo_srvco_slctdo , cntdd_slctda           , fcha_ultma_mdfccn                 ,
											usro_ultma_mdfccn         , prstcn_cptda           , cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
											cnsctvo_cdgo_tpo_srvco    , cnsctvo_cdgo_estdo_mega, id_agrpdr                         ,
											vlr_rfrnca				  , cnsctvo_cdgo_rcbro
											)
		Select     TSA.cnsctvo_slctd_atrzcn_srvco, SSO.cnsctvo_srvco_slctdo           , SSO.fcha_imprsn                       ,
					SSO.cnsctvo_cdgo_srvco_slctdo , SSO.cntdd_atrzda                   , SSO.fcha_ultma_mdfccn                 ,
					SSO.usro_ultma_mdfccn         , SSO.prstcn_cptda                   , SSO.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
					SSO.cnsctvo_cdgo_tpo_srvco    , SSO.cnsctvo_cdgo_estdo_srvco_slctdo, TSA.id                                ,
					SSO.vlr_lqdcn_srvco			  , SSO.cnsctvo_cdgo_rcbro
		From		#tmpinformacionsolicitud TSA With(NoLock)
		Inner Join	bdcna.gsa.tbASServiciosSolicitados SSO With(NoLock)
		On			SSO.cnsctvo_slctd_atrzcn_srvco = TSA.cnsctvo_slctd_atrzcn_srvco
		Where		SSO.cnsctvo_cdgo_estdo_srvco_slctdo != @vlr_cro 
		And			SSO.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_prcsdo_mga -- Se agrega temporalmente mientras se define la impresion de las solicitudes migradas a Mega.
		And			SSO.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_usdo_mga 
		Group By	TSA.cnsctvo_slctd_atrzcn_srvco, SSO.cnsctvo_srvco_slctdo           , SSO.fcha_imprsn                       ,
					SSO.cnsctvo_cdgo_srvco_slctdo , SSO.cntdd_atrzda                   , SSO.fcha_ultma_mdfccn                 ,
					SSO.usro_ultma_mdfccn         , SSO.prstcn_cptda                   , SSO.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
					SSO.cnsctvo_cdgo_tpo_srvco    , SSO.cnsctvo_cdgo_estdo_srvco_slctdo, TSA.id                                ,
					SSO.vlr_lqdcn_srvco			  , SSO.cnsctvo_cdgo_rcbro

		-- Si la cantidad autorizada es 0, se trae la cantidad solicitada.
		Update		iss
		Set			cntdd_slctda = ss.cntdd_slctda
		From		#tmpinformacionservicios iss 
		Inner Join	bdcna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			iss.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
		And			iss.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		And			iss.cnsctvo_cdgo_srvco_slctdo = ss.cnsctvo_cdgo_srvco_slctdo
		Where		iss.cntdd_slctda = @vlr_cro

		-- Se obtiene el agrupador
		Update     ise
		Set        cnsctvo_cdgo_agrpdr_prstcn = CSP.cnsctvo_agrpdr_prstcn
		From       #tmpinformacionservicios ISE
		Inner Join bdsisalud.dbo.tbCupsServicios CSP With (NOLOCK)
		On         ISE.cnsctvo_cdgo_srvco_slctdo = CSP.cnsctvo_prstcn
		Where      @fcha_actl BETWEEN CSP.inco_vgnca AND CSP.fn_vgnca
		And		   ISE.cnsctvo_cdgo_tpo_srvco = @cnsctvo_prstcn --CUPS	

		-- Se pone agrupador 0 a los medicamentos
		Update	#tmpinformacionservicios
		Set		cnsctvo_cdgo_agrpdr_prstcn = @vlr_cro
		Where	cnsctvo_cdgo_agrpdr_prstcn Is Null

		--Actualiza el campo vlr_rfrnca en caso que sea nulo
		Update	#tmpinformacionservicios
		Set		vlr_rfrnca = @vlr_cro
		Where	vlr_rfrnca is null

		--Actializaci�n de informaci�n de prestaciones
		Update		tis
		Set			cnsctvo_cdgo_prsntcn   = mso.cnsctvo_cdgo_prsntcn,
					cnsctvo_mdcmnto_slctdo = mso.cnsctvo_mdcmnto_slctdo
		From		#tmpinformacionservicios TIS
		Inner Join	bdcna.gsa.tbASMedicamentosSolicitados MSO With(NoLock)
		On			MSO.cnsctvo_srvco_slctdo = TIS.cnsctvo_srvco_slctdo
		Where		TIS.cnsctvo_cdgo_tpo_srvco = @cnsctvo_mdcmnto --Medicamentos

		Update		tis
		Set			cnsctvo_cdgo_va_accso   = pso.cnsctvo_cdgo_va_accso,
					cnsctvo_prcdmnto_slctdo = pso.cnsctvo_prcdmnto_insmo_slctdo,
					cnsctvo_cdgo_ltrldd     = pso.cnsctvo_cdgo_ltrldd
		From		#tmpinformacionservicios TIS
		Inner Join	bdcna.gsa.tbASProcedimientosInsumosSolicitados PSO With(NoLock)
		On			PSO.cnsctvo_srvco_slctdo = TIS.cnsctvo_srvco_slctdo
		Where		TIS.cnsctvo_cdgo_tpo_srvco = @cnsctvo_prstcn --Procedimientos

		-- 
		Update		tis
		Set			cnsctvo_cdgo_va_accso   = pso.cnsctvo_cdgo_va_accso,
					cnsctvo_prcdmnto_slctdo = pso.cnsctvo_prcdmnto_insmo_slctdo,
					cnsctvo_cdgo_ltrldd     = pso.cnsctvo_cdgo_ltrldd
		From		#tmpinformacionservicios TIS
		Inner Join	bdcna.gsa.tbASProcedimientosInsumosSolicitados PSO With(NoLock)
		On			PSO.cnsctvo_srvco_slctdo = TIS.cnsctvo_srvco_slctdo
		Where		TIS.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cuos -- CUOS y PIS

		-- 
		Update		#tmpinformacionservicios
		Set			cnsctvo_cdgo_ltrldd = @cnsctvo_cdgo_ltrldd4
		Where		cnsctvo_cdgo_ltrldd = @vlr_cro

		--cnsctvo_cdgo_estdo solicitud
		Update     iso
		Set        cnsctvo_cdgo_estdo = esv.cnsctvo_cdgo_estdo_hmlgdo_sprs
		From       #tmpinformacionsolicitud ISO
		Inner Join bdcna.prm.tbASEstadosServiciosSolicitados_Vigencias ESV With(NoLock)
		On         ISO.cnsctvo_cdgo_estdo_mega = ESV.cnsctvo_cdgo_estdo_srvco_slctdo
		Where      @fcha_actl Between ESV.inco_vgnca And ESV.fn_vgnca

		-- Se borra la informacion para reutilizar la tabla temporal.
		Delete From #tmpsolicitudes

		
		-- Se carga la informacion de la empresa
		Insert Into #tempInformacionEmpresasxAfiliado
		(
					cnsctvo_cdgo_tpo_idntfccn_empldr,			nmro_idntfccn_empldr,					nmbre_empldr,
					cnsctvo_scrsl,								cnsctvo_cdgo_ofcna,						inco_vgnca_cbrnza,
					fn_vgnca_cbrnza,							slro_bse,								cnsctvo_cdgo_tpo_cbrnza,
					cnsctvo_cdgo_entdd_arp,						fcha_ultma_mdfccn,						usro_ultma_mdfccn,
					cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,	nmro_unco_idntfccn_empldr
		)
		Select		iea.cnsctvo_cdgo_tpo_idntfccn_empldr,			nmro_idntfccn_empldr,					nmbre_empldr,
					cnsctvo_scrsl,									iss.cnsctvo_cdgo_ofcna,					inco_vgnca_cbrnza,
					fn_vgnca_cbrnza,								slro_bse,								cnsctvo_cdgo_tpo_cbrnza,
					cnsctvo_cdgo_entdd_arp,							iea.fcha_ultma_mdfccn,					iea.usro_ultma_mdfccn,
					iss.cnsctvo_slctd_atrzcn_srvco,					nmro_unco_idntfccn_empldr
		From		#tmpinformacionsolicitud iss
		Inner Join	bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas With(NoLock)
		On			iasas.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.tbAsInformacionEmpresasxAfiliado iea With(NoLock)
		On			iasas.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco

		--
		Update #tmpinformacionsolicitud 
		Set nuam = Null,
			cnsctvo_cdgo_ofcna = Null


		--Informaci�n de los conceptos
		Insert 
		Into       #tmpinformacionconceptos (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo   , cnsctvo_ops             ,
											cnsctvo_cdgo_cncpto_gsto  , fcha_ultma_mdfccn      , cnsctvo_cdgo_grpo_imprsn ,
											nmro_rdccn_cm             , cnsctvo_cdgo_ofcna_cm  , nmro_unco_ops            ,
											vlr_cncpto_cm             , gnrdo                  , fcha_utlzcn_dsde         ,
											fcha_utlzcn_hsta          , cnsctvo_cdgo_estdo_mega, vlr_lqdcn				  ,
											cdgo_intrno -- Se agrega el numero unico del prestador qvisionclr 2017/01/18
											)
		Select		ISR.cnsctvo_slctd_atrzcn_srvco, ISR.cnsctvo_srvco_slctdo                  , CCS.cnsctvo_ops             ,
					CCS.cnsctvo_cdgo_cncpto_gsto  , CCS.fcha_ultma_mdfccn                     , CCS.cnsctvo_cdgo_grpo_imprsn,
					CCS.nmro_rdccn_cm             , CCS.cnsctvo_cdgo_ofcna_cm                 , CCS.nmro_unco_ops           ,
					CCS.vlr_cncpto_cm             , CCS.gnrdo                                 , CCS.fcha_utlzcn_dsde        ,
					CCS.fcha_utlzcn_hsta          , CCS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, CCS.vlr_lqdcn				,
					ccs.cdgo_intrno_prstdr_atrzdo   -- qvisionclr2017/01/18
		From		#tmpinformacionservicios ISR
		Inner Join	bdcna.gsa.tbASConceptosServicioSolicitado CCS With(NoLock)
		On			CCS.cnsctvo_mdcmnto_slctdo = ISR.cnsctvo_mdcmnto_slctdo
		Where		CCS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo != @vlr_cro
		Union All
		Select		ISR.cnsctvo_slctd_atrzcn_srvco, ISR.cnsctvo_srvco_slctdo                  , CCS.cnsctvo_ops             ,
					CCS.cnsctvo_cdgo_cncpto_gsto  , CCS.fcha_ultma_mdfccn                     , CCS.cnsctvo_cdgo_grpo_imprsn,
					CCS.nmro_rdccn_cm             , CCS.cnsctvo_cdgo_ofcna_cm                 , CCS.nmro_unco_ops           ,
					CCS.vlr_cncpto_cm             , CCS.gnrdo                                 , CCS.fcha_utlzcn_dsde        ,
					CCS.fcha_utlzcn_hsta          , CCS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, CCS.vlr_lqdcn,
					ccs.cdgo_intrno_prstdr_atrzdo -- qvisionclr2017/01/18
		From       #tmpinformacionservicios ISR
		Inner Join bdcna.gsa.tbASConceptosServicioSolicitado CCS With(NoLock)
		On         CCS.cnsctvo_prcdmnto_insmo_slctdo = ISR.cnsctvo_prcdmnto_slctdo
		Where      CCS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo != @vlr_cro


		-- Se evita la migracion de los servicios diagnosticos especializados si estos no tienen numero de OPS o no tienen conceptos
		Delete		ise 
		From		#tmpinformacionservicios ise
		Left Join	#tmpinformacionconceptos ico
		On			ico.cnsctvo_slctd_atrzcn_srvco = ise.cnsctvo_slctd_atrzcn_srvco
		And			ico.cnsctvo_srvco_slctdo = ise.cnsctvo_srvco_slctdo
		Where		ise.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn4
		And			ico.cnsctvo_srvco_slctdo Is Null

		-- 
		Delete		ise 
		From		#tmpinformacionservicios ise
		Inner Join	#tmpinformacionconceptos ico
		On			ico.cnsctvo_slctd_atrzcn_srvco = ise.cnsctvo_slctd_atrzcn_srvco
		And			ico.cnsctvo_srvco_slctdo = ise.cnsctvo_srvco_slctdo
		Where		ise.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn4
		And			ico.nmro_unco_ops = @vlr_cro


		Update		ic
		Set			nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr
		From		#tmpinformacionconceptos ic
		Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
		On			dp.cdgo_intrno = ic.cdgo_intrno
		
		--calcula el estado de los conceptos
		Update     iso
		Set        cnsctvo_cdgo_estdo = esv.cnsctvo_cdgo_estdo_hmlgdo_sprs
		From       #tmpinformacionconceptos ISO
		Inner Join bdcna.prm.tbASEstadosServiciosSolicitados_Vigencias ESV With(NoLock)
		On         ISO.cnsctvo_cdgo_estdo_mega = ESV.cnsctvo_cdgo_estdo_srvco_slctdo
		Where      @fcha_actl Between ESV.inco_vgnca And ESV.fn_vgnca 
		
		-- 
		Update		iss
		Set			nuam = hss.nuam,
					cnsctvo_cdgo_ofcna = hss.cnsctvo_cdgo_ofcna
		From		#tmpinformacionsolicitud iso
		Inner Join	#tmpinformacionservicios iss
		On			iss.id_agrpdr = iso.id
		And			iss.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdcna.gsa.tbashomologacionserviciosmegasipres HSS With(NoLock)
		On			HSS.cnsctvo_srvco_slctdo = ISS.cnsctvo_srvco_slctdo

		-- Se limpia el campo id_agrpdr
		Update		#tmpinformacionservicios
		Set			id_agrpdr = Null

		-- Se duplica la informacion de las solicitudes por (nuam, oficina)
		Insert
		Into #tempInformacionSolicitudAux (cnsctvo_slctd_atrzcn_srvco, fcha_slctd              , cdgo_intrno                ,
										nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
										cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
										usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
										cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
										cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
										cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
										cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
										fcha_crcn					, nuam					   , cnsctvo_cdgo_ofcna			,
										fcha_ingrso_hsptlzcn		, fcha_egrso_hsptlzcn	   , crte_cnta				    , 
										cnsctvo_cdgo_clse_hbtcn
										)
		Select		ins.cnsctvo_slctd_atrzcn_srvco, fcha_slctd             , ins.cdgo_intrno                ,
					ins.nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
					cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              ,
					usro_crcn                   , ins.usro_ultma_mdfccn    , ins.fcha_ultma_mdfccn      ,
					cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, iss.cnsctvo_cdgo_rcbro     ,
					cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
					ins.cnsctvo_cdgo_estdo_mega , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
					ins.cnsctvo_cdgo_estdo      , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
					fcha_crcn					, iss.nuam				   , iss.cnsctvo_cdgo_ofcna		,	
					ins.fcha_ingrso_hsptlzcn	, ins.fcha_egrso_hsptlzcn   , ins.crte_cnta				, 
					ins.cnsctvo_cdgo_clse_hbtcn
		From		#tmpinformacionsolicitud ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco	
		Where		iss.nuam != @vlr_cro
		And			iss.nuam Is Not Null	
		Group By	ins.cnsctvo_slctd_atrzcn_srvco, fcha_slctd             , ins.cdgo_intrno            ,
					ins.nmro_unco_idntfccn_prstdr, nmro_unco_idntfccn_mdco , cnsctvo_cdgo_cntngnca      ,
					cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              ,
					usro_crcn                   , ins.usro_ultma_mdfccn    , ins.fcha_ultma_mdfccn      ,
					cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, iss.cnsctvo_cdgo_rcbro     ,
					cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
					ins.cnsctvo_cdgo_estdo_mega , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
					ins.cnsctvo_cdgo_estdo      , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
					fcha_crcn					, iss.nuam				   , iss.cnsctvo_cdgo_ofcna		,
					ins.fcha_ingrso_hsptlzcn	, ins.fcha_egrso_hsptlzcn   , ins.crte_cnta				, 
					ins.cnsctvo_cdgo_clse_hbtcn

		-- Se asigna el id_agrpdr 
		Update		iss
		Set			id_agrpdr = ins.id
		From		#tempInformacionSolicitudAux ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And			ins.nuam = iss.nuam
		And			ins.cnsctvo_cdgo_ofcna = iss.cnsctvo_cdgo_ofcna

		-- Se miran las prestaciones con mismo numero de OPS y se les asigna el nuam, oficina y agrupador
		-- de las que no tengan estos datos.
		Update		iss2
		Set			nuam = iss.nuam,
					id_agrpdr = iss.id_agrpdr,
					cnsctvo_cdgo_ofcna = iss.cnsctvo_cdgo_ofcna
		From		#tempInformacionSolicitudAux ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And			ins.id = iss.id_agrpdr
		Inner Join	#tmpinformacionconceptos ico 
		On			ico.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And			ico.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
		Inner Join	#tmpinformacionconceptos ico2
		On			ico2.cnsctvo_slctd_atrzcn_srvco = ico.cnsctvo_slctd_atrzcn_srvco
		And			ico2.nmro_unco_ops = ico.nmro_unco_ops
		Inner Join	#tmpinformacionservicios iss2 
		On			iss2.cnsctvo_slctd_atrzcn_srvco = ico2.cnsctvo_slctd_atrzcn_srvco
		And			iss2.cnsctvo_srvco_slctdo = ico2.cnsctvo_srvco_slctdo
		Where		iss2.nuam = @vlr_cro
		And			iss2.id_agrpdr Is Null

		-- Se llama el proceso de agrupacion para prestaciones
		Exec bdCNA.gsa.spASAgrupacionIntegracionSipres

		-- 
		Update		isa
		Set			fcha_slctd = iss.fcha_slctd,
					cnsctvo_cdgo_mrbldd = iss.cnsctvo_cdgo_mrbldd,
					cnsctvo_cdgo_tpo_atncn = iss.cnsctvo_cdgo_tpo_atncn,
					hra_dgta = iss.hra_dgta,
					nmro_atncn = iss.nmro_atncn,
					fcha_imprsn = iss.fcha_imprsn,
					tpo_prstcn = iss.tpo_prstcn,         
					cnsctvo_cdgo_grpo_entrga = iss.cnsctvo_cdgo_grpo_entrga,
					nmro_vsts = iss.nmro_vsts,
					ds_estnca = iss.ds_estnca,            
					cnsctvo_atncn_ops = iss.cnsctvo_atncn_ops,
					cnsctvo_cdgo_tpo_orgn_slctd = iss.cnsctvo_cdgo_tpo_orgn_slctd
		From		#tempInformacionSolicitudAux isa
		Inner Join	#tmpinformacionsolicitud iss
		On			isa.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco

		-- Se limpia la temporal principal de solicitudes
		Delete From #tmpinformacionsolicitud

		-- Se permite el insert al campo identity de #tmpinformacionsolicitud
		Set Identity_Insert #tmpinformacionsolicitud On

		Insert Into #tmpinformacionsolicitud
		(
			id,
			cnsctvo_slctd_atrzcn_srvco  , fcha_slctd               , cdgo_intrno                ,
			nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
			cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
			usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
			cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
			cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
			cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
			cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
			fcha_crcn					, cnsctvo_cdgo_agrpdr_prstcn,cnsctvo_cdgo_ofcna			,
			nuam						, cnsctvo_cdgo_mrbldd	   , cnsctvo_cdgo_tpo_atncn		,		
			hra_dgta					, nmro_atncn			   , fcha_imprsn				,		
			tpo_prstcn					, cnsctvo_cdgo_grpo_entrga , nmro_vsts					,		
			ds_estnca					, cnsctvo_atncn_ops		   , cnsctvo_cdgo_tpo_orgn_slctd, 
			fcha_ingrso_hsptlzcn		, fcha_egrso_hsptlzcn	   , crte_cnta				    , 
			cnsctvo_cdgo_clse_hbtcn
		)
		Select 	
			id,
			cnsctvo_slctd_atrzcn_srvco  , fcha_slctd               , cdgo_intrno                ,
			nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
			cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
			usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
			cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
			cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
			cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
			cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
			fcha_crcn					, cnsctvo_cdgo_agrpdr_prstcn,cnsctvo_cdgo_ofcna			,
			nuam						, cnsctvo_cdgo_mrbldd	   , cnsctvo_cdgo_tpo_atncn		,		
			hra_dgta					, nmro_atncn			   , fcha_imprsn				,		
			tpo_prstcn					, cnsctvo_cdgo_grpo_entrga , nmro_vsts					,		
			ds_estnca					, cnsctvo_atncn_ops		   , cnsctvo_cdgo_tpo_orgn_slctd, 
			fcha_ingrso_hsptlzcn		, fcha_egrso_hsptlzcn	   , crte_cnta				    , 
			cnsctvo_cdgo_clse_hbtcn
		From	#tempInformacionSolicitudAux

		Set Identity_Insert #tmpinformacionsolicitud Off

		Update		iso
		Set			cnsctvo_cdgo_rcbro = ise.cnsctvo_cdgo_rcbro 
		From		#tmpinformacionsolicitud iso
		Inner Join	#tmpinformacionservicios ise
		On			ise.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
		And			ise.id_agrpdr = iso.id

		--Se determina el valor del campo tpo_prstcn
		Update		iso
		Set			tpo_prstcn = @afrmcn
		From		#tmpinformacionsolicitud ISO
		Inner Join	#tmpinformacionservicios ISS
		On			ISO.id = ISS.id_agrpdr
		Where		ISS.cnsctvo_cdgo_tpo_srvco = @cnsctvo_prstcn;

		--Se actualiza el consecutivo de la OPS, de forma incremental para cada solicitud.
		With tmpConsecutivos
		As (	Select		ICC.id,
							ROW_NUMBER() Over (Partition By ISS.id_agrpdr Order By ICC.cnsctvo_slctd_atrzcn_srvco, ICC.cnsctvo_srvco_slctdo) As "fila"
				From		#tmpinformacionservicios ISS
				Inner Join  #tmpinformacionconceptos ICC
				On			ISS.cnsctvo_srvco_slctdo = ICC.cnsctvo_srvco_slctdo
		)
		Update		icc
		Set			cnsctvo_ops = tmp.fila
		From		#tmpinformacionconceptos ICC
		Inner Join	tmpConsecutivos TMP
		On			ICC.id = TMP.id

		-- Se obtiene el nuam para verificar si la prestacion ya existe.
		Update		iso
		Set			nuam = hss.nuam,
					cnsctvo_cdgo_ofcna = hss.cnsctvo_cdgo_ofcna
		From		#tmpinformacionsolicitud ISO
		Inner Join	#tmpinformacionservicios ISS
		On			ISS.id_agrpdr = ISO.id
		Inner Join	bdcna.gsa.tbashomologacionserviciosmegasipres HSS With (NOLOCK)
		On			HSS.cnsctvo_srvco_slctdo = ISS.cnsctvo_srvco_slctdo

		-- Se eliminana las prestaciones en estado anulado, no autorizado y devuelto que no hayan migrado con anterioridad
		Delete		ISS
		From		#tmpinformacionsolicitud ISO
		Inner Join	#tmpinformacionservicios ISS
		On			ISS.id_agrpdr = ISO.id
		left Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dam With(NoLock)
		On			ISO.nuam = dam.nuam
		And			ISO.cnsctvo_cdgo_ofcna = dam.cnsctvo_cdgo_ofcna
		And			ISS.cnsctvo_cdgo_srvco_slctdo = dam.cnsctvo_prstcn
		Where		ISS.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_anldo_mga
		And			dam.cnsctvo_prstcn Is Null
		
		-- 
		Delete		ISS
		From		#tmpinformacionsolicitud ISO
		Inner Join	#tmpinformacionservicios ISS
		On			ISS.id_agrpdr = ISO.id
		left Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dam With(NoLock)
		On			ISO.nuam = dam.nuam
		And			ISO.cnsctvo_cdgo_ofcna = dam.cnsctvo_cdgo_ofcna
		And			ISS.cnsctvo_cdgo_srvco_slctdo = dam.cnsctvo_prstcn
		Where		ISS.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_no_atrzdo_mga
		And			dam.cnsctvo_prstcn Is Null

		-- 
		Delete		ISS
		From		#tmpinformacionsolicitud ISO
		Inner Join	#tmpinformacionservicios ISS
		On			ISS.id_agrpdr = ISO.id
		left Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dam With(NoLock)
		On			ISO.nuam = dam.nuam
		And			ISO.cnsctvo_cdgo_ofcna = dam.cnsctvo_cdgo_ofcna
		And			ISS.cnsctvo_cdgo_srvco_slctdo = dam.cnsctvo_prstcn
		Where		ISS.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_dvlto_mga
		And			dam.cnsctvo_prstcn Is Null
		
		-- Se borran las solicitudes que no tengan prestaciones
		Delete		iso
		From		#tmpinformacionsolicitud ISO
		Left Join	#tmpinformacionservicios ISS
		On			ISS.id_agrpdr = ISO.id
		Where		ISS.id_agrpdr Is Null

		--Se obtiene el estado con el que migrara a sipres de la tabla de homologos.
		Update     iso
		Set        cnsctvo_cdgo_estdo = esv.cnsctvo_cdgo_estdo_hmlgdo_sprs
		From       #tmpinformacionservicios ISO
		Inner Join bdcna.prm.tbASEstadosServiciosSolicitados_Vigencias ESV With(NoLock)
		On         ISO.cnsctvo_cdgo_estdo_mega = ESV.cnsctvo_cdgo_estdo_srvco_slctdo
		Where      @fcha_actl Between ESV.inco_vgnca And ESV.fn_vgnca

		-- se sacan las solicitudes que tengan almenos 1 prestacion en estado homologo 1 - Ingresada, en sipres
		Insert Into #tmpsolicitudes
		(
					cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo
		)
		Select		cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo
		From		#tmpinformacionservicios
		Where		cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo_ingrsdo_sprs

		-- Si existe almenos una prestacion que pase en estado Ingresado, todas las prestaciones de la 
		-- solicitud deben pasar en el mismo estado.
		Update		a
		Set			cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo_ingrsdo_sprs
		From		#tmpinformacionservicios a
		Inner Join	#tmpsolicitudes b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Where		a.cnsctvo_cdgo_estdo_mega != @cnsctvo_cdgo_estdo_anldo_mga
		And			a.cnsctvo_cdgo_estdo_mega != @cnsctvo_cdgo_estdo_dvlto_mga
		And			a.cnsctvo_cdgo_estdo_mega != @cnsctvo_cdgo_estdo_no_atrzdo_mga

		-- 
		Delete		ic
		From		#tmpinformacionconceptos ic
		Inner Join	#tmpinformacionservicios iss
		On			ic.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
		Where		iss.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo_ingrsdo_sprs

		----se actualiza el estado de la atencion con el estado de la prestacion
		Update     ISO
		Set        cnsctvo_cdgo_estdo_atncn = ISS.cnsctvo_cdgo_estdo
		From       #tmpinformacionsolicitud ISO
		Inner Join #tmpinformacionservicios ISS
		On         ISS.id_agrpdr = ISO.id		

		-- se obteiene los id_agrpdr para las solicitudes con los estados, agrupado
		Insert Into #tempPrestacionesAgrupadasDiferenteEstado
		( 
					id_agrpdr,	cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_mga
		)
		Select		id_agrpdr, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_mega
		From		#tmpinformacionservicios
		Group By	id_agrpdr, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_mega

		-- Obtiene los id_agrpdr que tengan mas de una solicitud en diferente estado
		Insert	Into #tempAgrupadoresDiferenteEstado
		(
					id_agrpdr,		cnsctvo_cdgo_estdo
		)
		Select		id_agrpdr,		@cnsctvo_cdgo_estdo_ingrsdo_sprs
		From		#tempPrestacionesAgrupadasDiferenteEstado
		Group By	id_agrpdr
		Having		COUNT(id_agrpdr) > @vlr_uno

		Update     ISO
		Set        cnsctvo_cdgo_estdo_atncn = ISS.cnsctvo_cdgo_estdo
		From       #tmpinformacionsolicitud ISO
		Inner Join #tempAgrupadoresDiferenteEstado ISS
		On         ISS.id_agrpdr = ISO.id
		
		-- 
		Drop Table #tempServiciosXSolicitud
		Drop Table #tempInformacionSolicitudAux
		Drop Table #tempRelSolicitudServicios
		Drop Table #tmpsolicitudagrupador
		Drop Table #tmpsolicitudes
		Drop Table #tempPrestacionesAgrupadasDiferenteEstado
		Drop Table #tempAgrupadoresDiferenteEstado
End

