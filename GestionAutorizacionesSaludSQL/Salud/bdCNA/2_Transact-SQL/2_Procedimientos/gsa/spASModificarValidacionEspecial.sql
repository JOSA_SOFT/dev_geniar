USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarValidacionEspecial]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarValidacionEspecial
* Desarrollado por   : <\A Jonathan Chamizo    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite actualizar información sobre la validación 
						   especial de la solicitud D\> 
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 01/03/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>   
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarValidacionEspecial] 
AS
  SET NOCOUNT ON
	DECLARE @Fecha_Actual as datetime
	SET @Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASRegistroValidacionEspecial
		DELETE			VE
		FROM			BdCNA.gsa.tbASRegistroValidacionEspecial VE
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				VE.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- INSERTAR INFORMACIÓN DE ANEXOS EN TABLA tbASRegistroValidacionEspecial
		INSERT INTO	 BdCNA.gsa.tbASRegistroValidacionEspecial(
					 cnsctvo_slctd_atrzcn_srvco
					 ,cnsctvo_cdgo_tpo_idntfccn_afldo
					 ,nmro_idntfccn_afldo
					 ,nmro_vldcn_espcl
					 ,cnsctvo_cdgo_ofcna
					 ,usro_vldcn_espcl
					 ,accn_vldcn_espcl
					 ,cnsctvo_ops
					 ,fcha_vldcn_espcl
					 ,usro_crcn
					 ,fcha_crcn
					 ,usro_ultma_mdfccn
					 ,fcha_ultma_mdfccn)
		SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,AF.cnsctvo_cdgo_tpo_idntfccn_afldo	
					,AF.nmro_idntfccn_afldo
					,VE.nmro_vldcn_espcl
					,VE.cnsctvo_cdgo_ofcna
					,VE.usro_vldcn_espcl
					,VE.accn_vldcn_espcl
					,VE.cnsctvo_ops
					,VE.fcha_vldcn_espcl
					,VE.usro_crcn
					,@Fecha_Actual	--fcha_crcn	
					,VE.usro_crcn							
					,@Fecha_Actual	--fcha_ultma_mdfccn
		FROM		#TMod_ValidacionEspecial	VE WITH (NOLOCK)
		INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
		ON			VE.id = IDS.id
		INNER JOIN  #TMod_Afiliados AF WITH (NOLOCK)	
		ON			AF.id = IDS.id;	
	END

GO
