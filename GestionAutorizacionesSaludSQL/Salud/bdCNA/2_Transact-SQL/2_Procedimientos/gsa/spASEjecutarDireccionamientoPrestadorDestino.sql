USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarDireccionamientoPrestadorDestino]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEjecutarDireccionamientoPrestadorDestino
* Desarrollado por   : <\A Jois Lombana SánchezA\>    
* Descripcion        : <\D Procedimiento expuesto que inicia la ejecución del calculo de direccionamiento   D\>
          	      <\D de cada una de las prestaciones de la(s) solicitud(es) enviadas				    D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASEjecutarDireccionamientoPrestadorDestino] @cdgs_slctds nvarchar(max)
, @usro varchar(50)
, @estdo_ejccn varchar(5) OUTPUT
, @msje_errr varchar(2000) OUTPUT
, @msje_rspsta nvarchar(max) OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  BEGIN TRY
  --  Creación de tablas temporales para la ejecución de la gestión de direccionamiento de las prestaciones de una o más solicitudes.
  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  CREATE TABLE #tmpInformacionSolicitudPrestacion (
    id udtConsecutivo IDENTITY (1, 1) NOT NULL,
    cnsctvo_cdgo_prstcn udtConsecutivo NOT NULL,
    cdgo_intrno_ips_slctnte udtCodigoIps NOT NULL,
    cdgo_prstcn varchar(30) NULL,
    cnsctvo_slctd udtConsecutivo NOT NULL,
    cnsctvo_srvco udtConsecutivo NOT NULL,
    pln_prstcn udtConsecutivo NULL,
    nmro_id_pcnte udtNumeroIdentificacionLargo NULL,
    cnsctvo_tpo_id_pcnte udtConsecutivo NULL,
    nmro_slctd varchar(17) NOT NULL,
    nmro_prvdr varchar(17) NOT NULL,
    cnsctvo_cdgo_cdd_afldo udtConsecutivo NOT NULL,
    ubccn_pcnte udtConsecutivo NULL,
    clse_atncn_orgn int NOT NULL,
    clse_atncn int NOT NULL,
    ni_pcnte int NOT NULL,
    cdgo_zna int NULL,
    cnsctvo_sde_ips_afldo int NOT NULL,
    cnsctvo_cdgo_cdd_prmra int NOT NULL,
    cdgo_intrno_ips_afldo_prmra char(8) NOT NULL,
    edd_afldo int NOT NULL,
    cnsctvo_rsgo_prstcn int NULL,
    cdgo_intrno_ips_dstno char(8) NULL,
    vlr_trfdo float NOT NULL,
    sfcnca_ms int NOT NULL,
    sfcnca_ips int NOT NULL,
    cnsctvo_dtlle_lsta int NULL,
    cnsctvo_cnfgrcn int NOT NULL,
    fcha_imprsn datetime NULL,
    cdgo_intrno_prstdr_antrr varchar(8) NULL,
    cntdd_ds_mx int NULL,
    usro udtUsuario,
    cnsctvo_dgnstco_prncpl int NULL,
    cnsctvo_rsgo_dgnstco int NULL,
    cnsctvo_cdgo_tpo_cntrto int,
    nmro_cntrto int,
    cnsctvo_cdgo_tpo_idntfccn_afldo int,
    nmro_idntfccn udtNumeroIdentificacionLargo,
    nmro_idntfccn_empldr int,
	fcha_mnma_bsqda datetime,
	mrca_msmo_prstdor int default 0,
	cnsctvo_cdgo_tpo_pln int,
	cnsctvo_cdgo_frma_atncn int
  );

  CREATE TABLE #tmpPrestadoresDestino (
    id udtConsecutivo IDENTITY (1, 1) NOT NULL,
    id_slctd_x_prstcn udtConsecutivo NOT NULL,
    cnsctvo_cdgo_prstcn udtConsecutivo NOT NULL,
    cnsctvo_cdgo_rsgo_dgnstco udtConsecutivo,
    cnsctvo_vgnca_drcnmnto_prstdr udtConsecutivo NOT NULL,
    cnsctvo_cdgo_dt_lsta_prco udtConsecutivo NOT NULL,
    sfcnca_ips int NULL,
    nvl_prrdd int,
    sfcnca_ms int NULL,
    fcha_inco_vgnca_sfcnca datetime NULL,
    fcha_fn_vgnca_sfcnca datetime NULL,
    cdgo_intrno udtCodigoIps NOT NULL,
    cnsctvo_cdfccn udtConsecutivo NULL,
    cmple char(1) NOT NULL DEFAULT 'N',
    vlr_trfdo int,
    cnsctvo_cdgo_cdd udtConsecutivo NOT NULL,
    nmbre_scrsl udtDescripcion NOT NULL,
    cnsctvo_drcnmnto_prstdr udtConsecutivo NULL,
    cnsctvo_cdgo_zna udtConsecutivo,
    cnsctvo_cdgo_sde udtConsecutivo,
    nmro_idntfccn_empldr_prstdr int,
	tmpo_dsplzmnto int,
	sfcnca_orgnl INT NULL
  );

  DECLARE @negacion char(1),
          @codigoError char(2),
          @cdgs_slctds_XML xml,
          @salida xml,
		  @tipoTransaccionCrear char(1) = 'C',
		  @tipoProceso udtCodigo = 'DR',
		  @estadoControl char(2)

  SET @negacion = 'N';
  SET @codigoError = 'ET';
  SET @cdgs_slctds_XML = CONVERT(xml, @cdgs_slctds);

    --Se obtiene la información básica de la solicitud.
	EXEC bdCNA.gsa.spASObtenerDatosBasicos @cdgs_slctds_XML

		-- Se realiza la carga de la información en las temporales.
	EXEC BDCna.gsa.spASDireccionamientoObtenerDatos @usro

	EXEC gsa.spASDireccionamientoPrestadorDestino @usro,
													@negacion,
													@estdo_ejccn OUTPUT,
													@msje_errr OUTPUT,
													@salida OUTPUT			

	SET @msje_rspsta = CONVERT(nvarchar(max), @salida)

	   --Control de proceso para el caso de error en ejecución.
   EXEC bdCNA.gsa.[spASControlProcesoSolicitud] @tipoProceso,
												@tipoTransaccionCrear,
												@usro,
												@msje_errr,
												@estadoControl OUTPUT
  END TRY
  BEGIN CATCH
    SET @msje_errr =
    'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
    'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
    'Message:' + ERROR_MESSAGE() + CHAR(13) +
    'Procedure:' + ERROR_PROCEDURE();
    SET @estdo_ejccn = @codigoError;
    SET @msje_rspsta = NULL

  END CATCH

   -- Borrar temporal 
   IF (OBJECT_ID('tempdb..#tmpPrestadoresDestino') IS NOT NULL)
   BEGIN
	DROP TABLE #tmpPrestadoresDestino
   END

  IF (OBJECT_ID('tempdb..#tmpInformacionSolicitudPrestacion') IS NOT NULL)
  BEGIN
	DROP TABLE #tmpInformacionSolicitudPrestacion
  END

  IF (OBJECT_ID('tempdb..#tmpNmroSlctds') IS NOT NULL)
  BEGIN
	DROP TABLE #tmpNmroSlctds
  END
END

GO
