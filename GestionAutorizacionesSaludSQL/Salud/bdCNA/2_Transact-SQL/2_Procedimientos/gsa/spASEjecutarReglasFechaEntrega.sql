USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarReglasFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASEjecutarReglasFechaEntrega
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga ejecutar las reglas de fecha Entrega  D\>
* Observaciones    : <\O      O\>    
* Parametros       : <\P P\>    
* Variables        : <\V       V\>    
* Fecha Creacion   : <\FC 12/10/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [gsa].[spASEjecutarReglasFechaEntrega] @cnsctvo_prcso udtConsecutivo,
@es_btch char(1)
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @ldfechaActual datetime,
          @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
          @agrpa_prstcn udtLogico,
          @cdgo_grpo_entrga CHAR(4),
          @nmbre_sp_vldcn udtNombreObjeto,
          @instruccionSQL nvarchar(4000),
          @max_cnsctvo udtConsecutivo,
          @min_cnsctvo udtConsecutivo,
          @negacion char(1),
          @vacio numeric(2),
          @inexistente numeric(2),
          @afirmacion UDTLOGICO

  SET @ldfechaActual = GETDATE()
  SET @negacion = 'N'
  SET @vacio = 0
  SET @inexistente = 99
  SET @afirmacion = 'S'

  --creacion de tablas temporal para la ejecución
  DECLARE @tbMVReglasValidacion TABLE (
    cnsctvo udtConsecutivo IDENTITY (1, 1),
    cnsctvo_cdgo_grpo_entrga udtConsecutivo,
    cdgo_grpo_entrga CHAR(4),
    jrrqa numeric(2),
    nmbre_sp char(50),
    agrpa_prstcn udtLogico
  )

  --Se consultan las reglas a ejecutar.
  INSERT @tbMVReglasValidacion (cnsctvo_cdgo_grpo_entrga,
  cdgo_grpo_entrga,
  jrrqa,
  nmbre_sp,
  agrpa_prstcn)
    SELECT
      m.cnsctvo_cdgo_grpo_entrga,
      ge.cdgo_grpo_entrga,
      m.jrrqa,
      m.nmbre_sp,
      m.agrpa_prstcn
    FROM bdCNA.prm.tbASReglasFechaEntrega_Vigencias m WITH (NOLOCK)
    INNER JOIN bdCNA.prm.tbASGruposEntrega_Vigencias ge WITH (NOLOCK)
      ON ge.cnsctvo_cdgo_grpo_entrga = m.cnsctvo_cdgo_grpo_entrga
    WHERE @ldfechaActual BETWEEN m.inco_vgnca AND m.fn_vgnca
    AND @ldfechaActual BETWEEN ge.inco_vgnca AND ge.fn_vgnca
    AND m.cnsctvo_vgnca_cdgo_rgla_fcha_entrga NOT IN (@vacio, @inexistente)
    AND m.vsble_usro = @afirmacion
    ORDER BY m.jrrqa

  SELECT
    @max_cnsctvo = MAX(cnsctvo)
  FROM @tbMVReglasValidacion
  SET @min_cnsctvo = 1

  --Se valida si es procesamiento en línea.
  IF @es_btch = @negacion
  BEGIN

    WHILE (@min_cnsctvo <= @max_cnsctvo)
    BEGIN

      SELECT
        @cnsctvo_cdgo_grpo_entrga = cnsctvo_cdgo_grpo_entrga,
        @agrpa_prstcn = agrpa_prstcn,
        @cdgo_grpo_entrga = cdgo_grpo_entrga,
        @nmbre_sp_vldcn = nmbre_sp
      FROM @tbMVReglasValidacion
      WHERE cnsctvo = @min_cnsctvo

      --ejecutar reglas de fecha de entrega

      SET @instruccionSQL = N'EXEC gsa.' + LTRIM(RTRIM(@nmbre_sp_vldcn)) + N' @param1, @param2, @param3';
      EXECUTE sp_executesql @instruccionSQL,
                            N'@param1 as udtConsecutivo, @param2 as char(4), @param3 as udtLogico',
                            @param1 = @cnsctvo_cdgo_grpo_entrga,
                            @param2 = @cdgo_grpo_entrga,
                            @param3 = @agrpa_prstcn;

      SET @min_cnsctvo = @min_cnsctvo + 1

    END
  END
END

GO
