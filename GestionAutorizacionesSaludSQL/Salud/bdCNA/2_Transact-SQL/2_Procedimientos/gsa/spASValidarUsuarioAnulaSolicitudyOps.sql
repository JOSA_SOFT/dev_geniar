USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarUsuarioAnulaSolicitudyOps]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------
* Procedimiento			: gsa.spASValidarUsuarioAnulaSolicitudyOps
* Desarrollado por		: <\A Luis Fernando Benavides				\A>
* Descripción			: <\D Valida usuario anula solicitud y ops	\D>
* Observaciones			: <\O O\>
* Parámetros			: <\P @lgn_usro: usuario autorizado para anular solicitud opsP\>
* Variables				: <\V V\>
* Fecha Creación		: <\FC 2016/05/06 FC\>
*-----------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-----------------------------------------------------------------------------------------
* Modificado Por         : <\AM  AM\>
* Descripcion            : <\DM  DM\>
* Nuevos Parametros      : <\PM  PM\>
* Nuevas Variables       : <\VM  VM\>
* Fecha Modificacion     : <\FM  FM\>
*-----------------------------------------------------------------------------------------*/

/*
exec gsa.spASValidarUsuarioAnulaSolicitudyOps 'user20'
*/

ALTER PROCEDURE [gsa].[spASValidarUsuarioAnulaSolicitudyOps]
@lgn_usro	udtUsuario
AS

Begin
	Set NoCount On

	Declare @cnsctvo_cdgo_mdlo udtconsecutivo = 46,
			@cnsctvo_cdgo_prfl udtconsecutivo = 246 /*4000012 Coordinador de Prestaciones Medicas*/

	Select		a.cdgo_prfl
				,a.cnsctvo_cdgo_prfl
				,a.dscrpcn_prfl, 
				CASE WHEN a.cnsctvo_cdgo_prfl IN (@cnsctvo_cdgo_prfl) THEN 'S' ELSE 'N' END As atrzdo
	From		bdSeguridad.dbo.fnPerfilesActivos() a 
	Inner join	bdSeguridad.dbo.fnLoginsxPerfilesActivos() b
	On			a.cnsctvo_cdgo_prfl = b.cnsctvo_cdgo_prfl
	Where		b.lgn_usro			= @lgn_usro
	And			a.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo
End 


GO
