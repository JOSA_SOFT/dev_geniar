USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerDatosPVencActividades]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASObtenerDatosPVencActividades
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta datos del empleador.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASObtenerDatosPVencActividades]
	@plan_atencion varchar(40),
	@tipo_plan varchar(40),
	@empleador varchar(40),
	@riesgo varchar(40),
	@tipo_poblacion varchar(40),
	@legal varchar(40),
	@reputacional varchar(40)
	
AS
BEGIN

	SET NOCOUNT ON;
	Declare @total_pesos					int = 0,
			@prioridad						int,
			@prioridad_tarea				udtDescripcion = 'PRIORIDAD TAREA',
			@fecha_validacion				datetime = getDate(),
			@cdgo_tpo_rgla_prrzcn_vncmnto	udtConsecutivo = 8, --valida las prioridades
			
			@codigo_plan_atencion			udtConsecutivo = 1,
			@codigo_tipo_plan				udtConsecutivo = 2,
			@codigo_empleador				udtConsecutivo = 3,
			@codigo_riesgo					udtConsecutivo = 4,
			@codigo_tipo_poblacion			udtConsecutivo = 5,
			@codigo_legal					udtConsecutivo = 6,
			@codigo_reputacional			udtConsecutivo = 7;

 
	CREATE TABLE #DATOSAFILIADOREGLAS(
		CONCEPTO		varchar(50),
		TIPO_REGLA		int,
		PESOCONCEPTO	int default 0
	)

	
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@plan_atencion, @codigo_plan_atencion);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@tipo_plan, @codigo_tipo_plan);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@empleador, @codigo_empleador);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@riesgo, @codigo_riesgo);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@tipo_poblacion, @codigo_tipo_poblacion);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@legal, @codigo_legal);
	insert into #DATOSAFILIADOREGLAS(	CONCEPTO, TIPO_REGLA) values(@reputacional, @codigo_reputacional);

	update dar set PESOCONCEPTO =  rpv.nmro_pso_rgla
	from #DATOSAFILIADOREGLAS dar
	inner join BDCna.gsa.tbASReglasPriorizacionVencimiento rpv With(NoLock) on dar.concepto = rpv.nmbre_rgla_prrzcn_vncmnto
	And rpv.cnsctvo_cdgo_tpo_rgla_prrzcn_vncmnto = dar.tipo_regla
	
	Set @total_pesos = (Select sum(PESOCONCEPTO) from #DATOSAFILIADOREGLAS) ;

	Set @prioridad = (Select nmro_pso_rgla from BDCna.gsa.tbASReglasPriorizacionVencimiento rpv With(NoLock) 
					  where cnsctvo_cdgo_tpo_rgla_prrzcn_vncmnto = @cdgo_tpo_rgla_prrzcn_vncmnto 
					  AND @total_pesos between rpv.rngo_incl and rpv.rngo_fnl)

	Select  dscrpcn_prrdd_tra nombreTarea, vncmnto_tra vencimientoActividad from BDCna.prm.tbASPrioridadTarea_vigencias With(NoLock)
	where prrdd_tra = @prioridad
	And @fecha_validacion between inco_vgnca and fin_vgnca
	union all
	Select @prioridad_tarea nombreTarea, @prioridad vencimientoActividad;
		
	drop table #DATOSAFILIADOREGLAS
END

GO
