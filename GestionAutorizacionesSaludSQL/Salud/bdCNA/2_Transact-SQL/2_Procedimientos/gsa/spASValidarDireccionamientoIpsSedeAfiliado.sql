USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoIpsSedeAfiliado]    Script Date: 10/01/2017 13:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoIpsSedeAfiliado
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D Sede de la IPS del afiliado										 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza ajuste en el procedimiento para que se tenga en cuenta la ciudad del afiliado y se valide la marca
						   de ciudad principal
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 20/01/2017 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoIpsSedeAfiliado] 
    @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON;


  DECLARE @cmple         Char(1),
          @cdd_prncpl    Char(1),
		  @cdd_no_prncpl Char(1),
		  @vlor_cro      Int;
		    
  Set @cmple         = 'S'
  Set @cdd_prncpl    = 'S'
  Set @cdd_no_prncpl = 'N'
  Set @vlor_cro      = 0

  --Se valida la Sede de la IPS del afiliado
  IF EXISTS (SELECT     TCO.id_slctd_x_prstcn
             FROM       #tmpPrestadoresDestino TCO
             INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
             ON         ISP.id                    = TCO.id_slctd_x_prstcn AND 
			            ISP.cnsctvo_sde_ips_afldo = TCO.cnsctvo_cdgo_sde
             Where      ISP.cnsctvo_sde_ips_afldo IS NOT NULL
	         And        ISP.mrca_msmo_prstdor = @vlor_cro
			)
  BEGIN
    --Se actualizan los prestadores que apliquen el filtro, siempre y cuando se haya encontrado mínimo uno
    --que cumpla el filtro de ciudad para la prestación a evaluar.
    UPDATE     TCO
    SET        cmple = @cmple
    FROM       #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
    ON         ISP.id                    = TCO.id_slctd_x_prstcn And 
	           ISP.cnsctvo_sde_ips_afldo = TCO.cnsctvo_cdgo_sde  
	Inner Join BDAfiliacionValidador.dbo.tbCiudades_Vigencias c WITH (NOLOCK)
	On         c.cnsctvo_cdgo_cdd = ISP.cnsctvo_cdgo_cdd_afldo
    Where      ISP.cnsctvo_sde_ips_afldo IS NOT NULL
	And        ISP.mrca_msmo_prstdor = @vlor_cro
	And        c.mrca_drccnto_mga    = @cdd_prncpl


	UPDATE     TCO
    SET        cmple = @cmple
    FROM       #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
    ON         ISP.id                    = TCO.id_slctd_x_prstcn And 
	           ISP.cnsctvo_sde_ips_afldo <> TCO.cnsctvo_cdgo_sde  
	Inner Join BDAfiliacionValidador.dbo.tbCiudades_Vigencias c WITH (NOLOCK)
	On         c.cnsctvo_cdgo_cdd = ISP.cnsctvo_cdgo_cdd_afldo
    Where      ISP.cnsctvo_sde_ips_afldo IS NOT NULL
	And        ISP.mrca_msmo_prstdor = @vlor_cro
	And        c.mrca_drccnto_mga    = @cdd_no_prncpl	


	EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]

  END
END
