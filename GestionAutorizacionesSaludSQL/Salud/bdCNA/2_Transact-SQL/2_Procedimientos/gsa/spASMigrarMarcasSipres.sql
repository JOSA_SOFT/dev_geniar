USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASMigrarMarcasSipres]    Script Date: 07/04/2017 14:31:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spASMigrarMarcasSipres							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Crea la marca de OPS Virtual, tomada de proceso implementado por el Ing. Juan Carlos Vásquez García,
									y migra mas marcas de los servicios de Mega D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  1-  
                              P\>  													
* Variables					: <\V  1- 

							  V\>  	
* Metdos o PRG Relacionados	:      1 - BdCna.gsa.spascargarinformacionsipres
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/13 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se cambia la tabla de referencia de los estados de #tmpinformacionsolicitud a
							   #tmpinformacionservicios DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/08 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez - sisjvg01 AM\>  													
* Descripcion			: <\DM Por OPS Virtual -Se Modifica para incluir en las marcas de ops así:
								1. Si el afiliado tiene correo se crea la marca de '4-OPS Virtual'
								2. Si el afiliado no tiene correo se crea la marca '7-OPS Virtual Afiliado Sin Correo' DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/06 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spASMigrarMarcasSipres]

As

Begin
		Set NoCount On

		Declare	  @fcha_actl						DateTime,
				  @usro								udtusuario,
				  @cnsctvo_cdgo_tpo_orgn_slctd3		Int, /* '3-proceso Ops Virtual con Prog. Entrega' */
				  @cnsctvo_cdgo_estdo_mega11		Int, /* '11-Autorizada' */
				  @cnsctvo_cdgo_estdo_mega15		Int, /* '15-Entregada' */
				  @cnsctvo_cdgo_estdo153            Int, /* '153-Entregada */
				  @lcnsctvo_mrca_atnc_ops			Int,
				  @fcha_fn_vgnca					Date,
				  @cnsctvo_cdgo_tps_mrcs_ops4		UdtConsecutivo, /* OPS VIRTUAL CON AFILIADO CON CORREO */
				  @cnsctvo_cdgo_tps_mrcs_ops7		UdtConsecutivo, /* OPS VIRTUAL CON AFILIADO SIN CORREO */
				  @vldo								UdtLogico;
				             

		Set @fcha_actl = getDate();
		Set @usro = subString(System_User, CharIndex('\', System_User) + 1, Len(System_User))
		Set @cnsctvo_cdgo_tpo_orgn_slctd3 = 3;
		Set @cnsctvo_cdgo_estdo_mega11 = 11; -- 11-Autorizada
		Set @cnsctvo_cdgo_estdo_mega15 = 15; -- 15-Entregada
		Set @cnsctvo_cdgo_estdo153 = 153;    -- '153-Entregada
		Set @lcnsctvo_mrca_atnc_ops = 0;
		Set @fcha_fn_vgnca = '99991231'
		Set @cnsctvo_cdgo_tps_mrcs_ops4 = 4; -- Ops Virtual
		Set @cnsctvo_cdgo_tps_mrcs_ops7 = 7; -- Ops Virtual Afiliado sin Correo
		Set @vldo = 'S';		


		Create Table #TmpTbMarcasXAtencionOPS
		(
			id								Int Identity(1,1),
			cnsctvo_cdgo_tps_mrcs_ops		UdtConsecutivo default 4,
			cnsctvo_atncn_ops				UdtConsecutivo,
			inco_vgnca						Date,
			fn_vgnca						Date,
			fcha_crcn						Date,
			fcha_fn_vldz_rgstro				Date,
			vldo							UdtLogico,
			usro_crcn						UdtUsuario,
			usro_ultma_mdfccn				UdtUsuario,
			nmro_unco_idntfccn_afldo        UdtConsecutivo,
		);

		Create Table #TmpActualizaProcedimientos
		(
			id									Int Identity(1,1),
			cnsctvo_slctd_atrzcn_srvco			UdtConsecutivo,
			cnsctvo_srvco_slctdo				UdtConsecutivo,
			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	UdtConsecutivo,
			cnsctvo_det_prgrmcn_fcha			udtConsecutivo,
			cnsctvo_prgrmcn_prstcn				UdtConsecutivo,
			cnsctvo_prgrmcn_fcha_evnto			UdtConsecutivo,
			nuam								numeric(18,0),
			cnsctvo_cdgo_ofcna                  UdtConsecutivo,
		)

		Create Table #TmpEmailAfiliado
		(
		  id			int identity,
		  email         varchar(100),
		  nui_afldo		UdtConsecutivo
		)

		-- Actualizamos el Consecutivo de Atencion Ops   
		Update		iso
		Set			cnsctvo_atncn_ops = AOS.cnsctvo_atncn_ops
		From		#tmpinformacionsolicitud iso
		Inner Join	bdSisalud.dbo.tbAtencionOps AOS With(NoLock)
		On			AOS.nuam = iso.nuam
		And			AOS.cnsctvo_cdgo_ofcna = iso.cnsctvo_cdgo_ofcna

		-- Recuperamos datos de la programacion de entrega
		Insert		#TmpActualizaProcedimientos
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
					cnsctvo_prgrmcn_prstcn,				cnsctvo_prgrmcn_fcha_evnto,		nuam,
					cnsctvo_cdgo_ofcna,					cnsctvo_det_prgrmcn_fcha
		)
		Select
					iso.cnsctvo_slctd_atrzcn_srvco,		iss.cnsctvo_srvco_slctdo,			iss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
					p.cnsctvo_prgrmcn_prstcn,			p.cnsctvo_prgrmcn_fcha_evnto,		iso.nuam,
					iso.cnsctvo_cdgo_ofcna,				p.cnsctvo_det_prgrmcn_fcha				
		From		#tmpinformacionsolicitud iso
		Inner Join	#tmpinformacionservicios iss
		ON			iss.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
		Inner join  #tmpinformacionconceptos ics
		On          ics.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
		Inner Join  BdSisalud.dbo.tbDetProgramacionFechaEvento p with(nolock)
		On          p.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = iss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
		where       iss.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_mega15

		-- Actualizamos los procedimientos con la programacion de entrega
		Update		p
		Set			cnsctvo_det_prgrmcn_fcha = t.cnsctvo_det_prgrmcn_fcha,
					cnsctvo_prgrmcn_prstcn = t.cnsctvo_prgrmcn_prstcn,
					cnsctvo_prgrmcn_fcha_evnto = t.cnsctvo_prgrmcn_fcha_evnto,
					fcha_ultma_mdfccn = @fcha_actl,
					usro_ultma_mdfccn = @usro
		From		#TmpActualizaProcedimientos t
		Inner join  bdSisalud.dbo.tbprocedimientos p
		On          p.nuam = t.nuam
		And			p.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
		

		-- Actualizamos el estado de la programación de entrega
		Update		d
		Set			cnsctvo_cdgo_estds_entrga = @cnsctvo_cdgo_estdo153,
		            cnsctvo_ops = t.nuam,
					cnsctvo_cdgo_ofcna_ops = t.cnsctvo_cdgo_ofcna,
					usro_mdfccn = @usro,
					fcha_ultma_mdfccn = @fcha_actl
		From		#TmpActualizaProcedimientos t
		Inner  join bdSisalud.dbo.tbDetProgramacionFechaEvento d with(nolock)
		On          d.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
		And         d.cnsctvo_prgrmcn_prstcn = t.cnsctvo_prgrmcn_prstcn
		And         d.cnsctvo_prgrmcn_fcha_evnto = t.cnsctvo_prgrmcn_fcha_evnto

		-- Insertamos las marcas de la atención Ops Virtual 
		Insert Into	#TmpTbMarcasXAtencionOPS
		(
					cnsctvo_atncn_ops,			inco_vgnca,							fn_vgnca,					
					fcha_crcn,					fcha_fn_vldz_rgstro,				vldo,
					usro_crcn,					usro_ultma_mdfccn,					nmro_unco_idntfccn_afldo
		)
		Select
					iso.cnsctvo_atncn_ops,		@fcha_actl,							@fcha_fn_vgnca,				
					@fcha_actl,					@fcha_fn_vgnca,						@vldo,						
					@usro,						@usro,								iaf.nmro_unco_idntfccn_afldo
		From		#tmpinformacionsolicitud iso
		Inner Join  #tmpinformacionafiliado iaf
		On          iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
		Inner Join	#tmpinformacionservicios iss
		ON			iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco 
		And			iso.id = iss.id_agrpdr
		Left Join	bdCna.dbo.tbMarcasXAtencionOPS mxa With(NoLock)
		ON			mxa.cnsctvo_atncn_ops = iso.cnsctvo_atncn_ops
		Where		iso.cnsctvo_cdgo_tpo_orgn_slctd = @cnsctvo_cdgo_tpo_orgn_slctd3
		And         iss.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_mega15
		And         mxa.cnsctvo_atncn_ops Is Null;

		-- Recuperamos los afiliados con correo
		Insert      #TmpEmailAfiliado
		(
					email,			nui_afldo
		)
		Select      
					s.eml,			s.nmro_unco_idntfccn_usro	
		From        #TmpTbMarcasXAtencionOPS t
		Inner Join  bdSeguridad.dbo.tbRegistroUsuariosWeb s WITH (NOLOCK)
		On          s.nmro_unco_idntfccn_usro = t.nmro_unco_idntfccn_afldo
		where       (isnull(ltrim(rtrim(s.eml)),'')<> '' And s.eml like '%@%')
		Union
		Select      
					s.eml,			s.nmro_unco_idntfccn_afldo	
		From        #TmpTbMarcasXAtencionOPS t
		Inner Join  BDAfiliacionValidador.dbo.tbBeneficiariosValidador s WITH (NOLOCK)
		On          s.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo
		where       (isnull(ltrim(rtrim(s.eml)),'')<> '' And s.eml like '%@%')
		And         @fcha_actl Between s.inco_vgnca_bnfcro and s.fn_vgnca_bnfcro

		Update	   t	
		Set        cnsctvo_cdgo_tps_mrcs_ops = @cnsctvo_cdgo_tps_mrcs_ops7
		From       #TmpTbMarcasXAtencionOPS t
		left join  #TmpEmailAfiliado t1
		on         t1.nui_afldo = t.nmro_unco_idntfccn_afldo
		Where      t1.nui_afldo is null
				
		Insert Into	#TmpTbMarcasXAtencionOPS
		(
					cnsctvo_atncn_ops,			cnsctvo_cdgo_tps_mrcs_ops,			inco_vgnca,
					fn_vgnca,					fcha_crcn,							fcha_fn_vldz_rgstro,
					vldo,						usro_crcn,							usro_ultma_mdfccn
		)
		Select
					iso.cnsctvo_atncn_ops,		tmssv.cnsctvo_cdgo_tps_mrcs_ops,	@fcha_actl,
					@fcha_fn_vgnca,				@fcha_actl,							@fcha_fn_vgnca,
					@vldo,						@usro,								@usro
		From		#tmpinformacionsolicitud iso
		Inner Join	#tmpinformacionservicios iss
		ON			iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco 
		And			iso.id = iss.id_agrpdr
		Inner Join	BdCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
		On			mss.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
		Inner Join	BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias tmssv With(NoLock)
		On			tmssv.cnsctvo_cdgo_tpo_mrca = mss.cnsctvo_cdgo_tpo_mrca
		Left Join	bdCna.dbo.tbMarcasXAtencionOPS mxa With(NoLock)
		ON			mxa.cnsctvo_atncn_ops = iso.cnsctvo_atncn_ops
		Where		iss.cnsctvo_cdgo_estdo_mega = @cnsctvo_cdgo_estdo_mega11
		And         mxa.cnsctvo_atncn_ops Is Null
		And			@fcha_actl Between tmssv.inco_vgnca And tmssv.fn_vgnca
		And			tmssv.cnsctvo_cdgo_tps_mrcs_ops Is Not Null;


  		IF Exists ( Select 1 From #TmpTbMarcasXAtencionOPS)
		Begin
				-- Recuperamos el último id de la tabla tbMarcasXAtencionOPS	
				Select		@lcnsctvo_mrca_atnc_ops = Isnull(Max(cnsctvo_cdgo_mrcs_x_atncn_ops),0) + 1     
				From		BDCna.dbo.tbMarcasXAtencionOPS With(NoLock)
  
				-- Insertamos la marca de Envio OPS Virtual 
				--Se almacena la información de tbMarcasXAtencionOPS
				Merge bdCna.dbo.tbMarcasXAtencionOPS With(RowLock) As t
				Using(
					Select 
								id,											cnsctvo_cdgo_tps_mrcs_ops,		cnsctvo_atncn_ops,
								inco_vgnca,									fn_vgnca,						fcha_crcn,
								fcha_fn_vldz_rgstro,						vldo,							usro_crcn,
								usro_ultma_mdfccn
					From        #TmpTbMarcasXAtencionOPS
				) As s
				On ( 
						t.cnsctvo_atncn_ops = s.cnsctvo_atncn_ops					And
						t.cnsctvo_cdgo_tps_mrcs_ops = s.cnsctvo_cdgo_tps_mrcs_ops
					)
				When Not Matched Then
						Insert
						(
									cnsctvo_cdgo_mrcs_x_atncn_ops,				cnsctvo_cdgo_tps_mrcs_ops,		cnsctvo_atncn_ops,
									inco_vgnca,									fn_vgnca,						fcha_crcn,
									fcha_fn_vldz_rgstro,						vldo,							usro_crcn,
									usro_ultma_mdfccn
						)
						Values(
									@lcnsctvo_mrca_atnc_ops + s.id,				s.cnsctvo_cdgo_tps_mrcs_ops,	s.cnsctvo_atncn_ops,
									s.inco_vgnca,								s.fn_vgnca,						s.fcha_crcn,
									s.fcha_fn_vldz_rgstro,						s.vldo,							s.usro_crcn,
									s.usro_ultma_mdfccn
						);					
		  
		End
	Drop Table #TmpEmailAfiliado
End
