USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCargarInformacionConsultaFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCargarInformacionConsultaFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite cargar la información necesaria para el procedo D\>
					   <\D de consulta fecha esperada de entrega									 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 04/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASCargarInformacionConsultaFechaEntrega] @es_btch char(1)
AS

BEGIN

  DECLARE @fechaActual DATETIME
  SET @fechaActual = GETDATE()

  SET NOCOUNT ON
  INSERT INTO #tmpDatosSolicitudFechaEntrega (cnsctvo_slctd_srvco_sld_rcbda,
  cnsctvo_srvco_slctdo,
  cnsctvo_cdfccn,
  cdgo_cdfccn,
  cnsctvo_pln,
  cdgo_pln,
  cnsctvo_tpo_pln,
  cdgo_tpo_pln,
  cnsctvo_mdo_cntcto,
  cdgo_mdo_cntcto,
  fcha_slctd,
  nmro_unco_idntfccn_afldo,
  nmro_cntrto,
  cnsctvo_cdgo_tpo_cntrto,
  nmro_slctd_ss,
  nmro_slctd_prvdr,
  cnsctvo_cdgo_tpo_idntfccn_afldo,
  nmro_idntfccn,
  cnsctvo_grpo_entrga_fnl,
  cdgo_grpo_entrga_fnl,
  fcha_estmda_entrga_fnl)
    SELECT
      ss.cnsctvo_slctd_atrzcn_srvco,
      ss.cnsctvo_srvco_slctdo,
      ss.cnsctvo_cdgo_srvco_slctdo,
      sso.cdgo_srvco_slctdo,
      iasas.cnsctvo_cdgo_pln,
      iasaso.cdgo_pln,
      iasas.cnsctvo_cdgo_tpo_pln,
      iasaso.cdgo_tpo_pln,
      sas.cnsctvo_cdgo_mdo_cntcto_slctd,
      ccv.dscrpcn_mdo_cntcto,
      sas.fcha_slctd,
      iasas.nmro_unco_idntfccn_afldo,
      iasas.nmro_cntrto,
      iasas.cnsctvo_cdgo_tpo_cntrto,
      sas.nmro_slctd_atrzcn_ss,
      sas.nmro_slctd_prvdr,
      iasas.cnsctvo_cdgo_tpo_idntfccn_afldo,
      iasas.nmro_idntfccn_afldo,
      rfe.cnsctvo_cdgo_grpo_entrga,
      g.cdgo_grpo_entrga,
      rfe.fcha_estmda_entrga
    FROM [BDCna].[gsa].[tbASResultadoFechaEntrega] rfe WITH (NOLOCK)
    INNER JOIN #tmpNmroSlctds slc WITH (NOLOCK)
      ON slc.cnsctvo_slctd_srvco_sld_rcbda = rfe.cnsctvo_slctd_atrzcn_srvco
    INNER JOIN BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)--Servicios 
      ON ss.cnsctvo_slctd_atrzcn_srvco = slc.cnsctvo_slctd_srvco_sld_rcbda
      AND ss.cnsctvo_srvco_slctdo = rfe.cnsctvo_srvco_slctdo
    INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)-- Solicitudes
      ON ss.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco
    LEFT JOIN BDCna.gsa.tbASServiciosSolicitadosOriginal sso WITH (NOLOCK)-- Servicios Solicitados Original
      ON sso.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
    INNER JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas WITH (NOLOCK)
      ON sas.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
    LEFT JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal iasaso WITH (NOLOCK)
      ON iasaso.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
    INNER JOIN BDCna.prm.tbASMediosContacto_Vigencias ccv WITH (NOLOCK)
      ON ccv.cnsctvo_cdgo_mdo_cntcto = sas.cnsctvo_cdgo_mdo_cntcto_slctd
    INNER JOIN bdCNA.prm.tbASGruposEntrega_Vigencias g WITH (NOLOCK)
      ON rfe.cnsctvo_cdgo_grpo_entrga = g.cnsctvo_cdgo_grpo_entrga
	WHERE @fechaActual BETWEEN ccv.inco_vgnca AND ccv.fn_vgnca
	 AND @fechaActual BETWEEN g.inco_vgnca AND g.fn_vgnca
END

GO
