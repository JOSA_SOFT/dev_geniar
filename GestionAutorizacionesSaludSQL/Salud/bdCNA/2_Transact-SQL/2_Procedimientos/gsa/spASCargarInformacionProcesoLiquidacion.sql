USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCargarInformacionProcesoLiquidacion]    Script Date: 22/06/2017 16:14:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASCargarInformacionProcesoLiquidacion
* Desarrollado por		: <\A Ing. Jonathan - SETI SAS	  						A\>
* Descripcion			: <\D Sp que permite la carga de la información			D\>
						  <\D necesaria para la liquidación						D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													P\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 01/06/2016										FC\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jonathan - SETI SAS AM\>
* Descripcion			 : <\DM	Se adicionó la captura de información de prestaciones
								PIS (Cuos) para aplicar la liquidación 
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	09/09/2016 FM\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para actualizar la forma de atencion, teniendo en cuenta la clase 
								de atencion, ya que se identifica que el servicio de guardar no esta deja la informacion correcta.
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	06/12/2016 FM\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andrés López Ramírez AM\>
* Descripcion			 : <\DM	
                                Se corrige update que hacia referencia a un campo que no existe en la tabla 
								temporal #cnslddo_slctud
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	07/12/2016 FM\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento con el fin de actualizar en la tabla temporal la clase de 
								habitacion a los conceptos que corresponden para que se realice la liquidacion correctamente.
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	22/12/2016 FM\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para que no tome los conceptos que se encuentran con 
								un numero unico de OPS calculado.
								Adicionalmente se organizar la consulta inicial para extraer la informacion mas relevante 
								y por medio de actualizacion se complete la informacion requerida para el proceso
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	29/03/2017 FM\>
*----------------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------  
* Modificado Por		 : <\AM	Ing. Carlos Andres lopez AM\>
* Descripcion			 : <\DM	
                                Se agrega tabla temporal #tmpMaxResultadoDireccionamiento donde se obtienen el 
								ultimo registro de direccionamiento para las solicitudes que han requerido recalcular 
								el direccionamiento.
								
								Se agrega update para recuperar el codigo interno de la tabla tbASResultadoDireccionamiento.
								En llenado de la tabla #cnslddo_slctud se cambia la tabla tbASResultadoDireccionamiento por la
								tabla temporal #tmpMaxResultadoDireccionamiento.								 
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	31/03/2017 FM\>
*----------------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por		 : <\AM	Ing. Juan Carlos Vásquez AM\>
* Descripcion			 : <\DM	
                               Por OPS VIRTUAL Se Modifica Procedimiento para ajustar la fecha programación de 
							   entrega a la fecha actual con el fin de generar ops 								 
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	 @Valor_TRES VM\>
* Fecha Modificacion	 : <\FM	03-04-2017	 FM\>
*----------------------------------------------------------------------------------------------------------------------------------   
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                               Se modifica el procedimiento para ajustar la fecha programación de entrega con el 
							   formato requerido con el fin de que se realice el proceso de alistamiento 								 
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM  VM\>
* Fecha Modificacion	 : <\FM	05-04-2017	 FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza modificacion del procedimiento para que agrupe las prestaciones x item de presupuesto
							 con el fin de que realice la liquidacion correctamente
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 22/06/2016		FM\>
*----------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASCargarInformacionProcesoLiquidacion] 
AS
BEGIN
  SET NOCOUNT ON;
  
  DECLARE @fechaliquidacion	           Datetime ,
          @fcha_estmda_entrga_ops_vrtl Date     ,
          @si				           udtLogico,
          @no				           udtLogico,
          @ms_bilateralidad	           udtLogico,
		  @fcha_actl		           Datetime	,
		  @mrca_lqdcn_prstcn           udtlogico,
		  @vlr_cro			           Int		,
		  @actva_ds_estnca             udtLogico,
		  @Valor_TRES                  Int ;	 


  Create Table #tmpMaxResultadoDireccionamiento
  (
	cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
	cnsctvo_srvco_slctdo				udtConsecutivo,
	cdgo_intrno							Char(8),
	cnsctvo_rsltdo_drccnmnto			udtConsecutivo
  )


  Set @fechaliquidacion            = CONVERT(CHAR(10), GETDATE(), 111)
  Set @si                          = 'S'
  Set @no                          = 'N'
  Set @ms_bilateralidad            = 'B'
  Set @fcha_actl                   = getDate()
  Set @mrca_lqdcn_prstcn           = 'S'
  Set @vlr_cro                     = 0
  Set @actva_ds_estnca             = 'S'
  Set @fcha_estmda_entrga_ops_vrtl = CONVERT(CHAR(10), @fcha_actl, 111)
  Set @Valor_TRES                  = 3


  -- Se agrega para los casos que han sido reliquidados y por lo tanto existen varios direccionamientos
  -- Se saca el maximo direccionamiento para cada prestacion
  Insert Into #tmpMaxResultadoDireccionamiento
  (
		cnsctvo_slctd_atrzcn_srvco,    cnsctvo_srvco_slctdo,     
		cnsctvo_rsltdo_drccnmnto
  )
  Select	 SAS.cnsctvo_slctd_atrzcn_srvco,	SS.cnsctvo_srvco_slctdo,
			 MAX(RDR.cnsctvo_rsltdo_drccnmnto)
  From	     #tmpnmroslctds NST
  Inner Join gsa.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK) 
  On		 SAS.cnsctvo_slctd_atrzcn_srvco = NST.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join gsa.tbASServiciosSolicitados SS WITH (NOLOCK)
  On		 SS.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
  Inner Join gsa.tbASResultadoDireccionamiento RDR WITH(NOLOCK) 
  On		 RDR.cnsctvo_srvco_slctdo = SS.cnsctvo_srvco_slctdo
  Group By	 SAS.cnsctvo_slctd_atrzcn_srvco,	SS.cnsctvo_srvco_slctdo

  -- se obtiene el codigo interno de la tabla de resultados de direccionamiento.
  Update		#tmpMaxResultadoDireccionamiento
  Set			cdgo_intrno = b.cdgo_intrno
  From			#tmpMaxResultadoDireccionamiento a
  Inner Join	bdCNA.gsa.tbASResultadoDireccionamiento  b With(NoLock)
  On			b.cnsctvo_rsltdo_drccnmnto = a.cnsctvo_rsltdo_drccnmnto
		  		   
  Insert 
  Into       #cnslddo_slctud(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo   , cnsctvo_cdgo_srvco_slctdo,
                             cdgo_intrno               , cnsctvo_cdgo_frma_atncn, cntdd_slctda
                            )
  Select     SAS.cnsctvo_slctd_atrzcn_srvco, SS.cnsctvo_srvco_slctdo    , SS.cnsctvo_cdgo_srvco_slctdo,
	         RDR.cdgo_intrno               , SAS.cnsctvo_cdgo_frma_atncn, SS.cntdd_slctda
  From	     #tmpnmroslctds NST
  Inner Join gsa.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK) 
  On		 SAS.cnsctvo_slctd_atrzcn_srvco = NST.cnsctvo_slctd_srvco_sld_rcbda
  Inner Join gsa.tbASServiciosSolicitados SS WITH (NOLOCK)
  On		 SS.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
  Inner Join #tmpMaxResultadoDireccionamiento RDR WITH(NOLOCK) 
  On		 RDR.cnsctvo_srvco_slctdo = SS.cnsctvo_srvco_slctdo
  Inner Join prm.tbASEstadosServiciosSolicitados_Vigencias ESV WITH(NOLOCK)
  On		 ESV.cnsctvo_cdgo_estdo_srvco_slctdo = SS.cnsctvo_cdgo_estdo_srvco_slctdo
  Where		 ESV.mrca_lqdcn_prstcn = @mrca_lqdcn_prstcn
  And        @fcha_actl Between ESV.inco_vgnca And ESV.fn_vgnca
  And		 RDR.cdgo_intrno IS NOT NULL
  Order By	 SS.cnsctvo_cdgo_srvco_slctdo DESC;  
  
  --Se actualiza la especialidad del medico tratante
  Update     #cnslddo_slctud
  Set        cnsctvo_cdgo_espcldd_mdco_trtnte = MTS.cnsctvo_cdgo_espcldd_mdco_trtnte
  From       #cnslddo_slctud a
  Inner Join gsa.tbASMedicoTratanteSolicitudAutorizacionServicios MTS WITH(NOLOCK) 
  On		 MTS.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco

  --Se actualiza el consecutivo del plan pos
  Update     #cnslddo_slctud
  Set        cnsctvo_cdgo_pln = IAS.cnsctvo_cdgo_pln 
  From       #cnslddo_slctud a
  Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios IAS WITH(NOLOCK) 
  On		 IAS.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
  
  --Se actualiza la fecha de entrega por prestación	
  UPDATE	 CS
  SET		 fcha_estmda_entrga = RFE.fcha_estmda_entrga
  FROM		 #cnslddo_slctud CS
  Inner Join gsa.tbASResultadoFechaEntrega RFE WITH(NOLOCK) 
  ON		 RFE.cnsctvo_slctd_atrzcn_srvco = CS.cnsctvo_slctd_atrzcn_srvco
  AND		 RFE.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo;

   --Se actualiza la fecha de entrega por prestación para OPS Virtual - sisjvg01- 2017-04-03
  UPDATE	 CS
  SET		 fcha_estmda_entrga = @fcha_estmda_entrga_ops_vrtl
  FROM		 #cnslddo_slctud CS
  Inner Join gsa.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK)
  ON		 SAS.cnsctvo_slctd_atrzcn_srvco = CS.cnsctvo_slctd_atrzcn_srvco
  wHERE      SAS.cnsctvo_cdgo_tpo_orgn_slctd =  @Valor_TRES;

  --se actualiza la forma de atencion tomando como dato la clase
  Update     #cnslddo_slctud
  Set        cnsctvo_cdgo_frma_atncn = d.cnsctvo_cdgo_frma_atncn
  From       #cnslddo_slctud a
  Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH(NOLOCK) 
  ON		 b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
  Inner Join bdSisalud.dbo.tbClasesAtencion_vigencias c WITH(NOLOCK) 
  On         c.cnsctvo_cdgo_clse_atncn = b.cnsctvo_cdgo_clse_atncn
  Inner Join bdSisalud.dbo.tbformasAtencion_vigencias d WITH(NOLOCK) 
  On         d.cnsctvo_cdgo_frma_atncn = c.cnsctvo_cdgo_frma_atncn  
  Where      @fcha_actl Between c.inco_vgnca And c.fn_vgnca
  And        @fcha_actl Between d.inco_vgnca And d.fn_vgnca


  Insert 
  Into       #tbConceptoGastosTmp_1(cnsctvo_srvco_lqdrc, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_cps        ,
		                            cnsctvo_ops        , cnsctvo_cdgo_frma_atncn   , cntdd                   ,		
		                            espcldd            , cnsctvo_srvco_slctdo      , cdgo_intrno_prstdr      ,			
		                            fcha_lqdcn         , cnsctvo_cdgo_pln          , cnsctvo_cdgo_cncpto_gsto,
									agrpdr_srvco_lqdr
		                           )
  Select     CS.cnsctvo_cnslddo_slctud          , CS.cnsctvo_slctd_atrzcn_srvco, CS.cnsctvo_cdgo_srvco_slctdo,  
		     @vlr_cro                           , CS.cnsctvo_cdgo_frma_atncn   , CS.cntdd_slctda             ,	
		     CS.cnsctvo_cdgo_espcldd_mdco_trtnte, CS.cnsctvo_srvco_slctdo      , CS.cdgo_intrno              ,	  
		     @fechaLiquidacion                  , CS.cnsctvo_cdgo_pln          , d.cnsctvo_cdgo_cncpto_gsto  ,
			 b.cnsctvo_cdgo_itm_prspsto
  From		 #cnslddo_slctud CS	
  Inner Join bdSisalud.dbo.tbCupsServicios b WITH(NOLOCK)					
  On         b.cnsctvo_prstcn = CS.cnsctvo_cdgo_srvco_slctdo
  Inner Join bdSisalud.dbo.tbModeloLiquidacionxConceptos c WITH(NOLOCK)		
  On         c.cnsctvo_cdgo_mdlo_lqdcn_cncpto = b.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbDetModeloLiquidacionxConceptos d WITH(NOLOCK)	
  On         d.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbcodificaciones e WITH(NOLOCK)					
  On         e.cnsctvo_cdfccn = CS.cnsctvo_cdgo_srvco_slctdo
  Where      @fechaLiquidacion Between c.inco_vgnca And c.fn_vgnca
  And        @fechaLiquidacion Between d.inco_vgnca And d.fn_vgnca
  And        @fechaLiquidacion Between b.inco_vgnca And b.fn_vgnca
  Union
  Select     CS.cnsctvo_cnslddo_slctud          , CS.cnsctvo_slctd_atrzcn_srvco, CS.cnsctvo_cdgo_srvco_slctdo,
			 ROW_NUMBER() OVER(PARTITION BY CS.cnsctvo_cdgo_srvco_slctdo  ORDER BY CS.cnsctvo_cdgo_srvco_slctdo DESC), 
			 CS.cnsctvo_cdgo_frma_atncn         , CS.cntdd_slctda,    
			 CS.cnsctvo_cdgo_espcldd_mdco_trtnte, CS.cnsctvo_srvco_slctdo      , CS.cdgo_intrno              ,           
			 @fechaLiquidacion                  , CS.cnsctvo_cdgo_pln          , d.cnsctvo_cdgo_cncpto_gsto  ,
			 b.cnsctvo_cdgo_itm_prspsto
  From		 #cnslddo_slctud CS	
  Inner Join bdSisalud.dbo.tbCums b WITH(NOLOCK)                    
  On		 b.cnsctvo_cms = CS.cnsctvo_cdgo_srvco_slctdo
  Inner Join bdSisalud.dbo.tbModeloLiquidacionxConceptos c WITH(NOLOCK)        
  On		 c.cnsctvo_cdgo_mdlo_lqdcn_cncpto = b.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbDetModeloLiquidacionxConceptos d WITH(NOLOCK)    
  On		 d.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbcodificaciones e WITH(NOLOCK)                    
  On		 e.cnsctvo_cdfccn = CS.cnsctvo_cdgo_srvco_slctdo
  Inner Join BDCna.gsa.tbASMedicamentosSolicitados MED WITH(NOLOCK) 
  On		 MED.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
  Where      @fechaLiquidacion Between c.inco_vgnca And c.fn_vgnca
  And        @fechaLiquidacion Between d.inco_vgnca And d.fn_vgnca
  And        @fechaLiquidacion Between b.inco_vgnca And b.fn_vgnca
  Union
  Select     CS.cnsctvo_cnslddo_slctud          , CS.cnsctvo_slctd_atrzcn_srvco, CS.cnsctvo_cdgo_srvco_slctdo,
			 ROW_NUMBER() OVER(PARTITION BY CS.cnsctvo_cdgo_srvco_slctdo  ORDER BY CS.cnsctvo_cdgo_srvco_slctdo DESC), 
			 CS.cnsctvo_cdgo_frma_atncn         , CS.cntdd_slctda,    
			 CS.cnsctvo_cdgo_espcldd_mdco_trtnte, CS.cnsctvo_srvco_slctdo      , CS.cdgo_intrno              ,           
			 @fechaLiquidacion                  , CS.cnsctvo_cdgo_pln          , d.cnsctvo_cdgo_cncpto_gsto  ,
			 p.cnsctvo_cdgo_itm_prspsto
  From		 #cnslddo_slctud CS	
  Inner Join bdSisalud.dbo.tbPrestacionPis p WITH(NOLOCK)                    
  On		 p.cnsctvo_prstcn_pis = CS.cnsctvo_cdgo_srvco_slctdo 	
  Inner Join bdSisalud.dbo.tbModeloLiquidacionxConceptos c WITH(NOLOCK)        
  On		 c.cnsctvo_cdgo_mdlo_lqdcn_cncpto = p.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbDetModeloLiquidacionxConceptos d WITH(NOLOCK)    
  On		 d.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
  Inner Join bdSisalud.dbo.tbcodificaciones e WITH(NOLOCK)                    
  On		 e.cnsctvo_cdfccn = CS.cnsctvo_cdgo_srvco_slctdo 
  Inner Join bdcna.gsa.tbassolicitudesautorizacionservicios SAS WITH(NOLOCK)   
  On		 SAS.cnsctvo_slctd_atrzcn_srvco = CS.cnsctvo_slctd_atrzcn_srvco
  Where      @fechaLiquidacion Between c.inco_vgnca And c.fn_vgnca
  And        @fechaLiquidacion Between d.inco_vgnca And d.fn_vgnca
  And        @fechaLiquidacion Between p.inco_vgnca And p.fn_vgnca;
		
  Update	 CGT
  Set		 CGT.bltrl							= Case LTR.cdgo_ltrldd           When @ms_bilateralidad Then @si Else @no End,
			 CGT.msma_va						= Case PIS.cnsctvo_cdgo_va_accso When 1                 Then @si Else @no End, 
			 CGT.cnsctvo_prcdmnto_insmo_slctdo	= PIS.cnsctvo_prcdmnto_insmo_slctdo,
			 CGT.rmpe_pqte						= @no
  From		 #tbConceptoGastosTmp_1 CGT
  Inner Join #cnslddo_slctud CS
  On		 CGT.cnsctvo_slctd_atrzcn_srvco = CS.cnsctvo_slctd_atrzcn_srvco And 
             CGT.cnsctvo_cdgo_cps           = CS.cnsctvo_cdgo_srvco_slctdo
  Right Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados  PIS WITH (NOLOCK)
  On		 PIS.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
  Inner Join bdSisalud.dbo.tbLateralidades_Vigencias LTR WITH (NOLOCK)
  On		 LTR.cnsctvo_cdgo_ltrldd = PIS.cnsctvo_cdgo_ltrldd
  Where		 @fechaliquidacion Between LTR.inco_vgnca And LTR.fn_vgnca;

  Update	 CGT
  Set		 CGT.bltrl					= @no                       ,
			 CGT.msma_va				= @no                       , 
			 CGT.cnsctvo_mdcmnto_slctdo	= MED.cnsctvo_mdcmnto_slctdo,
			 CGT.rmpe_pqte				= @no
  From		 #tbConceptoGastosTmp_1 CGT
  Inner Join #cnslddo_slctud CS
  On		 CS.cnsctvo_slctd_atrzcn_srvco = CGT.cnsctvo_slctd_atrzcn_srvco And 
             CS.cnsctvo_cdgo_srvco_slctdo = CGT.cnsctvo_cdgo_cps
  Inner Join gsa.tbASMedicamentosSolicitados MED WITH(NOLOCK) 
  On		 CS.cnsctvo_srvco_slctdo = MED.cnsctvo_srvco_slctdo;

  Update	 CGT
  Set		 CGT.bltrl	   = @no,
			 CGT.msma_va   = @no, 
			 CGT.rmpe_pqte = @no
  From		 #tbConceptoGastosTmp_1 CGT
  Inner Join #cnslddo_slctud CS
  On		 CS.cnsctvo_slctd_atrzcn_srvco = CGT.cnsctvo_slctd_atrzcn_srvco
  And		 CS.cnsctvo_cdgo_srvco_slctdo = CGT.cnsctvo_cdgo_cps
  Inner Join bdsisalud.dbo.tbPrestacionPis p    WITH(NOLOCK)                    
  On		 CS.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn_pis;
	
  ---- Se actualiza en la tabla temporal la clase de habitacion a los conceptos que corresponden
  Update	 CGT 
  Set		 cnsctvo_cdgo_clse_hbtcn = IH.cnsctvo_cdgo_clse_hbtcn
  From		 #tbConceptoGastosTmp_1 CGT 
  Inner Join BDCNA.GSA.tbASInformacionHospitalariaSolicitudAutorizacionServicios IH WITH(NOLOCK) 
  On		 IH.cnsctvo_slctd_atrzcn_srvco = CGT.cnsctvo_slctd_atrzcn_srvco
  Inner Join bdSisalud.dbo.tbConceptosGasto_Vigencias c WITH(NOLOCK) 
  On         c.cnsctvo_cdgo_cncpto_gsto = CGT.cnsctvo_cdgo_cncpto_gsto
  Where      c.actva_ds_estnca = 'S'
	
  Insert 
  Into   #tbserviciosaliquidarorigen(cnsctvo_srvco_lqdr, cnsctvo_cdgo_cps  , cnsctvo_cdgo_tpo_atncn, 
		                             bltrl             , msma_va           , cntdd                 , 
		                             rmpe_pqte         , espcldd           , vlr_lqdcn             ,
		                             agrpdr_srvco_lqdr , cdgo_intrno_prstdr, fcha_lqdcn            , 
		                             cnsctvo_cdgo_pln  , rsltdo_lqdcn      , cnsctvo_pqte
									)
  Select    cnsctvo_srvco_lqdrc, cnsctvo_cdgo_cps  , cnsctvo_cdgo_frma_atncn,
		    bltrl              , msma_va           , cntdd                  ,
		    rmpe_pqte          , espcldd           , vlr_lqdcn              ,
		    agrpdr_srvco_lqdr  , cdgo_intrno_prstdr, fcha_lqdcn             ,
		    cnsctvo_cdgo_pln   , rsltdo_lqdcn      , cnsctvo_pqte
  From		#tbConceptoGastosTmp_1
  Group By	cnsctvo_srvco_lqdrc, cnsctvo_cdgo_cps  , cnsctvo_cdgo_frma_atncn,
			bltrl              , msma_va           , cntdd                  ,
			rmpe_pqte          , espcldd           , vlr_lqdcn              ,
			agrpdr_srvco_lqdr  , cdgo_intrno_prstdr, fcha_lqdcn             ,
			cnsctvo_cdgo_pln   , rsltdo_lqdcn      , cnsctvo_pqte;
				
  Insert 
  Into   #tbconceptosserviciosaliquidarorigen (CGT.cnsctvo_cncpto_lqdr, CGT.cnsctvo_srvco_lqdr     , CGT.cnsctvo_cdgo_cncpto_gsto,
		                                       CGT.vlr_lqdcn_cncpto   , CGT.cnsctvo_cdgo_clse_hbtcn
		                                      )
  Select cnsctvo_srvco_lqdr, cnsctvo_srvco_lqdrc    , cnsctvo_cdgo_cncpto_gsto,
		 vlr_lqdcn_cncpto  , cnsctvo_cdgo_clse_hbtcn
  From   #tbConceptoGastosTmp_1 CGT;

  Drop Table #tmpMaxResultadoDireccionamiento

END

