USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarResultadoDireccionamiento]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarResultadoDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite guardar el resultado del direccionamiento	D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Rodriguez  AM\>    
* Descripcion        : <\D  Se modifica SP para que solo guarde los registros de la tabla temporal que
							Lograron ser direccionados											       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   2016-11-18 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGuardarResultadoDireccionamiento] 
@es_btch char(1)
AS

BEGIN
  SET NOCOUNT ON
  DECLARE @fechaActual datetime

  SET @fechaActual = GETDATE()

  --Se Eliminan de la tabla temporal todos los registros que no se pudieron direccionar
  --Para no guardar registros con codigo Interno con valor null.
  Delete FROM #tmpInformacionSolicitudPrestacion 
  Where cdgo_intrno_ips_dstno is null;
  
  --Se almacena la información resultado
  MERGE [BDCna].[gsa].[tbASResultadoDireccionamiento] WITH (ROWLOCK) AS target
  USING (SELECT
    m.cnsctvo_slctd,
    m.cnsctvo_srvco,
    m.cdgo_intrno_ips_dstno AS "cdgo_intrno",
    m.usro
  FROM #tmpInformacionSolicitudPrestacion m) AS source (cnsctvo_slctd, cnsctvo_srvco, cdgo_intrno, usro)
  ON (target.cnsctvo_slctd_atrzcn_srvco = source.cnsctvo_slctd
  AND target.cnsctvo_srvco_slctdo = source.cnsctvo_srvco)
  WHEN MATCHED THEN
  UPDATE SET cdgo_intrno = source.cdgo_intrno,
  fcha_ultma_mdfccn = @fechaActual,
  usro_ultma_mdfccn = source.usro
  WHEN NOT MATCHED THEN
  INSERT (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cdgo_intrno, fcha_crcn, usro_crcn, fcha_ultma_mdfccn, usro_ultma_mdfccn)
  VALUES (source.cnsctvo_slctd, source.cnsctvo_srvco, source.cdgo_intrno, @fechaActual, source.usro, @fechaActual, source.usro);

END

GO
