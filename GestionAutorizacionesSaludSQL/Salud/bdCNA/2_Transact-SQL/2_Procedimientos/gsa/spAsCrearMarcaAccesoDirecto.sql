USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsCrearMarcaAccesoDirecto]    Script Date: 05/04/2017 09:09:13 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsCrearMarcaAccesoDirecto							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Guarda el registro de las marcas de los servicos segun los parametros del prestador D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  1- @usro_aplccn:usuario del sistema 
                              P\>  													
* Variables					: <\V  1- @fcha_actl: Fecha actual obtenida del sistema
								   2- @cnsctvo_cdgo_tpo_mrca7: tipo marca 7, Acceso directo
															   (Referencia: BdCna.gsa.tbASMarcasServiciosSolicitados)
								   3- @accso_drcto_si: Almacena el valor 'S', usado para filtrar los pretadores con 
													   marca de acceso directo
								   

							  V\>  	
* Metdos o PRG Relacionados	:      1 - BdCna.gsa.spASGuardarResultadoLiquidacion
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/11 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se modifican los cruces de las consultas ya que para la validacion de acceso directo 
							   para validar con tbAsociacionModeloActividad.
							   Se valida que en tbCupsServiciosxPlanes exista una vigencia para la prestacion.
							    DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agregan los campos cnsctvo_cdgo_frma_atncn a la tabla temporal #tempMarcaAccesoDirecto
							   Se agrega cruces con las tablas tbFormasAtencion_Vigencias y tbClasificacionAtencion
							   para validar la atencion.
							    DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/24 FM\>												
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsCrearMarcaAccesoDirecto]
			@usro_aplccn				udtusuario
As
Begin
	Set NoCount On
	Declare @fcha_actl				DateTime,
			@cnsctvo_cdgo_tpo_mrca7 UdtConsecutivo,
			@accso_drcto_si			UdtLogico;

	-- Crea tabla teporal donde almacena los datos para crear las marcas
	Create Table #tempMarcaPrestacionMarca
	(
		cnsctvo_srvco_slctdo			UdtConsecutivo,	
		cnsctvo_cdgo_tpo_srvco			UdtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo,
		cnsctvo_cdgo_mdlo_cnvno_prstcn	UdtConsecutivo, 
		cdgo_intrno						UdtCodigoIps,
		cnsctvo_mdlo_cnvno_pln			UdtConsecutivo,
		cnsctvo_cdgo_pln				UdtConsecutivo,
		cnsctvo_cdgo_frma_atncn			UdtConsecutivo
	);


	Create Table #tempMarcaAccesoDirecto
	(
		cnsctvo_srvco_slctdo			UdtConsecutivo,	
		cnsctvo_cdgo_tpo_mrca			UdtConsecutivo,		
		fcha_crcn						Date,
		usro_crcn						UdtUsuario,				
		fcha_ultma_mdfccn				Date,			
		usro_ultma_mdfccn				UdtUsuario,
		cnsctvo_cdgo_tpo_srvco			UdtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo,
		cnsctvo_cdgo_mdlo_cnvno_prstcn	UdtConsecutivo,
		cdgo_intrno						UdtCodigoIps,
		cnsctvo_mdlo_cnvno_pln			UdtConsecutivo,
		cnsctvo_cdgo_pln				UdtConsecutivo,
		cnsctvo_cdgo_frma_atncn			UdtConsecutivo,
		cnsctvo_cdgo_clsfccn_atncn		UdtConsecutivo
	);

	Set @fcha_actl = GETDATE();
	Set @cnsctvo_cdgo_tpo_mrca7 = 7; -- 7 - Acceso Directo
	Set @accso_drcto_si = 'S';

	

	-- Llena la tabla temporal con informacion de las prestaciones 
	Insert Into #tempMarcaPrestacionMarca
	(
				cnsctvo_srvco_slctdo,			cnsctvo_cdgo_srvco_slctdo,
				cnsctvo_cdgo_tpo_srvco,			cnsctvo_cdgo_pln,
				cnsctvo_cdgo_frma_atncn
	)
	Select		ss.cnsctvo_srvco_slctdo,		ss.cnsctvo_cdgo_srvco_slctdo,
				ss.cnsctvo_cdgo_tpo_srvco,		cgt.cnsctvo_cdgo_pln,
				sas.cnsctvo_cdgo_frma_atncn
	From		#tbConceptoGastosTmp_1 cgt
	Inner Join	bdCNA.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = cgt.cnsctvo_slctd_atrzcn_srvco
	Inner Join  bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco

	-- Se obtiene el codigo interno
	Update		mad
	Set			cdgo_intrno = css.cdgo_intrno_prstdr_atrzdo 
	From		#tempMarcaPrestacionMarca mad
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo  = mad.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
						
	-- Se valida si la prestacion es de acceso directo
	Insert Into #tempMarcaAccesoDirecto
	(
				cnsctvo_cdgo_mdlo_cnvno_prstcn,			cnsctvo_srvco_slctdo,			cnsctvo_cdgo_srvco_slctdo,
				cnsctvo_mdlo_cnvno_pln,					cnsctvo_cdgo_tpo_srvco,			cnsctvo_cdgo_pln,
				cdgo_intrno,							cnsctvo_cdgo_frma_atncn,		cnsctvo_cdgo_clsfccn_atncn
	)
	Select		dmcp.cnsctvo_cdgo_mdlo_cnvno_prstcn,	mad.cnsctvo_srvco_slctdo,		c.cnsctvo_cdfccn,
				ama.cnsctvo_mdlo_cnvno_pln,				mad.cnsctvo_cdgo_tpo_srvco,		ama.cnsctvo_cdgo_pln,
				ama.cdgo_intrno,						mad.cnsctvo_cdgo_frma_atncn,	dmcp.cnsctvo_cdgo_clsfccn_atncn
	From		#tempMarcaPrestacionMarca mad
	Inner Join	bdSisalud.dbo.tbCodificaciones c With(NoLock)
	On			c.cnsctvo_cdfccn = mad.cnsctvo_cdgo_srvco_slctdo
	And			c.cnsctvo_cdgo_tpo_cdfccn = mad.cnsctvo_cdgo_tpo_srvco
	Inner Join	bdSisalud.dbo.tbDetEquivalencias de With(NoLock)
	On			de.cnsctvo_cdfccn_b = c.cnsctvo_cdfccn
	Inner Join  BDContratacion.dbo.tbDetModeloConveniosPrestaciones dmcp With(NoLock)
	On			dmcp.cnsctvo_det_eqvlnca = de.cnsctvo_det_eqvlnca	
	Inner Join	bdContratacion.dbo.tbModeloConveniosPrestacionesxPlan mcpxp With(NoLock)	
	On			mcpxp.cnsctvo_cdgo_mdlo_cnvno_prstcn = dmcp.cnsctvo_cdgo_mdlo_cnvno_prstcn	
	Inner Join	bdSisalud.dbo.tbAsociacionModeloActividad ama With(NoLock)
	On			ama.cnsctvo_mdlo_cnvno_pln = mcpxp.cnsctvo_mdlo_cnvno_prstcn_pln
	And			ama.cnsctvo_cdgo_pln = mad.cnsctvo_cdgo_pln
	And			ama.cdgo_intrno = mad.cdgo_intrno
	Where		@fcha_actl Between dmcp.inco_vgnca  And dmcp.fn_vgnca
	And			@fcha_actl Between mcpxp.inco_vgnca And mcpxp.fn_vgnca
	And			@fcha_actl Between ama.inco_vgnca   And ama.fn_vgnca
	And			dmcp.accso_drcto= @accso_drcto_si;
						
	-- Se pone la marca			
	Update		mad
	Set			cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca7,
				usro_crcn             = @usro_aplccn,
				usro_ultma_mdfccn     = @usro_aplccn,
				fcha_crcn             = @fcha_actl,
				fcha_ultma_mdfccn     = @fcha_actl	
	From		#tempMarcaAccesoDirecto mad
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
	On			dp.cdgo_intrno = mad.cdgo_intrno
	Inner Join	bdsisalud.dbo.tbCupsServiciosxPlanes csxp With(NoLock) 
	On			csxp.cnsctvo_prstcn = mad.cnsctvo_cdgo_srvco_slctdo
	And			csxp.cnsctvo_cdgo_pln = mad.cnsctvo_cdgo_pln
	Inner Join	bdsisalud.dbo.tbCupsServiciosxPlanes_vigencias csxpv With(NoLock)
	On			csxp.cnsctvo_prstcn = csxpv.cnsctvo_prstcn 	
	And			csxp.cnsctvo_cdgo_pln = csxpv.cnsctvo_cdgo_pln
	Inner Join	bdSisalud.dbo.tbFormasAtencion_Vigencias fav With(NoLock)
	On			fav.cnsctvo_cdgo_frma_atncn = mad.cnsctvo_cdgo_frma_atncn
	Inner Join 	bdsisalud.dbo.tbClasificacionAtencion ca With(NoLock)
	On			ca.cnsctvo_cdgo_clsfccn_atncn = fav.cnsctvo_cdgo_clsfccn_atncn
	And			ca.cnsctvo_cdgo_clsfccn_atncn = mad.cnsctvo_cdgo_clsfccn_atncn	
	Where		@fcha_actl Between csxpv.inco_vgnca And csxpv.fn_vgnca
	And			@fcha_actl Between csxp.inco_vgnca And csxp.fn_vgnca
	And			@fcha_actl Between dp.inco_vgnca And dp.fn_vgnca
	And			@fcha_actl Between fav.inco_vgnca And fav.fn_vgnca	

	-- Guarda la informacion de la marca en la tabla real.
	Merge gsa.tbASMarcasServiciosSolicitados As t
	Using (
		Select		Distinct
					cnsctvo_srvco_slctdo,		cnsctvo_cdgo_tpo_mrca,			fcha_crcn,
					usro_crcn,					fcha_ultma_mdfccn,				usro_ultma_mdfccn
		From		#tempMarcaAccesoDirecto
		Where		cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca7
	) As s
	On (
					t.cnsctvo_srvco_slctdo = s.cnsctvo_srvco_slctdo		And
					t.cnsctvo_cdgo_tpo_mrca = s.cnsctvo_cdgo_tpo_mrca
	)
	When Not Matched Then
			Insert
			(
					cnsctvo_srvco_slctdo,	cnsctvo_cdgo_tpo_mrca,		fcha_crcn,
					usro_crcn,				fcha_ultma_mdfccn,			usro_ultma_mdfccn
			)
			Values
			(
					s.cnsctvo_srvco_slctdo,		s.cnsctvo_cdgo_tpo_mrca,		s.fcha_crcn,								
					s.usro_crcn,				s.fcha_ultma_mdfccn,			s.usro_ultma_mdfccn
			)
	When Matched Then
			Update
			Set		t.cnsctvo_srvco_slctdo  = s.cnsctvo_srvco_slctdo,			
					t.cnsctvo_cdgo_tpo_mrca = s.cnsctvo_cdgo_tpo_mrca,			
					t.fcha_ultma_mdfccn     = s.fcha_ultma_mdfccn,									
					t.usro_ultma_mdfccn     = s.usro_ultma_mdfccn;

	Drop Table #tempMarcaAccesoDirecto;
	Drop Table #tempMarcaPrestacionMarca;
End