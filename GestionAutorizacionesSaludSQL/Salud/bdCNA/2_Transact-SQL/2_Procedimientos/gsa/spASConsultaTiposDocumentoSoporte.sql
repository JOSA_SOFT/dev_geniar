USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaTiposDocumentoSoporte]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultaTiposDocumentoSoporte  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar los tipos de documento soporte por prestacion o item de 
						 presupuesto
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/01/20 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D 
                        Se realizo modificacion del procedimiento para que consulte los documentos de donde se toman
						para mi solicitud
                       D\>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/    
/*
Declare @xml xml
Set @xml ='<tiposDocumento>
		     <prestacion>
		       <codigoprestacion>19943211-01</codigoprestacion>
	           <itempresupuesto>1</itempresupuesto>	
			   <tipocodificacion>4</tipocodificacion>		  
	         </prestacion>
	         <prestacion>
	           <codigoprestacion>300203-878</codigoprestacion>	
	           <itempresupuesto>1</itempresupuesto>		
			   <tipocodificacion>4</tipocodificacion>	  
             </prestacion>
	         <prestacion>
               <codigoprestacion>300203-876</codigoprestacion>	
               <itempresupuesto>1</itempresupuesto>	
			   <tipocodificacion>4</tipocodificacion>		  
             </prestacion>
	         <prestacion>
	           <codigoprestacion>19015-2</codigoprestacion>	 
	           <itempresupuesto>1</itempresupuesto>		
			   <tipocodificacion>4</tipocodificacion>	  
	         </prestacion>
		   </tiposDocumento>'

Exec [gsa].[spASConsultaTiposDocumentoSoporte] @xml, 4
*/
ALTER PROCEDURE [gsa].[spASConsultaTiposDocumentoSoporte]    
    @xml                            xml           ,
	@cnsctvo_cdgo_mdlo_dcmnto_sprte udtConsecutivo
AS  

Begin
    SET NOCOUNT ON 

	Declare @ldFechaActual  Datetime      , 
	        @cnsctvo_cdfccn udtConsecutivo,
			@cdgo_cdfccn    Char(11)
	

	Create 
	Table #tempInfoPrestaciones(cdgo_cdfccn				 Char(11)      ,							
								cnsctvo_cdgo_itm_prspsto udtConsecutivo,           
                                cnsctvo_cdfccn	         udtConsecutivo,
								cnsctvo_cdgo_tpo_cdfccn	 udtConsecutivo	 					
	                           )
	Create 
	Table #tempDocPrestaciones(cdgo_cdfccn                    Char(11)      ,
	                           cnsctvo_cdfccn	              udtConsecutivo,
	                           cnsctvo_cdgo_mdlo_dcmnto_sprte udtConsecutivo,
							   cnsctvo_cdgo_dcmnto_sprte      udtConsecutivo,
							   cdgo_dcmnto_sprte              udtCodigo     ,
	                           dscrpcn_dcmnto_sprte           udtDescripcion,
							   tmno_mxmo                      Int           ,
							   url_ejmplo                     udtDescripcion,
							   frmts_prmtdo                   Varchar(50)   ,
							   oblgtro                        udtLogico     ,
							   cntdd_mxma_sprts_crgr          udtConsecutivo,
							   cnsctvo_cdgo_tpo_imgn          udtConsecutivo
	                          )

	Set @ldFechaActual  = getDate()  
	Set @cnsctvo_cdfccn = 0 
	Set @cdgo_cdfccn    = ''
    

	Insert Into #tempInfoPrestaciones (cdgo_cdfccn, cnsctvo_cdgo_itm_prspsto, cnsctvo_cdgo_tpo_cdfccn)
	SELECT pref.value('(codigoprestacion/text())[1]','Char(11)')       AS codigoPrestacion,
           pref.value('(itempresupuesto/text())[1]' ,'udtConsecutivo') AS itemPresupuesto ,
		   pref.value('(tipocodificacion/text())[1]','udtConsecutivo') AS tipoCodificacion 
    FROM   @xml.nodes('/tiposDocumento//prestacion') AS xml_slctd(Pref)
	

	Update     #tempInfoPrestaciones
	Set        cnsctvo_cdfccn = b.cnsctvo_cdfccn
	From       #tempInfoPrestaciones a
	Inner Join bdSiSalud.dbo.tbCodificaciones b With(NoLock)
	On         b.cdgo_cdfccn = a.cdgo_cdfccn And
	           b.cnsctvo_cdgo_tpo_cdfccn = a.cnsctvo_cdgo_tpo_cdfccn
	

	/*Se inserta la tabla temporal con los documentos soporte requeridos, consultando por prestacion */
	Insert 
	Into       #tempDocPrestaciones(cnsctvo_cdfccn       , cnsctvo_cdgo_mdlo_dcmnto_sprte, cnsctvo_cdgo_dcmnto_sprte,
	                                cdgo_dcmnto_sprte    , dscrpcn_dcmnto_sprte          , tmno_mxmo                ,
									url_ejmplo           , frmts_prmtdo                  , oblgtro                  ,
									cntdd_mxma_sprts_crgr, cnsctvo_cdgo_tpo_imgn         , cdgo_cdfccn
	                               )    
	Select     a.cnsctvo_cdfccn                 , b.cnsctvo_cdgo_mdlo_dcmnto_sprte, c.cnsctvo_cdgo_dcmnto_sprte,
		       d.cdgo_dcmnto_sprte              , d.dscrpcn_dcmnto_sprte          , d.tmno_mxmo                ,
			   d.url_ejmplo                     , c.frmts_prmtdo                  , c.oblgtro                  ,
			   ISNULL(c.cntdd_mxma_sprts_crgr,1), d.cnsctvo_cdgo_tpo_imgn         , a.cdgo_cdfccn
	From       #tempInfoPrestaciones a
	Inner Join bdSisalud.dbo.tbPrestacionesxModeloDocumentosSoporte_vigencias b With(NoLock)
	On         b.cnsctvo_cdfccn = a.cnsctvo_cdfccn
	Inner Join bdSisalud.dbo.tbDetModeloDocumentosSoporte3047 c  With(NoLock)
	On         c.cnsctvo_cdgo_mdlo_dcmnto_sprte = b.cnsctvo_cdgo_mdlo_dcmnto_sprte
	Inner Join bdCNA.sol.tbDocumentosSoporte_Vigencias d  With(NoLock)
	On         d.cnsctvo_cdgo_dcmnto_sprte = c.cnsctvo_cdgo_dcmnto_sprte	
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	And        @ldFechaActual Between c.inco_vgnca And c.fn_vgnca
	And        @ldFechaActual Between d.inco_vgnca And d.fn_vgnca
	
	/*Se actualiza la tabla temporal con los documentos soporte requeridos, consultando por item de presupuesto*/ 
	Insert 
	Into       #tempDocPrestaciones(cnsctvo_cdfccn       , cnsctvo_cdgo_mdlo_dcmnto_sprte, cnsctvo_cdgo_dcmnto_sprte,
	                                cdgo_dcmnto_sprte    , dscrpcn_dcmnto_sprte          , tmno_mxmo                ,
									url_ejmplo           , frmts_prmtdo                  , oblgtro                  ,
									cntdd_mxma_sprts_crgr, cnsctvo_cdgo_tpo_imgn         , cdgo_cdfccn
	                               ) 
	Select     a.cnsctvo_cdfccn                 , b.cnsctvo_cdgo_mdlo_dcmnto_sprte, c.cnsctvo_cdgo_dcmnto_sprte,
		       d.cdgo_dcmnto_sprte              , d.dscrpcn_dcmnto_sprte          , d.tmno_mxmo                ,
			   d.url_ejmplo                     , c.frmts_prmtdo                  , c.oblgtro                  ,
			   ISNULL(c.cntdd_mxma_sprts_crgr,1), d.cnsctvo_cdgo_tpo_imgn         , a.cdgo_cdfccn
	From       #tempDocPrestaciones x
	Inner Join #tempInfoPrestaciones a
	On         a.cnsctvo_cdfccn = x.cnsctvo_cdfccn
	Inner Join bdSisalud.dbo.tbItemsPresupuestoxModeloDocumentosSoporte_vigencias b With(NoLock)
	On         b.cnsctvo_cdgo_itm_prspsto = a.cnsctvo_cdgo_itm_prspsto		
	Inner Join bdSisalud.dbo.tbDetModeloDocumentosSoporte3047 c  With(NoLock)
	On         c.cnsctvo_cdgo_mdlo_dcmnto_sprte = b.cnsctvo_cdgo_mdlo_dcmnto_sprte	
	Inner Join bdCNA.sol.tbDocumentosSoporte_Vigencias d  With(NoLock)
	On         d.cnsctvo_cdgo_dcmnto_sprte = c.cnsctvo_cdgo_dcmnto_sprte
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	And        @ldFechaActual Between c.inco_vgnca And c.fn_vgnca
	And        @ldFechaActual Between d.inco_vgnca And d.fn_vgnca
	And        x.dscrpcn_dcmnto_sprte IS NULL


	/*Se actualiza la tabla temporal con los documentos soporte requeridos, de mi solicitud*/ 
	IF NOT EXISTS (SELECT TOP 1 1 
	               From   #tempDocPrestaciones
			      )
		Begin			
		  Insert 
		  Into       #tempDocPrestaciones(cnsctvo_cdfccn       , cnsctvo_cdgo_mdlo_dcmnto_sprte, cnsctvo_cdgo_dcmnto_sprte,
                                          cdgo_dcmnto_sprte    , dscrpcn_dcmnto_sprte          , tmno_mxmo                ,
                                          url_ejmplo           , frmts_prmtdo                  , oblgtro                  ,
                                          cntdd_mxma_sprts_crgr, cnsctvo_cdgo_tpo_imgn         , cdgo_cdfccn
                                         ) 
		  Select Distinct  @cnsctvo_cdfccn                   , det.cnsctvo_cdgo_mdlo_dcmnto_sprte, doc.cnsctvo_cdgo_dcmnto_sprte,
		                   doc.cdgo_dcmnto_sprte             , doc.dscrpcn_dcmnto_sprte          , doc.tmno_mxmo                ,
						   doc.url_ejmplo                    , det.frmts_prmtdo                  , det.oblgtro                  ,
						   ISNULL(det.cntdd_mxma_sprts_crgr,1), doc.cnsctvo_cdgo_tpo_imgn         , @cdgo_cdfccn
		  From       bdCNA.sol.tbDocumentosSoporte_Vigencias doc With(NoLock)
		  Inner Join bdSisalud.dbo.tbDetModeloDocumentosSoporte3047 det With(NoLock) 
		  On         det.cnsctvo_cdgo_dcmnto_sprte = doc.cnsctvo_cdgo_dcmnto_sprte 
		  Where      det.cnsctvo_cdgo_mdlo_dcmnto_sprte = @cnsctvo_cdgo_mdlo_dcmnto_sprte
		  And        @ldFechaActual Between det.inco_vgnca And det.fn_vgnca
		  And        @ldFechaActual Between doc.inco_vgnca And doc.fn_vgnca
		End


	Select distinct cnsctvo_cdgo_dcmnto_sprte, cdgo_dcmnto_sprte    , dscrpcn_dcmnto_sprte ,
	                tmno_mxmo                , url_ejmplo           , frmts_prmtdo         , 
					oblgtro                  , cntdd_mxma_sprts_crgr, cnsctvo_cdgo_tpo_imgn,
					cdgo_cdfccn
	From   #tempDocPrestaciones	

	Drop Table #tempInfoPrestaciones
	Drop Table #tempDocPrestaciones
End
GO
