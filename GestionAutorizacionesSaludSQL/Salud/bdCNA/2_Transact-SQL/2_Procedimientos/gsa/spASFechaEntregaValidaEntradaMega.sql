USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASFechaEntregaValidaEntradaMega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASFechaEntregaValidaEntradaMega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite validar que la información que ingresa, 
                           si se encuentre en la base de datos, para así ser procesada en fecha de entrega. D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Felipe Arcos Velez  AM\>    
* Descripcion        : <\D  Se extrae esta logica la cual estaba en  [spASFechaEntrega]  D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 10/03/2016  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASFechaEntregaValidaEntradaMega] (
    @cnsctvo_prcso udtConsecutivo,
    @es_btch char(1)
) AS
BEGIN
  SET NOCOUNT ON;

  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_srvco_sld_rcbda)
  WHERE s.cnsctvo_slctd_srvco_sld_rcbda IS NOT NULL

  --Cuando solo llega el número solicitud
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_atrzcn_ss = s.nmro_slctd)
  WHERE s.nmro_slctd IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

  --Cuando solo llega el número solicitud prestador	
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_prvdr = s.nmro_slctd_prvdr)
  WHERE s.nmro_slctd_prvdr IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL
END

GO
