USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASImpresionAutorizacionServicioOPS]    Script Date: 04/07/2017 09:56:51 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASImpresionAutorizacionServicioOPS
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento expuesto que permite consultar la información asociada a la 
            		       ops generada para las prestaciones asociadas a la solicitud 
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           nmro_unco_ops  
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 31/03/2016 FC\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento para buscar en contratos 
						   validador sin el numero unico del cotizante por solicitud de Angela
						   Sandoval
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 20/06/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento cambian el nombre de la tabla 
						   bdCNA.gsa.TBASImpresionCuotasServiciosSolicitados por 
						   bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps y el nombre del campo 
						   vlr_lqdcn_ttl  por vlr_lqdcn_cta_rcprcn_srvco_ops, de acuerdo a la solicitud
						   realizada por Angela Sandoval
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 19/09/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jorge Rodriguez De León AM\>    
* Descripcion        : <\D  
                           Se realiza modificación para que el SP devuelva datos en el campo nmro_acta_ctc
						   Siempre y cuando no se notifique que el registro correspode a una tutela.
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 11/10/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento se actualiza la informacion 
						   del afiliado con los datos de la solicitud, debido a que no esta vigente y 
						   se realiza validacion especial para la generacion de la solicitud
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 24/10/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento para que muestre el medico no
						   adscrito
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 24/10/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento para que sume el valor calculado
						   de la cuota moderadora y copago
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 26/12/2016 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento para que actualice la oficina 
						   del usuario quien realiza la impresion de la OPS. 
						   Se adiciona como parametro el login del usuario para calcular la oficina 
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 28/03/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento para que actualice el codigo de las prestacionnes
						   para que tome los homologos de cada prestador						  
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 20/04/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento que llame al proceso de migracion, cuando se
						   requiere descargar una solicitud de programacion de entrega				  
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 06/06/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se realiza la modificacion en el procedimiento en donde se aumenta el tamaño del campo 
						   cdgo_cdfccn en Varchar(30)			  
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 07/06/2017 FM\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
**---------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                             Se modifica el procedimiento para que consulte las ciudades de
							 bdAfiliacionValidador y no de bdSiSalud
                        DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 20/06/2017  FM\>    
**---------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D  
                           Se ajusta procedimiento para obtener el recobro de las prestaciones y no del diagnostico.			  
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 21/06/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D  
                           Se ajusta procedimiento para que al cambiar el estado de los conceptos los cambie por prestacion y 
						   no por OPS.		  
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 30/06/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D  
                           Se ajusta procedimiento para que actualice los campos fcha_ultma_mdfccn y usro_ultma_mdfccn
						   en las tablas tbASServiciosSolicitados y tbASConceptosServicioSolicitado
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 04/07/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------*/

 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 92586104, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 92586634, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 92586777, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 88385251, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 88386693, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 88430196, 'user1'
 --exec [gsa].[spASImpresionAutorizacionServicioOPS] 88431234, 'user1'
 
 ALTER PROCEDURE [gsa].[spASImpresionAutorizacionServicioOPS] 
  @nmro_unco_ops udtConsecutivo,
  @usro_imprsn   udtUsuario
AS

Begin
   SET NOCOUNT ON

   Declare @ldFechaActual                        Datetime      , 
           @lcAux                                Char(1)       ,
		   @lcVacio                              Char(1)       ,
		   @lcInd                                Char(3)       ,
		   @lncnsctvo_cdgo_tpo_dgnstco           udtConsecutivo,
		   @lcdscrpcn_rcbro                      udtDescripcion,
		   @lnCnsctvo_cdgo_rcbro	             udtConsecutivo,
		   @lnZero                               Int           ,
		   @lcDscrpcn_mnsje	                     udtDescripcion,
		   @lnCnsctvo_cdgo_mnsje	             udtConsecutivo,
		   @lnCnsctvo_cdgo_estdo                 udtConsecutivo,
		   @lnCnsctvo_cdgo_estdo_atrzda          udtConsecutivo,
		   @lnCncpto_pgo_cta_mdrdra              Int           ,
		   @lnCncpto_pgo_cpago                   Int           ,
		   @clasifevento				         Int           ,
		   @notificado					         Int		   ,
		   @confirmado					         Int           ,
		   @ms_tutela					         udtDescripcion,
		   @legal						         udtDescripcion,
		   @cnsctvo_cdgo_ofcna                   udtConsecutivo,
		   @cnsctvo_cdgo_tpo_cdfccn_cums         udtConsecutivo,
		   @cnsctvo_cdgo_tpo_cdfccn_cups         udtConsecutivo,
		   @cnsctvo_cdgo_tpo_cdfccn_pis          udtConsecutivo,
		   @cdgo_mdo_cntcto_slctd_prgrmcn_entrga udtConsecutivo,
		   @cnsctvo_slctd_atrzcn_srvco           udtConsecutivo,
		   @lncnsctvo_cdgo_clsfccn_prstcn_prstdr udtConsecutivo;       
   
   Create
   Table  #tempGrupoImpresion(nmro_unco_ops	                udtConsecutivo,							  
							  cnsctvo_prcdmnto_insmo_slctdo udtConsecutivo,
							  cnsctvo_mdcmnto_slctdo        udtConsecutivo,
							  cnsctvo_cdgo_grpo_imprsn      udtConsecutivo,
							  vlr_lqddo_grpo_imprsn         Int           ,
							  cnsctvo_srvco_slctdo          udtConsecutivo,
							  fcha_utlzcn_dsde              Datetime      ,
                              fcha_utlzcn_hsta              Datetime      ,
							  cdgo_intrno_prstdr_atrzdo     udtCodigoIps  ,
							  ds_vldz                       Int
						     )
   Create 
   Table  #ValorTotalOPS(nmro_unco_ops	      udtConsecutivo         ,
                         vlr_lqdcn_ttl_ops    Decimal(18,0) Default 0						 
                        )

   Create 
   Table  #informacionOPS(nmro_unco_ops	                           udtConsecutivo              ,
                          cnsctvo_slctd_atrzcn_srvco               udtConsecutivo              ,
						  cnsctvo_cdgo_srvco_slctdo                udtConsecutivo              ,
						  cnsctvo_prcdmnto_insmo_slctdo            udtConsecutivo              ,
						  cnsctvo_mdcmnto_slctdo                   udtConsecutivo              , 
						  cnsctvo_cdgo_tpo_idntfccn                udtConsecutivo              ,
						  cdgo_tpo_idntfccn			               Char(3)                     ,
                          nmro_idntfccn			 	               udtNumeroIdentificacionLargo,
                          nmro_unco_idntfccn_afldo	               udtConsecutivo              ,	                  
                          prmr_aplldo	                           udtApellido                 ,
                          sgndo_aplldo		                       udtApellido                 ,
                          prmr_nmbre		                       udtNombre                   ,
                          sgndo_nmbre		                       udtNombre                   ,
						  smns_aflcn                               udtConsecutivo              ,
						  nmbre_ctznte                             Varchar(200)   Default ''   ,                         
                          cnsctvo_cdgo_pln			               udtConsecutivo              ,			
                          dscrpcn_pln				               udtDescripcion              ,
						  drccn_afldo                              udtDireccion                ,
						  tlfno_afldo                              udtTelefono                 ,
						  cnsctvo_cdgo_dprtmnto_afldo              udtConsecutivo              ,
						  dscrpcn_dprtmnto_afldo		           udtDescripcion              ,
						  cnsctvo_cdgo_cdd_rsdnca                  udtConsecutivo              ,
						  dscrpcn_cdd_rsdnca_afldo                 udtDescripcion              ,
						  fcha_ncmnto_afldo                        Datetime                    , 
						  tlfno_cllr_afldo                         Char(30)        Default ''  ,
						  crro_elctrnco_afldo                      udtEmail        Default ''  ,
						  cnsctvo_cdgo_tpo_cntrto                  udtConsecutivo              ,
						  nmro_cntrto                              udtNumeroFormulario         ,
						  cnsctvo_cdgo_tpo_vnclcn_afldo            udtConsecutivo              ,
						  dscrpcn_prntsco                          udtDescripcion              ,
						  nmbre_empldr                             Varchar(200)    Default ''  ,
						  cdgo_tpo_idntfccn_empldr                 Char(3)         Default ''  ,
						  nmro_idntfccn_empldr                     udtNumeroIdentificacionLargo Default '',
						  nmro_unco_idntfccn_mdco                  udtConsecutivo              ,						  
						  nmbre_mdco_trtnte                        Varchar(200)    Default ''  ,   
						  cdgo_cdfccn	                           Varchar(30)                 ,       
                          dscrpcn_cdfccn                           udtdescripcion              ,
						  cnsctvo_cdgo_cncpto_gsto                 udtConsecutivo              ,
                          dscrpcn_cncpto_gsto                      udtdescripcion              ,
						  cnsctvo_cdgo_grpo_imprsn                 udtConsecutivo              ,
						  dscrpcn_grpo_imprsn	                   udtDescripcion              ,
                          rzn_scl_ips                              udtdescripcion  Default ''  ,
						  cnsctvo_cdgo_tpo_idntfccn_ips            udtConsecutivo              ,
						  cdgo_tpo_idntfccn_ips                    Char(3)         Default ''  ,
						  nmro_idntfccn_ips                        udtNumeroIdentificacionLargo Default '',
						  dgto_vrfccn					           Int             Default 0   ,
						  drccn_ips                                udtDireccion    Default ''  ,
                          tlfno_ips                                udtTelefono     Default ''  ,
						  cnsctvo_cdgo_cdd_ips                     udtConsecutivo              ,
						  dscrpcn_cdd_ips                          udtdescripcion  Default ''  , 
						  cnsctvo_cdgo_dprtmnto_ips                udtConsecutivo              ,
						  dscrpcn_dprtmnto_ips                     udtdescripcion  Default ''  ,
						  cdgo_prstdr_mnstro                       Char(12)        Default ''  ,
						  indctvo_prstdr                           Char(3)         Default ''  ,
						  nmro_slctd_atrzcn_ss                     Varchar(16)                 ,
						  nmro_slctd_prvdr                         Varchar(15)                 ,
						  obsrvcn_adcnl                            udtObservacion  Default ''  ,  
						  cnsctvo_cdgo_frma_atncn                  udtConsecutivo              ,
						  cnsctvo_cdgo_clse_atncn                  udtConsecutivo              ,
                          fcha_slctd_tmp                           Datetime                    ,
						  cnsctvo_cdgo_tpo_srvco_slctdo            udtConsecutivo              ,
						  cnsctvo_cdgo_tpo_ubccn_pcnte             udtConsecutivo              ,
						  vlr_prstcn                               Decimal(18, 0)              ,
						  vlr_acmldo                               Int                         ,
						  fcha_ingrso_tmp                          Datetime                    ,
						  cma                                      Varchar(10)     Default ''  ,
						  crte_cnta                                Int             Default 0   ,
						  cnsctvo_cdgo_clse_hbtcn                  udtConsecutivo              ,
						  dscrpcn_clse_hbtcn	                   udtDescripcion  Default ''  ,
						  cntdd_prstcn                             Int                         ,                          
                          fcha_imprsn                              Datetime                    ,
                          fcha_utlzcn_dsde                         Datetime                    ,
                          fcha_utlzcn_hsta                         Datetime                    ,
                          fcha_rdccn                               Datetime                    ,						  
                          nmro_unco_idntfccn_prstdr                udtconsecutivo              ,
						  cnsctvo_srvco_slctdo                     udtconsecutivo              ,
						  fcha_dsde						           VarChar(20)                 ,
		                  fcha_hsta						           VarChar(20)                 ,
						  fcha_slctd                               VarChar(20)                 ,
						  fcha_ncmnto                              Varchar(20)                 ,
						  fcha_ingrso                              Varchar(20)                 ,
						  hra							           Varchar(5)      Default ''  ,
						  hra_fcha_slctd                           Varchar(5)      Default ''  , 
						  cdgo_intrno_prstdr_atrzdo                udtCodigoIps                ,						 
						  cdgo_dgnstco	                           udtCodigoDiagnostico        ,
						  cnsctvo_cdgo_rcbro                       udtconsecutivo              ,
						  dscrpcn_rcbro                            udtdescripcion  Default ''  ,
						  cnsctvo_cdgo_cntngnca                    udtconsecutivo              ,
						  dscrpcn_cntngnca                         udtdescripcion  Default ''  ,
						  dscrpcn_tpo_ubccn_pcnte                  udtdescripcion  Default ''  ,
						  dscrpcn_tpo_srvco_slctdo                 udtdescripcion  Default ''	,
						  cdgo_ltrldd                              Char(2)	       Default ''	,
						  dscrpcn_gs_atncn                         Varchar(30)	   Default ''  ,
						  usro_atrza                               Varchar(100)    Default ''  ,
						  dscrpcn_crgo                             udtdescripcion  Default ''  ,
	                      tlfn_ofcna                               Varchar(50)     Default ''  ,
						  tlfn_cllr_atrzdr                         Char(30)        Default ''  ,
						  tlfn_ofcna_tmp                           udtTelefono     Default ''  ,
	                      indctvo_atrzdr                           Char(3)         Default ''  ,
						  mdss                                     Char(1)		   Default ''	 ,
						  cnsctvo_cdgo_dt_prgrmcn_fcha_evnto       udtconsecutivo  Default 0   ,
						  cnsctvo_cdgo_ntfccn	                   udtConsecutivo  Default 0   ,
						  nmro_acta_ctc                            Varchar(50)     Default ''  ,
						  cnsctvo_cdgo_ofcna_noti	               udtConsecutivo              ,
						  cmrcl							           udtConsecutivo  Default 0   ,
						  dscrpcn_mnsje                            udtdescripcion  Default ''  ,
						  cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco udtConsecutivo              ,
						  vlr_lqdcn_srvco                          Decimal(18,0)   Default 0   ,
						  ds_vldz                                  Int             Default 0   ,
						  cnsctvo_cdgo_ofcna                       udtConsecutivo              ,
						  dscrpcn_ofcna	                           udtdescripcion  Default ''  ,
						  prcntje_lqdcn							   Int             Default 0   ,
						  cpgo_pgo								   Float           Default 0   ,
		                  cta_mdrdra		                       Float           Default 0   ,
						  mrca_rcdo_cta_mdrdra                     udtLogico       Default ''  ,
						  mrca_rcdo_cpago                          udtLogico       Default ''  ,
						  vlr_lqdcn_ttl_srvco                      Float           Default 0   ,
						  vlr_lqdcn_ttl_cpago                      Float           Default 0   ,
						  vlr_lqdcn_cta_mdrdra                     Float           Default 0   ,
						  vlr_lqdcn_ttl_ops                        Decimal(18,0)   Default 0   ,
						  usro_imprsn_ops                          udtUsuario                  ,
						  cnsctvo_cdgo_mdo_cntcto_slctd            udtConsecutivo
						)
						
	Set @ldFechaActual                        = getDate()
	Set @lcAux                                = ' '
	Set @lcVacio                              = ''
	Set @lcInd                                = ' - '
	Set @lnCnsctvo_cdgo_tpo_dgnstco           = 1
	Set @lcDscrpcn_rcbro                      = 'PROGRAMA ESPECIAL'
	Set @lnCnsctvo_cdgo_rcbro                 = 16
	Set @lnZero                               = 0 
	Set @lnCnsctvo_cdgo_mnsje                 = 1	
	Set @lnCnsctvo_cdgo_estdo_atrzda          = 11 -- Autorizada
	Set @lnCnsctvo_cdgo_estdo                 = 15 -- Entregada
	Set @lnCncpto_pgo_cta_mdrdra              = 1  -- concepto de pago: Cuota moderadora
	Set @lnCncpto_pgo_cpago                   = 2  -- concepto de pago: Copago
	Set @clasifevento				          = 8
	Set @notificado					          = 14
	Set @confirmado					          = 15
	Set @ms_tutela					          = 'TUTELA'
	Set @cnsctvo_cdgo_tpo_cdfccn_cums         = 5  --Medicamentos
	Set @cnsctvo_cdgo_tpo_cdfccn_pis          = 9  --PIS
	Set @cnsctvo_cdgo_tpo_cdfccn_cups         = 4  --CUPS
	Set @lncnsctvo_cdgo_clsfccn_prstcn_prstdr = 3
	Set @cdgo_mdo_cntcto_slctd_prgrmcn_entrga = 5  --Medio de contacto Programacion de Entrega
		

	----Se inserta en la tabla temporal la informacion de los conceptos de gasto agrupados por grupos de impresion
	Insert 
	Into     #tempGrupoImpresion(cnsctvo_cdgo_grpo_imprsn, cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo, 
			                     nmro_unco_ops           , cdgo_intrno_prstdr_atrzdo    , cnsctvo_srvco_slctdo  ,
								 fcha_utlzcn_dsde        , fcha_utlzcn_hsta             , ds_vldz               ,
								 vlr_lqddo_grpo_imprsn    										 
								)
	Select     b.cnsctvo_cdgo_grpo_imprsn , b.cnsctvo_prcdmnto_insmo_slctdo, b.cnsctvo_mdcmnto_slctdo, 
			   b.nmro_unco_ops            , b.cdgo_intrno_prstdr_atrzdo    , p.cnsctvo_srvco_slctdo  ,
			   b.fcha_utlzcn_dsde         , b.fcha_utlzcn_hsta             , b.ds_vldz               ,
			   Sum(b.vlr_lqdcn)
	From       bdCNA.gsa.tbASConceptosServicioSolicitado b WITH (NOLOCK)
	Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados p WITH (NOLOCK)
    On         p.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo    
	Where      b.nmro_unco_ops = @nmro_unco_ops
	And        (b.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo_atrzda OR b.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo)
	Group By   b.cnsctvo_cdgo_grpo_imprsn , b.cnsctvo_prcdmnto_insmo_slctdo, b.cnsctvo_mdcmnto_slctdo, 
	           b.nmro_unco_ops            , b.cdgo_intrno_prstdr_atrzdo    , p.cnsctvo_srvco_slctdo  ,
	           b.fcha_utlzcn_dsde         , b.fcha_utlzcn_hsta             , b.ds_vldz 

	Insert 
	Into       #tempGrupoImpresion(cnsctvo_cdgo_grpo_imprsn, cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo, 
			                       nmro_unco_ops           , cdgo_intrno_prstdr_atrzdo    , cnsctvo_srvco_slctdo  ,
								   fcha_utlzcn_dsde        , fcha_utlzcn_hsta             , ds_vldz               ,
								   vlr_lqddo_grpo_imprsn   										 
								  )
	Select     b.cnsctvo_cdgo_grpo_imprsn , b.cnsctvo_prcdmnto_insmo_slctdo, b.cnsctvo_mdcmnto_slctdo, 
			   b.nmro_unco_ops            , b.cdgo_intrno_prstdr_atrzdo    , m.cnsctvo_srvco_slctdo  ,
			   b.fcha_utlzcn_dsde         , b.fcha_utlzcn_hsta             , b.ds_vldz               ,
			   Sum(b.vlr_lqdcn)
	From       bdCNA.gsa.tbASConceptosServicioSolicitado b WITH (NOLOCK)
	Inner Join bdcna.gsa.tbASMedicamentosSolicitados m WITH (NOLOCK)
    On         m.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo   
	Where      b.nmro_unco_ops = @nmro_unco_ops
	And        (b.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo_atrzda OR b.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo)
	Group By   b.cnsctvo_cdgo_grpo_imprsn , b.cnsctvo_prcdmnto_insmo_slctdo, b.cnsctvo_mdcmnto_slctdo, 
			   b.nmro_unco_ops            , b.cdgo_intrno_prstdr_atrzdo    , m.cnsctvo_srvco_slctdo  ,
			   b.fcha_utlzcn_dsde         , b.fcha_utlzcn_hsta             , b.ds_vldz 
					   
	---Se inserta en la tabla temporal la informacion de los procedimientos		
	Insert
	Into       #informacionOPS(cnsctvo_cdgo_srvco_slctdo, cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo            , 
                               vlr_acmldo               , fcha_imprsn                  , cnsctvo_slctd_atrzcn_srvco        ,
	                           nmro_unco_ops            , vlr_prstcn                   , cntdd_prstcn                      ,
		                       cnsctvo_srvco_slctdo     , cdgo_intrno_prstdr_atrzdo    , fcha_utlzcn_dsde                  ,  
	                           fcha_utlzcn_hsta         , cnsctvo_cdgo_grpo_imprsn     , cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
							   vlr_lqdcn_srvco          , ds_vldz                      , vlr_lqdcn_ttl_ops                 ,
							   cnsctvo_cdgo_rcbro
				              )
	Select     a.cnsctvo_cdgo_srvco_slctdo, s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo            , 
               a.vlr_lqdcn_srvco          , a.fcha_imprsn                  , a.cnsctvo_slctd_atrzcn_srvco        ,
	           s.nmro_unco_ops            , s.vlr_lqddo_grpo_imprsn        , a.cntdd_slctda                      ,
	           a.cnsctvo_srvco_slctdo     , s.cdgo_intrno_prstdr_atrzdo    , s.fcha_utlzcn_dsde                  , 
	           s.fcha_utlzcn_hsta         , s.cnsctvo_cdgo_grpo_imprsn     , a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
			   a.vlr_lqdcn_srvco          , s.ds_vldz                      , s.vlr_lqddo_grpo_imprsn			 ,
			   a.cnsctvo_cdgo_rcbro
    From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)            
    Inner Join #tempGrupoImpresion s 
	On         s.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo  	
	Where      a.cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo_atrzda	--Estado de la prestacion AUTORIZADA	         
	Union	
	Select     a.cnsctvo_cdgo_srvco_slctdo, s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo            , 
               a.vlr_lqdcn_srvco          , a.fcha_imprsn                  , a.cnsctvo_slctd_atrzcn_srvco        ,
	           s.nmro_unco_ops            , s.vlr_lqddo_grpo_imprsn        , a.cntdd_slctda                      ,
	           a.cnsctvo_srvco_slctdo     , s.cdgo_intrno_prstdr_atrzdo    , s.fcha_utlzcn_dsde                  , 
	           s.fcha_utlzcn_hsta         , s.cnsctvo_cdgo_grpo_imprsn     , a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
			   a.vlr_lqdcn_srvco          , s.ds_vldz                      , s.vlr_lqddo_grpo_imprsn			 ,
			   a.cnsctvo_cdgo_rcbro
    From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)            
    Inner Join #tempGrupoImpresion s 
	On         s.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo 
	Where      a.cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo --Estado de la prestacion ENTREGADA

	/*Se inserta en la tabal temporal el valor total de la OPS*/
	Insert 
	Into       #ValorTotalOPS(nmro_unco_ops, vlr_lqdcn_ttl_ops)
	Select     nmro_unco_ops, SUM(vlr_lqdcn_ttl_ops)
	From       #informacionOPS
	Group By   nmro_unco_ops

	/*Se actualiza el valor total de la ops*/
	Update     #informacionOPS
	Set        vlr_lqdcn_ttl_ops = b.vlr_lqdcn_ttl_ops
	From       #informacionOPS a
	Inner Join #ValorTotalOPS b
	On         b.nmro_unco_ops = a.nmro_unco_ops

	/*Se actualiza la descripcion del concepto de gasto*/
    Update     #informacionOPS
    Set        dscrpcn_cncpto_gsto = b.dscrpcn_cncpto_gsto
    From       #informacionOPS a
    Inner Join bdSiSalud.dbo.tbConceptosGasto_Vigencias b WITH (NOLOCK)
    On         b.cnsctvo_cdgo_cncpto_gsto = a.cnsctvo_cdgo_cncpto_gsto
    Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	/*grupo de impresion*/      
	Update		#informacionOPS        
	Set			dscrpcn_grpo_imprsn	= b.dscrpcn_grpo_imprsn        
	From		#informacionOPS a
	Inner Join	bdSiSalud.dbo.tbGrupoImpresion_Vigencias b With (NoLock) 
	On          a.cnsctvo_cdgo_grpo_imprsn = b.cnsctvo_cdgo_grpo_imprsn 
	Where       @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	
	---Se actualiza la informacion del afiliado
	Update      #informacionOPS
	Set         cnsctvo_cdgo_pln                         = b.cnsctvo_cdgo_pln                        ,		       
				nmro_idntfccn			                 = b.nmro_idntfccn_afldo                     ,  
                nmro_unco_idntfccn_afldo                 = b.nmro_unco_idntfccn_afldo                ,
				cnsctvo_cdgo_tpo_cntrto                  = b.cnsctvo_cdgo_tpo_cntrto                 ,					          
				nmro_cntrto                              = b.nmro_cntrto                             ,
				cnsctvo_cdgo_tpo_idntfccn                = b.cnsctvo_cdgo_tpo_idntfccn_afldo         ,
				cnsctvo_cdgo_tpo_vnclcn_afldo            = b.cnsctvo_cdgo_tpo_vnclcn_afldo           ,
				cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = b.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco                     				
    From        #informacionOPS a
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
	On          b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco

	--se actualiza la informacion de la empresa del trabajador
	Update      #informacionOPS
	Set         cdgo_tpo_idntfccn_empldr = t.cdgo_tpo_idntfccn             ,
	            nmbre_empldr             = IsNull(b.nmbre_empldr, @lcVacio),
				nmro_idntfccn_empldr     = IsNull(b.nmro_idntfccn_empldr, @lcVacio)   
    From        #informacionOPS a
	Inner Join  bdCNA.gsa.tbAsInformacionEmpresasxAfiliado b WITH (NOLOCK)
	On          b.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = a.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	Inner Join  bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias t WITH(NOLOCK)
	On          t.cnsctvo_cdgo_tpo_idntfccn = b.cnsctvo_cdgo_tpo_idntfccn_empldr
	Where       @ldFechaActual Between t.inco_vgnca And t.fn_vgnca

	---Se actualiza la informacion basica del afiliado
	Update      #informacionOPS
	Set         prmr_aplldo	            = Ltrim(Rtrim(c.prmr_aplldo))     ,				             
                sgndo_aplldo		    = Ltrim(Rtrim(c.sgndo_aplldo))    ,                   
                prmr_nmbre		        = Ltrim(Rtrim(c.prmr_nmbre))      ,                
                sgndo_nmbre		        = Ltrim(Rtrim(c.sgndo_nmbre))     , 
				drccn_afldo             = c.drccn_rsdnca                  , 
				tlfno_afldo             = IsNull(c.tlfno_rsdnca, @lcVacio),
				crro_elctrnco_afldo     = IsNull(c.eml, @lcVacio)         ,				
                fcha_ncmnto_afldo       = c.fcha_ncmnto                   ,
				smns_aflcn              = c.smns_ctzds                    ,
				cnsctvo_cdgo_cdd_rsdnca = c.cnsctvo_cdgo_cdd_rsdnca
	From        #informacionOPS a	
	Inner Join  bdAfiliacionValidador.dbo.tbBeneficiariosValidador c WITH (NOLOCK)
	On          c.cnsctvo_cdgo_tpo_cntrto  = a.cnsctvo_cdgo_tpo_cntrto  And
	            c.nmro_cntrto              = a.nmro_cntrto              And
				c.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn_afldo
	Where       @ldFechaActual Between c.inco_vgnca_bnfcro And c.fn_vgnca_bnfcro  

	--Se actualiza la informacion del afiliado con los datos de la solicitud, debido a que no esta vigente
	Update      #informacionOPS
	Set         prmr_aplldo	            = Ltrim(Rtrim(c.prmr_aplldo_afldo))      ,				             
                sgndo_aplldo		    = Ltrim(Rtrim(c.sgndo_aplldo_afldo))     ,                   
                prmr_nmbre		        = Ltrim(Rtrim(c.prmr_nmbre_afldo))       ,                
                sgndo_nmbre		        = Ltrim(Rtrim(c.sgndo_nmbre_afldo))      , 
				drccn_afldo             = IsNull(c.drccn_afldo , @lcVacio)       , 
				tlfno_afldo             = IsNull(c.tlfno_afldo , @lcVacio)       ,
				crro_elctrnco_afldo     = IsNull(c.crro_elctrnco_afldo, @lcVacio),				
                fcha_ncmnto_afldo       = c.fcha_ncmnto_afldo                    ,
				smns_aflcn              = b.smns_ctzds                           ,
				cnsctvo_cdgo_cdd_rsdnca = b.cnsctvo_cdgo_cdd_rsdnca_afldo
	From        #informacionOPS a	
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
	On          b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
	On          c.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = b.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	Where       a.prmr_nmbre IS NULL
	
	--actualiza el codigo del tipo de identificacion del afiliado
	Update     #informacionOPS
	Set        cdgo_tpo_idntfccn = t.cdgo_tpo_idntfccn	           
	From       #informacionOPS a
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
	On         t.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn
	Where      @ldFechaActual Between t.inco_vgnca And t.fn_vgnca
		
	--actualiza la informacion de la ciudad del afiliado
	Update     #informacionOPS
	Set        dscrpcn_cdd_rsdnca_afldo    = b.dscrpcn_cdd          ,	        
	           cnsctvo_cdgo_dprtmnto_afldo = b.cnsctvo_cdgo_dprtmnto                              
	From       #informacionOPS a
	Inner Join BDAfiliacionValidador.dbo.tbCiudades_vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_cdd = a.cnsctvo_cdgo_cdd_rsdnca          
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca	
	
	--actualiza la informacion del departamento del afiliado
	Update     #informacionOPS	   
	Set        dscrpcn_dprtmnto_afldo = b.dscrpcn_dprtmnto
	From       #informacionOPS a
	Inner Join bdAfiliacionValidador.dbo.tbDepartamentos_vigencias b WITH(NOLOCK)	
	On         b.cnsctvo_cdgo_dprtmnto = a.cnsctvo_cdgo_dprtmnto_afldo	    
    Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--actualiza la informacion del nexo familiar del afiliado
	Update     #informacionOPS  
    Set        dscrpcn_prntsco = b.dscrpcn_prntsco      
	From       #informacionOPS a      
	Inner Join bdafiliacionvalidador.dbo.tbParentescos_Vigencias b WITH(NOLOCK) 
	On         b.cnsctvo_cdgo_prntscs = a.cnsctvo_cdgo_tpo_vnclcn_afldo      
    Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca	

	---Se actualiza el nombre del trabajador
	Update     #informacionOPS 
    Set        nmbre_ctznte = CONCAT(RTRIM(c.prmr_nmbre)  , @lcAux, 
	                                 RTRIM(c.sgndo_nmbre) , @lcAux, 
									 RTRIM(c.prmr_aplldo) , @lcAux, 
									 RTRIM(c.sgndo_aplldo)
									)      
    From       #informacionOPS a      
    Inner Join bdafiliacionvalidador.dbo.tbContratosValidador c  WITH (NOLOCK)
    On         c.cnsctvo_cdgo_tpo_cntrto  = a.cnsctvo_cdgo_tpo_cntrto  And
               c.nmro_cntrto              = a.nmro_cntrto             
    Where      @ldFechaActual Between c.inco_vgnca_cntrto And c.fn_vgnca_cntrto   
	
	---Se actualiza la descripcion del plan
	Update      #informacionOPS
	Set         dscrpcn_pln = b.dscrpcn_pln
	From        #informacionOPS a
	Inner Join  bdAfiliacionValidador.dbo.tbPlanes_Vigencias b WITH (NOLOCK)
	On          b.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Where       @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	/*Se actualiza la informacion del medico tratante*/
	Update     #informacionOPS
	Set        nmro_unco_idntfccn_mdco = b.nmro_unco_idntfccn_mdco
	From       #informacionOPS a
	Inner Join bdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios b WITH (NOLOCK)
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
   
	Update     #informacionOPS
	Set        nmbre_mdco_trtnte = CONCAT(RTRIM(c.prmr_aplldo)      , @lcAux, 
	                                      RTRIM(c.sgndo_aplldo)     , @lcAux, 
										  RTRIM(c.prmr_nmbre_afldo) , @lcAux, 
										  RTRIM(c.sgndo_nmbre_afldo)
										 )
	From       #informacionOPS a
	Inner Join bdSisalud.dbo.tbMedicos c WITH (NOLOCK)
	On         c.nmro_unco_idntfccn_prstdr = a.nmro_unco_idntfccn_mdco

	/*Se actualiza la informacion del medico tratante no adscrito*/
	Update     #informacionOPS
	Set        nmbre_mdco_trtnte = CONCAT(RTRIM(c.prmr_nmbre_mdco_trtnte)  , @lcAux, 
	                                      RTRIM(c.sgndo_nmbre_mdco_trtnte) , @lcAux, 
										  RTRIM(c.prmr_aplldo_mdco_trtnte) , @lcAux, 
										  RTRIM(c.sgndo_aplldo_mdco_trtnte)
										 )
	From       #informacionOPS a
	Inner Join bdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
	On         c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where      nmbre_mdco_trtnte = @lcAux

	/*Se actualiza la informacion del prestador*/
    Update     #informacionOPS
    Set        nmro_unco_idntfccn_prstdr     = p.nmro_unco_idntfccn_prstdr,
               rzn_scl_ips                   = p.nmbre_scrsl              ,
			   cnsctvo_cdgo_tpo_idntfccn_ips = e.cnsctvo_cdgo_tpo_idntfccn,	           
	           nmro_idntfccn_ips             = e.nmro_idntfccn            ,
	           dgto_vrfccn					 = e.dgto_vrfccn              ,
	           drccn_ips                     = p.drccn                    ,
	           tlfno_ips                     = p.tlfno                    ,
	           cnsctvo_cdgo_cdd_ips          = p.cnsctvo_cdgo_cdd         ,	          
	           cdgo_prstdr_mnstro            = p.cdgo_prstdr_mnstro       			        
    From       #informacionOPS a    
	Inner Join bdSisalud.dbo.tbDireccionesPrestador p WITH(NOLOCK)
	On         p.cdgo_intrno = a.cdgo_intrno_prstdr_atrzdo
	Inner Join bdSisalud.dbo.tbPrestadores e WITH(NOLOCK)
	On         e.nmro_unco_idntfccn_prstdr = p.nmro_unco_idntfccn_prstdr	
	
	--actualiza el codigo del tipo de identificacion del prestador
	Update     #informacionOPS
	Set        cdgo_tpo_idntfccn_ips = t.cdgo_tpo_idntfccn
	From       #informacionOPS a
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
	On         t.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn_ips
	Where      @ldFechaActual Between t.inco_vgnca And t.fn_vgnca
		
	--actualiza la informacion de la ciudad del prestador
	Update     #informacionOPS
	Set        dscrpcn_cdd_ips           = b.dscrpcn_cdd          ,
	           indctvo_prstdr            = b.indctvo              ,             
	           cnsctvo_cdgo_dprtmnto_ips = b.cnsctvo_cdgo_dprtmnto                              
	From       #informacionOPS a
	Inner Join bdAfiliacionValidador.dbo.tbCiudades_vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_cdd = a.cnsctvo_cdgo_cdd_ips          
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca	
	
	--actualiza la informacion del departamento del prestador
	Update     #informacionOPS	   
	Set        dscrpcn_dprtmnto_ips = b.dscrpcn_dprtmnto
	From       #informacionOPS a
	Inner Join bdAfiliacionValidador.dbo.tbDepartamentos_vigencias b WITH(NOLOCK)	
	On         b.cnsctvo_cdgo_dprtmnto = a.cnsctvo_cdgo_dprtmnto_ips	    
    Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca	    
	
	--actualiza la informacion de la solicitud
	Update     #informacionOPS	
	Set        nmro_slctd_atrzcn_ss          = b.nmro_slctd_atrzcn_ss         ,
               nmro_slctd_prvdr              = b.nmro_slctd_prvdr             ,
               cnsctvo_cdgo_frma_atncn       = b.cnsctvo_cdgo_frma_atncn      ,
               cnsctvo_cdgo_clse_atncn       = b.cnsctvo_cdgo_clse_atncn      ,
			   fcha_slctd_tmp                = b.fcha_slctd                   ,
			   cnsctvo_cdgo_tpo_srvco_slctdo = b.cnsctvo_cdgo_tpo_srvco_slctdo,
	           cnsctvo_cdgo_tpo_ubccn_pcnte  = b.cnsctvo_cdgo_tpo_ubccn_pcnte ,
			   dscrpcn_gs_atncn              = b.ga_atncn                     ,
			   obsrvcn_adcnl                 = b.obsrvcn_adcnl				  ,
			   cnsctvo_cdgo_mdo_cntcto_slctd = b.cnsctvo_cdgo_mdo_cntcto_slctd
	From       #informacionOPS a
	Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH(NOLOCK) 
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco      	

	--actualiza la informacion de la descripcion de la ubicacion paciente
	Update     #informacionOPS
	Set        dscrpcn_tpo_ubccn_pcnte = LTRIM(RTRIM((b.dscrpcn_tpo_ubccn_pcnte)))
	From       #informacionOPS a
	Inner Join bdSiSalud.dbo.tbPMTipoUbicacionPaciente_vigencias b WITH(NOLOCK) 
	On         b.cnsctvo_cdgo_tpo_ubccn_pcnte = a.cnsctvo_cdgo_tpo_ubccn_pcnte
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--actualiza la informacion de la descripcion del servicio solicitado
	Update     #informacionOPS
	Set        dscrpcn_tpo_srvco_slctdo = LTRIM(RTRIM((b.dscrpcn_tpo_srvco_slctdo)))
	From       #informacionOPS a
	Inner Join bdSiSalud.dbo.tbPMTipoServicioSolicitado_vigencias b WITH(NOLOCK) 
	On         b.cnsctvo_cdgo_tpo_srvco_slctdo = a.cnsctvo_cdgo_tpo_srvco_slctdo
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca   
	
	--actualiza la informacion del codigo del diagnostico principal
	Update     #informacionOPS
	Set        cdgo_dgnstco          = b.cdgo_dgnstco         ,
	           cnsctvo_cdgo_cntngnca = d.cnsctvo_cdgo_cntngnca--,
			   -- cnsctvo_cdgo_rcbro    = d.cnsctvo_cdgo_rcbro
	From       #informacionOPS a
    Inner Join bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios d	WITH(NOLOCK)
	ON         d.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco	       
	Inner Join bdsisalud.dbo.tbDiagnosticos b WITH(NOLOCK)
    ON         b.cnsctvo_cdgo_dgnstco = d.cnsctvo_cdgo_dgnstco
	Where      d.cnsctvo_cdgo_tpo_dgnstco = @lnCnsctvo_cdgo_tpo_dgnstco
	
	--actualiza la informacion de la descripcion de la contingencia
	Update     #informacionOPS
	Set        dscrpcn_cntngnca = b.dscrpcn_cntngnca
	From       #informacionOPS a
	Inner Join bdSisalud.dbo.tbContingencias_Vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_cntngnca = a.cnsctvo_cdgo_cntngnca
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--actualiza la informacion de la descripcion del recobro
	Update		#informacionOPS        
	Set			dscrpcn_rcbro = @lcdscrpcn_rcbro        
	From		#informacionOPS	a
	Inner Join	bdSiSalud.dbo.tbClasificacionEventosNotificacion_vigencias b With (NoLock) 
	On          b.cnsctvo_cdgo_clsfccn_evnto = a.cnsctvo_cdgo_rcbro
	Where		a.cnsctvo_cdgo_rcbro = @lnCnsctvo_cdgo_rcbro /* no debe aparecer impreso VIH  */    
	And         @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--actualiza la informacion de la descripcion del recobro
	Update		#informacionOPS        
	Set			dscrpcn_rcbro = b.dscrpcn_clsfccn_evnto        
	From		#informacionOPS	a
	Inner Join	bdSiSalud.dbo.tbClasificacionEventosNotificacion_vigencias b With (NoLock) 
	On          b.cnsctvo_cdgo_clsfccn_evnto = a.cnsctvo_cdgo_rcbro
	Where		a.cnsctvo_cdgo_rcbro <> @lnCnsctvo_cdgo_rcbro -- no debe aparecer impreso VIH    
	And         @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
         
	--Completar informacion de hospitalizacion
	IF EXISTS (Select Top 1 1
	           From       #informacionOPS s
			   Inner Join bdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios h WITH(NOLOCK)
			   On         h.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco) 
		Begin
			Update	   #informacionOPS
			Set	       fcha_ingrso_tmp         = h.fcha_ingrso_hsptlzcn   ,
				       cma                     = h.cma                    ,
					   crte_cnta               = h.crte_cnta              ,
					   cnsctvo_cdgo_clse_hbtcn = h.cnsctvo_cdgo_clse_hbtcn
			From	   #informacionOPS ds
			Inner Join bdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios h WITH(NOLOCK)
			On	       h.cnsctvo_slctd_atrzcn_srvco = ds.cnsctvo_slctd_atrzcn_srvco

			Update     #informacionOPS
			Set        dscrpcn_clse_hbtcn = b.dscrpcn_clse_hbtcn
			From       #informacionOPS a
			Inner Join bdSisalud.dbo.tbClasesHabitacion_Vigencias b WITH(NOLOCK)
			On         b.cnsctvo_cdgo_clse_hbtcn = a.cnsctvo_cdgo_clse_hbtcn
            Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca 
		End	
	
	/*Se actualiza el codigo y la descripcion del servicio de los medicamentos*/
    Update     #informacionOPS
    Set        cdgo_cdfccn    = c.cdgo_cdfccn   ,
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #informacionOPS a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join bdSisalud.dbo.tbcums u With(NoLock) 
    ON         u.cnsctvo_cms = c.cnsctvo_cdfccn
	Where      c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn_cums -- Medicamentos

	/*Se actualiza el codigo y la descripcion del servicio para las prestaciones PIS*/
    Update     #informacionOPS
    Set        cdgo_cdfccn    = u.cdgo_srvco_prvdr,
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #informacionOPS a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join bdSisalud.dbo.tbPrestacionPis u With(NoLock) 
    ON         u.cnsctvo_prstcn_pis = c.cnsctvo_cdfccn
	Where      c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn_pis -- PIS


	/*Se actualiza el codigo y la descripcion del servicio para servicios CUPS */
	Update     #informacionOPS
    Set        cdgo_cdfccn    = p1.cdgo_prstcn_prstdr, 
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #informacionOPS a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join bdsisalud.dbo.tbcupsServicios g With(NoLock)
    On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn
	Inner Join BDIpsParametros.dbo.tbHomologacionPrestaciones h1 With(NoLock)
    On         h1.cdgo_intrno    = a.cdgo_intrno_prstdr_atrzdo  And 
               h1.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo 
	Inner Join BDIpsParametros.dbo.tbDetalle_HomologacionPrestaciones h3 With(NoLock) 
    On         h3.cnsctvo_cdgo_hmlgcn_prstcn = h1.cnsctvo_cdgo_hmlgcn_prstcn
	Inner Join BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
	On         p1.cdgo_intrno                = h1.cdgo_intrno  And 
               p1.cnsctvo_cdgo_prstcn_prstdr = h1.cnsctvo_cdgo_prstcn_prstdr
	Inner Join BDIpsParametros.dbo.tbDetalle_PrestacionesPrestador h2 With(NoLock)
    On         h2.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr     
	Where      h1.cnsctvo_cdgo_clsfccn_prstcn_prstdr = @lncnsctvo_cdgo_clsfccn_prstcn_prstdr  		
	And        c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn_cups
    And        @ldFechaActual Between h2.inco_vgnca And h2.fn_vgnca
    And        @ldFechaActual Between h3.inco_vgnca And h3.fn_vgnca
	        
    /*Se actualiza el codigo y la descripcion del servicio para servicios CUPS, si no hay homologo */
	Update     #informacionOPS
    Set        cdgo_cdfccn    = c.cdgo_cdfccn, 
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #informacionOPS a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join bdsisalud.dbo.tbcupsServicios g With(NoLock)
    On         g.cnsctvo_prstcn = c.cnsctvo_cdfccn
    Where      c.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn_cups
	And        a.cdgo_cdfccn IS NULL

	-- Se actualiza el codigo de la lateralidad    
	Update     #informacionOPS
	Set        cdgo_ltrldd = c.cdgo_ltrldd
	From       #informacionOPS a
	Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH(NOLOCK)
	On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join bdSiSalud.dbo.tbLateralidades_Vigencias c WITH(NOLOCK)
	On         c.cnsctvo_cdgo_ltrldd = b.cnsctvo_cdgo_ltrldd
	Where      @ldFechaActual Between c.inco_vgnca And c.fn_vgnca


	If Exists ( Select  top 1 'x' From  bdSisalud.dbo.tbafiliadosmarcados aa with(nolock)
				Inner join bdSisalud.dbo.tbtutela cc with(nolock) On    aa.cnsctvo_ntfccn     = cc.cnsctvo_ntfccn 
																  And   aa.cnsctvo_cdgo_ofcna = cc.cnsctvo_cdgo_ofcna
				Inner Join #informacionOPS bb					  On    aa.nmro_unco_idntfccn = bb.nmro_unco_idntfccn_afldo
			    Where  aa.cnsctvo_cdgo_clsfccn_evnto = @clasifevento
				And   aa.cnsctvo_cdgo_estdo_ntfccn in (@notificado,@confirmado) 
			  )
		Begin
			Set @legal = @ms_tutela
		End

	-- Se actualiza el consecutivo notificacion
	Update     #informacionOPS
	Set        cnsctvo_cdgo_ntfccn     = Case When c.cnsctvo_cdgo_ntfccn Is Null THEN @lnZero      
		                                      Else c.cnsctvo_cdgo_ntfccn        
	                                     End,
               nmro_acta_ctc           = Case When @legal Is not Null THEN @lcVacio      
		                                      Else c.cnsctvo_pr_ntfccn        
	                                     End,
			   cnsctvo_cdgo_ofcna_noti = c.cnsctvo_cdgo_ofcna,        
	           cmrcl                   = c.cmrcl       
	From       #informacionOPS a
	Inner Join bdSiSalud.dbo.tbDetProgramacionFechaEvento b WITH(NOLOCK)      
    On         b.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto 
    Inner Join bdSiSalud.dbo.tbProgramacionPrestacion c WITH(NOLOCK)     
    On         c.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn

	--Se saca el mensaje de atencion de la ops
	Select    @lcDscrpcn_mnsje = dscrpcn_mnsje
	From      bdsisalud.dbo.tbMensajesImpresion_vigencias WITH(NOLOCK)
	Where     cnsctvo_cdgo_mnsje = @lnCnsctvo_cdgo_mnsje
	And       @ldFechaActual Between inco_vgnca And fn_vgnca

	--Se actualiza el mensaje en la tabla
	Update    #informacionOPS 
	Set       dscrpcn_mnsje = @lcDscrpcn_mnsje

	--Se actualiza la informacion de la fecha de utilizacion desde 
	Update    #informacionOPS
	Set       fcha_utlzcn_dsde = @ldFechaActual
	Where     fcha_utlzcn_dsde IS NULL
	
	--Se actualiza la informacion de la fecha de utilizacion hasta 
	Update    #informacionOPS
	Set       fcha_utlzcn_hsta = @ldFechaActual + ds_vldz
	Where     fcha_utlzcn_hsta IS NULL

	-- Se actualiza los valores correspondientes a la Cuota moderadora
	Update     #informacionOPS 
	Set        mrca_rcdo_cta_mdrdra = Case When IsNull(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 0) > 0 Then 'X' 
	                                       Else @lcVacio 
	                                  End,
	           cta_mdrdra           = Round(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 2),
			   vlr_lqdcn_cta_mdrdra = Round(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 2)
	From       #informacionOPS a
	Inner Join bdCNA.gsa.tbASConsolidadoCuotaRecuperacionxOps b WITH(NOLOCK)
	On         b.nmro_unco_ops = a.nmro_unco_ops
	Where      b.cnsctvo_cdgo_cncpto_pgo = @lnCncpto_pgo_cta_mdrdra


	-- Se actualiza los valores correspondientes al copago
	Update     #informacionOPS
	Set        mrca_rcdo_cpago     = Case When IsNull(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 0) > 0 Then 'X' 
	                                      Else @lcVacio 
	                                 End,
               cpgo_pgo            = Round(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 2),
			   vlr_lqdcn_ttl_cpago = Round(b.vlr_lqdcn_cta_rcprcn_srvco_ops, 2)
	From       #informacionOPS a
	Inner Join bdCNA.gsa.tbASConsolidadoCuotaRecuperacionxOps b WITH(NOLOCK)
	On         b.nmro_unco_ops = a.nmro_unco_ops
	Where      b.cnsctvo_cdgo_cncpto_pgo = @lnCncpto_pgo_cpago 


	-- Se actualiza la sumatoria del valor de cuota moderadora y copago
	Update   #informacionOPS
	Set      vlr_lqdcn_ttl_srvco = vlr_lqdcn_cta_mdrdra + vlr_lqdcn_ttl_cpago

	-- transformamos los campos requiren presentacion
	Update #informacionOPS 
	Set    fcha_dsde      = Case When fcha_utlzcn_dsde Is Null Then @lcVacio
		                         Else Convert(VarChar(10), fcha_utlzcn_dsde, 111) 
		                    End, 										
           fcha_hsta      = Case When fcha_utlzcn_hsta Is Null Then @lcVacio
		                         Else Convert(VarChar(10), fcha_utlzcn_hsta, 111) 
	                        End,
		   fcha_ncmnto    = Case When fcha_ncmnto_afldo Is Null Then @lcVacio
		                         Else Convert(VarChar(10), fcha_ncmnto_afldo, 111) 
					        End,
	       hra 		      = Case When fcha_utlzcn_dsde Is Null Then @lcVacio
	                             Else Substring(Convert(VarChar(20),fcha_utlzcn_dsde,21),12,5) 
	                        End,
		   hra_fcha_slctd = Case When fcha_slctd Is Null Then @lcVacio
			                     Else Substring(Convert(VarChar(20),fcha_slctd,103),12,5) 
		                    End,
		   fcha_slctd     = Case When fcha_slctd_tmp Is Null Then @lcVacio
		                         Else Convert(VarChar(10), fcha_slctd_tmp, 111) 
	                        End,
		   fcha_ingrso    = Case When fcha_ingrso_tmp Is Null Then @lcVacio
		                         Else Convert(VarChar(19),fcha_ingrso_tmp,111) 
	                        End					
	
	
	--Se actualizan las fechas de utilizacion desde y hasta en la tabla transaccional
	Update		bdCNA.gsa.tbASConceptosServicioSolicitado
	Set			fcha_utlzcn_dsde = b.fcha_utlzcn_dsde,
				fcha_utlzcn_hsta = b.fcha_utlzcn_hsta,
				cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo,
				fcha_ultma_mdfccn = @ldFechaActual,
				usro_ultma_mdfccn = @usro_imprsn
	From		#informacionOPS b
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado a With(RowLock) 
	On			a.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
	Where		a.fcha_utlzcn_dsde IS NULL 
	And			a.fcha_utlzcn_hsta IS NULL


	Update		bdCNA.gsa.tbASConceptosServicioSolicitado
	Set			fcha_utlzcn_dsde = b.fcha_utlzcn_dsde,
				fcha_utlzcn_hsta = b.fcha_utlzcn_hsta,
				cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo,
				fcha_ultma_mdfccn = @ldFechaActual,
				usro_ultma_mdfccn = @usro_imprsn
	From		#informacionOPS b
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado a With(RowLock) 
	On			a.cnsctvo_mdcmnto_slctdo = pis.cnsctvo_mdcmnto_slctdo
	Where		a.fcha_utlzcn_dsde IS NULL 
	And			a.fcha_utlzcn_hsta IS NULL


	----Se actualizan el estado de la ops a entregada
	--Update     bdCNA.gsa.tbASConceptosServicioSolicitado
	--Set        cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo
	--Where      nmro_unco_ops = @nmro_unco_ops
	

	--Se consulta la oficina del usuario que imprime la OPS
	Select    @cnsctvo_cdgo_ofcna = cnsctvo_cdgo_ofcna
	From      bdSeguridad.dbo.tbUsuarios WITH(NOLOCK) 
	Where     lgn_usro = @usro_imprsn

	--Se actualizan la fecha de impresion y estado a entregada del servicio entregado
	Update     a
	Set        fcha_imprsn                     = @ldFechaActual       ,
	           cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo,
			   cnsctvo_cdgo_ofcna_imprsn_ops   = @cnsctvo_cdgo_ofcna  ,
			   usro_imprsn_ops                 = @usro_imprsn,
			   fcha_ultma_mdfccn			   = @ldFechaActual,
			   usro_ultma_mdfccn			   = @usro_imprsn
	From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)            
    Inner Join #informacionOPS s 
	On         s.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo  
	Where      a.fcha_imprsn IS NULL

	
	--si el nombre del usuario que autoriza no esta es xq no viene de una solicitud y se busca del que imprime la ops      
	UPDATE     #informacionOPS        
	SET        usro_atrza         = CONCAT(LTRIM(RTRIM(b.prmr_nmbre_usro))  , @lcAux ,
	                                       LTRIM(RTRIM(b.sgndo_nmbre_usro)) , @lcAux , 
					                       LTRIM(RTRIM(b.prmr_aplldo_usro)) , @lcAux , 
								           LTRIM(RTRIM(b.sgndo_aplldo_usro))
									      ),  
			   tlfn_ofcna         = CONCAT(RTRIM(f.indctvo)   , @lcInd,  
			                               RTRIM(e.tlfn_ofcna), @lcAux
									      )                            ,                          
	           dscrpcn_crgo       = c.dscrpcn_crgo_ss                  , 
			   tlfn_ofcna_tmp     = e.tlfn_ofcna                       ,        
	           indctvo_atrzdr     = f.indctvo                          ,
			   cnsctvo_cdgo_ofcna = s.cnsctvo_cdgo_ofcna_imprsn_ops    ,
			   usro_imprsn_ops    = s.usro_imprsn_ops   
    FROM       #informacionOPS a        
	Inner Join bdcna.gsa.tbASServiciosSolicitados s WITH (NOLOCK)  
	On         s.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo 
	Inner Join bdseguridad.dbo.tbUsuarios b WITH(NOLOCK) 
	On         b.lgn_usro = s.usro_imprsn_ops
	Left  Join bdSiSalud.dbo.tbCargosSOS_Vigencias c WITH(NOLOCK) 
	On         c.cnsctvo_cdgo_crgo_ss = b.cnsctvo_cdgo_crgo_usro       
	Inner Join bdafiliacionvalidador.dbo.tbOficinas_Vigencias e WITH(NOLOCK)
	On         e.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	Inner Join bdafiliacionvalidador.dbo.tbCiudades_Vigencias f WITH(NOLOCK) 
	On         f.cnsctvo_cdgo_cdd = e.cnsctvo_cdgo_cdd_rsdnca
	Where      @ldFechaActual Between f.inco_vgnca And f.fn_vgnca
	And        @ldFechaActual Between e.inco_vgnca And e.fn_vgnca
	And        @ldFechaActual Between c.inco_vgnca And c.fn_vgnca

	
	--Se actualiza la informacion de la oficina
	Update     #informacionOPS
	Set        dscrpcn_ofcna      = e.dscrpcn_ofcna
	From       #informacionOPS a
	Inner Join bdafiliacionvalidador.dbo.tbOficinas_Vigencias e WITH(NOLOCK)
	On         e.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	Where      @ldFechaActual Between e.inco_vgnca And e.fn_vgnca

	If Exists (Select top 1 'x' From #informacionOPS a Where a.cnsctvo_cdgo_mdo_cntcto_slctd = @cdgo_mdo_cntcto_slctd_prgrmcn_entrga)
	  Begin
	     Select Top 1 @cnsctvo_slctd_atrzcn_srvco = cnsctvo_slctd_atrzcn_srvco From #informacionOPS		 
	     Exec bdCNA.gsa.spASMigracionSipresxSolicitud @cnsctvo_slctd_atrzcn_srvco
	  End
	    	 		 

	Select nmro_unco_ops           , hra                     , rzn_scl_ips         , cdgo_tpo_idntfccn_ips   , 
	       nmro_idntfccn_ips       , dgto_vrfccn             , cdgo_prstdr_mnstro  , indctvo_prstdr          , 
		   tlfno_ips               , drccn_ips               , dscrpcn_dprtmnto_ips, dscrpcn_cdd_ips         , 
		   prmr_aplldo             , sgndo_aplldo            , prmr_nmbre          , sgndo_nmbre             , 
		   cdgo_tpo_idntfccn       , nmro_idntfccn           , tlfno_afldo         , drccn_afldo             , 
		   dscrpcn_dprtmnto_afldo  , dscrpcn_cdd_rsdnca_afldo, fcha_ncmnto         , tlfno_cllr_afldo        , 
		   crro_elctrnco_afldo     , dscrpcn_pln             , nmbre_ctznte        , cdgo_tpo_idntfccn_empldr, 
		   nmbre_empldr            , dscrpcn_prntsco         , nmbre_mdco_trtnte   , dscrpcn_ofcna           ,
		   cdgo_dgnstco            , fcha_dsde               , fcha_hsta           , nmro_slctd_atrzcn_ss    , 
		   nmro_slctd_prvdr        , fcha_slctd              , hra_fcha_slctd      , dscrpcn_tpo_ubccn_pcnte , 
		   dscrpcn_tpo_srvco_slctdo, cma                     , dscrpcn_clse_hbtcn  , fcha_ingrso             , 
		   dscrpcn_grpo_imprsn     , dscrpcn_gs_atncn        , vlr_lqdcn_srvco     , crte_cnta               , 
		   cdgo_cdfccn             , cntdd_prstcn            , dscrpcn_cdfccn      , cdgo_ltrldd             , 
		   smns_aflcn              , mrca_rcdo_cta_mdrdra    , cta_mdrdra          , mrca_rcdo_cpago         ,
		   cpgo_pgo                , prcntje_lqdcn           , usro_atrza          , tlfn_ofcna              , 
		   tlfn_cllr_atrzdr        , dscrpcn_crgo            , dscrpcn_mnsje       , cnsctvo_cdgo_ntfccn     , 
		   nmro_acta_ctc           , obsrvcn_adcnl           , dscrpcn_cntngnca    , dscrpcn_rcbro           ,
		   vlr_lqdcn_ttl_srvco     , vlr_lqdcn_ttl_ops       , nmro_idntfccn_empldr, usro_imprsn_ops
	From   #informacionOPS

	Drop Table #informacionOPS
	Drop Table #tempGrupoImpresion
	Drop Table #ValorTotalOPS
End

