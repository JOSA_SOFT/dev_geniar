Use bdCNA
Go

If(Object_id('gsa.spASAnularSolicitudPrestacionMega') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASAnularSolicitudPrestacionMega As Select 1';
End 

Go

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASAnularSolicitudPrestacionMega
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento que se encarga de anular las prestaciones en Mega D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 02/10/2017  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

Alter Procedure gsa.spASAnularSolicitudPrestacionMega
As
Begin
	Set Nocount On
	
	Declare	@cha_actl						Datetime,
			@cnsctvo_cdgo_estdo_mga_anldo	udtConsecutivo;
		
	Set @cha_actl = getDate();	
	Set @cnsctvo_cdgo_estdo_mga_anldo = 14;

	-- 
	Update		a
	Set			cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga,
				fcha_ultma_mdfccn = @cha_actl,
				usro_ultma_mdfccn = c.usro_anlcn
	From		bdCNA.gsa.tbASServiciosSolicitados a With(RowLock)
	Inner Join	#tempInformacionServicios b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join	#tempInformacionSolicitud c
	On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;

	-- 
	Update		a
	Set			cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga,
				fcha_ultma_mdfccn = @cha_actl,
				usro_ultma_mdfccn = c.usro_anlcn
	From		bdCNA.gsa.tbASServiciosSolicitados a With(RowLock)
	Inner Join	#tempInformacionServicios b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join	#tempInformacionSolicitud c
	On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;

	-- 
	Update		a
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga,
				fcha_ultma_mdfccn = @cha_actl,
				usro_ultma_mdfccn = c.usro_anlcn
	From		bdCNA.gsa.tbASConceptosServicioSolicitado a With(RowLock)
	Inner Join	#tempInformacionServicios b
	On			b.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	#tempInformacionSolicitud c
	On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;	

	-- 
	Update		a
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga,
				fcha_ultma_mdfccn = @cha_actl,
				usro_ultma_mdfccn = c.usro_anlcn
	From		bdCNA.gsa.tbASConceptosServicioSolicitado a With(RowLock)
	Inner Join	#tempInformacionServicios b
	On			b.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
	Inner Join	#tempInformacionSolicitud c
	On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;


End