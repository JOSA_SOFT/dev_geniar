USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarEdadPaciente]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarEdadPaciente
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite validar si la edad del usuario se encuentra en uno D\>
					   <\D	de los rangos parametrizados												D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarEdadPaciente] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@cdgo_grpo_entrga CHAR(4),
@agrpa_prstcn udtLogico
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @fcha_actl datetime,
          @afirmacion char(1),
		  @cnsctvo_edd_ans int,
		  @cnsctvo_edd_mss int,
		  @cnsctvo_edd_ds int

  SELECT @afirmacion = 'S',
		 @fcha_actl = GETDATE(),
		 @cnsctvo_edd_ans = 4 ,
		 @cnsctvo_edd_mss = 3 ,
		 @cnsctvo_edd_ds = 1

  IF EXISTS (SELECT
      sol.id
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL)
  BEGIN

    --Se actualiza al grupo en caso de que el afiliado se encuentren, se realiza por separado
	--para evitar el uso de "OR"
	--Validación por años.
    UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    INNER JOIN bdCNA.prm.tbasRangosEdadPaciente_Vigencias edd WITH (NOLOCK)
      ON sol.edd_afldo_ans BETWEEN edd.inco_edd_pcnte AND edd.fn_edd_pcnte 
	   AND edd.cnsctvo_cdgo_tpo_undd = @cnsctvo_edd_ans
    WHERE @fcha_actl BETWEEN edd.inco_vgnca AND edd.fn_vgnca
    AND sol.cnsctvo_grpo_entrga IS NULL;

	--Validación por meses.
	UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    INNER JOIN bdCNA.prm.tbasRangosEdadPaciente_Vigencias edd WITH (NOLOCK)
      ON sol.edd_afldo_mss BETWEEN edd.inco_edd_pcnte AND edd.fn_edd_pcnte
	   AND edd.cnsctvo_cdgo_tpo_undd = @cnsctvo_edd_mss
    WHERE @fcha_actl BETWEEN edd.inco_vgnca AND edd.fn_vgnca
    AND sol.cnsctvo_grpo_entrga IS NULL;

	--Validación por días.
	UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    INNER JOIN bdCNA.prm.tbasRangosEdadPaciente_Vigencias edd WITH (NOLOCK)
      ON sol.edd_afldo_ds BETWEEN edd.inco_edd_pcnte AND edd.fn_edd_pcnte
	   AND edd.cnsctvo_cdgo_tpo_undd = @cnsctvo_edd_ds
    WHERE @fcha_actl BETWEEN edd.inco_vgnca AND edd.fn_vgnca
    AND sol.cnsctvo_grpo_entrga IS NULL;

    --Se determina si se debe realizar agrupación
    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
      AND @agrpa_prstcn = @afirmacion)
    BEGIN
      UPDATE sol
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @cdgo_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN #tmpDatosSolicitudFechaEntrega tmp
        ON tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
      WHERE tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga

    END

  END
END

GO
