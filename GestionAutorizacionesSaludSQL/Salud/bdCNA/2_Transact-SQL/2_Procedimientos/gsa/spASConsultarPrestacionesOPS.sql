USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarPrestacionesOPS]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarPrestacionesOPS
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite consultar la información de las prestaciones     D\>    
					 : <\D a partir del número único de OPS											  D\>
* Observaciones      : <\O   O\>    
* Parametros         : <\P @id_slctd : Identificador único de la solicitud.							  P\>    
					 : <\P @nmro_unco_ops : Identificador de la OPS generada.						  P\> 
* Variables          : <\V   V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASConsultarPrestacionesOPS] @id_slctd INT,
@nmro_unco_ops INT
AS
BEGIN
SET NOCOUNT ON

DECLARE @ldFechaActual DATETIME

DECLARE @tmpPrestaciones TABLE(
cnsctvo_cdgo_tpo_srvco			INT,
cdgo_tpo_srvco					UDTCODIGO,
dscrpcn_tpo_srvco				UDTDESCRIPCION,
cnsctvo_cdfccn					INT,
cdgo_cdfccn						CHAR(15),
dscrpcn_cdfccn					UDTDESCRIPCION,
cntdd							INT,
cnsctvo_cdgo_cdd_rsdnca_afldo	UDTCONSECUTIVO,--default 8606,
cnsctvo_cdgo_pln				UDTCONSECUTIVO,-- default 1,
cnsctvo_slctd_atrzcn_srvco		UDTCONSECUTIVO
);

Set @ldFechaActual = getDate()

--Inserción de información de los servicios
INSERT INTO @tmpPrestaciones (cnsctvo_cdgo_tpo_srvco,
cnsctvo_cdfccn,
cntdd, cnsctvo_slctd_atrzcn_srvco)
SELECT SSO.cnsctvo_cdgo_tpo_srvco, SSO.cnsctvo_cdgo_srvco_slctdo, SSO.cntdd_slctda, SSO.cnsctvo_slctd_atrzcn_srvco
FROM bdCNA.gsa.tbASConceptosServicioSolicitado							CSS		WITH(NOLOCK)
INNER JOIN bdCNA.gsa.tbASMedicamentosSolicitados						MSS		WITH(NOLOCK)
 ON MSS.cnsctvo_mdcmnto_slctdo = CSS.cnsctvo_mdcmnto_slctdo
INNER JOIN bdCNA.gsa.tbAsServiciosSolicitados							SSO		WITH(NOLOCK)	
 ON SSO.cnsctvo_srvco_slctdo = MSS.cnsctvo_srvco_slctdo
 AND SSO.cnsctvo_slctd_atrzcn_srvco = @id_slctd
WHERE CSS.nmro_unco_ops = @nmro_unco_ops
UNION ALL
SELECT SSO.cnsctvo_cdgo_tpo_srvco, SSO.cnsctvo_cdgo_srvco_slctdo, SSO.cntdd_slctda, SSO.cnsctvo_slctd_atrzcn_srvco
FROM bdCNA.gsa.tbASConceptosServicioSolicitado								CSS		WITH(NOLOCK)
INNER JOIN bdCNA.gsa.tbASProcedimientosInsumosSolicitados					PIS		WITH(NOLOCK)
 ON PIS.cnsctvo_prcdmnto_insmo_slctdo = CSS.cnsctvo_prcdmnto_insmo_slctdo
INNER JOIN bdCNA.gsa.tbAsServiciosSolicitados								SSO		WITH(NOLOCK)	
 ON SSO.cnsctvo_srvco_slctdo = PIS.cnsctvo_srvco_slctdo
 AND SSO.cnsctvo_slctd_atrzcn_srvco = @id_slctd
WHERE CSS.nmro_unco_ops = @nmro_unco_ops

--Actualización de información adicional.
UPDATE TPS
SET cdgo_cdfccn = TCD.cdgo_cdfccn,
	dscrpcn_cdfccn = TCD.dscrpcn_cdfccn
FROM @tmpPrestaciones							TPS
INNER JOIN bdsisalud.dbo.tbCodificaciones		TCD
 ON TCD.cnsctvo_cdfccn = TPS.cnsctvo_cdfccn

--Actualización de tipo prestación.
UPDATE TPS
SET dscrpcn_tpo_srvco = TCD.dscrpcn_tpo_cdfccn,
	cdgo_tpo_srvco = TCD.cdgo_tpo_cdfccn
FROM @tmpPrestaciones										TPS
INNER JOIN bdsisalud.dbo.tbTipoCodificacion_vigencias		TCD
 ON TCD.cnsctvo_cdgo_tpo_cdfccn = TPS.cnsctvo_cdgo_tpo_srvco
 Where      @ldFechaActual Between TCD.inco_vgnca and TCD.fn_vgnca

 --Obtener la ciudad y el plan del afiliado
 Update TPS
 Set	cnsctvo_cdgo_pln			= ASA.cnsctvo_cdgo_pln,
		cnsctvo_cdgo_cdd_rsdnca_afldo  = ASA.cnsctvo_cdgo_cdd_rsdnca_afldo
 From @tmpPrestaciones TPS
 Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ASA WITH(NOLOCK)
 On TPS.cnsctvo_slctd_atrzcn_srvco = ASA.cnsctvo_slctd_atrzcn_srvco


 --Se retorna la información.
 SELECT DISTINCT cnsctvo_cdgo_tpo_srvco,
	cdgo_tpo_srvco,
	dscrpcn_tpo_srvco,
	cnsctvo_cdfccn,
	cdgo_cdfccn,
	dscrpcn_cdfccn,
	cntdd,
	cnsctvo_cdgo_cdd_rsdnca_afldo,
	cnsctvo_cdgo_pln
FROM @tmpPrestaciones

END

GO
