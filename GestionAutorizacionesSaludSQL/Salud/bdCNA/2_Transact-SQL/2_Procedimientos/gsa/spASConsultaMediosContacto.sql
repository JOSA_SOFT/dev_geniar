USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaMediosContacto]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------                                  
* Procedimiento     :  gsa.[spASConsultaMediosContacto]        
* Desarrollado por  : <\A Ing. Victor Hugo Gil Ramos.    A\>                                  
* Descripción       : <\D Procedimiento que consulta los medios de contacto  D\>                                  
* Observaciones     : <\O              O\>                                  
* Parámetros        : <\P              P\>                       
* Fecha Creación    : <\FC 2016/02/08  FC\>        
*---------------------------------------------------------------------------------------------------------------------------------------------------------                                  
* DATOS DE MODIFICACION        
*---------------------------------------------------------------------------------------------------------------------------------------------------------                        
* Modificado Por      : <\AM  AM\>                                  
* Descripción         : <\DM  DM\>                                  
* Nuevos Parámetros   : <\PM  PM\>                                  
* Nuevas Variables    : <\VM  VM\>                                  
* Fecha Modificación  : <\FM  FM\>                                  
*---------------------------------------------------------------------------------------------------------------------------------------------------------*/  

--exec gsa.[spASConsultaMediosContacto] null, null, null, null

ALTER PROCEDURE [gsa].[spASConsultaMediosContacto]

	@lnConsecutivoMedioContacto udtConsecutivo  = NULL,
	@lcCodigoMedioContacto      Varchar(4)      = NULL,                          
	@ldFechaActual              Datetime        = NULL,
	@vsble_usro			        udtLogico       = NULL  
As    

Begin
	Set Nocount On    

	If (@ldFechaActual Is Null)
	  Begin
	      Set @ldFechaActual = getDate()
	  End

	If (@vsble_usro Is Null)
	  Begin
	      Set @vsble_usro = 'S'
	  End

	If(@lnConsecutivoMedioContacto Is Not Null)
	   Begin
	       Select     a.cnsctvo_cdgo_mdo_cntcto, a.cdgo_mdo_cntcto, a.dscrpcn_mdo_cntcto
		   From       prm.tbASMediosContacto a With(NoLock)
		   Inner Join prm.tbASMediosContacto_Vigencias b With(NoLock)
		   On         b.cnsctvo_cdgo_mdo_cntcto = a.cnsctvo_cdgo_mdo_cntcto
		   Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
		   And        b.vsble_usro              = @vsble_usro
		   And        b.cnsctvo_cdgo_mdo_cntcto = @lnConsecutivoMedioContacto	      
	   End
	Else
	  Begin
	     If(@lcCodigoMedioContacto Is Not Null)
            Begin
               Select     a.cnsctvo_cdgo_mdo_cntcto, a.cdgo_mdo_cntcto, a.dscrpcn_mdo_cntcto
               From       prm.tbASMediosContacto a With(NoLock)
               Inner Join prm.tbASMediosContacto_Vigencias b With(NoLock)
               On         b.cnsctvo_cdgo_mdo_cntcto = a.cnsctvo_cdgo_mdo_cntcto
               Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
               And        b.vsble_usro      = @vsble_usro
               And        b.cdgo_mdo_cntcto = @lcCodigoMedioContacto
            End
	     Else
		   Begin
              Select     a.cnsctvo_cdgo_mdo_cntcto, a.cdgo_mdo_cntcto, a.dscrpcn_mdo_cntcto
              From       prm.tbASMediosContacto a With(NoLock)
              Inner Join prm.tbASMediosContacto_Vigencias b With(NoLock)
              On         b.cnsctvo_cdgo_mdo_cntcto = a.cnsctvo_cdgo_mdo_cntcto
              Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
              And        b.vsble_usro = @vsble_usro
           End
      End
End 

GO
