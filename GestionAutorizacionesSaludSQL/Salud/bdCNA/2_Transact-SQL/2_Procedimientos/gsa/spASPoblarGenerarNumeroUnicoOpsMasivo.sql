USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarGenerarNumeroUnicoOpsMasivo]    Script Date: 16/06/2017 08:34:39 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASPoblarGenerarNumeroUnicoOpsMasivo												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Poblar las tablas temporales para generar número unicio Ops de Modelo MEGA				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/21																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez GAM\>  													
* Descripcion			: <\DM Se Modifica sp para incluir el filtro de fecha de entrega <= a la fecha actual DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/06/15	FM\> 													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez GAM\>  													
* Descripcion			: <\DM Por OPS Virtual se agrega funcionalidad de tomar todas las programaciones futuras DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017-04-03						FM\> 													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez GAM\>  													
* Descripcion			: <\DM Se agrega recuperacion del recobro por prestaciones, control de cambio 068 DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017-06-16 FM\> 													
*-----------------------------------------------------------------------------------------------------------------------

*/ 

ALTER PROCEDURE [gsa].[spASPoblarGenerarNumeroUnicoOpsMasivo]

/*01*/ @cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo

AS

Begin
	SET NOCOUNT ON

	-- Declaramos variable del proceso
	Declare @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9	UdtConsecutivo = 9, -- 'Estado '9-Liquidada'
			@cnsctvo_cdgo_estdo_srvco_slctdo9			UdtConsecutivo = 9,  -- 'Estado '9-Liquidada'
			@ValorCERO									int = 0,
			@prmtrzcn_n_ds_ants_fcha_entrga				int = 0,
			@cnsctvo_cdgo_rgla							int = 155, -- Número de Días a la fecha de entrega
			@fechaactual								datetime = getdate(),
			@fcha_entrga_n_ds_ants						datetime ,
			@fcha_entrga_dia_sgnte						datetime = getdate(),
			@ValorTRES									datetime = 3
			Set @fcha_entrga_dia_sgnte += 1

	-- Recuperamos la cantidad de días antes de la fecha de entrega
	Select	@prmtrzcn_n_ds_ants_fcha_entrga = vlr_rgla 
    From	bdSisalud.dbo.tbReglas_Vigencias With(NoLock)
    Where	cnsctvo_cdgo_rgla = @cnsctvo_cdgo_rgla
    And		@fechaactual BETWEEN inco_vgnca AND fn_vgnca

    Set @fcha_entrga_n_ds_ants = DATEADD(DAY,-@prmtrzcn_n_ds_ants_fcha_entrga,@fcha_entrga_dia_sgnte);
	

	-- Paso 1. Poblamos tabla temporal ops para los procedimientos e insumos
	Insert		#tbConceptosOps 
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
				cnsctvo_prcdmnto_insmo_slctdo,			cnsctvo_prstcn_slctda,				grpo_imprsn,
				cnsctvo_cdgo_cncpto_gsto,				nmro_unco_ops,						cdgo_intrno,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_cdgo_rcbro
	)
	Select Distinct		
				sas.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_srvco_slctdo,			css.cnsctvo_cncpto_prcdmnto_slctdo,
				pis.cnsctvo_prcdmnto_insmo_slctdo,		css.cnsctvo_prstcn,					css.cnsctvo_cdgo_grpo_imprsn,
				css.cnsctvo_cdgo_cncpto_gsto,			css.nmro_unco_ops,					css.cdgo_intrno_prstdr_atrzdo,
				ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,	ss.cnsctvo_cdgo_rcbro
	From		gsa.tbASConceptosServicioSolicitado	css With(NoLock)
	Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join  gsa.tbASResultadoFechaEntrega rfe With(NoLock)
	On			rfe.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			rfe.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Where	    (@cnsctvo_slctd_atrzcn_srvco is null or sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 -- '9-Liquidada'
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9 -- '9-Liquidada'
	And			css.nmro_unco_ops = @ValorCERO
	And			cast(isnull(rfe.fcha_rl_entrga,rfe.fcha_estmda_entrga) as date) <= cast(@fcha_entrga_n_ds_ants as date)
	And			css.cnsctvo_cdgo_grpo_imprsn > @ValorCERO
	And			css.vlr_lqdcn > @ValorCERO
	And         sas.cnsctvo_cdgo_tpo_orgn_slctd != @ValorTRES

	-- Paso 2. Poblamos tabla temporal ops para los medicamentos
	Insert		#tbConceptosOps 
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
				cnsctvo_mdcmnto_slctdo,					cnsctvo_prstcn_slctda,				grpo_imprsn,
				cnsctvo_cdgo_cncpto_gsto,				nmro_unco_ops,						cdgo_intrno,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_cdgo_rcbro
	)
	Select distinct		
				sas.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_srvco_slctdo,			css.cnsctvo_cncpto_prcdmnto_slctdo,
				ms.cnsctvo_mdcmnto_slctdo,				css.cnsctvo_prstcn,					css.cnsctvo_cdgo_grpo_imprsn,
				css.cnsctvo_cdgo_cncpto_gsto,			css.nmro_unco_ops,					css.cdgo_intrno_prstdr_atrzdo,
				ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,	ss.cnsctvo_cdgo_rcbro
	From		gsa.tbASConceptosServicioSolicitado	css With(NoLock)
	Inner Join	gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join  gsa.tbASResultadoFechaEntrega rfe With(NoLock)
	On			rfe.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			rfe.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Where	    (@cnsctvo_slctd_atrzcn_srvco is null or sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 -- '9-Liquidada'
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9 -- '9-Liquidado'
	And			css.nmro_unco_ops = @ValorCERO
	And			cast(isnull(rfe.fcha_rl_entrga,rfe.fcha_estmda_entrga) as date) <= cast(@fcha_entrga_n_ds_ants as date)
	And			css.cnsctvo_cdgo_grpo_imprsn > @ValorCERO
	And			css.vlr_lqdcn > @ValorCERO
	And         sas.cnsctvo_cdgo_tpo_orgn_slctd != @ValorTRES

	-- Paso 3. Poblamos tabla temporal ops para los procedimientos e insumos de ops virtual - sisjvg01 -20170403
	Insert		#tbConceptosOps 
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
				cnsctvo_prcdmnto_insmo_slctdo,			cnsctvo_prstcn_slctda,				grpo_imprsn,
				cnsctvo_cdgo_cncpto_gsto,				nmro_unco_ops,						cdgo_intrno,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_cdgo_rcbro
	)
	Select distinct		
				sas.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_srvco_slctdo,			css.cnsctvo_cncpto_prcdmnto_slctdo,
				pis.cnsctvo_prcdmnto_insmo_slctdo,		css.cnsctvo_prstcn,					css.cnsctvo_cdgo_grpo_imprsn,
				css.cnsctvo_cdgo_cncpto_gsto,			css.nmro_unco_ops,					css.cdgo_intrno_prstdr_atrzdo,
				ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,	ss.cnsctvo_cdgo_rcbro
	from		gsa.tbASConceptosServicioSolicitado	css With(NoLock)
	Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where	    (@cnsctvo_slctd_atrzcn_srvco is null or sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 -- '9-Liquidada'
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9 -- '9-Liquidada'
	And			css.nmro_unco_ops = @ValorCERO
	And			css.cnsctvo_cdgo_grpo_imprsn > @ValorCERO
	And			css.vlr_lqdcn > @ValorCERO
    And         sas.cnsctvo_cdgo_tpo_orgn_slctd = @ValorTRES

	-- Paso 4. Poblamos tabla temporal ops para los medicamentos para ops virtual - sisjvg01 - 2017-04-03 -
	Insert		#tbConceptosOps 
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
				cnsctvo_mdcmnto_slctdo,					cnsctvo_prstcn_slctda,				grpo_imprsn,
				cnsctvo_cdgo_cncpto_gsto,				nmro_unco_ops,						cdgo_intrno,
				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_cdgo_rcbro
	)
	Select distinct		
				sas.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_srvco_slctdo,			css.cnsctvo_cncpto_prcdmnto_slctdo,
				ms.cnsctvo_mdcmnto_slctdo,				css.cnsctvo_prstcn,					css.cnsctvo_cdgo_grpo_imprsn,
				css.cnsctvo_cdgo_cncpto_gsto,			css.nmro_unco_ops,					css.cdgo_intrno_prstdr_atrzdo,
				ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,	ss.cnsctvo_cdgo_rcbro
	from		gsa.tbASConceptosServicioSolicitado	css With(NoLock)
	Inner Join	gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
	On			sas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where	    (@cnsctvo_slctd_atrzcn_srvco is null or sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 -- '9-Liquidada'
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9 -- '9-Liquidado'
	And			css.nmro_unco_ops = @ValorCERO
	And			css.cnsctvo_cdgo_grpo_imprsn > @ValorCERO
	And			css.vlr_lqdcn > @ValorCERO
	And         sas.cnsctvo_cdgo_tpo_orgn_slctd = @ValorTRES

	

End
