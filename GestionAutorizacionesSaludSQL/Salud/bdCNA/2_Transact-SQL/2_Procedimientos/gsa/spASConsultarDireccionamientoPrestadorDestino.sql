USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDireccionamientoPrestadorDestino]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarDireccionamientoPrestadorDestino
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite la orquestación de la consulta de la ejecución  D\>
					   <\D de direccionamiento prestador											 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 28/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDireccionamientoPrestadorDestino] @cdgs_slctds xml,
@es_btch char(1),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(2000) OUTPUT,
@msje_rspsta xml OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @negacion char(1),
          @errorSolicitud varchar(100),
          @codigoExito char(2),
          @codigoError char(5)

  SET @negacion = 'N'
  SET @errorSolicitud = 'Una o más solicitudes no existen.'
  SET @codigoExito = 'OK';
  SET @codigoError = 'ET';

  IF NOT EXISTS (SELECT
      id
    FROM #tmpNmroSlctds so
    WHERE so.cnsctvo_slctd_srvco_sld_rcbda IS NULL)
  BEGIN
    IF @es_btch = @negacion
	BEGIN
    --Se controla la transacción sólo para poder actualizar el control del proceso.
    BEGIN TRY
      --Ejecución Direccionamiento en línea.

      --Se retorna el resultado de la ejecución de Direccionamiento
      EXEC bdCNA.gsa.spASRetornarResultadoDireccionamiento @es_btch,
                                                           @msje_rspsta OUTPUT

      SET @estdo_ejccn = @codigoExito
    END TRY
    BEGIN CATCH
      SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();
      SET @estdo_ejccn = @codigoError;
      SET @msje_rspsta = NULL
    END CATCH
	END
  END
  ELSE
  BEGIN
    SET @estdo_ejccn = @codigoError;
    SET @msje_errr = @errorSolicitud;
    SET @msje_rspsta = NULL;
  END
END

GO
