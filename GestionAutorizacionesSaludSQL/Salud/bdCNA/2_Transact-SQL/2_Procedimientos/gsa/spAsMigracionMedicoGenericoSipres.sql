USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsMigracionMedicoGenericoSipres]    Script Date: 30/05/2017 05:31:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spAsMigracionMedicoGenericoSipres
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>    
* Descripcion        : <\D Migra los medicos nos adscritos a SOS D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P 	P\>    
* Variables          : <\V   @clse_prstdr
							 @estdo
							 @cnsctvo_cdgo_mdlo	V\>    
* Fecha Creacion     : <\FC 2017/04/05  FC\>    
*--------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                            Se agregan cruces a la clausula merge para validar con la llave primaria compuesta
					   D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  2017-05-30  FM\>    
*----------------------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsMigracionMedicoGenericoSipres]
As
Begin
		
		Set NoCount On

		Declare @clse_prstdr		Char(1),
				@estdo				Char(1),
				@cnsctvo_cdgo_mdlo	Int,
				@vlr_cro			Int;
		
		Create Table #tempGenericosDetalle
		(
			nmro_vldcn								Int,  -- nuam
			cnsctvo_cdgo_ofcna						Int,
			cnsctvo_gnrco							Int,
			cnsctvo_cdgo_mdlo						Int,
			clse_prstdr								Char(1),
			tpo_idntfccn							Char(3),
			nmro_idntfccn							VarChar(20),
			estdo									Char(1),
			fcha_ultma_mdfccn						DateTime,
			usro_ultma_mdfccn						udtUsuario,
			nmro_idntfccn_mdco_trtnte				udtNumeroIdentificacionLargo,
			cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	udtConsecutivo,
			cnsctvo_slctd_atrzcn_srvco				udtConsecutivo
		)

		Set	@clse_prstdr = 'M';
		Set @estdo = 'A';
		Set @cnsctvo_cdgo_mdlo = 21;
		Set @vlr_cro = 0;

	

		Insert Into #tempGenericosDetalle
		(
					nmro_vldcn,						cnsctvo_cdgo_ofcna,					cnsctvo_gnrco,
					cnsctvo_cdgo_mdlo,				clse_prstdr,						cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
					nmro_idntfccn_mdco_trtnte,		estdo,								cnsctvo_slctd_atrzcn_srvco					
		)
		Select		isd.nuam,						isd.cnsctvo_cdgo_ofcna,				mtsas.cnsctvo_gnrco,
					@cnsctvo_cdgo_mdlo,				@clse_prstdr,						mtsas.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
					mtsas.nmro_idntfccn_mdco_trtnte,@estdo,								isd.cnsctvo_slctd_atrzcn_srvco
		From		#tmpinformacionsolicitud isd
		Inner Join	#tmpinformacionservicios iss
		On			iss.cnsctvo_slctd_atrzcn_srvco = isd.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios mtsas With(NoLock)
		On			mtsas.cnsctvo_slctd_atrzcn_srvco = isd.cnsctvo_slctd_atrzcn_srvco
		Where		mtsas.cnsctvo_gnrco Is Not Null
		And			mtsas.cnsctvo_gnrco != @vlr_cro
		Group By	isd.nuam,			 isd.cnsctvo_cdgo_ofcna,		mtsas.cnsctvo_gnrco,
					mtsas.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,		mtsas.nmro_idntfccn_mdco_trtnte,
					isd.cnsctvo_slctd_atrzcn_srvco
		

		Update		#tempGenericosDetalle
		Set			tpo_idntfccn = Convert(Char(3), cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte)

		Update		#tempGenericosDetalle
		Set			nmro_idntfccn_mdco_trtnte = Ltrim(Rtrim(nmro_idntfccn_mdco_trtnte))

		Update		#tempGenericosDetalle
		Set			nmro_idntfccn = SubString(nmro_idntfccn_mdco_trtnte, 1, 20)

		Update		gd
		Set			fcha_ultma_mdfccn = isd.fcha_slctd,
					usro_ultma_mdfccn =  isd.usro_crcn
		From		#tempGenericosDetalle gd
		Inner Join	#tmpinformacionsolicitud  isd
		On			isd.cnsctvo_slctd_atrzcn_srvco = gd.cnsctvo_slctd_atrzcn_srvco

		Merge BDSisalud.dbo.tbGenericosDetalle With(RowLock) AS Target
		Using(
				Select		nmro_vldcn,						cnsctvo_cdgo_ofcna,					cnsctvo_gnrco,
							cnsctvo_cdgo_mdlo,				clse_prstdr,						tpo_idntfccn,
							nmro_idntfccn,					estdo,								fcha_ultma_mdfccn,
							usro_ultma_mdfccn
				From		#tempGenericosDetalle
		)As Source
		On (
			Target.nmro_vldcn = Source.nmro_vldcn					And
			Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna	And
			Target.cnsctvo_gnrco = Source.cnsctvo_gnrco				And
			Target.cnsctvo_cdgo_mdlo = Source.cnsctvo_cdgo_mdlo
		)
		When Matched Then
			Update
			Set		cnsctvo_gnrco = Source.cnsctvo_gnrco,
					nmro_idntfccn = Source.nmro_idntfccn,
					cnsctvo_cdgo_mdlo = Source.cnsctvo_cdgo_mdlo,
					clse_prstdr = Source.clse_prstdr,
					estdo = Source.estdo,
					fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,
					usro_ultma_mdfccn = Source.usro_ultma_mdfccn,
					tpo_idntfccn = Source.tpo_idntfccn
		When Not Matched Then
			Insert 
			(
				nmro_vldcn,						cnsctvo_cdgo_ofcna,					cnsctvo_gnrco,
				cnsctvo_cdgo_mdlo,				clse_prstdr,						tpo_idntfccn,
				nmro_idntfccn,					estdo,								fcha_ultma_mdfccn,
				usro_ultma_mdfccn
			)
			Values
			(
				Source.nmro_vldcn,						Source.cnsctvo_cdgo_ofcna,					Source.cnsctvo_gnrco,
				Source.cnsctvo_cdgo_mdlo,				Source.clse_prstdr,							Source.tpo_idntfccn,
				Source.nmro_idntfccn,					Source.estdo,								Source.fcha_ultma_mdfccn,
				Source.usro_ultma_mdfccn
			);

		Drop Table #tempGenericosDetalle;

End