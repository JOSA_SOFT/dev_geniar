USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarHistoricoDescargas]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultarHistoricoDescargas  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D Procedimiento que permite consultar el historico de descargas de los formatos D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/03/09 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------      
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------    
* Modificado Por     : <\AM Ing. Jorge Rodriguez De León AM\>    
* Descripcion        : <\DM	Se adiciona Descripción de la prestación.  Tener en cuenta que si es un CUMS debe visualizarse 
							la descripción de la prestación concatenada con la concentración y la presentación) CC 050 DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 17-02-2017	FM\>    
*-----------------------------------------------------------------------------------------------------------------------*/ 
--exec [gsa].[spASConsultarHistoricoDescargas]  2121

ALTER PROCEDURE  [gsa].[spASConsultarHistoricoDescargas]  
  @lnConsecutivoSolicitud udtConsecutivo
AS  

Begin 

    SET NOCOUNT ON 

	Declare @ldFechaActual				Datetime,
			@ldcnsctvo_cdgo_tpo_srvco	udtConsecutivo,
			@ldprntss_abrto				char(1),
			@ldprntss_crrdo				char(1),
			@ldspco_blnco				char(1)

	Create 
	Table #tmpHistoricoDescargaFormatos(cnsctvo_hstrco_dscrga_frmto  udtConsecutivo NOT NULL,
									    cnsctvo_slctd_atrzcn_srvco   udtConsecutivo NOT NULL,
										cnsctvo_srvco_slctdo         udtConsecutivo NOT NULL,
										dscrpcn_srvco_slctdo         udtDescripcion NOT NULL,
										cnsctvo_cdgo_mdo_cntcto      udtConsecutivo NOT NULL,
										dscrpcn_mdo_cntcto           udtDescripcion NOT NULL,
										fcha_dscrga_frmto            datetime NOT NULL,
										usro_dscrga_frmto            udtUsuario NOT NULL,
										nmbre_dcmnto_dscrga_frmto    udtDescripcion NOT NULL,
										frmto_dscrgdo                udtDescripcion NOT NULL,
										cntdd_dscrga_frmto           Int NOT NULL,
										cnsctvo_cdgo_tpo_srvco		 udtConsecutivo NOT NULL,
										dscrpcn_prstcn		         udtDescripcion
									   )
	Set @ldFechaActual				= getDate()
	Set @ldcnsctvo_cdgo_tpo_srvco	= 5 --Tipo Prestación CUMS
	Set	@ldprntss_abrto				= '('
	Set @ldprntss_crrdo				= ')'
	Set	@ldspco_blnco					= ' '

	Insert 
	Into   #tmpHistoricoDescargaFormatos(cnsctvo_hstrco_dscrga_frmto, cnsctvo_srvco_slctdo     , cnsctvo_slctd_atrzcn_srvco,
	                                     dscrpcn_srvco_slctdo       , cnsctvo_cdgo_mdo_cntcto  , dscrpcn_mdo_cntcto        , 
										 fcha_dscrga_frmto          , usro_dscrga_frmto        , nmbre_dcmnto_dscrga_frmto , 
										 frmto_dscrgdo              , cntdd_dscrga_frmto	   , cnsctvo_cdgo_tpo_srvco    ,
										 dscrpcn_prstcn
	                                    )
	Select     a.cnsctvo_hstrco_dscrga_frmto, a.cnsctvo_srvco_slctdo     , a.cnsctvo_slctd_atrzcn_srvco,
	           c.dscrpcn_srvco_slctdo       , a.cnsctvo_cdgo_mdo_cntcto  , b.dscrpcn_mdo_cntcto        , 
			   a.fcha_dscrga_frmto          , a.usro_dscrga_frmto        , a.nmbre_dcmnto_dscrga_frmto , 
			   t.dscrpcn_tpo_frmto_dscrga   , a.cntdd_dscrga_frmto		 , c.cnsctvo_cdgo_tpo_srvco    ,
			   c.dscrpcn_srvco_slctdo
	From       BDCna.gsa.tbASHistoricoDescargaFormatos a With(NoLock)
	Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios e With(NoLock)
	On         e.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join BDCna.gsa.tbASServiciosSolicitados c With(NoLock)
	On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join BDCna.prm.tbASMediosContacto_vigencias b With(NoLock)
	On         b.cnsctvo_cdgo_mdo_cntcto = a.cnsctvo_cdgo_mdo_cntcto 
	Inner Join bdCNA.prm.tbASTiposFormatoDescarga_Vigencias t With(NoLock)
	On         t.cnsctvo_cdgo_tpo_frmto_dscrga = a.cnsctvo_cdgo_tpo_frmto_dscrga
	Where      e.cnsctvo_slctd_atrzcn_srvco = @lnConsecutivoSolicitud
	And        @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	And        @ldFechaActual Between t.inco_vgnca And t.fn_vgnca
	
	--Se adiciona Descripción de la prestación.  Tener en cuenta que si es un CUMS debe visualizarse 
	--la descripción de la prestación concatenada con la concentración y la presentación) CC 050
	Update     h
	Set	       dscrpcn_prstcn = Concat(h.dscrpcn_srvco_slctdo, @ldspco_blnco, @ldprntss_abrto,
								       Rtrim(Ltrim(m.cncntrcn_dss)), @ldspco_blnco, Rtrim(Ltrim(c.dscrpcn_cncntrcn)), @ldspco_blnco  ,
								       Rtrim(Ltrim(m.prsntcn_dss )), @ldspco_blnco, Rtrim(Ltrim(p.dscrpcn_prsntcn)) , @ldprntss_crrdo
									  ) 
	From       #tmpHistoricoDescargaFormatos h
	Inner Join BDCna.gsa.tbASMedicamentosSolicitados m  With(NoLock)
	On         m.cnsctvo_srvco_slctdo = h.cnsctvo_srvco_slctdo
	Inner Join bdSisalud.dbo.tbConcentracion_Vigencias c With(NoLock)
	On         c.cnsctvo_vgnca_cncntrcn = m.cnsctvo_cdgo_undd_cncntrcn_dss
	Inner Join bdSisalud.dbo.tbPresentaciones_Vigencias p With(NoLock)
	On         p.cnsctvo_vgnca_prsntcn  = m.cnsctvo_cdgo_prsntcn_dss
	Where      h.cnsctvo_cdgo_tpo_srvco	=  @ldcnsctvo_cdgo_tpo_srvco   
	And		   @ldFechaActual Between c.inco_vgnca And c.fn_vgnca
	And        @ldFechaActual Between p.inco_vgnca And p.fn_vgnca

	Select   fcha_dscrga_frmto, dscrpcn_srvco_slctdo, frmto_dscrgdo       , 
	         usro_dscrga_frmto, dscrpcn_mdo_cntcto  , cnsctvo_srvco_slctdo, 
			 dscrpcn_prstcn
	From     #tmpHistoricoDescargaFormatos
	Order By fcha_dscrga_frmto ASC
End
GO
