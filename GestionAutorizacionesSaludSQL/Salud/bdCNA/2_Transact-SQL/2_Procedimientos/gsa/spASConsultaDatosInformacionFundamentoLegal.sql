USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionFundamentoLegal]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosFundamentoLegal
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D 													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionFundamentoLegal]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@cnsctvo_srvco_slctdo       udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ldfcha_consulta		DATETIME,
			@visible_usuario	char(1) = 'S',
			@flag_false			udtLogico = 0,
			@flag_true			udtLogico = 1;

	Create table #fundamentoLegal(
		cnsctvo_cdgo_fndmnto_lgl			udtConsecutivo,
		dscrpcn_fndmnto_lgl					udtDescripcion,
		estdo			udtLogico
	)


	SET	@ldfcha_consulta = GETDATE();

	Insert into #fundamentoLegal(
		cnsctvo_cdgo_fndmnto_lgl,		dscrpcn_fndmnto_lgl,		estdo
	) 
	Select 
		cnsctvo_cdgo_fndmnto_lgl, 
		dscrpcn_fndmnto_lgl, 
		@flag_false
	from prm.tbASFundamentosLegales_Vigencias WITH (NOLOCK)
	Where  vsble_usro = @visible_usuario
	And    @ldfcha_consulta between inco_vgnca And fn_vgnca

	Update tfl set estdo = @flag_true
	from #fundamentoLegal tfl
	Inner Join BDCna.gsa.tbASFundamentosLegalesxServiciosSolicitados fl WITH (NOLOCK) on fl.cnsctvo_cdgo_fndmnto_lgl = tfl.cnsctvo_cdgo_fndmnto_lgl 
	Inner Join BDCna.gsa.tbASServiciosSolicitados SS WITH (NOLOCK) on  SS.cnsctvo_srvco_slctdo  = fl.cnsctvo_srvco_slctdo
	Where SS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And	  SS.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo


	Select 
		cnsctvo_cdgo_fndmnto_lgl,		
		dscrpcn_fndmnto_lgl,		
		estdo 
	from #FundamentoLegal

	drop table #fundamentoLegal
END

GO
