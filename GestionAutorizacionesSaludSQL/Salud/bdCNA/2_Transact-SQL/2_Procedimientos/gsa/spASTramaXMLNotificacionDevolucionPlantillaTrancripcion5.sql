USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5]    Script Date: 31/01/2017 10:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5 AS SELECT 1;'
END

Go

/*--------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Construcci�n trama xml para el env�o de notificaci�n de afiliados negaci�n. Plantilla 5   D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2016-12-29  FC\>    
*--------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega variable con consecutivo de la plantilla y se formatea fecha de la presolicitud.
						   Se agrega update de Angela Sandoval y se cambia el tama�o del campo dscrpcn_csa_estdo_prstcn_slctd
						   a 300  D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-01-27 FM\>    
*--------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D   
                           Se modifica el procedimiento quitando el consecutivo de la plantilla y adicionando las validaciones de las vigencias
						   sobre las tablas parametro
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-01-31 FM\>    
*--------------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5] 
	@usrio Varchar(50),
	@cnstrccn_trma_xml XML OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @anno			Varchar(4),
			@mes			Varchar(2),
			@dia			Varchar(2),
			@hra			Varchar(2),
			@mnto			Varchar(2),
			@sgndo			Varchar(2),
			@string_trama	Varchar(4000),
			@fcha_actl		Datetime
	
	CREATE 
	TABLE  #tmpPrestacionesDevueltas(cnsctvo_slctd_atrzcn_srvco		 udtConsecutivo,
		                             fcha_crcn						 Datetime,
		                             dscrpcn_csa_estdo_prstcn_slctd	 Varchar(300),
		                             cnsctvo_cdgo_csa_ngcn_prstcn	 udtConsecutivo
									)

	Set	@anno	   = CONVERT(CHAR(4),DATEPART(YEAR,GETDATE()))
	Set	@mes	   = RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(MONTH,GETDATE())))),2)
	Set @dia	   = RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(DAY,GETDATE())))),2)
	Set @hra	   = CONVERT(CHAR(2),DATEPART(HOUR,GETDATE()))
	Set @mnto	   = CONVERT(CHAR(2),DATEPART(MINUTE,GETDATE()))
	Set @sgndo	   = CONVERT(CHAR(2),DATEPART(SECOND,GETDATE()))
	Set @fcha_actl = getDate()
		
	--Se obtienen los datos de las prestaciones
	Insert 
	Into        #tmpPrestacionesDevueltas(cnsctvo_slctd_atrzcn_srvco, fcha_crcn, cnsctvo_cdgo_csa_ngcn_prstcn)
	Select      p.cnsctvo_prslctd, p.fcha_crcn, p.cnsctvo_cdgo_csa_ngcn_prstcn
	From		BDCna.dbo.tbPrestacionesNegadasPreSolicitud P WITH (NOLOCK)
	Inner Join	#tmpAflds AFI 
	On			AFI.cnsctvo_slctd_srvco_sld_rcbda = P.cnsctvo_prslctd
		
	Update		p
	Set			dscrpcn_csa_estdo_prstcn_slctd = a.dscrpcn_csa_estdo_prstcn_slctd
	From		#tmpPrestacionesDevueltas p
	Inner Join	bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion_Vigencias a With(NoLock)
	On			p.cnsctvo_cdgo_csa_ngcn_prstcn = a.cnsctvo_cdgo_csa_estdo_prstcn_slctd
	Where       @fcha_actl Between a.inco_vgnca And a.fn_vgnca 
	
	UPDATE		p
	SET			dscrpcn_csa_estdo_prstcn_slctd = Ltrim(Rtrim(a.dscrpcn_csa_estdo_prstcn_slctd)) + ', ' + Ltrim(Rtrim(dap.jstfccn))
	FROM		#tmpPrestacionesDevueltas p
	INNER JOIN	bdSisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion_Vigencias a With(NoLock)
	on			a.cnsctvo_cdgo_csa_estdo_prstcn_slctd = p.cnsctvo_cdgo_csa_ngcn_prstcn
	inner join	bdCNA.gsa.tbASDatosAdicionalesPreSolicitud  dap  -- incluido por angela sandoval 2017-01-27
	on			dap.cnsctvo_prslctd = p.cnsctvo_slctd_atrzcn_srvco
	Where       @fcha_actl Between a.inco_vgnca And a.fn_vgnca 

	SET @cnstrccn_trma_xml = (
		SELECT		TSO.id_sld_rcbda				id,
					TAF.eml							correo,
					TAF.eml							co,
					TAF.bcc							bcc,
					TSO.plntlla			            'plantilla',
					(
						SELECT	
								'nmbre_cdd_ips_prmria_afldo'		'parametro/nombre',
								AFI.dscrpcn_sde						'parametro/valor',
								NULL,
								'dia'								'parametro/nombre',
								@dia								'parametro/valor',
								NULL,
								'mes'								'parametro/nombre',
								@mes								'parametro/valor',
								NULL,
								'ano'								'parametro/nombre',
								@anno								'parametro/valor',
								NULL,
								'nmbre_afldo'						'parametro/nombre',
								AFI.nmbre_afldo						'parametro/valor',
								NULL,
								'tbla_prstcns'						'parametro_1/nombre',
									(select 
										Convert(Date, fcha_crcn, 103)  'td',
										null,
										dscrpcn_csa_estdo_prstcn_slctd 'td',
										null
									from #tmpPrestacionesDevueltas
									FOR XML PATH('tr'), TYPE)	'parametro_2/valor/tbody',
								NULL,
								'usrio'								'parametro/nombre',
								RTRIM(LTRIM(@usrio))				'parametro/valor',
								NULL,
								'lnea_ncnal'						'parametro/nombre',
								TAF.lnea_ncnal						'parametro/valor',
								NULL,
								'crreo_atncn_clnte'					'parametro/nombre',
								TAF.crreo_atncn_clnte				'parametro/valor'
						FROM #tmpAflds AFI
						WHERE AFI.id = TAF.id
						FOR XML PATH('parametros'), TYPE
					),
					@dia		'fecha/dia',
					@hra		'fecha/hora',
					@mnto		'fecha/minutos',
					@mes		'fecha/mes',
					@sgndo		'fecha/segundos',
					@anno		'fecha/ano',
					RTRIM(LTRIM(@usrio)) 'usuario'					
		FROM		#tmpAflds TAF
		INNER JOIN	#tmpSlctdes TSO
		ON			TAF.id_sld_rcbda=TSO.id_sld_rcbda
		
		FOR XML PATH('mensajeCorreo')	
		)
		
		Set @string_trama = cast (@cnstrccn_trma_xml as Varchar(4000))
		If EXISTS(SELECT TOP 1 1 FROM #tmpPrestacionesDevueltas)
			BEGIN
				Set @string_trama = (SELECT REPLACE(@string_trama,'</parametro_1>',''));  
				Set @string_trama = (SELECT REPLACE(@string_trama,'<parametro_2>',''));
				Set @string_trama = (SELECT REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">'));
				Set @string_trama = (SELECT REPLACE(@string_trama,'</parametro_2>','</parametro>'));
			
			END
		Else
			BEGIN
				Set @string_trama = (SELECT REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">')); 
				Set @string_trama = (SELECT REPLACE(@string_trama,'</parametro_1>','</parametro>')); 
			END
		
		SET @cnstrccn_trma_xml = CONVERT(XML, @string_trama);


	drop table #tmpPrestacionesDevueltas
END
