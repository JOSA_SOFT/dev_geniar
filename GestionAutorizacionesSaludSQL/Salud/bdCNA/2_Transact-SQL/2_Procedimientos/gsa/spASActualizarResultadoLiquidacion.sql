USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarResultadoLiquidacion]    Script Date: 06/06/2017 9:48:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASActualizarResultadoLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS				  				A\>
* Descripcion			: <\D SE Actualiza el resultado del la liquidación en la tabla 
							  temporal #tbConceptoGastosTmp_1					D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/03/22										FC\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se modifica el procedimiento para que se adicionen los conceptos adicionales 
								generados por liquidacion de paquetes y elimine los conceptos que tienen valor 0
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	09/06/2017 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se modifica el procedimiento para que se adicionen los conceptos adicionales 
								generados por liquidacion de paquetes y elimine los conceptos que tienen valor 0.
								Adicionalmente se modifica la forma de actualizar los conceptos de gasto asociados
								a los paquetes
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	13/06/2017 FM\>
*--------------------------------------------------------------------------------------------------------------------------*/
ALTER Procedure [gsa].[spASActualizarResultadoLiquidacion]
As
Begin

	Set NoCount On	
  
	Declare @cntdd_rgstrs_cncpts_orgnl  Int,
	        @cntdd_rgstrs_cncpts_clcldo Int,
			@valor_cero			        Int			
				
	Create
	Table  #tmpConceptosAdicionales(cnsctvo_cncpto_lqdr               udtConsecutivo,
	                                cnsctvo_srvco_lqdr                udtConsecutivo,
									cnsctvo_cdgo_cncpto_gsto          udtConsecutivo,
									vlr_lqdcn_cncpto                  Int           ,
									cnsctvo_cdgo_clse_hbtcn           udtConsecutivo,
									rsltdo_lqdcn                      Int           ,
									cnsctvo_cdgo_cncpts_dt_lsta_prco  udtConsecutivo,
									cnsctvo_slctd_atrzcn_srvco        udtConsecutivo,
                                    cnsctvo_cdgo_cps                  udtConsecutivo,
                                    cnsctvo_cdgo_frma_atncn           udtConsecutivo,
                                    bltrl	                          udtLogico     , 
                                    msma_va                           udtLogico     ,  
                                    cntdd                             Int           ,
                                    rmpe_pqte                         udtLogico     ,     
                                    espcldd                           Int           , 
                                    agrpdr_srvco_lqdr                 Int           , 
                                    cdgo_intrno_prstdr                udtCodigoIPS  , 
                                    fcha_lqdcn                        Datetime      , 
                                    cnsctvo_cdgo_pln                  udtConsecutivo,
                                    cnsctvo_prcdmnto_insmo_slctdo     udtConsecutivo,
					                cnsctvo_mdcmnto_slctdo            udtConsecutivo,
									cnsctvo_prstcn_pqte               udtConsecutivo
	                               )

	Select @cntdd_rgstrs_cncpts_orgnl  = Count(1) From #tbConceptoGastosTmp_1
	Select @cntdd_rgstrs_cncpts_clcldo = Count(1) From #tbconceptosserviciosaliquidarorigen
	Set    @valor_cero = 0

	If(@cntdd_rgstrs_cncpts_orgnl != @cntdd_rgstrs_cncpts_clcldo)
	   Begin 
	
		  Insert 
		  Into             #tmpConceptosAdicionales(cnsctvo_cncpto_lqdr             , cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto,
		                                            vlr_lqdcn_cncpto                , cnsctvo_cdgo_clse_hbtcn, rsltdo_lqdcn            ,
										            cnsctvo_cdgo_cncpts_dt_lsta_prco, cnsctvo_prstcn_pqte
		                                           )
		  Select           cgt.cnsctvo_cncpto_lqdr             , cgt.cnsctvo_srvco_lqdr     , cgt.cnsctvo_cdgo_cncpto_gsto,
		                   cgt.vlr_lqdcn_cncpto                , cgt.cnsctvo_cdgo_clse_hbtcn, cgt.rsltdo_lqdcn            ,
				           cgt.cnsctvo_cdgo_cncpts_dt_lsta_prco, cgt.cnsctvo_prstcn_pqte
		  From             #tbconceptosserviciosaliquidarorigen cgt 
		  Left Outer Join  #tbConceptoGastosTmp_1 cl
		  On               cl.cnsctvo_srvco_lqdr = cgt.cnsctvo_cncpto_lqdr
		  Where            cl.cnsctvo_cdgo_cncpto_gsto IS Null

		  --Se insertan los conceptos adicionales
		  --SET IDENTITY_INSERT dbo.#tbConceptoGastosTmp_1 On
		
		  Insert
		  Into   #tbConceptoGastosTmp_1(cnsctvo_srvco_lqdrc    , cnsctvo_cdgo_cncpto_gsto, vlr_lqdcn_cncpto                , 
		                                cnsctvo_cdgo_clse_hbtcn, rsltdo_lqdcn            , cnsctvo_cdgo_cncpts_dt_lsta_prco, 
										vlr_lqdcn              , cnsctvo_pqte
	                                   )
		  Select cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto, vlr_lqdcn_cncpto                , 
		         cnsctvo_cdgo_clse_hbtcn, rsltdo_lqdcn            , cnsctvo_cdgo_cncpts_dt_lsta_prco, 
				 vlr_lqdcn_cncpto       , cnsctvo_prstcn_pqte
		  From   #tmpConceptosAdicionales
		  
		 
		  --Select * From #tbConceptoGastosTmp_1
		  --Select * From #tmpConceptosAdicionales

		  --SET IDENTITY_INSERT #tbConceptoGastosTmp_1 Off
		  		 
		  Update      #tmpConceptosAdicionales
		  Set         cnsctvo_slctd_atrzcn_srvco    = a.cnsctvo_slctd_atrzcn_srvco   ,
                      cnsctvo_cdgo_cps              = a.cnsctvo_cdgo_cps             ,
                      cnsctvo_cdgo_frma_atncn       = a.cnsctvo_cdgo_frma_atncn      ,
                      bltrl	                        = a.bltrl                        , 
                      msma_va                       = a.msma_va                      ,  
                      cntdd                         = a.cntdd                        ,
                      rmpe_pqte                     = a.rmpe_pqte                    ,     
                      espcldd                       = a.espcldd	                     , 
                      agrpdr_srvco_lqdr             = a.agrpdr_srvco_lqdr            , 
                      cdgo_intrno_prstdr            = a.cdgo_intrno_prstdr           , 
                      fcha_lqdcn                    = a.fcha_lqdcn                   , 
                      cnsctvo_cdgo_pln              = a.cnsctvo_cdgo_pln             ,
                      cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo,
					  cnsctvo_mdcmnto_slctdo        = a.cnsctvo_mdcmnto_slctdo          
		  From        #tmpConceptosAdicionales x
          Inner Join  #tbConceptoGastosTmp_1 a
		  On          a.cnsctvo_srvco_lqdrc = x.cnsctvo_srvco_lqdr
		  Where       a.cnsctvo_slctd_atrzcn_srvco Is Not Null

		  --Actuliza la informacion de los conceptos
		  Update      #tbConceptoGastosTmp_1
		  Set         cnsctvo_slctd_atrzcn_srvco    = a.cnsctvo_slctd_atrzcn_srvco   ,
                      cnsctvo_cdgo_cps              = a.cnsctvo_cdgo_cps             ,
                      cnsctvo_cdgo_frma_atncn       = a.cnsctvo_cdgo_frma_atncn      ,
                      bltrl	                        = a.bltrl                        , 
                      msma_va                       = a.msma_va                      ,  
                      cntdd                         = a.cntdd                        ,
                      rmpe_pqte                     = a.rmpe_pqte                    ,     
                      espcldd                       = a.espcldd	                     , 
                      agrpdr_srvco_lqdr             = a.agrpdr_srvco_lqdr            , 
                      cdgo_intrno_prstdr            = a.cdgo_intrno_prstdr           , 
                      fcha_lqdcn                    = a.fcha_lqdcn                   , 
                      cnsctvo_cdgo_pln              = a.cnsctvo_cdgo_pln             ,
                      cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo,
					  cnsctvo_mdcmnto_slctdo        = a.cnsctvo_mdcmnto_slctdo          
		  From        #tbConceptoGastosTmp_1 b
          Inner Join  #tmpConceptosAdicionales a 
		  --On          a.cnsctvo_cncpto_lqdr      = b.cnsctvo_srvco_lqdr       And
		  On          a.cnsctvo_cdgo_cncpto_gsto = b.cnsctvo_cdgo_cncpto_gsto And
					  a.cnsctvo_srvco_lqdr       = b.cnsctvo_srvco_lqdrc
		  

		  If Exists (Select Top 1 'X' From #tbConceptoGastosTmp_1 a Where a.cnsctvo_pqte = 0)
		     Begin
			    Update      cgt
                Set	        rsltdo_lqdcn              = Round(slo.rsltdo_lqdcn,0)    ,
                            vlr_lqdcn                 = Round(slo.vlr_lqdcn,0)       ,
                            cnsctvo_cdgo_dt_lsta_prco = slo.cnsctvo_cdgo_dt_lsta_prco
                From	    #tbConceptoGastosTmp_1 cgt
                Inner Join	#tbserviciosaliquidarorigen slo
                On			cgt.cnsctvo_cdgo_cps = slo.cnsctvo_cdgo_cps
				Where       cgt.cnsctvo_pqte     = @valor_cero;

	            Update     cgt
	            Set		   vlr_lqdcn_cncpto                 = Round(cls.vlr_lqdcn_cncpto,0),
                           rsltdo_lqdcnc                    = Round(cls.rsltdo_lqdcn,0),
                           cnsctvo_cdgo_cncpts_dt_lsta_prco = cls.cnsctvo_cdgo_cncpts_dt_lsta_prco
	            From	   #tbConceptoGastosTmp_1 cgt
	            Inner Join #tbconceptosserviciosaliquidarorigen cls
	            On		   cgt.cnsctvo_srvco_lqdr = cls.cnsctvo_cncpto_lqdr
				Where      cgt.cnsctvo_pqte       = @valor_cero			    
			 End

		  Delete From #tbConceptoGastosTmp_1 Where vlr_lqdcn_cncpto = @valor_cero
	   End
	Else
	   Begin
	     Update     cgt
         Set	    rsltdo_lqdcn              = Round(slo.rsltdo_lqdcn,0)    ,
                    vlr_lqdcn                 = Round(slo.vlr_lqdcn,0)       ,
                    cnsctvo_cdgo_dt_lsta_prco = slo.cnsctvo_cdgo_dt_lsta_prco
         From	    #tbConceptoGastosTmp_1 cgt
         Inner Join	#tbserviciosaliquidarorigen slo
         On			cgt.cnsctvo_cdgo_cps = slo.cnsctvo_cdgo_cps;

	     Update     cgt
	     Set		vlr_lqdcn_cncpto                 = Round(cls.vlr_lqdcn_cncpto,0),
                    rsltdo_lqdcnc                    = Round(cls.rsltdo_lqdcn,0),
                    cnsctvo_cdgo_cncpts_dt_lsta_prco = cls.cnsctvo_cdgo_cncpts_dt_lsta_prco
	     From		#tbConceptoGastosTmp_1 cgt
	     Inner Join	#tbconceptosserviciosaliquidarorigen cls
	     On			cgt.cnsctvo_srvco_lqdr = cls.cnsctvo_cncpto_lqdr;	
	   End

	Drop Table #tmpConceptosAdicionales
End
