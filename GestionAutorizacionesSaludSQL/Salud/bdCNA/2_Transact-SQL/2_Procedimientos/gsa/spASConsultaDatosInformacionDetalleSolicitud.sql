USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionDetalleSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosDetalleSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionDetalleSolicitud] 
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare	@ldfcha_consulta		DATETIME;


	Create table #detalleSolicitud(
		cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte udtConsecutivo,
		cdgo_tpo_idntfccn					char(3),
		nmro_idntfccn_mdco_trtnte			udtNumeroIdentificacionLargo,
		rgstro_mdco_trtnte					varchar(15),
		prmr_nmbre_afldo					udtNombre,
		sgndo_nmbre_afldo					udtNombre,
		prmr_aplldo							udtApellido,
		sgndo_aplldo						udtApellido,
		dscrpcn_espcldd						udtDescripcion,
		cnsctvo_cdgo_espcldd_mdco_trtnte	udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
		cnsctvo_cdgo_tpo_idntfccn_ips_slctnte udtConsecutivo,
		cdgo_tpo_idntfccn_ips				char(3),
		nmro_idntfccn_ips_slctnte			udtNumeroIdentificacionLargo,
		cdgo_intrno							udtCodigoIps,
		nmbre_scrsl							udtDescripcion,
		rzn_scl								varchar(200),
		dscrpcn_cdd							varchar(150)
	)

	SET	@ldfcha_consulta = GETDATE();


	Insert into #detalleSolicitud(
		cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
		cdgo_tpo_idntfccn ,
		nmro_idntfccn_mdco_trtnte,
		rgstro_mdco_trtnte			,
		prmr_nmbre_afldo		,
		sgndo_nmbre_afldo		,
		prmr_aplldo		,
		sgndo_aplldo		,
		dscrpcn_espcldd		,
		cnsctvo_cdgo_espcldd_mdco_trtnte,
		cnsctvo_slctd_atrzcn_srvco
	)
	Select	mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
			ti.cdgo_tpo_idntfccn,
			mts.nmro_idntfccn_mdco_trtnte,
			mts.rgstro_mdco_trtnte,			
			m.prmr_nmbre_afldo,
            m.sgndo_nmbre_afldo,
            m.prmr_aplldo,
            m.sgndo_aplldo,
            e.dscrpcn_espcldd,
			e.cnsctvo_cdgo_espcldd,
			@cnsctvo_slctd_atrzcn_srvco
	From	BDCna.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios mts With(NoLock)
	INNER JOIN  BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias		ti 	With(NoLock) on ti.cnsctvo_cdgo_tpo_idntfccn = mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	
	inner join bdSisalud..tbPrestadores p WITH (NOLOCK) on p.cnsctvo_cdgo_tpo_idntfccn = mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	and p.nmro_idntfccn = mts.nmro_idntfccn_mdco_trtnte
	Inner join bdSisalud..tbMedicos m WITH (NOLOCK) on m.nmro_unco_idntfccn_prstdr=p.nmro_unco_idntfccn_prstdr
	inner join bdSisalud..tbEspecialidades_Vigencias e WITH (NOLOCK) on m.cnsctvo_cdgo_espcldd=e.cnsctvo_cdgo_espcldd
	Where	mts.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And		@ldfcha_consulta between ti.inco_vgnca and ti.fn_vgnca
	And		@ldfcha_consulta between e.inco_vgnca and e.fn_vgnca


	if not exists(Select 1 from #detalleSolicitud)
		begin
			Insert into #detalleSolicitud(
				cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
				cdgo_tpo_idntfccn ,
				nmro_idntfccn_mdco_trtnte,
				rgstro_mdco_trtnte			,
				prmr_nmbre_afldo		,
				sgndo_nmbre_afldo		,
				prmr_aplldo		,
				sgndo_aplldo		,
				dscrpcn_espcldd		,
				cnsctvo_cdgo_espcldd_mdco_trtnte,
				cnsctvo_slctd_atrzcn_srvco
			)
			Select	mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
					ti.cdgo_tpo_idntfccn,
					mts.nmro_idntfccn_mdco_trtnte,
					mts.rgstro_mdco_trtnte,			
					gm.prmr_nmbre,
					gm.sgndo_nmbre,
					gm.prmr_aplldo,
					gm.sgndo_aplldo,
					e.dscrpcn_espcldd,
					mts.cnsctvo_cdgo_espcldd_mdco_trtnte,
					@cnsctvo_slctd_atrzcn_srvco
			From	BDCna.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios mts With(NoLock)
			INNER JOIN  BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias		ti 	With(NoLock) on ti.cnsctvo_cdgo_tpo_idntfccn = mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	
			INNER JOIN bdSisalud.dbo.tbGenericosMedicos gm With(NoLock) on gm.cnsctvo_cdgo_tpo_idntfccn = mts.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte And gm.nmro_idntfccn = mts.nmro_idntfccn_mdco_trtnte
			inner join bdSisalud..tbEspecialidades_Vigencias e WITH (NOLOCK) on gm.cnsctvo_cdgo_espcldd=e.cnsctvo_cdgo_espcldd
			Where	mts.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			And		@ldfcha_consulta between ti.inco_vgnca and ti.fn_vgnca
			And		@ldfcha_consulta between e.inco_vgnca and e.fn_vgnca
		end;

	Update ds	Set	cnsctvo_cdgo_tpo_idntfccn_ips_slctnte = Ips.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte,
					cdgo_tpo_idntfccn_ips = ti.cdgo_tpo_idntfccn,
					nmro_idntfccn_ips_slctnte = Ips.nmro_idntfccn_ips_slctnte,
					cdgo_intrno = Ips.cdgo_intrno,
					nmbre_scrsl = DP.nmbre_scrsl, 
					rzn_scl		= DP.nmbre_scrsl,
					dscrpcn_cdd = C.dscrpcn_cdd 
	From #detalleSolicitud ds
	inner join BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios Ips WITH (NOLOCK)on ds.cnsctvo_slctd_atrzcn_srvco = Ips.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN  BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias	ti 	With(NoLock) on ti.cnsctvo_cdgo_tpo_idntfccn = Ips.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	
	INNER JOIN  bdSisalud.dbo.tbDireccionesPrestador DP WITH (NOLOCK) ON DP.cdgo_intrno = Ips.cdgo_intrno
	LEFT JOIN  bdSisalud.dbo.tbPrestadores IPS_ WITH (NOLOCK) ON IPS_.nmro_unco_idntfccn_prstdr =  DP.nmro_unco_idntfccn_prstdr
	INNER JOIN bdAfiliacionValidador..TBCIUDADES_Vigencias    C WITH (NOLOCK) on dp.cnsctvo_cdgo_cdd  = C.cnsctvo_cdgo_cdd
	Where	Ips.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And		@ldfcha_consulta between ti.inco_vgnca and ti.fn_vgnca
	And		@ldfcha_consulta between C.inco_vgnca and C.fn_vgnca

	SELECT 		
		cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,
		cdgo_tpo_idntfccn,		nmro_idntfccn_mdco_trtnte,
		rgstro_mdco_trtnte,		prmr_nmbre_afldo,
		sgndo_nmbre_afldo,		prmr_aplldo,
		sgndo_aplldo,			dscrpcn_espcldd,
		cnsctvo_cdgo_espcldd_mdco_trtnte,
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_cdgo_tpo_idntfccn_ips_slctnte,
		cdgo_tpo_idntfccn_ips,		nmro_idntfccn_ips_slctnte,
		cdgo_intrno,			nmbre_scrsl,		
		rzn_scl,				dscrpcn_cdd
   from #detalleSolicitud sol

	DROP TABLE #detalleSolicitud

END

GO
