USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASImpresionFormatoDevuelta]    Script Date: 30/03/2017 16:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASImpresionFormatoDevuelta
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información requerida para la generacion+
						   del formato de devolucion 
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           @cnsctvo_slctd_atrzcn_srvco udtConsecutivo  
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 31/03/2016 FC\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                            Se modifica el procedimiento para realizar la extraccion de los campos
							estado del servicio y  observaciones de la nueva estructura creada. 
							Adicionalmente se adiciona la consulta para la extraccion de los servicios
							devueltos por el BPM
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016-11-25 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                            Se modifica el procedimiento para mostrar la informacion de capitacion en el formato de
							devolucion							
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-02-03 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                            Se modifica el procedimiento cambiando la descripcion dscrpcn_no_cnfrmdd_vldcn 
							para mostrar el campo mnsje_no_cnfrmdd_usro que corresponde al mensaje para el afiliado 							
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-03-08 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                            Se modifica el procedimiento para que valide si la devolucion es por acceso directo						
                       D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-03-10 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASImpresionFormatoDevuelta] 2243 --bpm y malla 
--exec [gsa].[spASImpresionFormatoDevuelta] 2139 --malla 
--exec [gsa].[spASImpresionFormatoDevuelta] 2239 --malla
--exec [gsa].[spASImpresionFormatoDevuelta] 3615
--exec [gsa].[spASImpresionFormatoDevuelta] 130687
--exec [gsa].[spASImpresionFormatoDevuelta] 130703
--exec [gsa].[spASImpresionFormatoDevuelta] 130720
--exec [gsa].[spASImpresionFormatoDevuelta] 130789



ALTER PROCEDURE [gsa].[spASImpresionFormatoDevuelta] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo 
AS

Begin
   SET NOCOUNT ON

   Declare @ldFechaActual                        Datetime      ,
           @lcFormato                            Varchar(10)   ,
           @lcDscrpcn_mnsje	                     udtDescripcion,
		   @lnCnsctvo_cdgo_mnsje                 udtConsecutivo,
		   @lcAux                                Char(1)       ,
		   @lnEstdo_srvco_slctdo                 udtConsecutivo,
		   @eml                                  udtEmail      ,
		   @lcCdgo_prmtro_prmtro_eml             Char(3)       ,
		   @lcCdgo_prmtro_prmtro_accso_drcto_pac Char(3)       ,
		   @lcCdgo_prmtro_prmtro_accso_drcto_pos Char(3)       ,
		   @vsble_usro                           udtLogico     ,
		   @tpo_dto_prmtro		                 udtLogico     ,
		   @vlr_prmtro_nmrco                     Numeric(18,0) ,
		   @vlr_prmtro_crctr                     Char(200)     ,
		   @vlr_prmtro_fcha                      Datetime      ,
		   @lcEstdo_no_cnfrmdd                   udtConsecutivo,
		   @lcOrgn_dvlcn_mll                     udtdescripcion,
		   @lcOrgn_dvlcn_bpm                     udtdescripcion,
		   @lcVacio                              udtLogico     ,
		   @ValorS                               udtLogico     ,
		   @no_cnfrmdd_vldcn_cptdo               udtConsecutivo,
		   @no_cnfrmdd_vldcn_csto_prspctvo       udtConsecutivo,
		   @no_cnfrmdd_vldcn_pf                  udtConsecutivo,
		   @cmdn_ips                             Varchar(20)   ,
		   @cmdn_tlfno                           Varchar(20)   ,
		   @cnsctvo_cdgo_tpo_mrca                udtConsecutivo,
		   @cnsctvo_cdgo_pln                     udtConsecutivo,
		   @mnsje_dvlcn_accso_drcto_pos          udtdescripcion,
		   @mnsje_dvlcn_accso_drcto_pac          udtdescripcion

   Create 
   Table  #tmpMax(cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
                  cnsctvo_srvco_slctdo       udtConsecutivo, 
				  fcha_dvlcn                 Datetime
                 )

   Create
   Table  #accesoDirecto(cnsctvo_slctd_atrzcn_srvco      udtConsecutivo,
                         cnsctvo_srvco_slctdo            udtConsecutivo,
						 nmbre_scrsl                     udtDescripcion,
					     tlfno                           udtTelefono   ,
						 cnsctvo_cdgo_pln                udtConsecutivo
						)                        

   Create
   Table  #devolucion(cnsctvo_slctd_atrzcn_srvco      udtConsecutivo            ,
                      fcha_crcn                       Datetime                  ,
					  cnsctvo_cdgo_tpo_cntrto         udtConsecutivo            ,
					  nmro_cntrto                     udtNumeroFormulario       ,
					  nmro_unco_idntfccn_afldo        udtConsecutivo            ,
					  fcha_dvlcn                      Datetime                  ,
					  nmbre_cmplto                    udtdescripcion            ,
					  cdgo_ips_prmra                  udtCodigoIPS              ,
					  dscrpcn_cdd                     udtdescripcion            ,
					  dscrpcn_mnsje                   udtdescripcion  Default '',
					  cnsctvo_srvco_slctdo            udtConsecutivo            ,
					  cnsctvo_cdgo_rgla_vldcn         udtConsecutivo            ,
					  dscrpcn_no_cnfrmdd_vldcn	      Varchar(1000)             ,
					  dscrpcn_estdo_srvco_mstrr_afldo udtDescripcion            ,
					  cdgo_cdfccn	                  Varchar(11)               ,       
                      dscrpcn_cdfccn                  udtdescripcion            ,
					  cnsctvo_cdgo_srvco_slctdo       udtConsecutivo	        ,
					  orgn_dvlcn	                  udtdescripcion		    ,
					  prstcn_cptda                    udtLogico                 ,
					  cnsctvo_cdgo_no_cnfrmdd_vldcn   udtConsecutivo            ,
					  nmbre_scrsl                     udtDescripcion            ,
					  tlfno                           udtTelefono               ,
					  mnsje_no_cnfrmdd_usro           udtDescripcion            ,
					  cnsctvo_cdgo_pln                udtConsecutivo
					 ) 
 
	Set @ldFechaActual                        = getDate()
	Set @lcFormato                            = 'dd/MM/yyyy'
	Set @lnCnsctvo_cdgo_mnsje                 = 1	
	Set @lcAux                                = ' '
	Set @lnEstdo_srvco_slctdo                 = 6 -- Devuelta
	Set @lcCdgo_prmtro_prmtro_eml             = '92'
	Set @lcCdgo_prmtro_prmtro_accso_drcto_pac = '6'
	Set @lcCdgo_prmtro_prmtro_accso_drcto_pos = '7'
	Set @vsble_usro                           = 'S'
	Set @tpo_dto_prmtro                       = ''
	Set @lcEstdo_no_cnfrmdd                   = 3 --No conformidad Devuelta Malla
	Set @lcOrgn_dvlcn_mll                     = 'Malla'
	Set @lcOrgn_dvlcn_bpm                     = 'bpm'
	Set @lcVacio                              = ''
	Set @ValorS                               = 'S'
	Set @no_cnfrmdd_vldcn_cptdo               = 65
	Set @no_cnfrmdd_vldcn_csto_prspctvo       = 48
	Set @no_cnfrmdd_vldcn_pf                  = 68
	Set @cmdn_ips                             = '<IPS>'
	Set @cmdn_tlfno                           = '<TELEFONO>'
	Set @cnsctvo_cdgo_tpo_mrca                = 7 --Marca ACCESO DIRECTO
	Set @cnsctvo_cdgo_pln                     = 1 --POS


	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_prmtro_eml, @vsble_usro, @lcCdgo_prmtro_prmtro_eml, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	Set  @eml = @vlr_prmtro_crctr 
	
	---Se insertan en la tabla la maxima fecha de devolucion
	Insert 
	Into     #tmpMax(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, fcha_dvlcn)
	Select   cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, Max(fcha_ultma_mdfccn)
	From     bdCNA.gsa.tbASServiciosSolicitados WITH (NOLOCK)
	Where    cnsctvo_cdgo_estdo_srvco_slctdo = @lnEstdo_srvco_slctdo
	And      cnsctvo_slctd_atrzcn_srvco      = @cnsctvo_slctd_atrzcn_srvco   
	Group By cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo

	---Se insertan en la tabla la informacion de las prestaciones devueltas x el proceso de la malla
	Insert
	Into       #devolucion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo         , 
	                       cnsctvo_cdgo_rgla_vldcn   , dscrpcn_no_cnfrmdd_vldcn     ,
						   cnsctvo_cdgo_srvco_slctdo , orgn_dvlcn                   ,
						   prstcn_cptda              , cnsctvo_cdgo_no_cnfrmdd_vldcn,
						   mnsje_no_cnfrmdd_usro
						  )
	Select     b.cnsctvo_slctd_atrzcn_srvco, a.llve_prmra_cncpto_prncpl_vlddo, 
	           a.cnsctvo_cdgo_rgla_vldcn   , n.mnsje_no_cnfrmdd_usro         ,
			   c.cnsctvo_cdgo_srvco_slctdo , @lcOrgn_dvlcn_mll               ,
			   c.prstcn_cptda              , n.cnsctvo_cdgo_no_cnfrmdd_vldcn ,
			   n.mnsje_no_cnfrmdd_usro
    From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH (NOLOCK)
    Inner Join bdCNA.gsa.tbASServiciosSolicitados c WITH (NOLOCK)	
    On         c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
    Inner Join bdprocesosSalud.dbo.tbResultadosProcesoMallaValidacion a WITH (NOLOCK)
    On         a.cnsctvo_prcso                  = b.cnsctvo_prcso              And
               a.llve_prmra_rgstro_vlddo        = b.cnsctvo_slctd_atrzcn_srvco And 
               a.llve_prmra_cncpto_prncpl_vlddo = c.cnsctvo_srvco_slctdo 
    Inner Join bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias n WITH (NOLOCK)
	On         n.cnsctvo_cdgo_no_cnfrmdd_vldcn   = a.cnsctvo_cdgo_no_cnfrmdd_vldcn
	Inner Join bdMallaCNA.dbo.tbReglasValidacion_Vigencias r WITH (NOLOCK)
	On         r.cnsctvo_cdgo_no_cnfrmdd_vldcn = n.cnsctvo_cdgo_no_cnfrmdd_vldcn
	Where      c.cnsctvo_cdgo_estdo_srvco_slctdo = @lnEstdo_srvco_slctdo
	And        b.cnsctvo_slctd_atrzcn_srvco      = @cnsctvo_slctd_atrzcn_srvco
	And        n.cnsctvo_cdgo_estdo_no_cnfrmdd   = @lcEstdo_no_cnfrmdd  
	Order By   b.cnsctvo_slctd_atrzcn_srvco, a.llve_prmra_cncpto_prncpl_vlddo
	
	---Se insertan en la tabla la informacion de las prestaciones devueltas en el bpm
	Insert
	Into       #devolucion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo         , 
	                       cnsctvo_cdgo_rgla_vldcn   , dscrpcn_no_cnfrmdd_vldcn     ,
						   cnsctvo_cdgo_srvco_slctdo , orgn_dvlcn                   ,
						   prstcn_cptda              , cnsctvo_cdgo_no_cnfrmdd_vldcn,
						   mnsje_no_cnfrmdd_usro
						  )
	Select     b.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo         , 
	           r.cnsctvo_cdgo_rgla_vldcn   , n.mnsje_no_cnfrmdd_usro        ,
		       c.cnsctvo_cdgo_srvco_slctdo , @lcOrgn_dvlcn_bpm              ,
			   c.prstcn_cptda              , n.cnsctvo_cdgo_no_cnfrmdd_vldcn,
			   n.mnsje_no_cnfrmdd_usro
    From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH (NOLOCK)
    Inner Join bdCNA.gsa.tbASServiciosSolicitados c WITH (NOLOCK)
    On         c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
    Inner Join bdCNA.gsa.tbASResultadoGestionAuditor a WITH (NOLOCK)
    On         a.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo  
    Inner Join bdCNA.gsa.tbASResultadoDetalleGestionAuditor g WITH (NOLOCK)
    On         g.cnsctvo_cdgo_rsltdo_gstn_adtr = a.cnsctvo_cdgo_rsltdo_gstn_adtr
    Inner Join bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias n WITH (NOLOCK)
    On         n.cnsctvo_cdgo_no_cnfrmdd_vldcn   = g.cnsctvo_cdgo_no_cnfrmdd_vldcn
	Inner Join bdMallaCNA.dbo.tbReglasValidacion_Vigencias r WITH (NOLOCK)
	On         r.cnsctvo_cdgo_no_cnfrmdd_vldcn = n.cnsctvo_cdgo_no_cnfrmdd_vldcn
    Where      b.cnsctvo_slctd_atrzcn_srvco            = @cnsctvo_slctd_atrzcn_srvco
    And        c.cnsctvo_cdgo_estdo_srvco_slctdo       = @lnEstdo_srvco_slctdo
    And        a.cnsctvo_cdgo_estdo_no_cnfrmdd         = @lnEstdo_srvco_slctdo
    And        n.cnsctvo_cdgo_estdo_no_cnfrmdd         = @lcEstdo_no_cnfrmdd  	
	Order By   b.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo


	Update     #devolucion
	Set        dscrpcn_no_cnfrmdd_vldcn = n.dscrpcn_no_cnfrmdd_vldcn
	From       #devolucion a
	Inner Join bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias n WITH (NOLOCK)
    On         n.cnsctvo_cdgo_no_cnfrmdd_vldcn   = a.cnsctvo_cdgo_no_cnfrmdd_vldcn
	Where      a.dscrpcn_no_cnfrmdd_vldcn Is NULL Or a.dscrpcn_no_cnfrmdd_vldcn = @lcVacio


	If Exists (Select 1 From #devolucion Where prstcn_cptda = @ValorS)
	  Begin
	      Update     #devolucion
		  Set        nmbre_scrsl = b.nmbre_scrsl,
		             tlfno       = b.tlfno
		  From       #devolucion a
		  Inner Join bdCNA.gsa.tbASServiciosSolicitados c WITH (NOLOCK)
		  On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		  Inner Join bdSisalud.dbo.tbDireccionesPrestador b WITH (NOLOCK)
		  On         b.cdgo_intrno = c.cdgo_intrno_cpta
		  Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

		  Update     #devolucion
		  Set        dscrpcn_no_cnfrmdd_vldcn = Replace(Replace(a.mnsje_no_cnfrmdd_usro, @cmdn_ips, ISNULL(LTRIM(RTRIM(a.nmbre_scrsl)), @lcVacio)), @cmdn_tlfno, ISNULL(LTRIM(RTRIM(a.tlfno)), @lcVacio))
		  From       #devolucion a
		  Inner Join bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias n WITH (NOLOCK)
		  On         n.cnsctvo_cdgo_no_cnfrmdd_vldcn = a.cnsctvo_cdgo_no_cnfrmdd_vldcn
		  Where      a.cnsctvo_cdgo_no_cnfrmdd_vldcn In(@no_cnfrmdd_vldcn_csto_prspctvo, @no_cnfrmdd_vldcn_cptdo, @no_cnfrmdd_vldcn_pf)
	End 

	/*Se valida si la devolucion es por ACCESO DIRECTO*/
	If Exists (Select	  1 
	           From       #devolucion a
			   Inner Join bdCNA.gsa.tbASMarcasServiciosSolicitados b With(NoLock)
			   On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	           Where      b.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca
			  )
	  Begin

	     /*Procedimiento que permite consultar los parametros generales*/
		 Exec BDCna.dbo.spASTraerParametroGeneral @lcCdgo_prmtro_prmtro_accso_drcto_pos, @vsble_usro, @lcCdgo_prmtro_prmtro_accso_drcto_pos, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
		 Set  @mnsje_dvlcn_accso_drcto_pos = @vlr_prmtro_crctr

		  /*Procedimiento que permite consultar los parametros generales*/
		 Exec BDCna.dbo.spASTraerParametroGeneral @lcCdgo_prmtro_prmtro_accso_drcto_pac, @vsble_usro, @lcCdgo_prmtro_prmtro_accso_drcto_pac, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
		 Set  @mnsje_dvlcn_accso_drcto_pac = @vlr_prmtro_crctr


	     Update     #devolucion
		 Set        cnsctvo_cdgo_pln = b.cnsctvo_cdgo_pln
		 From       #devolucion a 
		 Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b With(NoLock)
		 On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco		  

		 If Exists(Select 1 From #devolucion Where cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln)
			Begin			   
			    Insert
				Into       #accesoDirecto(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, nmbre_scrsl,
										  tlfno                     , cnsctvo_cdgo_pln                
										 )  
				Select     Distinct a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo, b.nmbre_scrsl,
						   b.tlfno                              , a.cnsctvo_cdgo_pln
				From       #devolucion a
				Inner Join bdCNA.gsa.tbASServiciosSolicitados c With(NoLock)
				On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
				Inner Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados p With(NoLock)
				On         p.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
				Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado s With(NoLock)
				On         s.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
				Inner Join bdSisalud.dbo.tbDireccionesPrestador b With(NoLock)
				On         b.cdgo_intrno = s.cdgo_intrno_prstdr_atrzdo
				Inner Join bdCNA.gsa.tbASMarcasServiciosSolicitados m With(NoLock)
				On         m.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo	           
				Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
				And        m.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca
			    And        a.cnsctvo_cdgo_pln      = @cnsctvo_cdgo_pln

				Insert
				Into       #accesoDirecto(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, nmbre_scrsl,
									      tlfno                     , cnsctvo_cdgo_pln                
									     )  
				Select     Distinct a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo, b.nmbre_scrsl,
						   b.tlfno                              , a.cnsctvo_cdgo_pln
				From       #devolucion a
				Inner Join bdCNA.gsa.tbASServiciosSolicitados c With(NoLock)
				On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
				Inner Join bdCNA.gsa.tbASMedicamentosSolicitados p With(NoLock)
				On         p.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
				Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado s With(NoLock)
				On         s.cnsctvo_mdcmnto_slctdo = p.cnsctvo_mdcmnto_slctdo
				Inner Join bdSisalud.dbo.tbDireccionesPrestador b With(NoLock)
				On         b.cdgo_intrno = s.cdgo_intrno_prstdr_atrzdo
				Inner Join bdCNA.gsa.tbASMarcasServiciosSolicitados m With(NoLock)
				On         m.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo	           
				Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
				And        m.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca
				And        a.cnsctvo_cdgo_pln      = @cnsctvo_cdgo_pln
			   		   			   
			    Update     #devolucion
			    Set        dscrpcn_no_cnfrmdd_vldcn = Replace(Replace(@mnsje_dvlcn_accso_drcto_pos, @cmdn_ips, ISNULL(LTRIM(RTRIM(n.nmbre_scrsl)), @lcVacio)), @cmdn_tlfno, ISNULL(LTRIM(RTRIM(n.tlfno)), @lcVacio))
			    From       #devolucion a
			    Inner Join #accesoDirecto n 
			    On         n.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco And
			               n.cnsctvo_srvco_slctdo       = a.cnsctvo_srvco_slctdo		  
			End	   
		 Else
		   Begin
		      Update     #devolucion
			  Set        dscrpcn_no_cnfrmdd_vldcn = @mnsje_dvlcn_accso_drcto_pac			    
		   End		  
	  End 
	
	--Se actualiza la descripcion del estado del servicio
	Update     #devolucion
	Set        dscrpcn_estdo_srvco_mstrr_afldo = e.dscrpcn_estdo_srvco_mstrr_afldo
	From       #devolucion a
	Inner Join bdMallaCNA.dbo.tbReglasValidacion_Vigencias b WITH (NOLOCK)
	On         b.cnsctvo_cdgo_rgla_vldcn = a.cnsctvo_cdgo_rgla_vldcn
	Inner Join bdMallaCNA.dbo.tbASEstadosServicioMostrarAfiliado_Vigencias e WITH (NOLOCK)
	On         e.cnsctvo_cdgo_estdo_srvco_mstrr_afldo = b.cnsctvo_cdgo_estdo_srvco_mstrr_afldo
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	And        @ldFechaActual Between e.inco_vgnca And e.fn_vgnca
		
	---Se actualiza  en la tabla la maxima fecha de modificacion para la prestaciones devueltas
	Update     #devolucion
	Set        fcha_dvlcn = b.fcha_dvlcn
	From       #devolucion a
	Inner Join #tmpMax b 	
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco And
	           b.cnsctvo_srvco_slctdo       = a.cnsctvo_srvco_slctdo	

	--Se actualiza la fecha de creacion de la solicitud
	Update     #devolucion
	Set        fcha_crcn = b.fcha_crcn
	From       #devolucion a
	Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH(NOLOCK) 
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco


	/*Se actualiza el codigo y la descripcion del servicio*/
    Update     #devolucion
    Set        cdgo_cdfccn    = c.cdgo_cdfccn   ,
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #devolucion a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo

	
	---Se actualiza la informacion del afiliado
	Update      #devolucion
	Set         nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo,
				cnsctvo_cdgo_tpo_cntrto  = b.cnsctvo_cdgo_tpo_cntrto ,					          
				nmro_cntrto              = b.nmro_cntrto             ,
				cdgo_ips_prmra           = b.cdgo_ips_prmra   				                 				
    From        #devolucion a
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
	On          b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco

	---Se actualiza la informacion basica del afiliado
	Update      #devolucion
	Set         nmbre_cmplto = CONCAT(LTRIM(RTRIM(c.prmr_nmbre))  , @lcAux, 
								      LTRIM(RTRIM(c.sgndo_nmbre)) , @lcAux,
									  LTRIM(RTRIM(c.prmr_aplldo)) , @lcAux,
	                                  LTRIM(RTRIM(c.sgndo_aplldo))
									 )				 				
	From        #devolucion a	
	Inner Join  bdAfiliacionValidador.dbo.tbBeneficiariosValidador c WITH (NOLOCK)
	On          c.cnsctvo_cdgo_tpo_cntrto  = a.cnsctvo_cdgo_tpo_cntrto  And
	            c.nmro_cntrto              = a.nmro_cntrto              And
				c.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn_afldo
	Where       @ldFechaActual Between c.inco_vgnca_bnfcro And c.fn_vgnca_bnfcro 

	---Se actualiza la informacion de la ciudad
	Update     #devolucion
	Set        dscrpcn_cdd = IsNull(Ltrim(Rtrim(b.dscrpcn_cdd)), @lcAux)
	From       #devolucion a
	Inner Join bdSisalud.dbo.tbDireccionesPrestador p WITH(NOLOCK)
	On         p.cdgo_intrno = a.cdgo_ips_prmra
	Inner Join bdSiSalud.dbo.tbCiudades_vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_cdd = p.cnsctvo_cdgo_cdd
	Where      @ldFechaActual Between p.inco_vgnca And p.fn_vgnca
	And        @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--Se saca el mensaje de atencion de la ops
	Select    @lcDscrpcn_mnsje = dscrpcn_mnsje	
	From      bdsisalud.dbo.tbMensajesImpresion_vigencias WITH(NOLOCK)
	Where     cnsctvo_cdgo_mnsje = @lnCnsctvo_cdgo_mnsje
	And       @ldFechaActual Between inco_vgnca And fn_vgnca

    --Se actualiza el mensaje en la tabla
	Update    #devolucion 
	Set       dscrpcn_mnsje = @lcDscrpcn_mnsje

	Select    IsNull(dscrpcn_cdd, @lcVacio) dscrpcn_cdd				, FORMAT(fcha_dvlcn, @lcFormato) fcha_dvlcn			  , 
	          nmbre_cmplto                             				, FORMAT(fcha_crcn , @lcFormato) fcha_crcn  		  , 
			  cdgo_cdfccn                              				, dscrpcn_cdfccn                            		  , 
			  dscrpcn_estdo_srvco_mstrr_afldo AS dscrpcn_rgla_vldcn	, dscrpcn_no_cnfrmdd_vldcn as infrmcn_adcnl_no_cnfrmdd,	 
			  dscrpcn_mnsje                            				, @eml As email                             		  , 
			  orgn_dvlcn
	From      #devolucion
	Order By  dscrpcn_cdfccn
		
	Drop Table #devolucion
	Drop Table #tmpMax
	Drop Table #accesoDirecto
End	

