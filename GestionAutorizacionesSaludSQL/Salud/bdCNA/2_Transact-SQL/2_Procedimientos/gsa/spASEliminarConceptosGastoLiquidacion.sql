USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEliminarConceptosGastoLiquidacion]    Script Date: 28/03/2017 17:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     		: spASEliminarConceptosGastoLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS A\>
* Descripcion			: <\D 
                              Eliminar los conceptos de gasto asociados a las prestaciones que se desean 
							  reliquidar				
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 01/06/2016 FC\>
*---------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Carlos Andrés López Ramírez AM\>
* Descripcion			 : <\DM 
                                Se agrega update para la elimnacion del histórico de descarga de formatos 
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 07/12/2016 FM\>
*---------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM 
                                Se modifica el procedimiento para que elimine del historico los 
								conceptos que tienen numero unico de OPS igual a 0 o NULL
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 29/03/2017 FM\>
*---------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASEliminarConceptosGastoLiquidacion]
AS
BEGIN
	SET NOCOUNT ON; 

	--Eliminación histórico de conceptos de gasto procedimientos
	Delete		HEC 
	From		gsa.tbASHistoricoEstadosConceptosServicioSolicitado HEC With(NoLock)
	Inner Join	gsa.tbASConceptosServicioSolicitado CSS With(NoLock)
	On			CSS.cnsctvo_cncpto_prcdmnto_slctdo = HEC.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	#cnslddo_slctud CS With(NoLock)
	On			CS.cnsctvo_prcdmnto_insmo_slctdo = CSS.cnsctvo_prcdmnto_insmo_slctdo
	Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL;

	--Eliminación histórico de conceptos de gasto medicamentos
	Delete		HEC 
	From		gsa.tbASHistoricoEstadosConceptosServicioSolicitado HEC With(NoLock)
	Inner Join	gsa.tbASConceptosServicioSolicitado CSS With(NoLock)
	On			CSS.cnsctvo_cncpto_prcdmnto_slctdo = HEC.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	#cnslddo_slctud CS With(NoLock)
	On			CS.cnsctvo_mdcmnto_slctdo = CSS.cnsctvo_mdcmnto_slctdo
	Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL;

	--Eliminación histórico de descarga de formatos --qvisionclr
	Delete		HDF 
	From		gsa.tbASHistoricoDescargaFormatos HDF With(NoLock)
	Inner Join	gsa.tbASConceptosServicioSolicitado CSS With(NoLock)
	On			CSS.cnsctvo_cncpto_prcdmnto_slctdo = HDF.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	#cnslddo_slctud CS With(NoLock)
	On			CS.cnsctvo_prcdmnto_insmo_slctdo = CSS.cnsctvo_prcdmnto_insmo_slctdo
	Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL;

	--Eliminación de los conceptos de gasto procedimientos
	IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.cnsctvo_prcdmnto_insmo_slctdo IS NOT NULL)
		BEGIN			
			Delete		CSS
			From		gsa.tbASConceptosServicioSolicitado CSS
			Inner Join	#cnslddo_slctud CS
			On			CSS.cnsctvo_prcdmnto_insmo_slctdo = CS.cnsctvo_prcdmnto_insmo_slctdo
			Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL
		END	

	--Eliminación de los conceptos de gasto medicamentos
	IF EXISTS (SELECT 1 FROM #cnslddo_slctud CS WHERE CS.cnsctvo_mdcmnto_slctdo IS NOT NULL)
		BEGIN
			Delete		CSS
			FROM		gsa.tbASConceptosServicioSolicitado CSS
			Inner Join	#cnslddo_slctud CS
			On			CSS.cnsctvo_mdcmnto_slctdo = CS.cnsctvo_mdcmnto_slctdo
			Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL
		END	
END;
