USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosIndicadoresBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosIndicadoresBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS					AM\>
* Descripcion			 : <\DM	Se eliminan los parametros de entrada			
								@cnsctvo_slctd_atrzcn_srvco y @id_tsk_prcss	dado 
								que no se necesitan en este SP. Se organiza el select
								por el campo id_indcdrDM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 	2016/08/12									FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosIndicadoresBPM]

AS
BEGIN

	SET NOCOUNT ON;

	SELECT id_indcdr, dscrpcn_indcdr
	FROM BDCna.ind.tbASIndicador With(NoLock)
	ORDER BY id_indcdr ASC
END

GO
