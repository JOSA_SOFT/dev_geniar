USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarTemporalesAnulacionCuotasRecuperacion]    Script Date: 12/06/2017 03:18:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASPoblarTemporalesAnulacionCuotasRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D 1. Para Masivo Poblar tablas temporales para el cálculo de Cuotas de Recuperación que cumpla la condición
							que las prestaciones tenga fecha de entrega el día siguiente a la fecha actual y el estado de 
							los servicios esten en estado '7-Aprobado'	
						  2. Por Demanda se Poblan tablas temporales para la solicitud enviada sin importar la fecha de entrega
						    y que  los servicios esten en estado '7-Aprobado' 								D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																									P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/25																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM Se agrega update para recuperar el rango salarial del afiliado,  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM @ValorCERO: guarda valor igual 0 VM\>  													
* Fecha Modificacion	: <\FM 21/12/2016 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM Se modifica consulta, se agrega cruce con el nmro_unco_ops en los cruces y la 
							   anulacion de cuotas para los estados Entregada y procesada DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM @ValorCERO: guarda valor igual 0 VM\>  													
* Fecha Modificacion	: <\FM 02/06/2016 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASPoblarTemporalesAnulacionCuotasRecuperacion]

AS

Begin
	SET NOCOUNT ON

	Declare		@cnsctvo_cdgo_estdo_slctd11		UdtConsecutivo = 11,  -- 'Estado Solicitud '11-Autorizada'
				@cnsctvo_cdgo_estdo_slctd15		udtConsecutivo,
				@cnsctvo_cdgo_estdo_slctd13		udtConsecutivo,
				@cnsctvo_cdgo_estdo_cta_rcprcn1	UdtConsecutivo = 1, --Estado Cuota de Recuperacion '1-Precalculado'
				@prncplSI						UdtLogico = 'S',
				@fechaactual					Datetime = getdate(),  -- fecha actual
				@ValorCERO						int;

	Set @ValorCERO = 0;			
	Set @cnsctvo_cdgo_estdo_slctd15 = 15;
	Set @cnsctvo_cdgo_estdo_slctd15 = 13;		

   -- Paso 1. Poblar Temporal de Solicitudes 
   Insert		#TempSolicitudes
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,				nmro_unco_idntfccn_afldo,
				cnsctvo_cdgo_tpo_cntrto,				nmro_cntrto,						cnsctvo_bnfcro
	)
	Select distinct     
				a.cnsctvo_slctd_atrzcn_srvco,			c.cnsctvo_srvco_slctdo,				b.nmro_unco_idntfccn_afldo,
				b.cnsctvo_cdgo_tpo_cntrto,				b.nmro_cntrto,						b.cnsctvo_bnfcro_cntrto		
	from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
	inner join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
	on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	inner join	gsa.tbASServiciosSolicitados c with(nolock)
	on			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	inner join  #tmpSolicitudAnulacionCuotasRecuperacion t
	on			t.cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco
	And			t.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdgo_srvco_slctdo
	where		c.cnsctvo_cdgo_estdo_srvco_slctdo In (@cnsctvo_cdgo_estdo_slctd11, @cnsctvo_cdgo_estdo_slctd15, @cnsctvo_cdgo_estdo_slctd13)	 -- Autorizada
	
	-- Actualizamos afiliados con el rango salarial 
	Update		a
	Set			a.cnsctvo_cdgo_rngo_slrl = cv.cnsctvo_cdgo_rngo_slrl
	From		#TempSolicitudes a
	Inner Join	bdAfiliacionValidador.dbo.tbContratosValidador cv with(nolock)
	On			a.cnsctvo_cdgo_tpo_cntrto = cv.cnsctvo_cdgo_tpo_cntrto
	And			a.nmro_cntrto = cv.nmro_cntrto
	Where		a.cnsctvo_cdgo_rngo_slrl = @ValorCERO
	And         @fechaactual between cv.inco_vgnca_cntrto And cv.fn_vgnca_cntrto

	-- Actualizamos Tipo de Afiliado (Cotizante-Beneficiario)
	update		a 
	Set			cnsctvo_cdgo_tpo_afldo = b.cnsctvo_cdgo_tpo_afldo
	from		#TempSolicitudes a
	inner join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador b with(nolock)
	On			a.cnsctvo_cdgo_tpo_cntrto = b.cnsctvo_cdgo_tpo_cntrto
	And			a.nmro_cntrto = b.nmro_cntrto
	And			a.cnsctvo_bnfcro = b.cnsctvo_bnfcro
	
	-- Actualizamos el Número Unico del Aportante o Empleador
	Update		a
	Set			nmro_unco_aprtnte = b.nmro_unco_idntfccn_aprtnte,
				cnsctvo_cdgo_cnvno_cmrcl = b.cnsctvo_prdcto_scrsl
	From		#TempSolicitudes a
	Inner join  BDAfiliacionValidador.dbo.tbCobranzasValidador b with(nolock)
	On			b.cnsctvo_cdgo_tpo_cntrto = a.cnsctvo_cdgo_tpo_cntrto
	And			b.nmro_cntrto = a.nmro_cntrto
	Where		@fechaactual between b.inco_vgnca_cbrnza And b.fn_vgnca_cbrnza
	And			b.prncpl = @prncplSI

	-- Paso 2. Poblar Temporal de Servicios para las Solicitudes Insertadas en el paso 1
	Insert		#TempServicioSolicitudes
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo	  
	)
	Select distinct
				a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo
	from		#TempSolicitudes a
	inner join	gsa.tbASServiciosSolicitados b With(NoLock)
	on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	And			a.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
	Where		b.cnsctvo_cdgo_estdo_srvco_slctdo In (@cnsctvo_cdgo_estdo_slctd11, @cnsctvo_cdgo_estdo_slctd15, @cnsctvo_cdgo_estdo_slctd13) -- Autorizada
			
	-- Paso 4. Recuperamos el Número Unico OPS de los procedimientos e insumos
	Update		ss
	Set			nmro_unco_ops = css.nmro_unco_ops,
				nmro_unco_prncpl_cta_rcprcn = css.nmro_unco_prncpl_cta_rcprcn
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis with(nolock)
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css with(nolock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo	
	--And			css.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo

	-- Paso 5. Recuperamos el Número Unico OPS de los Medicamentos
	Update		ss
	Set			nmro_unco_ops = css.nmro_unco_ops,
				nmro_unco_prncpl_cta_rcprcn = css.nmro_unco_prncpl_cta_rcprcn
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbASMedicamentosSolicitados ms with(nolock)
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css with(nolock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	And			css.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo

	-- Paso 6. Recuperamos el consecutivo detalle cuota de recuperacion, valor liquidación cuota recuperación servicio y concepto de pago 
	Update		ss
	Set			ss.cnsctvo_rgstro_dt_cta_rcprcn = dccr.cnsctvo_rgstro_dt_cta_rcprcn,
				ss.vlr_lqdcn_cta_rcprcn_srvco = dccr.vlr_lqdcn_cta_rcprcn_srvco,
				ss.cnsctvo_cdgo_cncpto_pgo = dccr.cnsctvo_cdgo_cncpto_pgo,
				ss.gnra_cbro = dccr.gnra_cbro
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbAsDetCalculoCuotaRecuperacion dccr with(nolock)
	On			dccr.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			dccr.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	And			dccr.nmro_unco_ops = ss.nmro_unco_ops
	Where		dccr.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn1  

	-- Paso 7. Recuperamos el consecutivo id de la tabla consolidada cuotas de recuperacion x ops
	Update		ss
	Set			cnsctvo_rgstro_cnslddo_cta_rcprcn_ops = ccro.cnsctvo_rgstro_cnslddo_cta_rcprcn_ops
	From		#TempServicioSolicitudes ss
	Inner Join  gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro with(nolock)
	On			ccro.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ccro.cnsctvo_cdgo_cncpto_pgo = ss.cnsctvo_cdgo_cncpto_pgo
	Where		ccro.cnsctvo_cdgo_estdo_cta_rcprcn =  @cnsctvo_cdgo_estdo_cta_rcprcn1	   

	-- Paso 8. Recuperamos el consecutivo registro consolidado cuotas de recuperacion
	Update		ss
	Set			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = adccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbAsCalculoConsolidadoCuotaRecuperacion adccr with(nolock)
	On			adccr.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			adccr.cnsctvo_cdgo_cncpto_pgo = ss.cnsctvo_cdgo_cncpto_pgo
	
End
