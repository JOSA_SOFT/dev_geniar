USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaMarcaAccesoDirecto]    Script Date: 11/01/2017 2:59:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASConsultaMarcaAccesoDirecto') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASConsultaMarcaAccesoDirecto AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaMarcaAccesoDirecto
* Desarrollado por		: <\A Ing. Julian Hernandez - SETI SAS  					 A\>
* Descripcion			: <\D  Consulta las prestaciones marcadas con acceso directo D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 11/01/2017 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SASAM\>
* Descripcion			 : <\DM	Se modifica SP para que reciba el consecutivo de la 
								Solicitud y realice las validaciones de marcas de AD 
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	01/02/2017                                       FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SASAM\>
* Descripcion			 : <\DM	Se modifica SP para que filtre las prestaciones y no devuelva las que se encuentran en estado
								devuelta 
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	07/03/2017                                       FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SASAM\>
* Descripcion			 : <\DM	Se modifica SP para que valide Prestaciones de acceso directo por c�difo de prestaci�n y no por numero de 
								Solicitud. 
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	28/03/2017                                       FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------*/
--Exec [BDCna].[gsa].[spASConsultaMarcaAccesoDirecto] 1910
ALTER PROCEDURE [gsa].[spASConsultaMarcaAccesoDirecto] 
@cnsctvo_srvco_slctdo		udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @cnsctvo_cdgo_tpo_mrca				udtConsecutivo,
			@vlr_lqdcn							decimal(18, 0),
			@cnsctvo_cdgo_estdo_srvco_slctdo	udtConsecutivo;
	
	Create 
	table #tmpPrestacionesAccesoDirecto(cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
										cnsctvo_srvco_slctdo			udtConsecutivo,
										cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
										cnsctvo_mdcmnto_slctdo			udtConsecutivo,
										vlr_lqdcn						decimal(18, 0)
									   )

	SET	@cnsctvo_cdgo_tpo_mrca  = 7;
	SET @vlr_lqdcn				= 0;
	SET @cnsctvo_cdgo_estdo_srvco_slctdo = 6


	--Se obtienen las prestaciones que estan marcadas como acceso directo dentro de la solicitud
	Insert 
	into       #tmpPrestacionesAccesoDirecto(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo)
	Select     ss.cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo
	From       BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
	Inner Join BDCna.gsa.tbASMarcasServiciosSolicitados mss WITH (NOLOCK)
	On         ss.cnsctvo_srvco_slctdo	 = mss.cnsctvo_srvco_slctdo
	Where      ss.cnsctvo_srvco_slctdo   = @cnsctvo_srvco_slctdo
	AND        mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca
	AND		   ss.cnsctvo_cdgo_estdo_srvco_slctdo <> @cnsctvo_cdgo_estdo_srvco_slctdo;

	--Se determina si la prestaci�n es un medicamento.
	Update p
	Set	cnsctvo_mdcmnto_slctdo = m.cnsctvo_mdcmnto_slctdo
	From #tmpPrestacionesAccesoDirecto p 
	Inner Join BDCna.gsa.tbASMedicamentosSolicitados m WITH (NOLOCK)
	On m.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo

	--Se determina si la prestaci�n es un Procedimiento
	Update p
	Set	cnsctvo_prcdmnto_insmo_slctdo = m.cnsctvo_prcdmnto_insmo_slctdo
	From #tmpPrestacionesAccesoDirecto p 
	Inner Join BDCna.gsa.tbASProcedimientosInsumosSolicitados m WITH (NOLOCK)
	On m.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo

	--Se obtiene el valor de la liquidaci�n para cada una de las prestaciones marcadas.
	Update p
	Set	vlr_lqdcn = c.vlr_lqdcn
	From #tmpPrestacionesAccesoDirecto p
	Inner Join BDCna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
	On p.cnsctvo_mdcmnto_slctdo = c.cnsctvo_mdcmnto_slctdo
	and c.cnsctvo_mdcmnto_slctdo is not null

	Update p
	Set	vlr_lqdcn = c.vlr_lqdcn
	From #tmpPrestacionesAccesoDirecto p
	Inner Join BDCna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
	On p.cnsctvo_prcdmnto_insmo_slctdo = c.cnsctvo_prcdmnto_insmo_slctdo
	and c.cnsctvo_prcdmnto_insmo_slctdo is not null

	Select @vlr_lqdcn = IsNull(Sum(vlr_lqdcn), 0) From #tmpPrestacionesAccesoDirecto 
		

	Select @vlr_lqdcn vlr_lqdcn

	Drop Table #tmpPrestacionesAccesoDirecto
END
