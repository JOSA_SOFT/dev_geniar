USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASTramaXMLNotificacionNegacionPlantilla4]    Script Date: 24/10/2016 10:09:21 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASTramaXMLNotificacionNegacionPlantilla4') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASTramaXMLNotificacionNegacionPlantilla4 AS SELECT 1;'
END
GO

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASTramaXMLNotificacionNegacionPlantilla4
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Construcción trama xml para el envío de notificación de afiliados. Plantilla 4   D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2016-12-29  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASTramaXMLNotificacionNegacionPlantilla4] 
	@usrio VARCHAR(50),
	@cnstrccn_trma_xml XML OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @anno					VARCHAR(4),
			@mes					VARCHAR(2),
			@dia					VARCHAR(2),
			@hra					VARCHAR(2),
			@mnto					VARCHAR(2),
			@sgndo					VARCHAR(2);
			

	

	SELECT	@anno	= CONVERT(CHAR(4),DATEPART(YEAR,GETDATE())),
			@mes	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(MONTH,GETDATE())))),2),
			@dia	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(DAY,GETDATE())))),2),
			@hra	= CONVERT(CHAR(2),DATEPART(HOUR,GETDATE())),
			@mnto	= CONVERT(CHAR(2),DATEPART(MINUTE,GETDATE())),
			@sgndo	= CONVERT(CHAR(2),DATEPART(SECOND,GETDATE()))

	Update  AFI
	SET		nmro_slctd_atrzcn_ss = SS.nmro_slctd_atrzcn_ss
	FROM #tmpAflds AFI
	Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios SS WITH (NOLOCK) On 
	AFI.cnsctvo_slctd_srvco_sld_rcbda = SS.cnsctvo_slctd_atrzcn_srvco
	
	
	SET @cnstrccn_trma_xml = (
		SELECT		TSO.id_sld_rcbda	id,
					TPR.crreo			correo,
					TAF.eml				co,
					TAF.bcc				bcc,
					TSO.plntlla			'plantilla',
					(
						SELECT	
								'nmbre_cdd_ips_prmria_afldo'		'parametro/nombre',
								AFI.dscrpcn_sde						'parametro/valor',
								NULL,
								'dia'								'parametro/nombre',
								@dia								'parametro/valor',
								NULL,
								'mes'								'parametro/nombre',
								@mes								'parametro/valor',
								NULL,
								'ano'								'parametro/nombre',
								@anno								'parametro/valor',
								NULL,
								'nmro_slctd_atrzcn_ss'				'parametro/nombre',
								AFI.nmro_slctd_atrzcn_ss			'parametro/valor',
								NULL,
								'usrio'								'parametro/nombre',
								RTRIM(LTRIM(@usrio))				'parametro/valor'
						FROM #tmpAflds AFI
						WHERE AFI.id = TAF.id
						FOR XML PATH('parametros'), TYPE
					),
					@dia		'fecha/dia',
					@hra		'fecha/hora',
					@mnto		'fecha/minutos',
					@mes		'fecha/mes',
					@sgndo		'fecha/segundos',
					@anno		'fecha/ano',
					RTRIM(LTRIM(@usrio)) 'usuario'					
		FROM #tmpAflds TAF
		INNER JOIN #tmpSlctdes TSO
					ON TAF.id_sld_rcbda=TSO.id_sld_rcbda
		INNER JOIN #tmpPrvdor TPR
					ON TSO.id_sld_rcbda = TPR.id_sld_rcbda
		FOR XML PATH('mensajeCorreo')	
		)

END
GO