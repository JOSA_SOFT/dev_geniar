USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarAnulacionCuotasRecuperacion]    Script Date: 05/04/2017 08:59:20 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarAnulacionCuotasRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D Controlador Para ejecución por petición (Individual o Masiva de Anulación Cuotas de Recuperación (Copagos-Cuota Moderadora)	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P tmpSolicitudAnulacionCuotasRecuperacion = tabla temporal con las solicitudes,servicios y ops x anular 	P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/05/25																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega validacio para el llenado de la tabla #tmpSolicitudAnulacionCuotasRecuperacion
							   de forma que siempre llene esta sin importar si entra numero de ops o consecutivo de solicitud DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 21/12/2016 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega Join para validar que existan registros consolidados DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 23/12/2016 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega With(NoLock) a las consultas DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 27/03/2017 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

/* 
Declare @mnsjertrno					 UdtDescripcion ,
        @cdgortrno					 varchar(3) 

Create table #tmpSolicitudAnulacionCuotasRecuperacion
(
	cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- número de la solicitud
	cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestación o servicio
	nmro_unco_ops					UdtConsecutivo  
)


Insert #tmpSolicitudAnulacionCuotasRecuperacion
(
	cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
)
Select		sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo, css.nmro_unco_ops 
From		bdCNA.gsa.tbASSolicitudesAutorizacionServicios sas
Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss
On			ss.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco
Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis
On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css
On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
Where		sas.nmro_slctd_atrzcn_ss = '2017-01-00000584'
Group By	sas.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo, css.nmro_unco_ops

exec bdcna.gsa.spASEjecutarAnulacionCuotasRecuperacion 'geniarjcv', @mnsjertrno output ,@cdgortrno output

Select @cdgortrno + ' ' + @mnsjertrno

Drop table #tmpSolicitudAnulacionCuotasRecuperacion

*/

ALTER PROCEDURE [gsa].[spASEjecutarAnulacionCuotasRecuperacion]

/*01*/  @lcUsrioAplcn				 UdtUsuario = null,     -- Usuario Registrado en el módulo
/*02*/  @mnsjertrno					 UdtDescripcion = null output,
/*03*/  @cdgortrno					 varchar(2) = null output,
/*04*/  @cnsctvo_slctd_atrzcn_srvco	 UdtConsecutivo = null,		
/*05*/  @cnsctvo_cdgo_srvco_slctdo	UdtConsecutivo = null,			
/*06*/  @nmro_unco_ops				 UdtConsecutivo = null
AS

Begin
	SET NOCOUNT ON

	

	Declare @cnsctvo_cdgo_estdo_slctd11		UdtConsecutivo = 11,  -- 'Estado Solicitud '11-Autorizada'
			@mnsje_extso					UdtDescripcion = 'Proceso Anulación Cuotas Recuperación Exitoso',
			@mnsje_err						UdtDescripcion = 'Se Presento Un Error al Anular las Cuotas de Recuperación',
			@cdgo_exto						varchar(2) = 'OK',
			@cdgo_err						varchar(2) = 'ET'

	if (@cnsctvo_slctd_atrzcn_srvco is not null Or @nmro_unco_ops Is Not Null)
	Begin
		Create table #tmpSolicitudAnulacionCuotasRecuperacion
		(
			cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- número de la solicitud
			cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestación o servicio
			nmro_unco_ops					UdtConsecutivo  
		)
	End

	if (@cnsctvo_slctd_atrzcn_srvco is not null And @nmro_unco_ops Is Null)
	   Begin
			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
			)
			Select Distinct
				ss.cnsctvo_slctd_atrzcn_srvco,  ss.cnsctvo_cdgo_srvco_slctdo,		css.nmro_unco_ops
			From		gsa.tbASServiciosSolicitados ss With(NoLock)
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
			Inner Join	gsa.tbAsCalculoConsolidadoCuotaRecuperacion ccr With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = ccr.cnsctvo_slctd_atrzcn_srvco
			Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			

			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
			)
			Select Distinct
				ss.cnsctvo_slctd_atrzcn_srvco,  ss.cnsctvo_cdgo_srvco_slctdo,		css.nmro_unco_ops
			From		gsa.tbASServiciosSolicitados ss With(NoLock)
			Inner Join	gsa.tbASMedicamentosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_mdcmnto_slctdo = pis.cnsctvo_mdcmnto_slctdo
			Inner Join	gsa.tbAsCalculoConsolidadoCuotaRecuperacion ccr With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = ccr.cnsctvo_slctd_atrzcn_srvco
			Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		End
		Else
		Begin
			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
			)
			Select Distinct
				ss.cnsctvo_slctd_atrzcn_srvco,  ss.cnsctvo_cdgo_srvco_slctdo,		css.nmro_unco_ops
			From		gsa.tbASServiciosSolicitados ss With(NoLock)
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
			Inner Join	gsa.tbAsCalculoConsolidadoCuotaRecuperacion ccr With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = ccr.cnsctvo_slctd_atrzcn_srvco
			Where		css.nmro_unco_ops = @nmro_unco_ops
			

			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
			)
			Select Distinct
				ss.cnsctvo_slctd_atrzcn_srvco,  ss.cnsctvo_cdgo_srvco_slctdo,		css.nmro_unco_ops
			From		gsa.tbASServiciosSolicitados ss With(NoLock)
			Inner Join	gsa.tbASMedicamentosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_mdcmnto_slctdo = pis.cnsctvo_mdcmnto_slctdo
			Inner Join	gsa.tbAsCalculoConsolidadoCuotaRecuperacion ccr With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = ccr.cnsctvo_slctd_atrzcn_srvco
			Where		css.nmro_unco_ops = @nmro_unco_ops
		End

	-- Validamos si existen servicios por Anular cuotas de recuperación 
	if not exists ( select	1 
					from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
					inner join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
					on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
					inner join	gsa.tbASServiciosSolicitados c with(nolock)
					on			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
					inner join  #tmpSolicitudAnulacionCuotasRecuperacion t
					on			t.cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco
					And			t.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdgo_srvco_slctdo)
					--where		c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd11)-- Autorizada
		Begin
			Set @mnsjertrno = 'No hay Datos Para Anular Cuotas de Recuperación Se Cancela El Proceso'
			Set @cdgortrno = 'OK'
			Return
		End

	-- Creamos tabla temporal para insertar los registros de solicitudes a procesar con el cálculo cuotas de recuperación
	Create Table #TempSolicitudes
	(
		id_tbla							int identity,
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, 
		nmro_unco_idntfccn_afldo		UdtConsecutivo,
		cnsctvo_srvco_slctdo			UdtConsecutivo,
		cnsctvo_cdgo_cnvno_cmrcl		UdtConsecutivo default 1,
		cnsctvo_cdgo_tpo_cntrto			UdtConsecutivo,
		nmro_cntrto						UdtConsecutivo,
		cnsctvo_bnfcro					UdtConsecutivo,
		cnsctvo_cdgo_tpo_afldo			UdtConsecutivo default 0,
		nmro_unco_aprtnte				UdtConsecutivo,
		cnsctvo_cdgo_rngo_slrl			UdtConsecutivo default 0
		
	)

	-- Creamos Tabla temporal para insertar los registros de prestaciones con el cálculo cuotas de recuperación
	Create Table #TempServicioSolicitudes
	(
		id_tbla									int identity,
		cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
		cnsctvo_srvco_slctdo					UdtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo				UdtConsecutivo,
		cnsctvo_cdgo_cncpto_pgo					UdtConsecutivo,
		cnsctvo_rgstro_dt_cta_rcprcn			UdtConsecutivo,
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn UdtConsecutivo,
		cnsctvo_rgstro_cnslddo_cta_rcprcn_ops	UdtConsecutivo,
		nmro_unco_ops							UdtConsecutivo,
		nmro_unco_prncpl_cta_rcprcn				UdtLogico,
		vlr_lqdcn_cta_rcprcn_srvco				float default 0,
		vlr_tpe_evnto_prmtro					float default 0,
		vlr_tpe_año_prmtro						float default 0,
		cnsctvo_dtlle_mdlo_tps_cpgo				UdtConsecutivo,
		cnsctvo_mdlo							UdtConsecutivo,
		gnra_cbro								UdtLogico 
	)

	-- Creamos temporal para validar el manejo de topes de copagos x afiliado-solicitud
	Create table #tempTbAsDetalleTopesCopago
	(
		id_tbla									int identity,
		cnsctvo_dtlle_mdlo_tps_cpgo				UdtConsecutivo default 0,
		cnsctvo_mdlo							UdtConsecutivo default 0,
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn UdtConsecutivo default 0,
		cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
		nmro_unco_idntfccn_afldo				UdtConsecutivo,
		vlr_tpe_año_prmtro						float default 0,
		vlr_tpe_evnto_prmtro					float default 0,
		vlr_lqdcn_cta_rcprcn_ttl				float default 0,
		sldo_antrr_dspnble_evnto				float default 0,
		sldo_antrr_dspnble_ano					float default 0,
		cnsctvo_cdgo_estdo_cta_rcprcn			UdtConsecutivo,
		vlr_lqdcn_cta_rcprcn_fnl				float default 0,
		sldo_dspnble_evnto						float default 0,
		sldo_dspnble_ano						float default 0,
		gnra_cbro								UdtLogico default 'N',
		cnsctvo_rgstro_dtlle_tpe_cpgo_orgn		UdtConsecutivo,
		cnsctvo_cdgo_tpo_trnsccn				udtConsecutivo
	)
			
	-- Paso 1. Poblar temporales 
	exec gsa.spASPoblarTemporalesAnulacionCuotasRecuperacion  
	-- Paso 2. Cálculo Cuotas de Recuperación
	exec gsa.spASAnulacionCuotasRecuperacion
	-- Paso 3. Insertar Tablas Definitivas Cálculo Cuotas de Recuperación
	exec gsa.spASGrabarAnulacionCuotasRecuperacion @lcUsrioAplcn

	if @@error = 0
		Begin
			Set @mnsjertrno = @mnsje_extso
			Set @cdgortrno = @cdgo_exto
		End
	else
		Begin
			Set @mnsjertrno = @mnsje_err
			Set @cdgortrno = @cdgo_err
		End

	Drop table #TempSolicitudes
	Drop table #TempServicioSolicitudes
	Drop table #tempTbAsDetalleTopesCopago

End