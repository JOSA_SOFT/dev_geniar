USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarSolicitudHospitalizacionDomi]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarSolicitudHospitalizacionDomi
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida si la solicitud es Hospitalización o DOMI\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 23/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarSolicitudHospitalizacionDomi] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@dscrpcn_grpo_entrga udtDescripcion,
@agrpa_prstcn udtLogico
AS
  SET NOCOUNT ON

  DECLARE @fcha_actl datetime,
          @agrupadorDOMI int,
          @consecutivoCerrada int,
          @consecutivoAnulada int,
		   @afirmacion CHAR(1)
  SET @afirmacion = 'S'
  SET @consecutivoCerrada = 118
  SET @consecutivoAnulada = 127
  SET @fcha_actl = GETDATE()
  SET @agrupadorDOMI = 6

  BEGIN

    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga IS NULL)
    BEGIN

      --Se actualiza al grupo en caso de que el afiliado esté en hospitalización
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN bdhospitalizacionmovil.dbo.tbmnsncdatosafiliado b WITH (NOLOCK)
        ON b.nmro_unco_idntfccn = sol.nmro_idntfccn_afldo
      INNER JOIN bdhospitalizacionmovil.dbo.tbmnsnchospitalizacion a WITH (NOLOCK)
        ON a.cnsctvo_ntfccn = b.cnsctvo_ntfccn
        AND a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
      WHERE sol.cnsctvo_grpo_entrga IS NULL
      AND cnsctvo_cdgo_estdo_ntfccn NOT IN (@consecutivoCerrada, @consecutivoAnulada)    -- cerrada, ni anulada 
      AND @fcha_actl BETWEEN a.fcha_ingrso AND a.fcha_egrso -- que el paciente se encuentre hospitalizado a la fecha de solicitud

      --Se actualiza al grupo en caso de que el afiliado esté en DOMI
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN bdsisalud.dbo.tbCupsServicios cs
        ON cs.cnsctvo_prstcn = sol.cnsctvo_cdfccn
        AND cnsctvo_agrpdr_prstcn = @agrupadorDOMI --CUPS que pertenecen;

		--Se determina si se debe realizar agrupación
	  IF EXISTS(SELECT
				sol.id
			  FROM #tmpDatosSolicitudFechaEntrega sol
			  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
					AND @agrpa_prstcn = @afirmacion)
		BEGIN
			UPDATE #tmpDatosSolicitudFechaEntrega
		    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
			    cdgo_grpo_entrga = @dscrpcn_grpo_entrga
		    FROM #tmpDatosSolicitudFechaEntrega sol
				 WHERE sol.cnsctvo_slctd_srvco_sld_rcbda IN (SELECT
												sol.cnsctvo_slctd_srvco_sld_rcbda 
											  FROM #tmpDatosSolicitudFechaEntrega sol
											  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga)

		END
    END
  END


GO
