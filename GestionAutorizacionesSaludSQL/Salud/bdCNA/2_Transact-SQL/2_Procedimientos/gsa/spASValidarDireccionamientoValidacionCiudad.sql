USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoValidacionCiudad]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoValidacionCiudad
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite validar que la ciudad del prestador sea la misma D\>
					   <\D ciudad de residencia del afiliado										  D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoValidacionCiudad] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON
  --Declaración de variables para proceso
  DECLARE @afirmacion char(1) = 'S',
		  @vlor_cro int = 0;

  --Se valida dentro de la ciudad.
  IF EXISTS (SELECT
      TCO.id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_cdd = ISP.cnsctvo_cdgo_cdd_afldo
	WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
  BEGIN
    --Se actualizan los prestadores que apliquen para la regla.
    UPDATE TCO
    SET cmple = @afirmacion
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_cdd = ISP.cnsctvo_cdgo_cdd_afldo
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro;

    EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
  END
END

GO
