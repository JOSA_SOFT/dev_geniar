USE BDCna
GO
/****** Object:  StoredProcedure [GSA].[spASCambiarFechaEntrega]    Script Date: 12/10/2017 04:09:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('GSA.spASCambiarFechaEntrega') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE GSA.spASCambiarFechaEntrega AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG :     GSA.spASCambiarFechaEntrega
* Desarrollado por : <\A Jos� Olmedo Sogo Aguirre - Geniar S.A.S A\>
* Descripcion  :     <\D Actualizar las fechas de entregea de las ops en Sipres y MEGA D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P    @fechaModificar		Nueva fecha de entrega P\>
					 <\P	@nuam				 P\>
					 <\P	@cnsctvoCdgoOfcina consecutivo codigo de oficina P\> 
					 <\P	@cnsctvoPrstcn		consecutivo prestacion P\>
					 <\P @usroCrcn Usuarios de creacionP\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/10/17 FC\>
* Ejemplo: 
    <\EJ        
        EXEC GSA.spASCambiarFechaEntrega null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE GSA.spASCambiarFechaEntrega
	@fchaMdfcr Datetime,
	@nuam numeric,
	@cnsctvoCdgoOfcina udtConsecutivo,
	@cnsctvoPrstcn udtConsecutivo,
	@usroCrcn udtUsuario
AS
BEGIN
	SET NOCOUNT ON;

	Declare 
			@cnsctvo_atncn_ops udtConsecutivo,
			@fchaActl	datetime,
			@cnsctvo_srvco_slctdo udtConsecutivo,
			@obsrvcn udtDescripcion,
			@cnsctvo_slctd_mga udtConsecutivo,
			@flsActlzds Integer,
			@nrmIncrmtno  Integer,
			@cnsctvo_cdgo_grpo_entrga udtConsecutivo,
			@N	char,
			@cnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo,
			@orgn_mdfccn varchar(44),
			@fchaAntrr datetime,
			@cero integer,
			@elmnto_orgn_mdfccn varchar(10),
			@dto_elmnto_orgn_mdfccn varchar(20),
			@cnsctvo_cdgo_csa_nvdd udtConsecutivo


	Set @fchaActl = getdate();
	Set @obsrvcn = 'Cambio masivo de fecha de entrega';
	Set @nrmIncrmtno = 1;
	Set @N ='N'
	Set @cnsctvo_cncpto_prcdmnto_slctdo = 37
	Set @orgn_mdfccn = 'tbASResultadoFechaEntrega.fcha_estmda_entrga'
	Set @cero = 0
	Set @elmnto_orgn_mdfccn = 'Prestacion'
	Set @dto_elmnto_orgn_mdfccn = 'Cambio fecha entrega'
	Set @cnsctvo_cdgo_csa_nvdd = 12

	Select @cnsctvo_atncn_ops =	cnsctvo_atncn_ops
	from bdsisalud.dbo.tbatencionops with(nolock)
	where cnsctvo_cdgo_ofcna =@cnsctvoCdgoOfcina
		and nuam = @nuam

	Select @cnsctvo_slctd_mga =	cnsctvo_slctd_mga, @cnsctvo_cdgo_grpo_entrga = cnsctvo_cdgo_grpo_entrga
	from bdSisalud.dbo.tbASDatosAdicionalesMegaSipres with(nolock)
	where cnsctvo_cdgo_ofcna =@cnsctvoCdgoOfcina
		and nuam = @nuam
		and cnsctvo_prstcn = @cnsctvoPrstcn
		

	if @cnsctvo_atncn_ops is not null 
	begin
				
		Insert into bdSisalud.dbo.tbRegistrarCambioFechaEntrega (cnsctvo_atncn_ops,		fcha_estmda_entrga_fnl,		obsrvcns,		fcha_crcn,
		usro_crcn,	fcha_ultma_mdfccn,		usro_ultma_mdfccn)
		Values (@cnsctvo_atncn_ops,@fchaMdfcr,@obsrvcn,@fchaActl,@usroCrcn,@fchaActl,@usroCrcn);

		Update bdsisalud.dbo.tbprocedimientos
		Set fcha_estmda_entrga_fnl = @fchaMdfcr,
			usro_ultma_mdfccn = @usroCrcn
		WHERE	nuam =	@nuam 
			and cnsctvo_prstcn= @cnsctvoPrstcn 
			and cnsctvo_cdgo_ofcna = @cnsctvoCdgoOfcina
		  
		Update bdsisalud.dbo.tbatencionops
		Set fcha_ultma_mdfccn = @fchaActl
		Where cnsctvo_cdgo_ofcna = @cnsctvoCdgoOfcina
			and nuam = @nuam

	end 


	if @cnsctvo_slctd_mga is not null
	begin

		Select @fchaAntrr = fcha_ultma_mdfccn
		from  BDCna.gsa.tbASResultadoFechaEntrega with(nolock)
		where cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_mga

		Update BDCna.gsa.tbASResultadoFechaEntrega
		Set fcha_estmda_entrga =@fchaMdfcr ,
			fcha_ultma_mdfccn =@fchaActl ,
			usro_ultma_mdfccn =@usroCrcn
		where cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_mga
			
		if @fchaAntrr is null
		begin
			
			Select @cnsctvo_srvco_slctdo = (Max(cnsctvo_srvco_slctdo)+@nrmIncrmtno)
			from BDCna.gsa.tbASResultadoFechaEntrega with(nolock)


			Insert into BDCna.gsa.tbASResultadoFechaEntrega (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo ,cnsctvo_cdgo_grpo_entrga, 
					fcha_estmda_entrga,  prrdd_admnstrtva,	fcha_crcn,	usro_crcn, fcha_ultma_mdfccn,	usro_ultma_mdfccn)
				Values(@cnsctvo_slctd_mga,@cnsctvo_srvco_slctdo,@cnsctvo_cdgo_grpo_entrga,@fchaMdfcr,@N,@fchaActl,@usroCrcn,@fchaActl,@usroCrcn)
			
		end

		--Se inserta la traza del cambio.

		Insert into BDCna.gsa.tbASTrazaModificacion (cnsctvo_cncpto_prcdmnto_slctdo ,cnsctvo_cdgo_csa_nvdd ,fcha_mdfccn ,
														orgn_mdfccn ,vlr_antrr ,vlr_nvo ,obsrvcns ,nmro_ip ,fcha_crcn ,
														usro_crcn ,fcha_ultma_mdfccn ,usro_ultma_mdfccn ,elmnto_orgn_mdfccn ,
														dto_elmnto_orgn_mdfccn ,dscrpcn_vlr_antrr ,dscrpcn_vlr_nvo )
				Values (@cnsctvo_cncpto_prcdmnto_slctdo, @cnsctvo_cdgo_csa_nvdd,@fchaActl,@orgn_mdfccn,@fchaAntrr,@fchaMdfcr,@obsrvcn,@cero,@fchaActl,
						@usroCrcn,@fchaActl,@usroCrcn,@elmnto_orgn_mdfccn,@dto_elmnto_orgn_mdfccn,@fchaAntrr,@fchaMdfcr);

	
	end

END

