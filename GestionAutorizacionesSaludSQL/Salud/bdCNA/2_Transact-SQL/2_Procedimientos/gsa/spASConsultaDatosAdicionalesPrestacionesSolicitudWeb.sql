USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosAdicionalesPrestacionesSolicitudWeb]    Script Date: 7/21/2017 1:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaDatosAdicionalesPrestacionesSolicitudWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos A\>
* Descripcion        : <\D 
                          Procedimiento que consulta informacion adicionale de las prestaciones 
						  asociadas a una solicitud 
					   D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P 
                          @cnsctvo_slctd_atrzcn_srvco						
					   P\>
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 13/07/2017 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento donde se ajusta la informacion 
							de los usuarios que radican o auditan las solicitudes 
                         DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 21/07/2017 FM\>    
*--------------------------------------------------------------------------------------------------------*/
ALTER Procedure [gsa].[spASConsultaDatosAdicionalesPrestacionesSolicitudWeb]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
Begin
	SET NOCOUNT ON;

	Declare @ldFechaActual           Datetime      ,
	        @lcVacio                 udtLogico     ,
			@lcAux                   udtLogico     ,
			@lcRqre_otra_gstn_adtr   udtDescripcion,
			@rqre_otra_gstn_adtr     udtDescripcion,
			@rqre_otra_gstn_adtr2    udtDescripcion,
			@cnsctvo_cdgo_tpo_srvco  udtConsecutivo,
			@usro_sstma_bd_atmtco    udtUsuario    ,
			@usro_sstma_bd           udtUsuario    ,
			@usro_sstma_bd_prcso     udtUsuario    ,                  
			@usro_sstma              udtUsuario      
                        

	Set @ldFechaActual          = getDate()
	Set @lcVacio                = ''
	Set @lcAux                  = ' '
	Set @lcRqre_otra_gstn_adtr  = 'Auditor Integral'
	Set @rqre_otra_gstn_adtr    = 'Administrador Integral General SOS'
	Set @rqre_otra_gstn_adtr2   = 'Administrador Integral General'
	Set @cnsctvo_cdgo_tpo_srvco = 4 --CUPS 
	Set @usro_sstma_bd_atmtco   = 'sqlagent'
	Set @usro_sstma_bd          = 'sisomm01'
	Set @usro_sstma_bd_prcso    = 'autsalud_bpm01'
	Set @usro_sstma             = 'Proceso'

	Create
	Table  #tbTempUsuarios(id_usro         udtConsecutivo,
	                       lgn_usro_prcso  udtUsuario    
	                      )

	Create
	Table  #tmpDatosAdicionalesPrestacion(cnsctvo_slctd_atrzcn_srvco      udtConsecutivo           ,
	                                      cnsctvo_srvco_slctdo            udtConsecutivo           ,
										  cnsctvo_cdgo_srvco_slctdo       udtConsecutivo           ,
										  cnsctvo_cdgo_tpo_srvco          udtConsecutivo           ,
										  cdgo_cdfccn	                  Char(11)                 ,
										  dscrpcn_srvco_slctdo            udtDescripcion           , --dscrpcn_cdfccn	
										  dscrpcn_tpo_cdfccn              udtDescripcion           ,
										  cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo           ,
										  dscrpcn_estdo_srvco_slctdo      udtDescripcion           ,
										  rqre_otra_gstn_adtr             Varchar(50)              ,
										  usro_crcn_atrza                 udtUsuario               ,              
                                          usro_crcn_audtr                 udtUsuario               ,
										  usro_atrza                      Varchar(100)   Default '',
										  usro_audtr                      Varchar(100)   Default '',
										  jstfccn_gstn_adtr               Varchar(2000)  Default '',
										  cdgo_intrno                     udtCodigoIps   Default '',
										  nmbre_scrsl	                  udtDescripcion Default '',
										  cnsctvo_cdgo_nvl	              udtConsecutivo           ,
							              dscrpcn_nvl                     udtDescripcion Default ''
	                                     )

    Insert Into #tbTempUsuarios(lgn_usro_prcso) Values (@usro_sstma_bd_atmtco)
    Insert Into #tbTempUsuarios(lgn_usro_prcso) Values (@usro_sstma_bd)
	Insert Into #tbTempUsuarios(lgn_usro_prcso) Values (@usro_sstma_bd_prcso)

	Insert
	Into       #tmpDatosAdicionalesPrestacion(cnsctvo_slctd_atrzcn_srvco     , cnsctvo_srvco_slctdo, cnsctvo_cdgo_srvco_slctdo,
	                                          cnsctvo_cdgo_tpo_srvco         , cdgo_cdfccn         , dscrpcn_srvco_slctdo     , 
											  cnsctvo_cdgo_estdo_srvco_slctdo, rqre_otra_gstn_adtr , usro_crcn_atrza
	                                         )
    Select     b.cnsctvo_slctd_atrzcn_srvco     , a.cnsctvo_srvco_slctdo                 , a.cnsctvo_cdgo_srvco_slctdo,  
	           a.cnsctvo_cdgo_tpo_srvco         , c.cdgo_srvco_slctdo                    , a.dscrpcn_srvco_slctdo     , 
			   a.cnsctvo_cdgo_estdo_srvco_slctdo, IsNull(a.rqre_otra_gstn_adtr, @lcVacio), a.usro_crcn
	From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH(NOLOCK)
	Inner Join bdCNA.gsa.tbASServiciosSolicitados a WITH(NOLOCK)
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASServiciosSolicitadosOriginal c WITH(NOLOCK)
	On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo	
	Where      b.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	

	--Se actualiza el estado de la prestacion 
	Update     #tmpDatosAdicionalesPrestacion
	Set        dscrpcn_estdo_srvco_slctdo = b.dscrpcn_estdo_srvco_slctdo
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_srvco_slctdo
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	
	--Se actualiza el tipo de codificacion
	Update     #tmpDatosAdicionalesPrestacion
	Set        dscrpcn_tpo_cdfccn  =    b.dscrpcn_tpo_cdfccn  
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSiSalud.dbo.tbTipoCodificacion_Vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_tpo_cdfccn = a.cnsctvo_cdgo_tpo_srvco
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca


	--Se actualiza la gestion del auditor
	Update     #tmpDatosAdicionalesPrestacion
	Set        jstfccn_gstn_adtr = IsNull(b.jstfccn_gstn_adtr, @lcVacio),
	           usro_crcn_audtr   = b.usro_crcn
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdCNA.gsa.tbASResultadoGestionAuditor b WITH(NOLOCK)
	On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

	--Se actualiza la descripcion del registro del auditor
	Update #tmpDatosAdicionalesPrestacion
	Set    rqre_otra_gstn_adtr = @lcRqre_otra_gstn_adtr
	Where  rqre_otra_gstn_adtr = @rqre_otra_gstn_adtr 
	
	
	--Se actualiza la descripcion del registro del auditor
	Update #tmpDatosAdicionalesPrestacion
	Set    rqre_otra_gstn_adtr = @lcRqre_otra_gstn_adtr
	Where  rqre_otra_gstn_adtr = @rqre_otra_gstn_adtr2 
	 

	--Se actualiza el codigo interno (direccionamiento) de la solicitud
	Update     #tmpDatosAdicionalesPrestacion
	Set        cdgo_intrno = b.cdgo_intrno
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join BDCna.gsa.tbASResultadoDireccionamiento b WITH(NOLOCK)
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco And
		       b.cnsctvo_srvco_slctdo       = a.cnsctvo_srvco_slctdo  

	
	--Se actualiza la sucursal (direccionamiento) de la solicitud
	Update     #tmpDatosAdicionalesPrestacion
	Set        nmbre_scrsl = b.nmbre_scrsl
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSisalud.dbo.tbDireccionesPrestador b WITH(NOLOCK)
	On         b.cdgo_intrno = a.cdgo_intrno
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
				   

	--Se actualiza la informacion del auditor
	Update     #tmpDatosAdicionalesPrestacion
	Set        usro_audtr         = CONCAT(LTRIM(RTRIM(b.prmr_nmbre_usro))  , @lcAux ,
	                                       LTRIM(RTRIM(b.sgndo_nmbre_usro)) , @lcAux , 
					                       LTRIM(RTRIM(b.prmr_aplldo_usro)) , @lcAux , 
								           LTRIM(RTRIM(b.sgndo_aplldo_usro))
									      )
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSeguridad.dbo.tbUsuarios b WITH(NOLOCK)
	On         b.lgn_usro = a.usro_crcn_audtr

	Update     #tmpDatosAdicionalesPrestacion  
	Set        usro_audtr = @usro_sstma
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join #tbTempUsuarios b
	On         b.lgn_usro_prcso = a.usro_crcn_audtr
	Where      usro_audtr = @lcVacio
		

	--Se actualiza la informacion del autorizadora
	Update     #tmpDatosAdicionalesPrestacion
	Set        usro_atrza         = CONCAT(LTRIM(RTRIM(b.prmr_nmbre_usro))  , @lcAux ,
	                                       LTRIM(RTRIM(b.sgndo_nmbre_usro)) , @lcAux , 
					                       LTRIM(RTRIM(b.prmr_aplldo_usro)) , @lcAux , 
								           LTRIM(RTRIM(b.sgndo_aplldo_usro))
									      )
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSeguridad.dbo.tbUsuarios b WITH(NOLOCK)
	On         b.lgn_usro = a.usro_crcn_atrza


	Update     #tmpDatosAdicionalesPrestacion  
	Set        usro_atrza = @usro_sstma
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join #tbTempUsuarios b
	On         b.lgn_usro_prcso = a.usro_crcn_atrza
	Where      usro_audtr = @lcVacio
		
	
	--Se actualiza el consecutivo del nivel de complejidad
	Update     #tmpDatosAdicionalesPrestacion
	Set        cnsctvo_cdgo_nvl = c.cnsctvo_cdgo_nvl_cmpljdd
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSisalud.dbo.tbCodificaciones b WITH(NOLOCK)
    On         b.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join bdSisalud.dbo.tbCupsServicios c WITH(NOLOCK)
	On         c.cnsctvo_prstcn = b.cnsctvo_cdfccn
	Where      a.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco	

	--Se actualiza la descripcion del nivel de complejidad
	Update     #tmpDatosAdicionalesPrestacion
	Set        dscrpcn_nvl = b.dscrpcn_nvl
	From       #tmpDatosAdicionalesPrestacion a
	Inner Join bdSiSalud.dbo.TbNivelesComplejidad_Vigencias b  WITH(NOLOCK)
	On         b.cnsctvo_cdgo_nvl = a.cnsctvo_cdgo_nvl
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	
	Select     dscrpcn_tpo_cdfccn        , cdgo_cdfccn      , dscrpcn_srvco_slctdo,
	           dscrpcn_estdo_srvco_slctdo, usro_atrza       , usro_audtr          ,
			   rqre_otra_gstn_adtr       , cdgo_intrno      , nmbre_scrsl         , 
			   dscrpcn_nvl               , jstfccn_gstn_adtr			    
	From       #tmpDatosAdicionalesPrestacion


	Drop Table #tmpDatosAdicionalesPrestacion
    Drop Table #tbTempUsuarios
   
End
