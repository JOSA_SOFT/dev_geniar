USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaUbicacionPaciente]    Script Date: 07/12/2016 8:07:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------                                  
* Procedimiento     :  gsa.[spASConsultaUbicacionPaciente]        
* Desarrollado por  : <\A Ing. Victor Hugo Gil Ramos.    A\>                                  
* Descripción       : <\D Procedimiento que consulta la ubicacion del paciente teniendo en cuenta la clase de atencion  D\>                                  
* Observaciones     : <\O              O\>                                  
* Parámetros        : <\P              P\>                       
* Fecha Creación    : <\FC 2016/02/04  FC\>        
*---------------------------------------------------------------------------------------------------------------------------------------------------------                                  
* DATOS DE MODIFICACION        
*---------------------------------------------------------------------------------------------------------------------------------------------------------                        
* Modificado Por      : <\AM  AM\>                                  
* Descripción         : <\DM  DM\>                                  
* Nuevos Parámetros   : <\PM  PM\>                                  
* Nuevas Variables    : <\VM  VM\>                                  
* Fecha Modificación  : <\FM  FM\>                                  
*---------------------------------------------------------------------------------------------------------------------------------------------------------*/  

--exec gsa.[spASConsultaUbicacionPaciente] 8, null, null

ALTER procedure  [gsa].[spASConsultaUbicacionPaciente]                   
	@lnConsecutivoClaseAtencion udtConsecutivo,                          
	@ldFechaActual              Datetime  = NULL,
	@vsble_usro			        udtLogico = NULL                    

As    
Begin

    Set Nocount On    

	Declare @cnsctvo_cdgo_tpo_ubccn_pcnte udtConsecutivo

	If (@ldFechaActual Is Null)
	  Begin
	      Set @ldFechaActual = getDate()
	  End

	Select @cnsctvo_cdgo_tpo_ubccn_pcnte = cnsctvo_cdgo_tpo_ubccn_pcnte
	From   bdCNA.prm.tbASRelUbicacionPacientexClaseAtencion_Vigencias With(Nolock)
	Where  cnsctvo_cdgo_clse_atncn = @lnConsecutivoClaseAtencion
	And    @ldFechaActual Between inco_vgnca And fn_vgnca 
	

	Select     Ltrim(Rtrim(a.dscrpcn_tpo_ubccn_pcnte)) As dscrpcn_tpo_ubccn_pcnte, a.cdgo_tpo_ubccn_pcnte, 
	           a.cnsctvo_cdgo_tpo_ubccn_pcnte  
    From       bdsisalud.dbo.tbPMTipoUbicacionPaciente a With(Nolock)
	Inner Join bdsisalud.dbo.tbPMTipoUbicacionPaciente_vigencias b With(Nolock)
    On         b.cnsctvo_cdgo_tpo_ubccn_pcnte = a.cnsctvo_cdgo_tpo_ubccn_pcnte	
	Where      b.cnsctvo_cdgo_tpo_ubccn_pcnte = @cnsctvo_cdgo_tpo_ubccn_pcnte
	And        @ldFechaActual Between b.inco_vgnca And b.fn_vgnca  
End  



