USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASAnulacionCuotasRecuperacion]    Script Date: 12/06/2017 03:07:59 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASAnulacionCuotasRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D sp de anulación x petición cuotas de recuperación (individual o masivo) e igualmente reversión saldos
							de topes acumados por evento y año para los sericios que aplique copagos '2-Copagos'	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																									P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/05/27																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM Se quita la variable @cnsctvo_cdgo_tpo_afldo2 la cual no correspondia a al tipo beneficiario,
							   Se agrega la variable @mxmo_cnsctvo_rgstro_dtlle_tpe_cpgo usada para obtener el maximo id de 
							   los detalles de topes del afiliado. Se agrega la tabla @tempConsolidadoXOPS usada para
							   recuperar el cnsecutivo de la solicitud y la suma del valor de las ops a anular. Se agrega un
							   update para asignar el valor recuperado de las ops DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM @cnsctvo_cdgo_tpo_afldo3:guarda el valor para el tipo de afiliado beneficiario.
							   @mxmo_cnsctvo_rgstro_dtlle_tpe_cpgo: Guarda el consecutivo del maximo registro de la tabla de 
							   topes para recuperar los saldos a la fecha. VM\>  													
* Fecha Modificacion	: <\FM 21/12/2016 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM Se agrega update para obtener el campo cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
							   de la tabla tbAsCalculoConsolidadoCuotaRecuperacion.
							   Se agrega validacion para evitar el llenado de la tabla temporal #tempTbAsDetalleTopesCopago 
							   cuando el campo cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn es Null DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 22/03/2017 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se modifica proceso de calculo de topes, se simplifica proceso haciendo
							   el calculo en base a los valores de cuota existentes. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 02/05/2017 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASAnulacionCuotasRecuperacion]

AS

Begin
	SET NOCOUNT ON

	Declare @pnSalario						int = 0,
			@ValorSI						UdtLogico	   = 'S',
			@ValorNO						UdtLogico	   = 'N',
			@ValorCero						int = 0,
			@ValorUno						int = 1,
			@ValorDos						int = 2,
			@minimo							Int = 0,
			@maximo							Int = 0,
			@mxmo_id_tpe_afldo				int = 0,
			@cnsctvo_cdgo_estdo_cta_rcprcn1 int = 1,				-- Estado Cuotas de Recuperación '1-Precalculado'
			@fechaactual					Datetime = getdate(),   -- fecha actual
			@cnsctvo_cdgo_clse_mdlo9		int = 9,				-- Técnicas de Bonificacion productos
			@cnsctvo_cdgo_estdo_tpes4       int = 4,				-- Estados Topes '4-Cerrado'
			@cnsctvo_cdgo_tpo_tpe1			int = 1,				-- Tope por Año
			@cnsctvo_cdgo_tpo_tpe2			int = 2,				-- Tope por Evento
			@cnsctvo_cdgo_cncpto_pgo2		int = 2,				-- Concepto Pago '2-Copagos'
			@cnsctvo_cdgo_tpo_afldo3		int = 3,				-- Tipo Afiliado '3-Beneficiario'
			@mxmo_cnsctvo_rgstro_dtlle_tpe_cpgo		udtConsecutivo,
			@fechainicioanio				Datetime, -- fecha inicio año
			@inco_ano						Date;

			

	-- Se crean temporales para reversar topes
	

    -- Creamos Temporal para recuperar el máximo registro de topes por afiliado
	Create table #tempMaxDetalleTopesCopago
	(
			cnsctvo_dtlle_mdlo_tps_cpgo				udtConsecutivo,
			cnsctvo_mdlo							udtConsecutivo,
			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn udtConsecutivo,
			cnsctvo_slctd_atrzcn_srvco				udtConsecutivo,
			nmro_unco_idntfccn_afldo				udtConsecutivo,
			sldo_antrr_dspnble_evnto				Float Default 0,
			sldo_antrr_dspnble_ano					Float Default 0,
			sldo_dspnble_evnto						Float Default 0,
			sldo_dspnble_ano						Float Default 0,
			cnsctvo_cdgo_estdo_cta_rcprcn			udtConsecutivo,
			fcha_crcn								datetime,
			usro_crcn								udtUsuario,
			fcha_ultma_mdfccn						datetime,
			usro_ultma_mdfccn						udtUsuario,
			fcha_anlcn								datetime,
			cnsctvo_rgstro_dtlle_tpe_cpgo_orgn		udtConsecutivo,
			cnsctvo_cdgo_tpo_trnsccn				udtConsecutivo	
	)

	-- 
	Create Table #tempValorReversarTope
	(
			cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
			vlr_ttl_rvrsr						Float
	)

	Create Table #tempMaxTopes
	(
		cnsctvo_slctd_atrzcn_srvco				udtConsecutivo,
		cnsctvo_rgstro_dtlle_tpe_cpgo			udtConsecutivo
	)

	Set @fechainicioanio = DATEADD(yy,DATEDIFF(yy,0,@fechaactual),0)
	Set @inco_ano = Concat(Year(getDate()), '0101')	
	-- 
	Insert Into #tempValorReversarTope
	(
				cnsctvo_slctd_atrzcn_srvco,		vlr_ttl_rvrsr
	)
	Select		dccr.cnsctvo_slctd_atrzcn_srvco, Sum(dccr.vlr_tcnca_lqdcn_nto)
	From		#TempServicioSolicitudes ss
	Inner Join	bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion dccr With(NoLock)
	On			dccr.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			dccr.nmro_unco_ops = ss.nmro_unco_ops
	Where		dccr.fcha_crcn >= @inco_ano
	And			dccr.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
	Group By	dccr.cnsctvo_slctd_atrzcn_srvco


	Insert Into #tempMaxTopes
	(			
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_rgstro_dtlle_tpe_cpgo
	)
	Select		a.cnsctvo_slctd_atrzcn_srvco, Max(cnsctvo_rgstro_dtlle_tpe_cpgo)
	From		gsa.tbAsDetalleTopesCopago dtc With(NoLock)
	Inner Join	#TempSolicitudes a
	On			a.nmro_unco_idntfccn_afldo = dtc.nmro_unco_idntfccn_afldo
	Inner Join	#tempValorReversarTope vrt
	On			vrt.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Group By	a.cnsctvo_slctd_atrzcn_srvco

	-- 
	Insert Into #tempMaxDetalleTopesCopago
	(
				cnsctvo_dtlle_mdlo_tps_cpgo,				cnsctvo_mdlo,						cnsctvo_slctd_atrzcn_srvco,					
				nmro_unco_idntfccn_afldo,					sldo_antrr_dspnble_evnto,			sldo_antrr_dspnble_ano,						
				sldo_dspnble_evnto,							sldo_dspnble_ano,					cnsctvo_cdgo_estdo_cta_rcprcn,				
				fcha_crcn,									usro_crcn,							fcha_ultma_mdfccn,							
				usro_ultma_mdfccn,							fcha_anlcn,							cnsctvo_cdgo_tpo_trnsccn,					
				cnsctvo_rgstro_dtlle_tpe_cpgo_orgn
	)
	Select		dtc.cnsctvo_dtlle_mdlo_tps_cpgo,			dtc.cnsctvo_mdlo,					s.cnsctvo_slctd_atrzcn_srvco,				
				dtc.nmro_unco_idntfccn_afldo,				@ValorCero,							dtc.sldo_dspnble_ano,						
				@ValorCero,									dtc.sldo_dspnble_ano,				dtc.cnsctvo_cdgo_estdo_cta_rcprcn,			
				dtc.fcha_crcn,								dtc.usro_crcn,						dtc.fcha_ultma_mdfccn,						
				dtc.usro_ultma_mdfccn,						dtc.fcha_anlcn,						@cnsctvo_cdgo_cncpto_pgo2,											
				dtc.cnsctvo_rgstro_dtlle_tpe_cpgo
	From		bdCNA.gsa.TbAsDetalleTopesCopago dtc With(NoLock)
	Inner Join	#TempSolicitudes s
	On			s.nmro_unco_idntfccn_afldo = dtc.nmro_unco_idntfccn_afldo
	Inner Join	#tempMaxTopes mt
	On			mt.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	And			mt.cnsctvo_rgstro_dtlle_tpe_cpgo = dtc.cnsctvo_rgstro_dtlle_tpe_cpgo
	Group By	dtc.cnsctvo_dtlle_mdlo_tps_cpgo,			dtc.cnsctvo_mdlo,					
				s.cnsctvo_slctd_atrzcn_srvco,				dtc.nmro_unco_idntfccn_afldo,
				dtc.sldo_dspnble_ano,									dtc.sldo_dspnble_ano,
				dtc.cnsctvo_cdgo_estdo_cta_rcprcn,			dtc.fcha_crcn,						dtc.usro_crcn,
				dtc.fcha_ultma_mdfccn,						dtc.usro_ultma_mdfccn,				dtc.fcha_anlcn,
				dtc.cnsctvo_rgstro_dtlle_tpe_cpgo
	
	Update		mdtc	
	Set			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = ss.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	From		#tempMaxDetalleTopesCopago mdtc
	Inner Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = mdtc.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2

	-- 
	Update		mdtc
	Set			sldo_dspnble_ano = sldo_dspnble_ano + vrt.vlr_ttl_rvrsr
	From		#tempMaxDetalleTopesCopago	mdtc
	Inner Join	#tempValorReversarTope vrt
	On			vrt.cnsctvo_slctd_atrzcn_srvco = mdtc.cnsctvo_slctd_atrzcn_srvco
	
	-- 
	Insert Into #tempTbAsDetalleTopesCopago
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,		nmro_unco_idntfccn_afldo,
				cnsctvo_dtlle_mdlo_tps_cpgo,		cnsctvo_mdlo,									cnsctvo_cdgo_estdo_cta_rcprcn,
				sldo_dspnble_ano,					sldo_dspnble_evnto,								sldo_antrr_dspnble_ano,
				sldo_antrr_dspnble_evnto,			cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,				cnsctvo_cdgo_tpo_trnsccn
	)
	Select		dtc.cnsctvo_slctd_atrzcn_srvco,		dtc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,	b.nmro_unco_idntfccn_afldo,
				dtc.cnsctvo_dtlle_mdlo_tps_cpgo,	dtc.cnsctvo_mdlo,								dtc.cnsctvo_cdgo_estdo_cta_rcprcn,
				dtc.sldo_dspnble_ano,				dtc.sldo_dspnble_evnto,							dtc.sldo_antrr_dspnble_ano,
				dtc.sldo_antrr_dspnble_evnto,		dtc.cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,			dtc.cnsctvo_cdgo_tpo_trnsccn
	From		#tempMaxDetalleTopesCopago dtc
	Inner Join	#TempServicioSolicitudes a
	On			a.cnsctvo_slctd_atrzcn_srvco = dtc.cnsctvo_slctd_atrzcn_srvco
	And			a.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = dtc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	Inner Join  #TempSolicitudes b
	On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco 
	Group By	dtc.cnsctvo_slctd_atrzcn_srvco,		dtc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,	b.nmro_unco_idntfccn_afldo,
				dtc.cnsctvo_dtlle_mdlo_tps_cpgo,	dtc.cnsctvo_mdlo,								dtc.cnsctvo_cdgo_estdo_cta_rcprcn,
				dtc.sldo_dspnble_ano,				dtc.sldo_dspnble_evnto,							dtc.sldo_antrr_dspnble_ano,
				dtc.sldo_antrr_dspnble_evnto,		dtc.cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,			dtc.cnsctvo_cdgo_tpo_trnsccn

	-- 
	Update		dtc
	Set			vlr_lqdcn_cta_rcprcn_fnl = Round(vrt.vlr_ttl_rvrsr, -2),
				vlr_lqdcn_cta_rcprcn_ttl = vrt.vlr_ttl_rvrsr 
	From		#tempTbAsDetalleTopesCopago dtc
	Inner Join	#tempValorReversarTope vrt 
	On			vrt.cnsctvo_slctd_atrzcn_srvco = dtc.cnsctvo_slctd_atrzcn_srvco

End




