USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaSolicitudHistoricoTareasWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaSolicitudHistoricoTareasWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos	A\>    
* Descripcion        : <\D 
                           Consulta informacion asociada a una solicitud mas la informacion del historico de 
                           las tareas por instancia 
						D\>					           
* Observaciones   	 : <\O O\>    
* Parametros         : <\P P\>   
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 14/04/2016  FC\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D    D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*----------------------------------------------------------------------------------------------------------------------------------*/

--exec [gsa].[spASConsultaSolicitudHistoricoTareasWeb]  NULL,NULL,NULL,'2016-01-00000279',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
--exec [gsa].[spASConsultaSolicitudHistoricoTareasWeb] '2016-04-01','2016-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
--exec [gsa].[spASConsultaSolicitudHistoricoTareasWeb] NULL,NULL,NULL,NULL,NULL,108,NULL,NULL,NULL,NULL,NULL,NULL

ALTER PROCEDURE [gsa].[spASConsultaSolicitudHistoricoTareasWeb]
	@ldfdesde								datetime,
	@ldfhasta								datetime,
	@cnsctvo_cdgo_pln						udtConsecutivo,
	@nmro_slctd_atrzcn_ss					varchar(16),
	@nmro_slctd_prvdr						varchar(15),
	@nmro_unco_ops                          int,
	--@solicitud_servicios
	@cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	udtConsecutivo,
	@nmro_idntfccn_ips_slctnte				udtNumeroIdentificacionLargo,
	@cdgo_intrno							udtCodigoIPS,	
	@cnsctvo_cdgo_srvco_slctdo				udtConsecutivo,	--consecutivo servicio CUMS, CUPS, PIS	
	@cnsctvo_cdgo_tpo_idntfccn_afldo        udtConsecutivo,
	@nmro_idntfccn_afldo                    udtNumeroIdentificacionLargo  
AS
Begin
	SET NOCOUNT ON

	Declare @tpo_accn udtLogico
		
	Create 
	Table #tmpDatosSolicitud(nmro_idntfccn_afldo        Varchar(50)   ,							
	                         nmbre_afldo                Varchar(200)  ,
	                         nmro_slctd_atrzcn_ss		Varchar(16)   ,
	                         fcha_slctd					Datetime      ,
	                         dscrpcn_srvco_slctdo		udtDescripcion,
	                         dscrpcn_estdo_srvco_slctdo	udtDescripcion,	                         
							 cnsctvo_slctd_atrzcn_srvco udtConsecutivo,	                        
	                         cnsctvo_cdgo_srvco_slctdo	udtConsecutivo,
							 cnsctvo_srvco_slctdo       udtConsecutivo,	                         
							 fcha_vncmnto_gstn          Datetime      ,
							 nmbre_tra                  udtDescripcion,
							 nmbre_grpo_rsgo            udtDescripcion,
							 usro_tra                   udtUsuario    	
							)

	Create 
	Table #tmpMaxHistoricoTareas(cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
	                             cnsctvo_tra_instnca        udtConsecutivo,
								 nmbre_tra                  udtDescripcion,
							     nmbre_grpo_rsgo            udtDescripcion,
							     usro_tra                   udtUsuario    
	                            )
	 Set @tpo_accn = 'I'

	 Insert 
	 Into   #tmpDatosSolicitud(nmro_idntfccn_afldo	     , nmbre_afldo              , nmro_slctd_atrzcn_ss      ,
                               fcha_slctd                , dscrpcn_srvco_slctdo     , dscrpcn_estdo_srvco_slctdo,
							   cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_srvco_slctdo, cnsctvo_srvco_slctdo      ,
							   fcha_vncmnto_gstn
                              )
	 Exec bdCNA.gsa.spASConsultaSolicitudWeb @ldfdesde                             , @ldfhasta                       , @cnsctvo_cdgo_pln   ,
	                                         @nmro_slctd_atrzcn_ss                 , @nmro_slctd_prvdr               , @nmro_unco_ops      ,
                                             @cnsctvo_cdgo_tpo_idntfccn_ips_slctnte, @nmro_idntfccn_ips_slctnte      , @cdgo_intrno        ,
                                             @cnsctvo_cdgo_srvco_slctdo            , @cnsctvo_cdgo_tpo_idntfccn_afldo, @nmro_idntfccn_afldo
	 
	 
	 /*Se registra la informacion de ultima tarea*/
	 Insert
	 Into       #tmpMaxHistoricoTareas(cnsctvo_slctd_atrzcn_srvco, cnsctvo_tra_instnca)
	 Select     a.cnsctvo_slctd_atrzcn_srvco, Max(a.cnsctvo_tra_instnca)	 
	 From       bdCNA.gsa.tbASHistoricoTareasxInstancia a WITH(NOLOCK)
	 Inner Join #tmpDatosSolicitud b
	 On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	 And        a.tpo_accn                   = @tpo_accn
	 Group By   a.cnsctvo_slctd_atrzcn_srvco

	 
	 /*Se actualiza la informacion del historico de las tareas*/
	 Update     #tmpMaxHistoricoTareas
	 Set        nmbre_tra       = b.nmbre_tra      ,
				nmbre_grpo_rsgo = b.nmbre_grpo_rsgo,
				usro_tra        = b.usro_tra                         
     From       #tmpMaxHistoricoTareas a
	 Inner Join bdCNA.gsa.tbASHistoricoTareasxInstancia b WITH(NOLOCK)
	 On         b.cnsctvo_tra_instnca = a.cnsctvo_tra_instnca

	 /*Se actualiza la informacion de la solicitud con el historico de las tareas*/
	 Update     #tmpDatosSolicitud
	 Set        nmbre_tra       = b.nmbre_tra      ,
				nmbre_grpo_rsgo = b.nmbre_grpo_rsgo,
				usro_tra        = b.usro_tra                         
     From       #tmpDatosSolicitud a
	 Inner Join #tmpMaxHistoricoTareas b
	 On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco


	 SELECT   nmro_idntfccn_afldo       , nmbre_afldo              , nmro_slctd_atrzcn_ss      ,
	          fcha_slctd                , dscrpcn_srvco_slctdo     , dscrpcn_estdo_srvco_slctdo,
			  cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_srvco_slctdo, cnsctvo_srvco_slctdo      ,
			  fcha_vncmnto_gstn         , nmbre_tra                , nmbre_grpo_rsgo           ,
			  usro_tra
     From     #tmpDatosSolicitud
     Order by nmro_slctd_atrzcn_ss Desc 


	 Drop Table #tmpDatosSolicitud
	 Drop Table #tmpMaxHistoricoTareas
End
GO
