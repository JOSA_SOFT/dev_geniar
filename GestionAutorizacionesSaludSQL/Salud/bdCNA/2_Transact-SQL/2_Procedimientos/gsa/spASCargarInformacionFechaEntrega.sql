USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCargarInformacionFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCargarInformacionFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite cargar la información necesaria para el procedo D\>
					   <\D de fecha esperada de entrega											     D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASCargarInformacionFechaEntrega] @cnsctvo_prcso udtConsecutivo,
@usro udtUsuario,
@es_btch char(1)
AS

BEGIN
  DECLARE @fechaActual DATETIME
  SET @fechaActual = GETDATE()

  SET NOCOUNT ON
  INSERT INTO #tmpDatosSolicitudFechaEntrega (cnsctvo_slctd_srvco_sld_rcbda,
  cnsctvo_srvco_slctdo,
  cnsctvo_cdfccn,
  cdgo_cdfccn,
  cnsctvo_cdgo_tpo_srvco,
  cnsctvo_pln,
  cdgo_pln,
  cnsctvo_tpo_pln,
  cdgo_tpo_pln,
  cnsctvo_mdo_cntcto,
  cdgo_mdo_cntcto,
  fcha_slctd,
  nmro_unco_idntfccn_afldo,
  edd_afldo_ans,
  edd_afldo_mss,
  edd_afldo_ds,
  nmro_cntrto,
  cnsctvo_cdgo_tpo_cntrto,
  usro,
  nmro_slctd_ss,
  nmro_slctd_prvdr,
  cnsctvo_cdgo_tpo_idntfccn_afldo,
  nmro_idntfccn)
    SELECT
      ss.cnsctvo_slctd_atrzcn_srvco,
      ss.cnsctvo_srvco_slctdo,
      ss.cnsctvo_cdgo_srvco_slctdo,
      sso.cdgo_srvco_slctdo,
	  ss.cnsctvo_cdgo_tpo_srvco,
      iasas.cnsctvo_cdgo_pln,
      iasaso.cdgo_pln,
      iasas.cnsctvo_cdgo_tpo_pln,
      iasaso.cdgo_tpo_pln,
      sas.cnsctvo_cdgo_mdo_cntcto_slctd,
      ccv.dscrpcn_mdo_cntcto,
      sas.fcha_slctd,
      iasas.nmro_unco_idntfccn_afldo,
      iasas.edd_afldo_ans,
	  iasas.edd_afldo_mss,
	  iasas.edd_afldo_ds,
      iasas.nmro_cntrto,
      iasas.cnsctvo_cdgo_tpo_cntrto,
      @usro,
      sas.nmro_slctd_atrzcn_ss,
      sas.nmro_slctd_prvdr,
      iasas.cnsctvo_cdgo_tpo_idntfccn_afldo,
      iasas.nmro_idntfccn_afldo
    FROM BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)--Servicios 
    INNER JOIN #tmpNmroSlctds slc WITH (NOLOCK)
      ON slc.cnsctvo_slctd_srvco_sld_rcbda = ss.cnsctvo_slctd_atrzcn_srvco
    INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)-- Solicitudes
      ON ss.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco
    LEFT JOIN BDCna.gsa.tbASServiciosSolicitadosOriginal sso WITH (NOLOCK)-- Servicios Solicitados Original
      ON sso.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
    INNER JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas WITH (NOLOCK)
      ON sas.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
    LEFT JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal iasaso WITH (NOLOCK)
      ON iasaso.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
    INNER JOIN BDCna.prm.tbASMediosContacto_Vigencias ccv WITH (NOLOCK)
      ON ccv.cnsctvo_cdgo_mdo_cntcto = sas.cnsctvo_cdgo_mdo_cntcto_slctd
	WHERE
	 @fechaActual BETWEEN ccv.inco_vgnca AND fn_vgnca
END

GO
