USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarNotificacionAfiliado]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASEjecutarNotificacionAfiliado
* Desarrollado por		: <\A Jonathan Chamizo - SETI SAS  					    A\>
* Descripcion			: <\D Ejecuta el procedimiento encargado de orquestar el 
							  envío de notificación del afiliado.				D\>
						  <\D 													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													 \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/03/25									   FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASEjecutarNotificacionAfiliado] 
	@slctdes		XML,
	@usrio			VARCHAR(50),
	@estdo_ejccn	VARCHAR(2) OUTPUT,
	@msje_rspsta	NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@codigoError	CHAR(2);
	SET @codigoError = 'ET';	

	BEGIN TRY	
		EXEC gsa.spASNotificacionAfiliado	@slctdes		= @slctdes,
											@usrio			= @usrio,
											@estdo_ejccn	= @estdo_ejccn OUTPUT,
											@msje_rspsta	= @msje_rspsta OUTPUT
	END TRY
	BEGIN CATCH
		SET @msje_rspsta =	'Number:' + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
							'Line:' + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
							'Message:' + ERROR_MESSAGE() + CHAR(13) +
							'Procedure:' + ERROR_PROCEDURE();
		SET @estdo_ejccn = @codigoError;
	END CATCH	
				
END

GO
