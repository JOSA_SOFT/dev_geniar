USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionAfiliadoxSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionAfiliadoxSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM	Se modifica el SP para que devuelva el plan complementario del 
								Afiliado para que se muestre en las pantallas de AuditoriaDM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016-12-29  FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM	Se modifica consulta en el SP para que devuelva el plan complementario del 
								Afiliado para que se muestre en las pantallas de AuditoriaDM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016-01-23  FM\>
*---------------------------------------------------------------------------------*/

--exec [gsa].[spASConsultaDatosInformacionAfiliadoxSolicitud]  394072
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionAfiliadoxSolicitud] 
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @altoRiesgo			udtLogico,
			@si					udtLogico,
			@no					udtLogico,
			@ldFechaActual   	Datetime ,
			@tpo_pln_pc			Int      ,
			@estdo_rprte_chrte	Int      , 
			@lcVacio            Char(1)  ,
			@lcInd              Char(1)  ,  
			@nmro_vldcn_espcl	Int		 ,
			@vlr_cro			int			

	CREATE 
	TABLE #tb_tmpAfiliado(cnsctvo_slctd_atrzcn_srvco	  udtConsecutivo              ,			
				          cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo              ,
						  cdgo_tpo_idntfccn				  Char(3)                     ,
						  cnsctvo_cdgo_pln                udtConsecutivo              ,
						  cnsctvo_cdgo_pln_pc             udtConsecutivo  Default 0   ,
						  nmro_idntfccn_afldo             udtNumeroIdentificacionLargo,
						  nmro_unco_idntfccnr             udtConsecutivo              ,
						  prmr_nmbre                      udtNombre       Default ''  ,
						  sgndo_nmbre                     udtNombre       Default ''  ,
						  prmr_aplldo                     udtApellido     Default ''  ,
						  sgndo_aplldo                    udtApellido     Default ''  ,
						  cnsctvo_cdgo_sxo                udtConsecutivo              ,
						  cdgo_sxo                        udtCodigo                   ,
						  alt_rsgo                        udtLogico                   ,
						  cnsctvo_cdgo_tpo_cntrto         udtConsecutivo              ,
						  nmro_cntrto                     udtNumeroFormulario         ,
						  fcha_ncmnto                     Datetime                    ,
						  edd                             Int                         ,
						  edd_mss                         Int                         ,
						  edd_ds                          Int                         ,
						  smns_ctzds                      udtConsecutivo  Default 0   ,
						  cnsctvo_cdgo_sxo_rcn_ncdo       udtConsecutivo              ,
						  cdgo_sxo_rcn_ncdo               udtCodigo                   ,
						  nmro_hjo_afldo                  udtConsecutivo              ,
						  rcn_ncdo                        udtLogico                   ,
						  fcha_ncmnto_rcn_ncdo            Datetime                    ,
						  fcha_crcn						  Datetime                    ,
						  cnsctvo_cdgo_cdd_rsdnca         udtConsecutivo   Default 0  ,
						  dscrpcn_cdd                     udtDescripcion   Default '' ,
						  cnsctvo_cdgo_estdo_drcho        udtConsecutivo              ,
						  dscrpcn_drcho                   udtDescripcion   Default '' ,
						  dscrpcn_pln                     udtDescripcion              ,
						  dscrpcn_pln_pc                  udtDescripcion   Default '' ,
						  cnsctvo_cdgo_tpo_afldo          udtConsecutivo   Default 0  ,
						  dscrpcn                         udtDescripcion   Default '' ,
						  cnsctvo_cdgo_ofcna_atrzcn       udtConsecutivo              ,
						  cnsctvo_rgstro_vldcn_espcl      udtConsecutivo   Default 0  ,
						  nmro_vldcn_espcl_atrzcn         Numeric(18,0)    Default 0  ,
						  nmro_vldcn_espcl				  udtDescripcion   Default '' ,
						  estdo	                          udtLogico        Default '' ,
						  smns_ctzds_antrr_eps            udtConsecutivo   Default 0
                         )
	
	
	Set @ldFechaActual     = GetDate()
	Set @si                = 'S'
	Set @no                = 'N'
	Set @tpo_pln_pc        = 2
	Set @estdo_rprte_chrte = 1 -- Ingresado
	Set @lcVacio           = ''
	Set @nmro_vldcn_espcl  = 0
	Set @lcInd             = '-'
	Set @vlr_cro		   = 0
					
	Insert 
	Into        #tb_tmpAfiliado(cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo     ,
	                            nmro_unco_idntfccnr       , cnsctvo_cdgo_pln               , cnsctvo_cdgo_tpo_cntrto ,
	                            nmro_cntrto               , cnsctvo_cdgo_sxo               , rcn_ncdo                , 
								nmro_hjo_afldo            , cnsctvo_cdgo_sxo_rcn_ncdo      , fcha_crcn               , 
								cnsctvo_cdgo_estdo_drcho  , cnsctvo_cdgo_ofcna_atrzcn
                               )
	SELECT  	a.cnsctvo_slctd_atrzcn_srvco, b.cnsctvo_cdgo_tpo_idntfccn_afldo, b.nmro_idntfccn_afldo     ,
	            b.nmro_unco_idntfccn_afldo  , b.cnsctvo_cdgo_pln               , b.cnsctvo_cdgo_tpo_cntrto ,
				b.nmro_cntrto               , b.cnsctvo_cdgo_sxo               , b.rcn_ncdo                , 
				b.nmro_hjo_afldo            , b.cnsctvo_cdgo_sxo_rcn_ncdo      , b.fcha_crcn               , 
				b.cnsctvo_cdgo_estdo_drcho  , a.cnsctvo_cdgo_ofcna_atrzcn
	FROM        bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
	On          b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco	
	Where       a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;

	
	--Se actualiza la informacion del afiliado
	Update      a
	Set         prmr_aplldo	            = Ltrim(Rtrim(c.prmr_aplldo)) ,				             
                sgndo_aplldo		    = Ltrim(Rtrim(c.sgndo_aplldo)),                   
                prmr_nmbre		        = Ltrim(Rtrim(c.prmr_nmbre))  ,                
                sgndo_nmbre		        = Ltrim(Rtrim(c.sgndo_nmbre)) , 
				fcha_ncmnto             = c.fcha_ncmnto               ,
				smns_ctzds              = c.smns_ctzds                ,
				cnsctvo_cdgo_cdd_rsdnca = c.cnsctvo_cdgo_cdd_rsdnca   ,
				cnsctvo_cdgo_tpo_afldo  = c.cnsctvo_cdgo_tpo_afldo    ,
				estdo                   = c.estdo                     ,
				smns_ctzds_antrr_eps    = c.smns_ctzds_antrr_eps
	From        #tb_tmpAfiliado a	
	Inner Join  bdAfiliacionValidador.dbo.tbBeneficiariosValidador c WITH (NOLOCK)
	On          c.cnsctvo_cdgo_tpo_cntrto  = a.cnsctvo_cdgo_tpo_cntrto  And
	            c.nmro_cntrto              = a.nmro_cntrto              And
				c.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccnr

	--actualiza el codigo del tipo de identificacion del afiliado
	Update     a
	Set        cdgo_tpo_idntfccn = t.cdgo_tpo_idntfccn	           
	From       #tb_tmpAfiliado a
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
	On         t.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn_afldo
	Where      @ldFechaActual Between t.inco_vgnca And t.fn_vgnca
		
	
	--Calculamos  la edad y la unidad de la edad
	Update	a 
	Set 	edd		= isnull(BDAfiliacionValidador.dbo.fnCalcularTiempo(fcha_ncmnto,@ldFechaActual,1,2),0),
		    edd_mss = isnull(BDAfiliacionValidador.dbo.fnCalcularTiempo(fcha_ncmnto,@ldFechaActual,2,2),0),
			edd_ds 	= isnull(BDAfiliacionValidador.dbo.fnCalcularTiempo(fcha_ncmnto,@ldFechaActual,3,2),0)
	From #tb_tmpAfiliado  a

	--Se actualiza la ciudad del afiliado	
	Update	   t
	Set		   dscrpcn_cdd	= c.dscrpcn_cdd
	From	   #tb_tmpAfiliado t 	
	Inner Join BDAfiliacionValidador.dbo.tbCiudades_Vigencias c With(NoLock)
	On	       c.cnsctvo_cdgo_cdd = t.cnsctvo_cdgo_cdd_rsdnca
	Where      @ldFechaActual Between c.inco_vgnca And c.fn_vgnca


	--Se actualiza el estado de derechos del afiliado
	Update	   t
	Set		   dscrpcn_drcho = e.dscrpcn_drcho
	From	   #tb_tmpAfiliado t 
	Inner Join bdAfiliacionValidador.dbo.tbEstadosDerechoValidador e With(NoLock)	 
	On		   e.cnsctvo_cdgo_estdo_drcho =	t.cnsctvo_cdgo_estdo_drcho
	Where      @ldFechaActual Between e.inco_vgnca And e.fn_vgnca


	--Se actualiza la descripcion del plan
	Update	   a
	Set        dscrpcn_pln = p.dscrpcn_pln
	From       #tb_tmpAfiliado a
	Inner Join bdAfiliacionValidador.dbo.tbPlanes_vigencias	p With(NoLock)
	On	       p.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Where      @ldFechaActual Between p.inco_vgnca And p.fn_vgnca
     
	--Se actualiza la descripcion del tipo de afiliado
	Update     a
	Set        dscrpcn = b.dscrpcn
	From       #tb_tmpAfiliado a
	Inner Join bdAfiliacionValidador.dbo.tbTiposAfiliado_vigencias b
	On         b.cnsctvo_cdgo_tpo_afldo = a.cnsctvo_cdgo_tpo_afldo
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	--Se actualiza el codigo del sexo del afiliado
	Update     a
	Set        cdgo_sxo = sx.cdgo_sxo
	From       #tb_tmpAfiliado a
	Inner Join bdAfiliacionValidador.dbo.tbSexos_Vigencias sx With(NoLock)
	On         sx.cnsctvo_cdgo_sxo = a.cnsctvo_cdgo_sxo
	Where      @ldFechaActual Between sx.inco_vgnca And sx.fn_vgnca


	--Se actualiza el codigo del sexo del recien nacido
	Update     a
	Set        cdgo_sxo_rcn_ncdo = IsNull(sx.cdgo_sxo, @lcVacio)
	From       #tb_tmpAfiliado a
	Inner Join bdAfiliacionValidador.dbo.tbSexos_Vigencias sx With(NoLock)
	On         sx.cnsctvo_cdgo_sxo = a.cnsctvo_cdgo_sxo_rcn_ncdo
	Where      @ldFechaActual Between sx.inco_vgnca And sx.fn_vgnca
	
	--Se actualiza el riesgo del afiliado segun la cohorte
	Select     @altoRiesgo = Case When Count(1) = 0 Then @NO 
	                              Else @SI
	                         End
	From       #tb_tmpAfiliado a
	Inner Join bdRiesgosSalud.dbo.tbReporteCohortesxAfiliado b With(NoLock) --Cohorte por afiliado
	On         b.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccnr 
	Inner Join bdRiesgosSalud.dbo.tbHistoricoEstadosReporteCohortesxAfiliado c With(NoLock)
	On         c.cnsctvo_rprte_chrte_x_afldo = b.cnsctvo_rprte_chrte_x_afldo
	Where      c.cnsctvo_cdgo_estdo_rprte_chrte = @estdo_rprte_chrte
	And        @ldFechaActual Between c.inco_vgnca_estdo And c.fn_vgnca_estdo

	if @altoRiesgo = @NO
	Begin
		Select     @altoRiesgo = Case When Count(1) = 0 Then @NO 
									Else @SI
								End
		From       #tb_tmpAfiliado a
		Inner Join bdRiesgosSalud.dbo.tbReporteCohortesxAfiliado b With(NoLock) --Cohorte por afiliado
		On         b.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccnr 
	End
	
	
	--Se actualiza el riesgo del afiliado segun la cohorte
	Update     a
	Set        alt_rsgo = @altoRiesgo
	From #tb_tmpAfiliado a

	--Actualiza el numero de validación especial
	Update		t
	Set			cnsctvo_rgstro_vldcn_espcl = a.cnsctvo_rgstro_vldcn_espcl ,
				nmro_vldcn_espcl_atrzcn    = a.nmro_vldcn_espcl             
	From		#tb_tmpAfiliado t 
	Inner Join 	bdCNA.gsa.tbASRegistroValidacionEspecial a With(NoLock)
	On          a.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
	Where       a.nmro_vldcn_espcl <> @nmro_vldcn_espcl
	
	--Actualiza el numero de validación especial
	Update		a
	Set         nmro_vldcn_espcl = Concat(a.nmro_vldcn_espcl_atrzcn, @lcInd, IsNull(ofi.cdgo_ofcna, @lcVacio))
	From        #tb_tmpAfiliado a
	Inner Join  BDAfiliacionValidador.dbo.tbOficinas_Vigencias ofi With(NoLock) 
	On          ofi.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna_atrzcn
	Where       @ldFechaActual Between ofi.inco_vgnca And ofi.fn_vgnca
	And         a.nmro_vldcn_espcl_atrzcn <> @nmro_vldcn_espcl

	--Actualiza la fecha de nacimiento del recien nacido
	Update      a
	Set         fcha_ncmnto_rcn_ncdo = b.fcha_ncmnto_rcn_ncdo
	From        #tb_tmpAfiliado a
	Inner Join  bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal b With(NoLock) 
	On          a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	
	--Actualiza la informacion del afiliado si tienen plan adicional
	Update a
	 Set cnsctvo_cdgo_pln_pc = IsNull(p.cnsctvo_cdgo_pln, @vlr_cro)
	From		bdAfiliacionValidador.dbo.tbBeneficiariosValidador			b	With(NoLock)
	Inner Join  #tb_tmpAfiliado a On b.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccnr
	INNER JOIN	bdAfiliacionValidador.dbo.tbContratosValidador				c	With(NoLock)	On	b.nmro_cntrto					= c.nmro_cntrto
																			And	b.cnsctvo_cdgo_tpo_cntrto	= c.cnsctvo_cdgo_tpo_cntrto
	Inner Join	bdAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador	v	With(NoLock)	On	v.cnsctvo_cdgo_tpo_cntrto		=	b.cnsctvo_cdgo_tpo_cntrto
																			And	v.nmro_cntrto				=	b.nmro_cntrto
																			And	v.cnsctvo_bnfcro			=	b.cnsctvo_bnfcro
	Inner Join  bdAfiliacionValidador.dbo.tbPlanes_Vigencias p With(NoLock)	On          p.cnsctvo_cdgo_pln          =  c.cnsctvo_cdgo_pln
	Inner Join  bdAfiliacionValidador.dbo.tbTiposPlan tp 	With(NoLock) On		   tp.cnsctvo_cdgo_tpo_pln = p.cnsctvo_cdgo_tpo_pln
	
	Where		@ldFechaActual Between v.inco_vgnca_estdo_bnfcro And v.fn_vgnca_estdo_bnfcro
	And         @ldFechaActual Between p.inco_vgnca And p.fn_vgnca
	And			c.cnsctvo_cdgo_pln = @tpo_pln_pc

	--Actualiza la informacion del afiliado si tienen plan adicional
	Update      t
	Set         dscrpcn_pln_pc = IsNull(p.dscrpcn_pln, @lcVacio) 
	From		#tb_tmpAfiliado	t
	Inner Join  bdAfiliacionValidador.dbo.tbPlanes_vigencias p With(NoLock)
	On          p.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln_pc
	Where       @ldFechaActual Between p.inco_vgnca And p.fn_vgnca
	

	Select   cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_idntfccn_afldo, cdgo_tpo_idntfccn         ,				
		     nmro_idntfccn_afldo       , prmr_nmbre                     , sgndo_nmbre               ,
		     prmr_aplldo               , sgndo_aplldo                   , cnsctvo_cdgo_sxo          ,
		     cdgo_sxo                  , alt_rsgo                       , cnsctvo_cdgo_tpo_afldo    ,			
		     dscrpcn                   , estdo                          , smns_ctzds_antrr_eps      ,
		     smns_ctzds                , fcha_ncmnto                    , cnsctvo_cdgo_pln          ,
		     dscrpcn_pln               , cnsctvo_cdgo_cdd_rsdnca        , dscrpcn_cdd               ,
		     cnsctvo_cdgo_estdo_drcho  , dscrpcn_drcho                  , cnsctvo_cdgo_pln_pc       ,	
		     dscrpcn_pln_pc            , rcn_ncdo                       , nmro_hjo_afldo            ,
		     fcha_ncmnto_rcn_ncdo      , cnsctvo_cdgo_sxo_rcn_ncdo      , cdgo_sxo_rcn_ncdo         ,				
		     fcha_crcn                 , cnsctvo_cdgo_tpo_cntrto        , edd                       ,
		     edd_mss                   , edd_ds                         , cnsctvo_rgstro_vldcn_espcl,
		     nmro_vldcn_espcl          , nmro_unco_idntfccnr								
	FROM     #tb_tmpAfiliado	
	Order By estdo ASC
	
	Drop Table  #tb_tmpAfiliado;
	
END

GO
