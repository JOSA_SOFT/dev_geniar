USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] AS SELECT 1;'
END
GO

/*-------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaInformacionSolicitudxProgramacionEntregaWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información de la solicitud correspondiente
						   a la programaciones de entrega generadas
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           @cnsctvo_slctd_atrzcn_srvco udtConsecutivo	
						   @@cnsctvo_cdgo_srvco_slctdo udtConsecutivo		         
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 11/07/2016 FC\>    
*---------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Luis Rodriguez De León AM\>    
* Descripcion        : <\D Se modifica el sp para devolver flag que indica si alguna las prestaciones liquidadas 
						   presenta algún concepto de liquidación en cero.
						D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 23/12/2016 FM\>    
*---------------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 2084, 3291
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 1031, 2192
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 2344, 3642
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 721, 1776
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 2104, 3317
 --exec [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 3222, 288250
 
 ALTER PROCEDURE [gsa].[spASConsultaInformacionSolicitudxProgramacionEntregaWeb] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
   @cnsctvo_cdgo_srvco_slctdo  udtConsecutivo
AS

Begin
   SET NOCOUNT ON
   
   Declare @vlr_si  char(1),
		   @vlr_no	char(1),
		   @vlr_cro int

   Create
   Table  #tempInfoSolicitud(cnsctvo_slctd_atrzcn_srvco             udtConsecutivo,
                             nmro_slctd_atrzcn_ss                   Varchar(16)   ,
							 cnsctvo_cdgo_estdo_slctd               udtConsecutivo,
                             cnsctvo_srvco_slctdo                   udtConsecutivo,	                             
							 cnsctvo_cdgo_srvco_slctdo              udtConsecutivo,	
					         cdgo_srvco_slctdo                      Char(15)	  ,
							 cnsctvo_cdgo_estdo_srvco_slctdo        udtConsecutivo, 			  
                             fcha_utlzcn_dsde                       Datetime      ,
                             fcha_utlzcn_hsta                       Datetime      ,                             
                             cnsctvo_cdgo_estdo_cncpto_srvco_slctdo udtConsecutivo, 						 
							 nmro_unco_ops                          udtConsecutivo,
							 vlr_lqddo_grpo_imprsn                  Int			  ,
							 vlr_cncpto_cro							char(1)          
					        )
	Set @vlr_si  = 'S'
	Set @vlr_no  = 'N'
	Set @vlr_cro = 0
	   
   --Se insertan en la tabla la informacion de los procedimientos
   Insert 
   Into   #tempInfoSolicitud(cnsctvo_slctd_atrzcn_srvco            , nmro_slctd_atrzcn_ss     , cnsctvo_cdgo_estdo_slctd       ,
                             cnsctvo_srvco_slctdo                  , cnsctvo_cdgo_srvco_slctdo, cnsctvo_cdgo_estdo_srvco_slctdo,
							 fcha_utlzcn_dsde                      , fcha_utlzcn_hsta         , nmro_unco_ops                  ,
							 cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, vlr_lqddo_grpo_imprsn	  , vlr_cncpto_cro
							)   
   Select      a.cnsctvo_slctd_atrzcn_srvco            , a.nmro_slctd_atrzcn_ss     , a.cnsctvo_cdgo_estdo_slctd       ,
               c.cnsctvo_srvco_slctdo                  , c.cnsctvo_cdgo_srvco_slctdo, c.cnsctvo_cdgo_estdo_srvco_slctdo, 
			   x.fcha_utlzcn_dsde                      , x.fcha_utlzcn_hsta         , x.nmro_unco_ops                  ,
			   x.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, Sum(x.vlr_lqdcn)			,
			   Case 
				When x.vlr_lqdcn > @vlr_cro Then @vlr_no
				Else @vlr_si
			   End	vlr_cncpto_cro
   From        bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
   Inner Join  bdCNA.gsa.tbASServiciosSolicitados c WITH (NOLOCK)	
   On          c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
   Inner Join  bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
   On          b.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
   Inner Join  bdcna.gsa.tbASConceptosServicioSolicitado x WITH (NOLOCK)
   On          x.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
   Where       a.cnsctvo_slctd_atrzcn_srvco   = @cnsctvo_slctd_atrzcn_srvco
   And         c.cnsctvo_cdgo_srvco_slctdo    = @cnsctvo_cdgo_srvco_slctdo
   Group By    a.cnsctvo_slctd_atrzcn_srvco            , a.nmro_slctd_atrzcn_ss     , a.cnsctvo_cdgo_estdo_slctd       ,
               c.cnsctvo_srvco_slctdo                  , c.cnsctvo_cdgo_srvco_slctdo, c.cnsctvo_cdgo_estdo_srvco_slctdo, 
			   x.fcha_utlzcn_dsde                      , x.fcha_utlzcn_hsta         , x.nmro_unco_ops                  ,
			   x.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, x.vlr_cncpto_cm			, x.vlr_lqdcn

   --Se insertan en la tabla la informacion de los medicamentos
   Insert 
   Into   #tempInfoSolicitud(cnsctvo_slctd_atrzcn_srvco            , nmro_slctd_atrzcn_ss     , cnsctvo_cdgo_estdo_slctd       ,
                             cnsctvo_srvco_slctdo                  , cnsctvo_cdgo_srvco_slctdo, cnsctvo_cdgo_estdo_srvco_slctdo,
							 fcha_utlzcn_dsde                      , fcha_utlzcn_hsta         , nmro_unco_ops                  ,
							 cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, vlr_lqddo_grpo_imprsn	  , vlr_cncpto_cro
							)   
   Select      a.cnsctvo_slctd_atrzcn_srvco            , a.nmro_slctd_atrzcn_ss     , a.cnsctvo_cdgo_estdo_slctd       ,
               c.cnsctvo_srvco_slctdo                  , c.cnsctvo_cdgo_srvco_slctdo, c.cnsctvo_cdgo_estdo_srvco_slctdo, 
			   x.fcha_utlzcn_dsde                      , x.fcha_utlzcn_hsta         , x.nmro_unco_ops                  ,
			   x.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, Sum(x.vlr_lqdcn)			,
			   Case 
				When x.vlr_lqdcn > @vlr_cro Then @vlr_no
				Else @vlr_si
			   End	vlr_cncpto_cro
   From        bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
   Inner Join  bdCNA.gsa.tbASServiciosSolicitados c WITH (NOLOCK)	
   On          c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
   Inner Join  bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
   On          b.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
   Inner Join  bdcna.gsa.tbASConceptosServicioSolicitado x WITH (NOLOCK)
   On          x.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
   Where       a.cnsctvo_slctd_atrzcn_srvco   = @cnsctvo_slctd_atrzcn_srvco
   And         c.cnsctvo_cdgo_srvco_slctdo    = @cnsctvo_cdgo_srvco_slctdo
   Group By    a.cnsctvo_slctd_atrzcn_srvco            , a.nmro_slctd_atrzcn_ss     , a.cnsctvo_cdgo_estdo_slctd       ,
               c.cnsctvo_srvco_slctdo                  , c.cnsctvo_cdgo_srvco_slctdo, c.cnsctvo_cdgo_estdo_srvco_slctdo, 
			   x.fcha_utlzcn_dsde                      , x.fcha_utlzcn_hsta         , x.nmro_unco_ops                  ,
			   x.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, x.vlr_cncpto_cm			, x.vlr_lqdcn
   
   --Se actualiza el codigo de o los servicios
   Update      #tempInfoSolicitud
   Set         cdgo_srvco_slctdo = ori.cdgo_srvco_slctdo
   From        #tempInfoSolicitud c
   Inner Join  bdCNA.gsa.tbASServiciosSolicitadosOriginal ori WITH (NOLOCK)	
   On          ori.cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco And
               ori.cnsctvo_srvco_slctdo       = c.cnsctvo_srvco_slctdo
         														 
   Select     cnsctvo_slctd_atrzcn_srvco            , nmro_slctd_atrzcn_ss           , cnsctvo_srvco_slctdo,
              cnsctvo_cdgo_srvco_slctdo             , cnsctvo_cdgo_estdo_srvco_slctdo, cdgo_srvco_slctdo   ,
			  fcha_utlzcn_dsde                      , fcha_utlzcn_hsta               , nmro_unco_ops       ,
			  cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, vlr_cncpto_cro
   From       #tempInfoSolicitud        

   Drop Table #tempInfoSolicitud

End


GO
