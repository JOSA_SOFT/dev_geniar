USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spAsGuardarReasignacionPrestacionAuditoriaIntegral]    Script Date: 10/02/2017 4:04:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spAsGuardarReasignacionPrestacionAuditoriaIntegral') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spAsGuardarReasignacionPrestacionAuditoriaIntegral AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spAsGuardarReasignacionPrestacionAuditoriaIntegral
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS						 A\>
* Descripcion			: <\D 
								SP creado para guardar el registro de la Reasignación de las prestaciones
								entre auditores. Registra las observaciones y el tipo de auditoria													
                          D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2017/03/08 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spAsGuardarReasignacionPrestacionAuditoriaIntegral] 227065, 'auditor1', 'Esta es una prueba de Pruebas', 'user1', null, null
AlTER PROCEDURE [gsa].[spAsGuardarReasignacionPrestacionAuditoriaIntegral] 
@cnsctvo_srvco_slctdo			udtConsecutivo,
@descGrupoAuditor				varchar(30),
@obsrvcn						udtObservacion,
@usuario_sistema				udtUsuario,
@cdgo_rsltdo					udtConsecutivo  output,
@mnsje_rsltdo					varchar(2000)	output

AS

BEGIN
	SET NOCOUNT ON;
	Declare @codigo_ok									int,
			@codigo_error								int,
			@mensaje_ok									udtDescripcion,
			@mensaje_error								varchar(2000),
			@fecha_actual								datetime,
			@cnsctvo_cdgo_srvco_slctdo					udtConsecutivo,
			@dscrpcn_prstcn								udtDescripcion,
			@rsto_cdna									varchar(3),
			@rsto_cdna2									varchar(3);
	

	Set @codigo_error					 = -1  -- Error SP
	Set @codigo_ok						 = 0   -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente'
	Set @fecha_actual					 = getDate()
	Set @rsto_cdna						 = ': '
	Set @rsto_cdna2						 = '->'
	
	BEGIN TRY


			--Se actualiza la prestación adicionando el auditor al que se remitió el caso
			Update ss
			Set rqre_otra_gstn_adtr = @descGrupoAuditor,
				usro_ultma_mdfccn	= @usuario_sistema,
				fcha_ultma_mdfccn   = @fecha_actual
			From BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK) 
			Where ss.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

			Select  @cnsctvo_cdgo_srvco_slctdo  = ss.cnsctvo_slctd_atrzcn_srvco, 
					@dscrpcn_prstcn				= ss.dscrpcn_srvco_slctdo
			from BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK) 
			Where ss.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo 
											
			--Se actualiza la observación ingresada por el auditor
			Update s
			Set obsrvcns_rqre_otra_gstn_adtr	= Concat(ISNULL (obsrvcns_rqre_otra_gstn_adtr,''), CHAR(13), 
														@rsto_cdna2, rtrim(ltrim(@dscrpcn_prstcn)), @rsto_cdna, @obsrvcn),
				usro_ultma_mdfccn				= @usuario_sistema,
				fcha_ultma_mdfccn				= @fecha_actual
			From BDCna.gsa.tbASSolicitudesAutorizacionServicios s WITH (NOLOCK)
			where s.cnsctvo_slctd_atrzcn_srvco	= @cnsctvo_cdgo_srvco_slctdo


			Set @cdgo_rsltdo	 = @codigo_ok				
			Set @mnsje_rsltdo	 = @mensaje_ok

			
	END TRY
	BEGIN CATCH
		SET @mensaje_error = Concat(	    'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
											'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
											'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
											'Procedure:' , ERROR_PROCEDURE()
										);

		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

END
