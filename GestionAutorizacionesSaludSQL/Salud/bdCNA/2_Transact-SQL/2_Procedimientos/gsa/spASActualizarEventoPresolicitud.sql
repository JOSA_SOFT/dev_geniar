USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEventoPresolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASActualizarEventoPresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASActualizarEventoPresolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASActualizarEventoPresolicitud
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Se actualizan los eventos presolicitudes			D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/25										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM													AM\>
* Descripcion			 : <\DM													DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarEventoPresolicitud]
	@cnsctvo_evnto udtconsecutivo,
	@cnsctvo_cdgo_estdo_evnto udtconsecutivo,
	@cmntrio_evnto	udtObservacion
AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @fcha_actl DATETIME = GETDATE(),
			@usr_lgn   udtusuario;

	SET @usr_lgn = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user));
  	
	UPDATE	gsa.tbAsIntegracionEventos
	SET		cnsctvo_cdgo_estdo_evnto = @cnsctvo_cdgo_estdo_evnto,
		    cmntro_evnto = @cmntrio_evnto,
			fcha_ultma_mdfccn = @fcha_actl,
			usro_ultma_mdfccn = @usr_lgn
	WHERE	cnsctvo_evnto = @cnsctvo_evnto;

END;

GO
