USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarEmpresaVIP]    Script Date: 27/06/2017 15:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarEmpresaVIP
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida si la empresa es VIP	 D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 23/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento para que realice los joins 
							de las tablas temporales con los campos correctos y trabaje con el 
							nmro_unco_idntfccn_empleador
                         DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 27/06/2017  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarEmpresaVIP] 
   @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
   @cdgo_grpo_entrga         Char(4),
   @agrpa_prstcn             udtLogico
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @consecutivoPAC                  udtConsecutivo              ,
          @afirmacion                      udtLogico                   ,
          @cantidadRegistros               Int                         ,
          @indice                          Int                         ,
          @cnsctvo_cdgo_tpo_cntrto         udtConsecutivo              ,
          @nmro_cntrto                     Char(15)                    ,
          @cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo              ,
          @nmro_idntfccn_afldo             udtNumeroIdentificacionLargo,
          @id                              Int                         ,
		  @cnsctvo_emprsa_vp               udtConsecutivo              ,
		  @fechaActual                     Datetime                    ;

  Create
  Table  #tmpEmpleadores(cdgo_tpo_idntfccn          Varchar(3)                  ,
                         nmro_idntfccn              udtNumeroIdentificacionLargo,
                         rzn_scl                    Varchar(200)                ,
                         idntfccn_cmplta_empldr     Varchar(30)                 ,
                         cnsctvo_cdgo_tpo_idntfccn  udtConsecutivo              ,
                         nmro_unco_idntfccn_aprtnte udtConsecutivo              ,
                         prncpl                     udtLogico                   ,
                         cnsctvo_scrsl_ctznte       udtConsecutivo              ,
                         cnsctvo_cdgo_crgo_empldo   udtConsecutivo              ,
                         cnsctvo_cdgo_clse_aprtnte  udtConsecutivo              ,
                         cnsctvo_cdgo_tpo_ctznte    udtConsecutivo              ,
                         inco_vgnca_cbrnza          Datetime                    ,
                         fn_vgnca_cbrnza            Datetime                    ,
                         cnsctvo_cdgo_tpo_cbrnza    udtConsecutivo              ,
                         cnsctvo_cbrnza             udtConsecutivo              ,
                         slro_bsco                  Int                         ,
                         cnsctvo_cdgo_tpo_cntrto    udtConsecutivo              ,
                         nmro_cntrto                Varchar(15)                 ,
                         cnsctvo_prdcto_scrsl       udtConsecutivo              ,
                         cnsctvo_scrsl              udtConsecutivo              ,
                         cnsctvo_cdgo_actvdd_ecnmca udtConsecutivo              ,
                         cnsctvo_cdgo_arp           udtConsecutivo              ,
                         drccn                      udtDireccion                ,
                         tlfno                      udtTelefono                 ,
                         cdgo_actvdd_ecnmca         Char(4)                     ,
                         dscrpcn_actvdd_ecnmca      udtDescripcion              ,
                         cdgo_crgo                  Char(4)                     ,
                         dscrpcn_crgo               udtDescripcion              ,
                         cdgo_entdd                 char(8)                     ,
                         dscrpcn_entdd              udtDescripcion              ,
                         cdgo_tpo_ctznte            udtCodigo                   ,
                         dscrpcn_tpo_ctznte         udtDescripcion              ,
                         nmbre_cntcto               Varchar(140)                ,
                         tlfno_cntcto               udtTelefono                 ,
                         eml_cntcto                 udtEmail                    ,
                         cnsctvo_cdgo_cdd           udtconsecutivo              ,
                         cdgo_cdd                   Char(8)                     ,
                         dscrpcn_cdd                udtDescripcion              ,
                         cnsctvo_cdgo_dprtmnto      udtConsecutivo              ,
                         cdgo_dprtmnto              Char(3)                     ,
                         dscrpcn_dprtmnto           udtDescripcion              ,
                         eml                        udtEmail                    ,
                         cdgo_cnvno_cmrcl           udtCodigo                   ,
                         dscrpcn_cnvno_cmrcl        udtDescripcion
                        )

  Set @afirmacion        = 'S';
  Set @consecutivoPAC    = 2;
  Set @indice            = 1;
  Set @cnsctvo_emprsa_vp = 45;
  Set @fechaActual       = getDate()

  If Exists (Select id 
             From   #tmpDatosSolicitudFechaEntrega 
             WHERE  cnsctvo_grpo_entrga Is Null
			)
	 BEGIN
	    SELECT @cantidadRegistros = COUNT(id) FROM #tmpDatosSolicitudFechaEntrega;

        While @indice <= @cantidadRegistros
          Begin
             Select @cnsctvo_cdgo_tpo_cntrto         = cnsctvo_cdgo_tpo_cntrto        ,
                    @nmro_cntrto                     = nmro_cntrto                    ,
                    @cnsctvo_cdgo_tpo_idntfccn_afldo = cnsctvo_cdgo_tpo_idntfccn_afldo,
                    @nmro_idntfccn_afldo             = nmro_idntfccn                  ,
                    @id                              = id
             From   #tmpDatosSolicitudFechaEntrega
             Where  id = @indice
             And    cnsctvo_grpo_entrga IS NULL;

             If @id IS NOT NULL
               Begin
                   /*Se obtienen los datos de empleador*/
                   Insert 
				   Into    #tmpEmpleadores(cdgo_tpo_idntfccn        , nmro_idntfccn             , rzn_scl                  , idntfccn_cmplta_empldr    , 
				                           cnsctvo_cdgo_tpo_idntfccn, nmro_unco_idntfccn_aprtnte, prncpl                   , cnsctvo_scrsl_ctznte      , 
                                           cnsctvo_cdgo_crgo_empldo , cnsctvo_cdgo_clse_aprtnte , cnsctvo_cdgo_tpo_ctznte  , inco_vgnca_cbrnza         , 
                                           fn_vgnca_cbrnza          , cnsctvo_cdgo_tpo_cbrnza   , cnsctvo_cbrnza, slro_bsco, cnsctvo_cdgo_tpo_cntrto   , 
	                                       nmro_cntrto              , cnsctvo_prdcto_scrsl      , cnsctvo_scrsl            , cnsctvo_cdgo_actvdd_ecnmca, 
	                                       cnsctvo_cdgo_arp         , drccn                     , tlfno                    , cdgo_actvdd_ecnmca        , 
	                                       dscrpcn_actvdd_ecnmca    , cdgo_crgo                 , dscrpcn_crgo             , cdgo_entdd                , 
	                                       dscrpcn_entdd            , cdgo_tpo_ctznte           , dscrpcn_tpo_ctznte       , nmbre_cntcto              , 
	                                       tlfno_cntcto             , eml_cntcto                , cnsctvo_cdgo_cdd         , cdgo_cdd                  ,
                                           dscrpcn_cdd              , cnsctvo_cdgo_dprtmnto     , cdgo_dprtmnto            , dscrpcn_dprtmnto          , 
                                           eml                      , cdgo_cnvno_cmrcl          , dscrpcn_cnvno_cmrcl
                                          )
                   Exec BDAfiliacionValidador.dbo.spPmConsultaDetEmpleadoresAfiliado @cnsctvo_cdgo_tpo_cntrto        , @nmro_cntrto        , 
				                                                                     NULL                            , NULL                , 
                                                                                     @cnsctvo_cdgo_tpo_idntfccn_afldo, @nmro_idntfccn_afldo,
                                                                                     NULL                            , NULL                ,
                                                                                     NULL                            , NULL;

                   /*Se asocia el afiliado con el empleador*/
                   Update #tmpDatosSolicitudFechaEntrega
                   Set    nmro_unco_idntfccn_empleador = nmro_unco_idntfccn_aprtnte
                   From   #tmpEmpleadores 
                   Where  id = @id

                   DELETE From #tmpEmpleadores
               End

             Set @indice = @indice + 1;
          End;

    --Se actualiza al grupo en caso de que la empresa sea VIP.
    Update     #tmpDatosSolicitudFechaEntrega
    Set        cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
               cdgo_grpo_entrga = @cdgo_grpo_entrga
    From       #tmpDatosSolicitudFechaEntrega sol
    Inner Join bdAfiliacionValidador.dbo.tbMarcasProductosxSucursal mps With(NoLock)
    On         mps.nmro_unco_idntfccn_empldr = sol.nmro_unco_idntfccn_empleador
    INNER JOIN BDAfiliacionValidador.dbo.tbMarcasEspeciales_Vigencias tbev With(NoLock)
	On         tbev.cnsctvo_mrca_espcl       = mps.cnsctvo_cdgo_mrca_espcl	           
    WHERE      tbev.cnsctvo_vgnca_mrca_espcl = @cnsctvo_emprsa_vp --VIP
	And        sol.cnsctvo_grpo_entrga IS NULL
	And        @fechaActual Between tbev.inco_vgnca And tbev.fn_vgnca;

    --Se determina si se debe realizar agrupación
    If Exists (Select id
               From   #tmpDatosSolicitudFechaEntrega
               Where  cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
               And    @agrpa_prstcn       = @afirmacion
			  )
        Begin
           Update     sol
           Set        cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
                      cdgo_grpo_entrga    = @cdgo_grpo_entrga
           From       #tmpDatosSolicitudFechaEntrega sol
           Inner Join #tmpDatosSolicitudFechaEntrega tmp
           ON         tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
           WHERE      tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
        End
  End

  Drop Table #tmpEmpleadores

END
