USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRegistrarCausalesDevolucionPresolicitud]    Script Date: 26/01/2017 17:23:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASRegistrarCausalesDevolucionPresolucitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  A\>
* Descripcion			: <\D 
                              Sp que se utiliza para guardar la causal de la de volución en la tabla 
                          D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/09/09 FC\>
*---------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se actualiza el procedimiento para que calcule correctamente el campo cnsctvo_prstcn_ngda_prslctd 
								el cual se debe calcular con el MAX + 1 	
								Se quita el uso del BEGIN - TRY
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/01/26  FM\>
*---------------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASRegistrarCausalesDevolucionPresolicitud]  
@cnsctvo_prslctd	udtConsecutivo,
@usro_gstn			udtUsuario


AS
BEGIN
	SET NOCOUNT ON;

	Declare @codigo_ok			        Int,
			@codigo_error		        Int,
			@mensaje_ok			        udtDescripcion,
			@mensaje_nok     	        udtDescripcion,
			@mensaje_error		        Varchar(2000),
			@ldfecha_validacion	        Datetime,
			@cnsctvo_prstcn_ngda        udtConsecutivo,
			@cdgo_rsltdo		        udtConsecutivo ,
			@mnsje_rsltdo		        Varchar(2000),
			@cnsctvo_cdfccn		        udtConsecutivo,
			@cnsctvo_cdgo_estdo_prslctd udtConsecutivo


	Create Table #tmpCausalesDevolucionPresolicitud(
		cnsctvo_cdgo_csa_nvdd	udtConsecutivo,
		jstfccn					udtDescripcion
	)
	
	Set @codigo_error		        = -1;  -- Error SP
	Set @codigo_ok			        =  0;  -- Codigo de mensaje Ok
	Set @mensaje_ok			        = 'Proceso ejecutado satisfactoriamente';
	Set @mensaje_nok			    = 'Proceso ejecutado con inconsistencias';
	Set @ldfecha_validacion	        = getDate();
	Set @cnsctvo_cdfccn		        = 0;
	Set @cnsctvo_cdgo_estdo_prslctd = 16

	
	--Consulta el detalle de la presolicitud devuelta
	Insert 
	Into    #tmpCausalesDevolucionPresolicitud(cnsctvo_cdgo_csa_nvdd, jstfccn)
	Select  cnsctvo_cdgo_csa_nvdd, jstfccn
	From    BDCna.gsa.tbASDatosAdicionalesPreSolicitud With(NoLock)
	where   cnsctvo_prslctd = @cnsctvo_prslctd

	Select @cnsctvo_prstcn_ngda = ISNULL(Max(cnsctvo_prstcn_ngda_prslctd),0) + 1 
	From   BDCna.dbo.tbPrestacionesNegadasPreSolicitud With(NoLock)
								   
	Insert 
	Into   BDCna.dbo.tbPrestacionesNegadasPreSolicitud (cnsctvo_prstcn_ngda_prslctd , cnsctvo_prslctd  , cnsctvo_cdfccn   , 
		                                                cnsctvo_cdgo_csa_ngcn_prstcn, obsrvcn_csa_ngcn , fcha_crcn        , 
														usro_crcn                   , fcha_ultma_mdfccn, usro_ultma_mdfccn
		                                                ) 
	Select @cnsctvo_prstcn_ngda , @cnsctvo_prslctd   , @cnsctvo_cdfccn    , 
		   cnsctvo_cdgo_csa_nvdd, jstfccn            , @ldfecha_validacion, 
		   @usro_gstn           , @ldfecha_validacion, @usro_gstn         
	From   #tmpCausalesDevolucionPresolicitud

	If @@ROWCOUNT > 0 
	   Begin
	       	Set @cdgo_rsltdo  =	@codigo_ok
	        Set @mnsje_rsltdo =	@mensaje_ok	

			Update bdcna.dbo.tbPreSolicitudes 
			Set    cnsctvo_cdgo_estdo_prslctd = @cnsctvo_cdgo_estdo_prslctd 
			Where  cnsctvo_prslctd = @cnsctvo_prslctd			
	   End	
	Else
	  Begin
	      Set @cdgo_rsltdo  =	@codigo_error
	      Set @mnsje_rsltdo =	@mensaje_nok	
	  End
	
	Select @cdgo_rsltdo cdgo_rsltdo, @mnsje_rsltdo mnsje_rsltdo

	Drop table #tmpCausalesDevolucionPresolicitud;
END
