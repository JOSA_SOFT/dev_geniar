USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionEstadosGestionAuditor]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosEstadosGestionAuditor
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM		Ing. Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM		Se ajusta SP para que devuelva  el estado de las 
									prestaciones incluyendo el estado En Euditotia
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 	28/03/2017FM\>
*---------------------------------------------------------------------------------*/
-- EXEC gsa.spASConsultaDatosInformacionEstadosGestionAuditor
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionEstadosGestionAuditor]

AS
BEGIN
	SET NOCOUNT ON;

	Declare @est_vacio 					udtConsecutivo 	= 0,
			@est_inexistente 			udtConsecutivo	= 99,
			@fecha_vigencia				datetime,
			@estds_srvco_adtra			udtConsecutivo,
			@estds_srvco_dvlta			udtConsecutivo,
			@estds_srvco_aprbda			udtConsecutivo,
			@estds_srvco_no_atrzda		udtConsecutivo,
			@estds_srvco_anlda			udtConsecutivo;


	Set @fecha_vigencia			= getDate();
	Set @estds_srvco_adtra		= 5
	Set @estds_srvco_dvlta		= 6
	Set @estds_srvco_aprbda		= 7	
	Set @estds_srvco_no_atrzda	= 8	
	Set	@estds_srvco_anlda		= 14

	SELECT est.cnsctvo_cdgo_estdo_srvco_slctdo , 
		   est.dscrpcn_estdo_srvco_slctdo 
	FROM BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias est WITH (NOLOCK)
	Where est.cnsctvo_cdgo_estdo_srvco_slctdo in (@estds_srvco_adtra, @estds_srvco_dvlta, 
												  @estds_srvco_aprbda, @estds_srvco_no_atrzda, 
												  @estds_srvco_anlda
												 )
	And   @fecha_vigencia between est.inco_vgnca and est.fn_vgnca
	

END

GO
