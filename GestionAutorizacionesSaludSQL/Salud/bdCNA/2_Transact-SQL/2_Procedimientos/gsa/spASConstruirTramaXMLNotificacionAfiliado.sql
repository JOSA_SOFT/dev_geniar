USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConstruirTramaXMLNotificacionAfiliado]    Script Date: 31/01/2017 10:49:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF (object_id('gsa.spASConstruirTramaXMLNotificacionAfiliado') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASConstruirTramaXMLNotificacionAfiliado AS SELECT 1;'
END

Go
/*--------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConstruirTramaXMLNotificacionAfiliado
* Desarrollado por		: <\A Jonathan Chamizo - SETI SAS  					 A\>
* Descripcion			: <\D Construcción trama xml para el envío de notificación 
							  de afiliados									 D\>
						  <\D .												 D\>
* Observaciones			: <\O  												 O\>
* Parametros			: <\P Usuario, TramaXML								 P\>
* Variables				: <\V  												 V\>
* Fecha Creacion		: <\FC 2016/03/25									 FC\>
*--------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge RodriguesAM\>
* Descripcion			 : <\DM	Se modifica SP para ejecutar dinamicamente la generacion de las plantillasDM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016-01-06 FM\>
*--------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para que consulte la plantilla que se de utilizar
                            DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-01-31 FM\>
*--------------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConstruirTramaXMLNotificacionAfiliado] 
	@usrio VARCHAR(50),
	@cnstrccn_trma_xml XML OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
			@instruccionSQL		NVARCHAR(4000),
			@ParmDefinition		NVARCHAR(500),
			@nmbre_sp			CHAR(100),
			@ldfechaActual		datetime,
			@afirmacion			UDTLOGICO;

	SET @ldfechaActual = GETDATE()
	SET @afirmacion = 'S'


	SET @ParmDefinition = N'@usrio				char(50),
							@cnstrccn_trma_xml	XML OUTPUT'

	
	SELECT     @nmbre_sp =  p.nmbre_sp 
	From       BDCna.prm.tbASPlantillasNotificaciones_Vigencias p WITH (NOLOCK)
	INNER JOIN #tmpSlctdes temp 
	ON         temp.plntlla = p.cnsctvo_cdgo_plntlla_mnsje
	WHERE      @ldfechaActual BETWEEN p.inco_vgnca AND p.fn_vgnca
    AND        p.vsble_usro = @afirmacion
			
	SET @instruccionSQL = N'EXEC bdcna.gsa.' + LTRIM(RTRIM(@nmbre_sp)) + N' @usrio, @cnstrccn_trma_xml OUTPUT'	
					
	EXECUTE sp_executesql @instruccionSQL, @ParmDefinition, @usrio = @usrio, @cnstrccn_trma_xml = @cnstrccn_trma_xml OUTPUT
		
END

