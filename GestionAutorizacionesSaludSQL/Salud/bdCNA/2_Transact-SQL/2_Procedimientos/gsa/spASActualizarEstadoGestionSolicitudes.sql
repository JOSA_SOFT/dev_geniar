USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEstadoGestionSolicitudes]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASactualizarEstadoGestionSolicitudes
* Desarrollado por		: <\A Jhon W. Olarte V.									A\>
* Descripcion			: <\D Procedimiento encargado de actualizar los			D\>
						  <\D estados de manera masiva							D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 24-05-2016										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarEstadoGestionSolicitudes]
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @vlr_cro	INT

  SELECT @vlr_cro = 0
  --Actualización de estados de conceptos OPS a partir de número único de OPS
  IF EXISTS (SELECT TOP 1
      1
    FROM #tmpestadosactualizar TEA
    WHERE TEA.nmro_unco_ops <> @vlr_cro)
  BEGIN
    UPDATE css
    SET cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = tea.cnsctvo_cdgo_estdo
    FROM bdcna.gsa.tbasconceptosserviciosolicitado CSS
    INNER JOIN #tmpestadosactualizar TEA
      ON CSS.nmro_unco_ops = TEA.nmro_unco_ops
    WHERE TEA.nmro_unco_ops <> @vlr_cro
  END

  --Actualización de estados de prestaciones.
  IF EXISTS (SELECT TOP 1
      1
    FROM #tmpestadosactualizar TEA
    WHERE TEA.cnsctvo_srvco_slctdo <> @vlr_cro)
  BEGIN
    UPDATE sso
    SET cnsctvo_cdgo_estdo_srvco_slctdo = tea.cnsctvo_cdgo_estdo
    FROM bdcna.gsa.tbasserviciossolicitados SSO
    INNER JOIN #tmpestadosactualizar TEA
      ON SSO.cnsctvo_srvco_slctdo = TEA.cnsctvo_srvco_slctdo
    WHERE TEA.cnsctvo_srvco_slctdo <> @vlr_cro
  END

  --Actualización de estados de Solicitud
  IF EXISTS (SELECT TOP 1
      1
    FROM #tmpestadosactualizar TEA
    WHERE TEA.cnsctvo_slctd_atrzcn_srvco <> @vlr_cro)
  BEGIN
    UPDATE sas
    SET cnsctvo_cdgo_estdo_slctd = tea.cnsctvo_cdgo_estdo
    FROM bdcna.gsa.tbassolicitudesautorizacionservicios SAS
    INNER JOIN #tmpestadosactualizar TEA
      ON SAS.cnsctvo_slctd_atrzcn_srvco = TEA.cnsctvo_slctd_atrzcn_srvco
    WHERE TEA.cnsctvo_slctd_atrzcn_srvco <> @vlr_cro
  END

END

GO
