Use bdCNA
Go

If(OBJECT_ID('gsa.spASValidarCausalesNoCobroCuota') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASValidarCausalesNoCobroCuota As Select 1';
End
Go

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASValidarCausalesNoCobroCuota												
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez							  A\>  
* Descripcion		: <\D Procedimiento encargado de aplicar las reglas de no cobro de cuota de recuperacion. D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2017/08/28 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>
* Descripcion			: <\DM DM\>
* Nuevos Parametros	 	: <\PM PM\>
* Nuevas Variables		: <\VM VM\>
* Fecha Modificacion	: <\FM FM\>
*-----------------------------------------------------------------------------------------------------------------------*/

Alter Procedure gsa.spASValidarCausalesNoCobroCuota
As
Begin

	Set Nocount On
	
	Declare		@dscrpcn_estdo_ntfccn_ntfcdo				udtDescripcion,
				@dscrpcn_estdo_ntfccn_cnfrmdo				udtDescripcion,
				@dscrpcn_estdo_ntfccn_prbble				udtDescripcion,
				@cnsctvo_cdgo_itm_bnfccn100					udtConsecutivo,
				@ValorNO									udtLogico,
				@cnsctvo_cdgo_pln1							udtConsecutivo,
				@cnsctvo_cdgo_pln9							udtConsecutivo,
				@cnsctvo_cdgo_tpo_srvco4					udtConsecutivo,
				@cnsctvo_cdgo_tpo_srvco5					udtConsecutivo,
				@cnsctvo_cdgo_agrpdr_prstcn294				udtConsecutivo,				
				@cnsctvo_cdgo_tpo_mrca_prstcn6				udtConsecutivo,
				@fcha_actl									Datetime,
				@vlr_si										udtLogico,
				@cnsctvo_cdgo_tpo_dscpcdd1					udtConsecutivo,
				@cnsctvo_cdgo_tpo_dscpcdd2					udtConsecutivo,
				@cnsctvo_cdgo_tpo_dscpcdd3					udtConsecutivo,
				@edd_afldo_ans1								Int,
				@edd_afldo_ans18							Int,
				@cnsctvo_cdgo_clsfccn_evnto_ntfccn13		udtConsecutivo,
				@cnsctvo_cdgo_rngo_slrl5					udtConsecutivo,
				@cnsctvo_cdgo_rngo_slrl6					udtConsecutivo,
				@ds_ano										Float,
				@cnsctvo_cdgo_tpo_mrca_dgnstco				udtConsecutivo,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn24		udtConsecutivo,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn25		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn26		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn27		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn28		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn29		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn30		udtConsecutivo
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn31		udtConsecutivo;

	Create Table #tempEdadAFechaSolicitud (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		edd_fcha_slctd					Int Default 0
	)

	-- 
	Set @dscrpcn_estdo_ntfccn_ntfcdo	= 'NOTIFICADO'; -- 
	Set @dscrpcn_estdo_ntfccn_cnfrmdo	= 'CONFIRMADO'; -- 
	Set @dscrpcn_estdo_ntfccn_prbble	= 'PROBABLE'; -- 
	Set @cnsctvo_cdgo_itm_bnfccn100		= 100; -- Alto Costo
	Set @ValorNO = 'N';
	Set @cnsctvo_cdgo_tpo_srvco4 = 4; -- CUPS
	Set @cnsctvo_cdgo_tpo_srvco5 = 5; -- CUMS
	Set @cnsctvo_cdgo_agrpdr_prstcn294 = 294; -- Cancer, VIH y ERC
	Set @cnsctvo_cdgo_pln9 = 9;
	Set @cnsctvo_cdgo_tpo_mrca_prstcn6 = 6; -- Prestacion Marcada para Rehabilitacion
	Set @fcha_actl = GETDATE();
	Set @vlr_si = 'S';
	Set @cnsctvo_cdgo_tpo_dscpcdd1 = 1; -- Discapacidad Fisica.
	Set @cnsctvo_cdgo_tpo_dscpcdd2 = 2; -- Discapacidad Mental.
	Set @cnsctvo_cdgo_tpo_dscpcdd3 = 3; -- Discapacidad Sensorial.
	Set @edd_afldo_ans1 = 1;
	Set @edd_afldo_ans18 = 18;
	Set @cnsctvo_cdgo_clsfccn_evnto_ntfccn13 = 13;
	Set @cnsctvo_cdgo_rngo_slrl5 = 5;
	Set @cnsctvo_cdgo_rngo_slrl6 = 6;
	Set @ds_ano = 365.25;
	Set @cnsctvo_cdgo_tpo_mrca_dgnstco = 1; -- Alto Costo
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn24 = 24;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn25 = 25;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn26 = 26;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn27 = 27;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn28 = 28;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn29 = 29;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn30 = 30;
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn31 = 31;

	-- Se calcula la edad del afiliado a la fecha de solicitud del medico
	Insert Into #tempEdadAFechaSolicitud (
				cnsctvo_slctd_atrzcn_srvco
	)
	Select Distinct a.cnsctvo_slctd_atrzcn_srvco
	From		#TempSolicitudes a;
	
	Update		a
	Set			edd_fcha_slctd = Convert(Int, DateDiff(d, b.fcha_ncmnto, b.fcha_slctd)/@ds_ano)
	From		#tempEdadAFechaSolicitud a
	Inner Join	#TempSolicitudes b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco;

	-- Regla 1 
	-- Evento de alto costo afiliados con CANCER-VIH-ERC-GRAN QUEMADO
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn24,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'El servicio con marca de Evento de Alto costo regimen contributivo, no cancela cuota de recuperaci�n'
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sl
	On			sl.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpNotificacionesAfiliado na 
	On			na.cnsctvo_slctd_atrzcn_srvco = sl.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn100 -- Alto costo
	And			sl.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln1
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco4
	And			na.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo);
	
	-- Regla 2
	-- Medicamentos asociados a evento de alto costo
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn25,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Medicamento asociado a evento de alto costo regimen contributivo'
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sl
	On			sl.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpNotificacionesAfiliado na 
	On			na.cnsctvo_slctd_atrzcn_srvco = sl.cnsctvo_slctd_atrzcn_srvco
	Where		sl.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln1
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5
	And			na.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo)
	And			ss.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn294;

	-- Regla 3
	-- 
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn31,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Solicitud con diagnostico de alto costo, afiliado del regimen subsidiado'
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sl
	On			sl.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpNotificacionesAfiliado na 
	On			na.cnsctvo_slctd_atrzcn_srvco = sl.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpDiagnosticos d
	On			d.cnsctvo_slctd_atrzcn_srvco = sl.cnsctvo_slctd_atrzcn_srvco
	Where		sl.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco4
	And			na.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo)
	And			d.cnsctvo_cdgo_tpo_mrca_dgnstco = @cnsctvo_cdgo_tpo_mrca_dgnstco;

	-- Regla 4
	-- Afiliados mayor de 18 con discapacidad fisica y/o sensorial pos subsidiado
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn26,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado mayor a 18 a�os con tipo discapacidad fisica o sensorial regimen Subsidiado'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco 
	Inner Join	bdsisalud.dbo.tbMarcasPrestacion mp With(NoLock)
	On			ss.cnsctvo_cdgo_srvco_slctdo = mp.cnsctvo_cdfccn
	Where		@fcha_actl Between mp.inco_vgnca_mrca_prstcn And mp.fn_vgnca_mrca_prstcn
	And			sa.cnsctvo_cdgo_tpo_dscpcdd  In (@cnsctvo_cdgo_tpo_dscpcdd1,@cnsctvo_cdgo_tpo_dscpcdd3) --FISICA,SENSORIAL
	And			mp.cnsctvo_cdgo_tpo_mrca_prstcn = @cnsctvo_cdgo_tpo_mrca_prstcn6 --Prestacion Marcada para Rehabilitacion
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9	
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco4
	And			sa.edd_afldo_ans >= @edd_afldo_ans18
	And			mp.vldo = @vlr_si;

	-- Regla 5
	-- Afiliado menor de 18 con discapacidad fisica o sensorial
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn27,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado menor de 18 a�os con tipo discapacidad fisica o sensorial regimen Subsidiado'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco 
	Where		sa.cnsctvo_cdgo_tpo_dscpcdd  In (@cnsctvo_cdgo_tpo_dscpcdd1,@cnsctvo_cdgo_tpo_dscpcdd3) --FISICA,SENSORIAL
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9
	And			sa.cnsctvo_cdgo_rngo_slrl In (@cnsctvo_cdgo_rngo_slrl5, @cnsctvo_cdgo_rngo_slrl6)	
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5
	And			sa.edd_afldo_ans < @edd_afldo_ans18;
	
	-- Regla 6
	-- Afiliado POS subsidiado menor a un a�o
	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn28 ,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado menor a 1 a�o regimen subsidiado'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempEdadAFechaSolicitud es
	On			es.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Where		sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9
	And			es.edd_fcha_slctd < @edd_afldo_ans1
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5

	-- Regla 7
	-- Afiliado POS contributivo menos de 18 a�os con cancer 
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn29,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado con Cancer menor de 18 a�os regimen contributivo'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpNotificacionesAfiliado na
	On			na.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempEdadAFechaSolicitud es
	On			es.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Where		na.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn13
	And			es.edd_fcha_slctd < @edd_afldo_ans18
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln1
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5

	-- Afiliado POS subsidiado menos de 18 a�os con cancer
	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn =  @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn30,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado con Cancer menor de 18 a�os regimen subsidiado'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tmpNotificacionesAfiliado na
	On			na.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempEdadAFechaSolicitud es
	On			es.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Where		na.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn13
	And			es.edd_fcha_slctd < @edd_afldo_ans18
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9
	And			ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5

	Drop Table #tempEdadAFechaSolicitud

End