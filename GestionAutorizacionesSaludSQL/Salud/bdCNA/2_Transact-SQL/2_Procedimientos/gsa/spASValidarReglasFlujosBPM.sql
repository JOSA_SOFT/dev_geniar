USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarReglasFlujosBPM]    Script Date: 7/10/2017 9:21:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASValidarReglasFlujosBPM
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D 
                         Valida en cupsxplanesxcargo con la prestaciones para verificar flujo en BPM 
					 D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2016   FC\>    
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Rodriguez AM\>    
* Descripcion        : <\DM 
                            Se actualiza la prestación adicionando el auditor al que se remitió el caso  
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 08/03/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se modifica el procedimiento adicionando validacion para los medicamentos, para que
							sean direccionados al grupo 4000017 Auditor Medicamentos
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 23/05/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se modifica el procedimiento la validacion de la jerarquia de los cargos
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 10/07/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------*/
-- EXEC [bdCNA].[gsa].[spASValidarReglasFlujosBPM] 28052
-- EXEC [bdCNA].[gsa].[spASValidarReglasFlujosBPM] 28077

ALTER PROCEDURE [gsa].[spASValidarReglasFlujosBPM]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON

	Declare @vlr_si				     udtLogico     ,
			@vlr_si_1			     udtLogico     ,
			@vlr_dmi			     Char(4)       ,
			@vlr_vco			     udtLogico     ,
			@ld_grpo_adtr_prcso	     udtDescripcion,
			@usuario_proceso	     udtUsuario    ,
			@grpo_adtr_prcso         udtDescripcion,
			@fecha_actual		     Datetime      ,
			@cnsctvo_cdgo_tpo_cdfccn udtConsecutivo,
			@cnsctvo_cdgo_fljo_crgo  udtConsecutivo,
			@nvl_jrrq                udtConsecutivo

	Create 
	Table  #tmpFlujosCargos(grpo_adtr_prcso			udtDescripcion,
		                    cnsctvo_cdgo_fljo_crgo	udtConsecutivo							
	                       )

	Set @vlr_si			         = 'S'       ;
	Set @vlr_si_1		         = '1'       ;
	Set @vlr_dmi		         = 'DOMI'    ;
	Set @vlr_vco		         = ''        ;
	Set @usuario_proceso         = 'BPMAdmin';
	Set @fecha_actual	         = getDate() ;
	Set @cnsctvo_cdgo_fljo_crgo  = 4         ;
	Set @cnsctvo_cdgo_tpo_cdfccn = 5         ;
	Set @grpo_adtr_prcso         = '4000017 Auditor Medicamentos'
	Set @nvl_jrrq                = 1
	
	Insert 
	Into   #tmpFlujosCargos(grpo_adtr_prcso, cnsctvo_cdgo_fljo_crgo)
	Select @vlr_dmi, @vlr_si_1
	From   BDCna.gsa.tbASSolicitudesAutorizacionServicios With(NoLock)
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And    dmi In (@vlr_si_1, @vlr_si)

	If Not Exists(Select 'X' grpo_adtr_prcso From #tmpFlujosCargos)
		Begin
			Insert 
			Into       #tmpFlujosCargos(grpo_adtr_prcso, cnsctvo_cdgo_fljo_crgo)
			Select     fcv.grpo_adtr_prcso, fcv.cnsctvo_cdgo_fljo_crgo
			From       bdSisalud.dbo.tbCupsxPlanxCargo_Vigencias cpc With(NoLock)
		    Inner Join bdSisalud.dbo.tbCargosSOS_Vigencias cv With(NoLock)
			On         cv.cnsctvo_cdgo_crgo_ss = cpc.cnsctvo_cdgo_crgo
			Inner Join BDCna.gsa.tbASServiciosSolicitados ss With(NoLock) 
			On         ss.cnsctvo_cdgo_srvco_slctdo = cpc.cnsctvo_cdfccn
			Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias With(NoLock) 
			On         ias.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco  And 
			           ias.cnsctvo_cdgo_pln           = cpc.cnsctvo_cdgo_pln
			Inner Join bdcna.prm.tbASFlujosCargos_Vigencias fcv With(NoLock) 
			On         fcv.cnsctvo_cdgo_fljo_crgo = cv.cnsctvo_cdgo_fljo_crgo
			Where      ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			And        cv.nvl_jrrq                   = @nvl_jrrq
			And        @fecha_actual Between fcv.inco_vgnca And fcv.fn_vgnca
			And        @fecha_actual Between cv.inco_vgnca  And cv.fn_vgnca
			And        @fecha_actual Between cpc.inco_vgnca And cpc.fn_vgnca			
		End
	
	If Not Exists(Select 'X' grpo_adtr_prcso From #tmpFlujosCargos)
	  Begin
	     Insert 
	     Into   #tmpFlujosCargos(grpo_adtr_prcso, cnsctvo_cdgo_fljo_crgo)
         Select @grpo_adtr_prcso, @cnsctvo_cdgo_fljo_crgo			
         From   BDCna.gsa.tbASServiciosSolicitados ss With(NoLock) 			
         Where  ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		 And    ss.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_cdfccn
	  End
	
	--Se actualiza la prestación adicionando el auditor al que se remitió el caso
	Select   Top 1 @ld_grpo_adtr_prcso = grpo_adtr_prcso 
	From     #tmpFlujosCargos 
	Order by cnsctvo_cdgo_fljo_crgo Asc 	
	
	--Se actualiza en la tabla de los servicio el grupo que debe gestionar la solicitud
	Update bdCNA.gsa.tbASServiciosSolicitados
	Set    rqre_otra_gstn_adtr = @ld_grpo_adtr_prcso,
		   usro_ultma_mdfccn   = @usuario_proceso   ,
		   fcha_ultma_mdfccn   = @fecha_actual
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

	Select  Top 1 cnsctvo_cdgo_fljo_crgo As consecutivo         , 
	              @vlr_vco               As codigo              , 
				  grpo_adtr_prcso        As descripcionSolicitud 
	From     #tmpFlujosCargos
	Order By cnsctvo_cdgo_fljo_crgo Asc
	
	 
	Drop Table #tmpFlujosCargos
END;
