Use bdCNA
Go

If(Object_id('gsa.spAsConsultarCausalesDeNoCobro') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsConsultarCausalesDeNoCobro As Select 1';
End
Go


/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsConsultarCausalesDeNoCobro							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	SP encargado de consultar las causales de no cobro que se mostraran
									en pantalla. D\>  												
* Observaciones				: <\O  O\>  													
* Parametros				: <\P  P\>  													
* Variables					: <\V  V\>  	
* Metdos o PRG Relacionados	:      
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/06/13 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

/**
	Exec bdCNA.gsa.spAsConsultarCausalesDeNoCobro
*/

Alter Procedure gsa.spAsConsultarCausalesDeNoCobro
As
Begin
	Set NoCount On

	Declare @fcha_actl	Date,
			@vlr_si		udtLogico;

	Set @fcha_actl = getDate();
	Set @vlr_si = 'S';
	
	Select	cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,	dscrpcn_csa_no_cbro_cta_rcprcn
	From	bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias With(NoLock)
	Where	@fcha_actl Between inco_vgnca And fn_vgnca
	And		vsble_pntlla = @vlr_si

End