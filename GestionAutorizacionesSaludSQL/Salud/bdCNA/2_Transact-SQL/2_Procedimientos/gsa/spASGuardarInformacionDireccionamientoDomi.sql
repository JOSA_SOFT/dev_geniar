USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionDireccionamientoDomi]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionDireccionamientoDomi
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													 D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones. Se guarda el consecutivo de la
								Solicitud												DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*---------------------------------------------------------------------------------*/
/*
 Declare @cdgo_rsltdo  udtConsecutivo,
	      @mnsje_rsltdo	Varchar(2000)
exec [gsa].[spASGuardarInformacionDireccionamientoDomi]
												'<gestionDomiciliariaDireccionamiento>
													<motivos>
														<codigoMotivo>1</codigoMotivo>
													</motivos>
													<fecha>29/03/2017</fecha>
													<codigoInterno>00017</codigoInterno>
													<justificacion>Esta es una prueba del direccionamiento</justificacion>
													<consecutivoPrestacion>1909</consecutivoPrestacion>
													<usuarioGestion>user_1</usuarioGestion>
												</gestionDomiciliariaDireccionamiento>', @cdgo_rsltdo, @mnsje_rsltdo
	Select @cdgo_rsltdo, @mnsje_rsltdo
	*/
ALTER PROCEDURE [gsa].[spASGuardarInformacionDireccionamientoDomi] @cnsctvs_mtvo_drccnmnto NVARCHAR(MAX), 
@cdgo_rsltdo udtconsecutivo OUTPUT,
@mnsje_rsltdo udtdescripcion OUTPUT
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @xml_cnsctvs_mtvo_drccnmnto	XML,
          @codigointerno				udtCodigoIPS,
          @justificacion				udtobservacion,
          @consecutivoprestacion		udtconsecutivo,
          @usuariogestion				udtusuario,
          @mensaje_ok					udtdescripcion = 'Proceso ejecutado satisfactoriamente',
          @mensaje_error				udtdescripcion = '',
          @codigo_ok					INT = 0,
          @codigo_error					INT = -1,
          @fcha_actl					DATETIME = GETDATE(),
          @vlr_ip_cnstnte				INT,
		  @cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
		  @cdgo_intrno_update			udtcodigoips,
		  @orgn_mdfccn					udtDescripcion,
		  @elmnto_orgn_mdfccn			udtdescripcion,
		  @dto_elmnto_orgn_mdfccn		udtdescripcion,
		  @dscrpcncdgo_intrno_ant		udtdescripcion,
		  @dscrpcn_cdgo_interno			udtdescripcion;



  SELECT
    @mnsje_rsltdo				= @mensaje_ok,
    @cdgo_rsltdo				= @codigo_ok,
    @xml_cnsctvs_mtvo_drccnmnto = CAST(@cnsctvs_mtvo_drccnmnto AS XML),
    @vlr_ip_cnstnte				= 0,
	@orgn_mdfccn				= 'tbASResultadoDireccionamiento.cdgo_intrno',
	@elmnto_orgn_mdfccn			= 'OPS',
	@dto_elmnto_orgn_mdfccn     = 'Cambio Direccionamiento'
	

  DECLARE @motivosdireccionamiento TABLE (
    codigomotivo udtconsecutivo
  );

  CREATE TABLE #tmptrazamodificacion (
    cnsctvo_slctd_atrzcn_srvco		udtconsecutivo,
    cnsctvo_srvco_slctdo			udtconsecutivo,
    cnsctvo_cncpto_prcdmnto_slctdo	udtconsecutivo,
    cnsctvo_cdgo_mtvo_csa			udtconsecutivo,
    fcha_mdfccn						DATETIME,
    orgn_mdfccn						VARCHAR(100),
    vlr_antrr						VARCHAR(20),
    vlr_nvo							VARCHAR(20),
    usro_mdfccn						udtusuario,
    obsrvcns						udtobservacion,
    nmro_ip							VARCHAR(12),
    fcha_crcn						DATETIME,
    usro_crcn						udtusuario,
    fcha_ultma_mdfccn				DATETIME,
    usro_ultma_mdfccn				udtusuario,
	elmnto_orgn_mdfccn				udtDescripcion,
	dto_elmnto_orgn_mdfccn			udtDescripcion,
	dscrpcn_vlr_antrr				udtDescripcion,
	dscrpcn_vlr_nvo					udtDescripcion
  )

  --Insert Into #InformacionDireccionamiento(codigoInterno, justificacion, consecutivoPrestacion, usuarioGestion)
  SELECT
    @codigointerno = pref.value('(codigoInterno/text())[1]', 'udtCodigoIPS'),
    @justificacion = pref.value('(justificacion/text())[1]', 'udtObservacion'),
    @consecutivoprestacion = pref.value('(consecutivoPrestacion/text())[1]', 'udtConsecutivo'),
    @usuariogestion = pref.value('(usuarioGestion/text())[1]', 'udtUsuario')
  FROM @xml_cnsctvs_mtvo_drccnmnto.nodes('/gestionDomiciliariaDireccionamiento') AS xml_slctd (Pref);

  INSERT INTO @motivosdireccionamiento (codigomotivo)
    SELECT
      pref.value('(codigoMotivo/text())[1]', 'udtConsecutivo') AS codigoMotivo
    FROM @xml_cnsctvs_mtvo_drccnmnto.nodes('/gestionDomiciliariaDireccionamiento//motivos') AS xml_slctd (Pref);
  
	Select top 1 @cnsctvo_slctd_atrzcn_srvco = rd.cnsctvo_slctd_atrzcn_srvco,
				 @cdgo_intrno_update		 = rd.cdgo_intrno,
				 @dscrpcncdgo_intrno_ant	 = dp.nmbre_scrsl  
	From  BDCna.gsa.tbASResultadoDireccionamiento rd With(NoLock)
	Inner Join bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
	On dp.cdgo_intrno = rd.cdgo_intrno
	Where rd.cnsctvo_srvco_slctdo = @consecutivoprestacion

	BEGIN TRY
	  IF @cnsctvo_slctd_atrzcn_srvco IS NOT NULL
	  BEGIN
	  
		--Se actualiza la información del prestador.
		UPDATE rd
		SET rd.cdgo_intrno		 = @codigointerno,
			rd.fcha_ultma_mdfccn = @fcha_actl,
			rd.usro_ultma_mdfccn = @usuariogestion
		FROM bdcna.gsa.tbasresultadodireccionamiento rd With(NoLock) 
		WHERE rd.cnsctvo_srvco_slctdo = @consecutivoprestacion
				
	  END
	  ELSE
	  BEGIN
		Set @cnsctvo_slctd_atrzcn_srvco = ( Select top 1 SS.cnsctvo_slctd_atrzcn_srvco from 
										  BDCna.gsa.tbASServiciosSolicitados SS With(NoLock) 
										  Where SS.cnsctvo_srvco_slctdo = @consecutivoprestacion)

		INSERT INTO BDCna.gsa.tbASResultadoDireccionamiento(
			cnsctvo_slctd_atrzcn_srvco,
			cnsctvo_srvco_slctdo,
			cdgo_intrno,
			fcha_crcn,
			usro_crcn,
			fcha_ultma_mdfccn,
			usro_ultma_mdfccn
		)VALUES(
			@cnsctvo_slctd_atrzcn_srvco,
			@consecutivoprestacion,
			@codigointerno,
			@fcha_actl,
			@usuariogestion,
			@fcha_actl,
			@usuariogestion
		)
		Set @cdgo_intrno_update = @codigointerno;

	  END

			
		--Se establece la información de la trazabilidad.
		
		Select @dscrpcn_cdgo_interno = dp.nmbre_scrsl
		From bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock) 
		Where dp.cdgo_intrno = @codigointerno 

		INSERT INTO #tmptrazamodificacion (cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo,
		cnsctvo_cncpto_prcdmnto_slctdo,
		cnsctvo_cdgo_mtvo_csa,
		fcha_mdfccn,
		orgn_mdfccn,
		vlr_antrr,
		vlr_nvo,
		usro_mdfccn,
		obsrvcns,
		nmro_ip,
		fcha_crcn,
		usro_crcn,
		fcha_ultma_mdfccn,
		usro_ultma_mdfccn,
		elmnto_orgn_mdfccn,
		dto_elmnto_orgn_mdfccn,
		dscrpcn_vlr_antrr,
		dscrpcn_vlr_nvo
		)
		  SELECT
			@cnsctvo_slctd_atrzcn_srvco,
			@consecutivoprestacion,
			NULL,
			MDR.codigomotivo,
			@fcha_actl,
			@orgn_mdfccn,
			@cdgo_intrno_update,
			@codigointerno,
			@usuariogestion,
			@justificacion,
			@vlr_ip_cnstnte,
			@fcha_actl,
			@usuariogestion,
			@fcha_actl,
			@usuariogestion,
			@elmnto_orgn_mdfccn,
			@dto_elmnto_orgn_mdfccn,
			@dscrpcncdgo_intrno_ant,
			@dscrpcn_cdgo_interno
		  FROM @motivosdireccionamiento MDR
		  
		--Se invoca el SP de creación de trazabilidad
		
		EXEC bdcna.gsa.spasguardartrazamodificacion
	END TRY
	BEGIN CATCH
		SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
			   				 'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
							 'Message:' + ERROR_MESSAGE() + CHAR(13) +
							 'Procedure:' + ERROR_PROCEDURE();
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error;
	END CATCH
  
  DROP TABLE #tmptrazamodificacion
END

GO
