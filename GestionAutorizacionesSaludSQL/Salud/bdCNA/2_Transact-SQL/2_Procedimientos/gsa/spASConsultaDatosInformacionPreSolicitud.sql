USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionPreSolicitud]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultaDatosInformacionPreSolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultaDatosInformacionPreSolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionPreSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Sp que devolverá la información de la presolicitud D\>
						  <\D para que permita a la pantalla recuperar los datos D\>
						  <\D requeridos y gestionarla a través de transcripción D\>
						  <\D en el proceso BPM.                                 D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/08 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM	Se ajusta SP para que devuelva la la sede del afiliado
								Para mostrarlo en pantalla							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016-11-30FM\>
*---------------------------------------------------------------------------------*/
--EXEC [gsa].[spASConsultaDatosInformacionPreSolicitud]  807, 12345
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionPreSolicitud]  
@cnsctvo_prslctd		udtConsecutivo,
@nmro_instnca_prcso		varchar(11) = null
AS
BEGIN
	SET NOCOUNT ON;

	Declare @ldFechaActual					datetime,
			@cnsctvo_cdgo_mdo_cntcto		udtConsecutivo,
			@sde_afldo						int,
			@dscrpcn_sde					udtDescripcion;

	Set @ldFechaActual				= GetDate();
	Set @cnsctvo_cdgo_mdo_cntcto	= 4;

	Create Table #tmpPresolicitudTranscripcion(
	   cnsctvo_prslctd				udtConsecutivo
      ,nmro_unco_idntfccn_afldo		udtConsecutivo
      ,indcdr_rcn_ncdo				udtLogico
      ,cnsctvo_cdgo_estdo_prslctd   udtConsecutivo
      ,cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo
	  ,cdgo_tpo_idntfccn			Char(3)
      ,nmro_idntfccn				udtNumeroIdentificacionLargo
      ,nmbre_cmplto_afldo			varchar(100)
      ,cnsctvo_cdgo_pln				udtConsecutivo
	  ,dscrpcn_pln					udtDescripcion
      ,obsrvcns						udtObservacion
      ,fcha_crcn					datetime
      ,usro_crcn					udtUsuario
	  ,cnsctvo_cdgo_mdo_cntcto		udtConsecutivo
	  ,dscrpcn_mdo_cntcto			udtDescripcion
	  ,cnsctvo_cdgo_tpo_cntrto		udtConsecutivo
	  ,cdgo_ips_prmra				udtCodigoIps
	)


	if @nmro_instnca_prcso is not null
	Begin
		Update da 
		Set    nmro_prcso_instnca = @nmro_instnca_prcso
		From   BDCna.gsa.tbAsDatosAdicionalesPreSolicitud da With(NoLock)
		Where  cnsctvo_prslctd = @cnsctvo_prslctd
	End

	--Se llena la tabla temporal con los datos de la presolicitud
	Insert Into #tmpPresolicitudTranscripcion(
	   cnsctvo_prslctd				
      ,nmro_unco_idntfccn_afldo		
      ,indcdr_rcn_ncdo				
      ,cnsctvo_cdgo_estdo_prslctd   
	  ,cdgo_tpo_idntfccn
	  ,nmro_idntfccn				
      ,nmbre_cmplto_afldo			
      ,cnsctvo_cdgo_pln				
	  ,obsrvcns						
      ,fcha_crcn					
      ,usro_crcn					
	)
    SELECT 
	   cnsctvo_prslctd
      ,nmro_unco_idntfccn_afldo
      ,indcdr_rcn_ncdo
      ,cnsctvo_cdgo_estdo_prslctd
	  ,cdgo_tpo_dcmnto
      ,nmro_idntfccn
      ,nmbre_cmplto_afldo
      ,cnsctvo_cdgo_pln
      ,obsrvcns
      ,fcha_crcn
      ,usro_crcn
  FROM BDCna.dbo.tbPreSolicitudes	With(NoLock)
  Where cnsctvo_prslctd = @cnsctvo_prslctd


   	--actualiza el codigo del tipo de identificacion del afiliado
	Update     st
	Set		   cnsctvo_cdgo_tpo_idntfccn = t.cnsctvo_cdgo_tpo_idntfccn
	From       #tmpPresolicitudTranscripcion st
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
	On         t.cdgo_tpo_idntfccn = st.cdgo_tpo_idntfccn
	Where      @ldFechaActual Between t.inco_vgnca And t.fn_vgnca

	
	Update	   st
	Set        dscrpcn_pln = p.dscrpcn_pln
	From       #tmpPresolicitudTranscripcion st
	Inner Join bdAfiliacionValidador.dbo.tbPlanes_vigencias	p With(NoLock)
	On	       p.cnsctvo_cdgo_pln = st.cnsctvo_cdgo_pln
	Where      @ldFechaActual Between p.inco_vgnca And p.fn_vgnca



	--actualiza el origen o fuente de la prestación
	Update     st
	Set         cnsctvo_cdgo_mdo_cntcto		=  t.cnsctvo_cdgo_mdo_cntcto
				,dscrpcn_mdo_cntcto			= t.dscrpcn_mdo_cntcto
	From       #tmpPresolicitudTranscripcion st
	Inner Join BDCna.prm.tbASMediosContacto_Vigencias t WITH(NOLOCK)
	On         t.cnsctvo_vgnca_cdgo_mdo_cntcto = @cnsctvo_cdgo_mdo_cntcto
	

	Update     TSO
		Set        cnsctvo_cdgo_tpo_cntrto	= b.cnsctvo_cdgo_tpo_cntrto
				   	           
		From       #tmpPresolicitudTranscripcion				TSO
		INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b	With(NoLock)
		On		   b.nmro_unco_idntfccn_afldo = TSO.nmro_unco_idntfccn_afldo
		INNER JOIN	BDAfiliacionValidador.dbo.tbContratosValidador				c	With(NoLock)	On	b.nmro_cntrto					=   c.nmro_cntrto
																				And	b.cnsctvo_cdgo_tpo_cntrto		=   c.cnsctvo_cdgo_tpo_cntrto
																				And c.cnsctvo_cdgo_pln				=   TSO.cnsctvo_cdgo_pln
		Inner Join	BDAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador	v	With(NoLock)	On	v.cnsctvo_cdgo_tpo_cntrto		=	b.cnsctvo_cdgo_tpo_cntrto
																				And	v.nmro_cntrto					=	b.nmro_cntrto
																				And	v.cnsctvo_bnfcro				=	b.cnsctvo_bnfcro
									
		Where	@ldFechaActual Between v.inco_vgnca_estdo_bnfcro And v.fn_vgnca_estdo_bnfcro
		And		@ldFechaActual Between b.inco_vgnca_bnfcro And b.fn_vgnca_bnfcro
		And		@ldFechaActual Between c.inco_vgnca_cntrto And c.fn_vgnca_cntrto



		Update     TSO
		Set        cdgo_ips_prmra = b.cdgo_intrno
		From       #tmpPresolicitudTranscripcion    TSO
		INNER JOIN BDAfiliacionValidador.dbo.tbBeneficiariosValidador b With(NoLock)
		On     b.nmro_unco_idntfccn_afldo = TSO.nmro_unco_idntfccn_afldo
		INNER JOIN BDAfiliacionValidador.dbo.tbContratosValidador    c With(NoLock) On b.nmro_cntrto     =   c.nmro_cntrto
                    And b.cnsctvo_cdgo_tpo_cntrto  =   c.cnsctvo_cdgo_tpo_cntrto
                    And c.cnsctvo_cdgo_pln    =   TSO.cnsctvo_cdgo_pln
        Where @ldFechaActual Between b.inco_vgnca_bnfcro And b.fn_vgnca_bnfcro
		And  @ldFechaActual Between c.inco_vgnca_cntrto And c.fn_vgnca_cntrto

		--Ajuste Calculo de Sede
		--Se obitene el consecutivo de la sede del afiliado.

		Select     @sde_afldo = i.cnsctvo_cdgo_sde_ips 
		From       #tmpPresolicitudTranscripcion   TSO
		INNER JOIN bdAfiliacionValidador.dbo.tbIpsPrimarias_vigencias	i	With(NoLock)	
		On	       TSO.cdgo_ips_prmra	= i.cdgo_intrno
		and        @ldFechaActual between i.inco_vgnca and i.fn_vgnca
									  
		Select @dscrpcn_sde = dscrpcn_sde 
		FROM   BDAfiliacionValidador.dbo.tbSedes_Vigencias s With(NoLock)
		where  s.cnsctvo_cdgo_sde = @sde_afldo
		and    @ldFechaActual between s.inco_vgnca and s.fn_vgnca
		
  Select
  	   cnsctvo_prslctd
      ,nmro_unco_idntfccn_afldo
      ,indcdr_rcn_ncdo
      ,cnsctvo_cdgo_estdo_prslctd
      ,cnsctvo_cdgo_tpo_idntfccn
      ,nmro_idntfccn
      ,nmbre_cmplto_afldo
      ,cnsctvo_cdgo_pln
      ,obsrvcns
      ,fcha_crcn
      ,usro_crcn
	  ,cdgo_tpo_idntfccn
	  ,dscrpcn_pln
	  ,cnsctvo_cdgo_mdo_cntcto
	  ,dscrpcn_mdo_cntcto
	  --,cnsctvo_cdgo_tpo_cntrto
	  ,LTRIM(RTRIM(cdgo_ips_prmra)) cdgo_ips_prmra
	  ,@dscrpcn_sde dscrpcn_ips_prmra
	  ,@sde_afldo cnsctvo_cdgo_sde_ips
  FROM #tmpPresolicitudTranscripcion

  

  Drop table #tmpPresolicitudTranscripcion

END
GO

GO
