USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudLlenarTemporales]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudLlenarTemporales
* Desarrollado por   : <\A Jois Lombana Sánchez     A\>    
* Descripcion        : <\D Procedimiento expuesto que permite llenar tablas temporales con la información
					 : enviada desde los XML para la modificación de la solicitud(es)  D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 17/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jonathan AM\>    
* Descripcion        : <\D  Se adiciona el parámetro de entrada @infrmcn_vldcion_espcial  D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 01/03/2016 FM\>   
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASModificarSolicitudLlenarTemporales]
	@infrmcn_slctd xml,
	@infrmcn_prstcn xml,
	@infrmcn_prstdr xml,
	@infrmcn_mdco xml,
	@infrmcn_dgnstco xml,
	@infrmcn_hsptlra xml,
	@infrmcn_afldo xml,
	@infrmcn_anxs xml,
	@infrmcn_vldcion_espcial xml
AS
	SET NOCOUNT ON
		-- Traer XML's a tablas SQL Server
		DECLARE @tmpCodigosSolicitud	TABLE(xml_slctd xml NOT NULL);
		DECLARE @tmpCodigosAfiliado		TABLE(xml_Afldo xml NOT NULL);
		DECLARE @tmpCodigosIPS			TABLE(xml_IPS xml NOT NULL);
		DECLARE @tmpCodigosInfHosp		TABLE(xml_InfHosp xml NOT NULL);
		DECLARE @tmpCodigosMed			TABLE(xml_Med xml NOT NULL);
		DECLARE @tmpCodigosDiag			TABLE(xml_Dig xml NOT NULL);	
		DECLARE @tmpCodigosPrestacion	TABLE(xml_Prstc xml NOT NULL);		
		DECLARE @tmpCodigosAnexos		TABLE(xml_Anxs xml NOT NULL);
		DECLARE @tmpCodigosValEspecial  TABLE(xml_ValEsp xml NOT NULL);
		DECLARE @ValorCero				INT = 0
		DECLARE @ValorVacio				CHAR(1) = ''
	BEGIN
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE SOLICITUDES::::::::::::::::::::::::*/
		INSERT INTO		@tmpCodigosSolicitud(xml_slctd) VALUES (@infrmcn_slctd)
		INSERT INTO		#TMod_Solicitudes( 
						 cnsctvo_slctd_atrzcn_srvco_rcbdo	
						,id									
						,cnsctvo_cdgo_orgn_atncn			
						,cnsctvo_cdgo_tpo_srvco_slctdo		
						,cnsctvo_cdgo_prrdd_atncn			
						,cnsctvo_cdgo_tpo_ubccn_pcnte		
						,cnsctvo_cdgo_tpo_slctd				
						,jstfccn_clnca						
						,cnsctvo_cdgo_mdo_cntcto_slctd		
						,cnsctvo_cdgo_tpo_trnsccn_srvco_sld	
						,nmro_slctd_prvdr					
						,nmro_slctd_pdre					
						,cdgo_eps							
						,cnsctvo_cdgo_clse_atncn			
						,cnsctvo_cdgo_frma_atncn			
						,fcha_slctd							
						,nmro_slctd_atrzcn_ss				
						,nmro_slctd_ss_rmplzo				
						,cnsctvo_cdgo_ofcna_atrzcn			
						,nuam								
						,srvco_hsptlzcn						
						,ga_atncn							
						,cnsctvo_prcso_vldcn				
						,cnsctvo_prcso_mgrcn				
						,mgrda_gstn							
						,spra_tpe_st						
						,dmi		
						,obsrvcn_adcnl								
						,usro_crcn							
						,cdgo_orgn_atncn					
						,cdgo_tpo_srvco_slctdo				
						,cdgo_prrdd_atncn					
						,cdgo_tpo_ubccn_pcnte				
						,cdgo_clse_atncn					
						,cdgo_frma_atncn					
						,cdgo_mdo_cntcto_slctd															
						,cdgo_tpo_trnsccn_srvco_sld)	
	SELECT				 Isnull(Convert(int,pref.value('(cnsctvo_slctd_atrzcn_srvco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_orgn_atncn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_srvco_slctdo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_prrdd_atncn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_ubccn_pcnte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_slctd/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(jstfccn_clnca/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_mdo_cntcto_slctd/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_trnsccn_srvco_sld/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(nmro_slctd_prvdr/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(nmro_slctd_pdre/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_eps/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_clse_atncn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_frma_atncn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Convert(datetime,pref.value('(fcha_slctd/text())[1]', 'nvarchar(max)'))
						,Isnull(pref.value('(nmro_slctd_atrzcn_ss/text())[1]', 'nvarchar(max)'),@ValorVacio) 
						,Isnull(pref.value('(nmro_slctd_ss_rmplzo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_ofcna_atrzcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(numeric(18, 0),pref.value('(nuam/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(srvco_hsptlzcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(ga_atncn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_prcso_vldcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_prcso_mgrcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(mgrda_gstn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(spra_tpe_st/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(dmi/text())[1]', 'nvarchar(max)'),@ValorVacio)		
						,Isnull(pref.value('(obsrvcn_adcnl/text())[1]', 'nvarchar(max)'),@ValorVacio)				
						,Isnull(pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)					
						,Isnull(pref.value('(cdgo_orgn_atncn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_srvco_slctdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_prrdd_atncn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_ubccn_pcnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_clse_atncn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_frma_atncn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_mdo_cntcto_slctd/text())[1]', 'nvarchar(max)'),@ValorVacio)						
						,Isnull(pref.value('(cdgo_tpo_trnsccn_srvco_sld/text())[1]', 'nvarchar(max)'),@ValorVacio)								
		FROM			@tmpCodigosSolicitud 
		CROSS APPLY		xml_slctd.nodes('/infrmcn_slctd/tbSolicitudesAutorizacionServicios') AS xml_slctd (Pref);
		/*:::::::::::::::::::ACTUALIZAR EL CONSECUTIVO DE LA SOLICITUD A MODIFICAR EN CASO DE QUE SOLO TRAIGA EL RADICADO:::::::::*/
		IF EXISTS (	SELECT	cnsctvo_slctd_atrzcn_srvco_rcbdo 
					FROM	#TMod_Solicitudes WITH (NOLOCK)
					WHERE	cnsctvo_slctd_atrzcn_srvco_rcbdo Is null
					OR		cnsctvo_slctd_atrzcn_srvco_rcbdo = 0)
		BEGIN 
			UPDATE	#TMod_Solicitudes
			SET		cnsctvo_slctd_atrzcn_srvco_rcbdo = SOL.cnsctvo_slctd_atrzcn_srvco
			FROM	BdCNA.gsa.tbASSolicitudesAutorizacionServicios SOL WITH(NOLOCK)
			WHERE	SOL.nmro_slctd_atrzcn_ss = SOL.nmro_slctd_atrzcn_ss
			AND		(cnsctvo_slctd_atrzcn_srvco_rcbdo Is null
			OR		 cnsctvo_slctd_atrzcn_srvco_rcbdo = 0)
		END
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE AFILIADOS::::::::::::::::::::::::*/
     	INSERT INTO		@tmpCodigosAfiliado(xml_Afldo) VALUES (@infrmcn_afldo) 
		INSERT INTO		#TMod_Afiliados(
						id											
						,cnsctvo_cdgo_tpo_idntfccn_afldo			
						,nmro_idntfccn_afldo						
						,cnsctvo_cdgo_dprtmnto_afldo				
						,cnsctvo_cdgo_cdd_rsdnca_afldo				
						,cnsctvo_cdgo_cbrtra_sld					
						,cnsctvo_cdgo_tpo_pln						
						,cnsctvo_cdgo_pln							
						,cnsctvo_cdgo_estdo_pln						
						,cnsctvo_cdgo_sxo							
						,cnsctvo_cdgo_tpo_vnclcn_afldo				
						,nmro_unco_idntfccn_afldo					
						,cnsctvo_afldo_slctd_orgn					
						,cnsctvo_cdgo_estdo_drcho					
						,cnsctvo_cdgo_tpo_cntrto					
						,nmro_cntrto								
						,cnsctvo_bnfcro_cntrto						
						,cdgo_ips_prmra								
						,cnsctvo_cdgo_sde_ips_prmra					
						,rcn_ncdo									
						,prto_mltple								
						,nmro_hjo_afldo								
						,cnsctvo_cdgo_sxo_rcn_ncdo					
						,ttla										
						,cnsctvo_cdgo_chrte							
						,cnsctvo_cdgo_rngo_slrl						
						,usro_crcn									
						,prmr_aplldo_afldo							
						,sgndo_aplldo_afldo							
						,prmr_nmbre_afldo							
						,sgndo_nmbre_afldo							
						,cdgo_tpo_idntfccn_afldo					
						,fcha_ncmnto_afldo							
						,drccn_afldo								
						,tlfno_afldo								
						,cdgo_dprtmnto_afldo						
						,dscrpcn_dprtmnto_afldo						
						,cdgo_cdd_rsdnca_afldo						
						,dscrpcn_cdd_rsdnca_afldo					
						,tlfno_cllr_afldo							
						,crro_elctrnco_afldo						
						,cdgo_cbrtra_sld							
						,cdgo_pln									
						,cdgo_sxo									
						,cdgo_sxo_rcn_ncdo							
						,cdgo_tpo_pln								
						,cdgo_tpo_vnclcn_afldo						
						,cdgo_estdo_pln								
						,fcha_ncmnto_rcn_ncdo)						
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_idntfccn_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(nmro_idntfccn_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_dprtmnto_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_cdd_rsdnca_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_cbrtra_sld/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(convert(int,pref.value('(cnsctvo_cdgo_tpo_pln/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_pln/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_estdo_pln/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_sxo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_vnclcn_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(nmro_unco_idntfccn_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_afldo_slctd_orgn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_estdo_drcho/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_cntrto/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(nmro_cntrto/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_bnfcro_cntrto/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cdgo_ips_prmra/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_sde_ips_prmra/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(rcn_ncdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(prto_mltple/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(nmro_hjo_afldo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_sxo_rcn_ncdo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(ttla/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_chrte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_rngo_slrl/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(prmr_aplldo_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(sgndo_aplldo_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(prmr_nmbre_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(sgndo_nmbre_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_idntfccn_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)			
						,Convert(datetime,pref.value('(fcha_ncmnto_afldo/text())[1]', 'nvarchar(max)'))
						,Isnull(pref.value('(drccn_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(tlfno_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_dprtmnto_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(dscrpcn_dprtmnto_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_cdd_rsdnca_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)				
						,Isnull(pref.value('(dscrpcn_cdd_rsdnca_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(tlfno_cllr_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio) 
						,Isnull(pref.value('(crro_elctrnco_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_cbrtra_sld/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_pln/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_sxo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_sxo_rcn_ncdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_pln/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_vnclcn_afldo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_estdo_pln/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Convert(Datetime,pref.value('(fcha_ncmnto_rcn_ncdo/text())[1]', 'nvarchar(max)'))					
		FROM			@tmpCodigosAfiliado 
		CROSS APPLY		xml_Afldo.nodes('/infrmcn_afldo/tbInformacionAfiliadoSolicitudAutorizacionServicios') AS xml_Afldo (Pref)
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE IPS::::::::::::::::::::::::*/
		INSERT INTO @tmpCodigosIPS(xml_IPS) VALUES (@infrmcn_prstdr)
		INSERT INTO		#TMod_IPS(
						id											
						,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte		
						,nmro_idntfccn_ips_slctnte					
						,cdgo_intrno								
						,nmro_indctvo_prstdr						
						,cnsctvo_cdgo_dprtmnto_prstdr				
						,cnsctvo_cdgo_cdd_prstdr					
						,cnsctvo_ips_slctd_orgn						
						,adscrto									
						,nmro_unco_idntfccn_prstdr					
						,usro_crcn									
						,nmbre_ips	
						,nmbre_scrsl								
						,cdgo_tpo_idntfccn_ips_slctnte				
						,dgto_vrfccn								
						,cdgo_hbltcn								
						,drccn_prstdr								
						,tlfno_prstdr								
						,cdgo_dprtmnto_prstdr						
						,cdgo_cdd_prstdr)
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_idntfccn_ips_slctnte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(nmro_idntfccn_ips_slctnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_intrno/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(nmro_indctvo_prstdr/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_dprtmnto_prstdr/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(convert(int,pref.value('(cnsctvo_cdgo_cdd_prstdr/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(convert(int,pref.value('(cnsctvo_ips_slctd_orgn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(adscrto/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(nmro_unco_idntfccn_prstdr/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)	
						,Isnull(pref.value('(nmbre_ips/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(nmbre_scrsl/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_idntfccn_ips_slctnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(dgto_vrfccn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cdgo_hbltcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(drccn_prstdr/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(tlfno_prstdr/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_dprtmnto_prstdr/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_cdd_prstdr/text())[1]', 'nvarchar(max)'),@ValorVacio)															
		FROM			@tmpCodigosIPS 
		CROSS APPLY		xml_IPS.nodes('/infrmcn_prstdr/tbIPSSolicitudesAutorizacionServicios') AS xml_IPS (Pref)
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE INFORMACIÓN  HOSPITALARIA::::::::::::::::::::::::*/
		INSERT INTO		@tmpCodigosInfHosp(xml_InfHosp) VALUES (@infrmcn_hsptlra)									
		INSERT INTO		#TMod_Hospitalaria(
						id								
						,ds_estnca						
						,nmro_vsts						
						,fcha_ingrso_hsptlzcn			
						,fcha_egrso_hsptlzcn			
						,cnsctvo_cdgo_clse_hbtcn		
						,cnsctvo_cdgo_srvco_hsptlzcn	
						,cma		
						,crte_cnta					
						,usro_crcn						
						,cdgo_clse_hbtcn				
						,cdgo_srvco_hsptlzcn)
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(ds_estnca/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(nmro_vsts/text())[1]', 'nvarchar(max)')),@ValorCero)
						,convert(datetime,pref.value('(fcha_ingrso_hsptlzcn/text())[1]', 'nvarchar(max)'))
						,Convert(datetime,pref.value('(fcha_egrso_hsptlzcn/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_clse_hbtcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(convert(int,pref.value('(cnsctvo_cdgo_srvco_hsptlzcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cma/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(convert(int,pref.value('(crte_cnta/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_clse_hbtcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_srvco_hsptlzcn/text())[1]', 'nvarchar(max)'),@ValorVacio)				
		FROM			@tmpCodigosInfHosp 
		CROSS APPLY		xml_InfHosp.nodes('/infrmcn_hsptlra/tbInformacionHospitalariaSolicitudAutorizacionServicios') AS xml_InfHosp (Pref)
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE MEDICO TRATANTE::::::::::::::::::::::::*/
		INSERT INTO		@tmpCodigosMed(xml_Med) VALUES (@infrmcn_mdco)
		INSERT INTO		#TMod_Medicos(
						id										
						,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		
						,cnsctvo_cdgo_espcldd_mdco_trtnte		
						,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	
						,nmro_idntfccn_mdco_trtnte				
						,rgstro_mdco_trtnte						
						,cnsctvo_mdco_trtnte_slctd_orgn			
						,adscrto								
						,nmro_unco_idntfccn_mdco				
						,usro_crcn								
						,cdgo_espcldd_mdco_trtnte				
						,cdgo_tpo_idntfccn_mdco_trtnte			
						,prmr_nmbre_mdco_trtnte					
						,sgndo_nmbre_mdco_trtnte				
						,prmr_aplldo_mdco_trtnte				
						,sgndo_aplldo_mdco_trtnte				
						,cdgo_tpo_aflcn_mdco_trtnte)
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_aflcn_mdco_trtnte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_espcldd_mdco_trtnte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(convert(int,pref.value('(cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(nmro_idntfccn_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(rgstro_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_mdco_trtnte_slctd_orgn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(adscrto/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(nmro_unco_idntfccn_mdco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_espcldd_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_idntfccn_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(prmr_nmbre_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(sgndo_nmbre_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(prmr_aplldo_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(sgndo_aplldo_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_aflcn_mdco_trtnte/text())[1]', 'nvarchar(max)'),@ValorVacio)					
		FROM			@tmpCodigosMed 
		CROSS APPLY		xml_Med.nodes('/infrmcn_mdco/tbMedicoTratanteSolicitudAutorizacionServicios') AS xml_Med (Pref)
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE DIAGNOSTICO::::::::::::::::::::::::*/
		INSERT INTO		@tmpCodigosDiag(xml_Dig) VALUES (@infrmcn_dgnstco)
		INSERT INTO		#TMod_Diagnostico(
						id						
						,cnsctvo_cdgo_tpo_dgnstco	
						,cnsctvo_cdgo_dgnstco	
						,cnsctvo_cdgo_cntngnca	
						,cnsctvo_cdgo_rcbro
						,usro_crcn					
						,cdgo_dgnstco				
						,cdgo_tpo_dgnstco
						,cdgo_cntngnca
						,cdgo_rcbro)
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_dgnstco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_dgnstco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_cntngnca/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_rcbro/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)	
						,Isnull(pref.value('(cdgo_dgnstco/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_dgnstco/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_cntngnca/text())[1]', 'nvarchar(max)'),@ValorVacio)	
						,Isnull(pref.value('(cdgo_rcbro/text())[1]', 'nvarchar(max)'),@ValorVacio)			
		FROM			@tmpCodigosDiag 
		CROSS APPLY		xml_Dig.nodes('/infrmcn_dgnstco/tbDiagnosticosSolicitudAutorizacionServicios') AS xml_Dig (Pref)
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE PRESTACIONES::::::::::::::::::::::::*/
		INSERT INTO		@tmpCodigosPrestacion(xml_Prstc) VALUES (@infrmcn_prstcn)
		INSERT INTO		#TMod_Prestaciones(
						id										
						,cnsctvo_cdgo_srvco_slctdo				
						,cnsctvo_cdgo_tpo_srvco					
						,dscrpcn_srvco_slctdo					
						,cntdd_slctda							
						,fcha_prstcn_srvco_slctdo				
						,tmpo_trtmnto_slctdo					
						,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	
						,vlr_lqdcn_srvco						
						,cnsctvo_srvco_slctdo_orgn				
						,cnsctvo_cdgo_tpo_atrzcn				
						,cnsctvo_cdgo_prstcn_prstdr				
						,cnfrmcn_cldd							
						,usro_crcn								
						,cdgo_tpo_srvco							
						,cdgo_srvco_slctdo						
						,cdgo_undd_tmpo_trtmnto_slctdo			
						,cdgo_prstcn_prstdr						
						,cdgo_tpo_atrzcn						
						,dss									
						,cnsctvo_cdgo_prsntcn_dss				
						,prdcdd_dss								
						,cnsctvo_cdgo_undd_prdcdd_dss			
						,cnsctvo_cdgo_frma_frmctca				
						,cnsctvo_cdgo_grpo_trptco				
						,cnsctvo_cdgo_prsntcn					
						,cnsctvo_cdgo_va_admnstrcn_mdcmnto		
						,cnsctvo_mdcmnto_slctdo_orgn			
						,cncntrcn_dss							
						,prsntcn_dss							
						,cnsctvo_cdgo_undd_cncntrcn_dss			
						,cdgo_prsntcn_dss						
						,cdgo_undd_prdcdd_dss					
						,cdgo_frma_frmctca						
						,cdgo_grpo_trptco						
						,cdgo_prsntcn							
						,cdgo_va_admnstrcn_mdcmnto				
						,cdgo_undd_cncntrcn_dss					
						,cnsctvo_cdgo_ltrldd	
						,cnsctvo_cdgo_va_accso				
						,cdgo_ltrldd
						,cdgo_va_accso)
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_srvco_slctdo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_srvco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(dscrpcn_srvco_slctdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cntdd_slctda/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Convert(datetime,pref.value('(fcha_prstcn_srvco_slctdo/text())[1]', 'nvarchar(max)'))
						,Isnull(convert(int,pref.value('(tmpo_trtmnto_slctdo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(vlr_lqdcn_srvco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_srvco_slctdo_orgn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_tpo_atrzcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_prstcn_prstdr/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cnfrmcn_cldd/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_srvco/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_srvco_slctdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_undd_tmpo_trtmnto_slctdo/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_prstcn_prstdr/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_tpo_atrzcn/text())[1]', 'nvarchar(max)'),@ValorVacio)	
						,Isnull(Convert(float,pref.value('(dss/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_prsntcn_dss/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(float,pref.value('(prdcdd_dss/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_undd_prdcdd_dss/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_frma_frmctca/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_grpo_trptco/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_prsntcn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_va_admnstrcn_mdcmnto/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_mdcmnto_slctdo_orgn/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cncntrcn_dss/text())[1]', 'nvarchar(max)'),@ValorCero)
						,Isnull(pref.value('(prsntcn_dss/text())[1]', 'nvarchar(max)'),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_undd_cncntrcn_dss/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(pref.value('(cdgo_prsntcn_dss/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_undd_prdcdd_dss/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_frma_frmctca/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_grpo_trptco/text())[1]', 'nvarchar(max)'),@ValorVacio)							
						,Isnull(pref.value('(cdgo_prsntcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_va_admnstrcn_mdcmnto/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(pref.value('(cdgo_undd_cncntrcn_dss/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_ltrldd/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_va_accso/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(cdgo_ltrldd/text())[1]', 'nvarchar(max)'),@ValorVacio)		
						,Isnull(Pref.value('(cdgo_va_accso/text())[1]', 'nvarchar(max)'),@ValorVacio)
		FROM			@tmpCodigosPrestacion 
		CROSS APPLY		xml_Prstc.nodes('/infrmcn_prstcn/tbServiciosSolicitados') AS xml_Prstc (Pref)
		/*::::::::::::LLENAR TABLA TEMPORAL DE HISTÓRICOS DE PRESTACIONES:::::::::::::*/
		INSERT INTO #tmp_tbASHistoricoEstadosServicioSolicitado(
										cnsctvo_hstrco_estdo_srvco_slctd,
										cnsctvo_srvco_slctdo,
										cnsctvo_cdgo_estdo_srvco_slctdo,
										inco_vgnca,
										fn_vgnca,
										fcha_fn_vldz_rgstro,
										vldo,
										usro_crcn,
										fcha_crcn,
										usro_ultma_mdfccn,
										fcha_ultma_mdfccn,
										id
										)
		SELECT		HES.cnsctvo_hstrco_estdo_srvco_slctd,
					HES.cnsctvo_srvco_slctdo,
					HES.cnsctvo_cdgo_estdo_srvco_slctdo,
					HES.inco_vgnca,
					HES.fn_vgnca,
					HES.fcha_fn_vldz_rgstro,
					HES.vldo,
					HES.usro_crcn,
					HES.fcha_crcn,
					HES.usro_ultma_mdfccn,
					HES.fcha_ultma_mdfccn,
					TSO.id		
		FROM		gsa.tbASHistoricoEstadosServicioSolicitado HES
		INNER JOIN  gsa.tbASServiciosSolicitados SS
		ON			HES.cnsctvo_srvco_slctdo = SS.cnsctvo_srvco_slctdo
		INNER JOIN  #TMod_Solicitudes TSO
		ON			SS.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo;

		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE ANEXOS::::::::::::::::::::::::*/
		INSERT INTO @tmpCodigosAnexos(xml_Anxs) VALUES (@infrmcn_anxs)
		INSERT INTO		#TMod_Anexos(
						id									
						,cnsctvo_cdgo_mdlo_dcmnto_sprte		
						,cnsctvo_cdgo_dcmnto_sprte			
						,usro_crcn)	
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_mdlo_dcmnto_sprte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_dcmnto_sprte/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
		FROM			@tmpCodigosAnexos 
		CROSS APPLY		xml_Anxs.nodes('/infrmcn_anxs/tbCNADocumentosAnexosServicios') AS xml_Anxs (Pref)
		/*::::::::::::::LLENAR TABLA TEMPORAL DE VALIDACIÓN ESPECIAL::::::::::::::::::*/
		INSERT INTO  @tmpCodigosValEspecial(xml_ValEsp) VALUES (@infrmcn_vldcion_espcial)
		INSERT INTO		#TMod_ValidacionEspecial(
						id			
						,nmro_vldcn_espcl
						,cnsctvo_cdgo_ofcna
						,usro_vldcn_espcl
						,accn_vldcn_espcl
						,cnsctvo_ops
						,fcha_vldcn_espcl
						,usro_crcn)	
		SELECT			 Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
						,Isnull(Convert(int,pref.value('(nmro_vldcn_espcl/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_ofcna/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(usro_vldcn_espcl/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Pref.value('(accn_vldcn_espcl/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Convert(int,pref.value('(cnsctvo_ops/text())[1]', 'nvarchar(max)')),@ValorCero)
						,Isnull(Pref.value('(fcha_vldcn_espcl/text())[1]', 'nvarchar(max)'),@ValorVacio)
						,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
		FROM			@tmpCodigosValEspecial 
		CROSS APPLY		xml_ValEsp.nodes('/infrmcn_vldcion_espcial/tbASRegistroValidacionEspecial') AS xml_VE (Pref);
		/*:::::::::::::::::::::LLENAR TABLA TEMPORAL DE PRESTACIONES ACTUALES::::::::::::::::::::::::*/
		INSERT INTO #ServiciosMod(
					 cnsctvo_srvco_slctdo
					,cnsctvo_dto_srvco_slctdo_orgnl					
					,idXML)
		SELECT		 SEO.cnsctvo_srvco_slctdo
					,SEO.cnsctvo_dto_srvco_slctdo_orgnl
					,SOL.id
		FROM		#TMod_Solicitudes							SOL WITH (NOLOCK)
		INNER JOIN 	BdCNA.gsa.tbASServiciosSolicitadosOriginal	SEO	WITH (NOLOCK)
		ON			SOL.cnsctvo_slctd_atrzcn_srvco_rcbdo = SEO.cnsctvo_slctd_atrzcn_srvco;

		--Se realiza la actualización de la secuencia de inserción, para posteriormente determinar el consecutivo para servicios solicitados original.
		WITH tmp AS (
				SELECT  ID	,ID_fila, ROW_NUMBER() OVER ( PARTITION BY ID ORDER BY ID) AS RowNbr
				FROM    #TMod_Prestaciones
			)
		UPDATE  t  
		SET     t.nmro_prstcn = r.RowNbr
		FROM    #TMod_Prestaciones t
		INNER JOIN tmp r ON t.ID_fila = r.ID_fila;

		--Se realiza la actualización de la secuencia de inserción de diagnósticos, para posteriormente determinar el consecutivo.
		WITH tmp AS (
				SELECT  ID	,ID_fila, ROW_NUMBER() OVER ( PARTITION BY ID ORDER BY ID) AS RowNbr
				FROM    #TMod_Diagnostico
			)
		UPDATE  t  
		SET     t.nmro_prstcn = r.RowNbr
		FROM    #TMod_Diagnostico t
		INNER JOIN tmp r ON t.ID_fila = r.ID_fila;
		
	END			


GO
