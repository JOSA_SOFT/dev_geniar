USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASFechaEntrega]    Script Date: 27/06/2017 14:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite la orquestación de la ejecución del servicio de D\>
					   <\D fecha esperada de entrega												 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se realiza modificacion del procedimiento actualizando el tipo de
							dato del campo nmro_idntfccn y nmro_idntfccn_empleador de INT a
							udtNumeroIdentificacionLargo(Varchar(23))
                         DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 27/06/2017  FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASFechaEntrega] 
    @rsltdo_mrcs xml,
	@usro        varchar(50),
	@es_btch     char(1),
	@estdo_ejccn varchar(5) OUTPUT,
	@msje_errr   varchar(2000) OUTPUT,
	@msje_rspsta xml OUTPUT
AS
BEGIN
  SET NOCOUNT ON
    
  DECLARE @cnsctvo_prcso udtConsecutivo,
          @cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo,
          @estdo_prcso_ok numeric(1),
          @negacion char(1),
          @errorSolicitud varchar(100),
          @codigoExito char(2),
          @codigoError char(5)


  --Creación de tablas temporales para la ejecución de la gestión de solicitudes y ejecución de reglas.
  --Temporal con los datos básicos de la solicitud
  CREATE 
  TABLE  #tmpDatosSolicitudFechaEntrega (id                              udtConsecutivo IDENTITY (1,1),
                                         cnsctvo_slctd_srvco_sld_rcbda   udtConsecutivo               ,
                                         cnsctvo_srvco_slctdo            udtConsecutivo               ,
                                         cnsctvo_cdfccn                  udtConsecutivo               ,     
	                                     cnsctvo_cdgo_tpo_srvco          udtConsecutivo               ,
                                         cdgo_cdfccn                     Varchar(30)                  ,   
                                         cnsctvo_pln                     udtConsecutivo               ,
                                         cdgo_pln                        Varchar(30)                  , 
                                         cnsctvo_tpo_pln                 udtConsecutivo               , 
                                         cdgo_tpo_pln                    Varchar(30)                  ,
                                         cnsctvo_mdo_cntcto              udtConsecutivo               ,
                                         cdgo_mdo_cntcto                 Varchar(30)                  ,
                                         cnsctvo_grpo_entrga             udtConsecutivo               ,
                                         cdgo_grpo_entrga                Varchar(30)                  ,
	                                     cnsctvo_cdgo_itm_prspsto        udtConsecutivo               ,
                                         fcha_slctd                      Datetime NOT NULL            , 
                                         fcha_estmda_entrga              Datetime                     ,
                                         nmro_unco_idntfccn_afldo        udtConsecutivo               ,
                                         nmro_idntfccn                   udtNumeroIdentificacionLargo ,
                                         edd_afldo_ans                   Int                          ,
	                                     edd_afldo_mss                   Int                          ,
	                                     edd_afldo_ds                    Int                          ,
                                         nmro_cntrto                     Char(15)                     ,
                                         cnsctvo_cdgo_tpo_cntrto         udtConsecutivo               ,
                                         usro                            udtUsuario                   ,
                                         nmro_slctd_ss                   Varchar(16)                  ,
                                         nmro_slctd_prvdr                Varchar(16)                  ,
                                         cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo               ,
                                         nmro_idntfccn_empleador         udtNumeroIdentificacionLargo ,
										 nmro_unco_idntfccn_empleador    udtConsecutivo               ,
	                                     cnsctvo_grpo_entrga_fnl         udtConsecutivo               ,
	                                     cdgo_grpo_entrga_fnl            Varchar(30)                  ,
	                                     fcha_estmda_entrga_fnl          Datetime
                                        )

  Create NonClustered Index [idx_cnsctvo_grpo_entrga]
  On #tmpDatosSolicitudFechaEntrega ([cnsctvo_grpo_entrga])
  Include ([cnsctvo_slctd_srvco_sld_rcbda], [nmro_cntrto],[cnsctvo_cdgo_tpo_cntrto])

  --Temporal con los datos necesarios de la marca de incapacidad.
  CREATE 
  TABLE  #tmpMarcas (id                            udtConsecutivo IDENTITY (1,1),
                     cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo,
                     nmro_unco_idntfccn_afldo      udtConsecutivo,
                     mrca_incpcdd                  udtLogico
                    )

  SET @cnsctvo_cdgo_tpo_prcso_lna = 19 -- Tipo proceso para procesamiento en Línea de fecha de entrega
  SET @estdo_prcso_ok = 2
  SET @negacion = 'N'
  SET @errorSolicitud = 'Una o más solicitudes no existen.'
  SET @codigoExito = 'OK';
  SET @codigoError = 'ET';

  --Se inserta la información de las marcas.
  INSERT INTO #tmpMarcas (cnsctvo_slctd_srvco_sld_rcbda,
  nmro_unco_idntfccn_afldo,
  mrca_incpcdd)
    SELECT
      pref.value('(cnsctvo_slctd_atrzcn_srvco/text())[1]', 'udtConsecutivo') AS cnsctvo,
      pref.value('(nmro_unco_idntfccn_afldo/text())[1]', 'udtConsecutivo') AS nui,
      pref.value('(mrca_incpcdd/text())[1]', 'char(1)') AS mrca
    FROM @rsltdo_mrcs.nodes('/rsltdo_mrcas//mrca') AS xml_slctd (Pref);

  IF NOT EXISTS (SELECT
      id
    FROM #tmpNmroSlctds so
    WHERE ISNULL(so.cnsctvo_slctd_srvco_sld_rcbda, 0) = 0)
  BEGIN
    IF @es_btch = @negacion
    --Se controla la transacción sólo para poder actualizar el control del proceso.
    BEGIN TRY
      --Ejecución fecha de entrega en línea.

      --Se carga la información.
      EXEC bdCNA.gsa.spASCargarInformacionFechaEntrega @cnsctvo_prcso,
                                                       @usro,
                                                       @es_btch
      --Se ejecutan las reglas de negocio.
      EXEC bdCNA.gsa.spASEjecutarReglasFechaEntrega @cnsctvo_prcso,
                                                    @es_btch

      --Se calcula el resultado de la fecha de entrega con base en las reglas.											
      EXEC bdCNA.gsa.spASCalcularResultadoFechaEntrega @cnsctvo_prcso,
                                                       @es_btch

      --Se Guarda el resultado de la ejecución de las reglas.
      EXEC bdCNA.gsa.spASGuardarResultadoFechaEntrega @cnsctvo_prcso,
                                                      @es_btch

      --Se retorna el resultado de la ejecución de fecha de entrega.
      EXEC bdCNA.gsa.spASRetornarResultadoFechaEntrega @cnsctvo_prcso,
                                                       @es_btch,
                                                       @msje_rspsta OUTPUT

      SET @estdo_ejccn = @codigoExito
    END TRY
    BEGIN CATCH
      SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();
      SET @estdo_ejccn = @codigoError;
      SET @msje_rspsta = NULL
    END CATCH
  END
  ELSE
  BEGIN
    RAISERROR (@errorSolicitud, 16, 2)
  END

  DROP TABLE #tmpDatosSolicitudFechaEntrega

END
