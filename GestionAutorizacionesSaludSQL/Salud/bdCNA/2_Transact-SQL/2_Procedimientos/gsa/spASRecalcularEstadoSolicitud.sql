USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRecalcularEstadoSolicitud]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG         	: spASRecalcularEstadoSolicitud
* Desarrollado por		: <\A Jhon W. Olarte V.									A\>
* Descripcion			: <\D Procedimiento encargado de recalcular el estado	D\>
						  <\D de la solicitud									D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 24-05-2016										FC\>
*
*---------------------------------------------------------------------------------  */
ALTER PROCEDURE [gsa].[spASRecalcularEstadoSolicitud] @id_slctd INT
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @cnsctvo_en_prcso INT,
          @cnsctvo_ingrsda INT,
          @cnsctvo_incnstnca INT,
          @cnsctvo_audtra INT,
          @cnsctvo_dvlta INT,
          @cnsctvo_aprbda INT,
          @cnsctvo_lqdda INT,
          @cnsctvo_prcsda INT,
          @cnsctvo_anlda INT

  SELECT
    @cnsctvo_en_prcso = 3,
    @cnsctvo_ingrsda = 2,
    @cnsctvo_incnstnca = 4,
    @cnsctvo_audtra = 5,
    @cnsctvo_dvlta = 6,
    @cnsctvo_aprbda = 7,
    @cnsctvo_lqdda = 9,
    @cnsctvo_prcsda = 13,
    @cnsctvo_anlda = 14

  --Si al menos una prestación está en estado intermedio
  IF EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASServiciosSolicitados SSO		WITH(NOLOCK)
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    AND cnsctvo_cdgo_estdo_srvco_slctdo IN (@cnsctvo_ingrsda, @cnsctvo_en_prcso, @cnsctvo_incnstnca, @cnsctvo_audtra, @cnsctvo_lqdda))
  BEGIN
    UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
    SET cnsctvo_cdgo_estdo_slctd = @cnsctvo_en_prcso
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    RETURN
  END

  --Si todas las prestaciones son devueltas
  IF NOT EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASServiciosSolicitados SSO			WITH(NOLOCK)
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    AND cnsctvo_cdgo_estdo_srvco_slctdo <> @cnsctvo_dvlta)
  BEGIN
    UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
    SET cnsctvo_cdgo_estdo_slctd = @cnsctvo_dvlta
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    RETURN
  END

  --Si todas las prestaciones son aprobadas
  IF NOT EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASServiciosSolicitados SSO			WITH(NOLOCK)
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    AND cnsctvo_cdgo_estdo_srvco_slctdo <> @cnsctvo_aprbda)
  BEGIN
    UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
    SET cnsctvo_cdgo_estdo_slctd = @cnsctvo_aprbda
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    RETURN
  END

  --Si todas las prestaciones son anuladas
  IF NOT EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASServiciosSolicitados SSO			WITH(NOLOCK)
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    AND cnsctvo_cdgo_estdo_srvco_slctdo <> @cnsctvo_anlda)
  BEGIN
    UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
    SET cnsctvo_cdgo_estdo_slctd = @cnsctvo_anlda
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    RETURN
  END

  --Si todas las prestaciones en estado final y no todas en devuelta, aprobada o anulada
  IF NOT EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASServiciosSolicitados SSO			WITH(NOLOCK)
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    AND cnsctvo_cdgo_estdo_srvco_slctdo NOT IN (@cnsctvo_dvlta, @cnsctvo_aprbda, @cnsctvo_anlda))
  BEGIN
    UPDATE bdCNA.gsa.tbASSolicitudesAutorizacionServicios
    SET cnsctvo_cdgo_estdo_slctd = @cnsctvo_prcsda
    WHERE cnsctvo_slctd_atrzcn_srvco = @id_slctd
    RETURN
  END

END

GO
