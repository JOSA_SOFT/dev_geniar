USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsActualizarIdSolicitudGestionPresolicitudBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spAsActualizarIdSolicitudGestionPresolicitudBPM]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spAsActualizarIdSolicitudGestionPresolicitudBPM] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: [spAsActualizarIdSolicitudGestionPresolicitudBPM]
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Sp que Actualiza la información de la presolicitud D\>
						  <\D para que permita llevar una traza del registro     D\>
						  <\D desde el BPM. Se actualiza el id de la solicitud	 D\>
						  <\D					                                 D\>
* Observaciones			: <\O  								  					 O\>
* Parametros			: <\P \>
* Variables				: <\V  													 V\>
* Fecha Creacion		: <\FC 2016/07/08 										 FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spAsActualizarIdSolicitudGestionPresolicitudBPM] 
@cnsctvo_prslctd					udtConsecutivo,
@cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
@usro_gstn							udtUsuario	
AS
BEGIN
	SET NOCOUNT ON;
	Declare @codigo_ok									int,
			@codigo_error								int,
			@mensaje_ok									udtDescripcion,
			@mensaje_error								varchar(2000),
			@cdgo_rsltdo								udtConsecutivo,
			@mnsje_rsltdo								varchar(2000),
			@fecha_validacion							datetime,
			@nmro_prcso_instnca							udtConsecutivo,
			@fcha_vncmnto_gstn							datetime;

	Set @codigo_error					 = -1;  -- Error SP
	Set @codigo_ok						 =  0;  -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente';
	Set @fecha_validacion				 = GetDate();
    

	BEGIN TRY

		--Se recupera el numero de la instancia y la fecha de vencimiento del proceso desde presolicitudes
		Select 
			@nmro_prcso_instnca		= nmro_prcso_instnca,
			@fcha_vncmnto_gstn		= fcha_vncmnto_gstn
		from  BDCna.gsa.tbASDatosAdicionalesPreSolicitud	WITH (NOLOCK)
		Where cnsctvo_prslctd = @cnsctvo_prslctd

		--Se actualiza el numero de la instancia de proceso y la fecha de vencimiento del proceso en la tabla de solicitudes
		Update SS
		Set		nmro_prcso_instnca	= @nmro_prcso_instnca,
				fcha_vncmnto_gstn	= @fcha_vncmnto_gstn,
				fcha_ultma_mdfccn	= @fecha_validacion,
				usro_ultma_mdfccn	= @usro_gstn
		From BDCna.gsa.tbASSolicitudesAutorizacionServicios SS	WITH (NOLOCK)
		Where SS.cnsctvo_slctd_atrzcn_srvco	= @cnsctvo_slctd_atrzcn_srvco

		--Se actualiza el consecutivo de la solicitud en la traza de la presolicitud.
		Update da Set 
			cnsctvo_slctd_atrzcn_srvco	= @cnsctvo_slctd_atrzcn_srvco,
			fcha_ultma_mdfccn			= @fecha_validacion,
			usro_ultma_mdfccn			= @usro_gstn
		From BDCna.gsa.tbASDatosAdicionalesPreSolicitud da	WITH (NOLOCK)
		Where cnsctvo_prslctd = @cnsctvo_prslctd

	Set @cdgo_rsltdo	=		@codigo_ok
	Set @mnsje_rsltdo	=		@mensaje_ok

	END TRY
	BEGIN CATCH
		SET @mensaje_error = Concat(
									'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
									'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
									'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
									'Procedure:' , ERROR_PROCEDURE()
									);
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

	Select @cdgo_rsltdo cdgo_rsltdo, @mnsje_rsltdo mnsje_rsltdo;
END
GO
