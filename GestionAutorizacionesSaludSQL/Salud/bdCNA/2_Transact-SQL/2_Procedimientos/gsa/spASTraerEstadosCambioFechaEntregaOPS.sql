USE BDCna
GO
/****** Object:  StoredProcedure [gsa].[spASTraerEstadosCambioFechaEntregaOPS]    Script Date: 16/11/2017 12:09:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASTraerEstadosCambioFechaEntregaOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASTraerEstadosCambioFechaEntregaOPS AS SELECT 1;'
END
GO
/*----------------------------------------------------------------------------------------
* Metodo o PRG :     GSA.spASTraerEstadosCambioFechaEntregaOPS
* Desarrollado por : <\A Ing. Germán Pérez - Geniar S.A.S A\>
* Descripcion  :     <\D Este procedimiento permite recuperar la lista de los estados que hay en MEGA D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P @ldFechaActual Fecha a la cual se valida la vigencia del grupo de entrega P\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/09/21 FC\>
* Ejemplo: 
    <\EJ        
        EXEC GSA.spASTraerEstadosCambioFechaEntregaOPS null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASTraerEstadosCambioFechaEntregaOPS
	@ldFechaActual Datetime
AS
BEGIN
	SET NOCOUNT ON;

	Declare @lcMarcaEstadoTransitorio udtLogico

	Set @lcMarcaEstadoTransitorio = 'S'

	If @ldFechaActual Is Null
		Set @ldFechaActual = getDate()

    Select     a.cnsctvo_cdgo_estdo_srvco_slctdo As cnsctvo_cdgo_estdo_mga,
			   a.cdgo_estdo_srvco_slctdo As cdgo_estdo_mga,
			   a.dscrpcn_estdo_srvco_slctdo As dscrpcn_estdo_mga,
			   b.cdgo_estdo_atncn, 
			   b.dscrpcn_estdo_atncn 
	From       prm.tbASEstadosServiciosSolicitados_Vigencias a with(nolock)
	Inner Join bdSisalud.dbo.tbEstadosAtencion_Vigencias b  with(nolock)
	On         a.cnsctvo_cdgo_estdo_hmlgdo_sprs = b.cnsctvo_cdgo_estdo_atncn
	Where      a.mrca_estdo_trnstro = @lcMarcaEstadoTransitorio
	And        @ldFechaActual Between a.inco_vgnca And a.fn_vgnca
	And        @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	Order By   a.cnsctvo_cdgo_estdo_srvco_slctdo
END
