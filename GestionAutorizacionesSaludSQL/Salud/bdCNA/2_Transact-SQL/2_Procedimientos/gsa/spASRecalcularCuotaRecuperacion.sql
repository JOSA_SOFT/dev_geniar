Use bdCNA
Go


If(OBJECT_ID('gsa.spASRecalcularCuotaRecuperacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASRecalcularCuotaRecuperacion As Select 1';
End 
Go

/*
-------------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		:  spASRecalcularCuotaRecuperacion												
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez A\>  
* Descripcion		: <\D  Procedimientos que llama al proceso de anulacion y generacion de cuota. D\>  												
* Observaciones		: <\O  O\>  													
* Parametros		: <\P  P\>  													
* Variables			: <\V  V\>  													
* Fecha Creacion	: <\FC 04/08/2017 FC\>											
*  															
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM  AM\>  													
* Descripcion			: <\DM  DM\>
* Nuevos Parametros	 	: <\PM  PM\>  													
* Nuevas Variables		: <\VM  VM\>  													
* Fecha Modificacion	: <\FM  FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
*/



Alter Procedure gsa.spASRecalcularCuotaRecuperacion
		@usro			udtUsuario
As
Begin
	Set Nocount On

	Declare		@cnsctvo_cdgo_estdo_atrzdo			udtConsecutivo,
				@cnsctvo_cdgo_estdo_lqddo			udtConsecutivo,
				@fcha_actl							Datetime,
				@cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
				@mnsje								udtDescripcion,
				@cdgo								Varchar(2),
				@vlr_nll							udtConsecutivo,
				@cdgo_err							varchar(2) = 'ET',
				@vlr_cro							Int;

	Create table #tmpSolicitudAnulacionCuotasRecuperacion (
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- n�mero de la solicitud
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestaci�n o servicio
		cnsctvo_srvco_slctdo			udtConsecutivo,
		nmro_unco_ops					UdtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
		cnsctvo_mdcmnto_slctdo			udtConsecutivo		  
	)
		

	Set @cnsctvo_cdgo_estdo_atrzdo = 11;
	Set @cnsctvo_cdgo_estdo_lqddo = 9;
	Set @fcha_actl = GETDATE();
	Set @vlr_nll = Null;
	Set @vlr_cro = 0;

	if(Exists (	Select		1
				From		#tempInformacionSolicitud a
				Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
				On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
				And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
				Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo)
	)
	Begin
			
			-- Se optienen las prestaciones a anular cuotas.
			Insert Into #tmpSolicitudAnulacionCuotasRecuperacion (
						cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,		cnsctvo_srvco_slctdo
			)
			Select		a.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_cdgo_srvco_slctdo,	ss.cnsctvo_srvco_slctdo
			From		#tempInformacionSolicitud a
			Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo	
			
			Update		a
			Set			cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
			From		#tmpSolicitudAnulacionCuotasRecuperacion a
			Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados b With(NoLock)
			On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			Update		a
			Set			cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
			From		#tmpSolicitudAnulacionCuotasRecuperacion a
			Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados b With(NoLock)
			On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			-- Se anulan las cuotas de recuperacion.
			Exec bdCNA.gsa.spASEjecutarAnulacionCuotasRecuperacion @usro, @mnsje, @cdgo, @vlr_nll, @vlr_nll, @vlr_nll;

			-- Valida el estado de ejecucion de la anulacion de cuotas y lanza el error si este falla.
			If(@cdgo = @cdgo_err)
			Begin
				RaisError (@mnsje, 16, 2) With SetError
			End

			-- Se actualizan los estados para calcular las cuotas de recuperacion.
			Update		ss
			Set			cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro
			From		#tmpSolicitudAnulacionCuotasRecuperacion a
			Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
			On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			And			ss.cnsctvo_cdgo_srvco_slctdo = a.cnsctvo_cdgo_srvco_slctdo
			Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo

			Update		css
			Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro
			From		#tmpSolicitudAnulacionCuotasRecuperacion a
			Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
			Where		css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo

			Update		css
			Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro
			From		#tmpSolicitudAnulacionCuotasRecuperacion a
			Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
			On			css.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
			Where		css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo

			-- Se recupera el consecutivo de la solicitud
			Select Top 1 @cnsctvo_slctd_atrzcn_srvco = cnsctvo_slctd_atrzcn_srvco
			From #tmpSolicitudAnulacionCuotasRecuperacion

			-- Se calculan las cuotas de recuperacion.
			Exec bdCNA.gsa.spASEjecutarCalculoCuotasRecuperacion @cnsctvo_slctd_atrzcn_srvco, @usro,@mnsje, @cdgo
			
			-- Valida el estado de ejecucion del calculo de cuota y lanza el error si este falla.
			If(@cdgo = @cdgo_err)
			Begin
				RaisError (@mnsje, 16, 2) With SetError
			End
					
	End

	Drop Table #tmpSolicitudAnulacionCuotasRecuperacion;

End