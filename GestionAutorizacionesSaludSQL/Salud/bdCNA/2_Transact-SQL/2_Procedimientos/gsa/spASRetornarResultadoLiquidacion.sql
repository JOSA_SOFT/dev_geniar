USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRetornarResultadoLiquidacion]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASRetornarResultadoLiquidacion
* Desarrollado por		: <\A Ing. Jhon Olarte V. - SETI SAS	  				A\>
* Descripcion			: <\D Sp que permite retornar el resultado				D\>
						  <\D del proceso de liquidación						D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/03/22										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASRetornarResultadoLiquidacion] 
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @ms_solicitudsinconceptos			VARCHAR(50)		= 'Servicio no tiene conceptos asociados',
          @ms_liquido						VARCHAR(50)		= 'Concepto servicio liquidado',
          @ms_noliquido						VARCHAR(50)		= 'Concepto servicio no liquidado',
		  @vlr_cro							INT				= 0;
          
  --Inserta los registros a devolver por el SP en la tabla temporal
  INSERT INTO #tmpresultadoliquidacion (
		cnsctvo_srvco_lqdr, 
		rsltdo_lqdcn, 
		mnsj_lqdcn,
		vlr_lqdcn, 
		cnsctvo_cdgo_cncpto_gsto, 
		vlr_lqdcn_cncpto,
		rsltdo_lqdcnc, 
		dscrpcn_cncpto_gsto)
    SELECT
		CGT.cnsctvo_slctd_atrzcn_srvco,
		CGT.rsltdo_lqdcn,
		CASE CGT.rsltdo_lqdcn
			WHEN 1 THEN @ms_liquido
			ELSE @ms_noliquido
		END,
		CGT.vlr_lqdcn,
		CGT.cnsctvo_cdgo_cncpto_gsto,
		CGT.vlr_lqdcn_cncpto,
		CGT.rsltdo_lqdcnc,
		LTRIM(RTRIM(CV.dscrpcn_cncpto_gsto))
    FROM			#tbConceptoGastosTmp_1 CGT
    INNER JOIN		bdsisalud.dbo.tbconceptosgasto_vigencias CV WITH (NOLOCK)
    ON				CV.cnsctvo_cdgo_cncpto_gsto = CGT.cnsctvo_cdgo_cncpto_gsto
	
  UPDATE	#tmpresultadoliquidacion 
  SET		mnsj_lqdcn=@ms_solicitudsinconceptos
  WHERE		cnsctvo_cdgo_cncpto_gsto = @vlr_cro;

  SELECT	cnsctvo_srvco_lqdr,
			rsltdo_lqdcn,
			mnsj_lqdcn,
			vlr_lqdcn,
			cnsctvo_cdgo_cncpto_gsto,
			vlr_lqdcn_cncpto,
			rsltdo_lqdcnc,
			dscrpcn_cncpto_gsto
   FROM		#tmpresultadoliquidacion
   ORDER BY cnsctvo_srvco_lqdr;

END

GO
