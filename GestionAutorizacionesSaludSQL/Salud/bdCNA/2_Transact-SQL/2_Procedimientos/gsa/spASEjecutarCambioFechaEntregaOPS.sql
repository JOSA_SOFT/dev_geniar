USE BDCna
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarCambioFechaEntregaOPS]    Script Date: 16/11/2017 08:32:29 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('gsa.spASEjecutarCambioFechaEntregaOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASEjecutarCambioFechaEntregaOPS AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG :     GSA.spASEjecutarCambioFechaEntregaOPS
* Desarrollado por : <\A Jos� Olmedo Sogo Aguirre - Geniar S.A.S A\>
* Descripcion  :     <\D Actualizar las fechas de entregea de las ops en Sipres y MEGA D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P @fechaModificar		Nueva fecha de entrega P\>					
					 <\P @usro_crcn Usuarios de creacionP\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/10/17 FC\>
* Ejemplo: 
    <\EJ        
        EXEC GSA.spASEjecutarCambioFechaEntregaOPS null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASEjecutarCambioFechaEntregaOPS
	@fcha_mdfcr Datetime,
	@usro_crcn udtUsuario
AS
BEGIN
	SET NOCOUNT ON;

	Declare 			
	@fcha_actl						datetime,
	@cnsctvo_srvco_slctdo			udtConsecutivo,
	@obsrvcn						udtDescripcion,						
	@nrm_incrmtno					int,			
	@N								udtLogico,
	@S								udtLogico,
	@cnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo,
	@orgn_mdfccn					varchar(44),			
	@cro							int,
	@elmnto_orgn_mdfccn				varchar(10),
	@dto_elmnto_orgn_mdfccn			varchar(20),
	@cnsctvo_cdgo_csa_nvdd			udtConsecutivo


	Set @fcha_actl							= getdate();
	Set @obsrvcn							= 'Cambio masivo de fecha de entrega';
	Set @nrm_incrmtno						= 1;
	Set @N									='N'
	Set @S									= 'S'
	Set @cnsctvo_cncpto_prcdmnto_slctdo		= 37
	Set @orgn_mdfccn						= 'tbASResultadoFechaEntrega.fcha_estmda_entrga'
	Set @cro								= 0
	Set @elmnto_orgn_mdfccn					= 'Prestacion'
	Set @dto_elmnto_orgn_mdfccn				= 'Cambio fecha entrega'
	Set @cnsctvo_cdgo_csa_nvdd				= 12

	UPDATE 	a
		SET cnsctvo_atncn_ops = b.cnsctvo_atncn_ops
	from bdcna.gsa.tbTmpAsOPSConsultadas a with(nolock)
	inner join bdsisalud.dbo.tbatencionops b with(nolock)
	on b.cnsctvo_cdgo_ofcna =a.cnsctvo_cdgo_ofcna and b.nuam = a.nuam
	where a.rclclr = @S

	UPDATE 	a
		SET cnsctvo_slctd_mga=	c.cnsctvo_slctd_mga, 
			fcha_ultma_mdfccn =d.fcha_ultma_mdfccn
	from bdcna.gsa.tbTmpAsOPSConsultadas a with(nolock)
	inner join  bdSisalud.dbo.tbASDatosAdicionalesMegaSipres c with(nolock) 		
	on a.cnsctvo_cdgo_ofcna =c.cnsctvo_cdgo_ofcna
		and a.nuam = c.nuam
		and a.cnsctvo_prstcn = c.cnsctvo_prstcn
	inner join  BDCna.gsa.tbASResultadoFechaEntrega d with(nolock)
	on d.cnsctvo_slctd_atrzcn_srvco =c.cnsctvo_slctd_mga
	where a.rclclr = @S
	

	Insert into bdSisalud.dbo.tbRegistrarCambioFechaEntrega 
				(cnsctvo_atncn_ops,		fcha_estmda_entrga_fnl,		obsrvcns,
				fcha_crcn,	usro_crcn,	fcha_ultma_mdfccn,		usro_ultma_mdfccn)
	Select		cnsctvo_atncn_ops,	@fcha_mdfcr,	@obsrvcn,
				@fcha_actl,		@usro_crcn,			@fcha_actl,		@usro_crcn
	From bdcna.gsa.tbTmpAsOPSConsultadas with(nolock)
	Where cnsctvo_atncn_ops is not null and rclclr = @S

	Update a
		Set fcha_estmda_entrga_fnl = @fcha_mdfcr,
			usro_ultma_mdfccn = @usro_crcn
	From bdsisalud.dbo.tbprocedimientos a with(nolock)
	inner join bdcna.gsa.tbTmpAsOPSConsultadas b with(nolock)
	on	a.nuam = b.nuam 
		and a.cnsctvo_prstcn= b.cnsctvo_prstcn 
		and a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	Where b.rclclr = @S and b.cnsctvo_atncn_ops is not null
		  
	Update a
		Set fcha_ultma_mdfccn = @fcha_actl
	From bdsisalud.dbo.tbatencionops a with(nolock)
	Inner join  bdcna.gsa.tbTmpAsOPSConsultadas b  with(nolock)
	On a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
		and a.nuam = b.nuam
	Where b.rclclr = @S and b.cnsctvo_atncn_ops is not null
		
	Update a
	Set fcha_estmda_entrga =@fcha_mdfcr ,
		fcha_ultma_mdfccn =@fcha_actl ,
		usro_ultma_mdfccn =@usro_crcn
	From BDCna.gsa.tbASResultadoFechaEntrega a with(nolock)
	Inner join  bdcna.gsa.tbTmpAsOPSConsultadas b  with(nolock)
	On cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_mga
	where b.rclclr = @S and b.cnsctvo_slctd_mga is not null
	
	Select @cnsctvo_srvco_slctdo = (Max(cnsctvo_srvco_slctdo)+@nrm_incrmtno)
	From BDCna.gsa.tbASResultadoFechaEntrega with(nolock)
					
	Insert into BDCna.gsa.tbASResultadoFechaEntrega 
				(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo ,cnsctvo_cdgo_grpo_entrga, 
				fcha_estmda_entrga,  prrdd_admnstrtva,	fcha_crcn,	usro_crcn, 
				fcha_ultma_mdfccn,	usro_ultma_mdfccn)
	Select cnsctvo_slctd_mga,	@cnsctvo_srvco_slctdo,	cnsctvo_cdgo_grpo_entrga,
			@fcha_mdfcr,	@N,	@fcha_actl,	@usro_crcn,
			@fcha_actl,		@usro_crcn	
	From  bdcna.gsa.tbTmpAsOPSConsultadas with(nolock) 
	where rclclr = @S and fcha_ultma_mdfccn is not null and cnsctvo_slctd_mga is not null
			
	--Se inserta la traza del cambio.

	Insert into BDCna.gsa.tbASTrazaModificacion (cnsctvo_cncpto_prcdmnto_slctdo ,cnsctvo_cdgo_csa_nvdd ,fcha_mdfccn ,
													orgn_mdfccn ,vlr_antrr ,vlr_nvo ,obsrvcns ,nmro_ip ,fcha_crcn ,
													usro_crcn ,fcha_ultma_mdfccn ,usro_ultma_mdfccn ,elmnto_orgn_mdfccn ,
													dto_elmnto_orgn_mdfccn ,dscrpcn_vlr_antrr ,dscrpcn_vlr_nvo )
	Select @cnsctvo_cncpto_prcdmnto_slctdo,	 @cnsctvo_cdgo_csa_nvdd,	@fcha_actl,
		   @orgn_mdfccn,	@fcha_actl,	@fcha_mdfcr,	@obsrvcn	,@cro,	@fcha_actl,
		   @usro_crcn,	@fcha_actl,	@usro_crcn,	@elmnto_orgn_mdfccn,	@dto_elmnto_orgn_mdfccn,
		   @fcha_actl,	@fcha_mdfcr
	From  bdcna.gsa.tbTmpAsOPSConsultadas with(nolock) 
	where rclclr = @S and fcha_ultma_mdfccn is not null	 and cnsctvo_slctd_mga is not null

END

