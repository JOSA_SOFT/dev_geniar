USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarRecobrosAuditoria]    Script Date: 12/05/2017 14:00:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASConsultarRecobrosAuditoria') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASConsultarRecobrosAuditoria AS SELECT 1;'
END
GO

/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarRecobrosAuditoria 
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos      A\>    
* Descripcion        : <\D 
                            Procedimiento que permite realizar el cambio de direccionamiento del servicio y la reliquidacion
							del mismo  
						D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 15/05/2017 FC\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\DM DM\>
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*------------------------------------------------------------------------------------------------------------------------------*/

--exec [gsa].[spASConsultarRecobrosAuditoria] 134009
ALTER PROCEDURE [gsa].[spASConsultarRecobrosAuditoria] 
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo	
AS
Begin
   SET NOCOUNT ON 

   Declare @cnsctvo_cdgo_cntngnca udtConsecutivo,
           @cnsctvo_cdgo_dgnstco  udtConsecutivo,
		   @fecha_actual          Datetime      ,
		   @vsble_usro            udtLogico     ,
		   @cnsctvo_cdgo_mdlo     udtConsecutivo

   Create 
   Table  #tmpRelacionContingenciaRecobro(cnsctvo_cdgo_rcbro    udtConsecutivo,
                                          dscrpcn_rcbro         udtDescripcion,
										  cdgo_rcbro            Char(3)
                                         )

   Set @fecha_actual      = getDate()
   Set @vsble_usro        = 'S'
   Set @cnsctvo_cdgo_mdlo = 31

   Select @cnsctvo_cdgo_cntngnca = cnsctvo_cdgo_cntngnca, 
          @cnsctvo_cdgo_dgnstco  = cnsctvo_cdgo_dgnstco 
   From   bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios With(NoLock)
   Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

   If @cnsctvo_cdgo_cntngnca = 0
      Begin
         Select @cnsctvo_cdgo_cntngnca = cnsctvo_cdgo_cntngnca 
         From   BDSisalud.dbo.tbDiagnosticos_Vigencias 
         Where  cnsctvo_cdgo_dgnstco = @cnsctvo_cdgo_dgnstco
      End

   Insert
   Into        #tmpRelacionContingenciaRecobro(cnsctvo_cdgo_rcbro, cdgo_rcbro, dscrpcn_rcbro)
   Select      b.cnsctvo_cdgo_clsfccn_evnto, b.cdgo_clsfccn_evnto, b.dscrpcn_clsfccn_evnto
   From        bdSiSalud.dbo.tbClasificacionEventosNotificacion_vigencias b  With(NoLock)
   Inner Join  bdSiSalud.dbo.tbMNClasificacionxTipoEvento_vigencias a With(NoLock)
   On          a.cnsctvo_cdgo_clsfccn_evnto = b.cnsctvo_cdgo_clsfccn_evnto          
   Inner Join  bdSiSalud.dbo.TbClasificacionEventoNotificacionxModulo c  With(NoLock)
   On          c.cnsctvo_cdgo_clsfccn_evnto = b.cnsctvo_cdgo_clsfccn_evnto  
   Where       @fecha_actual Between b.inco_vgnca And b.fn_vgnca
   And         @fecha_actual Between a.inco_vgnca And a.fn_vgnca
   And         b.vsble_usro            = @vsble_usro  
   And         a.cnsctvo_cdgo_cntngnca = @cnsctvo_cdgo_cntngnca  
   And         c.cnsctvo_cdgo_mdlo     = @cnsctvo_cdgo_mdlo -- Eventos que aplican solo para sipres 
   Order By    b.mrca_rcbro Desc 

   Select  dscrpcn_rcbro, cnsctvo_cdgo_rcbro, cdgo_rcbro
   From    #tmpRelacionContingenciaRecobro

End