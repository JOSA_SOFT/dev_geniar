USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarAnulacionSolicitudPrestacion]    Script Date: 10/4/2017 8:07:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

If(Object_id('gsa.spASEjecutarAnulacionSolicitudPrestacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASEjecutarAnulacionSolicitudPrestacion As Select 1';
End
Go



/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEjecutarAnulacionSolicitudPrestacion
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento principal de la anulacion en mega y sipres. D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 02/10/2017  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/**
Declare @xml				varchar(Max),
		@cdgo_rsltdo		udtConsecutivo,
		@mnsje_rsltdo		Varchar(Max);

Set @xml = 
	'<anular>
		<solicitud>
			<numSolicitud>98605</numSolicitud>
			<causal>57</causal>
			<observacion>xxxxxxxxxxxxxxxxxxxxxxxx</observacion>
			<usuario>user19</usuario>
			<origenModificacion>Solicitud</origenModificacion>
		</solicitud>
		<prestaciones>
			<prestacion>
				<numSolicitud>98605</numSolicitud>
				<consecutivo>98002</consecutivo>
			</prestacion>
		</prestaciones>
	</anular>'

	Exec bdCNA.gsa.spASEjecutarAnulacionSolicitudPrestacion @xml, @cdgo_rsltdo Output, @mnsje_rsltdo Output;

	Select @cdgo_rsltdo, @mnsje_rsltdo;
*/


ALTER Procedure [gsa].[spASEjecutarAnulacionSolicitudPrestacion]
		@xml_slctd_srvcs_anlr		Varchar(Max),		
		@cdgo_rsltdo			    udtConsecutivo  Output,
		@mnsje_rsltdo			    Varchar(Max)	Output
		
As
Begin

	Set Nocount On

	Declare @dts_anlcn_xml		XML,
			@cdgo_errr			udtConsecutivo,
			@cdgo_exto			udtConsecutivo,			
			@cdgo_mnsje			Char(2),
			@mnsje				udtDescripcion,
			@usro				udtUsuario,
			@cdgo_mnsje_errr	Char(2),
			@vlr_nll			udtConsecutivo,
			@mnsje_exto			udtDescripcion;

	--
	Create Table #tempInformacionSolicitud (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_cdgo_estdo_slctd		udtConsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtCOnsecutivo,
		obsrvcn							udtObservacion,
		usro_anlcn						udtUsuario,
		orgn_mdfccn						udtDescripcion		
	)

	Create Table #tempAtenciones (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_cdgo_ofcna				udtConsecutivo,
		nuam							udtConsecutivo,
		cnsctvo_cdgo_estdo				udtConsecutivo
	)

	--
	Create Table #tempInformacionServicios (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
		cnsctvo_mdcmnto_slctdo			udtConsecutivo,
		cnsctvo_cdgo_estdo_mga_antrr	udtConsecutivo,
		cnsctvo_cdgo_estdo_mga			udtConsecutivo,
		cnsctvo_cdgo_estdo_sprs			udtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo		udtConsecutivo,
		cnsctvo_cdgo_ofcna				udtConsecutivo,
		nuam							udtCOnsecutivo
	)

	--	
	Create Table #tmpSolicitudAnulacionCuotasRecuperacion (
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- número de la solicitud
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestación o servicio
		nmro_unco_ops					UdtConsecutivo  
	)

	--
	Create Table #tmpTrazaModificacion (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo	udtConsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
		fcha_mdfccn						datetime,
		orgn_mdfccn						varchar(100),
		vlr_antrr						varchar(20),
		vlr_nvo							varchar(20),
		usro_mdfccn						udtUsuario,
		obsrvcns						udtObservacion,
		nmro_ip							varchar(12),
		fcha_crcn						datetime,
		usro_crcn						udtUsuario,
		fcha_ultma_mdfccn				datetime,
		usro_ultma_mdfccn				udtUsuario,
		elmnto_orgn_mdfccn				udtDescripcion,
		dto_elmnto_orgn_mdfccn			udtDescripcion,
		dscrpcn_vlr_antrr				udtDescripcion,
		dscrpcn_vlr_nvo					udtDescripcion
	)

	Set @dts_anlcn_xml = Convert(XML, @xml_slctd_srvcs_anlr);
	Set @cdgo_errr = -1;
	Set @cdgo_exto = 1;	
	Set @cdgo_mnsje_errr = 'ET';
	Set @vlr_nll = Null;
	Set	@mnsje_exto = 'Anulacion completada exitosamente';

	Begin Try
		
		-- Se obtienen los datos del XML
		Exec gsa.spASObtenerDatosAnulacion @dts_anlcn_xml;
			
		-- Se obtienen la informacion adicional necesaria de las tablas fisicas
		Exec gsa.spASObtenerDatosAdicionalesAnulacion;
		
		--
		Select @usro = usro_anlcn
		From #tempInformacionSolicitud;

		--
		Exec cja.spRCGenerarNotaCreditoAnulacion @usro;

		-- 
		Exec bdCNA.gsa.spASEjecutarAnulacionCuotasRecuperacion @usro,@mnsje, @cdgo_mnsje, @vlr_nll, @vlr_nll, @vlr_nll;

		If(@cdgo_mnsje = @cdgo_mnsje_errr)
		Begin
			RaisError(@mnsje, 1, 16) With SetError; 
		End
		
		-- Se realiza la anulacion en Mega
		Exec gsa.spASAnularSolicitudPrestacionMega;

		-- Se realizan la anulacion en Sipres
		Exec gsa.spASAnularAtencionProcedimientoSipres		;
		
		-- Se calculan los estados de la solicitud y las atenciones
		Exec gsa.spASAnularSolicitudAtencion;

		-- 
		Exec gsa.spASDesasociarHospitalizacion;

		-- 
		Exec gsa.spASDesasociarProgramacionEntrega;

		-- Se guarda la traza de modificacion.
		Exec gsa.spASCrearTrazaModificacionAnulacion;
		 
		Set @cdgo_rsltdo = @cdgo_exto;
		Set @mnsje_rsltdo = @mnsje_exto;

	End Try
	Begin Catch
		Set @mnsje_rsltdo = Concat(ERROR_MESSAGE(), ' ', ERROR_PROCEDURE(), ' ', ERROR_LINE());
		Set @cdgo_rsltdo = @cdgo_errr;
		Raiserror(@mnsje_rsltdo, 1, 16)With Seterror
	End Catch
	
	Drop Table #tempInformacionSolicitud;
	Drop Table #tempAtenciones;
	Drop Table #tempInformacionServicios;
	Drop Table #tmpSolicitudAnulacionCuotasRecuperacion;
	Drop Table #tmpTrazaModificacion;

End