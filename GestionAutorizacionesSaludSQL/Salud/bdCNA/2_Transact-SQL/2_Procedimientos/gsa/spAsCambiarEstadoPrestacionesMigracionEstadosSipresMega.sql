USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega]    Script Date: 24/05/2017 08:41:30 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Cambia el estado de las prestaciones al equivalente al estado en Sipres, anula las 
									cuotas de recuperacion para los estados anulado y vencido por no tramite
									y guarda el historico de cambios en Mega.
							  D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  Entrada:
								   1- @usro_sstma: usuario del sistema
							  P\>  													
* Variables					: <\V  1- @fcha_actl: Fecha Actual obtenida del sistema.
								   2- @fin_vigencia: Fecha de fin de vigencia utilizada para el historico de 
												     cambio de estado de conceptos
								   3- @vldo_si: Valor por defecto para el cambio de estado que indica la valides del registro
								   4- @cnsctvo_cdgo_mdlo_cra46: codigo que crea mega
								   5- @vlr_cro: Almacena el valor 0
								   6- @cnsctvo_cdgo_estdo8: Codigo de estado que indica que la ops esta impresa en Sipres
								   7- @cnsctvo_cdgo_estdo3: Codigo Sipres que indica que la ops esta anulada.
								   8- @cnsctvo_cdgo_estdo11: Codigo sipres que indica que la ops esta vencida.
								   9- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15: Codigo Mega que indica que la ops esta entregada
								   10- @vlr_uno: Almacena el valor 1
								   11- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14: Codigo Mega que indica que la Ops fue anulada
								   12- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10: Codigo Mega que indica que la ops esta vencida.
								   13- @cdgo_exto: Codigo que indica que el proceso termino de forma exitosa.
								   14- @cdgo_errr: Codigo que indica que el proceso termino con errores.
								   15- @mnsge_exto: mensaje que informa la terminacion correcta del proceso.
								   16- @mnsge_errr: Mensaje que informa que el proceso termino con errores
								   17- @mnmo: Variable utilizada para ciclo de anulacion de cuotas de recuperacion.
								   18- @mxmo: Variable utilizada para ciclo de anulacion de cuotas de recuperacion.
								   19- @nmro_unco_ops: Utilizada en ciclo de anulacin de cuotas de recuperacion, guarda los numero de 
													   ops que se les anulara cuota.
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.spAsMigracionEstadosSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se modifica referencia de tabla de prestaciones DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/02 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se generaliza la actualizacion de los estados y se agrega actualizacion del campo 
							   vlr_lqdcn_srvco en la tabla tbAsServiciosSolicitados DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/10 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega validacion para evitar la anulacion de cuotas de recuperacion
							   de las prestaciones en estado cobrado. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/10 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se update a la fecha de impresion DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/15 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega merge para guardar o actualizar la informacion en la tabla de trazabilidad de
							   migracion, tbASDAtosAdicionalesSipresMega DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/04 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se retiran llamados a procedimientos spASEjecutarAnulacionCuotasRecuperacion y
							   spAsCambiarEstadoConceptosMigracionEstadosSipresMega. Se retira el Try-Catch
							   para hacer el manejo del error desde el sp orquestador. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/09 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega clasula Group By en el Merge DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/


ALTER Procedure [gsa].[spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega]
			@usro_sstma									UdtUsuario
As
Begin
	Set NoCount On

	Declare	@fcha_actl									Date,
			@fin_vigencia								Date,
			@vldo_si									Int,
			@cnsctvo_cdgo_mdlo_cra46					UdtConsecutivo,
			@vlr_cro									Int,
			@cnsctvo_cdgo_estdo7						UdtConsecutivo,
			@cnsctvo_cdgo_estdo8						UdtConsecutivo,
			@cnsctvo_cdgo_estdo3						UdtConsecutivo,
			@cnsctvo_cdgo_estdo11						UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15	UdtConsecutivo,
			@vlr_uno									Int,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10	UdtConsecutivo,
			@mnmo										Int,
			@mxmo										Int,
			@nmro_unco_ops								UdtConsecutivo;
			
			
	--Create Table #tempNumeroOps
	--(
	--	id						UdtConsecutivo Identity,
	--	nmro_unco_ops			UdtConsecutivo
	--);

	Set @fin_vigencia = '9999-12-31 00:00:00.000'; 
	Set @vldo_si = 1;
	Set @cnsctvo_cdgo_mdlo_cra46 = 46;
	Set @vlr_cro = 0;
	Set @cnsctvo_cdgo_estdo7 = 7;
	Set @cnsctvo_cdgo_estdo8 = 8;
	Set @cnsctvo_cdgo_estdo3 = 3;
	Set @cnsctvo_cdgo_estdo11 = 11;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15 = 15;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14 = 14;
	Set @fcha_actl = getDate();
	Set @vlr_uno = 1;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10 = 10;
	

	
		-- Solicitudes entregadas

			-- Actualiza el estado de las prestaciones para procedimientos-insumos.

			Update		ss
			Set			cnsctvo_cdgo_estdo_srvco_slctdo = em.cnsctvo_cdgo_estdo_srvco_slctdo,
						vlr_lqdcn_srvco = em.vlr_lqdcn_srvco,
						fcha_imprsn = em.fcha_imprsn
			From		BdCna.gsa.tbASServiciosSolicitados ss With(RowLock)
			Inner Join	BdCna.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	#tempPrestaciones em
			On			em.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
			Where		em.cnsctvo_mdcmnto_slctdo Is Null
			

			-- Actualiza el estado de las prestaciones para medicamentos
			Update		ss
			Set			cnsctvo_cdgo_estdo_srvco_slctdo = em.cnsctvo_cdgo_estdo_srvco_slctdo,
						vlr_lqdcn_srvco = em.vlr_lqdcn_srvco,
						fcha_imprsn = em.fcha_imprsn
			From		BdCna.gsa.tbASServiciosSolicitados ss With(RowLock)
			Inner Join	BdCna.gsa.tbASMedicamentosSolicitados pis With(NoLock)
			On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	#tempPrestaciones em
			On			em.cnsctvo_mdcmnto_slctdo = pis.cnsctvo_mdcmnto_slctdo
			Where		em.cnsctvo_prcdmnto_insmo_slctdo Is Null
			
			
			---- Se saca el numero de ops de las prestaciones anuladas y/o vencidas para anular las cuotas de recuperacion

			---- Saca los numero de OPS de las prestaciones anuladas
			--Insert Into #tempNumeroOps
			--(
			--			nmro_unco_ops
			--)
			--Select Distinct 
			--			cp.nmro_unco_ops
			--From		#tempPrestaciones em
			--Inner Join	#tempConceptosPrestaciones cp
			--On			cp.cnsctvo_prcdmnto_insmo_slctdo = em.cnsctvo_prcdmnto_insmo_slctdo
			--Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
			--On			cp.nmro_unco_ops = ccro.nmro_unco_ops
			--Where		em.cnsctvo_cdgo_estdo != @cnsctvo_cdgo_estdo8
			--And			em.cnsctvo_cdgo_estdo != @cnsctvo_cdgo_estdo7;

			--Insert Into #tempNumeroOps
			--(
			--			nmro_unco_ops
			--)
			--Select Distinct 
			--			cp.nmro_unco_ops
			--From		#tempPrestaciones em
			--Inner Join	#tempConceptosPrestaciones cp
			--On			cp.cnsctvo_mdcmnto_slctdo = em.cnsctvo_mdcmnto_slctdo
			--Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
			--On			cp.nmro_unco_ops = ccro.nmro_unco_ops
			--Where		em.cnsctvo_cdgo_estdo != @cnsctvo_cdgo_estdo8
			--And			em.cnsctvo_cdgo_estdo != @cnsctvo_cdgo_estdo7;

			--Select @mnmo = Min(id) From #tempNumeroOps
			--Select @mxmo = Max(id) From #tempNumeroOps

			---- Saca uno a uno los numero de ops y anula las cuotas de recuperacion
			--While(@mnmo <= @mxmo)
			--Begin

			--	-- Optiene los numeros de ops a anular cuotas de recuperacion
			--	Select		@nmro_unco_ops = nmro_unco_ops
			--	From		#tempNumeroOps
			--	Where		id = @mnmo;

			--	-- Se anulan las cuotas de recuperacion
			--	Exec BDCna.gsa.spASEjecutarAnulacionCuotasRecuperacion @usro_sstma, @mnsge_slda Output, @cdgo_slda Output, Null, Null, @nmro_unco_ops;

			--	Set @mnmo += 1;
			--End

			-- Se actualiza la vigencia del ultimo dato historico
			Update		hess
			Set			fn_vgnca = @fcha_actl,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro_sstma
			From		gsa.tbASHistoricoEstadosServicioSolicitado hess With(RowLock)
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
			On			hess.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
			Inner Join	#tempPrestaciones sv
			On			pis.cnsctvo_prcdmnto_insmo_slctdo = sv.cnsctvo_prcdmnto_insmo_slctdo;

			Update		hess
			Set			fn_vgnca = @fcha_actl,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro_sstma
			From		gsa.tbASHistoricoEstadosServicioSolicitado hess With(RowLock)
			Inner Join	gsa.tbASMedicamentosSolicitados ms With(NoLock)
			On			hess.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
			Inner Join	#tempPrestaciones sv
			On			ms.cnsctvo_mdcmnto_slctdo = sv.cnsctvo_mdcmnto_slctdo;

			-- Se crea el nuevo registro historico
			--
			Insert Into BdCna.gsa.tbASHistoricoEstadosServicioSolicitado
			(
						cnsctvo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo,			inco_vgnca,
						fn_vgnca,						fcha_fn_vldz_rgstro,						vldo,
						usro_crcn,						fcha_crcn,									usro_ultma_mdfccn,
						fcha_ultma_mdfccn
			)
			Select	Distinct
						pis.cnsctvo_srvco_slctdo,		em.cnsctvo_cdgo_estdo_srvco_slctdo,			@fcha_actl,
						@fin_vigencia,					@fin_vigencia,								@vldo_si,
						@usro_sstma,					@fcha_actl,								@usro_sstma,
						@fcha_actl
			From		#tempPrestaciones em
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
			On			em.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo;

			-- 
			Insert Into BdCna.gsa.tbASHistoricoEstadosServicioSolicitado
			(
						cnsctvo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo,			inco_vgnca,
						fn_vgnca,						fcha_fn_vldz_rgstro,						vldo,
						usro_crcn,						fcha_crcn,									usro_ultma_mdfccn,
						fcha_ultma_mdfccn
			)
			Select	Distinct
						ms.cnsctvo_srvco_slctdo,		em.cnsctvo_cdgo_estdo_srvco_slctdo,			@fcha_actl,
						@fin_vigencia,					@fin_vigencia,								@vldo_si,
						@usro_sstma,					@fcha_actl,								@usro_sstma,
						@fcha_actl
			From		#tempPrestaciones em
			Inner Join	gsa.tbASMedicamentosSolicitados ms With(NoLock)
			On			em.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo;

			Merge bdCNA.gsa.tbASDatosMigracionSipresMega With(RowLock) As Target
			Using(
				Select		cnsctvo_srvco_slctdo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,
							cnsctvo_cdgo_ofcna,			nuam,							cnsctvo_cdgo_estdo_srvco_slctdo
				From		#tempPrestaciones
				Group By	cnsctvo_srvco_slctdo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,
							cnsctvo_cdgo_ofcna,			nuam,							cnsctvo_cdgo_estdo_srvco_slctdo
			) As Source
			On (
				Target.cnsctvo_srvco_slctdo = Source.cnsctvo_srvco_slctdo				And
				Target.cnsctvo_slctd_atrzcn_srvco = Source.cnsctvo_slctd_atrzcn_srvco
			)
			When Matched Then
				Update
				Set		cnsctvo_cdgo_estdo_srvco_slctdo = Source.cnsctvo_cdgo_estdo_srvco_slctdo,
						fcha_ultma_mdfccn = @fcha_actl,
						usro_ultma_mdfccn = @usro_sstma
			When Not Matched Then
				Insert
				(
					cnsctvo_srvco_slctdo,				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,
					cnsctvo_cdgo_estdo_srvco_slctdo,	nuam,								cnsctvo_cdgo_ofcna,
					fcha_crcn,							usro_crcn,							fcha_ultma_mdfccn,
					usro_ultma_mdfccn
				)
				Values
				(
					Source.cnsctvo_srvco_slctdo,				Source.cnsctvo_slctd_atrzcn_srvco,			Source.cnsctvo_cdgo_srvco_slctdo,
					Source.cnsctvo_cdgo_estdo_srvco_slctdo,		Source.nuam,								Source.cnsctvo_cdgo_ofcna,	
					@fcha_actl,									@usro_sstma,								@fcha_actl,		
					@usro_sstma
				);

		--
		

	--Drop Table #tempNumeroOps;
End