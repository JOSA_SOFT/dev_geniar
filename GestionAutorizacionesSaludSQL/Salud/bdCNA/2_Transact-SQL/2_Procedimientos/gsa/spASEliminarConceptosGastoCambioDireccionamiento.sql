Use bdCNA
Go

If(OBJECT_ID('gsa.spASEliminarConceptosGastoCambioDireccionamiento') Is null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASEliminarConceptosGastoCambioDireccionamiento As Select 1';
End
Go

/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEliminarConceptosGastoCambioDireccionamiento 
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez  A\>    
* Descripcion        : <\D 
                            Procedimiento que borra los conceptos de gasto de la tabla de conceptos	y 
							anula los conceptos de bdSiSalud
						D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 19/07/2017 FC\>    
*------------------------------------------------------------------------------------------------------------------------------*/

Alter Procedure gsa.spASEliminarConceptosGastoCambioDireccionamiento
As
Begin
	
	Set NoCount On

	Declare		@cnsctvo_cdgo_estdo		udtConsecutivo;

	Set @cnsctvo_cdgo_estdo = 3;

	-- Elimina los historiales de descarga y estados de los conceptos
	Delete		hdf 
	From		bdCNA.gsa.tbASHistoricoDescargaFormatos hdf With(NoLock)
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_cncpto_prcdmnto_slctdo = hdf.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- 
	Delete		hcss 
	From		bdCNA.gsa.tbASHistoricoEstadosConceptosServicioSolicitado hcss With(NoLock)
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_cncpto_prcdmnto_slctdo = hcss.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- 
	Delete		hdf 
	From		bdCNA.gsa.tbASHistoricoDescargaFormatos hdf With(NoLock)
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_cncpto_prcdmnto_slctdo = hdf.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- 
	Delete		hcss 
	From		bdCNA.gsa.tbASHistoricoEstadosConceptosServicioSolicitado hcss With(NoLock)
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_cncpto_prcdmnto_slctdo = hcss.cnsctvo_cncpto_prcdmnto_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- Elimina los conceptos de tbASConceptosServicioSolicitado
	Delete		css 
	From		bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- 
	Delete		css 
	From		bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Inner Join	#tmpprestacionesops sac
	On			sac.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

	-- Actualiza el estado de los conceptos en Sipres a anulado.
	Update		co
	Set			cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo
	From		#tmpprestacionesops sac
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = sac.cnsctvo_srvco_slctdo
	Inner Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dam With(NoLock)
	On			dam.cnsctvo_slctd_mga = ss.cnsctvo_slctd_atrzcn_srvco
	And			dam.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo
	Inner Join	bdSisalud.dbo.tbConceptosOps co With(RowLock)
	On			co.cnsctvo_cdgo_ofcna = dam.cnsctvo_cdgo_ofcna
	And			co.nuam = dam.nuam
	And			co.cnsctvo_prstcn = dam.cnsctvo_prstcn
	

End