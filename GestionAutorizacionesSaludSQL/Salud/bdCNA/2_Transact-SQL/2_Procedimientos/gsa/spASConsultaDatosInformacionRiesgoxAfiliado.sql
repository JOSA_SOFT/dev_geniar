USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionRiesgoxAfiliado]    Script Date: 6/30/2017 4:39:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionRiesgoxAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el riesgo clinico del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02 										FC\>
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez AM\>
* Descripcion			 : <\DM Se actualiza la prestación adicionando el auditor 
								al que se remitió el caso	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	09/03/2017 FM\>
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM 
                                Se actualiza el procedimiento para que los medicamentos sean direccionados
								al auditor de medicamentos
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	25/05/2017 FM\>
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM 
                                Se actualiza el procedimiento para que las prestaciones sin 
								direccionamiento sean direccionadas al rol Enfermera
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	25/05/2017 FM\>
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM 
                                Se actualiza el procedimiento para que las prestaciones sin 
								direccionamiento y se ajusta la validacion de las vigencias
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	30/06/2017 FM\>
*---------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaDatosInformacionRiesgoxAfiliado] 807   , 4
--exec [gsa].[spASConsultaDatosInformacionRiesgoxAfiliado] 394127, 4 
--exec [gsa].[spASConsultaDatosInformacionRiesgoxAfiliado] 29560 , 5 

ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionRiesgoxAfiliado] 
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
	@tpo_adtr				    udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @sde_afldo				      udtConsecutivo,
			@cnsctvo_cdgo_chrte_vco	      udtConsecutivo,
			@cnsctvo_cdgo_chrte_dflt      udtConsecutivo,
			@usuario_proceso		      udtUsuario    ,
			@fecha_actual			      Datetime      ,
			@cdgo_ips_prmra               udtCodigoIps  ,
			@cnsctvo_cdgo_fljo_crgo       udtConsecutivo,
			@grpo_adtr_prcso              udtDescripcion,
			@grpo_adtr_prcso_sn_drccnmnto udtDescripcion,
			@ld_grpo_adtr_prcso		      udtDescripcion,	
			@cnsctvo_cdgo_tpo_cdfccn      udtConsecutivo,
			@rqre_otra_gstn_adtr          Varchar(50)   ,
			@lcVacio                      udtLogico     ,
			@ld_vlor_uno                  Int           ,
			@ld_vlor_cro                  Int           ,
			@ld_ctdd_rgstro               Int           ,
			@cnsctvo_cdgo_crgo_ss         udtConsecutivo;

	Create 
	Table  #RiesgoAfiliadoTemp(consecutivo			udtConsecutivo,
		                       cdgo_ips_prmra		udtCodigoIps,
		                       descripcionSolicitud	udtDescripcion
	                          )

	Set @usuario_proceso              = 'BPMAdmin'                    ;
	Set @fecha_actual	              = getDate()                     ;
	Set @cnsctvo_cdgo_chrte_vco       = 0                             ;
	Set @cnsctvo_cdgo_chrte_dflt      = 99                            ;
	Set @cdgo_ips_prmra               = '0'                           ;
	Set @cnsctvo_cdgo_fljo_crgo       = 4                             ;
	Set @cnsctvo_cdgo_tpo_cdfccn      = 5                             ;
	Set @grpo_adtr_prcso              = '4000017 Auditor Medicamentos';
	Set @grpo_adtr_prcso_sn_drccnmnto = '4000018 Enfermera SOS'       ;
	Set @cnsctvo_cdgo_crgo_ss         = 9                             ; --AUXILIAR PRESTACIONES MEDICAS
	Set @lcVacio                      = ''
	Set @ld_vlor_uno                  = 1
	Set @ld_vlor_cro                  = 0

	Select @sde_afldo = IAS.cnsctvo_cdgo_sde_ips_prmra 
    From   BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios IAS With(NoLock)
	Where  IAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

	Insert 
	Into       #RiesgoAfiliadoTemp(consecutivo, cdgo_ips_prmra, descripcionSolicitud)
	Select     @sde_afldo, IAS.cdgo_ips_prmra, TAR.grpo_adtr_prcso  
	From       BDcna.gsa.tbASTipoAuditorxRiesgoAfiliado TAR With(NoLock)
	Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios IAS With(NoLock)
	On         (
	            IAS.cnsctvo_cdgo_chrte = TAR.cnsctvo_cdgo_chrte Or 
	            TAR.cnsctvo_cdgo_chrte = @cnsctvo_cdgo_chrte_vco
			   )
	Where      IAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And        TAR.cdgo_pso_rsgo              = @tpo_adtr
	And        TAR.cnsctvo_cdgo_sde           = @sde_afldo

	--Se adiciona codigo temporal para que las prestaciones de medicamentos seas direccionadas al auditor de medicamentos
	If Not Exists(Select Top 1 consecutivo from #RiesgoAfiliadoTemp)
	   Begin
	     Insert 
	     Into   #RiesgoAfiliadoTemp(consecutivo, cdgo_ips_prmra, descripcionSolicitud)
         Select Distinct @cnsctvo_cdgo_fljo_crgo, @cdgo_ips_prmra, @grpo_adtr_prcso			
         From   BDCna.gsa.tbASServiciosSolicitados ss With(NoLock) 			
         Where  ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		 And    ss.cnsctvo_cdgo_tpo_srvco     = @cnsctvo_cdgo_tpo_cdfccn
	   End

	Select Top 1 @rqre_otra_gstn_adtr = ISNULL(rqre_otra_gstn_adtr, @lcVacio) 
	From   BdCNA.gsa.tbASServiciosSolicitados With(NoLock) 
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

	Select @ld_ctdd_rgstro = Count(@ld_vlor_uno) From #RiesgoAfiliadoTemp

	--Se valida que las prestaciones sean de ASI
	If @rqre_otra_gstn_adtr = @lcVacio And @ld_ctdd_rgstro = @ld_vlor_cro
	   Begin
	      Insert 
		  Into       #RiesgoAfiliadoTemp(consecutivo, cdgo_ips_prmra, descripcionSolicitud)
		  Select     Distinct @sde_afldo, @cdgo_ips_prmra, @grpo_adtr_prcso_sn_drccnmnto
		  From       bdSisalud.dbo.tbCupsxPlanxCargo_Vigencias cpc With(NoLock)
		  Inner Join bdSisalud.dbo.tbCargosSOS_Vigencias cv With(NoLock)
		  On         cv.cnsctvo_cdgo_crgo_ss = cpc.cnsctvo_cdgo_crgo
		  Inner Join BDCna.gsa.tbASServiciosSolicitados ss With(NoLock) 
		  On         ss.cnsctvo_cdgo_srvco_slctdo = cpc.cnsctvo_cdfccn
		  Where      ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		  And        cpc.cnsctvo_cdgo_crgo         = @cnsctvo_cdgo_crgo_ss
		  And        @fecha_actual Between cpc.inco_vgnca And cpc.fn_vgnca
		  And        @fecha_actual Between cv.inco_vgnca  And cv.fn_vgnca
	   End
	   	

	Select @ld_ctdd_rgstro = Count(@ld_vlor_uno) From #RiesgoAfiliadoTemp

	--Se no calcula grupo auditor para asignar, se asigna al Administrador Integral General SOS
	If @rqre_otra_gstn_adtr = @lcVacio And @ld_ctdd_rgstro = @ld_vlor_cro
		Begin
			Insert 
			Into   #RiesgoAfiliadoTemp(consecutivo, cdgo_ips_prmra, descripcionSolicitud)
			Select @sde_afldo, @cdgo_ips_prmra, TAR.grpo_adtr_prcso 
			From   BDcna.gsa.tbASTipoAuditorxRiesgoAfiliado TAR With(NoLock)
			where  TAR.cnsctvo_cdgo_chrte = @cnsctvo_cdgo_chrte_dflt;
		End	
		  
	--Se actualiza la prestación adicionando el auditor al que se remitió el caso
	Select @ld_grpo_adtr_prcso = descripcionSolicitud 
	From   #RiesgoAfiliadoTemp
	
	If @ld_grpo_adtr_prcso Is Not Null
	  Begin
         Update BdCNA.gsa.tbASServiciosSolicitados
	     Set    rqre_otra_gstn_adtr = @ld_grpo_adtr_prcso,
	            usro_ultma_mdfccn   = @usuario_proceso   ,
	            fcha_ultma_mdfccn   = @fecha_actual
         Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	  End
	Else
	  Begin
	     Insert 
		 Into   #RiesgoAfiliadoTemp(consecutivo, cdgo_ips_prmra, descripcionSolicitud)
		 Select @sde_afldo, @cdgo_ips_prmra, @rqre_otra_gstn_adtr	     
	  End

	Select consecutivo, cdgo_ips_prmra, descripcionSolicitud 
	From   #RiesgoAfiliadoTemp

	Drop table #RiesgoAfiliadoTemp;
End