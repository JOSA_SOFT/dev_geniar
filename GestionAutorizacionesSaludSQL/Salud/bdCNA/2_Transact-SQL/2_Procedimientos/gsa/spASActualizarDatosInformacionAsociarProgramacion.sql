USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarDatosInformacionAsociarProgramacion]    Script Date: 12/05/2017 11:58:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASActualizarDatosInformacionAsociarProgramacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el peso reputacional del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Carlos Andrés López Ramírez AM\>
* Descripcion			 : <\DM Se agregan sentencias para actualizar la fecha de entrega en
								la tabla tbASResultadoFechaEntrega, el codigo Interno en las 
								tablas tbAsConceptosServicioSolicitado y tbASResultadoDireccionamiento DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM @fcha_entrga
								@cdgo_intrno
								@cnsctvo_srvco_slctdo VM\>
* Fecha Modificacion	 : <\FM 2017/05/12 FM\>
*---------------------------------------------------------------------------------*/

/**

	Declare		@cdgo_rsltdo				udtConsecutivo = 0,
				@mnsje_rsltdo				udtDescripcion = null
	Exec bdCNA.gsa.spASActualizarDatosInformacionAsociarProgramacion 133907, 93710, 5, '1107864818', 'qvisionclr', @cdgo_rsltdo Output, @mnsje_rsltdo Output
	Select @cdgo_rsltdo, @mnsje_rsltdo
*/
ALTER PROCEDURE [gsa].[spASActualizarDatosInformacionAsociarProgramacion] 

/*01*/ @cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
/*02*/ @cnsctvo_cdgo_srvco_slctdo		udtConsecutivo,
/*03*/ @cnsctvo_cdgo_tpo_idntfccn_afldo	UdtConsecutivo,
/*04*/ @nmro_idntfccn_afldo				UdtNumeroIdentificacionLargo, 
/*05*/ @usro_prcso						udtUsuario,
/*06*/ @cdgo_rsltdo						udtConsecutivo = 0		output,
/*07*/ @mnsje_rsltdo					udtDescripcion = null	output

AS

BEGIN

	SET NOCOUNT ON;

	Declare @estdo_entrga						int = 2, -- Estado Entrega
			@mensaje_ok							udtDescripcion = 'Proceso ejecutado satisfactoriamente',
			@mensaje_error						udtDescripcion = 'No Existe Programación de Entrega Para el Afiliado y la prestación solicitada',
			@codigo_ok							int			   = 0,
			@codigo_error						int			   = -1,
			@fcha_actlzcn						date		   = getDate(),
			@estdo								char(1)		= 'A',
			@cnsctvo_cdgo_dt_prgrmcn_fcha_evnto UdtConsecutivo,
			@nmro_unco_idntfccn					UdtConsecutivo,
			@sn_entrgr							int				= 151, -- Estado Entrega '151-Sin Entregar'
			@rprgrmdo							int				= 152, -- Estado Entrega '152-Reprogramado'
			@ascdo								int				= 170,  -- Estado '170-Asociado OPS'
			@fcha_entrga						Date,
			@cdgo_intrno						udtCodigoIPS,
			@cnsctvo_srvco_slctdo				udtConsecutivo;

	Set @mnsje_rsltdo = @mensaje_ok;
	Set @cdgo_rsltdo  = @codigo_ok;
	
	
	BEGIN TRY
			
			-- Recuperamos el número único del afiliado
			Select 		@nmro_unco_idntfccn = ias.nmro_unco_idntfccn_afldo
			From		bdcna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias with(nolock)
			Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas with(nolock)
			On			sas.cnsctvo_slctd_atrzcn_srvco = ias.cnsctvo_slctd_atrzcn_srvco		
			Where		ias.cnsctvo_cdgo_tpo_idntfccn_afldo = 	@cnsctvo_cdgo_tpo_idntfccn_afldo
			And			ias.nmro_idntfccn_afldo	= @nmro_idntfccn_afldo
			And			ias.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

			-- Recuperamos el consecutivo detalle programación fecha evento
			Select distinct
						@cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto,
						@fcha_entrga = c.fcha_entrga,
						@cdgo_intrno = e.cdgo_intrno
			From		bdsisalud.dbo.tbProgramacionPrestacion a With(Nolock)
			INNER JOIN	bdsisalud.dbo.tbProgramacionFechaEvento b With(Nolock)	
			ON			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
			INNER JOIN	bdsisalud.dbo.tbDetProgramacionFechaEvento c With(Nolock)	
			ON			a.cnsctvo_prgrmcn_prstcn = c.cnsctvo_prgrmcn_prstcn 
			And			(c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo))
			INNER JOIN	bdsisalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
			ON			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
			AND			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha 
			AND			e.estdo = @estdo
			INNER JOIN	bdsisalud.dbo.tbAfiliadosMarcados f with(nolock)
			ON			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
			And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
			where		f.nmro_unco_idntfccn = @nmro_unco_idntfccn
			And			a.cnsctvo_prstcn = @cnsctvo_cdgo_srvco_slctdo 
			And			b.fcha_dsde <= @fcha_actlzcn

			-- Si hay programación de entrega se actualizan los estados y se asocia el servicio 
			if @cnsctvo_cdgo_dt_prgrmcn_fcha_evnto is not null 
			   Begin
					-- Actualizamos los Estados de programación Entrega
					Update		t
					Set			cnsctvo_cdgo_estds_entrga	= @estdo_entrga,
								usro_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn			= @fcha_actlzcn
					From		bdsisalud.dbo.tbProgramacionFechaEvento t With(RowLock)
					Inner Join	bdsisalud.dbo.tbdetProgramacionFechaEvento dt With(Nolock)
					on			t.cnsctvo_prgrmcn_prstcn = dt.cnsctvo_prgrmcn_prstcn 
					where		dt.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = @cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
   

					-- Actualizamos los Estados del detalle programación de Entrega
					Update  t
					Set			cnsctvo_cdgo_estds_entrga			= @ascdo,
								usro_mdfccn							= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
					From		bdsisalud.dbo.tbdetProgramacionFechaEvento t With(RowLock)
					where		t.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = @cnsctvo_cdgo_dt_prgrmcn_fcha_evnto

					-- Se actualiza la asociación de la programación de entrega con la solicitud creada
				   Update		ss
				   Set			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	= @cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
								usro_ultma_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
				   From			BdCna.gsa.tbASServiciosSolicitados ss With(RowLock)
				   where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			ss.cnsctvo_cdgo_srvco_slctdo = @cnsctvo_cdgo_srvco_slctdo
				  
				  -- Se recupera el consecutivo de la prestacion para actualizar 
				   Select		@cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
				   From			BdCna.gsa.tbASServiciosSolicitados ss With(RowLock)
				   where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			ss.cnsctvo_cdgo_srvco_slctdo = @cnsctvo_cdgo_srvco_slctdo

				   -- Se actualiza el codigo interno en los conceptos
				   Update		css
				   Set			cdgo_intrno_prstdr_atrzdo			= @cdgo_intrno,
								usro_ultma_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
				   From			BdCna.gsa.tbASServiciosSolicitados ss With(NoLock)
				   Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
				   On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
				   Inner Join	bdCNA.gsa.tbAsConceptosServicioSolicitado css With(RowLock)
				   On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
				   where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			ss.cnsctvo_cdgo_srvco_slctdo = @cnsctvo_cdgo_srvco_slctdo

				   Update		css
				   Set			cdgo_intrno_prstdr_atrzdo			= @cdgo_intrno,
								usro_ultma_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
				   From			BdCna.gsa.tbASServiciosSolicitados ss With(NoLock)
				   Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados m With(NoLock)
				   On			m.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
				   Inner Join	bdCNA.gsa.tbAsConceptosServicioSolicitado css With(RowLock)
				   On			css.cnsctvo_mdcmnto_slctdo =m.cnsctvo_mdcmnto_slctdo
				   where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			ss.cnsctvo_cdgo_srvco_slctdo = @cnsctvo_cdgo_srvco_slctdo;

				   -- Se actualiza el codigo interno en el direccionamiento
				   Update		rd
				   Set			cdgo_intrno							= @cdgo_intrno,
								usro_ultma_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
				   From			bdCNA.gsa.tbASResultadoDireccionamiento rd With(RowLock)
				   where		cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo;

				   -- Se actualiza la fecha de entrega.
				   Update		rfe
				   Set			fcha_estmda_entrga					= @fcha_entrga,
								usro_ultma_mdfccn					= @usro_prcso,
								fcha_ultma_mdfccn					= @fcha_actlzcn
				   From			bdCNA.gsa.tbASResultadoFechaEntrega rfe With(RowLock)
				   Where		cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
				   And			cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo;

				   -- Mensaje Exitoso
				   Set @cdgo_rsltdo = @codigo_ok
				   Set @mnsje_rsltdo = @mensaje_ok
				End
			Else
				Begin
					-- No Existe Programación de Entrega para el afiliado y el servicio
					Set @cdgo_rsltdo = @codigo_error
				    Set  @mnsje_rsltdo = @mensaje_error
				End

		END TRY
		BEGIN CATCH
			
			Set @mnsje_rsltdo = 'Number:' + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
								'Line:' + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
								'Message:' + ERROR_MESSAGE() + CHAR(13) +
								'Procedure:' + ERROR_PROCEDURE();
			Set @cdgo_rsltdo  = @codigo_error;
		END CATCH
	
END
