USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionGestionAuditor]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosGestionAuditor
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el peso reputacional del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionGestionAuditor] 
@cnsctvo_srvco_slctdo       udtConsecutivo

AS
BEGIN
	SET NOCOUNT ON;
	Declare @fecha_vigencia	datetime;

	Set @fecha_vigencia = getDate();

Select rga.usro_lgn_adtr,
	   rga.dscrpcn_tpo_adtra ,	
	   rga.cnsctvo_cdgo_estdo_no_cnfrmdd,
	   est.dscrpcn_estdo_no_cnfrmdd,
	   rga.obsrvcn_gstn_adtr,
	   rgda.cnsctvo_cdgo_no_cnfrmdd_vldcn,
	   ncv.dscrpcn_no_cnfrmdd_vldcn
	   from BDCna.gsa.tbASResultadoGestionAuditor  rga WITH (NOLOCK)
Inner Join BDCna.gsa.tbASResultadoDetalleGestionAuditor rgda WITH (NOLOCK) on  rga.cnsctvo_cdgo_rsltdo_gstn_adtr = rgda.cnsctvo_cdgo_rsltdo_gstn_adtr
Inner Join bdMallaCNA.dbo.tbEstadosNoConformidades_Vigencias est WITH (NOLOCK) on rga.cnsctvo_cdgo_estdo_no_cnfrmdd = est.cnsctvo_cdgo_estdo_no_cnfrmdd
Inner Join bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias ncv WITH (NOLOCK) on ncv.cnsctvo_cdgo_no_cnfrmdd_vldcn = rgda.cnsctvo_cdgo_no_cnfrmdd_vldcn
WHERE	
rga.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
And		@fecha_vigencia between NCV.inco_vgnca and NCV.fn_vgnca
And		@fecha_vigencia between est.inco_vgnca and est.fn_vgnca

END

GO
