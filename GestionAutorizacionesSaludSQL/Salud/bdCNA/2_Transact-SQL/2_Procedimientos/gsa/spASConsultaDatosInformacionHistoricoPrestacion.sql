USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionHistoricoPrestacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionCausasEstados
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Luis Fernando Benavides AM\>
* Descripcion			 : <\DM	Cambio consecutivo 147 por 154 DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/05/26 FM\>
*---------------------------------------------------------------------------------
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM cambia la descripción No Pos/Pos ya que estaba invertida DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionHistoricoPrestacion]
@cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
@nmro_idntfccn				udtNumeroIdentificacionLargo

AS
BEGIN
	SET NOCOUNT ON;
	Declare @fecha_historico			datetime,
			@getDate_					datetime = getDate(),
			@reglaFecha_inicio			int = 154,
			@acotar_antiguedad			int,
			@no							char(1) = 'N',
			@no_pos						varchar(10) = 'NO POS',
			@pos						varchar(10) = 'POS';


	Set @acotar_antiguedad = ( SELECT vlr_rgla  FROM bdSisalud.dbo.tbReglas_Vigencias WITH (NOLOCK)
						       WHERE cnsctvo_cdgo_rgla = @reglaFecha_inicio AND @getDate_ BETWEEN inco_vgnca AND fn_vgnca)

	Set @fecha_historico = (SELECT DATEADD(mm, -@acotar_antiguedad, GETDATE()));
	
	Select 
		SAS.fcha_slctd ,
		ESS.dscrpcn_estdo_srvco_slctdo  ,
		DP.nmbre_scrsl,
		SS.cntdd_slctda,
		TCF.dscrpcn_tpo_cdfccn,
		CF.dscrpcn_cdfccn,
		Case SS.indcdr_no_ps When @no Then @pos Else @no_pos End indcdr_no_ps
	From BDCna.gsa.tbASSolicitudesAutorizacionServicios SAS
	Inner Join	 BDCna.gsa.tbASServiciosSolicitados SS With(NoLock) on SAS.cnsctvo_slctd_atrzcn_srvco =  SS.cnsctvo_slctd_atrzcn_srvco
	Inner Join	 BDCna.prm.tbASEstadosServiciosSolicitados ESS With(NoLock) on ESS.cnsctvo_cdgo_estdo_srvco_slctdo = SS.cnsctvo_cdgo_estdo_srvco_slctdo
	Inner Join	 BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios IPS With(NoLock) on IPS.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
	INNER Join   BDSisalud.dbo.tbDireccionesPrestador DP With(NoLock) ON DP.cdgo_intrno = IPS.cdgo_intrno
	INNER Join   BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK) on ias.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN bdSisalud.dbo.tbCodificaciones CF WITH (NOLOCK) on CF.cnsctvo_cdfccn = SS.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on cf.cnsctvo_cdgo_tpo_cdfccn = tcf.cnsctvo_cdgo_tpo_cdfccn
	where ias.cnsctvo_cdgo_tpo_idntfccn_afldo   = @cnsctvo_cdgo_tpo_idntfccn
	And	  ias.nmro_idntfccn_afldo				= @nmro_idntfccn
	And  @getdate_ between tcf.inco_vgnca and tcf.fn_vgnca
	And SAS.fcha_slctd > @fecha_historico;
END

GO
