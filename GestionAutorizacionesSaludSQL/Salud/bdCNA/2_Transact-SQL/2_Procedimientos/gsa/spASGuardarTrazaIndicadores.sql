USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarTrazaIndicadores]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarTrazaIndicadores
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					     A\>
* Descripcion			: <\D Sp utiliazado para guardar la traza de los indicadores D\>
						  <\D .													     D\>
* Observaciones			: <\O  													     O\>
* Parametros			: <\P \>
* Variables				: <\V  													     V\>
* Fecha Creacion		: <\FC 2016/02/20										    FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASGuardarTrazaIndicadores]
@fcha_hra		 datetime,
@instnca_prcso	 varchar(20),
@id_ngcio		 udtDescripcion,		
@id_indcdr		 udtConsecutivo, 
@id_prcso_actvdd udtConsecutivo, 
@prptro_instnca  varchar(20), 
@da_smna		 varchar(12), 
@vlr_nmrco		 int, 
@vlr_fcha		 datetime, 
@vlr_dcml		 float, 
@vlr_txto		 varchar(100), 
@stdo_rgstro	 varchar(2)


AS
BEGIN
	SET NOCOUNT ON;
	--Begin Transaction
	BEGIN TRY
		INSERT INTO BDCNA.IND.TBASDETALLEINDICADOR 
		(  
			FCHA_HRA, 
			INSTNCA_PRCSO, 
			ID_NGCIO, 
			ID_INDCDR, 
			ID_PRCSO_ACTVDD, 
			PRPTRO_INSTNCA, 
			DA_SMNA, 
			VLR_NMRCO, 
			VLR_FCHA, 
			VLR_DCML, 
			VLR_TXTO, 
			STDO_RGSTRO
		) 
		VALUES 
		(
			@fcha_hra,
			@instnca_prcso,
			@id_ngcio, 
			@id_indcdr, 
			@id_prcso_actvdd, 
			@prptro_instnca, 
			@da_smna, 
			@vlr_nmrco, 
			@vlr_fcha, 
			@vlr_dcml, 
			@vlr_txto, 
			@stdo_rgstro
		)

		--COMMIT;
		Select 1 as resultado
	END TRY
	BEGIN CATCH
		--RollBack;
		Select 0 as resultado
	END CATCH

	
END

GO
