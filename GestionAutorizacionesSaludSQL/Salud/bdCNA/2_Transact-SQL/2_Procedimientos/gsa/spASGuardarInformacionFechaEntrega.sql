USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionFechaEntrega]    Script Date: 8/22/2017 2:49:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionFechaEntrega
* Desarrollado por		: <\A Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D 
                              Recibe un xml con la informaci�n de las prestaciones de una solicituda 
                              a las que se les modific� la fecha de entrega, con su respectiva causal y 
							  justificaci�n de cambio y posteriormente altera los registros relacionados 
							  en la BD. Nuevas fechas de entrega e Hist�rico de 
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/10/24 FC\>
*-------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	
                                Se modifica SP para guardar el origen de la modificaci�n que sea mas 
								claro para mostrarlo en la pantalla Historico Modificaciones
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*-------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM	
                                Se modifica SP que permitra registrar y actualizar las fecha de entrega
								a los servicios asociados a una solicitud
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/08/22	FM\>
*-------------------------------------------------------------------------------------------------------------*/
/*
Begin
Declare @cnsctvs_srvco_fcha_entrga  Varchar(MAX)  , 
        @cdgo_rsltdo                udtconsecutivo, 
		@mnsje_rsltdo               udtdescripcion 

Set @cnsctvs_srvco_fcha_entrga = '<gestionFechaEntrega>
	<prestaciones>
		<prestacion>
			<codigoSOS>890280</codigoSOS>
			<consecutivo>98047</consecutivo>
			<fecha>25-08-2017 00:00:00.000</fecha>
		</prestacion>		
	</prestaciones>
	<solicitud>98641</solicitud>
	<radicado>2017-01-00087029</radicado>
	<causal>14</causal>
	<usuarioModificacion>user1</usuarioModificacion>
	<justificacion>Esta es una prueba de fecha entrega</justificacion>
</gestionFechaEntrega>' 


Exec [gsa].[spASGuardarInformacionFechaEntrega] @cnsctvs_srvco_fcha_entrga, @cdgo_rsltdo Output, @mnsje_rsltdo Output

Select  @cdgo_rsltdo, @mnsje_rsltdo
End
*/
ALTER PROCEDURE [gsa].[spASGuardarInformacionFechaEntrega]
	@cnsctvs_srvco_fcha_entrga Varchar(MAX)         , 
	@cdgo_rsltdo               udtConsecutivo Output,
	@mnsje_rsltdo              udtDescripcion Output  
AS

BEGIN
	SET NOCOUNT ON;

	Declare @xml_cnsctvs_srvco_fcha_entrga	XML           ,
			@cnsctvo_slctd_srvco			udtConsecutivo,
			@nmro_rdcdo						varchar(16)   ,
			@mtvo_mdfccn					udtConsecutivo,
			@usro_mdfccn					udtUsuario    ,
			@jstfccn_mdfccn				    udtObservacion,
			@codigo_ok						Int           ,
            @codigo_error					Int           ,
			@mensaje_ok						udtdescripcion,
            @mensaje_error					udtdescripcion,
			@vlr_uno						Int           ,
			@fcha_actl						Datetime	  ,
			@vlr_ip_cnstnte					Int			  ,
			@orgn_mdfccn					udtdescripcion,
			@elmnto_orgn_mdfccn				udtdescripcion,
			@mrca_actlzcn_srvco             udtLogico     ,
			@mrca_no_actlzcn_srvco          udtLogico     ,
			@dto_elmnto_orgn_mdfccn			udtdescripcion,
			@dto_elmnto_orgn_insrccn        udtdescripcion,
			@cnsctvo_anulada		        udtConsecutivo,
			@mrca_incpcdd                   udtLogico     ,
			@prrdd_admnstrtva               udtLogico     ,
			@cnsctvo_prcso                  udtConsecutivo,
			@vlr_antrr_insrccn              Int           ,
			@es_btch                        udtLogico     ;

	Create 
	Table #tmpPrestacionesFechaEntregaXML(cdgo_srvco_slctdo			 Varchar(11)          ,
		                                  cnsctvo_srvco_slctdo		 udtConsecutivo       ,
										  fcha_entrg_actlzr          Datetime             ,
										  fcha_entrga_envda		     Varchar(24)          ,
		                                  fcha_entrga_antrr 	     Datetime             ,		                                 
		                                  indcdr_actlzcn_fcha_entrga Int       Default 0  ,
										  mrca_actlzcn_srvco         udtLogico Default '0'										   	
	                                     )	

	Create 
	Table  #tmpSolicitudFechaEntrega(cnsctvo_slctd_atrzcn_srvco udtConsecutivo           ,	                                       
		                             cnsctvo_srvco_slctdo		udtConsecutivo           ,
									 cnsctvo_cdgo_grpo_entrga   udtConsecutivo Default 0 ,
									 cdgo_grpo_entrga           Varchar(30)    Default ''
									)
	Create
	Table  #tmpTrazaModificacion(cnsctvo_slctd_atrzcn_srvco	    udtconsecutivo,
                                 cnsctvo_srvco_slctdo			udtconsecutivo,
                                 cnsctvo_cncpto_prcdmnto_slctdo udtconsecutivo,
                                 cnsctvo_cdgo_mtvo_csa		    udtconsecutivo,
                                 fcha_mdfccn					Datetime      ,
                                 orgn_mdfccn					Varchar(100)  ,
                                 vlr_antrr					    Varchar(20)   ,
                                 vlr_nvo						Varchar(20)   ,
                                 usro_mdfccn					udtusuario    ,
                                 obsrvcns						udtobservacion,
                                 nmro_ip						Varchar(12)   ,
                                 fcha_crcn					    Datetime      ,
                                 usro_crcn					    udtusuario    ,
                                 fcha_ultma_mdfccn			    Datetime      ,
                                 usro_ultma_mdfccn			    udtusuario    ,
                                 elmnto_orgn_mdfccn			    udtDescripcion,
                                 dto_elmnto_orgn_mdfccn		    udtDescripcion,
                                 dscrpcn_vlr_antrr			    udtDescripcion,
                                 dscrpcn_vlr_nvo				udtDescripcion
                                )
                         
    --Creaci�n de tablas temporales para la ejecuci�n de la gesti�n de solicitudes y ejecuci�n de reglas.
    --Temporal con los datos b�sicos de la solicitud
    Create 
    Table  #tmpDatosSolicitudFechaEntrega(id                              udtConsecutivo IDENTITY (1,1),
                                          cnsctvo_slctd_srvco_sld_rcbda   udtConsecutivo               ,
                                          cnsctvo_srvco_slctdo            udtConsecutivo               ,
                                          cnsctvo_cdfccn                  udtConsecutivo               ,     
	                                      cnsctvo_cdgo_tpo_srvco          udtConsecutivo               ,
                                          cdgo_cdfccn                     Varchar(30)                  ,   
                                          cnsctvo_pln                     udtConsecutivo               ,
                                          cdgo_pln                        Varchar(30)                  , 
                                          cnsctvo_tpo_pln                 udtConsecutivo               , 
                                          cdgo_tpo_pln                    Varchar(30)                  ,
                                          cnsctvo_mdo_cntcto              udtConsecutivo               ,
                                          cdgo_mdo_cntcto                 Varchar(30)                  ,
                                          cnsctvo_grpo_entrga             udtConsecutivo               ,
                                          cdgo_grpo_entrga                Varchar(30)                  ,
	                                      cnsctvo_cdgo_itm_prspsto        udtConsecutivo               ,
                                          fcha_slctd                      Datetime NOT NULL            , 
                                          fcha_estmda_entrga              Datetime                     ,
                                          nmro_unco_idntfccn_afldo        udtConsecutivo               ,
                                          nmro_idntfccn                   udtNumeroIdentificacionLargo ,
                                          edd_afldo_ans                   Int                          ,
	                                      edd_afldo_mss                   Int                          ,
	                                      edd_afldo_ds                    Int                          ,
                                          nmro_cntrto                     Char(15)                     ,
                                          cnsctvo_cdgo_tpo_cntrto         udtConsecutivo               ,
                                          usro                            udtUsuario                   ,
                                          nmro_slctd_ss                   Varchar(16)                  ,
                                          nmro_slctd_prvdr                Varchar(16)                  ,
                                          cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo               ,
                                          nmro_idntfccn_empleador         udtNumeroIdentificacionLargo ,
										  nmro_unco_idntfccn_empleador    udtConsecutivo               ,
	                                      cnsctvo_grpo_entrga_fnl         udtConsecutivo               ,
	                                      cdgo_grpo_entrga_fnl            Varchar(30)                  ,
	                                      fcha_estmda_entrga_fnl          Datetime
                                         )

	--Se crea tabla temporal para agrupar por jerarquia
	Create
	Table    #tempAgrupadorFechaEntrega(cnsctvo_slctd_srvco_sld_rcbda   udtConsecutivo,
                                        cnsctvo_srvco_slctdo            udtConsecutivo,
			                            cnsctvo_cdgo_grpo_entrga        udtConsecutivo, 
			                            cdgo_grpo_entrga                Varchar(30)   ,   
			                            jrrqa                           Numeric(2,0)
									   )
    
	--Se crea tabla temporal para validacion de regla de incapacidades
    Create 
    Table  #tmpMarcas(id                            udtConsecutivo IDENTITY (1,1),
                      cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo               ,
                      nmro_unco_idntfccn_afldo      udtConsecutivo               ,
                      mrca_incpcdd                  udtLogico
                     )

    Create  NonClustered Index idx_cnsctvo_grpo_entrga
    On      #tmpDatosSolicitudFechaEntrega (cnsctvo_grpo_entrga)
    Include (cnsctvo_slctd_srvco_sld_rcbda, nmro_cntrto, cnsctvo_cdgo_tpo_cntrto)


	Set @vlr_ip_cnstnte                 = 0
    Set @fcha_actl                      = getDate()
	Set	@xml_cnsctvs_srvco_fcha_entrga	= CAST(@cnsctvs_srvco_fcha_entrga AS XML)
	Set	@orgn_mdfccn					= 'tbASResultadoFechaEntrega.fcha_estmda_entrga'
	Set	@elmnto_orgn_mdfccn				= 'Prestaci�n'
	Set	@dto_elmnto_orgn_mdfccn			= 'Cambio Fecha Entrega'
	Set	@dto_elmnto_orgn_insrccn        = 'Registro Fecha Entrega'
	Set @codigo_error                   = -1
    Set @mensaje_error                  = 'No existe Fecha Entrega para la Prestaci�n'
	Set @codigo_ok                      = 0
	Set @mensaje_ok                     = 'Proceso ejecutado satisfactoriamente'
	Set @vlr_uno                        = 1	
	Set @mrca_actlzcn_srvco             = '1'  
	Set @mrca_no_actlzcn_srvco          = '0' 
	Set @prrdd_admnstrtva               = 'N'
	Set @mrca_incpcdd                   = 'N'
    Set @cnsctvo_anulada                = 5
	Set @cnsctvo_prcso                  = 0
	Set @es_btch                        = 'N'
	Set @vlr_antrr_insrccn              = 0

	Begin Try
		Select @cnsctvo_slctd_srvco = pref.value('(solicitud/text())[1]'          , 'udtConsecutivo'),
			   @nmro_rdcdo          = pref.value('(radicado/text())[1]'           , 'Varchar(16)'   ),
			   @mtvo_mdfccn         = pref.value('(causal/text())[1]'             , 'udtConsecutivo'),
			   @usro_mdfccn         = pref.value('(usuarioModificacion/text())[1]', 'udtusuario'    ),
			   @jstfccn_mdfccn      = pref.value('(justificacion/text())[1]'      , 'udtObservacion')
		From   @xml_cnsctvs_srvco_fcha_entrga.nodes('/gestionFechaEntrega') AS xml_slctd (Pref);

		Insert 
		Into   #tmpPrestacionesFechaEntregaXML(cdgo_srvco_slctdo, cnsctvo_srvco_slctdo, fcha_entrga_envda)
		Select pref.value('(codigoSOS/text())[1]'  , 'Varchar(11)')    As cdgo_srvco_slctdo   ,
			   pref.value('(consecutivo/text())[1]', 'udtConsecutivo') As cnsctvo_srvco_slctdo,
			   pref.value('(fecha/text())[1]'      , 'Varchar(24)')    As fcha_entrga_envda
		From   @xml_cnsctvs_srvco_fcha_entrga.nodes('/gestionFechaEntrega/prestaciones/prestacion') AS xml_slctd (Pref);		
	

		--Se actualiza en la tabla temporal la fecha de entrega enviada y la cual se debe actualizar
		Update #tmpPrestacionesFechaEntregaXML 
		Set    fcha_entrg_actlzr = Convert(Datetime, fcha_entrga_envda, 105)

    
		--Se actualiza en la tabla temporal la marca y fecha anterior del servicio, si se ha calculado anteriormente
		Update     a
		Set        mrca_actlzcn_srvco = @mrca_actlzcn_srvco ,
				   fcha_entrga_antrr  = b.fcha_estmda_entrga 
		From       #tmpPrestacionesFechaEntregaXML a
		Inner Join bdCNA.gsa.tbASResultadoFechaEntrega b With(NoLock)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Where      b.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_srvco
	
	
		--Se actualiza en la tabla la nueva fecha estimada de entrega
		Update     rd 
		Set        fcha_estmda_entrga = pfe.fcha_entrg_actlzr,
				   fcha_ultma_mdfccn  = @fcha_actl           ,
				   usro_ultma_mdfccn  = @usro_mdfccn
		From       bdCNA.gsa.tbASResultadoFechaEntrega rd WITH(NOLOCK)
		Inner Join #tmpPrestacionesFechaEntregaXML pfe
		On         pfe.cnsctvo_srvco_slctdo = rd.cnsctvo_srvco_slctdo 	           
		Where      pfe.mrca_actlzcn_srvco        = @mrca_actlzcn_srvco    
		And        rd.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_srvco


		--Se establece la informaci�n de la trazabilidad.
		Insert 
		Into   #tmpTrazaModificacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo  , cnsctvo_cncpto_prcdmnto_slctdo,
									 cnsctvo_cdgo_mtvo_csa     , fcha_mdfccn           , orgn_mdfccn                   ,
									 vlr_antrr                 , vlr_nvo               , usro_mdfccn                   ,
									 obsrvcns                  , nmro_ip               , fcha_crcn                     ,
									 usro_crcn                 , fcha_ultma_mdfccn     , usro_ultma_mdfccn             ,
									 elmnto_orgn_mdfccn        , dto_elmnto_orgn_mdfccn, dscrpcn_vlr_antrr             ,
									 dscrpcn_vlr_nvo
									)
		Select	@cnsctvo_slctd_srvco, cnsctvo_srvco_slctdo   , NULL            ,
				@mtvo_mdfccn        , @fcha_actl             , @orgn_mdfccn     ,
				fcha_entrga_antrr   , fcha_entrg_actlzr      , @usro_mdfccn     ,
				@jstfccn_mdfccn     , @vlr_ip_cnstnte        , @fcha_actl       ,
				@usro_mdfccn        , @fcha_actl             , @usro_mdfccn     ,
				@elmnto_orgn_mdfccn , @dto_elmnto_orgn_mdfccn, fcha_entrga_antrr,
				fcha_entrg_actlzr
		From    #tmpPrestacionesFechaEntregaXML pfe
		Where   mrca_actlzcn_srvco = @mrca_actlzcn_srvco  
	

		If Exists (Select Top 1 cnsctvo_srvco_slctdo
				   From   #tmpPrestacionesFechaEntregaXML
				   Where  mrca_actlzcn_srvco = @mrca_no_actlzcn_srvco
				  )
		   Begin

			  Insert
			  Into    #tmpSolicitudFechaEntrega(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo)
			  Select  @cnsctvo_slctd_srvco, cnsctvo_srvco_slctdo
			  From    #tmpPrestacionesFechaEntregaXML
			  Where   mrca_actlzcn_srvco = @mrca_no_actlzcn_srvco

			  --Se inserta en la tabla temporal la informacion requerida para calcular y ejecutar las reglas del fecha de entrega
			  Insert 
			  Into       #tmpDatosSolicitudFechaEntrega(cnsctvo_slctd_srvco_sld_rcbda  , cnsctvo_srvco_slctdo  , cnsctvo_cdfccn         ,
														cdgo_cdfccn                    , cnsctvo_cdgo_tpo_srvco, cnsctvo_pln            ,
														cdgo_pln                       , cnsctvo_tpo_pln       , cdgo_tpo_pln           ,
														cnsctvo_mdo_cntcto             , cdgo_mdo_cntcto       , fcha_slctd             ,
														nmro_unco_idntfccn_afldo       , edd_afldo_ans         , edd_afldo_mss          ,
														edd_afldo_ds                   , nmro_cntrto           , cnsctvo_cdgo_tpo_cntrto,
														usro                           , nmro_slctd_ss         , nmro_slctd_prvdr       ,
														cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn
													   )
			  Select     ss.cnsctvo_slctd_atrzcn_srvco     , ss.cnsctvo_srvco_slctdo  , ss.cnsctvo_cdgo_srvco_slctdo,
						 sso.cdgo_srvco_slctdo             , ss.cnsctvo_cdgo_tpo_srvco, af.cnsctvo_cdgo_pln         ,
						 afo.cdgo_pln                      , af.cnsctvo_cdgo_tpo_pln  , afo.cdgo_tpo_pln            , 
						 s.cnsctvo_cdgo_mdo_cntcto_slctd   , m.dscrpcn_mdo_cntcto     , s.fcha_slctd                ,
						 af.nmro_unco_idntfccn_afldo       , af.edd_afldo_ans         , af.edd_afldo_mss            ,
						 af.edd_afldo_ds                   , af.nmro_cntrto           , af.cnsctvo_cdgo_tpo_cntrto  ,
						 @usro_mdfccn                      , s.nmro_slctd_atrzcn_ss   , s.nmro_slctd_prvdr          ,
						 af.cnsctvo_cdgo_tpo_idntfccn_afldo, af.nmro_idntfccn_afldo
			  From       #tmpSolicitudFechaEntrega a  
			  Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)--Servicios
			  On         ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco And
						 ss.cnsctvo_srvco_slctdo       = a.cnsctvo_srvco_slctdo
			  Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios s With(NoLock)-- Solicitudes
			  On         s.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
			  Inner Join bdCNA.gsa.tbASServiciosSolicitadosOriginal sso With(NoLock)-- Servicios Solicitados Original
			  On         sso.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
			  Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios af With(NoLock)
			  On         af.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
			  Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal afo With(NoLock)
			  On         afo.cnsctvo_slctd_atrzcn_srvco = af.cnsctvo_slctd_atrzcn_srvco
			  Inner Join bdCNA.prm.tbASMediosContacto_Vigencias m With(NoLock)
			  On         m.cnsctvo_cdgo_mdo_cntcto = s.cnsctvo_cdgo_mdo_cntcto_slctd
			  Where      @fcha_actl Between m.inco_vgnca And m.fn_vgnca
		  
			  --Se inserta la informaci�n de las marcas.
			  Insert 
			  Into        #tmpMarcas(cnsctvo_slctd_srvco_sld_rcbda,	nmro_unco_idntfccn_afldo, mrca_incpcdd)
			  Select      a.cnsctvo_slctd_srvco_sld_rcbda, a.cnsctvo_srvco_slctdo, @mrca_incpcdd
			  From		  #tmpDatosSolicitudFechaEntrega a
			  Inner Join  bdSiSalud.dbo.tbMaestroIncapacidades b With(NoLock)
			  On          b.nmro_unco_idntfccn_ctznte = a.nmro_unco_idntfccn_afldo		
			  Inner Join  bdSiSalud.dbo.tbDetalleIncapacidad d WITH (NOLOCK)
			  On          d.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna And 
						  d.nmro_flo           = b.nmro_flo
			  Where		  b.cnsctvo_cdgo_estdo_sprco <> @cnsctvo_anulada /*ANULADA*/
			  And         @fcha_actl Between d.inco_lcnca And d.fn_lcnca;	   
		  
			  /*Inicio ejecuta las reglas de fecha entrega para calcular el grupo de entrega */   
		
			  Exec bdCNA.gsa.spASEjecutarReglasFechaEntrega @cnsctvo_prcso, @es_btch;

			  /*Fin ejecuta las reglas de fecha entrega para calcular el grupo de entrega */ 
		  
			  --Se realiza la agrupaci�n para el grupo de m�s prioridad, para todas las pretaciones.
			  Insert 
			  Into       #tempAgrupadorFechaEntrega(cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_srvco_slctdo, cnsctvo_cdgo_grpo_entrga,
													cdgo_grpo_entrga             , jrrqa
												   )
			  Select     t.cnsctvo_slctd_srvco_sld_rcbda, t.cnsctvo_srvco_slctdo, r.cnsctvo_cdgo_grpo_entrga, 
						 g.cdgo_grpo_entrga             , Min(r.jrrqa)
			  From       #tmpDatosSolicitudFechaEntrega t
			  Inner Join bdCNA.prm.tbASReglasFechaEntrega_Vigencias r With(NoLock)
			  On         r.cnsctvo_cdgo_grpo_entrga = t.cnsctvo_grpo_entrga 				
			  Inner Join bdCNA.prm.tbASGruposEntrega_Vigencias g With(NoLock)
			  On         g.cnsctvo_cdgo_grpo_entrga = r.cnsctvo_cdgo_grpo_entrga
			  Where      @fcha_actl Between r.inco_vgnca And r.fn_vgnca
			  And        @fcha_actl Between g.inco_vgnca And g.fn_vgnca
			  Group By   t.cnsctvo_slctd_srvco_sld_rcbda, t.cnsctvo_srvco_slctdo, 
						 r.cnsctvo_cdgo_grpo_entrga     , g.cdgo_grpo_entrga
					  
			  Update     up
			  Set        cnsctvo_grpo_entrga_fnl = tmp.cnsctvo_cdgo_grpo_entrga,
						 cdgo_grpo_entrga_fnl    = tmp.cdgo_grpo_entrga
			  From       #tmpDatosSolicitudFechaEntrega up 
			  Inner Join #tempAgrupadorFechaEntrega tmp
			  On         tmp.cnsctvo_slctd_srvco_sld_rcbda = up.cnsctvo_slctd_srvco_sld_rcbda And 
						 tmp.cnsctvo_srvco_slctdo          = up.cnsctvo_srvco_slctdo;          


			  --Se actualiza el consecutivo grupo de entrega
			  Update     a
			  Set        cnsctvo_cdgo_grpo_entrga = b.cnsctvo_grpo_entrga
			  From       #tmpSolicitudFechaEntrega a
			  Inner Join #tmpDatosSolicitudFechaEntrega b 
			  On         b.cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco And
						 b.cnsctvo_srvco_slctdo          = a.cnsctvo_srvco_slctdo

			  ---Se inserta la fecha de entrega
			  Insert 
			  Into       bdCNA.gsa.tbASResultadoFechaEntrega(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_cdgo_grpo_entrga,
															 fcha_estmda_entrga        , fcha_rl_entrga      , prrdd_admnstrtva        ,
															 fcha_crcn                 , usro_crcn           , fcha_ultma_mdfccn       ,
															 usro_ultma_mdfccn
															)
			  Select     Distinct a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo, a.cnsctvo_cdgo_grpo_entrga, 	
						 b.fcha_entrg_actlzr         , NULL                  , @prrdd_admnstrtva         ,
						 @fcha_actl                  , @usro_mdfccn          , @fcha_actl                ,
						 @usro_mdfccn
			  From       #tmpSolicitudFechaEntrega a 
			  Inner Join #tmpPrestacionesFechaEntregaXML b
			  On         a.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo		  
			  Where      b.mrca_actlzcn_srvco         = @mrca_no_actlzcn_srvco
			  And        a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_srvco		  
		  
			  --Se establece la informaci�n de la trazabilidad.
			  Insert 
			  Into    #tmpTrazaModificacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo  , cnsctvo_cncpto_prcdmnto_slctdo,
											cnsctvo_cdgo_mtvo_csa     , fcha_mdfccn           , orgn_mdfccn                   ,
											vlr_antrr                 , vlr_nvo               , usro_mdfccn                   ,
											obsrvcns                  , nmro_ip               , fcha_crcn                     ,
											usro_crcn                 , fcha_ultma_mdfccn     , usro_ultma_mdfccn             ,
											elmnto_orgn_mdfccn        , dto_elmnto_orgn_mdfccn, dscrpcn_vlr_antrr             ,
											dscrpcn_vlr_nvo
										   )
			  Select  @cnsctvo_slctd_srvco, cnsctvo_srvco_slctdo    , NULL        ,
					  @mtvo_mdfccn        , @fcha_actl              , @orgn_mdfccn,
					  @vlr_antrr_insrccn  , fcha_entrg_actlzr       , @usro_mdfccn,
					  @jstfccn_mdfccn     , @vlr_ip_cnstnte         , @fcha_actl  ,
					  @usro_mdfccn        , @fcha_actl              , @usro_mdfccn,
					  @elmnto_orgn_mdfccn , @dto_elmnto_orgn_insrccn, NULL        ,
					  fcha_entrg_actlzr
			  From    #tmpPrestacionesFechaEntregaXML pfe
			  Where   mrca_actlzcn_srvco = @mrca_no_actlzcn_srvco 
		  	      
		   End

		--Se ejecuta procedimiento que registra la trazabilidad de las modificaciones realizadas
		Exec bdCNA.gsa.spASGuardarTrazaModificacion

      	Set	@cdgo_rsltdo  = @codigo_ok
		Set @mnsje_rsltdo = @mensaje_ok
	

	End Try
	Begin Catch
		Set @mensaje_error = concat('Number:',  CAST(ERROR_NUMBER() AS char),  CHAR(13), 
			   				 'Line:', CAST(ERROR_LINE() AS char), CHAR(13),
							 'Message:', ERROR_MESSAGE(), CHAR(13),
							 'Procedure:', ERROR_PROCEDURE());
			
		Set @cdgo_rsltdo  =  @codigo_error;
		Set @mnsje_rsltdo =  @mensaje_error;
	End Catch


	Drop Table #tmpPrestacionesFechaEntregaXML
	Drop Table #tmpSolicitudFechaEntrega
    Drop Table #tmpTrazaModificacion
	Drop Table #tmpDatosSolicitudFechaEntrega
	Drop Table #tmpMarcas
	Drop Table #tempAgrupadorFechaEntrega
End