USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarUsuarioPAC]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarUsuarioPAC
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida si el usuario tiene PAC. D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 23/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarUsuarioPAC] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@cdgo_grpo_entrga CHAR(4),
@agrpa_prstcn udtLogico
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @fcha_actl datetime,
          @consecutivoPAC udtConsecutivo,
          @afirmacion char(1)
  SET @afirmacion = 'S'
  SET @fcha_actl = GETDATE()
  SET @consecutivoPAC = 2

  IF EXISTS (SELECT
      sol.id
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL)
  BEGIN

    --Se actualiza al grupo en caso de que el afiliado tenga contrato PAC.
    UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    INNER JOIN BDAfiliacionValidador.dbo.tbContratosValidador cv WITH (NOLOCK)
      ON cv.nmro_cntrto = sol.nmro_cntrto
      AND cv.cnsctvo_cdgo_tpo_cntrto = sol.cnsctvo_cdgo_tpo_cntrto
    INNER JOIN BDAfiliacionValidador.dbo.tbTiposContrato_Vigencias tcv WITH (NOLOCK)
      ON cv.cnsctvo_cdgo_tpo_cntrto = tcv.cnsctvo_cdgo_tpo_cntrto
      AND tcv.cnsctvo_cdgo_tpo_cntrto = @consecutivoPAC
    WHERE @fcha_actl BETWEEN tcv.inco_vgnca AND tcv.fn_vgnca
    AND sol.cnsctvo_grpo_entrga IS NULL;

    --Se determina si se debe realizar agrupación
    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
      AND @agrpa_prstcn = @afirmacion)
    BEGIN
      UPDATE sol
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @cdgo_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN #tmpDatosSolicitudFechaEntrega tmp
        ON tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
      WHERE tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga

    END

  END
END

GO
