USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarModificacionDireccionamientoSolicitud]    Script Date: 23/08/2017 08:21:59 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarModificacionDireccionamientoSolicitud 
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos      A\>    
* Descripcion        : <\D 
                            Procedimiento que permite realizar el cambio de direccionamiento del servicio y la reliquidacion
							del mismo  
						D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 15/05/2017 FC\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\DM Se agrega llamado a procedimiento que elimina los conceptos de gasto, si la prestacion tiene conceptos. DM\>
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 19/07/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------------*/
/*
BEGIN
	DECLARE @cdgo_rsltdo  INT, 
	        @mnsje_rsltdo VARCHAR(2000)
	EXEC bdCNA.gsa.spASGuardarModificacionDireccionamientoSolicitud 135150             , 76226               , '00267'             ,
	                                                                'qvisionclr'       , 28                  , 'Cambio por que si', 
																	@cdgo_rsltdo OUTPUT, @mnsje_rsltdo OUTPUT
	SELECT @cdgo_rsltdo, @mnsje_rsltdo
END
*/
ALTER PROCEDURE [gsa].[spASGuardarModificacionDireccionamientoSolicitud] 
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo       ,
	@cnsctvo_srvco_slctdo       udtConsecutivo       ,
	@cdgo_intrno                udtCodigoIps         ,
	@usro                       udtusuario           ,
	@id_csa                     udtConsecutivo       ,
	@obsrvcns                   udtdescripcion       ,
	@cdgo_rsltdo                udtconsecutivo Output,
	@mnsje_rsltdo               Varchar(2000)  Output
AS
BEGIN
   SET NOCOUNT ON 
   
   DECLARE  @indx          Int           ,
            @ttl           Int           ,
            @xml_in        Xml           ,
            @parameter_sp  NVarchar(Max) ,
            @codigo_ok     Int           ,
            @codigo_error  Int           ,
            @mnsje         Varchar(1000) ,
            @cdgo_error    Varchar(100)  ,
            @ms_exto       Varchar(50)   ,
            @mnsje_lqdcn   Varchar(2000) ,		
            @mnsje_prstcns Varchar(100)  ,
            @mnsjae_exto   Varchar(100)  ,
			@cdgo_err      Varchar(2)    , 
			@separator     udtLogico      

    Create 
	Table  #tmpprestacionesops(id                   udtconsecutivo Identity(1, 1),
                               cnsctvo_srvco_slctdo udtconsecutivo
                              )  

    Set @indx          = 1
    Set @codigo_ok     = 0
    Set @codigo_error  = -1
    Set @ms_exto       = 'OK'
    Set @mnsje_lqdcn   = 'LIQUIDACION'
    Set @mnsje_prstcns = 'No se encontró información de las prestaciones para el número de OPS y solicitud enviado.'
    Set @mnsjae_exto   = 'Proceso ejecutado satisfactoriamente.'
	Set @cdgo_err      = 'ET'
	Set @separator     = '-'


	BEGIN TRY

      --Se consultan las prestaciones asociadas a la solicitud.
	  --Inserción de información de los servicios
      Insert 
	  Into       #tmpprestacionesops(cnsctvo_srvco_slctdo)
      Select     b.cnsctvo_srvco_slctdo     	             
      From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios a With(NoLock)	
      Inner Join bdcna.gsa.tbASServiciosSolicitados b WITH (NOLOCK)
      On         b.cnsctvo_slctd_atrzcn_srvco =  a.cnsctvo_slctd_atrzcn_srvco  
	  Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
      And        b.cnsctvo_srvco_slctdo       = @cnsctvo_srvco_slctdo
     	  

      --Se realiza la iteración de la información por prestación, para cambiar el direccionamiento.
      Select @ttl = Count(1) From #tmpprestacionesops

      If @ttl > 0
         Begin
	       While @indx <= @ttl
             Begin
                --Se construye el XML de entrada
                Set @xml_in = (Select @cdgo_intrno             codigoInterno        ,
									  gestionDomiciliariaDireccionamiento.cnsctvo_srvco_slctdo consecutivoPrestacion,
									  @usro                    usuarioGestion       ,
									  @obsrvcns                justificacion        ,
			              			  (Select Top 1 @id_csa
									   From   #tmpprestacionesops codigoMotivo
									   FOR XML AUTO, TYPE, ELEMENTS
									  )motivos			
								From #tmpprestacionesops gestionDomiciliariaDireccionamiento
								Where gestionDomiciliariaDireccionamiento.id = @indx
								FOR XML AUTO, TYPE, ELEMENTS)

			   Set @parameter_sp = CONVERT(NVARCHAR(MAX), @xml_in)
			   
			   --Se guarda la información del direccionamiento.
			   Exec bdCNA.gsa.spasguardarinformaciondireccionamientodomi @parameter_sp       ,
																         @cdgo_rsltdo  Output,
																         @mnsje_rsltdo Output
			   If @cdgo_rsltdo = @codigo_error
                  Begin
		             RAISERROR (@mnsje_rsltdo, 16, 2) WITH SETERROR
		          End

			    Set @indx = @indx + 1
			 End		  

			-- Se llama al procedimientos encargado de realizar la anulacion de cuotas.
			Exec bdCNA.gsa.spASAnulacionCuotasCambioDireccionamiento @usro;

			-- Se eliminan los conceptos de gasto si existen.
			Exec bdCNA.gsa.spAsEliminarConceptosGastoCambioDireccionamiento

		     --Se ejecuta la re-liquidación
		     --Se construye el XML de entrada

			 
		
		     Set @xml_in = (Select Top 1 @usro usro,
			                            (SELECT TOP 1 @cnsctvo_slctd_atrzcn_srvco
			                             FROM #tmpprestacionesops cnsctvo_slctd
			                             FOR XML AUTO, TYPE, ELEMENTS) idnt_slctds
			                From #tmpprestacionesops cdgs_slctds
			                FOR XML AUTO, TYPE, ELEMENTS
					        )

		     Set @parameter_sp = CONVERT(NVARCHAR(MAX), @xml_in)

		     --Se ejecuta el proceso de liquidación
		     Exec bdCNA.gsa.spasejecutarprocesoliquidacion @parameter_sp     ,
													       @mnsje      Output,
													       @cdgo_error Output

		     --Se valida que la respuesta sea exitosa
		     If @cdgo_error <> @ms_exto
		        Begin
			      Set @mnsje_lqdcn = @mnsje_lqdcn + @separator + @mnsje
			      RAISERROR (@mnsje_lqdcn, 16, 2) WITH SETERROR
                End
	     End
      Else
		  Begin
			Set @cdgo_rsltdo  = @codigo_error
			Set @mnsje_rsltdo = @mnsje_prstcns
		  End

      SET @cdgo_rsltdo = @codigo_ok
      SET @mnsje_rsltdo = @mnsjae_exto     
  END TRY
  BEGIN CATCH
    SET @mnsje_rsltdo = 'Number:'    + CAST(ERROR_NUMBER() AS CHAR) + CHAR(13) +
						'Line:'      + CAST(ERROR_LINE() AS CHAR) + CHAR(13) +
						'Message:'   + ERROR_MESSAGE() + CHAR(13) +
						'Procedure:' + ERROR_PROCEDURE();
    SET @cdgo_rsltdo = @codigo_error

  END CATCH
END
