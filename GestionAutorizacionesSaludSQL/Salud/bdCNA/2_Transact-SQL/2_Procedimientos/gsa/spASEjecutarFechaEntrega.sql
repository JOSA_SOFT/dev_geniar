USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEjecutarFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento expuesto que inicia la ejecución del calculo de la fecha   D\>
            			<\D esperada de entrega													    D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASEjecutarFechaEntrega] @cdgs_slctds nvarchar(max),
@rsltdo_mrcs xml,
@usro varchar(50),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(2000) OUTPUT,
@msje_rspsta nvarchar(max) OUTPUT
AS
BEGIN

  SET NOCOUNT ON

   --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  DECLARE @negacion char(1),
          @codigoError char(2),
          @xml xml,
          @xmlRespuesta xml,
		  @tipoProceso udtCodigo,
          @tipoTransaccionCrear udtLogico,
          @tipoTransaccionActualizar udtLogico,
		  @estadoControl char(2),
		  @codigoExito char(2) = 'OK',@en_prcso udtLogico,
		  @mnsje_ejccn VARCHAR(50) = 'Hay solicitudes que se encuetran en ejecución.',
		  @xstate int,
		  @trancount INT

  SET @negacion = 'N'
  SET @codigoError = 'ET'
  SET @tipoProceso = 'FE'
  SET @tipoTransaccionCrear = 'C'
  SET @tipoTransaccionActualizar = 'A'

 BEGIN TRY

	 
   SET @xml = CONVERT(xml, @cdgs_slctds)

	 --Se obtiene la información básica de la solicitud.
	EXEC bdCNA.gsa.spASObtenerDatosBasicos @xml

	EXEC gsa.spASFechaEntrega @rsltdo_mrcs,
								@usro,
								@negacion,
								@estdo_ejccn OUTPUT,
								@msje_errr OUTPUT,
								@xmlRespuesta OUTPUT

	SET @msje_rspsta = CONVERT(nvarchar(max), @xmlRespuesta)

	
		--Control de proceso de ejecución de fecha de entrega
	EXEC bdCNA.gsa.[spASControlProcesoSolicitud] @tipoProceso,
												@tipoTransaccionCrear,
												@usro,
												NULL,
												@estadoControl OUTPUT

  END TRY
  BEGIN CATCH
    SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
    'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
    'Message:' + ERROR_MESSAGE() + CHAR(13) +
    'Procedure:' + ERROR_PROCEDURE();
    SET @estdo_ejccn = @codigoError;
    SET @msje_rspsta = NULL

  END CATCH

IF (OBJECT_ID('#tmpNmroSlctds') IS NOT NULL)
  DROP TABLE #tmpNmroSlctds
END

GO
