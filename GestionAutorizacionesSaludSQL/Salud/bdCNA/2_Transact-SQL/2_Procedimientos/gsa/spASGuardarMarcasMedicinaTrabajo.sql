USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarMarcasMedicinaTrabajo]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
	Set @xml ='<marcasMedicinaTrabajo>
		     <prestacion>
		       <codigoprestacion>1</codigoprestacion>
	           <marcaatel>1</marcaatel>	
			   <usuario>setijrl</usuario>	  
	         </prestacion>
	         <prestacion>
	           <codigoprestacion>2</codigoprestacion>	
	           <marcaatel>1</marcaatel>	
			   <usuario>setijrl</usuario>	   
             </prestacion>
	         	         
		   </marcasMedicinaTrabajo>';
	
*/

ALTER PROCEDURE [gsa].[spASGuardarMarcasMedicinaTrabajo]
@consecutivos_prestaciones	nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @xml xml;
	Set @xml = @consecutivos_prestaciones;

	Create Table #marcasMedicinaTrabajo
	(
		cdgo_cdfccn				udtConsecutivo,							
		cnsctvo_cdgo_tpo_mrca	udtConsecutivo,           
        usro_crcn				udtUsuario,
		estdo					int default 0							
	)


	Insert Into #marcasMedicinaTrabajo (cdgo_cdfccn, cnsctvo_cdgo_tpo_mrca, usro_crcn)
	SELECT pref.value('(codigoprestacion/text())[1]','udtConsecutivo')       AS codigoPrestacion,
           pref.value('(marcaatel/text())[1]', 'udtConsecutivo') AS marcaAtel,
		   pref.value('(usuario/text())[1]', 'udtUsuario') AS usuario
    FROM   @xml.nodes('/marcasMedicinaTrabajo//prestacion') AS xml_slctd(Pref)

	
	--Se elimina cualquier registro que se encuentra en la BD para esa prestación y se inserta lo nuevo
	Delete mss from BDCna.gsa.tbASMarcasServiciosSolicitados mss WITH(NOLOCK)
	inner Join #marcasMedicinaTrabajo mmt on mmt.cdgo_cdfccn = mss.cnsctvo_srvco_slctdo


	insert into BDCna.gsa.tbASMarcasServiciosSolicitados
	(		
		cnsctvo_srvco_slctdo,
		cnsctvo_cdgo_tpo_mrca,
		usro_crcn,
		fcha_crcn,
		fcha_ultma_mdfccn,
		usro_ultma_mdfccn
	)
	select cdgo_cdfccn, cnsctvo_cdgo_tpo_mrca, usro_crcn, getDate(), getDate(), usro_crcn
	from #marcasMedicinaTrabajo 
		
	drop table #marcasMedicinaTrabajo;
	
END

GO
