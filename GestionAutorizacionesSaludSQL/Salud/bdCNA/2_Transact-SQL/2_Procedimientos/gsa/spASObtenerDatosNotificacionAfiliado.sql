USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerDatosNotificacionAfiliado]    Script Date: 10/03/2017 12:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASObtenerDatosNotificacionAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS A\>
* Descripcion			: <\D 
                              Carga la información del afiliado y del proveedor en las tablas temporales 
                              para el procesamiento del envío de la notificación						
						   D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2015/12/29 FC\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jonathan Chamizo - SETI SAS	AM\>
* Descripcion			 : <\DM	
                                Se actualiza el Sp para que procese múltiples 
								solicitudes										
							DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/03/23 FM\>
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jorge Rodriguez De León	AM\>
* Descripcion			 : <\DM	
                                Se actualiza el Sp para que consulte parametros 
								de sistemas requeridos para generar las plantillas	
							DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/01/06 FM\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para consultar el email del afiliado
								cuando en la solicitudes no tenga asociado alguno, con el fin de
								enviar correo de notificacion
							DM\>
* Nuevos Parametros	 	 : <\PM PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/03/09 FM\>
*------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASObtenerDatosNotificacionAfiliado] 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@fcha_actl					DATETIME   ,
			@valorCero					INT        ,
			@cdgo_prmtro_gnrl			CHAR(3)    ,
			@cdgo_prmtro_gnrl_vgnca		CHAR(3)    ,
			@vsble_usro                 udtLogico  ,
			@vlr_prmtro_nmrco           Numeric(18),
		    @vlr_prmtro_crctr           Char(200)  ,
		    @vlr_prmtro_fcha            Datetime   ,
			@tpo_dto_prmtro		        udtLogico  ,
			@cdgo_prmtro_gnrl_ln		CHAR(3)    ,
			@cdgo_prmtro_gnrl_vgnca_ln	CHAR(3)    ,
			@cdgo_prmtro_gnrl_ac		CHAR(3)    ,
			@cdgo_prmtro_gnrl_vgnca_ac	CHAR(3)    ,
			@lcVacio                    udtLogico


	Set @fcha_actl				   = GETDATE()
	Set @valorCero				   = 0
	Set	@cdgo_prmtro_gnrl		   = 111
	Set	@cdgo_prmtro_gnrl_vgnca	   = 111
	Set	@vsble_usro				   = 'S'
	Set	@tpo_dto_prmtro			   = 'C'
	Set	@cdgo_prmtro_gnrl_ln	   = 112
	Set	@cdgo_prmtro_gnrl_vgnca_ln = 112
	Set	@cdgo_prmtro_gnrl_ac	   = 92
	Set	@cdgo_prmtro_gnrl_vgnca_ac = 92
	Set @lcVacio                   = ' '
			   	

	Insert 
	Into   #tmpAflds(id_sld_rcbda            , nmbre_afldo                  , cnsctvo_cdgo_tpo_idntfccn_afldo         ,  
					 nmro_idntfccn_afldo     , cnsctvo_cdgo_tpo_cntrto      , nmro_cntrto                             ,						
					 cnsctvo_cdgo_pln        , nmro_unco_idntfccn_afldo     , cnsctvo_cdgo_rngo_slrl                  ,			
					 cdgo_ips_prmra          , cnsctvo_bnfcro_cntrto        , cnsctvo_cdgo_sde_ips_prmra              ,		
					 cnsctvo_cdgo_estdo_drcho, cdgo_tpo_idntfccn_afldo      , eml                                     ,
					 sde_afldo               , cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
				    )
	Select     tso.id_sld_rcbda               , CONCAT(RTRIM(AFO.prmr_nmbre_afldo) , @lcVacio, 
								                       RTRIM(AFO.sgndo_nmbre_afldo), @lcVacio,
								                       RTRIM(AFO.prmr_aplldo_afldo), @lcVacio,
								                       RTRIM(AFO.sgndo_aplldo_afldo)
								                      )                                      , afp.cnsctvo_cdgo_tpo_idntfccn_afldo         ,	
		       afp.nmro_idntfccn_afldo       , afp.cnsctvo_cdgo_tpo_cntrto                   , afp.nmro_cntrto	                           ,					
		       afp.cnsctvo_cdgo_pln          , afp.nmro_unco_idntfccn_afldo                  , afp.cnsctvo_cdgo_rngo_slrl                  ,				
		       afp.cdgo_ips_prmra	         , afp.cnsctvo_bnfcro_cntrto                     , afp.cnsctvo_cdgo_sde_ips_prmra	           ,		
			   afp.cnsctvo_cdgo_estdo_drcho  , afo.cdgo_tpo_idntfccn_afldo                   , s.eml                                       ,
			   afp.cnsctvo_cdgo_sde_ips_prmra, afp.cnsctvo_slctd_atrzcn_srvco                , afp.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	From	   #tmpSlctdes tso 
	Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios afp WITH (NOLOCK)
	On	       tso.cnsctvo_slctd_srvco_sld_rcbda = afp.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal afo WITH (NOLOCK)
	On	       afo.cnsctvo_slctd_atrzcn_srvco               = afp.cnsctvo_slctd_atrzcn_srvco              And	
	           afo.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	= AFP.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	
	Inner Join bdSeguridad.dbo.tbRegistroUsuariosWeb s WITH (NOLOCK)
	On         s.nmro_unco_idntfccn_usro = afp.nmro_unco_idntfccn_afldo
	
	--Se actualiza correo del afiliado de la informacion del afiliado que llega en la solicitud
	Update     #tmpAflds
	Set        eml = afo.crro_elctrnco_afldo
	From       #tmpAflds afp
	Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal afo WITH (NOLOCK)
	On	       afo.cnsctvo_slctd_atrzcn_srvco               = afp.cnsctvo_slctd_srvco_sld_rcbda            And	
	           afo.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	= afp.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	Where      afp.eml Is Null Or afp.eml = @lcVacio

	Update		taf
	Set			cnsctvo_cdgo_estdo_drcho = ISNULL(ede.cnsctvo_cdgo_estdo_drcho, @valorCero),
				dscrpcn_csa_drcho		 = cdv.dscrpcn_csa_drcho
	From		#tmpAflds	taf 
	Inner Join	#tmpSlctdes	tso 
	On	        tso.id_sld_rcbda = taf.id_sld_rcbda
	Inner Join	bdAfiliacionValidador.dbo.TbMatrizDerechosValidador	ede	WITH(NOLOCK) 
	On 	        ede.cnsctvo_cdgo_tpo_cntrto = taf.cnsctvo_cdgo_tpo_cntrto And 
	            ede.nmro_cntrto             = taf.nmro_cntrto             And
				ede.cnsctvo_bnfcro          = taf.cnsctvo_bnfcro_cntrto
	Inner Join	bdAfiliacionValidador.dbo.tbCausasDerechoValidador cdv WITH(NOLOCK) 
	On	        cdv.cnsctvo_cdgo_estdo_drcho = ede.cnsctvo_cdgo_estdo_drcho And	
	            cdv.cnsctvo_cdgo_csa_drcho   = ede.cnsctvo_cdgo_csa_drcho	 
	Where		@fcha_actl Between ede.inco_vgnca_estdo_drcho And ede.fn_vgnca_estdo_drcho
	And         @fcha_actl Between cdv.inco_vgnca And cdv.fn_vgnca;
	
	Update		taf
	Set			dscrpcn_pln = PLA.dscrpcn_pln
	From		#tmpAflds	taf 
	Inner Join	#tmpSlctdes	tso 
	ON	        tso.id_sld_rcbda = taf.id_sld_rcbda												
	Inner Join	bdAfiliacionValidador.dbo.tbPlanes_Vigencias pla WITH (NOLOCK)
	ON	        pla.cnsctvo_cdgo_pln = taf.cnsctvo_cdgo_pln
	WHERE		@fcha_actl Between pla.inco_vgnca And pla.fn_vgnca	


	Insert 
	Into	   #tmpPrvdor(id_sld_rcbda, crreo)
	Select     tso.id_sld_rcbda, DP.crro_elctrnco 
	From	   bdSisalud.dbo.tbDireccionesPrestador dp WITH(NOLOCK) 
	Inner Join #tmpAflds taf 
	On	       taf.cdgo_ips_prmra = dp.cdgo_intrno
	Inner Join #tmpSlctdes tso
	ON         tso.id_sld_rcbda = taf.id_sld_rcbda
	Where 	   @fcha_actl Between DP.inco_vgnca And DP.fn_vgnca;

		--Actualiza la descripcion  de la sede de la IPS
	Update	   taf
	Set		   dscrpcn_sde = s.dscrpcn_sde
	From	   #tmpAflds taf
	Inner Join BDAfiliacionValidador.dbo.tbSedes_Vigencias s With(NoLock) 
	On		   s.cnsctvo_cdgo_sde = TAF.sde_afldo
	Where      @fcha_actl Between s.inco_vgnca And s.fn_vgnca

	--Se obtiene parametro para tag bcc
	Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl, 
												@vsble_usro, 
												@cdgo_prmtro_gnrl_vgnca, 
												@vsble_usro, 
												@tpo_dto_prmtro, 
												@vlr_prmtro_nmrco Output, 
												@vlr_prmtro_crctr Output, 
												@vlr_prmtro_fcha  Output;

	Update #tmpAflds
	Set	   bcc = RTRIM(LTRIM(@vlr_prmtro_crctr))
	
	--Se obtiene parametro para la linea nacional
	Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl_ln, 
												@vsble_usro, 
												@cdgo_prmtro_gnrl_vgnca_ln, 
												@vsble_usro, 
												@tpo_dto_prmtro, 
												@vlr_prmtro_nmrco Output, 
												@vlr_prmtro_crctr Output, 
												@vlr_prmtro_fcha Output;

	Update	#tmpAflds
	Set		lnea_ncnal = RTRIM(LTRIM(@vlr_prmtro_crctr))
	

	--Se obtiene parametro para correo de atención al cliente
	Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl_ac, 
												@vsble_usro, 
												@cdgo_prmtro_gnrl_vgnca_ac, 
												@vsble_usro, 
												@tpo_dto_prmtro, 
												@vlr_prmtro_nmrco Output, 
												@vlr_prmtro_crctr Output, 
												@vlr_prmtro_fcha Output;
	
	UPDATE	#tmpAflds
	SET		crreo_atncn_clnte =	RTRIM(LTRIM(@vlr_prmtro_crctr))	
End

