USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASMarcaServiciosOnlineReLiquidacion]    Script Date: 28/03/2017 17:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASMarcaServiciosOnlineReLiquidacion
* Desarrollado por		: <\A Ing. Jonathan - SETI SAS	  						A\>
* Descripcion			: <\D Se marcan los servicios online que se van a
							  reliquidar										D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													P\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 15/06/2016										FC\>
*
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se realiza modificacion al procedimiento para que no marque las prestaciones que tiene
								numero unico de OPS mayor a 0
                            DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	28/03/2017 FM\>
*------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASMarcaServiciosOnlineReLiquidacion] 
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @mrca_rlqdcn udtlogico = 'S'; 
   
  --Se marcan los procedimientos que se deben reliquidar	
  Update	  CS
  Set		  cnsctvo_prcdmnto_insmo_slctdo = CSS.cnsctvo_prcdmnto_insmo_slctdo,
			  nmro_unco_ops                 = CSS.nmro_unco_ops,
			  mrca_rlqdcn	                = @mrca_rlqdcn
  From		  #cnslddo_slctud CS
  Inner Join  GSA.tbASProcedimientosInsumosSolicitados PIS With(NoLock)
  On		  PIS.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
  Inner Join  gsa.tbASConceptosServicioSolicitado CSS  With(NoLock)
  On		  CSS.cnsctvo_prcdmnto_insmo_slctdo = PIS.cnsctvo_prcdmnto_insmo_slctdo
  Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL

  --Se marcan los medicamentos que se deben reliquidar	
  Update	  CS
  Set		  cnsctvo_mdcmnto_slctdo = CSS.cnsctvo_mdcmnto_slctdo,
			  nmro_unco_ops          = CSS.nmro_unco_ops,
			  mrca_rlqdcn	         = @mrca_rlqdcn
  From		  #cnslddo_slctud CS
  Inner Join  gsa.tbASMedicamentosSolicitados MS  With(NoLock)
  On		  MS.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
  Inner Join  gsa.tbASConceptosServicioSolicitado CSS  With(NoLock)
  On		  CSS.cnsctvo_mdcmnto_slctdo = MS.cnsctvo_mdcmnto_slctdo
  Where       CSS.nmro_unco_ops = 0 OR CSS.nmro_unco_ops IS NULL      		
END
