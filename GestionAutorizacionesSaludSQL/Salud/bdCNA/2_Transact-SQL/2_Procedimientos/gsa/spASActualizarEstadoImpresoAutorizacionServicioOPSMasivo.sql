USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEstadoImpresoAutorizacionServicioOPSMasivo]    Script Date: 11/05/2017 02:12:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASActualizarEstadoImpresoAutorizacionServicioOPSMasivo
* Desarrollado por   : <\A Ing. Juan Carlos Vásquez G.  A\>    
* Descripcion        : <\D Procedmiento que actualiza el estado de las autorizaciones masivamente
                           
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P @cdgo_rsltdo,@mnsje_rsltdo
                           
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 2017-03-31 FC\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andrés López Ramírez - qvisionclr AM\>    
* Descripcion        : <\D Se modifica proceso para recuperar la información para prestaciones con
						   varios conceptos. D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-05-11 FM\>
*----------------------------------------------------------------------------------------------------------------------------*/

/*
  Declare @cdgo_rsltdo char(2), @mnsje_rsltdo Varchar(150)
  exec bdcna.gsa.spASActualizarEstadoImpresoAutorizacionServicioOPSMasivo @cdgo_rsltdo output,@mnsje_rsltdo output
  Select concat(@cdgo_rsltdo,' ', @mnsje_rsltdo)
*/

ALTER PROCEDURE [gsa].[spASActualizarEstadoImpresoAutorizacionServicioOPSMasivo] 

/*01*/ @cdgo_rsltdo								char(2) = null	output,
/*02*/ @mnsje_rsltdo							UdtDescripcion = null output 

AS

Begin
   SET NOCOUNT ON

   Declare  @ldFcha_Actl						Datetime  = getdate(),
		    @lnCnsctvo_cdgo_estdo_atrzda		int = 11, --Estado de la prestacion AUTORIZADA
		    @lncnsctvo_cdgo_tpo_orgn_slctd3	int = 3, -- Tipo origen solicitud es OPS VIRTUAL
		    @lnCnsctvo_cdgo_estdo			UdtConsecutivo = 15, -- Entregada
		    @usro_imprsn                     UdtUsuario = 'proc_progentrega',
			@mensajeExito					char(30) = 'Ejecución Exitosa',
			@mensajeError					varchar(2000) = 'Se Presento Un Error al Crear la(s) Solicitud(es) con Programación Entrega',
			@mnsje_lg_evnto_prcso			Varchar(500),
			@cnsctvo_cdgo_tpo_incnsstnca int = 1,
			@cnsctvo_estdo_err				udtConsecutivo = 3,
			@cnsctvo_lg						int = 0,
			@tpo_rgstro_log_147				int = 147,
			@codigoExito					char(2) = 'OK',
			@codigoError					char(2) = 'ET',
			@tpo_prcso						int = 36, -- 'Proceso Automático Cambio de Estado Entregado Solicitudes Ops Virtual Masivo'
			@cnsctvo_prcso					int,
			@lcUsuario						UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

   Create Table #TmpSolicitudes
   (
		id									int identity,
		cnsctvo_slctd_atrzcn_srvco			UdtConsecutivo,
		cnsctvo_srvco_slctdo				udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo		UdtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo		udtConsecutivo,
		cnsctvo_mdcmnto_slctdo				udtConsecutivo,
		cnsctvo_cdgo_estdo_srvco_slctdo		UdtConsecutivo,
		cnsctvo_cdgo_tpo_orgn_slctd			UdtConsecutivo,
		fcha_utlzcn_dsde					datetime,
		fcha_utlzcn_hsta					datetime,
		ds_vldz								int,
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	UdtConsecutivo,
		cnsctvo_cdgo_ofcna                  UdtConsecutivo,

   )

	-- Se recupera la informacion para los procedimientos-insumos
    Insert		#TmpSolicitudes
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_cdgo_tpo_orgn_slctd,			    cnsctvo_srvco_slctdo,
				cnsctvo_cdgo_estdo_srvco_slctdo,		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,			cnsctvo_prcdmnto_insmo_slctdo,
				cnsctvo_cncpto_prcdmnto_slctdo,			ds_vldz
	)
	Select 
				sa.cnsctvo_slctd_atrzcn_srvco,			sa.cnsctvo_cdgo_tpo_orgn_slctd,				ss.cnsctvo_srvco_slctdo,
				ss.cnsctvo_cdgo_estdo_srvco_slctdo,		ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		p.cnsctvo_prcdmnto_insmo_slctdo,
				cs.cnsctvo_cncpto_prcdmnto_slctdo,		isnull(cs.ds_vldz,0)
	From		bdcna.gsa.tbASSolicitudesAutorizacionServicios sa with(nolock)
	Inner join	bdcna.gsa.tbASServiciosSolicitados ss with(nolock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Inner Join	bdcna.gsa.tbASProcedimientosInsumosSolicitados p with(nolock)
    On			p.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo    
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado cs with(nolock)    
    On			cs.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
	Where		sa.cnsctvo_cdgo_tpo_orgn_slctd	=	@lncnsctvo_cdgo_tpo_orgn_slctd3
	And         ss.cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo_atrzda
	Group By	sa.cnsctvo_slctd_atrzcn_srvco,			sa.cnsctvo_cdgo_tpo_orgn_slctd,				ss.cnsctvo_srvco_slctdo,
				ss.cnsctvo_cdgo_estdo_srvco_slctdo,		ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		p.cnsctvo_prcdmnto_insmo_slctdo,
				cs.cnsctvo_cncpto_prcdmnto_slctdo,		cs.ds_vldz

	-- Se recupera la informacion para los medicamentos
	Insert		#TmpSolicitudes
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_cdgo_tpo_orgn_slctd,			    cnsctvo_srvco_slctdo,
				cnsctvo_cdgo_estdo_srvco_slctdo,		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,			cnsctvo_mdcmnto_slctdo,
				cnsctvo_cncpto_prcdmnto_slctdo,			ds_vldz
	)
	Select 
				sa.cnsctvo_slctd_atrzcn_srvco,			sa.cnsctvo_cdgo_tpo_orgn_slctd,				ss.cnsctvo_srvco_slctdo,
				ss.cnsctvo_cdgo_estdo_srvco_slctdo,		ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		m.cnsctvo_mdcmnto_slctdo,
				cs.cnsctvo_cncpto_prcdmnto_slctdo,		isnull(cs.ds_vldz,0)
	From		bdcna.gsa.tbASSolicitudesAutorizacionServicios sa with(nolock)
	Inner join	bdcna.gsa.tbASServiciosSolicitados ss with(nolock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdcna.gsa.tbASMedicamentosSolicitados m with(nolock) 
    On         m.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo    
	Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado cs with(nolock)    
    On         cs.cnsctvo_mdcmnto_slctdo = m.cnsctvo_mdcmnto_slctdo
	Where		sa.cnsctvo_cdgo_tpo_orgn_slctd = @lncnsctvo_cdgo_tpo_orgn_slctd3
	And         ss.cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo_atrzda
	Group By	sa.cnsctvo_slctd_atrzcn_srvco,			sa.cnsctvo_cdgo_tpo_orgn_slctd,				ss.cnsctvo_srvco_slctdo,
				ss.cnsctvo_cdgo_estdo_srvco_slctdo,		ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		m.cnsctvo_mdcmnto_slctdo,
				cs.cnsctvo_cncpto_prcdmnto_slctdo,		cs.ds_vldz

	-- Se actualizan las fechas de utilizacion.
	Update  t
	Set		fcha_utlzcn_dsde = @ldFcha_Actl,
			fcha_utlzcn_hsta = @ldFcha_Actl + ds_vldz
	From    #TmpSolicitudes t

	-- Registramos el inicio proceso automático log de procesos "36- Proceso Automático Cambio de Estado Entregado Solicitudes Ops Virtual Masivo"
	exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output
	Begin transaction
	Begin Try
		set @mnsje_lg_evnto_prcso = 'Paso 1. Actualizacion estado Entregado Autorizacion servicios OPS Masivo'
		exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_147,@cnsctvo_lg output

		--Se actualizan las fechas de utilizacion desde y hasta en la tabla transaccional
		Update     cs
		Set        fcha_utlzcn_dsde = t.fcha_utlzcn_dsde,
				   fcha_utlzcn_hsta = t.fcha_utlzcn_hsta,
				   cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCnsctvo_cdgo_estdo,
				   fcha_ultma_mdfccn = @ldFcha_Actl,
				   usro_ultma_mdfccn = @usro_imprsn
		From       #TmpSolicitudes t
		Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado cs WITH(NOLOCK) 
		On         cs.cnsctvo_cncpto_prcdmnto_slctdo = t.cnsctvo_cncpto_prcdmnto_slctdo
		Where      cs.fcha_utlzcn_dsde IS NULL 
		And        cs.fcha_utlzcn_hsta IS NULL
		And        cs.nmro_unco_ops is not null

		--Se consulta la oficina del usuario que imprime la OPS
		Update		t
		Set			cnsctvo_cdgo_ofcna = p1.cnsctvo_cdgo_ofcna
		From		#TmpSolicitudes t
		Inner Join	bdSisalud.dbo.tbDetProgramacionFechaEvento p with(nolock)
		On			p.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
		Inner Join	BdSiSalud.dbo.tbProgramacionPrestacion p1 with(nolock)
		On			p1.cnsctvo_prgrmcn_prstcn = p.cnsctvo_prgrmcn_prstcn 

		--Se actualizan la fecha de impresion y estado a entregada del servicio entregado
		Update     ss
		Set        fcha_imprsn                     = @ldFcha_Actl,
				   cnsctvo_cdgo_estdo_srvco_slctdo = @lnCnsctvo_cdgo_estdo,
				   cnsctvo_cdgo_ofcna_imprsn_ops   = t.cnsctvo_cdgo_ofcna,
				   usro_imprsn_ops                 = @usro_imprsn,
				   fcha_ultma_mdfccn               = @ldFcha_Actl,
				   usro_ultma_mdfccn               = @usro_imprsn
		From       bdcna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)            
		Inner Join #TmpSolicitudes t 
		On         t.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
		And        t.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo  
		Where      ss.fcha_imprsn IS NULL
		 
		set @cdgo_rsltdo = @codigoExito
		set @mnsje_rsltdo = @mensajeExito
		
		exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
		-- Registramos el fin del proceso Automático log de procesos "36- Proceso Automático Cambio de Estado Entregado Solicitudes Ops Virtual Masivo"
		exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,2
		
		commit transaction

	End Try
	Begin Catch
		Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
		set @cdgo_rsltdo = @codigoError
		set @mnsje_rsltdo = @mensajeError
		RollBack transaction;
		Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
		Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err;
		throw;
	End Catch
	
	Drop Table #TmpSolicitudes
End