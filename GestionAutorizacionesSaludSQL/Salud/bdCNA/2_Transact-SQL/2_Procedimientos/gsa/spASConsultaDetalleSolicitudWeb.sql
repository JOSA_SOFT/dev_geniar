USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDetalleSolicitudWeb]    Script Date: 18/05/2017 7:46:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaDetalleSolicitudWeb
* Desarrollado por   : <\A Luis Felipe Aguirre Ramirez (SETI)	A\>    
* Descripcion        : <\D 
                           Consulta informacion del detalle de la solicitud  
					   D\>	  
* Observaciones   	 : <\O O\>    
* Parametros         : <\P @nmro_slctd_atrzcn_ss P\>   
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 18/03/2016           FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                             Se modifica el procedimiento para que consulte las IPS cuando 
							 la programacion de entrega no guarda el codigo interno 
							 generico
                        DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 18/05/2017  FM\>    
*----------------------------------------------------------------------------------------------------*/
---exec [gsa].[spASConsultaDetalleSolicitudWeb] '2016-01-00000655'
---exec [gsa].[spASConsultaDetalleSolicitudWeb] '2016-04-00002153'
---exec [gsa].[spASConsultaDetalleSolicitudWeb] '2017-05-00002916'

ALTER PROCEDURE [gsa].[spASConsultaDetalleSolicitudWeb]
	@nmro_slctd_atrzcn_ss					varchar(16)
AS
BEGIN
	SET NOCOUNT ON	
		
	DECLARE @lncnsctvo_slctd_atrzcn_srvco   udtConsecutivo,
			@ldFechaActual				    datetime      ,
			@lncnsctvo_cdgo_tpo_dgnstco     udtConsecutivo,			
			@lcobsrvcn_adcnl			    udtObservacion,			
			@lcSI						    udtCodigo     ,
			@lcNO						    udtCodigo     ,
			@lcAux                          Char(1)       ,
			@lcAuxSI                        Char(1)       		

	
	CREATE 
	TABLE #tmpDatosSolicitudAfi(cnsctvo_slctd_atrzcn_srvco	udtConsecutivo           ,	
								fcha_crcn					datetime                 ,		
								fcha_slctd					datetime                 ,		
								nmro_slctd_atrzcn_ss		varchar(16)              ,		
								dscrpcn_tpo_ubccn_pcnte		udtDescripcion           ,	
								dscrpcn_prrdd_atncn			udtDescripcion           ,	
								adtr_slctnte				udtDescripcion           ,	
								cdgo_mdo_cntcto				udtDescripcion           ,	
								dscrpcn_orgn_atncn			udtDescripcion           , 	
								dscrpcn_clse_atncn			udtDescripcion           ,
								jstfccn_clnca				varchar(2000)            ,
								spra_tpe_st                 Char(2)                  ,
								obsrvcn_adcnl				udtObservacion           ,
								dscrpcn_rcbro               udtDescripcion           ,
								fcha_ingrso_hsptlzcn		datetime                 ,
								fcha_egrso_hsptlzcn			datetime                 ,
								crte_cnta					udtConsecutivo  Default 0,
								nmro_slctd_prvdr			varchar(15)              ,
								cnsctvo_cdgo_frma_atncn		udtConsecutivo															
							   )

	CREATE 
	TABLE #tmpDatosAfiliadoSol(cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,			
							   cnsctvo_cdgo_tpo_idntfccn_afldo	udtConsecutivo,
							   cdgo_tpo_idntfccn				char(3),				
							   nmro_idntfccn     				udtNumeroIdentificacionLargo,
							   prmr_nmbre						udtNombre,				
						       sgndo_nmbre						udtNombre,
							   prmr_aplldo						udtApellido,			
							   sgndo_aplldo					    udtApellido,		
							   cnsctvo_cdgo_sxo				    udtConsecutivo,			
							   cdgo_sxo						    udtCodigo,
							   alt_rsgo						    char(1),
							   cnsctvo_cdgo_tpo_afldo			udtConsecutivo,			
							   dscrpcn							udtDescripcion,
							   estdo							udtLogico,				
							   smns_ctzds_antrr_eps			    udtConsecutivo,
							   smns_ctzds						udtConsecutivo,			
							   fcha_ncmnto						datetime,
							   cnsctvo_cdgo_pln			        udtConsecutivo,			
							   dscrpcn_pln						udtDescripcion,
							   cnsctvo_cdgo_cdd_rsdnca			udtConsecutivo,			
							   dscrpcn_cdd						udtDescripcion,
							   cnsctvo_cdgo_estdo_drcho		    udtConsecutivo,			
							   dscrpcn_drcho					udtDescripcion,
							   cnsctvo_cdgo_pln_pc				udtConsecutivo,			
							   dscrpcn_pln_pc					udtDescripcion, 
							   rcn_ncdo						    udtLogico,				
							   nmro_hjo_afldo					udtConsecutivo,
							   fcha_ncmnto_rcn_ncdo			    datetime,				
							   cnsctvo_cdgo_sxo_rcn_ncdo		udtConsecutivo,
							   cdgo_sxo_rcn_ncdo				udtCodigo,				
							   fcha_crcn						datetime,
							   cnsctvo_cdgo_tpo_cntrto			udtConsecutivo,			
							   edd								int,
							   edd_mss							int,					
							   edd_ds							int,
							   cnsctvo_rgstro_vldcn_espcl		udtConsecutivo,			
							   nmro_vldcn_espcl				    udtDescripcion,
							   prto_mltple                      Char(2)     ,
							   nmro_unco_idntfccnr				udtConsecutivo,
							   nmbre                            udtDescripcion,
							   nmro_idntfccn_afldo              udtDescripcion
						      )
	
	CREATE 
	TABLE  #tmpDatosPrestador(cnsctvo_slctd_atrzcn_srvco            udtConsecutivo              ,
	                          cnsctvo_cdgo_tpo_idntfccn_ips_slctnte udtConsecutivo              ,
							  cdgo_tpo_idntfccn			            char(3)                     ,
							  nmro_idntfccn_ips_slctnte	            udtNumeroIdentificacionLargo,
							  cdgo_intrno				            udtCodigoIPS                ,
							  nmbre_scrsl				            udtDescripcion  Default ''  ,
							  rzn_scl					            varchar(200)    Default ''  ,
							  dscrpcn_cdd				            udtDescripcion  Default ''  ,
							  nmro_unco_idntfccn_prstdr             udtConsecutivo              ,
							  idntfccion_prstdor                    udtDescripcion
							 )
	
		                                        
	SET	@ldFechaActual = GETDATE()

	--Completar informacion del Recobro principa	
	SET @lncnsctvo_cdgo_tpo_dgnstco = 1
	Set @lcAux = ' '
	SET @lcSI = 'SI'
	SET @lcNO = 'NO'
	Set @lcAuxSI = 'S'


	--Consultar el numero de solicitud con el numero de radicado									
	SELECT  @lncnsctvo_slctd_atrzcn_srvco   = s.cnsctvo_slctd_atrzcn_srvco  ,	        
			@lcobsrvcn_adcnl                = s.obsrvcn_adcnl                 			
	FROM    gsa.tbAsSolicitudesAutorizacionServicios s WITH(NOLOCK)
	WHERE   s.nmro_slctd_atrzcn_ss = @nmro_slctd_atrzcn_ss
	

	INSERT 
	INTO  #tmpDatosSolicitudAfi(cnsctvo_slctd_atrzcn_srvco, fcha_crcn              , fcha_slctd             ,		
							    nmro_slctd_atrzcn_ss      , dscrpcn_tpo_ubccn_pcnte, dscrpcn_prrdd_atncn    ,	
								adtr_slctnte              , cdgo_mdo_cntcto        , dscrpcn_orgn_atncn     ,	
								dscrpcn_clse_atncn        , jstfccn_clnca          , cnsctvo_cdgo_frma_atncn,
								spra_tpe_st               , dscrpcn_rcbro          , nmro_slctd_prvdr
							   )
	EXEC gsa.spASConsultaDatosInformacionSolicitud @lncnsctvo_slctd_atrzcn_srvco;
	    
	--Se actualiza la observacion adicional
	Update     #tmpDatosSolicitudAfi
	Set        obsrvcn_adcnl = @lcobsrvcn_adcnl


	UPDATE	   #tmpDatosSolicitudAfi
	SET	       dscrpcn_rcbro = cv.dscrpcn_clsfccn_evnto
	FROM	   #tmpDatosSolicitudAfi ds
	INNER JOIN gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK)
	ON	       s.cnsctvo_slctd_atrzcn_srvco = ds.cnsctvo_slctd_atrzcn_srvco 
	INNER JOIN gsa.tbASDiagnosticosSolicitudAutorizacionServicios d	WITH(NOLOCK)
	ON         d.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco	       
	INNER JOIN bdsisalud.dbo.tbClasificacionEventosNotificacion_vigencias CV WITH(NOLOCK) 
	ON d.cnsctvo_cdgo_rcbro = cv.cnsctvo_cdgo_clsfccn_evnto
	Where  d.cnsctvo_cdgo_tpo_dgnstco = @lncnsctvo_cdgo_tpo_dgnstco 
	And @ldFechaActual BETWEEN CV.inco_vgnca AND CV.fn_vgnca;
						
	--Completar informacion de hospitalizacion
	IF EXISTS (SELECT TOP 1 1
	           FROM       #tmpDatosSolicitudAfi s
			   INNER JOIN gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios h WITH(NOLOCK)
			   ON         h.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco) 
		BEGIN
			UPDATE	   #tmpDatosSolicitudAfi
			SET	       fcha_ingrso_hsptlzcn = h.fcha_ingrso_hsptlzcn,
				       fcha_egrso_hsptlzcn  = h.fcha_egrso_hsptlzcn,
					   crte_cnta            = h.crte_cnta
			FROM	   #tmpDatosSolicitudAfi ds
			INNER JOIN gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios h WITH(NOLOCK)
			ON	       ds.cnsctvo_slctd_atrzcn_srvco = h.cnsctvo_slctd_atrzcn_srvco 
		END						

	INSERT 
	INTO   #tmpDatosAfiliadoSol(cnsctvo_slctd_atrzcn_srvco,	cnsctvo_cdgo_tpo_idntfccn_afldo, cdgo_tpo_idntfccn         ,				
								nmro_idntfccn             , prmr_nmbre                     , sgndo_nmbre               ,
								prmr_aplldo               , sgndo_aplldo                   , cnsctvo_cdgo_sxo          ,
								cdgo_sxo                  , alt_rsgo                       , cnsctvo_cdgo_tpo_afldo    ,			
								dscrpcn                   , estdo                          , smns_ctzds_antrr_eps      ,
								smns_ctzds                , fcha_ncmnto                    , cnsctvo_cdgo_pln          ,
								dscrpcn_pln               , cnsctvo_cdgo_cdd_rsdnca        , dscrpcn_cdd               ,
								cnsctvo_cdgo_estdo_drcho  , dscrpcn_drcho                  , cnsctvo_cdgo_pln_pc       ,	
								dscrpcn_pln_pc            , rcn_ncdo                       , nmro_hjo_afldo            ,
							    fcha_ncmnto_rcn_ncdo      , cnsctvo_cdgo_sxo_rcn_ncdo      , cdgo_sxo_rcn_ncdo         ,				
								fcha_crcn                 , cnsctvo_cdgo_tpo_cntrto        , edd                       ,
								edd_mss                   , edd_ds                         , cnsctvo_rgstro_vldcn_espcl,
								nmro_vldcn_espcl          , nmro_unco_idntfccnr
							   )
	EXEC gsa.spASConsultaDatosInformacionAfiliadoxSolicitud @lncnsctvo_slctd_atrzcn_srvco;

	Update #tmpDatosAfiliadoSol
	Set    nmbre               = CONCAT(RTRIM(prmr_nmbre), @lcAux, RTRIM(sgndo_nmbre), @lcAux, RTRIM(prmr_aplldo), @lcAux, RTRIM(sgndo_aplldo)),
	       nmro_idntfccn_afldo = CONCAT(RTRIM(cdgo_tpo_idntfccn), @lcAux, nmro_idntfccn)  ,
		   prto_mltple         = CASE WHEN prto_mltple = @lcAuxSI THEN @lcSI ELSE @lcNO END	       
    From   #tmpDatosAfiliadoSol

	Update #tmpDatosSolicitudAfi
	Set    spra_tpe_st  = CASE WHEN spra_tpe_st = @lcAuxSI THEN @lcSI ELSE @lcNO END 
	From   #tmpDatosSolicitudAfi

	--Completar informacion del afiliado informacion recien nacido parto multiple

	UPDATE	   #tmpDatosAfiliadoSol
	SET	       prto_mltple = afs.prto_mltple
	FROM	   #tmpDatosAfiliadoSol da
	INNER JOIN gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios afs WITH(NOLOCK)
	ON	       afs.cnsctvo_slctd_atrzcn_srvco = da.cnsctvo_slctd_atrzcn_srvco

	--Informacion del Prestador	
	Insert Into #tmpDatosPrestador(cnsctvo_slctd_atrzcn_srvco, nmro_idntfccn_ips_slctnte            ,					
								   cdgo_intrno               , cnsctvo_cdgo_tpo_idntfccn_ips_slctnte        
								  )
	Select          s.cnsctvo_slctd_atrzcn_srvco, t.nmro_idntfccn_ips_slctnte            , 
	                t.cdgo_intrno               , t.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte
	From            #tmpDatosSolicitudAfi s
	Left Outer Join gsa.tbASIPSSolicitudesAutorizacionServicios t	WITH(NOLOCK)
	ON              t.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	
	--Informacion del Prestador	
	Update     #tmpDatosPrestador	
	Set        nmbre_scrsl               = p.nmbre_scrsl              ,
			   rzn_scl                   = p.nmbre_scrsl              ,
			   dscrpcn_cdd               = c.dscrpcn_cdd              ,
			   nmro_unco_idntfccn_prstdr = p.nmro_unco_idntfccn_prstdr
	From       #tmpDatosPrestador t
	INNER JOIN bdSisalud.dbo.tbDireccionesPrestador p WITH(NOLOCK)
	ON         p.cdgo_intrno = t.cdgo_intrno 	   
	Inner Join bdSisalud.dbo.tbPrestadores e WITH(NOLOCK)
	On         e.nmro_unco_idntfccn_prstdr = p.nmro_unco_idntfccn_prstdr
	INNER JOIN bdSiSalud.dbo.tbciudades_vigencias c WITH(NOLOCK)
	ON         c.cnsctvo_cdgo_cdd = p.cnsctvo_cdgo_cdd 	   
	Where      @ldFechaActual BETWEEN c.inco_vgnca AND c.fn_vgnca

	
	--Actualiza el codigo tipo de identificacion del prestador
	Update     #tmpDatosPrestador
	Set        cdgo_tpo_idntfccn  = b.cdgo_tpo_idntfccn 
	From       #tmpDatosPrestador t
	INNER JOIN bdafiliacionvalidador.dbo.tbtiposidentificacion_vigencias b WITH(NOLOCK)
	ON         b.cnsctvo_cdgo_tpo_idntfccn = t.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte 	
	And        @ldFechaActual BETWEEN b.inco_vgnca AND b.fn_vgnca
	
	
	--Valida si la razon social es una persona natural
	UPDATE     #tmpDatosPrestador
	SET        rzn_scl = CONCAT(LTRIM(RTRIM(m.prmr_nmbre_afldo))  , @lcAux, 
	                            LTRIM(RTRIM(m.sgndo_nmbre_afldo)) , @lcAux, 
								LTRIM(RTRIM(m.prmr_aplldo))       , @lcAux, 
								LTRIM(RTRIM(m.sgndo_aplldo))
							) 
	FROM       #tmpDatosPrestador ps
	INNER JOIN bdSisalud.dbo.tbmedicos m WITH(NOLOCK)
	ON         m.nmro_unco_idntfccn_prstdr = ps.nmro_unco_idntfccn_prstdr	
	WHERE      ps.rzn_scl IS NULL	
	
	Update   #tmpDatosPrestador
	Set      idntfccion_prstdor = CONCAT(RTRIM(cdgo_tpo_idntfccn), @lcAux, RTRIM(nmro_idntfccn_ips_slctnte)),
	         rzn_scl            = LTRIM(RTRIM(rzn_scl))
	From     #tmpDatosPrestador	
	
	SELECT     ds.cdgo_mdo_cntcto        , ds.fcha_crcn           , ds.nmro_slctd_prvdr             ,	
		       da.nmro_idntfccn_afldo    , da.nmbre               , da.dscrpcn_pln                  ,
		       da.edd                    , da.edd_mss             , da.edd_ds                       ,
		       da.rcn_ncdo               , da.prto_mltple         , da.fcha_ncmnto_rcn_ncdo         , 
		       da.cdgo_sxo_rcn_ncdo      , da.nmro_hjo_afldo      , ds.fcha_slctd fcha_slctd_prfsnal, 
		       ds.dscrpcn_tpo_ubccn_pcnte, ds.dscrpcn_orgn_atncn  , ds.dscrpcn_prrdd_atncn          , 
		       ds.dscrpcn_rcbro          , ds.jstfccn_clnca       , ds.spra_tpe_st                  ,
		       ds.obsrvcn_adcnl          , ds.fcha_ingrso_hsptlzcn, ds.fcha_egrso_hsptlzcn          , 
		       ds.crte_cnta              , pr.idntfccion_prstdor  , pr.cdgo_intrno                  ,
		       pr.nmbre_scrsl            , pr.rzn_scl             , pr.dscrpcn_cdd
	FROM       #tmpDatosSolicitudAfi ds
	INNER JOIN #tmpDatosAfiliadoSol da 
	ON         ds.cnsctvo_slctd_atrzcn_srvco = da.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN #tmpDatosPrestador pr
	ON         pr.cnsctvo_slctd_atrzcn_srvco = ds.cnsctvo_slctd_atrzcn_srvco

	
	DROP TABLE #tmpDatosSolicitudAfi
	DROP TABLE #tmpDatosAfiliadoSol
	DROP TABLE #tmpDatosPrestador
END
