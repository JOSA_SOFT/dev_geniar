
Use bdCNA

Go

If(Object_id('gsa.spAsGuardarInformacionRecobroXPrestacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsGuardarInformacionRecobroXPrestacion As Select 1';
End

Go

/*
-------------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spAsGuardarInformacionRecobroXPrestacion												
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez A\>  
* Descripcion		: <\DProcedimientos que guarda la informacion del recobro por prestaciones. D\>  												
* Observaciones		: <\O  O\>  													
* Parametros		: <\P  P\>  													
* Variables			: <\V  V\>  													
* Fecha Creacion	: <\FC 2017/05/10 FC\>											
*  															
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM  AM\>  													
* Descripcion			: <\DM  DM\>
* Nuevos Parametros	 	: <\PM  PM\>  													
* Nuevas Variables		: <\VM  VM\>  													
* Fecha Modificacion	: <\FM  FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
*/

/*
	Declare @cdgo_rsltdo				udtConsecutivo,
			@mnsje_rsltdo				VarChar(1000)
	Exec bdCNA.gsa.spAsGuardarInformacionRecobroXPrestacion 133937, 74876, 11, 'xxxxxxxxxxx', 'user19', @cdgo_rsltdo Output, @mnsje_rsltdo Output
	Select @cdgo_rsltdo, @mnsje_rsltdo
*/

Alter Procedure gsa.spAsGuardarInformacionRecobroXPrestacion
			@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
			@cnsctvo_srvco_slctdo		udtConsecutivo,
			@cnsctvo_cdgo_rcbro			udtConsecutivo,
			@obsrvcns					udtObservacion,
			@cnsctvo_cdgo_cntngnca		udtConsecutivo,
			@usro						udtUsuario,
			@cdgo_rsltdo				udtConsecutivo Output,
			@mnsje_rsltdo				VarChar(1000) Output
As
Begin
	-- 
	Set NoCount On

	-- 
	Declare	@fcha_actl					Date,
			@orgn_mdfccn				Varchar(100),
			@cnsc_cdgo_mtvo_csa			udtConsecutivo,
			@dscrpcn_rcbro				udtDescripcion,
			@nmro_ip					Varchar(9),
			@elmnto_orgn_mdfccn			udtDescripcion,
			@dto_elmnto_orgn_mdfccn		udtDescripcion,
			@cnsctvo_cdgo_mdlo			udtConsecutivo,
			@cdgo_rcbro					udtConsecutivo,
			@vsble_usro					Char(1),
			@cdgo_ok					Int,
			@cdgo_errr					Int,
			@mnsje_ok					udtDescripcion,
			@cnsctvo_cdgo_rcbro_ant		udtConsecutivo,
			@dscrpcn_rcbro_ant			udtDescripcion;

	-- 
	Create Table #tmpTrazaModificacion 
	(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
		fcha_mdfccn					datetime,
		orgn_mdfccn					varchar(100),
		vlr_antrr						varchar(20),
		vlr_nvo						varchar(20),
		usro_mdfccn					udtUsuario,
		obsrvcns						udtObservacion,
		nmro_ip						varchar(12),
		fcha_crcn						datetime,
		usro_crcn						udtUsuario,
		fcha_ultma_mdfccn				datetime,
		usro_ultma_mdfccn				udtUsuario,
		elmnto_orgn_mdfccn				udtDescripcion,
		dto_elmnto_orgn_mdfccn			udtDescripcion,
		dscrpcn_vlr_antrr				udtDescripcion,
		dscrpcn_vlr_nvo					udtDescripcion
	)

	Set @fcha_actl = getDate();
	Set @orgn_mdfccn = 'tbASServiciosSolicitados.cnsctvo_cdgo_rcbro';
	Set @cnsc_cdgo_mtvo_csa = 7;
	Set @nmro_ip = '127.0.0.1';
	Set	@elmnto_orgn_mdfccn = 'Prestacion';
	Set @dto_elmnto_orgn_mdfccn = 'Actualizacion';
	Set @cnsctvo_cdgo_mdlo = 31;
	Set @vsble_usro = 'S';
	Set @cdgo_ok = 0;
	Set @cdgo_errr = -1;
	Set @mnsje_ok = 'Proceso ejecutado satisfactoriamente'


	Begin Try

		-- 
		Select	@cnsctvo_cdgo_rcbro_ant = ss.cnsctvo_cdgo_rcbro
		From	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
		Where	cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And		cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

		-- 
		Select   	@dscrpcn_rcbro_ant = b.dscrpcn_clsfccn_evnto
		From		bdSisalud.dbo.tbClasificacionEventosNotificacion_vigencias b  With(NoLock)
		Inner Join	bdSisalud.dbo.tbMNClasificacionxTipoEvento_vigencias a 
		On			a.cnsctvo_cdgo_clsfccn_evnto=b.cnsctvo_cdgo_clsfccn_evnto          
		Inner Join	bdSisalud.dbo.tbClasificacionEventoNotificacionxModulo c 
		On			b.cnsctvo_cdgo_clsfccn_evnto = c.cnsctvo_cdgo_clsfccn_evnto  
		Where		@fcha_actl  between b.inco_vgnca And b.fn_vgnca
		And			@fcha_actl  between a.inco_vgnca And a.fn_vgnca 
		And			b.Vsble_Usro = @vsble_usro  
		And			a.cnsctvo_cdgo_cntngnca = @cnsctvo_cdgo_cntngnca  
		And			c.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo -- Eventos que aplican solo para sipres 
		And			b.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_rcbro_ant

		-- 
		Select   	@dscrpcn_rcbro = b.dscrpcn_clsfccn_evnto,  
					@cdgo_rcbro = b.cdgo_clsfccn_evnto
		From		bdSisalud.dbo.tbClasificacionEventosNotificacion_vigencias b  With(NoLock)
		Inner Join	bdSisalud.dbo.tbMNClasificacionxTipoEvento_vigencias a 
		On			a.cnsctvo_cdgo_clsfccn_evnto=b.cnsctvo_cdgo_clsfccn_evnto          
		Inner Join	bdSisalud.dbo.tbClasificacionEventoNotificacionxModulo c 
		On			b.cnsctvo_cdgo_clsfccn_evnto = c.cnsctvo_cdgo_clsfccn_evnto  
		Where		@fcha_actl  between b.inco_vgnca And b.fn_vgnca
		And			@fcha_actl  between a.inco_vgnca And a.fn_vgnca 
		And			b.Vsble_Usro = @vsble_usro  
		And			a.cnsctvo_cdgo_cntngnca = @cnsctvo_cdgo_cntngnca  
		And			c.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo -- Eventos que aplican solo para sipres 
		And			b.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_rcbro
		
		-- 
		Insert Into #tmpTrazaModificacion
		(
					cnsctvo_slctd_atrzcn_srvco		,	cnsctvo_cdgo_mtvo_csa			,	fcha_mdfccn					   ,	
					orgn_mdfccn					    ,	vlr_antrr						,	vlr_nvo						   ,	
					usro_mdfccn					    ,	obsrvcns						,	nmro_ip						   ,	
					fcha_crcn					    ,	usro_crcn						,	fcha_ultma_mdfccn			   ,	
					usro_ultma_mdfccn			    ,	elmnto_orgn_mdfccn				,	dto_elmnto_orgn_mdfccn		   ,	
					dscrpcn_vlr_antrr			    ,	dscrpcn_vlr_nvo					,	cnsctvo_srvco_slctdo
		)
		Select		ss.cnsctvo_slctd_atrzcn_srvco   ,	@cnsc_cdgo_mtvo_csa				,	@fcha_actl						,
					@orgn_mdfccn					,	@cnsctvo_cdgo_rcbro_ant			,	@cnsctvo_cdgo_rcbro				,
					@usro							,	@obsrvcns						,	@nmro_ip						,
					@fcha_actl						,	@usro							,	@fcha_actl						,
					@usro							,	@elmnto_orgn_mdfccn				,	@dto_elmnto_orgn_mdfccn			,
					@dscrpcn_rcbro_ant				,	@dscrpcn_rcbro					,	ss.cnsctvo_srvco_slctdo
		From		bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
		Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And			ss.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	
		-- 
		Update		bdCNA.gsa.tbASServiciosSolicitados With(RowLock)
		Set			cnsctvo_cdgo_rcbro = @cnsctvo_cdgo_rcbro,
					fcha_ultma_mdfccn = @fcha_actl,
					usro_ultma_mdfccn = @usro
		Where		cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And			cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

		-- 
		Update		bdCNA.gsa.tbASServiciosSolicitadosOriginal With(RowLock)
		Set			cdgo_rcbro = @cdgo_rcbro,
					fcha_ultma_mdfccn = @fcha_actl,
					usro_ultma_mdfccn = @usro
		Where		cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And			cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

		-- 
		Exec		BDCna.gsa.spASGuardarTrazaModificacion;

		-- 
		Set @cdgo_rsltdo = @cdgo_ok;
		Set @mnsje_rsltdo = @mnsje_ok;

	End Try
	Begin Catch

		Set @mnsje_rsltdo = Concat('Number:', CAST(ERROR_NUMBER() AS char), CHAR(13),
				   				'Line:', CAST(ERROR_LINE() AS char), CHAR(13),
								'Message:', ERROR_MESSAGE(), CHAR(13),
								'Procedure:', ERROR_PROCEDURE());
			
		Set @cdgo_rsltdo  =  @cdgo_errr;

		RaisError(@mnsje_rsltdo, 16, 1) With SetError 
		
	End Catch


End