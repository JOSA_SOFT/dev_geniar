USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudMedico]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudMedico
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Médico como 
					 :	modificación de una solicitud especifica D\> 
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>  
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASModificarSolicitudMedico] 
AS
  SET NOCOUNT ON
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal
		DELETE			MTO
		FROM			BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal MTO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				MTO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASMedicoTratanteSolicitudAutorizacionServicios
		DELETE			MTP
		FROM			BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios	MTP 
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				MTP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- INSERTAR INFORMACIÓN DE Medico EN TABLA DE PROCESO tbASMedicoTratanteSolicitudAutorizacionServicios 	
		MERGE INTO			BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServicios
		USING (SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
							,TME.id
							,TME.cnsctvo_cdgo_tpo_aflcn_mdco_trtnte	
							,TME.cnsctvo_cdgo_espcldd_mdco_trtnte
							,TME.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte
							,TME.nmro_idntfccn_mdco_trtnte	
							,TME.rgstro_mdco_trtnte			
							,TME.cnsctvo_mdco_trtnte_slctd_orgn
							,TME.adscrto						
							,TME.nmro_unco_idntfccn_mdco		
							,TME.usro_crcn
				FROM		#TMod_Medicos		TME WITH (NOLOCK)		
				INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
				ON			TME.id = IDS.id) AS MED
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte
				,cnsctvo_cdgo_espcldd_mdco_trtnte
				,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte
				,nmro_idntfccn_mdco_trtnte
				,rgstro_mdco_trtnte
				,cnsctvo_mdco_trtnte_slctd_orgn
				,adscrto
				,nmro_unco_idntfccn_mdco
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn
				)
		VALUES ( MED.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,MED.cnsctvo_cdgo_tpo_aflcn_mdco_trtnte
				,MED.cnsctvo_cdgo_espcldd_mdco_trtnte	
				,MED.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte
				,MED.nmro_idntfccn_mdco_trtnte	
				,MED.rgstro_mdco_trtnte			
				,MED.cnsctvo_mdco_trtnte_slctd_orgn
				,MED.adscrto						
				,MED.nmro_unco_idntfccn_mdco		
				,@Fecha_Actual	--fcha_crcn
				,MED.usro_crcn
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,MED.usro_crcn	--usro_ultma_mdfccn
				)
		OUTPUT	inserted.cnsctvo_mdco_trtnte_slctd_atrzcn_srvco
				,MED.id
		INTO	#IdMedico_Mod; 	
		-- INSERTAR INFORMACIÓN DE Medico EN TABLA ORIGINAL tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASMedicoTratanteSolicitudAutorizacionServiciosOriginal(
					 cnsctvo_mdco_trtnte_slctd_atrzcn_srvco
					,cnsctvo_slctd_atrzcn_srvco
					,cdgo_espcldd_mdco_trtnte
					,cdgo_tpo_idntfccn_mdco_trtnte
					,nmro_idntfccn_mdco_trtnte
					,prmr_nmbre_mdco_trtnte
					,sgndo_nmbre_mdco_trtnte
					,prmr_aplldo_mdco_trtnte
					,sgndo_aplldo_mdco_trtnte
					,rgstro_mdco_trtnte
					,adscrto
					,cdgo_tpo_aflcn_mdco_trtnte
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDM.cnsctvo_mdco_trtnte_slctd_atrzcn_srvco	
					,IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,MED.cdgo_espcldd_mdco_trtnte				
					,MED.cdgo_tpo_idntfccn_mdco_trtnte	
					,MED.nmro_idntfccn_mdco_trtnte			
					,MED.prmr_nmbre_mdco_trtnte					
					,MED.sgndo_nmbre_mdco_trtnte				
					,MED.prmr_aplldo_mdco_trtnte				
					,MED.sgndo_aplldo_mdco_trtnte
					,MED.rgstro_mdco_trtnte		
					,MED.adscrto					
					,MED.cdgo_tpo_aflcn_mdco_trtnte				
					,@Fecha_Actual	--fcha_crcn
					,MED.usro_crcn
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,MED.usro_crcn	--usro_ultma_mdfccn
		FROM		#TMod_Medicos		MED WITH (NOLOCK)
		INNER JOIN	#IdMedico_Mod		IDM WITH (NOLOCK)
		ON			MED.id = IDM.idXML
		INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
		ON			MED.id = IDS.id
	END



GO
