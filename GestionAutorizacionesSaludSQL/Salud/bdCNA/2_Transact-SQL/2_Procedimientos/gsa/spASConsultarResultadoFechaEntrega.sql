USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarResultadoFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarResultadoFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento expuesto que permite consultar el resultado de la ejecución   D\>  
					   <\D del proceso de fecha de entrega		D\>	
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 04/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASConsultarResultadoFechaEntrega] @cdgs_slctds nvarchar(max),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(2000) OUTPUT,
@msje_rspsta nvarchar(max) OUTPUT
AS

BEGIN
  SET NOCOUNT ON

  --Temporal con los datos básicos para fecha de entrega
  CREATE TABLE #tmpDatosSolicitudFechaEntrega (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    cnsctvo_srvco_slctdo int,
    cnsctvo_cdfccn int,
    cdgo_cdfccn varchar(30),
    cnsctvo_pln int,
    cdgo_pln varchar(30),
    cnsctvo_tpo_pln int,
    cdgo_tpo_pln varchar(30),
    cnsctvo_mdo_cntcto int,
    cdgo_mdo_cntcto varchar(30),
    cnsctvo_grpo_entrga int,
    cdgo_grpo_entrga varchar(30),
    fcha_slctd datetime NOT NULL,
    fcha_estmda_entrga datetime,
    nmro_unco_idntfccn_afldo int,
    nmro_idntfccn int,
    nmro_cntrto char(15),
    cnsctvo_cdgo_tpo_cntrto int,
    usro udtUsuario,
    nmro_slctd_ss varchar(16),
    nmro_slctd_prvdr varchar(16),
    cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo,
    nmro_idntfccn_empleador int,
    cnsctvo_grpo_entrga_fnl int,
    cdgo_grpo_entrga_fnl varchar(4),
	fcha_estmda_entrga_fnl datetime
  )

  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  DECLARE @negacion char(1),
          @codigoError char(2),
          @xml xml,
          @respsta xml,
		  @tipoProceso udtCodigo = 'FE',
		  @en_prcso udtLogico,
		  @codigoExito char(2) = 'OK',
		  @mnsje_ejccn VARCHAR(50) = 'Hay solicitudes que se encuetran en ejecución.'

  SET @negacion = 'N';
  SET @codigoError = 'ET';
  BEGIN TRY
    SET @xml = CONVERT(xml, @cdgs_slctds)

	--Se obtiene la información básica de la solicitud.
	EXEC bdCNA.gsa.spASObtenerDatosBasicos @xml

	EXEC gsa.spASConsultarFechaEntrega @negacion,
										   @estdo_ejccn OUTPUT,
										   @msje_errr OUTPUT,
										   @respsta OUTPUT
	SET @msje_rspsta = CONVERT(nvarchar(max), @respsta)

  END TRY
  BEGIN CATCH
    SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
    'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
    'Message:' + ERROR_MESSAGE() + CHAR(13) +
    'Procedure:' + ERROR_PROCEDURE();
    SET @estdo_ejccn = @codigoError;
    SET @msje_rspsta = NULL
  END CATCH

  DROP TABLE #tmpNmroSlctds
  DROP TABLE #tmpDatosSolicitudFechaEntrega
END

GO
