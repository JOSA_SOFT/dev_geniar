USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASDesasociarHospitalizacion]    Script Date: 10/3/2017 11:30:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------ 
* Método o PRG		:		spASDesasociarHospitalizacion										 
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez G.												A\>	 
* Descripción		: <\D	Desasociar una  Hospitalizacion a una solicitudOPS						D\>	 
* Observaciones		: <\O																			O\>	 
* Parámetros		: <\P																			P\>	 
* Variables			: <\V																			V\>	 
* Fecha Creación	: <\FC	2016/03/07																FC\> 
*------------------------------------------------------------------------------------------- 
*------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACIÓN																		 
*------------------------------------------------------------------------------------------- 
* Modificado Por	: <\AM Ing. Carlos Andres Lopez Ramirez AM\> 
* Descripción		: <\DM Se modifica procedimiento para que funcione con nuevos cambios 
						   en el proceso de anulacion. DM\> 
* Nuevos Parámetros	: <\PM																PM\> 
* Nuevas Variables	: <\VM																VM\> 
* Fecha Modificación: <\FM 2017/10/03 FM\>
*------------------------------------------------------------------------------------------- */

/*
Declare @cdgo_msg		udtCodigo,
		@msg			UdtDescripcion
exec bdcna.gsa.spASDesasociarHospitalizacion 2,null,@cdgo_msg output, @msg output

Select @cdgo_msg,@msg
*/
ALTER procedure [gsa].[spASDesasociarHospitalizacion]
As
Begin
	Set nocount On

	Declare @fechaActual     datetime,
			@lcusuario		 UdtUsuario,
			@vlr_nll		 udtConsecutivo;

	Set @fechaActual = getdate();
	Set @lcusuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User));
	Set @vlr_nll = Null;

	-- Actualizamos la desasociación de las solicitudes con notificaciones x hospitalización
	Update		sa
	Set			cnsctvo_ntfccn_hsptlzcn = @vlr_nll,
				cnsctvo_cdgo_ofcna_hsptlzcn = @vlr_nll,
				fcha_ultma_mdfccn = @fechaActual,
				usro_ultma_mdfccn = @lcusuario
	From		bdcna.gsa.tbASSolicitudesAutorizacionServicios sa 
	Inner Join	#tempInformacionSolicitud b
	On			b.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco   

End



