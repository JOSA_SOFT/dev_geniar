USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionAlternativasAuditor]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosAlternativasAuditor
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el peso reputacional del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionAlternativasAuditor] 
@cnsctvo_srvco_slctdo       udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	Select cnsctvo_cdgo_rsltdo_altrntva_gstn, altrntva from
    BDCna.gsa.tbASResultadoAlternativasGestionAuditor WITH (NOLOCK)
	Where cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	--And   cnsctvo_slctd_atrzcn_srvco @cnsctvo_slctd_atrzcn_srvco

END

GO
