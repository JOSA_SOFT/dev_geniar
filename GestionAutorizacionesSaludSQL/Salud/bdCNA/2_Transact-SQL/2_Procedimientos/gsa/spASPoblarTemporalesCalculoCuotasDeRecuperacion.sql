USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarTemporalesCalculoCuotasDeRecuperacion]    Script Date: 28/08/2017 01:09:14 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASPoblarTemporalesCalculoCuotasDeRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D 1. Para Masivo Poblar tablas temporales para el cálculo de Cuotas de Recuperación que cumpla la condición
							que las prestaciones tenga fecha de entrega el día siguiente a la fecha actual y el estado de 
							los servicios esten en estado '9-Liquidada'	
						  2. Por Demanda se Poblan tablas temporales para la solicitud enviada sin importar la fecha de entrega
						    y que  los servicios esten en estado '9-Liquidada' 								D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																									P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/25																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez García AM\>  													
* Descripcion			: <\DM se modifica para recuperar el consecutivo rango salarial afiliado de la fuente 
								bdafiliacionValidador.dbo.tbContratosValidador									DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2016/11/09 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se modifica para sacar el numero unico cuota recuperacion de los procedimientos, insumos y medicamenteos	DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2016/11/18 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Agrega un update en el llenado de la tabla temporal #TempServicioSolicitudes
							   para evitar que se carguen las que tiene el campo nmro_unco_ops = 0,
							   Se divide el llenado de la tabla en dos partes, una para procedimientos e insumos y otra para medicamentos DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2016/11/18 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Agrega un update para los casos de corte de cuenta de hospitalizacion, no se calculaba los 
							   valores correctamente debido que nunca se obtenia este valor DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2016/12/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega update para el rango salarial cuando este no se calcula y el tipo de plan es 2 DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM @cnsccdgo_rngo_slrl4: Guarda el consecutivo del rango salarial PAC
							   @cnsctvo_cdgo_tpo_pln2: Guarda el consecutivo del tipo de plan PAC VM\>  													
* Fecha Modificacion	: <\FM  2016/12/23 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM se quita condicional para recuperar los numero de ops del DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2017/01/06 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega condicional para recuperar el valor de corte de cuenta hospitalaria 
							   solo si este es mayo a cero DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2017/04/07 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se retira condicional del proceso masivo que cargaba solo programacion de entrega. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2017/06/01 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega carga del campo cnsctvo_cdgo_csa_no_cbro_cta_rcprcn de la tabla
							   tbAsServiciosSolicitados, control de cambio 092 punto 19 DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/13 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se modifica consulta de carga de informacion de la solicitud por demanda para que,
							   se carguen las solicitudes generadas por proceso de programacion de entrega y se 
							   agregan delete para los casos de solicitudes no generadas por proceso masivo
							   que no cumplan con la condicion de fecha de entrega menor o igual a la fecha de corte. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM  2017/06/27 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASPoblarTemporalesCalculoCuotasDeRecuperacion]

/*01*/ @cnsctvo_prcso				UdtConsecutivo,
/*02*/ @cnsctvo_slctd_atrzcn_srvco	UdtConsecutivo

AS

Begin
	SET NOCOUNT ON

	Declare		@cnsctvo_cdgo_estdo_slctd9		UdtConsecutivo = 9,  -- 'Estado Solicitud '9-Liquidada'
				@fechacrtelqdccn				Datetime = getdate(), -- fecha de corte liquidación
				@ValorSI						UdtLogico = 'S',
				@ValorNO						UdtLogico = 'N',
				@ValorCERO						int = 0,
				@ValorUNO						int = 1,
				@cnsctvo_cdgo_tpo_dscpcdd1		int = 1,				-- Tipo discapacidad '1-Fisica'
				@cnsctvo_cdgo_tpo_dscpcdd2		int = 2,				-- Tipo discapacidad '2-Mental'
				@cnsctvo_cdgo_tpo_dscpcdd3		int = 3,				-- Tipo discapacidad '3-Sonora'
				@fechaactual					Datetime = getdate(),   -- fecha actual
				@cnsccdgo_rngo_slrl4			udtConsecutivo,			-- Rango salarial PAC
				@valorTRES                      int = 3,
				@cnsctvo_cdgo_tpo_pln2			udtConsecutivo			-- Codigo para tipo de plan complementario
			
	Set	@fechacrtelqdccn += @ValorUNO
	Set	@fechacrtelqdccn = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))
	Set @cnsccdgo_rngo_slrl4 = 4
	Set	@cnsctvo_cdgo_tpo_pln2 = 2

   -- Validamos si el proceso debe hacerce por demanda ó masivo
   If	@cnsctvo_slctd_atrzcn_srvco is not null
		Begin
			-- Inicializamos el corte de liquidación a la fecha actual porque se solicito por demanda
			Set	@fechacrtelqdccn = @fechaactual
		End
		

   -- Paso 1. Poblar Temporal de Solicitudes 
   if @cnsctvo_slctd_atrzcn_srvco is not null -- por demanda
		Begin
		   Insert		#TempSolicitudes
		   (
						cnsctvo_prcso,						cnsctvo_slctd_atrzcn_srvco,				nmro_slctd_atrzcn_ss,					
						fcha_slctd,							nmro_unco_idntfccn_afldo,				cnsctvo_cdgo_cdd_rsdnca_afldo,			
						cnsctvo_cdgo_pln,					cnsctvo_cdgo_rngo_slrl,					cnsctvo_cdgo_tpo_cntrto,
						nmro_cntrto,						cnsctvo_bnfcro,							cnsctvo_cdgo_chrte,	
						edd_afldo_ans,						cnsctvo_cdgo_frma_atncn,				fcha_entrga,
						cnsctvo_cdgo_tpo_orgn_slctd	
			)
			Select distinct     
						@cnsctvo_prcso,						a.cnsctvo_slctd_atrzcn_srvco,			a.nmro_slctd_atrzcn_ss,
						a.fcha_slctd,						b.nmro_unco_idntfccn_afldo,				b.cnsctvo_cdgo_cdd_rsdnca_afldo,
						b.cnsctvo_cdgo_pln,					b.cnsctvo_cdgo_rngo_slrl,				b.cnsctvo_cdgo_tpo_cntrto,
						b.nmro_cntrto,						b.cnsctvo_bnfcro_cntrto,				b.cnsctvo_cdgo_chrte,
						b.edd_afldo_ans,					a.cnsctvo_cdgo_frma_atncn,				@fechacrtelqdccn,
						a.cnsctvo_cdgo_tpo_orgn_slctd
			From		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
			Inner Join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
			On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASServiciosSolicitados c with(nolock)
			On			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			Where		a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			And			c.vlr_lqdcn_srvco > @ValorCERO
			And			c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			

		End
	Else -- por masivo
		Begin
			/*
			 Insert		#TempSolicitudes
		   (
						cnsctvo_prcso,						cnsctvo_slctd_atrzcn_srvco,				nmro_slctd_atrzcn_ss,					
						fcha_slctd,							nmro_unco_idntfccn_afldo,				cnsctvo_cdgo_cdd_rsdnca_afldo,			
						cnsctvo_cdgo_pln,					cnsctvo_cdgo_rngo_slrl,					cnsctvo_cdgo_tpo_cntrto,
						nmro_cntrto,						cnsctvo_bnfcro,							cnsctvo_cdgo_chrte,	
						edd_afldo_ans,						cnsctvo_cdgo_frma_atncn,				fcha_entrga				
			)
			Select distinct     
						@cnsctvo_prcso,						a.cnsctvo_slctd_atrzcn_srvco,			a.nmro_slctd_atrzcn_ss,
						a.fcha_slctd,						b.nmro_unco_idntfccn_afldo,				b.cnsctvo_cdgo_cdd_rsdnca_afldo,
						b.cnsctvo_cdgo_pln,					b.cnsctvo_cdgo_rngo_slrl,				b.cnsctvo_cdgo_tpo_cntrto,
						b.nmro_cntrto,						b.cnsctvo_bnfcro_cntrto,				b.cnsctvo_cdgo_chrte,
						b.edd_afldo_ans,					a.cnsctvo_cdgo_frma_atncn,				@fechacrtelqdccn
			from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
			Inner Join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASServiciosSolicitados c with(nolock)
			on			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			Inner Join  gsa.tbASResultadoFechaEntrega d with(nolock)
			On			d.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			And         d.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
			And         @fechacrtelqdccn >= isnull(d.fcha_rl_entrga,d.fcha_estmda_entrga)
			where		c.vlr_lqdcn_srvco > @ValorCERO
			And			c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			*/
		    -- insertamos las solicitudes por programación de entrega virtuales  -- sisjvg01 - 2017-04-03
		    Insert		#TempSolicitudes
		   (
						cnsctvo_prcso,						cnsctvo_slctd_atrzcn_srvco,				nmro_slctd_atrzcn_ss,					
						fcha_slctd,							nmro_unco_idntfccn_afldo,				cnsctvo_cdgo_cdd_rsdnca_afldo,			
						cnsctvo_cdgo_pln,					cnsctvo_cdgo_rngo_slrl,					cnsctvo_cdgo_tpo_cntrto,
						nmro_cntrto,						cnsctvo_bnfcro,							cnsctvo_cdgo_chrte,	
						edd_afldo_ans,						cnsctvo_cdgo_frma_atncn,				fcha_entrga,
						cnsctvo_cdgo_tpo_orgn_slctd			
			)
			Select distinct     
						@cnsctvo_prcso,						a.cnsctvo_slctd_atrzcn_srvco,			a.nmro_slctd_atrzcn_ss,
						a.fcha_slctd,						b.nmro_unco_idntfccn_afldo,				b.cnsctvo_cdgo_cdd_rsdnca_afldo,
						b.cnsctvo_cdgo_pln,					b.cnsctvo_cdgo_rngo_slrl,				b.cnsctvo_cdgo_tpo_cntrto,
						b.nmro_cntrto,						b.cnsctvo_bnfcro_cntrto,				b.cnsctvo_cdgo_chrte,
						b.edd_afldo_ans,					a.cnsctvo_cdgo_frma_atncn,				@fechacrtelqdccn,
						a.cnsctvo_cdgo_tpo_orgn_slctd
			from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
			Inner Join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASServiciosSolicitados c with(nolock)
			on			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			where		c.vlr_lqdcn_srvco > @ValorCERO
			And			c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			-- And         a.cnsctvo_cdgo_tpo_orgn_slctd = @ValorTRES

		End

	-- Actualizamos Datos de la temporal de solicitudes
	
	-- Actualizamos Tipo de Afiliado (Cotizante-Beneficiario)
	update		a 
	Set			cnsctvo_cdgo_tpo_afldo = b.cnsctvo_cdgo_tpo_afldo,
				fcha_ncmnto = b.fcha_ncmnto
	from		#TempSolicitudes a
	Inner Join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador b with(nolock)
	On			a.cnsctvo_cdgo_tpo_cntrto = b.cnsctvo_cdgo_tpo_cntrto
	And			a.nmro_cntrto = b.nmro_cntrto
	And			a.cnsctvo_bnfcro = b.cnsctvo_bnfcro
	And			@fechaactual between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro

	-- Actualizamos el Número Unico del Aportante o Empleador Principal
	Update		a
	Set			nmro_unco_aprtnte = b.nmro_unco_idntfccn_aprtnte,
				cnsctvo_cdgo_cnvno_cmrcl = b.cnsctvo_prdcto_scrsl
	From		#TempSolicitudes a
	Inner Join  BDAfiliacionValidador.dbo.tbCobranzasValidador b with(nolock)
	On			b.cnsctvo_cdgo_tpo_cntrto = a.cnsctvo_cdgo_tpo_cntrto
	And			b.nmro_cntrto = a.nmro_cntrto
	Where		@fechaactual between b.inco_vgnca_cbrnza And b.fn_vgnca_cbrnza
	And			b.prncpl = @ValorSI

	-- Actualizamos el Número Unico del Aportante o Empleador
	Update		a
	Set			nmro_unco_aprtnte = b.nmro_unco_idntfccn_aprtnte,
				cnsctvo_cdgo_cnvno_cmrcl = b.cnsctvo_prdcto_scrsl
	From		#TempSolicitudes a
	Inner Join  BDAfiliacionValidador.dbo.tbCobranzasValidador b with(nolock)
	On			b.cnsctvo_cdgo_tpo_cntrto = a.cnsctvo_cdgo_tpo_cntrto
	And			b.nmro_cntrto = a.nmro_cntrto
	Where		@fechaactual between b.inco_vgnca_cbrnza And b.fn_vgnca_cbrnza
	And			a.nmro_unco_aprtnte is null

	-- Actualizamos afiliados con discapacidad fisica mental y/o sensorial
	Update		a
	Set			cnsctvo_cdgo_tpo_dscpcdd = av.cnsctvo_cdgo_tpo_dscpcdd
	From		#TempSolicitudes a
	Inner Join	bdAfiliacionValidador.dbo.tbAfiliadosValidador av with(nolock)
    On			av.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn_afldo	   	
	Where		av.cnsctvo_cdgo_tpo_dscpcdd in (@cnsctvo_cdgo_tpo_dscpcdd1,@cnsctvo_cdgo_tpo_dscpcdd2,@cnsctvo_cdgo_tpo_dscpcdd3) /* Fisica, mental, sensorial */

	-- Actualizamos afiliados con el rango salarial  -- sisjvg01 - 2016/11/09
	Update		a
	Set			cnsctvo_cdgo_rngo_slrl = cv.cnsctvo_cdgo_rngo_slrl
	From		#TempSolicitudes a
	Inner Join	bdAfiliacionValidador.dbo.tbContratosValidador cv with(nolock)
	On			a.cnsctvo_cdgo_tpo_cntrto = cv.cnsctvo_cdgo_tpo_cntrto
	And			a.nmro_cntrto = cv.nmro_cntrto
	Where		a.cnsctvo_cdgo_rngo_slrl = @ValorCERO
	And         @fechaactual between cv.inco_vgnca_cntrto And cv.fn_vgnca_cntrto

	-- Actualizamos el tipo Plan Afiliado  -- sisjvg01 - 2016-11-10
	Update		a
	Set			cnsctvo_cdgo_tpo_pln = pv.cnsctvo_cdgo_tpo_pln
	From		#TempSolicitudes a
	Inner Join  bdAfiliacionValidador.dbo.tbplanes_vigencias pv with(nolock)
	On          pv.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	And         @fechaactual between pv.inco_vgnca and pv.fn_vgnca

	-- Actualizamos  el rango salarial para los pac que no le calculo
	Update		a
	Set			cnsctvo_cdgo_rngo_slrl = @cnsccdgo_rngo_slrl4  -- Rango salarial PAC
	From		#TempSolicitudes a
	Where		a.cnsctvo_cdgo_rngo_slrl = @ValorCERO
	and         a.cnsctvo_cdgo_tpo_pln     = @cnsctvo_cdgo_tpo_pln2 -- PAC

	-- Paso 2. Poblar Temporal de Servicios para las Solicitudes Insertadas en el paso 1
	if @cnsctvo_slctd_atrzcn_srvco is not null
		Begin
			
			--Se cargan las solicitudes de servicio para procedimientos e insumos.
			Insert		#TempServicioSolicitudes
			(
						cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo,	  
						cnsctvo_cdgo_tpo_srvco,					cntdd_slctda,							vlr_lqdcn_srvco,
						cnsctvo_cdgo_prstcn_prstdr,				cnsctvo_prcso,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			)
			Select distinct
						a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo,
						b.cnsctvo_cdgo_tpo_srvco,				b.cntdd_slctda,							 b.vlr_lqdcn_srvco,
						b.cnsctvo_cdgo_prstcn_prstdr, 			@cnsctvo_prcso,							 b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			from		#TempSolicitudes a
			Inner Join	gsa.tbASServiciosSolicitados b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis with(nolock)
			On			pis.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado	css with(nolock)
			On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
			Where		css.nmro_unco_ops != @ValorCERO --Se agrega filtro para evitar que se autoricen las ops con nmro_unco_ops = 0
			And			b.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada

			--Se cargan las solicitudes de servicio para medicamentos.
			Insert		#TempServicioSolicitudes
			(
						cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo,	  
						cnsctvo_cdgo_tpo_srvco,					cntdd_slctda,							vlr_lqdcn_srvco,
						cnsctvo_cdgo_prstcn_prstdr,				cnsctvo_prcso,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			)
			Select distinct
						a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo,
						b.cnsctvo_cdgo_tpo_srvco,				b.cntdd_slctda,							 b.vlr_lqdcn_srvco,
						b.cnsctvo_cdgo_prstcn_prstdr, 			@cnsctvo_prcso,							 b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			from		#TempSolicitudes a
			Inner Join	gsa.tbASServiciosSolicitados b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASMedicamentosSolicitados ms with(nolock)
			On			b.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado	css with(nolock)
			On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
			Where		css.nmro_unco_ops != @ValorCERO --Se agrega filtro para evitar que se autoricen las ops con nmro_unco_ops = 0
			And			b.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada

		End
	Else
		Begin
			/*Insert		#TempServicioSolicitudes
			(
						cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo,	  
						cnsctvo_cdgo_tpo_srvco,					cntdd_slctda,							vlr_lqdcn_srvco,
						cnsctvo_cdgo_prstcn_prstdr,				cnsctvo_prcso
			)
			Select distinct
						a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo,
						b.cnsctvo_cdgo_tpo_srvco,				b.cntdd_slctda,							 b.vlr_lqdcn_srvco,
						b.cnsctvo_cdgo_prstcn_prstdr, 			@cnsctvo_prcso
			from		#TempSolicitudes a
			Inner Join	gsa.tbASServiciosSolicitados b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join  gsa.tbASResultadoFechaEntrega c with(nolock)
			On			a.cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco
			And			b.cnsctvo_srvco_slctdo = c.cnsctvo_srvco_slctdo
			where		@fechacrtelqdccn >= isnull(c.fcha_rl_entrga,c.fcha_estmda_entrga)
			And			b.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			*/
			--Se cargan las solicitudes de servicio para procedimientos e insumos.
			Insert		#TempServicioSolicitudes
			(
						cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo,	  
						cnsctvo_cdgo_tpo_srvco,					cntdd_slctda,							vlr_lqdcn_srvco,
						cnsctvo_cdgo_prstcn_prstdr,				cnsctvo_prcso,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			)
			Select distinct
						a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo,
						b.cnsctvo_cdgo_tpo_srvco,				b.cntdd_slctda,							 b.vlr_lqdcn_srvco,
						b.cnsctvo_cdgo_prstcn_prstdr, 			@cnsctvo_prcso,							 b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			from		#TempSolicitudes a
			Inner Join	gsa.tbASServiciosSolicitados b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis with(nolock)
			On			pis.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado	css with(nolock)
			On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
			where		b.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			And		    css.nmro_unco_ops != @ValorCERO --Se agrega filtro para evitar que se autoricen las ops con nmro_unco_ops = 0

			--Se cargan las solicitudes de servicio para medicamentos.
			Insert		#TempServicioSolicitudes
			(
						cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,					cnsctvo_cdgo_srvco_slctdo,	  
						cnsctvo_cdgo_tpo_srvco,					cntdd_slctda,							vlr_lqdcn_srvco,
						cnsctvo_cdgo_prstcn_prstdr,				cnsctvo_prcso,							cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			)
			Select distinct
						a.cnsctvo_slctd_atrzcn_srvco,			b.cnsctvo_srvco_slctdo,					 b.cnsctvo_cdgo_srvco_slctdo,
						b.cnsctvo_cdgo_tpo_srvco,				b.cntdd_slctda,							 b.vlr_lqdcn_srvco,
						b.cnsctvo_cdgo_prstcn_prstdr, 			@cnsctvo_prcso,							 b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
			from		#TempSolicitudes a
			Inner Join	gsa.tbASServiciosSolicitados b with(nolock)
			on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
			Inner Join	gsa.tbASMedicamentosSolicitados ms with(nolock)
			On			b.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
			Inner Join	gsa.tbASConceptosServicioSolicitado	css with(nolock)
			On			ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
			where		b.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9 -- Liquidada
			And		    css.nmro_unco_ops != @ValorCERO --Se agrega filtro para evitar que se autoricen las ops con nmro_unco_ops = 0

		End

	-- Se borran las solicitudes no generadas por proceso masivo de programacion de entrega
	-- que no cumplan la condicion de fecha de entrega.
	Delete		ss
	From		#TempSolicitudes so
	Inner Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = so.cnsctvo_slctd_atrzcn_srvco
	Inner Join	bdCNA.gsa.tbASResultadoFechaEntrega rfe With(NoLock)
	On			rfe.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			rfe.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Where		@fechacrtelqdccn < isNull(rfe.fcha_rl_entrga,rfe.fcha_estmda_entrga)
	And			so.cnsctvo_cdgo_tpo_orgn_slctd != @ValorTRES

	-- 
	Delete		so
	From		#TempSolicitudes so
	Left Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = so.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_slctd_atrzcn_srvco Is Null

	--Se saca el valor de corte de la hospitalizacion.
	Update		#TempServicioSolicitudes
	Set			vlr_lqdcn_srvco = b.crte_cnta
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios b With(NoLock)
	On			b.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		b.crte_cnta > @ValorCERO

	-- Paso 4. Recuperamos el Número Unico OPS de los procedimientos e insumos

	Update		ss
	Set			nmro_unco_ops = css.nmro_unco_ops,
				nmro_unco_prncpl_cta_rcprcn = css.nmro_unco_prncpl_cta_rcprcn
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis with(nolock)
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css with(nolock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo	
	And			css.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo
	

	-- Paso 5. Recuperamos el Número Unico OPS de los Medicamentos

	Update		ss
	Set			nmro_unco_ops = css.nmro_unco_ops,
				nmro_unco_prncpl_cta_rcprcn = css.nmro_unco_prncpl_cta_rcprcn
	From		#TempServicioSolicitudes ss
	Inner Join	gsa.tbASMedicamentosSolicitados ms with(nolock)
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	gsa.tbASConceptosServicioSolicitado css with(nolock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	And			css.cnsctvo_prstcn = ss.cnsctvo_cdgo_srvco_slctdo

	Exec bdCNA.gsa.spAsConsultarNotificacionesAfiliado;
	
End

