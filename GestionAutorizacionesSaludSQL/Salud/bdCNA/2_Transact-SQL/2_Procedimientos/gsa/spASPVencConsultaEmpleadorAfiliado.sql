USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPVencConsultaEmpleadorAfiliado]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spCNAPVencConsultaEmpleadorAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta datos del empleador.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASPVencConsultaEmpleadorAfiliado] 
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn udtNumeroIdentificacionLargo,
	@nui_Afldo	UdtConsecutivo = NULL,
	@cnsctvo_cdgo_pln udtConsecutivo,
	@empleador varchar(40) output
AS
BEGIN
Set Nocount On
	
	CREATE TABLE #TMPEMPLEADORES(
		nmro_unco_idntfccn_aprtnte	udtConsecutivo
	)

	Declare @getDate_ datetime = getDate(),
			@cod_empleador integer = 1,
			@con_convenio varchar(20) = 'CON CONVENIO',
			@sin_convenio varchar(20) = 'SIN CONVENIO',
			@sin_empleador varchar(20) = 'SIN EMPLEADOR';

	
	BEGIN

		if @nui_Afldo is null
		begin
			Set @nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn)
		end

		
			INSERT INTO #TMPEMPLEADORES( nmro_unco_idntfccn_aprtnte )
			SELECT e.nmro_unco_idntfccn_aprtnte empleador 
			From          bdAfiliacionValidador.dbo.tbBeneficiariosValidador            b    With(NoLock)
			INNER JOIN    bdAfiliacionValidador.dbo.tbContratosValidador                c    With(NoLock)    
																						On b.nmro_cntrto               = c.nmro_cntrto
																						and b.cnsctvo_cdgo_tpo_cntrto  = c.cnsctvo_cdgo_tpo_cntrto
			INNER JOIN    bdAfiliacionValidador.dbo.tbCobranzasValidador                d    With(NoLock)   
																						On  b.nmro_cntrto                 = d.nmro_cntrto
																						And    b.cnsctvo_cdgo_tpo_cntrto  = d.cnsctvo_cdgo_tpo_cntrto
			INNER JOIN  bdafiliacionvalidador..tbAportanteValidador      e With(NoLock) on   e.nmro_unco_idntfccn_aprtnte = d.nmro_unco_idntfccn_aprtnte
			Where b.nmro_unco_idntfccn_afldo    = @nui_Afldo
			And c.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
			and @getDate_ between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro
			and @getDate_ between c.inco_vgnca_cntrto and c.fn_vgnca_cntrto
			and @getDate_ between d.inco_vgnca_cbrnza and d.fn_vgnca_cbrnza
			and e.cnsctvo_cdgo_clse_aprtnte  = @cod_empleador 
			group by b.nmro_unco_idntfccn_afldo, e.nmro_unco_idntfccn_aprtnte 


			
			IF EXISTS (SELECT nmro_unco_idntfccn_aprtnte FROM #TMPEMPLEADORES)
			BEGIN
				
				IF EXISTS (SELECT EMP.nmro_unco_idntfccn_aprtnte FROM #TMPEMPLEADORES EMP
						   INNER JOIN BDAfiliacionValidador.dbo.tbMarcasProductosxSucursal MPS With(NoLock)
						   ON MPS.nmro_unco_idntfccn_empldr = EMP.nmro_unco_idntfccn_aprtnte)
				BEGIN
					Set @empleador = @con_convenio
				END
				ELSE
				BEGIN
					Set @empleador = @sin_convenio
				END
				
			END
			ELSE
			BEGIN
				Set @empleador = @sin_empleador
			END

	END
END
GO
