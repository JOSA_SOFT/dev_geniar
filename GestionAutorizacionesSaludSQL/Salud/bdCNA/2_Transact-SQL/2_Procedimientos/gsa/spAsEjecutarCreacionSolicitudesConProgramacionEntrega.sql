USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsEjecutarCreacionSolicitudesConProgramacionEntrega]    Script Date: 19/05/2017 02:50:59 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------
* Método o PRG		:		gsa.spAsEjecutarCreacionSolicitudesConProgramacionEntrega							
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez García										   A\>	
* Descripción		: <\D	Proyecto Mega: Sp de Creación Solicitudes Con Programación de Entrega	D\>
* Observaciones		: <\O 																		   O\>	
* Parámetros		: <\P 	P\>	
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/03/15															FC\>
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento adicionando la consulta del parametro general de los dias para la impresion de la OPS D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2016-10-21  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01- AM\>    
* Descripcion        : <\D Se modifica el procedimiento adicionando nueva funcionalidad para OPS Virtual D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2016-12-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01- AM\>    
* Descripcion        : <\D   Por OPS Virtual Se modifica Para Incluir el plan del afiliado requerido para validar si la programación
							 esta convenida  D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-07  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01- AM\>    
* Descripcion        : <\D   Por OPS Virtual Se modifica Para Incluir el tipo, nmro de contrato y consecutivo para validar si el afiliado esta vigente  D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-12  FM\>    
*----------------------------------------------------------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing.Angela Sandoval- AM\>    
* Descripcion        : <\D   Se modifica porque esta calculando mal la programacion de entrega  D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 2017-04-12  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*
Declare @cdgo_rsltdo char(2), @mnsje_rsltdo Varchar(150),@cnsctvo_slctd_atrzcn_srvco int
     EXEC bdcna.gsa.spAsEjecutarCreacionSolicitudesConProgramacionEntrega null,null,null,null,null
--   EXEC bdcna.gsa.spAsEjecutarCreacionSolicitudesConProgramacionEntrega 3454461,'geniarjcv',@cnsctvo_slctd_atrzcn_srvco output,@cdgo_rsltdo output,@mnsje_rsltdo output

Select concat(@cnsctvo_slctd_atrzcn_srvco,' ',@cdgo_rsltdo,' ', @mnsje_rsltdo)

*/

ALTER PROCEDURE  [gsa].[spAsEjecutarCreacionSolicitudesConProgramacionEntrega]

/*01*/ @cnsctvo_cdgo_det_prgrmcn_fcha_evnto		UdtConsecutivo = null,
/*02*/ @usro_prcso								UdtUsuario = null,
/*03*/ @cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo =  null output,
/*04*/ @cdgo_rsltdo								int = null	output,
/*05*/ @mnsje_rsltdo							UdtDescripcion = null output 

AS 

Begin

	SET NOCOUNT ON

	-- Declaramos variables del proceso
	Declare	@fechaactual					datetime = getdate(),
			@fcha_crte_inco					Datetime, -- fecha inicio corte liquidación
			@fechacrtelqdccn				Datetime = getdate(), -- fecha fin corte liquidacion   
			@lcUsuario						UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@sn_entrgr						int			= 151, -- Estado Entrega '151-Sin Entregar'
			@rprgrmdo						int			= 152, -- Estado Entrega '152-Reprogramado'
			@estdo							char(1)		= 'A',
	        @mensajeExito					char(30) = 'Ejecución Exitosa',
			@codigoExito					char(2) = 0,
			@codigoError					char(2) = -1,
			@codigoSalir					char(2) = -2,
			@i								int,
			@Max							int,
			@mensajeError					varchar(2000) = 'Se Presento Un Error al Crear la(s) Solicitud(es) con Programación Entrega',
			@cnsctvo_lg						int = 0,
			@tpo_prcso						int = 25, -- 'Proceso Automático Creación Solicitudes Con Programación Entrega Masivo'
			@cnsctvo_prcso					int,
			@ValorUNO						int	= 1,
			@cnsctvo_cdgo_tpo_incnsstnca int = 1,
			@tpo_rgstro_log_80				int = 80,
			@tpo_rgstro_log_81				int = 81,
			@tpo_rgstro_log_82				int = 82,
			@tpo_rgstro_log_83				int = 83,
			@tpo_rgstro_log_84				int = 84,
			@tpo_rgstro_log_85				int = 85,
			@tpo_rgstro_log_86				int = 86,
			@tpo_rgstro_log_87				int = 87,
			@tpo_rgstro_log_121				int = 121,
			@tpo_rgstro_log_122				int = 122,
			@mnsje_lg_evnto_prcso			Varchar(500),
			@cnsctvo_estdo_err				udtConsecutivo = 3,
			@estdo_pso1						int = 0,  /*Estado paso 1 */
			@estdo_pso2						int = 0,  /*Estado paso 2 */
			@estdo_pso3						int = 0,  /*Estado paso 3 */
			@estdo_pso4						int = 0,  /*Estado paso 4 */
			@estdo_pso5						int = 0,  /*Estado paso 5 */
			@estdo_pso6						int = 0,  /*Estado paso 6 */
			@estdo_pso7						int = 0,  /*Estado paso 7 */
			@estdo_pso8						int = 0,  /*Estado paso 8 */
			@estdo_pso9						int = 0,  /*Estado paso 9 */
			@estdo_pso10					int = 0,   /*Estado paso 10 */

			--Variables que se utilizan para la consulta del parametro general de los dias para la impresion de la OPS
			@lcCdgo_prmtro_cntdd_mss_frmto  Char(3),   
			@vlr_prmtro_nmrco               Numeric(18,0),
			@vlr_prmtro_crctr               Char(200)    ,
			@vlr_prmtro_fcha                Datetime     ,
			@vsble_usro			            udtLogico    ,
			@tpo_dto_prmtro		            udtLogico    
    
	--Procedimiento que permite consultar los parametros generales para recuperar la cantidad de días antes de la fecha actual
	-- a tomar para la prog. entrega */

	Set @lcCdgo_prmtro_cntdd_mss_frmto  = '88'
	Set @vsble_usro                     = 'S'
	Set @tpo_dto_prmtro                 = ''

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	

	Set @fcha_crte_inco   = DATEADD(dd,DATEDIFF(dd,0,@fechaactual),0)
	Set	@fcha_crte_inco -= @vlr_prmtro_nmrco

	--Procedimiento que permite consultar los parametros generales para recuperar la cantidad de días posteriores a la fecha actual
	-- a tomar para la prog. entrega */
	Set @lcCdgo_prmtro_cntdd_mss_frmto  = '110'
	Set @vsble_usro                     = 'S'
	Set @tpo_dto_prmtro                 = ''

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output

	Set	@fechacrtelqdccn += @vlr_prmtro_nmrco
	Set	@fechacrtelqdccn = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))
	

	-- Validamos si existen programaciones de entrega por procesar
	if @cnsctvo_cdgo_det_prgrmcn_fcha_evnto is not null -- es por demanda
		Begin

			-- Validamos si existe la solicitud con la programacion de entrega
			if exists ( select 1 
						from gsa.tbASServiciosSolicitados  a with(nolock)
						Where a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = @cnsctvo_cdgo_det_prgrmcn_fcha_evnto)
				Begin
					set @cdgo_rsltdo = @codigoExito
					set @mnsje_rsltdo = @mensajeExito

					-- Recuperamos el consecutivo de la solicitud
					select	@cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
					from	gsa.tbASServiciosSolicitados  a with(nolock)
					Where	a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = @cnsctvo_cdgo_det_prgrmcn_fcha_evnto
					Return
				End
			If Not Exists ( Select 1	From		bdsisalud.dbo.tbProgramacionPrestacion a With(Nolock)	
										INNER JOIN	bdsisalud.dbo.tbProgramacionFechaEvento b	With(Nolock)	
										ON			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
										INNER JOIN	bdsisalud.dbo.tbDetProgramacionFechaEvento c	With(Nolock)	
										ON			a.cnsctvo_prgrmcn_prstcn = c.cnsctvo_prgrmcn_prstcn 
										and         c.cnsctvo_prgrmcn_fcha_evnto =b.cnsctvo_prgrmcn_fcha_evnto  -- AJUSTADO por angela 19-05-2017
										And			(c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo))
										And			c.fcha_entrga between @fcha_crte_inco and @fechacrtelqdccn 
										INNER JOIN	bdsisalud.dbo.tbCodificaciones d With(Nolock)	
										ON			d.cnsctvo_cdfccn = a.cnsctvo_prstcn
										INNER JOIN	bdsisalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
										ON			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
										AND			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha 
										AND			e.estdo = @estdo
										INNER JOIN	bdsisalud.dbo.tbAfiliadosMarcados f with(nolock)
										ON			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
										And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
										Where		c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = @cnsctvo_cdgo_det_prgrmcn_fcha_evnto)
				Begin
					Set @cdgo_rsltdo = @codigoError
					Set  @mnsje_rsltdo = 'La Programación De Entrega Ya Esta Asociada En Aplicativo Anterior ó Corresponde a Una Fecha Posterior'
					Return
				End
		End
	Else -- Es masivo
		Begin

			If Not Exists ( Select 1	From		bdsisalud.dbo.tbProgramacionPrestacion a With(Nolock)	
										INNER JOIN	bdsisalud.dbo.tbProgramacionFechaEvento b	With(Nolock)	
										ON			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
										INNER JOIN	bdsisalud.dbo.tbDetProgramacionFechaEvento c	With(Nolock)	
										ON			a.cnsctvo_prgrmcn_prstcn = c.cnsctvo_prgrmcn_prstcn 
										and         c.cnsctvo_prgrmcn_fcha_evnto =b.cnsctvo_prgrmcn_fcha_evnto  -- AJUSTADO por angela 19-05-2017
										And			(c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo))
										And			c.fcha_entrga between @fcha_crte_inco and @fechacrtelqdccn 
										INNER JOIN	bdsisalud.dbo.tbCodificaciones d With(Nolock)	
										ON			d.cnsctvo_cdfccn = a.cnsctvo_prstcn
										INNER JOIN	bdsisalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
										ON			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
										AND			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha 
										AND			e.estdo = @estdo
										INNER JOIN	bdsisalud.dbo.tbAfiliadosMarcados f with(nolock)
										ON			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
										And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna)
				Begin
					Set @cdgo_rsltdo = @codigoExito
					Set @mnsje_rsltdo = 'No Existen Programaciones De Entrega Para Procesar Se Cancela el Proceso'
					Return
				End
			End


	-- Creamos Tablas Temporales
	/*::::::::::::CREACIÓN DE TABLAS TEMPORALES QUE GUARDARÁN LOS CONSECUTIVOS GENERADOS POR CADA INSERCIÓN:::::::::::::: 
		:::::::::::::ÉSTO CON EL FIN DE EVITAR LA CONSULTA A TABLAS REALES QUE PUEDAN DEMORAR EL PROCESAMIENTO:::::::::::::::*/	 
	CREATE TABLE #Idsolicitudes		(cnsctvo_slctd_atrzcn_srvco					udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdAfiliados		(cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)			
	CREATE TABLE #IdIPS				(cnsctvo_ips_slctd_atrzcn_srvco				udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdHospitalaria	(cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)					
	CREATE TABLE #IdMedico			(cnsctvo_mdco_trtnte_slctd_atrzcn_srvco		udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdDiagnostico		(cnsctvo_dgnstco_slctd_atrzcn_srvco			udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServicios		(cnsctvo_srvco_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServiciosOr		(cnsctvo_dto_srvco_slctdo_orgnl				udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdMedicamentos	(cnsctvo_mdcmnto_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdInsumos			(cnsctvo_prcdmnto_insmo_slctdo				udtConsecutivo NOT NULL, idXML	int NOT NULL)

	Create Table #tbDetProgramacion		  
	(
		id_tbla								int identity,
		cnsctvo_prgrmcn_fcha_evnto			UdtConsecutivo,
		cnsctvo_prgrmcn_prstcn				UdtConsecutivo,
		cnsctvo_det_prgrmcn_fcha			UdtConsecutivo,
		cnsctvo_cdgo_ntfccn					UdtConsecutivo,
		cnsctvo_cdgo_ofcna					UdtConsecutivo,
		cnsctvo_pr_ntfccn					Varchar(50),
		cnsctvo_prstcn						UdtConsecutivo,
		cnsctvo_cdgo_clsfccn_cdfccn			UdtConsecutivo,
		fcha_dsde							datetime,
		fcha_hsta							datetime,
		fcha_entrga							datetime,
		fcha_rl_entrga						datetime,
		cnsctvo_cdgo_estds_entrga			UdtConsecutivo,
		cdgo_intrno							UdtCodigoIps,
		nmro_unco_idntfccn_prstdr			UdtConsecutivo default 0,
		cntdd								int,
		cnsctvo_cdgo_dgnstco				UdtConsecutivo,
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	udtConsecutivo,
		mrca_lsta_chk						UdtLogico default 'N',
		nmro_acta							char(50),
		cnsctvo_cdgo_clsfccn_evnto          UdtConsecutivo,
		nmro_unco_idntfccn_afldo			UdtConsecutivo,
		cnsctvo_cdgo_tpo_cntrto				udtConsecutivo,
		nmro_cntrto							char(15),
		cnsctvo_bnfcro						UdtConsecutivo,
		cnsctvo_cdgo_pln					UdtConsecutivo,
		mrca_ops_vrtl						UdtLogico default 'N',
		mrca_cnvno							UdtLogico default 'N',
		mrca_lqdcn							UdtLogico default 'N',
	) 

	CREATE TABLE #Tempo_Solicitudes
	( 
		id_tbla								int identity
		,id									Int  NOT NULL										
		,cnsctvo_cdgo_orgn_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_srvco_slctdo		udtConsecutivo NULL
		,cnsctvo_cdgo_prrdd_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_ubccn_pcnte		udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_slctd				udtConsecutivo NULL
		,jstfccn_clnca						varchar(2000) NULL
		,cnsctvo_cdgo_mdo_cntcto_slctd		udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_trnsccn_srvco_sld	udtConsecutivo NULL
		,nmro_slctd_prvdr					varchar(15) NULL
		,nmro_slctd_pdre					varchar(15) NULL
		,cdgo_eps							char(10) NULL
		,cnsctvo_cdgo_clse_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_frma_atncn			udtConsecutivo NULL
		,fcha_slctd							datetime NULL
		,anio_slctd							char(4) NULL
		,nmro_slctd_atrzcn_ss				varchar(16) NULL
		,nmro_slctd_ss_rmplzo				varchar(15) NULL
		,cnsctvo_cdgo_ofcna_atrzcn			udtConsecutivo NULL
		,nuam								numeric(18,0) NULL
		,srvco_hsptlzcn						varchar(30) NULL
		,ga_atncn							varchar(30) NULL
		,cnsctvo_prcso_vldcn				udtConsecutivo NULL
		,cnsctvo_prcso_mgrcn				udtConsecutivo NULL
		,mgrda_gstn							int NULL
		,spra_tpe_st						udtLogico NULL
		,dmi								udtLogico NULL
		,obsrvcn_adcnl						UdtObservacion NULL
		,usro_crcn							udtUsuario NULL
		,cdgo_orgn_atncn					udtCodigo NULL
		,cdgo_tpo_srvco_slctdo				udtCodigo NULL
		,cdgo_prrdd_atncn					udtCodigo NULL
		,cdgo_tpo_ubccn_pcnte				udtCodigo NULL
		,cdgo_clse_atncn					char(3) NULL
		,cdgo_frma_atncn					udtCodigo NULL
		,cdgo_mdo_cntcto_slctd				udtCodigo NULL
		,cdgo_tpo_trnsccn_srvco_sld			udtCodigo NULL
		,cnsctvo_cdgo_tpo_orgn_slctd		UdtConsecutivo NULL							
	)


	CREATE TABLE #Tempo_Afiliados 
	( 
		id_tbla										int identity
		,id											Int	NOT NULL
		,cnsctvo_cdgo_tpo_idntfccn_afldo			udtConsecutivo NULL default 0
		,nmro_idntfccn_afldo						udtNumeroIdentificacionLargo NULL
		,cnsctvo_cdgo_dprtmnto_afldo				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cdd_rsdnca_afldo				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cbrtra_sld					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_pln						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_pln							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_estdo_pln						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_sxo							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_vnclcn_afldo				udtConsecutivo NULL default 0
		,nmro_unco_idntfccn_afldo					udtConsecutivo NULL default 0
		,cnsctvo_afldo_slctd_orgn					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_estdo_drcho					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_cntrto					udtConsecutivo NULL default 0
		,nmro_cntrto								udtNumeroFormulario NULL default ''
		,cnsctvo_bnfcro_cntrto						udtConsecutivo NULL default 0
		,cdgo_ips_prmra								udtCodigoIps NULL default ''
		,cnsctvo_cdgo_sde_ips_prmra					udtConsecutivo NULL default 0
		,rcn_ncdo									udtLogico default 'N'
		,prto_mltple								udtLogico default 'N'
		,nmro_hjo_afldo								udtConsecutivo default 0
		,cnsctvo_cdgo_sxo_rcn_ncdo					udtConsecutivo default 0
		,ttla										udtLogico NULL default 'N'
		,cnsctvo_cdgo_chrte							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_rngo_slrl						udtConsecutivo NULL default 0
		,usro_crcn									udtUsuario NULL					
		,prmr_aplldo_afldo							udtApellido NULL
		,sgndo_aplldo_afldo							udtApellido NULL
		,prmr_nmbre_afldo							udtNombre NULL
		,sgndo_nmbre_afldo							udtNombre NULL
		,cdgo_tpo_idntfccn_afldo					udtCodigo NULL
		,fcha_ncmnto_afldo							datetime NULL
		,drccn_afldo								udtDireccion NULL
		,tlfno_afldo								udtTelefono NULL
		,cdgo_dprtmnto_afldo						char(3) NULL
		,dscrpcn_dprtmnto_afldo						udtDescripcion NULL
		,cdgo_cdd_rsdnca_afldo						udtCiudad NULL
		,dscrpcn_cdd_rsdnca_afldo					udtDescripcion NULL
		,tlfno_cllr_afldo							char(30) NULL
		,crro_elctrnco_afldo						udtEmail NULL
		,cdgo_cbrtra_sld							varchar(5) NULL
		,cdgo_pln									udtCodigo NULL
		,cdgo_sxo									udtCodigo NULL
		,cdgo_sxo_rcn_ncdo							udtCodigo NULL
		,cdgo_tpo_pln								udtCodigo NULL
		,cdgo_tpo_vnclcn_afldo						udtCodigo NULL
		,cdgo_estdo_pln								udtCodigo NULL
		,fcha_ncmnto_rcn_ncdo						datetime NULL
		,edd_afldo_ans								UdtConsecutivo
		,edd_afldo_mss								UdtConsecutivo
		,edd_afldo_ds								UdtConsecutivo
	)

	CREATE TABLE #Tempo_IPS		
	(  
		id_tbla										int identity
		,id											Int NOT NULL	
		,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte		udtConsecutivo NULL default 0
		,nmro_idntfccn_ips_slctnte					udtNumeroIdentificacionLargo NULL default ''
		,cdgo_intrno								udtCodigoIps NULL default ''
		,nmro_indctvo_prstdr						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_dprtmnto_prstdr				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cdd_prstdr					udtConsecutivo NULL default 0
		,cnsctvo_ips_slctd_orgn						udtConsecutivo NULL default 0
		,adscrto									udtLogico NULL default 'N'
		,nmro_unco_idntfccn_prstdr					udtConsecutivo NULL default 0
		,usro_crcn									udtUsuario NULL	
		,nmbre_ips									udtDescripcion NULL
		,nmbre_scrsl								udtDescripcion NULL
		,cdgo_tpo_idntfccn_ips_slctnte				udtCodigo NULL default ''
		,dgto_vrfccn								int NULL
		,cdgo_hbltcn								char(12) NULL
		,drccn_prstdr								udtDireccion NULL
		,tlfno_prstdr								udtTelefono NULL
		,cdgo_dprtmnto_prstdr						char(3) NULL
		,cdgo_cdd_prstdr							udtCiudad NULL																
	)

	

	CREATE TABLE #Tempo_Medicos 
	(   
		id_tbla									int identity
		,id										Int	NOT NULL
		,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		udtConsecutivo default 0
		,cnsctvo_cdgo_espcldd_mdco_trtnte		udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	udtConsecutivo NULL default 0
		,nmro_idntfccn_mdco_trtnte				udtNumeroIdentificacionLargo NULL default ''
		,rgstro_mdco_trtnte						varchar(20) NULL default ''
		,cnsctvo_mdco_trtnte_slctd_orgn			udtConsecutivo default 0
		,adscrto								udtLogico NULL default 'N'
		,nmro_unco_idntfccn_mdco				udtConsecutivo NULL default 0
		,usro_crcn								udtUsuario NULL
		,cdgo_espcldd_mdco_trtnte				char(3) NULL default ''
		,cdgo_tpo_idntfccn_mdco_trtnte			char(2) NULL default ''
		,prmr_nmbre_mdco_trtnte					udtNombre NULL
		,sgndo_nmbre_mdco_trtnte				udtNombre NULL
		,prmr_aplldo_mdco_trtnte				udtApellido NULL
		,sgndo_aplldo_mdco_trtnte				udtApellido NULL
		,cdgo_tpo_aflcn_mdco_trtnte				udtCodigo default ''
		,cnsctvo_cdgo_ntfccn					udtConsecutivo NULL
		,cnsctvo_cdgo_ofcna						udtConsecutivo NULL
		,cnsctvo_cdgo_ctc						udtConsecutivo NULL
	)

	CREATE TABLE #Tempo_Diagnostico 
	( 
		id_fila						int identity
		,id							Int NOT NULL
		,nmro_prstcn				INT	default 0	
		,cnsctvo_cdgo_tpo_dgnstco	udtConsecutivo NULL default 0
		,cnsctvo_cdgo_dgnstco		udtConsecutivo NULL
		,cnsctvo_cdgo_cntngnca		udtConsecutivo NULL
		,cnsctvo_cdgo_rcbro			udtConsecutivo NULL default 9
		,usro_crcn					udtUsuario NULL
		,cdgo_dgnstco				udtCodigoDiagnostico NULL
		,cdgo_tpo_dgnstco			char(3) NULL
		,cdgo_cntngnca				Char(3) NULL
		,cdgo_rcbro					char(3) NULL
	)

	CREATE TABLE #Tempo_Prestaciones 
	(	
		id_fila									INT IDENTITY(1,1)
		,id										Int NOT NULL
		,nmro_prstcn							INT default 0
		,cnsctvo_cdgo_srvco_slctdo				udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_srvco					udtConsecutivo NULL
		,dscrpcn_srvco_slctdo					udtDescripcion NULL 
		,cntdd_slctda							int NULL
		,fcha_prstcn_srvco_slctdo				datetime NULL
		,tmpo_trtmnto_slctdo					int NULL
		,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	udtConsecutivo NULL default 0
		,vlr_lqdcn_srvco						udtConsecutivo NULL default 0
		,cnsctvo_srvco_slctdo_orgn				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_atrzcn				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_prstcn_prstdr				udtConsecutivo NULL default 0
		,cnfrmcn_cldd							udtLogico NULL
		,usro_crcn								udtUsuario NULL
		,cdgo_tpo_srvco							udtCodigo NULL
		,cdgo_srvco_slctdo						char(15) NULL
		,cdgo_undd_tmpo_trtmnto_slctdo			udtCodigo NULL
		,cdgo_prstcn_prstdr						udtCodigo NULL
		,cdgo_tpo_atrzcn						udtCodigo NULL								
		,dss									float NULL
		,cnsctvo_cdgo_prsntcn_dss				udtConsecutivo NULL default 0
		,prdcdd_dss								float NULL
		,cnsctvo_cdgo_undd_prdcdd_dss			udtConsecutivo NULL default 0
		,cnsctvo_cdgo_frma_frmctca				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_grpo_trptco				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_prsntcn					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_va_admnstrcn_mdcmnto		udtConsecutivo NULL default 0
		,cnsctvo_mdcmnto_slctdo_orgn			udtConsecutivo NULL default 0
		,cncntrcn_dss							char(20) NULL
		,prsntcn_dss							char(20) NULL
		,cnsctvo_cdgo_undd_cncntrcn_dss			udtConsecutivo NULL
		,cdgo_prsntcn_dss						udtCodigo NULL
		,cdgo_undd_prdcdd_dss					udtCodigo NULL
		,cdgo_frma_frmctca						udtCodigo NULL
		,cdgo_grpo_trptco						char(3) NULL
		,cdgo_prsntcn							udtCodigo NULL
		,cdgo_va_admnstrcn_mdcmnto				udtCodigo NULL
		,cdgo_undd_cncntrcn_dss					char(10) NULL									
		,cnsctvo_cdgo_ltrldd					udtConsecutivo NULL
		,cnsctvo_cdgo_va_accso					udtConsecutivo NULL
		,cdgo_ltrldd							udtCodigo NULL
		,cdgo_va_accso							udtcodigo NULL
		,cnsctvo_cdgo_dt_prgrmcn_fcha_evnto		udtConsecutivo NULL
		,prgrmcn_entrga							udtLogico default 'S'
	)	

	CREATE TABLE #solicitudes_guardadas (id int identity(1,1) NOT NULL, cnsctvo_slctd_atrzcn_srvco udtconsecutivo NOT NULL, Identificador_sol udtconsecutivo NOT NULL)
	
	if @cnsctvo_cdgo_det_prgrmcn_fcha_evnto is null -- si el proceso es masivo activamos control de proceso
		Begin
			-- Registramos el inicio proceso automático log de procesos "25- Proceso Automático Creación Solicitudes Con Programación Entrega Masivo"
			   exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output
			
			Begin Transaction prog_entrega
			Begin Try
					-- Poblar temporales para el proceso de Creación Solicitudes con Programación de Entrega
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 1. Inserción a tablas TMP - Programación, Afiliados,prestadores, médicos, Diagnosticos, prestaciones'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_80,@cnsctvo_lg output
						EXEC gsa.spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega @cnsctvo_cdgo_det_prgrmcn_fcha_evnto,@usro_prcso
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try

					Begin Catch
					    -- error en paso 1
						select	@estdo_pso1 = @codigoError,
								@estdo_pso2 = @codigoSalir,
								@estdo_pso3 = @codigoSalir,
								@estdo_pso4 = @codigoSalir,
								@estdo_pso5 = @codigoSalir,
								@estdo_pso6 = @codigoSalir,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
					End Catch

					-- Grabar solicitudes
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 2. Grabar Solicitudes - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_81,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudSolicitudes 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 2
						select	@estdo_pso2 = @codigoError,
								@estdo_pso3 = @codigoSalir,
								@estdo_pso4 = @codigoSalir,
								@estdo_pso5 = @codigoSalir,
								@estdo_pso6 = @codigoSalir,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
						
					End Catch

					-- Grabar Afiliados
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar Solicitud Afiliados - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_82,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudAfiliados 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 3
						select	@estdo_pso3 = @codigoError,
								@estdo_pso4 = @codigoSalir,
								@estdo_pso5 = @codigoSalir,
								@estdo_pso6 = @codigoSalir,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
						
					End Catch
				
					-- Grabar IPS - Prestador
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 4. Grabar Solicitud Prestador - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_83,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudIPS 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 4
						select	@estdo_pso4 = @codigoError,
								@estdo_pso5 = @codigoSalir,
								@estdo_pso6 = @codigoSalir,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
					End Catch

					--Grabar Informacion de Medico
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 5. Grabar Solicitud Médico - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_84,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudMedico
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 5
						select	@estdo_pso5 = @codigoError,
								@estdo_pso6 = @codigoSalir,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
						
					End Catch 

					--Grabar Diagnostico
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 6. Grabar Solicitud Diagnóstico - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_85,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudDiagnostico 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 6
						select	@estdo_pso6 = @codigoError,
								@estdo_pso7 = @codigoSalir,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
					
					End Catch

					-- Grabar Prestaciones
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 7. Grabar Solicitud Prestaciones - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_86,@cnsctvo_lg output
						EXEC gsa.spASGrabarSolicitudPrestaciones 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 7
						select	@estdo_pso7 = @codigoError,
								@estdo_pso8 = @codigoSalir,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
					End Catch

					-- Actualizar Asociación y Estados Solicitudes - Programación Entrega
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 8. Actualizar Asociación y Estados Solicitudes - Programación Entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_87,@cnsctvo_lg output
						EXEC gsa.spASActualizarEstadosyAsociarSolicitudesConProgramacionEntrega @lcUsuario
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 8
						select	@estdo_pso8 = @codigoError,
								@estdo_pso9 = @codigoSalir,
								@estdo_pso10 = @codigoSalir;
						 throw
					End Catch

					-- Grabar Direccionamiento Prestador - Programación Entrega - fecha de entrega
					Begin Try
						set @mnsje_lg_evnto_prcso = 'Paso 9. Grabar Direccionamiento Prestador - Programación Entrega - fecha entrega'
						exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_121,@cnsctvo_lg output
						EXEC gsa.spASGrabarDireccionamientoPrestadorConProgramacionEntrega @lcUsuario
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						-- error en paso 9
						select	@estdo_pso9 = @codigoError,
								@estdo_pso10 = @codigoSalir;
						 throw
					End Catch


					/*::::::::::::::::GENERACIÓN DE NRO DE RADICADO DE LA SOLICITUD:::::::::::::::::::::::::::::::::::*/
					BEGIN TRY			
						BEGIN
							set @mnsje_lg_evnto_prcso = 'Paso 10. Generar Número de Radicado de La Solicitud - Programación Entrega'
							exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_122,@cnsctvo_lg output
							INSERT INTO		#solicitudes_guardadas(cnsctvo_slctd_atrzcn_srvco, identificador_Sol)
							SELECT	    	IDS.cnsctvo_slctd_atrzcn_srvco													
											,IDS.idXML
							FROM			#Idsolicitudes			IDS WITH (NOLOCK)
							INNER JOIN		#Tempo_Solicitudes		SOL	WITH (NOLOCK)
							ON				IDS.idXML = SOL.id		
				
							SELECT	@i = 1
									,@Max = Max(id)
							FROM	#solicitudes_guardadas 
							
							WHILE	@i <= @max
							BEGIN
								-- ACTUALIZAR SECUENCIA EN TABLA DE SOLICITUDES
								UPDATE		BdCNA.gsa.tbASSolicitudesAutorizacionServicios
								SET			nmro_slctd_atrzcn_ss =concat(convert(varchar(4),year(SOL.fcha_slctd)),'-',LTRIM(RTRIM(SOL.cdgo_mdo_cntcto_slctd)),'-',replicate('0' ,8-len(convert(varchar(100),SEC.vlr_scnca))),convert(varchar(100),SEC.vlr_scnca))
								FROM		BdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia			SEC WITH (NOLOCK)
								INNER JOIN	#Tempo_Solicitudes										SOL	WITH (NOLOCK)
								ON			SEC.prdo_scnca = SOL.anio_slctd
								INNER JOIN	#solicitudes_guardadas									SGU WITH (NOLOCK)
								ON			SGU.identificador_sol = SOL.id
								INNER JOIN	BdCNA.gsa.tbASSolicitudesAutorizacionServicios			SOP	WITH (NOLOCK)
								ON			SOP.cnsctvo_slctd_atrzcn_srvco = SGU.cnsctvo_slctd_atrzcn_srvco
								WHERE		SGU.id = @i
								-- ACTUALIZAR SECUENCIA EN TABLA DE SECUENCIAS
								UPDATE		BdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia
								SET			vlr_scnca = concat(replicate('0' ,8-len(convert(varchar(100),vlr_scnca + 1))) , convert(varchar(100),vlr_scnca + 1))
								FROM		#Tempo_Solicitudes		SOL WITH (NOLOCK)
								INNER JOIN  #IdSolicitudes			IDS WITH (NOLOCK)
								ON			IDS.idXML = SOL.id		
								Inner join	#solicitudes_guardadas	SGU WITH (NOLOCK)
								on			SGU.cnsctvo_slctd_atrzcn_srvco = IDS.cnsctvo_slctd_atrzcn_srvco
								WHERE		SGU.id = @i
								AND			prdo_scnca = SOL.anio_slctd
								SET			@i = @i + 1
							END
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
							
						END
					END TRY
					Begin Catch
						-- error en paso 10
						select	@estdo_pso10 = @codigoError;
						 throw
					End Catch
					set @cdgo_rsltdo = @codigoExito
					set @mnsje_rsltdo = @mensajeExito

					-- Registramos el fin del proceso Automático log de procesos "25- Proceso Automático Creación Solicitudes Con Programación Entrega Masivo"
					exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,2
				    commit transaction prog_entrega
			End Try
			Begin Catch
					-- se Presento un error
					Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
					set @cdgo_rsltdo = @codigoError
					set @mnsje_rsltdo = @mensajeError
					RollBack transaction prog_entrega

					-- Paso 1
					set @mnsje_lg_evnto_prcso = 'Paso 1. Inserción a tablas TMP - Programación, Afiliados,prestadores, médicos, Diagnosticos, prestaciones'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_80,@cnsctvo_lg output
					if @estdo_pso1 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso1 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 2
					set @mnsje_lg_evnto_prcso = 'Paso 2. Grabar Solicitudes - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_81,@cnsctvo_lg output
					if @estdo_pso2 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso2 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 3
					set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar Solicitud Afiliados - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_82,@cnsctvo_lg output

					if @estdo_pso3 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso3 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 4
					set @mnsje_lg_evnto_prcso = 'Paso 4. Grabar Solicitud Prestador - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_83,@cnsctvo_lg output

					if @estdo_pso4 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso4 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 5
					set @mnsje_lg_evnto_prcso = 'Paso 5. Grabar Solicitud Médico - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_84,@cnsctvo_lg output

					if @estdo_pso5 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso5 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 6
					set @mnsje_lg_evnto_prcso = 'Paso 6. Grabar Solicitud Diagnóstico - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_85,@cnsctvo_lg output

					if @estdo_pso6 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso6 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End
					-- Paso 7
					set @mnsje_lg_evnto_prcso = 'Paso 7. Grabar Solicitud Prestaciones - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_86,@cnsctvo_lg output

					if @estdo_pso7 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso7 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 8
					set @mnsje_lg_evnto_prcso = 'Paso 8. Actualizar Asociación y Estados Solicitudes - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_87,@cnsctvo_lg output

					if @estdo_pso8 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso8 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 9
					set @mnsje_lg_evnto_prcso = 'Paso 9. Grabar Direccionamiento Prestador - Programación Entrega - fecha entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_121,@cnsctvo_lg output

					if @estdo_pso9 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso9 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End

					-- Paso 10
					set @mnsje_lg_evnto_prcso = 'Paso 10. Generar Número de Radicado de La Solicitud - Programación Entrega'
					exec bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsje_lg_evnto_prcso,@tpo_rgstro_log_122,@cnsctvo_lg output

					if @estdo_pso10 = @codigoExito
					   Begin
							exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					   End
					if @estdo_pso10 = @codigoError
					   Begin
							Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
							Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
							return
					   End
					 
			End Catch
	End
	Else
		Begin
			Set @lcUsuario = @usro_prcso
			-- Poblar temporales para el proceso de Creación Solicitudes con Programación de Entrega
			EXEC gsa.spASPoblarTemporalesCreacionSolicitudesConProgramacionEntrega @cnsctvo_cdgo_det_prgrmcn_fcha_evnto,@usro_prcso
			-- Grabar Solicitud
			EXEC gsa.spASGrabarSolicitudSolicitudes

			-- Grabar Afiliados Solicitud
			EXEC gsa.spASGrabarSolicitudAfiliados

			-- Grabar Prestador Solicitud
			EXEC gsa.spASGrabarSolicitudIPS

			-- Grabar Medico Solicitud
			EXEC gsa.spASGrabarSolicitudMedico

			-- Grabar Diagnostico Solicitud
			EXEC gsa.spASGrabarSolicitudDiagnostico

			-- Grabar Prestaciones Solicitud
			EXEC gsa.spASGrabarSolicitudPrestaciones 

			-- Actualizar Asociación y Estados Solicitudes - Programación Entrega
			EXEC BdCna.gsa.spASActualizarEstadosyAsociarSolicitudesConProgramacionEntrega @lcUsuario
			-- Grabar Direccionamiento Prestador - Programación Entrega - fecha entrega
			EXEC gsa.spASGrabarDireccionamientoPrestadorConProgramacionEntrega @lcUsuario

			-- Generar Número de Radicado de La Solicitud - Programación Entrega
			INSERT INTO		#solicitudes_guardadas(cnsctvo_slctd_atrzcn_srvco, identificador_Sol)
			SELECT	    	IDS.cnsctvo_slctd_atrzcn_srvco													
							,IDS.idXML
			FROM			#Idsolicitudes			IDS WITH (NOLOCK)
			INNER JOIN		#Tempo_Solicitudes		SOL	WITH (NOLOCK)
			ON				IDS.idXML = SOL.id		
				
			SELECT	@i = 1
					,@Max = Max(id)
			FROM	#solicitudes_guardadas 

			WHILE	@i <= @max
			BEGIN
				-- ACTUALIZAR SECUENCIA EN TABLA DE SOLICITUDES
				UPDATE		BdCNA.gsa.tbASSolicitudesAutorizacionServicios
				SET			nmro_slctd_atrzcn_ss =concat(convert(varchar(4),year(SOL.fcha_slctd)),'-',LTRIM(RTRIM(SOL.cdgo_mdo_cntcto_slctd)),'-',replicate('0' ,8-len(convert(varchar(100),SEC.vlr_scnca))),convert(varchar(100),SEC.vlr_scnca))
				FROM		BdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia			SEC WITH (NOLOCK)
				INNER JOIN	#Tempo_Solicitudes										SOL	WITH (NOLOCK)
				ON			SEC.prdo_scnca = SOL.anio_slctd
				INNER JOIN	#solicitudes_guardadas									SGU WITH (NOLOCK)
				ON			SGU.identificador_sol = SOL.id
				INNER JOIN	BdCNA.gsa.tbASSolicitudesAutorizacionServicios			SOP	WITH (NOLOCK)
				ON			SOP.cnsctvo_slctd_atrzcn_srvco = SGU.cnsctvo_slctd_atrzcn_srvco
				WHERE		SGU.id = @i
				-- ACTUALIZAR SECUENCIA EN TABLA DE SECUENCIAS
				UPDATE		BdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia
				SET			vlr_scnca = concat(replicate('0' ,8-len(convert(varchar(100),vlr_scnca + 1))) , convert(varchar(100),vlr_scnca + 1))
				FROM		#Tempo_Solicitudes		SOL WITH (NOLOCK)
				INNER JOIN  #IdSolicitudes			IDS WITH (NOLOCK)
				ON			IDS.idXML = SOL.id		
				Inner join	#solicitudes_guardadas	SGU WITH (NOLOCK)
				on			SGU.cnsctvo_slctd_atrzcn_srvco = IDS.cnsctvo_slctd_atrzcn_srvco
				WHERE		SGU.id = @i
				AND			prdo_scnca = SOL.anio_slctd
				SET			@i = @i + 1
			END

			set @cdgo_rsltdo = @codigoExito
			set @mnsje_rsltdo = @mensajeExito

			-- Recuperamos el consecutivo de la solicitud
			select @cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			from gsa.tbASServiciosSolicitados  a with(nolock)
			Where a.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = @cnsctvo_cdgo_det_prgrmcn_fcha_evnto

			If	@@error	<> 0
				Begin
					set @cdgo_rsltdo = @codigoError
					set  @mnsje_rsltdo = @mensajeError
				End
				
		End		
	DROP TABLE #Idsolicitudes		
	DROP TABLE #IdAfiliados	
	DROP TABLE #IdIPS
	DROP TABLE #IdHospitalaria
	DROP TABLE #IdMedico								
	DROP TABLE #IdDiagnostico						
	DROP TABLE #IdServicios							
	DROP TABLE #IdServiciosOr		
	DROP TABLE #IdMedicamentos								
	DROP TABLE #IdInsumos
	Drop Table #tbDetProgramacion
	Drop Table #Tempo_Solicitudes
	Drop Table #Tempo_Afiliados
	Drop Table #Tempo_IPS
	Drop Table #Tempo_Medicos
	Drop Table #Tempo_Diagnostico
	Drop Table #Tempo_Prestaciones
	DROP TABLE #solicitudes_guardadas

End