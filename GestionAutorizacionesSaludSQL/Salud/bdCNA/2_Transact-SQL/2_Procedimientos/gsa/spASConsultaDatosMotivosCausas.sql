USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosMotivosCausas]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarMotivosCausas
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Devuelve las causales dependiendo de la pantalla que solicite.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez AM\>
* Descripcion			 : <\DM	Se modifica SP para devolver si la cuasal requiere numero de 
								autorización IPS DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosMotivosCausas] 
@cnsctvo_cdgo_tpo_csa_nvdd		udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	Declare @ldfcha_consulta	datetime,
			@dato_visible	char(1) = 'S';
	
	SET	@ldfcha_consulta = GETDATE();

	SELECT	cnsctvo_cdgo_csa_nvdd, 
			cdgo_csa_nvdd, 
			dscrpcn_cdgo_csa_nvdd,
			rqre_nmro_atrzcn_ips
	from bdcna.prm.tbASCausaNovedad_Vigencias WITH (NOLOCK)
	where cnsctvo_cdgo_tpo_csa_nvdd = @cnsctvo_cdgo_tpo_csa_nvdd 
	And	  @ldfcha_consulta between inco_vgnca and fn_vgnca and vsble_usro = @dato_visible;
END

GO
