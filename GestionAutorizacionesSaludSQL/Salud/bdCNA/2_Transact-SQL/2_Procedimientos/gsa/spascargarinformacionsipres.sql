USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spascargarinformacionsipres]    Script Date: 21/07/2017 03:31:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	* Metodo o PRG       : spASCargarInformacionSipres
	* Desarrollado por   : <\A Jhon Olarte     A\>    
	* Descripcion        : <\D Procedimiento encargado de extraer la información necesaria del otros	   D\>    
						 :     sistemas para realizar la replicación al sistema destino de Sipres.  D\>    
	* Observaciones      : <\O      O\>    
	* Parametros         : <\P		P\>    
	* Variables          : <\V      V\>    
	* Fecha Creacion     : <\FC 02/05/2016  FC\>    
	*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	* DATOS DE MODIFICACION    
	*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	* Modificado Por     : <\AM Jonathan Chamizo Quina  AM\>    
	* Descripcion        : <\D Se adiciona campo cnsctvo_cdgo_mdlo_cra y cnsctvo_slctd_mga al insert sobre
						 :     la tabla bdsisalud.dbo.tbactua.
						 :     Se guardan datos en la tabla BDCna.dbo.spPMCrearHistoricoEstadosAtencionOPS 
						 :     cuando el estado de la OPS  es autorizado o impreso.
						 :	   Se guardan datos en la tabla BDCna.dbo.tbconceptosanulados cuando el estado 
						 :     de la OPS es autorizado o impreso.
						 :     Se adiciona script para validar si existe el SP en la BD. En caso contrario
						 :     crea con la instrucción As Select 1										D\> 
	* Nuevas Variables   : <\VM   VM\>    
	* Fecha Modificacion : <\FM  2016-09-27  FM\>    
	*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Luis Fernando Benavides  AM\>    
* Descripcion        : <\D 1.Implementacion spPMCrearHistoricoEstadosAtencionOPSMasivo, masivo que registra 
				     :     la historia de cambios en el estado de la atencion de la OPS para mejorar 
					 :     rendimiento.
					 :     2.Ajusta Merge que pobla la tabla bdsisalud.dbo.tbprocedimientos, se suma la 
					 :     la cantidad solicitada y se omiten los campos cnsctvo_srvco_slctdo y 
					 :     cnsctvo_prcdmnto_slctdo para evitar duplicacion de datos
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-13  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento para que no tome las solicitudes y prestaciones
                           con estado 0 y adicionalmente se agrega el campo de estado en la tabla de lo 
						   conceptos							     
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-19  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento para adicionar en la tabla temporal de solicitudes el estado de la atencion, con el estado del concepto o 
                           el estado de la solicitud cuando no se tengan conceptos de gasto asociados
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                           Se modifica el procedimiento para insertar el valor de liquidacion sobre la tabla de tbconceptosops
                           Adicionalmente se cambia el valor @cnsctvo_cdgo_csa de 10033 a 0 para solucionar NC 	0011741 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-11-11  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01 AM\>    
* Descripcion        : <\D Se modifica el procedimiento para agregar  el manejo de la nueva funcionalidad
							de Envio de OPS Virtual correspondiente a solicitudes creadas por proceso automático de programación de entrega 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                            Se modifica el procedimiento que se actualice o inserte el campo cnsctvo_cdgo_ofcna_atncn de la tabla tbatencionops
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-30  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D 
                            Se cambia la seccion de creacion de marca de ops virtual y se agrega el llamado al procedimiento spASMigrarMarcasSipres
							donde se pone esta marca y se migran las marcas de los servicios.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-16  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D 
                            Se cambia referencia de la tabla fuente para sacar el nmro_unco_idntfccn_prstdr y el cdgo_intrno 
							al llenar la tabla bdsisalud.dbo.tbconceptosops
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-18  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D 
                            Se modifica merge de tbprocedimientos para evitar que el mismo registro se repita y se agrega 
							update para actualizar la fecha de ultima modificacion en esta tabla
					   D\> 
* Nuevas Variables   : <\VM @fecha: almacena la fecha actual  VM\>    
* Fecha Modificacion : <\FM  2017-01-24  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
                            Se quitan campos fcha_estmda_entrga_fnl y cnsctvo_cdgo_grpo_entrga en el guardado en la tabla tbProcedimientos,
							Se quita campo fcha_estmda_entrga_fnl en el guardado en la tabla tbAtencionOps,
							Se quitan campos cnsctvo_cdgo_mdlo_cra y cnsctvo_slctd_mga en el guardado en la tabla tbActua y
							Se agrega merge para guardar en la tabla relacion bdSisalud.dbo.tbASDatosAdicionalesMegaSipres
					   D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  2017-01-27  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
                            Se agrega Merge para migrar la empresa a la tabla bdsisalud.dbo.TbInfempresasxafiliado
							Se mo difica merge de guardado en tbConceptosOps, se coloca como fecha de ingreso la fecha de ultima modificacion, 
							el cnsctvo_cdgo_frma_lqdcn_prstcn se pone en 2 por defecto, el campo ds_vldz en 0, cnsctvo_cdgo_frma_lqdcn_prstcn en 2,
							cnsctvo_cdgo_frma_lqdcn_cncpto en 2
					   D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  2017-01-30  FM\>    
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                            En el campo fcha_expdcn de la tabla tbAtencionOps guardaba el campo fcha_slctd
							de la tabla temporal #tmpinformacionslctd, se cambia el campo de referencia por 
							el campo fcha_crcn de la misma tabla temporal para guardar en el campo fcha_expdcn de la tabla tbAtencionOps
					   D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  2017-02-02  FM\>    
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se deja el campo cdgo_intrno_prvdr_insmo en vacio por defecto, ya que se presento error en SIPRES
						   al consultar las solicitudes
					   D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM  2017-02-03  FM\>    
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se cambia el valor migrado al campo nmro_slctd_orgn de la tabla tbAtencionOps, 
						   se deja por defecto en Null.
					   D\> 
* Nuevas Variables   : <\VM @vlr_nll VM\>    
* Fecha Modificacion : <\FM  2017-03-02  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se mueve update que obtiene el campo cnsctvo_atncn_ops de la tabla tbAtencionOps al procedimiento
						   spASMigrarMarcasSipres
					   D\> 
* Nuevas Variables   : <\VM @vlr_nll VM\>    
* Fecha Modificacion : <\FM  2017-03-14  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se agrega llamado al procedimiento gsa.spAsMigracionCuotasDeRecuperacionSipres
						   el cual migra las cuotas de recuperacion
					   D\> 
* Nuevas Variables   : <\VM @vlr_nll VM\>    
* Fecha Modificacion : <\FM  2017-03-31  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se retira llamado a procedimientos spASMigrarMarcasSipres y spAsMigracionCuotasDeRecuperacionSipres
						   para ponerlos en el sp orquestador y guardar log del proceso.
					   D\> 
* Nuevas Variables   : <\VM @vlr_nll VM\>    
* Fecha Modificacion : <\FM  2017-04-03  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se agrega migracion del campo cnsctvo_cdgo_tpo_srvco de tbAsServiciosSolicitados a la tabla 
						   tbAtencionOPS campo cnsctvo_cdgo_tpo_cdfccn
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-04-04  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se cambia el campo fuente que guarda en tbprocedimientos (cnsctvo_det_prgrmcn_fcha)
						   se deja comentado el campo anterior para futura referencia.
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-04-10  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se agrega comando para inactivar trigger TrIncidencias_NovIdentificaciontbActua
						   de tbActua
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-05-31  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se agrega parametro que indica si el proceso es masivo o por demanda, de forma que si es por demanda
						   no desactive el trigger TrIncidencias_NovIdentificaciontbActua de tbActua.
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-06-07  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D  
                           Se ajusta procedimientos para guardar la informacion de hospitalizacion.
					   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  2017-06-27  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------*/


ALTER Procedure [gsa].[spascargarinformacionsipres]
		@es_btch				udtLogico
As
Begin
  Set NoCount On

  Declare @fcha_actl Datetime,
          @usro udtusuario,
          @ngcn Char(1),
          @orgn_lqdcn_cncpto Char(1),
          @cnsctvo_cdgo_estdo_cnts Int,
          @rcbro Int,
          @cnsctvo_cdgo_estdo Int,
          @cnsctvo_cdgo_estdo_prcso Int,
          @cnsctvo_imprsn Int,
		  @cnsctvo_cdgo_mdlo_cra Int, 
		  @estdo_sprs_aprvdo Int,
		  @estdo_sprs_imprso Int,
		  @cnsctvo_cdgo_csa	Int,
		  @cnsctvo_cdgo_prfl Int,
		  @cnsctvo_cdgo_mdo_cntcto_sprs Int,
		  @cnsctvo_prstcn_atrzda_prslctd udtConsecutivo,
		  @cnsctvo_cdgo_tpo_orgn_slctd3 Int = 3, /* '3-proceso Ops Virtual con Prog. Entrega' */
		  @cnsctvo_cdgo_estdo_mega11 Int = 11, /* '11-Autorizada' */
		  @lcnsctvo_mrca_atnc_ops Int = 0,
		  @lcodigoInterno_Empty   Char(8),
		  @lcVacio udtLogico,
		  @vlr_nll			Varchar(1),
		  @vlr_si			udtLogico;		 

		-- 
		CREATE Table #serviciosAutorizados_Impresos
		( 
			id						Int Identity(1,1),  
			cnsctvo_srvco_slctdo	udtconsecutivo,
			nuam					Numeric(18,0),  
			cnsctvo_cdgo_ofcna		Int,
			cnsctvo_atncn_ops		Int,    
			usro					udtusuario
		);

		-- 
		Create Table #TmpTbMarcasXAtencionOPS
		(
			id								Int Identity(1,1),
			cnsctvo_cdgo_tps_mrcs_ops		UdtConsecutivo,
			cnsctvo_atncn_ops				UdtConsecutivo,
			inco_vgnca						Date,
			fn_vgnca						Date,
			fcha_crcn						Date,
			fcha_fn_vldz_rgstro				Date,
			vldo							UdtLogico,
			usro_crcn						UdtUsuario,
			usro_ultma_mdfccn				UdtUsuario
		);

		-- 
		Create Table #tmpDatosAdicionalesMegaSipres
		(
			cnsctvo_slctd_mga		UdtConsecutivo
		);
  
		--
		Set @fcha_actl                    = GETDATE()
		Set @usro                         = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user))
		Set @ngcn                         = 'N'
		Set @orgn_lqdcn_cncpto            = 'C'
		Set @cnsctvo_cdgo_estdo_cnts      = 8
		Set @rcbro                        = 1
		Set @cnsctvo_cdgo_estdo           = 1
		Set @cnsctvo_cdgo_estdo_prcso     = 1
		Set @cnsctvo_imprsn               = 1
		Set @cnsctvo_cdgo_mdlo_cra        = 46  --Gestion Autorizaciones Salud
		Set @estdo_sprs_aprvdo            = 6 --bdsisalud..tbestadosatencion (APROBADA)
		Set @estdo_sprs_imprso            = 8 --bdsisalud..tbestadosatencion (IMPRESA)
		Set @cnsctvo_cdgo_csa             = 0   --10033,
		Set @cnsctvo_cdgo_prfl            = 121
		Set @cnsctvo_cdgo_mdo_cntcto_sprs = 4 --Medio de contacto - Sipres.
		Set @lcodigoInterno_Empty = ''
		Set @lcVacio = ''
		Set @vlr_nll = Null;
		Set @vlr_si = 'S';

		-- Se almacena la información de tbAtencionOPS
		Merge bdsisalud.dbo.tbatencionops With (RowLock) As Target
				Using (Select   iso.cnsctvo_slctd_atrzcn_srvco  , iso.cnsctvo_cdgo_ofcna       , iso.nuam                       ,
								iso.fcha_slctd                  , iso.cdgo_intrno              , iso.nmro_unco_idntfccn_prstdr  ,
								iso.nmro_unco_idntfccn_mdco     , iso.cnsctvo_cdgo_cntngnca    , iso.fcha_estmda_entrga         ,
								iso.cnsctvo_cdgo_clse_atncn     , iso.cnsctvo_cdgo_frma_atncn  , iso.obsrvcn_adcnl              ,
								iso.usro_crcn                   , iso.usro_ultma_mdfccn        , iso.fcha_ultma_mdfccn          ,
								iso.cnsctvo_cdgo_dgnstco        , iso.cnsctvo_cdgo_ofcna_atrzcn, iso.cnsctvo_cdgo_rcbro         ,
								iso.cnsctvo_cdgo_tpo_ubccn_pcnte, iso.nmro_slctd_prvdr         , iso.cnsctvo_cdgo_srvco_hsptlzcn,
								iso.cnsctvo_cdgo_mrbldd         , iso.cnsctvo_cdgo_tpo_atncn   , iso.cnsctvo_cdgo_estdo_mega    ,
								iso.cnsctvo_cdgo_estdo          , iso.hra_dgta                 , iso.nmro_atncn                 ,
								iso.hra_slctd_orgn              , iso.cnsctvo_cdgo_mdo_cntcto  , iaf.nmro_unco_idntfccn_afldo   ,
								iaf.cnsctvo_cdgo_pln            , iso.fcha_imprsn              , iso.tpo_prstcn                 ,
								iso.cnsctvo_cdgo_estdo_atncn	, iso.fcha_crcn				   , iss.cnsctvo_cdgo_tpo_srvco		,
								iso.crte_cnta
				From          #tmpinformacionsolicitud iso
				Inner Join    #tmpinformacionafiliado iaf
				On            iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
				Inner Join	  #tmpinformacionservicios iss
				On			  iss.id_agrpdr = iso.id
				Group By		iso.cnsctvo_slctd_atrzcn_srvco  , iso.cnsctvo_cdgo_ofcna       , iso.nuam                       ,
								iso.fcha_slctd                  , iso.cdgo_intrno              , iso.nmro_unco_idntfccn_prstdr  ,
								iso.nmro_unco_idntfccn_mdco     , iso.cnsctvo_cdgo_cntngnca    , iso.fcha_estmda_entrga         ,
								iso.cnsctvo_cdgo_clse_atncn     , iso.cnsctvo_cdgo_frma_atncn  , iso.obsrvcn_adcnl              ,
								iso.usro_crcn                   , iso.usro_ultma_mdfccn        , iso.fcha_ultma_mdfccn          ,
								iso.cnsctvo_cdgo_dgnstco        , iso.cnsctvo_cdgo_ofcna_atrzcn, iso.cnsctvo_cdgo_rcbro         ,
								iso.cnsctvo_cdgo_tpo_ubccn_pcnte, iso.nmro_slctd_prvdr         , iso.cnsctvo_cdgo_srvco_hsptlzcn,
								iso.cnsctvo_cdgo_mrbldd         , iso.cnsctvo_cdgo_tpo_atncn   , iso.cnsctvo_cdgo_estdo_mega    ,
								iso.cnsctvo_cdgo_estdo          , iso.hra_dgta                 , iso.nmro_atncn                 ,
								iso.hra_slctd_orgn              , iso.cnsctvo_cdgo_mdo_cntcto  , iaf.nmro_unco_idntfccn_afldo   ,
								iaf.cnsctvo_cdgo_pln            , iso.fcha_imprsn              , iso.tpo_prstcn                 ,
								iso.cnsctvo_cdgo_estdo_atncn	, iso.fcha_crcn				   , iss.cnsctvo_cdgo_tpo_srvco		,
								iso.crte_cnta
				) As Source (cnsctvo_slctd_atrzcn_srvco  , cnsctvo_cdgo_ofcna       ,
																										nuam                        , fcha_slctd               ,
																										cdgo_intrno                 , nmro_unco_idntfccn_prstdr,
																										nmro_unco_idntfccn_mdco     , cnsctvo_cdgo_cntngnca    ,
																										fcha_estmda_entrga          , cnsctvo_cdgo_clse_atncn  ,
																										cnsctvo_cdgo_frma_atncn     , obsrvcn_adcnl            ,
																										usro_crcn                   , usro_ultma_mdfccn        ,
																										fcha_ultma_mdfccn           , cnsctvo_cdgo_dgnstco     ,
																										cnsctvo_cdgo_ofcna_atrzcn   , cnsctvo_cdgo_rcbro       ,
																										cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         ,
																										cnsctvo_cdgo_srvco_hsptlzcn , cnsctvo_cdgo_mrbldd      ,
																										cnsctvo_cdgo_tpo_atncn      , cnsctvo_cdgo_estdo_mega  ,
																										cnsctvo_cdgo_estdo          , hra_dgta                 ,
																										nmro_atncn                  , hra_slctd_orgn           ,
																										cnsctvo_cdgo_mdo_cntcto     , nmro_unco_idntfccn_afldo ,
																										cnsctvo_cdgo_pln            , fcha_imprsn              ,
																										tpo_prstcn                  , cnsctvo_cdgo_estdo_atncn ,
																										fcha_crcn					, cnsctvo_cdgo_tpo_srvco   ,
																										crte_cnta
																										)
				On (Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna And 
					Target.nuam               = Source.nuam
					)
				When Matched Then
				Update
				Set fcha_expdcn                 = Source.fcha_crcn                   , --fcha_expdcn
					nmro_unco_idntfccn_afldo    = Source.nmro_unco_idntfccn_afldo    , --nmro_unco_idntfccn_afldo
					cnsctvo_cdgo_pln            = Source.cnsctvo_cdgo_pln            , --cnsctvo_cdgo_pln
					cdgo_intrno                 = Source.cdgo_intrno                 , --cdgo_intrno 
					nmro_unco_idntfccn_ips      = Source.nmro_unco_idntfccn_prstdr   , --nmro_unco_idntfccn_ips
					nmro_unco_idntfccn_mdco     = Source.nmro_unco_idntfccn_mdco     , --nmro_unco_idntfccn_mdco
					cnsctvo_cdgo_mrbldd         = IsNull(Source.cnsctvo_cdgo_mrbldd,0)         , --cnsctvo_cdgo_mrbldd
					cnsctvo_cdgo_cntngnca       = Source.cnsctvo_cdgo_cntngnca       , --cnsctvo_cdgo_cntngnca
					cnsctvo_cdgo_tpo_atncn      = 0                                  , --cnsctvo_cdgo_tpo_atncn
					fcha_entrga                 = Source.fcha_estmda_entrga          , --fcha_entrga,
					cnsctvo_cdgo_clse_atncn     = Source.cnsctvo_cdgo_clse_atncn     , --cnsctvo_cdgo_clse_atncn,
					cnsctvo_cdgo_frma_atncn     = Source.cnsctvo_cdgo_frma_atncn     , --cnsctvo_cdgo_frma_atncn,
					obsrvcn                     = Source.obsrvcn_adcnl               , --obsrvcn,
					usro_ingrsa                 = Source.usro_crcn                   , --usro_ingrsa,
					cnsctvo_cdgo_estdo          = Source.cnsctvo_cdgo_estdo_atncn    ,         --Source.cnsctvo_cdgo_estdo          , --cnsctvo_cdgo_estdo,
					hra_dgta                    = Source.hra_slctd_orgn              , --hra_dgta,
					fcha_imprsn                 = Source.fcha_imprsn                 , --fcha_imprsn,
					usro_ultma_mdfccn           = Source.usro_ultma_mdfccn           , --usro_ultma_mdfccn,
					fcha_ultma_mdfccn           = Source.fcha_ultma_mdfccn           , --fcha_ultma_mdfccn,
					cnsctvo_cdgo_dgnstco        = Source.cnsctvo_cdgo_dgnstco        , --cnsctvo_cdgo_dgnstco,
					nmro_atncn                  = Source.nuam                        , --nmro_atncn,
					cnsctvo_cdgo_ofcna_atncn    = Source.cnsctvo_cdgo_ofcna          , --cnsctvo_cdgo_ofcna_atncn,
					rcbro                       = @rcbro                             , --rcbro,
					cnsctvo_cdgo_rcbro          = Source.cnsctvo_cdgo_rcbro          , --cnsctvo_cdgo_rcbro,
					cnsctvo_cdgo_ptlga          = 0                                  , --cnsctvo_cdgo_ptlga,
					fcha_slctd_mdco             = Source.fcha_slctd                  , --fcha_slctd_mdco,
					tpo_prstcn                  = Source.tpo_prstcn                  , --tpo_prstcn,
					cnsctvo_cdgo_tpo_cdfccn     = Source.cnsctvo_cdgo_tpo_srvco      , --cnsctvo_cdgo_tpo_cdfccn,
					vlr_ttl_crte                = Source.crte_cnta                   , --vlr_ttl_crte,
					cnsctvo_cdgo_tpo_mrccn_ops  = 0                                  , --cnsctvo_cdgo_tpo_mrccn_ops,
					cnsctvo_cdgo_ubccn_pcnte    = Source.cnsctvo_cdgo_tpo_ubccn_pcnte, --cnsctvo_cdgo_ubccn_pcnte,
					fcha_slctd_orgn             = Source.fcha_slctd                  , --fcha_slctd_orgn,
					hra_slctd_orgn              = Source.hra_slctd_orgn              , --hra_slctd_orgn,
					cnsctvo_cdgo_srvco_hsptlzcn = Source.cnsctvo_cdgo_srvco_hsptlzcn  --cnsctvo_cdgo_srvco_hsptlzcn,
					
				When Not Matched Then
				Insert (cnsctvo_cdgo_ofcna      , nuam                       , fcha_expdcn            ,
						nmro_unco_idntfccn_afldo, cnsctvo_cdgo_pln           , cdgo_intrno            ,
						nmro_unco_idntfccn_ips  , nmro_unco_idntfccn_mdco    , cnsctvo_cdgo_mrbldd    ,
						cnsctvo_cdgo_cntngnca   , cnsctvo_cdgo_tpo_atncn     , fcha_entrga            ,
						cnsctvo_cdgo_clse_atncn , cnsctvo_cdgo_frma_atncn    , obsrvcn                ,
						usro_ingrsa             , cnsctvo_cdgo_estdo         , hra_dgta               ,
						ultmo_cncpto            , fcha_imprsn                , cnsctvo_imprsn         ,
						cnsctvo_cdgo_estdo_prcso, cnsctvo_cdgo_no_cpgo       , usro_ultma_mdfccn      ,
						fcha_ultma_mdfccn       , cnsctvo_cdgo_dgnstco       , nmro_atncn             ,
						cnsctvo_cdgo_ofcna_atncn, rcbro                      , cnsctvo_cdgo_rcbro     ,
						fcha_accdnt_trnsto      , fcha_enfrmdd_prfsnl        , cnsctvo_cdgo_ptlga     ,
						fcha_slctd_mdco         , tpo_prstcn                 , cnsctvo_cdgo_tpo_cdfccn,
						vlr_ttl_crte            , cnsctvo_cdgo_tpo_mrccn_ops , fcha_mrccn_ops         ,
						cnsctvo_cdgo_ubccn_pcnte, nmro_slctd_orgn            , fcha_slctd_orgn        ,
						hra_slctd_orgn          , cnsctvo_cdgo_srvco_hsptlzcn, cma_ips_slcta          ,
						cnsctvo_cdgo_ofcna_anlda, nuam_anlda
						)
				Values (Source.cnsctvo_cdgo_ofcna          , --cnsctvo_cdgo_ofcna
						Source.nuam                        , --nuam
						Source.fcha_crcn                  , --fcha_expdcn
						Source.nmro_unco_idntfccn_afldo    , --nmro_unco_idntfccn_afldo
						Source.cnsctvo_cdgo_pln            , --cnsctvo_cdgo_pln
						Source.cdgo_intrno                 , --cdgo_intrno 
						Source.nmro_unco_idntfccn_prstdr   , --nmro_unco_idntfccn_ips
						Source.nmro_unco_idntfccn_mdco     , --nmro_unco_idntfccn_mdco
						IsNull(Source.cnsctvo_cdgo_mrbldd,0)         , --cnsctvo_cdgo_mrbldd
						Source.cnsctvo_cdgo_cntngnca       , --cnsctvo_cdgo_cntngnca
						0                                  , --cnsctvo_cdgo_tpo_atncn
						Source.fcha_estmda_entrga          , --fcha_entrga,
						Source.cnsctvo_cdgo_clse_atncn     , --cnsctvo_cdgo_clse_atncn,
						Source.cnsctvo_cdgo_frma_atncn     , --cnsctvo_cdgo_frma_atncn,
						Source.obsrvcn_adcnl               , --obsrvcn,
						Source.usro_crcn                   , --usro_ingrsa,
						Source.cnsctvo_cdgo_estdo_atncn    , --Source.cnsctvo_cdgo_estdo          , --cnsctvo_cdgo_estdo,
						Source.hra_slctd_orgn              , --hra_dgta,
						Null                               , --ultmo_cncpto,
						Source.fcha_imprsn                 , --fcha_imprsn,
						Null                               , --cnsctvo_imprsn,
						Null                               , --cnsctvo_cdgo_estdo_prcso,
						Null                               , --cnsctvo_cdgo_no_cpgo,
						Source.usro_ultma_mdfccn           , --usro_ultma_mdfccn,
						Source.fcha_ultma_mdfccn           , --fcha_ultma_mdfccn,
						Source.cnsctvo_cdgo_dgnstco        , --cnsctvo_cdgo_dgnstco,
						Source.nuam                        , --nmro_atncn,
						Source.cnsctvo_cdgo_ofcna          , --cnsctvo_cdgo_ofcna_atncn,
						@rcbro                             , --rcbro,
						Source.cnsctvo_cdgo_rcbro          , --cnsctvo_cdgo_rcbro,
						Null                               , --fcha_accdnt_trnsto,
						Null                               , --fcha_enfrmdd_prfsnl,
						0                                  , --cnsctvo_cdgo_ptlga,
						Source.fcha_slctd                  , --fcha_slctd_mdco,
						Source.tpo_prstcn                  , --tpo_prstcn,
						Source.cnsctvo_cdgo_tpo_srvco      , --cnsctvo_cdgo_tpo_cdfccn,
						Source.crte_cnta                   , --vlr_ttl_crte,
						0                                  , --cnsctvo_cdgo_tpo_mrccn_ops,
						Null                               , --fcha_mrccn_ops,
						Source.cnsctvo_cdgo_tpo_ubccn_pcnte, --cnsctvo_cdgo_ubccn_pcnte,
						@vlr_nll						   , --nmro_slctd_orgn,
						Source.fcha_slctd                  , --fcha_slctd_orgn,
						Source.hra_slctd_orgn              , --hra_slctd_orgn,
						Source.cnsctvo_cdgo_srvco_hsptlzcn , --cnsctvo_cdgo_srvco_hsptlzcn,
						Null                               , --cma_ips_slcta,
						Null                               , --cnsctvo_cdgo_ofcna_Anlda,
						Null                                 --nuam_anlda,						
						
						);

		Update		ao
		Set			cnsctvo_cdgo_tpo_cdfccn = iss.cnsctvo_cdgo_tpo_srvco
		From		bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		Inner Join	#tmpinformacionsolicitud iso
		On			iso.nuam = ao.nuam
		And			iso.cnsctvo_cdgo_ofcna = ao.cnsctvo_cdgo_ofcna
		Inner Join  #tmpinformacionservicios iss
		On          iss.id_agrpdr = iso.id
		
		-- Guarda la informacion de las empresas relacionadas a las prestacones
		Merge bdsisalud.dbo.TbInfempresasxafiliado With(RowLock) As Target
		Using(
			 Select		iss.cnsctvo_cdgo_ofcna,				iss.nuam,						iea.nmro_unco_idntfccn_empldr,
						iea.nmbre_empldr,					iea.inco_vgnca_cbrnza,			iea.fn_vgnca_cbrnza,
						iea.slro_bse,						iea.cnsctvo_cdgo_tpo_cbrnza,	iea.cnsctvo_cdgo_entdd_arp,
						iea.fcha_ultma_mdfccn,				iea.usro_ultma_mdfccn
			From		#tempInformacionEmpresasxAfiliado iea
			Inner Join	#tmpinformacionsolicitud	iss
			On			iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
			Group By	iss.cnsctvo_cdgo_ofcna,				iss.nuam,						iea.nmro_unco_idntfccn_empldr,
						iea.nmbre_empldr,					iea.inco_vgnca_cbrnza,			iea.fn_vgnca_cbrnza,
						iea.slro_bse,						iea.cnsctvo_cdgo_tpo_cbrnza,	iea.cnsctvo_cdgo_entdd_arp,
						iea.fcha_ultma_mdfccn,				iea.usro_ultma_mdfccn
						
		) As Source
		On (
			Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna					And
			Target.nuam = Source.nuam												And
			Target.nmro_unco_idntfccn_empldr = Source.nmro_unco_idntfccn_empldr
		)
		When Not Matched Then
			Insert
			(
						cnsctvo_cdgo_ofcna,					nuam,							nmro_unco_idntfccn_empldr,
						nmbre_empldr,						inco_vgnca_cbrnza,				fn_vgnca_cbrnza,
						slro_bse,							cnsctvo_cdgo_tpo_cbrnza,		cnsctvo_cdgo_entdd_arp,
						fcha_ultma_mdfccn,					usro_ultma_mdfccn
			)
			Values	
			(	
				Source.cnsctvo_cdgo_ofcna,				Source.nuam,						Source.nmro_unco_idntfccn_empldr,
				Source.nmbre_empldr,					Source.inco_vgnca_cbrnza,			Source.fn_vgnca_cbrnza,
				Source.slro_bse,						Source.cnsctvo_cdgo_tpo_cbrnza,		Source.cnsctvo_cdgo_entdd_arp,
				Source.fcha_ultma_mdfccn,				Source.usro_ultma_mdfccn	
			)
		When Matched Then
			Update
			Set	Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna,					
				Target.nuam = Source.nuam,							
				Target.nmro_unco_idntfccn_empldr = Source.nmro_unco_idntfccn_empldr,
				Target.nmbre_empldr = Source.nmbre_empldr,						
				Target.inco_vgnca_cbrnza = Source.inco_vgnca_cbrnza,				
				Target.fn_vgnca_cbrnza = Source.fn_vgnca_cbrnza,
				Target.slro_bse = Source.slro_bse,							
				Target.cnsctvo_cdgo_tpo_cbrnza = Source.cnsctvo_cdgo_tpo_cbrnza,		
				Target.cnsctvo_cdgo_entdd_arp = Source.cnsctvo_cdgo_entdd_arp,
				Target.fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,					
				Target.usro_ultma_mdfccn = Source.usro_ultma_mdfccn;
				   
		--Se almacena la información de tbProcedimientos
		Merge bdsisalud.dbo.tbprocedimientos With (RowLock) As Target
		Using (Select  iso.cnsctvo_slctd_atrzcn_srvco, iso.cnsctvo_cdgo_ofcna               , iso.nuam                              ,
					iso.fcha_slctd                , iso.cdgo_intrno                      , iss.fcha_imprsn                       ,
					iss.cnsctvo_cdgo_srvco_slctdo , SUM(iss.cntdd_slctda) As cntdd_slctda, iss.cnsctvo_cdgo_ltrldd               ,
					@fcha_actl fcha_ultma_mdfccn  , iss.cnsctvo_cdgo_va_accso            , iss.usro_ultma_mdfccn                 ,
					iss.cnsctvo_cdgo_prsntcn      , iss.prstcn_cptda                     , iss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
					iss.cnsctvo_cdgo_tpo_srvco    , iss.cnsctvo_mdcmnto_slctdo           , iss.vlr_rfrnca                        ,
					iss.cpgo_lqudcn               , iss.cnsctvo_cdgo_estdo_mega          , iss.cnsctvo_cdgo_estdo                ,
					iss.cnsctvo_cdgo_ntfccn       , iss.nmro_acta                        , iss.cnsctvo_prgrmcn_prstcn            , 
					iss.cnsctvo_prgrmcn_fcha_evnto, iss.rqre_prvdr                       , iso.cnsctvo_cdgo_grpo_entrga			 ,
					iss.cnsctvo_det_prgrmcn_fcha
		From          #tmpinformacionsolicitud iso
		Inner Join    #tmpinformacionservicios iss
		On            iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And           iss.id_agrpdr = iso.id
		Group BY      iso.cnsctvo_slctd_atrzcn_srvco, iso.cnsctvo_cdgo_ofcna                , iso.nuam                      ,
					iso.fcha_slctd                , iso.cdgo_intrno                       , iss.fcha_imprsn               ,
					iss.cnsctvo_cdgo_srvco_slctdo , iss.cnsctvo_cdgo_ltrldd               , 
					iss.cnsctvo_cdgo_va_accso     , iss.usro_ultma_mdfccn                 , iss.cnsctvo_cdgo_prsntcn      ,
					iss.prstcn_cptda              , iss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto, iss.cnsctvo_cdgo_tpo_srvco    ,
					iss.cnsctvo_mdcmnto_slctdo    , iss.vlr_rfrnca                        , iss.cpgo_lqudcn               ,
					iss.cnsctvo_cdgo_estdo_mega   , iss.cnsctvo_cdgo_estdo                , iss.cnsctvo_cdgo_ntfccn       ,
					iss.nmro_acta                 , iss.cnsctvo_prgrmcn_prstcn            , iss.cnsctvo_prgrmcn_fcha_evnto,
					iss.rqre_prvdr                , iso.cnsctvo_cdgo_grpo_entrga		  , iss.cnsctvo_det_prgrmcn_fcha) As Source 
																							(   cnsctvo_slctd_atrzcn_srvco        , cnsctvo_cdgo_ofcna,
																								nuam                              , fcha_slctd             ,
																								cdgo_intrno                       , fcha_imprsn            ,
																								cnsctvo_cdgo_srvco_slctdo         , cntdd_slctda           ,
																								cnsctvo_cdgo_ltrldd               , fcha_ultma_mdfccn      ,
																								cnsctvo_cdgo_va_accso             , usro_ultma_mdfccn      ,
																								cnsctvo_cdgo_prsntcn              , prstcn_cptda           ,
																								cnsctvo_cdgo_dt_prgrmcn_fcha_evnto, cnsctvo_cdgo_tpo_srvco ,
																								cnsctvo_mdcmnto_slctdo            , vlr_rfrnca             ,
																								cpgo_lqudcn                       , cnsctvo_cdgo_estdo_mega,
																								cnsctvo_cdgo_estdo                , cnsctvo_cdgo_ntfccn    ,
																								nmro_acta                         , cnsctvo_prgrmcn_prstcn ,
																								cnsctvo_prgrmcn_fcha_evnto        , rqre_prvdr             ,
																								cnsctvo_cdgo_grpo_entrga		  , cnsctvo_det_prgrmcn_fcha
																							)
		On (Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna And 
			Target.nuam               = Source.nuam               And 
			Target.cnsctvo_prstcn     = Source.cnsctvo_cdgo_srvco_slctdo 
			)
		When Matched Then
		Update 
		Set cntdd_prstcn               = Source.cntdd_slctda,
			vlr_rfrnca                 = Source.vlr_rfrnca,
			cpgo_lqudcn                = Source.cpgo_lqudcn,
			ltrldd                     = Source.cnsctvo_cdgo_ltrldd,
			mrca_prspto_aprbdo         = 0,
			mrca_prspto_imprso         = 0,
			prcntje                    = 0,
			cnsctvo_cdgo_estdo         = Source.cnsctvo_cdgo_estdo,
			cnsctvo_cdgo_accn          = 0,
			pgo_prprcnl                = 0,
			fcha_ultma_mdfccn          = Source.fcha_ultma_mdfccn,
			cnsctvo_cdgo_va            = Source.cnsctvo_cdgo_va_accso,
			fcha_gstn                  = @fcha_actl,
			cntdd_prstcn_rl            = 0,
			vlr_rfrnca_rl              = 0,
			fcha_atncn                 = @fcha_actl,
			usro_ultma_mdfccn          = Source.usro_ultma_mdfccn,
			gnrco                      = Null,
			cnsctvo_cdgo_prsntcn       = Source.cnsctvo_cdgo_prsntcn,
			mdcmnto                    = @lcVacio,
			prstcn_cptda               = Source.prstcn_cptda,
			mrca_mdfccn                = @lcVacio,
			cnsctvo_prgrmcn_prstcn     = Source.cnsctvo_prgrmcn_prstcn,
			cnsctvo_prgrmcn_fcha_evnto = Source.cnsctvo_prgrmcn_fcha_evnto,
			cnsctvo_det_prgrmcn_fcha   = Source.cnsctvo_det_prgrmcn_fcha,   -- Source.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto, -- qvisionclr 2017-04-10
			rqre_prvdr                 = Source.rqre_prvdr,
			rqre_drccnmnto             = @ngcn,
			rqre_aprbcn                = @ngcn,
			cnsctvo_cdgo_drccnmnto     = Null,
			srvco_ngdo                 = 0,
			cnsctvo_csa_estdo          = 0,
			cnsctvo_mgrcn_3047         = Null,
			cdgo_intrno_prvdr_insmo    = @lcodigoInterno_Empty,
			tpo_lqdcn                  = Null
		When Not Matched Then
		Insert (cnsctvo_cdgo_ofcna      , nuam                  , cnsctvo_prstcn            ,
				cntdd_prstcn            , vlr_rfrnca            , cpgo_lqudcn               , 
				ltrldd                  , mrca_prspto_aprbdo    , mrca_prspto_imprso        ,
				prcntje                 , cnsctvo_cdgo_estdo    , cnsctvo_cdgo_accn         ,
				pgo_prprcnl             , fcha_ultma_mdfccn     , cnsctvo_cdgo_va           ,
				fcha_gstn               , cntdd_prstcn_rl       , vlr_rfrnca_rl             ,
				fcha_atncn              , usro_ultma_mdfccn     , gnrco                     ,
				cnsctvo_cdgo_prsntcn    , mdcmnto               , prstcn_cptda              , 
				mrca_mdfccn             , cnsctvo_prgrmcn_prstcn, cnsctvo_prgrmcn_fcha_evnto,
				cnsctvo_det_prgrmcn_fcha, rqre_prvdr            , rqre_drccnmnto            ,
				rqre_aprbcn             , cnsctvo_cdgo_drccnmnto, srvco_ngdo                ,
				cnsctvo_csa_estdo       , cnsctvo_mgrcn_3047    , cdgo_intrno_prvdr_insmo   ,
				tpo_lqdcn
				)
		Values (Source.cnsctvo_cdgo_ofcna                , --cnsctvo_cdgo_ofcna,
				Source.nuam                              , --nuam,
				Source.cnsctvo_cdgo_srvco_slctdo         , --cnsctvo_prstcn,
				Source.cntdd_slctda                      , --cntdd_prstcn,
				Source.vlr_rfrnca                        , --vlr_rfrnca,
				Source.cpgo_lqudcn                       , --cpgo_lqudcn,
				Source.cnsctvo_cdgo_ltrldd               , --ltrldd,
				0                                        , --mrca_prspto_aprbdo,
				0                                        , --mrca_prspto_imprso,
				0                                        , --prcntje,
				Source.cnsctvo_cdgo_estdo                , --cnsctvo_cdgo_estdo,
				0                                        , --cnsctvo_cdgo_accn,
				0                                        , --pgo_prprcnl,
				Source.fcha_ultma_mdfccn                 , --fcha_ultma_mdfccn,
				Source.cnsctvo_cdgo_va_accso             , --cnsctvo_cdgo_va,
				@fcha_actl                               , --fcha_gstn,
				0                                        , --cntdd_prstcn_rl,
				0                                        , --vlr_rfrnca_rl,
				@fcha_actl                               , --fcha_atncn,
				Source.usro_ultma_mdfccn                 , --usro_ultma_mdfccn,
				Null                                     , --gnrco,
				Source.cnsctvo_cdgo_prsntcn              , --cnsctvo_cdgo_prsntcn,
				@lcVacio                                 , --mdcmnto,
				Source.prstcn_cptda                      , -- prstcn_cptda,
				@lcVacio                                 , --mrca_mdfccn,
				Source.cnsctvo_prgrmcn_prstcn            , --cnsctvo_prgrmcn_prstcn,
				Source.cnsctvo_prgrmcn_fcha_evnto        , --cnsctvo_prgrmcn_fcha_evnto,
				Source.cnsctvo_det_prgrmcn_fcha, --cnsctvo_det_prgrmcn_fcha,
				Source.rqre_prvdr                        , --rqre_prvdr,
				@ngcn                                    , --rqre_drccnmnto,
				@ngcn                                    , --rqre_aprbcn,
				Null                                     , --cnsctvo_cdgo_drccnmnto,
				0                                        , --srvco_ngdo,
				0                                        , --cnsctvo_csa_estdo,
				Null                                     , --cnsctvo_mgrcn_3047,
				@lcodigoInterno_Empty         			 , --cdgo_intrno_prvdr_insmo,
				Null                                      --tpo_lqdcn,				
				);

		-- Se actualiza la fecha de ultima modificacion
		Update		  p
		Set			  fcha_ultma_mdfccn = iss.fcha_ultma_mdfccn
		From          #tmpinformacionsolicitud iso
		Inner Join    #tmpinformacionservicios iss
		On            iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And           iss.id_agrpdr = iso.id
		Inner Join    bdSisalud.dbo.tbprocedimientos p
		On			  iso.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			  iso.nuam = p.nuam
		And			  iss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn

		--Se actualiza la programacion de entrega en la tabla resultante
		Update		a
		Set			cnsctvo_ops            = iso.nuam              ,
					cnsctvo_cdgo_ofcna_ops = iso.cnsctvo_cdgo_ofcna
		From        #tmpinformacionsolicitud iso
		Inner Join  #tmpinformacionservicios iss
		On          iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And         iss.id_agrpdr = iso.id
		Inner Join  bdSisalud.dbo.tbDetProgramacionFechaEvento a With (NoLock)
		On          a.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = iss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto

		If(@es_btch = @vlr_si)
		Begin
			Alter Table bdSisalud.dbo.tbActua Disable Trigger TrIncidencias_NovIdentificaciontbActua
		End

		--Se almacena la información de tbActua
		Merge bdsisalud.dbo.tbactua With (RowLock) As Target
		Using (		Select
								iso.cnsctvo_slctd_atrzcn_srvco,			iso.cnsctvo_cdgo_ofcna,					iso.nuam,
								iso.fcha_slctd,							iso.cdgo_intrno,						iaf.nmro_unco_idntfccn_afldo,
								iaf.cnsctvo_cdgo_pln,					iaf.cnsctvo_cdgo_sde_ips_prmra,			iaf.cnsctvo_cdgo_tpo_idntfccn_afldo,
								iaf.nmro_idntfccn_afldo,				iaf.prmr_aplldo_afldo,					iaf.sgndo_aplldo_afldo,
								iaf.prmr_nmbre_afldo,					iaf.sgndo_nmbre_afldo,					iaf.cnsctvo_bnfcro_cntrto,
								iaf.nmro_cntrto,						iaf.cnsctvo_cdgo_tpo_cntrto,			iaf.cnsctvo_cdgo_sxo,
								iaf.cnsctvo_cdgo_tpo_undd,				iaf.edd_afldo_ans,						iaf.smns_ctzds,
								iaf.cnsctvo_cdgo_tpo_vnclcn_afldo,		iaf.cnsctvo_cdgo_rngo_slrl,				iaf.fcha_ultma_mdfccn,
								iaf.cdgo_ips_prmra,						iaf.fcha_ncmnto_afldo,					iaf.drccn_afldo,
								iaf.cnsctvo_cdgo_cdd_rsdnca_afldo,		iaf.tlfno_afldo,						iaf.tlfno_cllr_afldo,
								iaf.crro_elctrnco_afldo,				iaf.cnsctvo_cdgo_prntscs,				iaf.cnsctvo_cdgo_tpo_cbrnza,
								iaf.fcha_fn_vgnca,						iaf.fcha_inco_vgnca,					iaf.nmro_unco_idntfccn_aprtnt,
								iaf.rzn_scl
					From		#tmpinformacionsolicitud iso
					Inner Join	#tmpinformacionafiliado iaf
					On			iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
					Group By	iso.cnsctvo_slctd_atrzcn_srvco,			iso.cnsctvo_cdgo_ofcna,					iso.nuam,
								iso.fcha_slctd,							iso.cdgo_intrno,						iaf.nmro_unco_idntfccn_afldo,
								iaf.cnsctvo_cdgo_pln,					iaf.cnsctvo_cdgo_sde_ips_prmra,			iaf.cnsctvo_cdgo_tpo_idntfccn_afldo,
								iaf.nmro_idntfccn_afldo,				iaf.prmr_aplldo_afldo,					iaf.sgndo_aplldo_afldo,
								iaf.prmr_nmbre_afldo,					iaf.sgndo_nmbre_afldo,					iaf.cnsctvo_bnfcro_cntrto,
								iaf.nmro_cntrto,						iaf.cnsctvo_cdgo_tpo_cntrto,			iaf.cnsctvo_cdgo_sxo,
								iaf.cnsctvo_cdgo_tpo_undd,				iaf.edd_afldo_ans,						iaf.smns_ctzds,
								iaf.cnsctvo_cdgo_tpo_vnclcn_afldo,		iaf.cnsctvo_cdgo_rngo_slrl,				iaf.fcha_ultma_mdfccn,
								iaf.cdgo_ips_prmra,						iaf.fcha_ncmnto_afldo,					iaf.drccn_afldo,
								iaf.cnsctvo_cdgo_cdd_rsdnca_afldo,		iaf.tlfno_afldo,						iaf.tlfno_cllr_afldo,
								iaf.crro_elctrnco_afldo,				iaf.cnsctvo_cdgo_prntscs,				iaf.cnsctvo_cdgo_tpo_cbrnza,
								iaf.fcha_fn_vgnca,						iaf.fcha_inco_vgnca,					iaf.nmro_unco_idntfccn_aprtnt,
								iaf.rzn_scl
		) As Source (
								cnsctvo_slctd_atrzcn_srvco,				cnsctvo_cdgo_ofcna,						nuam,
								fcha_slctd,								cdgo_intrno,							nmro_unco_idntfccn_afldo,
								cnsctvo_cdgo_pln,						cnsctvo_cdgo_sde_ips_prmra,				cnsctvo_cdgo_tpo_idntfccn_afldo,
								nmro_idntfccn_afldo,					prmr_aplldo_afldo,						sgndo_aplldo_afldo,
								prmr_nmbre_afldo,						sgndo_nmbre_afldo,						cnsctvo_bnfcro_cntrto,
								nmro_cntrto,							cnsctvo_cdgo_tpo_cntrto,				cnsctvo_cdgo_sxo,
								cnsctvo_cdgo_tpo_undd,					edd_afldo_ans,							smns_ctzds,
								cnsctvo_cdgo_tpo_vnclcn_afldo,			cnsctvo_cdgo_rngo_slrl,					fcha_ultma_mdfccn,
								cdgo_ips_prmra,							fcha_ncmnto_afldo,						drccn_afldo,
								cnsctvo_cdgo_cdd_rsdnca_afldo,			tlfno_afldo,							tlfno_cllr_afldo,
								crro_elctrnco_afldo,					cnsctvo_cdgo_prntscs,					cnsctvo_cdgo_tpo_cbrnza,
								fcha_fn_vgnca,							fcha_inco_vgnca,						nmro_unco_idntfccn_aprtnt,
								rzn_scl
		)
		On (
			Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna		And
			Target.nuam = Source.nuam
		)
		When Matched Then
				Update
				Set cnsctvo_cdgo_sde = Source.cnsctvo_cdgo_sde_ips_prmra,
				cnsctvo_cdgo_pln = Source.cnsctvo_cdgo_pln,
				cnsctvo_cdgo_tpo_idntfccn = Source.cnsctvo_cdgo_tpo_idntfccn_afldo,
				nmro_idntfccn = Source.nmro_idntfccn_afldo,
				nmro_unco_idntfccn_afldo = Source.nmro_unco_idntfccn_afldo,
				prmr_aplldo = Source.prmr_aplldo_afldo,
				sgndo_aplldo = Source.sgndo_aplldo_afldo,
				prmr_nmbre = Source.prmr_nmbre_afldo,
				sgndo_nmbre = Source.sgndo_nmbre_afldo,
				nmro_bnfcro = Source.cnsctvo_bnfcro_cntrto,
				cnsctvo_cdgo_sxo = Source.cnsctvo_cdgo_sxo,
				edd = Source.edd_afldo_ans,
				cnsctvo_cdgo_tpo_undd = Source.cnsctvo_cdgo_tpo_undd,
				smns_ctzds = Source.smns_ctzds,
				cnsctvo_cdgo_tpo_afldo = Source.cnsctvo_cdgo_tpo_vnclcn_afldo,
				cnsctvo_cdgo_prntscs = Source.cnsctvo_cdgo_tpo_vnclcn_afldo,
				cnsctvo_cdgo_tpo_cbrnza = Source.cnsctvo_cdgo_tpo_cbrnza,
				cnsctvo_cdgo_rngo_slrl = Source.cnsctvo_cdgo_rngo_slrl,
				fcha_inco_vgnca = Source.fcha_inco_vgnca,
				fcha_fn_vgnca = Source.fcha_fn_vgnca,
				mrca = @lcVacio,
				cpgo_atncn = 0,
				cta = 0,
				cpgo_pgo = 0,
				dspnble_evnto = 0,
				dspnble_ano = 0,
				pgo_prprcnl = 0,
				espcl = 0,
				nmbre_trbjo = IsNull(Source.rzn_scl,@lcVacio),
				mra = @lcVacio,
				fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,
				cdgo_ips = Source.cdgo_ips_prmra,
				cnsctvo_cdgo_ofcna_vrfccn = Null,
				nmro_vrfccn = Null,
				iva = 0,
				fcha_ncmnto = Source.fcha_ncmnto_afldo,
				drccn_rsdnca = Source.drccn_afldo,
				cnsctvo_cdgo_cdd_rsdnca = Source.cnsctvo_cdgo_cdd_rsdnca_afldo,
				tlfno_rsdnca = Source.tlfno_afldo,
				cllr = Source.tlfno_cllr_afldo,
				eml = Source.crro_elctrnco_afldo,
				smns_aflcn = 0
		When Not Matched Then
				Insert (
				cnsctvo_cdgo_ofcna,
				nuam,
				cnsctvo_cdgo_sde,
				cnsctvo_cdgo_pln,
				cnsctvo_cdgo_tpo_idntfccn,
				nmro_idntfccn,
				nmro_unco_idntfccn_afldo,
				prmr_aplldo,
				sgndo_aplldo,
				prmr_nmbre,
				sgndo_nmbre,
				nmro_bnfcro,
				cnsctvo_cdgo_sxo,
				edd,
				cnsctvo_cdgo_tpo_undd,
				smns_ctzds,
				cnsctvo_cdgo_tpo_afldo,
				cnsctvo_cdgo_prntscs,
				cnsctvo_cdgo_tpo_cbrnza,
				cnsctvo_cdgo_rngo_slrl,
				fcha_inco_vgnca,
				fcha_fn_vgnca,
				mrca,
				cpgo_atncn,
				cta,
				cpgo_pgo,
				dspnble_evnto,
				dspnble_ano,
				pgo_prprcnl,
				espcl,
				nmbre_trbjo,
				mra,
				fcha_ultma_mdfccn,
				cdgo_ips,
				cnsctvo_cdgo_ofcna_vrfccn,
				nmro_vrfccn,
				iva,
				fcha_ncmnto,
				drccn_rsdnca,
				cnsctvo_cdgo_cdd_rsdnca,
				tlfno_rsdnca,
				cllr,
				eml,
				smns_aflcn)
				Values (Source.cnsctvo_cdgo_ofcna, --cnsctvo_cdgo_ofcna,
				Source.nuam, --nuam,
				Source.cnsctvo_cdgo_sde_ips_prmra, --cnsctvo_cdgo_sde,
				Source.cnsctvo_cdgo_pln, -- cnsctvo_cdgo_pln,
				Source.cnsctvo_cdgo_tpo_idntfccn_afldo, --cnsctvo_cdgo_tpo_idntfccn,
				Source.nmro_idntfccn_afldo, --nmro_idntfccn,
				Source.nmro_unco_idntfccn_afldo, --nmro_unco_idntfccn_afldo,
				Source.prmr_aplldo_afldo, --prmr_aplldo,
				Source.sgndo_aplldo_afldo, --sgndo_aplldo,
				Source.prmr_nmbre_afldo, --prmr_nmbre,
				Source.sgndo_nmbre_afldo, --sgndo_nmbre,
				Source.cnsctvo_bnfcro_cntrto, --nmro_bnfcro,
				Source.cnsctvo_cdgo_sxo, --cnsctvo_cdgo_sxo,
				Source.edd_afldo_ans, --edd,
				Source.cnsctvo_cdgo_tpo_undd, --cnsctvo_cdgo_tpo_undd,
				Source.smns_ctzds, --smns_ctzds,
				Source.cnsctvo_cdgo_tpo_vnclcn_afldo, --cnsctvo_cdgo_tpo_afldo,
				Source.cnsctvo_cdgo_tpo_vnclcn_afldo, --cnsctvo_cdgo_prntscs,
				Source.cnsctvo_cdgo_tpo_cbrnza, --cnsctvo_cdgo_tpo_cbrnza,
				Source.cnsctvo_cdgo_rngo_slrl, --cnsctvo_cdgo_rngo_slrl,
				Source.fcha_inco_vgnca, --fcha_inco_vgnca,
				Source.fcha_fn_vgnca, --fcha_fn_vgnca,
				'', --mrca,
				0, --cpgo_atncn,
				0, --cta,
				0, --cpgo_pgo,
				0, --dspnble_evnto,
				0, --dspnble_ano,
				0, --pgo_prprcnl,
				0, --espcl,
				IsNull(Source.rzn_scl,''), --nmbre_trbjo,
				@lcVacio, --mra,
				Source.fcha_ultma_mdfccn, --fcha_ultma_mdfccn,
				Source.cdgo_ips_prmra, --cdgo_ips,
				Null, --cnsctvo_cdgo_ofcna_vrfccn,
				Null, --nmro_vrfccn,
				0, --iva,
				Source.fcha_ncmnto_afldo, --fcha_ncmnto,
				Source.drccn_afldo, --drccn_rsdnca,
				Source.cnsctvo_cdgo_cdd_rsdnca_afldo, --cnsctvo_cdgo_cdd_rsdnca,
				Source.tlfno_afldo, --tlfno_rsdnca,
				Source.tlfno_cllr_afldo, --cllr,
				Source.crro_elctrnco_afldo, --eml,
				0 --smns_aflcn,
				);
	
		If(@es_btch = @vlr_si)
		Begin
			Alter Table bdSisalud.dbo.tbActua Enable Trigger TrIncidencias_NovIdentificaciontbActua
		End

		--Se almacena la información de tbConceptosOPS
		Merge bdsisalud.dbo.tbconceptosops With (RowLock) As Target
		Using (Select       iso.cnsctvo_slctd_atrzcn_srvco, iso.cnsctvo_cdgo_ofcna       , iso.nuam                    ,
							ics.cnsctvo_srvco_slctdo      , ics.cnsctvo_ops              , ics.cnsctvo_cdgo_cncpto_gsto,
							ics.fcha_ultma_mdfccn         , ics.cnsctvo_cdgo_grpo_imprsn , ics.nmro_rdccn_cm           ,
							ics.cnsctvo_cdgo_ofcna_cm     , ics.nmro_unco_ops            , ics.vlr_cncpto_cm           ,
							ics.gnrdo                     , ics.fcha_utlzcn_dsde         , ics.fcha_utlzcn_hsta        ,
							iss.fcha_imprsn               , iss.cnsctvo_cdgo_srvco_slctdo, iss.cntdd_slctda            ,
							iso.nmro_vsts                 , iso.ds_estnca                , ics.cdgo_intrno             ,
							ics.nmro_unco_idntfccn_prstdr , ics.vlr_lqdcn                , ics.cnsctvo_cdgo_estdo	   ,-- se cambia referencia de iso a ics qvisionclr 2017/01/18
							iso.fcha_ingrso_hsptlzcn	  , iso.fcha_egrso_hsptlzcn		 , iso.cnsctvo_cdgo_clse_hbtcn
				From       #tmpinformacionsolicitud iso
				Inner Join #tmpinformacionafiliado iaf
				On         iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
				Inner Join #tmpinformacionservicios iss
				On         iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco And 
						   iso.id                         = iss.id_agrpdr
				Inner Join #tmpinformacionconceptos ics
				On         ics.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
				Group By	iso.cnsctvo_slctd_atrzcn_srvco, iso.cnsctvo_cdgo_ofcna       , iso.nuam                    ,
							ics.cnsctvo_srvco_slctdo      , ics.cnsctvo_ops              , ics.cnsctvo_cdgo_cncpto_gsto,
							ics.fcha_ultma_mdfccn         , ics.cnsctvo_cdgo_grpo_imprsn , ics.nmro_rdccn_cm           ,
							ics.cnsctvo_cdgo_ofcna_cm     , ics.nmro_unco_ops            , ics.vlr_cncpto_cm           ,
							ics.gnrdo                     , ics.fcha_utlzcn_dsde         , ics.fcha_utlzcn_hsta        ,
							iss.fcha_imprsn               , iss.cnsctvo_cdgo_srvco_slctdo, iss.cntdd_slctda            ,
							iso.nmro_vsts                 , iso.ds_estnca                , ics.cdgo_intrno             ,
							ics.nmro_unco_idntfccn_prstdr , ics.vlr_lqdcn                , ics.cnsctvo_cdgo_estdo	   ,
							iso.fcha_ingrso_hsptlzcn	  , iso.fcha_egrso_hsptlzcn		 , iso.cnsctvo_cdgo_clse_hbtcn ) As Source (					cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_ofcna       ,
																						nuam                      , cnsctvo_srvco_slctdo     ,
																						cnsctvo_ops               , cnsctvo_cdgo_cncpto_gsto ,
																						fcha_ultma_mdfccn         , cnsctvo_cdgo_grpo_imprsn ,
																						nmro_rdccn_cm             , cnsctvo_cdgo_ofcna_cm    ,
																						nmro_unco_ops             , vlr_cncpto_cm            ,
																						gnrdo                     , fcha_utlzcn_dsde         ,
																						fcha_utlzcn_hsta          , fcha_imprsn              ,
																						cnsctvo_cdgo_srvco_slctdo , cntdd_slctda             , 
																						nmro_vsts                 , ds_estnca                ,
																						cdgo_intrno               , nmro_unco_idntfccn_prstdr,
																						vlr_lqdcn                 , cnsctvo_cdgo_estdo       ,
																						fcha_ingrso_hsptlzcn	  , fcha_egrso_hsptlzcn		 , 
																						cnsctvo_cdgo_clse_hbtcn
																						)
		On (Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna And 
			Target.nuam               = Source.nuam               And 
			Target.cnsctvo_ops        = Source.cnsctvo_ops
			)
		When Matched Then
		Update
		Set cntdd_prstcn                       = Source.cntdd_slctda,
			vlr_rfrnca                         = Source.vlr_lqdcn,
			prcntje_lqdcn                      = 0,
			vlr_orgnal                         = 0,
			cnsctvo_cdgo_cncpto_gsto           = Source.cnsctvo_cdgo_cncpto_gsto,
			nmro_unco_idntfccn_prstdr          = Source.nmro_unco_idntfccn_prstdr,
			cdgo_prstdr                        = Source.cdgo_intrno,
			cnsctvo_cdgo_frma_lqdcn_prstcn     = 2,
			cnsctvo_cdgo_frma_lqdcn_cncpto     = 2,
			cnsctvo_cdgo_ttlo_cnvno            = 0,
			cnsctvo_cdgo_estdo                 = Source.cnsctvo_cdgo_estdo,
			cnsctvo_cdgo_estdo_prcso           = @cnsctvo_cdgo_estdo_prcso,
			cnsctvo_cdgo_mdlo_trfa             = Null,
			da_estnca                          = Source.ds_estnca,
			cnsctvo_imprsn                     = @cnsctvo_imprsn,
			fcha_imprsn                        = Source.fcha_imprsn,
			nvo_cncpto                         = 0,
			fcha_ultma_mdfccn                  = Source.fcha_ultma_mdfccn,
			cnsctvo_cdgo_grpo_imprsn           = Source.cnsctvo_cdgo_grpo_imprsn,
			nro_visita                         = Source.nmro_vsts,
			fcha_ingrso                        = Source.fcha_ingrso_hsptlzcn,
			fcha_egrso                         = Source.fcha_egrso_hsptlzcn,
			cnsctvo_cdgo_prrqsto               = 0,
			trfa                               = Null,
			ds_dsde                            = 0,
			mrca_pqte                          = Null,
			cnsctvo_cdgo_clse_hbtcn            = Source.cnsctvo_cdgo_clse_hbtcn,
			cnsctvo_cnslta                     = 0,
			cnsctvo_cdgo_estdo_cnts            = @cnsctvo_cdgo_estdo_cnts,
			vlr_cnts                           = 0,
			fcha_actlzcn_cnts                  = @fcha_actl,
			usro_cnts                          = @usro,
			usro_ultma_mdfccn                  = @usro,
			cnsctvo_cdgo_ofcna_atrzcn          = Source.cnsctvo_cdgo_ofcna,
			cnsctvo_cdgo_frma_lqdcn_ops        = 2,
			cnsctvo_cdgo_det_mdlo_cnvno_prstcn = Null,
			cnsctvo_cdgo_det_mdlo_cnvno_cncpto = Null,
			orgn_lqdcn_cncpto                  = @orgn_lqdcn_cncpto,
			cnsctvo_cdgo_css_ops_fnls          = Null,
			usro_ultma_mdfccn_prmtrs           = Null,
			fcha_ultma_mdfccn_csa_fnl          = Null,
			nmro_rdccn_cm                      = Source.nmro_rdccn_cm,
			cnsctvo_cdgo_ofcna_cm              = Source.cnsctvo_cdgo_ofcna_cm,
			nmro_unco_ops                      = Source.nmro_unco_ops,
			cnsctvo_gnrco                      = Null,
			cnsctvo_cdgo_frma_envo             = Null,
			vlr_cncpto_cm                      = Source.vlr_cncpto_cm,
			hra_imprsn                         = Null,
			cnsctvo_mgrcn_3047                 = Null,
			cnsctvo_cdgo_dt_lsta_prco          = Null,
			cnsctvo_cdgo_cncpts_dt_lsta_prco   = Null,
			cnsctvo_pqte                       = Null,
			gnrdo                              = Source.gnrdo,
			fcha_utlzcn_dsde                   = Source.fcha_utlzcn_dsde,
			fcha_utlzcn_hsta                   = Source.fcha_utlzcn_hsta
		When Not Matched Then
		Insert (cnsctvo_cdgo_ofcna                , nuam                              , cnsctvo_ops                   ,
				cnsctvo_prstcn                    , cntdd_prstcn                      , vlr_rfrnca                    ,
				prcntje_lqdcn                     , vlr_orgnal                        , cnsctvo_cdgo_cncpto_gsto      ,
				nmro_unco_idntfccn_prstdr         , cdgo_prstdr                       , cnsctvo_cdgo_frma_lqdcn_prstcn,
				cnsctvo_cdgo_frma_lqdcn_cncpto    , cnsctvo_cdgo_ttlo_cnvno           , cnsctvo_cdgo_estdo            ,
				cnsctvo_cdgo_estdo_prcso          , cnsctvo_cdgo_mdlo_trfa            , da_estnca                     ,
				cnsctvo_imprsn                    , fcha_imprsn                       , nvo_cncpto                    ,
				fcha_ultma_mdfccn                 , cnsctvo_cdgo_grpo_imprsn          , nro_visita                    ,
				fcha_ingrso                       , fcha_egrso                        , cnsctvo_cdgo_prrqsto          ,
				trfa                              , ds_dsde                           , mrca_pqte                     ,
				cnsctvo_cdgo_clse_hbtcn           , cnsctvo_cnslta                    , cnsctvo_cdgo_estdo_cnts       ,
				vlr_cnts                          , fcha_actlzcn_cnts                 , usro_cnts                     ,
				usro_ultma_mdfccn                 , cnsctvo_cdgo_ofcna_atrzcn         , cnsctvo_cdgo_frma_lqdcn_ops   ,
				cnsctvo_cdgo_det_mdlo_cnvno_prstcn, cnsctvo_cdgo_det_mdlo_cnvno_cncpto, orgn_lqdcn_cncpto             ,
				cnsctvo_cdgo_css_ops_fnls         , usro_ultma_mdfccn_prmtrs          , fcha_ultma_mdfccn_csa_fnl     ,
				nmro_rdccn_cm                     , cnsctvo_cdgo_ofcna_cm             , nmro_unco_ops                 ,
				cnsctvo_gnrco                     , cnsctvo_cdgo_frma_envo            , vlr_cncpto_cm                 ,
				hra_imprsn                        , cnsctvo_mgrcn_3047                , cnsctvo_cdgo_dt_lsta_prco     ,
				cnsctvo_cdgo_cncpts_dt_lsta_prco  , cnsctvo_pqte                      , gnrdo                         ,
				fcha_utlzcn_dsde                  , fcha_utlzcn_hsta	
				)
		Values (Source.cnsctvo_cdgo_ofcna       , Source.nuam                    , Source.cnsctvo_ops             ,
				Source.cnsctvo_cdgo_srvco_slctdo, Source.cntdd_slctda            , Source.vlr_lqdcn               ,
				0                               , 0                              , Source.cnsctvo_cdgo_cncpto_gsto, 
				Source.nmro_unco_idntfccn_prstdr, Source.cdgo_intrno             , 2                              , 
				2                               , 0                              , Source.cnsctvo_cdgo_estdo      , 
				1                               , Null                           , Source.ds_estnca               ,
				1                               , Source.fcha_imprsn             , 0                              ,
				Source.fcha_ultma_mdfccn        , Source.cnsctvo_cdgo_grpo_imprsn, Source.nmro_vsts               ,
				Source.fcha_ingrso_hsptlzcn     , Source.fcha_egrso_hsptlzcn     , 0                              ,
				Null                            , 0                              , Null                           ,
				Source.cnsctvo_cdgo_clse_hbtcn  , 0                              , @cnsctvo_cdgo_estdo_cnts       ,
				0                               , @fcha_actl                     , @usro                          ,
				@usro                           , Source.cnsctvo_cdgo_ofcna      , 2                              ,
				Null                            , Null                           , @orgn_lqdcn_cncpto             , 
				Null                            , Null                           , Null                           , 
				Source.nmro_rdccn_cm            , Source.cnsctvo_cdgo_ofcna_cm   , Source.nmro_unco_ops           ,
				Null                            , Null                           , Source.vlr_cncpto_cm           , 
				Null                            , Null                           , Null                           ,
				Null                            , Null                           , Source.gnrdo                   ,
				Source.fcha_utlzcn_dsde         , Source.fcha_utlzcn_hsta
				);    

				
				
				Merge BdSisalud.dbo.tbASDatosAdicionalesMegaSipres With(RowLock) As t
				Using(
						Select		iso.cnsctvo_cdgo_ofcna ,						iso.nuam,							iss.cnsctvo_cdgo_srvco_slctdo,
									iss.fcha_imprsn,								iso.cnsctvo_cdgo_grpo_entrga, 		@cnsctvo_cdgo_mdlo_cra cnsctvo_cdgo_mdlo_cra,	
									iso.cnsctvo_slctd_atrzcn_srvco --tbActua
						From		#tmpinformacionsolicitud iso -- tbActual
						Inner Join	#tmpinformacionafiliado iaf
						On			iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
						Inner Join  #tmpinformacionservicios iss
						On          iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
						And         iss.id_agrpdr = iso.id
						Group By	iso.cnsctvo_cdgo_ofcna ,						iso.nuam,							iss.cnsctvo_cdgo_srvco_slctdo,
									iss.fcha_imprsn,								iso.cnsctvo_cdgo_grpo_entrga, 		iso.cnsctvo_slctd_atrzcn_srvco
				) As s
				On (t.cnsctvo_cdgo_ofcna = s.cnsctvo_cdgo_ofcna					And
					t.nuam = s.nuam												And	
					t.cnsctvo_prstcn = s.cnsctvo_cdgo_srvco_slctdo)
				When Not Matched Then
					Insert 
					(
							cnsctvo_cdgo_ofcna,				nuam,						cnsctvo_prstcn,
							fcha_estmda_entrga_fnl,			cnsctvo_cdgo_grpo_entrga,	cnsctvo_cdgo_mdlo_cra,
							cnsctvo_slctd_mga,				fcha_crcn,					usro_crcn,
							fcha_ultma_mdfccn,				usro_ultma_mdfccn
					)
					Values(
							s.cnsctvo_cdgo_ofcna,			s.nuam,						s.cnsctvo_cdgo_srvco_slctdo,
							s.fcha_imprsn,					s.cnsctvo_cdgo_grpo_entrga,	s.cnsctvo_cdgo_mdlo_cra,
							s.cnsctvo_slctd_atrzcn_srvco,	@fcha_actl,					@usro,
							@fcha_actl,						@usro
					)
				When Matched Then
					Update
					Set fcha_estmda_entrga_fnl = s.fcha_imprsn,			
						cnsctvo_cdgo_grpo_entrga = s.cnsctvo_cdgo_grpo_entrga,	
						cnsctvo_cdgo_mdlo_cra = s.cnsctvo_cdgo_mdlo_cra,
						fcha_ultma_mdfccn = @fcha_actl,				
						usro_ultma_mdfccn = @usro;
				
		-- 
		Insert Into	#serviciosAutorizados_Impresos(cnsctvo_srvco_slctdo, nuam, cnsctvo_cdgo_ofcna)
		Select		iss.cnsctvo_srvco_slctdo, iso.nuam, iso.cnsctvo_cdgo_ofcna
		From		#tmpinformacionsolicitud iso
		Inner Join	#tmpinformacionservicios iss
		On			iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		Inner Join	BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ess With (NoLock)
		On			ess.cnsctvo_cdgo_estdo_srvco_slctdo = iss.cnsctvo_cdgo_estdo_mega
		Inner Join	#tmpinformacionconceptos ic
		On			ic.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo
		Where		ess.cnsctvo_cdgo_estdo_hmlgdo_sprs IN (@estdo_sprs_aprvdo, @estdo_sprs_imprso)
		And			@fcha_actl BETWEEN ess.inco_vgnca And fn_vgnca;

		/*Crea historia de cambios en el estado de la atencion de la OPS*/
		Update #serviciosAutorizados_Impresos Set usro = @usro

		--
		EXEC BDCna.dbo.spPMCrearHistoricoEstadosAtencionOPSMasivo

		--Se almacena la información de tbconceptosanulados
		Merge bdSisalud.dbo.tbconceptosanulados With (RowLock) As Target
		Using (Select	    iso.cnsctvo_cdgo_ofcna,
						iso.nuam              ,
						iso.usro_crcn         ,
						ics.cnsctvo_ops       ,
						ics.cnsctvo_cdgo_estdo
				From		#tmpinformacionsolicitud iso
				Inner Join	#tmpinformacionservicios iss
				On			iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco And
						iso.id = iss.id_agrpdr
				Inner Join	#tmpinformacionconceptos ics
				On			ics.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo) As Source (cnsctvo_cdgo_ofcna ,
																						nuam               ,
																						usro_crcn          ,
																						cnsctvo_ops        ,
																						cnsctvo_cdgo_estdo
																						)
		On	(Target.cnsctvo_cdgo_ofcna  = Source.cnsctvo_cdgo_ofcna And 
				Target.nuam                = Source.nuam               And	
				Target.cnsctvo_ops         = Source.cnsctvo_ops        And
				Target.cnsctvo_cdgo_estnca = Source.cnsctvo_cdgo_estdo 
			)
		When Matched Then
			Update
			Set		cnsctvo_cdgo_ofcna  = Source.cnsctvo_cdgo_ofcna,
					nuam                = Source.nuam,
					cnsctvo_ops         = Source.cnsctvo_ops,
					usro_anla           = Source.usro_crcn,
					fcha_anla           = @fcha_actl,
					cnsctvo_cdgo_csa    = @cnsctvo_cdgo_csa,
					cnsctvo_cdgo_prfl   = @cnsctvo_cdgo_prfl,
					cnsctvo_cdgo_estnca = Source.cnsctvo_cdgo_estdo,
					fcha_ultma_mdfccn   = @fcha_actl
		When Not Matched Then
			Insert (cnsctvo_cdgo_ofcna, nuam               , cnsctvo_ops     ,
					usro_anla         , fcha_anla          , cnsctvo_cdgo_csa,
					cnsctvo_cdgo_prfl , cnsctvo_cdgo_estnca, fcha_ultma_mdfccn
					)
			Values (Source.cnsctvo_cdgo_ofcna, Source.nuam              , Source.cnsctvo_ops, 
					Source.usro_crcn         , @fcha_actl               , @cnsctvo_cdgo_csa ,
					@cnsctvo_cdgo_prfl       , Source.cnsctvo_cdgo_estdo,  @fcha_actl
				);  


				Insert Into #tmpDatosAdicionalesMegaSipres
				(			
							cnsctvo_slctd_mga             
				)
				Select Distinct
							iso.cnsctvo_slctd_atrzcn_srvco
				From		#tmpinformacionsolicitud iso
			  	Inner Join  bdsisalud.dbo.tbASDatosAdicionalesMegaSipres a With(NoLock)
				On          a.cnsctvo_slctd_mga = iso.cnsctvo_slctd_atrzcn_srvco;
 
		--Se almacena la información de tbPrestacionesAutorizadasPreSolicitud
		Set @cnsctvo_prstcn_atrzda_prslctd = (Select IsNull(Max(cnsctvo_prstcn_atrzda_prslctd), 0) + 1 
											From bdcna.dbo.tbPrestacionesAutorizadasPreSolicitud );
		
		-- 
		Insert Into bdcna.dbo.tbPrestacionesAutorizadasPreSolicitud  (cnsctvo_prstcn_atrzda_prslctd,
																			cnsctvo_prslctd,
																			cnsctvo_cdgo_ofcna,
																			nuam,
																			cnsctvo_cdfccn,
																			fcha_crcn,
																			usro_crcn,
																			fcha_ultma_mdfccn,
																			usro_ultma_mdfccn)  
				Select		Row_Number() Over(Order BY ps.cnsctvo_prslctd Desc) + 
												@cnsctvo_prstcn_atrzda_prslctd,
							ps.cnsctvo_prslctd, 
							iso.cnsctvo_cdgo_ofcna,
							iso.nuam,
							iss.cnsctvo_cdgo_srvco_slctdo,
							iso.fcha_crcn,
							iso.usro_crcn,
							iso.fcha_ultma_mdfccn,
							iso.usro_ultma_mdfccn
				From			#tmpinformacionsolicitud iso
				Inner Join	#tmpinformacionservicios iss
				On			iso.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
				And			iso.id = iss.id_agrpdr
				Inner Join	#tmpinformacionafiliado iaf
				On			iso.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
				Inner Join  gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
				On	        sas.cnsctvo_slctd_atrzcn_srvco = iaf.cnsctvo_slctd_atrzcn_srvco
				Inner Join  BDCna.gsa.tbASDatosAdicionalesPreSolicitud dap With(NoLock)
				On          dap.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco
				Inner Join	BDCna.dbo.tbPreSolicitudes ps With(NoLock)
				On			ps.cnsctvo_prslctd = dap.cnsctvo_prslctd
				Inner Join  #tmpDatosAdicionalesMegaSipres a
				On          a.cnsctvo_slctd_mga = sas.cnsctvo_slctd_atrzcn_srvco
				Left Join	bdcna.dbo.tbPrestacionesAutorizadasPreSolicitud PAP With(NoLock)
				On			PAP.cnsctvo_prslctd = ps.cnsctvo_prslctd
				And			PAP.cnsctvo_cdfccn = iss.cnsctvo_cdgo_srvco_slctdo				
				Where		iso.cnsctvo_cdgo_mdo_cntcto = @cnsctvo_cdgo_mdo_cntcto_sprs
				And         PAP.cnsctvo_prslctd IS Null;
 
		
		

		-- 
		Drop Table #serviciosAutorizados_Impresos
		Drop Table #TmpTbMarcasXAtencionOPS
		Drop Table #tmpDatosAdicionalesMegaSipres

END

