USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarTemporalAfiliadoSolicitudxCreacionEmpresa]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASPoblarTemporalAfiliadoSolicitudxCreacionEmpresa											
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Sp Poblar Temporales para ejecución Masiva ó por demanda de la Creación de Empresa del Afiliado x solicitud	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P	#TemptbAsInformacionEmpresasxAfiliado = temporal que contiene los datos del afiliado x solicitud P\>   													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/05/16																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASPoblarTemporalAfiliadoSolicitudxCreacionEmpresa]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null

AS

Begin
	SET NOCOUNT ON
	 
	Insert		#TemptbAsInformacionEmpresasxAfiliado
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,				cnsctvo_cdgo_tpo_idntfccn_afldo,
				nmro_idntfccn_afldo,					nmro_unco_idntfccn_afldo,								cnsctvo_cdgo_tpo_cntrto,
				nmro_cntrto,							cnsctvo_bnfcro_cntrto		
	)
	select
				sa.cnsctvo_slctd_atrzcn_srvco,			ia.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,			ia.cnsctvo_cdgo_tpo_idntfccn_afldo,
				ia.nmro_idntfccn_afldo,					ia.nmro_unco_idntfccn_afldo,							ia.cnsctvo_cdgo_tpo_cntrto,
				ia.nmro_cntrto,							ia.cnsctvo_bnfcro_cntrto		
	from		gsa.tbASSolicitudesAutorizacionServicios sa with(nolock)
	Inner Join  gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ia with(nolock)
    On			ia.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
	Left Join	gsa.tbAsInformacionEmpresasxAfiliado iea with(nolock)
	On			iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = ia.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	Where		iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco is  null	
	And			(@cnsctvo_slctd_atrzcn_srvco is null or sa.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)

End

GO
