USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPVencConsultaTipoPoblacionAfiliado]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spCNAPVencConsultaTipoPoblacionAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el tipo de población al que pertenece el afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASPVencConsultaTipoPoblacionAfiliado] 
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn udtNumeroIdentificacionLargo,
	@Nui_Afldo	   UdtConsecutivo = null,
	@cnsctvo_cdgo_pln udtConsecutivo,
	@tipo_poblacion varchar(40) output

AS
BEGIN
Set Nocount On

	Declare	@fcha_vldccn				datetime,
			@fecha_nacimiento			datetime,
			@edad_afiliado				int,
					
			
			@getDate_					datetime = getDate(),
			@incapProlongada			int = 12,
			@seguimiento				int = 92,
			@tramite					int = 93,
			@confirmado					int = 94,
			@reglaMenorEdad				int = 144,
			@reglaAdultomayor			int = 145,
			
			@menorEdad					int,
			@adultomayor				int ,

			@nvl_dscpcdd1				int = 1,
			@nvl_dscpcdd3				int = 3,
			@grupo_poblacional			int = 9,
			@grupo_poblacionalFP		int = 23,
			@ms_incapacidadProlongada	varchar(20) = 'COTIZANTE CON INCAPACIDAD CONTINUA PROLONGADA',
			@ms_niño					varchar(10) = 'NIÑO',
			@ms_adultoMayor				varchar(20) = 'ADULTO MAYOR',
			@ms_discapacidad			varchar(20) = 'SITUACION DISCAPACIDAD',
			@ms_desplazado				varchar(50) = 'DESPLAZADO POR LA VIOLENCIA CERTIFICADO',
			@ms_normal					varchar(10) = 'NORMAL',
			@ms_veteradosFP				varchar(30) = 'VETERANOS DE FUERZA PUBLICA';

	Set		@fcha_vldccn = Convert(char(10),getDate(),111)
	Set		@edad_afiliado = 0

	Select	@fecha_nacimiento = b.fcha_ncmnto
			From		BDAfiliacionValidador.dbo.tbBeneficiariosValidador			b	With(NoLock)
			INNER JOIN    bdAfiliacionValidador.dbo.tbContratosValidador                c    With(NoLock)    
																						On b.nmro_cntrto               = c.nmro_cntrto
																						and b.cnsctvo_cdgo_tpo_cntrto  = c.cnsctvo_cdgo_tpo_cntrto
			Where		b.cnsctvo_cdgo_tpo_idntfccn		= @cnsctvo_cdgo_tpo_idntfccn
			And			b.nmro_idntfccn					= @nmro_idntfccn
			And			c.cnsctvo_cdgo_pln				= @cnsctvo_cdgo_pln
			and			@getDate_ between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro

		if @Nui_Afldo is null
		begin
			Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn)
		end		

		Set @edad_afiliado = isnull(BDAfiliacionValidador.dbo.fnCalcularTiempo(@fecha_nacimiento, @fcha_vldccn,1,2),0)


		--COTIZANTE CON INCAPACIDAD CONTINUA PROLONGADA

		If Exists (Select 'X' existe 
				   From  bdSisalud.dbo.tbafiliadosmarcados aa With(NoLock)
				   Where  aa.cnsctvo_cdgo_clsfccn_evnto = @incapProlongada --12 incapacidad continua prolongada
				   and nmro_unco_idntfccn=@Nui_Afldo
				   And   aa.cnsctvo_cdgo_estdo_ntfccn in (@seguimiento, @tramite, @confirmado) --(92,93,94) EN SEGUIMIENTO, TRAMITE DE PENSIÓN, CONFIRMADO
				  )
		begin
			set @tipo_poblacion = @ms_incapacidadProlongada;
		end

		--NIÑO 
		if @tipo_poblacion is null
		BEGIN
			Set @menoredad = ( SELECT vlr_rgla  FROM [bdSisalud].[dbo].[tbReglas_Vigencias] 
							   WHERE cnsctvo_cdgo_rgla = @reglaMenorEdad AND @getDate_ BETWEEN inco_vgnca AND fn_vgnca)
			IF @edad_afiliado <= @menoredad
			BEGIN
				set @tipo_poblacion = @ms_niño
			END
		END

		--ADULTO MAYOR
		if @tipo_poblacion is null 
		begin
			Set @adultomayor = (SELECT vlr_rgla  FROM [bdSisalud].[dbo].[tbReglas_Vigencias] 
						        WHERE cnsctvo_cdgo_rgla = @reglaAdultomayor AND @getDate_ BETWEEN inco_vgnca AND fn_vgnca)
			
			IF  @edad_afiliado >= @adultomayor
			BEGIN
				set @tipo_poblacion = @ms_adultoMayor
			END
		end
   
	   --VETERANOS DE FUERZA PUBLICA
	   if @tipo_poblacion is null
		begin
			if exists (Select 'x' desplazado From BDAfiliacionValidador.dbo.tbGruposPoblacionesAfiliados with(nolock)
								  Where nmro_unco_idntfccn_afldo =  @Nui_Afldo
								  and cnsctvo_cdgo_grpo_pblcnl = @grupo_poblacionalFP
								  And  fn_vgnca > @getDate_)
			begin
				Set @tipo_poblacion = @ms_veteradosFP
			end
		end


	   -- SITUACION DISCAPACIDAD
	   if @tipo_poblacion is null
	   begin

			If exists (Select 'x' existe From BDAfiliacionValidador.dbo.tbAfiliadosValidador b With(NoLock)
						where	nmro_unco_idntfccn_afldo = @Nui_Afldo
						and cnsctvo_cdgo_nvl_dscpcdd between @nvl_dscpcdd1 and @nvl_dscpcdd3)
			begin
				set @tipo_poblacion = @ms_discapacidad
			end
		end

	   --DESPLAZADO POR LA VIOLENCIA CERTIFICADO
	   if @tipo_poblacion is null
		begin
			if exists (Select 'x' desplazado From BDAfiliacionValidador.dbo.tbGruposPoblacionesAfiliados with(nolock)
								  Where nmro_unco_idntfccn_afldo =  @Nui_Afldo
								  and cnsctvo_cdgo_grpo_pblcnl = @grupo_poblacional
								  And  fn_vgnca > @getDate_)
			begin
				Set @tipo_poblacion = @ms_desplazado
			end

		end

		if @tipo_poblacion is null
		begin
			Set @tipo_poblacion = @ms_normal
		end
END
GO
