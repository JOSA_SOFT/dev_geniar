USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRecuperarDatosEmpresaAfiliadoxSolicitud]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASRecuperarDatosEmpresaAfiliadoxSolicitud											
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Sp Se recuperan los datos de la empresa del afiliado x solicitud 	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P	#TemptbAsInformacionEmpresasxAfiliado = temporal que contiene los datos del afiliado x solicitud P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/05/16																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASRecuperarDatosEmpresaAfiliadoxSolicitud]


AS

Begin
	SET NOCOUNT ON

	Declare @fechaactual datetime = getdate() 

	Update		t
	Set			nmro_unco_idntfccn_aprtnte = tcv.nmro_unco_idntfccn_aprtnte,
				cnsctvo_cdgo_tpo_cbrnza	 = tcv.cnsctvo_cdgo_tpo_cbrnza,
				cnsctvo_cbrnza = tcv.cnsctvo_cbrnza,
				inco_vgnca_cbrnza = tcv.inco_vgnca_cbrnza,
				fn_vgnca_cbrnza = tcv.fn_vgnca_cbrnza,
				cnsctvo_cdgo_clse_aprtnte = tcv.cnsctvo_cdgo_clse_aprtnte,
				cnsctvo_scrsl = tcv.cnsctvo_scrsl_ctznte,
				cnsctvo_cdgo_prdcto = tcv.cnsctvo_prdcto_scrsl,
				slro_bse = tcv.ingrso_bse
	From		#TemptbAsInformacionEmpresasxAfiliado t
	Inner Join	bdafiliacionValidador.dbo.tbCobranzasValidador tcv with(nolock)
	On			tcv.cnsctvo_cdgo_tpo_cntrto = t.cnsctvo_cdgo_tpo_cntrto
	And			tcv.nmro_cntrto = t.nmro_cntrto
	And         @fechaactual between tcv.inco_vgnca_cbrnza and tcv.fn_vgnca_cbrnza

	
	Update		t
	Set			cnsctvo_cdgo_tpo_idntfccn_empldr = av.cnsctvo_cdgo_tpo_idntfccn_empldr,
				nmro_idntfccn_empldr = av.nmro_idntfccn_empldr,
				nmbre_empldr = av.rzn_scl
	From		#TemptbAsInformacionEmpresasxAfiliado t
	Inner Join  bdafiliacionValidador.dbo.tbAportanteValidador av with(nolock)
	On			av.nmro_unco_idntfccn_aprtnte = t.nmro_unco_idntfccn_aprtnte

	Update		t
	Set			cnsctvo_cdgo_entdd_arp = sa.cnsctvo_cdgo_arp
	From		#TemptbAsInformacionEmpresasxAfiliado t
	Inner Join	bdafiliacionValidador.dbo.tbSucursalesAportante sa with(nolock)
	On			sa.cnsctvo_scrsl = t.cnsctvo_scrsl
	And			sa.nmro_unco_idntfccn_empldr = t.nmro_unco_idntfccn_aprtnte
	And			sa.cnsctvo_cdgo_clse_aprtnte = t.cnsctvo_cdgo_clse_aprtnte		

End
GO
