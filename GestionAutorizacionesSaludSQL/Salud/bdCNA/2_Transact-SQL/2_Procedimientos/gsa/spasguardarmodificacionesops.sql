USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spasguardarmodificacionesops]    Script Date: 19/07/2017 01:56:29 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarModificacionesOPS 
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D 
                            Procedimiento que permite almacenar la actualización realizada a la OPS	  
						D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D
                            Se realiza modificacion en el procedimiento para que realice la actualizacion del 
							numero de OPS a 0 y permita anular las cuotas de recuperacion
                        D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 12/05/2017  FM\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez  AM\>    
* Descripcion        : <\D
                            Se cambia nombre de parametro de entrada de @nmro_unco_ops a @nmro_ops, por conflicto
							con el nombre de parametro de procedimiento de anulacion de cuotas.
                        D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 20/06/2017  FM\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D
                            Se modifica el procedimiento para que toma el servicio a liquidar y no el concepto
                        D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 05/07/2017  FM\>    
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D
                            Se agrega llamado a procedimiento para eliminar los conceptos de gasto.
                        D\>
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 19/07/2017  FM\>    
*------------------------------------------------------------------------------------------------------------------------------*/

/*
BEGIN
	DECLARE @cdgo_rsltdo INT, @mnsje_rsltdo VARCHAR(2000)
	EXEC bdCNA.gsa.spASGuardarModificacionesOPS 135096, 88430521, '00264','setijol',1,'Cambio por que si', @cdgo_rsltdo OUTPUT,@mnsje_rsltdo OUTPUT
	SELECT @cdgo_rsltdo, @mnsje_rsltdo
END
*/
ALTER PROCEDURE [gsa].[spasguardarmodificacionesops] 
	@id_slctd              udtConsecutivo       ,
	@nmro_ops			   udtConsecutivo       , -- nmro_unco_ops
	@cdgo_intrno           udtCodigoIps         ,
	@usro                  udtusuario           ,
	@id_csa                udtConsecutivo       ,
	@obsrvcns              udtdescripcion       ,
	@cdgo_rsltdo           udtconsecutivo Output,
	@mnsje_rsltdo          VARCHAR(2000)  Output
AS
BEGIN
   SET NOCOUNT ON 
   
   DECLARE @indx           Int           ,
            @ttl           Int           ,
            @xml_in        Xml           ,
            @parameter_sp  NVarchar(Max) ,
            @codigo_ok     Int           ,
            @codigo_error  Int           ,          
            @mnsje         Varchar(1000) ,
            @cdgo_error    Varchar(100)  ,
            @ms_exto       Varchar(50)   ,
            @mnsje_lqdcn   Varchar(2000) ,
			@mnsje_anular  Varchar(2000) ,
            @mnsje_prstcns Varchar(100)  ,
            @mnsjae_exto   Varchar(100)  ,
			@mnsjertrno	   udtDescripcion,
            @cdgortrno	   Varchar(3)    ,
			@DatoNull      udtconsecutivo,
			@cdgo_err      Varchar(2)    , 
			@separator     udtLogico     ,
			@valorCero     Int           ,
			@valorVacio    udtLogico     ,
			@cnsctvo_estdo udtconsecutivo		 

    Create 
	Table  #tmpprestacionesops(id                   udtconsecutivo Identity(1, 1),
                               cnsctvo_srvco_slctdo udtconsecutivo
                              )  

	Create 
	Table #tmpSolicitudAnulacionCuotasRecuperacion(cnsctvo_slctd_atrzcn_srvco    udtconsecutivo, -- número de la solicitud
												   cnsctvo_cdgo_srvco_slctdo     udtconsecutivo, -- consecutivo de la prestación o servicio
												   nmro_unco_ops			     udtconsecutivo,
												   cnsctvo_srvco_slctdo          udtconsecutivo,
												   cnsctvo_mdcmnto_slctdo        udtconsecutivo,
												   cnsctvo_prcdmnto_insmo_slctdo udtconsecutivo
												  )
    Set @indx          = 1
	Set @valorCero     = 0
    Set @codigo_ok     = 0
    Set @codigo_error  = -1
    Set @ms_exto       = 'OK'
    Set @mnsje_lqdcn   = 'LIQUIDACION'
	Set @mnsje_anular  = 'ANULACION CUOTA'
    Set @mnsje_prstcns = 'No se encontró información de las prestaciones para el número de OPS y solicitud enviado.'
    Set @mnsjae_exto   = 'Proceso ejecutado satisfactoriamente.'
	Set @DatoNull      = NULL
	Set @cdgo_err      = 'ET'
	Set @separator     = '-'
	Set @cnsctvo_estdo = 9
	Set @valorVacio    = ''

	BEGIN TRY
      --Se consultan las prestaciones asociadas a la OPS.
	  --Inserción de información de los servicios
      Insert 
	  Into       #tmpSolicitudAnulacionCuotasRecuperacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo     ,
	                                                      nmro_unco_ops             , cnsctvo_cdgo_srvco_slctdo,
														  cnsctvo_mdcmnto_slctdo
	                                                     )
      Select     sso.cnsctvo_slctd_atrzcn_srvco, sso.cnsctvo_srvco_slctdo     ,
	             ISNULL(css.nmro_unco_ops, 0)  , sso.cnsctvo_cdgo_srvco_slctdo,
				 mss.cnsctvo_mdcmnto_slctdo
      From       bdcna.gsa.tbasconceptosserviciosolicitado css WITH (NOLOCK)
      Inner Join bdcna.gsa.tbasmedicamentossolicitados mss WITH (NOLOCK)
      On         mss.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
      Inner Join bdcna.gsa.tbasserviciossolicitados sso WITH (NOLOCK)
      On         sso.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
	  Where      sso.cnsctvo_slctd_atrzcn_srvco = @id_slctd
      And        css.nmro_unco_ops              = @nmro_ops
      
	  Insert 
	  Into       #tmpSolicitudAnulacionCuotasRecuperacion(cnsctvo_slctd_atrzcn_srvco   , cnsctvo_srvco_slctdo     ,
	                                                      nmro_unco_ops                , cnsctvo_cdgo_srvco_slctdo,
														  cnsctvo_prcdmnto_insmo_slctdo
	                                                     )
      Select     sso.cnsctvo_slctd_atrzcn_srvco   , sso.cnsctvo_srvco_slctdo     ,
	             ISNULL(css.nmro_unco_ops, 0)     , sso.cnsctvo_cdgo_srvco_slctdo,
				 pis.cnsctvo_prcdmnto_insmo_slctdo
      From       bdcna.gsa.tbasconceptosserviciosolicitado css WITH (NOLOCK)
      Inner Join bdcna.gsa.tbasprocedimientosinsumossolicitados pis WITH (NOLOCK)
      On         pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
      Inner Join bdcna.gsa.tbasserviciossolicitados sso WITH (NOLOCK)
      On         sso.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
      Where      sso.cnsctvo_slctd_atrzcn_srvco = @id_slctd
      And        css.nmro_unco_ops              = @nmro_ops
	  
	  Insert 
	  Into   #tmpprestacionesops(cnsctvo_srvco_slctdo)
	  Select cnsctvo_srvco_slctdo
	  From   #tmpSolicitudAnulacionCuotasRecuperacion 

      --Se realiza la iteración de la información por prestación, para cambiar el direccionamiento.
      Select @ttl = Count(1) From #tmpprestacionesops

      If @ttl > 0
		Begin
			While @indx <= @ttl
			Begin
				-- Se construye el XML de entrada
				Set @xml_in = (Select @cdgo_intrno             codigoInterno        ,
										gestionDomiciliariaDireccionamiento.cnsctvo_srvco_slctdo consecutivoPrestacion,
										@usro                    usuarioGestion       ,
										@obsrvcns                justificacion        ,
			              				(Select Top 1 @id_csa
										From   #tmpprestacionesops codigoMotivo
										FOR XML AUTO, TYPE, ELEMENTS
										)motivos			
								From #tmpprestacionesops gestionDomiciliariaDireccionamiento
								Where gestionDomiciliariaDireccionamiento.id = @indx
								FOR XML AUTO, TYPE, ELEMENTS)

				Set @parameter_sp = CONVERT(NVARCHAR(MAX), @xml_in)
			   
				--Se guarda la información del direccionamiento.
				Exec bdCNA.gsa.spasguardarinformaciondireccionamientodomi @parameter_sp       ,
																			@cdgo_rsltdo  Output,
																			@mnsje_rsltdo Output
				If @cdgo_rsltdo = @codigo_error
				Begin
					RAISERROR (@mnsje_rsltdo, 16, 2) WITH SETERROR
				End

				Set @indx = @indx + 1
			End
		
			--Se realiza la anulacion de las cuotas de recuperacion
			Exec bdcna.gsa.spASEjecutarAnulacionCuotasRecuperacion @DatoNull, @mnsjertrno Output, @cdgortrno Output, 
																@DatoNull, @DatoNull         , @DatoNull

			If @cdgo_err = @cdgortrno
			Begin
				Set @mnsje_anular = @mnsje_anular + @separator + @mnsjertrno
				RAISERROR (@mnsje_anular, 16, 2) WITH SETERROR
			End
		
			-- Se actualizan el servicio al estado liquidado para que se puedan reliquidar
			Update     a
			Set 	   cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_estdo	
			From       bdcna.gsa.tbASServiciosSolicitados a With(NoLock)   
			Inner Join #tmpSolicitudAnulacionCuotasRecuperacion b
			On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	
			-- Se actualizan los conceptos a estado liquidado y el valor de la ops en 0 para que se puedan reliquidar
			Update     a
			Set        nmro_unco_ops                          = @valorCero    ,
				       cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_estdo,
				       cdgo_intrno_prstdr_atrzdo              = @valorVacio	   
			From       bdcna.gsa.tbasconceptosserviciosolicitado a
			Inner Join #tmpSolicitudAnulacionCuotasRecuperacion  b
			On         b.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
			Where      b.cnsctvo_mdcmnto_slctdo Is Not Null


			-- Se actualizan los conceptos a estado liquidado y el valor de la ops en 0 para que se puedan reliquidar
			Update     a
			Set        nmro_unco_ops                          = @valorCero    ,
				       cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_estdo,
				       cdgo_intrno_prstdr_atrzdo              = @valorVacio	   
			From       bdcna.gsa.tbasconceptosserviciosolicitado a
			Inner Join #tmpSolicitudAnulacionCuotasRecuperacion  b
			On         b.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
			Where      b.cnsctvo_prcdmnto_insmo_slctdo Is Not Null

			-- Se aliminan los conceptos de gasto.
			Exec bdCNA.gsa.spAsEliminarConceptosGastoCambioDireccionamiento

			--Se ejecuta la re-liquidación
			--Se construye el XML de entrada
		
			Set @xml_in = (Select Top 1 @usro usro,
									(SELECT TOP 1 @id_slctd
										FROM #tmpprestacionesops cnsctvo_slctd
										FOR XML AUTO, TYPE, ELEMENTS) idnt_slctds
						From #tmpprestacionesops cdgs_slctds
						FOR XML AUTO, TYPE, ELEMENTS
						)

			Set @parameter_sp = CONVERT(NVARCHAR(MAX), @xml_in)

			--Se ejecuta el proceso de liquidación
			Exec bdCNA.gsa.spasejecutarprocesoliquidacion @parameter_sp     ,
														@mnsje      Output,
														@cdgo_error Output

			--Se valida que la respuesta sea exitosa
			If @cdgo_error <> @ms_exto
			Begin
				Set @mnsje_lqdcn = @mnsje_lqdcn + @separator + @mnsje
				RAISERROR (@mnsje_lqdcn, 16, 2) WITH SETERROR
			End
		End
		Else
		Begin
			Set @cdgo_rsltdo  = @codigo_error
			Set @mnsje_rsltdo = @mnsje_prstcns
		End

      SET @cdgo_rsltdo = @codigo_ok
      SET @mnsje_rsltdo = @mnsjae_exto     
  END TRY
  BEGIN CATCH
    SET @mnsje_rsltdo = 'Number:'    + CAST(ERROR_NUMBER() AS CHAR) + CHAR(13) +
						'Line:'      + CAST(ERROR_LINE() AS CHAR) + CHAR(13) +
						'Message:'   + ERROR_MESSAGE() + CHAR(13) +
						'Procedure:' + ERROR_PROCEDURE();
    SET @cdgo_rsltdo = @codigo_error

  END CATCH
END
