USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionAntescedentesPrestacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosAntescedentesPrestacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D 													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionAntescedentesPrestacion]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@cnsctvo_srvco_slctdo       udtConsecutivo
AS
BEGIN
	Set Nocount On
	Declare @fecha_historico			datetime,
			@ldfcha_consulta			datetime,
			@reglaFecha_inicio			int = 154,
			@acotar_antiguedad			int,
			@cnsctvo_cdgo_tpo_idntfccn 	udtConsecutivo, 
			@nmro_idntfccn				udtNumeroIdentificacionLargo;

	Set @ldfcha_consulta = getDate()
	Set @acotar_antiguedad = ( SELECT vlr_rgla  FROM bdSisalud.dbo.tbReglas_Vigencias WITH (NOLOCK)
						       WHERE cnsctvo_cdgo_rgla = @reglaFecha_inicio AND @ldfcha_consulta BETWEEN inco_vgnca AND fn_vgnca)

	Set @fecha_historico = (SELECT DATEADD(mm, -@acotar_antiguedad, @ldfcha_consulta)) 


	SELECT  @cnsctvo_cdgo_tpo_idntfccn = cnsctvo_cdgo_tpo_idntfccn_afldo, 
			@nmro_idntfccn		= nmro_idntfccn_afldo 
	FROM gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK)
	WHERE ias.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;


	Select 
		SAS.fcha_slctd,
		ESS.dscrpcn_estdo_srvco_slctdo ,
		DP.nmbre_scrsl,
		SS.cntdd_slctda,
		TCF.dscrpcn_tpo_cdfccn,
		CF.dscrpcn_cdfccn,
		SS.indcdr_no_ps
	From BDCna.gsa.tbASSolicitudesAutorizacionServicios SAS With(NoLock)
	Inner Join	 BDCna.gsa.tbASServiciosSolicitados SS With(NoLock) on SAS.cnsctvo_slctd_atrzcn_srvco =  SS.cnsctvo_slctd_atrzcn_srvco
	Inner Join	 BDCna.prm.tbASEstadosServiciosSolicitados ESS With(NoLock) on ESS.cnsctvo_cdgo_estdo_srvco_slctdo = SS.cnsctvo_cdgo_estdo_srvco_slctdo
	Inner Join	 BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios IPS With(NoLock) on IPS.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
	INNER Join   BDSisalud.dbo.tbDireccionesPrestador DP With(NoLock) ON DP.cdgo_intrno = IPS.cdgo_intrno
	INNER Join   BDSisalud.dbo.tbDireccionesPrestador_Vigencias DPV With(NoLock) ON DP.cdgo_intrno = DPV.cdgo_intrno
	INNER Join   BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK) on ias.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
	INNER JOIN bdSisalud.dbo.tbCodificaciones CF WITH (NOLOCK) on CF.cnsctvo_cdfccn = SS.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on cf.cnsctvo_cdgo_tpo_cdfccn = tcf.cnsctvo_cdgo_tpo_cdfccn
	where SS.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	And	  ias.cnsctvo_cdgo_tpo_idntfccn_afldo   = @cnsctvo_cdgo_tpo_idntfccn
	And	  ias.nmro_idntfccn_afldo				= @nmro_idntfccn
	And   @ldfcha_consulta between tcf.inco_vgnca and tcf.fn_vgnca
	And	  @ldfcha_consulta between dpv.inco_vgnca and dpv.fn_vgnca
	And   SAS.fcha_slctd > @fecha_historico;
END

GO
