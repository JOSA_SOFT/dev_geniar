USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarDireccionamientoPrestadorConProgramacionEntrega]    Script Date: 05/04/2017 19:07:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------
* Método o PRG		:		gsa.spASGrabarDireccionamientoPrestadorConProgramacionEntrega							
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez García										A\>	
* Descripción		: <\D	Graba el direccionamiento de los prestadores generados a partir
							del proceso automático de creación de solicitudes con programación de entrega	D\>
* Observaciones		: <\O 																		O\>	
* Parámetros		: <\P 	#Idsolicitudes = temporal contiene los consecutivos de las solicitudes creadas
							#Tempo_IPS = temporal contiene los prestadores de las solicitudes creadas por programación entrega D\>
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/03/31														FC\>
*-------------------------------------------------------------------------------------
* Modificado Por	: <\AM  Ing. Juan Carlos Vasquez G.AM\>      
* Descripción		: <\DM	Se modifica para corregir error de prestador direccionado y no el solicitante DM\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2017-04-05 FM\>
*-------------------------------------------------------------------------------------*/

--  EXEC bdcna.gsa.spASGrabarDireccionamientoPrestadorConProgramacionEntrega

ALTER PROCEDURE  [gsa].[spASGrabarDireccionamientoPrestadorConProgramacionEntrega]

/*01*/ @usro_prcso	UdtUsuario = null

AS 

Begin

	SET NOCOUNT ON
	-- Declaramos variables del proceso
	Declare	@fechaactual datetime = getdate(),
			@lcUsuario	 UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))
	if @usro_prcso is not null
		Begin
		Set @lcUsuario = @usro_prcso
		End

    -- Insertamos el resultado del direccionamiento para la(s) solicitud(es) creada(s) ó asociada
	Insert gsa.tbASResultadoDireccionamiento
	(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,			cdgo_intrno,
				fcha_crcn,							usro_crcn,						fcha_ultma_mdfccn,
				usro_ultma_mdfccn
	)
 	Select
				SOL.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,		dp.cdgo_intrno,
				@fechaactual,						@lcUsuario,						@fechaactual,
				@lcUsuario
	From		#Idsolicitudes			IDS WITH (NOLOCK)
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios	SOL with(nolock)
	On			IDS.cnsctvo_slctd_atrzcn_srvco = SOL.cnsctvo_slctd_atrzcn_srvco
	Inner Join  gsa.tbASServiciosSolicitados ss with(nolock)
	On			sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#Tempo_Solicitudes t
	On          t.id = ids.idxml
	Inner Join	#Tempo_IPS p
	On			p.id = t.id
	Inner Join	#tbDetProgramacion dp
	On			dp.id_tbla = ids.idxml

	-- Insertamos el resultado de la fecha de entrega para la(s) solicitud(es) creada(s) ó asociada
	Insert gsa.tbASResultadoFechaEntrega
	(
				cnsctvo_slctd_atrzcn_srvco,					cnsctvo_srvco_slctdo,			cnsctvo_cdgo_grpo_entrga,
				fcha_estmda_entrga,							fcha_rl_entrga,					prrdd_admnstrtva,
				fcha_crcn,									usro_crcn,						fcha_ultma_mdfccn,
				usro_ultma_mdfccn		
	)
	Select
				SOL.cnsctvo_slctd_atrzcn_srvco,				ss.cnsctvo_srvco_slctdo,		1,
				dp.fcha_entrga,								dp.fcha_rl_entrga,				'N',
				@fechaactual,								@lcUsuario,						@fechaactual,
				@lcUsuario
	From		#Idsolicitudes			IDS WITH (NOLOCK)
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios	SOL with(nolock)
	On			IDS.cnsctvo_slctd_atrzcn_srvco = SOL.cnsctvo_slctd_atrzcn_srvco
	Inner Join  gsa.tbASServiciosSolicitados ss with(nolock)
	On			sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#Tempo_Solicitudes t
	On          t.id = ids.idxml
	Inner Join	#tbDetProgramacion dp
	On			dp.id_tbla = t.id

	-- Actualizamos la edad del afiliado
	Update      ias
	Set			edd_afldo_ans = ta.edd_afldo_ans,
				edd_afldo_mss = ta.edd_afldo_mss,
				edd_afldo_ds = ta.edd_afldo_ds
	From		#Idsolicitudes			IDS WITH (NOLOCK)
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios	SOL with(nolock)
	On			IDS.cnsctvo_slctd_atrzcn_srvco = SOL.cnsctvo_slctd_atrzcn_srvco
	Inner Join	tbASInformacionAfiliadoSolicitudAutorizacionServicios ias 
	On			sol.cnsctvo_slctd_atrzcn_srvco = ias.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#Tempo_Solicitudes t
	On          t.id = ids.idxml
	Inner Join  #Tempo_Afiliados ta
	On			ta.id = t.id
	Inner Join	#tbDetProgramacion dp
	On			dp.id_tbla = t.id

	
End