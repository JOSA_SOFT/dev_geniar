USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaPrestacionesSolicitudWeb]    Script Date: 9/29/2017 3:25:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaPrestacionesSolicitudWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos	A\>    
* Descripcion        : <\D Procedimiento que consulta la informacion de las prestaciones asociadas a la solicitud  D\>					           
* Observaciones   	 : <\O O\>    
* Parametros         : <\P P\>   
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 25/03/2016  FC\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Rodriguez De León  AM\>    
* Descripcion        : <\D  Se adicionan campos al select de salida para que contenga 2 nuevos parametros
							que indican si la prestación fue devuelta por las causales PAF-POC-Capita y
							si se ha hecho alguna gestion de parte del usuario  D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-14 FM\>
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Giovanny González  AM\>    
* Descripcion        : <\D  Se adiciona campo al select de salida para mostrar la fecha de Entrega de la prestación  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-02-10 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM  
                            Se modifica procedimiento para que retorne hblta_dts_atrzcn en 1 cuando se tenga 
							causales de devolucion
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-02-10 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez  AM\>    
* Descripcion        : <\DM  
                            Se agrega recuperacion de la informacion del recobro por prestacion,
							control de cambio 068
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-05-19 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez  AM\>    
* Descripcion        : <\DM  
                            Se se agrega el retorno del consecutivo del estado de las prestaciones, ajustes control de cambio 068				
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-06-07 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez  AM\>    
* Descripcion        : <\DM  
                            Se agrega recuperacion de las causalesd e no cobro por prestacion.				
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-07-18 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM  
                            Se realiza modificacion al procedimiento para que los dias de gestion se calculen tomando la fecha de
							creacion de la solicitud y no la fecha de solicitud medica
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-08-14 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM  
                            Se realiza modificacion al procedimiento para que la marca de gestion de autorizacion se
							habilite cuando el servicio este devuelto
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-08-31 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\DM  
                           Se agrega recuperacion de la descripcion de las causales de no cobro
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017-10-04 FM\>     
*----------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaPrestacionesSolicitudWeb] 807

ALTER PROCEDURE [gsa].[spASConsultaPrestacionesSolicitudWeb]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
Begin
	SET NOCOUNT ON;

	Declare @lcValorNo	            udtCodigo     ,
			@lcValorSi	            udtCodigo     ,
			@lcValorS               udtLogico     ,
			@ldFechaActual	        Datetime      ,
			@lnCums                 udtconsecutivo,
			@lnCups                 udtconsecutivo,
			@lnPis                  udtconsecutivo,
			@lcAux                  udtLogico     ,
			@lcAuxX                 udtLogico     ,
			@vlr_uno		        Int			  ,
			@orgn_mdfccn			udtDescripcion,
			@cdgo_prmtro_gnrl		Char(3)       ,
			@vsble_usro				udtLogico     ,
			@cdgo_prmtro_gnrl_vgnca	Char(3)       ,
			@tpo_dto_prmtro			udtLogico     ,
			@vlr_prmtro_nmrco		Numeric(18,0) ,
			@vlr_prmtro_crctr       Varchar(200)  ,
			@vlr_prmtro_fcha		Datetime      ,
			@fcha_rdccn_slctd	    Datetime      ,
			@nmro_ds_slctd		    Int           ,
			@fechaDia			    Datetime      ,
			@cnsctvo_cdgo_mdlo		udtConsecutivo,
			@lnEstdo_srvco_slctdo   udtConsecutivo,
			@vlr_cro				Int;

	Create 
	Table #tmpPrestacionesSolicitadas(cnsctvo_srvco_slctdo					udtconsecutivo,
	                                  cdgo_cdfccn							Char(11)      ,	
									  dscrpcn_srvco_slctdo					udtDescripcion, --dscrpcn_cdfccn	
									  cnsctvo_cdgo_tpo_srvco				udtconsecutivo,	
									  dscrpcn_tpo_cdfccn					udtDescripcion,							  								  
									  indcdr_no_ps							udtCodigo     ,
									  cnsctvo_cdgo_srvco_slctdo				udtconsecutivo, --cnsctvo_cdfccn
									  cnsctvo_cdgo_ltrldd					udtconsecutivo,
									  dscrpcn_ltrldd						udtDescripcion  Default '',
									  cnsctvo_cdgo_va_accso					udtconsecutivo,
									  dscrpcn_va_accso						udtDescripcion  Default '',
									  cntdd_slctda							int           ,
									  dss									float           Default 0 ,	
									  cnsctvo_cdgo_prsntcn_dss				udtconsecutivo, --Posologia - Dosis							 
									  dscrpcn_dss							udtDescripcion  Default '',
									  prdcdd_dss							float           Default 0 ,
									  cncntrcn_dss							Char(20)      ,
									  prsntcn_dss							Char(20)      ,
									  cdgo_prsntcn							udtCodigo       Default '',
									  dscrpcn_prsntcn						udtDescripcion  Default '',
									  cdgo_undd_cncntrcn_dss				Char(10)        Default '',
									  dscrpcn_cncntrcn						udtDescripcion  Default '',
									  cnsctvo_slctd_atrzcn_srvco			udtconsecutivo,
									  dscrpcn_prsntcn_mdcmnto				udtDescripcion  Default '',
									  dscrpcn_cncntrcn_mdcmnto				udtDescripcion  Default '',
									  dscrpcn_dss_mdcmnto					udtDescripcion  Default '',
									  hblta_dts_atrzcn						char(1)		 Default '0', --Si la prestación es devuelta indica si se puede autorizar
									  hblta_dts_atrzcn_gstn					char(1)		 Default '0',  --Indica si la prestación que fue devuelta ya se gesionó
									  fcha_estmda_entrga					Datetime,
									  cnsctvo_cdgo_rcbro					udtConsecutivo,
									  cdgo_rcbro							Char(3),
									  dscrpcn_rcbro							udtDescripcion,
									  cnsctvo_cdgo_estado_servco_slctdo		udtConsecutivo,
									  cnsctvo_cdgo_csa_no_cbro_cta_rcprcn	udtConsecutivo Default 0,
									  dscrpcn_csa_no_cbro_cta_rcprcn		udtDescripcion Default ''
									 ) 

	Set @lcValorNo				= 'NO'
	Set @lcValorSi				= 'SI'
	Set @lcValorS				= 'S'
	Set @ldFechaActual			= getDate()
	Set @lnCums					= 5
	Set @lnCups					= 4
	Set @lnPis					= 9
	Set @lcAux					= ' '
	Set @lcAuxX					= 'x'
	Set @vlr_uno				= 1
	Set @orgn_mdfccn			= 'tbASAutorizaNoConformidadValidacion.cnsctvo_cdgo_estdo_srvco_slctdo'
	Set	@cdgo_prmtro_gnrl		= '109'
	Set	@vsble_usro				= 'S'
	Set	@cdgo_prmtro_gnrl_vgnca	= '109'
	Set	@tpo_dto_prmtro			= 'N'
	Set @fechaDia				= Convert(char(10),getDate(),111);
	Set @cnsctvo_cdgo_mdlo		= 31 
	Set @cdgo_prmtro_gnrl       = '109'
	Set @cdgo_prmtro_gnrl_vgnca = '109'
	Set @vsble_usro             = 'S'
	Set @tpo_dto_prmtro         = 'N'
	Set @lnEstdo_srvco_slctdo   = 6	
	Set	@vlr_cro				= 0;

	Insert 
	Into       #tmpPrestacionesSolicitadas(cnsctvo_srvco_slctdo  , cdgo_cdfccn						 , dscrpcn_srvco_slctdo               , 
	                                       cnsctvo_cdgo_tpo_srvco, cntdd_slctda					     , cnsctvo_slctd_atrzcn_srvco         ,
										   indcdr_no_ps          , cnsctvo_cdgo_srvco_slctdo		 , cnsctvo_cdgo_rcbro		          ,
										   cdgo_rcbro			 ,	cnsctvo_cdgo_estado_servco_slctdo, cnsctvo_cdgo_csa_no_cbro_cta_rcprcn					
									      )   
	Select     a.cnsctvo_srvco_slctdo     , c.cdgo_srvco_slctdo, a.dscrpcn_srvco_slctdo     ,
	           a.cnsctvo_cdgo_tpo_srvco   , a.cntdd_slctda     , @cnsctvo_slctd_atrzcn_srvco,
			   Case When a.indcdr_no_ps = @lcValorS THEN @lcValorSi Else @lcValorNo End indcdr_no_ps,
			   a.cnsctvo_cdgo_srvco_slctdo,	a.cnsctvo_cdgo_rcbro, c.cdgo_rcbro,
			   a.cnsctvo_cdgo_estdo_srvco_slctdo, isNull(a.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn, @vlr_cro)
	From       bdCNA.gsa.tbASServiciosSolicitados a WITH(NOLOCK)
	Inner Join bdCNA.gsa.tbASServiciosSolicitadosOriginal c WITH(NOLOCK)
	On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH(NOLOCK)
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

	Update     #tmpPrestacionesSolicitadas
	Set        dscrpcn_tpo_cdfccn  =    b.dscrpcn_tpo_cdfccn  
	From       #tmpPrestacionesSolicitadas a
	Inner Join bdSiSalud.dbo.tbTipoCodificacion_Vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_tpo_cdfccn = a.cnsctvo_cdgo_tpo_srvco
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	Update     #tmpPrestacionesSolicitadas
	Set        fcha_estmda_entrga = b.fcha_estmda_entrga
	From       #tmpPrestacionesSolicitadas a
	Inner Join bdCNA.gsa.tbASResultadoFechaEntrega b WITH (NOLOCK)	
	On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo		

	Update		ps
	Set			dscrpcn_rcbro = b.dscrpcn_clsfccn_evnto	
	From		#tmpPrestacionesSolicitadas ps
	Inner Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios dsas With(NoLock)
	On			dsas.cnsctvo_slctd_atrzcn_srvco = ps.cnsctvo_slctd_atrzcn_srvco	 
	Inner Join	bdSisalud.dbo.tbMNClasificacionxTipoEvento_vigencias a With(NoLock)
	On			a.cnsctvo_cdgo_cntngnca = dsas.cnsctvo_cdgo_cntngnca
	Inner Join  bdSisalud.dbo.tbClasificacionEventosNotificacion_vigencias b With(NoLock)
	On			a.cnsctvo_cdgo_clsfccn_evnto=b.cnsctvo_cdgo_clsfccn_evnto  
	And			b.cnsctvo_cdgo_clsfccn_evnto = ps.cnsctvo_cdgo_rcbro
	Inner Join	bdSisalud.dbo.TbClasificacionEventoNotificacionxModulo c With(NoLock)
	On			b.cnsctvo_cdgo_clsfccn_evnto = c.cnsctvo_cdgo_clsfccn_evnto  
	Where		@ldFechaActual  between b.inco_vgnca And b.fn_vgnca
	And			@ldFechaActual  between a.inco_vgnca And a.fn_vgnca 
	And			b.Vsble_Usro = @lcValorS  	
	And			c.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo
			
	IF EXISTS (SELECT TOP 1 1 
	           From   #tmpPrestacionesSolicitadas
			   Where  cnsctvo_cdgo_tpo_srvco = @lnCums
			  )
	   Begin
	      Update     #tmpPrestacionesSolicitadas
		  Set        dss                      = b.dss                     ,
		             cnsctvo_cdgo_prsntcn_dss = b.cnsctvo_cdgo_prsntcn_dss,
		             prdcdd_dss               = b.prdcdd_dss              , 
					 cncntrcn_dss             = b.cncntrcn_dss            , 					 
					 prsntcn_dss              = b.prsntcn_dss             ,
					 cdgo_prsntcn             = c.cdgo_prsntcn            ,
					 cdgo_undd_cncntrcn_dss   = c.cdgo_undd_cncntrcn_dss     
		  From       #tmpPrestacionesSolicitadas a
		  Inner Join bdCNA.gsa.tbASMedicamentosSolicitados b WITH(NOLOCK)
		  On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		  Inner Join bdCNA.gsa.tbASMedicamentosSolicitadosOriginal c WITH(NOLOCK)
		  On         c.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo

		  Update     #tmpPrestacionesSolicitadas
		  Set        dscrpcn_prsntcn         = b.dscrpcn_prsntcn,
		             dscrpcn_prsntcn_mdcmnto = CONCAT(b.dscrpcn_prsntcn, @lcAux, @lcAuxX, @lcAux, RTRIM(a.prsntcn_dss))
		  From       #tmpPrestacionesSolicitadas a
		  Inner Join bdSISalud.dbo.tbPresentaciones_Vigencias b WITH(NOLOCK)
		  On         b.cdgo_prsntcn = a.cdgo_prsntcn
		  Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

		  Update     #tmpPrestacionesSolicitadas
		  Set        dscrpcn_cncntrcn         = b.dscrpcn_cncntrcn,
		             dscrpcn_cncntrcn_mdcmnto = CONCAT(RTRIM(a.cncntrcn_dss), @lcAux, b.dscrpcn_cncntrcn)
		  From       #tmpPrestacionesSolicitadas a
		  Inner Join bdSISalud.dbo.tbConcentracion_Vigencias b WITH(NOLOCK)
		  On         b.cdgo_cncntrcn = a.cdgo_undd_cncntrcn_dss
		  Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

		  Update     #tmpPrestacionesSolicitadas
		  Set        dscrpcn_dss         = b.dscrpcn_dss,
		             dscrpcn_dss_mdcmnto = CONCAT(RTRIM(a.dss), @lcAux, b.dscrpcn_dss)
		  From       #tmpPrestacionesSolicitadas a
		  Inner Join bdSISalud.dbo.tbMNDosis_Vigencias b WITH(NOLOCK)
		  On         b.cnsctvo_cdgo_dss = a.cnsctvo_cdgo_prsntcn_dss
		  Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	
	   End	   

	IF EXISTS (SELECT TOP 1 1 
	           From   #tmpPrestacionesSolicitadas
			   Where  cnsctvo_cdgo_tpo_srvco = @lnCups
			  )
	   Begin
	        Update     #tmpPrestacionesSolicitadas
			Set        cnsctvo_cdgo_ltrldd   = b.cnsctvo_cdgo_ltrldd,
			           cnsctvo_cdgo_va_accso = b.cnsctvo_cdgo_va_accso
			From       #tmpPrestacionesSolicitadas a
			Inner Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados b WITH(NOLOCK)
			On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			Update     #tmpPrestacionesSolicitadas
			Set        dscrpcn_ltrldd = b.dscrpcn_ltrldd
			From       #tmpPrestacionesSolicitadas a
			Inner Join bdSiSalud.dbo.tbLateralidades_Vigencias b WITH(NOLOCK)
			On         b.cnsctvo_cdgo_ltrldd = a.cnsctvo_cdgo_ltrldd
			Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
			           

			Update     #tmpPrestacionesSolicitadas
			Set        dscrpcn_va_accso = b.dscrpcn_va_accso
			From       #tmpPrestacionesSolicitadas a
			Inner Join bdSiSalud.dbo.tbViasAccesoQx_Vigencias b WITH(NOLOCK)
			On         b.cnsctvo_cdgo_va_accso = a.cnsctvo_cdgo_va_accso
			Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca
	   End 


	SELECT @fcha_rdccn_slctd =	fcha_crcn
	FROM   BDCNA.GSA.tbASSolicitudesAutorizacionServicios WITH(NOLOCK)
	WHERE  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;

	Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl, 
											@vsble_usro, 
											@cdgo_prmtro_gnrl_vgnca, 
											@vsble_usro, 
											@tpo_dto_prmtro, 
											@vlr_prmtro_nmrco Output, 
											@vlr_prmtro_crctr Output, 
											@vlr_prmtro_fcha Output;

	Set @nmro_ds_slctd = (SELECT DATEDIFF(day, @fcha_rdccn_slctd, @fechaDia))
		
	If @vlr_prmtro_nmrco is not null And @vlr_prmtro_nmrco > @nmro_ds_slctd
		Begin
			--Se actualiza flag que indica si la prestación esta devuelta 
			Update     pa 
			Set        hblta_dts_atrzcn = @vlr_uno
			From       #tmpPrestacionesSolicitadas pa 
			Inner Join bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion rpm With(Nolock)
			On         rpm.llve_prmra_cncpto_prncpl_vlddo = pa.cnsctvo_srvco_slctdo       And
				       rpm.llve_prmra_rgstro_vlddo		  = pa.cnsctvo_slctd_atrzcn_srvco	
		    Where      pa.cnsctvo_cdgo_estdo_srvco_slctdo = @lnEstdo_srvco_slctdo			

			--Se actualiza Flag que determina si una prestación devuelta ya fue gestionada.
			UPDATE     pa 
			SET        hblta_dts_atrzcn_gstn = @vlr_uno
			FROM       #tmpPrestacionesSolicitadas pa
			Inner Join BDCna.gsa.tbASTrazaModificacion tm With(Nolock)
			On         pa.cnsctvo_slctd_atrzcn_srvco  = tm.cnsctvo_slctd_atrzcn_srvco And 
				       pa.cnsctvo_srvco_slctdo		  = tm.cnsctvo_srvco_slctdo       					 
			Where      pa.hblta_dts_atrzcn = @vlr_uno	
			And        tm.orgn_mdfccn	   = @orgn_mdfccn
		End

	-- Se recuperan las causas de no cobro de cuota calculadas, si la prestacion no esta marcada
	Update		pa
	Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = dcc.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
	From		#tmpPrestacionesSolicitadas pa
	Inner Join	bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion dcc With(NoLock)
	On			dcc.cnsctvo_slctd_atrzcn_srvco = pa.cnsctvo_slctd_atrzcn_srvco
	And			dcc.cnsctvo_srvco_slctdo = pa.cnsctvo_srvco_slctdo
	Where		pa.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @vlr_cro;

	Update		#tmpPrestacionesSolicitadas
	Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @vlr_cro
	Where		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn Is Null

	-- Se recupera 
	Update		pa
	Set			dscrpcn_csa_no_cbro_cta_rcprcn = dcc.dscrpcn_csa_no_cbro_cta_rcprcn
	From		#tmpPrestacionesSolicitadas pa
	Inner Join	bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias dcc With(NoLock)
	On			dcc.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = pa.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
	Where		pa.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn > @vlr_cro;


	Select dscrpcn_tpo_cdfccn       , cdgo_cdfccn            , dscrpcn_srvco_slctdo     , cntdd_slctda       ,  
	       indcdr_no_ps             , dscrpcn_ltrldd         , dscrpcn_va_accso         , dscrpcn_dss_mdcmnto,
		   dscrpcn_cncntrcn_mdcmnto , dscrpcn_prsntcn_mdcmnto, cnsctvo_cdgo_srvco_slctdo As cnsctvo_cdfccn,
		   cnsctvo_srvco_slctdo		, hblta_dts_atrzcn		 , hblta_dts_atrzcn_gstn	, fcha_estmda_entrga,
		   cnsctvo_cdgo_rcbro		, cdgo_rcbro			 , dscrpcn_rcbro			, cnsctvo_cdgo_estado_servco_slctdo,
		   cnsctvo_cdgo_csa_no_cbro_cta_rcprcn				 , dscrpcn_csa_no_cbro_cta_rcprcn
	From   #tmpPrestacionesSolicitadas


	Drop Table #tmpPrestacionesSolicitadas
End
