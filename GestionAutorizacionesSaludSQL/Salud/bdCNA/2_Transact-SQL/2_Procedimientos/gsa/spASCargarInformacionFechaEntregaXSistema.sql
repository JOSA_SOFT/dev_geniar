USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCargarInformacionFechaEntregaXSistema]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCargarInformacionFechaEntregaXSistema
* Desarrollado por   : <\A A\>    
* Descripcion        : <\D Procedimiento que permite cargar la información necesaria para el procedimiento D\>
					   <\D de fecha esperada de entrega cuando es utilizado por SIPRESD\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 10/03/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASCargarInformacionFechaEntregaXSistema] @cnsctvo_prcso udtConsecutivo,
@usro udtUsuario,
@es_btch char(1),
@sistemaOrigen char
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @fechaActual DATETIME
  SET @fechaActual = GETDATE()

  INSERT INTO #tmpDatosSolicitudFechaEntrega (cnsctvo_slctd_srvco_sld_rcbda,
  cnsctvo_srvco_slctdo,
  cnsctvo_cdfccn,
  cdgo_cdfccn,
  cnsctvo_cdgo_tpo_srvco,
  cnsctvo_pln,
  cdgo_pln,
  cnsctvo_tpo_pln,
  cdgo_tpo_pln,
  cnsctvo_mdo_cntcto,
  cdgo_mdo_cntcto,
  fcha_slctd,
  nmro_unco_idntfccn_afldo,
  edd_afldo_ans,
  edd_afldo_mss,
  edd_afldo_ds,
  nmro_cntrto,
  cnsctvo_cdgo_tpo_cntrto,
  usro,
  nmro_slctd_ss,
  nmro_slctd_prvdr,
  cnsctvo_cdgo_tpo_idntfccn_afldo,
  nmro_idntfccn)
    SELECT
      NULL as cnsctvo_slctd_atrzcn_srvco,
      NULL as cnsctvo_srvco_slctdo,
      NULL as cnsctvo_cdgo_srvco_slctdo,
      NULL as cdgo_srvco_slctdo,
	  NULL as cnsctvo_cdgo_tpo_srvco,
      NULL as cnsctvo_cdgo_pln,
      NULL as cdgo_pln,
      NULL as cnsctvo_cdgo_tpo_pln,
      NULL as cdgo_tpo_pln,
      NULL as cnsctvo_cdgo_mdo_cntcto_slctd,
      NULL as dscrpcn_mdo_cntcto,
      NULL as fcha_slctd,
      NULL  as nmro_unco_idntfccn_afldo,
      NULL  as edd_afldo_ans,
	  NULL  as edd_afldo_mss,
	  NULL  as edd_afldo_ds,
      NULL  as nmro_cntrto,
      NULL  as cnsctvo_cdgo_tpo_cntrto,
      @usro,
      NULL  as nmro_slctd_atrzcn_ss,
      NULL  as nmro_slctd_prvdr,
      NULL  as cnsctvo_cdgo_tpo_idntfccn_afldo,
      NULL  as nmro_idntfccn_afldo;
END

GO
