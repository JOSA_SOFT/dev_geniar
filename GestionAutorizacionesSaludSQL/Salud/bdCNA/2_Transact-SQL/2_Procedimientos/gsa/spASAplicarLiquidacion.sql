USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASAplicarLiquidacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASAplicarLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS				  				A\>
* Descripcion			: <\D Sp que permite la ejecución del proceso de		D\>
						  <\D liquidación en línea y masivo						D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/03/22										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASAplicarLiquidacion] 
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @fechaliquidacion	DATETIME = CONVERT(CHAR(10), GETDATE(), 111)

  BEGIN
		
		-- Liquida prestaciones basada en convenios tarificado en una lista de Precios 	
		EXECUTE bdcontratacion.dbo.spLSLiquidacionMasivaListaPrecios;

		-- Se actualiza el resultado del la liquidación en la tabla temporal #tbConceptoGastosTmp_1
		EXEC bdcna.gsa.spASActualizarResultadoLiquidacion;

		-- Calcula el grupo de impresion de un conjunto de prestaciones registradas
		EXEC bdcna.gsa.spascalculargrupoimpresion @fechaliquidacion;

  END
END

GO
