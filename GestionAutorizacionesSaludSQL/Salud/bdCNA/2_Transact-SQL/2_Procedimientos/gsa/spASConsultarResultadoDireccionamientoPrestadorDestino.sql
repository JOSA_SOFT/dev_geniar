USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarResultadoDireccionamientoPrestadorDestino]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarResultadoDireccionamientoPrestadorDestino
* Desarrollado por   : <\A Jhon Olarte\>    
* Descripcion        : <\D Procedimiento expuesto que consulta el calculo de direccionamiento   D\>
        		      <\D de cada una de las prestaciones de la(s) solicitud(es) enviadas				    D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P cdgs_slctd  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 28/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarResultadoDireccionamientoPrestadorDestino] @cdgs_slctds nvarchar(max)
, @estdo_ejccn varchar(5) OUTPUT
, @msje_errr varchar(2000) OUTPUT
, @msje_rspsta nvarchar(max) OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  BEGIN TRY

  --  Creación de tablas temporales para la ejecución de la gestión de solicitudes y ejecución de reglas.
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
    cntrl_prcso char(1),
    usro udtUsuario
  )

  CREATE TABLE #tmpInformacionSolicitudPrestacion (
    id udtConsecutivo IDENTITY (1, 1) NOT NULL,
    cnsctvo_cdgo_prstcn udtConsecutivo NOT NULL,
    cdgo_prstcn varchar(30) NULL,
    cnsctvo_slctd udtConsecutivo NOT NULL,
    nmro_slctd varchar(17) NOT NULL,
    nmro_prvdr varchar(17) NOT NULL,
    cdgo_intrno_ips_dstno char(8) NULL
  );

  DECLARE @negacion char(1),
          @codigoError char(2),
          @cdgs_slctds_XML xml,
          @respsta xml

  SET @negacion = 'N';
  SET @codigoError = 'ET';
    
      SET @cdgs_slctds_XML = CONVERT(xml, @cdgs_slctds)

	  --Se obtiene la información básica de la solicitud.
	  EXEC bdCNA.gsa.spASObtenerDatosBasicos @cdgs_slctds_XML

	  --Se carga la información.
      EXEC bdCNA.gsa.spASCargarInformacionConsultaDireccionamiento @cdgs_slctds_XML

      EXEC gsa.spASConsultarDireccionamientoPrestadorDestino @cdgs_slctds_XML,
                                                             @negacion,
                                                             @estdo_ejccn OUTPUT,
                                                             @msje_errr OUTPUT,
                                                             @respsta OUTPUT
      SET @msje_rspsta = CONVERT(nvarchar(max), @respsta)

    END TRY
    BEGIN CATCH
      SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();
      SET @estdo_ejccn = @codigoError;
      SET @msje_rspsta = NULL
    END CATCH
	
  DROP TABLE #tmpNmroSlctds
  DROP TABLE #tmpInformacionSolicitudPrestacion

  END

GO
