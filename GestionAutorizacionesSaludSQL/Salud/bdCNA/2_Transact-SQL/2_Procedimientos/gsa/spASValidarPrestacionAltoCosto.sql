USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarPrestacionAltoCosto]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarPrestacionAltoCosto
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida si la prestación es de alto costo	 D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarPrestacionAltoCosto] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@cdgo_grpo_entrga CHAR(4),
@agrpa_prstcn udtLogico
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @tbTempConsecutivos TABLE (
    cnsctvo_cdfccn udtConsecutivo
  );

  DECLARE @fechaActual DATETIME,
          @afirmacion char(1)
  SELECT
    @fechaActual = GETDATE(),
    @afirmacion = 'S'

  IF EXISTS (SELECT
      sol.id
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL)
  BEGIN

    --Se obtienen las prestaciones que sean de alto riesgo.
    INSERT INTO @tbTempConsecutivos (cnsctvo_cdfccn)
      SELECT
        c.cnsctvo_prstcn
      FROM bdSisalud.dbo.tbCupsServicios c WITH (NOLOCK)
      INNER JOIN bdSisalud.dbo.tbItemsPresupuesto i WITH (NOLOCK)
        ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
	 INNER JOIN bdSisalud.prm.tbItemPresupuestoAltoCosto_vigencias l WITH(NOLOCK)
		ON i.cnsctvo_cdgo_itm_prspsto = l.cnsctvo_cdgo_itm_prspsto
      WHERE i.dscrpcn_itm_prspsto IS NOT NULL
      AND @fechaActual BETWEEN l.inco_vgnca AND l.fn_vgnca
      UNION ALL
      SELECT
        c.cnsctvo_cms
      FROM bdSisalud.dbo.tbCums AS c WITH (NOLOCK)
      INNER JOIN bdSisalud.dbo.tbItemsPresupuesto AS i WITH (NOLOCK)
        ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
      INNER JOIN bdSisalud.prm.tbItemPresupuestoAltoCosto_vigencias l WITH(NOLOCK)
		ON i.cnsctvo_cdgo_itm_prspsto = l.cnsctvo_cdgo_itm_prspsto
      WHERE i.dscrpcn_itm_prspsto IS NOT NULL
      AND @fechaActual BETWEEN l.inco_vgnca AND l.fn_vgnca
      UNION ALL
      SELECT
        c.cnsctvo_prstcn_pis
      FROM bdsisalud.dbo.tbPrestacionPis AS c WITH (NOLOCK)
      INNER JOIN bdsisalud.dbo.tbItemsPresupuesto AS i WITH (NOLOCK)
        ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
      INNER JOIN bdSisalud.prm.tbItemPresupuestoAltoCosto_vigencias l WITH(NOLOCK)
		ON i.cnsctvo_cdgo_itm_prspsto = l.cnsctvo_cdgo_itm_prspsto
      WHERE i.dscrpcn_itm_prspsto IS NOT NULL
      AND @fechaActual BETWEEN l.inco_vgnca AND l.fn_vgnca

    --Se actualiza al grupo en caso de que la prestación sea de alto riesgo.
    UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
	INNER JOIN @tbTempConsecutivos c
	 ON c.cnsctvo_cdfccn =  sol.cnsctvo_cdfccn
    WHERE sol.cnsctvo_grpo_entrga IS NULL;

    --Se determina si se debe realizar agrupación
    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
      AND @agrpa_prstcn = @afirmacion)
    BEGIN
      UPDATE sol
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @cdgo_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN #tmpDatosSolicitudFechaEntrega tmp
        ON tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
      WHERE tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga

    END
  END
END

GO
