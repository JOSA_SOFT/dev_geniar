Use bdCNA
Go



If(Object_id('gsa.spASAnularAtencionProcedimientoSipres') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASAnularAtencionProcedimientoSipres As Select 1'
End
Go

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASAnularAtencionProcedimientoSipres
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento que se encarga de anular las prestaciones en Sipres D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 02/10/2017  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/



Alter Procedure gsa.spASAnularAtencionProcedimientoSipres
As
Begin
	Set Nocount On

	Declare	@fcha_actl			Datetime,
			@cnsctvo_cdgo_csa	udtConsecutivo,
			@cnsctvo_cdgo_prfl	udtConsecutivo,
			@cnsctvo_cdgo_estdo_mga_anldo udtConsecutivo;

	Create Table #tempConceptosAnulados (
		cnsctvo_cdgo_ofcna		udtConsecutivo,
		nuam					Decimal(18, 0),
		cnsctvo_ops				Float,
		usro_anla				udtUsuario,
		fcha_anla				Datetime,
		cnsctvo_cdgo_csa		udtConsecutivo,
		cnsctvo_cdgo_prfl		udtConsecutivo,
		cnsctvo_cdgo_estnca		udtConsecutivo,
		fcha_ultma_mdfccn		Datetime
	)

	Set @fcha_actl = getDate();
	Set	@cnsctvo_cdgo_csa = 0;
	Set @cnsctvo_cdgo_prfl = 121;
	Set @cnsctvo_cdgo_estdo_mga_anldo = 14;

	-- 
	Update		c
	Set			cnsctvo_cdgo_estdo = a.cnsctvo_cdgo_estdo_sprs, 
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = s.usro_anlcn
	From		#tempInformacionSolicitud s
	Inner Join	#tempInformacionServicios a
	On			a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BDSisalud.dbo.tbASDatosAdicionalesMegaSipres b With(NoLock)
	On			b.cnsctvo_slctd_mga = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	And			b.nuam = a.nuam
	And			b.cnsctvo_prstcn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join	BDSisalud.dbo.tbProcedimientos c With(RowLock)
	On			c.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	And			c.nuam = b.nuam
	And			c.cnsctvo_prstcn = b.cnsctvo_prstcn
	Where		a.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;

	-- 
	Update		c
	Set			cnsctvo_cdgo_estdo = a.cnsctvo_cdgo_estdo_sprs, 
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = s.usro_anlcn
	From		#tempInformacionSolicitud s
	Inner Join	#tempInformacionServicios a
	On			a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BDSisalud.dbo.tbASDatosAdicionalesMegaSipres b With(NoLock)
	On			b.cnsctvo_slctd_mga = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	And			b.nuam = a.nuam
	And			b.cnsctvo_prstcn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join	BDSisalud.dbo.tbConceptosOps c With(RowLock)
	On			c.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	And			c.nuam = b.nuam
	And			c.cnsctvo_prstcn = b.cnsctvo_prstcn
	Where		a.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;

	-- 
	Insert Into #tempConceptosAnulados (
				cnsctvo_cdgo_ofcna		,nuam					,cnsctvo_ops	  ,
				usro_anla				,fcha_anla				,cnsctvo_cdgo_csa ,
				cnsctvo_cdgo_prfl		,cnsctvo_cdgo_estnca	,fcha_ultma_mdfccn
	)
	Select		Distinct c.cnsctvo_cdgo_ofcna,	c.nuam,					c.cnsctvo_ops,
				s.usro_anlcn,			@fcha_actl,				@cnsctvo_cdgo_csa,
				@cnsctvo_cdgo_prfl,		c.cnsctvo_cdgo_estdo,	@fcha_actl
	From		#tempInformacionSolicitud s
	Inner Join	#tempInformacionServicios a
	On			a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BDSisalud.dbo.tbASDatosAdicionalesMegaSipres b With(NoLock)
	On			b.cnsctvo_slctd_mga = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	And			b.nuam = a.nuam
	And			b.cnsctvo_prstcn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join	BDSisalud.dbo.tbConceptosOps c With(RowLock)
	On			c.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	And			c.nuam = b.nuam
	And			c.cnsctvo_prstcn = b.cnsctvo_prstcn
	Where		a.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;

		
	Insert Into BDSisalud.dbo.tbConceptosAnulados (
			cnsctvo_cdgo_ofcna		,nuam					,cnsctvo_ops	  ,
			usro_anla				,fcha_anla				,cnsctvo_cdgo_csa ,
			cnsctvo_cdgo_prfl		,cnsctvo_cdgo_estnca	,fcha_ultma_mdfccn
	)
	Select	Distinct cnsctvo_cdgo_ofcna		,nuam					,cnsctvo_ops	  ,
			usro_anla				,fcha_anla				,cnsctvo_cdgo_csa ,
			cnsctvo_cdgo_prfl		,cnsctvo_cdgo_estnca	,fcha_ultma_mdfccn
	From	#tempConceptosAnulados;

	Drop Table #tempConceptosAnulados;

End



