USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionPrestacionSolicitud]    Script Date: 13/06/2017 01:06:30 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosConsecutivoPrestaciones
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jorge Rodriguez De León	AM\>
* Descripcion			 : <\DM	
                                Se modifica SP para que devuelva el auditor al que fue redireccionada 
								la prestación y se muestre en la pantalla de contratacion						
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-03-08 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jorge Rodriguez De León	AM\>
* Descripcion			 : <\DM	
                                Se modifica SP para que devuelva el tipo de prestación CUPS correcto					
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-03-28 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM	
                                Se modifica SP para que permita mostrar las prestaciones en 
								Gestión DOMI				
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-05-17 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Carlos Andres Lopez Ramirez	AM\>
* Descripcion			 : <\DM	
                                Se agrega recuperacion del campo cnsctvo_cdgo_rcbro de la tabla de servicios,
								control de cambio 068
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-05-19 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing. Carlos Andres Lopez Ramirez	AM\>
* Descripcion			 : <\DM	
                                Se agrega recuperacion del campo cnsctvo_cdgo_csa_no_cbro_cta_rcprcn de la tabla de servicios,
								control de cambio 092, punto 19: Causales de no cobro por prestacion.
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-06-13 FM\>
*--------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaDatosInformacionPrestacionSolicitud] 395128
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionPrestacionSolicitud]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
	@tpo_prstcn				varchar(10) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	declare @si							varchar(10)	  = 'MISMA VIA',
			@no							CHAR(2)	  = 'NO',
			@estadoSi					CHAR(2)     ='SI',
			@cdgo_pln					udtConsecutivo,
			@cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
			@nmro_idntfccn				udtNumeroIdentificacionLargo,
			@Nui_Afldo					udtConsecutivo,
			@plan_atencion				udtDescripcion,
			@tipo_plan					udtDescripcion,
			@fechaDia					datetime = Convert(char(10),getDate(),111),
			@riesgo_clinico				udtDescripcion,
			@cups						int = 4,
			@cnsctvo_cdgo_cdd			udtConsecutivo,
			@cps						varchar(5) = 'CUPS',
			@cms						varchar(5) = 'CUMS',
			@cnsctvo_cdgo_va_accso		int = 1,
			@cnsctvo_cdgo_estdo_srvco_slctdo	int = 5,			
			@cdna_vca					char(1) = '';
			
			

	CREATE TABLE #PrestacionesAprobadas(
		cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
		cnsctvo_cdgo_tpo_cdfccn				udtConsecutivo,
		dscrpcn_tpo_cdfccn					udtDescripcion,
		cnsctvo_cdgo_srvco_slctdo			udtConsecutivo,
		cnsctvo_srvco_slctdo				udtConsecutivo,
		cdgo_cdfccn							char(11),
		dscrpcn_cdfccn						udtDescripcion,
		cnsctvo_cdgo_pln					udtConsecutivo,
		pln_afldo							udtDescripcion,
		obsrvcn_adcnl						udtObservacion,
		cdgo_ltrldd							char(5),
		dscrpcn_ltrldd						udtDescripcion,
		cnsctvo_cdgo_va_accso				udtConsecutivo,
		dscrpcn_cdgo_va_accso				udtDescripcion,
		cdgo_intrno							udtCodigoIps,
		nmbre_scrsl							udtDescripcion,
		usro_ultma_mdfccn					udtUsuario,
		fcha_ultma_mdfccn					datetime,
		dscrpcn_estdo_srvco_slctdo			udtDescripcion,
		obsrvcn								udtObservacion default '',
		atl									udtLogico default 0,
		cnsctvo_cdgo_estdo_srvco_slctdo		udtConsecutivo,
		rqur_adtra							char(2),
		cntdd_slctda						udtConsecutivo,
		dss 								udtConsecutivo, --consPosologiaCUMS
		dscrpcn_dss 						udtDescripcion, --posologiaCUMS
		prdcdd_dss 							udtConsecutivo, --cada
		dscrpcn_frcnca						udtDescripcion,
		dscrpcn_chrte						udtDescripcion, -- recobro
		cnsctvo_cdgo_cdd_rsdnca_afldo		udtConsecutivo,
		cnsctvo_cdgo_prsntcn_dss			udtConsecutivo,
		cnsctvo_cdgo_undd_prdcdd_dss		udtConsecutivo,
		cnsctvo_cdgo_ltrldd					udtConsecutivo,
		rqre_otra_gstn_adtr					udtDescripcion,
		cnsctvo_cdgo_rcbro					udtConsecutivo, 
		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn	udtConsecutivo	
	);

	Create table #HistPrestadorServiciosSolicitado(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		obsrvcn							udtObservacion
	)

	If @tpo_prstcn = @cms
	   Begin  
	       Set @tpo_prstcn = @cdna_vca
	   End

	
	SELECT	   @cdgo_pln                  = AFI.cnsctvo_cdgo_pln,
			   @cnsctvo_cdgo_tpo_idntfccn = AFI.cnsctvo_cdgo_tpo_idntfccn_afldo,
			   @nmro_idntfccn             = AFI.nmro_idntfccn_afldo,
			   @cnsctvo_cdgo_cdd          = AFI.cnsctvo_cdgo_cdd_rsdnca_afldo
	FROM       BDCNA.GSA.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK)
	INNER JOIN BDCNA.GSA.tbASInformacionAfiliadoSolicitudAutorizacionServicios AFI WITH(NOLOCK)  ON AFI.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
	WHERE      SAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;
	
	Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn);
	Exec gsa.spASPVencConsultaPlanAtencionAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @Nui_Afldo, @cdgo_pln,  @plan_atencion output, @tipo_plan output
	
	IF @tpo_prstcn = @cps or @tpo_prstcn IS NULL or @tpo_prstcn = @cdna_vca
	BEGIN

		INSERT INTO #PrestacionesAprobadas (
					cnsctvo_slctd_atrzcn_srvco,
					cnsctvo_cdgo_tpo_cdfccn,
					dscrpcn_tpo_cdfccn,
					cnsctvo_cdgo_srvco_slctdo,
					cdgo_cdfccn,
					dscrpcn_cdfccn,
					cnsctvo_cdgo_pln,
					pln_afldo,
					cdgo_ltrldd,
					dscrpcn_ltrldd,
					cnsctvo_cdgo_va_accso,
					dscrpcn_cdgo_va_accso,
					usro_ultma_mdfccn,
					fcha_ultma_mdfccn,
					dscrpcn_estdo_srvco_slctdo,
					rqur_adtra,
					cntdd_slctda,
					cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_cdd_rsdnca_afldo,
					cnsctvo_cdgo_prsntcn_dss,
					cnsctvo_cdgo_undd_prdcdd_dss,
					cnsctvo_cdgo_estdo_srvco_slctdo,
					cnsctvo_cdgo_ltrldd,
					rqre_otra_gstn_adtr,
					cnsctvo_cdgo_rcbro,
					cnsctvo_cdgo_csa_no_cbro_cta_rcprcn								
		)
		SELECT		@cnsctvo_slctd_atrzcn_srvco,
					ss.cnsctvo_cdgo_tpo_srvco,
					TCF.dscrpcn_tpo_cdfccn,
					SS.cnsctvo_cdgo_srvco_slctdo,
					CF.cdgo_cdfccn,
					CF.dscrpcn_cdfccn,
					AFI.cnsctvo_cdgo_pln,
					@plan_atencion,
					LTR.cdgo_ltrldd,
					LTR.dscrpcn_ltrldd,
					PIS.cnsctvo_cdgo_va_accso,
					CASE PIS.cnsctvo_cdgo_va_accso WHEN @cnsctvo_cdgo_va_accso THEN @si ELSE @no END,
					SS.usro_ultma_mdfccn,
					SS.fcha_ultma_mdfccn,
					ESS.dscrpcn_estdo_srvco_slctdo,
					CASE ESS.cnsctvo_cdgo_estdo_srvco_slctdo WHEN @cnsctvo_cdgo_estdo_srvco_slctdo THEN  @estadoSi ELSE @no END,
					SS.cntdd_slctda,
					SS.cnsctvo_srvco_slctdo,
					@cnsctvo_cdgo_cdd,
					null,
					null,
					SS.cnsctvo_cdgo_estdo_srvco_slctdo,
					LTR.cnsctvo_cdgo_ltrldd,
					ISNULL(ss.rqre_otra_gstn_adtr,@cdna_vca),
					ss.cnsctvo_cdgo_rcbro,
					ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
		FROM BDCNA.GSA.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK)
		INNER JOIN BDCNA.GSA.tbASServiciosSolicitados SS WITH(NOLOCK) ON SAS.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
		INNER JOIN BDCNA.GSA.tbASProcedimientosInsumosSolicitados PIS WITH(NOLOCK) ON SS.cnsctvo_srvco_slctdo = PIS.cnsctvo_srvco_slctdo 
		INNER JOIN BDCNA.GSA.tbASInformacionAfiliadoSolicitudAutorizacionServicios AFI WITH(NOLOCK)  ON AFI.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
		INNER JOIN bdSisalud.dbo.tbCodificaciones CF WITH (NOLOCK) on CF.cnsctvo_cdfccn = SS.cnsctvo_cdgo_srvco_slctdo
		INNER JOIN bdSisalud.dbo.tbLateralidades_Vigencias LTR WITH(NOLOCK) ON PIS.cnsctvo_cdgo_ltrldd = LTR.cnsctvo_cdgo_ltrldd
		INNER JOIN BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios IPS WITH(NOLOCK) ON IPS.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
		INNER JOIN BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ESS  WITH(NOLOCK) ON ESS.cnsctvo_cdgo_estdo_srvco_slctdo = SS.cnsctvo_cdgo_estdo_srvco_slctdo
		INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on ss.cnsctvo_cdgo_tpo_srvco = tcf.cnsctvo_cdgo_tpo_cdfccn
		WHERE SAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		AND  ss.cnsctvo_cdgo_tpo_srvco in(@cups)
		AND  @fechaDia BETWEEN LTR.inco_vgnca and LTR.fn_vgnca
		AND  @fechaDia BETWEEN ESS.inco_vgnca and ESS.fn_vgnca
		AND  @fechaDia between tcf.inco_vgnca and tcf.fn_vgnca;
	END

	IF @tpo_prstcn = @cms or @tpo_prstcn IS NULL or @tpo_prstcn = @cdna_vca
	BEGIN
		INSERT INTO #PrestacionesAprobadas (
					cnsctvo_slctd_atrzcn_srvco,
					cnsctvo_cdgo_tpo_cdfccn,
					dscrpcn_tpo_cdfccn,
					cnsctvo_cdgo_srvco_slctdo,
					cdgo_cdfccn,
					dscrpcn_cdfccn,
					cnsctvo_cdgo_pln,
					pln_afldo,
					cdgo_ltrldd,
					dscrpcn_ltrldd,
					cnsctvo_cdgo_va_accso,
					dscrpcn_cdgo_va_accso,
					usro_ultma_mdfccn,
					fcha_ultma_mdfccn,
					dscrpcn_estdo_srvco_slctdo,
					rqur_adtra,
					cntdd_slctda,
					dss,
					dscrpcn_dss,
					prdcdd_dss,
					dscrpcn_frcnca,
					cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_cdd_rsdnca_afldo,
					cnsctvo_cdgo_prsntcn_dss,
					cnsctvo_cdgo_undd_prdcdd_dss,
					cnsctvo_cdgo_estdo_srvco_slctdo,
					cnsctvo_cdgo_ltrldd,
					rqre_otra_gstn_adtr,
					cnsctvo_cdgo_rcbro,
					cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
		)
		SELECT		@cnsctvo_slctd_atrzcn_srvco,
					ss.cnsctvo_cdgo_tpo_srvco,
					TCF.dscrpcn_tpo_cdfccn,
					SS.cnsctvo_cdgo_srvco_slctdo,
					CF.cdgo_cdfccn,
					CF.dscrpcn_cdfccn,
					AFI.cnsctvo_cdgo_pln,
					@plan_atencion,
					null, 
					null, 
					null, 
					null,
					SS.usro_ultma_mdfccn,
					SS.fcha_ultma_mdfccn,
					ESS.dscrpcn_estdo_srvco_slctdo,
					CASE ESS.cnsctvo_cdgo_estdo_srvco_slctdo WHEN @cnsctvo_cdgo_estdo_srvco_slctdo THEN @estadoSi ELSE @no END,
					SS.cntdd_slctda,
					MED.dss,
					P.dscrpcn_dss,
					MED.prdcdd_dss,
					F.dscrpcn_frcnca,
					SS.cnsctvo_srvco_slctdo,
					@cnsctvo_cdgo_cdd,
					MED.cnsctvo_cdgo_prsntcn_dss,
					MED.cnsctvo_cdgo_undd_prdcdd_dss,
					SS.cnsctvo_cdgo_estdo_srvco_slctdo,
					null,
					ISNULL(ss.rqre_otra_gstn_adtr,@cdna_vca),
					ss.cnsctvo_cdgo_rcbro,
					ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
		FROM BDCNA.GSA.tbASSolicitudesAutorizacionServicios SAS WITH(NOLOCK)
		INNER JOIN BDCNA.GSA.tbASServiciosSolicitados SS WITH(NOLOCK) ON SAS.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
		INNER JOIN bdSisalud.dbo.tbCodificaciones CF WITH (NOLOCK) on CF.cnsctvo_cdfccn = SS.cnsctvo_cdgo_srvco_slctdo
		INNER JOIN BDCNA.GSA.tbASInformacionAfiliadoSolicitudAutorizacionServicios AFI WITH(NOLOCK)  ON AFI.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco

		INNER JOIN BDCna.gsa.tbASMedicamentosSolicitados MED WITH(NOLOCK) ON SS.cnsctvo_srvco_slctdo = MED.cnsctvo_srvco_slctdo 
		INNER JOIN BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios IPS WITH(NOLOCK) ON IPS.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
		
		INNER JOIN BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ESS WITH (NOLOCK) ON ESS.cnsctvo_cdgo_estdo_srvco_slctdo = SS.cnsctvo_cdgo_estdo_srvco_slctdo
		INNER JOIN bdSisalud.dbo.tbMNDosis_Vigencias P  WITH(NOLOCK) ON P.cnsctvo_cdgo_dss = MED.cnsctvo_cdgo_prsntcn_dss
		INNER JOIN bdSisalud.dbo.tbMNFrecuencia F  WITH(NOLOCK) ON F.cnsctvo_cdgo_frcnca =  MED.cnsctvo_cdgo_undd_prdcdd_dss
		INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on cf.cnsctvo_cdgo_tpo_cdfccn = tcf.cnsctvo_cdgo_tpo_cdfccn
		
		WHERE SAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		AND  @fechaDia BETWEEN ESS.inco_vgnca and ESS.fn_vgnca
		AND  @fechaDia BETWEEN P.inco_vgnca and P.fn_vgnca
		AND  @fechaDia BETWEEN tcf.inco_vgnca and tcf.fn_vgnca;

	END
	--Devuelve el prestador para cada una de las prestaciones
	UPDATE  pa 	Set cdgo_intrno = rd.cdgo_intrno,
					nmbre_scrsl = dp.nmbre_scrsl
	From		#PrestacionesAprobadas pa
	Inner join	BDCna.gsa.tbASResultadoDireccionamiento rd WITH(NOLOCK) on pa.cnsctvo_srvco_slctdo = rd.cnsctvo_srvco_slctdo
	Inner join	BDSisalud.dbo.tbDireccionesPrestador DP WITH(NOLOCK) ON DP.cdgo_intrno = rd.cdgo_intrno
	

	--Devuelve Marcas Atel
	UPDATE  pa 	Set atl = 1
	From #PrestacionesAprobadas pa
	Inner join BDCna.gsa.tbASMarcasServiciosSolicitados m WITH(NOLOCK) 
	on m.cnsctvo_srvco_slctdo = pa.cnsctvo_srvco_slctdo;

	--Valida si el afiliado tiene Cohortes asociadas
	Set @riesgo_clinico = (select c.dscrpcn_chrte from BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias with(nolock)
						   inner join BdRiesgosSalud.dbo.tbCohortes c with(nolock)
						   on ias.cnsctvo_cdgo_chrte = c.cnsctvo_cdgo_chrte
						   where ias.cnsctvo_slctd_atrzcn_srvco = 	@cnsctvo_slctd_atrzcn_srvco)
	

	UPDATE pa SET dscrpcn_chrte = @riesgo_clinico 
	FROM #PrestacionesAprobadas pa

	Insert Into #HistPrestadorServiciosSolicitado (
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo,
		obsrvcn
	)
	Select hps.cnsctvo_slctd_atrzcn_srvco, hps.cnsctvo_srvco_slctdo, hps.obsrvcn from BDCna.gsa.tbASHistoricoPrestadorServiciosSolicitados hps
	Inner Join #PrestacionesAprobadas pa On pa.cnsctvo_slctd_atrzcn_srvco = hps.cnsctvo_slctd_atrzcn_srvco
	where hps.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And cnsctvo_hstrco_prstdr_srvco_slctd = (select Max(cnsctvo_hstrco_prstdr_srvco_slctd) from BDCna.gsa.tbASHistoricoPrestadorServiciosSolicitados hps2
											 where hps2.cnsctvo_slctd_atrzcn_srvco = pa.cnsctvo_slctd_atrzcn_srvco 
											 and hps.cnsctvo_srvco_slctdo = pa.cnsctvo_srvco_slctdo) 

	UPDATE pa SET obsrvcn_adcnl = temp.obsrvcn 
	FROM #PrestacionesAprobadas pa
	Inner Join #HistPrestadorServiciosSolicitado temp on temp.cnsctvo_slctd_atrzcn_srvco = pa.cnsctvo_slctd_atrzcn_srvco
	And temp.cnsctvo_srvco_slctdo = pa.cnsctvo_srvco_slctdo

	SELECT 	cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_tpo_cdfccn	,
			dscrpcn_tpo_cdfccn,					cnsctvo_cdgo_srvco_slctdo,
			cdgo_cdfccn	,						dscrpcn_cdfccn,						
			cnsctvo_cdgo_pln,					pln_afldo,							
			obsrvcn_adcnl,
			cdgo_ltrldd,						dscrpcn_ltrldd,
			cnsctvo_cdgo_va_accso,				dscrpcn_cdgo_va_accso,
			cdgo_intrno,						nmbre_scrsl,				
			usro_ultma_mdfccn,					fcha_ultma_mdfccn,
			dscrpcn_estdo_srvco_slctdo,			'' obsrvcn	,
			atl,							
			rqur_adtra,							cntdd_slctda,
			dss,								dscrpcn_dss,
			prdcdd_dss,							dscrpcn_frcnca,
			dscrpcn_chrte,
			cnsctvo_srvco_slctdo,
			cnsctvo_cdgo_cdd_rsdnca_afldo,		cnsctvo_cdgo_prsntcn_dss,
			cnsctvo_cdgo_undd_prdcdd_dss,
			cnsctvo_cdgo_estdo_srvco_slctdo,	cnsctvo_cdgo_ltrldd,
			rqre_otra_gstn_adtr,
			cnsctvo_cdgo_rcbro,					cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
		FROM #PrestacionesAprobadas;

	DROP TABLE #HistPrestadorServiciosSolicitado; 
	DROP TABLE #PrestacionesAprobadas;
END
