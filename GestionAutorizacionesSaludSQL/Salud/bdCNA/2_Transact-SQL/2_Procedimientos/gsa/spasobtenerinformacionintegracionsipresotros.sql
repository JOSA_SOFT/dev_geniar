USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spasobtenerinformacionintegracionsipresotros]    Script Date: 21/07/2017 02:27:35 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASObtenerInformacionIntegracionSipresOTROS
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento encargado de extraer la información necesaria del otros	D\>    
					   <\D sistemas para realizar la replicación al sistema destino de Sipres		D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jonathan Chamizo Quina AM\>    
* Descripcion        : <\D Se adiciona script para validar si existe el SP en la BD. En caso contrario
					 :     crea con la instrucción AS SELECT 1										   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-09-20  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Luis Fernando Benavides AM\>    
* Descripcion        : <\D 1.Cambio logica calculo empresa aportante para mejorar rendimiento		  
					 :     2.Asignar valor 0 (VACIO) si la morbilidad no esta parametrizada		       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-13  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Luis Fernando Benavides AM\>    
* Descripcion        : <\D 1.Cambio logica calculo empresa aportante para mejorar rendimiento		  
					 :     2.Asignar valor 0 (VACIO) si la morbilidad no esta parametrizada		       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-13  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                          Se modifica el procedimiento quitando el calculo del valor de referencia para que se tome de la tabla de servicios
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  11/11/2016  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se retira update que saca el estado homologo para las prestaciones en sipres, ya se calcula en
						  spAsObtenerInformacionIntegracionSipresMega
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  09/02/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se agrega update para recuperar el cnsctvo_cdgo_ofcna de tbASSolicitudesAutorizacionServicios
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  05/04/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se agrega recuperacion del campo cnsctvo_det_prgrmcn_fcha en la tabla temporal #tmpinformacionservicios
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  10/04/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se agrega cruce con el consecutivo de la solicitud al recuperar el nuam de tbashomologacionserviciosmegasipres
						  y al guardar en dicha tabla.
						  Se separan los condicionales del los Join, se agregan en el where, se dejan en los Join unicamente los 
						  condicionales que corresponden a referencias entre tablas
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  17/04/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se agregan sentencias para actualizar el codigo de la oficina en tbAsSolicitudesAutorizacionServicios 
						  si esta es null o 0
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  24/04/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                          Se agregan Condicionales para recuperar el consecutivo de la oficina,.
						  Se cambia declaracion de tablas temporales del sp.
						  Se cambia Merge con tbashomologacionserviciosmegasipres de forma que 
						  solo inserte y no actualice.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  26/05/2017  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez G. sisjvg01 AM\>    
* Descripcion        : <\D 
                          Se Implementa bloqueo a nivel de registro a la tabla tbConsecutivosPorOficina y se modifica el 
						  sppmgeneraconsecutivoatencion por el sppmgeneraconsecutivoatencion
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017/06/12  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D 
                          Se retira la recuperacion del nuam y la oficina ya que estos se recuperan para el proceso de agrupacion.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017/06/22  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D 
                          Se agrega update para actualizar los campos nuam con valor 0 a Null
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017/07/21  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spasobtenerinformacionintegracionsipresotros]
AS
BEGIN
  SET NOCOUNT ON

  Create Table #tmpProcedimientos (
    slccn INT,
    cdgo_prstcn VARCHAR(30),
    dscrpcn_prstcn VARCHAR(150),
    dscrpcn_orgn_prstcn VARCHAR(150),
    cnsctvo_orgn_prstcn INT,
    cnsctvo_prstcn INT,
    cnsctvo_prgrmcn_prstcn INT,
    cnsctvo_prgrmcn_fcha_evnto INT,
    cnsctvo_cdgo_prsntcn INT,
    cntdd_prsntcn CHAR(20),
    cnsctvo_cdgo_prdcdd INT,
    fcha_inco DATETIME,
    nmro_entrgs INT,
    cntdd_entrgs INT,
    cntdd_dss INT,
    cnsctvo_cdgo_dss INT,
    cntdd_cda INT,
    cnsctvo_cdgo_frcnca_cda INT,
    cnsctvo_cdgo_estds_entrga INT,
    fcha_dsde DATETIME,
    fcha_hsta DATETIME,
    cnsctvo_cdgo_frcnca INT,
    cnsctvo_det_prgrmcn_fcha INT,
    fcha_entrga DATETIME,
    cntdd INT,
    dscrpcn_estdo_entrga VARCHAR(150),
    nmro_unco_idntfccn_afldo INT,
    rqre_prvdr CHAR(1),
    cnsctvo_cdgo_ofcna_ops INT,
    cnsctvo_ops INT,
    cdgo_intrno_prvdr_insmo CHAR(8),
	cnsctvo_cdgo_clsfccn_evnto int,
	ds_mxmo_prgrmcns_ftrs_ntfccns int
  )

  Create Table #tmpEmpleadores (
    cdgo_tpo_idntfccn VARCHAR(3),
    nmro_idntfccn VARCHAR(23),
    rzn_scl VARCHAR(200),
    idntfccn_cmplta_empldr VARCHAR(30),
    cnsctvo_cdgo_tpo_idntfccn INT,
    nmro_unco_idntfccn_aprtnte INT,
    prncpl CHAR,
    cnsctvo_scrsl_ctznte INT,
    cnsctvo_cdgo_crgo_empldo INT,
    cnsctvo_cdgo_clse_aprtnte INT,
    cnsctvo_cdgo_tpo_ctznte INT,
    inco_vgnca_cbrnza DATETIME,
    fn_vgnca_cbrnza DATETIME,
    cnsctvo_cdgo_tpo_cbrnza INT,
    cnsctvo_cbrnza INT,
    slro_bsco INT,
    cnsctvo_cdgo_tpo_cntrto INT,
    nmro_cntrto VARCHAR(15),
    cnsctvo_prdcto_scrsl INT,
    cnsctvo_scrsl INT,
    cnsctvo_cdgo_actvdd_ecnmca INT,
    cnsctvo_cdgo_arp INT,
    drccn VARCHAR(80),
    tlfno VARCHAR(30),
    cdgo_actvdd_ecnmca CHAR(4),
    dscrpcn_actvdd_ecnmca VARCHAR(150),
    cdgo_crgo CHAR(4),
    dscrpcn_crgo VARCHAR(150),
    cdgo_entdd CHAR(8),
    dscrpcn_entdd VARCHAR(150),
    cdgo_tpo_ctznte CHAR(2),
    dscrpcn_tpo_ctznte VARCHAR(150),
    nmbre_cntcto VARCHAR(140),
    tlfno_cntcto udttelefono,
    eml_cntcto udtemail,
    cnsctvo_cdgo_cdd udtconsecutivo,
    cdgo_cdd CHAR(8),
    dscrpcn_cdd udtdescripcion,
    cnsctvo_cdgo_dprtmnto udtconsecutivo,
    cdgo_dprtmnto CHAR(3),
    dscrpcn_dprtmnto udtdescripcion,
    eml udtemail,
    cdgo_cnvno_cmrcl CHAR(2),
    dscrpcn_cnvno_cmrcl CHAR(150)
  )

  Create Table #tmpEmpleadoresxafiliado (
    id INT IDENTITY (1, 1),
    id_afldo INT,
    nmro_unco_idntfccn_aprtnt VARCHAR(15),
    prncpl CHAR(1) DEFAULT 'N',
    rzn_scl CHAR(35)
  )

  Create Table #tmpTmpSolicitud (
    id INT IDENTITY (1, 1),
    id_sol INT
  )

  DECLARE @afrmcn CHAR(1),
          @cnsctvo_tpo_cncpto INT,
          @ngcn CHAR(1),
          @cnsctvo_cms INT,
          @cnsctvo_prstcn INT,
          @cnsctnte_cro INT,
          @ttl INT,
          @indce INT,
          @ofcna INT,
          @lnmroatncn INT,
          @cnsctvo_cdgo_tpo_cntrto INT,
          @nmro_cntrto VARCHAR(15),
          @cnsctvo_cdgo_tpo_idntfccn_afldo INT,
          @nmro_unco_idntfccn_afldo VARCHAR(23),
          @id INT,
          @lnnmro_unco INT,
          @lnnmro INT,
          @lccnsctvo_cdgo_ofcna_ntfccn INT,
          @lntpoidntfccnpcnte INT,
          @lcnmroidntfccnpcnte udtNumeroIdentificacion,
          @lcnmroacta INT,
          @lcmensaje CHAR(200),
          @fcha_actl DATETIME,
          @usro udtusuario,
		  @cnsctvo_cdgo_tpo_cbrnza	udtconsecutivo = 4,
		  @estdo char(1) = 'S',
		  @prncpl char(1) = 'S',
		  @vlr_cro			Int,
		  @vlr_nll			udtConsecutivo;


  SELECT  @afrmcn = 'S',
          @cnsctvo_tpo_cncpto = 12, --NUAM
          @ngcn = 'N',
          @cnsctvo_cms = 5,
          @cnsctvo_prstcn = 4,
          @cnsctnte_cro = 0,
          @indce = 1,
          @fcha_actl = GETDATE(),
          @usro = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user))

  Set @vlr_cro = 0;
  Set @vlr_nll = Null;

  -- cnsctvo_cdgo_ofcna
  UPDATE	 iso
  SET		 cnsctvo_cdgo_ofcna = hss.cnsctvo_cdgo_ofcna_atrzcn
  FROM		 #tmpinformacionsolicitud ISO
  INNER JOIN bdcna.gsa.tbASSolicitudesAutorizacionServicios HSS WITH (NOLOCK)
  ON		 hss.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
  Where		 hss.cnsctvo_cdgo_ofcna_atrzcn != @vlr_cro
  And		 hss.cnsctvo_cdgo_ofcna_atrzcn Is Not Null
  And		 iso.cnsctvo_cdgo_ofcna = @vlr_cro Or  iso.cnsctvo_cdgo_ofcna Is Null

  UPDATE     iso
  SET        cnsctvo_cdgo_ofcna = ofv.cnsctvo_cdgo_ofcna
  FROM       #tmpinformacionsolicitud ISO
  INNER JOIN #tmpinformacionafiliado IAF
  ON         IAF.cnsctvo_slctd_atrzcn_srvco = ISO.cnsctvo_slctd_atrzcn_srvco
  INNER JOIN bdAfiliacionValidador.dbo.tbOficinas_Vigencias OFV WITH (NOLOCK)
  ON         OFV.cnsctvo_cdgo_sde = IAF.cnsctvo_cdgo_sde_ips_prmra
  Where      OFV.prncpl = @afrmcn
  AND        @fcha_actl BETWEEN OFV.inco_vgnca AND OFV.fn_vgnca
  And		 iso.cnsctvo_cdgo_ofcna = @vlr_cro Or  iso.cnsctvo_cdgo_ofcna Is Null

  UPDATE     iso
  SET        cnsctvo_cdgo_ofcna = ous.cnsctvo_cdgo_ofcna
  FROM       #tmpinformacionsolicitud ISO
  INNER JOIN #tmpinformacionafiliado IAF
  ON         IAF.cnsctvo_slctd_atrzcn_srvco = ISO.cnsctvo_slctd_atrzcn_srvco
  INNER JOIN bdSeguridad.dbo.tbUsuariosWeb OUS WITH (NOLOCK)
  ON         OUS.lgn_usro = ISO.usro_crcn
  WHERE      iso.cnsctvo_cdgo_ofcna = @vlr_cro Or  iso.cnsctvo_cdgo_ofcna Is Null

  -- Se valida si la solicitud tiene la oficina y si no la actualiza
  UPDATE	 hss
  SET		 cnsctvo_cdgo_ofcna_atrzcn= iso.cnsctvo_cdgo_ofcna
  FROM		 bdcna.gsa.tbASSolicitudesAutorizacionServicios HSS With (RowLock)
  INNER JOIN #tmpinformacionsolicitud ISO
  ON		 hss.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
  Where		 hss.cnsctvo_cdgo_ofcna_atrzcn = @vlr_cro
  

  UPDATE	 hss
  SET		 cnsctvo_cdgo_ofcna_atrzcn= iso.cnsctvo_cdgo_ofcna
  FROM		 bdcna.gsa.tbASSolicitudesAutorizacionServicios HSS With (RowLock)
  INNER JOIN #tmpinformacionsolicitud ISO
  ON		 hss.cnsctvo_slctd_atrzcn_srvco = iso.cnsctvo_slctd_atrzcn_srvco
  Where		 hss.cnsctvo_cdgo_ofcna_atrzcn Is Null

  --Se marcan las solicitudes que no existan en el destino.
  Update	#tmpinformacionsolicitud
  Set		nuam = @vlr_nll
  Where		nuam = @vlr_cro

  INSERT INTO #tmpTmpSolicitud (id_sol)
  SELECT ISO.id
  FROM   #tmpinformacionsolicitud ISO
  WHERE  ISO.nuam Is Null

  SELECT @ttl = COUNT(1) FROM #tmpTmpSolicitud;

  WHILE @indce <= @ttl
  BEGIN
		--nuam
		SELECT		@ofcna = ISO.cnsctvo_cdgo_ofcna,
					@id    = TTI.id_sol
		FROM		#tmpinformacionsolicitud ISO
		INNER JOIN	#tmpTmpSolicitud TTI
		ON			TTI.id_sol = ISO.id
		WHERE		TTI.id = @indce

		--Se incrementa el valor de la secuencia a 1.
		EXEC bdsisalud.dbo.sppmgeneraconsecutivoatencion1 @ofcna,
														 @cnsctvo_tpo_cncpto,
														 @lnmroatncn OUTPUT

		UPDATE iso
		SET    nuam = @lnmroatncn 
		FROM   #tmpinformacionsolicitud ISO
		WHERE  ISO.id = @id

		SET @indce = @indce + 1

		/*
		--Se incrementa el valor de la secuencia a 1.
		UPDATE cpo
		SET    ultmo_vlr = cpo.ultmo_vlr + 1
		FROM   bdsisalud.dbo.tbconsecutivosporoficina CPO With(RowLock) 
		WHERE  CPO.cnsctvo_cdgo_ofcna = @ofcna
		AND    CPO.cnsctvo_cdgo_cncpto = @cnsctvo_tpo_cncpto
		*/
  END

  /*Calcula Empresa Aportante*/
  UPDATE		IAF
  SET			IAF.nmro_unco_idntfccn_aprtnt	= CV.nmro_unco_idntfccn_aprtnte,
				IAF.rzn_scl						= SUBSTRING(AV.rzn_scl, 1, 35)
  FROM			#tmpinformacionafiliado IAF
  INNER JOIN	BDAfiliacionValidador.dbo.tbCobranzasValidador CV WITH(NOLOCK)
  ON			CV.cnsctvo_cdgo_tpo_cntrto		= IAF.cnsctvo_cdgo_tpo_cntrto
  AND			CV.nmro_cntrto					= IAF.nmro_cntrto
  INNER JOIN	[bdafiliacionvalidador].[dbo].[tbAportanteValidador] AV WITH(NOLOCK)
  ON			AV.nmro_unco_idntfccn_aprtnte	= CV.nmro_unco_idntfccn_aprtnte
  AND			AV.cnsctvo_cdgo_clse_aprtnte	= CV.cnsctvo_cdgo_clse_aprtnte
  Where			CV.cnsctvo_cdgo_tpo_cbrnza		<> @cnsctvo_cdgo_tpo_cbrnza
  AND			CV.estdo						=  @estdo  
  AND			CV.prncpl						=  @prncpl
  AND			@fcha_actl BETWEEN CV.inco_vgnca_cbrnza AND CV.fn_vgnca_cbrnza
  
  --cnsctvo_cdgo_mrbldd
  UPDATE     iso
  SET        cnsctvo_cdgo_mrbldd = dgv.cnsctvo_cdgo_mrbldd
  FROM       #tmpinformacionsolicitud ISO
  INNER JOIN bdsisalud.dbo.tbdiagnosticos_vigencias DGV WITH (NOLOCK)
  ON         ISO.cnsctvo_cdgo_dgnstco = DGV.cnsctvo_cdgo_dgnstco
  Where      @fcha_actl BETWEEN DGV.inco_vgnca AND DGV.fn_vgnca
  
  /*Asignar valor 0 (VACIO) si la morbilidad no esta parametrizada*/
  UPDATE #tmpinformacionsolicitud
  SET	 cnsctvo_cdgo_mrbldd = 0 /*VACIO*/
  WHERE  cnsctvo_cdgo_mrbldd IS NULL 

  -- cpgo_lqudcn
  UPDATE     ise
  SET        cpgo_lqudcn = ISNULL(csp.vlr_rfrnca_cpgo, 0)
  FROM       #tmpinformacionservicios ISE
  INNER JOIN bdsisalud.dbo.tbcupsserviciosxplanes CSP WITH (NOLOCK)
  ON         ISE.cnsctvo_cdgo_srvco_slctdo = CSP.cnsctvo_prstcn
  Where      @fcha_actl BETWEEN CSP.inco_vgnca AND CSP.fn_vgnca
  And		 ISE.cnsctvo_cdgo_tpo_srvco = @cnsctvo_prstcn --CUPS

  UPDATE     ise
  SET        cpgo_lqudcn = cmp.vlr_rfrnca_cpgo
  FROM       #tmpinformacionservicios ISE
  INNER JOIN bdsisalud.dbo.tbcumsxplanes CMP WITH (NOLOCK)
  ON         ISE.cnsctvo_cdgo_srvco_slctdo = CMP.cnsctvo_cdgo_cms
  Where      @fcha_actl BETWEEN CMP.inco_vgnca AND CMP.fn_vgnca
  And        ISE.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cms --CUMS

  --cnsctvo_cdgo_estdo prestación
  UPDATE     iso
  SET        cnsctvo_cdgo_estdo = esv.cnsctvo_cdgo_estdo_hmlgdo_sprs
  FROM       #tmpinformacionservicios ISO
  INNER JOIN bdcna.prm.tbasestadosserviciossolicitados_vigencias ESV WITH (NOLOCK)
  ON         ISO.cnsctvo_cdgo_estdo_mega = ESV.cnsctvo_cdgo_estdo_srvco_slctdo
  Where      @fcha_actl BETWEEN ESV.inco_vgnca AND ESV.fn_vgnca

  --cnsctvo_prgrmcn_prstcn, cnsctvo_prgrmcn_fcha_evnto, rqre_prvdr
  --Actualización de campos necesarios para obtener los parámetro del SP.
  UPDATE          iso
  SET             cnsctvo_cdgo_ntfccn = prp.cnsctvo_cdgo_ntfccn,
                  cnsctvo_cdgo_ofcna = prp.cnsctvo_cdgo_ofcna,
                  nmro_acta = ISNULL(cre.nmro_acta, '')
  FROM            #tmpinformacionservicios ISO
  INNER JOIN      bdsisalud.dbo.tbdetprogramacionfechaevento DPE WITH (NOLOCK)
  ON              DPE.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = ISO.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
  INNER JOIN      bdsisalud.dbo.tbprogramacionprestacion PRP WITH (NOLOCK)
  ON              PRP.cnsctvo_prgrmcn_prstcn = DPE.cnsctvo_prgrmcn_prstcn
  LEFT OUTER JOIN bdsisalud.dbo.tbctcrecobro CRE WITH (NOLOCK)
  ON              CRE.cnsctvo_ntfccn = PRP.cnsctvo_cdgo_ntfccn
  AND             CRE.cnsctvo_cdgo_ofcna = PRP.cnsctvo_cdgo_ofcna

  SELECT @ttl = COUNT(1) FROM #tmpinformacionservicios
  SET @indce = 1

  WHILE @indce <= @ttl
	  BEGIN
		SELECT     @lnnmro_unco                 = IAF.nmro_unco_idntfccn_afldo,
		           @lnnmro                      = ISE.cnsctvo_cdgo_ntfccn,
		           @lccnsctvo_cdgo_ofcna_ntfccn = ISE.cnsctvo_cdgo_ofcna,
		           @lntpoidntfccnpcnte          = IAF.cnsctvo_cdgo_tpo_idntfccn_afldo,
		           @lcnmroidntfccnpcnte         = IAF.nmro_idntfccn_afldo,
		           @lcnmroacta                  = ISE.nmro_acta
		FROM       #tmpinformacionservicios ISE
		INNER JOIN #tmpinformacionafiliado IAF
		ON         ISE.cnsctvo_slctd_atrzcn_srvco = IAF.cnsctvo_slctd_atrzcn_srvco
		WHERE      ISE.id = @indce

		INSERT INTO #tmpProcedimientos (slccn                     , cdgo_prstcn               , dscrpcn_prstcn     , dscrpcn_orgn_prstcn     , dscrpcn_estdo_entrga  , 
		                                fcha_dsde                 , fcha_hsta                 , cnsctvo_orgn_prstcn, cnsctvo_prstcn          , cnsctvo_prgrmcn_prstcn, 
										cnsctvo_prgrmcn_fcha_evnto, cnsctvo_cdgo_prsntcn      , cntdd_prsntcn      , cnsctvo_cdgo_prdcdd     , fcha_inco             , 
										nmro_entrgs               , cntdd_entrgs              , cntdd_dss          , cnsctvo_cdgo_dss        , cntdd_cda             , 
										cnsctvo_cdgo_frcnca_cda   , cnsctvo_cdgo_estds_entrga , cnsctvo_cdgo_frcnca, cnsctvo_det_prgrmcn_fcha, fcha_entrga           , 
										cntdd                     , nmro_unco_idntfccn_afldo  , rqre_prvdr         , cnsctvo_cdgo_ofcna_ops  , cnsctvo_ops           , 
										cdgo_intrno_prvdr_insmo   , cnsctvo_cdgo_clsfccn_evnto, ds_mxmo_prgrmcns_ftrs_ntfccns
									   )
		EXEC bdsisalud.dbo.sppmserviciosnotificacion @lnnmro_unco,
													 @lnnmro,
													 @lccnsctvo_cdgo_ofcna_ntfccn,
													 @lntpoidntfccnpcnte,
													 @lcnmroidntfccnpcnte,
													 @lcnmroacta,
													 @lcmensaje OUTPUT

		--Se actualizan los valores cnsctvo_prgrmcn_prstcn, cnsctvo_prgrmcn_fcha_evnto, rqre_prvdr
		UPDATE     iso
		SET        cnsctvo_prgrmcn_prstcn = pro.cnsctvo_prgrmcn_prstcn,
			       cnsctvo_prgrmcn_fcha_evnto = pro.cnsctvo_prgrmcn_fcha_evnto,
				   cnsctvo_det_prgrmcn_fcha = pro.cnsctvo_det_prgrmcn_fcha,  -- qvisionclr 2017-04-10
			       rqre_prvdr = pro.rqre_prvdr
		FROM       #tmpinformacionservicios ISO
		INNER JOIN #tmpProcedimientos PRO
		ON         PRO.nmro_unco_idntfccn_afldo = @lnnmro_unco
		WHERE      ISO.id = @indce

		SET @indce = @indce + 1
		DELETE #tmpProcedimientos

	END

  --Se actualiza la información del afiliado.

  UPDATE iaf
  SET prmr_aplldo_afldo = bva.prmr_aplldo,
      sgndo_aplldo_afldo = bva.sgndo_aplldo,
      prmr_nmbre_afldo = bva.prmr_nmbre,
      sgndo_nmbre_afldo = bva.sgndo_nmbre,
      fcha_ncmnto_afldo = bva.fcha_ncmnto,
      drccn_afldo = bva.drccn_rsdnca,
      cnsctvo_cdgo_cdd_rsdnca_afldo = bva.cnsctvo_cdgo_cdd_rsdnca,
      tlfno_afldo = bva.tlfno_rsdnca,
      tlfno_cllr_afldo = bva.tlfno_rsdnca,
      crro_elctrnco_afldo = bva.eml,
      fcha_inco_vgnca = bva.inco_vgnca_bnfcro,
      fcha_fn_vgnca = bva.fn_vgnca_bnfcro
  FROM #tmpinformacionafiliado IAF
  INNER JOIN bdafiliacionvalidador.dbo.tbbeneficiariosvalidador BVA WITH (NOLOCK)
    ON BVA.cnsctvo_cdgo_tpo_cntrto = IAF.cnsctvo_cdgo_tpo_cntrto
    AND BVA.cnsctvo_bnfcro = IAF.cnsctvo_bnfcro_cntrto
    AND BVA.nmro_cntrto = IAF.nmro_cntrto
    Where @fcha_actl BETWEEN BVA.inco_vgnca_bnfcro AND BVA.fn_vgnca_bnfcro

  --cnsctvo_cdgo_prntscs
  UPDATE iaf
  SET cnsctvo_cdgo_prntscs = isnull(eap.cnsctvo_cdgo_prntscs,0)
  FROM #tmpinformacionafiliado IAF
  INNER JOIN bdafiliacionvalidador.dbo.tbeapb EAP WITH (NOLOCK)
    ON EAP.cnsctvo_cdgo_tpo_idntfccn_bnfcro = IAF.cnsctvo_cdgo_tpo_idntfccn_afldo
    AND EAP.nmro_idntfccn_bnfcro = IAF.nmro_idntfccn_afldo
    Where @fcha_actl BETWEEN EAP.inco_vgnca AND EAP.fn_vgnca

  --cnsctvo_cdgo_tpo_cbrnza
	UPDATE iaf
	SET cnsctvo_cdgo_tpo_cbrnza = cva.cnsctvo_cdgo_tpo_cbrnza
	FROM #tmpinformacionafiliado IAF
	INNER JOIN bdafiliacionvalidador.dbo.tbcobranzasvalidador CVA WITH (NOLOCK)
	ON CVA.cnsctvo_cdgo_tpo_cntrto = IAF.cnsctvo_cdgo_tpo_cntrto
	AND CVA.nmro_cntrto = IAF.nmro_cntrto
	Where @fcha_actl BETWEEN CVA.inco_vgnca_cbrnza AND CVA.fn_vgnca_cbrnza

  --Se actualiza la tabla de homologación de MEGA con las solicitudes psadas en el proceso.
  MERGE bdcna.gsa.tbashomologacionserviciosmegasipres WITH (ROWLOCK) AS target
  USING (
	  SELECT		ISE.cnsctvo_srvco_slctdo,
					ISO.cnsctvo_cdgo_ofcna,
					ISO.nuam
	  FROM			#tmpinformacionservicios ISE
	  INNER JOIN	#tmpinformacionsolicitud ISO
	  ON			ISO.id = ISE.id_agrpdr
	  And			iso.cnsctvo_slctd_atrzcn_srvco = ise.cnsctvo_slctd_atrzcn_srvco
	  Group By		ISE.cnsctvo_srvco_slctdo,
					ISO.cnsctvo_cdgo_ofcna,
					ISO.nuam
	) AS source (	cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_ofcna,
					nuam)
  ON (target.cnsctvo_srvco_slctdo = source.cnsctvo_srvco_slctdo)  
  WHEN NOT MATCHED THEN
	  INSERT (
		  cnsctvo_srvco_slctdo,		cnsctvo_cdgo_ofcna,
		  nuam,						usro_crcn,
		  fcha_crcn,				usro_ultma_mdfccn,
		  fcha_ultma_mdfccn
	  )
	  VALUES (
		  source.cnsctvo_srvco_slctdo,		source.cnsctvo_cdgo_ofcna,
		  source.nuam,						@usro,
		  @fcha_actl,						@usro,
		  @fcha_actl
	  );
  -- El nuam y la oficina no deben cambiar
  --WHEN MATCHED THEN
  --UPDATE
  --SET cnsctvo_cdgo_ofcna = source.cnsctvo_cdgo_ofcna,
  --nuam = source.nuam,
  --usro_ultma_mdfccn = @usro,
  --fcha_ultma_mdfccn = @fcha_actl

  Drop Table #tmpEmpleadores
  Drop Table #tmpProcedimientos
  Drop Table #tmpEmpleadoresxafiliado
  Drop Table #tmpTmpSolicitud

END
