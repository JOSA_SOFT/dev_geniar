USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarSolicitudSolicitudes]    Script Date: 09/12/2016 14:28:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarSolicitudSolicitudes
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de solicitudes   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P	P\>    
* Variables          : <\V  V\>    
* Fecha Creacion  	 : <\FC 07/12/2015  FC\>    
*--------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajusta la creación de variables dentro de los Procedimientos 
						para el manejo de los valores constantes? D\>  
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>     
*--------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D 
                           Se ajusta el Procedimiento para que inserte correctamente la ubicacion del paciente teniendo en cuenta la 
						   la clase de atencion
					    D\>  
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2016 FM\>     
*--------------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGrabarSolicitudSolicitudes] 
AS
  SET NOCOUNT ON
	
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN

	    Update     #Tempo_Solicitudes 
		Set        cnsctvo_cdgo_tpo_ubccn_pcnte = b.cnsctvo_cdgo_tpo_ubccn_pcnte
		From       #Tempo_Solicitudes a
		Inner Join bdCNA.prm.tbASRelUbicacionPacientexClaseAtencion_Vigencias b With(Nolock)
	    On         b.cnsctvo_cdgo_clse_atncn = a.cnsctvo_cdgo_clse_atncn
	    Where      @Fecha_Actual Between b.inco_vgnca And b.fn_vgnca 
	   

		-- INSERTAR INFORMACIÓN DE SOLICITUDES EN TABLA DE PROCESO tbASSolicitudesAutorizacionServicios 	
		INSERT INTO		BdCNA.gsa.tbASSolicitudesAutorizacionServicios  (cnsctvo_cdgo_orgn_atncn
								,cnsctvo_cdgo_tpo_srvco_slctdo
								,cnsctvo_cdgo_prrdd_atncn
								,cnsctvo_cdgo_tpo_ubccn_pcnte
								,cnsctvo_cdgo_tpo_slctd
								,jstfccn_clnca
								,cnsctvo_cdgo_mdo_cntcto_slctd
								,cnsctvo_cdgo_tpo_trnsccn_srvco_sld
								,nmro_slctd_prvdr
								,nmro_slctd_pdre
								,cdgo_eps
								,cnsctvo_cdgo_clse_atncn
								,cnsctvo_cdgo_frma_atncn
								,fcha_slctd
								,nmro_slctd_atrzcn_ss
								,cnsctvo_cdgo_estdo_slctd
								,nmro_slctd_ss_rmplzo
								,cnsctvo_slctd_srvco_sld_orgn
								,cnsctvo_cdgo_ofcna_atrzcn
								,nuam
								,srvco_hsptlzcn
								,ga_atncn
								,cnsctvo_prcso_vldcn
								,cnsctvo_prcso_mgrcn
								,mgrda_gstn
								,spra_tpe_st
								,dmi
								,cnsctvo_prcso
								,obsrvcn_adcnl
								,fcha_crcn
								,usro_crcn
								,fcha_ultma_mdfccn
								,usro_ultma_mdfccn)		
		OUTPUT			INSERTED.cnsctvo_slctd_atrzcn_srvco	
						,Convert(Int,INSERTED.nmro_slctd_atrzcn_ss)
		INTO	#Idsolicitudes 		
		SELECT	TSO.cnsctvo_cdgo_orgn_atncn		
				,TSO.cnsctvo_cdgo_tpo_srvco_slctdo
				,TSO.cnsctvo_cdgo_prrdd_atncn			
				,TSO.cnsctvo_cdgo_tpo_ubccn_pcnte		
				,TSO.cnsctvo_cdgo_tpo_slctd				
				,TSO.jstfccn_clnca						
				,TSO.cnsctvo_cdgo_mdo_cntcto_slctd		
				,TSO.cnsctvo_cdgo_tpo_trnsccn_srvco_sld	
				,TSO.nmro_slctd_prvdr					
				,TSO.nmro_slctd_pdre					
				,TSO.cdgo_eps							
				,TSO.cnsctvo_cdgo_clse_atncn			
				,TSO.cnsctvo_cdgo_frma_atncn			
				,TSO.fcha_slctd							
				,TSO.nmro_slctd_atrzcn_ss	
				,0 --cnsctvo_cdgo_estdo_slctd			
				,TSO.nmro_slctd_ss_rmplzo			
				,0 --cnsctvo_slctd_srvco_sld_orgn	
				,TSO.cnsctvo_cdgo_ofcna_atrzcn			
				,TSO.nuam								
				,TSO.srvco_hsptlzcn						
				,TSO.ga_atncn							
				,TSO.cnsctvo_prcso_vldcn				
				,TSO.cnsctvo_prcso_mgrcn				
				,TSO.mgrda_gstn	
				,TSO.spra_tpe_st
				,TSO.dmi
				,0 --cnsctvo_prcso
				,TSO.obsrvcn_adcnl
				,@Fecha_Actual	--fcha_crcn
				,TSO.usro_crcn		
				,@Fecha_Actual	--fcha_ultma_mdfccn	
				,TSO.usro_crcn	--usro_ultma_mdfccn				
		FROM	#Tempo_Solicitudes TSO WITH (NOLOCK)
		

		-- INSERTAR INFORMACIÓN DE SOLICITUDES EN TABLA ORIGINAL tbASSolicitudesAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASSolicitudesAutorizacionServiciosOriginal(
					 cnsctvo_slctd_atrzcn_srvco
					,cdgo_orgn_atncn
					,cdgo_tpo_srvco_slctdo
					,cdgo_prrdd_atncn
					,cdgo_tpo_ubccn_pcnte
					,jstfccn_clnca
					,nmro_slctd_prvdr
					,nmro_slctd_pdre
					,cdgo_eps
					,cdgo_clse_atncn
					,cdgo_frma_atncn
					,fcha_slctd
					,nmro_slctd_atrzcn_ss
					,nmro_slctd_ss_rmplzo
					,nuam
					,srvco_hsptlzcn
					,ga_atncn
					,mgrda_gstn
					,cdgo_mdo_cntcto_slctd
					,cdgo_tpo_trnsccn_srvco_sld
					,cdgo_estdo_slctd
					,obsrvcn_adcnl
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)		
		SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco
					,SOL.cdgo_orgn_atncn	
					,SOL.cdgo_tpo_srvco_slctdo
					,SOL.cdgo_prrdd_atncn		
					,SOL.cdgo_tpo_ubccn_pcnte	
					,SOL.jstfccn_clnca
					,SOL.nmro_slctd_prvdr
					,SOL.nmro_slctd_pdre
					,SOL.cdgo_eps
					,SOL.cdgo_clse_atncn		
					,SOL.cdgo_frma_atncn	
					,SOL.fcha_slctd	
					,SOL.nmro_slctd_atrzcn_ss
					,SOL.nmro_slctd_ss_rmplzo
					,SOL.nuam
					,SOL.srvco_hsptlzcn
					,SOL.ga_atncn
					,SOL.mgrda_gstn
					,SOL.cdgo_mdo_cntcto_slctd	
					,SOL.cdgo_tpo_trnsccn_srvco_sld	
					,''				--cdgo_estdo_slctd	
					,SOL.obsrvcn_adcnl
					,@Fecha_Actual	--fcha_crcn
					,SOL.usro_crcn		
					,@Fecha_Actual	--fcha_ultma_mdfccn	
					,SOL.usro_crcn	--usro_ultma_mdfccn	
		FROM		#Tempo_Solicitudes	SOL WITH (NOLOCK)
		INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
		on			IDS.IdXML = SOL.id	
	
	END
