USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionAdicionarPrestacion]    Script Date: 26/07/2017 08:51:17 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionAdicionarPrestacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  A\>
* Descripcion			: <\D 	
                              Procedimiento que permite guardar informacion adicional de las prestaciones desde el auditor
					      D\>				
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/01/18 FC\>
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                 Se modifica el procedimiento con el fin de que consulte las prestaciones tenienedo en cuenta
								 el tipo de codificacion
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/01/25 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones		DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Rairez AM\>
* Descripcion			 : <\DM	Se ajusta sp para recuperar y guardar la informacion del recobro en la prestacion DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/07/26	FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASGuardarInformacionAdicionarPrestacion] 
	@lncnsctvo_tpo_prstcn				udtConsecutivo,
	@lncdgo_cdfccn						char(11),
	@lnltrldd							varchar(15),
	@lnva_accso							varchar(15),
	@cntdd_slctda						int,
	@dss								float,
	@cnsctvo_cdgo_prsntcn_dss			udtConsecutivo,
	@prdcdd_dss							float,
	@cnsctvo_cdgo_undd_prdcdd_dss		udtConsecutivo,
	@lncnsctvo_cdgo_no_cnfrmdd_vldcn	udtConsecutivo,
	@lnobsrvcn_ops						udtObservacion,
	@lnmro_slctd_prvdr					udtConsecutivo,
	@lcusro_crcn						udtUsuario,
	@cdgo_rsltdo						udtConsecutivo  output,
	@mnsje_rsltdo						udtDescripcion	output
AS
BEGIN

	SET NOCOUNT ON;
	Declare	@mensaje_ok							udtDescripcion = 'Proceso ejecutado satisfactoriamente',
			@mensaje_error						udtDescripcion = 'No existe Fecha Entrega para la Prestación',
			@codigo_ok							int			   = 0,
			@codigo_error						int			   = -1,
			@ldFenchaGestion					datetime,
			@lndscrpcn_cdfccn					udtDescripcion,
			@lncnsctvo_cdgo_srvco_slctdo		udtConsecutivo,
			@ValorCero							int = 0,
			@valorVacio							char(1) = '',
			@cnsctvo_cdgo_estdo_srvco			udtConsecutivo,
			@lncnsctvo_srvco_slctdo_nw			udtConsecutivo,
			@cums								int = 5,
			@cups								int = 4,
			@lncnsctvo_cncpto_prcdmnto_slctdo	udtConsecutivo,
			@ldOrgn_mdfccn						udtdescripcion,
			@ip									varchar(10) = '127.0.0.1',
			@elmnto_orgn_mdfccn					udtdescripcion,
			@dto_elmnto_orgn_mdfccn				udtdescripcion,
			@dscrpcn_vlr_antrr					udtdescripcion,
			@cnsctvo_cdgo_rcbro					udtConsecutivo,
			@cdgo_rcbro							Char(3),
			@cnsctvo_cdgo_tpo_dgnstco			udtConsecutivo;				
	
	Set @ldFenchaGestion				= getDate();
	Set @cnsctvo_cdgo_estdo_srvco		= 2; --Estado Auditoria Malla
	Set @ldOrgn_mdfccn					= 'tbASServiciosSolicitados.cnsctvo_cdgo_estdo_srvco_slctdo';
	Set @elmnto_orgn_mdfccn				= 'Solicitud';
	Set	@dto_elmnto_orgn_mdfccn			= 'Adicionar Prestación';
	Set @cnsctvo_cdgo_tpo_dgnstco		= 1;

	
	DECLARE @tmpPrestaciones table (cnsctvo_srvco_slctdo	udtConsecutivo)
	DECLARE	@tmpProcedimientosMedicamentos table(cnsctvo_insmo_slctdo	udtConsecutivo)

	CREATE TABLE #tmpTrazaModificacion (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo  udtConsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
		fcha_mdfccn						datetime,
		orgn_mdfccn						varchar(100),
		vlr_antrr						varchar(20),
		vlr_nvo							varchar(20),
		usro_mdfccn						udtUsuario,
		obsrvcns						udtObservacion,
		nmro_ip							varchar(12),
		fcha_crcn						datetime,
		usro_crcn						udtUsuario,
		fcha_ultma_mdfccn				datetime,
		usro_ultma_mdfccn				udtUsuario,
		elmnto_orgn_mdfccn				udtDescripcion,
		dto_elmnto_orgn_mdfccn			udtDescripcion,
		dscrpcn_vlr_antrr				udtDescripcion,
		dscrpcn_vlr_nvo					udtDescripcion
	)

	Set @mnsje_rsltdo = @mensaje_ok;
	Set @cdgo_rsltdo  = @codigo_ok;
	
	 BEGIN TRY
		
		SELECT @lndscrpcn_cdfccn			= dscrpcn_cdfccn,
			   @lncnsctvo_cdgo_srvco_slctdo = cnsctvo_cdfccn 
		FROM   bdSisalud.dbo.tbCodificaciones With(NoLock)
		Where  cdgo_cdfccn = @lncdgo_cdfccn
		And    cnsctvo_cdgo_tpo_cdfccn = @lncnsctvo_tpo_prstcn

		-- Se recupera la informacion del recobro.
		Select		@cnsctvo_cdgo_rcbro = a.cnsctvo_cdgo_rcbro,
					@cdgo_rcbro = b.cdgo_rcbro
		From		bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios a With(NoLock)
		Inner Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal b With(NoLock)
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_dgnstco_slctd_atrzcn_srvco = a.cnsctvo_dgnstco_slctd_atrzcn_srvco
		Where		a.cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr
		And			a.cnsctvo_cdgo_tpo_dgnstco = @cnsctvo_cdgo_tpo_dgnstco

		-- 		
		Insert Into BDCna.gsa.tbASServiciosSolicitados(
			cnsctvo_cdgo_srvco_slctdo				,cnsctvo_cdgo_tpo_srvco		,dscrpcn_srvco_slctdo			,					
			cntdd_slctda							,fcha_prstcn_srvco_slctdo	,tmpo_trtmnto_slctdo			,						
			cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	,vlr_lqdcn_srvco			,cnsctvo_srvco_slctdo_orgn		,				
			cntdd_atrzda							,cnsctvo_cdgo_tpo_atrzcn	,cnsctvo_cdgo_estdo_srvco_slctdo,			
			cnsctvo_cdgo_prstcn_prstdr				,crgo_drccnmnto				,indcdr_no_ps					,		
			prstcn_vgnte							,prstcn_cptda				,prgrmcn_entrga					,		
			cnfrmcn_cldd							,fcha_ultma_mdfccn			,usro_crcn						,		
			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto		,fcha_imprsn												,
			fcha_crcn								,usro_ultma_mdfccn			,cnsctvo_slctd_atrzcn_srvco		,
			nmro_atrzcn_srvco						,cnsctvo_cdgo_rcbro					
			
		)Output inserted.cnsctvo_srvco_slctdo into @tmpPrestaciones 
		Values (
			@lncnsctvo_cdgo_srvco_slctdo			,@lncnsctvo_tpo_prstcn		,@lndscrpcn_cdfccn				,
			@cntdd_slctda							,@ldFenchaGestion			,@ValorCero						,
			@ValorCero								,@ValorCero					,@ValorCero						,
			@ValorCero								,@ValorCero					,@cnsctvo_cdgo_estdo_srvco		,
			@ValorCero								,@ValorCero					,''								,
			''										,''							,''								,
			''										,@ldFenchaGestion			,@lcusro_crcn					,
			null									,null														,						
			@ldFenchaGestion						,@lcusro_crcn				,@lnmro_slctd_prvdr				,
			@ValorCero								,@cnsctvo_cdgo_rcbro		

		) 

		Select  @lncnsctvo_srvco_slctdo_nw = cnsctvo_srvco_slctdo 
		From	@tmpPrestaciones
		
		-- 
		Insert Into BDCna.gsa.tbASServiciosSolicitadosOriginal(
			cnsctvo_srvco_slctdo					,cnsctvo_slctd_atrzcn_srvco		,cdgo_tpo_srvco					,					
			cdgo_srvco_slctdo						,dscrpcn_srvco_slctdo			,cntdd_slctda					,						
			fcha_prstcn_srvco_slctdo				,tmpo_trtmnto_slctdo			,cdgo_undd_tmpo_trtmnto_slctdo	,				
			vlr_lqdcn_srvco							,cdgo_prstcn_prstdr				,cdgo_tpo_atrzcn				,			
			fcha_crcn								,usro_crcn						,fcha_ultma_mdfccn				,		
			usro_ultma_mdfccn						,cdgo_rcbro						
		)Values(
			@lncnsctvo_srvco_slctdo_nw				,@lnmro_slctd_prvdr				,@valorVacio					,
			@lncdgo_cdfccn							,@lndscrpcn_cdfccn				,@cntdd_slctda					,
			@ldFenchaGestion						,@ValorCero						,@ValorCero						,
			@ValorCero								,@valorVacio					,@valorVacio					,
			@ldFenchaGestion						,@lcusro_crcn					,@ldFenchaGestion				,
			@lcusro_crcn							,@cdgo_rcbro
		)


		If @lncnsctvo_tpo_prstcn = @cums
		Begin
			Insert into BDCna.gsa.tbASMedicamentosSolicitados(
				cnsctvo_srvco_slctdo				,dss							,cnsctvo_cdgo_prsntcn_dss			,
				prdcdd_dss							,cnsctvo_cdgo_undd_prdcdd_dss	,cnsctvo_cdgo_frma_frmctca			,
				cnsctvo_cdgo_grpo_trptco			,cnsctvo_cdgo_prsntcn			,cnsctvo_cdgo_va_admnstrcn_mdcmnto	,
				cnsctvo_mdcmnto_slctdo_orgn			,cncntrcn_dss					,prsntcn_dss						,
				fcha_vncmnto_invma					,mstra_mdca						,uso_cndcndo						,
				mss_mxmo_rnvcn						,cnsctvo_cdgo_undd_cncntrcn_dss	,fcha_crcn						    ,
				usro_crcn							,fcha_ultma_mdfccn				,usro_ultma_mdfccn				
			)Output inserted.cnsctvo_mdcmnto_slctdo into @tmpProcedimientosMedicamentos
			Values(
				@lncnsctvo_srvco_slctdo_nw			,@dss							,@cnsctvo_cdgo_prsntcn_dss			,
				@prdcdd_dss							,@cnsctvo_cdgo_undd_prdcdd_dss	,@lncnsctvo_cdgo_srvco_slctdo		,
				@ValorCero							,@ValorCero						,@ValorCero							,
				@ValorCero							,@ValorCero						,@ValorCero							,
				null								,''								,''									,
				@ValorCero							,@ValorCero						,@ldFenchaGestion					,
				@lcusro_crcn						,@ldFenchaGestion				,@lcusro_crcn	
			)
		End	

		If @lncnsctvo_tpo_prstcn = @cups
		Begin
			Insert Into BDCna.gsa.tbASProcedimientosInsumosSolicitados(
				cnsctvo_srvco_slctdo		,cnsctvo_cdgo_ltrldd			,cnsctvo_cdgo_va_accso		,
				undd_cntdd					,cntdd_mxma						,frcnca_entrga				,
				fcha_crcn					,usro_crcn						,fcha_ultma_mdfccn			,
				usro_ultma_mdfccn			,accso_drcto
			)Output inserted.cnsctvo_prcdmnto_insmo_slctdo into @tmpProcedimientosMedicamentos
			Values(
				@lncnsctvo_srvco_slctdo_nw	,@lnltrldd						,@lnva_accso				,
				@cntdd_slctda				,@ValorCero						,null						,
				@ldFenchaGestion			,@lcusro_crcn					,@ldFenchaGestion			,
				@lcusro_crcn				,''
			)


		ENd

		Select  @lncnsctvo_cncpto_prcdmnto_slctdo = cnsctvo_insmo_slctdo 
		From	@tmpProcedimientosMedicamentos
		
		Select @dscrpcn_vlr_antrr = e.dscrpcn_estdo_srvco_slctdo
		From   BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias e With(NoLock)
		Where  e.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco

		Insert Into #tmpTrazaModificacion 
		(cnsctvo_slctd_atrzcn_srvco,	  cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
		 cnsctvo_cdgo_mtvo_csa,			  fcha_mdfccn,						orgn_mdfccn,					
		 vlr_antrr,						  vlr_nvo,							usro_mdfccn,					
		 obsrvcns,						  nmro_ip,							fcha_crcn,						
		 usro_crcn,						  fcha_ultma_mdfccn,				usro_ultma_mdfccn,
		 elmnto_orgn_mdfccn,			  dto_elmnto_orgn_mdfccn,			dscrpcn_vlr_antrr,	
		 dscrpcn_vlr_nvo
		)
		Values(
		 @lnmro_slctd_prvdr					, @lncnsctvo_srvco_slctdo_nw		,@lncnsctvo_cncpto_prcdmnto_slctdo	,
		 @lncnsctvo_cdgo_no_cnfrmdd_vldcn	, @ldFenchaGestion					,@ldOrgn_mdfccn						,				
		 @cnsctvo_cdgo_estdo_srvco			, @cnsctvo_cdgo_estdo_srvco			,@lcusro_crcn						,
		 @lnobsrvcn_ops						, @ip								,@ldFenchaGestion					,
		 @lcusro_crcn						, @ldFenchaGestion					,@lcusro_crcn						,
		 @elmnto_orgn_mdfccn				, @dto_elmnto_orgn_mdfccn			,@dscrpcn_vlr_antrr					,
		 @dscrpcn_vlr_antrr
		)
		                 							  
	    Exec BDCna.gsa.spASGuardarTrazaModificacion;

	 END TRY
	 BEGIN CATCH
		SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
			   				 'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
							 'Message:' + ERROR_MESSAGE() + CHAR(13) +
							 'Procedure:' + ERROR_PROCEDURE();
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error;
	END CATCH

	 Drop table #tmpTrazaModificacion;
END

