USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionTareaProceso]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [gsa].[spASGuardarInformacionTareaProceso] 
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
	@id_prcso					varchar(50),
	@cnsctvo_tra				udtConsecutivo,
	@nmbre_tra					udtDescripcion,
	@usro_tra					udtUsuario,
	@tpo_accn					udtLogico,
	@cnsctvo_cdgo_idntfccn		udtConsecutivo,
	@nmro_indtfccn_afldo		udtNumeroIdentificacionLargo,
	@tpo_tra					int,
	@nmbre_grpo_rsgo			udtDescripcion
AS
BEGIN

	SET NOCOUNT ON;
	Declare @Nui_Afldo							udtConsecutivo,
			@ldfcha_consulta					datetime,
			@mensaje_error						varchar(2000);

	Set @ldfcha_consulta = GetDate();

	Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_idntfccn, @nmro_indtfccn_afldo);
	
	 --BEGIN TRANSACTION 
		BEGIN TRY	
			Insert Into BDCna.gsa.tbASHistoricoTareasxInstancia(
				  cnsctvo_slctd_atrzcn_srvco,
				  id_prcso,
				  cnsctvo_tra,
				  nmbre_tra,
				  fcha_tra,
				  usro_tra,
				  tpo_accn,
				  nmro_unco_idntfccn_afldo,
				  cnsctvo_cdgo_tpo_idntfccn,
				  nmro_idntfccn_afldo,
				  fcha_crcn,
				  usro_crcn,
				  fcha_ultma_mdfccn,
				  usro_ultma_mdfccn,
				  tpo_tra,
				  nmbre_grpo_rsgo
			) Values (
				@cnsctvo_slctd_atrzcn_srvco,
				@id_prcso,
				@cnsctvo_tra,
				@nmbre_tra,
				@ldfcha_consulta,
				@usro_tra,
				@tpo_accn,
				@Nui_Afldo,
				@cnsctvo_cdgo_idntfccn,
				@nmro_indtfccn_afldo,
				@ldfcha_consulta,
				@usro_tra,
				@ldfcha_consulta,
				@usro_tra,
				@tpo_tra,
				@nmbre_grpo_rsgo
			)
			--COMMIT
		END TRY
		BEGIN CATCH
			--ROLLBACK 
			SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
				   				 'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
								 'Message:' + ERROR_MESSAGE() + CHAR(13) +
								 'Procedure:' + ERROR_PROCEDURE();
		END CATCH
    
END

GO
