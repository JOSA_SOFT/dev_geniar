USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarUsuarioPrestacionAltoCosto]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarUsuarioPrestacionAltoCosto
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida si la prestación o el usuario es de alto costo	 D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarUsuarioPrestacionAltoCosto] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@dscrpcn_grpo_entrga udtDescripcion,
@agrpa_prstcn udtLogico
AS
  SET NOCOUNT ON

  DECLARE @fcha_actl datetime
  SET @fcha_actl = GETDATE()

  BEGIN

    DECLARE @tbTempConsecutivos TABLE (
      cnsctvo_cdfccn udtConsecutivo
    );

    DECLARE @itmPresupuesto1 udtConsecutivo,
            @itmPresupuesto2 udtConsecutivo,
            @itmPresupuesto3 udtConsecutivo,
            @itmPresupuesto4 udtConsecutivo,
            @itmPresupuesto5 udtConsecutivo,
            @itmPresupuesto6 udtConsecutivo,
            @itmPresupuesto7 udtConsecutivo,
            @itmPresupuesto8 udtConsecutivo,
            @itmPresupuesto9 udtConsecutivo,
            @itmPresupuesto10 udtConsecutivo,
            @afirmacion char(1)
    SELECT
      @itmPresupuesto1 = 101,
      @itmPresupuesto2 = 102,
      @itmPresupuesto3 = 109,
      @itmPresupuesto4 = 110,
      @itmPresupuesto5 = 171,
      @itmPresupuesto6 = 103,
      @itmPresupuesto7 = 71,
      @itmPresupuesto8 = 108,
      @itmPresupuesto9 = 100,
      @itmPresupuesto10 = 172,
      @afirmacion = 'S'

    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga IS NULL)
    BEGIN

      --Se obtienen las prestaciones que sean de alto riesgo.
      INSERT INTO @tbTempConsecutivos (cnsctvo_cdfccn)
        SELECT
          cnsctvo_prstcn
        FROM bdSisalud.dbo.tbCupsServicios c WITH (NOLOCK)
        INNER JOIN bdSisalud.dbo.tbItemsPresupuesto i WITH (NOLOCK)
          ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
        WHERE i.dscrpcn_itm_prspsto IS NOT NULL
        AND i.cnsctvo_cdgo_itm_prspsto IN (@itmPresupuesto1, @itmPresupuesto2, @itmPresupuesto3, @itmPresupuesto4, @itmPresupuesto5,
        @itmPresupuesto6, @itmPresupuesto7, @itmPresupuesto8, @itmPresupuesto9, @itmPresupuesto10)
        UNION ALL
        SELECT
          cnsctvo_cms
        FROM bdSisalud.dbo.tbCums AS c WITH (NOLOCK)
        INNER JOIN bdSisalud.dbo.tbItemsPresupuesto AS i WITH (NOLOCK)
          ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
        WHERE i.cnsctvo_cdgo_itm_prspsto IS NOT NULL
        AND i.cnsctvo_cdgo_itm_prspsto IN (@itmPresupuesto1, @itmPresupuesto2, @itmPresupuesto3, @itmPresupuesto4, @itmPresupuesto5,
        @itmPresupuesto6, @itmPresupuesto7, @itmPresupuesto8, @itmPresupuesto9, @itmPresupuesto10)
        UNION ALL
        SELECT
          cnsctvo_prstcn_pis
        FROM bdsisalud.dbo.tbPrestacionPis AS c WITH (NOLOCK)
        INNER JOIN bdsisalud.dbo.tbItemsPresupuesto AS i WITH (NOLOCK)
          ON i.cnsctvo_cdgo_itm_prspsto = c.cnsctvo_cdgo_itm_prspsto
        WHERE i.cnsctvo_cdgo_itm_prspsto IS NOT NULL
        AND i.cnsctvo_cdgo_itm_prspsto IN (@itmPresupuesto1, @itmPresupuesto2, @itmPresupuesto3, @itmPresupuesto4, @itmPresupuesto5,
        @itmPresupuesto6, @itmPresupuesto7, @itmPresupuesto8, @itmPresupuesto9, @itmPresupuesto10)

      --Se actualiza al grupo en caso de que la prestación sea de alto riesgo.
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga IS NULL
      AND sol.cnsctvo_cdfccn IN (SELECT
        cnsctvo_cdfccn
      FROM @tbTempConsecutivos);

      --Se actualiza al grupo en caso de que el afiliado sea de alto riesgo.
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN BdRiesgosSalud.dbo.tbReporteCohortesxAfiliado rca --Cohorte por afiliado
        ON rca.nmro_unco_idntfccn_afldo = sol.nmro_idntfccn_afldo
      INNER JOIN bdRiesgosSalud.dbo.tbHistoricoEstadosReporteCohortesxAfiliado hrca
        ON rca.cnsctvo_rprte_chrte_x_afldo = hrca.cnsctvo_rprte_chrte_x_afldo
        AND hrca.vldo = @afirmacion
        AND @fcha_actl BETWEEN hrca.inco_vgnca_estdo AND hrca.fn_vgnca_estdo
      WHERE sol.cnsctvo_grpo_entrga IS NULL;

	  --Se determina si se debe realizar agrupación
	  IF EXISTS(SELECT
				sol.id
			  FROM #tmpDatosSolicitudFechaEntrega sol
			  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
					AND @agrpa_prstcn = @afirmacion)
		BEGIN
			UPDATE #tmpDatosSolicitudFechaEntrega
		    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
			    cdgo_grpo_entrga = @dscrpcn_grpo_entrga
		    FROM #tmpDatosSolicitudFechaEntrega sol
				 WHERE sol.cnsctvo_slctd_srvco_sld_rcbda IN (SELECT
												sol.cnsctvo_slctd_srvco_sld_rcbda 
											  FROM #tmpDatosSolicitudFechaEntrega sol
											  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga)

		END
    END
  END


GO
