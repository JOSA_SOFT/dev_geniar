Use bdCNA
Go


If(OBJECT_ID('gsa.spASObtenerPrestacionesAsociadasOPSWeb') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASObtenerPrestacionesAsociadasOPSWeb As Select 1';
End

Go

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASObtenerPrestacionesAsociadasOPSWeb
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez  A\>
* Descripcion        : <\D 
                           Carga la informacion de las OPS de prestaciones relacionadas por algun 
						   concepto. 
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           @cnsctvo_slctd_atrzcn_srvco
						   @cnsctvo_srvco_slctdo
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 12/07/2017 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*--------------------------------------------------------------------------------------------------------*/


Alter Procedure gsa.spASObtenerPrestacionesAsociadasOPSWeb
		@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		@cnsctvo_srvco_slctdo			udtConsecutivo
As
Begin

	Declare @cnsctvo_cdgo_estdo_atrzda		udtConsecutivo,
			@cnsctvo_cdgo_estdo_entrgda		udtConsecutivo,
			@fcha_actl						Date;

	Create Table #tempServiciosAsociadosxOPS
	(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
		cnsctvo_mdcmnto_slctdo			udtConsecutivo
	)

	Set @cnsctvo_cdgo_estdo_atrzda  = 11;
	Set @cnsctvo_cdgo_estdo_entrgda = 15;
	Set @fcha_actl				    = getDate();

	-- Se obtienen las prestaciones cuyos conceptos comparten un mismo numero de OPS
	-- Excepto la prestacion de la cual se solicito.
	Insert Into #tempServiciosAsociadosxOPS
	(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,	cnsctvo_prcdmnto_insmo_slctdo
	)

	Select		ss.cnsctvo_slctd_atrzcn_srvco, ss.cnsctvo_srvco_slctdo, pis.cnsctvo_prcdmnto_insmo_slctdo
	From		#tempInfoOPS i
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.nmro_unco_ops = i.nmro_unco_ops
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	Where		ss.cnsctvo_srvco_slctdo != @cnsctvo_srvco_slctdo
	And			ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And        (css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzda  Or 
				 css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_entrgda )
	
	Insert Into #tempServiciosAsociadosxOPS
	(
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,	cnsctvo_mdcmnto_slctdo
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco, ss.cnsctvo_srvco_slctdo, ms.cnsctvo_mdcmnto_slctdo
	From		#tempInfoOPS i
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.nmro_unco_ops = i.nmro_unco_ops
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo= css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	Where		ss.cnsctvo_srvco_slctdo != @cnsctvo_srvco_slctdo
	And			ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And        ( css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzda Or 
				 css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_entrgda )

	-- Se obtienen los numeros de OPS de las prestaciones asociadas.
	-- Se excluyen las que ya fueron cargadas.
	Insert Into #tempInfoOPS
	(
				fcha_utlzcn_dsde          ,		fcha_utlzcn_hsta,		cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                dscrpcn_estdo_srvco_slctdo,		nmro_unco_ops
	)
	Select		css.fcha_utlzcn_dsde      ,		css.fcha_utlzcn_hsta,	css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                e.dscrpcn_estdo_srvco_slctdo,	css.nmro_unco_ops
	From		#tempServiciosAsociadosxOPS sao
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			sao.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
    On			e.cnsctvo_cdgo_estdo_srvco_slctdo = css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
	Left Join	#tempInfoOPS i
	On			i.nmro_unco_ops = css.nmro_unco_ops
	Where		@fcha_actl Between e.inco_vgnca And e.fn_vgnca
	And			i.nmro_unco_ops Is Null
	Group By	css.fcha_utlzcn_dsde      ,		css.fcha_utlzcn_hsta,	css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                e.dscrpcn_estdo_srvco_slctdo,	css.nmro_unco_ops

	Insert Into #tempInfoOPS
	(
				fcha_utlzcn_dsde          ,		fcha_utlzcn_hsta,		cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                dscrpcn_estdo_srvco_slctdo,		nmro_unco_ops
	)
	Select		css.fcha_utlzcn_dsde      ,		css.fcha_utlzcn_hsta,	css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                e.dscrpcn_estdo_srvco_slctdo,	css.nmro_unco_ops
	From		#tempServiciosAsociadosxOPS sao
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			sao.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
    On			e.cnsctvo_cdgo_estdo_srvco_slctdo = css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
	Left Join	#tempInfoOPS i
	On			i.nmro_unco_ops = css.nmro_unco_ops
	Where		@fcha_actl Between e.inco_vgnca And e.fn_vgnca
	And			i.nmro_unco_ops Is Null
	Group By	css.fcha_utlzcn_dsde      ,		css.fcha_utlzcn_hsta,	css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                e.dscrpcn_estdo_srvco_slctdo,	css.nmro_unco_ops
	
	Drop Table #tempServiciosAsociadosxOPS

End