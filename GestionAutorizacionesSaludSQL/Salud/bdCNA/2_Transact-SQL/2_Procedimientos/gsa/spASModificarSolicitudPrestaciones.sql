USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudPrestaciones]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudPrestaciones
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Prestaciones como 
					 :	modificación de una solicitud especifica D\>  
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\> 
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASModificarSolicitudPrestaciones] 
AS
  SET NOCOUNT ON
	PRINT('ENTRE PRESTACION')
	DECLARE @Fecha_Actual as datetime
	DECLARE @Medicamento as Int
	DECLARE @Procedimiento as Int
	SELECT	@Fecha_Actual = Getdate()
			,@Medicamento = 5
			,@Procedimiento = 4
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASProcedimientosInsumosSolicitadosOriginal
		DELETE			PIO
		FROM			BdCNA.gsa.tbASProcedimientosInsumosSolicitadosOriginal PIO
		INNER JOIN		#ServiciosMod	SEM
		ON				PIO.cnsctvo_dto_srvco_slctdo_orgnl = SEM.cnsctvo_dto_srvco_slctdo_orgnl
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASProcedimientosInsumosSolicitados
		DELETE			PIP
		FROM			BdCNA.gsa.tbASProcedimientosInsumosSolicitados PIP
		INNER JOIN		#ServiciosMod	SEM
		ON				PIP.cnsctvo_srvco_slctdo = SEM.cnsctvo_srvco_slctdo
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASMedicamentosSolicitadosOriginal
		DELETE			MCO
		FROM			BdCNA.gsa.tbASMedicamentosSolicitadosOriginal MCO
		INNER JOIN		#ServiciosMod	SEM
		ON				MCO.cnsctvo_dto_srvco_slctdo_orgnl = SEM.cnsctvo_dto_srvco_slctdo_orgnl
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASMedicamentosSolicitados
		DELETE			MCP
		FROM			BdCNA.gsa.tbASMedicamentosSolicitados  MCP
		INNER JOIN		#ServiciosMod SEM
		ON				MCP.cnsctvo_srvco_slctdo = SEM.cnsctvo_srvco_slctdo
		--ELIMINAR REGISTROS EXISTENTES TABLA ORIGINAL tbASServiciosSolicitadosOriginal
		DELETE			SEO
		FROM			BdCNA.gsa.tbASServiciosSolicitadosOriginal SEO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				SEO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		--ELIMINAR REGISTROS EXISTENTES TABLA PROCESO tbASHistoricoEstadosServicioSolicitado
		DELETE			HES
		FROM			BdCNA.gsa.tbASHistoricoEstadosServicioSolicitado HES
		INNER JOIN		#tmp_tbASHistoricoEstadosServicioSolicitado	THE
		ON				HES.cnsctvo_hstrco_estdo_srvco_slctd = THE.cnsctvo_hstrco_estdo_srvco_slctd
		--ELIMINAR REGISTROS EXISTENTES TABLA PROCESO tbASServiciosSolicitados
		DELETE			SEP
		FROM			BdCNA.gsa.tbASServiciosSolicitados SEP
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				SEP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo		
		-- INSERTAR INFORMACIÓN DE PRESTACIONES EN TABLA DE PROCESO tbASServiciosSolicitados 	
		MERGE INTO			 BdCNA.gsa.tbASServiciosSolicitados
		USING (SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
							,TPR.id					
							,TPR.cnsctvo_cdgo_srvco_slctdo
							,TPR.cnsctvo_cdgo_tpo_srvco		
							,TPR.dscrpcn_srvco_slctdo		
							,TPR.cntdd_slctda							
							,TPR.fcha_prstcn_srvco_slctdo				
							,TPR.tmpo_trtmnto_slctdo					
							,TPR.cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	
							,TPR.vlr_lqdcn_srvco						
							,TPR.cnsctvo_srvco_slctdo_orgn				
							,TPR.cnsctvo_cdgo_tpo_atrzcn				
							,TPR.cnsctvo_cdgo_prstcn_prstdr				
							,TPR.cnfrmcn_cldd							
							,TPR.usro_crcn		
							,TPR.nmro_prstcn						
				FROM		#TMod_Prestaciones	TPR WITH (NOLOCK)		
				INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
				ON			TPR.id = IDS.id) AS PRE
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,cnsctvo_cdgo_srvco_slctdo
				,cnsctvo_cdgo_tpo_srvco
				,dscrpcn_srvco_slctdo
				,cntdd_slctda
				,fcha_prstcn_srvco_slctdo
				,tmpo_trtmnto_slctdo
				,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo
				,vlr_lqdcn_srvco
				,cnsctvo_srvco_slctdo_orgn
				,cntdd_atrzda
				,nmro_atrzcn_srvco
				,cnsctvo_cdgo_tpo_atrzcn
				,cnsctvo_cdgo_estdo_srvco_slctdo
				,cnsctvo_cdgo_prstcn_prstdr
				,crgo_drccnmnto
				,indcdr_no_ps
				,prstcn_vgnte
				,prstcn_cptda
				,prgrmcn_entrga
				,cnfrmcn_cldd
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn
			)
		VALUES ( PRE.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,PRE.cnsctvo_cdgo_srvco_slctdo
				,PRE.cnsctvo_cdgo_tpo_srvco		
				,PRE.dscrpcn_srvco_slctdo		
				,PRE.cntdd_slctda							
				,PRE.fcha_prstcn_srvco_slctdo				
				,PRE.tmpo_trtmnto_slctdo					
				,PRE.cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	
				,PRE.vlr_lqdcn_srvco						
				,PRE.cnsctvo_srvco_slctdo_orgn	
				,0				--cntdd_atrzda	
				,0				--nmro_atrzcn_srvco		
				,PRE.cnsctvo_cdgo_tpo_atrzcn	
				,0				--cnsctvo_cdgo_estdo_srvco_slctdo			
				,PRE.cnsctvo_cdgo_prstcn_prstdr	
				,0				--crgo_drccnmnto	
				,''				--indcdr_no_ps	
				,''				--prstcn_vgnte
				,''				--prstcn_cptda
				,''				--prgrmcn_entrga
				,PRE.cnfrmcn_cldd	
				,@Fecha_Actual	--fcha_crcn						
				,PRE.usro_crcn
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,PRE.usro_crcn 	--usro_ultma_mdfccn		
				)
		OUTPUT	inserted.cnsctvo_srvco_slctdo
				,PRE.id
				,PRE.cnsctvo_cdgo_tpo_srvco
				,PRE.nmro_prstcn
		INTO	#IdServicios_Mod; 
		-- INSERTAR INFORMACIÓN DE PRESTACIONES EN TABLA DE PROCESO tbASHistoricoEstadosServicioSolicitado
		INSERT INTO gsa.tbASHistoricoEstadosServicioSolicitado(
					cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_estdo_srvco_slctdo,
					inco_vgnca,
					fn_vgnca,
					fcha_fn_vldz_rgstro,
					vldo,
					usro_crcn,
					fcha_crcn,
					usro_ultma_mdfccn,
					fcha_ultma_mdfccn
					)
		SELECT		IDE.cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_estdo_srvco_slctdo,
					inco_vgnca,
					fn_vgnca,
					fcha_fn_vldz_rgstro,
					vldo,
					usro_crcn,
					fcha_crcn,
					usro_ultma_mdfccn,
					fcha_ultma_mdfccn		
		FROM		#tmp_tbASHistoricoEstadosServicioSolicitado THE
		INNER JOIN	#IdServicios_Mod	IDE WITH (NOLOCK)
		ON			IDE.idXML = THE.id;
		-- INSERTAR INFORMACIÓN DE PRESTACIONES EN TABLA ORIGINAL tbASServiciosSolicitadosOriginal 
		MERGE INTO			 BdCNA.gsa.tbASServiciosSolicitadosOriginal	
		USING (SELECT		 IDE.cnsctvo_srvco_slctdo
							,IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
							,PRE.id
							,PRE.cdgo_tpo_srvco	
							,PRE.cdgo_srvco_slctdo		
							,PRE.dscrpcn_srvco_slctdo	
							,PRE.cntdd_slctda	
							,PRE.fcha_prstcn_srvco_slctdo
							,PRE.tmpo_trtmnto_slctdo						
							,PRE.cdgo_undd_tmpo_trtmnto_slctdo
							,PRE.vlr_lqdcn_srvco	
							,PRE.cdgo_prstcn_prstdr						
							,PRE.cdgo_tpo_atrzcn						
							,PRE.usro_crcn			
							,PRE.nmro_prstcn				
				FROM		#TMod_Prestaciones	PRE WITH (NOLOCK)
				INNER JOIN	#IdServicios_Mod	IDE WITH (NOLOCK)
				ON			PRE.Id = IDE.idXML
				AND			PRE.nmro_prstcn = IDE.nmro_prstcn
				INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
				ON			PRE.Id = IDS.id) AS PRO
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_srvco_slctdo
				,cnsctvo_slctd_atrzcn_srvco
				,cdgo_tpo_srvco
				,cdgo_srvco_slctdo
				,dscrpcn_srvco_slctdo
				,cntdd_slctda
				,fcha_prstcn_srvco_slctdo
				,tmpo_trtmnto_slctdo
				,cdgo_undd_tmpo_trtmnto_slctdo
				,vlr_lqdcn_srvco
				,cdgo_prstcn_prstdr
				,cdgo_tpo_atrzcn
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn)
		VALUES ( PRO.cnsctvo_srvco_slctdo
				,PRO.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,PRO.cdgo_tpo_srvco	
				,PRO.cdgo_srvco_slctdo		
				,PRO.dscrpcn_srvco_slctdo	
				,PRO.cntdd_slctda	
				,PRO.fcha_prstcn_srvco_slctdo
				,PRO.tmpo_trtmnto_slctdo						
				,PRO.cdgo_undd_tmpo_trtmnto_slctdo
				,PRO.vlr_lqdcn_srvco	
				,PRO.cdgo_prstcn_prstdr						
				,PRO.cdgo_tpo_atrzcn
				,@Fecha_Actual	--fcha_crcn						
				,PRO.usro_crcn			
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,PRO.usro_crcn	--usro_ultma_mdfccn
				)
		 OUTPUT	inserted.cnsctvo_dto_srvco_slctdo_orgnl
				,PRO.id
				,PRO.nmro_prstcn 
		 INTO	#IdServiciosOr_Mod; 
		-- INSERTAR INFORMACIÓN DE MEDICAMENTOS EN TABLA DE PROCESO tbASMedicamentosSolicitados
		MERGE INTO			 BdCNA.gsa.tbASMedicamentosSolicitados
		USING (SELECT DISTINCT	 IDE.cnsctvo_srvco_slctdo
							,TME.id
							,TME.dss									
							,TME.cnsctvo_cdgo_prsntcn_dss				
							,TME.prdcdd_dss								
							,TME.cnsctvo_cdgo_undd_prdcdd_dss			
							,TME.cnsctvo_cdgo_frma_frmctca				
							,TME.cnsctvo_cdgo_grpo_trptco				
							,TME.cnsctvo_cdgo_prsntcn					
							,TME.cnsctvo_cdgo_va_admnstrcn_mdcmnto		
							,TME.cnsctvo_mdcmnto_slctdo_orgn			
							,TME.cncntrcn_dss							
							,TME.prsntcn_dss							
							,TME.cnsctvo_cdgo_undd_cncntrcn_dss			
							,TME.usro_crcn								
				FROM		#TMod_Prestaciones	TME WITH (NOLOCK)		
				INNER JOIN	#IdServicios_Mod	IDE WITH (NOLOCK)
				ON			TME.id = IDE.idXML
				AND			TME.cnsctvo_cdgo_tpo_srvco = IDE.cnsctvo_cdgo_tpo_srvco				
				AND			TME.nmro_prstcn = IDE.nmro_prstcn
				WHERE		TME.cnsctvo_cdgo_tpo_srvco = @Medicamento
				AND			IDE.cnsctvo_cdgo_tpo_srvco = @Medicamento) AS MED
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_srvco_slctdo
				,dss
				,cnsctvo_cdgo_prsntcn_dss
				,prdcdd_dss
				,cnsctvo_cdgo_undd_prdcdd_dss
				,cnsctvo_cdgo_frma_frmctca
				,cnsctvo_cdgo_grpo_trptco
				,cnsctvo_cdgo_prsntcn
				,cnsctvo_cdgo_va_admnstrcn_mdcmnto
				,cnsctvo_mdcmnto_slctdo_orgn
				,cncntrcn_dss
				,prsntcn_dss
				,fcha_vncmnto_invma
				,mstra_mdca
				,uso_cndcndo
				,mss_mxmo_rnvcn
				,cnsctvo_cdgo_undd_cncntrcn_dss
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn)
		VALUES ( MED.cnsctvo_srvco_slctdo
				,MED.dss									
				,MED.cnsctvo_cdgo_prsntcn_dss				
				,MED.prdcdd_dss								
				,MED.cnsctvo_cdgo_undd_prdcdd_dss			
				,MED.cnsctvo_cdgo_frma_frmctca				
				,MED.cnsctvo_cdgo_grpo_trptco				
				,MED.cnsctvo_cdgo_prsntcn					
				,MED.cnsctvo_cdgo_va_admnstrcn_mdcmnto		
				,MED.cnsctvo_mdcmnto_slctdo_orgn			
				,MED.cncntrcn_dss							
				,MED.prsntcn_dss	
				,NULL			--fcha_vncmnto_invma		
				,''				--mstra_mdca	
				,''				--uso_cndcndo			
				,0				--mss_mxmo_rnvcn
				,MED.cnsctvo_cdgo_undd_cncntrcn_dss		
				,@Fecha_Actual	--fcha_crcn
				,MED.usro_crcn		
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,MED.usro_crcn	--usro_ultma_mdfccn	
				)
		OUTPUT	inserted.cnsctvo_mdcmnto_slctdo
				,MED.Id
		INTO	#IdMedicamentos_Mod;
		-- INSERTAR INFORMACIÓN DE MEDICAMENTOS EN TABLA ORIGINAL tbASMedicamentosSolicitadosOriginal
		INSERT INTO	 BdCNA.gsa.tbASMedicamentosSolicitadosOriginal(
					 cnsctvo_dto_srvco_slctdo_orgnl
					,cnsctvo_mdcmnto_slctdo
					,dss
					,cdgo_prsntcn_dss
					,prdcdd_dss
					,cdgo_undd_prdcdd_dss
					,cdgo_frma_frmctca
					,cdgo_grpo_trptco
					,cdgo_prsntcn
					,cdgo_va_admnstrcn_mdcmnto
					,cncntrcn_dss
					,prsntcn_dss
					,cdgo_undd_cncntrcn_dss
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDO.cnsctvo_dto_srvco_slctdo_orgnl
					,IDM.cnsctvo_mdcmnto_slctdo	
					,MED.dss
					,MED.cdgo_prsntcn_dss	
					,MED.prdcdd_dss				
					,MED.cdgo_undd_prdcdd_dss				
					,MED.cdgo_frma_frmctca						
					,MED.cdgo_grpo_trptco						
					,MED.cdgo_prsntcn							
					,MED.cdgo_va_admnstrcn_mdcmnto
					,MED.cncntrcn_dss
					,MED.prsntcn_dss
					,MED.cdgo_undd_cncntrcn_dss	
					,@Fecha_Actual	--fcha_crcn
					,MED.usro_crcn		
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,MED.usro_crcn	--usro_ultma_mdfccn		
		FROM		#TMod_Prestaciones	MED WITH (NOLOCK)
		INNER JOIN	#IdServiciosOr_Mod	IDO WITH (NOLOCK)
		ON			MED.Id = IDO.idXML
		AND			MED.nmro_prstcn = IDO.nmro_prstcn
		INNER JOIN	#IdMedicamentos_Mod	IDM WITH (NOLOCK)
		ON			MED.Id = IDM.idXML
		WHERE		MED.cnsctvo_cdgo_tpo_srvco = @Medicamento
		-- INSERTAR INFORMACIÓN DE PROCEDIMIENTOS EN TABLA DE PROCESO tbASProcedimientosInsumosSolicitados
		MERGE INTO			BdCNA.gsa.tbASProcedimientosInsumosSolicitados
		USING (SELECT DISTINCT		IDE.cnsctvo_srvco_slctdo
							,TIN.id
							,TIN.cnsctvo_cdgo_ltrldd
							,TIN.cnsctvo_cdgo_va_accso
							,TIN.usro_crcn			
				FROM		#TMod_Prestaciones	TIN WITH (NOLOCK)		
				INNER JOIN	#IdServicios_Mod	IDE WITH (NOLOCK)
				ON			TIN.id = IDE.idXML
				AND			TIN.cnsctvo_cdgo_tpo_srvco = IDE.cnsctvo_cdgo_tpo_srvco
				AND			TIN.nmro_prstcn = IDE.nmro_prstcn
				WHERE		TIN.cnsctvo_cdgo_tpo_srvco = @Procedimiento
				AND			IDE.cnsctvo_cdgo_tpo_srvco = @Procedimiento) AS INS
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT (cnsctvo_srvco_slctdo
				,undd_cntdd
				,cnsctvo_cdgo_ltrldd
				,cnsctvo_cdgo_va_accso
				,cntdd_mxma
				,frcnca_entrga				
				,accso_drcto
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn)
		VALUES ( INS.cnsctvo_srvco_slctdo
				,''				--undd_cntdd
				,INS.cnsctvo_cdgo_ltrldd
				,INS.cnsctvo_cdgo_va_accso
				,0				--cntdd_mxma
				,0				--frcnca_entrga
				,''				--accso_drcto
				,@Fecha_Actual	--fcha_crcn
				,INS.usro_crcn	
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,INS.usro_crcn	--usro_ultma_mdfccn
				)
		OUTPUT	inserted.cnsctvo_prcdmnto_insmo_slctdo
				,INS.id
		INTO	#IdInsumos_Mod;
		-- INSERTAR INFORMACIÓN DE PROCEDIMIENTOS EN TABLA ORIGINAL tbASProcedimientosInsumosSolicitadosOriginal
		INSERT INTO	 BdCNA.gsa.tbASProcedimientosInsumosSolicitadosOriginal(
					 cnsctvo_dto_srvco_slctdo_orgnl
					,cnsctvo_prcdmnto_insmo_slctdo
					,cdgo_ltrldd
					,cdgo_va_accso
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDO.cnsctvo_dto_srvco_slctdo_orgnl
					,IDI.cnsctvo_prcdmnto_insmo_slctdo
					,INS.cdgo_ltrldd
					,INS.cdgo_va_accso
					,@Fecha_Actual	--fcha_crcn
					,INS.usro_crcn	
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,INS.usro_crcn	--usro_ultma_mdfccn
		FROM		#TMod_Prestaciones	INS WITH (NOLOCK)
		INNER JOIN	#IdServiciosOr_Mod	IDO WITH (NOLOCK)
		ON			INS.Id = IDO.idXML
		AND			INS.nmro_prstcn = IDO.nmro_prstcn
		INNER JOIN	#IdInsumos_Mod		IDI WITH (NOLOCK)
		ON			INS.Id = IDI.idXML
		WHERE		INS.cnsctvo_cdgo_tpo_srvco = @Procedimiento
		PRINT('FIN PRESTACION')
	END	


GO
