USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASDireccionarMigracion3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASDireccionarMigracion3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASDireccionarMigracion3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Se encarga de formar el XML para el direccionamiento de las prestaciones y enviarlas a 
					 direccionamiento, la tabla temporal #Idsolicitudes debe existir con el campo cnsctvo_slctd_atrzcn_srvco D\>    
* Observaciones : <\O Es necesaria la tabla temporal de solicitudes para formar el XML. O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/06/20 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASDireccionarMigracion3047
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASDireccionarMigracion3047(
	@usro udtUsuario, 
	@estdo_ejccn varchar(5) OUTPUT, 
	@msje_errr varchar(2000) OUTPUT, 
	@msje_rspsta nvarchar(max) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @xml xml, 
			@xml_varchar varchar(max),
			@cdgo_slctd udtCodigo,
			@cdgo_slctd_prstdr udtCodigo
	
	SET @cdgo_slctd = NULL
	SET @cdgo_slctd_prstdr = NULL
	
	--Se forma XML apartir de la tabla temporal de solicitudes.
	SET @xml = (SELECT cnsctvo_slctd_atrzcn_srvco as cnsctvo_slctd,
						@cdgo_slctd as cdgo_slctd, 
						@cdgo_slctd_prstdr as cdgo_slctd_prstdr
				FROM #Idsolicitudes
				FOR XML RAW ('idnt_slctds'), ROOT ('cdgs_slctds'), ELEMENTS)

	--Se pasa la variable a varchar ya que el parametro del sp de direccionamiento es varchar.
	SET @xml_varchar = CAST (@xml as varchar(max))

	EXEC gsa.spASEjecutarDireccionamientoPrestadorDestino @xml_varchar, @usro, @estdo_ejccn OUTPUT, @msje_errr OUTPUT, @msje_rspsta OUTPUT
END
GO
