USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEstadoCerradoPresolicitudes]    Script Date: 11/01/2017 2:59:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASActualizarEstadoCerradoPresolicitudes') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASActualizarEstadoCerradoPresolicitudes AS SELECT 1;'
END
GO
/*------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASActualizarEstadoCerradoPresolicitudes
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos    A\>    
* Descripcion        : <\D 
                          Procedimiento que permite actualizar a estado cerrado las presolicitudes gestionadas en MEGA
					   D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 02/12/2015  FC\>    
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarEstadoCerradoPresolicitudes] 

AS    

BEGIN    

 	SET NOCOUNT ON

	DECLARE @cnsctvo_cdgo_estdo_prslctd_crrdo  udtConsecutivo,
	        @cnsctvo_cdgo_estdo_prslctd_dvlto  udtConsecutivo,
			@fcha_crcn                         Datetime      ,
			@fcha_actl                         Datetime
			
	Set     @cnsctvo_cdgo_estdo_prslctd_crrdo = 16
	Set     @cnsctvo_cdgo_estdo_prslctd_dvlto = 11
	Set     @fcha_crcn = '20170123'
	Set     @fcha_actl = getDate()

	Update          bdcna.dbo.tbPreSolicitudes 
	Set             cnsctvo_cdgo_estdo_prslctd = @cnsctvo_cdgo_estdo_prslctd_crrdo 
	From            bdcna.dbo.tbPreSolicitudes a WITH (NOLOCK)
	Inner Join      bdcna.gsa.tbASDatosAdicionalesPreSolicitud b  WITH (NOLOCK)
    On              b.cnsctvo_prslctd = a.cnsctvo_prslctd
	Left Outer Join bdsisalud.dbo.tbSolicitudOPSCausasEstadosPrestacion_vigencias d WITH (NOLOCK)
    On              d.cnsctvo_cdgo_csa_estdo_prstcn_slctd = b.cnsctvo_cdgo_csa_nvdd
	WHERE           a.fcha_crcn >= @fcha_crcn 
	And            (b.cnsctvo_cdgo_csa_nvdd Is Not Null Or b.cnsctvo_slctd_atrzcn_srvco Is Not Null)
	And             cnsctvo_cdgo_estdo_prslctd Not In (@cnsctvo_cdgo_estdo_prslctd_dvlto, @cnsctvo_cdgo_estdo_prslctd_crrdo)
	And             @fcha_actl Between d.inco_vgnca And d.fn_vgnca
End