USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosSolicitudServicio]    Script Date: 25/07/2017 04:37:52 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosSolicitudServicio
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez	- SETI SAS  					AM\>
* Descripcion			 : <\DM	Se cambia el tipo de dato de la variable 
								cdgo_mdo_cntcto de varchar(4) a udtDescripcion	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	 2016/08/12									    FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge RodriguezAM\>
* Descripcion			 : <\DM	Se ajusta SP para que devuelva la la sede del afiliado
								Para mostrarlo en pantalla Auditoria y Contratación DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-02-28 FM\>
*------------------------------------------------------------------------------------------------ 
* Modificado Por		 : <\AM	Jorge RodriguezAM\>
* Descripcion			 : <\DM	Se ajusta SP para que devuelva las observaciones ingresadas por
								los auditores y mostrarla en la pantalla de gestión contratación DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-03-08 FM\>
*--------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez AM\>
* Descripcion			 : <\DM	Se ajusta sp para que retorne el consecutivo del plan de la solicitud. DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017-07-25 FM\>
*--------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaDatosSolicitudServicio] 394127
ALTER PROCEDURE [gsa].[spASConsultaDatosSolicitudServicio]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @cnsctvo_cdgo_rcbro			udtConsecutivo,
			@sde_afldo					int,
			@dscrpcn_sde				udtDescripcion,
			@fcha_vldccn				datetime ;

	CREATE TABLE #DatosSolicitud(
		cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,	
		fcha_crcn					datetime,		
		fcha_slctd					datetime,		
		nmro_slctd_atrzcn_ss		varchar(16),		
		dscrpcn_tpo_ubccn_pcnte		udtDescripcion,	
		dscrpcn_prrdd_atncn			udtDescripcion,	
		adtr_slctnte				udtDescripcion,	
		cdgo_mdo_cntcto				udtDescripcion,	
		dscrpcn_orgn_atncn			udtDescripcion,	
		dscrpcn_clse_atncn			udtDescripcion,
		cnsctvo_cdgo_frma_atncn		udtConsecutivo,
		jstfccn_clnca				varchar(2000),
		spra_tpe_st					udtLogico,
		dscrpcn_rcbro				udtDescripcion,
		nmro_slctd_prvdr			varchar(15),
		obsrvcn_adcnl				udtObservacion,
		cnsctvo_cdgo_rcbro			udtConsecutivo,
		obsrvcns_rqre_otra_gstn_adtr varchar(2000)
	);


		CREATE TABLE #DatosAfiliado(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,			cnsctvo_cdgo_tpo_idntfccn_afldo	udtConsecutivo,
		cdgo_tpo_idntfccn				char(3),				nmro_idntfccn_afldo				udtNumeroIdentificacionLargo,
		prmr_nmbre						udtNombre,				sgndo_nmbre						udtNombre,
		prmr_aplldo						udtApellido,			sgndo_aplldo					udtApellido,		
		cnsctvo_cdgo_sxo				udtConsecutivo,			cdgo_sxo						char(2),
		alt_rsgo						char(1),
		cnsctvo_cdgo_tpo_afldo			udtConsecutivo,			dscrpcn							udtDescripcion,
		estdo							udtLogico,				smns_ctzds_antrr_eps			udtConsecutivo,
		smns_ctzds						udtConsecutivo,			fcha_ncmnto						datetime,
		cnsctvo_cdgo_pln			    udtConsecutivo,			dscrpcn_pln						udtDescripcion,
		cnsctvo_cdgo_cdd_rsdnca			udtConsecutivo,			dscrpcn_cdd						udtDescripcion,
		cnsctvo_cdgo_estdo_drcho		udtConsecutivo,			dscrpcn_drcho					udtDescripcion,
		cnsctvo_cdgo_pln_pc				udtConsecutivo,			dscrpcn_pln_pc					udtDescripcion, --Plan complementario
		rcn_ncdo						udtLogico,				nmro_hjo_afldo					udtConsecutivo,
		fcha_ncmnto_rcn_ncdo			datetime,				cnsctvo_cdgo_sxo_rcn_ncdo		udtConsecutivo,
		cdgo_sxo_rcn_ncdo				char(2),				fcha_crcn						datetime,
		cnsctvo_cdgo_tpo_cntrto			udtConsecutivo,			edd								int,
		edd_mss							int,					edd_ds							int,
		cnsctvo_rgstro_vldcn_espcl		udtConsecutivo,			nmro_vldcn_espcl				udtDescripcion,
		nmro_unco_idntfccnr				udtConsecutivo								

	);
		
		Set @fcha_vldccn = getDate();
		--Se obitene el consecutivo de la sede del afiliado.

		Select		@dscrpcn_sde =  s.dscrpcn_sde,
					@sde_afldo	=  ias.cnsctvo_cdgo_sde_ips_prmra
		From		BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias With(NoLock)
		Inner Join	BDAfiliacionValidador.dbo.tbSedes_Vigencias s  With(NoLock)
		On			s.cnsctvo_cdgo_sde	=	ias.cnsctvo_cdgo_sde_ips_prmra
		Where		ias.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		and			@fcha_vldccn between s.inco_vgnca and s.fn_vgnca
	
	Insert Into #DatosSolicitud(
		cnsctvo_slctd_atrzcn_srvco,	
		fcha_crcn,		
		fcha_slctd,		
		nmro_slctd_atrzcn_ss,		
		dscrpcn_tpo_ubccn_pcnte,	
		dscrpcn_prrdd_atncn	,	
		adtr_slctnte,	
		cdgo_mdo_cntcto	,	
		dscrpcn_orgn_atncn,	
		dscrpcn_clse_atncn,
		jstfccn_clnca,
		cnsctvo_cdgo_frma_atncn,
		spra_tpe_st,
		dscrpcn_rcbro,
		nmro_slctd_prvdr
	)
	EXEC gsa.spASConsultaDatosInformacionSolicitud @cnsctvo_slctd_atrzcn_srvco;
	



	Insert Into  #DatosAfiliado(
		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_tpo_idntfccn_afldo,	cdgo_tpo_idntfccn,				
		nmro_idntfccn_afldo,			prmr_nmbre,							sgndo_nmbre,
		prmr_aplldo,					sgndo_aplldo,						cnsctvo_cdgo_sxo,
		cdgo_sxo,						alt_rsgo,							cnsctvo_cdgo_tpo_afldo,			
		dscrpcn,						estdo,								smns_ctzds_antrr_eps,
		smns_ctzds,						fcha_ncmnto,						cnsctvo_cdgo_pln,
		dscrpcn_pln,					cnsctvo_cdgo_cdd_rsdnca,			dscrpcn_cdd,
		cnsctvo_cdgo_estdo_drcho,		dscrpcn_drcho,						cnsctvo_cdgo_pln_pc,	
		dscrpcn_pln_pc,					rcn_ncdo,							nmro_hjo_afldo,
		fcha_ncmnto_rcn_ncdo,			cnsctvo_cdgo_sxo_rcn_ncdo,			cdgo_sxo_rcn_ncdo,				
		fcha_crcn,						cnsctvo_cdgo_tpo_cntrto,			edd,
		edd_mss,						edd_ds,								cnsctvo_rgstro_vldcn_espcl,
		nmro_vldcn_espcl,				nmro_unco_idntfccnr
		)
	EXEC gsa.spASConsultaDatosInformacionAfiliadoxSolicitud @cnsctvo_slctd_atrzcn_srvco;
	
	Set @cnsctvo_cdgo_rcbro = ( Select top 1 dg.cnsctvo_cdgo_rcbro From 
								gsa.tbASDiagnosticosSolicitudAutorizacionServicios dg WITH(NOLOCK) 
								Where dg.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco )

	Update ds Set cnsctvo_cdgo_rcbro = @cnsctvo_cdgo_rcbro
	From #DatosSolicitud ds

	Update ds  
	Set obsrvcn_adcnl				 = sas.obsrvcn_adcnl,
		obsrvcns_rqre_otra_gstn_adtr = sas.obsrvcns_rqre_otra_gstn_adtr
	From #DatosSolicitud ds
	Inner Join BDCna.gsa.tbASSolicitudesAutorizacionServicios sas  WITH(NOLOCK) 
	On SAS.cnsctvo_slctd_atrzcn_srvco = ds.cnsctvo_slctd_atrzcn_srvco
	
	SELECT 	
	ds.cnsctvo_slctd_atrzcn_srvco,	ds.fcha_crcn,			ds.fcha_slctd,		ds.nmro_slctd_atrzcn_ss,		
	ds.dscrpcn_tpo_ubccn_pcnte,		ds.dscrpcn_prrdd_atncn,	ds.adtr_slctnte,	ds.cdgo_mdo_cntcto,
	ds.dscrpcn_orgn_atncn,			ds.dscrpcn_clse_atncn,	ds.jstfccn_clnca,	
	
	da.cdgo_tpo_idntfccn,			da.nmro_idntfccn_afldo,	Concat(rtrim(da.prmr_nmbre),' ',rtrim(da.sgndo_nmbre),' ',rtrim(da.prmr_aplldo),' ',rtrim(da.sgndo_aplldo)) nmbre,
	da.cdgo_sxo,					da.edd,					da.dscrpcn,			da.estdo,					
	da.smns_ctzds_antrr_eps,		da.smns_ctzds,			da.dscrpcn_pln,		da.dscrpcn_cdd,
	da.dscrpcn_drcho,				da.dscrpcn_pln_pc,		da.alt_rsgo,		da.rcn_ncdo,			
	da.nmro_hjo_afldo,				da.cdgo_sxo_rcn_ncdo,	da.fcha_crcn,		da.cnsctvo_cdgo_tpo_idntfccn_afldo,
	da.fcha_ncmnto_rcn_ncdo,		da.nmro_vldcn_espcl,	ds.cnsctvo_cdgo_frma_atncn,
	ds.spra_tpe_st,					ds.dscrpcn_rcbro,		da.nmro_unco_idntfccnr,
	ds.nmro_slctd_prvdr,			ds.obsrvcn_adcnl,		ds.cnsctvo_cdgo_rcbro,
	@sde_afldo sde_afldo,			@dscrpcn_sde  dscrpcn_sde, ds.obsrvcns_rqre_otra_gstn_adtr,
	da.cnsctvo_cdgo_pln
	FROM		#DatosSolicitud ds
	INNER JOIN  #DatosAfiliado da 
	On			ds.cnsctvo_slctd_atrzcn_srvco = da.cnsctvo_slctd_atrzcn_srvco
		
	drop table #DatosSolicitud;
	drop table #DatosAfiliado;
	
END

