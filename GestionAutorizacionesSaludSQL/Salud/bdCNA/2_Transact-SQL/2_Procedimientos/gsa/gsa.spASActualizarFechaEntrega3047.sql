USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASActualizarFechaEntrega3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASActualizarFechaEntrega3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASActualizarFechaEntrega3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Se encarga de guardar las fechas de entrega de la solicitud. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/06/22 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASActualizarFechaEntrega3047
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASActualizarFechaEntrega3047
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @NO char(1),
			@uno int,
			@fechaActual datetime

	SET @NO = 'N'
	SET @uno = 1
	SET @fechaActual = getdate()

	INSERT gsa.tbASResultadoFechaEntrega(
		cnsctvo_slctd_atrzcn_srvco,					cnsctvo_srvco_slctdo,			cnsctvo_cdgo_grpo_entrga,
		fcha_estmda_entrga,							fcha_rl_entrga,					prrdd_admnstrtva,
		fcha_crcn,									usro_crcn,						fcha_ultma_mdfccn,
		usro_ultma_mdfccn)
	SELECT 
		SOL.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_cdgo_srvco_slctdo,		@uno,
		ops.fcha_incl_imprsn,					ops.fcha_fnl_imprsn,				@NO,
		@fechaActual,							t.usro_crcn,						@fechaActual,
		t.usro_crcn
	FROM #Idsolicitudes IDS
	Inner Join  gsa.tbASSolicitudesAutorizacionServicios SOL with(nolock) On IDS.cnsctvo_slctd_atrzcn_srvco = SOL.cnsctvo_slctd_atrzcn_srvco
	Inner Join  gsa.tbASServiciosSolicitados ss with(nolock) On sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#Tempo_Solicitudes t On t.nmro_slctd_atrzcn_ss = sol.nmro_slctd_atrzcn_ss
	Inner Join	#solicitudes3047 dp On 	dp.cnsctvo_slctd_ops = t.id
	INNER JOIN bdSisalud.dbo.tbSolicitudOPSPrestaciones ops with(nolock) ON dp.cnsctvo_slctd_ops = ops.cnsctvo_slctd_ops and ss.cnsctvo_cdgo_srvco_slctdo = ops.cnsctvo_cdfccn
	WHERE ops.fcha_incl_imprsn is not null
END
GO
