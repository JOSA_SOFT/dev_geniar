USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosEstadosPrestacionesBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosEstadosPrestacionesBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosEstadosPrestacionesBPM]
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo

AS
BEGIN
	SET NOCOUNT ON;
	Declare @fcha_vldcn					datetime,
			@countEstadoAuditoria		int = 0,
			@countEstadoFechaEntregaDia	int = 0,
			@contEstadoAprobado			int = 7;

 
	Set @fcha_vldcn = getDate();
	Set @countEstadoAuditoria = 
							  (SELECT count(1)
							  FROM BDCna.gsa.tbASServiciosSolicitados ss
							   Inner Join BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ess on ss.cnsctvo_cdgo_estdo_srvco_slctdo = ess.cnsctvo_cdgo_estdo_srvco_slctdo
							  where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
							  And ess.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo
							  And @fcha_vldcn between ess.inco_vgnca and  ess.fn_vgnca);

	Set @countEstadoFechaEntregaDia = 
								 (SELECT count(1)
								  FROM BDCna.gsa.tbASServiciosSolicitados ss
								   Inner Join BDCna.gsa.tbASResultadoFechaEntrega rfe On ss.cnsctvo_slctd_atrzcn_srvco = rfe.cnsctvo_slctd_atrzcn_srvco And ss.cnsctvo_srvco_slctdo = rfe.cnsctvo_srvco_slctdo
								   Inner Join BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ess on ss.cnsctvo_cdgo_estdo_srvco_slctdo = ess.cnsctvo_cdgo_estdo_srvco_slctdo
								  where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
								   And ss.cnsctvo_cdgo_estdo_srvco_slctdo = @contEstadoAprobado
								   And rfe.fcha_estmda_entrga <= @fcha_vldcn
								   And @fcha_vldcn between ess.inco_vgnca and  ess.fn_vgnca);

	Select @countEstadoAuditoria countEstadoAuditoria, @countEstadoFechaEntregaDia countEstadoFechaEntregaDia
END

GO
