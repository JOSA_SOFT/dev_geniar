USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaAutorizacionServicioOPSxSolicitudWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaAutorizacionServicioOPSxSolicitudWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información las autorizaciones generadas
						   a una solicitud
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           cnsctvo_slctd_atrzcn_srvco  					         
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 31/03/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*--------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASConsultaAutorizacionServicioOPSxSolicitudWeb] 657, null

 ALTER PROCEDURE [gsa].[spASConsultaAutorizacionServicioOPSxSolicitudWeb] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
   @cnsctvo_srvco_slctdo       udtConsecutivo = Null
AS

Begin
   SET NOCOUNT ON

   Declare @cnsctvo_cdgo_estdo udtConsecutivo

   Create
   Table  #tempGrupoImpresion(nmro_unco_ops	                udtConsecutivo,							  
                              cnsctvo_prcdmnto_insmo_slctdo udtConsecutivo,
                              cnsctvo_mdcmnto_slctdo        udtConsecutivo,
                              cnsctvo_cdgo_grpo_imprsn      udtConsecutivo,
                              vlr_lqddo_grpo_imprsn         Int           ,
                              cnsctvo_srvco_slctdo          udtConsecutivo,                            
                              cdgo_intrno_prstdr_atrzdo     udtCodigoIps  ,
							  cnsctvo_slctd_atrzcn_srvco    udtConsecutivo
						     )
   Create
   Table  #tempOPS(nmro_unco_ops	          udtConsecutivo,
                   cdgo_intrno_prstdr_atrzdo  udtCodigoIps  ,
				   vlr_ttal                   Int           ,
				   cnsctvo_slctd_atrzcn_srvco udtConsecutivo
                  )
   
   Set @cnsctvo_cdgo_estdo = 11 --Autorizada  9 Liquidada 

   ----Se inserta en la tabla temporal la informacion de los conceptos de gasto agrupados por grupos de impresion consultados por solicitud
   Insert 
   Into       #tempGrupoImpresion(cnsctvo_cdgo_grpo_imprsn  , cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo, 
                                  nmro_unco_ops             , cdgo_intrno_prstdr_atrzdo    , cnsctvo_srvco_slctdo  ,
                                  cnsctvo_slctd_atrzcn_srvco, vlr_lqddo_grpo_imprsn     										 
                                 )
   Select     s.cnsctvo_cdgo_grpo_imprsn  , s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo, 
              s.nmro_unco_ops             , s.cdgo_intrno_prstdr_atrzdo    , a.cnsctvo_srvco_slctdo  ,
              a.cnsctvo_slctd_atrzcn_srvco, Sum(s.vlr_lqdcn)
   From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
   Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
   On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo    
   Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
   On         s.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
   Where      a.cnsctvo_slctd_atrzcn_srvco             = @cnsctvo_slctd_atrzcn_srvco
   And        s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo
   Group By   s.cnsctvo_cdgo_grpo_imprsn  , s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo, 
              s.nmro_unco_ops             , s.cdgo_intrno_prstdr_atrzdo    , a.cnsctvo_srvco_slctdo  ,
              a.cnsctvo_slctd_atrzcn_srvco
   
   ----Se inserta en la tabla temporal la informacion de los conceptos de gasto agrupados por grupos de impresion consultados por solicitud
   Insert 
   Into       #tempGrupoImpresion(cnsctvo_cdgo_grpo_imprsn  , cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo, 
                                  nmro_unco_ops             , cdgo_intrno_prstdr_atrzdo    , cnsctvo_srvco_slctdo  ,
                                  cnsctvo_slctd_atrzcn_srvco, vlr_lqddo_grpo_imprsn     										 
                                 )
   Select     s.cnsctvo_cdgo_grpo_imprsn  , s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo, 
              s.nmro_unco_ops             , s.cdgo_intrno_prstdr_atrzdo    , a.cnsctvo_srvco_slctdo  ,
              a.cnsctvo_slctd_atrzcn_srvco, Sum(s.vlr_lqdcn)
   From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
   Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
   On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo    
   Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
   On         s.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
   Where      a.cnsctvo_slctd_atrzcn_srvco             = @cnsctvo_slctd_atrzcn_srvco
   And        s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo
   Group By   s.cnsctvo_cdgo_grpo_imprsn  , s.cnsctvo_prcdmnto_insmo_slctdo, s.cnsctvo_mdcmnto_slctdo, 
              s.nmro_unco_ops             , s.cdgo_intrno_prstdr_atrzdo    , a.cnsctvo_srvco_slctdo  ,
              a.cnsctvo_slctd_atrzcn_srvco
   
   If @cnsctvo_srvco_slctdo IS NULL
      Begin
		Insert 
		Into        #tempOPS(nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco, vlr_ttal)
		Select      nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco, Max(vlr_lqddo_grpo_imprsn)
		From        #tempGrupoImpresion
		Where       cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		Group By    nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco
	  End
	Else
	  Begin
	    Insert 
		Into        #tempOPS(nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco, vlr_ttal)
		Select      nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco, Max(vlr_lqddo_grpo_imprsn)
		From        #tempGrupoImpresion
		Where       cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And         cnsctvo_srvco_slctdo       = @cnsctvo_srvco_slctdo
		Group By    nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco
	  End
	  
   Select nmro_unco_ops, cdgo_intrno_prstdr_atrzdo, cnsctvo_slctd_atrzcn_srvco, vlr_ttal
   From   #tempOPS
   
   Drop Table #tempGrupoImpresion
   Drop Table #tempOPS

End


GO
