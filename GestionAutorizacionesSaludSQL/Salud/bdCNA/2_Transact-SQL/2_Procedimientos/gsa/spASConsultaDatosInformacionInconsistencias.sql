USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionInconsistencias]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionInconsistencias
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionInconsistencias] 
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@cnsctvo_cdgo_no_cnfrmdd_vldcn	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @ldFechaValidacion datetime;
	CREATE TABLE #InconsistenciasPrestaciones(
		cnsctvo_dt_prcso_vldcn_ctc		udtConsecutivo,
		infrmcn_adcnl_no_cnfrmdd		udtObservacion,
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo
	)
	
	Create TABLE #NoConformidadesValidacion(
		cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
	)

	Set @ldFechaValidacion = getDate()
	

	Insert Into #NoConformidadesValidacion
	(cnsctvo_cdgo_no_cnfrmdd_vldcn)
	SELECT 
	cnsctvo_cdgo_no_cnfrmdd_vldcn
	FROM bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias
	where cnsctvo_cdgo_estdo_no_cnfrmdd = @cnsctvo_cdgo_no_cnfrmdd_vldcn


	Insert Into #InconsistenciasPrestaciones (
		cnsctvo_dt_prcso_vldcn_ctc,
		infrmcn_adcnl_no_cnfrmdd,
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo
	)
	SELECT
	    ROW_NUMBER() OVER(ORDER BY RPM.cnsctvo_dt_prcso_vldcn_ctc DESC) cnsctvo_dt_prcso_vldcn_ctc, 
		RPM.infrmcn_adcnl_no_cnfrmdd,
		@cnsctvo_slctd_atrzcn_srvco,
		RPM.llve_prmra_cncpto_prncpl_vlddo
	FROM bdProcesosSalud.dbo.tbResultadosProcesoMallaValidacion RPM WITH (NOLOCK)
	Inner Join #NoConformidadesValidacion NCV On NCV.cnsctvo_cdgo_no_cnfrmdd_vldcn = RPM.cnsctvo_cdgo_no_cnfrmdd_vldcn
	WHERE llve_prmra_rgstro_vlddo = @cnsctvo_slctd_atrzcn_srvco
	
	
	Update IP
	Set		infrmcn_adcnl_no_cnfrmdd = Concat(TCF.dscrpcn_tpo_cdfccn,'-',
											  CF.cdgo_cdfccn,'-', 
											  SUBSTRING(dscrpcn_srvco_slctdo, 1, 20), '-',
											  infrmcn_adcnl_no_cnfrmdd)
	From #InconsistenciasPrestaciones IP
	INNER JOIN [BDCna].[gsa].[tbASServiciosSolicitados] SS WITH (NOLOCK) On SS.cnsctvo_srvco_slctdo = IP.cnsctvo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbCodificaciones CF WITH (NOLOCK) on CF.cnsctvo_cdfccn = SS.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on cf.cnsctvo_cdgo_tpo_cdfccn = tcf.cnsctvo_cdgo_tpo_cdfccn
	Where SS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And  @ldFechaValidacion between tcf.inco_vgnca and tcf.fn_vgnca;

	Select 
		cnsctvo_dt_prcso_vldcn_ctc, 
		infrmcn_adcnl_no_cnfrmdd
	from #InconsistenciasPrestaciones

	Drop table #InconsistenciasPrestaciones
	Drop table #NoConformidadesValidacion
	
END

GO
