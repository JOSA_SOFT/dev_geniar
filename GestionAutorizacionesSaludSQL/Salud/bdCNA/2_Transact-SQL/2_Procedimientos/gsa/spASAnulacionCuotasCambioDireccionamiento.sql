Use bdCNA
GO

If(OBJECT_ID('gsa.spASAnulacionCuotasCambioDireccionamiento') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASAnulacionCuotasCambioDireccionamiento As Select 1';
End 
Go


/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASAnulacionCuotasCambioDireccionamiento 
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez  A\>    
* Descripcion        : <\D 
                            Procedimiento que busca las prestaciones relacionadas por numero de OPS, 
							ejecuta la anulacion de cuotas y cambia los estados para el recalculo de cuotas.
						D\>    
* Observaciones      : <\O O\>    
* Parametros         : <\P P\>    
* Variables          : <\V V\>    
* Fecha Creacion     : <\FC 23/08/2017 FC\>    
*------------------------------------------------------------------------------------------------------------------------------*/


Alter Procedure gsa.spASAnulacionCuotasCambioDireccionamiento
	@usro			udtUsuario
As
Begin
	
	Set Nocount On

	Declare @vlr_cro					Int,
			@mnsje_intrno				Varchar(150),
			@cdgo_intrno				Varchar(2),
			@vlr_nll					udtConsecutivo,
			@cdgo_errr					Varchar(2),
			@cnsctvo_cdgo_estdo_lqddo	udtConsecutivo,
			@fcha_actl					Datetime;

	Create Table #tempOPS (
		cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
		cnsctvo_srvco_slctdo		udtConsecutivo,
		nmro_unco_ops				udtConsecutivo
	);

	Create table #tmpSolicitudAnulacionCuotasRecuperacion (
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- n�mero de la solicitud
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestaci�n o servicio
		nmro_unco_ops					UdtConsecutivo  
	);

	Set @vlr_cro = 0;
	Set @vlr_nll = Null;
	Set @cdgo_errr = 'ET';
	Set @cnsctvo_cdgo_estdo_lqddo = 9;
	Set @fcha_actl = getDate();

	-- Se cargan las prestaciones con numeros de OPS
	Insert Into #tempOPS (
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,
				nmro_unco_ops
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_srvco_slctdo,
				css.nmro_unco_ops
	From		#tmpprestacionesops po
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = po.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
	Where		css.nmro_unco_ops != @vlr_cro;

	-- 
	Insert Into #tempOPS (
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,
				nmro_unco_ops
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_srvco_slctdo,
				css.nmro_unco_ops
	From		#tmpprestacionesops po
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = po.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	Where		css.nmro_unco_ops != @vlr_cro;

	-- Se cargan las prestaciones a las cuales se les anulara las cuotas.
	Insert Into #tmpSolicitudAnulacionCuotasRecuperacion (
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,		cnsctvo_cdgo_srvco_slctdo
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_srvco_slctdo
	From		#tempOPS a
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.nmro_unco_ops = a.nmro_unco_ops
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	And			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco;

	-- 
	Insert Into #tmpSolicitudAnulacionCuotasRecuperacion (
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,		cnsctvo_cdgo_srvco_slctdo
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_srvco_slctdo,	ss.cnsctvo_cdgo_srvco_slctdo
	From		#tempOPS a
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.nmro_unco_ops = a.nmro_unco_ops
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_mdcmnto_slctdo= css.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
	And			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco;

	-- Se ejecuta la anulacion de cuotas
	Exec		bdCNA.gsa.spASEjecutarAnulacionCuotasRecuperacion @usro, @mnsje_intrno, @cdgo_intrno, @vlr_nll, @vlr_nll, @vlr_nll;

	If(@cdgo_intrno = @cdgo_errr)
	Begin
			RaisError (@mnsje_intrno, 16, 2) With SetError
	End 

	-- se actualizan los estados de las prestaciones.
	Update		ss
	Set			cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
				usro_ultma_mdfccn = @usro,
				fcha_ultma_mdfccn = @fcha_actl
	From		#tmpSolicitudAnulacionCuotasRecuperacion a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(RowLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

	-- se actualizan los estados de los conceptos.
	Update		css
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
				usro_ultma_mdfccn = @usro,
				fcha_ultma_mdfccn = @fcha_actl
	From		#tmpSolicitudAnulacionCuotasRecuperacion a
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(RowLock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo

	-- 
	Update		css
	Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_lqddo,
				usro_ultma_mdfccn = @usro,
				fcha_ultma_mdfccn = @fcha_actl
	From		#tmpSolicitudAnulacionCuotasRecuperacion a
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(RowLock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo

	-- Agrega las prestaciones relacionadas
	Insert Into #tmpprestacionesops (
				cnsctvo_srvco_slctdo
	)
	Select		a.cnsctvo_srvco_slctdo 
	From		#tmpSolicitudAnulacionCuotasRecuperacion a
	Left Join	#tmpprestacionesops b
	On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		b.cnsctvo_srvco_slctdo Is Null

	-- 
	Drop Table #tempOPS;
	Drop Table #tmpSolicitudAnulacionCuotasRecuperacion;

End