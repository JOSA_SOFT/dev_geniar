USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASconsultaDatosSolicitudModificarFechaEntrega]    Script Date: 7/17/2017 8:52:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASconsultaDatosSolicitudModificarFechaEntrega
* Desarrollado por		: <\A Jorge Rodriguez - SETI SAS  A\>
* Descripcion			: <\D 
                              Consulta las prestaciones o servicios solicitados en 
							  estado Aprobado que están relacionados a una Solicitud.
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P 
                              consecutivo o 
                              numero de Radicado de la Solicitud  
						   P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/10/24 FC\>
*----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*----------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento para que realice el cambio de fecha de entrega
								sin tener en cuenta el direccionamiento
                             DM\>						  
* Nuevos Parametros	 	 : <\PM	 PM\>
* Nuevas Variables		 : <\VM	 VM\>
* Fecha Modificacion	 : <\FM	2017/07/17 FM\>
*----------------------------------------------------------------------------------------------------------------------*/
--EXEC	[gsa].[spASconsultaDatosSolicitudModificarFechaEntrega] 392819, '2016-04-00062744'

ALTER PROCEDURE [gsa].[spASconsultaDatosSolicitudModificarFechaEntrega]
	@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo =  null,
	@nmro_slctd_atrzcn_ss			Varchar(16)    =  null
AS
BEGIN
	SET NOCOUNT ON;

	Declare @estdo_aprbdo	udtConsecutivo,
			@estdo_lqddo	udtConsecutivo,
			@estdo_atrzdo	udtConsecutivo,
			@vlr_uno		int = 1,
			@vlr_cro		int = 0;

	Create 
	Table  #tmpDetalleServiciosFechaEntrega(cnsctvo_cdgo_tpo_srvco_slctdo	udtConsecutivo, --Tipo Codificación
		                                    dscrpcn_tpo_cdfccn				udtDescripcion, --Desctripcion tipo codificación
		                                    cnsctvo_cdgo_srvco_slctdo		udtConsecutivo, --Consecutivo tbASServicio Solicitado. Homologado con SOS
		                                    cdgo_cdfccn						char(11),       --Codigo Codificación (Cumps, Cups, Cuos)
		                                    dscrpcn_cdfccn					udtDescripcion, --Descripción Codificación
		                                    fcha_estmda_entrga				datetime,		--Fecha Entrega
		                                    cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,	--Consecutivo de la Solicitud MEGA
		                                    cnsctvo_srvco_slctdo			udtConsecutivo,  --Consecutivo de la prestación MEGA
		                                    flg_drccnmnto					int default 0,	-->Indicador Direccionamiento
		                                    flg_prgrmcn_entrga				int default 0,	-->Indicador Direccionamiento
               	                           )

	Set @estdo_aprbdo = 7;
	Set @estdo_lqddo  = 9;
	Set @estdo_atrzdo = 11;

	If @nmro_slctd_atrzcn_ss is not null And @cnsctvo_slctd_atrzcn_srvco is null
		Begin
			Select @cnsctvo_slctd_atrzcn_srvco = cnsctvo_slctd_atrzcn_srvco
			From   bdCNA.gsa.tbASSolicitudesAutorizacionServicios WITH(NOLOCK)
			Where  nmro_slctd_atrzcn_ss = @nmro_slctd_atrzcn_ss 
		End

	Insert 
	Into   #tmpDetalleServiciosFechaEntrega(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo         ,
		                                    cnsctvo_cdgo_srvco_slctdo , cnsctvo_cdgo_tpo_srvco_slctdo
	                                       )
	Select @cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo  ,
		   cnsctvo_cdgo_srvco_slctdo  , cnsctvo_cdgo_tpo_srvco
	From   BDCna.gsa.tbASServiciosSolicitados WITH(NOLOCK)
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And    cnsctvo_cdgo_estdo_srvco_slctdo In (@estdo_aprbdo, @estdo_lqddo, @estdo_atrzdo)


	Update     ds
	Set        dscrpcn_tpo_cdfccn = tc.dscrpcn_tpo_cdfccn
	From       #tmpDetalleServiciosFechaEntrega ds
	Inner Join bdSisalud.dbo.tbTipoCodificacion_Vigencias tc WITH(NOLOCK)
	On         tc.cnsctvo_cdgo_tpo_cdfccn = ds.cnsctvo_cdgo_tpo_srvco_slctdo

	Update     ds
	Set        cdgo_cdfccn	  = cd.cdgo_cdfccn,
		       dscrpcn_cdfccn = cd.dscrpcn_cdfccn
	From       #tmpDetalleServiciosFechaEntrega ds
	Inner Join bdSisalud.dbo.tbCodificaciones cd WITH(NOLOCK)
	On         cd.cnsctvo_cdfccn = ds.cnsctvo_cdgo_srvco_slctdo; 

	Update     ds 
	Set        fcha_estmda_entrga = fe.fcha_estmda_entrga
	From       #tmpDetalleServiciosFechaEntrega ds
	Inner Join bdCNA.gsa.tbASResultadoFechaEntrega fe WITH(NOLOCK)
	On         fe.cnsctvo_srvco_slctdo = ds.cnsctvo_srvco_slctdo
	
	--Update     ds
	--Set        flg_drccnmnto = @vlr_uno
	--From       #tmpDetalleServiciosFechaEntrega ds
	--Inner Join BDCna.gsa.tbASResultadoDireccionamiento rd WITH(NOLOCK)
	--On         rd.cnsctvo_srvco_slctdo = ds.cnsctvo_srvco_slctdo

	Update     ds
	Set        flg_prgrmcn_entrga = @vlr_uno
	From       #tmpDetalleServiciosFechaEntrega ds
	Inner Join BDCna.gsa.tbASServiciosSolicitados ss WITH(NOLOCK)
	On         ss.cnsctvo_srvco_slctdo = ds.cnsctvo_srvco_slctdo
	Where      ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto is null


	SELECT  cnsctvo_cdgo_tpo_srvco_slctdo, dscrpcn_tpo_cdfccn , cnsctvo_cdgo_srvco_slctdo,
			cdgo_cdfccn                  , dscrpcn_cdfccn      , fcha_estmda_entrga       ,
			cnsctvo_slctd_atrzcn_srvco   , cnsctvo_srvco_slctdo
	From    #tmpDetalleServiciosFechaEntrega
	Where   flg_prgrmcn_entrga = @vlr_uno

	Drop Table #tmpDetalleServiciosFechaEntrega
END

