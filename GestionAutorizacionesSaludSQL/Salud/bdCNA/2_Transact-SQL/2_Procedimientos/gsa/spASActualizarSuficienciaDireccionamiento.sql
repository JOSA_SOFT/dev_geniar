USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarSuficienciaDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASActualizarSuficienciaDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite actualizar el estado de la suficiencia a los   D\>
					   <\D prestadores seleccionados para las prestaciones							D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarSuficienciaDireccionamiento] 
@es_btch char(1)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @fechaActual datetime = getdate(),
		  @vlor_cro int = 0; --la solicitud no es hospitalaria y prestaciones sin pac

IF EXISTS (SELECT 1 FROM #tmpInformacionSolicitudPrestacion ISP WHERE ISP.mrca_msmo_prstdor=@vlor_cro)
	BEGIN
	  IF EXISTS (
			SELECT  1
			FROM    #tmpPrestadoresDestino PD
			INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
			ON ISP.id = PD.id_slctd_x_prstcn
			WHERE   PD.cnsctvo_drcnmnto_prstdr IS NULL
			AND		ISP.mrca_msmo_prstdor = @vlor_cro
		)
		BEGIN

			DECLARE @anho char(4) = CONVERT(CHAR(4),YEAR(GETDATE()));
			DECLARE @mes  char(2) = CASE WHEN MONTH(@fechaActual) <= 9
											THEN '0' + CONVERT(CHAR(2), MONTH(@fechaActual))
											ELSE CONVERT(CHAR(2), MONTH(@fechaActual))
											END;
			DECLARE @fechaInicialSuficiencia  DATETIME = dbo.fnObtenerPrimerDiaPeriodo(@anho + @mes);
			DECLARE @fechaFinalSuficiencia    DATETIME = dbo.fnObtenerUltimoDiaPeriodo(@anho + @mes);
					
			INSERT INTO bdCNA.sol.tbCNASuficienciaPrestadoresPorMes (
				cnsctvo_drcnmnto_prstdr,    fcha_inco_vgnca,
				fcha_fn_vgnca,              sfcnca_IPS
			)
			SELECT DISTINCT
					isp.cnsctvo_vgnca_drcnmnto_prstdr,    
					@fechaInicialSuficiencia,
					@fechaFinalSuficiencia,
					isp.sfcnca_ips -1
			FROM   #tmpPrestadoresDestino isp
			WHERE  isp.cnsctvo_drcnmnto_prstdr IS NULL;
		END
		ELSE
		BEGIN
		UPDATE  csppm
		SET     sfcnca_IPS =  CASE 
								WHEN isp.sfcnca_orgnl IS NOT NULL 
								THEN ISNULL(isp.sfcnca_ms, isp.sfcnca_ips) - 1
								ELSE isp.sfcnca_ips - 1
							  END
		FROM    bdCNA.sol.tbCNASuficienciaPrestadoresPorMes csppm
					INNER JOIN #tmpPrestadoresDestino isp
						ON csppm.cnsctvo_drcnmnto_prstdr = isp.cnsctvo_vgnca_drcnmnto_prstdr
		WHERE   @fechaActual BETWEEN csppm.fcha_inco_vgnca AND csppm.fcha_fn_vgnca;
		END

		UPDATE  ccp
		SET     cdgo_intrno_dstno           = isp.cdgo_intrno,
				vlr_lqdcn                   = isp.vlr_trfdo,
				cnsctvo_cdgo_dt_lsta_prco   = isp.cnsctvo_cdgo_dt_lsta_prco,
				cnsctvo_drccnmnto_prstdr    = isp.cnsctvo_vgnca_drcnmnto_prstdr,
				fcha_ultma_mdfccn           = @fechaActual
		FROM    bdCNA.sol.tbCNAConceptosPrestaciones ccp
					INNER JOIN #tmpPrestadoresDestino isp
						ON ccp.cnsctvo_slctd_prstcn = isp.cnsctvo_cdfccn;

	END
END

GO
