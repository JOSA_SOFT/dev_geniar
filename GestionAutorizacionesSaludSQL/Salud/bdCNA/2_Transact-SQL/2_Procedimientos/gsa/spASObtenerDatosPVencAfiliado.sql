USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerDatosPVencAfiliado]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spCNAObtenerDatosPVencAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Ejecuta SPs que devuelven datos para Prioridad y ven D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS					AM\>
* Descripcion			 : <\DM	Se modifica el SP para que el campo Nui_Afldo se 
								recupere de la tabla tbBeneficiariosValidador DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/08/12 	FM\>
*---------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASObtenerDatosPVencAfiliado] 
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn udtNumeroIdentificacionLargo,
	@cnsctvo_cdgo_pln udtConsecutivo,
	@plan_atencion varchar(40) output, 
	@tipo_plan varchar(40) output, 
	@empleador varchar(40) output, 
	@riesgo varchar(40) output,  
	@tipo_poblacion varchar(40) output, 
	@legal varchar(40) output, 
	@reputacional varchar(40) output,
	@Nui_AfldoOut integer output,
	@ejecucionMensaje	varchar(40) output
	 
	
AS
BEGIN
Set Nocount On


	Declare @Nui_Afldo	UdtConsecutivo,
			@ms_error varchar(20) = 'error Nui_Afldo',
			@cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo = 23,
			@cnsctvo_prcso				udtConsecutivo,
			@estdo_prcso_ok				numeric(1)  = 2,
			@usr_lgn					varchar(20) = SYSTEM_USER;

	BEGIN
		
		Set @Nui_Afldo = (	select	top 1	nmro_unco_idntfccn_afldo
							From	BDAfiliacionValidador.dbo.tbBeneficiariosValidador	With(NoLock)
							Where	cnsctvo_cdgo_tpo_idntfccn		= @cnsctvo_cdgo_tpo_idntfccn
							And		nmro_idntfccn					= @nmro_idntfccn
							order by fn_vgnca_bnfcro  desc
						 )

		--Se crea el Proceso para la ejecución del proceso Priorización y vencimiento de las actividades..
		EXEC bdcna.dbo.spPRORegistraProceso @cnsctvo_cdgo_tpo_prcso_lna,
											@usr_lgn,
                                            @cnsctvo_prcso OUTPUT
	
		if @Nui_Afldo is not null 
		begin

			Set @Nui_AfldoOut = @Nui_Afldo;
	
			Exec gsa.spASPVencConsultaPlanAtencionAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @Nui_Afldo, @cnsctvo_cdgo_pln,  @plan_atencion output, @tipo_plan output
			Exec gsa.spASPVencConsultaEmpleadorAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @nui_Afldo, @cnsctvo_cdgo_pln, @empleador output
			Exec gsa.spASPVencConsultaRiesgoClinicoAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @Nui_Afldo, @riesgo output
			Exec gsa.spASPVencConsultaTipoPoblacionAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @Nui_Afldo,@cnsctvo_cdgo_pln, @tipo_poblacion output
			Exec gsa.spASPVencConsultaLegalAfiliado @cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn, @Nui_Afldo, @legal output

		end;
		else
		begin
			Set @ejecucionMensaje = @ms_error;
		end
	END

	----Actualizar estado de ejecución el proceso Priorización y vencimiento de las actividades.
	EXEC bdcna.dbo.spActualizaEstadoProceso @cnsctvo_prcso,
		                                                  @cnsctvo_cdgo_tpo_prcso_lna,
                                                          @estdo_prcso_ok	
END
GO
