USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarMasivoProcesoLiquidacion]    Script Date: 15/02/2017 7:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     		: spASEjecutarMasivoProcesoLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Ejecuta el procedimiento orquestador de aplicar
							  la liquidación									D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*----------------------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Juan Carlos Vásquez AM\>
* Descripcion			 : <\DM	Se Modifica Para Incluir En la Liquidación las 
								Solicitudes con Tipo Origen '3-Proceso Automático Ops Virtual' DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM	 @cnsctvo_cdgo_tpo_orgn_slctd3 = '3-Proceso Automático OPS Virtual' VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se Modifica el procedimiento para que tome las solicituds a liquidar teniendo en cuenta, si existe una
								fecha de entrega y un direccionamiento, adicionalmente para que tome los servicios 
								que esten en estado aprobado				
                            DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <VM  \VM	 
* Fecha Modificacion	 : <\FM	2017/01/31 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [gsa].[spASEjecutarMasivoProcesoLiquidacion] 
	@fcha_dia_antrr_lqdcn	DATE,
	@mensaje				VARCHAR(1000) OUTPUT,
	@codigo_error			VARCHAR(100) OUTPUT
AS
BEGIN
  SET NOCOUNT ON;
  BEGIN TRY	

	DECLARE @es_btch						 Udtlogico	= 'S', --El proceso se ejecuta de manera Masiva
			@cdgo_err						 CHAR(5)		= 'ERROR',
			@un_dia							 INT			= 1,
			@fcha_entrga					 DATE,
			@cdgo_ok						 CHAR(2)		= 'OK',			
			@cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo

	-- Declaramos variables a utilizar para recuperar las solicitudes correspondientes a Ops Virtual
	Declare	@fechaactual					datetime = getdate(),
			@fcha_crte_inco					Datetime, -- fecha inicio corte liquidación
			@fechacrtelqdccn				Datetime = getdate(), -- fecha fin corte liquidacion   
			@cnsctvo_cdgo_tpo_orgn_slctd3	int = 3, /*'3-Proceso Automático OPS Virtual con Programacion de Entrega'*/
			@lcCdgo_prmtro_cntdd_mss_frmto  Char(3),   
			@vlr_prmtro_nmrco               Numeric(18,0),
			@vlr_prmtro_crctr               Char(200)    ,
			@vlr_prmtro_fcha                Datetime     ,
			@vsble_usro			            udtLogico    ,
			@tpo_dto_prmtro		            udtLogico      
    
	Set @lcCdgo_prmtro_cntdd_mss_frmto  = '88'
	Set @vsble_usro                     = 'S'
	Set @tpo_dto_prmtro                 = ''
	
	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	Set @fcha_crte_inco   = DATEADD(dd,DATEDIFF(dd,0,@fechaactual),0)
	Set	@fechacrtelqdccn += @vlr_prmtro_nmrco
	Set	@fechacrtelqdccn  = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))
	
	Set @cnsctvo_cdgo_estdo_srvco_slctdo = 7 --Aprobado
	

    CREATE TABLE #tmpnmroslctds (
			id								INT IDENTITY (1, 1),
			cnsctvo_slctd_srvco_sld_rcbda	udtconsecutivo,
			nmro_slctd						udtconsecutivo,
			nmro_slctd_prvdr				udtconsecutivo DEFAULT 0,
			stdo_slctd						INT DEFAULT 0,
			cntrl_prcso						CHAR(1)
	  );	
	  
	SET @fcha_entrga = DATEADD(DAY,@un_dia,@fcha_dia_antrr_lqdcn);	

	BEGIN
	  
		INSERT INTO #tmpnmroslctds
		(
					cnsctvo_slctd_srvco_sld_rcbda,			nmro_slctd_prvdr
		)
		SELECT		SAS.cnsctvo_slctd_atrzcn_srvco, 		SAS.nmro_slctd_prvdr
		FROM		gsa.tbASSolicitudesAutorizacionServicios SAS with(nolock)
		Inner Join  gsa.tbASServiciosSolicitados a with(nolock)
		On          a.cnsctvo_slctd_atrzcn_srvco = SAS.cnsctvo_slctd_atrzcn_srvco
		INNER JOIN  gsa.tbASResultadoFechaEntrega RFE with(nolock)
		ON			SAS.cnsctvo_slctd_atrzcn_srvco = RFE.cnsctvo_slctd_atrzcn_srvco		
		Inner Join  gsa.tbASResultadoDireccionamiento rd with(nolock)
		On          SAS.cnsctvo_slctd_atrzcn_srvco	= rd.cnsctvo_slctd_atrzcn_srvco 
		Where       a.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo
		GROUP BY	SAS.cnsctvo_slctd_atrzcn_srvco, SAS.nmro_slctd_prvdr

		-- Se insertan solicitudes correspondientes a programaciones de entrega para envio de OPS Virtual -- sisjvg01- 2016/12/20
		INSERT INTO #tmpnmroslctds
		(
					cnsctvo_slctd_srvco_sld_rcbda,			nmro_slctd_prvdr
		)
		SELECT		SAS.cnsctvo_slctd_atrzcn_srvco, 		SAS.nmro_slctd_prvdr
		FROM		gsa.tbASSolicitudesAutorizacionServicios SAS with(nolock)
		LEFT JOIN	#tmpnmroslctds t
		ON          t.cnsctvo_slctd_srvco_sld_rcbda = SAS.cnsctvo_slctd_atrzcn_srvco
		WHERE       t.cnsctvo_slctd_srvco_sld_rcbda is null
		AND         SAS.cnsctvo_cdgo_tpo_orgn_slctd = @cnsctvo_cdgo_tpo_orgn_slctd3 
		AND         SAS.fcha_slctd Between @fcha_crte_inco And @fechacrtelqdccn
		GROUP BY	SAS.cnsctvo_slctd_atrzcn_srvco, SAS.nmro_slctd_prvdr;

		--Se ejecuta el SP orquestador
		EXEC gsa.spASProcesoLiquidacion @fcha_entrga, @es_btch, @mensaje OUTPUT, @codigo_error OUTPUT

	END
  END TRY
  BEGIN CATCH
		SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
						'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
						'Message:'   + ERROR_MESSAGE() + CHAR(13) +
						'Procedure:' + ERROR_PROCEDURE();
		SET @codigo_error = @cdgo_err;
  END CATCH
  
  DROP TABLE #tmpnmroslctds;

END;
