USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarSolicitudDiagnostico]    Script Date: 23/05/2017 05:30:36 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarSolicitudDiagnostico
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Diagnóstico de una
					 : solicitud ingresada D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajusta la creación de variables dentro de los Procedimientos 
						para el manejo de los valores constantes? D\>  
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*-----------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\D Se ajusta procedimientos para guardar vigencias de los diagnosticos,
						   control de cambio 068 D\>  
* Nuevas Variables   : <\VM@fn_vgnca VM\>    
* Fecha Modificacion : <\FM 23/05/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGrabarSolicitudDiagnostico] 
AS
  SET NOCOUNT ON
  	DECLARE @Fecha_Actual as datetime,
			@fn_vgnca		Date;

	SET		@Fecha_Actual = Getdate()
	Set		@fn_vgnca = '99991231';
	BEGIN
		-- INSERTAR INFORMACIÓN DE DIAGNOSTICO EN TABLA DE PROCESO tbASDiagnosticosSolicitudAutorizacionServicios 	
		MERGE INTO			 BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios
		USING (	SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco
							,TDI.id
							,TDI.cnsctvo_cdgo_tpo_dgnstco	
							,TDI.cnsctvo_cdgo_dgnstco
							,TDI.cnsctvo_cdgo_cntngnca
							,TDI.cnsctvo_cdgo_rcbro		
							,TDI.usro_crcn
							,TDI.nmro_prstcn					
				FROM		#Tempo_Diagnostico	TDI WITH (NOLOCK)		
				INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
				ON			TDI.id = IDS.IdXML) AS DIA
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,cnsctvo_cdgo_tpo_dgnstco
				,cnsctvo_cdgo_dgnstco
				,cnsctvo_cdgo_cntngnca
				,cnsctvo_cdgo_rcbro		
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn	
				)
		VALUES ( DIA.cnsctvo_slctd_atrzcn_srvco
				,DIA.cnsctvo_cdgo_tpo_dgnstco	
				,DIA.cnsctvo_cdgo_dgnstco	
				,DIA.cnsctvo_cdgo_cntngnca
				,DIA.cnsctvo_cdgo_rcbro
				,@Fecha_Actual	--fcha_crcn	
				,DIA.usro_crcn			
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,DIA.usro_crcn	--usro_ultma_mdfccn
				)
		 OUTPUT	inserted.cnsctvo_dgnstco_slctd_atrzcn_srvco
				,DIA.Id
				,DIA.nmro_prstcn
		 INTO	#IdDiagnostico;
		-- INSERTAR INFORMACIÓN DE DIAGNOSTICO EN TABLA ORIGINAL tbASDiagnosticosSolicitudAutorizacionServiciosOriginal 
		INSERT INTO	 BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal(
					cnsctvo_dgnstco_slctd_atrzcn_srvco
					,cnsctvo_slctd_atrzcn_srvco
					,cdgo_dgnstco
					,cdgo_tpo_dgnstco
					,cdgo_cntngnca
					,cdgo_rcbro	
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDD.cnsctvo_dgnstco_slctd_atrzcn_srvco
					,IDS.cnsctvo_slctd_atrzcn_srvco
					,DIA.cdgo_dgnstco				
					,DIA.cdgo_tpo_dgnstco
					,DIA.cdgo_cntngnca
					,DIA.cdgo_rcbro
					,@Fecha_Actual	--fcha_crcn
					,DIA.usro_crcn
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,DIA.usro_crcn	--usro_ultma_mdfccn			
		FROM		#Tempo_Diagnostico	DIA WITH (NOLOCK)
		INNER JOIN	#IdDiagnostico		IDD WITH (NOLOCK)
		ON			DIA.Id = IDD.idXML
		AND			DIA.nmro_prstcn = IDD.nmro_prstcn
		INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
		ON			DIA.Id = IDS.idXML				  


		Insert Into  bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias
		(
					cnsctvo_dgnstco_slctd_atrzcn_srvco,		cnsctvo_slctd_atrzcn_srvco,
					cnsctvo_cdgo_dgnstco,					inco_vgnca,
					fn_vgnca,								fcha_crcn,
					usro_crcn,								fcha_ultma_mdfccn,
					usro_ultma_mdfccn
		)
		SELECT		 IDD.cnsctvo_dgnstco_slctd_atrzcn_srvco ,IDS.cnsctvo_slctd_atrzcn_srvco
					,DIA.cnsctvo_cdgo_dgnstco				,@Fecha_Actual
					,@fn_vgnca								,@Fecha_Actual	
					,DIA.usro_crcn							,@Fecha_Actual	
					,DIA.usro_crcn	--usro_ultma_mdfccn			
		FROM		#Tempo_Diagnostico	DIA WITH (NOLOCK)
		INNER JOIN	#IdDiagnostico		IDD WITH (NOLOCK)
		ON			DIA.Id = IDD.idXML
		AND			DIA.nmro_prstcn = IDD.nmro_prstcn
		INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
		ON			DIA.Id = IDS.idXML


	END


