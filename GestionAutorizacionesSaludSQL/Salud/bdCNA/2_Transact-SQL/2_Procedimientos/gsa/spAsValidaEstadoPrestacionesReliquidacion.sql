USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spAsValidaEstadoPrestacionesReliquidacion]    Script Date: 10/02/2017 4:04:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spAsValidaEstadoPrestacionesReliquidacion') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spAsValidaEstadoPrestacionesReliquidacion AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spAsValidaEstadoPrestacionesReliquidacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS						 A\>
* Descripcion			: <\D 
								valida si existe alguna prestación con estados que permitan la reliquidación													
                          D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2017/02/10 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spAsValidaEstadoPrestacionesReliquidacion] 393875
AlTER PROCEDURE [gsa].[spAsValidaEstadoPrestacionesReliquidacion] 
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo

AS

BEGIN
	SET NOCOUNT ON;

	Declare @total_registros Int,
			@estdo_aprbdo	 udtConsecutivo,
			@estdo_lqddo	 udtConsecutivo,
			@estdo_atrzdo	 udtConsecutivo
			
	Set @total_registros = 0
	Set @estdo_aprbdo	 = 7
	Set @estdo_lqddo	 = 9
	Set @estdo_atrzdo	 = 11

	
	Select  @total_registros = count(1) 
	From    BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
	Where   ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And     cnsctvo_cdgo_estdo_srvco_slctdo in(@estdo_aprbdo, @estdo_lqddo, @estdo_atrzdo)
		
	Select @total_registros total_registros
END
