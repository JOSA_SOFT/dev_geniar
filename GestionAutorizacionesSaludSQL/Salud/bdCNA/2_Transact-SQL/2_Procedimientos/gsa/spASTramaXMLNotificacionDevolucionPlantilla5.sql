USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASTramaXMLNotificacionDevolucionPlantilla5]    Script Date: 09/03/2017 11:21:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASTramaXMLNotificacionDevolucionPlantilla5
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Construcción trama xml para el envío de notificación de afiliados. Plantilla 2   D\>
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2016-12-29  FC\>    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion        : <\D 
                           Se hace llamado a sp de impresion para recuperar los datos de la devoluvion de la solicitud
						   en la tabla temporal #tempDatosDevolucion, creada para esta consulta.    
						D\>
* Nuevas Variables   : <\VM @cnsctvo_cdgo_estdo_no_cnfrmdd3: 3 - Devuelto
							@cnsctvo_slctd_atrzcn_srvco: guarda el consecutivo de la solicitud VM\>
* Fecha Modificacion : <\FM 2017-02-06 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion        : <\D 
                          Se modifica tipo de dato de los campos fcha_dvlcn y fcha_crcn en la tabla temporal 
						   #tempDatosDevolucion de Date a NVarchar(20) ya que en el procedimiento
						   bdCNA.gsa.spASImpresionFormatoDevuelta retorna estos campos formateados dejandolos de tipo NVarchar
						   y al insertarce en la tabla temporal generaban problemas.
						   
						   Se modifica Insert a tabla temporal #tmpDetallePrestaciones para sacar los datos la tabla 
						   #tempDatosDevolucion y se modifican los update para completar la informacion obteniendola de
						   las tablas bdSisalud.dbo.tbCodificaciones y BDCna.gsa.tbASServiciosSolicitados 
					   D\>
* Nuevas Variables   : <\VM VM\>
* Fecha Modificacion : <\FM 2017-02-13 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>
* Descripcion        : <\DM 
                          Se modifica el procedimiento cambiando el tamaño del camopo infrmcn_adcnl_no_cnfrmdd de las tablas temporales
					   DM\>
* Nuevas Variables   : <\VM VM\>
* Fecha Modificacion : <\FM 2017-03-09 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASTramaXMLNotificacionDevolucionPlantilla5]
	@usrio VARCHAR(50),
	@cnstrccn_trma_xml XML OUTPUT
AS
Begin
		Set NOCOUNT ON;

		DECLARE @anno							VARCHAR(4),
				@mes							VARCHAR(2),
				@dia							VARCHAR(2),
				@hra							VARCHAR(2),
				@mnto							VARCHAR(2),
				@sgndo							VARCHAR(2),
				@string_trama					varchar(4000),
				@cnsctvo_cdgo_estdo_no_cnfrmdd3	UdtConsecutivo,
				@fcha_actl						Date,
				@cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo;
						
	
		Create 
		Table #tmpDetallePrestaciones(cnsctvo_slctd_atrzcn_srvco	 udtConsecutivo,
									  cnsctvo_srvco_slctdo			 udtConsecutivo,
									  cnsctvo_cdgo_srvco_slctdo		 udtConsecutivo,
									  cdgo_cdfccn					 char(11),
									  dscrpcn_cdfccn				 udtDescripcion,
									  dscrpcn_csa_estdo_prstcn_slctd udtDescripcion,
									  cnsctvo_cdgo_csa_ngcn_prstcn	 udtConsecutivo,
									  infrmcn_adcnl_no_cnfrmdd		 Varchar(1000) default ' '
		                             )

		Create 
		Table  #tempDatosDevolucion(dscrpcn_cdd				  udtDescripcion,
									fcha_dvlcn				  NVarchar(20),
									nmbre_cmplto			  UdtDescripcion,
									fcha_crcn				  NVarchar(20),
									cdgo_cdfccn				  Varchar(11),
									dscrpcn_cdfccn			  UdtDescripcion,
									dscrpcn_rgla_vldcn		  UdtDescripcion,
									infrmcn_adcnl_no_cnfrmdd  Varchar(1000) ,
									dscrpcn_mnsje			  UdtDescripcion,
									email					  UdtEmail,
									orgn_dvlcn				  UdtDescripcion
		                           )
		
		Set	@anno	= CONVERT(CHAR(4),DATEPART(YEAR,GETDATE()))
		Set	@mes	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(MONTH,GETDATE())))),2)
		Set	@dia	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(DAY,GETDATE())))),2)
		Set	@hra	= CONVERT(CHAR(2),DATEPART(HOUR,GETDATE()))
		Set	@mnto	= CONVERT(CHAR(2),DATEPART(MINUTE,GETDATE()))
		Set	@sgndo	= CONVERT(CHAR(2),DATEPART(SECOND,GETDATE()))
		Set @cnsctvo_cdgo_estdo_no_cnfrmdd3 = 3;
		Set @fcha_actl = getDate();
	
	
		Select	@cnsctvo_slctd_atrzcn_srvco = cnsctvo_slctd_srvco_sld_rcbda From #tmpAflds


		--Se recuperan los datos de la devolucion
		Insert 
		Into   #tempDatosDevolucion(dscrpcn_cdd       ,	fcha_dvlcn              , nmbre_cmplto  ,
									fcha_crcn         ,	cdgo_cdfccn             , dscrpcn_cdfccn,
									dscrpcn_rgla_vldcn,	infrmcn_adcnl_no_cnfrmdd, dscrpcn_mnsje ,
									email             , orgn_dvlcn
									)
		Exec bdCNA.gsa.spASImpresionFormatoDevuelta @cnsctvo_slctd_atrzcn_srvco;

		-- Se llena la tabla temporal de  prestaciones.
	
		Insert 
		Into   #tmpDetallePrestaciones(cnsctvo_slctd_atrzcn_srvco    , cdgo_cdfccn             , dscrpcn_cdfccn,		
					                   dscrpcn_csa_estdo_prstcn_slctd, infrmcn_adcnl_no_cnfrmdd
		                              )
		Select		@cnsctvo_slctd_atrzcn_srvco, cdgo_cdfccn             , dscrpcn_cdfccn,		
					dscrpcn_rgla_vldcn         , infrmcn_adcnl_no_cnfrmdd
		From		#tempDatosDevolucion
			
		---- Se obtiene la informacion de la prestacion.
		Update		p
		Set			cnsctvo_cdgo_srvco_slctdo		= ss.cnsctvo_cdgo_srvco_slctdo,
					dscrpcn_cdfccn	= c.dscrpcn_cdfccn
		From		#tmpDetallePrestaciones p	
		Inner Join	bdSisalud.dbo.tbCodificaciones c	With(NoLock)
		ON			c.cdgo_cdfccn = p.cdgo_cdfccn
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdfccn
		And			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco

		Update		p
		Set			cnsctvo_cdgo_csa_ngcn_prstcn = ss.cnsctvo_cdgo_estdo_srvco_slctdo,
					cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		From		#tmpDetallePrestaciones p	
		Inner Join	bdSisalud.dbo.tbCodificaciones c	With(NoLock)
		ON			c.cdgo_cdfccn = p.cdgo_cdfccn
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_cdgo_srvco_slctdo = c.cnsctvo_cdfccn
		And			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	
	
		Set @cnstrccn_trma_xml = (
			Select		TSO.id_sld_rcbda	id,
						TPR.crreo			correo,
						TAF.eml				co,
						TAF.bcc				bcc,
						TSO.plntlla			'plantilla',
						(
							Select	
									'nmbre_cdd_ips_prmria_afldo'		'parametro/nombre',
									AFI.dscrpcn_sde						'parametro/valor',
									NULL,
									'dia'								'parametro/nombre',
									@dia								'parametro/valor',
									NULL,
									'mes'								'parametro/nombre',
									@mes								'parametro/valor',
									NULL,
									'ano'								'parametro/nombre',
									@anno								'parametro/valor',
									NULL,
									'nmbre_afldo'						'parametro/nombre',
									AFI.nmbre_afldo						'parametro/valor',
									NULL,
									'tbla_prstcns'						'parametro_1/nombre',
										(Select
											cdgo_cdfccn					'td',
											null,
											dscrpcn_cdfccn				'td',
											null,
											dscrpcn_csa_estdo_prstcn_slctd 'td',
											null,
											infrmcn_adcnl_no_cnfrmdd	'td',
											null
										From #tmpDetallePrestaciones
										Where cnsctvo_cdgo_csa_ngcn_prstcn IS NOT NULL
										FOR XML PATH('tr'), TYPE)	'parametro_2/valor/tbody',
									NULL,
									'usrio'								'parametro/nombre',
									RTRIM(LTRIM(@usrio))				'parametro/valor',
									NULL,
									'lnea_ncnal'						'parametro/nombre',
									TAF.lnea_ncnal						'parametro/valor',
									NULL,
									'crreo_atncn_clnte'					'parametro/nombre',
									TAF.crreo_atncn_clnte				'parametro/valor'
							From #tmpAflds AFI
							Where AFI.id = TAF.id
							FOR XML PATH('parametros'), TYPE
						),
						@dia		'fecha/dia',
						@hra		'fecha/hora',
						@mnto		'fecha/minutos',
						@mes		'fecha/mes',
						@sgndo		'fecha/segundos',
						@anno		'fecha/ano',
						RTRIM(LTRIM(@usrio)) 'usuario'					
			From       #tmpAflds TAF
			Inner Join #tmpSlctdes TSO
			ON         TAF.id_sld_rcbda=TSO.id_sld_rcbda
			Inner Join #tmpPrvdor TPR
			ON         TSO.id_sld_rcbda = TPR.id_sld_rcbda
			FOR XML PATH('mensajeCorreo')	
		)
		
		Set @string_trama = cast (@cnstrccn_trma_xml as VARCHAR(4000))

		If Exists(
					Select		TOP 1 1 
					From		#tmpDetallePrestaciones 
					Where		cnsctvo_cdgo_csa_ngcn_prstcn IS NOT NULL
					)
			Begin
				Set @string_trama = (Select REPLACE(@string_trama,'</parametro_1>',''));  
				Set @string_trama = (Select REPLACE(@string_trama,'<parametro_2>',''));
				Set @string_trama = (Select REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">'));
				Set @string_trama = (Select REPLACE(@string_trama,'</parametro_2>','</parametro>'));
			End
		Else
			Begin
				Set @string_trama = (Select REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">')); 
				Set @string_trama = (Select REPLACE(@string_trama,'</parametro_1>','</parametro>')); 
			End

		Set @cnstrccn_trma_xml = CONVERT(XML, @string_trama);

		Drop Table #tempDatosDevolucion
		Drop Table #tmpDetallePrestaciones
End
