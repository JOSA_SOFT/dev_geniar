USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudSolicitudes]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudSolicitudes
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite Modificar información de solicitudes   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P	P\>    
* Variables          : <\V  V\>    
* Fecha Creacion  	 : <\FC 07/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudSolicitudes] 
AS
  SET NOCOUNT ON
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASSolicitudesAutorizacionServiciosOriginal
		DELETE			SOO
		FROM			BdCNA.gsa.tbASSolicitudesAutorizacionServiciosOriginal SOO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				SOO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- MODIFICAR INFORMACIÓN DE SOLICITUDES EN TABLA DE PROCESO tbASSolicitudesAutorizacionServicios 	
		SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; 
		BEGIN TRAN 
			UPDATE	BdCNA.gsa.tbASSolicitudesAutorizacionServicios With (ROWLOCK)
			SET		 cnsctvo_cdgo_orgn_atncn			= TSO.cnsctvo_cdgo_orgn_atncn		
					,cnsctvo_cdgo_tpo_srvco_slctdo		= TSO.cnsctvo_cdgo_tpo_srvco_slctdo
					,cnsctvo_cdgo_prrdd_atncn			= TSO.cnsctvo_cdgo_prrdd_atncn			
					,cnsctvo_cdgo_tpo_ubccn_pcnte		= TSO.cnsctvo_cdgo_tpo_ubccn_pcnte		
					,cnsctvo_cdgo_tpo_slctd				= TSO.cnsctvo_cdgo_tpo_slctd				
					,jstfccn_clnca						= TSO.jstfccn_clnca						
					,cnsctvo_cdgo_mdo_cntcto_slctd		= TSO.cnsctvo_cdgo_mdo_cntcto_slctd		
					,cnsctvo_cdgo_tpo_trnsccn_srvco_sld = TSO.cnsctvo_cdgo_tpo_trnsccn_srvco_sld	
					,nmro_slctd_prvdr					= TSO.nmro_slctd_prvdr					
					,nmro_slctd_pdre					= TSO.nmro_slctd_pdre					
					,cdgo_eps							= TSO.cdgo_eps							
					,cnsctvo_cdgo_clse_atncn			= TSO.cnsctvo_cdgo_clse_atncn			
					,cnsctvo_cdgo_frma_atncn			= TSO.cnsctvo_cdgo_frma_atncn			
					,fcha_slctd							= TSO.fcha_slctd							
					,nmro_slctd_ss_rmplzo				= TSO.nmro_slctd_ss_rmplzo			
					,cnsctvo_cdgo_ofcna_atrzcn			= TSO.cnsctvo_cdgo_ofcna_atrzcn			
					,nuam								= TSO.nuam								
					,srvco_hsptlzcn						= TSO.srvco_hsptlzcn						
					,ga_atncn							= TSO.ga_atncn							
					,cnsctvo_prcso_vldcn				= TSO.cnsctvo_prcso_vldcn				
					,cnsctvo_prcso_mgrcn				= TSO.cnsctvo_prcso_mgrcn				
					,mgrda_gstn							= TSO.mgrda_gstn	
					,spra_tpe_st						= TSO.spra_tpe_st
					,dmi								= TSO.dmi
					,obsrvcn_adcnl						= TSO.obsrvcn_adcnl
					,fcha_ultma_mdfccn					= @Fecha_Actual
					,usro_crcn							= TSO.usro_crcn
			FROM	#TMod_Solicitudes					  TSO WITH (NOLOCK)
			WHERE	cnsctvo_slctd_atrzcn_srvco			= TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		If @@ROWCOUNT > 0 
		BEGIN
			COMMIT TRAN 
		END
		ELSE
		BEGIN
			ROLLBACK TRAN
		END
		-- INSERTAR INFORMACIÓN DE SOLICITUDES EN TABLA ORIGINAL tbASSolicitudesAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASSolicitudesAutorizacionServiciosOriginal(
					 cnsctvo_slctd_atrzcn_srvco
					,cdgo_orgn_atncn
					,cdgo_tpo_srvco_slctdo
					,cdgo_prrdd_atncn
					,cdgo_tpo_ubccn_pcnte
					,jstfccn_clnca
					,nmro_slctd_prvdr
					,nmro_slctd_pdre
					,cdgo_eps
					,cdgo_clse_atncn
					,cdgo_frma_atncn
					,fcha_slctd
					,nmro_slctd_ss_rmplzo
					,nuam
					,srvco_hsptlzcn
					,ga_atncn
					,mgrda_gstn
					,cdgo_mdo_cntcto_slctd
					,cdgo_tpo_trnsccn_srvco_sld
					,cdgo_estdo_slctd
					,obsrvcn_adcnl
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)		
		SELECT		 SOL.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,SOL.cdgo_orgn_atncn	
					,SOL.cdgo_tpo_srvco_slctdo
					,SOL.cdgo_prrdd_atncn		
					,SOL.cdgo_tpo_ubccn_pcnte	
					,SOL.jstfccn_clnca
					,SOL.nmro_slctd_prvdr
					,SOL.nmro_slctd_pdre
					,SOL.cdgo_eps
					,SOL.cdgo_clse_atncn		
					,SOL.cdgo_frma_atncn	
					,SOL.fcha_slctd	
					,SOL.nmro_slctd_ss_rmplzo
					,SOL.nuam
					,SOL.srvco_hsptlzcn
					,SOL.ga_atncn
					,SOL.mgrda_gstn
					,SOL.cdgo_mdo_cntcto_slctd								
					,SOL.cdgo_tpo_trnsccn_srvco_sld	
					,''				--cdgo_estdo_slctd	
					,obsrvcn_adcnl
					,@Fecha_Actual	--fcha_crcn
					,SOL.usro_crcn		
					,@Fecha_Actual	--fcha_ultma_mdfccn	
					,SOL.usro_crcn	--usro_ultma_mdfccn	
		FROM		#TMod_Solicitudes	SOL WITH (NOLOCK)		
	END

GO
