USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarGenerarNumeroUnicoOpsMasivo]    Script Date: 21/06/2017 08:52:27 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarGenerarNumeroUnicoOpsMasivo												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Controlador Para ejecución Masiva o por demanda de la generación Número Unico Ops Mega	D\>  												
* Observaciones		: <\O O\> 1- si es por demanda se deben enviar los parámetros del contrato 
									@cnsctvo_slctd_atrzcn_srvco, @lcUsrioAplcn
							  2- si es masivo se deben enviar los parámetros del contrato en null 													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/21																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez G	AM\>  													
* Descripcion			: <\DM Se Modifica sp para incluir el filtro de fecha de entrega <= a la fecha actual DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/06/15	FM\>  													
*-----------------------------------------------------------------------------------------------------------------------  															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez G	AM\>  													
* Descripcion			: <\DM se incluye manejo de Transacción para el proceso masivo DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/17	FM\>  													
*-----------------------------------------------------------------------------------------------------------------------										
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Victor Hugo Gil Ramos AM\>  													
* Descripcion			: <\DM 
                               Se actualiza el procedimiento cambiando el llamado sp que crea el proceso 
							   spPRORegistraLogEventoProceso por spRegistraLogEventoProceso 
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM 
                               Se agrega validacion para el numero de ops por demanda. 
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/03 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM 
                               Se quita mensaje de la validacion inicial de forma que si la prestacion ya tiene numero de OPS 
							   no muestre mensaje y se siga el proceso.
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/22 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM 
                              Se agrega campo cnsctvo_cdgo_rcbro a la tabla temporal #tbConceptos, control de cambio 068
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

/* 
Declare @mnsjertrno					 UdtDescripcion,
		@cdgortrno					 varchar(2)
--exec bdcna.gsa.spASEjecutarGenerarNumeroUnicoOpsMasivo null, null, @mnsjertrno output, @cdgortrno output
exec bdcna.gsa.spASEjecutarGenerarNumeroUnicoOpsMasivo 2104, 'sisjvg01', @mnsjertrno output, @cdgortrno output

Select @cdgortrno, @mnsjertrno

*/

ALTER PROCEDURE [gsa].[spASEjecutarGenerarNumeroUnicoOpsMasivo]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null,
/*02*/  @lcUsrioAplcn				 UdtUsuario = null,
/*03*/  @mnsjertrno					 UdtDescripcion = null output,
/*04*/  @cdgortrno					 varchar(2) = null output

AS

Begin
	SET NOCOUNT ON

	Declare @lcUsuario				                  UdtUsuario     = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@tpo_prcso				                  Int            = 32, -- 'Proceso Automático Generación Número Unico OPS Masivo - MEGA'
			@cnsctvo_prcso			                  Int            = 0,
			@cnsctvo_cdgo_tpo_incnsstnca              Int            = 1,
			@prcso_extso			                  Int            = 2,
			@codigoExito			                  Char(2)        = 0,
			@codigoError			                  Char(2)        = -1,
			@codigoSalir			                  Char(2)        = -2,
			@estdo_pso1				                  Int            = 0,  /*Estado paso 1 */
			@estdo_pso2				                  Int            = 0,  /*Estado paso 2 */
			@estdo_pso3				                  Int            = 0,  /*Estado paso 3 */
			@tpo_rgstro_log_93		                  Int            = 93,
			@tpo_rgstro_log_94		                  Int            = 94,
			@tpo_rgstro_log_146		                  Int            = 146,
			@mensajeError			                  Varchar(2000)  = '',
			@cnsctvo_lg				                  Int            = 0,
			@cnsctvo_estdo_err		                  udtConsecutivo = 3,			
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9  UdtConsecutivo = 9, -- 'Estado Concepto '9-Liquidada'
			@cnsctvo_cdgo_estdo_srvco_slctdo9         UdtConsecutivo = 9,-- Estado Servicio '9-Liquidado'
			@ValorCERO				                  Int            = 0,
			@prmtrzcn_n_ds_ants_fcha_entrga           Int            = 0,
			@cnsctvo_cdgo_rgla				          Int           , -- Número de Días a la fecha de entrega
			@fechaactual					          Datetime      ,
			@fcha_entrga_n_ds_ants			          Datetime      ,
			@fcha_entrga_dia_sgnte			          Datetime      ,
	        @mnsje_lg_evnto_prcso	                  Varchar(500)  ,
			@estdo_en_prcso                           Int           ,
			@cnsctvo_rgstro_lg_evnto_x_prcso          udtConsecutivo
	
	Set @estdo_en_prcso        = 1
	Set @cnsctvo_cdgo_rgla     = 155
	Set @fechaactual           = getDate()
	Set @fcha_entrga_dia_sgnte = getDate()
	Set @fcha_entrga_dia_sgnte += 1

	If @lcUsrioAplcn is not null
	   Begin
	     Set @lcUsuario = @lcUsrioAplcn
	   End 

	-- Recuperamos la cantidad de días antes de la fecha de entrega
	Select	@prmtrzcn_n_ds_ants_fcha_entrga = vlr_rgla 
    From	bdSisalud.dbo.tbReglas_Vigencias with(nolock)
    Where	cnsctvo_cdgo_rgla = @cnsctvo_cdgo_rgla
    And		@fechaactual BETWEEN inco_vgnca AND fn_vgnca

    Set @fcha_entrga_n_ds_ants = DATEADD(DAY,-@prmtrzcn_n_ds_ants_fcha_entrga,@fcha_entrga_dia_sgnte);
	
	-- Validamos si existen conceptos de servicios por asignar numero unico ops 

	-- Para un caso especifico
	If(@cnsctvo_slctd_atrzcn_srvco Is Not Null)-- qvisionclr 2017/01/03
	Begin
		-- Se validan los conceptos para los procedimientos-insumos	
		If(Not Exists	(	Select Top 1 1
							From		gsa.tbASServiciosSolicitados ss With(NoLock)
							Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
							On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
							Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
							On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
							Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
							And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9
							And			css.nmro_unco_ops = @ValorCERO
							Union
							Select Top 1 1 
							From		gsa.tbASServiciosSolicitados ss With(NoLock)
							Inner Join	gsa.tbASMedicamentosSolicitados ms With(NoLock)
							On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
							Inner Join	gsa.tbASConceptosServicioSolicitado css With(NoLock)
							On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
							Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
							And			ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9
							And			css.nmro_unco_ops = @ValorCERO))
		Begin
			--Set @mnsjertrno = 'No hay Datos Para Generar Número Unico OPS Se Cancela El Proceso'
			--Set @cdgortrno = 'ET'
		    ---Select @mnsjertrno, @cdgortrno
			Return	
		End
	End
	Else --Para proceso masivo
	if not exists	(   select	1 
						from	gsa.tbASConceptosServicioSolicitado	css with(nolock)
						Where	css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo9 /* '9-Liquidada' */	
						And		css.nmro_unco_ops = @ValorCERO
						And		css.cnsctvo_cdgo_grpo_imprsn > @ValorCERO
					)

				Begin
					Set @mnsjertrno = 'No hay Datos Para Generar Número Unico OPS Se Cancela El Proceso'
					Set @cdgortrno = 'ET'
					Return
				End
	else
		Begin
		if not exists ( select		1 
						from		gsa.tbASServiciosSolicitados	ss with(nolock)
						inner join  gsa.tbASResultadoFechaEntrega rfe with(nolock)
						On			rfe.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
						And			rfe.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
						Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo9 /* '9-Liquidada' */	
						And			cast(isnull(rfe.fcha_rl_entrga,rfe.fcha_estmda_entrga) as date) <= cast(@fcha_entrga_n_ds_ants as date)  )
				 Begin
					--Set @mnsjertrno = 'No hay Datos Para Generar Número Unico OPS Se Cancela El Proceso'
					--Set @cdgortrno = 'OK'
					Return
				End
		End	

	-- Creamos tabla temporal para insertar los registros de la tbASConceptosServicioSolicitado y actualizarlo con el número único OPS
	Create table #tbConceptosOps
	(
		id									int identity,
		cnsctvo_slctd_atrzcn_srvco 			UdtConsecutivo,
		cnsctvo_srvco_slctdo				UdtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo		UdtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo		UdtConsecutivo,
		cnsctvo_mdcmnto_slctdo				UdtConsecutivo,		
		cnsctvo_prstcn_slctda				UdtConsecutivo,
		grpo_imprsn							UdtConsecutivo,
		cdgo_intrno							UdtCodigoIps,
		cnsctvo_cdgo_cncpto_gsto			UdtConsecutivo,
		nmro_unco_ops						UdtConsecutivo,
		ds_vldz								UdtConsecutivo default  0,
		nmro_unco_prncpl_cta_rcprcn			UdtLogico default 'N',
		agrpdr_imprsn						UdtLogico default 'N',
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	UdtConsecutivo,
		cnsctvo_cdgo_rcbro					UdtConsecutivo

	)
	if @cnsctvo_slctd_atrzcn_srvco is null -- si el proceso es masivo se aplica control de transacciones
		Begin
			Begin Try
			-- Registramos el inicio proceso automático log de procesos 'Proceso Automático Generación Número Unico OPS Masivo - MEGA'
			exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output

			-- Paso 1. Poblar temporales 
			Begin Try
			    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 1
				set  @mnsje_lg_evnto_prcso = 'Paso 1. Inserción a tablas TMP - gsa.tbASConceptosServicioSolicitado - MEGA'
				exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_93, @estdo_en_prcso, @cnsctvo_lg output
				exec gsa.spASPoblarGenerarNumeroUnicoOpsMasivo @cnsctvo_slctd_atrzcn_srvco
				exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			End Try
			
			Begin Catch
				-- error en paso 1
				select	@estdo_pso1 = @codigoError,
						@estdo_pso2 = @codigoSalir;
				throw
			End Catch
			
			-- Paso 2. Generar Número Unico OPS
			Begin Try
				
				Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 2
				set  @mnsje_lg_evnto_prcso = 'Paso 2. Generar Número Unico OPS - MEGA'				
				exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_94, @estdo_en_prcso, @cnsctvo_lg output
				exec gsa.spASGenerarNumeroUnicoOpsMasivo @lcUsrioAplcn
				exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg			
				
				exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso				
				
			End Try
			Begin Catch
				-- error en paso 2
				select	@estdo_pso2 = @codigoError,
						@estdo_pso3 = @codigoSalir;
				throw
			End Catch

			-- Paso 3. Grabar a Tablas Definitivas el Número Unico OPS
			Begin Transaction generar_nmro_unico_ops
			Begin Try
				Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 3
				set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar a Tablas Definitivas Numero Unico OPS - MEGA'
				exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_146, @estdo_en_prcso, @cnsctvo_lg output
				exec gsa.spASGrabarGenerarNumeroUnicoOpsMasivo @lcUsrioAplcn
				exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
				
				exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso				
				
			End Try
			Begin Catch
				-- error en paso 2
				select	@estdo_pso3 = @codigoError;
				throw
			End Catch

			-- Registramos el fin del proceso Automático log de procesos 'Proceso Automático Generación Número Unico OPS Masivo - MEGA'
			exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
			set @cdgortrno = 'OK'
			set @mnsjertrno = 'El Proceso Generar Número Unico OPS se realizo con Exito'
			commit transaction generar_nmro_unico_ops
			
	End Try
	Begin Catch
		-- El proceso se ejecuto con error
		Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
		-- Paso 1
		if @estdo_pso1 = @codigoError
			Begin
				Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
				Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
				Set @mnsjertrno = 'Paso 1. Se Presento un Error Inserción a tablas TMP Generar Numero Unico OPS - MEGA'
				Set @cdgortrno = 'ET'
				return
			End
		-- Paso 2
		if @estdo_pso2 = @codigoError
			Begin
				Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
				Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
				Set @mnsjertrno = 'Paso 2. Se Presento un Error  al Generar Número Unico OPS - MEGA '
				Set @cdgortrno = 'ET'
				return
			End
		-- Paso 3
				RollBack transaction generar_nmro_unico_ops
				Set @cnsctvo_rgstro_lg_evnto_x_prcso = 3
				set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar a Tablas Definitivas Numero Unico OPS - MEGA'

				exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_94, @estdo_en_prcso, @cnsctvo_lg output
							
				if @estdo_pso3 = @codigoExito
					Begin
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End
				if @estdo_pso3 = @codigoError
					Begin
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						Set @mnsjertrno = 'Paso 3. Grabar a Tablas Definitivas Numero Unico OPS - MEGA'
						Set @cdgortrno = 'ET'
						return
					End
	End Catch
	
	End

	Else
		Begin
			-- Paso 1. Poblar temporales 
			exec gsa.spASPoblarGenerarNumeroUnicoOpsMasivo @cnsctvo_slctd_atrzcn_srvco
			
			if not exists(select 1 
						from #tbConceptosOps)
				Begin
					set @cdgortrno = 'ET'
					set @mnsjertrno = 'No Se Genero Número Unico OPS'
					
				End
			else
				Begin
					-- Paso 2. Generar Número Unicio OPS
					exec gsa.spASGenerarNumeroUnicoOpsMasivo @lcUsrioAplcn	

					If	@@error	<> 0
						Begin
							Set @cdgortrno = 'ET'
							set @mnsjertrno = 'Ocurrio un Error al Generar Número Unico OPS'
						End
					else
						Begin
							-- Paso 3. Grabar Generar Número Unicio OPS
							exec gsa.spASGrabarGenerarNumeroUnicoOpsMasivo @lcUsrioAplcn
							
							set @cdgortrno = 'OK'
							set @mnsjertrno = 'El Proceso Generar Número Unico OPS se realizo con Exito'
							
							If	@@error	<> 0
								Begin
									Set @cdgortrno = 'ET'
									set @mnsjertrno = 'Ocurrio un Error al Generar Número Unico OPS'
								End	
					    End			 
				End
	End
	
	Drop table #tbConceptosOps

End

