USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosNotificacionPrestacionesBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosNotificacionPrestacionesBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/05/31										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosNotificacionPrestacionesBPM] 
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@estdo_prstcn					int --1=APROBADAS|2=DEVOLUCION/NO AUTORIZADA
AS
BEGIN
	
	Create table #tmpEstadoNotificacion(
		cnsctvo_cdgo_estdo		udtConsecutivo,
		dscrpcn_estdo			udtDescripcion
	)

	Declare @countEstadoFechaEntregaDia	int = 0,
			@countEstadoAuditoria		int = 0,
			@fcha_vldcn					datetime,
			@fcha_vgnca					datetime = getDate();

	SET NOCOUNT ON;
	If @estdo_prstcn = 1
	Begin
		Set @fcha_vldcn = getDate();
		Insert Into #tmpEstadoNotificacion( cnsctvo_cdgo_estdo, dscrpcn_estdo)
		Values(7, 'APROBADA');

		Set @countEstadoFechaEntregaDia = 
								 (SELECT count(1)
								  FROM BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
								   Inner Join BDCna.gsa.tbASResultadoFechaEntrega rfe WITH (NOLOCK) On ss.cnsctvo_slctd_atrzcn_srvco = rfe.cnsctvo_slctd_atrzcn_srvco And ss.cnsctvo_srvco_slctdo = rfe.cnsctvo_srvco_slctdo
								   Inner Join #tmpEstadoNotificacion en on  ss.cnsctvo_cdgo_estdo_srvco_slctdo = en.cnsctvo_cdgo_estdo
								  where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
								   And rfe.fcha_estmda_entrga <= @fcha_vldcn 
								  );
		End
	If @estdo_prstcn = 2
	Begin
		Set @fcha_vldcn = '99991231';
		Insert Into #tmpEstadoNotificacion( cnsctvo_cdgo_estdo, dscrpcn_estdo)Values(6, 'DEVUELTA');
		Insert Into #tmpEstadoNotificacion( cnsctvo_cdgo_estdo, dscrpcn_estdo)Values(8, 'NO AUTORIZADA');

		Set @countEstadoFechaEntregaDia = 
								 (SELECT count(1)
								  FROM BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
								   Inner Join #tmpEstadoNotificacion en on  ss.cnsctvo_cdgo_estdo_srvco_slctdo = en.cnsctvo_cdgo_estdo
								  where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
								 );
	End	    
		
	Select @countEstadoAuditoria countEstadoAuditoria, @countEstadoFechaEntregaDia countEstadoFechaEntregaDia

	Drop table #tmpEstadoNotificacion;
END

GO
