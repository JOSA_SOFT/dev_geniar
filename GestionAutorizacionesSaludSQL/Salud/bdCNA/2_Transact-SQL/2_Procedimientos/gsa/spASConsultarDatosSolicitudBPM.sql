USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosSolicitudBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosSolicitudBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					   A\>
* Descripcion			: <\D Ejecuta SPs que devuelven los datos de la solicitud  D\>
						  <\D requeridos por el proceso del BPM					   D\>
* Observaciones			: <\O  													   O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/02/20										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge RodriguezAM\>
* Descripcion			 : <\DM	Se ajusta SP para que devuelva la la sede del afiliado
								Para mostrarlo en pantalla DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016-12-07 FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosSolicitudBPM] 
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@nmro_isntnca_prcso	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

		Declare @fcha_vldccn					datetime = Convert(char(10),getDate(),111),
				@cnsctvo_cdgo_estdo_slctd		udtConsecutivo,
				@dscrpcn_estdo_srvco_slctdo		udtDescripcion,
				@vlr_uno						int = 1,
				@sde_afldo						int,
				@dscrpcn_sde					udtDescripcion;

		Create table #DatosSolicitudBpm(
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			nmro_slctd_ss_rmplzo			varchar(50), 
			cnsctvo_cdgo_mdo_cntcto_slctd	udtConsecutivo,
			dscrpcn_mdo_cntcto				udtDescripcion,
			cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo,
			cdgo_tpo_idntfccn				char(3),
			nmro_idntfccn_afldo				udtNumeroIdentificacionLargo,
			nmro_unco_idntfccn_afldo		udtConsecutivo,
			cnsctvo_cdgo_pln				udtConsecutivo,
			fcha_slctd						datetime,
			cnsctvo_cdgo_estdo_slctd		udtConsecutivo,
			dscrpcn_estdo_srvco_slctdo		udtDescripcion,
			indcdr_drccnmnto				int default 0,
			indcdr_fcha_entrga				int default 0,
	
		)

		
		--Se obitene el consecutivo de la sede del afiliado.
		Set @sde_afldo = (Select IAS.cnsctvo_cdgo_sde_ips_prmra From BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios IAS With(NoLock)
		Where IAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco);

		Set @dscrpcn_sde = (Select dscrpcn_sde 
							FROM BDAfiliacionValidador.dbo.tbSedes_Vigencias s
							where cnsctvo_cdgo_sde = @sde_afldo
							and   @fcha_vldccn between s.inco_vgnca and s.fn_vgnca)


		--Se actualiza la instacia del proceso en el registro de la solicitud de servicio
		UPDATE sas SET nmro_prcso_instnca = @nmro_isntnca_prcso
		FROM BDCna.gsa.tbASSolicitudesAutorizacionServicios sas
		WHERE sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

		Insert into #DatosSolicitudBpm(
			cnsctvo_slctd_atrzcn_srvco,
			nmro_slctd_ss_rmplzo, 
			cnsctvo_cdgo_mdo_cntcto_slctd,
			dscrpcn_mdo_cntcto,
			cnsctvo_cdgo_tpo_idntfccn_afldo,
			cdgo_tpo_idntfccn,
			nmro_idntfccn_afldo,
			nmro_unco_idntfccn_afldo,
			cnsctvo_cdgo_pln,
			fcha_slctd,
			cnsctvo_cdgo_estdo_slctd,
			dscrpcn_estdo_srvco_slctdo
		)
		SELECT 
			@cnsctvo_slctd_atrzcn_srvco			 consecutivoSolicitud,
			sol.nmro_slctd_atrzcn_ss             numeroSolicitud, 
			sol.cnsctvo_cdgo_mdo_cntcto_slctd    cnsOrigenSolicitud,
			mcontac.dscrpcn_mdo_cntcto			 origenSolicitud,
			afil.cnsctvo_cdgo_tpo_idntfccn_afldo cnsTipoDocumento,
			ti.cdgo_tpo_idntfccn				 desTipoDocumento,
			afil.nmro_idntfccn_afldo             numeroIdentificacion,
			afil.nmro_unco_idntfccn_afldo        numeroUnicoIdentificacion,
			afil.cnsctvo_cdgo_pln                cnsCodigoPlan,
			sol.fcha_slctd						 fechaRadicacion,
			sol.cnsctvo_cdgo_estdo_slctd		 consecutivoEstado,
			ess.dscrpcn_estdo_srvco_slctdo		 estadoSolicitud
		FROM  BDCna.gsa.tbasSolicitudesAutorizacionServicios sol With(NoLock)
		inner join BDCna.gsa.tbAsInformacionAfiliadoSolicitudAutorizacionServicios afil With(NoLock) on sol.cnsctvo_slctd_atrzcn_srvco = afil.cnsctvo_slctd_atrzcn_srvco
		inner join BDCna.prm.tbASMediosContacto_Vigencias mcontac With(NoLock) on sol.cnsctvo_cdgo_mdo_cntcto_slctd = mcontac.cnsctvo_cdgo_mdo_cntcto
		inner join BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias ti With(NoLock) on ti.cnsctvo_cdgo_tpo_idntfccn = afil.cnsctvo_cdgo_tpo_idntfccn_afldo
		inner join BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias ess With(NoLock) on ess.cnsctvo_cdgo_estdo_srvco_slctdo = sol.cnsctvo_cdgo_estdo_slctd
		WHERE sol.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
   		And   @fcha_vldccn between mcontac.inco_vgnca and mcontac.fn_vgnca
		And   @fcha_vldccn between ti.inco_vgnca and ti.fn_vgnca
		And   @fcha_vldccn between ess.inco_vgnca and ess.fn_vgnca

		
		If	@cnsctvo_cdgo_estdo_slctd Is Not Null
		Begin
			Update ds 
			Set	   cnsctvo_cdgo_estdo_slctd	= @cnsctvo_cdgo_estdo_slctd,
				   dscrpcn_estdo_srvco_slctdo	= @dscrpcn_estdo_srvco_slctdo
			From   #DatosSolicitudBpm ds
		End

		Update     ds 
		Set        indcdr_drccnmnto = @vlr_uno
		From       #DatosSolicitudBpm ds
		Inner Join BDCna.gsa.tbASResultadoDireccionamiento rd With(NoLock) on ds.cnsctvo_slctd_atrzcn_srvco = rd.cnsctvo_slctd_atrzcn_srvco
		And        rd.cdgo_intrno is not null;

		Update     ds 
		Set        indcdr_fcha_entrga = @vlr_uno
		From       #DatosSolicitudBpm ds
		Inner Join BDCna.gsa.tbASResultadoFechaEntrega rf With(NoLock) on ds.cnsctvo_slctd_atrzcn_srvco = rf.cnsctvo_slctd_atrzcn_srvco;


		
		Select 			
			nmro_slctd_ss_rmplzo				numeroSolicitud, 
			cnsctvo_cdgo_mdo_cntcto_slctd		cnsOrigenSolicitud,
			dscrpcn_mdo_cntcto					origenSolicitud,
			cnsctvo_cdgo_tpo_idntfccn_afldo		cnsTipoDocumento,
			cdgo_tpo_idntfccn					desTipoDocumento,
			nmro_idntfccn_afldo					numeroIdentificacion,
			nmro_unco_idntfccn_afldo			numeroUnicoIdentificacion,
			cnsctvo_cdgo_pln					cnsCodigoPlan,
			fcha_slctd							fechaRadicacion,
			cnsctvo_cdgo_estdo_slctd			consecutivoEstado,
			dscrpcn_estdo_srvco_slctdo			estadoSolicitud,
			indcdr_drccnmnto					indicadorDireccionamiento,
			indcdr_fcha_entrga					indicadorFechaEntrega,
			@sde_afldo							codigoSede,
			@dscrpcn_sde						descripcionSede
		From #DatosSolicitudBpm;

		Drop table #DatosSolicitudBpm;

END
GO
