USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarReglasDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASEjecutarReglasDireccionamiento
* Desarrollado por : <\A Jhon Olarte     A\>    
* Descripcion      : <\D Procedimiento que se encarga ejecutar las reglas de Direccionamiento  D\>
* Observaciones    : <\O      O\>    
* Parametros       : <\P P\>    
* Variables        : <\V       V\>    
* Fecha Creacion   : <\FC 21/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
/*    */
ALTER PROCEDURE [gsa].[spASEjecutarReglasDireccionamiento] @cnsctvo_prcso udtConsecutivo,
@cnsctvo_cdgo_tpo_prcso udtConsecutivo,
@es_btch char(1)
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @ldfechaActual datetime,
          @agrpa_prstcn udtLogico,
          @nmbre_sp_vldcn udtNombreObjeto,
          @instruccionSQL nvarchar(4000),
          @max_cnsctvo udtConsecutivo,
          @min_cnsctvo udtConsecutivo,
          @negacion char(1),
          @vacio numeric(2),
          @inexistente numeric(2),
          @afirmacion UDTLOGICO,
		  @dscrpcn_rgla varchar(500),
		  @mensaje_rgla varchar(100),
		  @procesoInicioBatchReglasNegocio int,
		  @mensajeError varchar(2000),
		  @cnsctvo_lg int;

  SET @ldfechaActual = GETDATE()
  SET @negacion = 'N'
  SET @vacio = 0
  SET @inexistente = 99
  SET @afirmacion = 'S'
  SET @mensaje_rgla = 'Regla Direccionamiento: '
  SET @procesoInicioBatchReglasNegocio = 68

  --creacion de tablas temporal para la ejecución
  DECLARE @tbMVReglasValidacion TABLE (
    cnsctvo udtConsecutivo IDENTITY (1, 1),
    jrrqa numeric(2),
    nmbre_sp char(50),
    agrpa_prstcn char(1)
  )

  --Se consultan las reglas a ejecutar.
  INSERT @tbMVReglasValidacion (jrrqa,
  nmbre_sp,
  agrpa_prstcn)
    SELECT
      m.jrrqa,
      m.nmbre_sp,
      m.agrpa_prstcn
    FROM bdCNA.prm.tbASReglasDireccionamiento_Vigencias m WITH (NOLOCK)
    WHERE @ldfechaActual BETWEEN m.inco_vgnca AND m.fn_vgnca
    AND m.cnsctvo_vgnca_cdgo_rgla_drccnmnto NOT IN (@vacio, @inexistente)
    AND m.vsble_usro = @afirmacion
    ORDER BY m.jrrqa

  SELECT
    @max_cnsctvo = MAX(cnsctvo)
  FROM @tbMVReglasValidacion
  SET @min_cnsctvo = 1

  --Se valida si es procesamiento en línea.
  IF @es_btch = @negacion
  BEGIN

    WHILE (@min_cnsctvo <= @max_cnsctvo)
    BEGIN
      SELECT
        @agrpa_prstcn = agrpa_prstcn,
        @nmbre_sp_vldcn = nmbre_sp
      FROM @tbMVReglasValidacion
      WHERE cnsctvo = @min_cnsctvo

      --ejecutar reglas de direccionamiento

      SET @instruccionSQL = N'EXEC gsa.' + LTRIM(RTRIM(@nmbre_sp_vldcn)) + N' @param1';
      EXECUTE sp_executesql @instruccionSQL,
                            N'@param1 as udtLogico',
                            @param1 = @agrpa_prstcn;

	  
	  IF EXISTS (SELECT 1 FROM #tmpPrestadoresDestino TCO
				 GROUP BY TCO.id_slctd_x_prstcn
				 HAVING COUNT(TCO.id_slctd_x_prstcn) > 1)
		  BEGIN
			SET @min_cnsctvo = @min_cnsctvo + 1;
		  END
	  ELSE
		  BEGIN
			SET @min_cnsctvo = @max_cnsctvo + 1;
		  END
	END
  END
  ELSE
	  BEGIN
		WHILE (@min_cnsctvo <= @max_cnsctvo)
		BEGIN
		  SELECT
			@agrpa_prstcn = agrpa_prstcn,
			@nmbre_sp_vldcn = nmbre_sp
		  FROM @tbMVReglasValidacion
		  WHERE cnsctvo = @min_cnsctvo

		  --ejecutar reglas de direccionamiento
		  BEGIN TRY
			SET @dscrpcn_rgla = @mensaje_rgla + @nmbre_sp_vldcn
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
															  @cnsctvo_cdgo_tpo_prcso,
															  @dscrpcn_rgla,
															  @procesoInicioBatchReglasNegocio,
															  @cnsctvo_lg OUTPUT

		  SET @instruccionSQL = N'EXEC gsa.' + LTRIM(RTRIM(@nmbre_sp_vldcn)) + N' @param1';
		  EXECUTE sp_executesql @instruccionSQL,
								N'@param1 as udtLogico',
								@param1 = @agrpa_prstcn;
		  SET @min_cnsctvo = @min_cnsctvo + 1

		  EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
		  END TRY
		  BEGIN CATCH
			SET @mensajeError = @nmbre_sp_vldcn + ' : ' + ERROR_MESSAGE();
			EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
																		 @mensajeError
			RAISERROR (@mensajeError, 16, 2) WITH SETERROR
		  END CATCH
		END
	  END
END

GO
