USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionConceptosGasto]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionConceptosGasto
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el peso reputacional del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionConceptosGasto] 
@cnsctvo_srvco_slctdo       udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @fechaLiquidacion DATETIME = GETDATE()

	Select  
		f.cdgo_cncpto_gsto,
		f.dscrpcn_cncpto_gsto 
	from  BDCna.gsa.tbASServiciosSolicitados SS WITH(NOLOCK)
	inner join bdsisalud..tbCupsServicios b	WITH(NOLOCK)					on SS.cnsctvo_cdgo_srvco_slctdo	= b.cnsctvo_prstcn
	inner join bdsisalud..tbModeloLiquidacionxConceptos c WITH(NOLOCK)		on b.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
	inner join bdsisalud..tbDetModeloLiquidacionxConceptos d WITH(NOLOCK)	on d.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
	inner join bdsisalud..tbConceptosGasto_vigencias f	WITH(NOLOCK)		on f.cnsctvo_cdgo_cncpto_gsto = d.cnsctvo_cdgo_cncpto_gsto 
	WHERE SS.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	AND   @fechaLiquidacion BETWEEN c.inco_vgnca and c.fn_vgnca
	AND   @fechaLiquidacion BETWEEN d.inco_vgnca and d.fn_vgnca
	AND   @fechaLiquidacion BETWEEN b.inco_vgnca and b.fn_vgnca
	AND   @fechaLiquidacion BETWEEN f.inco_vgnca and f.fn_vgnca
	
	Union All

	Select  
		f.cdgo_cncpto_gsto,
		f.dscrpcn_cncpto_gsto 
	from  BDCna.gsa.tbASServiciosSolicitados SS WITH(NOLOCK)
	inner join bdsisalud..tbCums b	WITH(NOLOCK)							on SS.cnsctvo_cdgo_srvco_slctdo	= b.cnsctvo_cms
	inner join bdsisalud..tbModeloLiquidacionxConceptos c WITH(NOLOCK)		on b.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
	inner join bdsisalud..tbDetModeloLiquidacionxConceptos d WITH(NOLOCK)	on d.cnsctvo_cdgo_mdlo_lqdcn_cncpto = c.cnsctvo_cdgo_mdlo_lqdcn_cncpto
	inner join bdsisalud..tbConceptosGasto_vigencias f	WITH(NOLOCK)		on f.cnsctvo_cdgo_cncpto_gsto = d.cnsctvo_cdgo_cncpto_gsto 
	WHERE SS.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	AND   @fechaLiquidacion BETWEEN c.inco_vgnca and c.fn_vgnca
	AND   @fechaLiquidacion BETWEEN d.inco_vgnca and d.fn_vgnca
	AND   @fechaLiquidacion BETWEEN b.inco_vgnca and b.fn_vgnca
	AND   @fechaLiquidacion BETWEEN f.inco_vgnca and f.fn_vgnca
    
	
END

GO
