USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoUltimaIps]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoUltimaIps
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D Ultima IPS que presto el servicio											 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoUltimaIps] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @fechaActual datetime = getdate(),
		  @afirmacion char(1) = 'S',
		  @vlor_cro int = 0;

  --Se valida que haya mas de 1 posible prestador asociado a cualquier prestación.
  IF EXISTS (SELECT
      id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino
    GROUP BY id_slctd_x_prstcn
    HAVING COUNT(id_slctd_x_prstcn) > 1)
  BEGIN
    --Actualiza si la prestacion fue atendida en un pasado
    UPDATE isp
    SET fcha_imprsn = atenPas.fcha_imprsn,
        cdgo_intrno_prstdr_antrr = atenPas.cdgo_intrno_prstdr_antrr
    FROM #tmpInformacionSolicitudPrestacion isp
    INNER JOIN (SELECT
      isp.cnsctvo_cdgo_prstcn,
      MAX(aten.fcha_imprsn) AS fcha_imprsn,
      aten.cdgo_intrno AS cdgo_intrno_prstdr_antrr
    FROM #tmpInformacionSolicitudPrestacion isp
    INNER JOIN bdSisalud.dbo.tbAtencionOps aten WITH (NOLOCK)
      ON isp.ni_pcnte = aten.nmro_unco_idntfccn_afldo
	  AND aten.fcha_imprsn BETWEEN isp.fcha_mnma_bsqda AND @fechaActual
    INNER JOIN bdSisalud.dbo.tbProcedimientos b WITH (NOLOCK)
      ON aten.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
      AND aten.nuam = b.nuam
      AND isp.cnsctvo_cdgo_prstcn = b.cnsctvo_prstcn
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro
    GROUP BY isp.cnsctvo_cdgo_prstcn,
             aten.cdgo_intrno) atenPas
      ON isp.cnsctvo_cdgo_prstcn = atenPas.cnsctvo_cdgo_prstcn
    WHERE isp.cdgo_intrno_ips_dstno IS NULL;

    IF EXISTS (SELECT
        TCO.id_slctd_x_prstcn
      FROM #tmpPrestadoresDestino TCO
      INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
        ON ISP.id = TCO.id_slctd_x_prstcn
        AND TCO.cdgo_intrno = ISP.cdgo_intrno_prstdr_antrr
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
    BEGIN
      --Se actualizan los prestadores que apliquen el filtro, siempre y cuando se haya encontrado mínimo uno
      --que cumpla el filtro de ciudad para la prestación a evaluar.
      UPDATE TCO
      SET cmple = @afirmacion
      FROM #tmpPrestadoresDestino TCO
      INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
        ON ISP.id = TCO.id_slctd_x_prstcn
        AND TCO.cdgo_intrno = ISP.cdgo_intrno_prstdr_antrr
		WHERE ISP.mrca_msmo_prstdor = @vlor_cro;

      EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
    END
  END
END

GO
