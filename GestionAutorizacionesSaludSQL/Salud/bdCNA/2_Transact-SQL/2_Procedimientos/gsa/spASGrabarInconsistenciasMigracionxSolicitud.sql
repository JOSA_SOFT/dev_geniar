Use bdCNA
Go

If(OBJECT_ID('gsa.spASGrabarInconsistenciasMigracionxSolicitud') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASGrabarInconsistenciasMigracionxSolicitud As Select 1';
End

Go

/*---------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarInconsistenciasMigracionSolicitud
* Desarrollado por   : <\A Ing. Carlos Andr�s L�pez Ram�rez - qvisionclr  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite guardar la informacion de las inconsistencias en la migracion
						   a solicitud.
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           1. @mnsje_incnsstca  
						   2. @usro
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 07/06/2017 FC\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*----------------------------------------------------------------------------------------------------------------------------*/




Alter Procedure gsa.spASGrabarInconsistenciasMigracionxSolicitud
	@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
	@cnsctvo_srvco_slctdo			udtConsecutivo,
	@mnsje_incnsstca				VarChar(1000),
	@usro							udtUsuario
As
Begin
	Set NoCount On

	Declare @fcha_actl		DateTime;

	Set @fcha_actl = getDate();

	Insert Into bdCNA.gsa.tbASInconsistenciasMigracionxSolicitud
	(
		cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,		mnsje_incnstnca,		
		fcha_crcn,							usro_crcn,					fcha_ultma_mdfccn,
		usro_ultma_mdfccn
	)
	Values
	(
		@cnsctvo_slctd_atrzcn_srvco,		@cnsctvo_srvco_slctdo,		@mnsje_incnsstca,		
		@fcha_actl,							@usro,						@fcha_actl,
		@usro
	)

End