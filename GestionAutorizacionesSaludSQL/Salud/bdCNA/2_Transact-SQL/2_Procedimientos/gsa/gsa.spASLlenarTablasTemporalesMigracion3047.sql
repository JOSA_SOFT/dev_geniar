USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASLlenarTablasTemporalesMigracion3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASLlenarTablasTemporalesMigracion3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASLlenarTablasTemporalesMigracion3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Este proceso almacenado se encarga de llenar las tablas temporales que guardan en MEGA a partir de la 
					 informacion que se encuentra en 3047 D\>    
* Observaciones : <\O Para realizar la ejecucion de este sp es necesario que las tablas temporales de MEGA existan. O\>    
* Parametros  : <\P En caso de que se vaya a llenar las tablas con una solicitud en particular se pasa el @cnsctvo_slctd_ops 
					en caso de ser null, se realizara masivo P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/06/13 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASLlenarTablasTemporalesMigracion3047 null
		EXEC gsa.spASLlenarTablasTemporalesMigracion3047 1
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo  AM\>    
* Descripcion        : <\DM Se adicionan validacion para la informacion del medico en caso de ser null se colocan el medico comodin DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/07 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se realizan ajustes para obtener informacion que provienen de la fuente null (oficina, nui afiliado, nuam, lateralidad, entre otros) DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/11 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajustes sobre la consulta masiva para que traiga las prestaciones aprobadas sin nuam, adicional se ajusta tipo de diagnostico 
							para que si es 0 o null coloque 1 DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/13 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se realiza ajuste para para que diferencia entre los diagnosticos principales y secundarios DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/17 FM\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se realiza ajuste para que solo tenga en cuenta los estados aprobado en la prestacion de 3047 DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/19 FM\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se adiciona validacion de nuam en null cuando se ejecuta proceso por solicitud DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/19 FM\>
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASLlenarTablasTemporalesMigracion3047 (
	@cnsctvo_slctd_ops udtConsecutivo = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @estado_aprobada_3047	int,
			@fechaActual			datetime,
			@ValorCERO				int,
			@ValorCuatro			int,
			@valorVacio				udtLogico,
			@SI						udtLogico,
			@NO						udtLogico,
			@medio3047				udtConsecutivo,
			@inicial				udtConsecutivo,
			@procesoAutomatico		udtConsecutivo,
			@valorUno				int,
			@codigo3047				udtCodigo,
			@ValorTRES				int,
			@valorDos				int,
			@fechaInicial			datetime,
			@cnsctvo_tpo_cncpto		udtConsecutivo,
			@maximo					udtConsecutivo,
			@contador				udtConsecutivo,
			@nuam					udtConsecutivo,
			@oficina				udtConsecutivo,
			@cnsctvo_slctd_ops_aux	udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto_cncr	udtConsecutivo,
			@cnsctvo_cdgo_clsfccn_evnto_vh      udtConsecutivo,
			@cdgo_estdo_ntfccn_ingrsdo          udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo           udtConsecutivo,
			@cdgo_estdo_ntfccn_prbble           udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo1          udtConsecutivo,
			@cdgo_estdo_ntfccn_cnfrmdo          udtConsecutivo,
			@cdgo_estdo_ntfccn_ntfcdo2          udtConsecutivo,
			@cdgo_estdo_ntfccn_cnfrmdo1         udtConsecutivo,
			@cdgo_estdo_ntfccn_prbble1          udtConsecutivo,
			@cdgo_estdo_ntfccn_cn_srvco         udtConsecutivo,
			@cdgo_estdo_ntfccn_sn_srvco         udtConsecutivo,
			@cnsctvo_cdgo_rcbro					udtConsecutivo,
			@numeroUnicoMedio					udtConsecutivo,
			@cnsctvo_cdgo_tpo_idntfccn_mdco		udtConsecutivo,
			@nmro_idntfccn_mdco_cmdn			udtNumeroIdentificacionLargo

	CREATE TABLE #estadosNotificacion(
		cnsctvo_cdgo_estdo_ntfccn int
	)		

	CREATE TABLE #clasificacionEvento(
		cnsctvo_cdgo_clsfccn_evnto int
	)

	CREATE TABLE #solicitudesSinOficina(
		id	int identity,
		cnsctvo_slctd_ops	udtConsecutivo,
		cnsctvo_cdgo_ofcna	udtConsecutivo,
		nuam				udtConsecutivo
	)
	
	SET @estado_aprobada_3047 = 3
	SET @fechaActual = getdate()
	SET @ValorCERO = 0
	SET @valorVacio = ''
	SET @SI = 'S'
	SET @NO = 'N'
	SET @inicial = 1
	SET @codigo3047 = '03'
	SET @valorUno = 1
	SET @procesoAutomatico = 4
	SET @cnsctvo_cdgo_clsfccn_evnto_cncr = 13 --cancer
	SET @cnsctvo_cdgo_clsfccn_evnto_vh   = 16 --vih
	SET @cdgo_estdo_ntfccn_ingrsdo  = 13  -- Ingresado
	SET @cdgo_estdo_ntfccn_ntfcdo   = 14  -- Notificado
	SET @cdgo_estdo_ntfccn_prbble   = 87  -- Probable
	SET @cdgo_estdo_ntfccn_ntfcdo1  = 88  -- Notificado
	SET @cdgo_estdo_ntfccn_cnfrmdo  = 89  -- Confirmado
	SET @cdgo_estdo_ntfccn_ntfcdo2  = 119 -- Notificado	
	SET @cdgo_estdo_ntfccn_cnfrmdo1 = 120 -- Confirmado
	SET @cdgo_estdo_ntfccn_prbble1  = 156 -- Probable
	SET @cdgo_estdo_ntfccn_cn_srvco = 167 -- Con Servicio
	SET @cdgo_estdo_ntfccn_sn_srvco = 168 -- Sin Servicio
	SET @cnsctvo_cdgo_rcbro = 9
	SET @medio3047 = 3
	SET @ValorTRES = 3
	SET @fechaInicial = '20170601'
	SET @numeroUnicoMedio = 100961
	SET @cnsctvo_cdgo_tpo_idntfccn_mdco = 1
	SET @nmro_idntfccn_mdco_cmdn = '11111111'
	SET @ValorCuatro = 4
	SET @cnsctvo_tpo_cncpto = 12
	SET @contador = 1
	SET @valorDos = 2

	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_ingrsdo)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_ntfcdo)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_prbble)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_ntfcdo1)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_cnfrmdo)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_ntfcdo2)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_cnfrmdo1)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_prbble1)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_cn_srvco)
	INSERT INTO #estadosNotificacion (cnsctvo_cdgo_estdo_ntfccn) values (@cdgo_estdo_ntfccn_sn_srvco)

	INSERT INTO #clasificacionEvento (cnsctvo_cdgo_clsfccn_evnto) VALUES (@cnsctvo_cdgo_clsfccn_evnto_cncr)
	INSERT INTO #clasificacionEvento (cnsctvo_cdgo_clsfccn_evnto) VALUES (@cnsctvo_cdgo_clsfccn_evnto_vh)
	
	IF @cnsctvo_slctd_ops IS NOT NULL --Se va a migrar una solicitud en particular
	BEGIN
		INSERT INTO #solicitudes3047 (cnsctvo_slctd_ops)
		SELECT distinct a.cnsctvo_slctd_ops
		FROM bdsisalud.dbo.tbSolicitudOPS a WITH(NOLOCK)
		INNER JOIN bdsisalud.dbo.tbSolicitudOPSPrestaciones b WITH(NOLOCK) ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
		LEFT JOIN gsa.tbASDatosMigracion3047Mega c WITH(NOLOCK) ON c.cnsctvo_slctd_prstcn = b.cnsctvo_slctd_prstcn
		WHERE a.cnsctvo_slctd_ops = @cnsctvo_slctd_ops
		AND b.cnsctvo_cdgo_estdo_prstcn_slctd = @estado_aprobada_3047
		AND c.cnsctvo_slctd_prstcn is null
		AND b.nuam is null
	END
	ELSE --Es Masivo
	BEGIN
		INSERT INTO #solicitudes3047 (cnsctvo_slctd_ops)
		SELECT distinct a.cnsctvo_slctd_ops
		FROM bdsisalud.dbo.tbSolicitudOPS a WITH(NOLOCK)
		INNER JOIN bdsisalud.dbo.tbSolicitudOPSPrestaciones b WITH(NOLOCK) ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
		LEFT JOIN gsa.tbASDatosMigracion3047Mega c WITH(NOLOCK) ON c.cnsctvo_slctd_prstcn = b.cnsctvo_slctd_prstcn
		WHERE b.cnsctvo_cdgo_estdo_prstcn_slctd = @estado_aprobada_3047
		AND b.fcha >= @fechaInicial
		AND b.nuam is null
		AND c.cnsctvo_slctd_prstcn is null
	END
	
	INSERT	#Tempo_Afiliados (
		id,									cnsctvo_cdgo_tpo_idntfccn_afldo,		nmro_idntfccn_afldo,						
		cnsctvo_cdgo_cdd_rsdnca_afldo,		cnsctvo_cdgo_pln,						nmro_unco_idntfccn_afldo,						
		usro_crcn,							prmr_aplldo_afldo,						sgndo_aplldo_afldo,
		prmr_nmbre_afldo,					sgndo_nmbre_afldo,						fcha_ncmnto_afldo,
		cnsctvo_cdgo_cbrtra_sld,			cnsctvo_cdgo_dprtmnto_afldo,			
		drccn_afldo,						tlfno_afldo,							tlfno_cllr_afldo,
		crro_elctrnco_afldo,				cdgo_sxo_rcn_ncdo										
	)
	SELECT 
		a.cnsctvo_slctd_ops,				a.cnsctvo_cdgo_tpo_idntfccn_pcnte,		a.nmro_idntfccn_pcnte,
		a.cnsctvo_cdgo_cdd_rsdnca,			a.cnsctvo_cdgo_pln,						a.nmro_unco_idntfccn_afldo,
		a.usro_slctd,						a.prmr_aplldo,							a.sgndo_aplldo,
		a.prmr_nmbre,						a.sgndo_nmbre,							a.fcha_ncmnto_pcnte,
		a.cnsctvo_cdgo_cbrtra_sld_pgo,		a.cnsctvo_cdgo_dprtmnto_rsdnca,			
		a.drccn_pcnte,						a.tlfno_pcnte,							a.tlfn_cllr_pcnte,
		a.eml,								@valorVacio					
	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops

	UPDATE a
	SET a.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbBeneficiariosValidador b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_idntfccn_afldo = b.cnsctvo_cdgo_tpo_idntfccn 
																				 and a.nmro_idntfccn_afldo = b.nmro_idntfccn
	WHERE @fechaActual between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro
	
	
	UPDATE a
	SET a.cnsctvo_cdgo_sxo = b.cnsctvo_cdgo_sxo,
		a.cnsctvo_cdgo_tpo_vnclcn_afldo = b.cnsctvo_cdgo_prntsco,
		a.cnsctvo_cdgo_tpo_cntrto = b.cnsctvo_cdgo_tpo_cntrto,
		a.nmro_cntrto = b.nmro_cntrto,
		a.cnsctvo_bnfcro_cntrto = b.cnsctvo_bnfcro,
		a.cdgo_ips_prmra = isnull(b.cdgo_intrno, @valorVacio)
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbBeneficiariosValidador b WITH(NOLOCK) ON a.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	WHERE @fechaActual between b.inco_vgnca_bnfcro and b.fn_vgnca_bnfcro
		
	UPDATE a
	SET cnsctvo_cdgo_rngo_slrl = cv.cnsctvo_cdgo_rngo_slrl
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbContratosValidador cv WITH(NOLOCK) ON cv.cnsctvo_cdgo_tpo_cntrto = a.cnsctvo_cdgo_tpo_cntrto  
																				AND cv.nmro_cntrto = a.nmro_cntrto
	WHERE @fechaActual between cv.inco_vgnca_cntrto and cv.fn_vgnca_cntrto

	UPDATE a
	SET a.cdgo_cdd_rsdnca_afldo = b.cdgo_cdd,
		a.dscrpcn_cdd_rsdnca_afldo = b.dscrpcn_cdd
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbCiudades_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_cdd_rsdnca_afldo = b.cnsctvo_cdgo_cdd
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_dprtmnto_afldo = b.cdgo_dprtmnto,
		a.dscrpcn_dprtmnto_afldo = b.dscrpcn_dprtmnto
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbDepartamentos_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_dprtmnto_afldo = b.cnsctvo_cdgo_dprtmnto
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cnsctvo_cdgo_tpo_pln = b.cnsctvo_cdgo_tpo_pln,
		a.cdgo_pln = b.cnsctvo_cdgo_pln
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbPlanes_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_pln = b.cnsctvo_cdgo_pln
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_cbrtra_sld = b.cdgo_cbrtra_sld
	FROM #Tempo_Afiliados a
	INNER JOIN bdsisalud.dbo.tbPMCoberturasSalud_vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_cbrtra_sld = b.cnsctvo_cdgo_cbrtra_sld
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cnsctvo_cdgo_sde_ips_prmra = b.cnsctvo_cdgo_sde_ips
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbIpsPrimarias_vigencias b WITH(NOLOCK) ON a.cdgo_ips_prmra = b.cdgo_intrno
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_tpo_vnclcn_afldo = b.cdgo_prntscs
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbParentescos_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_vnclcn_afldo = b.cnsctvo_cdgo_prntscs
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_sxo = b.cdgo_sxo
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbSexos_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_sxo = b.cnsctvo_cdgo_sxo
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_tpo_pln = b.cdgo_tpo_pln
	FROM #Tempo_Afiliados a
	INNER JOIN BDafiliacionValidador.dbo.tbTiposPlan_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_pln = b.cnsctvo_cdgo_tpo_pln
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE	a
	SET	cnsctvo_cdgo_estdo_drcho = mdv.cnsctvo_cdgo_estdo_drcho 
	FROM #Tempo_Afiliados a
	INNER JOIN	BDafiliacionValidador.dbo.TbMatrizDerechosValidador mdv With(NoLock)
		ON	mdv.cnsctvo_cdgo_tpo_cntrto   = a.cnsctvo_cdgo_tpo_cntrto    And
			mdv.nmro_cntrto               = a.nmro_cntrto                And
			mdv.nmro_unco_idntfccn_bnfcro = a.nmro_unco_idntfccn_afldo
	WHERE @fechaActual Between mdv.inco_vgnca_estdo_drcho And mdv.fn_vgnca_estdo_drcho

	UPDATE a
	SET a.cnsctvo_cdgo_chrte = ISNULL(b.cnsctvo_cdgo_clsfccn_evnto, @ValorCERO)
	FROM #Tempo_Afiliados a
	INNER JOIN  bdSisalud.dbo.tbafiliadosMarcados b with(nolock) ON b.nmro_unco_idntfccn = a.nmro_unco_idntfccn_afldo
	INNER JOIN #estadosNotificacion c ON b.cnsctvo_cdgo_estdo_ntfccn = c.cnsctvo_cdgo_estdo_ntfccn
	INNER JOIN #clasificacionEvento d ON b.cnsctvo_cdgo_clsfccn_evnto = d.cnsctvo_cdgo_clsfccn_evnto
	

	UPDATE a
	SET edd_afldo_ans = datediff(yy,a.fcha_ncmnto_afldo, @fechaActual),
		edd_afldo_mss = datediff(mm,a.fcha_ncmnto_afldo, @fechaActual),
		edd_afldo_ds = datediff(dd,a.fcha_ncmnto_afldo, @fechaActual)
	FROM #Tempo_Afiliados a

	INSERT INTO #Tempo_IPS (
		cnsctvo_cdgo_tpo_idntfccn_ips_slctnte,	nmro_idntfccn_ips_slctnte,		cdgo_intrno,
		nmro_indctvo_prstdr,					cnsctvo_cdgo_dprtmnto_prstdr,	cnsctvo_cdgo_cdd_prstdr,
		drccn_prstdr,							tlfno_prstdr,					id,
		usro_crcn)
	SELECT 
		a.cnsctvo_cdgo_tpo_idntfccn_prstdr,		a.nmro_idntfccn,					a.cdgo_intrno,
		a.nmro_indctvo_prstdr,					a.cnsctvo_cdgo_dprtmnto,			a.cnsctvo_cdgo_cdd,
		a.drccn_prstdr,							a.tlfno_idnctvo_prstdr,				a.cnsctvo_slctd_ops,
		a.usro_slctd
	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops

	UPDATE a
	SET a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr,
		a.nmbre_scrsl = b.nmbre_scrsl,
		a.nmbre_ips = b.nmbre_scrsl,
		a.cdgo_hbltcn = b.cdgo_prstdr_mnstro
	FROM #Tempo_IPS a
	INNER JOIN BdSiSalud.dbo.tbdireccionesprestador b WITH(NOLOCK) ON a.cdgo_intrno = b.cdgo_intrno
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_tpo_idntfccn_ips_slctnte = b.cnsctvo_cdgo_tpo_idntfccn,
		a.dgto_vrfccn = b.dgto_vrfccn
	FROM #Tempo_IPS a
	INNER JOIN BdSiSalud.dbo.tbprestadores b WITH(NOLOCK) ON a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

	UPDATE a
	SET a.cdgo_cdd_prstdr = b.cdgo_cdd
	FROM #Tempo_IPS a
	INNER JOIN BDafiliacionValidador.dbo.tbCiudades_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_cdd_prstdr = b.cnsctvo_cdgo_cdd
	AND @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_dprtmnto_prstdr = b.cdgo_dprtmnto
	FROM #Tempo_IPS a
	INNER JOIN BDafiliacionValidador.dbo.tbDepartamentos_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_dprtmnto_prstdr = b.cnsctvo_cdgo_dprtmnto
	AND @fechaActual between b.inco_vgnca and b.fn_vgnca

	INSERT INTO #Tempo_Medicos(
		cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte,		nmro_idntfccn_mdco_trtnte,		rgstro_mdco_trtnte,
		usro_crcn,									prmr_nmbre_mdco_trtnte,			sgndo_nmbre_mdco_trtnte,					
		prmr_aplldo_mdco_trtnte,					sgndo_aplldo_mdco_trtnte,		id)
	SELECT 
		a.csnctv_cdgo_tpo_idntfccn_mdco_slctnte,	a.nmr_idtfccn_mdco_slctnte,		a.rgstro_prfsnal,
		a.usro_slctd,								a.prmr_nmbre_mdc_slctnte,		a.sgndo_nmbre_mdc_slctnte,
		a.prmr_aplldo_mdc_slctnte,					a.sgndo_aplldo_mdc_slctnte,		a.cnsctvo_slctd_ops
	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops

	UPDATE #Tempo_Medicos
	SET cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = @cnsctvo_cdgo_tpo_idntfccn_mdco,
		nmro_idntfccn_mdco_trtnte = @nmro_idntfccn_mdco_cmdn,
		nmro_unco_idntfccn_mdco = @numeroUnicoMedio
	WHERE cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte IS NULL
	OR nmro_idntfccn_mdco_trtnte IS NULL 
	OR nmro_unco_idntfccn_mdco IS NULL

	UPDATE a
	SET a.nmro_unco_idntfccn_mdco = b.nmro_unco_idntfccn_prstdr,
		a.adscrto = @SI
	FROM #Tempo_Medicos a
	INNER JOIN bdsisalud.dbo.tbprestadores b WITH(NOLOCK) 
		ON a.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = b.cnsctvo_cdgo_tpo_idntfccn
		AND a.nmro_idntfccn_mdco_trtnte = b.nmro_idntfccn
		
	UPDATE a
	SET a.cdgo_tpo_idntfccn_mdco_trtnte = b.cdgo_tpo_idntfccn
	FROM #Tempo_Medicos a
	INNER JOIN BDafiliacionValidador.dbo.tbTiposIdentificacion b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte = b.cnsctvo_cdgo_tpo_idntfccn
		
	UPDATE a
	SET a.cnsctvo_cdgo_espcldd_mdco_trtnte = b.cnsctvo_cdgo_espcldd
	FROM #Tempo_Medicos a
	INNER JOIN bdsisalud.dbo.tbmedicos b WITH(NOLOCK) ON a.nmro_unco_idntfccn_mdco = b.nmro_unco_idntfccn_prstdr
		
	UPDATE a
	SET a.cdgo_espcldd_mdco_trtnte = b.cdgo_espcldd
	FROM #Tempo_Medicos a
	INNER JOIN bdsisalud.dbo.tbEspecialidades_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_espcldd_mdco_trtnte = b.cnsctvo_cdgo_espcldd
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE #Tempo_Medicos
	SET cnsctvo_cdgo_ntfccn = @ValorCERO,
		cnsctvo_cdgo_ofcna = @ValorCERO,
		cnsctvo_cdgo_ctc = @ValorCERO

	
	INSERT INTO #Tempo_Diagnostico(cnsctvo_cdgo_dgnstco, id, cnsctvo_cdgo_rcbro, usro_crcn,cnsctvo_cdgo_tpo_dgnstco)
	SELECT c.cnsctvo_cdgo_dgnstco, a.cnsctvo_slctd_ops, @cnsctvo_cdgo_rcbro, a.usro_slctd, CASE WHEN c.prncpl = @SI THEN @valorUno ELSE @valorDos END
	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
	INNER JOIN bdSisalud.dbo.tbSolicitudOPSDiagnosticos c WITH(NOLOCK) ON c.cnsctvo_slctd_ops = a.cnsctvo_slctd_ops

	UPDATE a
	SET a.cnsctvo_cdgo_cntngnca	= b.cnsctvo_cdgo_cntngnca,
		a.cdgo_dgnstco = b.cdgo_dgnstco
	FROM #Tempo_Diagnostico a
	INNER JOIN bdsisalud.dbo.tbdiagnosticos_vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_dgnstco = b.cnsctvo_cdgo_dgnstco
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_cntngnca = b.cdgo_cntngnca
	FROM #Tempo_Diagnostico a
	INNER JOIN bdsisalud.dbo.tbContingencias_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_cntngnca = b.cnsctvo_cdgo_cntngnca
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_rcbro = b.cdgo_rcbro
	FROM #Tempo_Diagnostico a
	INNER JOIN bdSiSalud.dbo.tbRecobros_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_rcbro = b.cnsctvo_cdgo_rcbro
	WHERE a.cnsctvo_cdgo_rcbro = @cnsctvo_cdgo_rcbro
	AND @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_tpo_dgnstco = b.cdgo_tpo_dgnstco
	FROM #Tempo_Diagnostico a
	INNER JOIN bdsisalud.dbo.tbTipoDiagnostico_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_dgnstco = b.cnsctvo_cdgo_tpo_dgnstco
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca
	
	INSERT INTO #Tempo_Prestaciones(
		cnsctvo_cdgo_srvco_slctdo,	cntdd_slctda,						fcha_prstcn_srvco_slctdo,
		usro_crcn,					cdgo_prstcn_prstdr,					cdgo_tpo_atrzcn,
		cnsctvo_cdgo_ltrldd,		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,	id,	
		tmpo_trtmnto_slctdo,		cdgo_undd_tmpo_trtmnto_slctdo,		dss,
		cnsctvo_cdgo_prsntcn_dss,	prdcdd_dss,							cnsctvo_cdgo_undd_prdcdd_dss,
		prgrmcn_entrga,				cnsctvo_cdgo_va_accso,				cdgo_va_accso
	)
	SELECT 
		c.cnsctvo_cdfccn,							c.cntdd,							a.fcha,
		a.usro_slctd,								@ValorCERO,							@ValorCERO,
		isnull(c.cnsctvo_cdgo_ltrldd,@ValorCuatro),		c.cnsctvo_prgrmcn_fcha_evnto,		a.cnsctvo_slctd_ops,		
		@ValorCERO,									@valorVacio,						@valorVacio,
		@ValorCERO,									@ValorCERO,							@ValorCERO,
		@NO,										@ValorTRES,							@ValorCERO
	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
	INNER JOIN bdSisalud.dbo.tbSolicitudOPSPrestaciones c WITH(NOLOCK) ON c.cnsctvo_slctd_ops = a.cnsctvo_slctd_ops
	WHERE c.cnsctvo_cdgo_estdo_prstcn_slctd = @estado_aprobada_3047

	UPDATE a
	SET a.cnsctvo_cdgo_tpo_srvco = b.cnsctvo_cdgo_tpo_cdfccn,
		a.dscrpcn_srvco_slctdo = b.dscrpcn_cdfccn,
		a.cdgo_srvco_slctdo = b.cdgo_cdfccn
	FROM #Tempo_Prestaciones a 
	INNER JOIN bdsisalud.dbo.tbCodificaciones b WITH(NOLOCK) ON a.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cdfccn

	UPDATE a
	SET a.cdgo_tpo_srvco = b.cdgo_tpo_cdfccn
	FROM #Tempo_Prestaciones a 
	INNER JOIN bdsisalud.dbo.tbTipoCodificacion_vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_srvco = b.cnsctvo_cdgo_tpo_cdfccn
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cnsctvo_cdgo_frma_frmctca = b.cnsctvo_frma_frmctca,
		a.cnsctvo_cdgo_va_admnstrcn_mdcmnto = b.cnsctvo_cdgo_va_admnstrcn,
		a.cnsctvo_mdcmnto_slctdo_orgn = b.cnsctvo_cms,
		a.cncntrcn_dss = b.cntdd_cncntrcn,
		a.cnsctvo_cdgo_undd_cncntrcn_dss = b.cnsctvo_cncntrcn,
		a.cnsctvo_cdgo_prsntcn = b.cnsctvo_prsntcn
	FROM #Tempo_Prestaciones a 
	INNER JOIN bdSisalud.dbo.tbCums b WITH(NOLOCK) ON a.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cms
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_ltrldd = b.cdgo_ltrldd
	FROM #Tempo_Prestaciones a 
	INNER JOIN bdsisalud.dbo.tbLateralidades_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_ltrldd = b.cnsctvo_cdgo_ltrldd
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	Update      p
	Set         cnsctvo_cdgo_grpo_trptco =   c.cnsctvo_cdgo_sbgrpo_trptco
	From        #Tempo_Prestaciones p
	Inner Join  bdSisalud.dbo.tbcums a with(nolock)
	On          a.cnsctvo_cms = p.cnsctvo_cdgo_srvco_slctdo
	Inner Join  bdSisalud.dbo.tbMNATC_Vigencias b With(NoLock)
	On          b.cnsctvo_cdgo_atc = a.cnsctvo_atc
	Inner Join  bdSisalud.dbo.tbSubgrupoQuimico_vigencias e With(NoLock)              
	On          e.cnsctvo_cdgo_sbgrpo_qmco = b.cnsctvo_cdgo_sbgrpo_qmco               
	Inner Join  bdSisalud.dbo.tbSubgrupoFarmacologico_Vigencias f With(NoLock)               
	On          f.cnsctvo_cdgo_sbgrpo_frmclgco = e.cnsctvo_cdgo_sbgrpo_frmclgco
	Inner Join  bdSisalud.dbo.tbSubgrupoTerapeutico_vigencias c With(NoLock)  
	On          c.cnsctvo_cdgo_sbgrpo_trptco = f.cnsctvo_cdgo_sbgrpo_trptco
	Where       @fechaactual Between e.inco_vgnca and e.fn_vgnca              
	And         @fechaactual Between b.inco_vgnca and b.fn_vgnca              
	And         @fechaactual Between c.inco_vgnca and c.fn_vgnca              
	And         @fechaactual Between f.inco_vgnca and f.fn_vgnca
	And         @fechaactual Between a.inco_vgnca and a.fn_vgnca


	INSERT INTO #Tempo_Solicitudes(
		cnsctvo_cdgo_orgn_atncn,		cnsctvo_cdgo_tpo_srvco_slctdo,		cnsctvo_cdgo_prrdd_atncn,
		cnsctvo_cdgo_tpo_ubccn_pcnte,	jstfccn_clnca,						nmro_slctd_prvdr,
		cdgo_eps,						cnsctvo_cdgo_clse_atncn,			fcha_slctd,
		anio_slctd,						nmro_slctd_ss_rmplzo,				obsrvcn_adcnl,
		usro_crcn,						id,
		cnsctvo_cdgo_tpo_slctd,			cnsctvo_cdgo_mdo_cntcto_slctd,		cnsctvo_cdgo_tpo_trnsccn_srvco_sld,
		ga_atncn,						cnsctvo_prcso_vldcn,				cnsctvo_prcso_mgrcn,
		mgrda_gstn,						spra_tpe_st,						dmi,
		cdgo_tpo_trnsccn_srvco_sld,		cnsctvo_cdgo_tpo_orgn_slctd,
		cnsctvo_cdgo_frma_atncn,		cdgo_mdo_cntcto_slctd,				nmro_slctd_atrzcn_ss		
			
		)
	SELECT
		a.cnsctvo_cdgo_orgn_atncn,		a.cnsctvo_cdgo_tpo_srvco_slctd,		a.cnsctvo_cdgo_prrdd_atncn,
		a.cnsctvo_cdgo_ubccn_pcnte,		a.jstfccn_clnca,					a.nmro_slctd,
		a.cdgo_pgdr,					a.cnsctvo_cdgo_ubccn_pcnte,			a.fcha,
		DATEPART(YYYY,a.fcha),			a.nmro_slctd,						a.jstfccn_clnca,
		a.usro_slctd,					a.cnsctvo_slctd_ops,
		@ValorCERO,						@medio3047,							@ValorCERO,
		@valorVacio,					@ValorCERO,							@ValorCERO,
		@ValorCERO,						@NO,								@NO,
		@inicial,						@procesoAutomatico,
		@valorUno,						@codigo3047,						a.cnsctvo_slctd_ops

	FROM bdSisalud.dbo.tbSolicitudOps a WITH(NOLOCK)
	INNER JOIN #solicitudes3047 b ON a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
		
	UPDATE a
	SET a.cnsctvo_cdgo_ofcna_atrzcn = b.cnsctvo_cdgo_ofcna,
		a.nuam = b.nuam
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdSisalud.dbo.tbSolicitudOPSPrestaciones b WITH(NOLOCK) ON a.id = b.cnsctvo_slctd_ops
	WHERE b.cnsctvo_cdgo_ofcna is not null

	UPDATE a
	SET a.cdgo_orgn_atncn = b.cdgo_orgn_atncn
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdSisalud.dbo.tbPMOrigenAtencion_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_orgn_atncn = b.cnsctvo_cdgo_orgn_atncn
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_tpo_srvco_slctdo = b.cdgo_tpo_srvco_slctdo
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdSisalud.dbo.tbPMTipoServicioSolicitado_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_tpo_srvco_slctdo = b.cnsctvo_cdgo_tpo_srvco_slctdo
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_prrdd_atncn = b.cdgo_prrdd_atncn
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdSisalud.dbo.tbPMPrioridadAtencion_Vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_prrdd_atncn = b.cnsctvo_cdgo_prrdd_atncn
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_clse_atncn = b.cdgo_clse_atncn
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdsisalud.dbo.tbclasesatencion_vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_clse_atncn = b.cnsctvo_cdgo_clse_atncn
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	UPDATE a
	SET a.cdgo_frma_atncn = b.cdgo_frma_atncn
	FROM #Tempo_Solicitudes a 
	INNER JOIN bdsisalud.dbo.tbFormasAtencion_vigencias b WITH(NOLOCK) ON a.cnsctvo_cdgo_frma_atncn = b.cnsctvo_cdgo_frma_atncn
	WHERE @fechaActual between b.inco_vgnca and b.fn_vgnca

	
	INSERT INTO #solicitudesSinOficina(cnsctvo_slctd_ops)
	SELECT id
	FROM #Tempo_Solicitudes
	WHERE cnsctvo_cdgo_ofcna_atrzcn IS NULL

	INSERT INTO #solicitudesSinOficina(cnsctvo_slctd_ops)
	SELECT cnsctvo_slctd_ops
	FROM #Tempo_Solicitudes a
	LEFT JOIN #solicitudesSinOficina b ON a.id = b.cnsctvo_slctd_ops
	WHERE a.nuam IS NULL
	AND b.cnsctvo_slctd_ops IS NULL

	UPDATE b
	SET b.cnsctvo_cdgo_ofcna = d.cnsctvo_cdgo_ofcna
	FROM #solicitudesSinOficina b 
	INNER JOIN #tempo_afiliados c ON b.cnsctvo_slctd_ops = c.id
	INNER JOIN bdafiliacionvalidador.dbo.tbOficinas_vigencias d ON c.cnsctvo_cdgo_sde_ips_prmra = d.cnsctvo_cdgo_sde
	WHERE @fechaActual between d.inco_vgnca and d.fn_vgnca 
	and prncpl=@SI

	SELECT @maximo = max(id)
	FROM #solicitudesSinOficina

	WHILE (@contador <= @maximo)
	BEGIN
		SELECT @oficina = cnsctvo_cdgo_ofcna,
			   @cnsctvo_slctd_ops_aux = cnsctvo_slctd_ops
		FROM #solicitudesSinOficina
		WHERE id = @contador

		EXEC bdsisalud.dbo.sppmgeneraconsecutivoatencion1 @oficina,@cnsctvo_tpo_cncpto,@nuam OUTPUT

		UPDATE #Tempo_Solicitudes
		SET nuam= @nuam, cnsctvo_cdgo_ofcna_atrzcn = @oficina
		WHERE id = @cnsctvo_slctd_ops_aux
		
		SET @contador = @contador + 1
	END


	DROP TABLE #estadosNotificacion
	DROP TABLE #clasificacionEvento
	DROP TABLE #solicitudesSinOficina

END

GO
