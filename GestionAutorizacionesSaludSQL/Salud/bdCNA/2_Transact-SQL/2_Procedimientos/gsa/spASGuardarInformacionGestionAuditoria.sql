USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionGestionAuditoria]    Script Date: 13/06/2017 03:46:19 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionGestionAuditoria
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													 D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez AM\>
* Descripcion			 : <\DM	Se adicionaInsert sobre la tabla de diagnosticosDM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 25-05-2016 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez AM\>
* Descripcion			 : <\DM	Se adicionaInsert de los documentos Soportes  DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 03-03-2017 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez AM\>
* Descripcion			 : <\DM	Modifica SP para que actualice la cantidad de prestaciones
								en la tabla de servicios que son gestionadas por el auditor  DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 27-03-2017 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se cambia el nombre de la variable @principal por
								@dgnstco_prncpl para reconocer mas facil el objetivo 
								de esta. Se cambia el valir de la variable de 
								'PRINCIPAL' por 'SI' ya que de el BPM se envia el valor
								SI para el diagnostico principal y al cambiar el diagnostico 
								en el BPM ningun diagnostico quedaba como el principal.   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 05-05-2017 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se agrega variable @dgnstco_prncpl_si para hacer la validacion
							    y actualzacion del campo prncpl de la tabla temporal 
								#tmpDiagnosticos para los casos que el diagnostico viene 
								marcado como 'SI' actualizarlo a 'PRINCIPAL'   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 08-05-2017 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se realizan modificaciones para guardar el recobro por 
								servicio. 
								Se realizan mejoras al proceso, de guardado, se quita 
								borrado de diagnosticos y se agrega tabla de vigencias DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 22-05-2017 FM\>
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se realizan modificaciones para guardar las causales
								de no cobro de cuota de recuperacion seleccionadas 
								por el auditor. Puto 19 Control de cambio 092 DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 13-06-2017 FM\>
*---------------------------------------------------------------------------------*/
/*<gestion>
	<usuarioGestion></usuarioGestion>
	<afiliado>
		<tipoDocumento></tipoDocumento>
		<numDocumento></numDocumento>
		<altoRiesgo></altoRiesgo>
		<codRiesgo></codRiesgo>
	</afiliado>
	<solicitud>
		<numSolicitud></numSolicitud>
		<diagnosticos>
			<diagnostico>
				<principal></principal>
				<codigoDiagnostico></codigoDiagnostico>
				<asignadoSOS></asignadoSOS>
			</diagnostico>
			<diagnostico>
				<principal></principal>
				<codigoDiagnostico></codigoDiagnostico>
				<asignadoSOS></asignadoSOS>
			</diagnostico>
		</diagnosticos>
		<observacionSolicitud></observacionSolicitud>
	</solicitud>
	<prestacionesSolicitadas>
		<prestacionSolicitada>
			<tipoPrestacion></tipoPrestacion>
			<codigoPrestacion></codigoPrestacion>
			<estadoPrestacion></estadoPrestacion>
			<reqAuditoria></reqAuditoria>   
		</prestacionSolicitada>
		<prestacionSolicitada>
			<tipoPrestacion></tipoPrestacion>
			<codigoPrestacion></codigoPrestacion>
			<estadoPrestacion></estadoPrestacion>
			<reqAuditoria></reqAuditoria>   
		</prestacionSolicitada>
	</prestacionesSolicitadas>
	<numPrestacion></numPrestacion>
	<gestionAuditor>
		<cantidadSOS></cantidadSOS>
		<lateralidad></lateralidad>
		<viaAcceso></viaAcceso>
		<recobro></recobro>
		<dosis></dosis>
		<presentacion></presentacion>
		<cada></cada>
		<frecuencia></frecuencia>
		<codEstadoPrestacion></codEstadoPrestacion>
		<causas>
			<codCausa></codCausa>
			<codCausa></codCausa>
		</causas>
		<justificacion></justificacion>
		<observacionOPS></observacionOPS>
	</gestionAuditor>
	<datosNegacion>
		<fundamentosLegales>
			<codFundamento></codFundamento>
			<codFundamento></codFundamento>
		</fundamentosLegales>
		<usrMedicoAuditor></usrMedicoAuditor>
		<alternativas>
			<codAlternativa></codAlternativa>
			<codAlternativa></codAlternativa>
		</alternativas>
	</datosNegacion>
</gestion>*/

ALTER PROCEDURE [gsa].[spASGuardarInformacionGestionAuditoria]
	@infrmcion_prstcion		xml,
	@lcdscrpcn_tpo_adtra	udtDescripcion,
	@cdgo_rsltdo			udtConsecutivo  output,
	@mnsje_rsltdo			varchar(2000)	output 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @codigo_ok						 Int           ,
			@codigo_error					 Int           ,
			@mensaje_ok						 udtDescripcion,
			@mensaje_error					 Varchar(2000) ,
			@lncnsctvo_srvco_slctdo			 udtConsecutivo,
			@lnmro_slctd_prvdr				 Varchar(15)   ,
			@lcusro_crcn                     udtUsuario    ,
			@lncnsctvo_cdgo_rsltdo_gstn_adtr udtConsecutivo,
			@ldFechaActual				     Date		   ,
			@alto_rsgo						 UdtLogico     , 
			@cdgo_rsgo						 udtConsecutivo,
			@nmro_unco_idntfccn_afldo		 udtNumeroIdentificacionLargo,
			@ValorCero						 Int           ,
			@re_cbro						 udtConsecutivo,
			@dgnstco_prncpl					 Varchar(12)   ,
			@valor1							 Int           ,
			@valor2							 Int           ,
			@usrMedicoAuditor				 udtUsuario    ,
			@obsrvcn_slctd					 udtObservacion,
			@ValorVacio						 udtLogico,
			@dgnstco_prncpl_si				 VarChar(12),
			@fn_vgnca						 Date

	Declare  @idDiagnosticos Table(cnsctvo_dgnstco_slctd_atrzcn_srvco	udtConsecutivo,
								   cnsctvo_cdgo_dgnstco					udtConsecutivo)	

	Create 
	Table #tmpInformacionPrestacion(cnsctvo_srvco_slctdo				udtConsecutivo,
									nmro_slctd_prvdr					Varchar(15)   ,
									cnsctvo_cdgo_prstcn_prstdr			udtConsecutivo,
									cnsctvo_cdgo_estdo_prstcion			udtConsecutivo,
									jstfccn_gstn_adtr					Varchar(2000) ,
									obsrvcn_gstn_adtr					Varchar(2000) ,
	 								cntddSS								Int           ,
									ltrldd								udtConsecutivo,
									va_accso							udtConsecutivo,
									re_cbro								udtConsecutivo,
									dss									Int           ,
									prsntcn_dss							udtConsecutivo,
									cda									Int           ,
									frcnca								udtConsecutivo,
									cnsctvo_cdgo_csa_no_cbro_cta_rcprcn	udtConsecutivo
	                               )
	   	
	Create 
	Table #tmpInformacionCausa(cnsctvo_srvco_slctdo			 udtConsecutivo,
							   nmro_slctd_prvdr				 Varchar(15)   ,
							   cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo
							  )
		
	Create 
	Table #tmpAlternativa(cnsctvo_srvco_slctdo udtConsecutivo,
		                  nmro_slctd_prvdr	   Varchar(15)   ,
		                  altrntva             udtDescripcion
						 )
	
	-- 
	Create 
	Table #tmpDiagnosticos(prncpl								Varchar(15)   ,
						   cdgo_dgnstco							Varchar(15)   ,
						   cnsctvo_cdgo_dgnstco					udtConsecutivo,
						   cnsctvo_cdgo_tpo_dgnstco				udtConsecutivo Default 2,
						   cnsctvo_dgnstco_slctd_atrzcn_srvco	udtConsecutivo,
						   cnsctvo_cdgo_cntngnca					udtConsecutivo,
						   cnsctvo_cdgo_rcbro					udtConsecutivo,
						   cdgo_rcbro							udtCodigoDiagnostico
						  )
	
	--
	Create 
	Table #tmpFundamentosLegales(cdgo_fndmnto udtConsecutivo)

	-- 
	Create 
	Table  #Tempo_Anexos(id								Int	NOT NULL  ,
					     cnsctvo_cdgo_mdlo_dcmnto_sprte	udtConsecutivo,
					     cnsctvo_cdgo_dcmnto_sprte		udtConsecutivo,
					     usro_crcn						udtUsuario 
	                    )

	-- 
	Set @codigo_ok	  = 0
	Set @codigo_error = -1
	Set @mensaje_ok	  = 'Proceso ejecutado satisfactoriamente'
	Set @ValorCero    = 0
	Set @dgnstco_prncpl    = 'PRINCIPAL'
	Set @valor1		  = 1
	Set @valor2		  = 2
	Set @ValorVacio	  = ''
	Set @mnsje_rsltdo = @mensaje_ok
	Set @cdgo_rsltdo  = @codigo_ok
	Set	@ldFechaActual = GETDATE()
	Set @dgnstco_prncpl_si = 'SI'
	Set @fn_vgnca = '99991231';

	--extraccion informacion de la solicitud asociada a la prestacion
	Select @lnmro_slctd_prvdr  = pref.value('(numSolicitud/text())[1]','varchar(15)'),
	       @obsrvcn_slctd	   = Isnull(pref.value('(observacionSolicitud/text())[1]', 'nvarchar(max)'), '') 
	From   @infrmcion_prstcion.nodes('/gestion/solicitud') AS xml_gesdom(Pref);
	
	-- 
	Insert 
	Into   #tmpDiagnosticos(prncpl, cdgo_dgnstco)
	Select Isnull(pref.value('(principal/text())[1]', 'nvarchar(max)'), ''), 
	       Isnull(pref.value('(codigoDiagnostico/text())[1]', 'nvarchar(max)'), '')
	From   @infrmcion_prstcion.nodes('/gestion/solicitud/diagnosticos/diagnostico') AS xml_gesdom(Pref);

	-- Si el campo principal llega como 'SI' se actualiza a 'PRINCIPAL' -- qvisionclr
	Update		#tmpDiagnosticos
	Set			prncpl = @dgnstco_prncpl
	Where		Ltrim(Rtrim(prncpl)) = @dgnstco_prncpl_si

	-- 
	Update		#tmpDiagnosticos
	Set			cnsctvo_cdgo_tpo_dgnstco = @valor1
	Where		Ltrim(Rtrim(prncpl)) = @dgnstco_prncpl

    -- 
	Update     diag
	Set        cnsctvo_cdgo_dgnstco = a.cnsctvo_cdgo_dgnstco
	From       #tmpDiagnosticos diag
	Inner Join bdSisalud.dbo.tbDiagnosticos_Vigencias a WITH (NOLOCK) 
	On         a.cdgo_dgnstco = diag.cdgo_dgnstco       
    Where 	   a.vsble_usro  = 'S'
	AND        @ldFechaActual Between a.inco_vgnca And a.fn_vgnca 
  
	-- 
	Insert 
	Into   #tmpFundamentosLegales(cdgo_fndmnto)
	Select Isnull(pref.value('(codFundamento/text())[1]', 'nvarchar(max)'), '')
	From   @infrmcion_prstcion.nodes('/gestion/datosNegacion/fundamentosLegales') AS xml_gesdom(Pref);

	 -- extraccion informacion general de la gestion domiciliaria
    Select @lncnsctvo_srvco_slctdo = pref.value('(numPrestacion/text())[1]','udtConsecutivo'), 
 	       @lcusro_crcn			   = pref.value('(usuarioGestion/text())[1]','udtUsuario') 
	From   @infrmcion_prstcion.nodes('/gestion') AS xml_gesdom(Pref);

	-- extraccion informacion riesgo 
	Select @alto_rsgo				= pref.value('(altoRiesgo/text())[1]','udtLogico'),
	       @cdgo_rsgo				= pref.value('(codRiesgo/text())[1]','udtConsecutivo'),
	       @nmro_unco_idntfccn_afldo = pref.value('(numDocumento/text())[1]','udtNumeroIdentificacionLargo')
	From   @infrmcion_prstcion.nodes('/gestion/afiliado') AS xml_gesdom(Pref);


	Select @nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo 
	From   bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b WITH (NOLOCK)
	Where b.cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr
	
	--temporal de informacion general de la prestacion que se va a modificar
    Insert 
	Into #tmpInformacionPrestacion (cnsctvo_srvco_slctdo, nmro_slctd_prvdr , cnsctvo_cdgo_estdo_prstcion,	
								   jstfccn_gstn_adtr   , obsrvcn_gstn_adtr, cntddSS                    ,
								   ltrldd              , va_accso         , re_cbro                    ,
								   dss                 , prsntcn_dss      , cda                        ,
								   frcnca			   , cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
								  )		
	 Select @lncnsctvo_srvco_slctdo											cnsctvo_srvco_slctdo,
	        @lnmro_slctd_prvdr												nmro_slctd_prvdr,
	        Isnull(Convert(int,pref.value('(codEstadoPrestacion/text())[1]','udtConsecutivo')),@ValorCero),
			pref.value('(justificacion/text())[1]','varchar(2000)')			jstfccn_gstn_adtr,
			pref.value('(observacionOPS/text())[1]','varchar(2000)')		obsrvcn_gstn_adtr,
			pref.value('(cantidadSOS/text())[1]','int'),
			Isnull(Convert(int,pref.value('(lateralidad/text())[1]','udtConsecutivo')),@ValorCero),
			Isnull(Convert(int,pref.value('(viaAcceso/text())[1]','udtConsecutivo')),@ValorCero),
			pref.value('(recobro/text())[1]','udtConsecutivo'),
			Isnull(Convert(int,pref.value('(dosis/text())[1]', 'nvarchar(max)')),@ValorCero),
			Isnull(Convert(int,pref.value('(presentacion/text())[1]', 'nvarchar(max)')),@ValorCero),
			Isnull(Convert(int,pref.value('(cada/text())[1]', 'int')),@ValorCero),
			Isnull(Convert(int,pref.value('(frecuencia/text())[1]', 'udtConsecutivo')),@ValorCero),
			pref.value('(causaNoCobro/text())[1]','udtConsecutivo')
	 From   @infrmcion_prstcion.nodes('/gestion/gestionAuditor') AS xml_gesdom(Pref);


	-- temporal de informacion Gestion Auditor causas
	Insert 
	Into   #tmpInformacionCausa(cnsctvo_srvco_slctdo, nmro_slctd_prvdr,	cnsctvo_cdgo_no_cnfrmdd_vldcn)
	Select @lncnsctvo_srvco_slctdo											cnsctvo_srvco_slctdo,
	       @lnmro_slctd_prvdr												nmro_slctd_prvdr,
	       pref.value('(codCausa/text())[1]','udtConsecutivo')				cod_causa
	From   @infrmcion_prstcion.nodes('/gestion/gestionAuditor/causas') AS xml_gesdom(Pref);
	
	-- extraccion de informacion Gestion Informacion alternativas
	Insert 
    Into #tmpAlternativa(cnsctvo_srvco_slctdo, nmro_slctd_prvdr, altrntva)
	Select @lncnsctvo_srvco_slctdo											cnsctvo_srvco_slctdo,
	       @lnmro_slctd_prvdr												nmro_slctd_prvdr,
           pref.value('(codAlternativa/text())[1]','udtDescripcion')		altrntva
	From   @infrmcion_prstcion.nodes('/gestion/datosNegacion/alternativas') AS xml_gesdom(Pref);

	Select @usrMedicoAuditor  = Isnull(pref.value('(usrMedicoAuditor/text())[1]', 'udtUsuario'), '')
	From   @infrmcion_prstcion.nodes('/gestion/datosNegacion') AS xml_gesdom(Pref);

	Insert 
	Into   #Tempo_Anexos(id                       , cnsctvo_cdgo_mdlo_dcmnto_sprte,		
						 cnsctvo_cdgo_dcmnto_sprte, usro_crcn
					    )
    Select	Convert(int,pref.value('(id/text())[1]', 'nvarchar(max)'))
					,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_mdlo_dcmnto_sprte/text())[1]', 'nvarchar(max)')),@ValorCero)
					,Isnull(Convert(int,pref.value('(cnsctvo_cdgo_dcmnto_sprte/text())[1]', 'nvarchar(max)')),@ValorCero)
					,Isnull(Pref.value('(usro_crcn/text())[1]', 'nvarchar(max)'),@ValorVacio)
	From   @infrmcion_prstcion.nodes('/gestion/infrmcn_anxs/tbCNADocumentosAnexosServicios') AS xml_gesdom(Pref);												  	
	

	 If @usrMedicoAuditor is null
		 Begin
			Set @usrMedicoAuditor = @lcusro_crcn
		 End

	--Iniciar transaccion para guardar las modificaciones
	BEGIN TRY
		If @cdgo_rsgo <> 0 
		   Begin
				Insert 
				Into   bdRiesgosSalud.dbo.tbReporteCohortesxAfiliado(nmro_unco_idntfccn_afldo, cnsctvo_cdgo_chrte, fcha_crcn        ,
																	 usro_crcn               , fcha_ultma_mdfccn , usro_ultma_mdfccn
																    )
				Values(@nmro_unco_idntfccn_afldo, @cdgo_rsgo    , @ldFechaActual,
					   @lcusro_crcn             , @ldFechaActual, @lcusro_crcn
				      )																	 
			End;
		--Actualizar el estado en la tabla 	tbASServiciosSolicitados
		Update     ss
		Set        cnsctvo_cdgo_estdo_srvco_slctdo = ip.cnsctvo_cdgo_estdo_prstcion,
				   cntdd_slctda					   = ip.cntddSS,	
			       fcha_ultma_mdfccn               = @ldFechaActual,	
			       usro_ultma_mdfccn               = @lcusro_crcn,
				   cnsctvo_cdgo_rcbro			   = ip.re_cbro, -- Se actualiza el recobro de la prestacion, control de cambio 068
				   cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = ip.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn
		From       gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion ip
		On         ip.cnsctvo_srvco_slctdo        = ss.cnsctvo_srvco_slctdo 

		Select @re_cbro = IP.re_cbro
		From  #tmpInformacionPrestacion IP	  
		     
		-- 
		Update     ss
		Set        cnsctvo_cdgo_estdo_srvco_slctdo = ip.cnsctvo_cdgo_estdo_prstcion,
				   cntdd_slctda					   = ip.cntddSS,	
			       fcha_ultma_mdfccn               = @ldFechaActual,	
			       usro_ultma_mdfccn               = @lcusro_crcn,
				   cnsctvo_cdgo_rcbro			   = ip.re_cbro
		From       gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion ip
		On         ip.cnsctvo_srvco_slctdo        = ss.cnsctvo_srvco_slctdo 

		-- 
		Update     MSS 
		Set	       dss							= IP.dss,
			       cnsctvo_cdgo_prsntcn_dss	    = IP.prsntcn_dss,
			       prdcdd_dss					= IP.cda,
			       cnsctvo_cdgo_undd_prdcdd_dss = IP.frcnca,
			       fcha_ultma_mdfccn			= @ldFechaActual,
			       usro_ultma_mdfccn			= @lcusro_crcn
		From       BDCna.gsa.tbASMedicamentosSolicitados MSS WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  MSS.cnsctvo_srvco_slctdo = IP.cnsctvo_srvco_slctdo
		Where      IP.dss > 0 And IP.prsntcn_dss > 0	

		--Se actualiza la tabla Medicamentos Original
		Update     MSS 
		Set	       dss							= IP.dss,
			       cdgo_prsntcn_dss				= b.cdgo_dss,
			       fcha_ultma_mdfccn			= @ldFechaActual,
			       usro_ultma_mdfccn			= @lcusro_crcn
		From       BDCna.gsa.tbASMedicamentosSolicitadosOriginal MSS WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  MSS.cnsctvo_dto_srvco_slctdo_orgnl = IP.cnsctvo_srvco_slctdo
		Inner Join bdSISalud.dbo.tbMNDosis_Vigencias b WITH(NOLOCK) On IP.prsntcn_dss = b.cnsctvo_cdgo_dss
		Where      IP.dss > 0 And IP.prsntcn_dss > 0	

		Update     MSS 
		Set	       prdcdd_dss					= IP.cda,
			       cdgo_prsntcn					= IP.frcnca
		From       BDCna.gsa.tbASMedicamentosSolicitadosOriginal MSS WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  MSS.cnsctvo_dto_srvco_slctdo_orgnl = IP.cnsctvo_srvco_slctdo
		Inner Join bdSISalud.dbo.tbPresentaciones_Vigencias b WITH(NOLOCK) On ip.frcnca = b.cnsctvo_cdgo_prsntcn
		Where      IP.dss > 0 And IP.prsntcn_dss > 0	

		-- Actualiza la Observación de la solicitud
		If @obsrvcn_slctd is not null
			Begin
				Update SAS Set obsrvcn_adcnl = @obsrvcn_slctd
				From   BDCna.gsa.tbASSolicitudesAutorizacionServicios SAS WITH (NOLOCK)
				where  cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr
			End
		
		Update     PR 
		Set		   undd_cntdd				= IP.cntddSS,
				   fcha_ultma_mdfccn		= @ldFechaActual,
				   usro_ultma_mdfccn		= @lcusro_crcn
		From       BDCna.gsa.tbASProcedimientosInsumosSolicitados PR WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  PR.cnsctvo_srvco_slctdo = IP.cnsctvo_srvco_slctdo
		
		Update     PR 
		Set	       cnsctvo_cdgo_ltrldd		= IP.ltrldd
				   From       BDCna.gsa.tbASProcedimientosInsumosSolicitados PR WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  PR.cnsctvo_srvco_slctdo = IP.cnsctvo_srvco_slctdo
		Where      IP.ltrldd > 0
		
		Update     PR 
		Set	       cnsctvo_cdgo_va_accso	= IP.va_accso
		From       BDCna.gsa.tbASProcedimientosInsumosSolicitados PR WITH (NOLOCK)
		Inner Join #tmpInformacionPrestacion IP On  PR.cnsctvo_srvco_slctdo = IP.cnsctvo_srvco_slctdo
		Where      IP.va_accso > 0

			
		If Exists(Select 1 From #tmpDiagnosticos)
			Begin
				
				-- Se recupera la informacion del recobro y la contingencia
				Update		d
				Set			cnsctvo_cdgo_rcbro = ds.cnsctvo_cdgo_rcbro,
							cnsctvo_cdgo_cntngnca = ds.cnsctvo_cdgo_cntngnca,
							cdgo_rcbro = dso.cdgo_rcbro							
				From		#tmpDiagnosticos d
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios ds WITH (NOLOCK)
				On			d.cnsctvo_cdgo_tpo_dgnstco = ds.cnsctvo_cdgo_tpo_dgnstco
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal dso With(NoLock)
				On			dso.cnsctvo_dgnstco_slctd_atrzcn_srvco = ds.cnsctvo_dgnstco_slctd_atrzcn_srvco
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias  dsv With(RowLock)
				On			dsv.cnsctvo_dgnstco_slctd_atrzcn_srvco = ds.cnsctvo_dgnstco_slctd_atrzcn_srvco
				Where		ds.cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr
				And			ds.cnsctvo_cdgo_tpo_dgnstco = @valor1
				And			getDAte() Between dsv.inco_vgnca And dsv.fn_vgnca

				
				Update		#tmpDiagnosticos
				Set			cnsctvo_cdgo_rcbro = @ValorCero,
							cnsctvo_cdgo_cntngnca = @ValorCero,
							cdgo_rcbro = @ValorCero
				Where cnsctvo_cdgo_tpo_dgnstco = @valor2

				Delete 
				From #tmpDiagnosticos
				Where cnsctvo_cdgo_tpo_dgnstco Is Null
				And	cnsctvo_cdgo_cntngnca Is Null

				-- Se cierran las vigencias para el diagnostico.
				Update		dsv
				Set			fn_vgnca = @ldFechaActual,
							fcha_ultma_mdfccn = @ldFechaActual,
							usro_ultma_mdfccn = @usrMedicoAuditor
				From		BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios DS WITH (NOLOCK)
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias  dsv With(RowLock)
				On			dsv.cnsctvo_dgnstco_slctd_atrzcn_srvco = ds.cnsctvo_dgnstco_slctd_atrzcn_srvco
				Left Join	#tmpDiagnosticos d
				On			d.cnsctvo_cdgo_tpo_dgnstco = ds.cnsctvo_cdgo_tpo_dgnstco
				And			d.cnsctvo_cdgo_dgnstco = ds.cnsctvo_cdgo_dgnstco
				Where		DS.cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr
				And			@ldFechaActual Between dsv.inco_vgnca And dsv.fn_vgnca
				And			d.cnsctvo_cdgo_dgnstco Is Null

				Delete		d
				From		#tmpDiagnosticos d
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios DS WITH (NOLOCK)
				On			d.cnsctvo_cdgo_tpo_dgnstco = ds.cnsctvo_cdgo_tpo_dgnstco
				And			d.cnsctvo_cdgo_dgnstco = ds.cnsctvo_cdgo_dgnstco
				Inner Join	BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias  dsv With(RowLock)
				On			dsv.cnsctvo_dgnstco_slctd_atrzcn_srvco = ds.cnsctvo_dgnstco_slctd_atrzcn_srvco				
				Where		@ldFechaActual Between dsv.inco_vgnca And dsv.fn_vgnca
				And			DS.cnsctvo_slctd_atrzcn_srvco = @lnmro_slctd_prvdr

				-- 
				Insert
			    Into   BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios(cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_dgnstco,
																				cnsctvo_cdgo_dgnstco      , cnsctvo_cdgo_cntngnca   ,
																				cnsctvo_cdgo_rcbro        , fcha_crcn               ,
																				usro_crcn                 , fcha_ultma_mdfccn       ,
																				usro_ultma_mdfccn
																			   )
				OUTPUT inserted.cnsctvo_dgnstco_slctd_atrzcn_srvco,Inserted.cnsctvo_cdgo_dgnstco Into @idDiagnosticos
				Select @lnmro_slctd_prvdr                                       , di.cnsctvo_cdgo_tpo_dgnstco,
					   di.cnsctvo_cdgo_dgnstco                                  , di.cnsctvo_cdgo_cntngnca       ,
					   di.cnsctvo_cdgo_rcbro									, @ldFechaActual,
					   @lcusro_crcn                                             , @ldFechaActual,
					   @lcusro_crcn
				From		#tmpDiagnosticos Di							

				-- 
				Insert 
			    Into       BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal(cnsctvo_dgnstco_slctd_atrzcn_srvco, cnsctvo_slctd_atrzcn_srvco,
																						    cdgo_tpo_dgnstco                  , cdgo_dgnstco              ,
																						    cdgo_cntngnca                     , cdgo_rcbro			      , 
																							fcha_crcn                         , usro_crcn                 ,
																						    fcha_ultma_mdfccn                 , usro_ultma_mdfccn					
				                                                                           )
				Select		id.cnsctvo_dgnstco_slctd_atrzcn_srvco                         , @lnmro_slctd_prvdr,
							Case When DI.prncpl = @dgnstco_prncpl THEN @valor1 ELSE @valor2 END, Di.cdgo_dgnstco   ,
							@valor1                                                       , 
							di.cdgo_rcbro												,
							@ldFechaActual                                                , @lcusro_crcn,
							@ldFechaActual                                                , @lcusro_crcn					
				From		#tmpDiagnosticos Di
				Inner Join	@idDiagnosticos id 
				On			id.cnsctvo_cdgo_dgnstco = Di.cnsctvo_cdgo_dgnstco
				

				Insert Into bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias
				(
						cnsctvo_dgnstco_slctd_atrzcn_srvco,		cnsctvo_slctd_atrzcn_srvco,
						cnsctvo_cdgo_dgnstco,					inco_vgnca,
						fn_vgnca,								fcha_crcn,
						usro_crcn,								fcha_ultma_mdfccn,
						usro_ultma_mdfccn
				)
				Select		id.cnsctvo_dgnstco_slctd_atrzcn_srvco, @lnmro_slctd_prvdr	, 
							di.cnsctvo_cdgo_dgnstco              , @ldFechaActual		,
						    @fn_vgnca							 , @ldFechaActual		,
						    @lcusro_crcn                         , @ldFechaActual		,
						    @lcusro_crcn
				From		#tmpDiagnosticos Di	
				Inner Join	@idDiagnosticos id 
				On			id.cnsctvo_cdgo_dgnstco = Di.cnsctvo_cdgo_dgnstco	

			End			

		--insertar informacion general de la prestacion
		Insert 
		Into   bdCNA.gsa.tbASResultadoGestionAuditor(cnsctvo_srvco_slctdo, cnsctvo_cdgo_estdo_no_cnfrmdd,
				                                     usro_lgn_adtr       , dscrpcn_tpo_adtra            ,
				                                     jstfccn_gstn_adtr   , obsrvcn_gstn_adtr            ,
				                                     fcha_crcn           , usro_crcn                    ,
				                                     fcha_ultma_mdfccn   , usro_ultma_mdfccn
													)
		Select 	ga.cnsctvo_srvco_slctdo, ga.cnsctvo_cdgo_estdo_prstcion,
				@usrMedicoAuditor      , @lcdscrpcn_tpo_adtra          ,
				ga.jstfccn_gstn_adtr   , ga.obsrvcn_gstn_adtr          ,
				@ldFechaActual         , @lcusro_crcn                  ,
				@ldFechaActual         , @lcusro_crcn
		From    #tmpInformacionPrestacion ga
		Where   ga.cnsctvo_srvco_slctdo is not null
			
		Select @lncnsctvo_cdgo_rsltdo_gstn_adtr = SCOPE_IDENTITY();		

		--insertar causas asociadas al estado de la prestacion
		If Exists(Select 1 From #tmpInformacionCausa)
			Begin
				Insert 
				Into    bdCNA.gsa.tbASResultadoDetalleGestionAuditor(cnsctvo_cdgo_rsltdo_gstn_adtr, cnsctvo_cdgo_no_cnfrmdd_vldcn,
						                                             fcha_crcn                    , usro_crcn                    ,
						                                             fcha_ultma_mdfccn            , usro_ultma_mdfccn
																	)
				Select  @lncnsctvo_cdgo_rsltdo_gstn_adtr, ifc.cnsctvo_cdgo_no_cnfrmdd_vldcn,
						@ldFechaActual                  , @lcusro_crcn                     ,
						@ldFechaActual                  , @lcusro_crcn
				From #tmpInformacionCausa ifc
			End

		--insertar alternativas prestacion
		If Exists(Select 1 From #tmpAlternativa)
			Begin

				Delete 
				From   BDCna.gsa.tbASResultadoAlternativasGestionAuditor
				Where  cnsctvo_srvco_slctdo = @lncnsctvo_srvco_slctdo
				
				Insert 
				Into    BDCna.gsa.tbASResultadoAlternativasGestionAuditor(cnsctvo_srvco_slctdo, altrntva         ,
						                                                  fcha_crcn           , usro_crcn        ,
						                                                  fcha_ultma_mdfccn   , usro_ultma_mdfccn
																		 )
				Select  al.cnsctvo_srvco_slctdo, al.altrntva ,
						@ldFechaActual         , @lcusro_crcn,
						@ldFechaActual         , @lcusro_crcn
				From #tmpAlternativa al
			End

		If Exists(Select 1 From #tmpFundamentosLegales)
			Begin
				Insert Into gsa.tbASFundamentosLegalesxServiciosSolicitados(
					cnsctvo_srvco_slctdo,
					cnsctvo_cdgo_fndmnto_lgl,
					fcha_crcn,
					usro_crcn,
					fcha_ultma_mdfccn,
					usro_ultma_mdfccn
				)
				Select 
					@lncnsctvo_srvco_slctdo,
					f.cdgo_fndmnto,
					@ldFechaActual,
					@lcusro_crcn,
					@ldFechaActual,
					@lcusro_crcn
				From #tmpFundamentosLegales f	
							
			End

		If Exists(Select 1 From #Tempo_Anexos)
			Begin
				Insert
				Into	BdCNA.gsa.tbASDocumentosAnexosServicios(cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_mdlo_dcmnto_sprte,
					                                            cnsctvo_cdgo_dcmnto_sprte , fcha_crcn                     ,
					                                            usro_crcn                 , fcha_ultma_mdfccn             ,
					                                            usro_ultma_mdfccn
															   )
		        Select @lnmro_slctd_prvdr           , ANX.cnsctvo_cdgo_mdlo_dcmnto_sprte,	
					   ANX.cnsctvo_cdgo_dcmnto_sprte, @ldFechaActual	                ,
					   ANX.usro_crcn				, @ldFechaActual                    ,	
					   ANX.usro_crcn		
		        From   #Tempo_Anexos	ANX 

			End
		
		Set @cdgo_rsltdo  =  @codigo_ok
		Set @mnsje_rsltdo = @mensaje_ok

	END TRY
	BEGIN CATCH		 
		SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
				   				'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
								'Message:' + ERROR_MESSAGE() + CHAR(13) +
								'Procedure:' + ERROR_PROCEDURE();
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error

		RaisError(@mensaje_error, 1, 16) With SetError

	END CATCH 

	DROP Table #tmpDiagnosticos
	DROP Table #tmpInformacionPrestacion
	DROP Table #tmpInformacionCausa
	DROP Table #tmpAlternativa
	DROP Table #tmpFundamentosLegales
	DROP Table #Tempo_Anexos
END

