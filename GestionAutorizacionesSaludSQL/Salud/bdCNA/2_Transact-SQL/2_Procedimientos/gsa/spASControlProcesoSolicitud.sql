USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASControlProcesoSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASControlProcesoSolicitud
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite validar si una solicitud se encuentra en proceso D\>
  						<\D y si no, la crea y la deja en proceso.   D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 03/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASControlProcesoSolicitud] @tpo_prcso udtCodigo,
@tpo_trnsccn udtLogico,
@usro udtUsuario,
@obsrvcns VARCHAR(2000),
@vldo udtLogico OUTPUT
AS

Begin
  SET NOCOUNT ON

  DECLARE @afirmacion char(1),
          @negacion char(1),
          @inexistente char(1),
          @cantidad numeric(3),
          @fecha datetime,
		  @xstate int,
		  @trancount int,
		  @variableSetPoint	VARCHAR(32),
          @creacion char(1);

  SELECT
    @afirmacion = 'S',
    @negacion = 'N',
    @inexistente = 'I',
    @cantidad = 0,
    @fecha = GETDATE(),
    @creacion = 'C';

    --Se valida que efectivamente exista el consecutivo de solicitud.
  IF NOT EXISTS (SELECT
      so.id
    FROM #tmpNmroSlctds so
    WHERE so.cnsctvo_slctd_srvco_sld_rcbda IS NULL)

	BEGIN

			IF @tpo_trnsccn = @creacion
			BEGIN

			  IF NOT EXISTS(SELECT 1 FROM #tmpNmroSlctds ns WHERE ns.cntrl_prcso = @afirmacion)
			  BEGIN
				MERGE  bdCNA.gsa.tbASControlProcesoSolicitudesAutorizacionServicios AS target
				USING (SELECT
				  ns.cnsctvo_slctd_srvco_sld_rcbda cnsctvo_slctd_srvco_sld_rcbda
				FROM #tmpNmroSlctds ns WITH (NOLOCK)) AS source (cnsctvo_slctd_srvco_sld_rcbda)
				ON (target.cnsctvo_slctd_atrzcn_srvco = source.cnsctvo_slctd_srvco_sld_rcbda
				AND target.tpo_prcso = @tpo_prcso)
				WHEN MATCHED THEN
				UPDATE SET en_prcso = @negacion,
				  fcha_ultma_mdfccn = @fecha,
				  usro_ultma_mdfccn = @usro
				WHEN NOT MATCHED THEN
				INSERT (cnsctvo_slctd_atrzcn_srvco,en_prcso,tpo_prcso,fcha_crcn,usro_crcn,fcha_ultma_mdfccn,usro_ultma_mdfccn, vsble_usro)
				VALUES (source.cnsctvo_slctd_srvco_sld_rcbda,@negacion, @tpo_prcso, @fecha, @usro, @fecha, @usro, @afirmacion);
				END
			END
			ELSE
			BEGIN
			  --Se actualiza el final de la ejecución.
			  UPDATE SS 
			  SET en_prcso = @negacion,
				  fcha_ultma_mdfccn = @fecha,
				  usro_ultma_mdfccn = @usro,
				  obsrvcns = @obsrvcns
			  FROM bdCNA.gsa.tbASControlProcesoSolicitudesAutorizacionServicios ss WITH (NOLOCK)
			  INNER JOIN #tmpNmroSlctds ns WITH (NOLOCK)
				ON (ns.cnsctvo_slctd_srvco_sld_rcbda = ss.cnsctvo_slctd_atrzcn_srvco)
			  WHERE ss.tpo_prcso = @tpo_prcso;

			END

	END
END

GO
