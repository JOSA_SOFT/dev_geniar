USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPoblarTemporalHomologacionPrestacionesPrestador]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASPoblarTemporalHomologacionPrestacionesPrestador											
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Sp Poblar Temporales para ejecución Masiva ó por demanda de la Homologación de Prestaciones
						  del prestador																				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/29																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASPoblarTemporalHomologacionPrestacionesPrestador]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null

AS

Begin
	SET NOCOUNT ON
	Declare			@ValorCero		int = 0
	-- 
	Insert		#ServiciosSolicitados
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo,			cnsctvo_cdgo_srvco_slctdo,
				cdgo_intrno
	)
	select	
				ss.cnsctvo_slctd_atrzcn_srvco,		ss.cnsctvo_srvco_slctdo,		ss.cnsctvo_cdgo_srvco_slctdo,
				iss.cdgo_intrno
	from		gsa.tbASServiciosSolicitados	ss with(nolock)
	Inner Join	gsa.tbASIPSSolicitudesAutorizacionServicios iss with(nolock)
	On			iss.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_cdgo_prstcn_prstdr = @ValorCero
	And			(@cnsctvo_slctd_atrzcn_srvco is null or ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco)

End
GO
