USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDocumentosAnexosxSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultarDocumentosAnexosxSolicitud  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar los documentos anexos a una solicitud
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/17/26 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------      
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------    
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\DM DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*-----------------------------------------------------------------------------------------------------------------------  */
--exec [gsa].spASConsultarDocumentosAnexosxSolicitud 63

ALTER PROCEDURE [gsa].[spASConsultarDocumentosAnexosxSolicitud]
  @cnsctvo_slctd_atrzcn_srvco	udtConsecutivo

AS
BEGIN
	SET NOCOUNT ON;

	Declare @ldFechaActual Datetime

	Create 
	Table     #tempDocumentosAnexos(cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
	                                cnsctvo_cdgo_dcmnto_sprte  udtConsecutivo,
									cnsctvo_cdgo_dcmnto        udtConsecutivo,
									dscrpcn_dcmnto_sprte       udtDescripcion
	                               )

	Set @ldFechaActual = getDate()

    Insert
	Into       #tempDocumentosAnexos(cnsctvo_cdgo_dcmnto_sprte, cnsctvo_slctd_atrzcn_srvco,
	                                 cnsctvo_cdgo_dcmnto      , dscrpcn_dcmnto_sprte     
									)
	Select     Distinct a.cnsctvo_cdgo_dcmnto_sprte, a.cnsctvo_slctd_atrzcn_srvco,
	                    a.cnsctvo_cdgo_dcmnto      , d.dscrpcn_dcmnto_sprte           
    From       bdCNA.gsa.tbASDocumentosAnexosServicios a With(NoLock)
	Inner Join bdCNA.sol.tbDocumentosSoporte_Vigencias d  With(NoLock)
	On         d.cnsctvo_cdgo_dcmnto_sprte = a.cnsctvo_cdgo_dcmnto_sprte	
	Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And        @ldFechaActual Between d.inco_vgnca And d.fn_vgnca


	Select cnsctvo_cdgo_dcmnto_sprte, cnsctvo_slctd_atrzcn_srvco, 
	       cnsctvo_cdgo_dcmnto      , dscrpcn_dcmnto_sprte
	From   #tempDocumentosAnexos
END
GO
