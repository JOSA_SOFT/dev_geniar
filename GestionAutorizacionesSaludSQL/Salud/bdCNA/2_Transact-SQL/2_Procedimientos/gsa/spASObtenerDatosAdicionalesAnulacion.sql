Use bdCNA
Go

If(Object_id('gsa.spASObtenerDatosAdicionalesAnulacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASObtenerDatosAdicionalesAnulacion As Select 1';
End
Go


/*------------------------------------------------------------------------------------------ 
* Método o PRG		:	  spASObtenerDatosAdicionalesAnulacion										 
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez A\>	 
* Descripción		: <\D SP que consulta los datos adicionales para la anulacion de 
						  prestaciones-Solicitudes D\>	 
* Observaciones		: <\O																			O\>	 
* Parámetros		: <\P																			P\>	 
* Variables			: <\V																			V\>	 
* Fecha Creación	: <\FC	2016/03/07																FC\> 
*------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACIÓN																		 
*------------------------------------------------------------------------------------------- 
* Modificado Por	: <\AM  AM\> 
* Descripción		: <\DM  DM\> 
* Nuevos Parámetros	: <\PM  PM\> 
* Nuevas Variables	: <\VM	VM\> 
* Fecha Modificación: <\FM  FM\>
*------------------------------------------------------------------------------------------- */


Alter Procedure gsa.spASObtenerDatosAdicionalesAnulacion
As
Begin

	Set Nocount On

	Declare @cnsctvo_cdgo_estdo_anldo_mga	udtConsecutivo,
			@fcha_actl						Datetime,
			@vlr_cro						Int,
			@orgn_mdfccn_slctd				udtDescripcion;

	-- 
	Create Table #tempInformacionConceptos (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo	udtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
		cnsctvo_mdcmnto_slctdo			udtConsecutivo,
		cnsctvo_cdgo_cncpto_gsto		udtConsecutivo,
		nmro_unco_ops					udtConsecutivo,
		cnsctvo_ops						udtConsecutivo
	)

	-- 
	Set @cnsctvo_cdgo_estdo_anldo_mga = 14;
	Set @fcha_actl = getDate();
	Set @vlr_cro = 0;
	Set	@orgn_mdfccn_slctd = 'Solicitud';
	
	-- 
	Update		a
	Set			cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cdgo_srvco_slctdo,
				cnsctvo_cdgo_estdo_mga_antrr = b.cnsctvo_cdgo_estdo_srvco_slctdo
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados b With(NoLock)
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

	-- Se obtiene la informacion de los conceptos
	Update		a
	Set			cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados b With(NoLock)
	On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

	-- 
	Update		a
	Set			cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados b With(NoLock)
	On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;
	
	-- 
	Insert Into #tempInformacionConceptos (
				cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_mdcmnto_slctdo,				cnsctvo_prcdmnto_insmo_slctdo,		
				cnsctvo_cdgo_cncpto_gsto,			nmro_unco_ops,						cnsctvo_slctd_atrzcn_srvco					
	)
	Select		b.cnsctvo_cncpto_prcdmnto_slctdo,	b.cnsctvo_mdcmnto_slctdo,			a.cnsctvo_prcdmnto_insmo_slctdo,	
				b.cnsctvo_cdgo_cncpto_gsto,			b.nmro_unco_ops,					a.cnsctvo_slctd_atrzcn_srvco
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado b With(NoLock)
	On			b.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
	
	-- 
	Insert Into #tempInformacionConceptos (
				cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_mdcmnto_slctdo,				cnsctvo_prcdmnto_insmo_slctdo,		
				cnsctvo_cdgo_cncpto_gsto,			nmro_unco_ops,						cnsctvo_slctd_atrzcn_srvco					
	)
	Select		b.cnsctvo_cncpto_prcdmnto_slctdo,	b.cnsctvo_cncpto_prcdmnto_slctdo,	a.cnsctvo_prcdmnto_insmo_slctdo,	
				b.cnsctvo_cdgo_cncpto_gsto,			b.nmro_unco_ops,					a.cnsctvo_slctd_atrzcn_srvco
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado b With(NoLock)
	On			b.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo;

	-- Se marcan las prestaciones con estado anulado
	Update		#tempInformacionServicios
	Set			cnsctvo_cdgo_estdo_mga = @cnsctvo_cdgo_estdo_anldo_mga;

	-- Se obtiene  el consecutivo de prestacion
	Update		a
	Set			cnsctvo_cdgo_estdo_sprs = b.cnsctvo_cdgo_estdo_hmlgdo_sprs
	From		#tempInformacionServicios a
	Inner Join	bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias b With(NoLock)
	On			b.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_mga
	Where		@fcha_actl Between b.inco_vgnca And b.fn_vgnca

	-- Se obtiene el consecutivo de la oficina y el nuam, si ya estan migradas.
	Update		a
	Set			cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna,
				nuam = b.nuam
	From		#tempInformacionServicios a
	Inner Join	bdCNA.gsa.tbASHomologacionServiciosMegaSipres b With(NoLock)
	On			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

	-- Se carga la informacion de las atenciones
	Insert Into #tempAtenciones (
			cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_ofcna,  nuam
	)
	Select  Distinct 
			cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_ofcna,	 nuam 
	From	#tempInformacionServicios

	-- Se carga la informacion para anulacion de cuotas de recuperacion.
	Insert Into #tmpSolicitudAnulacionCuotasRecuperacion (
				cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_srvco_slctdo
	)
	Select Distinct a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_cdgo_srvco_slctdo
	From		#tempInformacionServicios a	
		
	
	-- 
	Drop Table #tempInformacionConceptos;	

End
