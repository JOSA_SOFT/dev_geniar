USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[SpAsActualizarEstadoProgramacionEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[SpAsActualizarEstadoProgramacionEntrega]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[SpAsActualizarEstadoProgramacionEntrega] AS SELECT 1;'
END
GO

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : SpAsActualizarEstadoProgramacionEntrega
* Desarrollado por   : <\A Ing. Juan Carlos Vásquez G.													A\>    
* Descripcion        : <\D Procedimiento Para Actualizar Estados de Programación de Entrega
							1. Cuando se imprime o descarga pasa a estado '153-Entregado'
							2. Cuando se Anula la solicitud pasa a estado '152-Reprogramado				D\>
          				    
* Observaciones      : <\O   -- tabla temporal que se recibe si es masivo
								Create table #TmpProgramacionEntregaRecibida
								(
								 cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
								 cnsctvo_cdgo_det_prgrmcn_fcha_evnto	UdtConsecutivo,
								 usro_prcso								UdtUsuario,
								 cnsctvo_cdgo_estdo_srvco_slctdo		UdtConsecutivo -- (14-Anulada ó 15- Entregada)
								)																		O\>    
* Parametros         : <\P   @cdgo_rsltdo,
							 @mnsje_rsltdo																P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 2016/07/27  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
*					   <\D  \> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 																 FM\>    
*-----------------------------------------------------------------------------------------------------*/


ALTER PROCEDURE [gsa].[SpAsActualizarEstadoProgramacionEntrega] 

/*01*/  @cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo = null,
/*02*/  @cnsctvo_cdgo_det_prgrmcn_fcha_evnto	UdtConsecutivo = null,
/*03*/  @usro_prcso								UdtUsuario     = null,
/*04*/  @cnsctvo_cdgo_estdo_srvco_slctdo		UdtConsecutivo = null,  /* (14-Anulada ó 15- Entregada) */
/*05*/  @cdgo_rsltdo							int = null	output,
/*06*/  @mnsje_rsltdo							UdtDescripcion = null output 

AS

Begin
	Set NoCount On

	-- Declaramos las variables a utilizar
	Declare	@fecha_actual						datetime = getdate(),
			@cnsctvo_cdgo_estds_entrga152		UdtConsecutivo = 152, -- '152-Reprogramado'
			@cnsctvo_cdgo_estds_entrga153		UdtConsecutivo = 153, -- '153-Entregado'
			@cnsctvo_cdgo_estdo_srvco_slctdo14	UdtConsecutivo = 14, -- '14-Anulado'
			@cnsctvo_cdgo_estdo_srvco_slctdo15	UdtConsecutivo = 15, -- '15-Entregado'
			@cnsctvo_cdgo_estdo_cncpto8			UdtConsecutivo = 8,  -- '8-Impreso'
			@cnsctvo_cdgo_estdo_cncpto3			UdtConsecutivo = 3   -- '3-Anulado'

	DECLARE @codigoExito					char(2) = 0,
			@mensajeExito					char(30) = 'Ejecución Exitosa',
			@codigoError					char(2) = -1,
			@mensajeError					varchar(2000) = 'Se Presento Un Error al Actualizar Cambio de Estado Prog. Entrega'

	if @cnsctvo_slctd_atrzcn_srvco is not null
	   Begin
			-- es por demanda
			Create table #TmpProgramacionEntregaRecibida
			(
				 cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
				 cnsctvo_cdgo_det_prgrmcn_fcha_evnto	UdtConsecutivo,
				 usro_prcso								UdtUsuario,
				 cnsctvo_cdgo_estdo_srvco_slctdo		UdtConsecutivo		/* (14-Anulada ó 15- Entregada)*/
			)			

			Insert #TmpProgramacionEntregaRecibida
			(
				cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		usro_prcso,
				cnsctvo_cdgo_estdo_srvco_slctdo	
			)
			Values
			(
				@cnsctvo_slctd_atrzcn_srvco,		@cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		@usro_prcso,
				@cnsctvo_cdgo_estdo_srvco_slctdo
			) 

	   End


    -- Temporal para ejecución Proceso
	Create table #TmpProgramacionEntregaActualizar
	(
	 id_tbla								int identity,
	 cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
	 cnsctvo_cdgo_det_prgrmcn_fcha_evnto	UdtConsecutivo,
	 fcha_entrga							datetime,
	 usro_prcso								UdtUsuario,
	 cnsctvo_cdgo_estdo_srvco_slctdo		UdtConsecutivo
	)

    -- Paso 1. Recuperamos programación de entrega a actualizar
	Insert #TmpProgramacionEntregaActualizar
	(
			cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		usro_prcso,
			cnsctvo_cdgo_estdo_srvco_slctdo									
	)
	Select
			cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		usro_prcso,
			cnsctvo_cdgo_estdo_srvco_slctdo
    From	#TmpProgramacionEntregaRecibida

	-- Paso 2. Recuperamos la fecha de entrega desde la tabla resultado fecha entrega
	Update		t
	Set			fcha_entrga = iif(r.fcha_rl_entrga is null,r.fcha_estmda_entrga,r.fcha_rl_entrga)
	From		#TmpProgramacionEntregaActualizar t
	Inner Join	bdcna.gsa.tbASResultadoFechaEntrega r with(nolock)
	On			r.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco

	-- Paso 3. Actualiza el registro en el detalle de programacion cuando se entrego ó  anulo
	
	Update		b 
	Set  		cnsctvo_cdgo_estds_entrga  = iif(t.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo14,@cnsctvo_cdgo_estds_entrga152,@cnsctvo_cdgo_estds_entrga153), /*reprogramado ó entregado*/
				usro_mdfccn = t.usro_prcso,
				fcha_ultma_mdfccn = @fecha_actual
	from		bdsisalud.dbo.tbDetProgramacionFechaEvento b  with(nolock)
	Inner Join  #TmpProgramacionEntregaActualizar t
	On			t.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = b.cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	Where		(t.fcha_entrga = b.fcha_entrga or t.fcha_entrga = b.fcha_rl_entrga)
	And         t.cnsctvo_cdgo_estdo_srvco_slctdo in( @cnsctvo_cdgo_estdo_srvco_slctdo14,@cnsctvo_cdgo_estdo_srvco_slctdo15)

	-- Paso 4. Actualiza de cada concepto asociado detalle de programacion cuando se entrego ó anulo
	
	Update		c 
	Set  		cnsctvo_cdgo_estdo_cncpto  = iif(t.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo14, @cnsctvo_cdgo_estdo_cncpto3,@cnsctvo_cdgo_estdo_cncpto8), /* anulado ó impreso*/
				usro_mdfccn = t.usro_prcso,
				fcha_mdfccn = @fecha_actual
	from		bdsisalud.dbo.tbDetProgramacionFechaEvento b  with(nolock)
	Inner Join  bdsisalud.dbo.tbDetProgramacionProveedoresConceptos c with(nolock)
	On          c.cnsctvo_det_prgrmcn_fcha = b.cnsctvo_det_prgrmcn_fcha
	And			c.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn
	And         c.cnsctvo_prgrmcn_fcha_evnto = b.cnsctvo_prgrmcn_fcha_evnto
	Inner Join  #TmpProgramacionEntregaActualizar t
	On			t.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = b.cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	Where		(t.fcha_entrga = b.fcha_entrga or t.fcha_entrga = b.fcha_rl_entrga)
	And         t.cnsctvo_cdgo_estdo_srvco_slctdo in( @cnsctvo_cdgo_estdo_srvco_slctdo14,@cnsctvo_cdgo_estdo_srvco_slctdo15)

	set @cdgo_rsltdo = @codigoExito
	set @mnsje_rsltdo = @mensajeExito

	If	@@error	<> 0
		Begin
			set  @cdgo_rsltdo = @codigoError
			set  @mnsje_rsltdo = @mensajeError
		End

	Drop Table #TmpProgramacionEntregaActualizar
	--Drop Table #TmpProgramacionEntregaRecibida
End
GO
