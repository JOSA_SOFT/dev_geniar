USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarUsuarioOPrestacionComoRiesgo]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarUsuarioOPrestacionComoRiesgo
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite validar si el usuario se encuentra en uno de  D\>
					   <\D	los grupos de riesgo priorizados "O" la prestación se encuentra clasificada D\>
					   <\D	como riesgo D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarUsuarioOPrestacionComoRiesgo] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@dscrpcn_grpo_entrga udtDescripcion,
@agrpa_prstcn udtLogico
AS
  SET NOCOUNT ON
  DECLARE @fcha_actl datetime,
		 @afirmacion CHAR(1)
  SET @afirmacion = 'S'
  SET @fcha_actl = GETDATE()

  BEGIN

    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga IS NULL)
    BEGIN

      --Se actualiza al grupo en caso de que el afiliado se encuentren.
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN BdRiesgosSalud.dbo.tbReporteCohortesxAfiliado rca WITH (NOLOCK) --Cohorte por afiliado
        ON rca.nmro_unco_idntfccn_afldo = sol.nmro_idntfccn_afldo
      INNER JOIN BdRiesgosSalud.dbo.tbCohortes_Vigencias cohvig WITH (NOLOCK)
        ON cohvig.cnsctvo_cdgo_chrte = rca.cnsctvo_cdgo_chrte
      WHERE @fcha_actl BETWEEN cohvig.inco_vgnca AND cohvig.fn_vgnca
      AND sol.cnsctvo_grpo_entrga IS NULL;

      --Se actualiza al grupo en caso de que la prestación se encuentre
      UPDATE #tmpDatosSolicitudFechaEntrega
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @dscrpcn_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN BdRiesgosSalud.dbo.tbPrestacionesxCohortes pc WITH (NOLOCK)    --Cohorte por prestación
        ON pc.cnsctvo_cdfccn = sol.cnsctvo_cdfccn
      INNER JOIN BdRiesgosSalud.dbo.tbCohortes_Vigencias cohvig WITH (NOLOCK)
        ON cohvig.cnsctvo_cdgo_chrte = pc.cnsctvo_cdgo_chrte
      WHERE @fcha_actl BETWEEN cohvig.inco_vgnca AND cohvig.fn_vgnca
      AND sol.cnsctvo_grpo_entrga IS NULL;

	  --Se determina si se debe realizar agrupación
	  IF EXISTS(SELECT
				sol.id
			  FROM #tmpDatosSolicitudFechaEntrega sol
			  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
					AND @agrpa_prstcn = @afirmacion)
		BEGIN
			UPDATE #tmpDatosSolicitudFechaEntrega
		    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
			    cdgo_grpo_entrga = @dscrpcn_grpo_entrga
		    FROM #tmpDatosSolicitudFechaEntrega sol
				 WHERE sol.cnsctvo_slctd_srvco_sld_rcbda IN (SELECT
												sol.cnsctvo_slctd_srvco_sld_rcbda 
											  FROM #tmpDatosSolicitudFechaEntrega sol
											  WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga)

		END

    END
  END


GO
