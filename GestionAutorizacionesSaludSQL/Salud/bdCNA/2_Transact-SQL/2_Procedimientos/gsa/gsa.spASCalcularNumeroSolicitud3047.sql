USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASCalcularNumeroSolicitud3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASCalcularNumeroSolicitud3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASCalcularNumeroSolicitud3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Se encarga de calcular el numero de solicitud de las solictudes. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2016/06/22 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASCalcularNumeroSolicitud3047 ...
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASCalcularNumeroSolicitud3047
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @contadorCicloPrestaciones int,
			@max int,
			@cero varchar(1),
			@guion varchar(1),
			@ocho int,
			@uno int

	SET @contadorCicloPrestaciones = 1
	SET @cero = '0'
	SET @ocho = 8
	SET @guion = '-'
	SET @uno = 1

    INSERT INTO	#solicitudes_guardadas(cnsctvo_slctd_atrzcn_srvco, identificador_Sol)
	SELECT IDS.cnsctvo_slctd_atrzcn_srvco ,IDS.idXML
	FROM #Idsolicitudes	IDS WITH (NOLOCK)
	INNER JOIN	#Tempo_Solicitudes	SOL	WITH (NOLOCK)
	ON	IDS.idXML = SOL.id	

	SELECT @max = Max(id)
	FROM #solicitudes_guardadas 

	WHILE	@contadorCicloPrestaciones <= @max
	BEGIN
		-- ACTUALIZAR SECUENCIA EN TABLA DE SOLICITUDES
		UPDATE	gsa.tbASSolicitudesAutorizacionServicios
		SET	nmro_slctd_atrzcn_ss =concat(convert(varchar(4),year(SOL.fcha_slctd)),@guion,LTRIM(RTRIM(SOL.cdgo_mdo_cntcto_slctd)),@guion,replicate(@cero ,@ocho-len(convert(varchar(100),SEC.vlr_scnca))),convert(varchar(100),SEC.vlr_scnca))
		FROM gsa.tbASSolicitudesAutorizacionServiciosSecuencia SEC WITH (NOLOCK)
		INNER JOIN	#Tempo_Solicitudes SOL	ON	SEC.prdo_scnca = SOL.anio_slctd
		INNER JOIN	#solicitudes_guardadas SGU ON SGU.identificador_sol = SOL.id
		INNER JOIN	gsa.tbASSolicitudesAutorizacionServicios SOP WITH (NOLOCK) ON SOP.cnsctvo_slctd_atrzcn_srvco = SGU.cnsctvo_slctd_atrzcn_srvco
		WHERE SGU.id = @contadorCicloPrestaciones

		-- ACTUALIZAR SECUENCIA EN TABLA DE SECUENCIAS
		UPDATE gsa.tbASSolicitudesAutorizacionServiciosSecuencia
		SET vlr_scnca = concat(replicate(@cero ,@ocho-len(convert(varchar(100),vlr_scnca + @uno))) , convert(varchar(100),vlr_scnca + @uno))
		FROM #Tempo_Solicitudes SOL
		INNER JOIN  #IdSolicitudes IDS ON IDS.idXML = SOL.id		
		INNER JOIN	#solicitudes_guardadas SGU ON SGU.cnsctvo_slctd_atrzcn_srvco = IDS.cnsctvo_slctd_atrzcn_srvco
		WHERE SGU.id = @contadorCicloPrestaciones
		AND prdo_scnca = SOL.anio_slctd
				
		SET @contadorCicloPrestaciones = @contadorCicloPrestaciones + @uno
	END

END
GO
