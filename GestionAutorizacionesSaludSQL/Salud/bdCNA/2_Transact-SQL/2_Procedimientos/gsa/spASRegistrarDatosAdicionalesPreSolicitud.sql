USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRegistrarDatosAdicionalesPreSolicitud]    Script Date: 8/4/2017 9:21:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASRegistrarDatosAdicionalesPreSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS A\>
* Descripcion			: <\D 
                              Procedimiento que se utiliza para guardar el resto de datos de la presolicitud 
							  para su gestion desde el BPM	
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/07/01 FC\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se modifica el procedimiento con el fin de registrar las
								presolicitudes de la sede de bogota
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	01/06/2017 FM\>
*--------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*--------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se modifica el procedimiento con el fin de registrar las
								presolicitudes de la sede de bogota, buenaventura y antioquia
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	04/08/2017 FM\>
*--------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASRegistrarDatosAdicionalesPreSolicitud]  
	@cnsctvo_prslctd	udtConsecutivo,
	@usro_gstn			udtUsuario,
	@cdgo_rsltdo		udtConsecutivo  output,
	@mnsje_rsltdo		varchar(2000)	output
AS
BEGIN
	SET NOCOUNT ON;

	Declare @codigo_ok				  Int           ,
			@codigo_error             Int           ,
			@mensaje_ok				  udtDescripcion,
			@mensaje_error			  Varchar(2000) ,
			@ldFechaActual   		  Datetime      ,
			@cnsctvo_cdgo_estdo_rdccn udtConsecutivo,
			@accn_evnto               Int           ,
			@nmbre_evnto			  udtDescripcion,
			@cnsctvo_cdgo_sde_ips     udtConsecutivo,
			@cnsctvo_cdgo_sde_bgta    udtConsecutivo, -- SEDE BOGOTA
			@cnsctvo_cdgo_sde_bnvntra udtConsecutivo, -- SEDE BUENAVENTURA
			@cnsctvo_cdgo_sde_antqa   udtConsecutivo, -- SEDE ANTIOQUIA
			@cdgo_ips_prmra           udtCodigoIPS  ;

    Create
	Table  #tempSedes(id               udtConsecutivo Identity(1,1), 
	                  cnsctvo_cdgo_sde udtConsecutivo
				     )

	Set @codigo_error             = -1;  -- Error SP
	Set @codigo_ok	              =  0;  -- Codigo de mensaje Ok
	Set @mensaje_ok		          = 'Proceso ejecutado satisfactoriamente';
	Set @ldFechaActual            = getDate();
	Set @cnsctvo_cdgo_estdo_rdccn = 1; --Estado Radicado
	Set @accn_evnto	              = 1;
	Set @nmbre_evnto              = 'dbo.tbPreSolicitudes';
	Set @cnsctvo_cdgo_sde_bgta    = 11 -- SEDE BOGOTA
	Set @cnsctvo_cdgo_sde_bnvntra = 9  -- SEDE BUENAVENTURA
	Set @cnsctvo_cdgo_sde_antqa   = 17 -- SEDE ANTIOQUIA

    Insert Into #tempSedes(cnsctvo_cdgo_sde) Values (@cnsctvo_cdgo_sde_bgta)
	Insert Into #tempSedes(cnsctvo_cdgo_sde) Values (@cnsctvo_cdgo_sde_bnvntra)
	Insert Into #tempSedes(cnsctvo_cdgo_sde) Values (@cnsctvo_cdgo_sde_antqa)
		
	BEGIN TRY
	    
		Select     @cdgo_ips_prmra = b.cdgo_intrno			   	           
		From       tbPreSolicitudes pre		
		Inner Join BDAfiliacionValidador.dbo.tbBeneficiariosValidador b	With(NoLock)
		On		   b.nmro_unco_idntfccn_afldo = pre.nmro_unco_idntfccn_afldo
		Inner Join BDAfiliacionValidador.dbo.tbContratosValidador c	With(NoLock)
		On	       c.nmro_cntrto             = b.nmro_cntrto             And 
				   c.cnsctvo_cdgo_tpo_cntrto = b.cnsctvo_cdgo_tpo_cntrto And 
				   c.cnsctvo_cdgo_pln        = pre.cnsctvo_cdgo_pln
		Where      pre.cnsctvo_prslctd = @cnsctvo_prslctd 
		And        @ldFechaActual Between b.inco_vgnca_bnfcro And b.fn_vgnca_bnfcro
		And        @ldFechaActual Between c.inco_vgnca_cntrto And c.fn_vgnca_cntrto

		Select  @cnsctvo_cdgo_sde_ips = i.cnsctvo_cdgo_sde_ips 
	    From    bdAfiliacionValidador.dbo.tbIpsPrimarias_vigencias i With(NoLock)	
	    Where   cdgo_intrno = @cdgo_ips_prmra
		And     @ldFechaActual Between i.inco_vgnca and i.fn_vgnca
		
	    If Exists(Select Top 1 cnsctvo_cdgo_sde From #tempSedes Where cnsctvo_cdgo_sde = @cnsctvo_cdgo_sde_ips)
	      Begin
	        Insert 
	        Into   BDCna.gsa.tbASDatosAdicionalesPreSolicitud(cnsctvo_prslctd, cnsctvo_cdgo_estdo_srvco_slctdo, fcha_crcn        ,
                                                              usro_crcn      , fcha_ultma_mdfccn              , usro_ultma_mdfccn
                                                             )
            Values (@cnsctvo_prslctd, @cnsctvo_cdgo_estdo_rdccn, @ldFechaActual,
                    @usro_gstn      , @ldFechaActual           , @usro_gstn
                   );
            
            Insert 
            Into   BDCna.gsa.tbAsIntegracionEventos(cnsctvo_llve_objto, cnsctvo_cdgo_estdo_evnto, cnsctvo_cdgo_accn, 
                                                    nmbre_objto       , fcha_crcn               , usro_crcn
                                                   ) 
            Values (@cnsctvo_prslctd, @cnsctvo_cdgo_estdo_rdccn, @accn_evnto,
                    @nmbre_evnto    , @ldFechaActual           , @usro_gstn
                   );
	      End;
		
	      Set @cdgo_rsltdo  = @codigo_ok
	      Set @mnsje_rsltdo = @mensaje_ok
		
	END TRY
	BEGIN CATCH
		Set @mensaje_error = Concat(
								'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
								'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
								'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
								'Procedure:' , ERROR_PROCEDURE()
							);
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

	Drop Table #tempSedes
END