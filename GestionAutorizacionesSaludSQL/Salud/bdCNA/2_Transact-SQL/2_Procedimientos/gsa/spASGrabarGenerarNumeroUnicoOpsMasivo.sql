USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGenerarNumeroUnicoOpsMasivo]    Script Date: 18/11/2016 4:19:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------													
* Metodo o PRG 		: gsa.spASGrabarGenerarNumeroUnicoOpsMasivo												
* Desarrollado por	: <\A Ing. Juan Carlos V�squez Garc�a															A\>  
* Descripcion		: <\D Generar El N�mero Unico de OPS Modelo Mega												D\>  												
* Observaciones		: <\O Se recibe como par�metro una tabla temporal para ejecutar el c�lculo						O\>  													
* Parametros		: <\P	#tbConceptosOps																			P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/21																				FC\>  														
*  															
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------															
* DATOS DE MODIFICACION  															
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------														
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM agrpdr_imprsn																					VM\>  													
* Fecha Modificacion	: <\FM 2016/11/18																						FM\>  													
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
create PROCEDURE [gsa].[spASGrabarGenerarNumeroUnicoOpsMasivo]

/*01*/ @lcUsrioAplcn		UdtUsuario = null

AS

BEGIN
	SET NOCOUNT ON

	Declare		@fechaactual	Datetime = getdate(),
				@ValorCERO		int = 0,
				@lcUsuario		UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

	If @lcUsrioAplcn is not null
		Begin
			Set @lcUsuario = @lcUsrioAplcn
		End
		--Actualizamos tabla fisica con el Numero Unico OPS generado  por grupo de liquidaci�n 
	Update		t1
	Set			nmro_unco_ops = t2.nmro_unco_ops,
				nmro_unco_prncpl_cta_rcprcn = t2.nmro_unco_prncpl_cta_rcprcn,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_ultma_mdfccn = @fechaactual,
				ds_vldz = t2.ds_vldz
	From		gsa.tbASConceptosServicioSolicitado t1 
	Inner Join	#tbConceptosOps t2
	On			t2.cnsctvo_prstcn_slctda = t1.cnsctvo_prstcn
	And         t2.grpo_imprsn = t1.cnsctvo_cdgo_grpo_imprsn
	And			t2.cnsctvo_cncpto_prcdmnto_slctdo = t1.cnsctvo_cncpto_prcdmnto_slctdo
--	Where		t2.nmro_unco_ops != @valorCERO
	
    -- Actualizamos el n�mero �nico ops en el modelo programaci�n de entrega
	Update		dpfe
	Set			cnsctvo_ops = t.nmro_unco_ops,
				usro_mdfccn = @lcUsuario,
				fcha_ultma_mdfccn = @fechaactual
	From        #tbConceptosOps t
	Inner Join	gsa.tbASServiciosSolicitados ss with(nolock)
	On			t.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco			
	And			t.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdSiSalud.dbo.tbDetProgramacionFechaEvento dpfe with(nolock)
	On			ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = dpfe.cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	Where		t.nmro_unco_ops != @valorCERO


END