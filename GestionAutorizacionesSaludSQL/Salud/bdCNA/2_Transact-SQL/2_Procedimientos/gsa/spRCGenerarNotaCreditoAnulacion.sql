USE [BDCna]
GO
/****** Object:  StoredProcedure [cja].[spRCGenerarNotaCreditoAnulacion]    Script Date: 10/3/2017 9:28:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG     : cja.spRCGenerarNotaCreditoAnulacion
* Desarrollado por : <\A Jorge Andres Garcia Erazo <jorge.garcia@geniar.net> A\>
* Descripcion      : <\D Realiza las notas creditos de la OPS anuladas. D\>    
* Observaciones    : <\O 
                         Para el correcto funcionamiento del SP es necesaria la tabla #anular
					     que contiene las solicitudes que seran anuladas.		 
				     O\>    
* Parametros       : <\P P\>
* Variables        : <\V V\>
* Fecha Creacion   : <\FC 2016/11/06 FC\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta llamado a sp spRCGuardarDocumentoCaja ya que se le agrego nuevo parametro DM/>
* Nuevos Parametros  : <\PM @insrta_dtlle PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta proceso para que no tenga en cuenta las prestaciones que ya se encuentra anuladas, 
							se retira validacion de ops que ya tengan notas credito generadas. DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/26 FM\>   
*-----------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta validacion para que sea un delete de la tabla temporal y no un IF DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/05/09 FM\>   
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp que no tome las cuotas de recuperacion anuladas
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing.Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\DM 
                            Se ajusta sp para que funcione con nuevo proceso de anulacion
						DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2016/04/21 FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------------
 Ejemplo: 
    <\EJ
        Create  Table  #anular(	
			nmro_unco_ops	                udtConsecutivo,							  
			cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
			cnsctvo_mdcmnto_slctdo			udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo,
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo			  
			) 

		insert into #anular (nmro_unco_ops, cnsctvo_slctd_atrzcn_srvco) values (92587782, 2344)

		EXEC cja.spRCGenerarNotaCreditoAnulacion 'geniar'

		select * from cja.tbRCDocumentosCaja

		drop table #anular
    EJ\>
*-----------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [cja].[spRCGenerarNotaCreditoAnulacion]
   @usr udtUsuario
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @cnsctvo_cdgo_cncpto_gnrcn		udtConsecutivo,
			@cnsctvo_cdgo_tpo_dcmnto_cja	udtConsecutivo,
			@cnsctvo_cdgo_estdo_dcmnto_cja	udtConsecutivo,
			@obsrvcn						udtObservacion,
			@contador						Int           ,  
			@tamano							Int           ,			
			@nmro_unco_idntfccn_afldo		udtConsecutivo,			
			@vlr_rcbo						Numeric(18,0) ,
			@vldo_s							udtLogico     ,
			@vldo_n							udtLogico     ,
			@tpo_dcmnto_rcbo				udtConsecutivo,
			@numeroRecibo					udtConsecutivo,
			@llave							udtConsecutivo,
			@fechaActual					Datetime      ,
			@cnsctvo_dcmnto_cja				udtConsecutivo,
			@nmro_unco_ops					udtConsecutivo,
			@no								Char(2)       ,
			@estado_anulado					udtConsecutivo,
			@valor_cero						Int           ,
			@estdo_cta_rcprcn_anldo         udtConsecutivo

	Create 
	Table  #opsAnular(id                         udtConsecutivo Identity,
		              nmro_unco_ops              udtConsecutivo, 
		              nmro_unco_idntfccn_afldo   udtConsecutivo,
		              cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
		              cnsctvo_srvco_slctdo       udtConsecutivo,
		              vlr_rcbo                   Numeric(18,0) ,
		              cnsctvo_dcmnto_cja         udtConsecutivo,
		              vldo                       udtLogico Default 'N'
	                 )

	Create 
	Table  #notaCreditoGenerada(id                 udtConsecutivo Identity,
		                        nui                udtConsecutivo,
		                        cnsctvo_dcmnto_cja udtConsecutivo,
		                        vlr_rcbo           Numeric(18,0),
		                        nmro_unco_ops      udtConsecutivo
	                           )

	Set @cnsctvo_cdgo_cncpto_gnrcn     = 1
	Set @cnsctvo_cdgo_tpo_dcmnto_cja   = 2
	Set @cnsctvo_cdgo_estdo_dcmnto_cja = 2
	Set @tpo_dcmnto_rcbo               = 1
	Set @obsrvcn                       = 'Nota credito generada por anulacion'
	Set @contador                      = 1
	Set @vldo_s	                       = 'S'
	Set @vldo_n	                       = 'N'
	Set @fechaActual                   = getdate()
	Set @no                            = 'NO'
	Set @estado_anulado                = 14
	Set @valor_cero                    = 0
	Set @estdo_cta_rcprcn_anldo        = 4 --Cuota de recuperacion Anulado

	Insert 
	Into     #opsAnular(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo)
	Select   Distinct cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo
	FROM     #tempInformacionServicios
	where    cnsctvo_cdgo_estdo_mga_antrr != @estado_anulado

	--	
	Update     a
	Set        nmro_unco_ops = b.nmro_unco_ops
	From       #opsAnular a 
	Inner Join bdCNA.gsa.TbAsDetCalculoCuotaRecuperacion b With(Nolock) 
	On         a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco And 
	           a.cnsctvo_srvco_slctdo       = b.cnsctvo_srvco_slctdo       
	Where      b.nmro_unco_ops                 != @valor_cero
	And        b.cnsctvo_cdgo_estdo_cta_rcprcn != @estdo_cta_rcprcn_anldo

	Update     a
	Set        nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	From       #opsAnular a
	Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b With(Nolock) 
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco

	Update     a
	Set        vldo               = @vldo_s                  ,
		       cnsctvo_dcmnto_cja = c.cnsctvo_cdgo_dcmnto_cja
	From       #opsAnular a 
	Inner Join bdCNA.cja.tbRCSoportesDocumentosCaja b With(Nolock) 
	On         b.nmro_unco_ops = a.nmro_unco_ops 
	Inner Join bdCNA.cja.tbRCDocumentosCaja c With(Nolock) 
	On         c.cnsctvo_cdgo_dcmnto_cja  = b.cnsctvo_dcmnto_cja      And 
	           c.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn_afldo
	Where      c.cnsctvo_cdgo_tpo_dcmnto_cja = @tpo_dcmnto_rcbo

	Update     a
	Set        vlr_rcbo = cro.vlr_lqdcn_cta_rcprcn_srvco_ops
	From       bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps cro With(Nolock) 
	Inner Join #opsAnular a 
	On         a.nmro_unco_ops              = cro.nmro_unco_ops And 
	           a.cnsctvo_slctd_atrzcn_srvco = cro.cnsctvo_slctd_atrzcn_srvco

	Delete 
	From    #opsAnular 
	Where   vldo          = @vldo_n
	Or      nmro_unco_ops Is Null
	Or      vlr_rcbo      Is Null

	Delete     a
	From       #opsAnular a
	Inner Join bdCNA.cja.tbRCSoportesDocumentosCaja b With(Nolock)
	On         b.nmro_unco_ops = a.nmro_unco_ops
	Inner Join bdCNA.cja.tbRCDocumentosCaja c With(Nolock)
	On         c.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja
	Where      c.cnsctvo_cdgo_tpo_dcmnto_cja = @cnsctvo_cdgo_tpo_dcmnto_cja
	And        Ltrim(Rtrim(c.obsrvcn)) = @obsrvcn

	Insert 
	Into   #notaCreditoGenerada(nui     , cnsctvo_dcmnto_cja, 
	                            vlr_rcbo, nmro_unco_ops
							   )
	Select Distinct nmro_unco_idntfccn_afldo, cnsctvo_dcmnto_cja, 
	                vlr_rcbo                , nmro_unco_ops
	From   #opsAnular
	
	Select @tamano = max(id)From #notaCreditoGenerada

	While(@contador <= @tamano)
		Begin
			Select @nmro_unco_idntfccn_afldo = nui               ,
		 		   @vlr_rcbo                 = vlr_rcbo          ,
				   @cnsctvo_dcmnto_cja       = cnsctvo_dcmnto_cja,
				   @nmro_unco_ops            = nmro_unco_ops
			From   #notaCreditoGenerada
			Where  id = @contador

			Exec cja.spRCGuardarDocumentoCaja @nmro_unco_idntfccn_afldo, 
											  @cnsctvo_cdgo_cncpto_gnrcn,
											  @vlr_rcbo, 
											  @cnsctvo_cdgo_tpo_dcmnto_cja, 
											  @cnsctvo_cdgo_estdo_dcmnto_cja,
											  null, 
											  null,
											  @obsrvcn, 
											  @usr,
											  null,
											  @llave OUTPUT,
											  @numeroRecibo OUTPUT,
											  @no
			Insert 
			Into   bdCNA.cja.tbRCSoportesDocumentosCaja(cnsctvo_dcmnto_cja, nmro_unco_ops    , fcha_crcn        ,
			                                            usro_crcn         , fcha_ultma_mdfccn, usro_ultma_mdfccn
													   )
			Select @llave, @nmro_unco_ops, @fechaActual, @usr, @fechaActual, @usr

			Set @contador = @contador + 1
		End

	Drop Table #opsAnular
	Drop Table #notaCreditoGenerada
END
