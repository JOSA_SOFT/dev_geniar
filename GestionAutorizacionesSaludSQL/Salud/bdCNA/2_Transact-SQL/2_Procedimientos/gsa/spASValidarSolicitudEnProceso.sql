USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarSolicitudEnProceso]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarSolicitudEnProceso
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite validar si una solicitud se encuentra en proceso D\>  
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 03/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASValidarSolicitudEnProceso] 
@tpo_prcso udtCodigo,
@en_prcso udtLogico OUTPUT
AS
  SET NOCOUNT ON

  DECLARE @afirmacion char(1),
          @negacion char(1),
          @inexistente char(1),
          @cantidad numeric(3),
          @fecha datetime,
          @creacion char(1);

  SELECT
    @afirmacion = 'S',
    @negacion = 'N',
    @inexistente = 'I',
    @cantidad = 0,
    @fecha = GETDATE(),
	@en_prcso = 'N',
    @creacion = 'C';

  BEGIN

  --Se valida que efectivamente exista el consecutivo de solicitud.
  IF NOT EXISTS (SELECT
      so.id
    FROM #tmpNmroSlctds so
    WHERE so.cnsctvo_slctd_srvco_sld_rcbda IS NULL)
		BEGIN

		  --Se marcan los registros que actualmente se encuentren en proceso
		  SELECT 
		  @en_prcso = @afirmacion
		  FROM #tmpNmroSlctds ns
		  INNER JOIN bdCNA.gsa.tbASControlProcesoSolicitudesAutorizacionServicios ss WITH (NOLOCK)
			ON (ns.cnsctvo_slctd_srvco_sld_rcbda = ss.cnsctvo_slctd_atrzcn_srvco
			AND ss.tpo_prcso = @tpo_prcso)
		  WHERE ss.en_prcso = @afirmacion
		  AND ss.vsble_usro = @afirmacion;

	END
  END

GO
