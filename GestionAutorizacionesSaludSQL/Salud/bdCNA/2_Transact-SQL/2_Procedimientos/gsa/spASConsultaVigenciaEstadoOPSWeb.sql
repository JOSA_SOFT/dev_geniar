USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaVigenciaEstadoOPSWeb]    Script Date: 12/07/2017 08:09:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaVigenciaEstadoOPSWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información de la vigencia y el estado 
						   de la OPS cuando esté en estado entregada
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           nmro_unco_ops  	udtConsecutivo				         
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 07/06/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jorge Rodriguez AM\>    
* Descripcion        : <\D  
                            Se adiciona ejecución de SP para que devuelva la fecha de vencimiento mas el 
							tiempo de Holgura permitido.												
						D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 03-10-2016 FM\>    
*--------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se agrega validacion para verificar si existen prestaciones relacionadas 
                           por algun concepto y llamado a sp de que carga la informacion de las OPS de 
						   las prestaciones relacionadas D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 12-07-2017 FM\>    
*--------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASConsultaVigenciaEstadoOPSWeb] 129573, 69052
 
 ALTER PROCEDURE [gsa].[spASConsultaVigenciaEstadoOPSWeb] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
   @cnsctvo_srvco_slctdo       udtConsecutivo
AS

Begin
   SET NOCOUNT ON

  Declare  @cnsctvo_cdgo_estdo_atrzda  udtConsecutivo,
           @cnsctvo_cdgo_estdo_entrgda udtConsecutivo,
           @ldFechaActual              Datetime,
		   @vlr_prmtro_fcha            Datetime,
		   @fcha_utlzcn_hsta_hlgra     Datetime,
           @cdgo_prmtro_gnrl           Char(3) ,
		   @vsble_usro                 Char(1) ,
		   @cdgo_prmtro_gnrl_vgnca     Char(3) ,
		   @tpo_dto_prmtro             Char(1) ,
		   @vlr_prmtro_nmrco           Numeric(18,0),
		   @vlr_prmtro_crctr           Varchar(200),
		   @prstcn_ascda			   Int,
		   @vlr_uno					   Int,
		   @vlr_cro					   Int;
		 

   Create
   Table  #tempInfoOPS(nmro_unco_ops	                      udtConsecutivo,							  
                       fcha_utlzcn_dsde                       Datetime      ,
                       fcha_utlzcn_hsta                       Datetime      ,                             
                       cnsctvo_cdgo_estdo_cncpto_srvco_slctdo udtConsecutivo,                    
					   dscrpcn_estdo_srvco_slctdo             udtDescripcion,
					   fcha_utlzcn_hsta_hlgra                 Datetime       
					  );  

   Set @ldFechaActual              = getDate();
   Set @cnsctvo_cdgo_estdo_atrzda  = 11; --Autorizada
   Set @cnsctvo_cdgo_estdo_entrgda = 15; --Entregada
   Set @cdgo_prmtro_gnrl           = '103';
   Set @vsble_usro                 = 'S';
   Set @cdgo_prmtro_gnrl_vgnca     = '103';
   Set @tpo_dto_prmtro             = 'N';
   Set @prstcn_ascda			   = 0;
   Set @vlr_uno					   = 1;
   Set @vlr_cro					   = 0;


   ----Se inserta en la tabla temporal la informacion de los conceptos de gasto agrupados por grupos de impresion consultados por solicitud
   Insert 
   Into       #tempInfoOPS(fcha_utlzcn_dsde          , fcha_utlzcn_hsta, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                           dscrpcn_estdo_srvco_slctdo, nmro_unco_ops  		    										 
                          )
   Select     s.fcha_utlzcn_dsde          , s.fcha_utlzcn_hsta, s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
              e.dscrpcn_estdo_srvco_slctdo, s.nmro_unco_ops  
   From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
   Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
   On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo    
   Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
   On         s.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
   Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
   On         e.cnsctvo_cdgo_estdo_srvco_slctdo = s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
   Where      a.cnsctvo_slctd_atrzcn_srvco              = @cnsctvo_slctd_atrzcn_srvco  
   And        a.cnsctvo_srvco_slctdo                    = @cnsctvo_srvco_slctdo
   And        (s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzda OR 
               s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_entrgda
			  )
   And        @ldFechaActual Between e.inco_vgnca And e.fn_vgnca
   And        s.nmro_unco_ops > @vlr_cro
   Group By   s.fcha_utlzcn_dsde          , s.fcha_utlzcn_hsta, s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
              e.dscrpcn_estdo_srvco_slctdo, s.nmro_unco_ops; 
   
   ----Se inserta en la tabla temporal la informacion de los conceptos de gasto agrupados por grupos de impresion consultados por solicitud
   Insert 
   Into       #tempInfoOPS(fcha_utlzcn_dsde          , fcha_utlzcn_hsta, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
                           dscrpcn_estdo_srvco_slctdo, nmro_unco_ops 								 
                          )
   Select     s.fcha_utlzcn_dsde          , s.fcha_utlzcn_hsta, s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
              e.dscrpcn_estdo_srvco_slctdo, s.nmro_unco_ops 
   From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
   Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
   On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo    
   Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
   On         s.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
   Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
   On         e.cnsctvo_cdgo_estdo_srvco_slctdo = s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
   Where      a.cnsctvo_slctd_atrzcn_srvco              = @cnsctvo_slctd_atrzcn_srvco  
   And        a.cnsctvo_srvco_slctdo                    = @cnsctvo_srvco_slctdo
   And        (s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzda OR 
               s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_entrgda
			  )
   And        @ldFechaActual Between e.inco_vgnca And e.fn_vgnca
   And        s.nmro_unco_ops > @vlr_cro
   Group By   s.fcha_utlzcn_dsde          , s.fcha_utlzcn_hsta, s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
              e.dscrpcn_estdo_srvco_slctdo, s.nmro_unco_ops;

   Select	  @prstcn_ascda = @vlr_uno 
   From		  #tempInfoOPS i
   Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
   On		  css.nmro_unco_ops = i.nmro_unco_ops
   Inner Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
   On		  pis.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo
   Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
   On		  ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
   Where	  ss.cnsctvo_srvco_slctdo != @cnsctvo_srvco_slctdo
   And		  ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

   Select	  @prstcn_ascda = @vlr_uno	
   From		  #tempInfoOPS i
   Inner Join bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
   On		  css.nmro_unco_ops = i.nmro_unco_ops
   Inner Join bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
   On		  ms.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
   Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
   On		  ss.cnsctvo_srvco_slctdo = ms.cnsctvo_srvco_slctdo
   Where	  ss.cnsctvo_srvco_slctdo != @cnsctvo_srvco_slctdo
   And		  ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

   If(@prstcn_ascda = @vlr_uno)
	  Begin
		Exec bdCNA.gsa.spASObtenerPrestacionesAsociadasOPSWeb @cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo
	  End


   --Se adiciona ejecución de SP para que devuelva la fecha de vencimiento mas el tiempo de Holgura permitido.
   Exec bdSiSalud.dbo.spTraerParametroGeneral @cdgo_prmtro_gnrl, 
											  @vsble_usro, 
											  @cdgo_prmtro_gnrl_vgnca, 
											  @vsble_usro, 
											  @tpo_dto_prmtro, 
											  @vlr_prmtro_nmrco Output, 
											  @vlr_prmtro_crctr Output, 
											  @vlr_prmtro_fcha Output;
  
  
   Select Top 1 @fcha_utlzcn_hsta_hlgra = fcha_utlzcn_hsta From #tempInfoOPS 
   SET @fcha_utlzcn_hsta_hlgra = DATEADD(day, @vlr_prmtro_nmrco, @fcha_utlzcn_hsta_hlgra);

   Update  #tempInfoOPS
   Set     fcha_utlzcn_hsta_hlgra = @fcha_utlzcn_hsta_hlgra


   Select  fcha_utlzcn_dsde			, fcha_utlzcn_hsta, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, 
           dscrpcn_estdo_srvco_slctdo, nmro_unco_ops   , fcha_utlzcn_hsta_hlgra             
   From    #tempInfoOPS
   
   Drop Table #tempInfoOPS
   
End
