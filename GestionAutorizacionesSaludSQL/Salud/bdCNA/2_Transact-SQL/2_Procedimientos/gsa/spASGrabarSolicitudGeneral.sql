USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarSolicitudGeneral]    Script Date: 23/05/2017 05:56:23 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarSolicitudGeneral
* Desarrollado por   : <\A Jois Lombana Sánchez    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite crear tablas temporales que se manejarán en
					 : el servicio de Grabar solicitud además de llamar todos los procedimientos para dicha
					 : función.   D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 02/12/2015  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jonathan AM\>    
* Descripcion        : <\D Se adiciona el parámetro de entrada @infrmcn_vldcion_espcial y se crea la 
						   tabla temporal #Tempo_ValidacionEspecial D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 01/03/2016 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                           Se adiciona tabla temporal para consultar las solicitudes creadas a partir de una presolicitud, con el fin
						   de poder gestionarlas y cerralas automaticamente
					    D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 03/02/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D 
                           Se modifica el campo nmro_slctd_atrzcn_ss tomando como fcha_slctd la feha del sistema y no la fecha de 
						   solicitud
					    D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 27/02/2017 FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------ 
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento cambiando el tamaño del campo rgstro_mdco_trtnte en
								la tabla temporal, ya que se realiza cambio del tamaño en las tablas
								transaccionales
                            DM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/04/26 FM\>
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez AM\>
* Descripcion			 : <\DM	
                                Se agrega update para actualizar el recobro en las prestaciones, 
								control de cambio 068
                            DM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/05/23 FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGrabarSolicitudGeneral] 
	@infrmcn_slctd xml,
	@infrmcn_prstcn xml,
	@infrmcn_prstdr xml,
	@infrmcn_mdco xml,
	@infrmcn_dgnstco xml,
	@infrmcn_hsptlra xml,
	@infrmcn_afldo xml,
	@infrmcn_anxs xml,
	@infrmcn_vldcion_espcial xml,
	@estdo_ejccn varchar(5) OUTPUT,
	@msje_errr varchar(2000) OUTPUT
AS    

BEGIN

	SET NOCOUNT ON

	DECLARE @codigoExito                   Char(2),
			@mensajeExito                  Char(30),
			@codigoError                   Char(5),
			@Mensaje                       Varchar(max),
			@i                             Int,
			@Max                           Int,
			@xstate                        Int,
			@trancount                     Int,
			@cnsctvo_cdgo_mdo_cntcto_slctd udtConsecutivo,
			@cnsctvo_cdgo_estdo_prslctd    udtConsecutivo,
			@cant                          Int           ,
			@fcha_actl                     Date
	
	Set @codigoExito                   = 'OK'
	Set	@codigoError                   = 'ET'
	Set	@mensajeExito                  = 'Ejecución Exitosa'
	Set @cnsctvo_cdgo_mdo_cntcto_slctd = 4
	Set @cnsctvo_cdgo_estdo_prslctd    = 16

	Set @fcha_actl = GETDATE()

	/*::::::::::::CREACIÓN DE TABLAS TEMPORALES QUE GUARDARÁN LOS CONSECUTIVOS GENERADOS POR CADA INSERCIÓN:::::::::::::: 

	:::::::::::::ÉSTO CON EL FIN DE EVITAR LA CONSULTA A TABLAS REALES QUE PUEDAN DEMORAR EL PROCESAMIENTO:::::::::::::::*/	 
	CREATE TABLE #Idsolicitudes		(cnsctvo_slctd_atrzcn_srvco					udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdAfiliados		(cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)			
	CREATE TABLE #IdIPS				(cnsctvo_ips_slctd_atrzcn_srvco				udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdHospitalaria	(cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)					
	CREATE TABLE #IdMedico			(cnsctvo_mdco_trtnte_slctd_atrzcn_srvco		udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdDiagnostico		(cnsctvo_dgnstco_slctd_atrzcn_srvco			udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServicios		(cnsctvo_srvco_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServiciosOr		(cnsctvo_dto_srvco_slctdo_orgnl				udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdMedicamentos	(cnsctvo_mdcmnto_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdInsumos			(cnsctvo_prcdmnto_insmo_slctdo				udtConsecutivo NOT NULL, idXML	int NOT NULL)

	/*:::::::::::::::CREACIÓN DE TABLAS TEMPORALES DONDE SE GUARDARÁ LA INFORMACIÓN DE LOS XML ENVIADOS::::::::::::::::::*/
  	CREATE TABLE #Tempo_Solicitudes( id									Int  NOT NULL										
									,cnsctvo_cdgo_orgn_atncn			udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_srvco_slctdo		udtConsecutivo NULL
									,cnsctvo_cdgo_prrdd_atncn			udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_ubccn_pcnte		udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_slctd				udtConsecutivo NULL
									,jstfccn_clnca						varchar(2000) NULL
									,cnsctvo_cdgo_mdo_cntcto_slctd		udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_trnsccn_srvco_sld	udtConsecutivo NULL
									,nmro_slctd_prvdr					varchar(15) NULL
									,nmro_slctd_pdre					varchar(15) NULL
									,cdgo_eps							char(10) NULL
									,cnsctvo_cdgo_clse_atncn			udtConsecutivo NULL
									,cnsctvo_cdgo_frma_atncn			udtConsecutivo NULL
									,fcha_slctd							datetime NULL
									,nmro_slctd_atrzcn_ss				varchar(16) NULL
									,nmro_slctd_ss_rmplzo				varchar(15) NULL
									,cnsctvo_cdgo_ofcna_atrzcn			udtConsecutivo NULL
									,nuam								numeric(18,0) NULL
									,srvco_hsptlzcn						varchar(30) NULL
									,ga_atncn							varchar(30) NULL
									,cnsctvo_prcso_vldcn				udtConsecutivo NULL
									,cnsctvo_prcso_mgrcn				udtConsecutivo NULL
									,mgrda_gstn							int NULL
									,spra_tpe_st						udtLogico NULL
									,dmi								udtLogico NULL
									,obsrvcn_adcnl						UdtObservacion NULL
									,usro_crcn							udtUsuario NULL
									,cdgo_orgn_atncn					udtCodigo NULL
									,cdgo_tpo_srvco_slctdo				udtCodigo NULL
									,cdgo_prrdd_atncn					udtCodigo NULL
									,cdgo_tpo_ubccn_pcnte				udtCodigo NULL
									,cdgo_clse_atncn					char(3) NULL
									,cdgo_frma_atncn					udtCodigo NULL
									,cdgo_mdo_cntcto_slctd				udtCodigo NULL
									,cdgo_tpo_trnsccn_srvco_sld			udtCodigo NULL								
								)
	CREATE TABLE #Tempo_Afiliados ( id											Int	NOT NULL
									,cnsctvo_cdgo_tpo_idntfccn_afldo			udtConsecutivo NULL
									,nmro_idntfccn_afldo						udtNumeroIdentificacionLargo NULL
									,cnsctvo_cdgo_dprtmnto_afldo				udtConsecutivo NULL
									,cnsctvo_cdgo_cdd_rsdnca_afldo				udtConsecutivo NULL
									,cnsctvo_cdgo_cbrtra_sld					udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_pln						udtConsecutivo NULL
									,cnsctvo_cdgo_pln							udtConsecutivo NULL
									,cnsctvo_cdgo_estdo_pln						udtConsecutivo NULL
									,cnsctvo_cdgo_sxo							udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_vnclcn_afldo				udtConsecutivo NULL
									,nmro_unco_idntfccn_afldo					udtConsecutivo NULL
									,cnsctvo_afldo_slctd_orgn					udtConsecutivo NULL
									,cnsctvo_cdgo_estdo_drcho					udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_cntrto					udtConsecutivo NULL
									,nmro_cntrto								udtNumeroFormulario NULL
									,cnsctvo_bnfcro_cntrto						udtConsecutivo NULL
									,cdgo_ips_prmra								udtCodigoIps NULL
									,cnsctvo_cdgo_sde_ips_prmra					udtConsecutivo NULL
									,rcn_ncdo									udtLogico NULL
									,prto_mltple								udtLogico NULL
									,nmro_hjo_afldo								udtConsecutivo NULL
									,cnsctvo_cdgo_sxo_rcn_ncdo					udtConsecutivo NULL
									,ttla										udtLogico NULL
									,cnsctvo_cdgo_chrte							udtConsecutivo NULL
									,cnsctvo_cdgo_rngo_slrl						udtConsecutivo NULL
									,usro_crcn									udtUsuario NULL					
									,prmr_aplldo_afldo							udtApellido NULL
									,sgndo_aplldo_afldo							udtApellido NULL
									,prmr_nmbre_afldo							udtNombre NULL
									,sgndo_nmbre_afldo							udtNombre NULL
									,cdgo_tpo_idntfccn_afldo					udtCodigo NULL
									,fcha_ncmnto_afldo							datetime NULL
									,drccn_afldo								udtDireccion NULL
									,tlfno_afldo								udtTelefono NULL
									,cdgo_dprtmnto_afldo						char(3) NULL
									,dscrpcn_dprtmnto_afldo						udtDescripcion NULL
									,cdgo_cdd_rsdnca_afldo						udtCiudad NULL
									,dscrpcn_cdd_rsdnca_afldo					udtDescripcion NULL
									,tlfno_cllr_afldo							char(30) NULL
									,crro_elctrnco_afldo						udtEmail NULL
									,cdgo_cbrtra_sld							varchar(5) NULL
									,cdgo_pln									udtCodigo NULL
									,cdgo_sxo									udtCodigo NULL
									,cdgo_sxo_rcn_ncdo							udtCodigo NULL
									,cdgo_tpo_pln								udtCodigo NULL
									,cdgo_tpo_vnclcn_afldo						udtCodigo NULL
									,cdgo_estdo_pln								udtCodigo NULL
									,fcha_ncmnto_rcn_ncdo						datetime NULL
									)

	CREATE TABLE #Tempo_IPS		 (  id											Int NOT NULL	
									,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte		udtConsecutivo NULL
									,nmro_idntfccn_ips_slctnte					udtNumeroIdentificacionLargo NULL
									,cdgo_intrno								udtCodigoIps NULL
									,nmro_indctvo_prstdr						udtConsecutivo NULL
									,cnsctvo_cdgo_dprtmnto_prstdr				udtConsecutivo NULL
									,cnsctvo_cdgo_cdd_prstdr					udtConsecutivo NULL
									,cnsctvo_ips_slctd_orgn						udtConsecutivo NULL
									,adscrto									udtLogico NULL
									,nmro_unco_idntfccn_prstdr					udtConsecutivo NULL
									,usro_crcn									udtUsuario NULL	
									,nmbre_ips									udtDescripcion NULL
									,nmbre_scrsl								udtDescripcion NULL
									,cdgo_tpo_idntfccn_ips_slctnte				udtCodigo NULL
									,dgto_vrfccn								int NULL
									,cdgo_hbltcn								char(12) NULL
									,drccn_prstdr								udtDireccion NULL
									,tlfno_prstdr								udtTelefono NULL
									,cdgo_dprtmnto_prstdr						char(3) NULL
									,cdgo_cdd_prstdr							udtCiudad NULL																
								)
										
	CREATE TABLE #Tempo_Hospitalaria (  id								Int	NOT NULL
										,ds_estnca						udtConsecutivo NULL
										,nmro_vsts						udtConsecutivo NULL
										,fcha_ingrso_hsptlzcn			datetime NULL
										,fcha_egrso_hsptlzcn			datetime NULL
										,cnsctvo_cdgo_clse_hbtcn		udtConsecutivo NULL
										,cnsctvo_cdgo_srvco_hsptlzcn	udtConsecutivo NULL
										,cma							varchar(10) NULL
										,crte_cnta						udtConsecutivo
										,usro_crcn						udtUsuario NULL								
										,cdgo_clse_hbtcn				udtCodigo NULL
										,cdgo_srvco_hsptlzcn			udtCodigo NULL											
									)
											
	CREATE TABLE #Tempo_Medicos (   id										Int	NOT NULL
									,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		udtConsecutivo NULL
									,cnsctvo_cdgo_espcldd_mdco_trtnte		udtConsecutivo NULL
									,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	udtConsecutivo NULL
									,nmro_idntfccn_mdco_trtnte				udtNumeroIdentificacionLargo NULL
									,rgstro_mdco_trtnte						varchar(20) NULL
									,cnsctvo_mdco_trtnte_slctd_orgn			udtConsecutivo NULL
									,adscrto								udtLogico NULL
									,nmro_unco_idntfccn_mdco				udtConsecutivo NULL
									,usro_crcn								udtUsuario NULL
									,cdgo_espcldd_mdco_trtnte				char(3) NULL
									,cdgo_tpo_idntfccn_mdco_trtnte			udtCodigo NULL
									,prmr_nmbre_mdco_trtnte					udtNombre NULL
									,sgndo_nmbre_mdco_trtnte				udtNombre NULL
									,prmr_aplldo_mdco_trtnte				udtApellido NULL
									,sgndo_aplldo_mdco_trtnte				udtApellido NULL
									,cdgo_tpo_aflcn_mdco_trtnte				udtCodigo NULL
								)

	CREATE TABLE #Tempo_Diagnostico (id_fila					INT IDENTITY(1,1) 
									,id							Int NOT NULL
									,nmro_prstcn				INT
									,cnsctvo_cdgo_tpo_dgnstco	udtConsecutivo NULL
									,cnsctvo_cdgo_dgnstco		udtConsecutivo NULL
									,cnsctvo_cdgo_cntngnca		udtConsecutivo NULL
									,cnsctvo_cdgo_rcbro			udtConsecutivo NULL
									,usro_crcn					udtUsuario NULL
									,cdgo_dgnstco				udtCodigoDiagnostico NULL
									,cdgo_tpo_dgnstco			char(3) NULL
									,cdgo_cntngnca				Char(3) NULL
									,cdgo_rcbro					char(3) NULL
									)

	CREATE TABLE #Tempo_Prestaciones (	id_fila									INT IDENTITY(1,1)
										,id										Int NOT NULL
										,nmro_prstcn							INT
										,cnsctvo_cdgo_srvco_slctdo				udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_srvco					udtConsecutivo NULL
										,dscrpcn_srvco_slctdo					udtDescripcion NULL 
										,cntdd_slctda							int NULL
										,fcha_prstcn_srvco_slctdo				datetime NULL
										,tmpo_trtmnto_slctdo					int NULL
										,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	udtConsecutivo NULL
										,vlr_lqdcn_srvco						udtConsecutivo NULL
										,cnsctvo_srvco_slctdo_orgn				udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_atrzcn				udtConsecutivo NULL
										,cnsctvo_cdgo_prstcn_prstdr				udtConsecutivo NULL
										,cnfrmcn_cldd							udtLogico NULL
										,usro_crcn								udtUsuario NULL
										,cdgo_tpo_srvco							udtCodigo NULL
										,cdgo_srvco_slctdo						char(15) NULL
										,cdgo_undd_tmpo_trtmnto_slctdo			udtCodigo NULL
										,cdgo_prstcn_prstdr						udtCodigo NULL
										,cdgo_tpo_atrzcn						udtCodigo NULL								
										,dss									float NULL
										,cnsctvo_cdgo_prsntcn_dss				udtConsecutivo NULL
										,prdcdd_dss								float NULL
										,cnsctvo_cdgo_undd_prdcdd_dss			udtConsecutivo NULL
										,cnsctvo_cdgo_frma_frmctca				udtConsecutivo NULL
										,cnsctvo_cdgo_grpo_trptco				udtConsecutivo NULL
										,cnsctvo_cdgo_prsntcn					udtConsecutivo NULL
										,cnsctvo_cdgo_va_admnstrcn_mdcmnto		udtConsecutivo NULL
										,cnsctvo_mdcmnto_slctdo_orgn			udtConsecutivo NULL
										,cncntrcn_dss							char(20) NULL
										,prsntcn_dss							char(20) NULL
										,cnsctvo_cdgo_undd_cncntrcn_dss			udtConsecutivo NULL
										,cdgo_prsntcn_dss						udtCodigo NULL
										,cdgo_undd_prdcdd_dss					udtCodigo NULL
										,cdgo_frma_frmctca						char(4) NULL
										,cdgo_grpo_trptco						char(3) NULL
										,cdgo_prsntcn							char(4) NULL
										,cdgo_va_admnstrcn_mdcmnto				udtCodigo NULL
										,cdgo_undd_cncntrcn_dss					char(10) NULL									
										,cnsctvo_cdgo_ltrldd					udtConsecutivo NULL
										,cnsctvo_cdgo_va_accso					udtConsecutivo NULL
										,cdgo_ltrldd							udtCodigo NULL
										,cdgo_va_accso							udtcodigo NULL
									)	
	CREATE 
	TABLE  #Tempo_Anexos(id								 udtConsecutivo	NOT NULL,
							cnsctvo_cdgo_mdlo_dcmnto_sprte	 udtConsecutivo NULL,
							cnsctvo_cdgo_dcmnto_sprte		 udtConsecutivo NULL,
							usro_crcn						 udtUsuario NULL
						)

	CREATE 
	TABLE #Tempo_ValidacionEspecial(id					udtConsecutivo NOT NULL,
									nmro_vldcn_espcl	NUMERIC(18,0),
									cnsctvo_cdgo_ofcna	udtConsecutivo NULL,
									usro_vldcn_espcl	udtUsuario NULL,
									accn_vldcn_espcl	udtDescripcion NULL,
									cnsctvo_ops			udtConsecutivo NULL,
									fcha_vldcn_espcl	DATETIME,
									usro_crcn			udtUsuario
									)		
											  								
	CREATE 
	TABLE #solicitudes_guardadas(id                         udtconsecutivo identity(1,1) NOT NULL,  
		                            cnsctvo_slctd_atrzcn_srvco udtconsecutivo NOT NULL              , 
									Identificador_sol          udtconsecutivo NOT NULL
								    )	
									 
	Create
	Table  #preSolicitudes(cnsctvo_slctd_atrzcn_srvco udtconsecutivo NOT NULL,
		                    cnsctvo_prslctd            udtconsecutivo
		                    )					
		
	BEGIN TRY
			
		set @trancount = @@trancount;
			if @trancount = 0
			begin transaction
		else
			save transaction spASGrabarSolicitudGeneral;

		-- Llenar Tablas Temporales
		EXEC BdCNA.gsa.spASGrabarSolicitudLlenarTemporales	 @infrmcn_slctd 
															,@infrmcn_prstcn 
															,@infrmcn_prstdr 
															,@infrmcn_mdco 
															,@infrmcn_dgnstco 
															,@infrmcn_hsptlra 
															,@infrmcn_afldo 
															,@infrmcn_anxs
															,@infrmcn_vldcion_espcial
																 
		-- Grabar solicicitudes
		EXEC BdCNA.gsa.spASGrabarSolicitudSolicitudes 
		-- Grabar Afiliados
		EXEC BdCNA.gsa.spASGrabarSolicitudAfiliados 
		-- Grabar IPS - Prestador
		EXEC BdCNA.gsa.spASGrabarSolicitudIPS 
		--Grabar Información Hospitalaria
		EXEC BdCNA.gsa.spASGrabarSolicitudInformacionHospitalaria
		--Grabar Informacion de Medico
		EXEC BdCNA.gsa.spASGrabarSolicitudMedico 
		--Grabar Diagnostico
		EXEC BdCNA.gsa.spASGrabarSolicitudDiagnostico 
		-- Grabar Prestaciones
		EXEC BdCNA.gsa.spASGrabarSolicitudPrestaciones 
		--Grabar Anexos
		EXEC BdCNA.gsa.spASGrabarSolicitudAnexos 		
		--Grabar Validación Especial
		EXEC BdCNA.gsa.spASGrabarValidacionEspecial

		Update		ss
		Set			cnsctvo_cdgo_rcbro = dsas.cnsctvo_cdgo_rcbro 
		From		#Idsolicitudes ids
		Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(RowLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = ids.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios dsas With(NoLock)
		On			dsas.cnsctvo_slctd_atrzcn_srvco = ids.cnsctvo_slctd_atrzcn_srvco

		/*::::::::::::::::GENERACIÓN DE NRO DE RADICADO DE LA SOLICITUD:::::::::::::::::::::::::::::::::::*/			
		BEGIN
			INSERT INTO		#solicitudes_guardadas(cnsctvo_slctd_atrzcn_srvco, identificador_Sol)
			SELECT	    	IDS.cnsctvo_slctd_atrzcn_srvco, IDS.idXML
			FROM			#Idsolicitudes		IDS 
			INNER JOIN		#Tempo_Solicitudes	SOL	
			ON				IDS.idXML = SOL.id		
				
			SELECT	@i = 1
					,@Max = Max(id)
			FROM	#solicitudes_guardadas 
							
			WHILE	@i <= @max
			BEGIN
				-- ACTUALIZAR SECUENCIA EN TABLA DE SOLICITUDES
				Update		BdCNA.gsa.tbASSolicitudesAutorizacionServicios
				Set			nmro_slctd_atrzcn_ss =convert(varchar(4),year(@fcha_actl))+'-'+ LTRIM(RTRIM(SOL.cdgo_mdo_cntcto_slctd)) + '-'+ replicate('0' ,8-len(convert(varchar(100),SEC.vlr_scnca))) + convert(varchar(100),SEC.vlr_scnca)
				From		bdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia SEC WITH (NOLOCK)
				Inner Join	#Tempo_Solicitudes										SOL	
				ON			SEC.prdo_scnca = convert(int,year(SOL.fcha_slctd))
				Inner Join	#solicitudes_guardadas									SGU 
				ON			SGU.identificador_sol = SOL.id
				Inner Join	BdCNA.gsa.tbASSolicitudesAutorizacionServicios			SOP	WITH (NOLOCK)
				ON			SOP.cnsctvo_slctd_atrzcn_srvco = SGU.cnsctvo_slctd_atrzcn_srvco
				Where		SGU.id = @i

				-- ACTUALIZAR SECUENCIA EN TABLA DE SECUENCIAS
				Update		BdCNA.gsa.tbASSolicitudesAutorizacionServiciosSecuencia
				Set			vlr_scnca = replicate('0' ,8-len(convert(varchar(100),vlr_scnca + 1))) + convert(varchar(100),vlr_scnca + 1)
				From		#Tempo_Solicitudes		SOL 
				Inner Join  #IdSolicitudes			IDS 
				ON			IDS.idXML = SOL.id		
				Inner Join	#solicitudes_guardadas	SGU 
				On			SGU.cnsctvo_slctd_atrzcn_srvco = IDS.cnsctvo_slctd_atrzcn_srvco
				Where		SGU.id = @i
				And			prdo_scnca = convert(int,year(SOL.fcha_slctd))

				SET			@i = @i + 1
			END
			
			/* Se adiciona para consultar las presolicitudes asociadas a la solicitud */
			Insert 
			Into        #preSolicitudes(cnsctvo_slctd_atrzcn_srvco, cnsctvo_prslctd)
			Select      a.cnsctvo_slctd_atrzcn_srvco, s.cnsctvo_prslctd
			From		bdCNA.gsa.tbASSolicitudesAutorizacionServicios	a WITH (NOLOCK)
			Inner Join	#IdSolicitudes	b 
			ON			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			Inner Join  bdCNA.gsa.tbASDatosAdicionalesPreSolicitud s WITH (NOLOCK)
			On          s.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			Where       a.cnsctvo_cdgo_mdo_cntcto_slctd = @cnsctvo_cdgo_mdo_cntcto_slctd

			/* Se valida la cantidad de registros, con el fin de cerrar las presolicitudes */
			Select @cant = Count(1) From #preSolicitudes
			If @cant > 0
			   Begin
			      Update     bdcna.dbo.tbPreSolicitudes 
				  Set        cnsctvo_cdgo_estdo_prslctd = @cnsctvo_cdgo_estdo_prslctd 
				  From       bdcna.dbo.tbPreSolicitudes a WITH (NOLOCK)
				  Inner Join #preSolicitudes b
				  On         b.cnsctvo_prslctd = a.cnsctvo_prslctd
			   End

			--RETORNAR
			SELECT		SOL.cnsctvo_slctd_atrzcn_srvco, SOL.nmro_slctd_atrzcn_ss
			FROM		BdCNA.gsa.tbASSolicitudesAutorizacionServicios	SOL WITH (NOLOCK)
			INNER JOIN	#IdSolicitudes									SGU 
			ON			SOL.cnsctvo_slctd_atrzcn_srvco = SGU.cnsctvo_slctd_atrzcn_srvco

			SELECT 	@estdo_ejccn = @codigoExito
					,@msje_errr = @mensajeExito

			-- BORRAR TEMPORALES
			Drop Table #Idsolicitudes		
			Drop Table #IdAfiliados	
			Drop Table #IdIPS
			Drop Table #IdHospitalaria
			Drop Table #IdMedico								
			Drop Table #IdDiagnostico						
			Drop Table #IdServicios							
			Drop Table #IdServiciosOr		
			Drop Table #IdMedicamentos								
			Drop Table #IdInsumos
			Drop Table #Tempo_Solicitudes
			Drop Table #Tempo_Afiliados
			Drop Table #Tempo_IPS
			Drop Table #Tempo_Hospitalaria
			Drop Table #Tempo_Medicos
			Drop Table #Tempo_Diagnostico
			Drop Table #Tempo_Prestaciones
			Drop Table #Tempo_Anexos
			Drop Table #Tempo_ValidacionEspecial
			Drop Table #solicitudes_guardadas	
			Drop Table #preSolicitudes
		END	
			
		if @trancount = 0   
		commit;
	END TRY
	BEGIN CATCH
		SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char(15)) + CHAR(13) +
		'Line:' + CAST(ERROR_LINE() AS char(15)) + CHAR(13) +
		'Message:' + ERROR_MESSAGE() + CHAR(13) +
		'Procedure:' + ERROR_PROCEDURE();
		SET @estdo_ejccn = @codigoError;
		SET @xstate = XACT_STATE()

			if @xstate = -1
				rollback;
			if @xstate = 1 and @trancount = 0
				rollback
			if @xstate = 1 and @trancount > 0
				rollback transaction spASGrabarSolicitudGeneral;
	END CATCH
END

