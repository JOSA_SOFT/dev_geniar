USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCargarInformacionConsultaDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCargarInformacionConsultaDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite cargar la información necesaria para el procedo D\>
					   <\D de consulta direccionamiento									 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 28/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASCargarInformacionConsultaDireccionamiento] @cdgs_slctds XML
AS

BEGIN
  SET NOCOUNT ON
   --Se inserta la información que llega para ser procesada para direccionamiento.
  INSERT INTO #tmpInformacionSolicitudPrestacion (cnsctvo_cdgo_prstcn
  , cdgo_prstcn
  , cnsctvo_slctd
  , nmro_slctd
  , nmro_prvdr
  , cdgo_intrno_ips_dstno)
    SELECT
      SER.cnsctvo_cdgo_srvco_slctdo,
      SRO.cdgo_srvco_slctdo,
      TPR.cnsctvo_slctd_srvco_sld_rcbda,
      SOL.nmro_slctd_atrzcn_ss,
      SOL.nmro_slctd_prvdr,
      RDR.cdgo_intrno
    FROM BDCna.gsa.tbASServiciosSolicitados SER WITH (NOLOCK)
    INNER JOIN #tmpNmroSlctds TPR WITH (NOLOCK)
      ON TPR.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_atrzcn_srvco
    INNER JOIN [BDCna].[gsa].[tbASResultadoDireccionamiento] RDR WITH (NOLOCK)
      ON TPR.cnsctvo_slctd_srvco_sld_rcbda = RDR.cnsctvo_slctd_atrzcn_srvco
      AND RDR.cnsctvo_srvco_slctdo = SER.cnsctvo_srvco_slctdo
    INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios SOL WITH (NOLOCK)
      ON SOL.cnsctvo_slctd_atrzcn_srvco = SER.cnsctvo_slctd_atrzcn_srvco
    LEFT JOIN BDCna.gsa.tbASServiciosSolicitadosOriginal SRO WITH (NOLOCK)
      ON SER.cnsctvo_srvco_slctdo = SRO.cnsctvo_srvco_slctdo;
END

GO
