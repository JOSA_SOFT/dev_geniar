USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarHistoricoDescargas]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASGuardarHistoricoDescargas  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D Procedimiento que permite guardar el historico de descargas de los formatos D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/03/11 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------      
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------    
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\DM DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*-----------------------------------------------------------------------------------------------------------------------*/ 
/*
Declare @xml           xml,
        @ldFechaActual datetime

Set @ldFechaActual = getDate()
Set @xml ='<prestacion>
				<servicio>		
				   <consecutivoservicio>73959</consecutivoservicio>	    
				   <codigoprestacion>19933740-01</codigoprestacion>
				   <consecutivoformato>3</consecutivoformato>
				   <nombredocumento>archivo1</nombredocumento>
				   <rutadescarga>archivo1</rutadescarga>
				   <cantidad>1</cantidad>
				</servicio>		
				<servicio>		
				   <consecutivoservicio>73959</consecutivoservicio>	    
				   <codigoprestacion>19933740-01</codigoprestacion>
				   <consecutivoformato>3</consecutivoformato>
				   <nombredocumento>archivo1</nombredocumento>
				   <rutadescarga>archivo1</rutadescarga>
				   <cantidad>1</cantidad>
				</servicio>						   			   	  
	       </prestacion>'

exec [gsa].[spASGuardarHistoricoDescargas] 83, 'user1', @ldFechaActual, '01', NULL, @xml 
*/
ALTER PROCEDURE  [gsa].[spASGuardarHistoricoDescargas]  
  @lnConsecutivoSolicitud   udtConsecutivo       ,
  @lcUsuarioDescargaFormato udtUsuario           ,
  @ldFechaDescargaFormato   datetime             ,
  @lcMedioContacto          char(4)              ,
  @nmro_unco_ops            udtConsecutivo = NULL,
  @xmlServicios             xml           

AS  
BEGIN	

    SET NOCOUNT ON 

	Declare @ldFechaActual                    Datetime      ,	
	        @lnConsecutivoMedioContacto       udtConsecutivo,
			@lnCnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo

	Create 
	Table  #tempGuardarHistoricoDescargas(cnsctvo_srvco_slctdo          udtConsecutivo,
	                                      cnsctvo_cdgo_srvco_slctdo     udtConsecutivo,
	                                      cdgo_srvco_slctdo             char(11)	  ,							  
										  nmbre_dcmnto_dscrga_frmto     udtDescripcion,
										  cnsctvo_cdgo_tpo_frmto_dscrga udtConsecutivo,
										  rta_dscrga_frmto              Varchar(50)   ,
										  cntdd_dscrga_frmto            Int           										   										 
										 )
	Set     @ldFechaActual = getDate()    


	Select @lnConsecutivoMedioContacto = cnsctvo_cdgo_mdo_cntcto
	From   bdCNA.prm.tbASMediosContacto_Vigencias With(NoLock)
	Where  cdgo_mdo_cntcto = @lcMedioContacto  
	And    @ldFechaActual Between inco_vgnca And fn_vgnca

	If (@nmro_unco_ops IS NOT NULL)
	   Begin
	       Select @lnCnsctvo_cncpto_prcdmnto_slctdo = cnsctvo_cncpto_prcdmnto_slctdo
		   From   bdCNA.gsa.tbASConceptosServicioSolicitado With(NoLock)
		   Where  nmro_unco_ops = @nmro_unco_ops
	   End

	Insert
	Into    #tempGuardarHistoricoDescargas(cnsctvo_cdgo_srvco_slctdo, cdgo_srvco_slctdo, cnsctvo_cdgo_tpo_frmto_dscrga,  
	                                       nmbre_dcmnto_dscrga_frmto, rta_dscrga_frmto , cntdd_dscrga_frmto
	                                      )
	SELECT  pref.value('consecutivoservicio[1]', 'udtConsecutivo') AS cnsctvo_cdgo_srvco_slctdo    ,
	        pref.value('codigoprestacion[1]'   , 'Char(11)')       AS cdgo_srvco_slctdo            ,
            pref.value('consecutivoformato[1]' , 'udtConsecutivo') AS cnsctvo_cdgo_tpo_frmto_dscrga,
			pref.value('nombredocumento[1]'    , 'udtDescripcion') AS nmbre_dcmnto_dscrga_frmto    ,
			pref.value('rutadescarga[1]'       , 'Varchar(50)')    AS rta_dscrga_frmto             ,
			pref.value('cantidad[1]'           , 'Int')            AS cntdd_dscrga_frmto        
	FROM    @xmlServicios.nodes('/prestacion//servicio') AS xml_slctd(Pref)


	Update     #tempGuardarHistoricoDescargas
	Set        cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	From       #tempGuardarHistoricoDescargas a
	Inner Join bdcna.gsa.tbASServiciosSolicitados ss With(NoLock)
	On         ss.cnsctvo_cdgo_srvco_slctdo  = a.cnsctvo_cdgo_srvco_slctdo And
	           ss.cnsctvo_slctd_atrzcn_srvco = @lnConsecutivoSolicitud
			   
	Insert 
	Into  bdCNA.gsa.tbASHistoricoDescargaFormatos(cnsctvo_slctd_atrzcn_srvco    , cnsctvo_srvco_slctdo         , cnsctvo_cdgo_mdo_cntcto,
                                                  cnsctvo_cncpto_prcdmnto_slctdo, fcha_dscrga_frmto            , usro_dscrga_frmto      , 
												  nmbre_dcmnto_dscrga_frmto     , cnsctvo_cdgo_tpo_frmto_dscrga, cntdd_dscrga_frmto     , 
												  rta_dscrga_frmto              , fcha_crcn                    , usro_crcn              , 
												  fcha_ultma_mdfccn             , usro_ultma_mdfccn
												 )
    Select @lnConsecutivoSolicitud          , a.cnsctvo_srvco_slctdo         , @lnConsecutivoMedioContacto,
	       @lnCnsctvo_cncpto_prcdmnto_slctdo, @ldFechaDescargaFormato        , @lcUsuarioDescargaFormato  , 
		   a.nmbre_dcmnto_dscrga_frmto      , a.cnsctvo_cdgo_tpo_frmto_dscrga, a.cntdd_dscrga_frmto       , 
		   a.rta_dscrga_frmto               , @ldFechaActual                 , @lcUsuarioDescargaFormato  , 
		   @ldFechaActual                   , @lcUsuarioDescargaFormato
	From   #tempGuardarHistoricoDescargas a 
End

GO
