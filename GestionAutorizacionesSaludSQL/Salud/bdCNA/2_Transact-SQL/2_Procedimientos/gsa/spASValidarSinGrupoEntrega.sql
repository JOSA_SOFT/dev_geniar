USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarSinGrupoEntrega]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarSinGrupoEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que asigna al grupo 0 las prestaciones que encuentre nulas. D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarSinGrupoEntrega] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@cdgo_grpo_entrga char(4),
@agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @afirmacion char(1)
  SET @afirmacion = 'S'

  IF EXISTS (SELECT
      sol.id
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL)
  BEGIN

    --Se actualiza al grupo en caso de que la prestación y el afiliado se encuentren.
    UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL;

    --Se determina si se debe realizar agrupación
    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
      AND @agrpa_prstcn = @afirmacion)
    BEGIN
      UPDATE sol
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @cdgo_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN #tmpDatosSolicitudFechaEntrega tmp
        ON tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
      WHERE tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga

    END
  END

END

GO
