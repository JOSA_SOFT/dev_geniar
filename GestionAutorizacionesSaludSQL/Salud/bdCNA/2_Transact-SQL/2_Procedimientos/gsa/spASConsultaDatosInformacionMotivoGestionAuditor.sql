USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionMotivoGestionAuditor]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionMotivoGestionAuditor
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el peso reputacional del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionMotivoGestionAuditor] 
	@cnsctvo_cdgo_tpo_mtvo_gstn	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;
	Declare @ldfcha_consulta	datetime,
			@dato_visible	char(1) = 'S';
	
	SET	@ldfcha_consulta = GETDATE();

	SELECT	cnsctvo_cdgo_mtvo_gstn, 
			cdgo_mtvo_gstn, 
			dscrpcn_mtvo_gstn  from bdcna.prm.tbASTiposMotivosGestionAuditor_Vigencias WITH (NOLOCK)
	where cnsctvo_cdgo_tpo_mtvo_gstn = @cnsctvo_cdgo_tpo_mtvo_gstn 
	And	  @ldfcha_consulta between inco_vgnca and fn_vgnca and vsble_usro = @dato_visible;
END

GO
