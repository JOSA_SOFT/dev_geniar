USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASAgrupacionIntegracionSipres]    Script Date: 7/25/2017 10:21:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASAgrupacionIntegracionSipres
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento que se encarga de realizar el proceso de agrupacion de las prestaciones. D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P  P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 21/07/2017 FC\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*--------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spASAgrupacionIntegracionSipres]
AS 
Begin
		Set NoCount On

		Declare @vlr_cro						udtConsecutivo,
				@cnsctvo_cdgo_agrpdr_prstcn5	udtConsecutivo,
				@cnsctvo_cdgo_agrpdr_prstcn4	udtConsecutivo,
				@vlr_nll						udtConsecutivo;

		-- 
		Create Table #tempPrestacionesConOPS (
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo,
			cnsctvo_cdgo_tpo_srvco			udtConsecutivo,
			cnsctvo_cdgo_rcbro				udtConsecutivo
		);

		-- 
		Create Table #tempPrestacionesSinOPS (
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo,
			cnsctvo_cdgo_tpo_srvco			udtConsecutivo,
			cnsctvo_cdgo_rcbro				udtConsecutivo,
			id_agrpdr						udtConsecutivo
		);

		-- 
		Create Table #tempConsultas (
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo
		);

		--
		Create Table #tmpServiciosAgrupar (
			id							Int Identity,
			cnsctvo_slctd_atrzcn_srvco  udtConsecutivo,
			cnsctvo_srvco_slctdo		udtConsecutivo,
			id_agrpdr					Int
		);

		--	
		Create Table #tempOPSxPrestacion (
			id							Int Identity,
			cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
			nmro_unco_ops				udtConsecutivo,
			id_agrpdr					udtConsecutivo
		);

		-- 
		Create Table #tempMinPrestacionOPS (
				cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
				cnsctvo_srvco_slctdo		udtConsecutivo,
				nmro_unco_ops				udtConsecutivo
		);

		--
		Create Table #tempAgrupadores (
			cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
			cnsctvo_srvco_slctdo		udtConsecutivo,
			cnsctvo_srvco_agrpdr		udtConsecutivo
		);

		--
		Create Table #tempAgrupacionSinOPS (
				id							Int Identity,
				cnsctvo_slctd_atrzcn_srvco	udtConsecutivo, 
				cnsctvo_cdgo_tpo_srvco		udtConsecutivo, 
				cnsctvo_cdgo_rcbro			udtConsecutivo ,
				id_agrpdr					udtConsecutivo
		);

		Set @vlr_cro = 0;
		Set	@cnsctvo_cdgo_agrpdr_prstcn5 = 5;
		Set	@cnsctvo_cdgo_agrpdr_prstcn4 = 4;
		Set @vlr_nll = Null;

		Update		#tmpinformacionservicios
		Set			nuam = @vlr_cro
		Where		nuam Is Null

		Update		#tmpinformacionconceptos
		Set			nmro_unco_ops = @vlr_cro
		Where		nmro_unco_ops Is Null

		-- Se obtienen las prestaciones con OPS
		Insert Into #tempPrestacionesConOPS (
					cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo, 
					cnsctvo_cdgo_tpo_srvco
		)
		Select Distinct 
					ise.cnsctvo_slctd_atrzcn_srvco, ise.cnsctvo_srvco_slctdo, 
					ise.cnsctvo_cdgo_tpo_srvco
		From		#tmpinformacionconceptos ico
		Inner Join	#tmpinformacionservicios ise
		On			ise.cnsctvo_srvco_slctdo = ico.cnsctvo_srvco_slctdo
		And			ise.cnsctvo_slctd_atrzcn_srvco = ico.cnsctvo_slctd_atrzcn_srvco
		Where		ise.nuam = @vlr_cro 
		And			ico.nmro_unco_ops > @vlr_cro
		And			ise.cnsctvo_cdgo_agrpdr_prstcn != @cnsctvo_cdgo_agrpdr_prstcn5;

		-- Se obtienen las prestaciones sin OPS
		Insert Into #tempPrestacionesSinOPS (
					cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo, 
					cnsctvo_cdgo_tpo_srvco,			cnsctvo_cdgo_rcbro
		)
		Select Distinct 
					ise.cnsctvo_slctd_atrzcn_srvco, ise.cnsctvo_srvco_slctdo, 
					ise.cnsctvo_cdgo_tpo_srvco,		ise.cnsctvo_cdgo_rcbro
		From		#tmpinformacionservicios ise
		Left Join	#tempPrestacionesConOPS pco 
		On			pco.cnsctvo_slctd_atrzcn_srvco = ise.cnsctvo_slctd_atrzcn_srvco
		And			pco.cnsctvo_srvco_slctdo = ise.cnsctvo_srvco_slctdo
		Where		ise.nuam = @vlr_cro 
		And			ise.cnsctvo_cdgo_agrpdr_prstcn != @cnsctvo_cdgo_agrpdr_prstcn5
		And			ise.cnsctvo_cdgo_agrpdr_prstcn != @cnsctvo_cdgo_agrpdr_prstcn4
		And			pco.cnsctvo_srvco_slctdo Is Null;

		-- Se obtienen las prestaciones de tipo consulta.
		Insert Into #tempConsultas (
					cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo
		)
		Select Distinct 
					ise.cnsctvo_slctd_atrzcn_srvco, ise.cnsctvo_srvco_slctdo
		From		#tmpinformacionservicios ise
		Where		ise.nuam = @vlr_cro 
		And			ise.cnsctvo_cdgo_agrpdr_prstcn = @cnsctvo_cdgo_agrpdr_prstcn5;

		-- Se obtienen todos los numeros de OPS con los el minimo servicio
		Insert Into #tempMinPrestacionOPS (
					cnsctvo_slctd_atrzcn_srvco,		nmro_unco_ops,		cnsctvo_srvco_slctdo
		)
		Select		pco.cnsctvo_slctd_atrzcn_srvco, ico.nmro_unco_ops,  Min(pco.cnsctvo_srvco_slctdo)
		From		#tempPrestacionesConOPS pco
		Inner Join	#tmpinformacionconceptos ico 
		On			ico.cnsctvo_slctd_atrzcn_srvco = pco.cnsctvo_slctd_atrzcn_srvco
		And			ico.cnsctvo_srvco_slctdo = pco.cnsctvo_srvco_slctdo
		Where		ico.nmro_unco_ops > @vlr_cro
		Group By	pco.cnsctvo_slctd_atrzcn_srvco, ico.nmro_unco_ops;

		-- Se sacan las prestaciones que tienen como agrupador una prestacion diferente a ella misma.
		Insert Into #tempAgrupadores
		(
					cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,	cnsctvo_srvco_agrpdr
		)
		Select		a.cnsctvo_slctd_atrzcn_srvco,	a.cnsctvo_srvco_slctdo, Min(b.cnsctvo_srvco_slctdo)
		From		#tmpinformacionconceptos a
		Inner Join	#tempMinPrestacionOPS b
		On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
		And			a.nmro_unco_ops = b.nmro_unco_ops
		Where		a.cnsctvo_srvco_slctdo != b.cnsctvo_srvco_slctdo
		Group By	a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo;

		-- Agrupa las prestaciones
		WITH tmpPrestacionesAgrupadas(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_srvco_agrpdor) 
		AS (
			Select		a.cnsctvo_slctd_atrzcn_srvco, a.cnsctvo_srvco_slctdo,	a.cnsctvo_srvco_agrpdr
			From		#tempAgrupadores a				
			Union All
			Select		d.cnsctvo_slctd_atrzcn_srvco, b.cnsctvo_srvco_slctdo,	d.cnsctvo_srvco_agrpdor
			From		tmpPrestacionesAgrupadas d				
			Inner Join	#tempAgrupadores b
			On			b.cnsctvo_slctd_atrzcn_srvco = d.cnsctvo_slctd_atrzcn_srvco
			And			b.cnsctvo_srvco_slctdo = d.cnsctvo_srvco_agrpdor
		)
		Insert Into #tmpServiciosAgrupar (
					cnsctvo_slctd_atrzcn_srvco,		cnsctvo_srvco_slctdo,	id_agrpdr
		)
		Select		d.cnsctvo_slctd_atrzcn_srvco,	d.cnsctvo_srvco_slctdo, Min(d.cnsctvo_srvco_agrpdor)
		From		tmpPrestacionesAgrupadas d
		Group By	cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_srvco_agrpdor;

		-- Se obtiene la prestacion agrupadora.
		Insert Into #tmpServiciosAgrupar (
						cnsctvo_slctd_atrzcn_srvco,	cnsctvo_srvco_slctdo,  id_agrpdr
		)
		Select Distinct a.cnsctvo_slctd_atrzcn_srvco,	a.cnsctvo_srvco_slctdo, c.id_agrpdr
		From		#tmpinformacionconceptos a
		Inner Join	#tempMinPrestacionOPS b
		On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
		And			a.nmro_unco_ops = b.nmro_unco_ops
		Inner Join	#tmpServiciosAgrupar c
		On			c.id_agrpdr = a.cnsctvo_srvco_slctdo;

		-- Se obtienen las prestaciones faltantes 
		Insert Into #tmpServiciosAgrupar (
						cnsctvo_slctd_atrzcn_srvco,	cnsctvo_srvco_slctdo,  id_agrpdr
		)
		Select		Distinct a.cnsctvo_slctd_atrzcn_srvco,	a.cnsctvo_srvco_slctdo, b.cnsctvo_srvco_slctdo
		From		#tmpinformacionconceptos a
		Inner Join	#tempMinPrestacionOPS b
		On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
		And			a.nmro_unco_ops = b.nmro_unco_ops
		left Join	#tempAgrupadores c
		On			c.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
		And			c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Where		c.cnsctvo_srvco_slctdo Is Null;

		-- 
		Insert
		Into #tempInformacionSolicitudAux (cnsctvo_slctd_atrzcn_srvco, fcha_slctd              , cdgo_intrno                ,
										nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
										cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
										usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
										cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
										cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
										cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
										cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
										fcha_crcn					, nuam					   , cnsctvo_cdgo_ofcna			,
										fcha_ingrso_hsptlzcn		, fcha_egrso_hsptlzcn	   , crte_cnta				    , 
										cnsctvo_cdgo_clse_hbtcn		, cnsctvo_cdgo_agrpdr_prstcn
										)
		Select	Distinct 
					ins.cnsctvo_slctd_atrzcn_srvco, fcha_slctd             , ins.cdgo_intrno            ,
					ins.nmro_unco_idntfccn_prstdr , nmro_unco_idntfccn_mdco, cnsctvo_cdgo_cntngnca      ,
					cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              ,
					usro_crcn                   , ins.usro_ultma_mdfccn    , ins.fcha_ultma_mdfccn      ,
					cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, iss.cnsctvo_cdgo_rcbro     ,
					cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
					ins.cnsctvo_cdgo_estdo_mega , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
					ins.cnsctvo_cdgo_estdo      , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
					fcha_crcn					, iss.nuam				   , iss.cnsctvo_cdgo_ofcna		,	
					ins.fcha_ingrso_hsptlzcn	, ins.fcha_egrso_hsptlzcn  , ins.crte_cnta				, 
					ins.cnsctvo_cdgo_clse_hbtcn , sa.id_agrpdr
		From		#tmpinformacionsolicitud ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco	
		Inner Join	#tmpServiciosAgrupar sa 
		On			sa.cnsctvo_slctd_atrzcn_srvco = ins.cnsctvo_slctd_atrzcn_srvco
		And			sa.id_agrpdr =	iss.cnsctvo_srvco_slctdo;

		-- 
		Update		a
		Set			id_agrpdr = b.id 
		From		#tmpServiciosAgrupar a
		Inner Join  #tempInformacionSolicitudAux b 
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_cdgo_agrpdr_prstcn = a.id_agrpdr;

		--
		Update		a
		Set			id_agrpdr = b.id_agrpdr 
		From		#tmpinformacionservicios a
		Inner Join	#tmpServiciosAgrupar b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

		-- 
		Update		#tempInformacionSolicitudAux
		Set			cnsctvo_cdgo_agrpdr_prstcn = @vlr_nll;
		
		-- 
		Insert Into #tempAgrupacionSinOPS (
				cnsctvo_slctd_atrzcn_srvco,			 cnsctvo_cdgo_tpo_srvco, cnsctvo_cdgo_rcbro
		)
		Select	Distinct cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_srvco, cnsctvo_cdgo_rcbro 
		From	#tempPrestacionesSinOPS;

		-- 
		Insert 
		Into #tempInformacionSolicitudAux (cnsctvo_slctd_atrzcn_srvco  , fcha_slctd               , cdgo_intrno                ,
										nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
										cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
										usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
										cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
										cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
										cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
										cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
										fcha_crcn					, fcha_ingrso_hsptlzcn	   , fcha_egrso_hsptlzcn		, 
										crte_cnta				    , cnsctvo_cdgo_clse_hbtcn  , cnsctvo_cdgo_agrpdr_prstcn
										)
		Select Distinct 
					ins.cnsctvo_slctd_atrzcn_srvco, fcha_slctd             , cdgo_intrno                ,
					nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
					cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              ,
					usro_crcn                   , ins.usro_ultma_mdfccn    , ins.fcha_ultma_mdfccn      ,
					cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, ac.cnsctvo_cdgo_rcbro     ,
					cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
					ins.cnsctvo_cdgo_estdo_mega , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
					ins.cnsctvo_cdgo_estdo      , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
					fcha_crcn					, fcha_ingrso_hsptlzcn	   , fcha_egrso_hsptlzcn		, 
					crte_cnta				    , cnsctvo_cdgo_clse_hbtcn  , ac.id
		From		#tmpinformacionsolicitud ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		Inner Join	#tempAgrupacionSinOPS ac
		On			ac.cnsctvo_slctd_atrzcn_srvco = ins.cnsctvo_slctd_atrzcn_srvco;

		-- 
		Update		a
		Set			id_agrpdr = b.id 
		From		#tempAgrupacionSinOPS a
		Inner Join	#tempInformacionSolicitudAux b
		On			b.cnsctvo_cdgo_agrpdr_prstcn = a.id;

		-- 
		Update		a
		Set			id_agrpdr = b.id_agrpdr
		From		#tempPrestacionesSinOPS a
		Inner Join	#tempAgrupacionSinOPS b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_cdgo_tpo_srvco = a.cnsctvo_cdgo_tpo_srvco
		And			b.cnsctvo_cdgo_rcbro = a.cnsctvo_cdgo_rcbro;

		--
		Update		a
		Set			id_agrpdr = b.id_agrpdr
		From		#tmpinformacionservicios a
		Inner Join	#tempPrestacionesSinOPS b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

		-- 
		Update		#tempInformacionSolicitudAux
		Set			cnsctvo_cdgo_agrpdr_prstcn = @vlr_nll;

		-- 
		Insert 
		Into #tempInformacionSolicitudAux (cnsctvo_slctd_atrzcn_srvco  , fcha_slctd               , cdgo_intrno                ,
										nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
										cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              , 
										usro_crcn                   , usro_ultma_mdfccn        , fcha_ultma_mdfccn          ,
										cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, cnsctvo_cdgo_rcbro         ,
										cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
										cnsctvo_cdgo_estdo_mega     , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
										cnsctvo_cdgo_estdo          , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
										fcha_crcn					, fcha_ingrso_hsptlzcn	   , fcha_egrso_hsptlzcn		, 
										crte_cnta				    , cnsctvo_cdgo_clse_hbtcn  , cnsctvo_cdgo_agrpdr_prstcn
										)
		Select Distinct 
					ins.cnsctvo_slctd_atrzcn_srvco, fcha_slctd             , cdgo_intrno                ,
					nmro_unco_idntfccn_prstdr   , nmro_unco_idntfccn_mdco  , cnsctvo_cdgo_cntngnca      ,
					cnsctvo_cdgo_clse_atncn     , cnsctvo_cdgo_frma_atncn  , obsrvcn_adcnl              ,
					usro_crcn                   , ins.usro_ultma_mdfccn    , ins.fcha_ultma_mdfccn      ,
					cnsctvo_cdgo_dgnstco        , cnsctvo_cdgo_ofcna_atrzcn, iss.cnsctvo_cdgo_rcbro     ,
					cnsctvo_cdgo_tpo_ubccn_pcnte, nmro_slctd_prvdr         , cnsctvo_cdgo_srvco_hsptlzcn,
					ins.cnsctvo_cdgo_estdo_mega , cnsctvo_cdgo_mdo_cntcto  , hra_slctd_orgn             ,
					ins.cnsctvo_cdgo_estdo      , fcha_estmda_entrga       , cnsctvo_cdgo_estdo_atncn   ,
					fcha_crcn					, fcha_ingrso_hsptlzcn	   , fcha_egrso_hsptlzcn		, 
					crte_cnta				    , cnsctvo_cdgo_clse_hbtcn  , ac.cnsctvo_srvco_slctdo
		From		#tmpinformacionsolicitud ins
		Inner Join	#tmpinformacionservicios iss
		On			ins.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		Inner Join	#tempConsultas ac
		On			ac.cnsctvo_slctd_atrzcn_srvco = ins.cnsctvo_slctd_atrzcn_srvco
		And			ac.cnsctvo_srvco_slctdo = iss.cnsctvo_srvco_slctdo;

		-- 
		Update		b
		Set			id_agrpdr = a.id
		From		#tempInformacionSolicitudAux a
		Inner Join	#tmpinformacionservicios b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		And			b.cnsctvo_srvco_slctdo = a.cnsctvo_cdgo_agrpdr_prstcn;

		-- 
		Update		#tempInformacionSolicitudAux
		Set			cnsctvo_cdgo_agrpdr_prstcn = @vlr_nll;

		
		Drop Table #tempAgrupacionSinOPS
		Drop Table #tempAgrupadores
		Drop Table #tempConsultas
		Drop Table #tempMinPrestacionOPS
		Drop Table #tempOPSxPrestacion
		Drop Table #tempPrestacionesConOPS
		Drop Table #tempPrestacionesSinOPS
		Drop Table #tmpServiciosAgrupar

End
