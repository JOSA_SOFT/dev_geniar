USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudInformacionHospitalaria]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudInformacionHospitalaria
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información Hospitalaria como 
					 :	modificación de una solicitud especifica D\>  
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-----------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*---------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudInformacionHospitalaria] 
AS
  SET NOCOUNT ON
  DECLARE	@Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal
		DELETE			IHO	
		FROM			BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal IHO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				IHO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASInformacionHospitalariaSolicitudAutorizacionServicios
		DELETE			IHP
		FROM			BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios	 IHP
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				IHP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- INSERTAR INFORMACIÓN DE Información Hospitalaria EN TABLA DE PROCESO tbASInformacionHospitalariaSolicitudAutorizacionServicios 	
		MERGE INTO			BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios
		USING (SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
							,THO.id
							,THO.ds_estnca	
							,THO.nmro_vsts
							,THO.fcha_ingrso_hsptlzcn			
							,THO.fcha_egrso_hsptlzcn			
							,THO.cnsctvo_cdgo_clse_hbtcn
							,THO.cnsctvo_cdgo_srvco_hsptlzcn	
							,THO.cma		
							,THO.crte_cnta					
							,THO.usro_crcn						
				FROM		#TMod_Hospitalaria	THO WITH (NOLOCK)		
				INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
				ON			THO.id = IDS.id) AS HOS								
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,ds_estnca
				,nmro_vsts
				,fcha_ingrso_hsptlzcn
				,fcha_egrso_hsptlzcn
				,cnsctvo_cdgo_clse_hbtcn
				,cnsctvo_cdgo_srvco_hsptlzcn
				,cma
				,crte_cnta
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn
				)
		VALUES (HOS.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,HOS.ds_estnca	
				,HOS.nmro_vsts
				,HOS.fcha_ingrso_hsptlzcn			
				,HOS.fcha_egrso_hsptlzcn			
				,HOS.cnsctvo_cdgo_clse_hbtcn
				,HOS.cnsctvo_cdgo_srvco_hsptlzcn	
				,HOS.cma	
				,HOS.crte_cnta
				,@Fecha_Actual	--fcha_crcn						
				,HOS.usro_crcn	
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,HOS.usro_crcn	--usro_ultma_mdfccn
				)	
		OUTPUT	inserted.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
				,HOS.id
		INTO	#IdHospitalaria_Mod;
	   -- INSERTAR INFORMACIÓN DE Información Hospitalaria EN TABLA ORIGINAL tbInformacionHospitalariaSolicitudAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal(
					 cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
					,ds_estnca
					,cnsctvo_slctd_atrzcn_srvco
					,nmro_vsts
					,fcha_ingrso_hsptlzcn
					,fcha_egrso_hsptlzcn
					,cdgo_clse_hbtcn
					,cma
					,crte_cnta
					,cdgo_srvco_hsptlzcn
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDH.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
					,HOS.ds_estnca	
					,IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,HOS.nmro_vsts
					,HOS.fcha_ingrso_hsptlzcn			
					,HOS.fcha_egrso_hsptlzcn	
					,HOS.cdgo_clse_hbtcn
					,HOS.cma		
					,HOS.crte_cnta		
					,HOS.cdgo_srvco_hsptlzcn	
					,@Fecha_Actual	--fcha_crcn						
					,HOS.usro_crcn	
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,HOS.usro_crcn	--usro_ultma_mdfccn		
		FROM		#TMod_Hospitalaria		HOS WITH (NOLOCK)
		INNER JOIN	#IdHospitalaria_Mod		IDH WITH (NOLOCK)
		ON			HOS.id = IDH.idXML
		INNER JOIN	#TMod_Solicitudes		IDS WITH (NOLOCK)
		ON			HOS.id = IDS.id
	END



GO
