USE BDCna
GO
/****** Object:  StoredProcedure [gsa].[spASRecalcularDetalleCambioFechaEntregaOPS]    Script Date: 16/11/2017 10:09:52 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASRecalcularDetalleCambioFechaEntregaOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASRecalcularDetalleCambioFechaEntregaOPS AS SELECT 1;'
END
GO

--GRANT EXECUTE on gsa.spASRecalcularDetalleCambioFechaEntregaOPS to  autsalud_rol


/*----------------------------------------------------------------------------------------
* Metodo o PRG :     gsa.spASRecalcularDetalleCambioFechaEntregaOPS
* Desarrollado por : <\A Ing. Germán Pérez - Geniar S.A.S A\>
* Descripcion  :     <\D 
						 Este procedimiento permite consultar el detalle de las OPS
						 de acuerdo a los criterios seleccionados en la pantalla de
						 'MODIFICAR FECHAS ENTREGA OPS'
					 D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P
					     @lxRsmnOPS     Lista con el resumen totalizado de las prestaciones a recalcular
						 @vlr		   valor base para recalcular si viene mayor a cero (0)
						 @prrdd        indica la prioridad para el recalculo (1) fecha mas reciente (2) menos reciente
						 
					 P\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/11/07 FC\>
* Ejemplo: 
    <\EJ
        exec spASRecalcularDetalleCambioFechaEntregaOPS null,2000000, 1, 'user1'


    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>   
*-----------------------------------------------------------------------------------------*/
 ALTER PROCEDURE gsa.spASRecalcularDetalleCambioFechaEntregaOPS 
	@lxRsmnOPS         xml =null,
	@vlr          	   numeric(14,2),
	@prrdd		       int ,
	@usro				udtUsuario
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @numeroRegistros 				int	
			,@indice 						int
			,@vlorAMdfcr 					numeric(14,2)
			,@cnsctvo_vgnca_agrpdr_prstcn	udtConsecutivo
			,@vlr_rfrnca					numeric(14, 2)
			,@id_fsco 						int
			,@cntdd_ops_mdfcds				int
			,@vlr_ops_mdfcds				numeric(14,2)
			,@prcntje_100					numeric(12,2)
			,@fcha_ms_rcnte					int
			,@uno							int
			,@S								udtLogico
			,@cro							int
	
	CREATE TABLE #tbTmpAsOPSConsultadas (
		id							int Identity (1,1),
		cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
		cdgo_tpo_idntfccn			varchar(3),
		nmro_idntfccn				udtNumeroIdentificacion,
		cnsctvo_cdgo_pln			udtConsecutivo, 
		dscrpcn_pln					varchar(11),
		cnsctvo_cdgo_tpo_afldo		udtConsecutivo,
		fcha_estmda_entrga_fnl		datetime,
		fcha_expdcn					datetime,
		cnsctvo_cdgo_estdo			 udtConsecutivo,
		cnsctvo_cdgo_sde			udtConsecutivo,
		cnsctvo_cdgo_grpo_entrga	udtConsecutivo,
		cnsctvo_prstcn				udtConsecutivo,
		cdgo_cdfccn					char(11),
		dscrpcn_agrpdr_prstcn		udtObservacion,
		vlr_rfrnca					numeric(14, 2),
		cnsctvo_vgnca_agrpdr_prstcn udtConsecutivo,
		nmbre						varchar(200),
		nmro_ops					varchar(100),
		dscrpcn_cdfccn				varchar(500),
		ttla						varchar(1) default 'N',
		nmro_unco_idntfccn_afldo	udtConsecutivo,
		incpcdd						varchar(1) default 'N',
		cdgo_grpo_entrga			varchar(10), 
		dscrpcn_grpo_entrga			udtDescripcion default ' ',
		pln_pc						varchar(1) default 'N',
		dscrpcn_estdo_mga			udtDescripcion default ' ',
		dscrpcn_clsfccn_evnto		udtDescripcion default ' ',
		fcha_entrga					datetime,
		cnsctvo_cdgo_ofcna			udtConsecutivo,
		nuam						numeric(18,0),
		cnsctvo_cdfccn				udtConsecutivo,
		rclclr						udtLogico default 'N',
		id_fsco						int

	)	

	CREATE  TABLE #tbTmpAsTotalOPSConsultadas (
		cnsctvo_vgnca_agrpdr_prstcn udtConsecutivo,
		prctj_vlr_ops_mdfcds		numeric(9,2)		
	)	 	


	CREATE TABLE #tmpOPSConsultadasFiltro(
		cnsctvo_vgnca_agrpdr_prstcn	udtConsecutivo,
		vlr_rfrnca_tmp	numeric(14,2),
		ttl_mdfcds 	numeric(14,2)
	)
	

	SET @prcntje_100	= 100.0
	SET @fcha_ms_rcnte	= 1
	SET @uno			= 1
	SET @cro			= 0
	SET @S				= 'S'
    --valida si el recalculo es por valor 
	if  @vlr>@cro
	BEGIN
		--Actualiza los porcentajes de valor y cantidad que se van a modificacion		 
		update tmp
			set  vlr_ops_rclclo	=  @vlr * (prctj_vlr_ops_cnsltds/@prcntje_100)
				,vlr_ops_mdfcds =  @vlr * (prctj_vlr_ops_cnsltds/@prcntje_100)	
		from   BDCna.gsa.tbTmpAsTotalOPSConsultadas tmp With (Nolock)
		where tmp.usro = @usro
	END 
	ELSE
		BEGIN

			Insert Into #tbTmpAsTotalOPSConsultadas (cnsctvo_vgnca_agrpdr_prstcn,prctj_vlr_ops_mdfcds)
			SELECT
				cast(colx.query('data(cnsctvoVgncaAgrpdrPrstcn)') as varchar)  as  cnsctvo_vgnca_agrpdr_prstcn
				,cast(colx.query('data(prctjVlrOpsMdfcds)') as varchar)  as  prctj_vlr_ops_mdfcds
			FROM @lxRsmnOPS.nodes('rsmnsOps/rsmnOps') as TABX(COLX);
		
			---Actualiza los porcentajes de valor y cantidad que se van a modificacion
			 --Valor = Porcentaje Valor de OPS a Modificar / Porcentaje Valor OPS Consultadas * Valor OPS Consultadas. 
			 --Se debe visualizar el valor real que suman las OPS  a modificar por agrupador.
			update a
				set  vlr_ops_rclclo	= b.prctj_vlr_ops_mdfcds/a.prctj_vlr_ops_cnsltds *vlr_ops_cnsltds
					,vlr_ops_mdfcds = b.prctj_vlr_ops_mdfcds/a.prctj_vlr_ops_cnsltds *vlr_ops_cnsltds		
			from   BDCna.gsa.tbTmpAsTotalOPSConsultadas a with (nolock)
			inner join #tbTmpAsTotalOPSConsultadas b
			on a.cnsctvo_vgnca_agrpdr_prstcn=b.cnsctvo_vgnca_agrpdr_prstcn
			where a.usro = @usro
		END
		
		--// valida que tipo de prioridad se tiene (1) fecha mas reciente (2) menos reciente
		IF @prrdd =@fcha_ms_rcnte
			BEGIN 
				INSERT INTO	#tbTmpAsOPSConsultadas (id_fsco	 ,vlr_rfrnca	, cnsctvo_vgnca_agrpdr_prstcn,cnsctvo_prstcn )
				SELECT id	,	vlr_rfrnca	, cnsctvo_vgnca_agrpdr_prstcn	,cnsctvo_prstcn 
				FROM BDCna.gsa.tbTmpAsOPSConsultadas  With (Nolock)
				where usro = @usro
				order by  cnsctvo_vgnca_agrpdr_prstcn asc, fcha_expdcn desc
			END 
		ELSE
			 BEGIN
				INSERT INTO #tbTmpAsOPSConsultadas (id_fsco	,vlr_rfrnca	 ,cnsctvo_vgnca_agrpdr_prstcn	,cnsctvo_prstcn )
					SELECT id	,vlr_rfrnca	, cnsctvo_vgnca_agrpdr_prstcn	,cnsctvo_prstcn 
					FROM BDCna.gsa.tbTmpAsOPSConsultadas  With (Nolock)
					where usro = @usro
					order by  cnsctvo_vgnca_agrpdr_prstcn asc, fcha_expdcn asc			 
			 END 
					
			
	--Cuenta cuantos datos hay en la tabla 
	SELECT @numeroRegistros = COUNT(id)  From #tbTmpAsOPSConsultadas
	SET @indice = @uno
	--Este ciclo es necesario, debido que para realizar el proceso de recalcular los detalles se deben realizar de esta manera.
	WHILE (@indice <= @numeroRegistros)
	BEGIN
		--Consulta el la prestacion a evaluar en la tabla temporal
		select @cnsctvo_vgnca_agrpdr_prstcn 	= cnsctvo_vgnca_agrpdr_prstcn
			,@vlr_rfrnca						= vlr_rfrnca
			,@id_fsco							=id_fsco
		from #tbTmpAsOPSConsultadas
		where id = @indice
		
		--Extraer el valor totalizado 
		select @vlorAMdfcr = vlr_ops_rclclo 
		from BDCna.gsa.tbTmpAsTotalOPSConsultadas  With (Nolock)
		where cnsctvo_vgnca_agrpdr_prstcn =@cnsctvo_vgnca_agrpdr_prstcn
			and usro = @usro
		--Valida si el valor de la solicitud es (<=) al valor autorizado
		if  @vlr_rfrnca	<= @vlorAMdfcr
		BEGIN 
			--Se marca la solicitud para el recalculo
			update BDCna.gsa.tbTmpAsOPSConsultadas 
				set rclclr =@S 
			where id = @id_fsco 
			and usro = @usro
			
			--se disminuye el valor autorizado
			update a 
				set vlr_ops_rclclo	= vlr_ops_rclclo - @vlr_rfrnca
				  ,cntdd_ops_rclclo = cntdd_ops_rclclo+@uno				 
				  ,cntdd_ops_mdfcds = cntdd_ops_rclclo+@uno
			from BDCna.gsa.tbTmpAsTotalOPSConsultadas a  With (Nolock) 
			where a.cnsctvo_vgnca_agrpdr_prstcn =@cnsctvo_vgnca_agrpdr_prstcn
			and  a.usro = @usro
		END 
		
		SET @indice = @indice + @uno
	END

	Insert into	#tmpOPSConsultadasFiltro (cnsctvo_vgnca_agrpdr_prstcn, vlr_rfrnca_tmp, ttl_mdfcds)
	select	tmp.cnsctvo_vgnca_agrpdr_prstcn, sum(vlr_rfrnca) vlr_rfrnca_tmp, count(@uno) ttl_mdfcds
	from BDCna.gsa.tbTmpAsTotalOPSConsultadas ttl  With (Nolock)
	inner join BDCna.gsa.tbTmpAsOPSConsultadas tmp  With (Nolock)
	on tmp.cnsctvo_vgnca_agrpdr_prstcn = ttl.cnsctvo_vgnca_agrpdr_prstcn 
		and tmp.usro = ttl.usro 
	where tmp.usro = @usro  and tmp.rclclr =@S
	group by  tmp.cnsctvo_vgnca_agrpdr_prstcn

	
	-- Se modifican los valores totales de las OPS que se eligieron a ser modificadas
	update BDCna.gsa.tbTmpAsTotalOPSConsultadas
		set vlr_ops_mdfcds =  @cro	,
			cntdd_ops_mdfcds = @cro				
	where usro = @usro 

	update a 
		set vlr_ops_mdfcds = vlr_rfrnca_tmp,
			cntdd_ops_mdfcds = ttl_mdfcds
	from  BDCna.gsa.tbTmpAsTotalOPSConsultadas a With (Nolock)	
	inner join #tmpOPSConsultadasFiltro tbl_tmp
	on a.cnsctvo_vgnca_agrpdr_prstcn   =  tbl_tmp.cnsctvo_vgnca_agrpdr_prstcn 
			
	--consultan los totales
	select   @cntdd_ops_mdfcds = COUNT(@uno) 		,@vlr_ops_mdfcds = SUM(vlr_rfrnca)  
	from     BDCna.gsa.tbTmpAsOPSConsultadas  With (Nolock)
	where  rclclr =@S and usro = @usro
	
	--Actualiza los porcentajes de valor y cantidad que inicialmente es el mismo para consulta y modificacion
	update  a 
		set prctj_cntdd_ops_mdfcds 		=  cntdd_ops_mdfcds*@prcntje_100/@cntdd_ops_mdfcds			
			,prctj_vlr_ops_mdfcds 		=  vlr_ops_mdfcds*@prcntje_100/@vlr_ops_mdfcds		
	from    BDCna.gsa.tbTmpAsTotalOPSConsultadas a  With (Nolock) 		
	where a.usro = @usro
	  
		 
	
	select cnsctvo_vgnca_agrpdr_prstcn ,dscrpcn_agrpdr_prstcn		 
		,cntdd_ops_cnsltds		 	,vlr_ops_cnsltds 		,prctj_cntdd_ops_cnsltds	,prctj_vlr_ops_cnsltds		
		,cntdd_ops_mdfcds			,vlr_ops_mdfcds			,prctj_cntdd_ops_mdfcds		,prctj_vlr_ops_mdfcds	
	from   BDCna.gsa.tbTmpAsTotalOPSConsultadas   With (Nolock)
	where usro = @usro


	drop table #tbTmpAsOPSConsultadas
	drop table #tbTmpAsTotalOPSConsultadas
	drop table #tmpOPSConsultadasFiltro
END

