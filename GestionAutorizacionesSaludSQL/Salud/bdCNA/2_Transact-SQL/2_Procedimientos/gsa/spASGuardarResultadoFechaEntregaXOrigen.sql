USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarResultadoFechaEntregaXOrigen]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarResultadoFechaEntregaXOrigen
* Desarrollado por   : <\A Felipe Arcos Velez A\>    
* Descripcion        : <\D Procedimiento que permite guardar la fecha de entrega acorde al grupo   D\>
					   <\D asignado a las prestaciones de fecha esperada de entrega	 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 10/03/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGuardarResultadoFechaEntregaXOrigen] @cnsctvo_prcso udtConsecutivo,
@es_btch char(1),
@sistemaOrigen char
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @fechaActual datetime

  SET @fechaActual = GETDATE()
  
END


GO
