USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoIpsDiferenteCiudad]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoIpsDiferenteCiudad
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D IPS de diferentes Ciudades												 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoIpsDiferenteCiudad] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON
  --Declaración de variables para proceso
  DECLARE @afirmacion char(1) = 'S',
		  @valorMaximoDesplazamiento numeric(18,0),
		  @fechaActual DATETIME = GETDATE(),
		  @valorDefectoCiudad int = 999,
		  @consecutivoDesplazamiento int = 84,
		  @vlor_cro	int = 0;

  --Se valida que sea diferente la ciudad.
  IF EXISTS (SELECT
      TCO.id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_cdd <> ISP.cnsctvo_cdgo_cdd_afldo
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
  BEGIN
    --Se consulta el parámetro del tiempo máximo de minutos para el desplazamiento.
    SELECT
      @valorMaximoDesplazamiento = PG.vlr_prmtro_nmrco
    FROM bdSisalud.dbo.tbParametrosGenerales_vigencias PG WITH (NOLOCK)
    WHERE PG.cnsctvo_cdgo_prmtro_gnrl = @consecutivoDesplazamiento
    AND @fechaActual BETWEEN PG.inco_vgnca AND PG.fn_vgnca

	--Se actualiza la información de la duración de los desplazamientos.
	UPDATE TCO
	SET tmpo_dsplzmnto = ISNULL(DDV.drcn_mnts_dsplzmnto, @valorDefectoCiudad)
	FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
	LEFT JOIN bdCNA.prm.tbASDuracionDesplazamiento_Vigencias DDV WITH (NOLOCK)
        ON DDV.cnsctvo_cdgo_cdd_orgn = ISP.cnsctvo_cdgo_cdd_afldo
        AND DDV.cnsctvo_cdgo_cdd_dstno = TCO.cnsctvo_cdgo_cdd
		AND @fechaActual BETWEEN DDV.inco_vgnca AND DDV.fn_vgnca
	WHERE ISP.mrca_msmo_prstdor = @vlor_cro	;
	
	--Se eliminan los prestadores que no se encuentren en el rango de tiempo establecido.
	DELETE #tmpPrestadoresDestino
	WHERE tmpo_dsplzmnto > @valorMaximoDesplazamiento;

    --Se actualizan los prestadores que apliquen el filtro
	WITH tmpDesplazamiento AS (SELECT TPD.id_slctd_x_prstcn, MIN(TPD.tmpo_dsplzmnto) tmpo_dsplzmnto
		FROM #tmpPrestadoresDestino TPD
		INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
		ON ISP.id = TPD.id_slctd_x_prstcn
		AND TPD.cnsctvo_cdgo_cdd <> ISP.cnsctvo_cdgo_cdd_afldo
		WHERE ISP.mrca_msmo_prstdor = @vlor_cro
		GROUP BY TPD.id_slctd_x_prstcn)
    UPDATE TCO
    SET cmple = @afirmacion
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_cdd <> ISP.cnsctvo_cdgo_cdd_afldo
	INNER JOIN tmpDesplazamiento TPD
	  ON TCO.id_slctd_x_prstcn = TPD.id_slctd_x_prstcn
	  AND TPD.tmpo_dsplzmnto = TCO.tmpo_dsplzmnto
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro;


	EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
  END
END

GO
