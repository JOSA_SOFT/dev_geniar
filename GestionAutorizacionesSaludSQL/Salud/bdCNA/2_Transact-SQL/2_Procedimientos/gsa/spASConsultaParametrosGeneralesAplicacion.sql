USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaParametrosGeneralesAplicacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultaParametrosGeneralesAplicacion  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar los parametros generales 
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/01/22 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\DM DM/>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/    
--exec [gsa].[spASConsultaParametrosGeneralesAplicacion]   

ALTER PROCEDURE [gsa].[spASConsultaParametrosGeneralesAplicacion]    	
As

Begin
    SET NOCOUNT ON 

	Declare @vsble_usro			                  udtLogico    ,
	        @tpo_dto_prmtro		                  udtLogico    ,
	        @lcCdgo_prmtro_cntdd_ds               Char(3)      ,
			@lcCdgo_prmtro_nmro_ds                Char(3)      ,
			@lcCdgo_prmtro_crte_mnmo              Char(3)      ,
			@lcCdgo_prmtro_crte_mxmo              Char(3)      ,
			@lcCdgo_prmtro_cntdd_mss_frmto        Char(3)      ,			
			@lcCdgo_prmtro_rngo_cntdd_mss         Char(3)      ,
			@lcCdgo_prmtro_rngo_cntdd_mss_entrga  Char(3)      ,
			@vlr_prmtro_nmrco                     Numeric(18,0),
			@vlr_prmtro_crctr                     Char(200)    ,
			@vlr_prmtro_fcha                      Datetime     
	
	
	/*Se crea tabla parametro donde se almacenan los parametros generales a utilizar */
	Create 
	Table  #tmpParametrosGeneralesAplicacion(cnsctvo_cdgo_prmtro_gnrl udtConsecutivo,
	                                         vlr_prmtro_nmrco         Numeric(18,0) , 
											 vlr_prmtro_crctr         Varchar(200)  ,
											 vlr_prmtro_fcha          Datetime
	                                        ) 

	Set @vsble_usro                          = 'S'
	Set @tpo_dto_prmtro                      = ''
	Set @lcCdgo_prmtro_cntdd_ds              = '78'
    Set @lcCdgo_prmtro_nmro_ds               = '79'
    Set @lcCdgo_prmtro_crte_mnmo             = '80'
	Set @lcCdgo_prmtro_crte_mxmo             = '81'
	Set @lcCdgo_prmtro_rngo_cntdd_mss        = '87'
	Set @lcCdgo_prmtro_cntdd_mss_frmto       = '88'
	Set @lcCdgo_prmtro_rngo_cntdd_mss_entrga = '94'
	
	
	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_ds, @vsble_usro, @lcCdgo_prmtro_cntdd_ds, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para la fecha de consulta */
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_cntdd_ds, @vlr_prmtro_nmrco

	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_nmro_ds, @vsble_usro, @lcCdgo_prmtro_nmro_ds, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para la fecha de solicitud  */
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_nmro_ds, @vlr_prmtro_nmrco

	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_crte_mnmo, @vsble_usro, @lcCdgo_prmtro_crte_mnmo, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para el corte cuenta*/
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_crte_mnmo, @vlr_prmtro_nmrco

	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_crte_mxmo, @vsble_usro, @lcCdgo_prmtro_crte_mxmo, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para el corte cuenta*/
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_crte_mxmo, @vlr_prmtro_nmrco	
	
	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_rngo_cntdd_mss, @vsble_usro, @lcCdgo_prmtro_rngo_cntdd_mss, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para la cantidad de meses a generar para los formatos de devolucion*/
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_rngo_cntdd_mss, @vlr_prmtro_nmrco
	
	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para la cantidad de meses a generar para los formatos de devolucion*/
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_cntdd_mss_frmto, @vlr_prmtro_nmrco


	/*Procedimiento que permite consultar los parametros generales*/
	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_rngo_cntdd_mss_entrga, @vsble_usro, @lcCdgo_prmtro_rngo_cntdd_mss_entrga, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output
	
	/*Se inserta en la tabla parametro los parametros generales a utilizar para la cantidad de meses a generar para los formatos de devolucion*/
	Insert Into #tmpParametrosGeneralesAplicacion (cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco)
	Select @lcCdgo_prmtro_rngo_cntdd_mss_entrga, @vlr_prmtro_nmrco

	Select cnsctvo_cdgo_prmtro_gnrl, vlr_prmtro_nmrco
	From   #tmpParametrosGeneralesAplicacion
End
GO
