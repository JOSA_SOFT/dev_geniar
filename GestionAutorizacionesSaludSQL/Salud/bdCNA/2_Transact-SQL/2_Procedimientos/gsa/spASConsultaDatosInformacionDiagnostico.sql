USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionDiagnostico]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosConsecutivosDiagnostico
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
---exec [gsa].[spASConsultaDatosInformacionDiagnostico] 64

ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionDiagnostico]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @ldfcha_consulta Datetime,
	        @no              udtCodigo;

	Create 
	Table #diagnosticos(dscrpcn_tpo_dgnstco_sld	udtDescripcion      ,
		                cdgo_dgnstco			udtCodigoDiagnostico,
		                dscrpcn_dgnstco			udtDescripcion      ,
		                asgnd_SS				char(2)
	                   )

	Set	@ldfcha_consulta = GETDATE()
	Set @no              = 'no'
	
	Insert 
	Into   #diagnosticos(dscrpcn_tpo_dgnstco_sld, cdgo_dgnstco,
		                 dscrpcn_dgnstco        , asgnd_SS
	                    )
	SELECT     td.dscrpcn_tpo_dgnstco_sld, d.cdgo_dgnstco,
		       d.dscrpcn_dgnstco         , @no
	FROM       BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios dss WITH(NOLOCK)
	Inner Join bdSisalud.dbo.tbDiagnosticos_Vigencias d WITH(NOLOCK) 
	On         dss.cnsctvo_cdgo_dgnstco = d.cnsctvo_cdgo_dgnstco
	Inner Join bdSisalud.prm.tbTiposDiagnosticosSalud_Vigencias td WITH(NOLOCK) 
	On         dss.cnsctvo_cdgo_tpo_dgnstco = td.cnsctvo_cdgo_tpo_dgnstco_sld
	WHERE      dss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And		   @ldfcha_consulta between d.inco_vgnca and d.fn_vgnca
	And		   @ldfcha_consulta between td.inco_vgnca and td.fn_vgnca
	
		
	SELECT dscrpcn_tpo_dgnstco_sld, cdgo_dgnstco, 
	       dscrpcn_dgnstco        , asgnd_SS
	FROM #diagnosticos
	
		
END

GO
