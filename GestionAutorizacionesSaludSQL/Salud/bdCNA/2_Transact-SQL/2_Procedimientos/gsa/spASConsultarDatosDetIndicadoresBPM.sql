USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosDetIndicadoresBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultarDatosDetIndicadoresBPM]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultarDatosDetIndicadoresBPM] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosDetIndicadoresBPM
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosDetIndicadoresBPM]
@cnsctvo_indcdr		udtConsecutivo,
@fcha_inco			datetime,
@fcha_fn			datetime

AS
BEGIN

	SET NOCOUNT ON;

	Declare @ind_existencia_derecho int = 4;
	Declare @ind_direccionamiento int = 7;

	create table #TempDatosConsulta (
		dia				varchar(5),
		fecha_detalle	varchar(10),
		indicador		udtConsecutivo
	)

	create table #TempDetIndicadores (
		totalRegistros	int default 0,
		dia				varchar(10),
		totalRegistros2	int default 0,
		fecha_hora		varchar(30)
	)

	create table #TempDetIndicadores2 (
		totalRegistros	int default 0,
		fecha_hora2		varchar(30)
	)

	If(@cnsctvo_indcdr = @ind_existencia_derecho)
	Begin
		insert into #TempDatosConsulta(dia, fecha_detalle, indicador)
		Select  
			CASE 
				when DATEPART(dw, fcha_crcn) = 2 then 'Lun'
				when DATEPART(dw, fcha_crcn) = 3 then 'Mar' 
				when DATEPART(dw, fcha_crcn) = 4 then 'Mie' 
				when DATEPART(dw, fcha_crcn) = 5 then 'Jue' 
				when DATEPART(dw, fcha_crcn) = 6 then 'Vie' 
				when DATEPART(dw, fcha_crcn) = 7 then 'Sab'
				when DATEPART(dw, fcha_crcn) = 1 then 'Dom'
			END DIA, FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
			@ind_existencia_derecho id_indcdr
		from BDCna.gsa.tbASSolicitudesAutorizacionServicios sas 
		Where sas.fcha_crcn between @fcha_inco and  @fcha_fn; 

		Insert into #TempDetIndicadores(totalRegistros, dia, fecha_hora )
		select count(*), dia, fecha_detalle from #TempDatosConsulta
		where indicador = @cnsctvo_indcdr
		group by dia, fecha_detalle

		insert into #TempDetIndicadores2 ( 	totalRegistros, 	fecha_hora2	) 
		Select COUNT(1) , FECHA_HORA 
		from (
				Select  FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA from BDCna.gsa.tbASRegistroValidacionEspecial rve 
				Where rve.fcha_crcn between @fcha_inco and  @fcha_fn) x
				group by FECHA_HORA

	End
	Else If(@cnsctvo_indcdr = @ind_direccionamiento)
	Begin

		insert into #TempDatosConsulta(dia, fecha_detalle, indicador)
			Select DIA, FECHA_HORA, id_indcdr from (
			Select  
				CASE 
					when DATEPART(dw, fcha_crcn) = 2 then 'Lun'
					when DATEPART(dw, fcha_crcn) = 3 then 'Mar' 
					when DATEPART(dw, fcha_crcn) = 4 then 'Mie' 
					when DATEPART(dw, fcha_crcn) = 5 then 'Jue' 
					when DATEPART(dw, fcha_crcn) = 6 then 'Vie' 
					when DATEPART(dw, fcha_crcn) = 7 then 'Sab'
					when DATEPART(dw, fcha_crcn) = 1 then 'Dom'
				END DIA, FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA,
				@ind_direccionamiento id_indcdr, cnsctvo_slctd_atrzcn_srvco
			from BDCna.gsa.tbASServiciosSolicitados ss 
			Where ss.fcha_crcn between  @fcha_inco and  @fcha_fn 
		) x
		Group by DIA, FECHA_HORA, id_indcdr, cnsctvo_slctd_atrzcn_srvco

		insert into #TempDetIndicadores2 ( 	totalRegistros, 	fecha_hora2	) 
		select count(*),  fecha_detalle from #TempDatosConsulta
		where indicador = @ind_direccionamiento
		group by dia, fecha_detalle

		Insert into #TempDetIndicadores(totalRegistros, dia, fecha_hora )
		Select COUNT(1) , dia, FECHA_HORA 
		from (
				Select  FORMAT(fcha_crcn, 'ddMMyyyy') FECHA_HORA, cnsctvo_slctd_atrzcn_srvco,
				CASE 
					when DATEPART(dw, fcha_crcn) = 2 then 'Lun'
					when DATEPART(dw, fcha_crcn) = 3 then 'Mar' 
					when DATEPART(dw, fcha_crcn) = 4 then 'Mie' 
					when DATEPART(dw, fcha_crcn) = 5 then 'Jue' 
					when DATEPART(dw, fcha_crcn) = 6 then 'Vie' 
					when DATEPART(dw, fcha_crcn) = 7 then 'Sab'
					when DATEPART(dw, fcha_crcn) = 1 then 'Dom'
				END DIA 
				from BDCna.gsa.tbASHistoricoPrestadorServiciosSolicitados rve 
				Where rve.fcha_crcn between @fcha_inco and  @fcha_fn ) x
				group by FECHA_HORA, cnsctvo_slctd_atrzcn_srvco, DIA

	End
	Else
	Begin
		insert into #TempDatosConsulta(dia, fecha_detalle, indicador)
		Select
			 CASE 
    			when da_smna = 1 then 'Lun'
    			when da_smna = 2 then 'Mar' 
    			when da_smna = 3 then 'Mie' 
    			when da_smna = 4 then 'Jue' 
    			when da_smna = 5 then 'Vie' 
    			when da_smna = 6 then 'Sab'
    			when da_smna = 7 then 'Dom'
			END DIA, 
			FORMAT(fcha_hra, 'ddMMyyyy') FECHA_HORA,
			id_indcdr
			from BDCna.ind.tbAsDetalleIndicador di  With(NoLock)
			where 
			di.fcha_hra	between @fcha_inco and  @fcha_fn; 
		

			Insert into #TempDetIndicadores(totalRegistros, dia, fecha_hora )
			select count(*), dia, fecha_detalle from #TempDatosConsulta
			where indicador = @cnsctvo_indcdr
			group by dia, fecha_detalle
		
			insert into #TempDetIndicadores2 ( 	totalRegistros, 	fecha_hora2	) 
			select count(*), fecha_detalle from #TempDatosConsulta
			group by  fecha_detalle


		End

		Update di set totalRegistros2 = di2.totalRegistros
		from #TempDetIndicadores di 
		Inner join #TempDetIndicadores2 di2 on di.fecha_hora = di2.fecha_hora2

		Select TOTALREGISTROS, (DIA + SUBSTRING (FECHA_HORA, 1, 4)) DIA,
						TOTALREGISTROS2 from #TempDetIndicadores

		
		drop table #TempDetIndicadores
		drop table #TempDetIndicadores2;
		drop table #TempDatosConsulta;

END

GO
