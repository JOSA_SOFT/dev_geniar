USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEstadoGestionSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG         	: spASactualizarEstadoGestionSolicitud
* Desarrollado por		: <\A Jhon W. Olarte V.									A\>
* Descripcion			: <\D Procedimiento encargado de actualizar los			D\>
						  <\D estados de manera individual						D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 24-05-2016										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarEstadoGestionSolicitud] @id_slctd INT,
@id_prsctcn INT,
@nmro_unco_ops INT,
@cnsctvo_estdo INT,
@cdgo_estdo INT	OUTPUT,
@mnsje VARCHAR(1000) OUTPUT
AS
BEGIN
  SET NOCOUNT ON

  BEGIN TRY
    DECLARE @cdgo_estdo_ok INT,
            @cdgo_estdo_err INT

    SELECT
      @cdgo_estdo_ok = 0,
      @cdgo_estdo_err = -1

    CREATE TABLE #tmpestadosactualizar (
      cnsctvo_slctd_atrzcn_srvco udtconsecutivo DEFAULT 0,      -- Identificador de la solicitud
      cnsctvo_srvco_slctdo udtconsecutivo DEFAULT 0,      -- Identificador del servicio solicitado o prestación
      nmro_unco_ops udtconsecutivo DEFAULT 0,      -- Identificador de la OPS
      cnsctvo_cdgo_estdo udtconsecutivo DEFAULT 0       -- Identificador del estado a asignar
    );

    --Temporal para procesamiento de actualizaciones
    INSERT INTO #tmpestadosactualizar (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, nmro_unco_ops, cnsctvo_cdgo_estdo)
      SELECT
        @id_slctd,
        @id_prsctcn,
        @nmro_unco_ops,
        @cnsctvo_estdo

    EXEC bdcna.gsa.spasactualizarestadogestionsolicitudes

    --Asignación estado ejecución exitosa
    SELECT
      @cdgo_estdo = @cdgo_estdo_ok

  END TRY
  BEGIN CATCH

    SELECT
      @cdgo_estdo = @cdgo_estdo_err

    SELECT
      @mnsje = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
      'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
      'Message:', ERROR_MESSAGE(), CHAR(13),
      'Procedure:', ERROR_PROCEDURE())
  END CATCH

  DROP TABLE #tmpestadosactualizar;

END

GO
