USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPVencConsultaPlanAtencionAfiliado]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASPVencConsultaPlanAtencionAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el tipo de población al que pertenece el afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/03										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASPVencConsultaPlanAtencionAfiliado] 
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn udtNumeroIdentificacionLargo,
	@Nui_Afldo	UdtConsecutivo,
	@cnsctvo_cdgo_pln udtConsecutivo,
	@plan_atencion varchar(20) output,
	@tipo_plan varchar(20) output
AS
BEGIN
Set Nocount On

	
	Declare @getDate_ datetime = getDate();

	if @Nui_Afldo is null
	begin
		Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn)
	end

		
	Select  @plan_atencion = p.dscrpcn_pln,	
			@tipo_plan = tp.dscrpcn_tpo_pln
	From		bdAfiliacionValidador.dbo.tbBeneficiariosValidador			b	With(NoLock)
	INNER JOIN	bdAfiliacionValidador.dbo.tbContratosValidador				c	With(NoLock)	On	b.nmro_cntrto					= c.nmro_cntrto
																			And	b.cnsctvo_cdgo_tpo_cntrto	= c.cnsctvo_cdgo_tpo_cntrto
	Inner Join	bdAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador	v	With(NoLock)	On	v.cnsctvo_cdgo_tpo_cntrto		=	b.cnsctvo_cdgo_tpo_cntrto
																			And	v.nmro_cntrto				=	b.nmro_cntrto
																			And	v.cnsctvo_bnfcro			=	b.cnsctvo_bnfcro
	Inner Join  bdAfiliacionValidador.dbo.tbPlanes_Vigencias p With(NoLock)	On          p.cnsctvo_cdgo_pln          =  c.cnsctvo_cdgo_pln
	Inner Join bdAfiliacionValidador.dbo.tbTiposPlan tp 	With(NoLock) On		   tp.cnsctvo_cdgo_tpo_pln = p.cnsctvo_cdgo_tpo_pln
	Where		b.nmro_unco_idntfccn_afldo	= @Nui_Afldo
	And			@getDate_ Between v.inco_vgnca_estdo_bnfcro And v.fn_vgnca_estdo_bnfcro
	And         @getDate_ Between p.inco_vgnca And p.fn_vgnca
	And			c.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln
	
END
	
GO
