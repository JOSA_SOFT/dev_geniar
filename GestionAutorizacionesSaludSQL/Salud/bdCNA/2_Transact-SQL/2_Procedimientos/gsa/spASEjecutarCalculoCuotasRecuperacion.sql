USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarCalculoCuotasRecuperacion]    Script Date: 8/25/2017 8:54:13 AM ******/
SET ANSI_NULLS On
GO
SET QUOTED_IDENTIFIER On
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarCalculoCuotasRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos V�squez Garc�a							  A\>  
* Descripcion		: <\D
                           Controlador Para ejecuci�n Masiva o por Demanda el C�lculo 
                           Cuotas de Recuperaci�n (Copagos-Cuota Moderadora)	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/25FC\>  																										
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Victor Hugo Gil Ramos AM\>  													
* Descripcion			: <\DM 
                               Se actualiza el procedimiento cambiando el llamado sp que crea el proceso 
							   spPRORegistraLogEventoProceso por spRegistraLogEventoProceso 
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM 
                                Se agrega el campo cnsctvo_cdgo_tcnca_lqdcn a la temporal #TempServicioSolicitudes
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/26 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM 
                                Se agregan los campos cnsctvo_dtlle_mdlo_tps_cpgo,	cnsctvo_mdlo y cnsctvo_cdgo_tcnca_lqdcn
								en la tabla temporal #TempServicioSolicitudes.
								Se agregan los campos cnsctvo_cdgo_tcnca_lqdcn, cnsctvo_dtlle_mdlo_tcncs_bnfccn, nmro_actvdds 
								y cnsctvo_cdgo_itm_bnfccn en la tabla temporal #tempTbAsConsolidadoCuotaRecuperacionxOps.
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/06 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM 
                                Se agrega el campo cnsctvo_cdgo_tpo_mrca en la tabla temporal #TempServicioSolicitudes.
								
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/11 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM 
                                Se quitan los valores por defecto de los campos cnsctvo_cdgo_csa_no_cbro_cta_rcprcn	y 
								dscrpcn_csa_csa_no_cbro_cta_rcprcn de la tabla temporal #TempServicioSolicitudes.								
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/07 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez AM\>  													
* Descripcion			: <\DM 
                                Se agrega el campo cnsctvo_cdgo_tpo_orgn_slctd a la tabla temporal 	#TempSolicitudes							
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/27 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

/* 
Declare @mnsjertrno					 UdtDescripcion ,
        @cdgortrno					 varchar(3) 
--Exec bdcna.gsa.spASEjecutarCalculoCuotasRecuperacion null, null, @mnsjertrno output ,@cdgortrno output
Exec bdcna.gsa.spASEjecutarCalculoCuotasRecuperacion 29534,'autsalud_webusr', @mnsjertrno output, @cdgortrno output

Select @cdgortrno + ' ' + @mnsjertrno


*/

ALTER PROCEDURE [gsa].[spASEjecutarCalculoCuotasRecuperacion]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null,
/*02*/  @lcUsrioAplcn				 UdtUsuario = null,
/*03*/  @mnsjertrno					 UdtDescripcion = null output,
/*04*/  @cdgortrno					 varchar(2) = null output

AS

Begin
	SET NOCOUNT On

	Declare @lcUsuario				         UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@tpo_prcso				         Int = 28, -- 'Proceso Autom�tico C�lculo Cuotas de Recuperaci�n Masivo - MEGA'
			@cnsctvo_prcso			         Int = 0,
			@cnsctvo_cdgo_tpo_incnsstnca     Int = 1,
			@prcso_extso			         Int = 2,
			@codigoExito			         Char(2) = 0,
			@codigoError			         Char(2) = -1,
			@codigoSalir			         Char(2) = -2,
			@estdo_pso1				         Int = 0,  /*Estado paso 1 */
			@estdo_pso2				         Int = 0,  /*Estado paso 2 */
			@estdo_pso3				         Int = 0,  /*Estado paso 3 */
			@tpo_rgstro_log_95		         Int = 95,
			@tpo_rgstro_log_96		         Int = 96,
			@tpo_rgstro_log_97		         Int = 97,
			@mensajeError			         Varchar(2000) = '',
			@cnsctvo_lg				         Int = 0,
			@cnsctvo_estdo_err		         udtConsecutivo = 3,
			@mnsje_lg_evnto_prcso	         Varchar(500),
			@cnsctvo_cdgo_estdo_slctd9       UdtConsecutivo = 9,  -- 'Estado Servicio Solicitud '9-Liquidada'
			@ValorCERO				         Int = 0,
			@ValorUNO				         Int = 1,
			@fechaactual			         Datetime,  -- fecha actual
			@fechacrtelqdccn		         Datetime, -- fecha de corte liquidaci�n
	        @estdo_en_prcso                  Int     ,
			@cnsctvo_rgstro_lg_evnto_x_prcso udtConsecutivo
	
  Set @fechaactual = getDate()
  Set @fechacrtelqdccn = getDate()
  Set @fechacrtelqdccn += @ValorUNO
  Set @fechacrtelqdccn = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))
  Set @estdo_en_prcso  = 1
	 
	 -- Validamos si el proceso debe hacerce por demanda � masivo
  If	@cnsctvo_slctd_atrzcn_srvco is not null -- es por demanda
		Begin
			-- Inicializamos el corte de liquidaci�n a la fecha actual porque se solicito por demanda
			Set	@fechacrtelqdccn = @fechaactual
			Set @lcUsuario = @lcUsrioAplcn

			-- Validamos si existen servicios por calcular cuotas de recuperaci�n 
			if not exists ( select	1 
							from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
							inner join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
							On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
							inner join	gsa.tbASServiciosSolicitados c with(nolock)
							On			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
							where		a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
							And			c.vlr_lqdcn_srvco > @ValorCERO
							And			c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9)-- Liquidada
				Begin
					Set @mnsjertrno = 'No hay Datos Para Procesar Cuotas de Recuperaci�n Se Cancela El Proceso'
					Set @cdgortrno = 'OK'
					Return
				End
		End

	Else  -- es masivo
		Begin
			if not exists ( select	1 
							from		gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
							inner join	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b with(nolock)
							On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
							inner join	gsa.tbASServiciosSolicitados c with(nolock)
							On			c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
							inner join  gsa.tbASResultadoFechaEntrega d with(nolock)
							On			d.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
							where		@fechacrtelqdccn  >= isnull(d.fcha_rl_entrga,d.fcha_estmda_entrga) 
							And			c.vlr_lqdcn_srvco > @ValorCERO
							And			c.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_slctd9) -- Liquidada
				Begin
					Set @mnsjertrno = 'No hay Datos Para Procesar Cuotas de Recuperaci�n Se Cancela El Proceso'
					Set @cdgortrno = 'OK'
					Return
				End

		End

	-- Creamos tabla temporal para insertar los registros de solicitudes a procesar con el c�lculo cuotas de recuperaci�n
	Create Table #TempSolicitudes
	(
		id_tbla							Int identity,
		cnsctvo_tpo_prcso				UdtConsecutivo default 28,
		cnsctvo_prcso					UdtConsecutivo default 0, 
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, 
		nmro_slctd_atrzcn_ss			Varchar(22), 
		fcha_slctd						Datetime, 
		fcha_ncmnto						Datetime,
		nmro_unco_idntfccn_afldo		UdtConsecutivo,
		cnsctvo_cdgo_cdd_rsdnca_afldo	UdtConsecutivo,
		cnsctvo_cdgo_tpo_pln			UdtConsecutivo default 1,
		cnsctvo_cdgo_pln				UdtConsecutivo,
		cnsctvo_cdgo_rngo_slrl			UdtConsecutivo default 0,
		cnsctvo_cdgo_tpo_cntrto			UdtConsecutivo,
		nmro_cntrto						UdtConsecutivo,
		cnsctvo_bnfcro					UdtConsecutivo,
		cnsctvo_cdgo_chrte				UdtConsecutivo, 
		edd_afldo_ans					UdtConsecutivo,
		cnsctvo_cdgo_cnvno_cmrcl		UdtConsecutivo default 1,  
		cnsctvo_cdgo_tpo_dscpcdd		UdtConsecutivo default 0, 
		cnsctvo_cdgo_grpo_pblcnl		UdtConsecutivo default 0, 
		cnsctvo_cdgo_tpo_afldo			UdtConsecutivo default 0,
		nmro_unco_aprtnte				UdtConsecutivo,
		cnsctvo_cdgo_frma_atncn			UdtConsecutivo,
		fcha_entrga						datetime,
		cnsctvo_cdgo_tpo_orgn_slctd		UdtConsecutivo
	);	

	-- se crea tabla temporal para cargar las notificaciones del afiliado.
	Create Table #tmpNotificacionesAfiliado (
			nmro_unco_idntfccn_afldo			udtConsecutivo,
			cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
			cnsctvo_cdgo_clsfccn_evnto_ntfccn	udtConsecutivo,
			cnsctvo_cdgo_ptlga_ctstrfca			udtConsecutivo,
			dscrpcn_estdo_ntfccn				udtDescripcion
	)

	
	Create Table #tmpDiagnosticos (
		cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
		cnsctvo_cdgo_dgnstco				udtConsecutivo,
		cnsctvo_cdgo_tpo_mrca_dgnstco		udtConsecutivo
	)

	
	-- Creamos Tabla temporal para insertar los registros de prestaciones con el c�lculo cuotas de recuperaci�n
	Create Table #TempServicioSolicitudes
	(
		id_tbla								int identity,
		cnsctvo_tpo_prcso					udtConsecutivo default 28,
		cnsctvo_prcso						udtConsecutivo default 0,
		cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
		cnsctvo_srvco_slctdo                udtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cdgo_tpo_srvco				udtConsecutivo, 
		cntdd_slctda						Int, 
		vlr_lqdcn_srvco						float default 0,
		vlr_tcnca_lqdcn						float default 0,
		vlr_tcnca_lqdcn_neto	            float default 0,
		vlr_lqdcn_cta_rcprcn_srvco			float default 0,
		cnsctvo_cdgo_prstcn_prstdr			udtConsecutivo,				-- Para determibnar si la c recup la hace el Prstdr directamente o sos 
		cnsctvo_dtlle_mdlo_tcncs_bnfccn		udtConsecutivo default 0,	-- Para identificar el detalle del parametro de la tecnica de liquidacion con la que se liquida el servicio
		cnsctvo_cdgo_itm_bnfccn				udtConsecutivo default 0,
		cnsctvo_cdgo_cncpto_pgo				udtConsecutivo default 0,   -- Consecutivo concepto pago (1-Cuotas Moderadoras, 2-Copago)
		cbro_cta_rcprcn_eac					Char(1)	default '',
		cdgo_intrno_prtsdr_dstno			udtCodigoIps default '',	-- Ips que presta el servicio 
		prtsdr_dstno_rcda_cta_rcprcn		udtLogico default 'N',		-- El Prestador Recauda Cuota de Recuperaci�n (S/N)
		fcha_entrga_estmda					Datetime, 
		nmro_actvdds						Int default 0,
		vlr_tpe_evnto_prmtro				float default 0,
		vlr_tpe_a�o_prmtro					float default 0,
		gnra_cbro							udtLogico default 'S',		-- Servicio genera cobro -- S/N
		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn	Int, 
		dscrpcn_csa_csa_no_cbro_cta_rcprcn	udtDescripcion,	-- Causa no cobro servicio - Cuando no se debe cobrar cuota de recuperaci�n por el servicio
		cnsctvo_cdgo_estdo_cta_rcprcn		udtConsecutivo default 0,
		nmro_unco_ops						udtConsecutivo default 0,
		nmro_unco_prncpl_cta_rcprcn			udtLogico	default 'N',
		cnsctvo_dtlle_mdlo_tps_cpgo			udtConsecutivo,
		cnsctvo_mdlo						udtConsecutivo,
		cnsctvo_cdgo_tcnca_lqdcn			udtConsecutivo,
		cnsctvo_cdgo_tpo_mrca				udtConsecutivo,
		cnsctvo_cdgo_agrpdr_prstcn			udtConsecutivo
	);
	
	-- Creamos temporal para insertar el detalle resultante del c�lculo cuotas de recuperaci�n
	create table #tempTbAsDetCalculoCuotaRecuperacion 
	(
		id_tbla									int identity,
		cnsctvo_rgstro_dt_cta_rcprcn			UdtConsecutivo,
		cnsctvo_tpo_prcso						UdtConsecutivo default 28,
		cnsctvo_prcso 	                        udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
		cnsctvo_srvco_slctdo	                udtConsecutivo,
		cnsctvo_dtlle_mdlo_tcncs_bnfccn			udtConsecutivo,
		cnsctvo_mdlo	                        udtConsecutivo,
		cntdd_slctda        	                Int,
		cnsctvo_cdgo_cncpto_pgo	                udtConsecutivo,
		cdgo_intrno_prtsdr_cbro	                UdtCodigoIps,
		vlr_lqdcn_srvco							float default 0,
		vlr_tcnca_lqdcn                         Float default 0,
		vlr_tcnca_lqdcn_neto	                Float default 0,
		vlr_lqdcn_cta_rcprcn_srvco	            Float default 0,
		gnra_cbro	                            UdtLogico,
		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn		udtConsecutivo,
		cnsctvo_cdgo_estdo_cta_rcprcn	        udtConsecutivo,
		nmro_unco_ops							UdtConsecutivo default 0,
		nmro_unco_idntfccn_afldo                udtConsecutivo,  -- se requiere para realizar el calculo x plan, mas no se registra en la tb final ya que este campo esta en la informacio de la solicitud.
		cnsctvo_cdgo_pln                        udtConsecutivo -- se requiere para realizar el calculo x plan, mas no se registra en la tb final ya qeue ste campo esta en la informacio de la solicitud.
	);

	--Creamos temporal para insertar  consolidado del c�lculo cuotas de recuperaci�n
	Create table #tempTbAsCalculoConsolidadoCuotaRecuperacion		
	( 
		id_tbla									int identity,
		cnsctvo_tpo_prcso						UdtConsecutivo default 28,
		cnsctvo_prcso							udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco				udtConsecutivo,
		nmro_unco_idntfccn_afldo				udtConsecutivo,
		fcha_entrga								datetime,
		cnsctvo_cdgo_estdo_cta_rcprcn			udtConsecutivo,
		cnsctvo_cdgo_cncpto_pgo					udtConsecutivo,
		vlr_lqdcn_cta_rcprcn_ttl				Float default 0,
		vlr_lqdcn_cta_rcprcn_ttl_fnl			Float default 0,
		cnsctvo_cdgo_pln						udtConsecutivo
	);
	
	-- Creamos temporal para validar el manejo de topes de copagos x afiliado-solicitud
	Create table #tempTbAsDetalleTopesCopago
	(
		id_tbla									int identity,
		cnsctvo_dtlle_mdlo_tps_cpgo				UdtConsecutivo default 0,
		cnsctvo_mdlo							UdtConsecutivo default 0,
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn UdtConsecutivo default 0,
		cnsctvo_slctd_atrzcn_srvco				UdtConsecutivo,
		nmro_unco_idntfccn_afldo				UdtConsecutivo,
		vlr_lqdcn_cta_rcprcn_ttl				float default 0,
		sldo_antrr_dspnble_evnto				float default 0,
		sldo_antrr_dspnble_ano					float default 0,
		cnsctvo_cdgo_estdo_cta_rcprcn			UdtConsecutivo,
		vlr_lqdcn_cta_rcprcn_fnl				float default 0,
		sldo_dspnble_evnto						float default 0,
		sldo_dspnble_ano						float default 0,
		gnra_cbro								UdtLogico default 'N',
		cnsctvo_rgstro_dtlle_tpe_cpgo_orgn		UdtConsecutivo,
		cnsctvo_cdgo_tpo_trnsccn				UdtConsecutivo default 1
	);

	--Creamos temporal para insertar  consolidado del c�lculo cuotas de recuperaci�n
	Create table #tempTbAsConsolidadoCuotaRecuperacionxOps		
	( 
		id_tbla									int identity,
		cnsctvo_slctd_atrzcn_srvco				udtConsecutivo,
		nmro_unco_ops							udtConsecutivo,
		cnsctvo_cdgo_cncpto_pgo					udtConsecutivo,
		cnsctvo_cdgo_estdo_cta_rcprcn			udtConsecutivo default 1,
		vlr_lqdcn_cta_rcprcn_srvco_ops			Float default 0,
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn	udtConsecutivo,
		cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn    udtConsecutivo default 1,
		prtsdr_dstno_rcda_cta_rcprcn			UdtLogico,
		nmro_unco_prncpl_cta_rcprcn				UdtLogico,
		cnsctvo_cdgo_tcnca_lqdcn				UdtConsecutivo,
		cnsctvo_dtlle_mdlo_tcncs_bnfccn			UdtConsecutivo,
		cnsctvo_cdgo_itm_bnfccn					UdtConsecutivo,
		nmro_actvdds							Int
	);
	
	if @cnsctvo_slctd_atrzcn_srvco is null  -- si el proceso es masivo se activa el control de transacciones
		Begin
			Begin Try
				-- Registramos el inicio proceso autom�tico log de procesos 'Proceso Autom�tico C�lculo Cuotas de Recuperaci�n Masivo - MEGA'
				Exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output

				Begin Transaction calculo_cuotasrecuperacion
				-- Paso 1. Poblar temporales 
				Begin Try
				    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 1

					set @mnsje_lg_evnto_prcso = 'Paso 1. Poblar a tablas TMP Para El Calculo Cuotas de Recuperacion - MEGA'
					Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_95, @estdo_en_prcso, @cnsctvo_lg output
					Exec gsa.spASPoblarTemporalesCalculoCuotasDeRecuperacion @cnsctvo_prcso, @cnsctvo_slctd_atrzcn_srvco
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
				End Try
				Begin Catch
					-- error en paso 1
						select	@estdo_pso1 = @codigoError,
								@estdo_pso2 = @codigoSalir,
								@estdo_pso3 = @codigoSalir;
					throw
				End Catch
				-- Paso 2. C�lculo Cuotas de Recuperaci�n 
				Begin Try
				    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 2
					set @mnsje_lg_evnto_prcso = 'Paso 2. C�lculo Cuotas de Recuperaci�n - MEGA'
					Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_96, @estdo_en_prcso, @cnsctvo_lg output
					Exec gsa.spASCalcularCuotasDeRecuperacion
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					-- Registramos el fin del proceso Autom�tico log de procesos 'Proceso Autom�tico C�lculo Cuotas de Recuperaci�n Masivo - MEGA'
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
				End Try
				Begin Catch
					-- error en paso 2
						select	@estdo_pso2 = @codigoError,
								@estdo_pso3 = @codigoSalir;
					throw
				End Catch

				-- Paso 3. Grabar Tablas Definitivas C�lculo Cuotas de Recuperaci�n
				Begin Try
				    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 3
					set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar a Tablas Definitivas el C�lculo Cuotas de Recuperaci�n - MEGA'
					Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_97, @estdo_en_prcso, @cnsctvo_lg output
					Exec gsa.spASGrabarCalculoCuotasDeRecuperacion @lcUsuario
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg				 
					-- Crea las marcas de las prestaciones
					Exec gsa.spAsMarcaPrestaciones @lcUsuario

				End Try
				Begin Catch
					-- error en paso 3
						select	@estdo_pso3 = @codigoError;
					throw
				End Catch

				-- Registramos el fin del proceso Autom�tico log de procesos 'Proceso Autom�tico Calculo Cuotas de Recuperaci�n - MEGA'
				Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
				Set @mnsjertrno = 'El Proceso de C�lculo Cuotas de Recuperaci�n Se Ejecuto Con Exito'
				Set @cdgortrno = 'OK'
				commit transaction calculo_cuotasrecuperacion
		End Try
		Begin Catch
		    -- El proceso se ejecuto con error
			Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			RollBack transaction calculo_cuotasrecuperacion

			-- Paso 1
			Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 1
			set @mnsje_lg_evnto_prcso = 'Paso 1. Poblar a tablas TMP Para El Calculo Cuotas de Recuperacion - MEGA'
			Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_95, @estdo_en_prcso, @cnsctvo_lg output
			
			if @estdo_pso1 = @codigoExito
				Begin
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
				End
			if @estdo_pso1 = @codigoError
				Begin
					Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
					Set @mnsjertrno = 'Se Presento un Error al Poblar TEMPORALES Cuotas de Recuperaci�n'
					Set @cdgortrno = 'ET'
					return
				End

			 -- Paso 2
			Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 2
			set @mnsje_lg_evnto_prcso = 'Paso 2. C�lculo Cuotas de Recuperaci�n - MEGA'
			Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_96, @estdo_en_prcso, @cnsctvo_lg output
			
			
			if @estdo_pso2 = @codigoExito
				Begin
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
				End
			if @estdo_pso2 = @codigoError
				Begin
					Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
					Set @mnsjertrno = 'Se Presento un Error en el C�lculo Cuotas de Recuperaci�n'
					Set @cdgortrno = 'ET'
					return
				End
			-- Paso 3
			Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 3
			set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar a Tablas Definitivas el C�lculo Cuotas de Recuperaci�n - MEGA'
			Exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_97, @estdo_en_prcso, @cnsctvo_lg output
			
			if @estdo_pso3 = @codigoExito
				Begin
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
				End
			if @estdo_pso3 = @codigoError
				Begin
					Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
					Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
					Set @mnsjertrno = 'Se Presento un Error Grabando a tablas Definitivas Cuotas de Recuperaci�n'
					Set @cdgortrno = 'ET'
					return
				End
		End Catch
		End

		Else
			Begin
				-- Registramos el inicio proceso autom�tico log de procesos 'Proceso Autom�tico C�lculo Cuotas de Recuperaci�n Masivo - MEGA'
				Exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output
				-- Paso 1. Poblar temporales 
				Exec gsa.spASPoblarTemporalesCalculoCuotasDeRecuperacion @cnsctvo_prcso, @cnsctvo_slctd_atrzcn_srvco
				-- Paso 2. C�lculo Cuotas de Recuperaci�n
				Exec gsa.spASCalcularCuotasDeRecuperacion
				-- Paso 3. Insertar Tablas Definitivas C�lculo Cuotas de Recuperaci�n
				Exec gsa.spASGrabarCalculoCuotasDeRecuperacion @lcUsrioAplcn
				-- Crea las marcas de las prestaciones
				Exec gsa.spAsMarcaPrestaciones @lcUsrioAplcn

				if @@error = 0
					Begin
						-- Registramos el fin del proceso Autom�tico log de procesos 'Proceso Autom�tico Calculo Cuotas de Recuperaci�n - MEGA'
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
						Set @mnsjertrno = 'El Proceso de C�lculo Cuotas de Recuperaci�n Se Ejecuto Con Exito'
						Set @cdgortrno = 'OK'
					End
				Else
					Begin
						-- El proceso se ejecuto con error
						Set @mnsjertrno = 'Se Presento un Error en el C�lculo Cuotas de Recuperaci�n'
						Set @cdgortrno = 'ET'
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR
					End
			End

	Drop table #TempSolicitudes
	Drop table #TempServicioSolicitudes
	Drop table #tempTbAsDetCalculoCuotaRecuperacion
	Drop table #tempTbAsCalculoConsolidadoCuotaRecuperacion
	Drop table #tempTbAsDetalleTopesCopago 
	Drop table #tempTbAsConsolidadoCuotaRecuperacionxOps
End
