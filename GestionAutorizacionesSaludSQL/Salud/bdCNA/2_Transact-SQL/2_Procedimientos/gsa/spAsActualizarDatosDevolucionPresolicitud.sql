USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsActualizarDatosDevolucionPresolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spAsActualizarDatosDevolucionPresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spAsActualizarDatosDevolucionPresolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spAsActualizarDatosDevolucionPresolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Sp que Actualiza la información de la presolicitud D\>
						  <\D para que permita llevar una traza del registro     D\>
						  <\D desde el BPM										 D\>
						  <\D					                                 D\>
* Observaciones			: <\O  								  					 O\>
* Parametros			: <\P \>
* Variables				: <\V  													 V\>
* Fecha Creacion		: <\FC 2016/07/08 										 FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spAsActualizarDatosDevolucionPresolicitud] 
@cnsctvo_prslctd			udtConsecutivo,
@cnsctvo_cdgo_csa_nvdd		udtConsecutivo,
@usro_gstn					udtUsuario,
@jstfccn					udtObservacion,
@cdgo_rsltdo				udtConsecutivo Output,
@mnsje_rsltdo				varchar(2000) Output
AS
BEGIN
	SET NOCOUNT ON;
	Declare @codigo_ok									int,
			@codigo_error								int,
			@mensaje_ok									udtDescripcion,
			@mensaje_error								varchar(2000),
			@fecha_validacion							datetime;
			

	Set @codigo_error					 = -1;  -- Error SP
	Set @codigo_ok						 =  0;  -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente';
	Set @fecha_validacion				 = GetDate();
	

	BEGIN TRY
		Update da Set 
			cnsctvo_cdgo_csa_nvdd			= @cnsctvo_cdgo_csa_nvdd,
			usro_dvlcn						= @usro_gstn,
			jstfccn							= @jstfccn,
			fcha_dvlcn						= @fecha_validacion,
			fcha_ultma_mdfccn				= @fecha_validacion,
			usro_ultma_mdfccn				= @usro_gstn
		From BDCna.gsa.tbASDatosAdicionalesPreSolicitud da With(NoLock)
		Where cnsctvo_prslctd = @cnsctvo_prslctd

		Set @cdgo_rsltdo	=		@codigo_ok
		Set @mnsje_rsltdo	=		@mensaje_ok

	END TRY
	BEGIN CATCH
		SET @mensaje_error =  Concat(
									'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
									'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
									'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
									'Procedure:' , ERROR_PROCEDURE()
								);
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

END
GO
