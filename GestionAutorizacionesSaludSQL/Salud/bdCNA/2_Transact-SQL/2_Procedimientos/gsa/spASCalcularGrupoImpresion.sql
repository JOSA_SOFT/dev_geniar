USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCalcularGrupoImpresion]    Script Date: 01/08/2017 02:16:31 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASCalcularGrupoImpresion
* Desarrollado por		: <\A   					 A\>
* Descripcion			: <\D Este sp calcula el grupo de impresion de un conjunto de prestaciones
                              registradas en la tabla temporal #tbConceptoGastosTmp_1.

                              El resultado del calculo del grupo de impresion se actualiza 
							  en el campo cnsctvo_cdgo_grpo_imprsn.

                              Este sp tambien actualiza el campo cnsctvo_cdgo_frma_lqdcn_prstcn con la forma de liquidacion
                              utilizada para calcular el grupo de impresion.
                          D\>
* Observaciones			: <\O 
                              Para la implementacion de este sp, se tomo como referencia 
                              [BDSisalud].[spPMInsertaConceptosOps] 
						  O\>
* Parametros			: <\P 
                            Se requiere de la tabla temporal #tbConceptoGastosTmp_1 
                            para la ejecucion de este sp y los siguientes campos:
                            1. Para la prestacion:  cnsctvo_cdgo_cps
                            2. Para el prestador:   cdgo_intrno_prstdr
                            3. Para el plan:        cnsctvo_cdgo_pln
                            4. Concepto gasto:      cnsctvo_cdgo_cncpto_gsto
                          P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2016/02/02 FC\>
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Felipe Arcos Velez AM\>
* Descripcion			 : <\DM	Ajuste para filtrar los registros utilizando forma de atencion en vez de tipo de atencion DM\>
* Nuevos Parametros	 	 : <\PM	cnsctvo_cdgo_frma_atncn de la tabla  #tbConceptoGastosTmp_1 PM\>
* Nuevas Variables		 : <\VM	ninguna VM\>
* Fecha Modificacion	 : <\FM	08/Jun/2016 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Luis Fernando Benavides AM\>
* Descripcion			 : <\DM	Se adiciona por defecto el grupo de impresion "INSUMOS" a los CUOS DM\>
* Nuevos Parametros	 	 : <\PM	 PM\>
* Nuevas Variables		 : <\VM	 VM\>
* Fecha Modificacion	 : <\FM	2016/09/09 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                               Se modifica el procedimiento para que calcule el grupo de impresion correspondiente a los 
							   paquetes
                            DM\>
* Nuevos Parametros	 	 : <\PM	 PM\>
* Nuevas Variables		 : <\VM	 VM\>
* Fecha Modificacion	 : <\FM	2017/06/09 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing Carlos Andres Lopez Ramirez AM\>
* Descripcion			 : <\DM	
                               Se crean tablas temporales, #tempInformacionGruposImpresion y #tempInformacionGruposImpresionPaquete, 
							   para cargar datos en comun de las distintas consultas y reduccir el acceso a las tablas reales. 
                            DM\>
* Nuevos Parametros	 	 : <\PM	 PM\>
* Nuevas Variables		 : <\VM	 VM\>
* Fecha Modificacion	 : <\FM	2017/06/23 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing Carlos Andres Lopez Ramirez AM\>
* Descripcion			 : <\DM	
                               Se modifica procedimiento, se quita condicional if y se condicionan las consultas para que a las 
							   prestaciones no liquidadas como paquete que liquiden por modelo de conceptos seles calcule el 
							   grupo de impresion correcto.
                            DM\>
* Nuevos Parametros	 	 : <\PM	 PM\>
* Nuevas Variables		 : <\VM	 VM\>
* Fecha Modificacion	 : <\FM	2017/08/02 FM\>
*---------------------------------------------------------------------------------------------------------------------------------------*/

/*
 Exec bdcna.gsa.spASCalcularGrupoImpresion '20160701'
*/

ALTER PROCEDURE [gsa].[spASCalcularGrupoImpresion] 
    @fechaLiquidacion		  DateTime
AS
BEGIN
		SET NOCOUNT ON;
			     
		Declare @valor_cero                     Int           ,
				@cnsctvo_cdgo_frma_lqdcn_prstcn udtConsecutivo,
				@cnsctvo_cdgo_tpo_cdfccn_cms    udtConsecutivo,
				@cnsctvo_cdgo_tpo_cdfccn_cs     udtConsecutivo,
				@cnsctvo_cdgo_grpo_imprsn_cms   udtConsecutivo,
				@cnsctvo_cdgo_grpo_imprsn_cs    udtConsecutivo,
				@cntdd_rgstro                   Int;     

		-- 
		Create 
		Table  #tempPaquete(cdgo_intrno	                   udtCodigoIps  ,
							cnsctvo_cdgo_pln               udtconsecutivo,
							cnsctvo_cdfccn_b	           udtconsecutivo,
							cnsctvo_cdgo_frma_atncn		   udtconsecutivo,
							cnsctvo_cdgo_frma_lqdcn_prstcn udtconsecutivo,
							cnsctvo_cdgo_grpo_imprsn       udtconsecutivo,
							cnsctvo_pqte                   udtconsecutivo,
							cnsctvo_cdgo_cncpto_gsto	   udtconsecutivo
							)
		
		-- 
		Create 
		Table  #temp1(cdgo_intrno					 udtCodigoIps  ,
						cnsctvo_cdgo_pln				 udtconsecutivo,
						cnsctvo_cdfccn_b				 udtconsecutivo,
						cnsctvo_cdgo_frma_atncn		 udtconsecutivo,
						cnsctvo_cdgo_frma_lqdcn_prstcn udtconsecutivo,
						cnsctvo_cdgo_grpo_imprsn		 udtconsecutivo
						)
		
		-- 
		Create 
		Table  #temp2(cdgo_intrno					 udtCodigoIps  ,
						cnsctvo_cdgo_pln				 udtconsecutivo,
						cnsctvo_cdfccn_b				 udtconsecutivo,
						cnsctvo_cdgo_frma_atncn		 udtconsecutivo,
						cnsctvo_cdgo_frma_lqdcn_prstcn udtconsecutivo,
						cnsctvo_cdgo_grpo_imprsn		 udtconsecutivo,
						cnsctvo_cdgo_cncpto_gsto		 udtconsecutivo
						)

		-- 
		Create Table #tempInformacionGruposImpresion
		(
			cnsctvo_cdgo_grpo_imprsn			udtConsecutivo,
			cnsctvo_cdgo_frma_lqdcn_prstcn		udtConsecutivo,
			cdgo_intrno							udtCodigoIps,
			cnsctvo_cdgo_pln					udtConsecutivo,
			cnsctvo_cdfccn_b					udtConsecutivo, 
			cnsctvo_cdgo_cncpto_gsto			udtConsecutivo,
			cnsctvo_cdgo_mdlo_cnvno_cncpto		udtConsecutivo,	
			cnsctvo_det_eqvlnca					udtConsecutivo
		)

		-- 
		Create Table #tempInformacionGruposImpresionPaquete
		(
			cnsctvo_cdgo_grpo_imprsn			udtConsecutivo,
			cnsctvo_cdgo_frma_lqdcn_prstcn		udtConsecutivo,
			cdgo_intrno							udtCodigoIps,
			cnsctvo_cdgo_pln					udtConsecutivo,
			cnsctvo_cdfccn_b					udtConsecutivo, 
			cnsctvo_pqte						udtConsecutivo,			
			cnsctvo_cdgo_cncpto_gsto			udtConsecutivo,
			cnsctvo_cdgo_mdlo_cnvno_cncpto		udtConsecutivo,	
			cnsctvo_det_eqvlnca					udtConsecutivo
		)

		-- 
		Set @valor_cero                     = 0;
		Set @cnsctvo_cdgo_frma_lqdcn_prstcn = 2;
		Set @cnsctvo_cdgo_tpo_cdfccn_cms    = 5; --CUMS
		Set @cnsctvo_cdgo_tpo_cdfccn_cs     = 9; --CUOS
		Set @cnsctvo_cdgo_grpo_imprsn_cms   = 35;  --GrupoImpresion CUMS
		Set @cnsctvo_cdgo_grpo_imprsn_cs    = 47;  --GrupoImpresion CUOS
		
		-- 
		Insert Into #tempInformacionGruposImpresion
		(
					cnsctvo_cdgo_grpo_imprsn  ,	cnsctvo_cdgo_frma_lqdcn_prstcn, cdgo_intrno         ,
					cnsctvo_cdgo_pln          ,	cnsctvo_cdfccn_b              , cnsctvo_cdgo_cncpto_gsto  ,	
					cnsctvo_cdgo_mdlo_cnvno_cncpto, cnsctvo_det_eqvlnca
		)
		Select Distinct d.cnsctvo_cdgo_grpo_imprsn  ,	d.cnsctvo_cdgo_frma_lqdcn_prstcn, b.cdgo_intrno             ,
					b.cnsctvo_cdgo_pln          ,	e.cnsctvo_cdfccn_b              , slo.cnsctvo_cdgo_cncpto_gsto,	
					d.cnsctvo_cdgo_mdlo_cnvno_cncpto, d.cnsctvo_det_eqvlnca--, d.*
		From		#tbConceptoGastosTmp_1 slo
		Inner Join	bdSiSalud.dbo.tbAsociacionModeloActividad b With(NoLock)
		On			b.cdgo_intrno		= slo.cdgo_intrno_prstdr -- /*prestador*/
		Inner Join	bdContratacion.dbo.tbModeloConveniosPrestacionesXPlan c With(NoLock)
		On			c.cnsctvo_mdlo_cnvno_prstcn_pln = b.cnsctvo_mdlo_cnvno_pln  
		And			c.cnsctvo_cdgo_pln              = b.cnsctvo_cdgo_pln
		And			b.cnsctvo_cdgo_pln = slo.cnsctvo_cdgo_pln
		Inner Join	bdContratacion.dbo.tbDetModeloConveniosPrestaciones d With(NoLock)	
		On			d.cnsctvo_cdgo_mdlo_cnvno_prstcn = c.cnsctvo_cdgo_mdlo_cnvno_prstcn 
		Inner Join	bdsisalud.dbo.tbDetEquivalencias e	With(NoLock)					
		On			e.cnsctvo_det_eqvlnca = d.cnsctvo_det_eqvlnca  
		And			e.cnsctvo_cdfccn_b    = slo.cnsctvo_cdgo_cps
		Where		@fechaLiquidacion Between b.inco_vgnca  And b.fn_vgnca
		And			@fechaLiquidacion Between c.inco_vgnca  And c.fn_vgnca
		And			@fechaLiquidacion Between d.inco_vgnca  And d.fn_vgnca
		And			@fechaLiquidacion Between e.inco_vgnca  And e.fn_vgnca
		And			slo.cnsctvo_pqte = @valor_cero
						
		-- 
		Insert Into #tempInformacionGruposImpresionPaquete
		(
					cnsctvo_cdgo_grpo_imprsn  ,	cnsctvo_cdgo_frma_lqdcn_prstcn, cdgo_intrno         ,
					cnsctvo_cdgo_pln          ,	cnsctvo_cdfccn_b              , cnsctvo_pqte        ,			
					cnsctvo_cdgo_cncpto_gsto  ,	cnsctvo_cdgo_mdlo_cnvno_cncpto, cnsctvo_det_eqvlnca
		)
		Select Distinct d.cnsctvo_cdgo_grpo_imprsn  ,	d.cnsctvo_cdgo_frma_lqdcn_prstcn, b.cdgo_intrno             ,
					b.cnsctvo_cdgo_pln          ,	e.cnsctvo_cdfccn_b              , slo.cnsctvo_pqte          ,			
					slo.cnsctvo_cdgo_cncpto_gsto,	d.cnsctvo_cdgo_mdlo_cnvno_cncpto, d.cnsctvo_det_eqvlnca
		From		#tbConceptoGastosTmp_1 slo
		Inner Join	bdSiSalud.dbo.tbAsociacionModeloActividad b With(NoLock)
		On			b.cdgo_intrno		= slo.cdgo_intrno_prstdr -- /*prestador*/
		Inner Join	bdContratacion.dbo.tbModeloConveniosPrestacionesXPlan c With(NoLock)
		On			c.cnsctvo_mdlo_cnvno_prstcn_pln = b.cnsctvo_mdlo_cnvno_pln  And
					c.cnsctvo_cdgo_pln              = b.cnsctvo_cdgo_pln
		Inner Join	bdContratacion.dbo.tbDetModeloConveniosPrestaciones d With(NoLock)	
		On			d.cnsctvo_cdgo_mdlo_cnvno_prstcn = c.cnsctvo_cdgo_mdlo_cnvno_prstcn 
		Inner Join	bdsisalud.dbo.tbDetEquivalencias e	With(NoLock)					
		On			e.cnsctvo_det_eqvlnca = d.cnsctvo_det_eqvlnca  
		And			e.cnsctvo_cdfccn_b    = slo.cnsctvo_pqte
		Where		@fechaLiquidacion Between b.inco_vgnca  And b.fn_vgnca
		And			@fechaLiquidacion Between c.inco_vgnca  And c.fn_vgnca
		And			@fechaLiquidacion Between d.inco_vgnca  And d.fn_vgnca
		And			@fechaLiquidacion Between e.inco_vgnca  And e.fn_vgnca
		And			slo.cnsctvo_pqte > @valor_cero
		
		-- 	
		Insert  
		Into #tempPaquete(
					cnsctvo_cdgo_grpo_imprsn , cnsctvo_cdgo_frma_lqdcn_prstcn, cdgo_intrno            ,					
					cnsctvo_cdgo_pln         , cnsctvo_cdfccn_b              , cnsctvo_cdgo_frma_atncn,
					cnsctvo_pqte             , cnsctvo_cdgo_cncpto_gsto
		)	
		Select      Distinct 
					igi.cnsctvo_cdgo_grpo_imprsn, igi.cnsctvo_cdgo_frma_lqdcn_prstcn, igi.cdgo_intrno             ,
					igi.cnsctvo_cdgo_pln        , cgt.cnsctvo_cdgo_cps              , fa.cnsctvo_cdgo_frma_atncn,
					cgt.cnsctvo_pqte			, cgt.cnsctvo_cdgo_cncpto_gsto 
		From		#tbConceptoGastosTmp_1 cgt
		Inner Join	#tempInformacionGruposImpresionPaquete igi 
		On			igi.cnsctvo_cdfccn_b = cgt.cnsctvo_pqte
		And			igi.cnsctvo_cdgo_pln = cgt.cnsctvo_cdgo_pln
		And			igi.cdgo_intrno = cgt.cdgo_intrno_prstdr
		Inner Join	bdSisalud.dbo.tbFormasAtencion_vigencias fa  With(NoLock)              
		On			fa.cnsctvo_cdgo_frma_atncn   = cgt.cnsctvo_cdgo_frma_atncn
		Where		@fechaLiquidacion Between fa.inco_vgnca And fa.fn_vgnca		
		  
		-- 
		Update		slo
		Set			cnsctvo_cdgo_grpo_imprsn = b.cnsctvo_cdgo_grpo_imprsn
		From		#tbConceptoGastosTmp_1 slo 
		Inner Join	#tempPaquete b
		ON			b.cdgo_intrno			    = slo.cdgo_intrno_prstdr       And
					b.cnsctvo_cdgo_pln		    = slo.cnsctvo_cdgo_pln         And 
					b.cnsctvo_cdgo_cncpto_gsto  = slo.cnsctvo_cdgo_cncpto_gsto And 
					b.cnsctvo_cdgo_frma_atncn   = slo.cnsctvo_cdgo_frma_atncn  And
					b.cnsctvo_pqte              = slo.cnsctvo_pqte
		Where		slo.cnsctvo_pqte > @valor_cero

		-- 
		Insert 
		Into       #temp1(
					cnsctvo_cdgo_grpo_imprsn, cnsctvo_cdgo_frma_lqdcn_prstcn, cdgo_intrno            ,					
					cnsctvo_cdgo_pln        , cnsctvo_cdfccn_b              , cnsctvo_cdgo_frma_atncn
		)
		Select Distinct	igi.cnsctvo_cdgo_grpo_imprsn, igi.cnsctvo_cdgo_frma_lqdcn_prstcn, igi.cdgo_intrno             ,
					igi.cnsctvo_cdgo_pln        , igi.cnsctvo_cdfccn_b              , fa.cnsctvo_cdgo_frma_atncn
		From	    #tbConceptoGastosTmp_1 cgt
		Inner Join	#tempInformacionGruposImpresion igi 
		On			igi.cnsctvo_cdfccn_b = cgt.cnsctvo_cdgo_cps
		And			igi.cnsctvo_cdgo_pln = cgt.cnsctvo_cdgo_pln
		And			igi.cdgo_intrno = cgt.cdgo_intrno_prstdr
		Inner Join	bdSisalud.dbo.tbFormasAtencion_vigencias fa with(nolock)               
		On			fa.cnsctvo_cdgo_frma_atncn = cgt.cnsctvo_cdgo_frma_atncn
		Where		@fechaLiquidacion Between fa.inco_vgnca And fa.fn_vgnca
		And			cgt.cnsctvo_pqte = @valor_cero

		/******* Calculo de la forma y tipo de impresion de acuerdo al convenio ***********/
		--1. Consulta la forma de liquidacion parta determinar de donde se obtiene el grupo de impresion
		Update		slo
		Set			cnsctvo_cdgo_frma_lqdcn_prstcn = b.cnsctvo_cdgo_frma_lqdcn_prstcn
		From		#tbConceptoGastosTmp_1 slo 
		Inner Join	#temp1 b
		On			b.cdgo_intrno			   = slo.cdgo_intrno_prstdr      And /*prestador*/
					b.cnsctvo_cdgo_pln		   = slo.cnsctvo_cdgo_pln        And /*plan*/
	      			b.cnsctvo_cdfccn_b	       = slo.cnsctvo_cdgo_cps        And
	          		b.cnsctvo_cdgo_frma_atncn = slo.cnsctvo_cdgo_frma_atncn
		Where		slo.cnsctvo_pqte = @valor_cero

		-- 2. Consulta grupos de impresion para prestaciones no liq por conceptos
		Update		slo
		Set			cnsctvo_cdgo_grpo_imprsn = b.cnsctvo_cdgo_grpo_imprsn
		From		#tbConceptoGastosTmp_1 slo 
		Inner Join	#temp1 b
		On			b.cdgo_intrno			    = slo.cdgo_intrno_prstdr  And  /*prestador*/
	      			b.cnsctvo_cdgo_pln		    = slo.cnsctvo_cdgo_pln    And  /*plan*/
	      			b.cnsctvo_cdfccn_b	        = slo.cnsctvo_cdgo_cps    And 
					b.cnsctvo_cdgo_frma_atncn  = slo.cnsctvo_cdgo_frma_atncn
		Where		slo.cnsctvo_cdgo_frma_lqdcn_prstcn != @valor_cero
		And			slo.cnsctvo_cdgo_frma_lqdcn_prstcn != @cnsctvo_cdgo_frma_lqdcn_prstcn
		And			slo.cnsctvo_pqte = @valor_cero
		
		-- 3. grupos de impresion de acuerdo a modelo de conceptos
		Insert 
		Into       #temp2(
					cnsctvo_cdgo_grpo_imprsn, cdgo_intrno             , cnsctvo_cdgo_pln       , 				
					cnsctvo_cdfccn_b        , cnsctvo_cdgo_cncpto_gsto, cnsctvo_cdgo_frma_atncn
		)
		Select	Distinct	x.cnsctvo_cdgo_grpo_imprsn, igi.cdgo_intrno             , igi.cnsctvo_cdgo_pln        ,  
					igi.cnsctvo_cdfccn_b        , u.cnsctvo_cdgo_cncpto_gsto, fa.cnsctvo_cdgo_frma_atncn  
		FROM		#tbConceptoGastosTmp_1 cgt
		Inner Join	#tempInformacionGruposImpresion igi 
		On			igi.cnsctvo_cdfccn_b = cgt.cnsctvo_cdgo_cps
		And			igi.cnsctvo_cdgo_pln = cgt.cnsctvo_cdgo_pln
		And			igi.cdgo_intrno = cgt.cdgo_intrno_prstdr  
		Inner Join	bdContratacion.dbo.tbModeloConveniosConceptos r  With(NoLock)           
		On			r.cnsctvo_cdgo_mdlo_cnvno_cncpto = igi.cnsctvo_cdgo_mdlo_cnvno_cncpto 			      
		Inner Join	bdContratacion.dbo.tbDetModeloConveniosConceptos x  With(NoLock)        
		On			x.cnsctvo_cdgo_mdlo_cnvno_cncpto = r.cnsctvo_cdgo_mdlo_cnvno_cncpto      
		Inner Join	BdSisalud.dbo.tbConceptosxTarifas u  With(NoLock)                      
		On			u.cnsctvo_trfa_cncpto      = x.cnsctvo_trfa_cncpto  And
					u.cnsctvo_cdgo_cncpto_gsto = cgt.cnsctvo_cdgo_cncpto_gsto
		Inner Join	bdSisalud.dbo.tbFormasAtencion_vigencias fa  With(NoLock)              
		On			fa.cnsctvo_cdgo_frma_atncn   = cgt.cnsctvo_cdgo_frma_atncn
		Where		cgt.cnsctvo_cdgo_frma_lqdcn_prstcn = @cnsctvo_cdgo_frma_lqdcn_prstcn
		And			@fechaLiquidacion Between x.inco_vgnca  And x.fn_vgnca
		And			@fechaLiquidacion Between u.inco_vgnca  And u.fn_vgnca
		And			@fechaLiquidacion Between r.inco_vgnca  And r.fn_vgnca
		And			@fechaLiquidacion Between fa.inco_vgnca And fa.fn_vgnca
		And			cgt.cnsctvo_pqte = @valor_cero

		-- 
		Update		slo
		Set			cnsctvo_cdgo_grpo_imprsn = b.cnsctvo_cdgo_grpo_imprsn
		From		#tbConceptoGastosTmp_1 slo 
		Inner Join	#temp2 b
		ON			b.cdgo_intrno			    = slo.cdgo_intrno_prstdr       And
					b.cnsctvo_cdgo_pln		    = slo.cnsctvo_cdgo_pln         And 
					b.cnsctvo_cdfccn_b	        = slo.cnsctvo_cdgo_cps         And 
					b.cnsctvo_cdgo_cncpto_gsto = slo.cnsctvo_cdgo_cncpto_gsto And 
					b.cnsctvo_cdgo_frma_atncn  = slo.cnsctvo_cdgo_frma_atncn
		And			slo.cnsctvo_pqte = @valor_cero
			  
		---------------------------------------------------------------------------------------------------      
		-- 4. se adiciona por defecto el grupo de impresion " medicamentos especializados" mientras 
		--    se incluye en los modelos de medicamentos en la estrcutura tbDetModeloConveniosMedicamentos      
		---------------------------------------------------------------------------------------------------
		Update		slo
		Set			cnsctvo_cdgo_grpo_imprsn = @cnsctvo_cdgo_grpo_imprsn_cms
		From		#tbConceptoGastosTmp_1 slo
		Inner Join	bdsisalud.dbo.tbcodificaciones cod With(NoLock) 
		On			cod.cnsctvo_cdfccn = slo.cnsctvo_cdgo_cps
		Where		slo.cnsctvo_cdgo_frma_lqdcn_prstcn = @valor_cero
		And			cod.cnsctvo_cdgo_tpo_cdfccn        = @cnsctvo_cdgo_tpo_cdfccn_cms; -- Cums Mdts

		---------------------------------------------------------------------------------------------------      
		-- 5. se adiciona por defecto el grupo de impresion "INSUMOS" a los CUOS (sislbr01)
		---------------------------------------------------------------------------------------------------
		Update		slo
		Set			cnsctvo_cdgo_grpo_imprsn = @cnsctvo_cdgo_grpo_imprsn_cs
		From		#tbConceptoGastosTmp_1 slo
		Inner Join	bdsisalud.dbo.tbcodificaciones cod With(NoLock) 
		On			cod.cnsctvo_cdfccn = slo.cnsctvo_cdgo_cps
		Where		slo.cnsctvo_cdgo_frma_lqdcn_prstcn = @valor_cero
		And			cod.cnsctvo_cdgo_tpo_cdfccn        = @cnsctvo_cdgo_tpo_cdfccn_cs; -- Cuos

		--
		Drop Table #TEMP1
		Drop Table #temp2
		Drop Table #tempPaquete
		Drop Table #tempInformacionGruposImpresion
		Drop Table #tempInformacionGruposImpresionPaquete

END

