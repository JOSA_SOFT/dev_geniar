USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudAfiliados]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudAfiliados
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Afiliados como 
					 :	modificación de una solicitud especifica D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 21/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudAfiliados] 	
AS
  SET NOCOUNT ON
	DECLARE @Fecha_actual as datetime
	SET @Fecha_actual = getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal
		DELETE			AFO
		FROM			BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal AFO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				AFO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo		
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASInformacionAfiliadoSolicitudAutorizacionServicios
		DELETE			AFP
		FROM			BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios	 AFP
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				AFP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo	
		-- INSERTAR INFORMACIÓN DE AFILIADOS EN TABLA DE PROCESO tbInformacionAfiliadoSolicitudAutorizacionServicios 	
		MERGE INTO		BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios 
		USING (SELECT	IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
						,TAF.id
						,TAF.cnsctvo_cdgo_tpo_idntfccn_afldo		
						,TAF.nmro_idntfccn_afldo					
						,TAF.cnsctvo_cdgo_dprtmnto_afldo				
						,TAF.cnsctvo_cdgo_cdd_rsdnca_afldo				
						,TAF.cnsctvo_cdgo_cbrtra_sld					
						,TAF.cnsctvo_cdgo_tpo_pln						
						,TAF.cnsctvo_cdgo_pln							
						,TAF.cnsctvo_cdgo_estdo_pln						
						,TAF.cnsctvo_cdgo_sxo							
						,TAF.cnsctvo_cdgo_tpo_vnclcn_afldo				
						,TAF.nmro_unco_idntfccn_afldo		
						,TAF.cnsctvo_afldo_slctd_orgn			
						,TAF.cnsctvo_cdgo_estdo_drcho	
						,TAF.cnsctvo_cdgo_tpo_cntrto		
						,TAF.nmro_cntrto							
						,TAF.cnsctvo_bnfcro_cntrto					
						,TAF.cdgo_ips_prmra				
						,TAF.cnsctvo_cdgo_sde_ips_prmra		
						,TAF.rcn_ncdo									
						,TAF.prto_mltple								
						,TAF.nmro_hjo_afldo								
						,TAF.cnsctvo_cdgo_sxo_rcn_ncdo					
						,TAF.ttla										
						,TAF.cnsctvo_cdgo_chrte		
						,TAF.cnsctvo_cdgo_rngo_slrl		
						,TAF.usro_crcn		
			FROM		#TMod_Afiliados		TAF WITH (NOLOCK)		
			INNER JOIN	#TMod_solicitudes	IDS WITH (NOLOCK)
			ON			TAF.id = IDS.id) AS AFI
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,cnsctvo_cdgo_tpo_idntfccn_afldo
				,nmro_idntfccn_afldo
				,cnsctvo_cdgo_dprtmnto_afldo
				,cnsctvo_cdgo_cdd_rsdnca_afldo
				,cnsctvo_cdgo_cbrtra_sld
				,cnsctvo_cdgo_tpo_pln
				,cnsctvo_cdgo_pln
				,cnsctvo_cdgo_estdo_pln
				,cnsctvo_cdgo_sxo
				,cnsctvo_cdgo_tpo_vnclcn_afldo
				,nmro_unco_idntfccn_afldo
				,cnsctvo_afldo_slctd_orgn
				,cnsctvo_cdgo_estdo_drcho
				,cnsctvo_cdgo_tpo_cntrto
				,nmro_cntrto
				,cnsctvo_bnfcro_cntrto
				,cdgo_ips_prmra
				,cnsctvo_cdgo_sde_ips_prmra
				,rcn_ncdo
				,prto_mltple
				,nmro_hjo_afldo
				,cnsctvo_cdgo_sxo_rcn_ncdo
				,ttla
				,cnsctvo_cdgo_chrte
				,smns_ctzds
				,cnsctvo_cdgo_rngo_slrl
				,edd_afldo_ans
				,edd_afldo_mss
				,edd_afldo_ds
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn)
		VALUES	(AFI.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,AFI.cnsctvo_cdgo_tpo_idntfccn_afldo		
				,AFI.nmro_idntfccn_afldo					
				,AFI.cnsctvo_cdgo_dprtmnto_afldo				
				,AFI.cnsctvo_cdgo_cdd_rsdnca_afldo				
				,AFI.cnsctvo_cdgo_cbrtra_sld					
				,AFI.cnsctvo_cdgo_tpo_pln						
				,AFI.cnsctvo_cdgo_pln							
				,AFI.cnsctvo_cdgo_estdo_pln						
				,AFI.cnsctvo_cdgo_sxo							
				,AFI.cnsctvo_cdgo_tpo_vnclcn_afldo				
				,AFI.nmro_unco_idntfccn_afldo		
				,AFI.cnsctvo_afldo_slctd_orgn			
				,AFI.cnsctvo_cdgo_estdo_drcho	
				,AFI.cnsctvo_cdgo_tpo_cntrto		
				,AFI.nmro_cntrto							
				,AFI.cnsctvo_bnfcro_cntrto					
				,AFI.cdgo_ips_prmra				
				,AFI.cnsctvo_cdgo_sde_ips_prmra		
				,AFI.rcn_ncdo									
				,AFI.prto_mltple								
				,AFI.nmro_hjo_afldo								
				,AFI.cnsctvo_cdgo_sxo_rcn_ncdo					
				,AFI.ttla										
				,AFI.cnsctvo_cdgo_chrte		
				,0				--smns_ctzds 					
				,AFI.cnsctvo_cdgo_rngo_slrl		
				,0				--edd_afldo_ans	
				,0				--edd_afldo_mss	
				,0				--edd_afldo_ds
				,@Fecha_actual	--fcha_crcn		
				,AFI.usro_crcn		
				,@Fecha_actual	--fcha_ultma_mdfccn
				,AFI.usro_crcn	--usro_ultma_mdfccn
				)
		OUTPUT	inserted.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
				,AFI.Id
		INTO	#IdAfiliados_Mod;  		
		-- INSERTAR INFORMACIÓN DE AFILIADOS EN TABLA ORIGINAL tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal 
		INSERT INTO		BdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal(
						cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
						,cnsctvo_slctd_atrzcn_srvco
						,prmr_aplldo_afldo
						,sgndo_aplldo_afldo
						,prmr_nmbre_afldo
						,sgndo_nmbre_afldo
						,cdgo_tpo_idntfccn_afldo
						,nmro_idntfccn_afldo
						,fcha_ncmnto_afldo
						,drccn_afldo
						,tlfno_afldo
						,cdgo_dprtmnto_afldo
						,dscrpcn_dprtmnto_afldo
						,cdgo_cdd_rsdnca_afldo
						,dscrpcn_cdd_rsdnca_afldo
						,tlfno_cllr_afldo
						,crro_elctrnco_afldo
						,cdgo_cbrtra_sld
						,cdgo_pln
						,cdgo_sxo
						,edd_afldo_ans
						,edd_afldo_mss
						,edd_afldo_ds
						,smns_ctzds_ps
						,nmro_cntrto
						,cdgo_ips_prmra
						,rcn_ncdo
						,prto_mltple
						,nmro_hjo_afldo
						,cdgo_sxo_rcn_ncdo
						,cdgo_tpo_pln
						,cdgo_tpo_vnclcn_afldo
						,cdgo_estdo_pln
						,fcha_ncmnto_rcn_ncdo
						,fcha_crcn
						,usro_crcn
						,fcha_ultma_mdfccn
						,usro_ultma_mdfccn)
		SELECT			 IDA.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
						,IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
						,AFI.prmr_aplldo_afldo	
						,AFI.sgndo_aplldo_afldo							
						,AFI.prmr_nmbre_afldo							
						,AFI.sgndo_nmbre_afldo							
						,AFI.cdgo_tpo_idntfccn_afldo	
						,AFI.nmro_idntfccn_afldo					
						,AFI.fcha_ncmnto_afldo	
						,AFI.drccn_afldo
						,AFI.tlfno_afldo
						,AFI.cdgo_dprtmnto_afldo						
						,AFI.dscrpcn_dprtmnto_afldo						
						,AFI.cdgo_cdd_rsdnca_afldo						
						,AFI.dscrpcn_cdd_rsdnca_afldo					
						,AFI.tlfno_cllr_afldo
						,AFI.crro_elctrnco_afldo						
						,AFI.cdgo_cbrtra_sld
						,AFI.cdgo_pln
						,AFI.cdgo_sxo
						,0				--edd_afldo_ans
						,0				--edd_afldo_mss
						,0				--edd_afldo_ds
						,0				--smns_ctzds_ps
						,AFI.nmro_cntrto	
						,AFI.cdgo_ips_prmra
						,AFI.rcn_ncdo									
						,AFI.prto_mltple								
						,AFI.nmro_hjo_afldo	
						,AFI.cdgo_sxo_rcn_ncdo						
						,AFI.cdgo_tpo_pln
						,AFI.cdgo_tpo_vnclcn_afldo
						,AFI.cdgo_estdo_pln		
						,AFI.fcha_ncmnto_rcn_ncdo
						,@Fecha_actual	--fcha_crcn		
						,AFI.usro_crcn		
						,@Fecha_actual	--fcha_ultma_mdfccn
						,AFI.usro_crcn	--usro_ultma_mdfccn
		FROM			#TMod_Afiliados		AFI WITH (NOLOCK)
		INNER JOIN		#TMod_Solicitudes	IDS WITH (NOLOCK)
		on				AFI.id = IDS.id
		INNER JOIN		#IdAfiliados_Mod	IDA WITH (NOLOCK)
		ON				AFI.id = IDA.idXML
	END


GO
