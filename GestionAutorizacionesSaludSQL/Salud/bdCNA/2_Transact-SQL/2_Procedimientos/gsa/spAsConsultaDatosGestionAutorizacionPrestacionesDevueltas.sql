USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas]    Script Date: 24/10/2016 10:09:21 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas AS SELECT 1;'
END
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Valida en cupsxplanesxcargo con la prestaciones para verificar flujo en BPM    D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 17/11/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
-- EXEC [bdCNA].[gsa].[spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas] 807, 1909
ALTER PROCEDURE [gsa].[spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@cnsctvo_srvco_slctdo			udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	declare @orgn_mdfccn	udtDescripcion = 'tbASAutorizaNoConformidadValidacion.cnsctvo_cdgo_estdo_srvco_slctdo'
	
		Select  top 1 vlr_antrr nmro_atrzcn_prstcn_ips,
				cnsctvo_cdgo_csa_nvdd ,
				obsrvcns,
				vlr_nvo cdgo_intrno
		From BDCna.gsa.tbASTrazaModificacion tm With(Nolock)
		Where 	tm.cnsctvo_slctd_atrzcn_srvco	= @cnsctvo_slctd_atrzcn_srvco
		And		tm.cnsctvo_srvco_slctdo			= @cnsctvo_srvco_slctdo
		And 	tm.orgn_mdfccn					= @orgn_mdfccn
	
	
END;
