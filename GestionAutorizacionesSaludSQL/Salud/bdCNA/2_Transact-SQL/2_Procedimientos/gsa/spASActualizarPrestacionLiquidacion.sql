USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarPrestacionLiquidacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASActualizarPrestacionLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Actualizar el estado de la prestación y el
							  valor de la liquidación							D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 01/06/2016										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarPrestacionLiquidacion]
	@usr_lgn			udtusuario
AS
BEGIN
  SET NOCOUNT ON; 
	
	DECLARE @estdo_aprbda	INT = 7,
			@vlr_nlo		VARCHAR = NULL,
			@fcha_actl		DATETIME = GETDATE();

	BEGIN
		
		UPDATE		SS
		SET			cnsctvo_cdgo_estdo_srvco_slctdo = @estdo_aprbda,
					vlr_lqdcn_srvco = @vlr_nlo,
					fcha_ultma_mdfccn = @fcha_actl,
					usro_ultma_mdfccn = @usr_lgn
		FROM		gsa.tbASServiciosSolicitados SS
		INNER JOIN	#cnslddo_slctud CS
		ON			CS.cnsctvo_slctd_atrzcn_srvco = SS.cnsctvo_slctd_atrzcn_srvco
		AND			CS.cnsctvo_srvco_slctdo = SS.cnsctvo_srvco_slctdo;

	END					

END;

GO
