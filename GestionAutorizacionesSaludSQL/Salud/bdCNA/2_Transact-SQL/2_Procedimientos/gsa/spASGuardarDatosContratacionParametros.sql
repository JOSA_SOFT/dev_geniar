USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarDatosContratacionParametros]    Script Date: 22/12/2016 14:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarDatosContratacionParametros
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D  D\>
* Observaciones			: <\O  O\>
* Parametros			: <\P> P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2016/03/07 FC\>
*----------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION
*----------------------------------------------------------------------------------------------------------------------------- 
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento almacenado con el fin de que guarde correctamente la informacion
								de la homologacion de la prestacion y el codigo interno del prestador en la tabla 
								tbASServiciosSolicitados y tbASResultadoDireccionamiento
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/12/22 FM\>
*----------------------------------------------------------------------------------------------------------------------------- 
* Modificado Por		 : <\AM	Ing. Jorge Rodriguez - SETI SAS AM\>
* Descripcion			 : <\DM	
                                Se modifica el procedimiento almacenado para que actualice sobre la tabla tbASServiciosSolicitadosOriginal
								cualquier modificación realizada sobre las prestaciones de la solicitud.
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/03/23 FM\>
*----------------------------------------------------------------------------------------------------------------------------- */
ALTER PROCEDURE [gsa].[spASGuardarDatosContratacionParametros]
@cnsctvo_srvco_slctdo		udtConsecutivo,
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@cnsctvo_cdgo_tpo_prstcn	udtConsecutivo,
@cnsctvo_cdfccn		        udtConsecutivo,
@cdgo_intrno                udtCodigoIps  ,
@usro_prcso					udtUsuario    ,
@obsrvcns					udtdescripcion, --Pendiente de definir donde se guarda el Registro
@dscrpcn_srvco_slctdo		udtdescripcion

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fechaActual			    DATETIME,
			@fechaFinVigencia		    DATETIME, 
			@valido					    CHAR (1),
			@cnsctvo_cdgo_prstcn_prstdr	udtConsecutivo,
			@lncdgo_cdfccn				CHAR (11),
			@lndscrpcn_cdfccn			udtDescripcion

	CREATE TABLE #tempDatosContratacion(	
		cnsctvo_srvco_slctdo			udtConsecutivo,
		usro_ultma_mdfccn				udtUsuario,
		cnsctvo_cdgo_prstcn_prstdr		udtCodigoIPS
	);
	
	Set  @fechaActual		= GETDATE();
	Set  @fechaFinVigencia	= '99991231';
	Set  @valido			= '1';

	Insert 
	Into   #tempDatosContratacion(cnsctvo_srvco_slctdo, usro_ultma_mdfccn, cnsctvo_cdgo_prstcn_prstdr)
	Select cnsctvo_srvco_slctdo, usro_ultma_mdfccn, cnsctvo_cdgo_prstcn_prstdr
	From   BDCna.gsa.tbASServiciosSolicitados
	Where  cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

	Insert 
	Into   BDCNA.gsa.tbASHistoricoPrestadorServiciosSolicitados(cnsctvo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco, cdgo_intrno      ,
		                                                        obsrvcn             , inco_vgnca                , fn_vgnca         ,
		                                                        fcha_fn_vldz_rgstro , vldo                      , usro_crcn        ,
		                                                        fcha_crcn           , usro_ultma_mdfccn         , fcha_ultma_mdfccn
	                                                           )
	Select  cnsctvo_srvco_slctdo, @cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_prstcn_prstdr,
			@obsrvcns           , @fechaActual               , @fechaFinVigencia         ,
			@fechaFinVigencia   , @valido                    , usro_ultma_mdfccn         ,
			@fechaActual        , usro_ultma_mdfccn          , @fechaActual
	FROM 	#tempDatosContratacion

	
	Select     @cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr
	From       BDIpsParametros.dbo.tbPrestacionesPrestador p1 With(NoLock)
    Inner Join BDIpsParametros.dbo.tbDetalle_PrestacionesPrestador h2 With(NoLock)
    On         h2.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr
    Inner Join BDIpsParametros.dbo.tbHomologacionPrestaciones h1 With(NoLock)
    On         h1.cdgo_intrno                = p1.cdgo_intrno  And 
               h1.cnsctvo_cdgo_prstcn_prstdr = p1.cnsctvo_cdgo_prstcn_prstdr 
    Inner Join BDIpsParametros.dbo.tbDetalle_HomologacionPrestaciones h3 With(NoLock) 
    On         h3.cnsctvo_cdgo_hmlgcn_prstcn = h1.cnsctvo_cdgo_hmlgcn_prstcn 
	Inner Join bdSiSalud.dbo.tbCodificaciones c
	On         c.cnsctvo_cdfccn = h1.cnsctvo_cdfccn
	Where      c.cnsctvo_cdfccn = @cnsctvo_cdfccn
	And        p1.cdgo_intrno   = @cdgo_intrno
	And        @fechaActual Between h2.inco_vgnca And h2.fn_vgnca
    And        @fechaActual Between h3.inco_vgnca And h3.fn_vgnca 

	If @cnsctvo_cdgo_prstcn_prstdr Is Null 
	   Begin
	      Set @cnsctvo_cdgo_prstcn_prstdr = @cnsctvo_cdfccn 
	   End

	SELECT @lndscrpcn_cdfccn			= dscrpcn_cdfccn,
		   @lncdgo_cdfccn				= cdgo_cdfccn 
	FROM   bdSisalud.dbo.tbCodificaciones With(NoLock)
	Where  cnsctvo_cdfccn				= @cnsctvo_cdfccn

	--Se adiciona Update sobre la tabla tbASServiciosSolicitados
	UPDATE BDCNA.GSA.tbASServiciosSolicitados  
	SET    cnsctvo_cdgo_srvco_slctdo  = @cnsctvo_cdfccn       		,
		   cnsctvo_cdgo_prstcn_prstdr = @cnsctvo_cdgo_prstcn_prstdr ,
           cnsctvo_cdgo_tpo_srvco     = @cnsctvo_cdgo_tpo_prstcn	,
           fcha_ultma_mdfccn		  = @fechaActual			    ,
           usro_ultma_mdfccn		  = @usro_prcso				    ,			  
           dscrpcn_srvco_slctdo		  = @lndscrpcn_cdfccn		
	WHERE  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	AND    cnsctvo_srvco_slctdo       = @cnsctvo_srvco_slctdo
	
	
	
	--Se adiciona Update sobre la tabla tbASServiciosSolicitadosOriginal
	UPDATE BDCNA.GSA.tbASServiciosSolicitadosOriginal  
	SET    cdgo_srvco_slctdo		  = @lncdgo_cdfccn       		,
		   fcha_ultma_mdfccn		  = @fechaActual			    ,
           usro_ultma_mdfccn		  = @usro_prcso				    ,			  
           dscrpcn_srvco_slctdo		  = @lndscrpcn_cdfccn		
	WHERE  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	AND    cnsctvo_srvco_slctdo       = @cnsctvo_srvco_slctdo


	If exists(Select 1 
	          From   BDCna.gsa.tbASResultadoDireccionamiento rd
			  Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			  And    cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo	
			 )
		Begin
			Update BDCna.gsa.tbASResultadoDireccionamiento 
			Set    cdgo_intrno		 = @cdgo_intrno,
				   usro_ultma_mdfccn = @usro_prcso,
				   fcha_ultma_mdfccn = @fechaActual
			Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			And    cnsctvo_srvco_slctdo       = @cnsctvo_srvco_slctdo	
		End
	Else
		Begin
			Insert 
			Into   BDCna.gsa.tbASResultadoDireccionamiento(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cdgo_intrno      ,
				                                           fcha_crcn                 , usro_crcn           , fcha_ultma_mdfccn,
				                                           usro_ultma_mdfccn
			                                              )
			Values(@cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo, @cdgo_intrno,
				   @fechaActual               , @usro_prcso          , @fechaActual,
				   @usro_prcso
				  )
		End
	DROP TABLE #tempDatosContratacion
END
