USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsGuardarInformacionModificacionPrestacion]    Script Date: 03/08/2017 12:18:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
-------------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spAsGuardarInformacionModificacionPrestacion												
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez A\>  
* Descripcion		: <\D  Procedimientos que guarda la informacion la prestacion modificada. D\>  												
* Observaciones		: <\O  O\>  													
* Parametros		: <\P  P\>  													
* Variables			: <\V  V\>  													
* Fecha Creacion	: <\FC 17/07/2017 FC\>											
*  															
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se realizan ajustes para guardar solo la informacion que se modifique.
							   Se agrega llamado a sp de recalculo de cuota de recuperacion DM\>
* Nuevos Parametros	 	: <\PM  PM\>  													
* Nuevas Variables		: <\VM  VM\>  													
* Fecha Modificacion	: <\FM  FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
*/

/*
	Declare @cdgo_rsltdo				udtConsecutivo,
			@mnsje_rsltdo				VarChar(1000)
	Exec bdCNA.gsa.spAsGuardarInformacionModificacionPrestacion 135212, 76294, 11, 21, 'xxxxxxxxxxx', 1 , 'user19'
*/

ALTER Procedure [gsa].[spAsGuardarInformacionModificacionPrestacion]
			@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
			@cnsctvo_srvco_slctdo		udtConsecutivo,
			@cnsctvo_cdgo_rcbro			udtConsecutivo,
			@cnsctvo_cdgo_cusa_no_cbro	udtConsecutivo, -- Causa de no cobro cuota de recuperacion.
			@obsrvcns					udtObservacion,
			@cnsctvo_cdgo_cntngnca		udtConsecutivo,
			@usro						udtUsuario
As
Begin
	-- 
	Set NoCount On

	-- 
	Declare	@fcha_actl					Date,
			@orgn_mdfccn1				Varchar(100),
			@orgn_mdfccn2				Varchar(100),
			@cnsc_cdgo_mtvo_csa			udtConsecutivo,			
			@nmro_ip					Varchar(9),
			@elmnto_orgn_mdfccn			udtDescripcion,
			@dto_elmnto_orgn_mdfccn		udtDescripcion,
			@cnsctvo_cdgo_mdlo			udtConsecutivo,			
			@vsble_usro					Char(1),
			@vlr_cro					Int,
			@cnsctvo_cdgo_estdo_entrgdo	udtConsecutivo,
			@cnsctvo_cdgo_estdo_usdo	udtConsecutivo,
			@cnsctvo_cdgo_estdo_anldo	udtConsecutivo,
			@cnsctvo_cdgo_estdo_cbrdo	udtConsecutivo,
			@cnsctvo_cdgo_estdo_atrzdo	udtConsecutivo;	
			
		

	Create Table #tempInformacionSolicitud (
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_cdgo_rcbro				udtConsecutivo,
		cnsctvo_cdgo_rcbro_ant			udtConsecutivo,
		cdgo_rcbro						Char(3),
		cdgo_rcbro_ant					Char(3),
		cnsctvo_cdgo_csa_no_cbro		udtConsecutivo,
		cnsctvo_cdgo_csa_no_cbro_ant	udtConsecutivo,
		cnsctvo_cdgo_cntngnca			udtConsecutivo,
		dscrpcn_rcbro					udtDescripcion,
		dscrpcn_rcbro_ant				udtDescripcion,
		dscrpcn_csa_no_cbro				udtDescripcion,
		dscrpcn_csa_no_cbro_ant			udtDescripcion Default 0
	);

	-- 
	Create Table #tmpTrazaModificacion (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
		fcha_mdfccn					datetime,
		orgn_mdfccn					varchar(100),
		vlr_antrr						varchar(20),
		vlr_nvo						varchar(20),
		usro_mdfccn					udtUsuario,
		obsrvcns						udtObservacion,
		nmro_ip						varchar(12),
		fcha_crcn						datetime,
		usro_crcn						udtUsuario,
		fcha_ultma_mdfccn				datetime,
		usro_ultma_mdfccn				udtUsuario,
		elmnto_orgn_mdfccn				udtDescripcion,
		dto_elmnto_orgn_mdfccn			udtDescripcion,
		dscrpcn_vlr_antrr				udtDescripcion,
		dscrpcn_vlr_nvo					udtDescripcion
	);

	Set @fcha_actl = getDate();
	Set @orgn_mdfccn1 = 'tbASServiciosSolicitados.cnsctvo_cdgo_rcbro';
	Set @orgn_mdfccn2 = 'tbASServiciosSolicitados.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn';
	Set @cnsc_cdgo_mtvo_csa = 7;
	Set @nmro_ip = '127.0.0.1';
	Set	@elmnto_orgn_mdfccn = 'Prestacion';
	Set @dto_elmnto_orgn_mdfccn = 'Actualizacion';
	Set @cnsctvo_cdgo_mdlo = 31;
	Set @vsble_usro = 'S';
	Set @vlr_cro = 0;
	Set @cnsctvo_cdgo_estdo_entrgdo	= 15;
	Set @cnsctvo_cdgo_estdo_usdo = 16;
	Set @cnsctvo_cdgo_estdo_anldo = 14;
	Set @cnsctvo_cdgo_estdo_cbrdo = 12;
	Set @cnsctvo_cdgo_estdo_atrzdo = 11;
	
	---
	Insert Into #tempInformacionSolicitud (
		cnsctvo_slctd_atrzcn_srvco,			cnsctvo_srvco_slctdo,
		cnsctvo_cdgo_rcbro,					cnsctvo_cdgo_csa_no_cbro,
		cnsctvo_cdgo_cntngnca
	)
	Select	@cnsctvo_slctd_atrzcn_srvco,		@cnsctvo_srvco_slctdo,
			@cnsctvo_cdgo_rcbro,				@cnsctvo_cdgo_cusa_no_cbro,
			@cnsctvo_cdgo_cntngnca;

	-- 
	Update		a
	Set			cnsctvo_cdgo_rcbro_ant = ss.cnsctvo_cdgo_rcbro,
				cnsctvo_cdgo_csa_no_cbro_ant = ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn 
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo;

	-- 
	Update		#tempInformacionSolicitud
	Set			cnsctvo_cdgo_rcbro_ant = @vlr_cro
	Where		cnsctvo_cdgo_rcbro_ant Is Null;

	-- 
	Update		#tempInformacionSolicitud
	Set			cnsctvo_cdgo_csa_no_cbro_ant = @vlr_cro
	Where		cnsctvo_cdgo_csa_no_cbro_ant Is Null;

	-- 
	Update		inf
	Set   		dscrpcn_rcbro_ant = b.dscrpcn_clsfccn_evnto
	From		#tempInformacionSolicitud inf
	Inner Join	bdSisalud.dbo.tbClasificacionEventosNotificacion_vigencias b  With(NoLock)
	On			b.cnsctvo_cdgo_clsfccn_evnto = inf.cnsctvo_cdgo_rcbro_ant
	Inner Join	bdSisalud.dbo.tbMNClasificacionxTipoEvento_vigencias a 
	On			a.cnsctvo_cdgo_clsfccn_evnto=b.cnsctvo_cdgo_clsfccn_evnto 
	And			a.cnsctvo_cdgo_cntngnca = inf.cnsctvo_cdgo_cntngnca         
	Inner Join	bdSisalud.dbo.tbClasificacionEventoNotificacionxModulo c 
	On			b.cnsctvo_cdgo_clsfccn_evnto = c.cnsctvo_cdgo_clsfccn_evnto  
	Where		@fcha_actl  between b.inco_vgnca And b.fn_vgnca
	And			@fcha_actl  between a.inco_vgnca And a.fn_vgnca 
	And			b.Vsble_Usro = @vsble_usro  	
	And			c.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo -- Eventos que aplican solo para sipres 
	
	-- 
	Update		inf
	Set   		dscrpcn_rcbro = b.dscrpcn_clsfccn_evnto,
				cdgo_rcbro = b.cdgo_clsfccn_evnto
	From		#tempInformacionSolicitud inf
	Inner Join	bdSisalud.dbo.tbClasificacionEventosNotificacion_vigencias b  With(NoLock)
	On			b.cnsctvo_cdgo_clsfccn_evnto = inf.cnsctvo_cdgo_rcbro
	Inner Join	bdSisalud.dbo.tbMNClasificacionxTipoEvento_vigencias a 
	On			a.cnsctvo_cdgo_clsfccn_evnto=b.cnsctvo_cdgo_clsfccn_evnto 
	And			a.cnsctvo_cdgo_cntngnca = inf.cnsctvo_cdgo_cntngnca         
	Inner Join	bdSisalud.dbo.tbClasificacionEventoNotificacionxModulo c 
	On			b.cnsctvo_cdgo_clsfccn_evnto = c.cnsctvo_cdgo_clsfccn_evnto  
	Where		@fcha_actl  between b.inco_vgnca And b.fn_vgnca
	And			@fcha_actl  between a.inco_vgnca And a.fn_vgnca 
	And			b.Vsble_Usro = @vsble_usro  	
	And			c.cnsctvo_cdgo_mdlo = @cnsctvo_cdgo_mdlo -- Eventos que aplican solo para sipres 

	-- 
	Update		a
	Set			dscrpcn_csa_no_cbro = b.dscrpcn_csa_no_cbro_cta_rcprcn 
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias b With(NoLock)
	On			b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = a.cnsctvo_cdgo_csa_no_cbro
	Where		@fcha_actl Between b.inco_vgnca And b.fn_vgnca;

	-- 
	Update		a
	Set			dscrpcn_csa_no_cbro_ant = b.dscrpcn_csa_no_cbro_cta_rcprcn 
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias b With(NoLock)
	On			b.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = a.cnsctvo_cdgo_csa_no_cbro_ant
	Where		@fcha_actl Between b.inco_vgnca And b.fn_vgnca;
		
	-- 
	Insert Into #tmpTrazaModificacion (
				cnsctvo_slctd_atrzcn_srvco		,	cnsctvo_cdgo_mtvo_csa			,	fcha_mdfccn					   ,	
				orgn_mdfccn					    ,	vlr_antrr						,	vlr_nvo						   ,	
				usro_mdfccn					    ,	obsrvcns						,	nmro_ip						   ,	
				fcha_crcn					    ,	usro_crcn						,	fcha_ultma_mdfccn			   ,	
				usro_ultma_mdfccn			    ,	elmnto_orgn_mdfccn				,	dto_elmnto_orgn_mdfccn		   ,	
				dscrpcn_vlr_antrr			    ,	dscrpcn_vlr_nvo					,	cnsctvo_srvco_slctdo
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco   ,	@cnsc_cdgo_mtvo_csa				,	@fcha_actl						,
				@orgn_mdfccn1					,	a.cnsctvo_cdgo_rcbro_ant		,	a.cnsctvo_cdgo_rcbro			,
				@usro							,	@obsrvcns						,	@nmro_ip						,
				@fcha_actl						,	@usro							,	@fcha_actl						,
				@usro							,	@elmnto_orgn_mdfccn				,	@dto_elmnto_orgn_mdfccn			,
				a.dscrpcn_rcbro_ant				,	a.dscrpcn_rcbro					,	ss.cnsctvo_srvco_slctdo
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	And			a.cnsctvo_cdgo_rcbro_ant != a.cnsctvo_cdgo_rcbro
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_entrgdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_anldo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_cbrdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_usdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_atrzdo;

	-- 
	Insert Into #tmpTrazaModificacion (
				cnsctvo_slctd_atrzcn_srvco		,	cnsctvo_cdgo_mtvo_csa			,	fcha_mdfccn					   ,	
				orgn_mdfccn					    ,	vlr_antrr						,	vlr_nvo						   ,	
				usro_mdfccn					    ,	obsrvcns						,	nmro_ip						   ,	
				fcha_crcn					    ,	usro_crcn						,	fcha_ultma_mdfccn			   ,	
				usro_ultma_mdfccn			    ,	elmnto_orgn_mdfccn				,	dto_elmnto_orgn_mdfccn		   ,	
				dscrpcn_vlr_antrr			    ,	dscrpcn_vlr_nvo					,	cnsctvo_srvco_slctdo
	)
	Select		ss.cnsctvo_slctd_atrzcn_srvco   ,	@cnsc_cdgo_mtvo_csa				,	@fcha_actl						,
				@orgn_mdfccn2					,	a.cnsctvo_cdgo_csa_no_cbro_ant	,	a.cnsctvo_cdgo_csa_no_cbro		,
				@usro							,	@obsrvcns						,	@nmro_ip						,
				@fcha_actl						,	@usro							,	@fcha_actl						,
				@usro							,	@elmnto_orgn_mdfccn				,	@dto_elmnto_orgn_mdfccn			,
				a.dscrpcn_csa_no_cbro_ant		,	a.dscrpcn_csa_no_cbro			,	ss.cnsctvo_srvco_slctdo
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo
	And			a.cnsctvo_cdgo_csa_no_cbro_ant != a.cnsctvo_cdgo_csa_no_cbro
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_entrgdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_anldo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_cbrdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_usdo;

	-- 
	Update		ss
	Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = a.cnsctvo_cdgo_csa_no_cbro,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = @usro
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		a.cnsctvo_cdgo_csa_no_cbro_ant != a.cnsctvo_cdgo_csa_no_cbro
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_entrgdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_anldo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_cbrdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_usdo;

	-- 
	Update		ss
	Set			cnsctvo_cdgo_rcbro = a.cnsctvo_cdgo_rcbro,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = @usro
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		a.cnsctvo_cdgo_csa_no_cbro_ant != a.cnsctvo_cdgo_csa_no_cbro
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_entrgdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_anldo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_cbrdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_usdo
	And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_atrzdo;

	-- 
	Update		ss
	Set			cdgo_rcbro = a.cdgo_rcbro,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = @usro
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitadosOriginal ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where		a.cnsctvo_cdgo_rcbro_ant != a.cnsctvo_cdgo_rcbro;

	-- 
	If(Exists (	Select		1
				From		#tempInformacionSolicitud a
				Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
				On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
				Where		ss.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_atrzdo)
	)
	Begin
		
		-- 
		Exec bdCNA.gsa.spASRecalcularCuotaRecuperacion @usro
		
	End

	-- 
	Exec BDCna.gsa.spASGuardarTrazaModificacion;


	Drop Table #tempInformacionSolicitud
	Drop Table #tmpTrazaModificacion

End