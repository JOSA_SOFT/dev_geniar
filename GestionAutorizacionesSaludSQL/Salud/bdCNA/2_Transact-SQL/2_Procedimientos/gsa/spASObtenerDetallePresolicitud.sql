USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerDetallePresolicitud]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASObtenerDetallePresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASObtenerDetallePresolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASObtenerDetallePresolicitud
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Se obtienen el detalle de la presolicitud			D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/25										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM													AM\>
* Descripcion			 : <\DM													DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASObtenerDetallePresolicitud] 
	@cnsctvo_llve_objto udtconsecutivo
AS
BEGIN
  SET NOCOUNT ON;
	
	DECLARE @fcha_actl DATETIME = GETDATE();
	
	SELECT		ps.cnsctvo_prslctd,
				tiv.cnsctvo_cdgo_tpo_idntfccn,
				ps.nmro_idntfccn,
				ps.cnsctvo_cdgo_pln
	FROM		dbo.tbPreSolicitudes ps  WITH (NOLOCK)
	INNER JOIN	bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias tiv WITH (NOLOCK)
	ON			ps.cdgo_tpo_dcmnto = tiv.cdgo_tpo_idntfccn
	WHERE		ps.cnsctvo_prslctd = @cnsctvo_llve_objto
	AND			@fcha_actl BETWEEN tiv.inco_vgnca AND tiv.fn_vgnca;
	
END;

GO
