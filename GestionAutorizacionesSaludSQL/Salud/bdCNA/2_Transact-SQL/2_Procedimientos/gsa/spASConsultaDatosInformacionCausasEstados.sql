USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionCausasEstados]    Script Date: 27/02/2017 11:46:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionCausasEstados
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  A\>
* Descripcion			: <\D
                              Procedimiento que permite consultar las causas a seleccionar para cada uno de lo estados
							  en la pantalla del auditor
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2015/12/02 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM
                                Se actualiza procedimiento para que liste las causas correspondientes al estado de Devolucion
								en la pantalla del auditor	
                            DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/02/27 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------------*/
--Exec [gsa].[spASConsultaDatosInformacionCausasEstados]  5, null

ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionCausasEstados] 
	@cnsctvo_cdgo_estdo_no_cnfrmdd	udtConsecutivo,
	@cnsctvo_srvco_slctdo           udtConsecutivo = null
AS

BEGIN
	SET NOCOUNT ON;

	Declare @ldfcha_consulta					datetime ,
			@est_vacio							udtConsecutivo = 0,
			@est_inexistente					udtConsecutivo = 99,
			@cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn  udtConsecutivo = 2,
			@udt_false							udtLogico      = 0,
			@cnsctvo_cdgo_estdo_no_cnfrmdd_mlla	udtConsecutivo    ,
			@mrca_css_mstr_adtr                 udtLogico         ,
			@cnsctvo_cdgo_estdo_no_cnfrmdd_aux  udtConsecutivo

	Set @mrca_css_mstr_adtr = 'S'
	Set @cnsctvo_cdgo_estdo_no_cnfrmdd_aux = 3 --Estado NO Conformidad DEVUELTA

	Create 
	Table #CausasEstados(cnsctvo_cdgo_no_cnfrmdd_vldcn udtConsecutivo,	 
	                     cnsctvo_cdgo_estdo_no_cnfrmdd udtConsecutivo,                   
		                 dscrpcn_no_cnfrmdd_vldcn	   udtDescripcion,
		                 estdo						   udtLogico     ,
						 mrca_css_mstr_adtr            udtLogico
	                    )

	SET	@ldfcha_consulta = GETDATE()

	Select @cnsctvo_cdgo_estdo_no_cnfrmdd_mlla = cnsctvo_cdgo_estdo_no_cnfrmdd 
	From   BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias WITH(NOLOCK)
    Where  cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_no_cnfrmdd
	And    @ldfcha_consulta Between inco_vgnca and fn_vgnca

	Insert 
	Into   #CausasEstados (cnsctvo_cdgo_no_cnfrmdd_vldcn, dscrpcn_no_cnfrmdd_vldcn, 
	                       estdo                        , mrca_css_mstr_adtr      ,
						   cnsctvo_cdgo_estdo_no_cnfrmdd
						  )
	Select cnsctvo_cdgo_no_cnfrmdd_vldcn, dscrpcn_no_cnfrmdd_vldcn, 
	       @udt_false                   , mrca_css_mstr_adtr      ,
		   cnsctvo_cdgo_estdo_no_cnfrmdd
    From   bdMallaCNA.dbo.tbNoConformidadesValidacion_Vigencias WITH (NOLOCK)
	Where  cnsctvo_cdgo_estdo_no_cnfrmdd     = @cnsctvo_cdgo_estdo_no_cnfrmdd_mlla
	And    cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn = @cnsctvo_cdgo_tpo_no_cnfrmdd_vldcn
	And    cnsctvo_cdgo_no_cnfrmdd_vldcn <> @est_vacio 
	And    cnsctvo_cdgo_no_cnfrmdd_vldcn <> @est_inexistente
	And	   @ldfcha_consulta Between inco_vgnca And fn_vgnca

	Delete From #CausasEstados Where mrca_css_mstr_adtr != @mrca_css_mstr_adtr And cnsctvo_cdgo_estdo_no_cnfrmdd = @cnsctvo_cdgo_estdo_no_cnfrmdd_aux


	-- Consultar el detalle de la gestión del auditor para devolver las NCs marcadas con anterioridad
	Update     ce 
	Set        estdo = 1
	From       #CausasEstados ce
	Inner Join BDCna.gsa.tbASResultadoDetalleGestionAuditor rdg WITH (NOLOCK)
	On         rdg.cnsctvo_cdgo_no_cnfrmdd_vldcn = ce.cnsctvo_cdgo_no_cnfrmdd_vldcn
	Inner Join BDCna.gsa.tbASResultadoGestionAuditor rg WITH (NOLOCK) 
	On         rg.cnsctvo_cdgo_rsltdo_gstn_adtr = rdg.cnsctvo_cdgo_rsltdo_gstn_adtr And 
	           rg.cnsctvo_srvco_slctdo          = @cnsctvo_srvco_slctdo;

	Select cnsctvo_cdgo_no_cnfrmdd_vldcn, dscrpcn_no_cnfrmdd_vldcn, estdo
	From   #CausasEstados

	Drop Table #CausasEstados
END
