USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudIPS]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudIPS
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de IPS como 
					 :	modificación de una solicitud especifica D\>   
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes?
*					 : 30 Se evita el uso de subquerys? En su lugar usar joins D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudIPS] 
AS
  SET NOCOUNT ON
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASIPSSolicitudesAutorizacionServiciosOriginal
		DELETE			IPO
		FROM			BdCNA.gsa.tbASIPSSolicitudesAutorizacionServiciosOriginal IPO
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				IPO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASIPSSolicitudesAutorizacionServicios
		DELETE			IPP
		FROM			BdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios	 IPP
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				IPP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- INSERTAR INFORMACIÓN DE IPS EN TABLA DE PROCESO tbASIPSSolicitudesAutorizacionServicios 	
		MERGE INTO			BdCNA.gsa.tbASIPSSolicitudesAutorizacionServicios
		USING (SELECT		IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
							,TIP.id
							,TIP.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte
							,TIP.nmro_idntfccn_ips_slctnte
							,TIP.cdgo_intrno
							,TIP.nmro_indctvo_prstdr
							,TIP.cnsctvo_cdgo_dprtmnto_prstdr
							,TIP.cnsctvo_cdgo_cdd_prstdr
							,TIP.cnsctvo_ips_slctd_orgn
							,TIP.adscrto
							,TIP.nmro_unco_idntfccn_prstdr
							,TIP.usro_crcn
				FROM		#TMod_IPS			TIP WITH (NOLOCK)		
				INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
				ON			TIP.id = IDS.id) AS IPS
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT (cnsctvo_slctd_atrzcn_srvco
				,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte
				,nmro_idntfccn_ips_slctnte
				,cdgo_intrno
				,nmro_indctvo_prstdr
				,cnsctvo_cdgo_dprtmnto_prstdr
				,cnsctvo_cdgo_cdd_prstdr
				,cnsctvo_ips_slctd_orgn
				,adscrto
				,nmro_unco_idntfccn_prstdr
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn)
		VALUES	(IPS.cnsctvo_slctd_atrzcn_srvco_rcbdo
				,IPS.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte
				,IPS.nmro_idntfccn_ips_slctnte
				,IPS.cdgo_intrno
				,IPS.nmro_indctvo_prstdr
				,IPS.cnsctvo_cdgo_dprtmnto_prstdr
				,IPS.cnsctvo_cdgo_cdd_prstdr
				,IPS.cnsctvo_ips_slctd_orgn
				,IPS.adscrto
				,IPS.nmro_unco_idntfccn_prstdr
				,@Fecha_Actual	--fcha_crcn
				,IPS.usro_crcn
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,IPS.usro_crcn	--usro_ultma_mdfccn
				)
		OUTPUT	inserted.cnsctvo_ips_slctd_atrzcn_srvco
				,IPS.Id
		INTO	#IdIPS_Mod;  
		-- INSERTAR INFORMACIÓN DE IPS EN TABLA ORIGINAL tbASIPSSolicitudesAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASIPSSolicitudesAutorizacionServiciosOriginal(							
					cnsctvo_ips_slctd_atrzcn_srvco
					,cnsctvo_slctd_atrzcn_srvco
					,nmbre_ips
					,nmbre_scrsl
					,cdgo_tpo_idntfccn_ips_slctnte
					,nmro_idntfccn_ips_slctnte
					,dgto_vrfccn
					,cdgo_hbltcn
					,cdgo_intrno
					,drccn_prstdr
					,tlfno_prstdr
					,cdgo_dprtmnto_prstdr
					,cdgo_cdd_prstdr
					,nmro_unco_idntfccn_prstdr
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)	
		SELECT		 IDI.cnsctvo_ips_slctd_atrzcn_srvco
					,IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,IPS.nmbre_ips		
					,IPS.nmbre_scrsl							
					,IPS.cdgo_tpo_idntfccn_ips_slctnte
					,IPS.nmro_idntfccn_ips_slctnte				
					,IPS.dgto_vrfccn								
					,IPS.cdgo_hbltcn								
					,IPS.cdgo_intrno
					,IPS.drccn_prstdr								
					,IPS.tlfno_prstdr								
					,IPS.cdgo_dprtmnto_prstdr	
					,IPS.cdgo_cdd_prstdr
					,IPS.nmro_unco_idntfccn_prstdr		
					,@Fecha_Actual --fcha_crcn
					,IPS.usro_crcn
					,@Fecha_Actual --fcha_ultma_mdfccn
					,IPS.usro_crcn --usro_ultma_mdfccn
		FROM		#TMod_IPS			IPS WITH (NOLOCK)
		INNER JOIN	#IdIPS_Mod			IDI WITH (NOLOCK)
		ON			IPS.id = IDI.idXML
		INNER JOIN	#TMod_Solicitudes	IDS WITH (NOLOCK)
		ON			IPS.id = IDS.id
	END



GO
