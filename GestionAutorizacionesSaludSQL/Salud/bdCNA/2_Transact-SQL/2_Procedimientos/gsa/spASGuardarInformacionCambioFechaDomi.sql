USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarInformacionCambioFechaDomi]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionCambioFechaDomi
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													 D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones		DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*---------------------------------------------------------------------------------*/
/*
Begin
Declare @cdgo_rsltdo udtconsecutivo, @mnsje_rsltdo udtdescripcion;
Exec [gsa].[spASGuardarInformacionCambioFechaDomi]
										'<gestionDomiciliariaDireccionamiento>
																				<motivos>
																					<codigoMotivo>1</codigoMotivo>
																				 </motivos>
																				<motivos>
																					<codigoMotivo>2</codigoMotivo>
																				 </motivos>
																				<motivos>
																					<codigoMotivo>3</codigoMotivo>
																				 </motivos>
																				 <fecha>13/04/2017</fecha>
																				 <codigoInterno>91228</codigoInterno>
																				 <justificacion>Esta es una prueba del direccionamiento</justificacion>
																				 <consecutivoPrestacion>1909</consecutivoPrestacion>
																				 <usuarioGestion>user_1</usuarioGestion>
																			 </gestionDomiciliariaDireccionamiento>', @cdgo_rsltdo, @mnsje_rsltdo
Select  @cdgo_rsltdo, @mnsje_rsltdo
End
					*/
ALTER PROCEDURE [gsa].[spASGuardarInformacionCambioFechaDomi]
@cnsctvs_mtvo_cmbo_fcha nvarchar(max),
@cdgo_rsltdo			udtConsecutivo  output,
@mnsje_rsltdo			udtDescripcion	output
AS
BEGIN
	SET NOCOUNT ON;
	Declare @xml_cnsctvs_mtvo_drccnmnto		xml,
			@fechaProgramacion				datetime,
			@justificacion					udtObservacion,
			@consecutivoPrestacion			udtConsecutivo,
			@usuarioGestion					udtUsuario,
			@mensaje_ok						udtDescripcion = 'Proceso ejecutado satisfactoriamente',
			@mensaje_error					udtDescripcion = 'No existe Fecha Entrega para la Prestación',
			@codigo_ok						int			   = 0,
			@codigo_error					int			   = -1,
			@fcha_actl						DATETIME	   = GETDATE(),
			@fechaTem						varchar(15),
			@vlr_ip_cnstnte					INT			   = 0,
			@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo	,
			@fcha_estmda_entrga				datetime,
			@ldOrgn_mdfccn					udtdescripcion,
			@elmnto_orgn_mdfccn				udtdescripcion,
			@dto_elmnto_orgn_mdfccn			udtdescripcion;
	




	Create table #motivosProgramacionEsperdos(
			codigoMotivo			udtConsecutivo,
	);

	DECLARE @fcha_estmda_entrga_vlr TABLE (
		fcha_estmda_entrga_updte datetime
    )

	 CREATE TABLE #tmptrazamodificacion (
		cnsctvo_slctd_atrzcn_srvco		udtconsecutivo,
		cnsctvo_srvco_slctdo			udtconsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo	udtconsecutivo,
		cnsctvo_cdgo_mtvo_csa			udtconsecutivo,
		fcha_mdfccn						DATETIME,
		orgn_mdfccn						VARCHAR(100),
		vlr_antrr						VARCHAR(20),
		vlr_nvo							VARCHAR(20),
		usro_mdfccn						udtusuario,
		obsrvcns						udtobservacion,
		nmro_ip							VARCHAR(12),
		fcha_crcn						DATETIME,
		usro_crcn						udtusuario,
		fcha_ultma_mdfccn				DATETIME,
		usro_ultma_mdfccn				udtusuario,
		elmnto_orgn_mdfccn				udtDescripcion,
		dto_elmnto_orgn_mdfccn			udtDescripcion,
		dscrpcn_vlr_antrr				udtDescripcion,
		dscrpcn_vlr_nvo					udtDescripcion
	  )


	Set @mnsje_rsltdo = @mensaje_ok;
	Set @cdgo_rsltdo  = @codigo_ok;
	Set @xml_cnsctvs_mtvo_drccnmnto = @cnsctvs_mtvo_cmbo_fcha;
	Set	@ldOrgn_mdfccn					= 'tbASResultadoFechaEntrega.fcha_estmda_entrga'
	Set	@elmnto_orgn_mdfccn				= 'Prestación'
	Set	@dto_elmnto_orgn_mdfccn			= 'Cambio Fecha Entrega';
	
	SELECT @fechaTem				= pref.value('(fecha/text())[1]','varchar(15)'),
           @justificacion			= pref.value('(justificacion/text())[1]', 'udtObservacion'),
		   @consecutivoPrestacion	= pref.value('(consecutivoPrestacion/text())[1]', 'udtConsecutivo'),
		   @usuarioGestion			= pref.value('(usuarioGestion/text())[1]', 'udtUsuario')
    FROM   @xml_cnsctvs_mtvo_drccnmnto.nodes('/gestionDomiciliariaDireccionamiento') AS xml_slctd(Pref);

	Insert Into #motivosProgramacionEsperdos(codigoMotivo)
	SELECT pref.value('(codigoMotivo/text())[1]','udtConsecutivo')       AS codigoMotivo
    FROM   @xml_cnsctvs_mtvo_drccnmnto.nodes('/gestionDomiciliariaDireccionamiento//motivos') AS xml_slctd(Pref);

	set @fechaProgramacion = convert(datetime, @fechaTem, 105);
	
	Set @cnsctvo_slctd_atrzcn_srvco =	( SELECT TOP 1 rf.cnsctvo_slctd_atrzcn_srvco
										  FROM BDCna.gsa.tbASResultadoFechaEntrega rf WITH(NOLOCK)
										  Where rf.cnsctvo_srvco_slctdo = @consecutivoprestacion
										 ) 

	BEGIN TRY 
	  IF @cnsctvo_slctd_atrzcn_srvco IS NOT NULL
	  BEGIN
			Update rd Set rd.fcha_estmda_entrga = @fechaProgramacion,
						  rd.fcha_ultma_mdfccn  = @fcha_actl,
						  rd.usro_ultma_mdfccn		= @usuariogestion
			OUTPUT DELETED.fcha_estmda_entrga INTO @fcha_estmda_entrga_vlr
			From BDCna.gsa.tbASResultadoFechaEntrega rd WITH(NOLOCK)
			where rd.cnsctvo_srvco_slctdo = @consecutivoPrestacion

			Set @fcha_estmda_entrga = (Select CDG.fcha_estmda_entrga_updte From @fcha_estmda_entrga_vlr CDG)
	  END
	  ELSE
	  BEGIN

		Set @cnsctvo_slctd_atrzcn_srvco = ( Select top 1 ss.cnsctvo_slctd_atrzcn_srvco from 
										  BDCna.gsa.tbASServiciosSolicitados ss WITH(NOLOCK)
										  Where ss.cnsctvo_srvco_slctdo = @consecutivoprestacion)

		INSERT INTO BDCna.gsa.tbASResultadoFechaEntrega(
			 cnsctvo_slctd_atrzcn_srvco
			,cnsctvo_srvco_slctdo
			,cnsctvo_cdgo_grpo_entrga
			,fcha_estmda_entrga
			,fcha_rl_entrga
			,prrdd_admnstrtva
			,fcha_crcn
			,usro_crcn
			,fcha_ultma_mdfccn
			,usro_ultma_mdfccn
		)VALUES(
			@cnsctvo_slctd_atrzcn_srvco,
			@consecutivoPrestacion,
			0,
			@fechaProgramacion,
			NULL,
			'N',
			@fcha_actl,
			@usuariogestion,
			@fcha_actl,
			@usuariogestion
		)

		Set @fcha_estmda_entrga = @fechaProgramacion;
	  END

		--Se establece la información de la trazabilidad.
		INSERT INTO #tmptrazamodificacion (cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo,
		cnsctvo_cncpto_prcdmnto_slctdo,
		cnsctvo_cdgo_mtvo_csa,
		fcha_mdfccn,
		orgn_mdfccn,
		vlr_antrr,
		vlr_nvo,
		usro_mdfccn,
		obsrvcns,
		nmro_ip,
		fcha_crcn,
		usro_crcn,
		fcha_ultma_mdfccn,
		usro_ultma_mdfccn,
		elmnto_orgn_mdfccn,
		dto_elmnto_orgn_mdfccn,
		dscrpcn_vlr_antrr,
		dscrpcn_vlr_nvo)
		  SELECT
			@cnsctvo_slctd_atrzcn_srvco,
			@consecutivoprestacion,
			NULL,
			mdr.codigomotivo,
			@fcha_actl,
			@ldOrgn_mdfccn,
			@fcha_estmda_entrga,
			@fechaProgramacion,
			@usuariogestion,
			@justificacion,
			@vlr_ip_cnstnte,
			@fcha_actl,
			@usuariogestion,
			@fcha_actl,
			@usuariogestion,
			@elmnto_orgn_mdfccn,
			@dto_elmnto_orgn_mdfccn,
			@fcha_estmda_entrga,
			@fechaProgramacion
		  FROM #motivosProgramacionEsperdos mdr
			   

		--Se invoca el SP de creación de trazabilidad
		EXEC bdcna.gsa.spasguardartrazamodificacion
		END TRY
	BEGIN CATCH
		SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
			   				 'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
							 'Message:' + ERROR_MESSAGE() + CHAR(13) +
							 'Procedure:' + ERROR_PROCEDURE();
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

	Drop Table #motivosProgramacionEsperdos;
END

GO
