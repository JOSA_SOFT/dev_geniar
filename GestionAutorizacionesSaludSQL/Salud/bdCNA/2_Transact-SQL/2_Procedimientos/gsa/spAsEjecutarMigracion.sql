USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsEjecutarMigracion]    Script Date: 09/02/2017 07:37:50 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsEjecutarMigracion							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\DM	
                                    Procedimiento almaceando que ejecuta los procesos de migracion entre los sistemas
									de MEGA y SIPRES									
							  DM\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P P\>  													
* Fecha Creacion			: <\FC 2017/02/02 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Victor Hugo Gil Ramos AM\>  													
* Descripcion			: <\DM 
                                Se actualiza el procedimiento para que cierre las presolicitudes
								que han sido gestionadas 
                           DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/08 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramire - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega llamado a proceso masivo de creacion de empresas por solicitud para que
							   se creen las empresas para las solicitudes que no la tengan. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/08 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

--Exec bdCNA.gsa.spAsEjecutarMigracion null

ALTER Procedure [gsa].[spAsEjecutarMigracion]
	@fcha_mgra	Date				
As
Begin
	
	Set NoCount On

	Declare	@cdgo_slda         UdtCodigo,
			@mnsge_slda        UdtDescripcion,
			@cdgo_errr		   Char(2),
			@vlr_nll		   Varchar(1);
		
	Set @cdgo_errr = 'ET';
	Set @vlr_nll = Null;

	
	-- Si no ingresa fecha, se pone la fecha actual por defecto.
	If(@fcha_mgra Is Null)
		Begin
			Set @fcha_mgra = Convert(DATE, GETDATE())
		End  
	
	Begin try

	    --Actualiza el estado de las presolicitudes
		Exec bdCNA.gsa.spASActualizarEstadoCerradoPresolicitudes

		-- Se ejecuta el proceso de creacion de empresas de forma masiva
		Exec bdCNA.gsa.spASEjecutarCreacionEmpresaAfiliadoSolicitud  @vlr_nll, @vlr_nll, @mnsge_slda Output, @cdgo_slda Output

		If(@cdgo_errr = @cdgo_slda)
		Begin
			-- Si la creacion de empresas tiene errores se cancela el proceso.
			Select @cdgo_slda, @mnsge_slda
			Return;
		End

		-- Migracion de Sipres a Mega
		Exec bdCNA.gsa.spAsMigracionEstadosSipresMega @fcha_mgra, @cdgo_slda Output, @mnsge_slda Output;
			
		If(@cdgo_errr = @cdgo_slda)
		Begin
			-- Si la migracion tiene errores se cancela el proceso.
			Select @cdgo_slda, @mnsge_slda
			Return;
		End

		-- Migracion de Mega a Sipres
		Exec bdCNA.gsa.spASEjecutarIntegracionSipres @fcha_mgra;		
		
	
	End try	
	Begin Catch
		 Set @cdgo_slda = @cdgo_errr;
		 Set @mnsge_slda = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
						  'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
						  'Message:', ERROR_MESSAGE(), CHAR(13),
						  'Procedure:', ERROR_PROCEDURE());

	End Catch
	
	Select @cdgo_slda cdgo_rsltdo, @mnsge_slda mnsje_rsltdo

End



