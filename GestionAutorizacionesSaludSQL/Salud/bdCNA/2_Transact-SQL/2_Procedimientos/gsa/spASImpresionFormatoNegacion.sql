USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASImpresionFormatoNegacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASImpresionFormatoNegacion
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información requerida para la generacion+
						   del formato de negacion 
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
						   @cnsctvo_srvco_slctdo       udtConsecutivo
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 05/05/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*--------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASImpresionFormatoNegacion] 840, 1990, 'user1'
--exec [gsa].[spASImpresionFormatoNegacion] 2242, 3504, 'user1'
--exec [gsa].[spASImpresionFormatoNegacion] 3664, 4231, 'user1'
--exec [gsa].[spASImpresionFormatoNegacion] 3645, 4195, 'user1'

ALTER PROCEDURE [gsa].[spASImpresionFormatoNegacion] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
   @cnsctvo_srvco_slctdo       udtConsecutivo,
   @usro_ngcn                  udtUsuario
AS

Begin
   SET NOCOUNT ON
   
   Declare @ldFechaActual                    Datetime      , 
           @lcFormato                        Varchar(10)   ,
           @lcAux                            Char(1)       ,
		   @lcAuxA                           Char(2)       ,
		   @lcVacio                          Char(1)       ,
		   @lnEstdo_srvco_slctdo             udtConsecutivo,
		   @lnpln_ps                         udtConsecutivo,
		   @lnpln_pc                         udtConsecutivo,
		   @lnpln_sb                         udtConsecutivo,
		   @lnestdo_drcho                    udtConsecutivo, ---Estado de Derecho Activo
	       @lnestdo_drcho_rt                 udtConsecutivo, ---Estado de Derecho Retirado
		   @lnestdo_drcho_sp                 udtConsecutivo, ---Estado de Derecho Suspendido
		   @lncnsctvo_cdgo_tpo_frmto_dscrga  udtConsecutivo, ---2 - Formato Negacion 
		   @lncnsctvo_scnca_tpo_frmto_dscrga udtConsecutivo,
		   @lnid                             udtConsecutivo


   Create
   Table  #tempFundamentosLegales(cnsctvo_srvco_slctdo       udtConsecutivo,
                                  cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
								  cnsctvo_cdgo_fndmnto_lgl   udtConsecutivo,
								  dscrpcn_fndmnto_lgl        udtDescripcion   Default '' 
                                 )
   
   Create
   Table  #tempAlternativas(id                         udtConsecutivo Identity(1,1),
                            cnsctvo_srvco_slctdo       udtConsecutivo              ,
                            cnsctvo_slctd_atrzcn_srvco udtConsecutivo              ,							
							altrntva                   udtDescripcion   Default '' 
                            )

   Create
   Table  #tempNegacionAfiliado(cnsctvo_slctd_atrzcn_srvco      udtConsecutivo              , 
                                cnsctvo_srvco_slctdo            udtConsecutivo              , 
								cnsctvo_cdgo_pln                udtConsecutivo              ,
	                            nmro_idntfccn_afldo             udtNumeroIdentificacionLargo, 
								nmro_unco_idntfccn_afldo        udtConsecutivo              , 
								cnsctvo_cdgo_tpo_cntrto         udtConsecutivo              ,
								nmro_cntrto                     udtNumeroFormulario         , 
								cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo              , 
								cnsctvo_cdgo_tpo_vnclcn_afldo   udtConsecutivo              ,
								cnsctvo_cdgo_estdo_drcho        udtConsecutivo              , 
								fcha_crcn                       Datetime                    , 
								cnsctvo_cdgo_srvco_slctdo       udtConsecutivo              ,
								cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo              ,
								cnsctvo_scnca_tpo_frmto_dscrga  udtConsecutivo
							   )

   Create 
   Table  #informacionNegacion(cnsctvo_srvco_slctdo            udtConsecutivo              ,
                               cnsctvo_slctd_atrzcn_srvco      udtConsecutivo              ,
                               cnsctvo_cdgo_tpo_idntfccn       udtConsecutivo              ,
                               cdgo_tpo_idntfccn			   Char(3)                     ,
                               nmro_idntfccn			 	   udtNumeroIdentificacionLargo,
                               nmro_unco_idntfccn_afldo	       udtConsecutivo              ,	                  
                               prmr_aplldo	                   udtApellido                 ,
                               sgndo_aplldo		               udtApellido                 ,
                               prmr_nmbre		               udtNombre                   ,
                               sgndo_nmbre		               udtNombre                   ,
							   tlfno_afldo                     udtTelefono                 ,
						       smns_aflcn                      udtConsecutivo              ,						                     
                               cnsctvo_cdgo_pln			       udtConsecutivo              ,			
                               dscrpcn_pln				       udtDescripcion              ,
							   cnsctvo_cdgo_estdo_drcho        udtConsecutivo,
							   cnsctvo_cdgo_tpo_cntrto         udtConsecutivo              ,
						       nmro_cntrto                     udtNumeroFormulario         ,
						       cnsctvo_cdgo_tpo_vnclcn_afldo   udtConsecutivo              ,
							   dscrpcn_cdd_rsdnca_afldo        udtDescripcion              ,  
							   cnsctvo_cdgo_dprtmnto_afldo     udtConsecutivo              ,  
							   dscrpcn_dprtmnto_afldo		   udtDescripcion              , 
							   cdgo_cdfccn	                   Varchar(11)                 ,       
                               dscrpcn_cdfccn                  udtdescripcion              ,  
							   cnsctvo_cdgo_srvco_slctdo       udtConsecutivo              ,						  
							   fcha_crcn                       Datetime                    ,
							   mrca_pln_usro                   udtLogico       Default ''  ,  
							   mrca_pln_pc_usro                udtLogico       Default ''  , 
							   mrca_pln_sb_usro                udtLogico       Default ''  ,  
							   mrca_estdo_drcho                udtLogico       Default ''  ,  
							   mrca_estdo_drcho_rt             udtLogico       Default ''  , 
							   mrca_estdo_drcho_sp             udtLogico       Default ''  ,  
							   cnsctvo_cdgo_tpo_pln            udtConsecutivo              ,
							   dscrpcn_fndmnto_lgl             udtDescripcion  Default ''  ,
							   cnsctvo_cdgo_cdd_rsdnca         udtConsecutivo              ,
							   nmbre_cmplto                    udtdescripcion  Default ''  ,
							   usro_ngcn                       udtdescripcion  Default ''  ,
							   jstfccn_gstn_adtr               Varchar(2000)   Default ''  ,
							   usro_lgn_adtr                   udtUsuario      Default ''  ,
							   dscrpcn_crgo                    udtdescripcion  Default ''  ,
							   cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo              ,
							   altrntva                        Varchar(200)    Default ''  ,
							   rgstro_mdco                     Varchar(20)     Default ''  ,
							   cnsctvo_scnca_tpo_frmto_dscrga  udtConsecutivo              ,
							   id_altrntva                     udtConsecutivo              ,
							   cnsctvo_cdgo_crgo_usro          udtConsecutivo
                              )
	
	Set @ldFechaActual                   = getDate()
	Set @lcFormato                       = 'yyyy/MM/dd'
	Set @lcAux                           = ' '
	Set @lcAuxA                          = '. '
	Set @lcVacio                         = ''
	Set @lnEstdo_srvco_slctdo            = 8 ---NO AUTORIZADA
	Set @lnpln_ps                        = 1 ---Plan POS
	Set @lnpln_pc                        = 2 ---Tipo Plan Pac
	Set @lnpln_sb                        = 9 ---Plan POS SUBSIDIADO
	Set @lnestdo_drcho                   = 1 ---Estado de Derecho Activo
	Set @lnestdo_drcho_rt                = 7 ---Estado de Derecho Retirado 
    Set @lnestdo_drcho_sp                = 8 ---Estado de Derecho Suspendido
	Set @lncnsctvo_cdgo_tpo_frmto_dscrga = 2 ---2 - Formato Negacion
	Set @lnid                            = 0

	---Se inserta la informacion de la solicitud, prestacion y del afiliado
	Insert 
	Into       #tempNegacionAfiliado(cnsctvo_slctd_atrzcn_srvco     , cnsctvo_srvco_slctdo           , cnsctvo_cdgo_pln             ,
	                                 nmro_idntfccn_afldo            , nmro_unco_idntfccn_afldo       , cnsctvo_cdgo_tpo_cntrto      ,
									 nmro_cntrto                    , cnsctvo_cdgo_tpo_idntfccn_afldo, cnsctvo_cdgo_tpo_vnclcn_afldo,
									 cnsctvo_cdgo_estdo_drcho       , fcha_crcn                      , cnsctvo_cdgo_srvco_slctdo    ,
									 cnsctvo_cdgo_estdo_srvco_slctdo
	                                )     
	Select     a.cnsctvo_slctd_atrzcn_srvco     , b.cnsctvo_srvco_slctdo           , i.cnsctvo_cdgo_pln             ,
	           i.nmro_idntfccn_afldo            , i.nmro_unco_idntfccn_afldo       , i.cnsctvo_cdgo_tpo_cntrto      ,
			   i.nmro_cntrto                    , i.cnsctvo_cdgo_tpo_idntfccn_afldo, i.cnsctvo_cdgo_tpo_vnclcn_afldo,
			   i.cnsctvo_cdgo_estdo_drcho       , a.fcha_crcn                      , b.cnsctvo_cdgo_srvco_slctdo    ,
			   b.cnsctvo_cdgo_estdo_srvco_slctdo  
    From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    Inner Join bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)	
    On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios i WITH (NOLOCK)
	On         i.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco	
	Where      a.cnsctvo_slctd_atrzcn_srvco      = @cnsctvo_slctd_atrzcn_srvco
	And        b.cnsctvo_srvco_slctdo            = @cnsctvo_srvco_slctdo
	And        b.cnsctvo_cdgo_estdo_srvco_slctdo = @lnEstdo_srvco_slctdo

	--Actualiza el consecutivo de la secuencia del formato de negacion
	Select     @lncnsctvo_scnca_tpo_frmto_dscrga = d.cnsctvo_scnca_tpo_frmto_dscrga 
	From       #tempNegacionAfiliado a
	Inner Join bdCNA.gsa.tbASConsecutivosxTiposFormatoDescarga d WITH(NOLOCK)
	On         d.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Where      d.cnsctvo_cdgo_tpo_frmto_dscrga = @lncnsctvo_cdgo_tpo_frmto_dscrga
	
	--Se valida si existe el consecutivo de la secuencia del formato de negacion
	IF (@lncnsctvo_scnca_tpo_frmto_dscrga IS NULL)
		Begin

		   SELECT @lncnsctvo_scnca_tpo_frmto_dscrga = NEXT VALUE FOR bdCNA.gsa.seqConsecutivoNegacionServicio; 
		   
		   Insert 
		   Into   bdCNA.gsa.tbASConsecutivosxTiposFormatoDescarga(cnsctvo_srvco_slctdo, cnsctvo_cdgo_tpo_frmto_dscrga, cnsctvo_scnca_tpo_frmto_dscrga,
		                                                          fcha_crcn           , usro_crcn                    , fcha_ultma_mdfccn             ,
																  usro_ultma_mdfccn 
		                                                         )
		   Values (@cnsctvo_srvco_slctdo, @lncnsctvo_cdgo_tpo_frmto_dscrga, @lncnsctvo_scnca_tpo_frmto_dscrga,
		           @ldFechaActual       , @usro_ngcn                      , @ldFechaActual                   ,
				   @usro_ngcn
		          )  
		End
	
	--Actualiza el consecutivo de la secuencia del formato de negacion
	Update #tempNegacionAfiliado
	Set    cnsctvo_scnca_tpo_frmto_dscrga = @lncnsctvo_scnca_tpo_frmto_dscrga
	Where  cnsctvo_srvco_slctdo           = @cnsctvo_srvco_slctdo	
	
	--inserta los fundamentos legales asociados a los servicios
	Insert 
	Into       #tempFundamentosLegales(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_cdgo_fndmnto_lgl)
	Select     b.cnsctvo_slctd_atrzcn_srvco, b.cnsctvo_srvco_slctdo, c.cnsctvo_cdgo_fndmnto_lgl
	From       bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)	
	Inner Join bdCNA.gsa.tbASFundamentosLegalesxServiciosSolicitados c WITH (NOLOCK)
	On         c.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
	Where      c.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

	--actualiza la descripcion de los fundamentos legales
	Update     #tempFundamentosLegales
	Set        dscrpcn_fndmnto_lgl = c.dscrpcn_fndmnto_lgl
	From       #tempFundamentosLegales a
	Inner Join bdCNA.prm.tbASFundamentosLegales_Vigencias c WITH (NOLOCK)
	On         c.cnsctvo_cdgo_fndmnto_lgl = a.cnsctvo_cdgo_fndmnto_lgl
	Where      @ldFechaActual Between c.inco_vgnca And c.fn_vgnca      

	--inserta las alternativas de autorizacion del servicio
	Insert
	Into       #tempAlternativas(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, altrntva)
	Select     b.cnsctvo_slctd_atrzcn_srvco, b.cnsctvo_srvco_slctdo, c.altrntva
	From       bdCNA.gsa.tbASServiciosSolicitados b WITH (NOLOCK)	
	Inner Join bdCNA.gsa.tbASResultadoAlternativasGestionAuditor c WITH (NOLOCK)
	On         c.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo    
	Where      c.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo

	--inserta en la tabla tempora la informacion del afialiado y los fundamentos legales de la negacion de la solicitud
	Insert 
	Into       #informacionNegacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo           , cnsctvo_cdgo_pln             ,
	                                nmro_idntfccn             , nmro_unco_idntfccn_afldo       , cnsctvo_cdgo_tpo_cntrto      ,
									nmro_cntrto               , cnsctvo_cdgo_tpo_idntfccn      , cnsctvo_cdgo_tpo_vnclcn_afldo,
									cnsctvo_cdgo_estdo_drcho  , fcha_crcn                      , cnsctvo_cdgo_srvco_slctdo    ,
									dscrpcn_fndmnto_lgl       , cnsctvo_cdgo_estdo_srvco_slctdo, id_altrntva                  , 
									altrntva
	                               )     
	Select     a.cnsctvo_slctd_atrzcn_srvco          , a.cnsctvo_srvco_slctdo           , a.cnsctvo_cdgo_pln             ,
	           a.nmro_idntfccn_afldo                 , a.nmro_unco_idntfccn_afldo       , a.cnsctvo_cdgo_tpo_cntrto      ,
			   a.nmro_cntrto                         , a.cnsctvo_cdgo_tpo_idntfccn_afldo, a.cnsctvo_cdgo_tpo_vnclcn_afldo,
			   a.cnsctvo_cdgo_estdo_drcho            , a.fcha_crcn                      , a.cnsctvo_cdgo_srvco_slctdo    ,
			   ISNULL(b.dscrpcn_fndmnto_lgl,@lcVacio), a.cnsctvo_cdgo_estdo_srvco_slctdo, @lnid                          ,
			   @lcVacio
	From       #tempNegacionAfiliado a
	Inner Join #tempFundamentosLegales b
	On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco And
	           b.cnsctvo_srvco_slctdo       = a.cnsctvo_srvco_slctdo
	Union All
	Select     a.cnsctvo_slctd_atrzcn_srvco          , a.cnsctvo_srvco_slctdo           , a.cnsctvo_cdgo_pln             ,
	           a.nmro_idntfccn_afldo                 , a.nmro_unco_idntfccn_afldo       , a.cnsctvo_cdgo_tpo_cntrto      ,
			   a.nmro_cntrto                         , a.cnsctvo_cdgo_tpo_idntfccn_afldo, a.cnsctvo_cdgo_tpo_vnclcn_afldo,
			   a.cnsctvo_cdgo_estdo_drcho            , a.fcha_crcn                      , a.cnsctvo_cdgo_srvco_slctdo    ,
			   @lcVacio                              , a.cnsctvo_cdgo_estdo_srvco_slctdo, id                             , 
			   c.altrntva
	From       #tempNegacionAfiliado a
	Inner Join #tempAlternativas c
	On         c.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

	--Actualiza el consecutivo de la secuencia del formato de negacion
	Update     #informacionNegacion
	Set        cnsctvo_scnca_tpo_frmto_dscrga = b.cnsctvo_scnca_tpo_frmto_dscrga
	From       #informacionNegacion a
	Inner Join #tempNegacionAfiliado b
	On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

	--actualiza el codigo del tipo de identificacion del afiliado
	Update     #informacionNegacion
	Set        cdgo_tpo_idntfccn = t.cdgo_tpo_idntfccn	           
	From       #informacionNegacion a
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias t WITH(NOLOCK)
	On         t.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn
	Where      @ldFechaActual Between t.inco_vgnca And t.fn_vgnca

	---Se actualiza la descripcion del plan
	Update      #informacionNegacion
	Set         dscrpcn_pln          = b.dscrpcn_pln         ,
	            cnsctvo_cdgo_tpo_pln = b.cnsctvo_cdgo_tpo_pln
	From        #informacionNegacion a
	Inner Join  bdAfiliacionValidador.dbo.tbPlanes_Vigencias b WITH (NOLOCK)
	On          b.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	Where       @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

	---Se actualiza la informacion basica del afiliado
	Update      #informacionNegacion
	Set         prmr_aplldo	            = Ltrim(Rtrim(c.prmr_aplldo))               ,				             
                sgndo_aplldo		    = Ltrim(Rtrim(c.sgndo_aplldo))              ,                   
                prmr_nmbre		        = Ltrim(Rtrim(c.prmr_nmbre))                ,                
                sgndo_nmbre		        = Ltrim(Rtrim(c.sgndo_nmbre))               , 
				nmbre_cmplto            = CONCAT(LTRIM(RTRIM(c.prmr_nmbre)) , @lcAux, 
								                 LTRIM(RTRIM(c.sgndo_nmbre))
												)                                   ,				
				tlfno_afldo             = IsNull(c.tlfno_rsdnca, @lcVacio)          ,			
				smns_aflcn              = c.smns_ctzds                              ,
				cnsctvo_cdgo_cdd_rsdnca = c.cnsctvo_cdgo_cdd_rsdnca
	From        #informacionNegacion a	
	Inner Join  bdAfiliacionValidador.dbo.tbBeneficiariosValidador c WITH (NOLOCK)
	On          c.cnsctvo_cdgo_tpo_cntrto  = a.cnsctvo_cdgo_tpo_cntrto  And
	            c.nmro_cntrto              = a.nmro_cntrto              And
				c.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn_afldo
	Where       @ldFechaActual Between c.inco_vgnca_bnfcro And c.fn_vgnca_bnfcro
	
	--actualiza la informacion de la ciudad del afiliado
	Update     #informacionNegacion
	Set        dscrpcn_cdd_rsdnca_afldo    = b.dscrpcn_cdd          ,	        
	           cnsctvo_cdgo_dprtmnto_afldo = b.cnsctvo_cdgo_dprtmnto                              
	From       #informacionNegacion a
	Inner Join bdSiSalud.dbo.tbCiudades_vigencias b WITH(NOLOCK)
	On         b.cnsctvo_cdgo_cdd = a.cnsctvo_cdgo_cdd_rsdnca          
	Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca	

	--actualiza la informacion del departamento del afiliado
	Update     #informacionNegacion	   
	Set        dscrpcn_dprtmnto_afldo = b.dscrpcn_dprtmnto
	From       #informacionNegacion a
	Inner Join bdAfiliacionValidador.dbo.tbDepartamentos_vigencias b WITH(NOLOCK)	
	On         b.cnsctvo_cdgo_dprtmnto = a.cnsctvo_cdgo_dprtmnto_afldo	    
    Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca  

	/*Se actualiza el codigo y la descripcion del servicio*/
    Update     #informacionNegacion
    Set        cdgo_cdfccn    = c.cdgo_cdfccn   ,
	           dscrpcn_cdfccn = c.dscrpcn_cdfccn
    From       #informacionNegacion a
    Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
    On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo	

	
	/*Se actualiza la justificacion de auditor para la negacion de la prestacion*/
	Update     #informacionNegacion
	Set        jstfccn_gstn_adtr = b.jstfccn_gstn_adtr,
	           usro_lgn_adtr     = b.usro_lgn_adtr
	From       #informacionNegacion a
	Inner Join bdCNA.gsa.tbASResultadoGestionAuditor b WITH (NOLOCK)
	On         a.cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_no_cnfrmdd And
	           a.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo

 	--si actualiza el nombre del usuario que niega la prestacion asociada a la solicitud      
	UPDATE     #informacionNegacion        
	SET        usro_ngcn     = CONCAT(LTRIM(RTRIM(b.prmr_nmbre_usro))  , @lcAux ,
	                                  LTRIM(RTRIM(b.sgndo_nmbre_usro)) , @lcAux , 
					                  LTRIM(RTRIM(b.prmr_aplldo_usro)) , @lcAux , 
								      LTRIM(RTRIM(b.sgndo_aplldo_usro))
									),			            
	           cnsctvo_cdgo_crgo_usro = b.cnsctvo_cdgo_crgo_usro   
    FROM       #informacionNegacion a        
	Inner Join bdseguridad.dbo.tbusuarios b WITH(NOLOCK) 
	On         b.lgn_usro = a.usro_lgn_adtr
	
	
	--si actualiza el cargo del usuario que niega la prestacion asociada a la solicitud  
	UPDATE     #informacionNegacion 
	Set        dscrpcn_crgo   = c.dscrpcn_crgo_ss 
	FROM       #informacionNegacion a	
	Left  Join bdSiSalud.dbo.tbCargosSOS_Vigencias c WITH(NOLOCK) 
	On         c.cnsctvo_cdgo_crgo_ss = a.cnsctvo_cdgo_crgo_usro       
	Where      @ldFechaActual Between c.inco_vgnca And c.fn_vgnca
	
	--si actualiza el registro medico del auditor que niega el servicio
	UPDATE     #informacionNegacion  
	Set        rgstro_mdco = c.rgstro_mdco
	From       #informacionNegacion a
	Inner Join bdSiSalud.dbo.tbAuditoresMedicos c WITH(NOLOCK) 
	On         c.lgn_usro = a.usro_lgn_adtr

	--actualiza la marca de los planes
	Update     #informacionNegacion
	Set        mrca_pln_usro    = Case When cnsctvo_cdgo_pln = @lnpln_ps Then 'X' 
	                                   Else @lcVacio 
	                              End,
               mrca_pln_sb_usro = Case When cnsctvo_cdgo_pln = @lnpln_sb Then 'X' 
	                                   Else @lcVacio 
	                              End,  
	           mrca_pln_pc_usro = Case When cnsctvo_cdgo_tpo_pln = @lnpln_pc Then 'X' 
	                                   Else @lcVacio 
	                              End
								  
	--actualiza la marca del estado de afiliacion
	Update     #informacionNegacion
	Set        mrca_estdo_drcho    = Case When cnsctvo_cdgo_estdo_drcho = @lnestdo_drcho Then 'X' 
	                                      Else @lcVacio 
	                                 End,
               mrca_estdo_drcho_rt = Case When cnsctvo_cdgo_estdo_drcho = @lnestdo_drcho_rt Then 'X' 
	                                      Else @lcVacio 
	                                 End,  
	           mrca_estdo_drcho_sp = Case When cnsctvo_cdgo_estdo_drcho = @lnestdo_drcho_sp Then 'X' 
	                                      Else @lcVacio 
	                                 End	
	

	Select  Distinct FORMAT(fcha_crcn, @lcFormato) fcha_crcn, prmr_aplldo                   , sgndo_aplldo    , nmbre_cmplto       ,
	        cdgo_tpo_idntfccn                               , nmro_idntfccn                 , nmro_cntrto     , tlfno_afldo        ,
			dscrpcn_cdd_rsdnca_afldo                        , dscrpcn_dprtmnto_afldo        , mrca_pln_usro   , mrca_pln_sb_usro   ,
			mrca_pln_pc_usro                                , smns_aflcn                    , mrca_estdo_drcho, mrca_estdo_drcho_rt,
			mrca_estdo_drcho_sp                             , cdgo_cdfccn                   , dscrpcn_cdfccn  , jstfccn_gstn_adtr  ,
			dscrpcn_fndmnto_lgl                             , altrntva                      , usro_ngcn       , rgstro_mdco        , 
			IsNull(dscrpcn_crgo, @lcVacio) dscrpcn_crgo     , cnsctvo_scnca_tpo_frmto_dscrga, id_altrntva
	From    #informacionNegacion
	Order By id_altrntva
	
    Drop Table #tempNegacionAfiliado
	Drop Table #tempFundamentosLegales
	Drop Table #tempAlternativas
    Drop Table #informacionNegacion
End

GO
