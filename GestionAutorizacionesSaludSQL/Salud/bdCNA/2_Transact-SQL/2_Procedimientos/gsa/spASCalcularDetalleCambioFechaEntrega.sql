USE BDCna
GO
/****** Object:  StoredProcedure [gsa].[spASCalcularDetalleCambioFechaEntregaOPS]    Script Date: 16/11/2017 09:57:35 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF object_id('gsa.spASCalcularDetalleCambioFechaEntregaOPS') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASCalcularDetalleCambioFechaEntregaOPS AS SELECT 1;'
END
GO

--GRANT EXECUTE on gsa.spASCalcularDetalleCambioFechaEntregaOPS to  autsalud_rol


/*----------------------------------------------------------------------------------------
* Metodo o PRG :     gsa.spASCalcularDetalleCambioFechaEntregaOPS
* Desarrollado por : <\A Ing. Germán Pérez - Geniar S.A.S A\>
* Descripcion  :     <\D 
						 Este procedimiento permite consultar el detalle de las OPS
						 de acuerdo a los criterios seleccionados en la pantalla de
						 'MODIFICAR FECHAS ENTREGA OPS'
					 D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P
					     @lxPlanes             XML tipos de plan seleccionados por el usuario
						 @lxAfiliados		   XML tipos de afiliado seleccionados por el usuario
						 @lcIncapacidad        indica si el usuario tiene incapacidad a la fecha
						 @ldfechaEntregaDesde  fecha estimada de entrega inicial seleccionada por el usuario
						 @ldfechaEntregaHasta  fecha estimada de entrega final seleccionada por el usuario
						 @ldfechaCreacionDesde fecha de creacion inicial seleccionada por el usuario
						 @ldfechaCreacionHasta fecha de creacion final seleccionada por el usuario
						 @lxEstados            XML estados seleccionados por el usuario
						 @lxSedes              XML sedes seleccionadas por el usuario
						 @lxGruposEntrega      XML grupos de entrega seleccionados por el usuario
						 @lnPrestacion         prestacion seleccionad por el usuario
						 @lcEntregaInmediata   indica si los agrupados son con entrega inmediata o no
					 P\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/09/25 FC\>
* Ejemplo: 
    <\EJ
        
EXEC gsa.spASCalcularDetalleCambioFechaEntregaOPS  
	'<plns><pln><dto>1</dto></pln></plns>'
	,'<aflds><afld><dto>1</dto></afld></aflds>', 
	null, 
	null, 
	null,
	'2017-06-01', 
	'2017-07-01'
	,'<estds><estdo><dto>1</dto></estdo></estds>' 
	,'<sds><sde><dto>2</dto></sde><sde><dto>3</dto></sde><sde><dto>4</dto></sde><sde><dto>5</dto></sde><sde><dto>6</dto></sde><sde><dto>7</dto></sde><sde><dto>9</dto></sde><sde><dto>10</dto></sde><sde><dto>12</dto></sde><sde><dto>16</dto></sde></sds>' 
	,'<grpo_entrgs><grpo_entrga><dto>1</dto></grpo_entrga><grpo_entrga><dto>2</dto></grpo_entrga><grpo_entrga><dto>3</dto></grpo_entrga><grpo_entrga><dto>4</dto></grpo_entrga><grpo_entrga><dto>5</dto></grpo_entrga><grpo_entrga><dto>6</dto></grpo_entrga><grpo_entrga><dto>7</dto></grpo_entrga><grpo_entrga><dto>8</dto></grpo_entrga></grpo_entrgs>'
	,null 
	,null, 
	'user1'
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
 ALTER PROCEDURE gsa.spASCalcularDetalleCambioFechaEntregaOPS
 
	@lxPlanes             xml = null,
	@lxAfiliados          xml = null,
	@lcIncapacidad        int = null,
	@ldfechaEntregaDesde  datetime = null,
	@ldfechaEntregaHasta  datetime = null,
	@ldfechaCreacionDesde datetime = null,
	@ldfechaCreacionHasta datetime = null,
	@lxEstados            xml,
	@lxSedes              xml = null,
	@lxGruposEntrega      xml = null,
	@lnPrestacion         udtConsecutivo = null,
	@lcEntregaInmediata   int = null,
	@usro			udtUsuario
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @fechaActual					datetime,
			@cnst_s 						udtLogico,
			@cnst_n 						udtLogico,
			@cnst_gn						udtTipoIdentificacion,
			@cnsctvo_cdgo_tpo_agrpdr_prstcn int,
			@espco						   udtLogico,
			@csctvoCdgoClsficcnTtla 	   udtConsecutivo,
			@cnsctvoCdgoEstdoTtlaCnfrmdo   udtConsecutivo,
			@cnsctvoClsfccnEvtoEAC 		   udtConsecutivo,
			@cnsctvoClsfccnEvtoCancer 	   udtConsecutivo,
			@cnsctvoClsfccnEvtoVIH 		   udtConsecutivo,
			@cnsctvoClsfccnEvtoIRC 		   udtConsecutivo,
			@cnsctvoEstdoNtffcnCnfrmdo 	   udtConsecutivo,
			@cnsctvoEstdoNtffcnCnfrmdo2    udtConsecutivo,
			@cnsctvoEstdoNtffcnCnfrmdo3    udtConsecutivo,
			@cnsctvoEstdoNtffcnCnfrmdo4    udtConsecutivo,
			@cnsctvoCdgoStadoAnldo 		   udtConsecutivo,
			@cntdd_ops_cnsltds			   int,
			@vlr_ops_cnsltds			   numeric(14,2),
			@prcntje_100				   numeric(12,2),
			@vco						   udtLogico,
			@cnst_uno					   int,
			@lcSentenceSql				   NVarchar(2000) = NULL,
			@lcWhereSql					   NVarchar(2000) = NULL 

	CREATE TABLE #tempPlanes (
		cnsctvo_pln udtConsecutivo
	)

	CREATE TABLE #tempAfiliados (
		cnsctvo_afldo udtConsecutivo
	)

	CREATE TABLE #tempEstados (
		cnsctvo_estdo udtConsecutivo
	)

	CREATE TABLE #tempSedes (
		cnsctvo_sde udtConsecutivo
	)

	CREATE TABLE #tempGruposEntrega (
		cnsctvo_grpo_entrga udtConsecutivo
	)	

	CREATE TABLE #estadosClasificacionEvento(
		cnsctvo_estdo udtConsecutivo
	)

	CREATE TABLE #estadosCodigoEstadoNotificacion(
		cnsctvo_estdo udtConsecutivo
	)
	

	SET @fechaActual = getDate()
	SET @cnst_s      = 'S'
	SET @cnst_n      = 'N'
	SET @cnst_gn     =' - '
	SET @cnsctvo_cdgo_tpo_agrpdr_prstcn = 13
	SET @espco = ' '
	SET @csctvoCdgoClsficcnTtla = 8
	SET @cnsctvoCdgoEstdoTtlaCnfrmdo = 15
	SET @cnsctvoClsfccnEvtoEAC = 3
	SET @cnsctvoClsfccnEvtoCancer = 13
	SET @cnsctvoClsfccnEvtoVIH = 16
	SET @cnsctvoClsfccnEvtoIRC = 23
	SET @cnsctvoEstdoNtffcnCnfrmdo = 3
	SET @cnsctvoEstdoNtffcnCnfrmdo2 = 89
	SET @cnsctvoEstdoNtffcnCnfrmdo3  =120
	SET @cnsctvoEstdoNtffcnCnfrmdo4 = 123
	SET @cnsctvoCdgoStadoAnldo = 5
	SET @prcntje_100			=100.0
	SET @vco = ''
	SET @cnst_uno = 1
	SET @lcSentenceSql = 
		'Select	 a.cnsctvo_cdgo_tpo_idntfccn	, a.nmro_idntfccn		, a.cnsctvo_cdgo_pln	, a.cnsctvo_cdgo_tpo_afldo			'+ CHAR(13) +
		'		,b.fcha_expdcn					, b.cnsctvo_cdgo_estdo	, a.cnsctvo_cdgo_sde										'+ CHAR(13) +
		'		,CONCAT( rtrim(ltrim(a.prmr_nmbre)), @espco																			'+ CHAR(13) +
		'				,case when rtrim(ltrim(a.sgndo_nmbre)) is null then @vco else rtrim(ltrim(a.sgndo_nmbre)) end , @espco		'+ CHAR(13) +
		'				,case when rtrim(ltrim(a.prmr_aplldo)) is null then @vco else rtrim(ltrim(a.prmr_aplldo)) end , @espco		'+ CHAR(13) +
		'				,case when rtrim(ltrim(a.sgndo_nmbre)) is null then @vco else rtrim(ltrim(a.sgndo_nmbre)) end  ) AS nmbre	'+ CHAR(13) +
		'			  , CONCAT (b.cnsctvo_cdgo_ofcna,@cnst_gn,b.nuam) AS nmro_ops													'+ CHAR(13) +
		'			  ,a.nmro_unco_idntfccn_afldo	,b.fcha_entrga			,b.cnsctvo_cdgo_ofcna	,b.nuam,@usro					'+ CHAR(13) +
		'			  ,ab.fcha_estmda_entrga_fnl, ab.cnsctvo_cdgo_grpo_entrga,ab.cnsctvo_prstcn										'+ CHAR(13) +
		'	From       bdsisalud.dbo.tbactua a With (Nolock)																					'+ CHAR(13) +
		'	Inner Join bdsisalud.dbo.tbAtencionOps b With (Nolock)																			'+ CHAR(13) +
		'	On         a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna	And        a.nuam = b.nuam										'+ CHAR(13) +			
		'	Inner Join bdsisalud.dbo.tbprocedimientos ab With (Nolock)																		'+ CHAR(13)	+
		'	On a.cnsctvo_cdgo_ofcna = ab.cnsctvo_cdgo_ofcna		And a.nuam = ab.nuam												'+ CHAR(13)				
			
	SET @lcWhereSql	 = ' Where    @fechaActual Between a.fcha_inco_vgnca And a.fcha_fn_vgnca	'+ CHAR(13)


	Insert into  #estadosClasificacionEvento (cnsctvo_estdo)
	values(@cnsctvoClsfccnEvtoEAC)

	Insert into  #estadosClasificacionEvento (cnsctvo_estdo)
	values(@cnsctvoClsfccnEvtoCancer)

	Insert into  #estadosClasificacionEvento (cnsctvo_estdo)
	values(@cnsctvoClsfccnEvtoVIH)

	Insert into  #estadosClasificacionEvento (cnsctvo_estdo)
	values(@cnsctvoClsfccnEvtoIRC)

	Insert into  #estadosCodigoEstadoNotificacion (cnsctvo_estdo)
	values(@cnsctvoEstdoNtffcnCnfrmdo)

	Insert into  #estadosCodigoEstadoNotificacion (cnsctvo_estdo)
	values(@cnsctvoEstdoNtffcnCnfrmdo2)

	Insert into  #estadosCodigoEstadoNotificacion (cnsctvo_estdo)
	values(@cnsctvoEstdoNtffcnCnfrmdo3)

	Insert into  #estadosCodigoEstadoNotificacion (cnsctvo_estdo)
	values(@cnsctvoEstdoNtffcnCnfrmdo4)	


	--Se borra los datos que pertenecen al usuario
	delete bdcna.gsa.tbTmpAsOPSConsultadas where usro = @usro
	delete bdcna.gsa.tbTmpAsTotalOPSConsultadas where usro = @usro

	IF @lxEstados IS NOT NULL
	BEGIN	
		-- Se crea una tabla para filtrar los estados
		Insert Into #tempEstados (cnsctvo_estdo)	
		SELECT
			cast(colx.query('data(dto)') as varchar)  as  cnsctvo_estdo
		FROM @lxEstados.nodes('estds/estdo') as TABX(COLX)	

		SET @lcSentenceSql+= ' INNER JOIN #tempEstados h on  b.cnsctvo_cdgo_estdo = h.cnsctvo_estdo '+ CHAR(13)		

	END
	
	
	IF @lxPlanes IS NOT NULL
	BEGIN
		-- Se crea una tabla para filtrar los planes	
		Insert Into #tempPlanes (cnsctvo_pln)		
		SELECT
			cast(colx.query('data(dto)') as varchar)  as  cnsctvo_pln
		FROM @lxPlanes.nodes('plns/pln') as TABX(COLX)

		SET @lcSentenceSql+= ' Inner Join #tempPlanes i On a.cnsctvo_cdgo_pln = i.cnsctvo_pln '+ CHAR(13)

	END

	IF @lxAfiliados IS NOT NULL
	BEGIN
		-- Se crea una tabla para filtrar los afiliados	
		Insert Into #tempAfiliados (cnsctvo_afldo)		
		SELECT
			cast(colx.query('data(dto)') as varchar)  as  cnsctvo_afldo
		FROM @lxAfiliados.nodes('aflds/afld') as TABX(COLX)	

		SET @lcSentenceSql+= ' Inner Join #tempAfiliados j On a.cnsctvo_cdgo_tpo_afldo = j.cnsctvo_afldo '+ CHAR(13)

	END

	IF @lxSedes IS NOT NULL
	BEGIN		
		-- Se crea una tabla para filtrar las sedes	
		Insert Into #tempSedes (cnsctvo_sde)		
		SELECT
			cast(colx.query('data(dto)') as varchar)  as  cnsctvo_sde
		FROM @lxSedes.nodes('sds/sde') as TABX(COLX)
		
		SET @lcSentenceSql+= ' Inner Join #tempSedes k On a.cnsctvo_cdgo_sde = k.cnsctvo_sde  '+ CHAR(13)
	END
	
	IF @lxGruposEntrega IS NOT NULL
	BEGIN
		-- Se crea una tabla para filtrar los grupos de entrega	
		Insert Into #tempGruposEntrega (cnsctvo_grpo_entrga)
		SELECT
			cast(colx.query('data(dto)') as varchar)  as  cnsctvo_grpo_entrga
		FROM @lxGruposEntrega.nodes('grpo_entrgs/grpo_entrga') as TABX(COLX);
		
		SET @lcSentenceSql+= ' Inner Join #tempGruposEntrega l On ab.cnsctvo_cdgo_grpo_entrga = l.cnsctvo_grpo_entrga '+ CHAR(13)
		
	END
	
	IF @ldfechaEntregaDesde IS NOT NULL AND @ldfechaCreacionDesde IS NOT NULL
	BEGIN
									
		SET @lcWhereSql+=' AND b.fcha_expdcn BETWEEN @ldfechaCreacionDesde And   @ldfechaCreacionHasta '+ CHAR(13) +
						' AND ab.fcha_estmda_entrga_fnl BETWEEN @ldfechaEntregaDesde AND @ldfechaEntregaHasta '+ CHAR(13)
					
	END
	ELSE
	BEGIN
		IF @ldfechaEntregaDesde IS NOT NULL
		BEGIN	
			
			SET @lcWhereSql+=' AND ab.fcha_estmda_entrga_fnl BETWEEN @ldfechaEntregaDesde AND @ldfechaEntregaHasta '+ CHAR(13)
			
		END
		ELSE
		BEGIN
			SET @lcWhereSql+=' AND b.fcha_expdcn BETWEEN @ldfechaCreacionDesde And   @ldfechaCreacionHasta '+ CHAR(13)
		END
	END


	SET @lcSentenceSql += @lcWhereSql

	--Se inserta los datos basicos en tabla temporal
	Insert Into bdcna.gsa.tbTmpAsOPSConsultadas (
				cnsctvo_cdgo_tpo_idntfccn	,nmro_idntfccn			,	cnsctvo_cdgo_pln  , cnsctvo_cdgo_tpo_afldo 
				,fcha_expdcn				,cnsctvo_cdgo_estdo		,	cnsctvo_cdgo_sde      
				, nmbre, nmro_ops			,nmro_unco_idntfccn_afldo, fcha_entrga
				,cnsctvo_cdgo_ofcna,		nuam,						usro,				
				fcha_estmda_entrga_fnl,		cnsctvo_cdgo_grpo_entrga,	cnsctvo_prstcn
			)
	EXEC sp_executesql @lcSentenceSql, 
		N'@espco udtLogico, @vco udtLogico, @cnst_gn udtTipoIdentificacion,  @fechaActual datetime, @usro udtUsuario,
		  @ldfechaCreacionDesde datetime,  @ldfechaCreacionHasta datetime, @ldfechaEntregaDesde datetime, @ldfechaEntregaHasta datetime',
		@espco, @vco, @cnst_gn,@fechaActual, @usro, @ldfechaCreacionDesde, @ldfechaCreacionHasta , @ldfechaEntregaDesde, @ldfechaEntregaHasta


	Update b
		Set cdgo_cdfccn		= a.cdgo_cdfccn    ,   
			dscrpcn_cdfccn	= a.dscrpcn_cdfccn,
			cnsctvo_cdfccn	=a.cnsctvo_cdfccn		
	FROM  bdcna.gsa.tbTmpAsOPSConsultadas b With (Nolock)
	Inner Join  bdsisalud.dbo.tbCodificaciones a With (Nolock)
	On a.cnsctvo_cdfccn = b.cnsctvo_prstcn
	
	Update b
		Set dscrpcn_agrpdr_prstcn			= f.dscrpcn_agrpdr_prstcn ,
			cnsctvo_vgnca_agrpdr_prstcn		= f.cnsctvo_vgnca_agrpdr_prstcn		
	FROM  bdcna.gsa.tbTmpAsOPSConsultadas b  With (Nolock)
	Inner Join bdsisalud.dbo.tbDetAgrupadoresPrestaciones_Vigencias e With (Nolock)
	On b.cnsctvo_cdfccn = e.cnsctvo_cdfccn
	Inner Join bdsisalud.dbo.tbAgrupadoresPrestaciones_vigencias f With (Nolock)
	On e.cnsctvo_cdgo_agrpdr_prstcn = f.cnsctvo_cdgo_agrpdr_prstcn	
	Where @fechaActual Between e.inco_vgnca And e.fn_vgnca
		And  @fechaActual Between f.inco_vgnca And f.fn_vgnca
		And f.cnsctvo_cdgo_tpo_agrpdr_prstcn=  @cnsctvo_cdgo_tpo_agrpdr_prstcn

			 
	Update b
		Set vlr_rfrnca	=a.vlr_rfrnca		
	FROM  bdcna.gsa.tbTmpAsOPSConsultadas b With (Nolock)
	Inner Join bdsisalud.dbo.tbconceptosops a With (Nolock)
	On a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
		And a.nuam = b.nuam 
		And  a.cnsctvo_prstcn = b.cnsctvo_prstcn

	Delete From  bdcna.gsa.tbTmpAsOPSConsultadas where fcha_estmda_entrga_fnl is null
	Delete from  bdcna.gsa.tbTmpAsOPSConsultadas where  vlr_rfrnca is null	
	Delete from  bdcna.gsa.tbTmpAsOPSConsultadas where  cnsctvo_vgnca_agrpdr_prstcn  is null


	--Actualiza el tipo de indentificacion
	update b 
			set cdgo_tpo_idntfccn = a.cdgo_tpo_idntfccn 
	from  bdcna.gsa.tbTmpAsOPSConsultadas b  with (noLock)
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias a  With (Nolock)
	On a.cnsctvo_cdgo_tpo_idntfccn =b.cnsctvo_cdgo_tpo_idntfccn
	where @fechaActual Between a.inco_vgnca And a.fn_vgnca
		
	--Actualiza la descripcion del plan
	update b
			set dscrpcn_pln = a.dscrpcn_pln 
	from  bdcna.gsa.tbTmpAsOPSConsultadas b  with (noLock)
	Inner Join bdAfiliacionValidador.dbo.tbPlanes_vigencias a  With (Nolock)
	On a.cnsctvo_cdgo_pln =b.cnsctvo_cdgo_pln
	where @fechaActual Between a.inco_vgnca And a.fn_vgnca

	-- Actualiza a los que tengan tutelas
	update b
			set ttla = @cnst_s 				
	from  bdcna.gsa.tbTmpAsOPSConsultadas b  with (noLock)
	Inner Join bdsisalud.dbo.tbafiliadosmarcados a  With (Nolock)
	On a.nmro_unco_idntfccn = b.nmro_unco_idntfccn_afldo
	Where a.cnsctvo_cdgo_clsfccn_evnto = @csctvoCdgoClsficcnTtla --8 Tutela
			And a.cnsctvo_cdgo_estdo_ntfccn = @cnsctvoCdgoEstdoTtlaCnfrmdo --confirmado tutela 


	-- Actualiza  riesgo afiliado
	update c
			set dscrpcn_clsfccn_evnto = b.dscrpcn_clsfccn_evnto 				  
	from bdcna.gsa.tbTmpAsOPSConsultadas c with (noLock)
	Inner Join bdsisalud.dbo.tbafiliadosmarcados a  With (Nolock)
	On a.nmro_unco_idntfccn = c.nmro_unco_idntfccn_afldo
	Inner Join bdsisalud.dbo.tbClasificacionEventosNotificacion_Vigencias b with(nolock)
	On a.cnsctvo_cdgo_clsfccn_evnto =a.cnsctvo_cdgo_clsfccn_evnto
	Inner join #estadosClasificacionEvento ee 
	on a.cnsctvo_cdgo_clsfccn_evnto = ee.cnsctvo_estdo
	Inner join #estadosCodigoEstadoNotificacion ece
	on a.cnsctvo_cdgo_estdo_ntfccn = ee.cnsctvo_estdo
	Where @fechaActual Between b.inco_vgnca And b.fn_vgnca
		
	--Actualiza a los que tengan incapacidad
	Update b
				set incpcdd = @cnst_s
    From  bdcna.gsa.tbTmpAsOPSConsultadas b  With(NoLock)
	Inner Join bdSiSalud.dbo.tbMaestroIncapacidades a With (Nolock)
	On  a.nmro_unco_idntfccn_ctznte = b.nmro_unco_idntfccn_afldo  
    Inner Join  bdSiSalud.dbo.tbDetalleIncapacidad c WITH (NOLOCK)
    On  c.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna 
		And c.nmro_flo           = a.nmro_flo
    Where   a.cnsctvo_cdgo_estdo_sprco <> @cnsctvoCdgoStadoAnldo/*ANULADA*/
		 And         @fechaActual Between c.inco_lcnca And c.fn_lcnca;  

	---Estado de la estado de una OPS: 
	Update c
		Set dscrpcn_estdo_mga=  CONCAT (a.dscrpcn_estdo_srvco_slctdo,@cnst_gn,b.dscrpcn_estdo_atncn) 
	From  bdcna.gsa.tbTmpAsOPSConsultadas c  with(nolock)
	Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias a   With (Nolock)
	on a.cdgo_estdo_srvco_slctdo = c.cnsctvo_cdgo_estdo
	Inner Join bdSisalud.dbo.tbEstadosAtencion_Vigencias b with(nolock)
	on a.cnsctvo_cdgo_estdo_hmlgdo_sprs=b.cnsctvo_cdgo_estdo_atncn	
	Where  @fechaActual Between a.inco_vgnca And a.fn_vgnca
	and @fechaActual Between b.inco_vgnca And b.fn_vgnca
	and a.mrca_estdo_trnstro = @cnst_s
	
 	 	
	-- Se obtiene el total y valor por cada agrupador
	insert into bdcna.gsa.tbTmpAsTotalOPSConsultadas (
		cnsctvo_vgnca_agrpdr_prstcn ,dscrpcn_agrpdr_prstcn		 
		,cntdd_ops_cnsltds		 	,vlr_ops_cnsltds 				
		,cntdd_ops_mdfcds			,vlr_ops_mdfcds	,usro		
	)
	select  cnsctvo_vgnca_agrpdr_prstcn		,dscrpcn_agrpdr_prstcn
		,COUNT(@cnst_uno) AS cntdd_ops_cnsltds		,SUM(vlr_rfrnca) AS vlr_ops_cnsltds
		,COUNT(@cnst_uno) AS cntdd_ops_mdfcds		,SUM(vlr_rfrnca) AS vlr_ops_mdfcds		,@usro
	from   bdcna.gsa.tbTmpAsOPSConsultadas with(nolock)
	group by cnsctvo_vgnca_agrpdr_prstcn,dscrpcn_agrpdr_prstcn
	
	--consultan los totales
	select  @cntdd_ops_cnsltds = COUNT(@cnst_uno) 		,@vlr_ops_cnsltds = SUM(vlr_rfrnca)  
	from     bdcna.gsa.tbTmpAsOPSConsultadas with(nolock)
	
	--Actualiza los porcentajes de valor y cantidad que inicialmente es el mismo para consulta y modificacion
 	update  bdcna.gsa.tbTmpAsTotalOPSConsultadas 
		Set  prctj_cntdd_ops_cnsltds	=  (cntdd_ops_cnsltds*@prcntje_100)/@cntdd_ops_cnsltds
			,prctj_cntdd_ops_mdfcds 	=  (cntdd_ops_cnsltds*@prcntje_100)/@cntdd_ops_cnsltds
			,prctj_vlr_ops_cnsltds 		=  (vlr_ops_cnsltds*@prcntje_100)/@vlr_ops_cnsltds
			,prctj_vlr_ops_mdfcds 		=  (vlr_ops_cnsltds*@prcntje_100)/@vlr_ops_cnsltds		
	 
	select 
		cnsctvo_vgnca_agrpdr_prstcn ,dscrpcn_agrpdr_prstcn		 
		,cntdd_ops_cnsltds		 	,vlr_ops_cnsltds 		,prctj_cntdd_ops_cnsltds	,prctj_vlr_ops_cnsltds		
		,cntdd_ops_mdfcds			,vlr_ops_mdfcds			,prctj_cntdd_ops_mdfcds		,prctj_vlr_ops_mdfcds	
	from   bdcna.gsa.tbTmpAsTotalOPSConsultadas with(nolock)
	
	

	DROP TABLE #tempPlanes
	DROP TABLE #tempAfiliados
	DROP TABLE #tempEstados
	DROP TABLE #tempSedes
	DROP TABLE #tempGruposEntrega
	DROP TABLE #estadosClasificacionEvento
	DROP TABLE #estadosCodigoEstadoNotificacion
	
END

