USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarResultadoLiquidacion]    Script Date: 7/4/2017 9:48:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarResultadoLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS				  				A\>
* Descripcion			: <\D Se inserta el resultado de la liquidación en la  tabla BDCna.gsa.tbasconceptosserviciosolicitado y 
							  se  actualiza el estado de las prestaciones liquidadas a 9(liquidada) y 
							  el resultado de la liquidación (suma de todos los conceptos de gasto por prestación) 
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 01/06/2016 FC\>
*
*-----------------------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Carlos Andres Lopez Ramirez	AM\>
* Descripcion			 : <\DM Se agrega llamado a procedimiento BdCna.gsa.spAsCrearMarcaAccesoDirecto DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 12/01/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr	AM\>
* Descripcion			 : <\DM Se agrega condicional para evitar el cambio de estado a liquidado DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 24/01/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr	AM\>
* Descripcion			 : <\DM Se modifica condicion para el no cambio de estado a liquidado de forma que el proceso sirva para 
								liquidacion masiva. Para esto se crea una tabla temporal, #tmpServiciosNoMarcarLiquidar, y se hace un 
								left join con esta tabla validando el consecutivo de la solicitud y 
								el consecutivo del servicio solicitado DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 03/04/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Carlos Andres Lopez Ramirez - qvisionclr	AM\>
* Descripcion			 : <\DM Se agrega bloque para actualizar la fecha estimada de entrega de las prestaciones marcadas 
								como acceso directo a la fecha de la liquidacion del servicio. DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM @cnsctvo_cdgo_tpo_mrca: Variable que contiene el consecutivo de la marca de acceso directo
							    @fcha_entrga:			almacena la fecha actual como tipo Date VM\>
* Fecha Modificacion	 : <\FM 11/04/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM 
                                Se actualiza el procedimiento para que marque las prestaciones con estado liquidada
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 16/06/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM Ing Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM 
                                Se actualiza el procedimiento para que no se guarde las prestaciones en NULL
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM VM\>
* Fecha Modificacion	 : <\FM 04/07/2017 FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGuardarResultadoLiquidacion] 
	@usr_lgn		udtusuario
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @estdo_lqddo			 udtConsecutivo, --Estado liquidado
			@vlr_cro				 Int           ,
			@fcha_actl				 Datetime      ,
			@mrca_estdo_lquddo		 Int           , --Marca estado liquidado
			@estdo_no_lqddo			 Int           , -- qvisionclr
			@cnsctvo_cdgo_tpo_mrca	 Int           ,
			@fcha_entrga			 Date;

	Create 
	Table  #tmpServiciosNoMarcarLiquidar(cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
				                         cnsctvo_srvco_slctdo		udtConsecutivo
		                                )

    Set @estdo_lqddo           = 9;
	Set @vlr_cro               = 0;
	Set @fcha_actl             = getDate();
	Set @mrca_estdo_lquddo     = 1;
	Set @estdo_no_lqddo        = 0; -- qvisionclr
	Set @cnsctvo_cdgo_tpo_mrca = 7; -- Acceso Directo
	Set @fcha_entrga           = CONVERT(CHAR(10), @fcha_actl, 111);
	
	Insert 
	Into      #tmpConsolidadoLiquidacionxPrestacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_cps, vlr_lqdcn)
	Select    cgt.cnsctvo_slctd_atrzcn_srvco, cgt.cnsctvo_cdgo_cps, cgt.vlr_lqdcn 
	From      #tbConceptoGastosTmp_1 cgt
	Where     cgt.rsltdo_lqdcn = @mrca_estdo_lquddo
	Group By  cgt.cnsctvo_slctd_atrzcn_srvco, cgt.cnsctvo_cdgo_cps, cgt.vlr_lqdcn

    
	Insert 
	Into     #tmpServiciosNoMarcarLiquidar(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo)
	Select	 cnsctvo_slctd_atrzcn_srvco, agrpdr_srvco_lqdr
	From	 #tbConceptoGastosTmp_1
	Where	 rsltdo_lqdcn = @estdo_no_lqddo
	Group By cnsctvo_slctd_atrzcn_srvco, agrpdr_srvco_lqdr


	Insert 
	Into   gsa.tbasconceptosserviciosolicitado(cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo  , cnsctvo_ops             ,
			                                   cnsctvo_prstcn               , vlr_lqdcn               , cnsctvo_cdgo_cncpto_gsto,
			                                   cdgo_intrno_prstdr_atrzdo    , cnsctvo_cdgo_grpo_imprsn, nmro_rdccn_cm           ,
			                                   cnsctvo_cdgo_ofcna_cm        , vlr_cncpto_cm           , gnrdo                   ,
			                                   nmro_unco_ops                , fcha_crcn               , usro_crcn               , 
			                                   fcha_ultma_mdfccn            , usro_ultma_mdfccn       , cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
			                                  )
	Select  cnsctvo_prcdmnto_insmo_slctdo     , cnsctvo_mdcmnto_slctdo  , cnsctvo_ops             ,
			ISNULL(cnsctvo_cdgo_cps, @vlr_cro), vlr_lqdcn_cncpto        , cnsctvo_cdgo_cncpto_gsto,
			cdgo_intrno_prstdr                , cnsctvo_cdgo_grpo_imprsn, @vlr_cro                ,
			@vlr_cro                          , @vlr_cro                , @vlr_cro                ,
			@vlr_cro                          , @fcha_actl              , @usr_lgn                ,
			@fcha_actl                        , @usr_lgn                , @estdo_lqddo
	From 	#tbConceptoGastosTmp_1
	Where	rsltdo_lqdcn = @mrca_estdo_lquddo;
	
	

	--Se actualiza la sumatoria de los conceptos de gasto por prestación y estado a liquidada
	Update		ss 
	Set			vlr_lqdcn_srvco   = clp.vlr_lqdcn,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = @usr_lgn                         
	From		BDCna.gsa.tbASServiciosSolicitados ss With(RowLock)
	Inner Join	#tmpConsolidadoLiquidacionxPrestacion clp 
	On			clp.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco And 
	            clp.cnsctvo_cdgo_cps           = ss.cnsctvo_cdgo_srvco_slctdo 

	Update	   ss 
	Set		   cnsctvo_cdgo_estdo_srvco_slctdo = @estdo_lqddo              
	From	   BDCna.gsa.tbASServiciosSolicitados ss With(RowLock)
	Inner Join #tmpConsolidadoLiquidacionxPrestacion clp 
	ON		   clp.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco And
	           clp.cnsctvo_cdgo_cps           = ss.cnsctvo_cdgo_srvco_slctdo 		
	Left Join  #tmpServiciosNoMarcarLiquidar snml
	On	       snml.cnsctvo_srvco_slctdo       = ss.cnsctvo_srvco_slctdo
	And	       snml.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco 
	Where      snml.cnsctvo_srvco_slctdo Is Null				

	Exec gsa.spAsCrearMarcaAccesoDirecto @usr_lgn;

	Update		rfe	
	Set			fcha_estmda_entrga = @fcha_entrga
	From		bdCNA.gsa.tbAsResultadoFechaEntrega rfe With(RowLock)
	Inner Join	bdCNA.gsa.tbAsMarcasServiciosSolicitados mss With(NoLock)
	On			mss.cnsctvo_srvco_slctdo = rfe.cnsctvo_srvco_slctdo
	Inner Join	#tbConceptoGastosTmp_1 cg
	On			rfe.cnsctvo_srvco_slctdo = cg.agrpdr_srvco_lqdr		
	Where		mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca

	Drop Table #tmpServiciosNoMarcarLiquidar;			
END
