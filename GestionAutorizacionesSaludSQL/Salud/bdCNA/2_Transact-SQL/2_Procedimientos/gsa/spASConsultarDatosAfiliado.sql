USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarDatosAfiliado]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarDatosAfiliado
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite consultar los números únicos de identificación D\>
					   <\D para las solicitudes														 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultarDatosAfiliado] @cdgs_slctd xml
AS

BEGIN
  SET NOCOUNT ON

  --Temporal con los datos básicos de la solicitud
  CREATE TABLE #tmpNmroSlctds (
    id int IDENTITY (1, 1),
    cnsctvo_slctd_srvco_sld_rcbda int,
    nmro_slctd varchar(16),
    nmro_slctd_prvdr varchar(15),
  )
  --Se inserta la información que llega para ser procesada para fecha de entrega
  INSERT INTO #tmpNmroSlctds (cnsctvo_slctd_srvco_sld_rcbda,
  nmro_slctd,
  nmro_slctd_prvdr)
    SELECT
      pref.value('(cnsctvo_slctd/text())[1]', 'udtConsecutivo') AS cnsctvo,
      pref.value('(cdgo_slctd/text())[1]', 'varchar(16)') AS cdgo,
      pref.value('(cdgo_slctd_prstdr/text())[1]', 'varchar(15)') AS cdgo_prvdr
    FROM @cdgs_slctd.nodes('/cdgs_slctds//idnt_slctds') AS xml_slctd (Pref);

  --Se valida que la información que ingresa, si se encuentre en la base de datos, para así ser procesada.
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_srvco_sld_rcbda)
  WHERE s.cnsctvo_slctd_srvco_sld_rcbda IS NOT NULL

  --Cuando solo llega el número solicitud
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_atrzcn_ss = s.nmro_slctd)
  WHERE s.nmro_slctd IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

  --Cuando solo llega el número solicitud prestador	
  UPDATE #tmpNmroSlctds
  SET cnsctvo_slctd_srvco_sld_rcbda = a.cnsctvo_slctd_atrzcn_srvco
  FROM #tmpNmroSlctds s WITH (NOLOCK)
  LEFT JOIN bdCNA.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
    ON (a.nmro_slctd_prvdr = s.nmro_slctd_prvdr)
  WHERE s.nmro_slctd_prvdr IS NOT NULL
  AND s.cnsctvo_slctd_srvco_sld_rcbda IS NULL

  --Se retorna la información de los NUI.
  SELECT
    slc.cnsctvo_slctd_srvco_sld_rcbda,
    iasas.nmro_unco_idntfccn_afldo
  FROM #tmpNmroSlctds slc WITH (NOLOCK)
  INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)-- Solicitudes
    ON slc.cnsctvo_slctd_srvco_sld_rcbda = sas.cnsctvo_slctd_atrzcn_srvco
  INNER JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios iasas WITH (NOLOCK)
    ON sas.cnsctvo_slctd_atrzcn_srvco = iasas.cnsctvo_slctd_atrzcn_srvco
END

GO
