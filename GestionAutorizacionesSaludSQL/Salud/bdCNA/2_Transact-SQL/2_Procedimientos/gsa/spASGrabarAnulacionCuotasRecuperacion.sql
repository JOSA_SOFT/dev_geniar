USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarAnulacionCuotasRecuperacion]    Script Date: 12/06/2017 03:11:39 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASGrabarAnulacionCuotasRecuperacion
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\> 
* Descripcion		: <\D sp que grabar la anulación de cuotas de recuperación cambiando los estados y actualizando los saldos
						  de topes por copagos de eventos y año																D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																									P\>  													
* Variables			: <\V V\>  
* Fecha Creacion	: <\FC 2016/05/26																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se quita update a un campo de la tabla gsa.TbAsConsolidadoCuotaRecuperacionxOps ya que el 
							   campo no existe  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/29 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega With(NoLock) a las consultas DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 27/03/2017 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andres lopez Ramirez AM\>  													
* Descripcion			: <\DM Se altera orden del procedimiento y se valida el registo consolidado 
							   de cuotas para evitar la anulacion cuando existan OPS activas. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 02/05/2017 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASGrabarAnulacionCuotasRecuperacion]

/*01*/  @lcUsrioAplcn UdtUsuario

AS

Begin
	SET NOCOUNT ON

	Declare @lcUsuario									UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fechaactual								Datetime = getdate(),
			@cnsctvo_cdgo_estdo_cta_rcprcn4				UdtConsecutivo = 4, -- Estado Cuota de Recuperacion '4-Anulado'
			@cnsctvo_cdgo_estdo_cta_rcprcn1				UdtConsecutivo = 1 --Estado Cuota de Recuperacion '1-Precalculado'

	-- 	
	Create Table #tempDetallexConsolidado
	(
		cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn	udtConsecutivo
	)

	-- 
	If  @lcUsrioAplcn is not null
	Begin
		Set @lcUsuario =  @lcUsrioAplcn
	End
		
	-- paso 1. Se marcan como anulado en TbAsConsolidadoCuotaRecuperacionxOps

	


	Update		ccro
	Set			cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn4,
				fcha_ultma_mdfccn = @fechaactual,
				usro_ultma_mdfccn = @lcUsuario
	From		gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(RowLock)
	Inner Join	#TempServicioSolicitudes ss
	On			ss.nmro_unco_ops = ccro.nmro_unco_ops
	And			ss.cnsctvo_slctd_atrzcn_srvco = ccro.cnsctvo_slctd_atrzcn_srvco

	-- Paso 2. Se marca como anulado el detalle de la cuota.
	Update		dccr
	Set			cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn4,
				fcha_ultma_mdfccn = @fechaactual,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_anlcn = @fechaactual
	From		gsa.tbAsDetCalculoCuotaRecuperacion dccr With(RowLock)
	Inner Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_rgstro_dt_cta_rcprcn = dccr.cnsctvo_rgstro_dt_cta_rcprcn
	And			ss.nmro_unco_ops = dccr.nmro_unco_ops
	Where		dccr.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn1

	-- Paso 3. Actualizamos el estado anulado el consolidado de cuotas de recuperacion	

	Insert Into #tempDetallexConsolidado
	(
				cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	)
	Select		cccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	From		gsa.tbAsCalculoConsolidadoCuotaRecuperacion cccr With(RowLock)
	Inner Join	gsa.tbAsAsocDetallexConsolidadoCuotaRecuperacion adccr with(NoLock)
	On			adccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = cccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
	Inner Join  gsa.tbAsDetCalculoCuotaRecuperacion dccr with(NoLock)
	On			dccr.cnsctvo_rgstro_dt_cta_rcprcn = adccr.cnsctvo_rgstro_dt_cta_rcprcn
	Inner Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = dccr.cnsctvo_slctd_atrzcn_srvco
	Where		dccr.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn1
	Group By	cccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn

	-- 
	Update		cccr
	Set			cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn4,
				fcha_ultma_mdfccn = @fechaactual,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_anlcn = @fechaactual
	From		gsa.tbAsCalculoConsolidadoCuotaRecuperacion cccr With(RowLock)
	Left Join	#tempDetallexConsolidado dc 
	On			dc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = cccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn	
	Where		dc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn Is Null


	-- paso 4. Actualizamos la fecha de anulación de los topes
	Update		dtc
	Set			fcha_ultma_mdfccn = @fechaactual,
				usro_ultma_mdfccn = @lcUsuario,
				fcha_anlcn = @fechaactual
	From		gsa.TbAsDetalleTopesCopago dtc With(RowLock)
	Inner Join	#TempServicioSolicitudes ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = dtc.cnsctvo_slctd_atrzcn_srvco
	
	-- paso 5. 	Insertamos el nuevo registro de saldos topes x evento y año
	Insert Into gsa.TbAsDetalleTopesCopago
	(
			cnsctvo_dtlle_mdlo_tps_cpgo,			cnsctvo_mdlo,						cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
			cnsctvo_slctd_atrzcn_srvco,				nmro_unco_idntfccn_afldo,			sldo_antrr_dspnble_evnto,
			sldo_antrr_dspnble_ano,					sldo_dspnble_evnto,					sldo_dspnble_ano,
			cnsctvo_cdgo_estdo_cta_rcprcn,			cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,	cnsctvo_cdgo_tpo_trnsccn,
			fcha_crcn,								usro_crcn,							fcha_ultma_mdfccn,
			usro_ultma_mdfccn

	)
	Select
			cnsctvo_dtlle_mdlo_tps_cpgo,			cnsctvo_mdlo,						cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn,
			cnsctvo_slctd_atrzcn_srvco,				nmro_unco_idntfccn_afldo,			sldo_antrr_dspnble_evnto,
			sldo_antrr_dspnble_ano,					sldo_dspnble_evnto,					sldo_dspnble_ano,
			cnsctvo_cdgo_estdo_cta_rcprcn,			cnsctvo_rgstro_dtlle_tpe_cpgo_orgn,	cnsctvo_cdgo_tpo_trnsccn,
			@fechaactual,							@lcUsuario,							@fechaactual,
			@lcUsuario
	From	#tempTbAsDetalleTopesCopago		

	Drop Table #tempDetallexConsolidado

End
