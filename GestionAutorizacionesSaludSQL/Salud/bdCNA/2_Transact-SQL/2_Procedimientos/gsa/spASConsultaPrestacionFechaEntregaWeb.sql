USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaPrestacionFechaEntregaWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaPrestacionFechaEntregaWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la fecha de entrega de las prestaciones 
						   que se encuentren aprobadas
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                          cnsctvo_slctd_atrzcn_srvco   				        
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 13/04/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*--------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASConsultaPrestacionFechaEntregaWeb] 375

 ALTER PROCEDURE [gsa].[spASConsultaPrestacionFechaEntregaWeb] 
   @cnsctvo_slctd_atrzcn_srvco  udtConsecutivo
 AS

Begin
    SET NOCOUNT ON

    Declare @ldEstdo_srvco_slctdo udtConsecutivo 

	Create 
	Table  #tempPrestacionesFechaEntrega(cnsctvo_srvco_slctdo udtConsecutivo,
	                                     dscrpcn_srvco_slctdo udtDescripcion,
	                                     fcha_estmda_entrga   Datetime
										)

	Set @ldEstdo_srvco_slctdo = 7 -- Prestacion Aprobada

	Insert
	Into       #tempPrestacionesFechaEntrega(cnsctvo_srvco_slctdo, dscrpcn_srvco_slctdo)
	Select     a.cnsctvo_srvco_slctdo, a.dscrpcn_srvco_slctdo
	From       bdCNA.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
	Where      a.cnsctvo_slctd_atrzcn_srvco      = @cnsctvo_slctd_atrzcn_srvco
	And        a.cnsctvo_cdgo_estdo_srvco_slctdo = @ldEstdo_srvco_slctdo	

	Update     #tempPrestacionesFechaEntrega
	Set        fcha_estmda_entrga = b.fcha_estmda_entrga
	From       #tempPrestacionesFechaEntrega a
	Inner Join bdCNA.gsa.tbASResultadoFechaEntrega b WITH (NOLOCK)	
	On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	
	Select     cnsctvo_srvco_slctdo, dscrpcn_srvco_slctdo, fcha_estmda_entrga
	From       #tempPrestacionesFechaEntrega

	Drop Table #tempPrestacionesFechaEntrega
End

GO
