USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsMigracionCuotasDeRecuperacionSipres]    Script Date: 05/04/2017 08:57:07 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spAsMigracionCuotasDeRecuperacionSipres
* Desarrollado por   : <\A Carlos Andres Lopez (Qvision)     A\>    
* Descripcion        : <\D Carga las cuotas de recuperacion de Mega para cargarlas en Sipres.  D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 2017-03-31 FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D Se cambia referencia de la tabla temporal de conceptos y se pone la tabla tbAsConceptosServicioSolicitado D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017-04-03 FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

--exec bdcna.gsa.spAsMigracionCuotasDeRecuperacionSipres

ALTER Procedure [gsa].[spAsMigracionCuotasDeRecuperacionSipres]
As

Begin
		SET NOCOUNT ON
		
		Declare @inco_vgnca_cta					Date,
				@fn_vgnca_cta					Date,
				@cnsctvo_cdgo_estdo_cta_rcprcn	udtConsecutivo,
				@cnsctvo_cdgo_cncpto_pgo1		udtConsecutivo,
				@cnsctvo_cdgo_cncpto_pgo2		udtConsecutivo;


		Set @inco_vgnca_cta = Convert(Date,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0));
		Set @fn_vgnca_cta = CONVERT(Date, DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0))));
		Set @cnsctvo_cdgo_estdo_cta_rcprcn = 1;
		Set @cnsctvo_cdgo_cncpto_pgo1 = 1;
		Set @cnsctvo_cdgo_cncpto_pgo2 = 2;



        
		Create Table #tmpDetalleCuotasRecuperacion
		(
			id									Int Identity(1, 1),
			cnsctvo_slctd_atrzcn_srvco			udtConsecutivo,
			cnsctvo_cdgo_ofcna					udtConsecutivo,
			nuam								udtConsecutivo,
			cnsctvo_cdgo_cncpto_pgo				udtConsecutivo,
			cpgo_pgo							Float Default 0,
			cta_mdrdra							Float Default 0,
			nmro_unco_idntfccn_afldo			Int,
			cnsctvo_cdgo_rngo_slrl				udtConsecutivo,
			vlr_cpgo_atncn						Float Default 0,
			prcntje_lqdcn						Float Default 0,
			cnsctvo_cdgo_pln					udtConsecutivo,
			cnsctvo_cdgo_estdo_atncn			udtConsecutivo,
			dspnble_evnto						Float Default 0,
			dspnble_ano							Float Default 0,
			fcha_crcn_rgstro					Date,
			usro_ultma_mdfccn					udtUsuario,
			fcha_ultma_mdfccn					Date,
			inco_vgnca_cta						Date,
			fn_vgnca_cta						Date,
			nmro_nta							Int,
			cnsctvo_cdgo_tpo_dcmnto				udtConsecutivo,
			vlr_lqdcn_atncn_cpgo				Float Default 0,
			vlr_lqdcn_atncn						Float Default 0,
			cnsctvo_cdgo_cta_mrbldd				udtConsecutivo Default 0,
			pgo_prprcnl							Float Default 0
		)


		Create Table #tmpValorCuotaRecuperacionSipres
		(
			nuam								udtConsecutivo,
			cnsctvo_cdgo_ofcna					udtConsecutivo,
			vlr_cta_rcprcn						Float Default 0,
			cnsctvo_cdgo_cncpto_pgo				udtConsecutivo			
		)

		
  		Insert Into #tmpDetalleCuotasRecuperacion
		(
					cnsctvo_slctd_atrzcn_srvco,			nmro_unco_idntfccn_afldo,			cnsctvo_cdgo_pln,
					cnsctvo_cdgo_rngo_slrl,				nuam,								cnsctvo_cdgo_ofcna,
					cnsctvo_cdgo_cncpto_pgo
		)
		Select		a.cnsctvo_slctd_atrzcn_srvco,		a.nmro_unco_idntfccn_afldo,			a.cnsctvo_cdgo_pln,
					a.cnsctvo_cdgo_rngo_slrl,			b.nuam,								b.cnsctvo_cdgo_ofcna,
					ccrp.cnsctvo_cdgo_cncpto_pgo
		From		#tmpinformacionafiliado a
		Inner Join  #tmpinformacionsolicitud b
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		Inner Join	#tmpinformacionservicios iss
		On			iss.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco		
		Inner Join	bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps ccrp With(NoLock)
		On			ccrp.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		On			css.nmro_unco_ops = ccrp.nmro_unco_ops
		Where		ccrp.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn
		Group By	a.cnsctvo_slctd_atrzcn_srvco,		a.nmro_unco_idntfccn_afldo,			a.cnsctvo_cdgo_pln,
					a.cnsctvo_cdgo_rngo_slrl,			b.nuam,								b.cnsctvo_cdgo_ofcna,
					ccrp.cnsctvo_cdgo_cncpto_pgo;
    

		
		Insert Into #tmpValorCuotaRecuperacionSipres
		(
					nuam,								cnsctvo_cdgo_ofcna,							cnsctvo_cdgo_cncpto_pgo,
					vlr_cta_rcprcn			
		)
		Select		dcr.nuam,							dcr.cnsctvo_cdgo_ofcna,						ccrp.cnsctvo_cdgo_cncpto_pgo, 
					Sum(vlr_lqdcn_cta_rcprcn_srvco_ops)			
		From		#tmpDetalleCuotasRecuperacion dcr
		Inner Join	#tmpinformacionservicios iss
		On			iss.cnsctvo_slctd_atrzcn_srvco = dcr.cnsctvo_slctd_atrzcn_srvco		
		Inner Join	bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps ccrp With(NoLock)
		On			ccrp.cnsctvo_cdgo_cncpto_pgo = dcr.cnsctvo_cdgo_cncpto_pgo
		And			ccrp.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		On			css.nmro_unco_ops = ccrp.nmro_unco_ops
		Group By	dcr.nuam,			dcr.cnsctvo_cdgo_ofcna,			ccrp.cnsctvo_cdgo_cncpto_pgo;


		Update		dcr
		Set			usro_ultma_mdfccn = ccrp.usro_ultma_mdfccn,
					fcha_ultma_mdfccn =	ccrp.fcha_ultma_mdfccn,	
					fcha_crcn_rgstro = ccrp.fcha_crcn
		From		#tmpDetalleCuotasRecuperacion dcr
		Inner Join	#tmpinformacionservicios iss
		On			iss.cnsctvo_slctd_atrzcn_srvco = dcr.cnsctvo_slctd_atrzcn_srvco
		Inner Join	bdCNA.gsa.TbAsConsolidadoCuotaRecuperacionxOps ccrp With(NoLock)
		On			ccrp.cnsctvo_slctd_atrzcn_srvco = iss.cnsctvo_slctd_atrzcn_srvco
		And			ccrp.cnsctvo_cdgo_cncpto_pgo = dcr.cnsctvo_cdgo_cncpto_pgo
		Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		On			css.nmro_unco_ops = ccrp.nmro_unco_ops


		Update		#tmpDetalleCuotasRecuperacion
		Set			cta_mdrdra = b.vlr_cta_rcprcn
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	#tmpValorCuotaRecuperacionSipres b
		On			b.nuam = a.nuam
		And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
		And			b.cnsctvo_cdgo_cncpto_pgo = a.cnsctvo_cdgo_cncpto_pgo
		Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1


		Update		#tmpDetalleCuotasRecuperacion
		Set			cpgo_pgo = b.vlr_cta_rcprcn,
					vlr_lqdcn_atncn_cpgo = b.vlr_cta_rcprcn,
					vlr_cpgo_atncn = b.vlr_cta_rcprcn
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	#tmpValorCuotaRecuperacionSipres b
		On			b.nuam = a.nuam
		And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
		And			b.cnsctvo_cdgo_cncpto_pgo = a.cnsctvo_cdgo_cncpto_pgo
		Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2

		Update		a
		Set			cnsctvo_cdgo_estdo_atncn =  co.cnsctvo_cdgo_estdo
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	bdSisalud.dbo.tbAtencionOps co With(NoLock)
		On			co.nuam = a.nuam
		And			co.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna

		Delete From #tmpValorCuotaRecuperacionSipres

		Insert Into #tmpValorCuotaRecuperacionSipres
		(
					nuam,								cnsctvo_cdgo_ofcna,							cnsctvo_cdgo_cncpto_pgo,
					vlr_cta_rcprcn			
		)
		Select		a.nuam,								a.cnsctvo_cdgo_ofcna,						a.cnsctvo_cdgo_cncpto_pgo, 
					Sum(co.vlr_rfrnca)
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	bdSisalud.dbo.tbConceptosOps co With(NoLock)
		On			co.nuam = a.nuam
		And			co.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
		Group By	a.nuam,								a.cnsctvo_cdgo_ofcna,						a.cnsctvo_cdgo_cncpto_pgo

		Update		#tmpDetalleCuotasRecuperacion
		Set			vlr_lqdcn_atncn = b.vlr_cta_rcprcn
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	#tmpValorCuotaRecuperacionSipres b
		On			b.nuam = a.nuam
		And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
		And			b.cnsctvo_cdgo_cncpto_pgo = a.cnsctvo_cdgo_cncpto_pgo
		Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1

		Update		#tmpDetalleCuotasRecuperacion
		Set			vlr_lqdcn_atncn = b.vlr_cta_rcprcn
		From		#tmpDetalleCuotasRecuperacion a
		Inner Join	#tmpValorCuotaRecuperacionSipres b
		On			b.nuam = a.nuam
		And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
		And			b.cnsctvo_cdgo_cncpto_pgo = a.cnsctvo_cdgo_cncpto_pgo
		Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
		

		Merge bdSisalud.dbo.tbDetalleCuotasRecuperacion With(RowLock) As Target
		Using (
				Select		cnsctvo_cdgo_ofcna,			nuam,							cnsctvo_cdgo_cta_mrbldd,
							cnsctvo_cdgo_cncpto_pgo,	vlr_lqdcn_atncn_cpgo,			vlr_cpgo_atncn,
							cpgo_pgo,					cta_mdrdra,						pgo_prprcnl,
							cnsctvo_cdgo_rngo_slrl,		prcntje_lqdcn,					usro_ultma_mdfccn,
							fcha_ultma_mdfccn,			cnsctvo_cdgo_estdo_atncn,		vlr_lqdcn_atncn,
							dspnble_evnto,				dspnble_ano,					cnsctvo_cdgo_tpo_dcmnto,
							nmro_unco_idntfccn_afldo,	@inco_vgnca_cta inco_vgnca_cta,	@fn_vgnca_cta fn_vgnca_cta,
							cnsctvo_cdgo_pln,			fcha_crcn_rgstro
				From		#tmpDetalleCuotasRecuperacion
		) As Source
		On (
				Target.nuam = Source.nuam											And
				Target.cnsctvo_cdgo_ofcna = Source.cnsctvo_cdgo_ofcna				And
				Target.cnsctvo_cdgo_cncpto_pgo = Source.cnsctvo_cdgo_cncpto_pgo
		)
		When Matched Then
				Update 
				Set
				cnsctvo_cdgo_cta_mrbldd = Source.cnsctvo_cdgo_cta_mrbldd,
				vlr_lqdcn_atncn_cpgo = Source.vlr_lqdcn_atncn_cpgo,			
				vlr_cpgo_atncn = Source.vlr_cpgo_atncn,
				cpgo_pgo = Source.cpgo_pgo,					
				cta_mdrdra = Source.cta_mdrdra,						
				pgo_prprcnl = Source.pgo_prprcnl,
				cnsctvo_cdgo_rngo_slrl  = Source.cnsctvo_cdgo_rngo_slrl,		
				prcntje_lqdcn = Source.prcntje_lqdcn,					
				usro_ultma_mdfccn = Source.usro_ultma_mdfccn,
				fcha_ultma_mdfccn = Source.fcha_ultma_mdfccn,			
				cnsctvo_cdgo_estdo_atncn = Source.cnsctvo_cdgo_estdo_atncn,		
				vlr_lqdcn_atncn = Source.vlr_lqdcn_atncn,
				dspnble_evnto = Source.dspnble_evnto,				
				dspnble_ano = Source.dspnble_ano,					
				cnsctvo_cdgo_tpo_dcmnto = Source.cnsctvo_cdgo_tpo_dcmnto,
				nmro_unco_idntfccn_afldo = Source.nmro_unco_idntfccn_afldo,	
				inco_vgnca_cta = Source.inco_vgnca_cta,	
				fn_vgnca_cta = Source.fn_vgnca_cta,
				cnsctvo_cdgo_pln = Source.cnsctvo_cdgo_pln
			When Not Matched Then
				Insert 
				(
							cnsctvo_cdgo_ofcna,			nuam,							cnsctvo_cdgo_cta_mrbldd,
							cnsctvo_cdgo_cncpto_pgo,	vlr_lqdcn_atncn_cpgo,			vlr_cpgo_atncn,
							cpgo_pgo,					cta_mdrdra,						pgo_prprcnl,
							cnsctvo_cdgo_rngo_slrl,		prcntje_lqdcn,					usro_ultma_mdfccn,
							fcha_ultma_mdfccn,			cnsctvo_cdgo_estdo_atncn,		vlr_lqdcn_atncn,
							dspnble_evnto,				dspnble_ano,					cnsctvo_cdgo_tpo_dcmnto,
							nmro_unco_idntfccn_afldo,	inco_vgnca_cta,					fn_vgnca_cta,
							cnsctvo_cdgo_pln,			fcha_crcn_rgstro
				)
				Values
				(
							Source.cnsctvo_cdgo_ofcna,			Source.nuam,							Source.cnsctvo_cdgo_cta_mrbldd,
							Source.cnsctvo_cdgo_cncpto_pgo,		Source.vlr_lqdcn_atncn_cpgo,			Source.vlr_cpgo_atncn,
							Source.cpgo_pgo,					Source.cta_mdrdra,						Source.pgo_prprcnl,
							Source.cnsctvo_cdgo_rngo_slrl,		Source.prcntje_lqdcn,					Source.usro_ultma_mdfccn,
							Source.fcha_ultma_mdfccn,			Source.cnsctvo_cdgo_estdo_atncn,		Source.vlr_lqdcn_atncn,
							Source.dspnble_evnto,				Source.dspnble_ano,						Source.cnsctvo_cdgo_tpo_dcmnto,
							Source.nmro_unco_idntfccn_afldo,	Source.inco_vgnca_cta,					Source.fn_vgnca_cta,
							Source.cnsctvo_cdgo_pln,			Source.fcha_crcn_rgstro
				);
End
