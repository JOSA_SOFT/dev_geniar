USE [bdCNA]
GO

/****** Object:  StoredProcedure [gsa].[spAsGuardarGestionAutorizaNoConformidadPrestacion]    Script Date: 07/07/2017 04:30:35 p.m. ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spAsGuardarGestionAutorizaNoConformidadPrestacion
* Desarrollado por   : <\A Jorge Rodriguez De León     A\>    
* Descripcion        : <\D 
                           Procedimiento que permite registrar la gestión de autorización de prestaciones     
  					       que fueron devueltas por la malla	
					   D\>									 
* Observaciones      : <\O   O\>    
* Parametros         : <\P	 P\>    
* Variables          : <\V   V\>    
* Fecha Creacion     : <\FC 02/12/2016  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\DM  
                            Se modifica el procedimiento para que actualice el campo del recobro de la solicitud a 
							CAPITACION RECOBRO                                                                                                                                    
                        DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  10/03/2017  FM\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones		DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez AM\>
* Descripcion			 : <\DM	Se ajusta procedimiento para que actualice el recobro de la solicitud que ingresa unicamente,
								Se corrige el consecutivo y codigo del recobro DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 07/07/2017	FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------*/
/*
  Declare @cdgo_rsltdo  udtConsecutivo,
	      @mnsje_rsltdo	Varchar(2000)
  exec [bdCNA].[gsa].[spAsGuardarGestionAutorizaNoConformidadPrestacion] 807, 1909, 1, 'nada', '00253', 'user1', '00017   ', @cdgo_rsltdo Output, @mnsje_rsltdo Output
  Select @cdgo_rsltdo, @mnsje_rsltdo
*/

ALTER PROCEDURE [gsa].[spAsGuardarGestionAutorizaNoConformidadPrestacion]
	@cnsctvo_slctd_atrzcn_srvco	udtconsecutivo,
	@cnsctvo_srvco_slctdo		udtconsecutivo,
	@cnsctvo_cdgo_mtvo_csa		udtconsecutivo,
	@obsrvcns					udtobservacion,
	@nmro_atrzcn_ips			Varchar(20)   ,
	@usro_mdfccn				udtusuario    ,
	@codigo_interno				udtCodigoIps  ,
	@cdgo_rsltdo				udtConsecutivo  Output,
	@mnsje_rsltdo				Varchar(2000)   Output

AS
BEGIN
  SET NOCOUNT ON

  Declare @cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo,
		  @ldFechaActual				   Datetime		 ,
	      @orgn_mdfccn					   udtDescripcion,
		  @vlr_ip_cnstnte			       Int			 ,
		  @codigo_ok					   Int           ,  -- Error SP,
		  @codigo_error					   Int           ,  -- Codigo de mensaje Ok			
	  	  @mensaje_ok					   udtDescripcion,
		  @mensaje_error				   Varchar(2000) ,
		  @tmp_nmro_atrzcn_ips			   Varchar(20)   ,
		  @cnsctvo_cncpto_prcdmnto_slctdo  udtConsecutivo,
		  @cnsctvo_cdgo_rcbro              udtConsecutivo,
		  @cdgo_rcbro                      Char(3)		 ,
		  @cnsctvo_cdgo_estdo_srvco_slctdo_old udtConsecutivo,
		  @dscrpcn_cdgo_estdo_srvco_slctdo_old udtdescripcion,
		  @elmnto_orgn_mdfccn				udtdescripcion,
		  @dto_elmnto_orgn_mdfccn			udtdescripcion,
		  @dscrpcn_cdgo_estdo_srvco_slctdo  udtdescripcion,
		  @dscrpcn_cdgo_interno				udtdescripcion,
		  @dscrpcncdgo_intrno_ant			udtdescripcion,
		  @cdgo_intrno_ant					udtCodigoIps;      

  Create
  Table  #tmptrazamodificacion(cnsctvo_slctd_atrzcn_srvco	  udtconsecutivo,
							   cnsctvo_srvco_slctdo			  udtconsecutivo,
							   cnsctvo_cncpto_prcdmnto_slctdo udtconsecutivo,
							   cnsctvo_cdgo_mtvo_csa		  udtconsecutivo,
							   fcha_mdfccn					  Datetime      ,
							   orgn_mdfccn					  Varchar(100)  ,
							   vlr_antrr					  Varchar(20)   ,
							   vlr_nvo						  Varchar(20)   ,
							   usro_mdfccn					  udtusuario    ,
						       obsrvcns						  udtobservacion,
							   nmro_ip						  Varchar(12)   ,
							   fcha_crcn				      Datetime      ,
							   usro_crcn					  udtusuario    ,
							   fcha_ultma_mdfccn		      Datetime      ,
		 	                   usro_ultma_mdfccn			  udtusuario	,
							   elmnto_orgn_mdfccn			  udtDescripcion,
							   dto_elmnto_orgn_mdfccn		  udtDescripcion,
							   dscrpcn_vlr_antrr			  udtDescripcion,
							   dscrpcn_vlr_nvo				  udtDescripcion
	                          )
   
   Set @cnsctvo_cdgo_estdo_srvco_slctdo = 7
   Set @dscrpcn_cdgo_estdo_srvco_slctdo = 'APROBADA'
   Set @orgn_mdfccn                     = 'tbASAutorizaNoConformidadValidacion.cnsctvo_cdgo_estdo_srvco_slctdo'
   Set @elmnto_orgn_mdfccn				= 'Prestación'
   Set @dto_elmnto_orgn_mdfccn			= 'Autoriza Prestación';
   Set @ldFechaActual                   = getDate()
   Set @vlr_ip_cnstnte                  = 0
   Set @codigo_ok                       = 0
   Set @codigo_error                    = -1
   Set @mensaje_ok                      = 'Proceso ejecutado satisfactoriamente'
   Set @tmp_nmro_atrzcn_ips             = '-'
   Set @cnsctvo_cncpto_prcdmnto_slctdo  = Null
   Set @cnsctvo_cdgo_rcbro              = 25   --CAPITACION RECOBRO
   Set @cdgo_rcbro                      = '25' --CAPITACION RECOBRO
    
   If(@nmro_atrzcn_ips Is Not Null)
	  Begin
		 Set @tmp_nmro_atrzcn_ips = @nmro_atrzcn_ips
	  End
	
   BEGIN TRY	
        
	    Update		a
		Set			cnsctvo_cdgo_rcbro = @cnsctvo_cdgo_rcbro,
					fcha_ultma_mdfccn  = @ldFechaActual     ,	
					usro_ultma_mdfccn  = @usro_mdfccn	
		From		bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios a WITH (NOLOCK)
		Inner Join	bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH (NOLOCK)
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		Where		b.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

		Update		c
		Set			cdgo_rcbro         = @cdgo_rcbro   ,
					fcha_ultma_mdfccn  = @ldFechaActual,	
					usro_ultma_mdfccn  = @usro_mdfccn	
        From		bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal c WITH (NOLOCK)
		Inner Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios a WITH (NOLOCK)
		On			a.cnsctvo_dgnstco_slctd_atrzcn_srvco = c.cnsctvo_dgnstco_slctd_atrzcn_srvco And
					a.cnsctvo_slctd_atrzcn_srvco         = c.cnsctvo_slctd_atrzcn_srvco 
		Inner Join	bdCNA.gsa.tbASSolicitudesAutorizacionServicios b WITH (NOLOCK)
		On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
		Where		b.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		 
		Select		@cnsctvo_cdgo_estdo_srvco_slctdo_old = s.cnsctvo_cdgo_estdo_srvco_slctdo,
					@dscrpcn_cdgo_estdo_srvco_slctdo_old = e.dscrpcn_estdo_srvco_slctdo
		From		bdCNA.gsa.tbASServiciosSolicitados s WITH (NOLOCK)
		Inner Join	BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
		On			s.cnsctvo_cdgo_estdo_srvco_slctdo = e.cnsctvo_cdgo_estdo_srvco_slctdo
		Where		s.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And			s.cnsctvo_srvco_slctdo		  = @cnsctvo_srvco_slctdo

		Update		bdCNA.gsa.tbASServiciosSolicitados 
		SET			cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo,
					fcha_ultma_mdfccn               = @ldFechaActual,	
					usro_ultma_mdfccn               = @usro_mdfccn		
		Where		cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		And			cnsctvo_srvco_slctdo		  = @cnsctvo_srvco_slctdo

		--Se inserta el direccionamiento para la prestación
		Select		@cdgo_intrno_ant		= r.cdgo_intrno,
					@dscrpcncdgo_intrno_ant	= dp.nmbre_scrsl 
		From		BDCna.gsa.tbASResultadoDireccionamiento r With(NoLock)
		Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
		On			dp.cdgo_intrno = r.cdgo_intrno
		Where		r.cnsctvo_srvco_slctdo	= @cnsctvo_srvco_slctdo
		And			r.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		
		If @cdgo_intrno_ant is null 
		Begin
			Insert 
			Into   BDCna.gsa.tbASResultadoDireccionamiento(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cdgo_intrno      ,
														   fcha_crcn                 , usro_crcn           , fcha_ultma_mdfccn,
														   usro_ultma_mdfccn
														   )
			Values(@cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo, @codigo_interno,
				   @ldFechaActual             , @usro_mdfccn         , @ldFechaActual ,
				   @usro_mdfccn
				  )
		End
		Else
		Begin
			Update	r
			Set		cdgo_intrno = @codigo_interno
			From	BDCna.gsa.tbASResultadoDireccionamiento r WITH (NOLOCK)
			Where	r.cnsctvo_srvco_slctdo	= @cnsctvo_srvco_slctdo

			Set		@tmp_nmro_atrzcn_ips = @cdgo_intrno_ant 
		End

		--Se establece la información de la trazabilidad.
		Select	@dscrpcn_cdgo_interno = dp.nmbre_scrsl
		From	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock) 
		Where	dp.cdgo_intrno = @codigo_interno 

		Insert 
		Into  #tmptrazamodificacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,
									cnsctvo_cdgo_mtvo_csa     , fcha_mdfccn         , orgn_mdfccn                   ,
									vlr_antrr                 , vlr_nvo             , usro_mdfccn                   ,
									obsrvcns                  , nmro_ip             , fcha_crcn                     ,
									usro_crcn                 , fcha_ultma_mdfccn   , usro_ultma_mdfccn				,
									elmnto_orgn_mdfccn		  , dto_elmnto_orgn_mdfccn,dscrpcn_vlr_antrr			,
									dscrpcn_vlr_nvo
								   )
		Values(@cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo, @cnsctvo_cncpto_prcdmnto_slctdo,
			   @cnsctvo_cdgo_mtvo_csa     , @ldFechaActual       , @orgn_mdfccn                   ,
			   @tmp_nmro_atrzcn_ips			  , @codigo_interno      , @usro_mdfccn                   ,
			   @obsrvcns                  , @vlr_ip_cnstnte      , @ldFechaActual                 ,
			   @usro_mdfccn               , @ldFechaActual       , @usro_mdfccn					  ,
			   @elmnto_orgn_mdfccn		  , @dto_elmnto_orgn_mdfccn, @dscrpcncdgo_intrno_ant						  ,
			   @dscrpcn_cdgo_interno
			  )

		
		Insert 
		Into  #tmptrazamodificacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,
									cnsctvo_cdgo_mtvo_csa     , fcha_mdfccn         , orgn_mdfccn                   ,
									vlr_antrr                 , vlr_nvo             , usro_mdfccn                   ,
									obsrvcns                  , nmro_ip             , fcha_crcn                     ,
									usro_crcn                 , fcha_ultma_mdfccn   , usro_ultma_mdfccn				,
									elmnto_orgn_mdfccn		  , dto_elmnto_orgn_mdfccn,dscrpcn_vlr_antrr			,
									dscrpcn_vlr_nvo
								   )
		Values(@cnsctvo_slctd_atrzcn_srvco			, @cnsctvo_srvco_slctdo				, @cnsctvo_cncpto_prcdmnto_slctdo,
			   @cnsctvo_cdgo_mtvo_csa				, @ldFechaActual					, @orgn_mdfccn		             ,
			   @cnsctvo_cdgo_estdo_srvco_slctdo_old , @cnsctvo_cdgo_estdo_srvco_slctdo  , @usro_mdfccn                   ,
			   @obsrvcns							, @vlr_ip_cnstnte					, @ldFechaActual                 ,
			   @usro_mdfccn							, @ldFechaActual					, @usro_mdfccn					 ,
			   @elmnto_orgn_mdfccn					, @dto_elmnto_orgn_mdfccn			, @dscrpcn_cdgo_estdo_srvco_slctdo,
			   @dscrpcn_cdgo_estdo_srvco_slctdo_old	
			  )
        Execute bdcna.gsa.spasguardartrazamodificacion
		
		Set @cdgo_rsltdo  =	@codigo_ok
		Set @mnsje_rsltdo = @mensaje_ok
			
	END TRY
	BEGIN CATCH
		 
		SET @mensaje_error = Concat('Number:', CAST(ERROR_NUMBER() AS char), CHAR(13),
				   				'Line:', CAST(ERROR_LINE() AS char), CHAR(13),
								'Message:', ERROR_MESSAGE(), CHAR(13),
								'Procedure:', ERROR_PROCEDURE());
			
		Set @cdgo_rsltdo  =  @codigo_error;
		Set @mnsje_rsltdo =  @mensaje_error
			
	END CATCH 
	Drop Table #tmptrazamodificacion
END
