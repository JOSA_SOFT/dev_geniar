USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitud
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento expuesto que permite modificar solicitudes   D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 02/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jonathan Chamizo AM\>    
* Descripcion        : <\D  Se adiciona el nuevo parámetro de entrada  @infrmcn_vldcion_espcial  D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 01/03/2016 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitud] 
@infrmcn_slctd Nvarchar(Max),
@infrmcn_prstcn Nvarchar(Max),
@infrmcn_prstdr Nvarchar(Max),
@infrmcn_mdco Nvarchar(Max),
@infrmcn_dgnstco Nvarchar(Max),
@infrmcn_hsptlra Nvarchar(Max),
@infrmcn_afldo Nvarchar(Max),
@infrmcn_anxs Nvarchar(Max),
@infrmcn_vldcion_espcial Nvarchar(Max),
@estdo_ejccn varchar(5) OUTPUT,
@msje_errr varchar(3000) OUTPUT
AS
  SET NOCOUNT ON
  
 
  DECLARE @codigoExito char(2),
          @mensajeExito char(30),
          @codigoError char(5),
		  @Mensaje Varchar(max),
		  @num numeric(2),
		  @infrmcn_slctd_XML XML,
		  @infrmcn_prstcn_XML XML,
		  @infrmcn_prstdr_XML XML,
		  @infrmcn_mdco_XML XML,
	      @infrmcn_dgnstco_XML XML,
		  @infrmcn_hsptlra_XML XML,
		  @infrmcn_afldo_XML XML,
	      @infrmcn_anxs_XML XML,
		  @infrmcn_vldcion_espcial_XML XML

	SELECT	@codigoExito = 'OK',
			@codigoError = 'ET',
			@mensajeExito = 'Ejecución Exitosa'
		
		SET @infrmcn_slctd_XML = CONVERT(XML,@infrmcn_slctd);
		SET @infrmcn_prstcn_XML = CONVERT(XML,@infrmcn_prstcn);
		SET @infrmcn_prstdr_XML = CONVERT(XML,@infrmcn_prstdr);
		SET @infrmcn_mdco_XML = CONVERT(XML,@infrmcn_mdco);
		SET @infrmcn_dgnstco_XML = CONVERT(XML,@infrmcn_dgnstco);
		SET @infrmcn_hsptlra_XML = CONVERT(XML,@infrmcn_hsptlra);
		SET @infrmcn_afldo_XML = CONVERT(XML,@infrmcn_afldo);
		SET @infrmcn_anxs_XML = CONVERT(XML,@infrmcn_anxs);
		SET @infrmcn_vldcion_espcial_XML = CONVERT(XML,@infrmcn_vldcion_espcial);
BEGIN		
  	BEGIN TRY
		exec [gsa].[spASModificarSolicitudGeneral] 
		@infrmcn_slctd_XML
		,@infrmcn_prstcn_XML 
		,@infrmcn_prstdr_XML 
		,@infrmcn_mdco_XML 
		,@infrmcn_dgnstco_XML 
		,@infrmcn_hsptlra_XML 
		,@infrmcn_afldo_XML 
		,@infrmcn_anxs_XML 
		,@infrmcn_vldcion_espcial_XML 
		,@estdo_ejccn OUTPUT
		,@msje_errr OUTPUT

	END TRY
	BEGIN CATCH
		SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char(15)) + CHAR(13) +
		'Line:' + CAST(ERROR_LINE() AS char(15)) + CHAR(13) +
		'Message:' + ERROR_MESSAGE() + CHAR(13) +
		'Procedure:' + ERROR_PROCEDURE();
		SET @estdo_ejccn = @codigoError; 
    END CATCH

	
END


GO
