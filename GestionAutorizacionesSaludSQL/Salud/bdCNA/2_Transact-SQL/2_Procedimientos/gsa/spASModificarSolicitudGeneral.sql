USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudGeneral]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudGeneral
* Desarrollado por   : <\A Jois Lombana   A\>    
* Descripcion        : <\D Procedimiento expuesto que permite Modificar la información de una o más solicitudes D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------

* Modificado Por     : <\AM Se adiciona el parámetro de entrada @infrmcn_vldcion_espcial y se crea la 
						   tabla temporal #Tempo_ValidacionEspecial  AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    

* Fecha Modificacion : <\FM  01/03/2016  FM\>    
*-----------------------------------------------------------------------------------------------------*
*-------------------------------------------------------------------------------------------------------

* Modificado Por     : <\AM Jorge Rodriguez - SETI SAS  AM\>    
* Descripcion        : <\D  Se adiciona llamado a SP spASGuardarTrazaIndicadorReLiquidacion
							para que deje rastro de las solicitudes que fuenron mal diligenciadas por la ASI. D\> 
* Nuevas Variables   : <\VM   VM\>    

* Fecha Modificacion : <\FM  01/03/2016  FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudGeneral] 
@infrmcn_slctd xml,
@infrmcn_prstcn xml,
@infrmcn_prstdr xml,
@infrmcn_mdco xml,
@infrmcn_dgnstco xml,
@infrmcn_hsptlra xml,
@infrmcn_afldo xml,
@infrmcn_anxs xml,
@infrmcn_vldcion_espcial xml,
@estdo_ejccn varchar(5) OUTPUT,

@msje_errr varchar(3000) OUTPUT
AS
  SET NOCOUNT ON
  
	DECLARE @codigoExito char(2),
			@mensajeExito char(30),
			@codigoError char(5),
			@Mensaje Varchar(max),
			@i as int,
			@Max as int,
			@xstate int,
			@trancount int,
			@mdfcr_slctd int;
	
	SELECT	@codigoExito = 'OK',
			@codigoError = 'ET',
			@mensajeExito = 'Ejecución Exitosa',
			@mdfcr_slctd  = 2

	DECLARE @estdo_ejccn2 varchar(5),
			@msje_errr2 varchar(2000);

	BEGIN
		/*::::::::::::CREACIÓN DE TABLAS TEMPORALES QUE GUARDARÁN LOS CONSECUTIVOS GENERADOS POR CADA INSERCIÓN:::::::::::::: 
		:::::::::::::ÉSTO CON EL FIN DE EVITAR LA CONSULTA A TABLAS REALES QUE PUEDAN DEMORAR EL PROCESAMIENTO:::::::::::::::*/	 
		CREATE TABLE #IdAfiliados_Mod	(cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL) 
		CREATE TABLE #IdIPS_Mod			(cnsctvo_ips_slctd_atrzcn_srvco				udtConsecutivo NOT NULL, idXML	int NOT NULL)
		CREATE TABLE #IdHospitalaria_Mod(cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)					
		CREATE TABLE #IdMedico_Mod		(cnsctvo_mdco_trtnte_slctd_atrzcn_srvco		udtConsecutivo NOT NULL, idXML	int NOT NULL)
		CREATE TABLE #IdDiagnostico_Mod	(cnsctvo_dgnstco_slctd_atrzcn_srvco			udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
		CREATE TABLE #IdServicios_Mod	(cnsctvo_srvco_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL, cnsctvo_cdgo_tpo_srvco udtconsecutivo NOT NULL, nmro_prstcn INT)
		CREATE TABLE #IdServiciosOr_Mod	(cnsctvo_dto_srvco_slctdo_orgnl				udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
		CREATE TABLE #IdMedicamentos_Mod(cnsctvo_mdcmnto_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL)
		CREATE TABLE #IdInsumos_Mod		(cnsctvo_prcdmnto_insmo_slctdo				udtConsecutivo NOT NULL, idXML	int NOT NULL)	
		CREATE TABLE #ServiciosMod		(cnsctvo_srvco_slctdo						udtconsecutivo NOT NULL
										,cnsctvo_dto_srvco_slctdo_orgnl udtconsecutivo NOT NULL
										,idXML	int NOT NULL)
		/*:::::::::::::::CREACIÓN DE TABLAS TEMPORALES DONDE SE GUARDARÁ LA INFORMACIÓN DE LOS XML ENVIADOS::::::::::::::::::*/
  		CREATE TABLE #TMod_Solicitudes( cnsctvo_slctd_atrzcn_srvco_rcbdo	udtConsecutivo NULL	
										,id									Int  NOT NULL										
										,cnsctvo_cdgo_orgn_atncn			udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_srvco_slctdo		udtConsecutivo NULL
										,cnsctvo_cdgo_prrdd_atncn			udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_ubccn_pcnte		udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_slctd				udtConsecutivo NULL
										,jstfccn_clnca						varchar(2000) NULL
										,cnsctvo_cdgo_mdo_cntcto_slctd		udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_trnsccn_srvco_sld	udtConsecutivo NULL
										,nmro_slctd_prvdr					varchar(15) NULL
										,nmro_slctd_pdre					varchar(15) NULL
										,cdgo_eps							char(10) NULL
										,cnsctvo_cdgo_clse_atncn			udtConsecutivo NULL
										,cnsctvo_cdgo_frma_atncn			udtConsecutivo NULL
										,fcha_slctd							datetime NULL
										,nmro_slctd_atrzcn_ss				varchar(16) NULL
										,nmro_slctd_ss_rmplzo				varchar(15) NULL
										,cnsctvo_cdgo_ofcna_atrzcn			udtConsecutivo NULL
										,nuam								numeric(18,0) NULL
										,srvco_hsptlzcn						varchar(30) NULL
										,ga_atncn							varchar(30) NULL
										,cnsctvo_prcso_vldcn				udtConsecutivo NULL
										,cnsctvo_prcso_mgrcn				udtConsecutivo NULL
										,mgrda_gstn							int NULL
										,spra_tpe_st						udtLogico NULL
										,dmi								udtLogico NULL
										,obsrvcn_adcnl						UdtObservacion NULL
										,usro_crcn							udtUsuario NULL
										,cdgo_orgn_atncn					udtCodigo NULL
										,cdgo_tpo_srvco_slctdo				udtCodigo NULL
										,cdgo_prrdd_atncn					udtCodigo NULL
										,cdgo_tpo_ubccn_pcnte				udtCodigo NULL
										,cdgo_clse_atncn					char(3) NULL
										,cdgo_frma_atncn					udtCodigo NULL
										,cdgo_mdo_cntcto_slctd				udtCodigo NULL
										,cdgo_tpo_trnsccn_srvco_sld			udtCodigo NULL								
									)
		CREATE TABLE #TMod_Afiliados ( id											Int	NOT NULL
										,cnsctvo_cdgo_tpo_idntfccn_afldo			udtConsecutivo NULL
										,nmro_idntfccn_afldo						udtNumeroIdentificacionLargo NULL
										,cnsctvo_cdgo_dprtmnto_afldo				udtConsecutivo NULL
										,cnsctvo_cdgo_cdd_rsdnca_afldo				udtConsecutivo NULL
										,cnsctvo_cdgo_cbrtra_sld					udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_pln						udtConsecutivo NULL
										,cnsctvo_cdgo_pln							udtConsecutivo NULL
										,cnsctvo_cdgo_estdo_pln						udtConsecutivo NULL
										,cnsctvo_cdgo_sxo							udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_vnclcn_afldo				udtConsecutivo NULL
										,nmro_unco_idntfccn_afldo					udtConsecutivo NULL
										,cnsctvo_afldo_slctd_orgn					udtConsecutivo NULL
										,cnsctvo_cdgo_estdo_drcho					udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_cntrto					udtConsecutivo NULL
										,nmro_cntrto								udtNumeroFormulario NULL
										,cnsctvo_bnfcro_cntrto						udtConsecutivo NULL
										,cdgo_ips_prmra								udtCodigoIps NULL
										,cnsctvo_cdgo_sde_ips_prmra					udtConsecutivo NULL
										,rcn_ncdo									udtLogico NULL
										,prto_mltple								udtLogico NULL
										,nmro_hjo_afldo								udtConsecutivo NULL
										,cnsctvo_cdgo_sxo_rcn_ncdo					udtConsecutivo NULL
										,ttla										udtLogico NULL
										,cnsctvo_cdgo_chrte							udtConsecutivo NULL
										,cnsctvo_cdgo_rngo_slrl						udtConsecutivo NULL
										,usro_crcn									udtUsuario NULL					
										,prmr_aplldo_afldo							udtApellido NULL
										,sgndo_aplldo_afldo							udtApellido NULL
										,prmr_nmbre_afldo							udtNombre NULL
										,sgndo_nmbre_afldo							udtNombre NULL
										,cdgo_tpo_idntfccn_afldo					udtCodigo NULL
										,fcha_ncmnto_afldo							datetime NULL
										,drccn_afldo								udtDireccion NULL
										,tlfno_afldo								udtTelefono NULL
										,cdgo_dprtmnto_afldo						char(3) NULL
										,dscrpcn_dprtmnto_afldo						udtDescripcion NULL
										,cdgo_cdd_rsdnca_afldo						udtCiudad NULL
										,dscrpcn_cdd_rsdnca_afldo					udtDescripcion NULL
										,tlfno_cllr_afldo							char(30) NULL
										,crro_elctrnco_afldo						udtEmail NULL
										,cdgo_cbrtra_sld							varchar(5) NULL
										,cdgo_pln									udtCodigo NULL
										,cdgo_sxo									udtCodigo NULL
										,cdgo_sxo_rcn_ncdo							udtCodigo NULL
										,cdgo_tpo_pln								udtCodigo NULL
										,cdgo_tpo_vnclcn_afldo						udtCodigo NULL
										,cdgo_estdo_pln								udtCodigo NULL
										,fcha_ncmnto_rcn_ncdo						datetime NULL
									 )
		CREATE TABLE #TMod_IPS		 (  id											Int NOT NULL	
										,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte		udtConsecutivo NULL
										,nmro_idntfccn_ips_slctnte					udtNumeroIdentificacionLargo NULL
										,cdgo_intrno								udtCodigoIps NULL
										,nmro_indctvo_prstdr						udtConsecutivo NULL
										,cnsctvo_cdgo_dprtmnto_prstdr				udtConsecutivo NULL
										,cnsctvo_cdgo_cdd_prstdr					udtConsecutivo NULL
										,cnsctvo_ips_slctd_orgn						udtConsecutivo NULL
										,adscrto									udtLogico NULL
										,nmro_unco_idntfccn_prstdr					udtConsecutivo NULL
										,usro_crcn									udtUsuario NULL	
										,nmbre_ips									udtDescripcion NULL
										,nmbre_scrsl								udtDescripcion NULL
										,cdgo_tpo_idntfccn_ips_slctnte				udtCodigo NULL
										,dgto_vrfccn								int NULL
										,cdgo_hbltcn								char(12) NULL
										,drccn_prstdr								udtDireccion NULL
										,tlfno_prstdr								udtTelefono NULL
										,cdgo_dprtmnto_prstdr						char(3) NULL
										,cdgo_cdd_prstdr							udtCiudad NULL																
									)
										
		CREATE TABLE #TMod_Hospitalaria (  id								Int	NOT NULL
											,ds_estnca						udtConsecutivo NULL
											,nmro_vsts						udtConsecutivo NULL
											,fcha_ingrso_hsptlzcn			datetime NULL
											,fcha_egrso_hsptlzcn			datetime NULL
											,cnsctvo_cdgo_clse_hbtcn		udtConsecutivo NULL
											,cnsctvo_cdgo_srvco_hsptlzcn	udtConsecutivo NULL
											,cma							varchar(10) NULL
											,crte_cnta						udtConsecutivo
											,usro_crcn						udtUsuario NULL								
											,cdgo_clse_hbtcn				udtCodigo NULL
											,cdgo_srvco_hsptlzcn			udtCodigo NULL											
										)
											
		CREATE TABLE #TMod_Medicos (   id										Int	NOT NULL
										,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		udtConsecutivo NULL
										,cnsctvo_cdgo_espcldd_mdco_trtnte		udtConsecutivo NULL
										,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	udtConsecutivo NULL
										,nmro_idntfccn_mdco_trtnte				udtNumeroIdentificacionLargo NULL
										,rgstro_mdco_trtnte						varchar(15) NULL
										,cnsctvo_mdco_trtnte_slctd_orgn			udtConsecutivo NULL
										,adscrto								udtLogico NULL
										,nmro_unco_idntfccn_mdco				udtConsecutivo NULL
										,usro_crcn								udtUsuario NULL
										,cdgo_espcldd_mdco_trtnte				char(3) NULL
										,cdgo_tpo_idntfccn_mdco_trtnte			udtCodigo NULL
										,prmr_nmbre_mdco_trtnte					udtNombre NULL
										,sgndo_nmbre_mdco_trtnte				udtNombre NULL
										,prmr_aplldo_mdco_trtnte				udtApellido NULL
										,sgndo_aplldo_mdco_trtnte				udtApellido NULL
										,cdgo_tpo_aflcn_mdco_trtnte				udtCodigo NULL
									)
		CREATE TABLE #TMod_Diagnostico ( id_fila					INT IDENTITY(1,1) 
										,id							Int NOT NULL
										,nmro_prstcn				INT	
										,cnsctvo_cdgo_tpo_dgnstco	udtConsecutivo NULL
										,cnsctvo_cdgo_dgnstco		udtConsecutivo NULL
										,cnsctvo_cdgo_cntngnca		udtConsecutivo NULL
										,cnsctvo_cdgo_rcbro			udtConsecutivo NULL
										,usro_crcn					udtUsuario NULL
										,cdgo_dgnstco				udtCodigoDiagnostico NULL
										,cdgo_tpo_dgnstco			char(3) NULL
										,cdgo_cntngnca				Char(3) NULL
										,cdgo_rcbro					char(3) NULL
										)		 

		CREATE TABLE #TMod_Prestaciones (	id_fila									INT IDENTITY(1,1)
											,id										Int NOT NULL	
											,nmro_prstcn							INT
											,cnsctvo_cdgo_srvco_slctdo				udtConsecutivo NULL
											,cnsctvo_cdgo_tpo_srvco					udtConsecutivo NULL
											,dscrpcn_srvco_slctdo					udtDescripcion NULL 
											,cntdd_slctda							int NULL
											,fcha_prstcn_srvco_slctdo				datetime NULL
											,tmpo_trtmnto_slctdo					int NULL
											,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	udtConsecutivo NULL
											,vlr_lqdcn_srvco						udtConsecutivo NULL
											,cnsctvo_srvco_slctdo_orgn				udtConsecutivo NULL
											,cnsctvo_cdgo_tpo_atrzcn				udtConsecutivo NULL
											,cnsctvo_cdgo_prstcn_prstdr				udtConsecutivo NULL
											,cnfrmcn_cldd							udtLogico NULL
											,usro_crcn								udtUsuario NULL
											,cdgo_tpo_srvco							udtCodigo NULL
											,cdgo_srvco_slctdo						char(15) NULL
											,cdgo_undd_tmpo_trtmnto_slctdo			udtCodigo NULL
											,cdgo_prstcn_prstdr						udtCodigo NULL
											,cdgo_tpo_atrzcn						udtCodigo NULL								
											,dss									float NULL
											,cnsctvo_cdgo_prsntcn_dss				udtConsecutivo NULL
											,prdcdd_dss								float NULL
											,cnsctvo_cdgo_undd_prdcdd_dss			udtConsecutivo NULL
											,cnsctvo_cdgo_frma_frmctca				udtConsecutivo NULL
											,cnsctvo_cdgo_grpo_trptco				udtConsecutivo NULL
											,cnsctvo_cdgo_prsntcn					udtConsecutivo NULL
											,cnsctvo_cdgo_va_admnstrcn_mdcmnto		udtConsecutivo NULL
											,cnsctvo_mdcmnto_slctdo_orgn			udtConsecutivo NULL
											,cncntrcn_dss							char(20) NULL
											,prsntcn_dss							char(20) NULL
											,cnsctvo_cdgo_undd_cncntrcn_dss			udtConsecutivo NULL
											,cdgo_prsntcn_dss						udtCodigo NULL
											,cdgo_undd_prdcdd_dss					udtCodigo NULL
											,cdgo_frma_frmctca						char(4) NULL
											,cdgo_grpo_trptco						char(3) NULL
											,cdgo_prsntcn							char(4) NULL
											,cdgo_va_admnstrcn_mdcmnto				udtCodigo NULL
											,cdgo_undd_cncntrcn_dss					char(10) NULL									
											,cnsctvo_cdgo_ltrldd					udtConsecutivo NULL
											,cnsctvo_cdgo_va_accso					udtConsecutivo NULL
											,cdgo_ltrldd							udtCodigo NULL
											,cdgo_va_accso							udtcodigo NULL
										)	

		CREATE TABLE #tmp_tbASHistoricoEstadosServicioSolicitado(
										cnsctvo_hstrco_estdo_srvco_slctd	udtConsecutivo	NOT NULL,
										cnsctvo_srvco_slctdo				udtConsecutivo	NOT NULL,
									    cnsctvo_cdgo_estdo_srvco_slctdo		udtConsecutivo	NOT NULL,
										inco_vgnca							datetime		NOT NULL,
										fn_vgnca							datetime		NOT NULL,
										fcha_fn_vldz_rgstro					datetime		NOT NULL,
										vldo								udtLogico		NOT NULL,
										usro_crcn							udtUsuario		NOT NULL,
										fcha_crcn							datetime		NOT NULL,
										usro_ultma_mdfccn					udtUsuario		NOT NULL,
										fcha_ultma_mdfccn					datetime		NOT NULl,
										id									udtConsecutivo	NOT NULL
										)

		CREATE TABLE #TMod_Anexos (	id									Int	NOT NULL
										,cnsctvo_cdgo_mdlo_dcmnto_sprte		udtConsecutivo NULL
										,cnsctvo_cdgo_dcmnto_sprte			udtConsecutivo NULL
										,usro_crcn							udtUsuario NULL
									)	
		CREATE TABLE #TMod_ValidacionEspecial(
											   id								Int NOT NULL
											   ,nmro_vldcn_espcl				NUMERIC(18,0)
											   ,cnsctvo_cdgo_ofcna				udtConsecutivo NULL
											   ,usro_vldcn_espcl				udtUsuario NULL
											   ,accn_vldcn_espcl				udtDescripcion NULL
											   ,cnsctvo_ops						udtConsecutivo NULL
											   ,fcha_vldcn_espcl				DATETIME
											   ,usro_crcn						udtUsuario
											  )		
	
		BEGIN TRY

			set @trancount = @@trancount;
		 if @trancount = 0
            begin transaction
        else
            save transaction spASModSolicitudGeneral;

			-- Llenar Tablas Temporales
			EXEC BdCNA.gsa.spASModificarSolicitudLlenarTemporales	 @infrmcn_slctd 
																	,@infrmcn_prstcn 
																	,@infrmcn_prstdr 
																	,@infrmcn_mdco 
																	,@infrmcn_dgnstco 
																	,@infrmcn_hsptlra 
																	,@infrmcn_afldo 

																	,@infrmcn_anxs
																	,@infrmcn_vldcion_espcial
			-- Grabar solicicitudes
			
			EXEC BdCNA.gsa.spASModificarSolicitudSolicitudes 
			-- Grabar Afiliados
			EXEC BdCNA.gsa.spASModificarSolicitudAfiliados 
			-- Grabar IPS - Prestador
			EXEC BdCNA.gsa.spASModificarSolicitudIPS 
			--Grabar Información Hospitalaria
			EXEC BdCNA.gsa.spASModificarSolicitudInformacionHospitalaria 
			--Grabar Informacion de Medico
			EXEC BdCNA.gsa.spASModificarSolicitudMedico 
			--Grabar Diagnostico
			EXEC BdCNA.gsa.spASModificarSolicitudDiagnostico 
			-- Grabar Prestaciones
			EXEC BdCNA.gsa.spASModificarSolicitudPrestaciones 
			--Grabar Anexos
			EXEC BdCNA.gsa.spASModificarSolicitudAnexos 		
			--Grabar Validación Especial
			EXEC BdCNA.gsa.spASModificarValidacionEspecial

			
			EXEC BDcna.gsa.spASGuardarTrazaIndicadorReLiquidacion		 '-',
																		  @mdfcr_slctd,
																		  @msje_errr OUTPUT,
																		  @estdo_ejccn OUTPUT

			--Retornar
			SELECT		SOL.cnsctvo_slctd_atrzcn_srvco, SOL.nmro_slctd_atrzcn_ss
			FROM		BdCNA.gsa.tbASSolicitudesAutorizacionServicios	SOL WITH (NOLOCK)
			INNER JOIN	#TMod_Solicitudes									S WITH (NOLOCK)
			ON			SOL.cnsctvo_slctd_atrzcn_srvco = S.cnsctvo_slctd_atrzcn_srvco_rcbdo



			-- BORRAR TEMPORALES
			DROP TABLE #IdAfiliados_Mod	
			DROP TABLE #IdIPS_Mod
			DROP TABLE #IdHospitalaria_Mod
			DROP TABLE #IdMedico_Mod								
			DROP TABLE #IdDiagnostico_Mod						
			DROP TABLE #IdServicios_Mod							
			DROP TABLE #IdServiciosOr_Mod		
			DROP TABLE #IdMedicamentos_Mod								
			DROP TABLE #IdInsumos_Mod
			DROP TABLE #TMod_Solicitudes
			DROP TABLE #TMod_Afiliados
			DROP TABLE #TMod_IPS
			DROP TABLE #TMod_Hospitalaria
			DROP TABLE #TMod_Medicos
			DROP TABLE #TMod_Diagnostico			
			DROP TABLE #TMod_Prestaciones
			DROP TABLE #tmp_tbASHistoricoEstadosServicioSolicitado
			DROP TABLE #TMod_Anexos
			DROP TABLE #ServiciosMod
			DROP TABLE #TMod_ValidacionEspecial			

			SELECT	@estdo_ejccn = @codigoExito
					,@msje_errr = @mensajeExito

			if @trancount = 0   
			commit;
		END TRY
		BEGIN CATCH

			
				SET @msje_errr = 'Number:' + CAST(ERROR_NUMBER() AS char(15)) + CHAR(13) +
				'Line:' + CAST(ERROR_LINE() AS char(15)) + CHAR(13) +
				'Message:' + ERROR_MESSAGE() + CHAR(13) +
				'Procedure:' + ERROR_PROCEDURE();
				SET @estdo_ejccn = @codigoError;
				
				SET @xstate = XACT_STATE()
				
				if @xstate = -1
					rollback;
				if @xstate = 1 and @trancount = 0
					rollback
				if @xstate = 1 and @trancount > 0
					rollback transaction spASModSolicitudGeneral;
		END CATCH ;
	END

GO
