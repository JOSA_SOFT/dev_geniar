USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASHomologarPrestacionesPrestador]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASHomologarPrestacionesPrestador 											
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Sp Poblar Temporales para ejecución Masiva ó por demanda de la Homologación de Prestaciones
						  del prestador																				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/29																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASHomologarPrestacionesPrestador]



AS

Begin
	SET NOCOUNT ON
	Declare @fechaactual  datetime = getdate(),
			@ValorCERO    int = 0

	-- Paso 1. Homologamos las prestaciones con las del prestador
	Update      ss
	Set         cnsctvo_cdgo_prstcn_prstdr = hp.cnsctvo_cdgo_prstcn_prstdr,
				hmlgdo = 'S'
	From		#ServiciosSolicitados ss
	Inner Join	bdsisalud.dbo.tbHomologacionPrestaciones hp with(nolock)
	On			hp.cdgo_intrno = ss.cdgo_intrno
	And			hp.cnsctvo_cdfccn = ss.cnsctvo_cdgo_srvco_slctdo
	Inner Join	bdSiSalud.dbo.tbDetalle_HomologacionPrestaciones dhp with(nolock)
	ON			hp.cnsctvo_cdgo_hmlgcn_prstcn = dhp.cnsctvo_cdgo_hmlgcn_prstcn
	Inner Join	bdSiSalud.dbo.tbClasificacionPrestacionPrestador dcp with(nolock)
	ON			(hp.cnsctvo_cdgo_clsfccn_prstcn_prstdr = dcp.csctvo_cdgo_clsfccn_prstcn_prstdr)
	Inner Join	bdSiSalud.dbo.tbPrestacionesPrestador pp with(nolock)
	ON			hp.cnsctvo_cdgo_prstcn_prstdr = pp.cnsctvo_cdgo_prstcn_prstdr
	Inner Join  bdSiSalud.dbo.tbDetalle_PrestacionesPrestador dpp with(nolock)
	ON			dpp.cnsctvo_cdgo_prstcn_prstdr = pp.cnsctvo_cdgo_prstcn_prstdr
	WHERE		@fechaActual between dpp.inco_vgnca AND   dpp.fn_vgnca
	AND			@fechaActual between dhp.inco_vgnca AND   dhp.fn_vgnca 

	-- Paso 2.  Si no hay homologación se deja la misma prestación de SOS

	Update		#ServiciosSolicitados
	Set			cnsctvo_cdgo_prstcn_prstdr = isnull(cnsctvo_cdgo_srvco_slctdo,0)
	Where		cnsctvo_cdgo_prstcn_prstdr = @ValorCERO



End
GO
