USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarTrazaIndicadorReLiquidacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASGuardarTrazaIndicadorReLiquidacion]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASGuardarTrazaIndicadorReLiquidacion] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarTrazaIndicadorReLiquidacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					     A\>
* Descripcion			: <\D Sp utiliazado para guardar la traza de los indicadores D\>
						  <\D .													     D\>
* Observaciones			: <\O  													     O\>
* Parametros			: <\P \>
* Variables				: <\V  													     V\>
* Fecha Creacion		: <\FC 2016/02/20										    FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASGuardarTrazaIndicadorReLiquidacion]
	@usr_lgn			udtusuario,
	@tpo_llmdo			int,
	@mensaje			VARCHAR(1000) OUTPUT,
	@codigo_error		VARCHAR(100) OUTPUT	
AS
BEGIN

	SET NOCOUNT ON;

	Declare @fcha_hra				datetime,
			@id_ngco				varchar(50),
			@fcha_frmto				varchar(15),
			@cdgo_err				CHAR(5)		= 'ERROR',
			@indcdr					int,
			@prcso_actvdd			int,
			@clse_indcdr			varchar(100),
			@da_smna				int;

	Create table #tempSolicitudesInd(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		usr_lgn							udtUsuario	
	);


	Set @fcha_frmto		= FORMAT(getDate(), 'ddMMyyyy');
	Set @fcha_hra		= getDate();
	Set @id_ngco		= concat('MEGA',@fcha_frmto);
    Set @da_smna		= DATEPART(day, 0);

	BEGIN TRY

		--Tipo llamado desde Reliquidación
		If @tpo_llmdo = 1 
		Begin
			Set @indcdr			= 9;
			Set @prcso_actvdd	= 9;
			Set @clse_indcdr	= 'Indicador Reliquidación';
			Insert Into #tempSolicitudesInd(cnsctvo_slctd_atrzcn_srvco, usr_lgn)
			Select cnsctvo_slctd_atrzcn_srvco, @usr_lgn from #cnslddo_slctud
		end
		
		--Tipo llamado desde Reliquidación
		If @tpo_llmdo = 2 
		Begin
			Set @indcdr			= 10;
			Set @prcso_actvdd	= 10;
			Set @clse_indcdr	= 'Indicador Calidad';
			Insert Into #tempSolicitudesInd(cnsctvo_slctd_atrzcn_srvco, usr_lgn)
			Select cnsctvo_slctd_atrzcn_srvco_rcbdo, usro_crcn from #TMod_Solicitudes
		end

		if Not exists(Select 1 from #tempSolicitudesInd si
					  Inner Join BDCNA.IND.TBASDETALLEINDICADOR di WITH (NOLOCK)
					  On si.cnsctvo_slctd_atrzcn_srvco = di.instnca_prcso) 
		Begin
			INSERT INTO BDCNA.IND.TBASDETALLEINDICADOR 
				(  
					FCHA_HRA, 
					INSTNCA_PRCSO, 
					ID_NGCIO, 
					ID_INDCDR, 
					ID_PRCSO_ACTVDD, 
					PRPTRO_INSTNCA, 
					DA_SMNA, 
					VLR_NMRCO, 
					VLR_FCHA, 
					VLR_DCML, 
					VLR_TXTO, 
					STDO_RGSTRO
				) 
			SELECT  
					@fcha_hra,
					CS.cnsctvo_slctd_atrzcn_srvco,
					Concat(CS.cnsctvo_slctd_atrzcn_srvco, @id_ngco),
					@indcdr,
					@prcso_actvdd,
					CS.usr_lgn,
					@da_smna,
					CS.cnsctvo_slctd_atrzcn_srvco,
					@fcha_hra,
					0,
					@clse_indcdr,
					1
			FROM	#tempSolicitudesInd CS
		End
	END TRY
	BEGIN CATCH
					Set @mensaje = Concat(
											'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
											'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
											'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
											'Procedure:' , ERROR_PROCEDURE()
										);
					SET @codigo_error = @cdgo_err;
					RAISERROR (@codigo_error, 16, 2) With SETERROR
	END CATCH

	Drop table #tempSolicitudesInd
END


GO
