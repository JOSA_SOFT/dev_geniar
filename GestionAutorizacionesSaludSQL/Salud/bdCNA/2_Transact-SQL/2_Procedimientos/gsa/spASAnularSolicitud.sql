USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASAnularSolicitud]    Script Date: 8/14/2017 4:33:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASAnularSolicitud
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS A\>
* Descripcion			: <\D Anula la solicitud cambiando el estado tbASSolicitudesAutorizacionServicios D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2015/12/02 FC\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Juan Carlos Vásquez G.	AM\>
* Descripcion			 : <\DM	#01 Se modifica por las siguientes causas
								1. Incluir Desasociar Hospitalización por solicitud o Numero Unico OPS
								2. Incluir Desasociar Prog.Entrega por solicitud ó Numero Unico OPS		
								3. Incluir Anulación Cuotas de Recuperación	por solicitud o Numero Unico OPS 
						   DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	@cta_rcprcn = Indicador Cuota de Recuperación VM\>
* Fecha Modificacion	 : <\FM 2016/11/09	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andrés López Ramírez AM\>
* Descripcion			 : <\DM	Se cambia de posicion la anulacion de cuotas de recuperacion,
							    este proceso se hacia despues de cambiar el estado de la solicitud
								por lo cual no encontraba informacion para la anulacion de estas 
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2016/12/22	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones		DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se modifica condicional para anulacion de cuotas de 
								recuperacion.
								Se saca consulta para obtener el consecutivo de la solicitud, de la tabla #anular, 
								de los condicionales  y se deja una sola.
								Se cambia la variable de referencia enviada al sp
								spASEjecutarAnulacionCuotasRecuperacion DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/04/25	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>
* Descripcion			 : <\DM	Se realizan ajustes en el llamado a la anulacion de cuotas de recuperacion. DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/05/12	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	Se realizan ajustes al procedimiento para que permita la anulacion de los 
                                servicios
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2018/08/04	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se realizan ajustes al procedimiento para que no tome la anulacion de OPS que tengan valor 0
                           DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2018/08/14	FM\>
*-------------------------------------------------------------------------------------------------------------------------------------*/

--Exec gsa.spASAnularSolicitud Null, Null, 88430379, 5, 'Se anula la Solicitud', 'user1', null, null

ALTER PROCEDURE [gsa].[spASAnularSolicitud] 

@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo = null,
@cnsctvo_srvco_slctdo       udtConsecutivo = null,
@nmro_unco_ops				udtConsecutivo = null,
@cnsctvo_cdgo_mtvo_csa		udtConsecutivo,
@obsrvcn					udtObservacion,
@usuario_sistema		    udtUsuario,
@cdgo_rsltdo			    udtConsecutivo  output,
@mnsje_rsltdo			    Varchar(2000)	output

AS

BEGIN
	SET NOCOUNT ON;

	Declare @codigo_ok						 Int           ,
			@codigo_error					 Int           ,
			@mensaje_ok					     udtDescripcion,
			@mensaje_error					 Varchar(2000),
			@cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo,
			@dscrpcn_estdo_srvco_slctdo		 udtDescripcion,
			@srvco_hsptlzcn					 udtLogico     ,    
			@prgrmcn_entrga					 udtLogico     ,
			@cta_rcprcn						 udtLogico     ,
			@anlr_Slctd						 udtLogico     ,
			@ldCnsctvo_slctd_atrzcn_srvco	 udtConsecutivo,
			@ldCnsctvo_srvco_slctdo			 udtConsecutivo,
			@si								 udtLogico     ,
			@si_dgto						 udtLogico     ,
			@no								 udtLogico     ,
			@cdgo_msg						 udtCodigo     ,
			@msg							 udtDescripcion,
			@ldfecha_validacion				 Datetime      ,
			@ldOrgn_mdfccn					 Varchar(100)  ,
			@elmnto_orgn_mdfccn				 udtdescripcion,
			@dto_elmnto_orgn_mdfccn			 udtdescripcion,
			@cntdd_srvcs                     Int           ,
			@vlr_uno                         Int           ;		


	Create  
	Table  #anular(	nmro_unco_ops	                udtConsecutivo,							  
			   		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
					cnsctvo_mdcmnto_slctdo			udtConsecutivo,
					cnsctvo_srvco_slctdo			udtConsecutivo,
			 		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
					cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo,
					cnsctvo_cncpto_prcdmnto_slctdo	udtConsecutivo		  
				   ) 

	Create 
	Table #tmpTrazaModificacion (cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
								 cnsctvo_srvco_slctdo			udtConsecutivo,
								 cnsctvo_cncpto_prcdmnto_slctdo udtConsecutivo,
								 cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
								 fcha_mdfccn					datetime,
								 orgn_mdfccn					varchar(100),
								 vlr_antrr						varchar(20),
								 vlr_nvo						varchar(20),
								 usro_mdfccn					udtUsuario,
								 obsrvcns						udtObservacion,
								 nmro_ip						varchar(12),
								 fcha_crcn						datetime,
								 usro_crcn						udtUsuario,
								 fcha_ultma_mdfccn				datetime,
								 usro_ultma_mdfccn				udtUsuario,
								 elmnto_orgn_mdfccn				udtDescripcion,
								 dto_elmnto_orgn_mdfccn			udtDescripcion,
								 dscrpcn_vlr_antrr				udtDescripcion,
								 dscrpcn_vlr_nvo				udtDescripcion
								)

	Create 
	Table #tmpSolicitudAnulacionCuotasRecuperacion(
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- número de la solicitud
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestación o servicio
		nmro_unco_ops					UdtConsecutivo  
	)

	Set @cnsctvo_cdgo_estdo_srvco_slctdo = 14  -- Estado Anulada
	Set @dscrpcn_estdo_srvco_slctdo		 = 'ANULADA'
	Set @codigo_error					 = -1  -- Error SP
	Set @codigo_ok						 =  0  -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente'
	Set @si								 = 'S'
	Set @si_dgto						 = '1'
	Set @no								 = 'N'
	Set @ldfecha_validacion				 = getDate()
	Set @ldOrgn_mdfccn					 = 'tbASConceptosServicioSolicitado.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo'	
	Set @vlr_uno                         = 1


	--Obtiene los datos a Anular cuando se envía el codigo de la OPS
	If(@nmro_unco_ops IS NOT NULL And @nmro_unco_ops > 0)
		Begin
			  --Se obtiene el registro de los procedimientos de la OPS a anular
			  Insert 
			  Into       #anular(cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_cdgo_estdo_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo, nmro_unco_ops)
			  Select     cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,  nmro_unco_ops
			  From       bdcna.gsa.tbASConceptosServicioSolicitado WITH (NOLOCK)
			  Where      nmro_unco_ops = @nmro_unco_ops
			  And		 (cnsctvo_prcdmnto_insmo_slctdo is not null or cnsctvo_prcdmnto_insmo_slctdo <> '')
			  Group By   cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,  nmro_unco_ops
		  
			  --Se obtiene el registro de los medicamentos de la OPS a anular
			  Insert 
			  Into       #anular(cnsctvo_mdcmnto_slctdo, cnsctvo_cdgo_estdo_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo, nmro_unco_ops)
			  Select     cnsctvo_mdcmnto_slctdo, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,  nmro_unco_ops		  
			  From       bdcna.gsa.tbASConceptosServicioSolicitado  WITH (NOLOCK)
			  Where      nmro_unco_ops = @nmro_unco_ops
			  And       (cnsctvo_mdcmnto_slctdo is not null or cnsctvo_mdcmnto_slctdo <> '')
			  Group By   cnsctvo_mdcmnto_slctdo, cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_cncpto_prcdmnto_slctdo,  nmro_unco_ops

			  --Se obtiene consecutivo de la prestación asociada al procedimiento.
			  Update     a
			  Set        cnsctvo_srvco_slctdo		= b.cnsctvo_srvco_slctdo,
						 cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco
			  From       #anular a
			  Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
			  On         b.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
			  Inner Join BDCna.gsa.tbASServiciosSolicitados c WITH (NOLOCK)
			  On		 c.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
			  Where      a.cnsctvo_prcdmnto_insmo_slctdo Is Not Null
		  
			  --Se obtiene consecutivo de la prestación asociada al medicamento.
			  Update     a
			  Set        cnsctvo_srvco_slctdo		= b.cnsctvo_srvco_slctdo,
						 cnsctvo_slctd_atrzcn_srvco = c.cnsctvo_slctd_atrzcn_srvco
			  From       #anular a
			  Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
			  On         b.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
			  Inner Join BDCna.gsa.tbASServiciosSolicitados c WITH (NOLOCK)
			  On		 c.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
			  Where      a.cnsctvo_mdcmnto_slctdo Is Not Null
			  
			  Set @elmnto_orgn_mdfccn		= 'OPS'
			  Set @dto_elmnto_orgn_mdfccn	= 'Anulación'

		End
	
	If(@cnsctvo_slctd_atrzcn_srvco IS NOT NULL)
		Begin
			 -- Se obtienen los registros de la Solicitud a anular
			 Insert 
			 Into       #anular(cnsctvo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_srvco_slctdo)
			 Select     ss.cnsctvo_srvco_slctdo, a.cnsctvo_slctd_atrzcn_srvco, ss.cnsctvo_cdgo_estdo_srvco_slctdo 
			 From	    BDCna.gsa.tbASSolicitudesAutorizacionServicios a WITH (NOLOCK)
			 Inner Join BDcna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
			 On         a.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
			 Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco				 			

			 Set @anlr_Slctd                   = @si			
			 Set @elmnto_orgn_mdfccn		   = 'Solicitud'
			 Set @dto_elmnto_orgn_mdfccn   	   = 'Anulación'
		End

	If(@cnsctvo_srvco_slctdo IS NOT NULL)
		Begin
			 -- Se obtienen los registros de la Solicitud a anular
			 Insert 
			 Into   #anular(cnsctvo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_srvco_slctdo)
			 Select cnsctvo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_estdo_srvco_slctdo 
			 From	bdCNA.gsa.tbASServiciosSolicitados WITH (NOLOCK)
			 Where  cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo	
			 	 						 
			 Select     @cntdd_srvcs = Count(a.cnsctvo_slctd_atrzcn_srvco)
			 From       bdCNA.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
			 Inner Join #anular b
			 On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
			 
			 If (@cntdd_srvcs = @vlr_uno)
			    Begin
				   Set @anlr_Slctd = @si
				End			 
			 
			 Set @elmnto_orgn_mdfccn		= 'Prestacion'
			 Set @dto_elmnto_orgn_mdfccn	= 'Anulación'
		End 


	If(@nmro_unco_ops IS NULL)
	   Begin
	        -- Se obtiene el consecutivo del procedimiento asociado a la prestación
			 Update     a 
			 Set        cnsctvo_prcdmnto_insmo_slctdo = PIS.cnsctvo_prcdmnto_insmo_slctdo
			 From       #anular a
			 Inner Join bdCNA.gsa.tbASProcedimientosInsumosSolicitados PIS WITH (NOLOCK)
			 On         PIS.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			 -- Se obtiene el consecutivo del medicamento asociado a la prestación 
			 Update     a 
			 Set        cnsctvo_mdcmnto_slctdo = MS.cnsctvo_mdcmnto_slctdo
			 From       #anular a
			 Inner Join bdCNA.gsa.tbASMedicamentosSolicitados MS WITH (NOLOCK)
			 On         MS.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo		

			 Update		a
		     Set		nmro_unco_ops = css.nmro_unco_ops 
		     From		#anular a
		     Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		     On			css.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
		     Where		css.cnsctvo_mdcmnto_slctdo Is Null

		     Update		a
		     Set		nmro_unco_ops = css.nmro_unco_ops 
		     From		#anular a
		     Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		     On			css.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
		     Where		css.cnsctvo_prcdmnto_insmo_slctdo Is Null
	   End

	

	-- Inicio sisjvg01 - 2016/11/09
	--Set @ldCnsctvo_slctd_atrzcn_srvco = (Select Top 1 cnsctvo_slctd_atrzcn_srvco From  #anular)
	Select  Top 1
			@ldCnsctvo_slctd_atrzcn_srvco = cnsctvo_slctd_atrzcn_srvco,
			@ldCnsctvo_srvco_slctdo       = cnsctvo_srvco_slctdo
	From    #anular
	-- Fin sisjvg01 - 2016/11/09

	--Llamado a la generacion de notas credito
	EXEC cja.spRCGenerarNotaCreditoAnulacion @usuario_sistema

	BEGIN TRY
		--Si la solicitud genero cobro cuota de recuperación se debe llamar el bdcna.gsa.spASEjecutarAnulacionCuotasRecuperacion
		--Set @cta_rcprcn = (Select top 1 gnra_cbro From bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion WITH (NOLOCK)
		--					   Where cnsctvo_slctd_atrzcn_srvco = @ldCnsctvo_slctd_atrzcn_srvco)	

		--Anula las cuotas de recuperacion, este proceso debe hacerse antes de cambiar el estado de la solicitud.
		If (Exists(	Select 1 From bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion WITH (NOLOCK)
					Where cnsctvo_slctd_atrzcn_srvco = @ldCnsctvo_slctd_atrzcn_srvco
					And	gnra_cbro = @si	)
			)
			Begin
				-- Se anulan las cuotas de recuperacion
				Insert #tmpSolicitudAnulacionCuotasRecuperacion
				(
							cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,			nmro_unco_ops
				)
				Select		a.cnsctvo_slctd_atrzcn_srvco,	ss.cnsctvo_cdgo_srvco_slctdo,		a.nmro_unco_ops
				From		#anular a
				Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(noLock)
				On			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
				And			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco

				Exec BDCna.gsa.spASEjecutarAnulacionCuotasRecuperacion @usuario_sistema,@msg, @cdgo_msg, Null, Null, Null

			End

		--Se actualiza el estado de las prestaciones a anulado
		Update     ss 
		Set        cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo,
				   fcha_ultma_mdfccn			   = @ldfecha_validacion,
				   usro_ultma_mdfccn               = @usuario_sistema
		From       bdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
		Inner Join #anular a 
		On         a.cnsctvo_srvco_slctdo = SS.cnsctvo_srvco_slctdo


		--Se actualiza el estado de los registros en la tabla tbASConceptosServicioSolicitado a anulado (OPS-CUPS)
		Update     css 
		Set        cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo,
				   fcha_ultma_mdfccn					  = @ldfecha_validacion,
				   usro_ultma_mdfccn				      = @usuario_sistema
		From       bdCNA.gsa.tbASConceptosServicioSolicitado css WITH (NOLOCK)
		Inner Join #anular a 
		On         a.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo


		--Se actualiza el estado de los registros en la tabla tbASConceptosServicioSolicitado a anulado (OPS-CUMS)
		Update     css 
		Set        cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo,
				   fcha_ultma_mdfccn					  = @ldfecha_validacion,
				   usro_ultma_mdfccn					  = @usuario_sistema
		From       bdCNA.gsa.tbASConceptosServicioSolicitado css WITH (NOLOCK)
		Inner Join #anular a 
		On         a.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo
					
		--Se valida si el estado de todas las prestaciones es anulado cuando se anula una OPS.
		If(@nmro_unco_ops Is Not Null)
			Begin
				Set @anlr_Slctd = @no
				-- Se valida si todas las prestaciones estan anuladas para proceder a anular la Solicitud
				If Not exists(Select cnsctvo_srvco_slctdo 
					          From   bdCNA.gsa.tbASServiciosSolicitados WITH (NOLOCK)
							  Where  cnsctvo_slctd_atrzcn_srvco = @ldCnsctvo_slctd_atrzcn_srvco
							  And    cnsctvo_cdgo_estdo_srvco_slctdo <> @cnsctvo_cdgo_estdo_srvco_slctdo
							 )						
					Begin
						Set @anlr_Slctd = @si
					End
			End

		--Se actualiza el estado de la Solicitud a anulado si la petición se realiza con el consecutivo solicitud
		If @anlr_Slctd = @si 
			Begin
				Update     sas 
				Set        cnsctvo_cdgo_estdo_slctd = @cnsctvo_cdgo_estdo_srvco_slctdo,
						   fcha_ultma_mdfccn		= @ldfecha_validacion,
						   usro_ultma_mdfccn		= @usuario_sistema
				From       BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH (NOLOCK)
				Inner Join #anular a 
				On         a.cnsctvo_slctd_atrzcn_srvco = sas.cnsctvo_slctd_atrzcn_srvco;
			End

		--Se debe hacer el llamado a este SP para que guarde la traza de la gestión
		Insert 
		Into   #tmpTrazaModificacion(cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo  , cnsctvo_cncpto_prcdmnto_slctdo,
			                         cnsctvo_cdgo_mtvo_csa     , fcha_mdfccn           , orgn_mdfccn                   ,					
			                         vlr_antrr                 , vlr_nvo               , usro_mdfccn                   ,					
			                         obsrvcns                  , nmro_ip               , fcha_crcn                     ,						
			                         usro_crcn                 , fcha_ultma_mdfccn     , usro_ultma_mdfccn             ,
			                         elmnto_orgn_mdfccn        , dto_elmnto_orgn_mdfccn, dscrpcn_vlr_antrr             ,
			                         dscrpcn_vlr_nvo
			                        )
		Select     a.cnsctvo_slctd_atrzcn_srvco     , a.cnsctvo_srvco_slctdo          , a.cnsctvo_cncpto_prcdmnto_slctdo,
			       @cnsctvo_cdgo_mtvo_csa           , @ldfecha_validacion             , @ldOrgn_mdfccn                  ,				
			       a.cnsctvo_cdgo_estdo_srvco_slctdo, @cnsctvo_cdgo_estdo_srvco_slctdo, @usuario_sistema                ,
			       @obsrvcn                         , '127.0.0.1'                     , @ldfecha_validacion             ,
			       @usuario_sistema                 , @ldfecha_validacion             , @usuario_sistema                ,
			       @elmnto_orgn_mdfccn              , @dto_elmnto_orgn_mdfccn         , e.dscrpcn_estdo_srvco_slctdo    ,
			       @dscrpcn_estdo_srvco_slctdo	
		From       #anular a
		Inner Join BDCna.prm.tbASEstadosServiciosSolicitados_Vigencias e WITH (NOLOCK)
		On         e.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_srvco_slctdo
		Where      @ldfecha_validacion Between e.inco_vgnca And e.fn_vgnca
		                 							  
		Exec BDCna.gsa.spASGuardarTrazaModificacion;
		
		--Si la solicitud está relacionado a una hospitalización se debe llamar el CU MEGA_CU_061_DesasociarHospitalización 
		Select top 1 @srvco_hsptlzcn = srvco_hsptlzcn
		From   bdCNA.gsa.tbASSolicitudesAutorizacionServicios WITH (NOLOCK)
		Where  cnsctvo_slctd_atrzcn_srvco = @ldCnsctvo_slctd_atrzcn_srvco

		If (@srvco_hsptlzcn = @si or @srvco_hsptlzcn = @si_dgto)
			Begin
					--Inicio  #01 sisjvg01 - 2016/11/09
				If(@cnsctvo_slctd_atrzcn_srvco IS NOT NULL and @cnsctvo_slctd_atrzcn_srvco <> '') 
					-- Anulación por solicitud
					Begin
						Exec BDCna.gsa.spASDesasociarHospitalizacion @ldCnsctvo_slctd_atrzcn_srvco, null, @cdgo_msg, @msg
					End
				Else
					-- Anulación Por Numero Unico OPS es decir por prestación
					Begin
						Exec BDCna.gsa.spASDesasociarHospitalizacion @ldCnsctvo_slctd_atrzcn_srvco, @ldCnsctvo_srvco_slctdo, @cdgo_msg, @msg
					End
			End

		--Si la solicitud está relacionado a una programación de entrega se debe llamar el CU MEGA_CU_062_DesasociarProgramaciónEntrega  
		Select top 1 @prgrmcn_entrga = prgrmcn_entrga 
		From   bdCNA.gsa.tbASServiciosSolicitados WITH (NOLOCK)
		Where  cnsctvo_slctd_atrzcn_srvco = @ldCnsctvo_slctd_atrzcn_srvco
		
		If (@prgrmcn_entrga = @si or @prgrmcn_entrga = @si_dgto)
			Begin
				--Inicio  #01 sisjvg01 - 2016/11/09
				If(@cnsctvo_slctd_atrzcn_srvco IS NOT NULL and @cnsctvo_slctd_atrzcn_srvco <> '') 
					Begin-- Anulación por solicitud
						Exec BDCna.gsa.spASDesasociarProgramacionEntrega @ldCnsctvo_slctd_atrzcn_srvco, null, @cdgo_msg, @msg
					End
				Else
					Begin-- Anulación Por Numero Unico OPS es decir por prestación
						Exec BDCna.gsa.spASDesasociarProgramacionEntrega @ldCnsctvo_slctd_atrzcn_srvco, @ldCnsctvo_srvco_slctdo, @cdgo_msg, @msg
					End
				-- Fin  #01 sisjvg01 - 2016/11/09
		End					
		
		Set @cdgo_rsltdo	=		@codigo_ok
		Set @mnsje_rsltdo	=		@mensaje_ok
			
	END TRY
	BEGIN CATCH
		SET @mensaje_error = 'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
			   					'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
								'Message:' + ERROR_MESSAGE() + CHAR(13) +
								'Procedure:' + ERROR_PROCEDURE();
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH
	
	Drop Table #tmpTrazaModificacion
	Drop Table #anular

END
