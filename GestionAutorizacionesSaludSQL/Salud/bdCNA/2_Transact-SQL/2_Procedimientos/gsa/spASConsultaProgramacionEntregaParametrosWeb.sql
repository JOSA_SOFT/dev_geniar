USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaProgramacionEntregaParametrosWeb]    Script Date: 12/09/2016 11:14:34 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultaProgramacionEntregaParametrosWeb]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultaProgramacionEntregaParametrosWeb] AS SELECT 1;'
END
GO

/**-----------------------------------------------------------------------------------------------------------------
* Método o PRG		: spASConsultaProgramacionEntregaParametrosWeb							
* Desarrollado por	: <\A Ing. Victor Hugo Gil Ramos A\>	
* Descripción		: <\D 
                         Proyecto Mega: consulta la programacion de Entrega uilizando los parametros 
						 enviados desde la aplicacion web
                      D\>
* Observaciones		: <\O 	O\>	
* Parámetros		: <\P 	P\>	
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/07/14 FC\>
*--------------------------------------------------------------------------------------------------------------------
* * Modificado Por	: <\A Rafael Cano A\>      
* Descripción		: <\D
							se agrego la holgura de 5 dias segun parametro para la fecha de finalizacion de la OPS	
						D\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  FM\>
*---------------------------------------------------------------------------------------------------------
* Modificado Por	: <\A Jorge Rodriguez A\>      
* Descripción		: <\D
							Se adiciona columna Oficina dentro del select y se devuelve para mostrar en pantalla	
						D\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2017-01-04 FM\>
*--------------------------------------------------------------------------------------------------------------------*/
---EXEC gsa.spASConsultaProgramacionEntregaParametrosWeb 33368292, '20160101', '20160315', 11  , 151 , 5   , 106051, NULL, NULL
---EXEC gsa.spASConsultaProgramacionEntregaParametrosWeb 33000627, '20140107', '20140331', Null, Null, Null, Null, NULL, NULL 
---EXEC gsa.spASConsultaProgramacionEntregaParametrosWeb 31802043, '20160701', '20161231', Null, Null, Null, Null, NULL, NULL

ALTER PROCEDURE  [gsa].[spASConsultaProgramacionEntregaParametrosWeb]      
	@nmro_unco_idntfccn	     udtconsecutivo       ,
	@fcha_dsde				 Datetime       = NULL,
	@fcha_hsta				 Datetime       = NULL,
	@cnsctvo_clsfccn_evnto	 udtConsecutivo = NULL,
	@cnsctvo_estdo_entrga	 udtConsecutivo = NULL,
	@cnsctvo_tpo_prstcn		 udtConsecutivo = NULL, 
	@cnsctvo_prstcn			 udtConsecutivo = NULL,
	@nmro_acta				 varchar(15)	= NULL,
	@nmro_ntfccn			 udtConsecutivo = NULL
AS 
   
BEGIN
	SET NOCOUNT ON    	
	
	DECLARE @estdo			           udtLogico     ,
			@fcha_actl		           Datetime      ,			
			@lcAux                     udtLogico     ,
			@lcAux_espco               Char(1)       ,
			@lnCnsctvo_cdgo_pln        udtConsecutivo,
			@lcSubsidiado              Varchar(10)   ,
			@lcContributivo            Varchar(12)   ,
			@lcCada                    Varchar(6)    ,
			@lnCnsctvo_cdgo_tpo_cdfccn udtConsecutivo,
			@lcSeparador               Char(3)       ,			
			@lncnsctvo_prstcn_cms      udtConsecutivo,
			@lncnsctvo_prstcn_cps      udtConsecutivo,
			@lncnsctvo_prstcn_cs       udtConsecutivo,
			@lcSentenceSql             Varchar(2000) ,
	        @lcWhereSql                Varchar(2000),
			@cdgo_prmtro_gnrl CHAR(3) = '103',
			@vsble_usro   CHAR(1) = 'S',
			@cdgo_prmtro_gnrl_vgnca CHAR(3) = '103',
			@tpo_dto_prmtro  CHAR(1) = 'N',
			@vlr_prmtro_nmrco NUMERIC(18,0),
			@vlr_prmtro_crctr VARCHAR(200),
			@vlr_prmtro_fcha DATETIME,
			@fcha_utlzcn_hsta_hlgra DATETIME;

    Create 
	Table  #tbParamProgramacion(cnsctvo_prgrmcn_prstcn              udtConsecutivo,	
	                            cnsctvo_cdgo_ofcna                  udtConsecutivo, 
								cnsctvo_det_prgrmcn_fcha            udtConsecutivo, 
								fcha_entrga                         Datetime      ,          
		                        cntdd                               Int           ,	
								cnsctvo_cdgo_estds_entrga           udtConsecutivo, 
								cnsctvo_prgrmcn_fcha_evnto          udtConsecutivo, 
								prvdr                               Varchar(250)  ,          
		                        cnsctvo_gnrco                       udtConsecutivo,	
								nmro_unco_idntfccn_prstdr           udtConsecutivo, 
								cnsctvo_cdgo_tpo_prstdr             udtConsecutivo, 
								cdgo_intrno                         udtCodigoIPS  ,
							    vlr_prstcn                          Float         ,	
								cnsctvo_prstcn                      udtConsecutivo, 
								usro_mdfccn                         udtUsuario    , 
								cnsctvo_cdgo_cncpto                 udtConsecutivo,
							    cnsctvo_cdgo_det_prgrmcn_fcha_evnto udtConsecutivo, 
								acta                                Char(50)      , 
								cnsctvo_cdgo_ntfccn                 udtConsecutivo, 
								cnsctvo_cdgo_clsfccn_evnto          udtConsecutivo, 
							    cnsctvo_cdgo_dgnstco                udtConsecutivo, 
								cnsctvo_cdgo_tpo_idntfccn           udtConsecutivo, 
								nmro_idntfccn                       Varchar(23)   , 
								prmr_aplldo                         udtApellido   , 
							    sgndo_aplldo                        udtApellido   , 
								prmr_nmbre                          udtNombre     , 
								sgndo_nmbre                         udtNombre     , 
								nmro_autrzcn                        udtConsecutivo, 
							    cnsctvo_cdgo_ofcna_ops              udtConsecutivo, 
								tpo_usrio                           Varchar(20)   ,
								cnsctvo_cdgo_tpo_cdfccn			    udtConsecutivo,
								fcha_fnlzcn							Datetime
							   )
	Create
	Table  #tbDetProgramacion(cnsctvo_prgrmcn_prstcn			  udtConsecutivo,            
							  cnsctvo_cdgo_ofcna				  udtConsecutivo,            
							  cnsctvo_det_prgrmcn_fcha		      udtConsecutivo,            
							  fcha_entrga						  Datetime      ,              
							  cntdd							      Int           ,            
							  cnsctvo_cdgo_estds_entrga		      udtConsecutivo,   
							  cnsctvo_prgrmcn_fcha_evnto		  udtConsecutivo,            
							  prvdr						          Varchar(250)  ,            
			                  cnsctvo_gnrco					      udtConsecutivo,            
						      nmro_unco_idntfccn_prstdr		      udtConsecutivo,            
							  cnsctvo_cdgo_tpo_prstdr			  udtConsecutivo,            
							  cdgo_intrno						  udtCodigoIPS  ,     
							  vlr_prstcn						  Float         ,          
							  cnsctvo_prstcn					  udtConsecutivo,
							  usro_mdfccn						  udtUsuario    ,
							  cnsctvo_cdgo_cncpto				  udtConsecutivo,
							  cnsctvo_cdgo_det_prgrmcn_fcha_evnto udtConsecutivo,
							  acta							      Char(50),--int, 
							  cnsctvo_cdgo_ntfccn				  udtConsecutivo, 
						  	  cnsctvo_cdgo_clsfccn_evnto		  udtConsecutivo,
							  dscrpcn_clsfccn_evnto			      udtDescripcion,          
							  dscrpcn_cdfccn					  udtDescripcion,
							  cdgo_cdfccn						  udtDescripcion,
							  dscrpcn_estdo_entrga			      udtDescripcion,
							  cnsctvo_cdgo_dgnstco			      udtConsecutivo,
							  cdgo_dgnstco					      udtCodigoDiagnostico,
							  dscrpcn_dgnstco					  udtDescripcion,
							  ttl_undds_trtmnto				      Int           ,
							  fcha_dsde_trtmnto				      Datetime      ,
							  fcha_hsta_trtmnto				      Datetime      ,
							  cntdd_dss						      Float         ,
							  cnsctvo_cdgo_dss				      udtConsecutivo,
							  cntdd_cda						      Float         ,
						 	  cnsctvo_cdgo_frcnca_cda			  udtConsecutivo,
							  dscrpcn_dss						  udtDescripcion,
							  dscrpcn_frcnca					  udtDescripcion,
							  pslgia						      udtDescripcion,
							  cnsctvo_cdgo_tpo_cdfccn			  udtConsecutivo,
							  cnsctvo_prsntcn					  udtConsecutivo,
							  cntdd_prsntcn					      Varchar(15)   ,
							  cnsctvo_cncntrcn				      Int           ,
							  cntdd_cncntrcn					  Varchar(15)   ,
							  cnsctvo_cdgo_va_admnstrcn		      udtConsecutivo,
							  cnsctvo_frma_frmctca			      udtConsecutivo,
							  dscrpcn_frma_frmctca			      udtDescripcion,
							  dscrpcn_cncntrcn				      udtDescripcion,
							  dscrpcn_prsntcn					  udtDescripcion,
							  dscrpcn_va_admnstrcn			      udtDescripcion,
							  tpo_usrio					          Varchar(20)   ,
							  cnsctvo_cdgo_tpo_idntfccn		      udtConsecutivo,
							  nmro_idntfccn					      Varchar(23)   ,
							  prmr_aplldo						  udtApellido   ,
							  sgndo_aplldo					      udtApellido   ,
							  prmr_nmbre						  udtNombre     ,
							  sgndo_nmbre						  udtNombre     ,
							  cdgo_tpo_idntfccn				      udtCodigo     ,
							  mdco_trtnte						  Varchar(200)  ,
							  nmro_autrzcn					      udtConsecutivo,
							  cnsctvo_cdgo_ofcna_ops			  udtConsecutivo,
							  fcha_fnlzcn						  Datetime,
							  cdgo_ofcna						  varchar(11)
 	                         ) 

	Set @estdo                     = 'A'
	Set @fcha_actl                 = getDate()		
	Set @lcAux                     = ' '
	Set @lcAux_espco               = ''
	Set @lnCnsctvo_cdgo_pln        = 9
	Set @lcSubsidiado              = 'Subsidiado'
	Set @lcContributivo            = 'Contributivo'
	Set @lcCada                    = ' Cada '
	Set @lnCnsctvo_cdgo_tpo_cdfccn = 5
	Set @lcSentenceSql             = NULL
	Set @lcWhereSql                = NULL
	Set @lcSeparador               = ' - '
	Set @lncnsctvo_prstcn_cms      = 5
	Set @lncnsctvo_prstcn_cps      = 4
	Set @lncnsctvo_prstcn_cs       = 9
				    		
	Insert 
	Into   #tbParamProgramacion(cnsctvo_prgrmcn_prstcn             , cnsctvo_cdgo_ofcna       , cnsctvo_det_prgrmcn_fcha  , fcha_entrga               ,          
		                        cntdd                              , cnsctvo_cdgo_estds_entrga, cnsctvo_prgrmcn_fcha_evnto, prvdr                     ,          
		                        cnsctvo_gnrco                      , nmro_unco_idntfccn_prstdr, cnsctvo_cdgo_tpo_prstdr   , cdgo_intrno               ,
							    vlr_prstcn                         , cnsctvo_prstcn           , usro_mdfccn               , cnsctvo_cdgo_cncpto       ,
							    cnsctvo_cdgo_det_prgrmcn_fcha_evnto, acta                     , cnsctvo_cdgo_ntfccn       , cnsctvo_cdgo_clsfccn_evnto, 
							    cnsctvo_cdgo_dgnstco               , cnsctvo_cdgo_tpo_idntfccn, nmro_idntfccn             , prmr_aplldo               , 
							    sgndo_aplldo                       , prmr_nmbre               , sgndo_nmbre               , nmro_autrzcn              , 
							    cnsctvo_cdgo_ofcna_ops             , cnsctvo_cdgo_tpo_cdfccn  , tpo_usrio				  , fcha_fnlzcn									   	      
	                          )  
	Select     a.cnsctvo_prgrmcn_prstcn             , a.cnsctvo_cdgo_ofcna           , b.cnsctvo_det_prgrmcn_fcha  , b.fcha_entrga                 ,              
               b.cntdd                              , b.cnsctvo_cdgo_estds_entrga    , b.cnsctvo_prgrmcn_fcha_evnto, @lcAux                        ,            
               e.cnsctvo_gnrco                      , e.nmro_unco_idntfccn_prstdr    , e.cnsctvo_cdgo_tpo_prstdr   , e.cdgo_intrno                 ,            
		       e.vlr_prstcn                         , a.cnsctvo_prstcn               , b.usro_mdfccn               , e.cnsctvo_cdgo_cncpto         ,
		       b.cnsctvo_cdgo_det_prgrmcn_fcha_evnto, a.cnsctvo_pr_ntfccn            , a.cnsctvo_cdgo_ntfccn       , afi.cnsctvo_cdgo_clsfccn_evnto, 
		       afi.cnsctvo_cdgo_dgnstco             , actua.cnsctvo_cdgo_tpo_idntfccn, actua.nmro_idntfccn         , actua.prmr_aplldo             , 
		       actua.sgndo_aplldo                   , actua.prmr_nmbre               , actua.sgndo_nmbre           , b.cnsctvo_ops                 , 
		       b.cnsctvo_cdgo_ofcna_ops             , d.cnsctvo_cdgo_tpo_cdfccn      , 
			   Case When actua.cnsctvo_cdgo_pln = @lnCnsctvo_cdgo_pln Then @lcSubsidiado Else @lcContributivo End as tpo_usrio,
			   b.fcha_fn_vgnca 
	From	   bdSiSalud.dbo.tbActuaNotificacion actua  With(Nolock)
	Inner Join bdSiSalud.dbo.tbProgramacionPrestacion a	With(Nolock)	
	On         a.cnsctvo_cdgo_ntfccn = actua.cnsctvo_ntfccn      And 
	           a.cnsctvo_cdgo_ofcna	 = actua.cnsctvo_cdgo_ofcna
	Inner Join bdSiSalud.dbo.tbAfiliadosMarcados afi    With(Nolock)	
	On         afi.cnsctvo_ntfccn     = actua.cnsctvo_ntfccn     And 
	           afi.cnsctvo_cdgo_ofcna = actua.cnsctvo_cdgo_ofcna --esta tabla tambien se puede relacionar con tbActuaNotificacion
	Inner Join bdSiSalud.dbo.tbDetProgramacionFechaEvento b With(Nolock)	
	ON         b.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn
	Inner Join bdSiSalud.dbo.tbDetProgramacionProveedores e	With(Nolock)	
	On         e.cnsctvo_prgrmcn_prstcn   = b.cnsctvo_prgrmcn_prstcn   And			
	           e.cnsctvo_det_prgrmcn_fcha = b.cnsctvo_det_prgrmcn_fcha			   
	Inner Join bdSiSalud.dbo.tbCodificaciones d	With(Nolock)	
	On         d.cnsctvo_cdfccn = a.cnsctvo_prstcn
	Where      actua.nmro_unco_idntfccn  =  @nmro_unco_idntfccn /*Numero unico de identificacion del afiliado SIEMPRE SE DEBE TENER ESTE FILTRO*/	
	And        e.estdo                   =  @estdo
	and		   d.cnsctvo_cdgo_tpo_cdfccn In (@lncnsctvo_prstcn_cms, @lncnsctvo_prstcn_cps, @lncnsctvo_prstcn_cs)
	
	Set @lcSentenceSql = 'SELECT ds.cnsctvo_prgrmcn_prstcn			   ,'  + CHAR(13) + --consecutivo de la programacion 
						        'ds.cnsctvo_cdgo_ofcna	               ,'  + CHAR(13) + --Id de la oficina donde se hizo la notificacion						        
						        'ds.cnsctvo_det_prgrmcn_fcha		   ,'  + CHAR(13) + --consecutivo de la progrmacion segun las entregas que tiene la prestacion
						        'ds.fcha_entrga						   ,'  + CHAR(13) + --fecha en la cual esta programada debe realizarse la entrega				
								'ds.cntdd						       ,'  + CHAR(13) + --cantidad de la prestacion que se entregara en esa entrega				
								'ds.cnsctvo_cdgo_estds_entrga		   ,'  + CHAR(13) + --consecutivo de estado				
								'ds.cnsctvo_prgrmcn_fcha_evnto         ,'  + CHAR(13) + --consecutvo de programacion de la fecha evento			
								'ds.prvdr						       ,'  + CHAR(13) + --nombre del prestador asignado para entregar la prestacion				
								'ds.cnsctvo_gnrco					   ,'  + CHAR(13) + --consecutivo generico				
								'ds.nmro_unco_idntfccn_prstdr		   ,'  + CHAR(13) + --numero unico de identificacion del prestador				
								'ds.cnsctvo_cdgo_tpo_prstdr			   ,'  + CHAR(13) + --consecutivo del tipo de prestador										        
								'ds.cdgo_intrno			               ,'  + CHAR(13) + --codigo interno del prestador	
						        'ds.vlr_prstcn		                   ,'  + CHAR(13) + --valor de la prestacion
						        'ds.cnsctvo_prstcn		               ,'  + CHAR(13) + --Id de la prestacion
						        'ds.usro_mdfccn		                   ,'  + CHAR(13) + --usuario que realizo la ultima modificacion
						        'ds.cnsctvo_cdgo_cncpto     		   ,'  + CHAR(13) + --consecutivo del concepto
								'ds.cnsctvo_cdgo_det_prgrmcn_fcha_evnto,'  + CHAR(13) + --id que identifica el regsitro 
								'ds.acta                               ,'  + CHAR(13) + --consecutivo de la notificacion
								'ds.cnsctvo_cdgo_ntfccn     		   ,'  + CHAR(13) + --numero de notificacion								
								'ds.cnsctvo_cdgo_clsfccn_evnto     	   ,'  + CHAR(13) + --consecutivo de la clasificacion del evento
								'ds.cnsctvo_cdgo_dgnstco     		   ,'  + CHAR(13) + --consecutivo del diagnostico
								'ds.cnsctvo_cdgo_tpo_idntfccn     	   ,'  + CHAR(13) + --consecutivo del tipo de identificacion del afiliado
								'ds.nmro_idntfccn				       ,'  + CHAR(13) + --numero de identificacion del afiliado								
								'ds.prmr_aplldo				           ,'  + CHAR(13) + --primer apellido del afiliado
								'ds.sgndo_aplldo				       ,'  + CHAR(13) + --segundo apellido del afiliado
								'ds.prmr_nmbre				           ,'  + CHAR(13) + --primer nombre del afiliado
								'ds.sgndo_nmbre				           ,'  + CHAR(13) + --segundo nombre del afiliado
								'ds.nmro_autrzcn				       ,'  + CHAR(13) + --numero de autorizacion						        
								'ds.cnsctvo_cdgo_ofcna_ops			   ,'  + CHAR(13) + --consecutivo de la oficina de la autorizacion
								'ds.cnsctvo_cdgo_tpo_cdfccn			   ,'  + CHAR(13) + --consecutivo de la oficina de la autorizacion
						        'ds.tpo_usrio	                       ,'  + CHAR(13) + --tipo usuario
								'ds.fcha_fnlzcn	                       '  + CHAR(13) + --fecha finalizacion
						 'From   #tbParamProgramacion ds '			       + CHAR(13) +
						 'WHERE  1=1'			   					       + CHAR(13) 	
	
	If (@cnsctvo_clsfccn_evnto != '' OR @cnsctvo_clsfccn_evnto IS NOT NULL)
	   Begin
		  Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_clsfccn_evnto = ' + CONVERT(VARCHAR,@cnsctvo_clsfccn_evnto) + CHAR(13) 
       End

	If (@cnsctvo_estdo_entrga != '' OR @cnsctvo_estdo_entrga IS NOT NULL) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_estds_entrga = ' + CONVERT(VARCHAR,@cnsctvo_estdo_entrga)   + CHAR(13) 
       End	

	If (@cnsctvo_tpo_prstcn != '' OR @cnsctvo_tpo_prstcn IS NOT NULL) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_tpo_cdfccn = ' + CONVERT(VARCHAR,@cnsctvo_tpo_prstcn)     + CHAR(13) 
       End	

	If (@cnsctvo_prstcn != '' OR @cnsctvo_prstcn IS NOT NULL) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_prstcn = ' + CONVERT(VARCHAR,@cnsctvo_prstcn)     + CHAR(13) 
       End

	If (@fcha_dsde != '' OR @fcha_dsde IS NOT NULL) And (@fcha_hsta != '' OR @fcha_hsta IS NOT NULL) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.fcha_entrga Between ' + Char(39) + CONVERT(VARCHAR,@fcha_dsde) + Char(39) + ' And ' + Char(39) + CONVERT(VARCHAR,@fcha_hsta) + Char(39)
       End

	If (@nmro_ntfccn != '' AND @nmro_ntfccn IS NOT NULL AND @nmro_ntfccn != 0) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_ntfccn = ' + CONVERT(VARCHAR,@nmro_ntfccn)     + CHAR(13) 
       End
	   
	If (@nmro_acta != ''  OR  @nmro_acta IS NOT NULL ) 
       Begin
          Set @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.acta = ' + Char(39) + CONVERT(VARCHAR, @nmro_acta) + Char(39) + CHAR(13) 
       End
	   

    SET @lcSentenceSql = @lcSentenceSql + ISNULL(@lcWhereSql,'') 
	
	Insert 
	Into   #tbDetProgramacion (cnsctvo_prgrmcn_prstcn             ,	cnsctvo_cdgo_ofcna       , cnsctvo_det_prgrmcn_fcha  , fcha_entrga               ,          
		                       cntdd                              ,	cnsctvo_cdgo_estds_entrga, cnsctvo_prgrmcn_fcha_evnto, prvdr                     ,          
		                       cnsctvo_gnrco                      ,	nmro_unco_idntfccn_prstdr, cnsctvo_cdgo_tpo_prstdr   , cdgo_intrno               ,
							   vlr_prstcn                         ,	cnsctvo_prstcn           , usro_mdfccn               , cnsctvo_cdgo_cncpto       ,
							   cnsctvo_cdgo_det_prgrmcn_fcha_evnto, acta                     , cnsctvo_cdgo_ntfccn       , cnsctvo_cdgo_clsfccn_evnto, 
							   cnsctvo_cdgo_dgnstco               , cnsctvo_cdgo_tpo_idntfccn, nmro_idntfccn             , prmr_aplldo               , 
							   sgndo_aplldo                       , prmr_nmbre               , sgndo_nmbre               , nmro_autrzcn              , 
							   cnsctvo_cdgo_ofcna_ops             , cnsctvo_cdgo_tpo_cdfccn  , tpo_usrio				 , fcha_fnlzcn
							  )
	Exec(@lcSentenceSql)

	--buscamos total unidad tratamientos, desde y hasta del tratamiento, total unidades tratamiento, y datos de la posologia
	Update     #tbDetProgramacion 
	Set        ttl_undds_trtmnto       = b.cntdd_entrgs           , -- total de unidad del tratamiento
               fcha_dsde_trtmnto       = b.fcha_dsde              ,	-- desde tratamiento	
               fcha_hsta_trtmnto       = b.fcha_hsta              ,	-- hasta tratamiento
               cntdd_dss               = b.cntdd_dss              ,	-- cantidad dosis de la posologia
               cnsctvo_cdgo_dss        = b.cnsctvo_cdgo_dss       ,	-- unidad de la dosis de la posologia
               cntdd_cda               = b.cntdd_cda              ,	-- cantidad cada de la posologia
               cnsctvo_cdgo_frcnca_cda = b.cnsctvo_cdgo_frcnca_cda	-- unidad cada de la posologia
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbProgramacionFechaEvento b With(Nolock) 
	On         b.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn
	and a.cnsctvo_prgrmcn_fcha_evnto = b.cnsctvo_prgrmcn_fcha_evnto
	
	--buscamos los nombres de las unidades de la posologia
	Update     #tbDetProgramacion Set  dscrpcn_dss = b.dscrpcn_dss
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbMNDosis b With(Nolock) 
	On         b.cnsctvo_cdgo_dss = a.cnsctvo_cdgo_dss
	Inner Join bdSiSalud.dbo.tbMNDosis_Vigencias m With(Nolock) 
	On         m.cnsctvo_cdgo_dss = b.cnsctvo_cdgo_dss
	Where      @fcha_actl Between m.inco_vgnca And m.fn_vgnca

	--buscamos los nombres de las unidades de la posologia del cada
	Update     #tbDetProgramacion 
	Set        dscrpcn_frcnca = b.dscrpcn_frcnca
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbMNFrecuencia b With(Nolock) 
	On         b.cnsctvo_cdgo_frcnca = a.cnsctvo_cdgo_frcnca_cda
	Inner Join bdSiSalud.dbo.tbMNFrecuencia_Vigencias f With(Nolock)
	On         f.cnsctvo_cdgo_frcnca = b.cnsctvo_cdgo_frcnca
	Where      @fcha_actl Between f.inco_vgnca And f.fn_vgnca

	--calculamos la posologia
	Update     #tbDetProgramacion 
	Set        pslgia = Concat(Case When cntdd_dss Is Not Null 
									Then Concat(Cast(cntdd_dss As Varchar(10)), @lcAux, dscrpcn_dss) 
									Else @lcAux_espco 
								End,
								@lcCada, Cast(cntdd_cda As Varchar(10)), @lcAux, dscrpcn_frcnca
							   )

	--buscamos la clasificacion del evento
	Update     #tbDetProgramacion 
	Set        dscrpcn_clsfccn_evnto = b.dscrpcn_clsfccn_evnto
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbClasificacionEventosNotificacion b With(Nolock) 
	On         b.cnsctvo_cdgo_clsfccn_evnto = a.cnsctvo_cdgo_clsfccn_evnto
	Inner Join bdSiSalud.dbo.tbClasificacionEventosNotificacion_Vigencias c With(Nolock)
	On         c.cnsctvo_cdgo_clsfccn_evnto = b.cnsctvo_cdgo_clsfccn_evnto
	Where      @fcha_actl Between c.inco_vgnca And c.fn_vgnca

	-- buscamos el estado de la entrega
	Update     #tbDetProgramacion 
	Set        dscrpcn_estdo_entrga = b.dscrpcn_estdo_ntfccn
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbEstadosNotificacion b With(Nolock)
	On         b.cnsctvo_cdgo_estdo_ntfccn = a.cnsctvo_cdgo_estds_entrga
	Inner Join bdSiSalud.dbo.tbEstadosNotificacion_Vigencias e With(Nolock)
	On         @fcha_actl Between e.inco_vgnca And e.fn_vgnca

	-- buscamos la prestacion, en caso de CUMS se debe asociar su concentracion y presentacion
	Update     #tbDetProgramacion 
	Set        dscrpcn_cdfccn		   = b.dscrpcn_cdfccn         ,
               cnsctvo_cdgo_tpo_cdfccn = b.cnsctvo_cdgo_tpo_cdfccn,
			   cdgo_cdfccn			   = b.cdgo_cdfccn 
	From       #tbDetProgramacion a
	Inner Join bdSiSalud.dbo.tbCodificaciones b With(Nolock)
	On         b.cnsctvo_cdfccn = a.cnsctvo_prstcn
	 
	-- buscamos el proveedor
	Update      #tbDetProgramacion 
	Set         prvdr = b.nmbre_scrsl
	From        #tbDetProgramacion a 
	Inner Join  bdSiSalud.dbo.tbDireccionesPrestador b With(Nolock)
	On          b.cdgo_intrno = a.cdgo_intrno
	Inner Join  bdSiSalud.dbo.tbDireccionesPrestador_Vigencias p With(Nolock)
	On          p.cdgo_intrno = a.cdgo_intrno
	Where       @fcha_actl Between p.inco_vgnca And p.fn_vgnca

	-- buscamos el diagnostico 
	Update      #tbDetProgramacion 
	Set         cdgo_dgnstco	= b.cdgo_dgnstco,
		        dscrpcn_dgnstco = b.dscrpcn_dgnstco
	From        #tbDetProgramacion a 
	Inner Join  bdSiSalud.dbo.tbDiagnosticos b With(Nolock) 
	On          b.cnsctvo_cdgo_dgnstco = a.cnsctvo_cdgo_dgnstco
	Inner Join  bdSiSalud.dbo.tbDiagnosticos_Vigencias d With(Nolock) 
	On          d.cnsctvo_cdgo_dgnstco = b.cnsctvo_cdgo_dgnstco
	Where       @fcha_actl Between d.inco_vgnca And d.fn_vgnca

	--bucamos datos del medicamento CUMS
	Update     #tbDetProgramacion 
	Set        cnsctvo_prsntcn			 = b.cnsctvo_prsntcn,
               cntdd_prsntcn			 = b.cntdd_prsntcn,
               cnsctvo_cncntrcn			 = b.cnsctvo_cncntrcn,
               cntdd_cncntrcn			 = b.cntdd_cncntrcn,
               cnsctvo_cdgo_va_admnstrcn = b.cnsctvo_cdgo_va_admnstrcn,
               cnsctvo_frma_frmctca		 = b.cnsctvo_frma_frmctca
	From       #tbDetProgramacion a 
	Inner Join bdSiSalud.dbo.tbCums b With(NoLock) 
	On         b.cnsctvo_cms = a.cnsctvo_prstcn
	Inner Join bdSiSalud.dbo.tbCums_vigencias c With(NoLock) 
	On         c.cnsctvo_cms = b.cnsctvo_cms
	Where      a.cnsctvo_cdgo_tpo_cdfccn = @lnCnsctvo_cdgo_tpo_cdfccn --medicamento
	And        @fcha_actl Between c.inco_vgnca And c.fn_vgnca

	-- buscamos la unidad de la concentracion del CUMS
	Update     #tbDetProgramacion 
	Set        dscrpcn_cncntrcn = b.dscrpcn_cncntrcn
	From       #tbDetProgramacion a 
	Inner Join bdSiSalud.dbo.tbConcentracion b With(NoLock) 
	On         b.cnsctvo_cdgo_cncntrcn = a.cnsctvo_cncntrcn
	Inner Join bdSiSalud.dbo.tbConcentracion_Vigencias c With(NoLock) 
	On         c.cnsctvo_cdgo_cncntrcn = b.cnsctvo_cdgo_cncntrcn
	Where      @fcha_actl Between c.inco_vgnca And c.fn_vgnca

	-- buscamos la unidad de la presentacion del CUMS
	Update     #tbDetProgramacion 
	Set        dscrpcn_prsntcn 	= b.dscrpcn_prsntcn
	From       #tbDetProgramacion a 
	Inner Join bdSiSalud.dbo.tbPresentaciones b With(NoLock) 
	On         b.cnsctvo_cdgo_prsntcn = a.cnsctvo_cncntrcn
	Inner Join bdSiSalud.dbo.tbPresentaciones_Vigencias p With(NoLock)
	On         p.cnsctvo_cdgo_prsntcn = b.cnsctvo_cdgo_prsntcn
	Where      @fcha_actl Between p.inco_vgnca And p.fn_vgnca

	-- buscamos forma farmaceutica del CUMS
	Update     #tbDetProgramacion 
	Set        dscrpcn_frma_frmctca = b.dscrpcn_frma_frmctca
	From       #tbDetProgramacion a 
	Inner Join bdSiSalud.dbo.tbFormaFarmaceutica b With(NoLock)
	On         b.cnsctvo_cdgo_frma_frmctca = a.cnsctvo_frma_frmctca
	Inner Join bdSiSalud.dbo.tbFormaFarmaceutica_Vigencias f With(NoLock)
	On         f.cnsctvo_cdgo_frma_frmctca = b.cnsctvo_cdgo_frma_frmctca
	Where      @fcha_actl Between f.inco_vgnca And f.fn_vgnca

	-- buscamos la via de adminitracion del CUMS
	Update     #tbDetProgramacion 
	Set        dscrpcn_va_admnstrcn = b.dscrpcn_va_admnstrcn
	From       #tbDetProgramacion a 
	Inner Join bdSiSalud.dbo.tbMNViasAdministracion b With(NoLock)
	On         b.cnsctvo_cdgo_va_admnstrcn = a.cnsctvo_cdgo_va_admnstrcn
	Inner Join bdSiSalud.dbo.tbMNViasAdministracion_Vigencias v With(NoLock)
	On         v.cnsctvo_cdgo_va_admnstrcn = b.cnsctvo_cdgo_va_admnstrcn
	Where      @fcha_actl Between v.inco_vgnca And v.fn_vgnca

	-- buscamos el tipo de  identicacion del afiliado
	Update     #tbDetProgramacion 
	Set        cdgo_tpo_idntfccn = b.cdgo_tpo_idntfccn    
	From       #tbDetProgramacion a 
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion b With(NoLock)
	On         b.cnsctvo_cdgo_tpo_idntfccn = a.cnsctvo_cdgo_tpo_idntfccn
	Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias i With(NoLock)
	On         i.cnsctvo_cdgo_tpo_idntfccn = b.cnsctvo_cdgo_tpo_idntfccn
	Where      @fcha_actl Between i.inco_vgnca And i.fn_vgnca
	

	--Se adiciona ejecución de SP para que devuelva la fecha de finalizacion mas el tiempo de Holgura permitido.
  Exec bdSiSalud.dbo.spTraerParametroGeneral  @cdgo_prmtro_gnrl, 
											  @vsble_usro, 
											  @cdgo_prmtro_gnrl_vgnca, 
											  @vsble_usro, 
											  @tpo_dto_prmtro, 
											  @vlr_prmtro_nmrco Output, 
											  @vlr_prmtro_crctr Output, 
											  @vlr_prmtro_fcha Output;

	-- se actualiza la temporal 
	Update     #tbDetProgramacion 
	Set        fcha_fnlzcn = DATEADD(day, @vlr_prmtro_nmrco, fcha_fnlzcn) 
	From       #tbDetProgramacion 


	--Se actualiza el código de la oficina
	Update     dp 
	Set        cdgo_ofcna = ofi.cdgo_ofcna
	From       #tbDetProgramacion  dp
	Inner Join BDAfiliacionValidador.dbo.tbOficinas_Vigencias ofi With(NoLock)
	On ofi.cnsctvo_cdgo_ofcna = dp.cnsctvo_cdgo_ofcna
	Where @fcha_actl between ofi.inco_vgnca and ofi.fn_vgnca 

	--datos a mostrar en pantalla cuando se consulta una entrega MEGA_PIU_080_Consulta de Programaciones de Entrega
	Select	 cnsctvo_det_prgrmcn_fcha	                                                                                        ,--consecutivo de la progrmacion segun las entregas que tiene la prestacion
	         fcha_entrga                                                                                                        ,--fecha en la cual esta programada debe realizarse la entrega				
	         Concat(cdgo_cdfccn, @lcAux,dscrpcn_cdfccn, @lcAux, cntdd_cncntrcn, @lcAux, dscrpcn_cncntrcn, @lcAux, dscrpcn_prsntcn) AS dscrpcn_prstcn,--nombre de la prestacion concatenando concentracion y presentacion
	         cntdd                                                                                                              ,--cantidad de la prestacion que se entregara en esa entrega
			 dscrpcn_estdo_entrga                                                                                               ,--estado en el cual esta la entrega
			 prvdr                                                                                                              ,--nombre del prestador asignado para entregar la prestacion
			 dscrpcn_clsfccn_evnto                                                                                              ,--evento de notificacion por el cual se origino la programacion
			 nmro_autrzcn                                                                                                       ,--numero de autorizacion (OPS) con la cual se entrego
			 usro_mdfccn                                                                                                        ,--usuario que realizo la ultima modificacion			
			 --datos adicionales que no seran mostrados en el listado
			 cnsctvo_cdgo_det_prgrmcn_fcha_evnto                                                                                ,--Id que identifica el regsitro 
			 dscrpcn_cdfccn
			 cdgo_cdfccn                                                                                                     ,--nombre de la prestacion sin concatenar concentracion y presentacion
			 cnsctvo_cdgo_ntfccn                                                                                                ,--numero de notificacion
			 cnsctvo_cdgo_ofcna                                                                                                 ,--Id de la oficina donde se hizo la notificacion
			 acta                                                                                                               ,--numero del acta
			 cnsctvo_prstcn                                                                                                     ,--Id de la prestacion
		 	 cdgo_dgnstco                                                                                                       ,--codigo del diagnostico
			 dscrpcn_dgnstco                                                                                                    ,--nombre del diagnostico
			 mdco_trtnte                                                                                                        ,--nombres y apellidos del medico tratante, por ahora no se cuenta con el
			 ttl_undds_trtmnto                                                                                                  ,--cantidad total de unidades del tratamiento
			 Concat(Convert(Varchar(10),fcha_dsde_trtmnto,111), @lcSeparador, Convert(Varchar(10),fcha_hsta_trtmnto,111)) As drcn_trtmnto,--duracion total del tratamiento
			 fcha_dsde_trtmnto                                                                                                  ,--fecha desde la que inicia el tratamiento
			 fcha_hsta_trtmnto                                                                                                  ,--fecha hasta la que finaliza el tratamiento
			 pslgia                                                                                                             ,--posologia compuesta por la dosis y frecuencia, la dosis no se diligencia para CUOS
			 --campos que conforman la posologia
			 cntdd_dss                                                                                                          ,--cantidad dosis
			 cnsctvo_cdgo_dss                                                                                                   ,--consecutivo dosis
			 cntdd_cda                                                                                                          ,--cantidad frecuencia dosis
			 cnsctvo_cdgo_frcnca_cda                                                                                            ,--consecutivo frecuencia dosis
			 dscrpcn_dss                                                                                                        ,--descripcion dosis
			 dscrpcn_frcnca                                                                                                     ,--descripcion frecuencia	 
			 --fin campos que conforman la posologia
			 cntdd_prsntcn                                                                                                      ,--cantidad presentacion
			 dscrpcn_prsntcn                                                                                                    ,--descripcion presentacion           
			 cntdd_cncntrcn                                                                                                     ,--cantidad concentracion
			 dscrpcn_cncntrcn                                                                                                   ,--descripcion concentracion 
			 dscrpcn_frma_frmctca                                                                                               ,--descripcion forma farmaceutica 
			 dscrpcn_va_admnstrcn                                                                                               ,--descripcion via adminitracion
			 --datos del afiliado
			 tpo_usrio                                                                                                          ,--tipo de usuario
			 cdgo_tpo_idntfccn                                                                                                  ,--codigo tipo identiificacion    
			 nmro_idntfccn                                                                                                      ,--numero de identificacion
			 prmr_aplldo                                                                                                        ,--primer apellido
			 sgndo_aplldo                                                                                                       ,--segundo apellido
			 prmr_nmbre                                                                                                         ,--primer nombre
			 sgndo_nmbre                                                                                                        ,--segundo nombre
			 --fin datos del afiliado
			 cnsctvo_cdgo_ofcna_ops                                                                                             ,--Si el campo numero_autorizacion no es Null y este campo si lo es quiere decir que la entrega tiene asociada una OPS del nuevo aplicativo de MEGA y no de Sipres
			 cnsctvo_cdgo_estds_entrga                                                                                          ,--consecutivo de estado	
			 fcha_fnlzcn																										,--fecha de finalizacion
			 cdgo_ofcna																											 -- Codigo de la oficina
	From     #tbDetProgramacion
	Order By fcha_entrga Asc

	Drop Table #tbParamProgramacion
	Drop Table #tbDetProgramacion
End
GO
