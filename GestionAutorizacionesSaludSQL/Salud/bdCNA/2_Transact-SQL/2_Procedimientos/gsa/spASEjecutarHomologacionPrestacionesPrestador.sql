USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarHomologacionPrestacionesPrestador]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarHomologacionPrestacionesPrestador												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Controlador Para ejecución Masiva ó por demanda de la Homologación de Prestaciones
						  del prestador																				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/29																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Victor Hugo Gil Ramos AM\>  													
* Descripcion			: <\DM 
                               Se actualiza el procedimiento cambiando el llamado sp que crea el proceso 
							   spPRORegistraLogEventoProceso por spRegistraLogEventoProceso 
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

/* 
Declare @mnsjertrno					 UdtDescripcion,
		@cdgortrno					 varchar(2)
exec bdcna.gsa.spASEjecutarHomologacionPrestacionesPrestador null, null, @mnsjertrno output, @cdgortrno output
--exec bdcna.gsa.spASEjecutarHomologacionPrestacionesPrestador 321, 'prueba1', @mnsjertrno output, @cdgortrno output

Select @cdgortrno, @mnsjertrno


*/

ALTER PROCEDURE [gsa].[spASEjecutarHomologacionPrestacionesPrestador]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null,
/*02*/  @lcUsrioAplcn				 UdtUsuario = null,
/*03*/  @mnsjertrno					 UdtDescripcion = null output,
/*04*/  @cdgortrno					 varchar(2) = null output

AS

Begin
	SET NOCOUNT ON

	Declare @lcUsuario				         UdtUsuario     = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@tpo_prcso				         Int            = 29, -- 'Proceso Automático Homologación Prestaciones Prestador - MEGA'
			@cnsctvo_prcso			         Int            = 0,
			@cnsctvo_cdgo_tpo_incnsstnca     Int            = 1,
			@prcso_extso			         Int            = 2,
			@tpo_rgstro_log_98		         Int            = 98,
			@tpo_rgstro_log_99		         Int            = 99,
			@tpo_rgstro_log_100		         Int            = 100,
			@mensajeError			         Varchar(2000)  = '',
			@cnsctvo_lg				         Int            = 0,
			@cnsctvo_estdo_err	 	         udtConsecutivo = 3,
			@mnsje_lg_evnto_prcso	         Varchar(500)      ,
			@estdo_en_prcso                  Int            = 1,
			@cnsctvo_rgstro_lg_evnto_x_prcso udtConsecutivo


	If @lcUsrioAplcn is not null
	   Begin
	     Set @lcUsuario = @lcUsrioAplcn
	   End 
	
	-- Validamos si existen conceptos de servicios x homologar
	if not exists ( select	1 
				from	gsa.tbASServiciosSolicitados	ss with(nolock)
				Where	ss.cnsctvo_cdgo_prstcn_prstdr = 0)
		Begin
			Set @mnsjertrno = 'No hay Datos Para Procesar Homologación Prestaciones Se Cancela El Proceso'
			Set @cdgortrno = 'OK'
			Return
		End

	-- Creamos tabla temporal para insertar 

	Create table #ServiciosSolicitados
	(
		id_tbla							int identity,
		cnsctvo_slctd_atrzcn_srvco 		UdtConsecutivo,
		cnsctvo_srvco_slctdo			UdtConsecutivo,
		cdgo_intrno						UdtCodigoIps,
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo,
		cnsctvo_cdgo_prstcn_prstdr		UdtConsecutivo default 0,
		hmlgdo							UdtLogico default 'N'
	)

    if @cnsctvo_slctd_atrzcn_srvco is null    -- Si el proceso es masivo se activa control de transacciones
		Begin
			Begin Try
					-- Registramos el inicio proceso automático log de procesos 'Proceso Automático Homologación Prestaciones Prestador - MEGA'
					exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output

					-- Paso 1. Poblar temporales 
					Begin Try
						Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 1
						Set  @mnsje_lg_evnto_prcso = 'Paso 1. Inserción a tablas TMP para Homologación Prestaciones Prestador - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_98, @estdo_en_prcso, @cnsctvo_lg output
						exec gsa.spASPoblarTemporalHomologacionPrestacionesPrestador @cnsctvo_slctd_atrzcn_srvco
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch
					-- Paso 2. Homologar Prestaciones Prestador
					Begin Try
	                    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 2
						set @mnsje_lg_evnto_prcso = 'Paso 2. Homologar Prestaciones Prestador - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_99, @estdo_en_prcso, @cnsctvo_lg output
						exec gsa.spASHomologarPrestacionesPrestador 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch

					-- Paso 3. Grabar Homologar Prestaciones Prestador
					Begin Try
				        Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 3
						set  @mnsje_lg_evnto_prcso = 'Paso 3. Grabar Homologar Prestaciones Prestador - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_100, @estdo_en_prcso, @cnsctvo_lg output
						exec gsa.spASGrabarHomologarPrestacionesPrestador @lcUsrioAplcn
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
						-- Registramos el fin del proceso Automático log de procesos 'Proceso Automático Homologación Prestaciones Prestador - MEGA'
						exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
						set @cdgortrno = 'OK'
						set @mnsjertrno = 'El Proceso Homologación Prestaciones Prestador se realizo con Exito'
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch

			End Try
			Begin Catch
				Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
				Set @cdgortrno = 'ET'
				set @mnsjertrno = @mensajeError
				RAISERROR (@mensajeError, 16, 2) With SETERROR 
			End Catch
		End
	Else
		Begin

			-- Paso 1. Poblar temporales 
			exec gsa.spASPoblarTemporalHomologacionPrestacionesPrestador @cnsctvo_slctd_atrzcn_srvco
			-- Paso 2. Homologar Prestaciones Prestador
			exec gsa.spASHomologarPrestacionesPrestador
			-- Paso 3. Grabar Homologar Prestaciones Prestador
			exec gsa.spASGrabarHomologarPrestacionesPrestador @lcUsrioAplcn
			set @cdgortrno = 'OK'
			set @mnsjertrno = 'El Proceso Homologación Prestaciones Prestador se realizo con Exito'

			if @@error <> 0
			   Begin
					Set @cdgortrno = 'ET'
					set @mnsjertrno = 'Ocurrio un Error en el Proceso Homologación Prestaciones Prestador'
			   End

		End
	Drop table #ServiciosSolicitados

End


GO
