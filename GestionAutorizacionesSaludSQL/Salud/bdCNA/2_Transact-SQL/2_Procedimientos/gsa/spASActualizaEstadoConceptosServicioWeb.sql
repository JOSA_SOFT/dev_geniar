USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizaEstadoConceptosServicioWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASActualizaEstadoConceptosServicioWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite actualizar el estado de los conceptos asociados a una
						   OPS
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           @nmro_unco_ops  					         
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 26/04/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*--------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASActualizaEstadoConceptosServicioWeb] 92575323

ALTER PROCEDURE [gsa].[spASActualizaEstadoConceptosServicioWeb] 
  @nmro_unco_ops  udtConsecutivo 
AS

Begin
   SET NOCOUNT ON

   Declare @lnCdgo_estdo_srvco_slctdo udtConsecutivo

   Set @lnCdgo_estdo_srvco_slctdo = 15 --Entregada

   Update  a
   Set     cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @lnCdgo_estdo_srvco_slctdo
   From    bdCNA.gsa.tbASConceptosServicioSolicitado a WITH (NOLOCK)
   Where   nmro_unco_ops = @nmro_unco_ops
End

GO
