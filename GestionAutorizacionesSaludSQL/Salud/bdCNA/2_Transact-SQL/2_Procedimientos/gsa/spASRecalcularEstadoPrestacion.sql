USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRecalcularEstadoPrestacion]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG         	: spASRecalcularEstadoPrestacion
* Desarrollado por		: <\A Jhon W. Olarte V.									A\>
* Descripcion			: <\D Procedimiento encargado de recalcular el estado	D\>
						  <\D de las prestaciones								D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													\>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 24-05-2016										FC\>
*
*---------------------------------------------------------------------------------  */
ALTER PROCEDURE [gsa].[spASRecalcularEstadoPrestacion] @id_prstcn INT
AS
BEGIN
  SET NOCOUNT ON

  DECLARE @cnsctvo_entrgado INT

  SELECT
    @cnsctvo_entrgado = 15

  DECLARE @tmpmedicamentos TABLE (
    cnsctvo_mdcmnto INT
  );

  DECLARE @tmpprocedimientos TABLE (
    cnsctvo_prcdmnto INT
  );

  --Se consulta todo lo que sea medicamentos
  INSERT INTO @tmpmedicamentos (cnsctvo_mdcmnto)
    SELECT
      TMD.cnsctvo_mdcmnto_slctdo
    FROM bdCNA.gsa.tbASMedicamentosSolicitados TMD		WITH(NOLOCK)
    INNER JOIN bdCNA.gsa.tbASServiciosSolicitados SSO	WITH(NOLOCK)
      ON TMD.cnsctvo_srvco_slctdo = SSO.cnsctvo_srvco_slctdo
    WHERE TMD.cnsctvo_srvco_slctdo = @id_prstcn

  --Se consulta todo lo que sea procedimientos
  INSERT INTO @tmpprocedimientos (cnsctvo_prcdmnto)
    SELECT
      TPI.cnsctvo_prcdmnto_insmo_slctdo
    FROM bdCNA.gsa.tbASProcedimientosInsumosSolicitados TPI		WITH(NOLOCK)
    INNER JOIN bdCNA.gsa.tbASServiciosSolicitados SSO			WITH(NOLOCK)
      ON TPI.cnsctvo_srvco_slctdo = SSO.cnsctvo_srvco_slctdo
    WHERE TPI.cnsctvo_srvco_slctdo = @id_prstcn

  --Se valida que todos los medicamentos o procedimientos estén en estado 15.
  IF NOT EXISTS (SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASConceptosServicioSolicitado CSS				WITH(NOLOCK)
    INNER JOIN @tmpprocedimientos TPR
      ON TPR.cnsctvo_prcdmnto = CSS.cnsctvo_prcdmnto_insmo_slctdo
      AND CSS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo <> @cnsctvo_entrgado
    UNION ALL
    SELECT TOP 1
      1
    FROM bdCNA.gsa.tbASConceptosServicioSolicitado CSS				WITH(NOLOCK)
    INNER JOIN @tmpmedicamentos TMD
      ON TMD.cnsctvo_mdcmnto = CSS.cnsctvo_mdcmnto_slctdo
      AND CSS.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo <> @cnsctvo_entrgado)
  BEGIN
    --Se actualiza en caso tal de que estén en estado 15 todos.
    UPDATE bdCNA.gsa.tbASServiciosSolicitados
    SET cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_entrgado
    WHERE cnsctvo_srvco_slctdo = @id_prstcn
  END
END

GO
