USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASCalcularCuotasDeRecuperacion]    Script Date: 28/08/2017 07:52:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASCalcularCuotasDeRecuperacion												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García																	A\>  
* Descripcion		: <\D 1. Si es Masivo se cálculan Cuotas de Recuperación que cumplan la condición que las prestaciones tenga fecha 
							de entrega el día siguiente a la fecha actual y el estado de los servicios esten en estado '9-Liquidada'
						  2. Si es Por Demanda se toma la solicitud enviada y los servicios asociados esten en estado '9-Liquidada'	D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																									P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/26																						FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez García AM\>  													
* Descripcion			: <\DM 
                               Se implemento funcionalidad para permitir generar copago aunque 
                               no existan parametros de topes 
						  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/09 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se agrega un cruce con la tabla temporal @TempServicioAgrpxItm en el llenado de la tabla
							   #tempTbAsConsolidadoCuotaRecuperacionxOps para sacar el valor real a pagar ya que anteriormente
							   se hacia una suma del campo vlr_lqdcn_cta_rcprcn_srvco de la tabla #TempServicioSolicitudes 
							   dando en ocasiones un valor incorrecto.
						  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/11/28 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se hace cambios de la ultima version, se separa de la tabla temporal 
							   #tempTbAsConsolidadoCuotaRecuperacionxOps, se agrega un update para actualizar el campo
							   vlr_lqdcn_cta_rcprcn_srvco cuando es cuota moderadora y se agrega la tabla temporal 
							   #tempCuotaModeradora
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/05 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se remueve linea "And c.cnsctvo_cdgo_frma_atncn = @cnsctvo_cdgo_frma_atncn1" de calculo de
							   la tecnica de liquidacion, ya que, segun la informacion brindada por el Ing. Alex Lopez, 
							   las formas de atencion no estan parametrizadas y por lo tanto no se debe tener en cuenta. 
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/09 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se agregan condiciones para los saldos en las tablas temporales y se agregan updates para 
							   poner el 0 el valor del saldo para evento y año en caso que en el descuento se pasen del 
							   valor y este quede negativo. 
							   Se agrega el campo nmro_unco_ops a la tabla @TempServicioAgrpxItm para evitar el producto
							   cartesiano al llenar la tabla temporal #tempCuotaModeradora
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/15 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se quita condicional de Select que obtiene el maximo ID de la tabla tbAsDetalleTopesCopago
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/19 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se modifica el calculo de la tecnica de liquidacion neto, se remplazan los updates que se
							   calculaban segun el tipo de cuota de recuperacion para calcular los valores de acuerdo a la
							   tecnica la cual indica si el cobro s por porcentaje, valor fijo, no genera cobro o el numero
							   de veces del costo. 
							   Se modifica el Insert a la tabla temporal #tempCuotaModeradora, se agrega un sum y se separa 
							   por concepto de pago, esto se hace debido que una ops puede tener varios conceptos de pago.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/26 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM 
                               Se modifica el calculo de cuota para copago en los casos donde hay mas de un servicio con los 
							   mismos parametros y valores.
							   Se modifica la actualizacion de los valores en la temporal servicios x ops para que tome los 
							   valores calcula el el agrupador de items.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/28 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               modifica el calculo de cuota, se quita la agrupacion por nuero unico principal y se agrupa
							   por el item de bonificacion, codigo tecnica de liquidacion, consecutivo detalle modelo tecnica 
							   de liquidacion y el numero de actividades, para cuota moderadora, para copago no se tiene en 
							   cuenta el numero de actividades, sino el numero de ops. Se divide el calculo de cuota para 
							   copago y cuota moderadora ya que la forma de agrupacion es diferente.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/06 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se separan las prestaciones con item de bonificacion 2 ya que cada una de las prestaciones 
							   con este item debe ser individual y generar un cobro aparte.
							   Se modifica la agrupacion de prestaciones para el calculo de la cuota.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se modifica el calculo de los topes para que el funcionamiento de los valores parciales.
							   Se modifica de forma que si el tope no cubre el valor total del copago cobre el
							   saldo disponible del tope y se agregue la causal del no cobro total.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/04 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se agrega condicional en validacion de topes para los casos donde el tope es mayor o igual
							   al valor de liquidacion de la prestacion, se valida que el valor disponible del tope sea 
							   mayor a cero
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/07 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se agrega cruce con el consecutivo del servicio al momento de sacar el resultado del 
							   direccionamiento
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/12 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se agrega causal de no cobro para casos donde no se encuentra el convenio comercial.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se retira las consultas para recuperar los saldos de topes por evento ya que estos se
							   manejan por solicitud, es decir, el tope por evento es el maximo que se paga en copagos
							   por solicitud, por lo tanto no requiere ser consultado.
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/19 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\> 													
* Descripcion			: <\DM 
                               Se agrega validacion para agregar causal cuando la tecnica de liquidacion es 4 - Sin Valor
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/12 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------ 															
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez - qvisionclr AM\> 													
* Descripcion			: <\DM 
                               Se agrega validacion para poner la marca de genera cobro en No para las prestaciones 
							   marcadas con causal de no cobro, control de cambio 092 punto 19
						  DM\>  				
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/13 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASCalcularCuotasDeRecuperacion]

AS

Begin
	Set NOCOUNT ON

	Declare		@cnsctvo_cdgo_tpo_srvco4		UdtConsecutivo = 4,  -- Tipo de Servicio '4-CUPS'
				@cnsctvo_cdgo_tpo_srvco5		UdtConsecutivo = 5,  -- Tipo de Servicio '5-CUMS'
				@cnsctvo_cdgo_tpo_srvco9		UdtConsecutivo = 9,  -- Tipo de Servicio '9-CUOS'
				@cnsctvo_cdgo_tpo_mrca_prstdr14 UdtConsecutivo = 14, -- Tipo Marca Prestador '14-Marca Prestador Cobro Cuotas de Recuperación'
				@ValorSI						UdtLogico	   = 'S',
				@ValorNO						UdtLogico	   = 'N',
				@pnSalario						Int = 0,
				@cnsctvo_cdgo_estdo_cta_rcprcn1 Int = 1,				-- Estado Cuotas de Recuperación '1-Precalculado'
				@cnsctvo_cdgo_estdo_tpes4       Int = 4,				-- Estados Topes '4-Cerrado'
				@cnsctvo_cdgo_clse_mdlo8		Int = 8,				-- MODELO TECNICAS DE BONIFICACION
				@cnsctvo_cdgo_clse_mdlo9		Int = 9,				-- Técnicas de Bonificacion productos
				@cnsctvo_cdgo_cncpto_pgo1		Int = 1,				-- Concepto Pago '1-Cuota Moderadora'
				@cnsctvo_cdgo_cncpto_pgo2		Int = 2,				-- Concepto Pago '2-Copagos'
				@cnsctvo_cdgo_tpo_tpe1			Int = 1,				-- Tope por Año
				@cnsctvo_cdgo_tpo_tpe2			Int = 2,				-- Tope por Evento
				@cnsctvo_cdgo_tpo_afldo1		Int = 1,				-- Tipo Afiliado '1-Cotizante'
				@cnsctvo_cdgo_tpo_afldo3		Int = 3,				-- Tipo Afiliado '2-Beneficiario'
				@cnsctvo_cdgo_tpo_afldo4        Int = 4,                -- Tipo Afiliado '4-Grupo Básico'
				@cnsctvo_cdgo_estdo_prdcto2		Int = 2,				-- Estado Productos '2-Aprobado'
				@cnsctvo_cdgo_estdo_prdcto4		Int = 4,				-- Estado Productos '4-Activo'
				@cnsctvo_cdgo_tps_entdd_prstdr2 Int = 2,				-- tecnica parametrizada para cobro en SOS -- Select * From bdafiliacionValidador..tbTiposEntidad
				@cnsctvo_cdgo_frma_atncn1		Int = 1,				-- Tecnica parametrizada para clase de atencion Ambulatorio por defecto.
				@cnsctvo_cdgo_chrte1			Int = 1,				-- Tipo de Cohorte '1-Cancer'
				@edd_afldo_ans5					Int = 5,				-- Edad Afiliado en años
				@edd_afldo_ans18				Int = 18,               -- Edad Afiliado en años
				@edd_afldo_ans65				Int = 65,               -- Edad Afiliado en años
				@cnsctvo_cdgo_tpo_dscpcdd1		Int = 1,				-- Tipo discapacidad '1-Fisica'
				@cnsctvo_cdgo_tpo_dscpcdd2		Int = 2,				-- Tipo discapacidad '2-Mental'
				@cnsctvo_cdgo_tpo_dscpcdd3		Int = 3,				-- Tipo discapacidad '3-Sonora'
				@cnsctvo_cdgo_tpo_mrca_prstcn6  Int = 6,                -- Prestacion Marcada para Rehabilitacion
				@cnsctvo_cdgo_tpo_mrca_prstcn7  Int = 7,                -- Prestacion Marcada Programas de control para regimen subsididado
				@cnsctvo_cdgo_tpo_mrca_prstcn12 Int = 12,				-- Prestacion Marcada para Cancer
				@cnsctvo_cdgo_pln1				Int = 1,				-- tipo plan '1- Pos'
				@cnsctvo_cdgo_pln8				Int = 8,				-- tipo plan '8- Bienestar'
				@cnsctvo_cdgo_pln9				Int = 9,				-- tipo plan '9-Pos Subsidiado'
				@cnsctvo_cdgo_grpo_pblcnl9		Int = 9,				-- Tipo Grupo Poblacional '9-'
				@cnsctvo_cdgo_rngo_slrl5		Int = 5,				-- Tipo Rango Salarial '5 - Nivel I Sisben'
				@cnsctvo_cdgo_itm_bnfccn13      Int = 13,				-- PROMOCION Y PREVENCION
				@cnsctvo_cdgo_itm_bnfccn100     Int = 100,				-- Alto Costo
				@cnsctvo_cdgo_cdd_rsdnca_afldo3543 Int = 3543,			-- Residencia Afiliado Bogotá
				@cnsctvo_cdgo_tpo_cntrto3       Int = 3,                -- Tipo de Contrato POS SUBSIDIADO
				@nmro_unco_aprtnte30049751		Int = 30049751,
				@nmro_unco_aprtnte30052383		Int = 30052383,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn11 Int = 11,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn12 Int = 12,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn13 Int = 13,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn14 Int = 14,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn19 Int,
				@minimo							Int = 0,
				@maximo							Int = 0,
				@ValorCero						Int = 0,
				@ValorUno						Int = 1,
				@ValorDos						Int = 2,
				@mxmo_id_tpe_afldo_anio			Int = 0,
				@fechaactual					Datetime = getdate(),    -- fecha actual
				@fechainicioanio				Datetime, -- fecha inicio año
				@Cnsctvo_cdgo_tcnca_lqdcn1		udtConsecutivo,
				@Cnsctvo_cdgo_tcnca_lqdcn3		udtConsecutivo,
				@Cnsctvo_cdgo_tcnca_lqdcn4		udtConsecutivo,
				@cnsctvo_cdgo_itm_bnfccn2		udtConsecutivo,
				@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn20 udtConsecutivo;

				Set @fechainicioanio = DATEADD(yy,DATEDIFF(yy,0,@fechaactual),0);
				Set @Cnsctvo_cdgo_tcnca_lqdcn1 = 1;
				Set @Cnsctvo_cdgo_tcnca_lqdcn3 = 3;
				Set @Cnsctvo_cdgo_tcnca_lqdcn4 = 4;
				Set @cnsctvo_cdgo_itm_bnfccn2 = 2;
				Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn19 = 19;
				Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn20 = 20;
			
	-- Actualizamos Datos de la temporal de servicios x solicitudes
	-- Actualizamos el prestador Destinatario del resultado del direccionamiento
	Update		a
	Set			cdgo_intrno_prtsdr_dstno = b.cdgo_intrno
	From		#TempServicioSolicitudes a
	Inner Join	gsa.tbASResultadoDireccionamiento b With(NoLock)
	On			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
	And			a.cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo;

	-- Actualizamos si el prestador destino cobra cuotas de recuperación (S/N)
	update		a 
	Set			prtsdr_dstno_rcda_cta_rcprcn = @ValorSI
	From		#TempServicioSolicitudes a
	inner join	bdsisalud.dbo.tbTiposMarcasxSucursal  b With(NoLock)
	On			a.cdgo_intrno_prtsdr_dstno = b.cdgo_intrno 
	Where		@fechaactual between inco_vgnca And fn_vgnca 
	And			cnsctvo_cdgo_tpo_mrca_prstdr = @cnsctvo_cdgo_tpo_mrca_prstdr14;

	-- Actualizamos items bonificación para cups, 
	update		a
	Set			cnsctvo_cdgo_itm_bnfccn = b.cnsctvo_cdgo_itm_bnfccn,
                cnsctvo_cdgo_cncpto_pgo = c.cnsctvo_cdgo_cncpto_pgo,
                cbro_cta_rcprcn_eac = b.cbro_cta_rcprcn_eac
	From		#TempServicioSolicitudes a
	inner join	bdsisalud.dbo.tbCupsServicios b With(NoLock)
	on			a.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_prstcn
	inner join	bdafiliacionvalidador.dbo.tbitemsbonificacion_vigencias c With(NoLock)
	on			b.cnsctvo_cdgo_itm_bnfccn = c.cnsctvo_cdgo_itm_bnfccn            
	inner join	bdsisalud.dbo.tbConceptosPago_Vigencias d With(NoLock)
	on			c.cnsctvo_cdgo_cncpto_pgo = d.cnsctvo_cdgo_cncpto_pgo
	Where		a.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco4 -- Cups
	And			@fechaactual between c.inco_vgnca And c.fn_vgnca
	And			@fechaactual between d.inco_vgnca And d.fn_vgnca;

	-- Actualizamos items bonificación para cums,
	update		a 
	Set			cnsctvo_cdgo_itm_bnfccn = b.cnsctvo_cdgo_itm_bnfccn,
				cnsctvo_cdgo_cncpto_pgo = c.cnsctvo_cdgo_cncpto_pgo,
				cbro_cta_rcprcn_eac = b.cbro_cta_rcprcn_eac
	From		#TempServicioSolicitudes a
	inner join	bdsisalud.dbo.tbCums b  With(NoLock)
	on			a.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cms
	inner join	bdafiliacionvalidador.dbo.tbitemsbonificacion_vigencias c With(NoLock)
	on			b.cnsctvo_cdgo_itm_bnfccn = c.cnsctvo_cdgo_itm_bnfccn            
	inner join	bdsisalud.dbo.tbconceptosPago_vigencias d With(NoLock)
	on			c.cnsctvo_cdgo_cncpto_pgo = d.cnsctvo_cdgo_cncpto_pgo
	Where		a.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco5 -- Cums
	And			@fechaactual between c.inco_vgnca And c.fn_vgnca
	And			@fechaactual between d.inco_vgnca And d.fn_vgnca;

	-- Actualizamos items bonificación para cuos,
	update		a 
	Set			cnsctvo_cdgo_itm_bnfccn = b.cnsctvo_cdgo_itm_bnfccn,
				cnsctvo_cdgo_cncpto_pgo = c.cnsctvo_cdgo_cncpto_pgo
	From		#TempServicioSolicitudes a
	inner join	bdsisalud.dbo.tbPrestacionPis b With(NoLock)
	on			a.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_prstcn_pis
	inner join	bdafiliacionvalidador.dbo.tbitemsbonificacion_vigencias c With(NoLock)
	on			b.cnsctvo_cdgo_itm_bnfccn = c.cnsctvo_cdgo_itm_bnfccn            
	inner join	bdsisalud.dbo.tbconceptosPago_Vigencias d With(NoLock)
	on			c.cnsctvo_cdgo_cncpto_pgo = d.cnsctvo_cdgo_cncpto_pgo
	Where		a.cnsctvo_cdgo_tpo_srvco = @cnsctvo_cdgo_tpo_srvco9 -- Cuos o Pis
	And			@fechaactual between c.inco_vgnca And c.fn_vgnca
	And			@fechaactual between d.inco_vgnca And d.fn_vgnca;
	
	-- Actualizamos los items de bonificacion o valores por tipo de cobro de las prestaciones (PRODUCTOS) 
	-- Para Tipo Plan POS
	Update		ss
	Set			vlr_tcnca_lqdcn = c.vlr_tcnca_lqdcn, 
				nmro_actvdds = c.Nmro_actvdds,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn = c.cnsctvo_dtlle_mdlo_tcncs_bnfccn,
				Cnsctvo_cdgo_tcnca_lqdcn = c.Cnsctvo_cdgo_tcnca_lqdcn
	From		#TempServicioSolicitudes ss 
	Inner join  #TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join	bdafiliacionvalidador.dbo.tbproductos a With(NoLock)
	On			sa.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	inner join	bdafiliacionvalidador.dbo.tbdetproductos b With(NoLock)
	On			a.cnsctvo_prdcto = b.cnsctvo_prdcto 
	inner join	bdafiliacionvalidador.dbo.tbmodelos d With(NoLock)
	on			d.cnsctvo_mdlo = b.cnsctvo_mdlo         
	inner join	bdafiliacionvalidador.dbo.tbdetmodeloTecnicasBonificacion c With(NoLock) 
	On			d.cnsctvo_mdlo = c.cnsctvo_mdlo 
	And			ss.cnsctvo_cdgo_itm_bnfccn = c.cnsctvo_cdgo_itm_bnfccn
	And			sa.cnsctvo_cdgo_rngo_slrl = c.cnsctvo_cdgo_rngo_slrl
	And			sa.cnsctvo_cdgo_tpo_afldo = c.cnsctvo_cdgo_tpo_afldo 
	--And			sa.cnsctvo_cdgo_frma_atncn = c.cnsctvo_cdgo_frma_atncn  /* se deja por defecto 1-ambulatoria -- sisjvg01-2016/09/20 */
	inner join	bdafiliacionvalidador.dbo.tbItemsBonificacion_vigencias f With(NoLock)
	On			c.cnsctvo_cdgo_itm_bnfccn = f.cnsctvo_cdgo_itm_bnfccn
	Where		a.cnsctvo_cdgo_estdo In (@cnsctvo_cdgo_estdo_prdcto2,@cnsctvo_cdgo_estdo_prdcto4)  --aprobado o activo             
	And			b.cnsctvo_cdgo_clse_mdlo = @cnsctvo_cdgo_clse_mdlo8 -- MODELO TECNICAS DE BONIFICACION
	And			a.cnsctvo_prdcto = sa.cnsctvo_cdgo_cnvno_cmrcl  
	And			c.Bnfcro_adcnl = @ValorNO
	And			c.cnsctvo_cdgo_tps_entdd_prstdr = @cnsctvo_cdgo_tps_entdd_prstdr2 -- técnica parametrizada para cobro en SOS -- Select * From bdafiliacionValidador..tbTiposEntidad
	And         sa.cnsctvo_cdgo_tpo_pln = @ValorUNO  /* Tipo Plan '1-POS' */
	And			@fechaactual between a.inco_vgnca_prdcto And a.fn_vgnca_prdcto                
	And			@fechaactual between b.inco_vgnca_asccn And b.fn_vgnca_asccn
	And			@fechaactual between f.inco_vgnca And f.fn_vgnca;

	-- Actualizamos los items de bonificacion o valores por tipo de cobro de las prestaciones (PRODUCTOS) 
	-- Para Tipo Plan PAC
	Update		ss
	Set			vlr_tcnca_lqdcn = c.vlr_tcnca_lqdcn, 
				nmro_actvdds = c.Nmro_actvdds,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn = c.cnsctvo_dtlle_mdlo_tcncs_bnfccn,
				Cnsctvo_cdgo_tcnca_lqdcn = c.Cnsctvo_cdgo_tcnca_lqdcn
	From		#TempServicioSolicitudes ss 
	Inner join  #TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join	bdafiliacionvalidador.dbo.tbproductos a With(NoLock)
	On			sa.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	inner join	bdafiliacionvalidador.dbo.tbdetproductos b With(NoLock)
	On			a.cnsctvo_prdcto = b.cnsctvo_prdcto 
	inner join	bdafiliacionvalidador.dbo.tbmodelos d With(NoLock)
	on			d.cnsctvo_mdlo = b.cnsctvo_mdlo         
	inner join	bdafiliacionvalidador.dbo.tbdetmodeloTecnicasBonificacion c With(NoLock) 
	On			d.cnsctvo_mdlo = c.cnsctvo_mdlo 
	And			ss.cnsctvo_cdgo_itm_bnfccn = c.cnsctvo_cdgo_itm_bnfccn
	And			sa.cnsctvo_cdgo_tpo_afldo = c.cnsctvo_cdgo_tpo_afldo 
	inner join	bdafiliacionvalidador.dbo.tbItemsBonificacion_vigencias f With(NoLock)
	On			c.cnsctvo_cdgo_itm_bnfccn = f.cnsctvo_cdgo_itm_bnfccn
	Where		a.cnsctvo_cdgo_estdo In (@cnsctvo_cdgo_estdo_prdcto2,@cnsctvo_cdgo_estdo_prdcto4)  --aprobado o activo             
	And			b.cnsctvo_cdgo_clse_mdlo = @cnsctvo_cdgo_clse_mdlo8 -- MODELO TECNICAS DE BONIFICACION
	And			a.cnsctvo_prdcto = sa.cnsctvo_cdgo_cnvno_cmrcl  
	And			c.Bnfcro_adcnl = @ValorNO
	And			c.cnsctvo_cdgo_tps_entdd_prstdr = @cnsctvo_cdgo_tps_entdd_prstdr2 -- técnica parametrizada para cobro en SOS -- Select * From bdafiliacionValidador..tbTiposEntidad
	And         sa.cnsctvo_cdgo_tpo_pln = @ValorDOS  /* Tipo Plan '2-PAC' */
	And			@fechaactual between a.inco_vgnca_prdcto And a.fn_vgnca_prdcto                
	And			@fechaactual between b.inco_vgnca_asccn And b.fn_vgnca_asccn
	And			@fechaactual between f.inco_vgnca And f.fn_vgnca
	And			c.cnsctvo_cdgo_frma_atncn = @cnsctvo_cdgo_frma_atncn1;

	
	
	-- Calculo neto según técnica de liquidación de la cuota de recuperacion por servicio

	--Calcula el valor de la cuota de recuperacion de acuerdo al porcentaje obtenido.
	Update  a 
	Set		vlr_lqdcn_cta_rcprcn_srvco = (a.vlr_lqdcn_srvco * a.vlr_tcnca_lqdcn)/100
	From	#TempServicioSolicitudes a 
	Where	a.Cnsctvo_cdgo_tcnca_lqdcn = @Cnsctvo_cdgo_tcnca_lqdcn1;--Tecnica de liquidacion  = 1 Porcentaje

	--Asigna el valor obtenido de la tecnica de liquidacion.
	Update  a 
	Set		vlr_lqdcn_cta_rcprcn_srvco =  a.vlr_tcnca_lqdcn
	From	#TempServicioSolicitudes a 
	Where	a.Cnsctvo_cdgo_tcnca_lqdcn = @Cnsctvo_cdgo_tcnca_lqdcn3; --Tecnica de liquidacion  = 3 valor Fijo	

	--Marca el valor de la cuota de recuperacion en 0 si la tecnica indica que no tiene valor.
	Update  a 
	Set		vlr_lqdcn_cta_rcprcn_srvco = @ValorCero
	From	#TempServicioSolicitudes a 
	Where	a.Cnsctvo_cdgo_tcnca_lqdcn = @Cnsctvo_cdgo_tcnca_lqdcn4; --Tecnica de liquidacion  = 4 Sin valor

	--** Proceso donde Evaluamos los afiliados a los cuales se debe exonerar de cuotas de recuperación **

	Update		ss
	Set			gnra_cbro = @ValorNO
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn Is Not Null
	And			ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn != @ValorCero

	--Update		ss 
	--Set			gnra_cbro = @ValorNO,
	--			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 1 ,
	--			dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado con Cancer menor de 18 años'
	--From		#TempServicioSolicitudes ss
	--Inner Join	#TempSolicitudes sa
	--On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	--Where		sa.cnsctvo_cdgo_chrte =  @cnsctvo_cdgo_chrte1 -- CANCER
	--And			sa.edd_afldo_ans < @edd_afldo_ans18;  -- afiliado menor a 18 años

	--Update		ss
	--Set			gnra_cbro = @ValorNO,
	--			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 2,
	--			dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliados con tipo discapacidad fisica o sensorial Pos Subsidiado'
	--From		#TempServicioSolicitudes ss 
	--Inner Join	#TempSolicitudes sa
	--On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	--inner join	bdsisalud.dbo.tbMarcasPrestacion mp With(NoLock)
	--on			ss.cnsctvo_cdgo_srvco_slctdo = mp.cnsctvo_cdfccn
	--Where		sa.cnsctvo_cdgo_tpo_dscpcdd In (@cnsctvo_cdgo_tpo_dscpcdd1,@cnsctvo_cdgo_tpo_dscpcdd3) --FISICA,SENSORIAL
	--And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9 -- Pos subsidiado
	--And			@fechaactual between mp.inco_vgnca_mrca_prstcn And mp.fn_vgnca_mrca_prstcn
	--And			mp.vldo = @ValorSI;

	Exec bdCNA.gsa.spASValidarCausalesNoCobroCuota

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 3,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliados con tipo discapacidad fisica o sensorial Pos Contibutivo y Pac'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco 
	inner join	bdsisalud.dbo.tbMarcasPrestacion mp With(NoLock)
	On			ss.cnsctvo_cdgo_srvco_slctdo = mp.cnsctvo_cdfccn
	Where		sa.cnsctvo_cdgo_tpo_dscpcdd  In (@cnsctvo_cdgo_tpo_dscpcdd1,@cnsctvo_cdgo_tpo_dscpcdd3) --FISICA,SENSORIAL
	And			sa.cnsctvo_cdgo_pln Between @cnsctvo_cdgo_pln1 And @cnsctvo_cdgo_pln8
	And			mp.cnsctvo_cdgo_tpo_mrca_prstcn = @cnsctvo_cdgo_tpo_mrca_prstcn6 --Prestacion Marcada para Rehabilitacion
	And			@fechaactual between mp.inco_vgnca_mrca_prstcn And mp.fn_vgnca_mrca_prstcn
	And			mp.vldo = @ValorSI;

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 4 ,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Circular 00016 de 2014,  afiliados con tipo discapacidad mental, Pos Subsidiado y Contributivo'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco 
	Where		sa.cnsctvo_cdgo_tpo_dscpcdd In (@cnsctvo_cdgo_tpo_dscpcdd2); -- Mental

	-- Remplazada por nueva regla.
	--Update		ss
	--Set			gnra_cbro = @ValorNO,
	--			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 5,
	--			dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'El servicio con marca de Evento de Alto costo, no cancela cuota de recuperación'
	--From		#TempServicioSolicitudes ss 
	--Where		ss.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn100; -- Alto costo

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 6,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Usuarios Sisben Nivel 1'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join	bdafiliacionvalidador.dbo.tbGruposPoblacionesAfiliados  b With(NoLock)
	on			sa.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2 -- Copago 
	And			sa.cnsctvo_cdgo_rngo_slrl = @cnsctvo_cdgo_rngo_slrl5 -- Nivel I SISBEN
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9 -- Pos subsidiado
	And			@fechaactual between b.inco_vgnca And fn_vgnca;

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 7 ,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Afiliado de Pos Subsidiado menor de 5 años y mayor de 65 años, con residencia en Bogota'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		(sa.edd_afldo_ans < @edd_afldo_ans5 or sa.edd_afldo_ans > @edd_afldo_ans65)
	And			sa.cnsctvo_cdgo_cdd_rsdnca_afldo	= @cnsctvo_cdgo_cdd_rsdnca_afldo3543 --BOGOTA D.C.
	And			sa.cnsctvo_cdgo_tpo_cntrto = @cnsctvo_cdgo_tpo_cntrto3; --POS SS

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 8,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Victimas del conflicto armado interno ley 1448 de 2012'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join	bdafiliacionvalidador.dbo.tbGruposPoblacionesAfiliados b With(NoLock)  
	on			sa.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	Where		b.cnsctvo_cdgo_grpo_pblcnl	= @cnsctvo_cdgo_grpo_pblcnl9
	And			@fechaactual between b.inco_vgnca And b.fn_vgnca
	And			sa.cnsctvo_cdgo_pln between @cnsctvo_cdgo_pln1 And @cnsctvo_cdgo_pln9; -- Pos subsidiado

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 9 ,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Cotizante del Pos Contributivo no cancela Copagos'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2-- Copago 
	--And			sa.cnsctvo_cdgo_pln Between @cnsctvo_cdgo_pln1 And @cnsctvo_cdgo_pln8
	And			sa.cnsctvo_cdgo_pln =  @cnsctvo_cdgo_pln1
	And			sa.cnsctvo_cdgo_tpo_afldo = @cnsctvo_cdgo_tpo_afldo1;
	
	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 10,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'No se Calculo Cuota de Recuperación, Convenio Comercial'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		ss.gnra_cbro = @ValorSI
	And			sa.nmro_unco_aprtnte In (@nmro_unco_aprtnte30049751)-- 890301960 CARVAJAL PULPA Y PAPEL S A
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln8 -- Bienestar
	And			sa.cnsctvo_cdgo_tpo_afldo <> @cnsctvo_cdgo_tpo_afldo4;  -- grupo basico

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 10,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'No se Calculo Cuota de Recuperación, Convenio Comercial'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		ss.gnra_cbro = @ValorSI
	And			sa.nmro_unco_aprtnte In (@nmro_unco_aprtnte30052383)-- COMPANIA DE ELECTRICIDAD DE TULUA S.A.
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln8; -- Bienestar
	
	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 15,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Mayores de 18 años con Cancer Regimen Contributivo'
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join  bdsisalud.dbo.tbMarcasPrestacion b With(NoLock)
	on			ss.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cdfccn
	Where		sa.cnsctvo_cdgo_chrte =  @cnsctvo_cdgo_chrte1 -- CANCER
	And			sa.edd_afldo_ans >= @edd_afldo_ans18
	And			sa.cnsctvo_cdgo_pln Between @cnsctvo_cdgo_pln1 And @cnsctvo_cdgo_pln8
	And			b.cnsctvo_cdgo_tpo_mrca_prstcn = @cnsctvo_cdgo_tpo_mrca_prstcn12 --Prestacion Marcada para Cancer
	And			@fechaactual between b.inco_vgnca_mrca_prstcn And b.fn_vgnca_mrca_prstcn;

	Update		ss
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 16,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Mayores de 18 años con Cancer Regimen Subsidiado'
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join	bdafiliacionvalidador.dbo.tbGruposPoblacionesAfiliados  b With(NoLock)
	on			sa.nmro_unco_idntfccn_afldo = b.nmro_unco_idntfccn_afldo
	Where		sa.cnsctvo_cdgo_chrte =  @cnsctvo_cdgo_chrte1 -- CANCER
	And			sa.edd_afldo_ans >= @edd_afldo_ans18 
	And			sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9
	And			@fechaactual between b.inco_vgnca And b.fn_vgnca;

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 17,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Servicios de Promoción y prevención de la enfermedad'
	From		#TempServicioSolicitudes ss
	Where		ss.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn13; -- PROMOCION Y PREVENCION

	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 18,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Programas de control para regimen subsididado'
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	inner join  bdsisalud.dbo.tbMarcasPrestacion b With(NoLock)
	on			ss.cnsctvo_cdgo_srvco_slctdo = b.cnsctvo_cdfccn
	Where		sa.cnsctvo_cdgo_pln = @cnsctvo_cdgo_pln9 -- Pos subsidiado
	And			b.cnsctvo_cdgo_tpo_mrca_prstcn = @cnsctvo_cdgo_tpo_mrca_prstcn7  --Programas de control para regimen subsididado
	And			@fechaactual between b.inco_vgnca_mrca_prstcn And b.fn_vgnca_mrca_prstcn;
	
	Update		ss 
	Set			gnra_cbro = @ValorSI,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @ValorCero,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Vacio - Revisar el no calculo'
	From		#TempServicioSolicitudes ss
	Where		ss.vlr_lqdcn_cta_rcprcn_srvco = @valorCero 
	And			ss.gnra_cbro = @ValorSI;

	Update		ss
	Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn19,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Cobranza sin producto de sucursal, validar con parametros del modulo productos'
	From		#TempServicioSolicitudes ss 
	Inner join  #TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		sa.cnsctvo_cdgo_cnvno_cmrcl = @valorCero
	And			ss.gnra_cbro = @ValorSI	

	-- Si encuentra tecnica de liquidacion en 4 - Sin Valor
	Update		ss 
	Set			gnra_cbro = @ValorNO,
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn20,
				dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'De acuerdo con modulo de productos no se genera cobro para el afiliado'
	From		#TempServicioSolicitudes ss
	Where		ss.vlr_tcnca_lqdcn = @valorCero 
	And			ss.gnra_cbro = @ValorSI
	And			ss.cnsctvo_cdgo_tcnca_lqdcn = @Cnsctvo_cdgo_tcnca_lqdcn4;

	-- Iniciamos Proceso Validar Topes para concepto '2-copagos'

	-- Recuperamos el salario mínimo legal vigente
	Select	@pnSalario = vlr_slro_mnmo 
	From	bdAfiliacionVALIDADOR.dbo.tbSalariosMinimos_Vigencias With(NoLock)               
	Where	@fechaactual between inco_vgnca And fn_vgnca        
	And		cnsctvo_cdgo_slro_mnmo not In(0,99999)
	And		vsble_usro = @ValorSI;

	--** Proceso donde consultamos los topes de ley para Evento y Año de los copagos  (PRODUCTOS)
	declare @TopesParametros TABLE
	(  
		cnsctvo_prdcto				Int,
		cnsctvo_cdgo_tpo_tpe		Int, --para copagos el tipo tope  (1-por Año, 2-Por Evento)
		cnsctvo_cdgo_rngo_slrl		Int, -- rango salarial  (1-A,2-B,3-C)
		prcntje_slro_mnmo			Float,
		vlr_tope					Float,
		cnsctvo_dtlle_mdlo_tps_cpgo Int,
		cnsctvo_mdlo				Int
	);

	Insert Into @TopesParametros 
	(  
				cnsctvo_prdcto,					cnsctvo_cdgo_tpo_tpe,							cnsctvo_cdgo_rngo_slrl, 
				prcntje_slro_mnmo,				vlr_tope,										cnsctvo_dtlle_mdlo_tps_cpgo,
				cnsctvo_mdlo
	)
	Select Distinct
				a.cnsctvo_prdcto,				e.cnsctvo_cdgo_tpo_tpe,							e.cnsctvo_cdgo_rngo_slrl, 
				e.prcntje_slro_mnmo,			(ROUND(e.prcntje_slro_mnmo,4) * @pnSalario ),	e.cnsctvo_dtlle_mdlo_tps_cpgo,
				e.cnsctvo_mdlo
	From		bdafiliacionvalidador.dbo.tbproductos a With(NoLock)
	inner join	bdafiliacionvalidador.dbo.tbdetproductos c  With(NoLock) 
	on			a.cnsctvo_prdcto = c.cnsctvo_prdcto             
	inner join	bdafiliacionvalidador.dbo.tbmodelos d With(NoLock)   
	on			d.cnsctvo_mdlo = c.cnsctvo_mdlo  
	And			c.cnsctvo_cdgo_clse_mdlo=d.cnsctvo_cdgo_clse_mdlo   
	And			a.cnsctvo_cdgo_pln=d.cnsctvo_cdgo_pln            
	inner join	bdafiliacionvalidador.dbo.tbdetmodelotopesCopago e  With(NoLock) 
	on			d.cnsctvo_mdlo = e.cnsctvo_mdlo
	inner join  #TempSolicitudes f
	On			f.cnsctvo_cdgo_cnvno_cmrcl = a.cnsctvo_prdcto      -- convenio asociado al producto        
	Where		@fechaactual between c.inco_vgnca_asccn And c.fn_vgnca_asccn  
	And			@fechaactual between a.inco_vgnca_prdcto And a.fn_vgnca_prdcto               
	And			c.cnsctvo_cdgo_clse_mdlo = @cnsctvo_cdgo_clse_mdlo9 -- Tecnicas de Bonificacion productos
	And			d.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo_tpes4 -- Cerrado
	And			e.cnsctvo_cdgo_tpo_tpe In (@cnsctvo_cdgo_tpo_tpe1,@cnsctvo_cdgo_tpo_tpe2);

	-- Actualizamos los topes de ley x año en los servicios de las solicitudes 
	
	update		a 
	Set			vlr_tpe_año_prmtro = c.vlr_tope,
				cnsctvo_dtlle_mdlo_tps_cpgo = c.cnsctvo_dtlle_mdlo_tps_cpgo,
				cnsctvo_mdlo = c.cnsctvo_mdlo
	From		#TempServicioSolicitudes a 
	inner join  #TempSolicitudes b
	on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco 
	inner join	@TopesParametros c 
	on			b.cnsctvo_cdgo_rngo_slrl = c.cnsctvo_cdgo_rngo_slrl 
	Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2 -- Solo aplica topes a servicios que realicen copagos
	And			b.cnsctvo_cdgo_tpo_afldo = @cnsctvo_cdgo_tpo_afldo3   -- Beneficiario
	And			c.cnsctvo_cdgo_tpo_tpe = @cnsctvo_cdgo_tpo_tpe1; -- Tope Año

	-- Actualizamos los topes de ley x evento en los servicios de las solicitudes 
	update		a 
	Set			vlr_tpe_evnto_prmtro = c.vlr_tope,
				cnsctvo_dtlle_mdlo_tps_cpgo = c.cnsctvo_dtlle_mdlo_tps_cpgo,
				cnsctvo_mdlo = c.cnsctvo_mdlo
	From		#TempServicioSolicitudes a 
	inner join  #TempSolicitudes b
	on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco 
	inner join	@TopesParametros c 
	on			b.cnsctvo_cdgo_rngo_slrl = c.cnsctvo_cdgo_rngo_slrl 
	Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2 -- Solo aplica topes a servicios que realicen copagos
	And			b.cnsctvo_cdgo_tpo_afldo = @cnsctvo_cdgo_tpo_afldo3  -- Beneficiario
	And			c.cnsctvo_cdgo_tpo_tpe = @cnsctvo_cdgo_tpo_tpe2; -- Tope Evento

	-- Recuperamos los saldos disponibles de topes de ley  x evento y año 
	Insert Into #tempTbAsDetalleTopesCopago
	(
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_idntfccn_afldo,				sldo_antrr_dspnble_evnto,
				sldo_antrr_dspnble_ano,					sldo_dspnble_evnto,						sldo_dspnble_ano,
				vlr_lqdcn_cta_rcprcn_ttl,				cnsctvo_dtlle_mdlo_tps_cpgo,			cnsctvo_mdlo,
				cnsctvo_cdgo_estdo_cta_rcprcn
	)
	Select
				b.cnsctvo_slctd_atrzcn_srvco,			b.nmro_unco_idntfccn_afldo,				a.vlr_tpe_evnto_prmtro,
				a.vlr_tpe_año_prmtro,					a.vlr_tpe_evnto_prmtro,					a.vlr_tpe_año_prmtro,
				@ValorCero,								a.cnsctvo_dtlle_mdlo_tps_cpgo,			a.cnsctvo_mdlo,
				@ValorUno
	From		#TempServicioSolicitudes a
	inner join  #TempSolicitudes b
	on			a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco 
	Where		a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2 -- Solo aplica topes a servicios que realicen copagos
	And			a.gnra_cbro = @ValorSI
	Group By    b.cnsctvo_slctd_atrzcn_srvco,			b.nmro_unco_idntfccn_afldo ,			a.vlr_tpe_evnto_prmtro,
				a.vlr_tpe_año_prmtro,					a.vlr_tpe_evnto_prmtro,					a.vlr_tpe_año_prmtro,
				a.cnsctvo_dtlle_mdlo_tps_cpgo,			a.cnsctvo_mdlo;		
				
	-- Iniciamos Ciclo para Actualizar los saldos disponibles del acumulado de topes x año afiliado
	Select @minimo = min(id_tbla) From #tempTbAsDetalleTopesCopago
	Select @maximo = max(id_tbla) From #tempTbAsDetalleTopesCopago

	Do while (@minimo <= @maximo And @minimo > @ValorCero )
		Begin
			
			-- obtenemos los saldos anteriores por año del afiliado
			Select		@mxmo_id_tpe_afldo_anio = max(dtc.cnsctvo_rgstro_dtlle_tpe_cpgo)
			From		#tempTbAsDetalleTopesCopago t	
			Inner Join	gsa.TbAsDetalleTopesCopago dtc With(NoLock)
			On			dtc.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo
			Inner Join  gsa.tbAsCalculoConsolidadoCuotaRecuperacion cccr With(NoLock)
			On			cccr.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn = dtc.cnsctvo_rgstro_clclo_cnslddo_cta_rcprcn
			Where		t.id_tbla = @minimo
			And			dtc.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn1  -- Estado '1-Precalculado'
			And         cccr.fcha_entrga between @fechainicioanio And @fechaactual;
			
			-- Actualizamos los saldos disponibles del acumulado de topes x año del afiliado
			Update		t
			Set			sldo_antrr_dspnble_ano = dtc.sldo_dspnble_ano,
						sldo_dspnble_ano = dtc.sldo_dspnble_ano,
						cnsctvo_rgstro_dtlle_tpe_cpgo_orgn = dtc.cnsctvo_rgstro_dtlle_tpe_cpgo
			From		#tempTbAsDetalleTopesCopago t	
			Inner Join	gsa.TbAsDetalleTopesCopago dtc With(NoLock)
			On			dtc.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo
			Where		t.id_tbla = @minimo
			And			dtc.cnsctvo_rgstro_dtlle_tpe_cpgo = @mxmo_id_tpe_afldo_anio
			And			dtc.cnsctvo_cdgo_estdo_cta_rcprcn = @cnsctvo_cdgo_estdo_cta_rcprcn1  -- Estado '1-Precalculado'
			And			dtc.sldo_dspnble_ano >= @ValorCERO; --mientras los valores sean mayor o igual a 0 actualiza saldos en temporales qvisionclr 15/12/2016

			Set @minimo += 1;
		End
		
	-- Iniciamos Ciclo para aplicar los topes y descontar a cuotas de recuperacion por concepto copago
	Set @minimo = 0;
	Set @maximo = 0;
	 
	Select	@minimo = min(id_tbla) From #TempServicioSolicitudes
	Select	@maximo = max(id_tbla) From	#TempServicioSolicitudes
	
	Do while (@minimo <= @maximo And @minimo > @ValorCero )
		Begin
			if exists  (Select	1
						From	#TempServicioSolicitudes 
						Where	id_tbla = @minimo
						And		cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2  -- 2-copagos
						And		gnra_cbro = @ValorSI ) 
				Begin

					If(Exists (
								Select		1
								From		#TempServicioSolicitudes t
								Inner Join  #tempTbAsDetalleTopesCopago t1
								On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
								Inner Join  #TempSolicitudes t2
								On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
								Where		t.id_tbla = @minimo
								And			cnsctvo_cdgo_pln != @cnsctvo_cdgo_pln9
							)
					)
					Begin
						
							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = @ValorCero, 
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn12,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Año Pos Contributivo',
										gnra_cbro = @ValorNO
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano = @ValorCero	
						
							Update		t
							Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = Null,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = Null,
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano >= t.vlr_lqdcn_cta_rcprcn_srvco	
							And			t1.sldo_dspnble_ano > @ValorCero		

							-- Si supera el tope parcialmente pone el valor disponible y la causal de cobro de todo el valor
							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = t1.sldo_dspnble_ano,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn12,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Año Pos Contributivo',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano < t.vlr_lqdcn_cta_rcprcn_srvco
												

							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = 0,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn11,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Evento Pos Contributivo',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto = 0

							Update		t
							Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = Null,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = Null,
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto >= t.vlr_lqdcn_cta_rcprcn_srvco
							And			t1.sldo_dspnble_evnto > @ValorCero

							-- Si supera el tope parcialmente pone el valor disponible y la causal de cobro de todo el valor
							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = t1.sldo_dspnble_evnto,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn11,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Evento Pos Contributivo',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto < t.vlr_lqdcn_cta_rcprcn_srvco					

					End
					Else
					Begin

							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = 0, 
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn14,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Año Pos Subsidiado N 2 Y 3',
										gnra_cbro = @ValorNO
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano = 0

							Update		t
							Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = Null,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = Null,
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano >= t.vlr_lqdcn_cta_rcprcn_srvco
							And			t1.sldo_dspnble_ano > @ValorCero

							-- Si supera el tope parcialmente pone el valor disponible y la causal de cobro de todo el valor
							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = t1.sldo_dspnble_ano,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn14,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Año Pos Subsidiado N 2 Y 3',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_ano < t.vlr_lqdcn_cta_rcprcn_srvco						

							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = 0,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn13,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'causal Supero el Tope Evento Pos Subsidiado N 2 Y 3',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto = 0

							Update		t
							Set			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = Null,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = Null,
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto >= t.vlr_lqdcn_cta_rcprcn_srvco
							And			t1.sldo_dspnble_evnto > @ValorCero

							-- Si supera el tope parcialmente pone el valor disponible y la causal de cobro de todo el valor
							Update		t
							Set			vlr_lqdcn_cta_rcprcn_srvco = t1.sldo_dspnble_evnto,
										cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn13,
										dscrpcn_csa_csa_no_cbro_cta_rcprcn = 'Causal Supero el Tope Evento Pos Subsidiado N 2 Y 3',
										gnra_cbro = @ValorSI
							From		#TempServicioSolicitudes t
							Inner Join  #tempTbAsDetalleTopesCopago t1
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Inner Join  #TempSolicitudes t2
							On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
							Where		t.id_tbla = @minimo
							And         t.cnsctvo_mdlo is not null
							And			t.gnra_cbro = @ValorSI
							And			t1.sldo_dspnble_evnto < t.vlr_lqdcn_cta_rcprcn_srvco					

					End

					-- Actualizamos saldos disponibles para el año por el afiliado y para el evento por solicitud

					Update		t1
					Set			t1.sldo_dspnble_evnto = t1.sldo_dspnble_evnto - t.vlr_lqdcn_cta_rcprcn_srvco,
								t1.sldo_dspnble_ano = t1.sldo_dspnble_ano - t.vlr_lqdcn_cta_rcprcn_srvco,
								t1.vlr_lqdcn_cta_rcprcn_fnl = t1.vlr_lqdcn_cta_rcprcn_fnl + t.vlr_lqdcn_cta_rcprcn_srvco,
								t1.gnra_cbro = @ValorSI
					From		#TempServicioSolicitudes t
					Inner Join  #tempTbAsDetalleTopesCopago t1
					On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
					Where		t.id_tbla = @minimo
					And			t.gnra_cbro = @ValorSI
					And			t1.sldo_dspnble_evnto > @ValorCERO;	--Debe descontar mientras el saldo sea mayor a 0 qvisionclr 15/12/2016

					--Si se pasa del tope pone en 0 el saldo del evento qvisionclr 15/12/2016	
					Update		t1
					Set			t1.sldo_dspnble_evnto = @ValorCERO
					From		#TempServicioSolicitudes t
					Inner Join  #tempTbAsDetalleTopesCopago t1
					On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
					Where		t.id_tbla = @minimo
					And			t.gnra_cbro = @ValorSI
					And			t1.sldo_dspnble_evnto < @ValorCERO;	
					
					--Si se pasa del tope pone en 0 el saldo del año qvisionclr 15/12/2016	
					Update		t1
					Set			t1.sldo_dspnble_ano = @ValorCERO
					From		#TempServicioSolicitudes t
					Inner Join  #tempTbAsDetalleTopesCopago t1
					On			t1.cnsctvo_slctd_atrzcn_srvco = t.cnsctvo_slctd_atrzcn_srvco
					Where		t.id_tbla = @minimo
					And			t.gnra_cbro = @ValorSI
					And			t1.sldo_dspnble_ano < @ValorCERO;

			End
			Set @minimo += 1;

		End		

		
	-- Insertamos los detalles en la temporal del cálculo de cuotas de recuperación resultante
	Insert Into #tempTbAsDetCalculoCuotaRecuperacion 
	(
				cnsctvo_prcso, 							cnsctvo_slctd_atrzcn_srvco,				cnsctvo_srvco_slctdo, 	
				cnsctvo_dtlle_mdlo_tcncs_bnfccn, 		cnsctvo_mdlo, 							cntdd_slctda,
				cnsctvo_cdgo_cncpto_pgo,				cdgo_intrno_prtsdr_cbro,				vlr_tcnca_lqdcn,
				vlr_tcnca_lqdcn_neto,					vlr_lqdcn_cta_rcprcn_srvco, 			gnra_cbro,	
				cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,	cnsctvo_cdgo_estdo_cta_rcprcn, 			nmro_unco_idntfccn_afldo,
				cnsctvo_cdgo_pln,						nmro_unco_ops,							vlr_lqdcn_srvco
	)
	Select Distinct
				ss.cnsctvo_prcso,						sa.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_srvco_slctdo,	
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		sa.cnsctvo_cdgo_cnvno_cmrcl, 			ss.cntdd_slctda,
				ss.cnsctvo_cdgo_cncpto_pgo,				ss.cdgo_intrno_prtsdr_dstno,			ss.vlr_tcnca_lqdcn,
				ss.vlr_lqdcn_cta_rcprcn_srvco,			ss.vlr_lqdcn_cta_rcprcn_srvco,			ss.gnra_cbro,
				ss.cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,	@ValorUno,								sa.nmro_unco_idntfccn_afldo,			
				sa.cnsctvo_cdgo_pln,					ss.nmro_unco_ops,						ss.vlr_lqdcn_srvco
	From		#TempServicioSolicitudes ss
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	--Where		ss.cnsctvo_cdgo_cncpto_pgo In(@cnsctvo_cdgo_cncpto_pgo1,@cnsctvo_cdgo_cncpto_pgo2);

	/*
		Iniciamos Proceso donde se agrupa los servicios que cancelan cuota de recuperación por afiliado, plan, tipo cuota de recuperacion(Copago-Cuota moderadora), 
		Agrupador tipo cuota recuperación (Item Bonificacion)
		Se realiza agrupacion de cuotas de recuperacion seleccionando los registros a calcular por fecha estimada de programación e item de bonificacion
		este bloque permite determinar el número de veces que debo cobrar una cuota moderadora por cada n servicios dependiendo la técnica de bonificación, 
		para si proceder a agrupar por concepto de pago.
	*/
	Declare @TempServicioAgrpxItm Table
	(
		id_tbla								Int identity,
		cnsctvo_tpo_prcso					UdtConsecutivo,
		cnsctvo_prcso						UdtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco			UdtConsecutivo,
		nmro_unco_idntfccn_afldo			UdtConsecutivo,
		cnsctvo_cdgo_pln					UdtConsecutivo,
		cnsctvo_cdgo_itm_bnfccn				UdtConsecutivo,
		prtsdr_dstno_rcda_cta_rcprcn		UdtLogico,
		nmro_actvdds						Int,	
		vlr_lqdcn_cta_rcprcn_srvco			Float,
		cnsctvo_cdgo_cncpto_pgo				UdtConsecutivo,
		Cnt_Srvco_itm						Int,
		Mult_No_Ctas						Int,
		vlr_lqdcn_cta_rcprcn_srvco_fnl		Float,
		fcha_entrga							datetime,
		Cnsctvo_cdgo_tcnca_lqdcn			UdtConsecutivo,
		cnsctvo_dtlle_mdlo_tcncs_bnfccn		UdtConsecutivo,
		nmro_unco_ops						UdtConsecutivo
	);

	-- Se agrupan las prestaciones con cuota moderadora, sin tener en cuenta el numero de ops
	Insert Into @TempServicioAgrpxItm
	(
				cnsctvo_slctd_atrzcn_srvco,					nmro_unco_idntfccn_afldo,					cnsctvo_cdgo_pln,
				cnsctvo_cdgo_itm_bnfccn,					prtsdr_dstno_rcda_cta_rcprcn,				nmro_actvdds,								
				vlr_lqdcn_cta_rcprcn_srvco,					cnsctvo_cdgo_cncpto_pgo,					Cnt_Srvco_itm,	
				Mult_No_Ctas,								vlr_lqdcn_cta_rcprcn_srvco_fnl,				cnsctvo_tpo_prcso,			
				cnsctvo_prcso,								fcha_entrga,								Cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn
	)
	Select 
				sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo, 				sa.cnsctvo_cdgo_pln,	
				ss.cnsctvo_cdgo_itm_bnfccn,					ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,							
				ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.cnsctvo_cdgo_cncpto_pgo,					count(1) as Cnt_Srvco_itm,					
				@ValorCero,									@ValorCero,									ss.cnsctvo_tpo_prcso,	
				ss.cnsctvo_prcso,							sa.fcha_entrga,								ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		gnra_cbro = @ValorSI
	And			vlr_lqdcn_cta_rcprcn_srvco > @ValorCero 
	And			ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			cnsctvo_cdgo_itm_bnfccn != @cnsctvo_cdgo_itm_bnfccn2
	Group By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				sa.cnsctvo_cdgo_pln,
				ss.cnsctvo_cdgo_itm_bnfccn, 				ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,
				ss.cnsctvo_cdgo_cncpto_pgo,					ss.cnsctvo_tpo_prcso,						ss.cnsctvo_prcso,							
				sa.fcha_entrga,								ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,			ss.cnsctvo_cdgo_cncpto_pgo
	Order By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				ss.cnsctvo_cdgo_cncpto_pgo;

	-- Se agrupan las prestaciones con item de bonificacion 2 ya que para estas se deben poner cada una aparte.
	Insert Into @TempServicioAgrpxItm
	(
				cnsctvo_slctd_atrzcn_srvco,					nmro_unco_idntfccn_afldo,					cnsctvo_cdgo_pln,
				cnsctvo_cdgo_itm_bnfccn,					prtsdr_dstno_rcda_cta_rcprcn,				nmro_actvdds,								
				vlr_lqdcn_cta_rcprcn_srvco,					cnsctvo_cdgo_cncpto_pgo,					Cnt_Srvco_itm,	
				Mult_No_Ctas,								vlr_lqdcn_cta_rcprcn_srvco_fnl,				cnsctvo_tpo_prcso,			
				cnsctvo_prcso,								fcha_entrga,								Cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,			nmro_unco_ops
	)
	Select 
				sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo, 				sa.cnsctvo_cdgo_pln,	
				ss.cnsctvo_cdgo_itm_bnfccn,					ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,							
				ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.cnsctvo_cdgo_cncpto_pgo,					count(1) as Cnt_Srvco_itm,					
				@ValorCero,									@ValorCero,									ss.cnsctvo_tpo_prcso,	
				ss.cnsctvo_prcso,							sa.fcha_entrga,								ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,			ss.nmro_unco_ops
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		gnra_cbro = @ValorSI
	And			vlr_lqdcn_cta_rcprcn_srvco > @ValorCero 
	And			ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			ss.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn2
	Group By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				sa.cnsctvo_cdgo_pln,
				ss.cnsctvo_cdgo_itm_bnfccn, 				ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,
				ss.cnsctvo_cdgo_cncpto_pgo,					ss.cnsctvo_tpo_prcso,						ss.cnsctvo_prcso,							
				sa.fcha_entrga,								ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,			ss.cnsctvo_cdgo_cncpto_pgo,					ss.nmro_unco_ops
	Order By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				ss.cnsctvo_cdgo_cncpto_pgo

	--Se agrupan las prestaciones con copago, para estas se tiene en cuenta el numero unico de ops
	Insert Into @TempServicioAgrpxItm
	(
				cnsctvo_slctd_atrzcn_srvco,					nmro_unco_idntfccn_afldo,					cnsctvo_cdgo_pln,
				cnsctvo_cdgo_itm_bnfccn,					prtsdr_dstno_rcda_cta_rcprcn,				nmro_actvdds,								
				vlr_lqdcn_cta_rcprcn_srvco,					cnsctvo_cdgo_cncpto_pgo,					Cnt_Srvco_itm,	
				Mult_No_Ctas,								vlr_lqdcn_cta_rcprcn_srvco_fnl,				cnsctvo_tpo_prcso,			
				cnsctvo_prcso,								fcha_entrga,								Cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,			nmro_unco_ops
	)
	Select 
				sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo, 				sa.cnsctvo_cdgo_pln,	
				ss.cnsctvo_cdgo_itm_bnfccn,					ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,							
				ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.cnsctvo_cdgo_cncpto_pgo,					count(1) as Cnt_Srvco_itm,					
				@ValorCero,									@ValorCero,									ss.cnsctvo_tpo_prcso,	
				ss.cnsctvo_prcso,							sa.fcha_entrga,								ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,			ss.nmro_unco_ops
	From		#TempServicioSolicitudes ss 
	Inner Join	#TempSolicitudes sa
	On			sa.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Where		gnra_cbro = @ValorSI
	And			vlr_lqdcn_cta_rcprcn_srvco > @ValorCero 
	And			ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
	Group By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				sa.cnsctvo_cdgo_pln,
				ss.cnsctvo_cdgo_itm_bnfccn, 				ss.prtsdr_dstno_rcda_cta_rcprcn,			ss.nmro_actvdds,
				ss.cnsctvo_cdgo_cncpto_pgo,					ss.cnsctvo_tpo_prcso,						ss.cnsctvo_prcso,							
				sa.fcha_entrga,								ss.vlr_lqdcn_cta_rcprcn_srvco,				ss.Cnsctvo_cdgo_tcnca_lqdcn,
				ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn,			ss.cnsctvo_cdgo_cncpto_pgo,					ss.nmro_unco_ops
	Order By	sa.cnsctvo_slctd_atrzcn_srvco,				sa.nmro_unco_idntfccn_afldo,				ss.cnsctvo_cdgo_cncpto_pgo;
	
	-- ** Iniciamos bloque donde determinamos el número de veces que debe cobrarse la cuota de recuperacion **-
	Update	t 
	Set		Mult_No_Ctas = (Cnt_Srvco_itm/nmro_actvdds) + @ValorUno
	From	@TempServicioAgrpxItm t
	Where	nmro_actvdds > @ValorUno

	-- Mult_No_Ctas 
	Update	t 
	Set		Mult_No_Ctas = nmro_actvdds
	From	@TempServicioAgrpxItm t
	Where	nmro_actvdds = @ValorUno -- mayores a 1 las cuales aplican para cuota moderadora, en copagos este valor siempre es igual a 1

	-- Multiplica el numero de veces a cobrar por el valor de la cuota de recuperación 
	update	t
	Set		vlr_lqdcn_cta_rcprcn_srvco_fnl = (Mult_No_Ctas * vlr_lqdcn_cta_rcprcn_srvco)
	From	@TempServicioAgrpxItm t

	--Multiplica la cantidad de conceptos por el valor para copago, cuando hay mas de un servicio con los mismos parametros y valor
	update	t
	Set		vlr_lqdcn_cta_rcprcn_srvco_fnl = (Cnt_Srvco_itm * vlr_lqdcn_cta_rcprcn_srvco)
	From	@TempServicioAgrpxItm t
	Where	cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
	And		Cnt_Srvco_itm > @ValorUno

	-- Insertamos  el consolidado resultante del cálculo cuotas de recuperación
	Insert Into #tempTbAsCalculoConsolidadoCuotaRecuperacion 
	(
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_idntfccn_afldo,				cnsctvo_cdgo_pln,
				cnsctvo_cdgo_cncpto_pgo,				vlr_lqdcn_cta_rcprcn_ttl,				vlr_lqdcn_cta_rcprcn_ttl_fnl,
				cnsctvo_cdgo_estdo_cta_rcprcn,			cnsctvo_tpo_prcso,						cnsctvo_prcso,
				fcha_entrga						
	)
	Select  
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_idntfccn_afldo,				cnsctvo_cdgo_pln,
				cnsctvo_cdgo_cncpto_pgo,				sum (vlr_lqdcn_cta_rcprcn_srvco),		sum(vlr_lqdcn_cta_rcprcn_srvco_fnl),
				@ValorUno,								cnsctvo_tpo_prcso,						cnsctvo_prcso,	
				fcha_entrga
	From		@TempServicioAgrpxItm
	Group By	cnsctvo_slctd_atrzcn_srvco, nmro_unco_idntfccn_afldo,	cnsctvo_cdgo_pln, 
				cnsctvo_cdgo_cncpto_pgo,	cnsctvo_tpo_prcso,			cnsctvo_prcso,		
				fcha_entrga,				nmro_unco_ops;

	-- Se calcula el valor de las cuotas de recuperacion, copago y cuota moderadora
	Declare @TempCuota Table
	(
		id_tbla								Int identity,
		cnsctvo_slctd_atrzcn_srvco			UdtConsecutivo,
		cnsctvo_cdgo_pln					UdtConsecutivo,
		cnsctvo_cdgo_itm_bnfccn				UdtConsecutivo,
		prtsdr_dstno_rcda_cta_rcprcn		UdtLogico,
		cnsctvo_cdgo_cncpto_pgo				UdtConsecutivo,
		vlr_lqdcn_cta_rcprcn_srvco_fnl		Float,
		cnsctvo_cdgo_tcnca_lqdcn			UdtConsecutivo,
		cnsctvo_dtlle_mdlo_tcncs_bnfccn		UdtConsecutivo,
		nmro_actvdds						Int,
		nmro_unco_ops						UdtConsecutivo
	);

	-- Se calcula la cuota para cuota moderadora, exepto para las prestaciones con item de bonificacion 2
	Insert Into @TempCuota
	(
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_srvco_fnl,			cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							cnsctvo_slctd_atrzcn_srvco
	)
	Select		cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			Sum(vlr_lqdcn_cta_rcprcn_srvco_fnl),	cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							cnsctvo_slctd_atrzcn_srvco
	From		@TempServicioAgrpxItm
	Where		cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			cnsctvo_cdgo_itm_bnfccn != @cnsctvo_cdgo_itm_bnfccn2
	Group By	cnsctvo_cdgo_cncpto_pgo,			cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_tcnca_lqdcn,				cnsctvo_dtlle_mdlo_tcncs_bnfccn,
				nmro_actvdds,						cnsctvo_slctd_atrzcn_srvco;

	-- Se calcula la cuota para las prestaciones con item de bonificacion 2, para cada prestacion se debe realiazr un cobro aparte
	Insert Into @TempCuota
	(
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_srvco_fnl,			cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							nmro_unco_ops,
				cnsctvo_slctd_atrzcn_srvco
	)
	Select		cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			Sum(vlr_lqdcn_cta_rcprcn_srvco_fnl),	cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							nmro_unco_ops,
				cnsctvo_slctd_atrzcn_srvco
	From		@TempServicioAgrpxItm
	Where		cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn2
	Group By	cnsctvo_cdgo_cncpto_pgo,			cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_tcnca_lqdcn,				cnsctvo_dtlle_mdlo_tcncs_bnfccn,
				nmro_actvdds,						nmro_unco_ops,							cnsctvo_slctd_atrzcn_srvco;

	-- Se calcula las cuotas para copagos
	Insert Into @TempCuota
	(
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			vlr_lqdcn_cta_rcprcn_srvco_fnl,			cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							nmro_unco_ops,
				cnsctvo_slctd_atrzcn_srvco
	)
	Select		cnsctvo_cdgo_pln,					cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_cncpto_pgo,			Sum(vlr_lqdcn_cta_rcprcn_srvco_fnl),	cnsctvo_cdgo_tcnca_lqdcn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,	nmro_actvdds,							nmro_unco_ops,
				cnsctvo_slctd_atrzcn_srvco
	From		@TempServicioAgrpxItm
	Where		cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
	Group By	cnsctvo_cdgo_cncpto_pgo,			cnsctvo_cdgo_itm_bnfccn,				prtsdr_dstno_rcda_cta_rcprcn,
				cnsctvo_cdgo_pln,					cnsctvo_cdgo_tcnca_lqdcn,				cnsctvo_dtlle_mdlo_tcncs_bnfccn,
				nmro_actvdds,						nmro_unco_ops,							cnsctvo_slctd_atrzcn_srvco;
	
	-- Se inserta la informacion del consolidado por los agrupadores que generan cobro Cnsctvo_cdgo_tcnca_lqdcn,
	-- cnsctvo_dtlle_mdlo_tcncs_bnfccn y cnsctvo_cdgo_cncpto_pgo, sin agregar numero de ops o valor para las cuotas moderadoras
	Insert Into #tempTbAsConsolidadoCuotaRecuperacionxOps
	(
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,							cnsctvo_cdgo_cncpto_pgo,
				cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,			prtsdr_dstno_rcda_cta_rcprcn,
				nmro_unco_prncpl_cta_rcprcn,			cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,	cnsctvo_cdgo_itm_bnfccn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,		cnsctvo_cdgo_tcnca_lqdcn,				nmro_actvdds
	)
	Select Distinct
				a.cnsctvo_slctd_atrzcn_srvco,			@ValorCero,										a.cnsctvo_cdgo_cncpto_pgo,
				@ValorUno,								@ValorCero,										a.prtsdr_dstno_rcda_cta_rcprcn,
				a.nmro_unco_prncpl_cta_rcprcn,			case a.prtsdr_dstno_rcda_cta_rcprcn when @ValorSI then @ValorDos 
				                                                                            when @ValorNO then @ValorUNo
																							else @ValorUNo
														End,
				a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.nmro_actvdds
	From		#TempServicioSolicitudes a
	Inner Join	#TempSolicitudes so
	On			so.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		a.gnra_cbro = @ValorSI
	And			a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			a.cnsctvo_cdgo_itm_bnfccn != @cnsctvo_cdgo_itm_bnfccn2
	Group By    a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.cnsctvo_slctd_atrzcn_srvco,			a.cnsctvo_cdgo_cncpto_pgo,				a.nmro_actvdds,
				a.prtsdr_dstno_rcda_cta_rcprcn,			a.nmro_unco_prncpl_cta_rcprcn;

	-- Se guarda un consolidado para cada prestacion con item de bonificacion 2
	Insert Into #tempTbAsConsolidadoCuotaRecuperacionxOps
	(
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,							cnsctvo_cdgo_cncpto_pgo,
				cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,			prtsdr_dstno_rcda_cta_rcprcn,
				nmro_unco_prncpl_cta_rcprcn,			cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,	cnsctvo_cdgo_itm_bnfccn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,		cnsctvo_cdgo_tcnca_lqdcn,				nmro_actvdds
	)
	Select Distinct
				a.cnsctvo_slctd_atrzcn_srvco,			nmro_unco_ops,						a.cnsctvo_cdgo_cncpto_pgo,
				1/*@ValorUno*/,							0,		a.prtsdr_dstno_rcda_cta_rcprcn,
				a.nmro_unco_prncpl_cta_rcprcn,			case a.prtsdr_dstno_rcda_cta_rcprcn when @ValorSI then @ValorDos 
				                                                                            when @ValorNO then @ValorUNo
																							else @ValorUNo
														End,
				a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.nmro_actvdds
	From		#TempServicioSolicitudes a
	Inner Join	#TempSolicitudes so
	On			so.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		a.gnra_cbro = @ValorSI
	And			a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			a.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn2
	Group By    a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.cnsctvo_slctd_atrzcn_srvco,			a.cnsctvo_cdgo_cncpto_pgo,				a.nmro_actvdds,
				a.prtsdr_dstno_rcda_cta_rcprcn,			a.nmro_unco_prncpl_cta_rcprcn,			a.nmro_unco_ops;

	-- Se inserta la informacion del consolidado por los agrupadores que generan cobro Cnsctvo_cdgo_tcnca_lqdcn,
	-- cnsctvo_dtlle_mdlo_tcncs_bnfccn, cnsctvo_cdgo_cncpto_pgo y nmro_unco_ops
	Insert Into #tempTbAsConsolidadoCuotaRecuperacionxOps
	(
				cnsctvo_slctd_atrzcn_srvco,				nmro_unco_ops,							cnsctvo_cdgo_cncpto_pgo,
				cnsctvo_cdgo_estdo_cta_rcprcn,			vlr_lqdcn_cta_rcprcn_srvco_ops,			prtsdr_dstno_rcda_cta_rcprcn,
				nmro_unco_prncpl_cta_rcprcn,			cnsctvo_cdgo_tpo_mdo_cbro_cta_rcprcn,	cnsctvo_cdgo_itm_bnfccn,
				cnsctvo_dtlle_mdlo_tcncs_bnfccn,		cnsctvo_cdgo_tcnca_lqdcn,				nmro_actvdds
	)
	Select Distinct
				a.cnsctvo_slctd_atrzcn_srvco,			a.nmro_unco_ops,						a.cnsctvo_cdgo_cncpto_pgo,
				@ValorUno,								@ValorCero,								a.prtsdr_dstno_rcda_cta_rcprcn,
				a.nmro_unco_prncpl_cta_rcprcn,			case a.prtsdr_dstno_rcda_cta_rcprcn when @ValorSI then @ValorDos 
				                                                                            when @ValorNO then @ValorUNo
																							else @ValorUNo
														End,
				a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.nmro_actvdds
	From		#TempServicioSolicitudes a
	Inner Join	#TempSolicitudes so
	On			so.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		a.gnra_cbro = @ValorSI
	And			a.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2
	Group By    a.cnsctvo_cdgo_itm_bnfccn,				a.cnsctvo_dtlle_mdlo_tcncs_bnfccn,		a.Cnsctvo_cdgo_tcnca_lqdcn,
				a.cnsctvo_slctd_atrzcn_srvco,			a.cnsctvo_cdgo_cncpto_pgo,				a.nmro_actvdds,
				a.prtsdr_dstno_rcda_cta_rcprcn,			a.nmro_unco_prncpl_cta_rcprcn,			a.nmro_unco_ops;

	-- Se asigna el valor a las prestaciones con cuota moderadora, exepto para las prestaciones con item de bonificacion 2
	Update		ccro
	Set			vlr_lqdcn_cta_rcprcn_srvco_ops = ss.vlr_lqdcn_cta_rcprcn_srvco_fnl
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps ccro
	Inner Join	@TempCuota ss	
	On			ccro.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ccro.cnsctvo_cdgo_tcnca_lqdcn = ss.cnsctvo_cdgo_tcnca_lqdcn
	And			ccro.cnsctvo_dtlle_mdlo_tcncs_bnfccn = ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn
	And			ccro.cnsctvo_cdgo_itm_bnfccn = ss.cnsctvo_cdgo_itm_bnfccn
	And			ccro.prtsdr_dstno_rcda_cta_rcprcn = ss.prtsdr_dstno_rcda_cta_rcprcn
	And			ccro.nmro_actvdds = ss.nmro_actvdds
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			ss.cnsctvo_cdgo_itm_bnfccn != @cnsctvo_cdgo_itm_bnfccn2;

	-- Se asigna el valor a las prestaciones con cuota moderadora para las prestaciones con item de bonificacion 2
	Update		ccro
	Set			vlr_lqdcn_cta_rcprcn_srvco_ops = ss.vlr_lqdcn_cta_rcprcn_srvco_fnl
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps ccro
	Inner Join	@TempCuota ss	
	On			ccro.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ccro.cnsctvo_cdgo_tcnca_lqdcn = ss.cnsctvo_cdgo_tcnca_lqdcn
	And			ccro.cnsctvo_dtlle_mdlo_tcncs_bnfccn = ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn
	And			ccro.cnsctvo_cdgo_itm_bnfccn = ss.cnsctvo_cdgo_itm_bnfccn
	And			ccro.prtsdr_dstno_rcda_cta_rcprcn = ss.prtsdr_dstno_rcda_cta_rcprcn
	And			ccro.nmro_actvdds = ss.nmro_actvdds
	And			ccro.nmro_unco_ops = ss.nmro_unco_ops
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			ss.cnsctvo_cdgo_itm_bnfccn = @cnsctvo_cdgo_itm_bnfccn2;

	-- Se asigna el valor a las prestaciones con copago
	Update		ccro
	Set			vlr_lqdcn_cta_rcprcn_srvco_ops = ss.vlr_lqdcn_cta_rcprcn_srvco_fnl
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps ccro
	Inner Join	@TempCuota ss	
	On			ccro.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ccro.cnsctvo_cdgo_tcnca_lqdcn = ss.cnsctvo_cdgo_tcnca_lqdcn
	And			ccro.cnsctvo_dtlle_mdlo_tcncs_bnfccn = ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn
	And			ccro.cnsctvo_cdgo_itm_bnfccn = ss.cnsctvo_cdgo_itm_bnfccn
	And			ccro.prtsdr_dstno_rcda_cta_rcprcn = ss.prtsdr_dstno_rcda_cta_rcprcn
	And			ccro.nmro_actvdds = ss.nmro_actvdds
	And			ccro.nmro_unco_ops = ss.nmro_unco_ops
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo2;

	-- Se graban los numero de ops que generaran el cobro a las prestaciones con cuota moderadora
	Update		ccro
	Set			nmro_unco_ops = ss.nmro_unco_ops
	From		#tempTbAsConsolidadoCuotaRecuperacionxOps ccro
	Inner Join	#TempServicioSolicitudes ss	
	On			ccro.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			ccro.cnsctvo_cdgo_tcnca_lqdcn = ss.cnsctvo_cdgo_tcnca_lqdcn
	And			ccro.cnsctvo_dtlle_mdlo_tcncs_bnfccn = ss.cnsctvo_dtlle_mdlo_tcncs_bnfccn
	And			ccro.cnsctvo_cdgo_itm_bnfccn = ss.cnsctvo_cdgo_itm_bnfccn
	And			ccro.prtsdr_dstno_rcda_cta_rcprcn = ss.prtsdr_dstno_rcda_cta_rcprcn
	And			ccro.nmro_actvdds = ss.nmro_actvdds
	Where		ss.cnsctvo_cdgo_cncpto_pgo = @cnsctvo_cdgo_cncpto_pgo1
	And			ss.cnsctvo_cdgo_itm_bnfccn != @cnsctvo_cdgo_itm_bnfccn2;

End

