USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRetornarResultadoDireccionamiento]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASRetornarResultadoDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite retornar el resultado del direccionamiento D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 29/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASRetornarResultadoDireccionamiento] 
@es_btch char(1),
@msje_rspsta xml OUTPUT
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @negacion char(1)
  SET @negacion = 'N'

  IF @es_btch = @negacion
  BEGIN
    WITH temp
    AS
    --Se usa distinct para retornar sólo la información cabecera, sin prestaciones.
    (SELECT DISTINCT
      slctd.cnsctvo_slctd AS cnsctvo_slctd_srvco_sld_rcbda,
      ISNULL(slctd.nmro_slctd, '') AS nmro_slctd_ss,
      ISNULL(slctd.nmro_prvdr, '') AS nmro_slctd_prvdr
    FROM #tmpInformacionSolicitudPrestacion slctd)
    SELECT
      @msje_rspsta = (SELECT
        slctd.cnsctvo_slctd_srvco_sld_rcbda AS "id_slctd",
        slctd.nmro_slctd_ss AS "nmro_slctd_ss",
        slctd.nmro_slctd_prvdr AS "nmro_slctd_prvdr",
        (SELECT
          prstcn.cnsctvo_cdgo_prstcn AS "cnsctvo_cdfccn",
          prstcn.cdgo_prstcn AS "cdgo_cdfccn",
          (SELECT
            prstdr.cdgo_intrno_ips_dstno AS "cdgo_intrno_ips_dstno"
          FROM #tmpInformacionSolicitudPrestacion AS prstdr
          WHERE prstdr.id = prstcn.id
          AND prstdr.cdgo_intrno_ips_dstno IS NOT NULL
          FOR xml AUTO, TYPE, ELEMENTS)
          AS prstdrs
        FROM #tmpInformacionSolicitudPrestacion AS prstcn
        WHERE prstcn.cnsctvo_slctd = slctd.cnsctvo_slctd_srvco_sld_rcbda
        FOR xml AUTO, TYPE, ELEMENTS)
      FROM temp AS slctd
      FOR xml AUTO, TYPE, ELEMENTS, ROOT ('rsltdo_drccnmnto'))
  END
END

GO
