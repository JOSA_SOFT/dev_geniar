USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudAnexos]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudAnexos
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Anexos como 
					 :	modificación de una(s) solicitud(es) especifica(s) D\> 
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajustan no conformidades: 
*					 : 25 Se crean variables dentro de los Procedimientos para el manejo de los valores constantes? D\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>   
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudAnexos] 
AS
  SET NOCOUNT ON
	DECLARE @Fecha_Actual as datetime
	SET @Fecha_Actual = Getdate()
	BEGIN
		-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASDocumentosAnexosServicios
		DELETE			ANX
		FROM			BdCNA.gsa.tbASDocumentosAnexosServicios ANX
		INNER JOIN		#TMod_Solicitudes	TSO
		ON				ANX.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		-- INSERTAR INFORMACIÓN DE ANEXOS EN TABLA tbASDocumentosAnexosServicios
		INSERT INTO	 BdCNA.gsa.tbASDocumentosAnexosServicios(
					 cnsctvo_slctd_atrzcn_srvco
					,cnsctvo_cdgo_mdlo_dcmnto_sprte
					,cnsctvo_cdgo_dcmnto_sprte
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo
					,ANX.cnsctvo_cdgo_mdlo_dcmnto_sprte	
					,ANX.cnsctvo_cdgo_dcmnto_sprte	
					,@Fecha_Actual	--fcha_crcn	
					,ANX.usro_crcn							
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,ANX.usro_crcn	--usro_ultma_mdfccn	
		FROM		#TMod_Anexos	ANX WITH (NOLOCK)

		INNER JOIN	#TMod_solicitudes	IDS WITH (NOLOCK)
		ON			ANX.id = IDS.id	
	END

GO
