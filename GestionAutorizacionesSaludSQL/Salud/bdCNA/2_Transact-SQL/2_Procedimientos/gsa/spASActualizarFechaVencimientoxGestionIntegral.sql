USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarFechaVencimientoxGestionIntegral]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASActualizarFechaVencimientoxGestionIntegral
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarFechaVencimientoxGestionIntegral]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@fcha_vncmnto_prcso			datetime = null
AS
BEGIN
	
	SET NOCOUNT ON;

    if(@fcha_vncmnto_prcso is not null)
	begin
		Update sas set fcha_vncmnto_gstn = @fcha_vncmnto_prcso
		From BDCna.gsa.tbASSolicitudesAutorizacionServicios sas
		Where cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	end


END

GO
