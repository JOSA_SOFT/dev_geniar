USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosDireccionamiento
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionDireccionamiento]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
	@cnsctvo_srvco_slctdo       udtConsecutivo

AS
BEGIN
SET NOCOUNT ON;
	DECLARE @ldfcha_consulta		DATETIME;

	Create Table #DireccionamientoFechaEntrega(
		cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
		cnsctvo_srvco_slctdo		udtConsecutivo,
		cdgo_intrno					udtCodigoIps,
		nmbre_scrsl					udtDescripcion,
		cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
		nmro_idntfccn				udtNumeroIdentificacionLargo,
		cdgo_tpo_idntfccn			char(3),
		fcha_estmda_entrga			datetime
	)

	SET	@ldfcha_consulta = GETDATE()

	Insert into #DireccionamientoFechaEntrega(
		cdgo_intrno,			cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo
	)
    SELECT 
		RD.cdgo_intrno,
		@cnsctvo_slctd_atrzcn_srvco,
		@cnsctvo_srvco_slctdo
	FROM BDCna.gsa.tbASResultadoDireccionamiento RD With(NoLock)
	WHERE RD.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	AND RD.cnsctvo_srvco_slctdo  = @cnsctvo_srvco_slctdo




	Update RD set   nmbre_scrsl					= DP.nmbre_scrsl,
					cnsctvo_cdgo_tpo_idntfccn	= PR.cnsctvo_cdgo_tpo_idntfccn,
					nmro_idntfccn				= PR.nmro_idntfccn,
					cdgo_tpo_idntfccn			= TI.cdgo_tpo_idntfccn
	FROM #DireccionamientoFechaEntrega RD With(NoLock)
	INNER JOIN bdSisalud.dbo.tbDireccionesPrestador DP With(NoLock) ON RD.cdgo_intrno = DP.cdgo_intrno
	INNER JOIN bdSisalud.dbo.tbprestadores PR With(NoLock) ON PR.nmro_unco_idntfccn_prstdr = DP.nmro_unco_idntfccn_prstdr
	INNER JOIN BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias	ti 	With(NoLock) on TI.cnsctvo_cdgo_tpo_idntfccn = PR.cnsctvo_cdgo_tpo_idntfccn
	WHERE @ldfcha_consulta between ti.inco_vgnca And ti.fn_vgnca;


	Update DF Set DF.fcha_estmda_entrga = RFE.fcha_estmda_entrga
	From #DireccionamientoFechaEntrega DF
	INNER JOIN BDCna.gsa.tbASResultadoFechaEntrega RFE With(NoLock) 
	ON	RFE.cnsctvo_slctd_atrzcn_srvco = DF.cnsctvo_slctd_atrzcn_srvco
	And RFE.cnsctvo_srvco_slctdo = DF.cnsctvo_srvco_slctdo

	Select 		
		cdgo_intrno,
		nmbre_scrsl,
		cnsctvo_cdgo_tpo_idntfccn,
		nmro_idntfccn,
		cdgo_tpo_idntfccn,
		fcha_estmda_entrga
	from #DireccionamientoFechaEntrega;

	Drop table #DireccionamientoFechaEntrega;

END

GO
