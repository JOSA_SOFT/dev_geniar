USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarSolicitudAnexos]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarSolicitudAnexos
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información de Anexos de una 
					 : solictud ingresada  D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajusta la creación de variables dentro de los Procedimientos 
						para el manejo de los valores constantes? D\>  
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>   
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGrabarSolicitudAnexos] 
AS
  SET NOCOUNT ON
  
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- INSERTAR INFORMACIÓN DE ANEXOS EN TABLA tbASDocumentosAnexosServicios
		INSERT INTO	 BdCNA.gsa.tbASDocumentosAnexosServicios(
					 cnsctvo_slctd_atrzcn_srvco
					,cnsctvo_cdgo_mdlo_dcmnto_sprte
					,cnsctvo_cdgo_dcmnto_sprte
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco
					,ANX.cnsctvo_cdgo_mdlo_dcmnto_sprte	
					,ANX.cnsctvo_cdgo_dcmnto_sprte	
					,@Fecha_Actual	--fcha_crcn	
					,ANX.usro_crcn							
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,ANX.usro_crcn	--usro_ultma_mdfccn	
		FROM		#Tempo_Anexos	ANX WITH (NOLOCK)
		INNER JOIN	#Idsolicitudes	IDS WITH (NOLOCK)
		ON			ANX.id = IDS.idXML;	
		
	END



GO
