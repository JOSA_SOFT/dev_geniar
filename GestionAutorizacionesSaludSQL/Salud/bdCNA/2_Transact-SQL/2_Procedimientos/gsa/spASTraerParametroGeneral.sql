USE [bdCNA]
GO
/****** Object:  StoredProcedure  [prm].[spASTraerParametroGeneral]     Script Date: 09/03/2017 15:46:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO

IF (object_id('spASTraerParametroGeneral') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE spASTraerParametroGeneral AS SELECT 1;'
END
GO

/*-------------------------------------------------------------------------------------------------------------
* Metodo o PRG 	         :  spASTraerParametroGeneral
* Desarrollado por		 :  <\A   Ing. Victor Hugo Gil Ramos A\>
* Descripcion			 :  <\D   
                                Procedimiento que permite recuperar el valor de los parametros generales
								de bdCNA 	
						    D\>
* Observaciones		     :  <\O	O\>
* Parametros			 :  <\P	P\>
* Fecha Creacion		 :  <\FC 2017/03/09	FC\>
*-------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION   
*-------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM   AM\>
* Descripcion			 : <\DM   DM\>
* Nuevos Parametros	 	 : <\PM   PM\>
* Nuevas Variables		 : <\VM   VM\>
* Fecha Modificacion	 : <\FM   FM\>
*-------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [dbo].[spASTraerParametroGeneral] 
	@cdgo_prmtro_gnrl	    Char(3)             ,
	@vsble_usro			    udtLogico           ,
	@cdgo_prmtro_gnrl_vgnca	Char(3)             ,
	@vsble_usro_vgnca	    udtLogico           ,
	@tpo_dto_prmtro		    udtLogico           ,
	@vlr_prmtro_nmrco	    Numeric(18,0) Output,
	@vlr_prmtro_crctr	    Varchar(200)  Output,
	@vlr_prmtro_fcha	    Datetime      Output
AS
Begin
    SET NOCOUNT ON

	DECLARE @cnsctvo_cdgo_prmtro_gnrl Int     ,
			@fcha_actual              Datetime

	Set @fcha_actual = getDate()

	Select @cnsctvo_cdgo_prmtro_gnrl = cnsctvo_cdgo_prmtro_gnrl
	From   tbParametrosGenerales 
	Where  cdgo_prmtro_gnrl = @cdgo_prmtro_gnrl
	And    vsble_usro = @vsble_usro

	IF @cnsctvo_cdgo_prmtro_gnrl IS NOT NULL
		Begin
			Select	@vlr_prmtro_nmrco = vlr_prmtro_nmrco,
					@vlr_prmtro_crctr = vlr_prmtro_crctr,
					@vlr_prmtro_fcha = vlr_prmtro_fcha
			From	tbParametrosGenerales_Vigencias With(NoLock)
			Where	cnsctvo_cdgo_prmtro_gnrl = @cnsctvo_cdgo_prmtro_gnrl
			And		cdgo_prmtro_gnrl         = @cdgo_prmtro_gnrl_vgnca
			And		vsble_usro               = @vsble_usro_vgnca
			And		(@fcha_actual >= inco_vgnca AND @fcha_actual <= fn_vgnca)
		End
End

