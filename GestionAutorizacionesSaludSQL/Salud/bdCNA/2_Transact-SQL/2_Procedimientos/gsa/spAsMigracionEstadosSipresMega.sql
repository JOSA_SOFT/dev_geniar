USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsMigracionEstadosSipresMega]    Script Date: 24/05/2017 01:43:33 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsMigracionEstadosSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	
							  D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  Entrada:
								   1- @fcha_mgra: Fecha a partir de la cual se migrara la informacion
								   Salida:
								   1- @cdgo_slda: Codigo que indica el estado del proceso.
								   2- @mnsge_slda: Mensaje que informa el estado del proceso.
							  P\>  													
* Variables					: <\V  1- @usro_sstma: Usuario del sistema.
								   2- @cdgo_exto: Codigo que indica que el proceso termino de forma correcta.
								   3- @cdgo_errr: Codigo que indica que ocurrio un error durante el proceso.
								   4- @mnsge_exto: Mensaje que informa que el proceso termino correctamente
								   5- @mnsge_errr: Mensaje que informa que el proceso termino con errores.
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.gsa.spAsPoblarTemporalesMigracionEstadosSipresMega
								   2- BdCna.gsa.spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega
								   3- BdCna.gsa.spAsModificarEstadoSolicitudMigracionSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega llamado al procedimiento spAsMigracionMarcasSipresMega DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/20 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega campo vlr_lqdcn_srvco en la tabla temporal #tempPrestaciones DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/10 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega campo fcha_imprsn en tabla temporal #tempPrestaciones DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/15 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se agrega ejecucion de procedimientos spAsDesAsociarProgramacionDeEntregaMigracionSipresMega
							   y spAsDesAsociarHospitalizacionMigracionSipresMega DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/03/08 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se quita tabla temporal #tempSolicitudes DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/09 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se ajusta procedimiento para registrar el log del proceso,
							   se agrega llamado a spASEjecutarAnulacionCuotasRecuperacion y
							   spAsCambiarEstadoConceptosMigracionEstadosSipresMega para tener control de los pasos 
							   ejecutados. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/27 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andrés López Ramírez AM\>  													
* Descripcion			: <\DM Se realiza ajuste para correcto funcionamiento del log DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/27 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/
/*

Exec gsa.spAsMigracionEstadosSipresMega '20170202'

*/
ALTER Procedure [gsa].[spAsMigracionEstadosSipresMega]
			@fcha_mgra									Date
As
Begin
	Set NoCount On
	
	Declare @usro_sstma							UdtUsuario,
			@cdgo_exto							UdtCodigo,
			@cdgo_errr							UdtCodigo,
			@mnsge_exto							UdtDescripcion,
			@mnsge_errr							UdtDescripcion,
			@cnsctvo_cdgo_tpo_prcso				UdtConsecutivo,
			@usro								UdtUsuario,
			@cnsctvo_prcso						UdtConsecutivo,
			@cnsctvo_lg							UdtConsecutivo,
			@prcso_crga_sprs					UdtConsecutivo,
			@prcso_anlcn_cts					UdtConsecutivo,
			@prcso_mdfccn_prstcn				UdtConsecutivo,
			@prcso_mdfccn_cncpts				UdtConsecutivo,
			@prcso_dsscr_prgrmcn				UdtConsecutivo,
			@prcso_dsscr_hsptlzcn				UdtConsecutivo,
			@prcso_mdfccn_slctd					UdtConsecutivo,
			@cnsctvo_cdgo_estdo11				UdtConsecutivo,
			@cnsctvo_cdgo_estdo3				UdtConsecutivo,
			@vlr_nll							VarChar(3),
			@pso1								Int,
			@pso2								Int,
			@pso3								Int,
			@pso4								Int,
			@pso5								Int,
			@pso6								Int,
			@pso7								Int,
			@errr								Int,
			@exto								Int,
			@cnsctvo_prcso_exto					UdtConsecutivo,
			@cnsctvo_prcso_err					UdtConsecutivo,
			@cdgo_slda							UdtCodigo ,
			@mnsge_slda							VarChar(1000);

	-- 
	Create Table #tempPrestaciones
	(
		cnsctvo_srvco_slctdo						UdtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo					UdtConsecutivo,
		cnsctvo_cdgo_estdo_srvco_slctdo				UdtConsecutivo, -- Estado Mega
		cnsctvo_cdgo_estdo							UdtConsecutivo, -- Estado Sipres
		cnsctvo_prcdmnto_insmo_slctdo				UdtConsecutivo,
		cnsctvo_mdcmnto_slctdo						UdtConsecutivo,
		cntdd_slctda								Int	Default 0,
		cntdd_atrzda								Int Default 0,	
		fcha_ultma_mdfccn							Date,
		usro_ultma_mdfccn							UdtUsuario,
		cnsctvo_cdgo_ofcna							UdtConsecutivo,
		nuam										Numeric(18, 0),
		vlr_lqdcn_srvco								Decimal(18, 0) Default 0,
		fcha_imprsn									Date,
		cnsctvo_cdgo_det_prgrmcn_fcha_evnto			UdtConsecutivo
	);

	-- 
	Create Table #tempConceptosPrestaciones
	(
		cnsctvo_cncpto_prcdmnto_slctdo				UdtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco					UdtConsecutivo,
		nuam										Numeric(18, 0),
		cnsctvo_ops									Int,
		cnsctvo_cdgo_srvco_slctdo					UdtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo				UdtConsecutivo,
		cnsctvo_mdcmnto_slctdo						UdtConsecutivo,
		cnsctvo_prstcn								UdtConsecutivo,
		vlr_lqdcn									Decimal(18,0) Default 0,
		cnsctvo_cdgo_cncpto_gsto					UdtConsecutivo,
		cdgo_intrno_prstdr_atrzdo					UdtCodigoIps,
		cnsctvo_cdgo_grpo_imprsn					UdtConsecutivo,
		nmro_rdccn_cm								UdtConsecutivo,
		cnsctvo_cdgo_ofcna_cm						udtConsecutivo,
		vlr_cncpto_cm								Float,
		gnrdo										udtLogico,
		fcha_utlzcn_dsde							Datetime,
		fcha_utlzcn_hsta							Datetime,
		nmro_unco_ops								udtConsecutivo, -- Preguntar
		cnsctvo_cdgo_estdo_cncpto_srvco_slctdo		udtConsecutivo,
		cnsctvo_cdgo_estdo_sprs						UdtConsecutivo,
		ds_vldz										Int, -- preguntar
		nmro_unco_prncpl_cta_rcprcn					UdtLogico, -- Preguntar
		fcha_crcn									Date,
		usro_crcn									UdtUsuario,
		fcha_ultma_mdfccn							Date,
		usro_ultma_mdfccn							UdtUsuario
	)

	-- 
	Create table #tmpSolicitudAnulacionCuotasRecuperacion
	(
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo, -- número de la solicitud
		cnsctvo_cdgo_srvco_slctdo		UdtConsecutivo, -- consecutivo de la prestación o servicio
		nmro_unco_ops					UdtConsecutivo  
	)

	Set @usro_sstma = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User));
	Set @cdgo_exto = 'OK';
	Set @cdgo_errr = 'ET';
	Set @mnsge_exto = 'Migracion exitosa';
	Set @mnsge_errr	= 'Ocurrio un error en el proceso de migracion';
	Set @cnsctvo_cdgo_tpo_prcso = 30;
	Set @usro = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user));
	Set @prcso_crga_sprs = 151;
	Set @prcso_anlcn_cts = 152;
	Set @prcso_mdfccn_prstcn = 153;
	Set @prcso_mdfccn_cncpts = 154;
	Set @prcso_dsscr_prgrmcn = 155;
	Set @prcso_dsscr_hsptlzcn = 156;
	Set @prcso_mdfccn_slctd = 157;
	Set @vlr_nll = Null;
	Set @pso1 = -2;
	Set @pso2 = -2;
	Set @pso3 = -2;
	Set @pso4 = -2;
	Set @pso5 = -2;
	Set @pso6 = -2;
	Set @pso7 = -2;
	Set @errr = -1;
	Set @exto = 0;
	Set @cnsctvo_prcso_exto = 2;
	Set @cnsctvo_prcso_err = 3;

	Begin Try
		
		Begin Transaction migracion_mega

		Exec bdprocesossalud.dbo.spproregistraproceso	@cnsctvo_cdgo_tpo_prcso,
														@usro,
														@cnsctvo_prcso OUTPUT;
		-- Llena la tabla temporal.

		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 1. Inicia proceso de carga de información de Sipres.',
																   @prcso_crga_sprs,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsPoblarTemporalesMigracionEstadosSipresMega @fcha_mgra;
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg;
			Set @pso1 = @exto;
		End Try
		Begin Catch
			Set @pso1 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch

		Begin Try
			
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 2. Anulacion de cuotas de prestaciones anuladas y vencidas.',
																   @prcso_anlcn_cts,
																   @cnsctvo_lg OUTPUT;
			--
			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
						cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,		nmro_unco_ops
			)
			Select Distinct 
						em.cnsctvo_slctd_atrzcn_srvco,	em.cnsctvo_cdgo_srvco_slctdo,	cp.nmro_unco_ops
			From		#tempPrestaciones em
			Inner Join	#tempConceptosPrestaciones cp
			On			cp.cnsctvo_prcdmnto_insmo_slctdo = em.cnsctvo_prcdmnto_insmo_slctdo
			Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
			On			cp.nmro_unco_ops = ccro.nmro_unco_ops
			Where		em.cnsctvo_cdgo_estdo In (@cnsctvo_cdgo_estdo3, @cnsctvo_cdgo_estdo11);

			-- 
			Insert #tmpSolicitudAnulacionCuotasRecuperacion
			(
						cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_srvco_slctdo,		nmro_unco_ops
			)
			Select Distinct 
						em.cnsctvo_slctd_atrzcn_srvco,	em.cnsctvo_cdgo_srvco_slctdo,	cp.nmro_unco_ops
			From		#tempPrestaciones em
			Inner Join	#tempConceptosPrestaciones cp
			On			cp.cnsctvo_mdcmnto_slctdo = em.cnsctvo_mdcmnto_slctdo
			Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
			On			cp.nmro_unco_ops = ccro.nmro_unco_ops
			Where		em.cnsctvo_cdgo_estdo In (@cnsctvo_cdgo_estdo3, @cnsctvo_cdgo_estdo11);

			Exec BDCna.gsa.spASEjecutarAnulacionCuotasRecuperacion @usro_sstma, @mnsge_slda Output, @cdgo_slda Output, @vlr_nll, @vlr_nll, @vlr_nll;
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg

		Set @pso2 = @exto;
		End Try
		Begin Catch
			Set @pso2 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch



		-- Cambia el estado de las prestaciones y guarda el historial de cambios
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 3. Actualización de estados de prestaciones en Mega.',
																   @prcso_mdfccn_prstcn,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega @usro_sstma;
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
			Set @pso3 = @exto;
		End Try
		Begin Catch
			Set @pso3 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch

		-- Actualiza el estado de los conceptos y guarda el historial de cambios
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 4. Actualización de estados de conceptos en Mega.',
																   @prcso_mdfccn_cncpts,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsCambiarEstadoConceptosMigracionEstadosSipresMega @usro_sstma
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg;
			Set @pso4 = @exto;
		End Try
		Begin Catch
			Set @pso4 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch
		

		-- DesAsocia la Programacion de entrega para las prestaciones vencidas y anuladas
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 5. Desasociación de programación de entrega para las prestaciones vencidas y anuladas.',
																   @prcso_dsscr_prgrmcn,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsDesAsociarProgramacionDeEntregaMigracionSipresMega
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg;
			Set @pso5 = @exto;
		End Try
		Begin Catch
			Set @pso5 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch

		-- DesAsocia la hospitalizacion para prestaciones vencidas y anuladas.
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 6. Desasociación de hospitalización para prestaciones vencidas y anuladas.',
																   @prcso_dsscr_hsptlzcn,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsDesAsociarHospitalizacionMigracionSipresMega
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg;
			Set @pso6 = @exto;
		End Try
		Begin Catch
			Set @pso6 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch
		
		-- Cambia el estado de las solictudes
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 7. Actualización de estados de las solictudes.',
																   @prcso_mdfccn_slctd,
																   @cnsctvo_lg OUTPUT;
			Exec gsa.spAsModificarEstadoSolicitudMigracionSipresMega @usro_sstma;
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg;
			Set @pso7 = @exto;
		End Try
		Begin Catch
			Set @pso7 = @errr;
			Set @cdgo_slda = @cdgo_errr;
			Set @mnsge_slda = concat(@mnsge_errr, ' ', ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
			Throw
		End Catch
					
		Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@cnsctvo_cdgo_tpo_prcso,@cnsctvo_prcso_exto	
		
		Commit Transaction migracion_mega
	End Try
	Begin Catch
		RollBack Transaction migracion_mega
		RAISERROR (@mnsge_slda, 16, 2) WITH SETERROR

		Exec bdprocesossalud.dbo.spproregistraproceso	@cnsctvo_cdgo_tpo_prcso,
														@usro,
														@cnsctvo_prcso OUTPUT;

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,		@cnsctvo_cdgo_tpo_prcso,
																'Paso 1. Inicia proceso de carga de información de Sipres.',
																@prcso_crga_sprs,	@cnsctvo_lg OUTPUT;								
		If(@pso1 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso1 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,		@cnsctvo_cdgo_tpo_prcso,
																'Paso 2. Anulacion de cuotas de prestaciones anuladas y vencidas.',
																@prcso_anlcn_cts,	@cnsctvo_lg OUTPUT;							
		If(@pso2 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso2 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,			@cnsctvo_cdgo_tpo_prcso,
																'Paso 3. Actualización de estados de prestaciones en Mega.',
																@prcso_mdfccn_prstcn,	@cnsctvo_lg OUTPUT;							
		If(@pso3 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso3 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,			@cnsctvo_cdgo_tpo_prcso,
																'Paso 4. Actualización de estados de conceptos en Mega.',
																@prcso_mdfccn_cncpts,	@cnsctvo_lg OUTPUT;							
		If(@pso4 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso4 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,			@cnsctvo_cdgo_tpo_prcso,
																'Paso 5. Desasociación de programación de entrega para las prestaciones vencidas y anuladas.',
																@prcso_dsscr_prgrmcn,	@cnsctvo_lg OUTPUT;							
		If(@pso5 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso5 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,				@cnsctvo_cdgo_tpo_prcso,
																'Paso 6. Desasociación de hospitalización para prestaciones vencidas y anuladas.',
																@prcso_dsscr_hsptlzcn,		@cnsctvo_lg OUTPUT;						
		If(@pso6 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso6 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	@mnsge_slda;
		End

		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,			@cnsctvo_cdgo_tpo_prcso,
																'Paso 7. Actualización de estados de las solictudes.',
																@prcso_mdfccn_slctd,	@cnsctvo_lg OUTPUT;							
		If(@pso7 = @exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@pso7 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,	  @mnsge_slda;
		End
		-- Registramos el fin del proceso Automático log de procesos con error.
		Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@cnsctvo_cdgo_tpo_prcso,@cnsctvo_prcso_err	
		--Envío de notificación
		Exec bdProcesosSalud.dbo.spPROEnviarNotificacion @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso, @cnsctvo_prcso_err
	End Catch

	Drop Table  #tempPrestaciones;
	Drop Table  #tempConceptosPrestaciones;

End