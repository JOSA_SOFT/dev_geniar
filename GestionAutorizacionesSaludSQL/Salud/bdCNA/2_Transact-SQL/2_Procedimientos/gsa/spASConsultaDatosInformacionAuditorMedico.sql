USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionAuditorMedico]    Script Date: 19/05/2017 8:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosAuditorMedico
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS A\>
* Descripcion			: <\D  D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2012/10/04 FC\>
*-----------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se modifica por completo el procedimiento para que consulte 
								la informacion basica del medico auditor.
								Se modifican los parametros de entrada del SP @cnsctvo_slctd_atrzcn_srvco
								@cnsctvo_cdfccn por @lgn_usro
                             DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*-----------------------------------------------------------------------------------------------------------------*/
--Exec [gsa].[spASConsultaDatosInformacionAuditorMedico] 'aumdat01'
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionAuditorMedico]
   @lgn_usro udtUsuario
AS
Begin
	SET NOCOUNT ON;

	Declare @cadenaVacia udtLogico
	Set     @cadenaVacia = ''
	
	Create 
	Table  #tmpAuditorMedico(cnsctvo_adtr  udtConsecutivo Identity(1,1),
		                     nmbre_usro	   Varchar(150),
		                     lgn_usro	   udtUsuario,
		                     dscrpcn_usro  udtDescripcion
	                        )

	Insert
	Into    #tmpAuditorMedico(nmbre_usro, lgn_usro, dscrpcn_usro)
	Select  CONCAT(ltrim(rtrim(prmr_nmbre_usro)) , @cadenaVacia,
	               ltrim(rtrim(sgndo_nmbre_usro)), @cadenaVacia,
	               ltrim(rtrim(prmr_aplldo_usro)), @cadenaVacia,
				   ltrim(rtrim(sgndo_aplldo_usro))
				  ),
            lgn_usro    , 
			dscrpcn_usro
	From    bdseguridad.dbo.tbusuarios
	Where   lgn_usro = @lgn_usro


    Select cnsctvo_adtr, nmbre_usro,
		   lgn_usro    , dscrpcn_usro
    From   #tmpAuditorMedico
End
