USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRegistrarGestionPresolicitudes]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASRegistrarGestionPresolicitudes]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASRegistrarGestionPresolicitudes] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASRegistrarGestionPresolicitudes
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Sp que se utiliza para guardar el resto de datos  D\>
						  <\D de la presolicitud para su gestion desde el BPM	D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/01 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASRegistrarGestionPresolicitudes] 
@cnsctvo_prslctd	udtConsecutivo,
@usro_gstn			udtUsuario
AS
BEGIN
	SET NOCOUNT ON;
	Declare @cdgo_rsltdo		udtConsecutivo ,
			@mnsje_rsltdo		varchar(2000),
			@codigo_error		int,
			@mensaje_error		varchar(2000);

	Set @codigo_error					 = -1;  -- Error SP

	Begin try
		Exec bdcna.gsa.spASRegistrarDatosAdicionalesPreSolicitud @cnsctvo_prslctd, @usro_gstn, @cdgo_rsltdo OUTPUT, @mnsje_rsltdo OUTPUT;
	END TRY
	BEGIN CATCH
		Set @mensaje_error = Concat(
								'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
								'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
								'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
								'Procedure:' , ERROR_PROCEDURE()
								);
			
		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH
END
GO
