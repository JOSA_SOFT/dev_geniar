USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaHistoricoTareasxInstanciaSolicitudWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaHistoricoTareasxInstanciaSolicitudWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos	A\>    
* Descripcion        : <\D Procedimiento que consulta la informacion del historico de las tareas por instancia para una solicitud  D\>					           
* Observaciones   	 : <\O O\>    
* Parametros         : <\P P\>   
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 01/04/2016  FC\>    
*---------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D    D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*----------------------------------------------------------------------------------------------------------------------------------*/

--exec [gsa].[spASConsultaHistoricoTareasxInstanciaSolicitudWeb] 1

ALTER PROCEDURE [gsa].[spASConsultaHistoricoTareasxInstanciaSolicitudWeb]
	@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
Begin
	SET NOCOUNT ON

	Declare @tpo_accn udtLogico


	Create 
	Table  #tmpHistoricoTareasxInstancia(cnsctvo_tra_instnca        udtConsecutivo,
	                                     cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
	                                     id_prcso                   varchar(50)   ,
	                                     cnsctvo_tra                udtConsecutivo,
	                                     nmbre_tra                  udtDescripcion,
	                                     fcha_tra                   datetime      ,
	                                     usro_tra                   udtUsuario    ,	                                     
										 tpo_tra                    int           ,
	                                     nmbre_grpo_rsgo            udtDescripcion
									    )

	Set @tpo_accn = 'I' 

	--Se inserta en la tabla el ultimo registro de tareas regsitradas en el bpm
	Insert 
	Into       #tmpHistoricoTareasxInstancia(cnsctvo_slctd_atrzcn_srvco, cnsctvo_tra_instnca) 
	Select     cnsctvo_slctd_atrzcn_srvco, Max(cnsctvo_tra_instnca)	 
	From       bdCNA.gsa.tbASHistoricoTareasxInstancia WITH(NOLOCK)	
	Where      cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And        tpo_accn = @tpo_accn
	Group By   cnsctvo_slctd_atrzcn_srvco
	
	--Se actualiza el resto de la informacion de la tareas en el bmp
	Update     #tmpHistoricoTareasxInstancia
	Set        id_prcso        = b.id_prcso       ,
			   cnsctvo_tra     = b.cnsctvo_tra    ,
			   nmbre_tra       = b.nmbre_tra      ,
			   fcha_tra        = b.fcha_tra       ,
			   usro_tra        = b.usro_tra       ,
			   tpo_tra         = b.tpo_tra        ,
			   nmbre_grpo_rsgo = b.nmbre_grpo_rsgo
	From       #tmpHistoricoTareasxInstancia a
	Inner Join bdCNA.gsa.tbASHistoricoTareasxInstancia b WITH(NOLOCK)
	On         b.cnsctvo_tra_instnca = a.cnsctvo_tra_instnca  

	Select   cnsctvo_tra_instnca  , cnsctvo_slctd_atrzcn_srvco, id_prcso       ,
             cnsctvo_tra          , nmbre_tra                 , fcha_tra       ,
             usro_tra             , tpo_tra                   , nmbre_grpo_rsgo
	From     #tmpHistoricoTareasxInstancia
	
	Drop Table #tmpHistoricoTareasxInstancia
End

GO
