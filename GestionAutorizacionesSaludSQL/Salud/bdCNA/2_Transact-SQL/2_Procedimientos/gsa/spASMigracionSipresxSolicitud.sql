USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASMigracionSipresxSolicitud]    Script Date: 25/07/2017 10:07:44 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*---------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASMigracionSipresxSolicitud
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite realiza la migracion de las programaciones de entrega
						   de manera automatica
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           cnsctvo_slctd_atrzcn_srvco  
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 06/06/2017 FC\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\DM Se crea tabla temporal para pasar los datos de la solicitud a migrar al 
							proceso de migracion de Mega a Sipres. 
							Se agrega llamado a procedimiento spASEjecutarIntegracionSipres  
						DM\> 
* Nuevas Variables   : <\VM @vlr_nll: Almacena el valor Null
							@msg_errr: Variable utilizada para almacenar la informacion del error, si este ocurre. VM\>    
* Fecha Modificacion : <\FM 06/06/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez AM\>    
* Descripcion        : <\DM Se ajusta sp para la migracion de la informacion de hospitalizacion. DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 25/07/2017 FM\>    
*----------------------------------------------------------------------------------------------------------------------------*/

/**
	Exec bdCNA.gsa.spASMigracionSipresxSolicitud 135064
*/

ALTER PROCEDURE [gsa].[spASMigracionSipresxSolicitud] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo
AS

Begin
    SET NOCOUNT ON;

	Declare	@mensaje				VarChar(1000),
			@vlr_no					udtLogico,
			@usro					udtUsuario,
			@cnsctvo_srvco_slctdo	udtConsecutivo;

	
	---- Se crea tabla temporal
	CREATE TABLE  #tmpinformacionsolicitud
	(
		id                           udtConsecutivo IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco   udtConsecutivo,
		cnsctvo_cdgo_ofcna           udtConsecutivo,
		nuam                         Numeric(18, 0),
		fcha_slctd                   Datetime      ,
		cdgo_intrno                  Char(8)       ,
		nmro_unco_idntfccn_prstdr    Int           ,
		nmro_unco_idntfccn_mdco      Int           ,
		cnsctvo_cdgo_cntngnca        udtConsecutivo,
		fcha_estmda_entrga           Datetime      ,
		cnsctvo_cdgo_clse_atncn      udtConsecutivo,
		cnsctvo_cdgo_frma_atncn      udtConsecutivo,
		obsrvcn_adcnl                Varchar(250)  ,
		fcha_crcn                    Datetime      ,
		usro_crcn                    udtusuario    ,
		usro_ultma_mdfccn            udtusuario    , 
		fcha_ultma_mdfccn            Datetime      ,
		cnsctvo_cdgo_dgnstco         udtConsecutivo,
		cnsctvo_cdgo_ofcna_atrzcn    udtConsecutivo,
		cnsctvo_cdgo_rcbro           udtConsecutivo,
		cnsctvo_cdgo_tpo_ubccn_pcnte udtConsecutivo,
		nmro_slctd_prvdr             Int           ,
		cnsctvo_cdgo_srvco_hsptlzcn  udtConsecutivo,
		cnsctvo_cdgo_mrbldd          udtConsecutivo,
		cnsctvo_cdgo_tpo_atncn       udtConsecutivo,
		cnsctvo_cdgo_estdo_mega      udtConsecutivo,
		cnsctvo_cdgo_estdo           udtConsecutivo,
		hra_dgta                     Char(10)      ,
		nmro_atncn                   Int           ,
		hra_slctd_orgn               Char(8)       ,
		cnsctvo_cdgo_mdo_cntcto      udtConsecutivo,
		fcha_imprsn                  Datetime      ,
		tpo_prstcn                   Char(1) DEFAULT 'N',
		cnsctvo_cdgo_grpo_entrga     udtConsecutivo,
		nmro_vsts                    Int           ,
		fcha_ingrso_hsptlzcn		 DateTime	   ,
		fcha_egrso_hsptlzcn			 DateTime	   ,
		ds_estnca                    Int Default 0 ,
		crte_cnta					 udtConsecutivo Default 0,
		cnsctvo_cdgo_clse_hbtcn		 udtConsecutivo,
		cnsctvo_cdgo_estdo_atncn     udtConsecutivo,
		cnsctvo_atncn_ops			 UdtConsecutivo,
		cnsctvo_cdgo_tpo_orgn_slctd  UdtConsecutivo,
		cnsctvo_cdgo_agrpdr_prstcn	 UdtConsecutivo
	)

	Create Table #tmpinformacionafiliado 
	(
		id Int IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco Int,
		nmro_unco_idntfccn_afldo Int,
		cnsctvo_cdgo_pln Int,
		cnsctvo_cdgo_sde_ips_prmra Int,
		cnsctvo_cdgo_tpo_idntfccn_afldo Int,
		nmro_idntfccn_afldo udtnumeroidentificacion,
		prmr_aplldo_afldo udtapellido Default '',
		sgndo_aplldo_afldo udtapellido Default '',
		prmr_nmbre_afldo udtnombre Default '',
		sgndo_nmbre_afldo udtnombre Default '',
		cnsctvo_bnfcro_cntrto Int,
		nmro_cntrto CHAR(15),
		cnsctvo_cdgo_tpo_cntrto Int,
		cnsctvo_cdgo_sxo Int,
		cnsctvo_cdgo_tpo_undd Int,
		edd_afldo_ans Int,
		smns_ctzds Int,
		cnsctvo_cdgo_tpo_vnclcn_afldo Int,
		cnsctvo_cdgo_rngo_slrl Int,
		fcha_ultma_mdfccn DATETIME,
		cdgo_ips_prmra udtcodigoips,
		fcha_ncmnto_afldo DATETIME,
		drccn_afldo udtdireccion,
		cnsctvo_cdgo_cdd_rsdnca_afldo Int,
		tlfno_afldo udttelefono,
		tlfno_cllr_afldo udttelefono,
		crro_elctrnco_afldo udtemail,
		cnsctvo_cdgo_ofcna udtcodigoips,
		cnsctvo_cdgo_prntscs Int Default 0,
		cnsctvo_cdgo_tpo_cbrnza Int Default 0,
		fcha_fn_vgnca DATETIME,
		fcha_inco_vgnca DATETIME,
		nmro_unco_idntfccn_aprtnt Int,
		rzn_scl CHAR(35) Default ''
	)

	Create Table #tempInformacionEmpresasxAfiliado
	(
		cnsctvo_cdgo_tpo_idntfccn_empldr			UdtConsecutivo,
		nmro_unco_idntfccn_empldr					Int,
		nmro_idntfccn_empldr						udtNumeroIdentificacionLargo,
		nmbre_empldr								Varchar(200),
		cnsctvo_scrsl								UdtConsecutivo,
		cnsctvo_cdgo_ofcna							UdtConsecutivo,
		inco_vgnca_cbrnza							Date,
		fn_vgnca_cbrnza								Date,
		slro_bse									Int,
		cnsctvo_cdgo_tpo_cbrnza						UdtConsecutivo,
		cnsctvo_cdgo_entdd_arp						UdtConsecutivo,
		fcha_ultma_mdfccn							Date,
		usro_ultma_mdfccn							UdtUsuario,
		nuam										Numeric(18, 0),		
		cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	UdtConsecutivo					
	)

	Create Table #tmpinformacionservicios 
	(
		id Int IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco Int,
		cnsctvo_srvco_slctdo Int,
		fcha_imprsn DATETIME,
		cnsctvo_cdgo_srvco_slctdo Int,
		cntdd_slctda Int,
		cnsctvo_cdgo_ltrldd Int Default 0,
		fcha_ultma_mdfccn DATETIME,
		cnsctvo_cdgo_va_accso Int Default 0,
		usro_ultma_mdfccn udtusuario,
		cnsctvo_cdgo_prsntcn Int,
		prstcn_cptda udtlogico,
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto Int,
		cnsctvo_cdgo_tpo_srvco Int,
		cnsctvo_mdcmnto_slctdo Int,
		cnsctvo_prcdmnto_slctdo Int,
		vlr_rfrnca DECIMAL(10, 0) Default 0,
		cpgo_lqudcn DECIMAL(8, 0) Default 0,
		cnsctvo_cdgo_estdo_mega Int,
		cnsctvo_cdgo_estdo Int,
		cnsctvo_cdgo_ntfccn Int,
		cnsctvo_cdgo_ofcna Int,
		nuam Int,
		nmro_acta Int,
		cnsctvo_prgrmcn_prstcn Int,
		cnsctvo_prgrmcn_fcha_evnto Int,
		cnsctvo_det_prgrmcn_fcha	Int,
		rqre_prvdr CHAR(1),
		cnsctvo_cdgo_agrpdr_prstcn Int,
		cnsctvo_cdgo_rcbro Int,
		id_agrpdr Int
	)

	Create Table  #tmpinformacionconceptos
	(
		id                         udtConsecutivo IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
		cnsctvo_srvco_slctdo       udtConsecutivo,
		cnsctvo_ops                Int,
		cnsctvo_cdgo_cncpto_gsto   udtConsecutivo,
		fcha_ultma_mdfccn          Datetime,
		cnsctvo_cdgo_grpo_imprsn   udtConsecutivo,
		nmro_rdccn_cm              Int,
		cnsctvo_cdgo_ofcna_cm      udtConsecutivo,
		nmro_unco_ops              Int,
		vlr_cncpto_cm              Float,
		gnrdo                      udtlogico,
		fcha_utlzcn_dsde           Datetime,
		fcha_utlzcn_hsta           Datetime,
		cnsctvo_cdgo_estdo         udtConsecutivo,
		cnsctvo_cdgo_estdo_mega    udtConsecutivo,
		vlr_lqdcn                  Decimal(18,0) Default 0,
		nmro_unco_idntfccn_prstdr  Int,									-- qvisionclr 2017/01/18
		cdgo_intrno                Char(8)
	 )

	 -- 
	 Create 
	 Table #tmpsolicitudes( cnsctvo_slctd_atrzcn_srvco   udtConsecutivo,
							cnsctvo_srvco_slctdo		 udtConsecutivo
						  );

	Set @vlr_no = 'N';
	Set @usro = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user));
	
	-- Se llena tabla con el consecutivo de la solicitud a migrar
	Insert Into #tmpsolicitudes(cnsctvo_slctd_atrzcn_srvco) Values(@cnsctvo_slctd_atrzcn_srvco);
	
	Begin Try
		
		Select		@cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		From		bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
		Where		ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
		
		-- Carga la informacion de la solicitud
		Exec gsa.spasobtenerinformacionintegracionsipresmega Null;

		-- Calcula el consecutivo de oficina y el nuam
		Exec gsa.spasobtenerinformacionintegracionsipresotros;

		-- Carga la Informacion en Sipres
		Exec gsa.spascargarinformacionsipres @vlr_no;

		-- Migra las cuotas de recuperacion, si tiene
		Exec bdCNA.gsa.spAsMigracionCuotasDeRecuperacionSipres;

		-- Migra las Marcas del servicio.
		Exec BdCna.gsa.spASMigrarMarcasSipres;

		-- Migra la informacion del medico
		Exec bdCNA.gsa.spAsMigracionMedicoGenericoSipres;

	End Try
	Begin Catch
		
		Set @mensaje = Concat(	'Message: ', ERROR_MESSAGE(),
								' Procedure: ', ERROR_PROCEDURE(),
								' Line: ', ERROR_LINE());

		Exec bdCNA.gsa.spASGrabarInconsistenciasMigracionxSolicitud @cnsctvo_slctd_atrzcn_srvco, @cnsctvo_srvco_slctdo, @mensaje, @usro;
		
	End Catch
		
   
End
