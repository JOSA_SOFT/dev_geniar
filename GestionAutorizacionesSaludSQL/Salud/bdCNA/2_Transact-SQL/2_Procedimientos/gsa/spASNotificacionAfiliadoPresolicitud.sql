USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASNotificacionAfiliadoPresolicitud]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASNotificacionAfiliadoPresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASNotificacionAfiliadoPresolicitud] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASNotificacionAfiliadoPresolicitud
* Desarrollado por		: <\A Jorge Rodriguez  - SETI SAS  					    A\>
* Descripcion			: <\D Orquesta los procedimientos encargados de
							  obtener y procesar el envío de correo a los
							  afiliados	cuando la presolicitud es devuelta		D\>
						  <\D 													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													 \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/14									   FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM Jorge Rodriguez  - SETI SAS	AM\>
* Descripcion			 : <\DM Se adicionan campos en las temporales para utilizarlos
								en la generación de las plantillas			DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2016-01-06	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASNotificacionAfiliadoPresolicitud]
	@slctdes		XML,
	@usrio			VARCHAR(50),
	@estdo_ejccn	VARCHAR(2) OUTPUT,
	@msje_rspsta	NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@cnstrccn_trma_xml	XML,@codigoExito		CHAR(2),
			@codigoError		CHAR(2),
			@valorCero			INT;

	DECLARE @rspsta_xml			TABLE (rspsta XML NOT NULL);	

	--Temporal con los datos básicos de la solicitud
	CREATE TABLE #tmpSlctdes(id_sld_rcbda INT IDENTITY (1, 1)
							 ,cnsctvo_slctd_srvco_sld_rcbda INT			NULL
							 ,nmro_slctd					VARCHAR(16) NULL
							 ,plntlla						INT			NULL)

	--Temporal con los datos necesario del afiliado
	CREATE TABLE #tmpAflds	(id INT IDENTITY (1, 1)
							 ,id_sld_rcbda						udtConsecutivo					NULL
							 ,nmbre_afldo						VARCHAR(100)					NULL
							 ,cnsctvo_slctd_srvco_sld_rcbda		udtConsecutivo					NULL
							 ,nmro_slctd_atrzcn_ss				varchar(16)						NULL
							 ,cnsctvo_cdgo_tpo_idntfccn_afldo   udtConsecutivo					NULL
							 ,nmro_idntfccn_afldo				udtNumeroIdentificacionLargo	NULL
							 ,cnsctvo_cdgo_tpo_cntrto			udtConsecutivo					NULL
							 ,nmro_cntrto						udtNumeroFormulario				NULL
							 ,cnsctvo_cdgo_pln					udtConsecutivo					NULL
							 ,nmro_unco_idntfccn_afldo			udtConsecutivo					NULL
							 ,cnsctvo_cdgo_rngo_slrl			udtConsecutivo					NULL
							 ,cdgo_ips_prmra					udtCodigoIps					NULL			
							 ,cnsctvo_bnfcro_cntrto				udtConsecutivo					NULL
							 ,cnsctvo_cdgo_sde_ips_prmra		udtConsecutivo					NULL
							 ,cnsctvo_cdgo_estdo_drcho			udtConsecutivo					NULL
							 ,dscrpcn_csa_drcho					VARCHAR(150)					NULL
							 ,cdgo_tpo_idntfccn_afldo			udtCodigo						NULL
							 ,dscrpcn_pln						VARCHAR(150)					NULL
							 ,eml								udtEmail						NULL
							 ,sde_afldo							udtCodigoIps					NULL
							 ,dscrpcn_sde						udtDescripcion					NULL
							 ,bcc								udtEmail						NULL
							 ,lnea_ncnal						udtDescripcion					NULL
							 ,crreo_atncn_clnte					udtEmail						NULL)
							 

	--Temporal con los datos necesario del proveedor
	CREATE TABLE #tmpPrvdor(id								INT IDENTITY (1, 1)	
							,id_sld_rcbda					INT						NULL
							,crreo							udtEmail				NULL)
	
	
	
		SELECT @codigoExito = 'OK',
			   @codigoError = 'ET',
			   @valorCero	=  0;

		/*Poblar tabla temporal Solicitudes*/
		INSERT #tmpSlctdes(
			cnsctvo_slctd_srvco_sld_rcbda,
			nmro_slctd,
			plntlla
		)
		SELECT	ISNULL(CAST(dta.query('data(cnsctvo_slctd_srvco_sld)') AS VARCHAR(150)), @valorCero),
				CAST(dta.query('data(nmro_slctd)') AS VARCHAR(16)),
				ISNULL(CAST(dta.query('data(plntlla)') AS VARCHAR(10)), @valorCero)
		From @slctdes.nodes('info_slctds/cdgos_slctds') AS slctdes(dta);		

		--Se actualiza solo si no se envía el consecutivo de la solicitud, pero si se
		--envía el número de radicación SOS
		UPDATE TSO
		SET cnsctvo_slctd_srvco_sld_rcbda = SAS.cnsctvo_slctd_atrzcn_srvco
		FROM #tmpSlctdes TSO 
		LEFT JOIN gsa.tbASSolicitudesAutorizacionServicios SAS WITH (NOLOCK)
		ON (SAS.nmro_slctd_atrzcn_ss = TSO.nmro_slctd)
		WHERE TSO.nmro_slctd IS NOT NULL
		AND TSO.cnsctvo_slctd_srvco_sld_rcbda = @valorCero;		
	
		BEGIN TRY
			
			/*Obtener y cargar en la tabla temporal la información del afiliado*/
			EXEC gsa.spASObtenerDatosNotificacionAfiliadoPresolicitud;

			/*Construir la trama XML que será enviada al sp encargado de guardar la norificación
			  del afiliado*/
			EXEC gsa.spASConstruirTramaXMLNotificacionAfiliado @usrio, @cnstrccn_trma_xml OUTPUT;

			INSERT INTO @rspsta_xml
			EXEC bdProcesosSalud.dbo.spEMGuardarMensajesCorreoMasivo @cnstrccn_trma_xml;					
				
			SELECT @msje_rspsta=CAST(rspsta AS NVARCHAR(MAX)) FROM @rspsta_xml;	
			SET @estdo_ejccn = @codigoExito;

		END TRY
		BEGIN CATCH

			SET @msje_rspsta =	Concat(
											'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
											'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
											'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
											'Procedure:' , ERROR_PROCEDURE()
										);
			SET @estdo_ejccn = @codigoError;
			
		END CATCH

		DROP TABLE #tmpAflds;
		DROP TABLE #tmpPrvdor;
		DROP TABLE #tmpSlctdes;
	
END
GO
