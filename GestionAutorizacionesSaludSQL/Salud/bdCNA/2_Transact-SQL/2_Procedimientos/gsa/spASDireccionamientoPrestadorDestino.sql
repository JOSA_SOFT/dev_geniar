USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASDireccionamientoPrestadorDestino]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASDireccionamientoPrestadorDestino
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite la orquestación de la ejecución del servicio de D\>
					   <\D Direccionamiento												 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASDireccionamientoPrestadorDestino] @usro varchar(50)
, @es_btch char(1)
, @estdo_ejccn varchar(5) OUTPUT
, @msje_errr varchar(2000) OUTPUT
, @msje_rspsta xml OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  
  DECLARE @negacion char(1) = 'N',
          @codigoExito char(2) = 'OK',
          @codigoError char(2) = 'ET',
          @errorSolicitud varchar(100) = 'Una o más solicitudes no existen.',
          @cnsctvo_cdgo_tpo_prcso_lna udtConsecutivo = 21, -- Tipo proceso para procesamiento en Línea de Direccionamiento
		  @cnsctvo_cdgo_tpo_prcso_btch udtConsecutivo = 20, -- Tipo proceso para procesamiento en Batch de Direccionamiento
          @cnsctvo_prcso udtConsecutivo,
          @estdo_prcso_ok numeric(1) = 2,
		  @mensajeInicio varchar(100) = 'Inicia Proceso de Direccionamiento En Línea.',
		  @mensajeInicioBatch varchar(100)= 'Inicia Proceso de Direccionamiento En Batch.',
		  @mensajeInicioBatchReglasNegocio varchar(100)= 'Ejecución Reglas Direccionamiento en Batch.',
		  @mensajeInicioBatchCalculoResultado varchar(100)= 'Ejecución Calcular Resultado Direccionamiento en Batch.',
		  @mensajeInicioBatchGuardarResultado varchar(100)= 'Ejecución Guardar Resultado Direccionamiento en Batch.',
		  @mensajeInicioBatchRetornarResultado varchar(100)= 'Ejecución Retornar Resultado Direccionamiento en Batch.',
		  @procesoInicioDireccionamientoLinea int =65,
		  @procesoInicioDireccionamientoBatch int =66,
		  @procesoInicioBatchReglasNegocio int= 68,
		  @procesoInicioBatchCalculoResultado int= 69,
		  @procesoInicioBatchGuardarResultado int= 70,
		  @procesoInicioBatchRetornarResultado int= 71,
		  @cnsctvo_lg int,
		  @prcso_nlo varchar(1) = null, --Para el proceso Online no se ejecutará el sp de registra proceso
		  @vlrCero int = 0;

  IF NOT EXISTS (SELECT
      id
    FROM #tmpNmroSlctds so
    WHERE so.cnsctvo_slctd_srvco_sld_rcbda IS NULL)
  BEGIN
    IF @es_btch = @negacion
	BEGIN
    BEGIN TRY
      --Ejecución Direccionamiento en línea.
	  
	  IF EXISTS (SELECT 1 FROM #tmpInformacionSolicitudPrestacion ISP WHERE ISP.mrca_msmo_prstdor=@vlrCero)
	  BEGIN
		  --Se ejecutan las reglas de validación.
		  EXEC bdCNA.gsa.spASEjecutarReglasDireccionamiento @prcso_nlo,
															@cnsctvo_cdgo_tpo_prcso_lna,
															@es_btch
	  END

      --Se calcula el resultado de del direccionamiento.
      EXEC bdCNA.gsa.spASCalcularResultadoDireccionamiento @es_btch

      --Se Guarda el resultado de la ejecución de las reglas.
      EXEC bdCNA.gsa.spASGuardarResultadoDireccionamiento @es_btch

	  --Actualizar información de suficiencia
	  EXEC bdCNA.[gsa].[spASActualizarSuficienciaDireccionamiento] @es_btch

      --Se retorna el resultado de la ejecución de Direccionamiento.
      EXEC bdCNA.gsa.spASRetornarResultadoDireccionamiento @es_btch,
                                                           @msje_rspsta OUTPUT

      SET @estdo_ejccn = @codigoExito

    END TRY
    BEGIN CATCH
	  SET @msje_errr =
      'Number:' + CAST(ERROR_NUMBER() AS char) + CHAR(13) +
      'Line:' + CAST(ERROR_LINE() AS char) + CHAR(13) +
      'Message:' + ERROR_MESSAGE() + CHAR(13) +
      'Procedure:' + ERROR_PROCEDURE();

      SET @estdo_ejccn = @codigoError;
      SET @msje_rspsta = NULL
    END CATCH
	END
	ELSE
	BEGIN

	  /*PASO 1  Crear Proceso */
      BEGIN TRY
        EXEC bdProcesosSalud.dbo.spPRORegistraProceso @cnsctvo_cdgo_tpo_prcso_btch,
                                                 @usro,
                                                 @cnsctvo_prcso OUTPUT
        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso_btch,
                                                          @mensajeInicioBatch,
                                                          @procesoInicioDireccionamientoBatch,
                                                          @cnsctvo_lg OUTPUT
        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
		RAISERROR (@msje_errr, 16, 2) WITH SETERROR
      END CATCH

	   /*PASO 3  Proceso Ejecutar relgas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso_btch,
                                                          @mensajeInicioBatchReglasNegocio,
                                                          @procesoInicioBatchReglasNegocio,
                                                          @cnsctvo_lg OUTPUT

		IF EXISTS (SELECT 1 FROM #tmpInformacionSolicitudPrestacion ISP WHERE ISP.mrca_msmo_prstdor=@vlrCero)
		BEGIN
			--Se ejecutan las reglas de validación.
			EXEC bdCNA.gsa.spASEjecutarReglasDireccionamiento	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso_btch,
																@es_btch

			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
		END

      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
        RAISERROR (@msje_errr, 16, 2) WITH SETERROR
	  END CATCH

	  /*PASO 4  Proceso Calcular Resultado relgas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso_btch,
                                                          @mensajeInicioBatchCalculoResultado,
                                                          @procesoInicioBatchCalculoResultado,
                                                          @cnsctvo_lg OUTPUT
        
        --Se calcula el resultado de del direccionamiento.
        EXEC bdCNA.gsa.spASCalcularResultadoDireccionamiento @es_btch

        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
        RAISERROR (@msje_errr, 16, 2) WITH SETERROR
	  END CATCH


	  /*PASO 5  Proceso Guardar Resultado relgas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso_btch,
                                                          @mensajeInicioBatchGuardarResultado,
                                                          @procesoInicioBatchGuardarResultado,
                                                          @cnsctvo_lg OUTPUT
        
		  --Se Guarda el resultado de la ejecución de las reglas.
		  EXEC bdCNA.gsa.spASGuardarResultadoDireccionamiento @es_btch

        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
        RAISERROR (@msje_errr, 16, 2) WITH SETERROR
	  END CATCH

      
	  /*PASO 6  Proceso Retornar Resultado relgas */
      BEGIN TRY

        EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso,
                                                          @cnsctvo_cdgo_tpo_prcso_btch,
                                                          @mensajeInicioBatchRetornarResultado,
                                                          @procesoInicioBatchRetornarResultado,
                                                          @cnsctvo_lg OUTPUT
        
		--Se retorna el resultado de la ejecución de Direccionamiento.
        EXEC bdCNA.gsa.spASRetornarResultadoDireccionamiento @es_btch,
                                                             @msje_rspsta OUTPUT

        EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg

      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
        RAISERROR (@msje_errr, 16, 2) WITH SETERROR
	  END CATCH


	  /* Paso 8 actualizar procesamiento exitoso*/
	  BEGIN TRY
        --Actualizar estado de ejecución de Direccionamiento.
      EXEC bdProcesosSalud.[dbo].[spActualizaEstadoProceso] @cnsctvo_cdgo_tpo_prcso_btch,
                                                       @estdo_prcso_ok
      END TRY
      BEGIN CATCH
        SET @msje_errr = ERROR_MESSAGE();
        EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaNoControlada @cnsctvo_lg,
                                                                     @msje_errr
		RAISERROR (@msje_errr, 16, 2) WITH SETERROR
      END CATCH

      SET @estdo_ejccn = @codigoExito
	END
  END
  ELSE
  BEGIN
    RAISERROR (@errorSolicitud, 16, 2)
  END

END

GO
