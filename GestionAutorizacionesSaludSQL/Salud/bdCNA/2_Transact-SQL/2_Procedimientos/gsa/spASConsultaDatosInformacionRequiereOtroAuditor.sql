USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionRequiereOtroAuditor]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionRequiereOtroAuditor
* Desarrollado por		: <\A Jonathan - SETI SAS  							    A\>
* Descripcion			: <\D Se actualiza el histórico del servicio solicitado
							  con base en la actualización del campo
							  cnsctvo_cdgo_estdo_srvco_slctdo   				D\>
						  <\D 													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													 \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/04/25									   FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM													AM\>
* Descripcion			 : <\DM													DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionRequiereOtroAuditor]
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@tpo_adtra						udtConsecutivo

AS
BEGIN
	SET NOCOUNT ON;
		
		Declare @sde_afldo					udtConsecutivo,
				@sde_vca					udtConsecutivo,
				@cntrtcn					udtDescripcion = 'Inconsistencia Prestador',
				@mdcna						udtDescripcion = 'Medicina del trabajo',
				@dmi						udtDescripcion = 'DOMI',
				@adtra						udtDescripcion = 'Auditoria medico por riesgo',
				@adtr_excldo				udtDescripcion,
				@cnsctvo_cdgo_chrte			udtConsecutivo = 99;

				


		if (@tpo_adtra = 1 )
			Set @adtr_excldo = @cntrtcn
		else if(@tpo_adtra = 2)
			Set @adtr_excldo = @mdcna
		else if(@tpo_adtra = 3)
			Set @adtr_excldo = @dmi
		else
			Set @adtr_excldo = @adtra
	
		Set @sde_vca = 0;
		Set @sde_afldo = (Select IAS.cnsctvo_cdgo_sde_ips_prmra From BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios IAS With(NoLock)
		Where IAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco);
		
		
		SELECT ROW_NUMBER() OVER(ORDER BY x.dscrpcn_grpo_adtr_prcso DESC) cnsctvo_cdgo_tpo_adtr_rsgo_afldo, x.dscrpcn_grpo_adtr_prcso from
		(		
		Select  distinct dscrpcn_grpo_adtr_prcso dscrpcn_grpo_adtr_prcso
		from BDCna.gsa.tbASTipoAuditorxRiesgoAfiliado With(NoLock)
		Where (cnsctvo_cdgo_sde = @sde_afldo or cnsctvo_cdgo_sde = @sde_vca)
		And dscrpcn_grpo_adtr_prcso is not null
		And cnsctvo_cdgo_chrte <> @cnsctvo_cdgo_chrte) x
		Where x.dscrpcn_grpo_adtr_prcso <> @adtr_excldo
		
END

GO
