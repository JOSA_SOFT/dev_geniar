USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionEmpleador]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosConsecutivosEmpleador
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionEmpleador]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @cnsctvo_cdgo_tpo_idntfccn 	udtConsecutivo, 
			@nmro_idntfccn				udtNumeroIdentificacionLargo,
			@cnsctvo_cdgo_pln 			udtConsecutivo,
			@nmro_unco_idntfccn_afldo	udtConsecutivo,
			@cnsctvo_cdgo_tpo_cntrto	udtConsecutivo,	
			@nmro_cntrto				udtConsecutivo,
			@fecha_validacion			datetime;

	CREATE TABLE #Empleadores(
		idntfccn_cmplta_empldr		varchar(30),
		rzn_scl						varchar(200),
		nmro_idntfccn				varchar(23),
		cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
		nmro_unco_idntfccn_aprtnte	udtConsecutivo,
		cdgo_tpo_idntfccn			varchar(3),
		prncpl 						char,
		cnsctvo_scrsl_ctznte		udtConsecutivo,
		cnsctvo_cdgo_crgo_empldo	udtConsecutivo,
		cnsctvo_cdgo_clse_aprtnte	udtConsecutivo,
		cnsctvo_cdgo_tpo_ctznte		udtConsecutivo,
		inco_vgnca_cbrnza			datetime,
		fn_vgnca_cbrnza				datetime,
		cnsctvo_cdgo_tpo_cbrnza		udtConsecutivo,
		cnsctvo_cbrnza				udtConsecutivo,	
		slro_bsco					udtConsecutivo,
		cnsctvo_cdgo_tpo_cntrto		udtConsecutivo,
		nmro_cntrto					varchar(15),
		cnsctvo_prdcto_scrsl		udtConsecutivo,
		cnsctvo_scrsl				udtConsecutivo,
		cnsctvo_cdgo_actvdd_ecnmca	udtConsecutivo,
		cnsctvo_cdgo_arp			udtConsecutivo,
		drccn						udtDireccion,
		tlfno						udtTelefono,
		cdgo_actvdd_ecnmca			char(4),
		dscrpcn_actvdd_ecnmca		udtDescripcion,
		cdgo_crgo					char(4),
		dscrpcn_crgo				udtDescripcion,
		cdgo_entdd					char(8),
		dscrpcn_entdd				udtDescripcion,
		cdgo_tpo_ctznte				char(2),
		dscrpcn_tpo_ctznte			udtDescripcion,
		nmbre_cntcto				udtNombre,
		tlfno_cntcto				udtTelefono,
		eml_cntcto					udtEmail,
		cnsctvo_cdgo_cdd			udtconsecutivo,
		cdgo_cdd					char(8),
		dscrpcn_cdd					udtDescripcion,
		cnsctvo_cdgo_dprtmnto		udtConsecutivo,
		cdgo_dprtmnto				char(3),
		dscrpcn_dprtmnto			udtDescripcion,
		eml							udtEmail,
		cdgo_cnvno_cmrcl			udtConsecutivo,
		dscrpcn_cnvno_cmrcl			udtDescripcion,
		cta_mdrdra					int default 0
	);
	

			
	Set @fecha_validacion = getDate();

	SELECT  @cnsctvo_cdgo_tpo_idntfccn	= ias.cnsctvo_cdgo_tpo_idntfccn_afldo, 
			@nmro_idntfccn				= ias.nmro_idntfccn_afldo, 
			@cnsctvo_cdgo_pln			= ias.cnsctvo_cdgo_pln
	FROM gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK)
	WHERE ias.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	

	IF @cnsctvo_cdgo_tpo_idntfccn IS NOT NULL 
	BEGIN
		Set @nmro_unco_idntfccn_afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn);
	END;


	SELECT  
			@cnsctvo_cdgo_tpo_cntrto	= b.cnsctvo_cdgo_tpo_cntrto,	
			@nmro_cntrto				= b.nmro_cntrto 
	FROM   BDAfiliacionValidador.dbo.tbBeneficiariosValidador	 b With(NoLock) 
	INNER JOIN	BDAfiliacionValidador.dbo.tbContratosValidador c	With(NoLock)					On		b.nmro_cntrto					= c.nmro_cntrto
																			And		b.cnsctvo_cdgo_tpo_cntrto		= c.cnsctvo_cdgo_tpo_cntrto
	Inner Join	BDAfiliacionValidador.dbo.tbVigenciasBeneficiariosValidador	v	With(NoLock)	On		v.cnsctvo_cdgo_tpo_cntrto		=	b.cnsctvo_cdgo_tpo_cntrto
																				And	v.nmro_cntrto					=	b.nmro_cntrto
																				And	v.cnsctvo_bnfcro				=	b.cnsctvo_bnfcro
	WHERE b.nmro_unco_idntfccn_afldo	= @nmro_unco_idntfccn_afldo
	And	c.cnsctvo_cdgo_pln			= @cnsctvo_cdgo_pln
	And	@fecha_validacion Between v.inco_vgnca_estdo_bnfcro And v.fn_vgnca_estdo_bnfcro


	INSERT INTO #Empleadores(
		cdgo_tpo_idntfccn,
		nmro_idntfccn,
		rzn_scl,
		idntfccn_cmplta_empldr,
		cnsctvo_cdgo_tpo_idntfccn,
		nmro_unco_idntfccn_aprtnte,
		prncpl,
		cnsctvo_scrsl_ctznte,
		cnsctvo_cdgo_crgo_empldo,
		cnsctvo_cdgo_clse_aprtnte,
		cnsctvo_cdgo_tpo_ctznte,
		inco_vgnca_cbrnza,
		fn_vgnca_cbrnza,
		cnsctvo_cdgo_tpo_cbrnza,
		cnsctvo_cbrnza,
		slro_bsco,
		cnsctvo_cdgo_tpo_cntrto,
		nmro_cntrto,
		cnsctvo_prdcto_scrsl,
		cnsctvo_scrsl,
		cnsctvo_cdgo_actvdd_ecnmca,
		cnsctvo_cdgo_arp,
		drccn,
		tlfno,
		cdgo_actvdd_ecnmca,
		dscrpcn_actvdd_ecnmca,
		cdgo_crgo,
		dscrpcn_crgo,
		cdgo_entdd,
		dscrpcn_entdd,
		cdgo_tpo_ctznte,
		dscrpcn_tpo_ctznte,
		nmbre_cntcto,
		tlfno_cntcto,
		eml_cntcto,
		cnsctvo_cdgo_cdd,
		cdgo_cdd,
		dscrpcn_cdd,
		cnsctvo_cdgo_dprtmnto,
		cdgo_dprtmnto,
		dscrpcn_dprtmnto,
		eml,
		cdgo_cnvno_cmrcl,
		dscrpcn_cnvno_cmrcl
			
	)
	EXEC BDAfiliacionValidador.dbo.spPmConsultaDetEmpleadoresAfiliado_alDia @cnsctvo_cdgo_tpo_cntrto, 
																			@nmro_cntrto,
																			@fecha_validacion,
																			NULL,
																			NULL,
																			NULL,
																			NULL,
																			NULL,
																			NULL

	select 
		cdgo_tpo_idntfccn,
		nmro_idntfccn,
		rzn_scl,
		idntfccn_cmplta_empldr,
		cnsctvo_cdgo_tpo_idntfccn,
		nmro_unco_idntfccn_aprtnte,
		cnsctvo_cdgo_arp,
		dscrpcn_entdd,
		cta_mdrdra
			
	from #Empleadores;

	DROP TABLE #Empleadores
END

GO
