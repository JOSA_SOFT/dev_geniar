USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarCreacionEmpresaAfiliadoSolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarCreacionEmpresaAfiliadoSolicitud												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Controlador Para ejecución Masiva ó por demanda de la Homologación de Prestaciones
						  del prestador																				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/29																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM Ing. Victor Hugo Gil Ramos AM\>  													
* Descripcion			: <\DM 
                               Se actualiza el procedimiento cambiando el llamado sp que crea el proceso 
							   spPRORegistraLogEventoProceso por spRegistraLogEventoProceso 
                          DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2016/12/16 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 

/* 
Declare @mnsjertrno					 UdtDescripcion,
		@cdgortrno					 varchar(2)
exec bdcna.gsa.spASEjecutarCreacionEmpresaAfiliadoSolicitud null, null, @mnsjertrno output, @cdgortrno output
--exec bdcna.gsa.spASEjecutarCreacionEmpresaAfiliadoSolicitud 321, 'prueba1', @mnsjertrno output, @cdgortrno output

Select @cdgortrno, @mnsjertrno


*/

ALTER PROCEDURE [gsa].[spASEjecutarCreacionEmpresaAfiliadoSolicitud]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null,
/*02*/  @lcUsrioAplcn				 UdtUsuario = null,
/*03*/  @mnsjertrno					 UdtDescripcion = null output,
/*04*/  @cdgortrno					 varchar(2) = null output

AS

Begin
	SET NOCOUNT ON

	Declare @lcUsuario				         UdtUsuario     = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@tpo_prcso				         Int            = 31 , -- 'Proceso Creación Empresa Afiliado Solicitud - MEGA'
			@cnsctvo_prcso			         Int            = 0  ,
			@cnsctvo_cdgo_tpo_incnsstnca     Int            = 1  ,
			@prcso_extso			         Int            = 2  ,
			@tpo_rgstro_log_105		         Int            = 105,
			@tpo_rgstro_log_106		         Int            = 106,
			@tpo_rgstro_log_107		         Int            = 107,
			@mensajeError			         Varchar(2000)  = '' ,
			@cnsctvo_lg				         Int            = 0  ,
			@cnsctvo_estdo_err		         udtConsecutivo = 3  ,
			@mnsje_lg_evnto_prcso	         Varchar(500)        ,
			@estdo_en_prcso                  Int            = 1  ,
			@cnsctvo_rgstro_lg_evnto_x_prcso udtConsecutivo

	If @lcUsrioAplcn is not null
	   Begin
	     Set @lcUsuario = @lcUsrioAplcn
	   End 
	
	-- Validamos si existen solicitudes en estado '9-Liquidada' para procesar la empresa del afiliado
	if not exists ( select	1 
					from		gsa.tbASSolicitudesAutorizacionServicios sa with(nolock)
					Inner Join  gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ia with(nolock)
					On			ia.cnsctvo_slctd_atrzcn_srvco = sa.cnsctvo_slctd_atrzcn_srvco
					Left Join	gsa.tbAsInformacionEmpresasxAfiliado iea with(nolock)
					On			iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = ia.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
					Where		iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco is  null)
		Begin
			Set @mnsjertrno = 'No hay Datos Para Crear Empresa Afiliado Se Cancela El Proceso'
			Set @cdgortrno = 'OK'
			Return
		End

	-- Creamos tabla temporal para las solicitudes del afiliado pendientes de crearles la empresa 
	Create table #TemptbAsInformacionEmpresasxAfiliado
	(
		id_tbla										int identity,
		cnsctvo_slctd_atrzcn_srvco 					UdtConsecutivo,
		cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	UdtConsecutivo,
		cnsctvo_cdgo_tpo_idntfccn_afldo				UdtConsecutivo default 0,
		nmro_idntfccn_afldo							udtNumeroIdentificacionLargo default '',
		nmro_unco_idntfccn_afldo					UdtConsecutivo,
		cnsctvo_cdgo_tpo_cntrto						UdtConsecutivo,
		nmro_cntrto									UdtNumeroFormulario,
		cnsctvo_bnfcro_cntrto						UdtConsecutivo,
		cnsctvo_cbrnza								UdtConsecutivo,
		cnsctvo_cdgo_clse_aprtnte					UdtConsecutivo,
		-- datos a insertar en la tabla empresa
		cnsctvo_cdgo_tpo_idntfccn_empldr			UdtConsecutivo default 0,
		nmro_unco_idntfccn_aprtnte					UdtConsecutivo default 0,
		nmro_idntfccn_empldr						UdtNumeroIdentificacionLargo default '',
		nmbre_empldr								Varchar(200) default '',
		cnsctvo_scrsl								UdtConsecutivo default 0,
		cnsctvo_cdgo_prdcto							UdtConsecutivo default 0,
		cnsctvo_cdgo_opcn_prncpl					UdtConsecutivo default 0,
		cnsctvo_cdgo_ofcna							UdtConsecutivo default 0,
		inco_vgnca_cbrnza							datetime,
		fn_vgnca_cbrnza								datetime,
		slro_bse									int,
		cnsctvo_cdgo_tpo_cbrnza						UdtConsecutivo default 0,
		cnsctvo_cdgo_entdd_arp						UdtConsecutivo default 0
	)

	if @cnsctvo_slctd_atrzcn_srvco is null -- si el proceso es masivo activamos el control de transacciones
		Begin
			Begin Try
					-- Registramos el inicio proceso automático log de procesos 'Proceso Creación Empresa Afiliado x Solicitud - MEGA'
					exec bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso,@lcUsuario, @cnsctvo_prcso output

					-- Paso 1. Poblar temporales 
					Begin Try
					    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 1
						set @mnsje_lg_evnto_prcso = 'Paso 1. Inserción a tablas TMP los Afiliados x Solicitud Para Creación de Empresas - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_105, @estdo_en_prcso, @cnsctvo_lg output
						exec gsa.spASPoblarTemporalAfiliadoSolicitudxCreacionEmpresa @cnsctvo_slctd_atrzcn_srvco
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch
					-- Paso 2. Recuperar Datos Empresa Afiliado x Solicitud
					Begin Try
	                    Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 2
						set @mnsje_lg_evnto_prcso = 'Paso 2. Recuperar Datos Empresa Afiliado x Solicitud - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_106, @estdo_en_prcso, @cnsctvo_lg output						
						exec gsa.spASRecuperarDatosEmpresaAfiliadoxSolicitud 
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch

					-- Paso 3. Grabar Empresa Afiliado x Solicitud
					Begin Try
				        Set  @cnsctvo_rgstro_lg_evnto_x_prcso = 3
						set @mnsje_lg_evnto_prcso = 'Paso 3. Grabar Empresa Afiliado x Solicitud - MEGA'
						exec bdProcesosSalud.dbo.spRegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso, @cnsctvo_rgstro_lg_evnto_x_prcso, @mnsje_lg_evnto_prcso, @tpo_rgstro_log_107, @estdo_en_prcso, @cnsctvo_lg output						
						exec gsa.spASGrabarEmpresaAfiliadoxsolicitud @lcUsrioAplcn
						exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
						-- Registramos el fin del proceso Automático log de procesos 'Proceso Creación Empresa Afiliado Solicitud - MEGA'
						exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@prcso_extso
						set @cdgortrno = 'OK'
						set @mnsjertrno = 'El Proceso de Creación Empresa Afiliado x Solicitud se realizo con Exito'
					End Try
					Begin Catch
						Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
						Exec bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
						Exec bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@cnsctvo_estdo_err
						RAISERROR (@mensajeError, 16, 2) With SETERROR 
					End Catch

			End Try
			Begin Catch
				Set @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
				Set @cdgortrno = 'ET'
				set @mnsjertrno = @mensajeError
				RAISERROR (@mensajeError, 16, 2) With SETERROR 
			End Catch

	End
	Else
		Begin
			-- Paso 1. Poblar temporales
				exec gsa.spASPoblarTemporalAfiliadoSolicitudxCreacionEmpresa @cnsctvo_slctd_atrzcn_srvco
			-- Paso 2. Recuperar Datos Empresa Afiliado x Solicitud
				exec gsa.spASRecuperarDatosEmpresaAfiliadoxSolicitud
			-- Paso 3. Grabar Empresa Afiliado x Solicitud
				exec gsa.spASGrabarEmpresaAfiliadoxsolicitud @lcUsrioAplcn
				set @cdgortrno = 'OK'
				set @mnsjertrno = 'El Proceso de Creación Empresa Afiliado x Solicitud se realizo con Exito'
			if @@error <> 0
				Begin
					Set @cdgortrno = 'ET'
					set @mnsjertrno = 'Ocurrio un Error en el Proceso de Creación Empresa Afiliado x Solicitud'
				End
		End
	Drop table #TemptbAsInformacionEmpresasxAfiliado
End

GO
