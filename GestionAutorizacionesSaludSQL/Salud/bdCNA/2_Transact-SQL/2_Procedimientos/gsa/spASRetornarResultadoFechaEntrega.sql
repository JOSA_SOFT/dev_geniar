USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRetornarResultadoFechaEntrega]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASRetornarResultadoFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite retornar el resultado de fecha de entrega  D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 29/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASRetornarResultadoFechaEntrega] @cnsctvo_prcso udtConsecutivo,
@es_btch char(1),
@msje_rspsta xml OUTPUT
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @negacion char(1)
  SET @negacion = 'N'

  IF @es_btch = @negacion
  BEGIN
    WITH temp
    AS
    --Se usa distinct para retornar sólo la información cabecera, sin prestaciones.
    (SELECT DISTINCT
      slctd.cnsctvo_slctd_srvco_sld_rcbda AS cnsctvo_slctd_srvco_sld_rcbda,
      ISNULL(slctd.nmro_slctd_ss, '') AS nmro_slctd_ss,
      ISNULL(slctd.nmro_slctd_prvdr, '') AS nmro_slctd_prvdr,
      ISNULL(slctd.cnsctvo_tpo_pln, '') AS cnsctvo_tpo_pln,
      ISNULL(slctd.cdgo_tpo_pln, '') AS cdgo_tpo_pln
    FROM #tmpDatosSolicitudFechaEntrega slctd)
    SELECT
      @msje_rspsta = (SELECT
        slctd.cnsctvo_slctd_srvco_sld_rcbda AS "id_slctd",
        slctd.nmro_slctd_ss AS "nmro_slctd_ss",
        slctd.nmro_slctd_prvdr AS "nmro_slctd_prvdr",
        slctd.cnsctvo_tpo_pln AS "cnsctvo_tpo_pln",
        slctd.cdgo_tpo_pln AS "tpo_pln",
        (SELECT
          prstcn.cnsctvo_cdfccn AS "cnsctvo_cdfccn",
          prstcn.cdgo_cdfccn AS "cdgo_cdfccn",
          prstcn.cnsctvo_grpo_entrga_fnl AS "cnsctvo_grpo_entrga",
          prstcn.cdgo_grpo_entrga_fnl AS "grpo_entrga",
          prstcn.fcha_slctd AS "fcha_prstcn",
          prstcn.fcha_estmda_entrga_fnl AS "fcha_estmda_entrga"
        FROM #tmpDatosSolicitudFechaEntrega AS prstcn
        WHERE prstcn.cnsctvo_slctd_srvco_sld_rcbda = slctd.cnsctvo_slctd_srvco_sld_rcbda
        FOR xml AUTO, TYPE, ELEMENTS)
      FROM temp AS slctd
      FOR xml AUTO, TYPE, ELEMENTS, ROOT ('rsltdo_fcha_entrga_vldcn'))
  END
END

GO
