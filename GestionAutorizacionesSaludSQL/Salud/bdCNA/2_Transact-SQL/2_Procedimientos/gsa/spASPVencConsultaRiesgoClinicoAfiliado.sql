USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPVencConsultaRiesgoClinicoAfiliado]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spCNAPVencConsultaRiesgoClinicoAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta el riesgo clinico del afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASPVencConsultaRiesgoClinicoAfiliado] 
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn udtNumeroIdentificacionLargo,
	@Nui_Afldo	UdtConsecutivo = null,
	@riesgo udtDescripcion output
AS
BEGIN
Set Nocount On

		Declare @riesgo_clinico		udtDescripcion,
				@ms_riesgo			udtDescripcion = 'RIESGO NO ASOCIADO',
				@fecha_validacion   datetime = getDate();
	
	CREATE TABLE #RIEGOS(
		descripcion		udtDescripcion,
		puntuacion		int
	)



	if @Nui_Afldo is null
	begin
		Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn)
	end
	
	INSERT INTO #RIEGOS 
	(	descripcion,	puntuacion )
	SELECT 
		c.dscrpcn_chrte,
		prc.pso_rsgo puntuacion 
	from BdRiesgosSalud.dbo.tbReporteCohortesxAfiliado rca with(nolock) 
	inner join BdRiesgosSalud.dbo.tbCohortes_vigencias c with(nolock) on rca.cnsctvo_cdgo_chrte = c.cnsctvo_cdgo_chrte
	inner join BDCna.gsa.tbASPesosRiesgoxCohorte prc with(nolock) on prc.cnsctvo_cdgo_chrte = c.cnsctvo_cdgo_chrte
	where rca.nmro_unco_idntfccn_afldo =  @Nui_Afldo
	And @fecha_validacion between c.inco_vgnca and c.fn_vgnca

	Set @riesgo_clinico = (SELECT TOP 1 r.descripcion from #RIEGOS r ORDER BY r.puntuacion DESC )
	
	if @riesgo_clinico is not null
		begin
			Set @riesgo = @riesgo_clinico
		end
	else
		begin
			Set @riesgo = @ms_riesgo
		end
	
	DROP TABLE #RIEGOS
END
GO
