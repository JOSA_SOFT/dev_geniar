USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASObtenerEventosPresolicitudes]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASObtenerEventosPresolicitudes]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASObtenerEventosPresolicitudes] AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASObtenerEventosPresolicitudes
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Se obtienen los eventos presolicitudes que están
							  disponibles para el procesamiento					D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/07/25										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM													AM\>
* Descripcion			 : <\DM													DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASObtenerEventosPresolicitudes]
AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @cnsctvo_cdgo_estdo_evnto	udtconsecutivo	= 1, --gsa.tbAsIntegracionEstadosEventos
			@cnsctvo_cdgo_prmtro_gnrl	INT	= 96,
			@vlr_prmtro_nmrco			INT,
			@fcha_actl					DATETIME = GETDATE(); 

	SELECT @vlr_prmtro_nmrco = vlr_prmtro_nmrco 
	FROM   bdSisalud.dbo.tbparametrosgenerales_vigencias 
	WHERE  cnsctvo_cdgo_prmtro_gnrl = @cnsctvo_cdgo_prmtro_gnrl
	AND	   @fcha_actl BETWEEN inco_vgnca AND fn_vgnca;
	
	SELECT	TOP (@vlr_prmtro_nmrco)
				IE.cnsctvo_evnto, 
				IE.cnsctvo_llve_objto,  
				IE.cnsctvo_cdgo_accn, 
				IE.nmbre_objto, 			
				IE.fcha_crcn, 
				IE.usro_crcn
	FROM		gsa.tbAsIntegracionEventos IE WITH (NOLOCK)
	INNER JOIN	prm.tbAsIntegracionEstadosEventos_Vigencias IEEV WITH (NOLOCK)
	ON			IE.cnsctvo_cdgo_estdo_evnto = IEEV.cnsctvo_cdgo_estdo_evnto
	INNER JOIN	prm.tbAsAccionIntegracionEventos_Vigencias AIEV WITH (NOLOCK)
	ON			IE.cnsctvo_cdgo_accn = AIEV.cnsctvo_cdgo_accn
	WHERE		IE.cnsctvo_cdgo_estdo_evnto = @cnsctvo_cdgo_estdo_evnto
	AND			@fcha_actl BETWEEN IEEV.inco_vgnca AND IEEV.fn_vgnca
	AND			@fcha_actl BETWEEN AIEV.inco_vgnca AND AIEV.fn_vgnca
	ORDER BY	cnsctvo_evnto ASC;

END;

GO
