USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASTramaXMLNotificacionAutorizacionSIAUDescargaPlantilla2]    Script Date: 08/03/2017 07:12:54 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASTramaXMLNotificacionAutorizacionSIAUDescargaPlantilla2
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Construcción trama xml para el envío de notificación de afiliados. Plantilla 2   D\>				  
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2016-12-29  FC\>    
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez -qvisionclr AM\>    
* Descripcion        : <\D 
                           Se agrega condicional para obtener solo las prestaciones autorizadas.
						   Se quita condicion en la consulta del prestador autorizado.
						   Se agrega tabla temporal para sacar el 1 pretacion por ops para asignar el valor. 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/02/06 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez -qvisionclr AM\>    
* Descripcion        : <\D 
                           Se agrega llenado de linea nacional en la plantilla xml. 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/02/14 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez - qvisionclr AM\>    
* Descripcion        : <\D 
                           Se agrega condicionales para los siguientes casos: 
						   1. Si la prestacion no tiene observaciones y tiene la marca de acceso directo, se pone 
						   el mensaje de la marca ventanilla
						   2. Si la prestacion no tiene observaciones y tiene marca de descarga IPS, e pone 
						   el mensaje de la marca ventanilla
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2017/03/08 FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------*/
ALTER Procedure [gsa].[spASTramaXMLNotificacionAutorizacionSIAUDescargaPlantilla2] 
	@usrio Varchar(50),
	@cnstrccn_trma_xml XML Output
As
Begin
	Set NoCount On;

	Declare	@anno								Varchar(4),
			@mes								Varchar(2),
			@dia								Varchar(2),
			@hra								Varchar(2),
			@mnto								Varchar(2),
			@sgndo								Varchar(2),
			@ldfcha_consulta					Date,
			@cnsctvo_cdgo_tpo_mrca_dscrga		udtDescripcion,
			@cnsctvo_cdgo_tpo_mrca_vntnlla		udtDescripcion,
			@cnsctvo_cdgo_tpo_mrca_accso_drcto	UdtCOnsecutivo,
			@cnsctvo_cdgo_tpo_mrca_dscrga_ips	UdtCOnsecutivo,
			@string_trama						Varchar(4000),
			@cnsctvo_cdgo_estdo_srvco_slctdo11	UdtConsecutivo,
			@obsrvcn							UdtObservacion;
			
	Create Table #tmpPrestacionesAprobadas(
		id								Int Identity(1, 1),
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		cnsctvo_srvco_slctdo			udtConsecutivo,
		cnsctvo_cdgo_srvco_slctdo		udtConsecutivo,
		dscrpcn_srvco_slctdo			udtDescripcion,
		cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo,
		cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
        cnsctvo_mdcmnto_slctdo			udtConsecutivo,
		nmro_unco_ops					udtConsecutivo,
		cdgo_intrno						udtCodigoIps,
		drccn_ips						udtDireccion,
		tlfno							udtTelefono,
		nmbre_scrsl						udtDescripcion default '',
		vlr_lqdcn_cta_rcprcn_srvco		Bigint default 0,
		obsrvcn							udtObservacion default '',
		cdgo_cdfccn						char(11),
	)

	Create Table #tempValoresXOps
	(
		id_rgtro_prncpl				Int,
		cnsctvo_slctd_atrzcn_srvco	UdtConsecutivo,
		nmro_unco_ops				UdtConsecutivo,
		vlr_ops						Bigint default 0
	)

	
	Set	@anno	= CONVERT(CHAR(4),DATEPART(YEAR,GETDATE()));
	Set	@mes	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(MONTH,GETDATE())))),2);
	Set	@dia	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(DAY,GETDATE())))),2);
	Set	@hra	= CONVERT(CHAR(2),DATEPART(HOUR,GETDATE()));
	Set	@mnto	= CONVERT(CHAR(2),DATEPART(MINUTE,GETDATE()));
	Set	@sgndo	= CONVERT(CHAR(2),DATEPART(SECOND,GETDATE()));
	Set	@ldfcha_consulta	= getDate();
	Set	@cnsctvo_cdgo_tpo_mrca_dscrga			= 2;
	Set	@cnsctvo_cdgo_tpo_mrca_vntnlla			= 6;
	Set @cnsctvo_cdgo_estdo_srvco_slctdo11 = 11;
	Set	@cnsctvo_cdgo_tpo_mrca_accso_drcto = 7;
	Set	@cnsctvo_cdgo_tpo_mrca_dscrga_ips = 5;

	Insert Into #tmpPrestacionesAprobadas(
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_srvco_slctdo,
		cnsctvo_cdgo_srvco_slctdo,
		cnsctvo_cdgo_estdo_srvco_slctdo,
		dscrpcn_srvco_slctdo
	)
	Select
				AFI.cnsctvo_slctd_srvco_sld_rcbda,
				SS.cnsctvo_srvco_slctdo,
				SS.cnsctvo_cdgo_srvco_slctdo,
				SS.cnsctvo_cdgo_estdo_srvco_slctdo,
				SS.dscrpcn_srvco_slctdo
	From		#tmpAflds AFI
	Inner Join	BDCna.gsa.tbASServiciosSolicitados SS With (NoLock)
	On			AFI.cnsctvo_slctd_srvco_sld_rcbda = SS.cnsctvo_slctd_atrzcn_srvco
	Where		SS.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo11
	

	Update		pa
	Set			cnsctvo_prcdmnto_insmo_slctdo = PIS.cnsctvo_prcdmnto_insmo_slctdo
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASProcedimientosInsumosSolicitados PIS With (NoLock)
	On			pa.cnsctvo_srvco_slctdo = PIS.cnsctvo_srvco_slctdo

	Update		pa
	Set			cnsctvo_mdcmnto_slctdo = MED.cnsctvo_mdcmnto_slctdo
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASMedicamentosSolicitados MED With (NoLock)
	On			pa.cnsctvo_srvco_slctdo = MED.cnsctvo_srvco_slctdo

	--Se obtiene el codigo de las prestaciones de codificaciones
	Update		pa
	Set			cdgo_cdfccn = CD.cdgo_cdfccn
	From		#tmpPrestacionesAprobadas pa
	Inner Join	bdSisalud.dbo.tbCodificaciones CD With (NoLock)
	On			pa.cnsctvo_cdgo_srvco_slctdo = CD.cnsctvo_cdfccn

	--Obtiene el codigo interno de la IPS donde se direccionó
	Update		pa
	Set			cdgo_intrno = RD.cdgo_intrno
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASResultadoDireccionamiento RD With(NoLock)
	On			pa.cnsctvo_srvco_slctdo = RD.cnsctvo_srvco_slctdo

	--Obtiene la dirección y el telefono de la IPS
	Update pa
	Set			drccn_ips	= dp.drccn,
				tlfno		= dp.tlfno,
				nmbre_scrsl	= dp.nmbre_scrsl	
	From		#tmpPrestacionesAprobadas pa
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock) 
	On			pa.cdgo_intrno = dp.cdgo_intrno
	Inner Join	bdSisalud.dbo.tbprestadores pr With(NoLock) 
	On			pr.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr
	Inner Join	BDAfiliacionValidador.dbo.tbTiposIdentificacion_Vigencias	ti 	With(NoLock) 
	On			ti.cnsctvo_cdgo_tpo_idntfccn = pr.cnsctvo_cdgo_tpo_idntfccn
	Where		@ldfcha_consulta between ti.inco_vgnca And ti.fn_vgnca
	And         @ldfcha_consulta Between dp.inco_vgnca And dp.fn_vgnca


	--Obtiene el consecutivo de los medicamentos y procedimientos
	Update		pa
	Set			nmro_unco_ops = css.nmro_unco_ops
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			pa.cnsctvo_prcdmnto_insmo_slctdo = css.cnsctvo_prcdmnto_insmo_slctdo 
	And			css.cnsctvo_prcdmnto_insmo_slctdo IS NOT NULL

	Update		pa
	Set			nmro_unco_ops = css.nmro_unco_ops
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			pa.cnsctvo_mdcmnto_slctdo = css.cnsctvo_mdcmnto_slctdo And css.cnsctvo_mdcmnto_slctdo IS NOT NULL
	
	-- Se el id de un registro agrupado por OPS, poner el valor 1 sola vez
	Insert Into #tempValoresXOps(cnsctvo_slctd_atrzcn_srvco, nmro_unco_ops,	id_rgtro_prncpl)
	Select 	  cnsctvo_slctd_atrzcn_srvco, nmro_unco_ops, Min(id)
	From      #tmpPrestacionesAprobadas
	Group By  cnsctvo_slctd_atrzcn_srvco,   nmro_unco_ops;

	--Se obtiene el valor de la OPS en 
	Update		pa
	Set			vlr_ops = ccr.vlr_lqdcn_cta_rcprcn_srvco_ops
	From		#tempValoresXOps pa
	Inner Join	BDCna.gsa.TbAsConsolidadoCuotaRecuperacionxOps ccr With(NoLock)
	On			pa.cnsctvo_slctd_atrzcn_srvco = ccr.cnsctvo_slctd_atrzcn_srvco 
	And			pa.nmro_unco_ops = ccr.nmro_unco_ops;
	
	-- Se asigna el valor de la ops a la tabla temporal principal.
	Update		pa
	Set			vlr_lqdcn_cta_rcprcn_srvco = vo.vlr_ops
	From		#tmpPrestacionesAprobadas pa
	Inner Join	#tempValoresXOps vo
	On			vo.id_rgtro_prncpl = pa.id;

	-- 
	Update		pa
	Set			obsrvcn = mssv.obsrvcns
	From		#tmpPrestacionesAprobadas pa
	Inner Join	BDCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
	On			pa.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
	Inner Join	BDCna.prm.tbASTipoMarcaServiciosSolicitados_vigencias mssv With(NoLock)
	On			mss.cnsctvo_cdgo_tpo_mrca = mssv.cnsctvo_cdgo_tpo_mrca
	And			mssv.cnsctvo_cdgo_tpo_mrca IN (@cnsctvo_cdgo_tpo_mrca_vntnlla, @cnsctvo_cdgo_tpo_mrca_dscrga)
	Where		@ldfcha_consulta between mssv.inco_vgnca And mssv.fn_vgnca;
		
	-- Si no se han cargado observaciones y tiene la marca de acceso directo se pone el mensaje de ventanilla
	If(
		Exists(
				Select		1
				From		#tmpPrestacionesAprobadas pa
				Inner Join	BDCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
				On			pa.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
				Where		mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_accso_drcto
				And			pa.obsrvcn = ''
			  )
	)
	Begin
		-- Se obtiene la observacion de la marca ventanilla
		Select		@obsrvcn = mssv.obsrvcns
		From		BDCna.prm.tbASTipoMarcaServiciosSolicitados_vigencias mssv With(NoLock)
		Where		mssv.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_vntnlla

		-- Se actualiza la observacion de la prestacion con marca acceso directo, con la observacion de la marca ventanilla.
		Update		pa
		Set			obsrvcn = @obsrvcn
		From		#tmpPrestacionesAprobadas pa
		Inner Join	BDCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
		On			pa.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
		Where		mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_accso_drcto
	End
	Else -- Si no se han cargado observaciones y tiene la marca de descarga IPS se pone el mensaje de ventanilla 
	If(
		Exists(
				Select		1
				From		#tmpPrestacionesAprobadas pa
				Inner Join	BDCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
				On			pa.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
				Where		mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_dscrga_ips
				And			pa.obsrvcn = ''
			  )
	)
	Begin
		-- Se obtiene la observacion de la marca ventanilla
		Select		@obsrvcn = mssv.obsrvcns
		From		BDCna.prm.tbASTipoMarcaServiciosSolicitados_vigencias mssv With(NoLock)
		Where		mssv.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_vntnlla

		-- Se actualiza la observacion de la prestacion con marca descarga IPS, con la observacion de la marca ventanilla.
		Update		pa
		Set			obsrvcn = @obsrvcn
		From		#tmpPrestacionesAprobadas pa
		Inner Join	BDCna.gsa.tbASMarcasServiciosSolicitados mss With(NoLock)
		On			pa.cnsctvo_srvco_slctdo = mss.cnsctvo_srvco_slctdo
		Where		mss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca_dscrga_ips
	End

	-- 
	Update		afi
	Set			nmro_slctd_atrzcn_ss = ss.nmro_slctd_atrzcn_ss
	From		#tmpAflds afi
	Inner Join	BDCna.gsa.tbASSolicitudesAutorizacionServicios ss  With(NoLock) 
	On			afi.cnsctvo_slctd_srvco_sld_rcbda = ss.cnsctvo_slctd_atrzcn_srvco 
		
	Set @cnstrccn_trma_xml = (
		Select		TSO.id_sld_rcbda	id,
					TPR.crreo			correo,
					TAF.eml				co,
					TAF.bcc				bcc,
					TSO.plntlla			'plantilla',
					(
						Select	
								'nmbre_cdd_ips_prmria_afldo'		'parametro/nombre',
								AFI.dscrpcn_sde						'parametro/valor',
								NULL,
								'dia'								'parametro/nombre',
								@dia								'parametro/valor',
								NULL,
								'mes'								'parametro/nombre',
								@mes								'parametro/valor',
								NULL,
								'ano'								'parametro/nombre',
								@anno								'parametro/valor',
								NULL,
								'nmbre_afldo'						'parametro/nombre',
								AFI.nmbre_afldo						'parametro/valor',
								NULL,
								'nmro_slctd_atrzcn_ss'				'parametro/nombre',
								AFI.nmro_slctd_atrzcn_ss			'parametro/valor',
								NULL,
								'tbla_prstcns'						'parametro_1/nombre',
									
								(Select 
										cdgo_cdfccn					'td',
										null,
										dscrpcn_srvco_slctdo		'td',
										null,
										nmro_unco_ops				'td',
										null,
										Concat(RTRIM(LTRIM(nmbre_scrsl)), ', ',
												RTRIM(LTRIM(drccn_ips)),', ',
												RTRIM(LTRIM(tlfno)))'td',
										null,
										vlr_lqdcn_cta_rcprcn_srvco	'td',
										null,
										obsrvcn						'td',
										null
									From #tmpPrestacionesAprobadas
									FOR XML Path('tr'), Type)	'parametro_2/valor/tbody',
									NULL,
								'usrio'								'parametro/nombre',
								RTRIM(LTRIM(@usrio))				'parametro/valor',
								NULL,
								'lnea_ncnal'						'parametro/nombre',
								TAF.lnea_ncnal						'parametro/valor',
								NULL,
								'crreo_atncn_clnte'					'parametro/nombre',
								TAF.crreo_atncn_clnte				'parametro/valor'
						From #tmpAflds AFI
						Where AFI.id = TAF.id
						For XML Path('parametros'), Type
					),
					@dia		'fecha/dia',
					@hra		'fecha/hora',
					@mnto		'fecha/minutos',
					@mes		'fecha/mes',
					@sgndo		'fecha/segundos',
					@anno		'fecha/ano',
					RTRIM(LTRIM(@usrio)) 'usuario'
		From		#tmpAflds taf
		Inner Join	#tmpSlctdes tso
		On			taf.id_sld_rcbda=tso.id_sld_rcbda
		Inner Join	#tmpPrvdor tpr
		On			tso.id_sld_rcbda = tpr.id_sld_rcbda
		For XML Path('mensajeCorreo')	
	)
			
	--
	Set @string_trama = cast (@cnstrccn_trma_xml as Varchar(4000))

	--
	If Exists(Select TOP 1 1 From #tmpPrestacionesAprobadas)
		Begin
			Set @string_trama = (Select REPLACE(@string_trama,'</parametro_1>',''));  
			Set @string_trama = (Select REPLACE(@string_trama,'<parametro_2>',''));
			Set @string_trama = (Select REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">'));
			Set @string_trama = (Select REPLACE(@string_trama,'</parametro_2>','</parametro>'));
		End
	Else
		Begin
			Set @string_trama = (Select REPLACE(@string_trama,'<parametro_1>','<parametro htmlTbody="1">')); 
			Set @string_trama = (Select REPLACE(@string_trama,'</parametro_1>','</parametro>')); 
		End

	--
	Set @cnstrccn_trma_xml = CONVERT(XML, @string_trama);
	
	-- 
	Drop Table #tempValoresXOps;
	Drop table #tmpPrestacionesAprobadas;
End
