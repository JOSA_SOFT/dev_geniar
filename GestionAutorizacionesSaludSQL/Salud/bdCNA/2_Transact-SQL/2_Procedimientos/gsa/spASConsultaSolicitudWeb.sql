USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaSolicitudWeb]    Script Date: 8/28/2017 11:54:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaSolicitudWeb
* Desarrollado por   : <\A Luis Felipe Aguirre Ramirez (SETI)	A\>    
* Descripcion        : <\D Consulta informacion asociada a una solicitud  D\>
					   <\D										 D\>         
* Observaciones   	 : <\O O\>    
* Parametros         : <\P P\>   
* Variables          : <\V V\>    
* Fecha Creacion  	 : <\FC 04/03/2016  FC\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento para consultar el numero unico de la OPS
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 27/04/2016  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jorge Luis Rodriguez De León  AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento para devolver el consecutivo del estado de la solicitud
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 17/05/2016  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento para devolver la fecha estimada de enterga de los 
						   servicio
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 23/06/2016  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jorge Rodriguez De León  AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento para devolver el consecutivo de la ciudad del  afiliado
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 23/06/2016  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jorge Rodriguez De León  AM\>    
* Descripcion        : <\D  
                           Se adiciona flag que indica si el valor del concepto gasto de la prestación está en cero o no
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 20/12/2016  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando el NUI del afiliado en la respuesta del 
						   procedimiento
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 11/07/2017  FM\>    
*--------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*--------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D  
                           Se modifica el procedimiento adicionando el NUI del afiliado como parametro en la busqueda
						   de la solicitud
                        D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 28/08/2017  FM\>    
*---------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spASConsultaSolicitudWeb]  NULL,NULL,NULL,'2016-01-00001214',NULL,NULL,NULL,NULL,NULL,NULL,NULL
--exec [gsa].[spASConsultaSolicitudWeb] '2017-04-06','2017-04-30',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
--exec [gsa].[spASConsultaSolicitudWeb] NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL

ALTER PROCEDURE [gsa].[spASConsultaSolicitudWeb]
	@ldfdesde								datetime,
	@ldfhasta								datetime,
	@cnsctvo_cdgo_pln						udtConsecutivo,
	@nmro_slctd_atrzcn_ss					varchar(16),
	@nmro_slctd_prvdr						varchar(15),
	@nmro_unco_ops                          int,
	--@solicitud_servicios
	@cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	udtConsecutivo,
	@nmro_idntfccn_ips_slctnte				udtNumeroIdentificacionLargo,
	@cdgo_intrno							udtCodigoIPS,	
	@cnsctvo_cdgo_srvco_slctdo				udtConsecutivo,	--consecutivo servicio CUMS, CUPS, PIS	
	@nmro_unco_idntfccn_afldo               udtConsecutivo	
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @ldFechaActual           Datetime, 
	        @lcSentenceSql           Varchar(2000) = NULL,
	        @lcWhereSql              Varchar(2000) = NULL,
			@lcOrderSql              Varchar(2000) = NULL,
	        @lnCantidadRegistros     Int = 0	         ,
			@fecha_desde             Varchar(30)         ,
			@fecha_hasta             Varchar(30)         ,			
			@lnEstadoFechaEntregaApr Int				 ,
			@vlr_si					 char(1)

	Create 
	Table #tmpDatosSolicitud(cnsctvo_cdgo_tpo_idntfccn_afldo		udtCodigo ,
	                         cdgo_tpo_idntfccn						char(3),             
	                         nmro_idntfccn_afldo					udtNumeroIdentificacionLargo,
	                         prmr_aplldo_afldo						udtApellido,
	                         sgndo_aplldo_afldo						udtApellido,
	                         prmr_nmbre_afldo						udtNombre,
	                         sgndo_nmbre_afldo						udtNombre,
	                         cnsctvo_cdgo_pln						udtConsecutivo,
	                         nmro_slctd_atrzcn_ss					varchar(16),
	                         fcha_slctd								datetime,
	                         dscrpcn_srvco_slctdo					udtDescripcion,
	                         dscrpcn_estdo_srvco_slctdo				udtDescripcion,
	                         cnsctvo_slctd_atrzcn_srvco             udtConsecutivo,
	                         cnsctvo_cdgo_tpo_idntfccn_ips_slctnte	udtConsecutivo,
	                         nmro_idntfccn_ips_slctnte              udtNumeroIdentificacionLargo,
	                         cdgo_intrno                            udtCodigoIPS,
	                         fcha_crcn                              datetime,
	                         cnsctvo_cdgo_tpo_srvco					udtConsecutivo,
	                         cnsctvo_cdgo_srvco_slctdo				udtConsecutivo,
							 cnsctvo_srvco_slctdo                   udtConsecutivo,
	                         cdgo_srvco_slctdo						char(15),
							 nmro_slctd_prvdr                       varchar(15),
							 fcha_vncmnto_gstn                      Datetime,
							 cnsctvo_cdgo_cdd_rsdnca_afldo			udtConsecutivo,
							 nmro_unco_idntfccn_afldo               udtConsecutivo
							)

	Create 
	Table  #tempInfConceptos(cnsctvo_prcdmnto_insmo_slctdo udtConsecutivo,
							 cnsctvo_mdcmnto_slctdo        udtConsecutivo,
							 cnsctvo_srvco_slctdo          udtConsecutivo
	                        )

	Create
	Table  #tempRespuesta(nmro_idntfccn_afldo                    Varchar(50)               ,
	                      nmbre_afldo                            Varchar(200)              ,
	                      nmro_slctd_atrzcn_ss                   Varchar(16)               ,
						  fcha_slctd                             Datetime                  , 
						  dscrpcn_srvco_slctdo                   udtDescripcion            ,
						  dscrpcn_estdo_srvco_slctdo             udtDescripcion            ,
						  cnsctvo_slctd_atrzcn_srvco             udtConsecutivo            ,
						  cnsctvo_cdgo_srvco_slctdo              udtConsecutivo            ,
						  cnsctvo_srvco_slctdo                   udtConsecutivo            ,
						  fcha_vncmnto_gstn                      Datetime                  ,
						  nmro_unco_ops                          udtConsecutivo  Default 0 ,
						  cnsctvo_cdgo_estdo_cncpto_srvco_slctdo udtConsecutivo            ,
						  dscrpcn_estdo_cncpto_srvco_slctdo      udtDescripcion  Default '',
						  fcha_utlzcn_hsta                       Datetime                  ,
						  cntdd_dscrga_frmto                     Int             Default 0 ,
						  nmro_prcso_instnca					 numeric(18, 0)            ,
						  cnsctvo_cdgo_estdo_slctd				 udtConsecutivo            ,
						  cnsctvo_cdgo_estdo_srvco_slctdo        udtConsecutivo			   ,
						  cnsctvo_cdgo_pln						 udtConsecutivo			   ,
						  cnsctvo_cdgo_tpo_idntfccn_afldo		 udtConsecutivo			   ,
						  fcha_estmda_entrga                     Datetime				   ,
						  cnsctvo_cdgo_cdd_rsdnca_afldo			 udtConsecutivo			   ,
						  vlda_vlr_cro_cncpto					 udtLogico 	    Default 'N',
						  nmro_unco_idntfccn_afldo               udtConsecutivo
	                     ) 
	
	Set	@ldFechaActual           = GETDATE()	             
	Set @lnEstadoFechaEntregaApr = 7
	Set @vlr_si					 = 'S'


	IF (@nmro_slctd_atrzcn_ss != '' OR @nmro_slctd_atrzcn_ss IS NOT NULL)
		BEGIN
		    
			--Se inserta en la tabla temporal informacion de la solictud, buscando por el numero de radicacion de la solicitud
			Insert
			Into       #tmpDatosSolicitud(cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo		 , prmr_aplldo_afldo            , 
									      sgndo_aplldo_afldo			 , prmr_nmbre_afldo			 , sgndo_nmbre_afldo            , 
									      cnsctvo_cdgo_pln				 , nmro_slctd_atrzcn_ss		 , fcha_slctd                   , 
									      dscrpcn_srvco_slctdo			 , dscrpcn_estdo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco   ,
									      fcha_crcn						 , nmro_slctd_prvdr          , cnsctvo_cdgo_srvco_slctdo    ,
									      fcha_vncmnto_gstn              , cnsctvo_srvco_slctdo		 , cnsctvo_cdgo_cdd_rsdnca_afldo,
									      nmro_unco_idntfccn_afldo
									     )
			SELECT     a.cnsctvo_cdgo_tpo_idntfccn_afldo , a.nmro_idntfccn_afldo		, ao.prmr_aplldo_afldo	         , 
					   ao.sgndo_aplldo_afldo		     , ao.prmr_nmbre_afldo			, ao.sgndo_nmbre_afldo           ,        
					   a.cnsctvo_cdgo_pln				 , s.nmro_slctd_atrzcn_ss		, s.fcha_slctd		             ,  
					   ss.dscrpcn_srvco_slctdo           , es.dscrpcn_estdo_srvco_slctdo, s.cnsctvo_slctd_atrzcn_srvco   , 
					   s.fcha_crcn						 , s.nmro_slctd_prvdr           , ss.cnsctvo_cdgo_srvco_slctdo   ,
					   s.fcha_vncmnto_gstn               , ss.cnsctvo_srvco_slctdo      , a.cnsctvo_cdgo_cdd_rsdnca_afldo,
					   a.nmro_unco_idntfccn_afldo
			From	   gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK)    
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios a   WITH(NOLOCK) 
			On         a.cnsctvo_slctd_atrzcn_srvco  = s.cnsctvo_slctd_atrzcn_srvco
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal ao WITH(NOLOCK)
			On         ao.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = a.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
			Inner Join gsa.tbASServiciosSolicitados ss WITH(NOLOCK)
			On         ss.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
			Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias es WITH(NOLOCK)
			On         es.cnsctvo_cdgo_estdo_srvco_slctdo = ss.cnsctvo_cdgo_estdo_srvco_slctdo
			Where	   s.nmro_slctd_atrzcn_ss = @nmro_slctd_atrzcn_ss
			And        @ldFechaActual Between es.inco_vgnca And es.fn_vgnca

			SET @lnCantidadRegistros = @@rowcount

		END

	IF ((@ldfdesde != '' OR @ldfdesde IS NOT NULL) AND (@ldfhasta != '' OR @ldfhasta IS NOT NULL))
		BEGIN	

		    Set  @fecha_desde = CONVERT(varchar(30), @ldfdesde +' 00:00:00',120)
			Set  @fecha_hasta = CONVERT(varchar(30), @ldfhasta +' 23:59:59',120)

			--Se inserta en la tabla temporal informacion de la solictud, buscando por rango de fechas
			Insert
			Into       #tmpDatosSolicitud(cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo		  , prmr_aplldo_afldo            , 
									      sgndo_aplldo_afldo			  , prmr_nmbre_afldo		  , sgndo_nmbre_afldo            , 
									      cnsctvo_cdgo_pln				  , nmro_slctd_atrzcn_ss	  , fcha_slctd                   , 
									      dscrpcn_srvco_slctdo			  , dscrpcn_estdo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco   ,
									      fcha_crcn					  , nmro_slctd_prvdr          , cnsctvo_cdgo_srvco_slctdo    ,
									      fcha_vncmnto_gstn              , cnsctvo_srvco_slctdo	  , cnsctvo_cdgo_cdd_rsdnca_afldo,
									      nmro_unco_idntfccn_afldo
									     )
			SELECT     a.cnsctvo_cdgo_tpo_idntfccn_afldo , a.nmro_idntfccn_afldo		, ao.prmr_aplldo_afldo	         , 
					   ao.sgndo_aplldo_afldo		     , ao.prmr_nmbre_afldo			, ao.sgndo_nmbre_afldo           ,        
					   a.cnsctvo_cdgo_pln				 , s.nmro_slctd_atrzcn_ss		, s.fcha_slctd		             , 
					   ss.dscrpcn_srvco_slctdo           , es.dscrpcn_estdo_srvco_slctdo, s.cnsctvo_slctd_atrzcn_srvco   , 
					   s.fcha_crcn						 , s.nmro_slctd_prvdr           , ss.cnsctvo_cdgo_srvco_slctdo   ,
					   s.fcha_vncmnto_gstn               , ss.cnsctvo_srvco_slctdo		, a.cnsctvo_cdgo_cdd_rsdnca_afldo,
					   a.nmro_unco_idntfccn_afldo
			From       gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK)    
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios a   WITH(NOLOCK) 
			On         a.cnsctvo_slctd_atrzcn_srvco  = s.cnsctvo_slctd_atrzcn_srvco  
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal ao WITH(NOLOCK)
			On         ao.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = a.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
			Inner Join gsa.tbASServiciosSolicitados ss WITH(NOLOCK)
			On         ss.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
			Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias es WITH(NOLOCK)
			On         es.cnsctvo_cdgo_estdo_srvco_slctdo = ss.cnsctvo_cdgo_estdo_srvco_slctdo
			Where	   s.fcha_crcn BETWEEN @fecha_desde AND @fecha_hasta
			And        @ldFechaActual Between es.inco_vgnca And es.fn_vgnca
			
			SET @lnCantidadRegistros = @@rowcount

		END

	IF (@nmro_slctd_prvdr != '' OR @nmro_slctd_prvdr IS NOT NULL)
		BEGIN
		    --Se inserta en la tabla temporal informacion de la solictud, buscando por el numero de solicitud
			Insert
			Into       #tmpDatosSolicitud(cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo		  , prmr_aplldo_afldo            , 
									      sgndo_aplldo_afldo			  , prmr_nmbre_afldo		  , sgndo_nmbre_afldo            , 
									      cnsctvo_cdgo_pln				  , nmro_slctd_atrzcn_ss	  , fcha_slctd                   , 
									      dscrpcn_srvco_slctdo			  , dscrpcn_estdo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco   ,
									      fcha_crcn					  , nmro_slctd_prvdr          , cnsctvo_cdgo_srvco_slctdo    ,
									      fcha_vncmnto_gstn              , cnsctvo_srvco_slctdo	  , cnsctvo_cdgo_cdd_rsdnca_afldo,
									      nmro_unco_idntfccn_afldo
									     )
			SELECT     a.cnsctvo_cdgo_tpo_idntfccn_afldo , a.nmro_idntfccn_afldo		, ao.prmr_aplldo_afldo	         , 
					   ao.sgndo_aplldo_afldo		     , ao.prmr_nmbre_afldo			, ao.sgndo_nmbre_afldo           ,        
					   a.cnsctvo_cdgo_pln				 , s.nmro_slctd_atrzcn_ss		, s.fcha_slctd		             , 
					   ss.dscrpcn_srvco_slctdo           , es.dscrpcn_estdo_srvco_slctdo, s.cnsctvo_slctd_atrzcn_srvco   ,    
					   s.fcha_crcn						 , s.nmro_slctd_prvdr           , ss.cnsctvo_cdgo_srvco_slctdo   ,
					   s.fcha_vncmnto_gstn               , ss.cnsctvo_srvco_slctdo		, a.cnsctvo_cdgo_cdd_rsdnca_afldo,
					   a.nmro_unco_idntfccn_afldo
			From	   gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK)    
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios a   WITH(NOLOCK) 
			On         a.cnsctvo_slctd_atrzcn_srvco  = s.cnsctvo_slctd_atrzcn_srvco 
			Inner Join gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal ao WITH(NOLOCK)
			On         ao.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = a.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
			Inner Join gsa.tbASServiciosSolicitados ss WITH(NOLOCK)
			On         ss.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
			Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias es WITH(NOLOCK)
			On         es.cnsctvo_cdgo_estdo_srvco_slctdo = ss.cnsctvo_cdgo_estdo_srvco_slctdo
			Where	   s.nmro_slctd_prvdr = @nmro_slctd_prvdr
			And        @ldFechaActual Between es.inco_vgnca And es.fn_vgnca

			SET @lnCantidadRegistros = @@rowcount

		END		
	IF (@nmro_unco_ops IS NOT NULL)	
	   Begin
	   
	      Insert 
		  Into       #tempInfConceptos(cnsctvo_prcdmnto_insmo_slctdo)
	      Select     cnsctvo_prcdmnto_insmo_slctdo
	      From       bdcna.gsa.tbASConceptosServicioSolicitado WITH (NOLOCK)
		  Where      nmro_unco_ops = @nmro_unco_ops  
		  Group By   cnsctvo_prcdmnto_insmo_slctdo
		  
		  Insert 
		  Into       #tempInfConceptos(cnsctvo_mdcmnto_slctdo)
	      Select     cnsctvo_mdcmnto_slctdo		  
	      From       bdcna.gsa.tbASConceptosServicioSolicitado  WITH (NOLOCK)
		  Where      nmro_unco_ops = @nmro_unco_ops 
		  Group By   cnsctvo_mdcmnto_slctdo

		  Update     #tempInfConceptos
		  Set        cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
		  From       #tempInfConceptos a
		  Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
		  On         b.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
		  Where      a.cnsctvo_prcdmnto_insmo_slctdo Is Not Null
		  
		  Update     #tempInfConceptos
		  Set        cnsctvo_srvco_slctdo = b.cnsctvo_srvco_slctdo
		  From       #tempInfConceptos a
		  Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
		  On         b.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
		  Where      a.cnsctvo_mdcmnto_slctdo Is Not Null
		  
		  Insert
		  Into       #tmpDatosSolicitud(cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo		, prmr_aplldo_afldo            , 
									    sgndo_aplldo_afldo			    , prmr_nmbre_afldo		    , sgndo_nmbre_afldo            ,    
									    cnsctvo_cdgo_pln				, nmro_slctd_atrzcn_ss	    , fcha_slctd                   , 
									    dscrpcn_srvco_slctdo			, dscrpcn_estdo_srvco_slctdo, cnsctvo_slctd_atrzcn_srvco   ,
									    fcha_crcn					    , nmro_slctd_prvdr          , cnsctvo_cdgo_srvco_slctdo    ,
									    fcha_vncmnto_gstn              , cnsctvo_srvco_slctdo		, cnsctvo_cdgo_cdd_rsdnca_afldo,
									    nmro_unco_idntfccn_afldo
									   )         
		  Select     a.cnsctvo_cdgo_tpo_idntfccn_afldo, a.nmro_idntfccn_afldo		 , ao.prmr_aplldo_afldo	          , 
					 ao.sgndo_aplldo_afldo		      , ao.prmr_nmbre_afldo		     , ao.sgndo_nmbre_afldo           ,        
					 a.cnsctvo_cdgo_pln				  , s.nmro_slctd_atrzcn_ss	     , s.fcha_slctd		              , 
					 ss.dscrpcn_srvco_slctdo          , es.dscrpcn_estdo_srvco_slctdo, s.cnsctvo_slctd_atrzcn_srvco   , 
					 s.fcha_crcn					  , s.nmro_slctd_prvdr           , ss.cnsctvo_cdgo_srvco_slctdo   ,
					 s.fcha_vncmnto_gstn	          , ss.cnsctvo_srvco_slctdo	     , a.cnsctvo_cdgo_cdd_rsdnca_afldo,
					 a.nmro_unco_idntfccn_afldo
		  From       bdcna.gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK)  
		  Inner Join bdcna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios a   WITH(NOLOCK) 
		  On         a.cnsctvo_slctd_atrzcn_srvco  = s.cnsctvo_slctd_atrzcn_srvco  
		  Inner Join bdcna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServiciosOriginal ao WITH(NOLOCK)
		  On         ao.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = a.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco		  
		  Inner Join bdcna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
		  On         ss.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco        
		  Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias es WITH(NOLOCK)
		  On         es.cnsctvo_cdgo_estdo_srvco_slctdo = ss.cnsctvo_cdgo_estdo_srvco_slctdo        
		  Inner Join #tempInfConceptos x
		  On         x.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		  Where      @ldFechaActual Between es.inco_vgnca And es.fn_vgnca
		  
		  SET @lnCantidadRegistros = @@rowcount
	   End

	IF (@lnCantidadRegistros > 0) 
		BEGIN		
			
			UPDATE	   ds
			SET        cdgo_tpo_idntfccn = ti.cdgo_tpo_idntfccn
			From       #tmpDatosSolicitud ds
			Inner Join bdAfiliacionValidador.dbo.tbTiposIdentificacion_vigencias ti WITH(NOLOCK)
			ON         ti.cnsctvo_cdgo_tpo_idntfccn = ds.cnsctvo_cdgo_tpo_idntfccn_afldo
			WHERE      @ldFechaActual BETWEEN ti.inco_vgnca AND ti.fn_vgnca	
						
			UPDATE     ds
			SET        cnsctvo_cdgo_tpo_idntfccn_ips_slctnte = ip.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte,
			           nmro_idntfccn_ips_slctnte             = ip.nmro_idntfccn_ips_slctnte,
			           cdgo_intrno                           = ip.cdgo_intrno
			From       #tmpDatosSolicitud ds
			Inner Join gsa.tbASIPSSolicitudesAutorizacionServicios ip WITH(NOLOCK)
			ON         ip.cnsctvo_slctd_atrzcn_srvco = ds.cnsctvo_slctd_atrzcn_srvco		
	
		END			           			

		SET @lcSentenceSql = 'SELECT CONCAT(RTRIM(ds.cdgo_tpo_idntfccn),''.'',ds.nmro_idntfccn_afldo)				nmro_idntfccn_afldo,'				+ CHAR(13) +  --TipoyNroDocumentoAfiliado 
									'CONCAT(RTRIM(ds.prmr_nmbre_afldo),'' '', RTRIM(ds.sgndo_nmbre_afldo),'' '','										+ CHAR(13) +   
									'	    RTRIM(ds.prmr_aplldo_afldo),'' '', RTRIM(ds.sgndo_aplldo_afldo))		nmbre_afldo                    ,'   + CHAR(13) +  --NombreCompletoAfiliado
									'ds.nmro_slctd_atrzcn_ss														nmro_slctd_atrzcn_ss           ,'	+ CHAR(13) +  --Nradicado
									'ds.fcha_crcn																	fcha_slctd                     ,'	+ CHAR(13) +  --FechaCreacion
									'ds.dscrpcn_srvco_slctdo														dscrpcn_srvco_slctdo           ,'	+ CHAR(13) +  --Prestacion	
									'ds.dscrpcn_estdo_srvco_slctdo													dscrpcn_estdo_srvco_slctdo     ,'	+ CHAR(13) +  --EstadoPrestacion
									'ds.cnsctvo_slctd_atrzcn_srvco													cnsctvo_slctd_atrzcn_srvco     ,'	+ CHAR(13) +  --ConsecutivoSolicitud
									'ds.cnsctvo_cdgo_srvco_slctdo													cnsctvo_cdgo_srvco_slctdo      ,'	+ CHAR(13) +  --ConsecutivoCodificacionServicio
									'ds.cnsctvo_srvco_slctdo     													cnsctvo_srvco_slctdo           ,'	+ CHAR(13) +  --ConsecutivoServicio
									'ds.fcha_vncmnto_gstn													        fcha_vncmnto_gstn              ,'	+ CHAR(13) +  --FechaVencimientoGestion
									'ds.cnsctvo_cdgo_pln															cnsctvo_cdgo_pln		       ,'	+ CHAR(13) +  --ConsecutivoCodigoPlan
									'ds.cnsctvo_cdgo_tpo_idntfccn_afldo												cnsctvo_cdgo_tpo_idntfccn_afldo,'	+ CHAR(13) +  --ConsecutivoCodigoTipoIdentificacionAfiliado
									'ds.cnsctvo_cdgo_cdd_rsdnca_afldo												cnsctvo_cdgo_cdd_rsdnca_afldo  ,'	+ CHAR(13) +  --Consecutivo residencia del afiliado
							        'ds.nmro_unco_idntfccn_afldo                                                    nmro_unco_idntfccn_afldo        '	+ CHAR(13) +  --Numero unico de identificacion del afiliado
							 'From #tmpDatosSolicitud ds '																								+ CHAR(13) +
							 'WHERE 1=1'																												+ CHAR(13) 			 

		IF (@cnsctvo_cdgo_pln != '' OR @cnsctvo_cdgo_pln IS NOT NULL)
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_pln = ' +  
								  CONVERT(VARCHAR,@cnsctvo_cdgo_pln) + CHAR(13) 
			END
			
		IF (@cnsctvo_cdgo_tpo_idntfccn_ips_slctnte != '' OR @cnsctvo_cdgo_tpo_idntfccn_ips_slctnte IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_tpo_idntfccn_ips_slctnte = ' + 
								  CONVERT(VARCHAR,@cnsctvo_cdgo_tpo_idntfccn_ips_slctnte)  + CHAR(13) 
			END	
		
		IF (@nmro_idntfccn_ips_slctnte != '' OR @nmro_idntfccn_ips_slctnte IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.nmro_idntfccn_ips_slctnte = ''' + 
					@nmro_idntfccn_ips_slctnte + '''' + CHAR(13) 
			END				
		
		IF (@cdgo_intrno != '' OR @cdgo_intrno IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cdgo_intrno = ''' + 
					@cdgo_intrno + '''' + CHAR(13) 
			END
	
		
		IF (@cnsctvo_cdgo_srvco_slctdo != '' OR @cnsctvo_cdgo_srvco_slctdo IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.cnsctvo_cdgo_srvco_slctdo = ' + 
					CONVERT(VARCHAR,@cnsctvo_cdgo_srvco_slctdo) + CHAR(13) 
			END					
		
		
		IF (@nmro_unco_idntfccn_afldo != '' OR @nmro_unco_idntfccn_afldo IS NOT NULL) 
			BEGIN
				SET @lcWhereSql = ISNULL(@lcWhereSql,'') + 'AND ds.nmro_unco_idntfccn_afldo = ' + 
					CONVERT(VARCHAR,@nmro_unco_idntfccn_afldo) + CHAR(13) 
			END	
	
		
		Set @lcOrderSql = 'Order by ds.nmro_slctd_atrzcn_ss Desc'
				
		SET @lcSentenceSql = @lcSentenceSql + ISNULL(@lcWhereSql,'') + @lcOrderSql
		
		--print (@lcSentenceSql)
		Insert 
		Into   #tempRespuesta(nmro_idntfccn_afldo          , nmbre_afldo              , nmro_slctd_atrzcn_ss      ,
		                      fcha_slctd                   , dscrpcn_srvco_slctdo     , dscrpcn_estdo_srvco_slctdo,
							  cnsctvo_slctd_atrzcn_srvco   , cnsctvo_cdgo_srvco_slctdo, cnsctvo_srvco_slctdo      ,
							  fcha_vncmnto_gstn			   , cnsctvo_cdgo_pln		   , cnsctvo_cdgo_tpo_idntfccn_afldo,
							  cnsctvo_cdgo_cdd_rsdnca_afldo, nmro_unco_idntfccn_afldo
		                     )
		EXEC(@lcSentenceSql)

		Update     #tempRespuesta
		Set        nmro_unco_ops                          = c.nmro_unco_ops                         ,
		           cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = c.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,
				   fcha_utlzcn_hsta                       = c.fcha_utlzcn_hsta
		From       #tempRespuesta a
		Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Inner Join bdcna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
		On         c.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
		  
		Update     #tempRespuesta
		Set        nmro_unco_ops                          = c.nmro_unco_ops                         ,
		           cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = c.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,
				   fcha_utlzcn_hsta                       = c.fcha_utlzcn_hsta
		From       #tempRespuesta a
		Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Inner Join bdcna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
		On         c.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo

		--Se actualiza flag que indica si algun concepto esta en cero
		Update     a
		Set		   vlda_vlr_cro_cncpto = @vlr_si
		From       #tempRespuesta a
		Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Inner Join bdcna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
		On         c.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
		and		   c.vlr_lqdcn <= 0	

		--Se actualiza flag que indica si algun concepto esta en cero
		Update     a
		Set		   vlda_vlr_cro_cncpto = @vlr_si
		From       #tempRespuesta a
		Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Inner Join bdcna.gsa.tbASConceptosServicioSolicitado c WITH (NOLOCK)
		On         c.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
		and		   c.vlr_lqdcn <= 0

		Update     #tempRespuesta
		Set        dscrpcn_estdo_cncpto_srvco_slctdo = es.dscrpcn_estdo_srvco_slctdo
		From       #tempRespuesta a
	    Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias es WITH(NOLOCK)
		On         es.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo   
		Where      @ldFechaActual Between es.inco_vgnca And es.fn_vgnca
		
		--Se actualiza la cantidad de formatos descargados
		Update     #tempRespuesta
		Set        cntdd_dscrga_frmto = b.cntdd_dscrga_frmto
		From       #tempRespuesta a
		Inner Join bdCNA.gsa.tbASHistoricoDescargaFormatos b WITH(NOLOCK)
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		
		Update     #tempRespuesta
		Set        nmro_prcso_instnca		= s.nmro_prcso_instnca,
				   cnsctvo_cdgo_estdo_slctd = s.cnsctvo_cdgo_estdo_slctd
		From       #tempRespuesta a
		Inner Join bdcna.gsa.tbASSolicitudesAutorizacionServicios s WITH(NOLOCK) 
		On		   a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco 

		Update     #tempRespuesta
		Set        cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_srvco_slctdo
		From       #tempRespuesta a
		Inner Join bdcna.prm.tbASEstadosServiciosSolicitados_Vigencias b WITH(NOLOCK) 
		On         b.dscrpcn_estdo_srvco_slctdo = a.dscrpcn_estdo_srvco_slctdo
		Where      @ldFechaActual Between b.inco_vgnca And b.fn_vgnca

		---Se actualiza la fecha de entrega del servicio
		Update     #tempRespuesta
		Set        fcha_estmda_entrga = b.fcha_estmda_entrga
		From       #tempRespuesta a
		Inner Join BDCna.gsa.tbASResultadoFechaEntrega b WITH(NOLOCK) 
		On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		Where      a.cnsctvo_cdgo_estdo_srvco_slctdo = @lnEstadoFechaEntregaApr
				

	    Select   nmro_idntfccn_afldo              , nmbre_afldo                    , nmro_slctd_atrzcn_ss                  ,
		         fcha_slctd                       , dscrpcn_srvco_slctdo           , dscrpcn_estdo_srvco_slctdo            ,
			     cnsctvo_slctd_atrzcn_srvco       , cnsctvo_cdgo_srvco_slctdo      , cnsctvo_srvco_slctdo                  ,
			     fcha_vncmnto_gstn                , nmro_unco_ops                  , cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,
				 dscrpcn_estdo_cncpto_srvco_slctdo, fcha_utlzcn_hsta               , cntdd_dscrga_frmto					   ,
				 nmro_prcso_instnca				  , cnsctvo_cdgo_estdo_slctd       , cnsctvo_cdgo_estdo_srvco_slctdo       ,
				 cnsctvo_cdgo_pln				  , cnsctvo_cdgo_tpo_idntfccn_afldo, fcha_estmda_entrga					   ,
				 cnsctvo_cdgo_cdd_rsdnca_afldo	  , vlda_vlr_cro_cncpto            , nmro_unco_idntfccn_afldo
		From     #tempRespuesta
		Order by nmro_slctd_atrzcn_ss Desc


		Drop Table #tmpDatosSolicitud
		Drop Table #tempInfConceptos
		Drop Table #tempRespuesta
END

