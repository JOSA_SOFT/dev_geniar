Use bdCNA
Go

If(OBJECT_ID('gsa.spAsConsultarNotificacionesAfiliado') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsConsultarNotificacionesAfiliado  As Select 1';
End
Go

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spAsConsultarNotificacionesAfiliado
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez  A\>
* Descripcion		: <\D SP que carga la informacion de notificaciones y diagnostico del afiliado. D\>
* Observaciones		: <\O O\>
* Parametros		: <\P P\>
* Variables			: <\V V\>
* Fecha Creacion	: <\FC 2017/08/25 FC\>
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>
* Descripcion			: <\DM DM\>
* Nuevos Parametros	 	: <\PM PM\>
* Nuevas Variables		: <\VM VM\>
* Fecha Modificacion	: <\FM FM\>
*-----------------------------------------------------------------------------------------------------------------------*/

Alter Procedure gsa.spAsConsultarNotificacionesAfiliado 
As
Begin

	Set Nocount On
	
	Declare		@cnsctvo_cdgo_clsfccn_evnto_ntfccn13		udtConsecutivo, 
				@cnsctvo_cdgo_clsfccn_evnto_ntfccn16		udtConsecutivo, 
				@cnsctvo_cdgo_clsfccn_evnto_ntfccn23		udtConsecutivo,
				@dscrpcn_estdo_ntfccn_ntfcdo				udtDescripcion,
				@dscrpcn_estdo_ntfccn_cnfrmdo				udtDescripcion,
				@dscrpcn_estdo_ntfccn_prbble				udtDescripcion,
				@cnsctvo_cdgo_ptlga_ctstrfca8				udtConsecutivo,
				@cnsctvo_cdgo_clsfccn_evnto_ntfccn3			udtConsecutivo,
				@fcha_actl									Date,
				@cnsctvo_cdgo_tpo_dgnstco					udtConsecutivo;

	-- 
	Set @cnsctvo_cdgo_clsfccn_evnto_ntfccn13	= 13; -- 13 Cancer
	Set @cnsctvo_cdgo_clsfccn_evnto_ntfccn16	= 16; -- VIH
	Set @cnsctvo_cdgo_clsfccn_evnto_ntfccn23	= 23; -- IRC (Insuficiencia renal cronica)
	Set @dscrpcn_estdo_ntfccn_ntfcdo			= 'NOTIFICADO'; -- 
	Set @dscrpcn_estdo_ntfccn_cnfrmdo			= 'CONFIRMADO'; -- 
	Set @dscrpcn_estdo_ntfccn_prbble			= 'PROBABLE'; -- 
	Set @cnsctvo_cdgo_ptlga_ctstrfca8			= 8; -- 8 Gran quemado
	Set @cnsctvo_cdgo_clsfccn_evnto_ntfccn3		= 3; -- 3 Enfermedad alto costo
	Set @fcha_actl = GETDATE();
	Set @cnsctvo_cdgo_tpo_dgnstco = 1; -- 1 -> Diagnostico principal

	-- 
	Insert Into #tmpNotificacionesAfiliado (
				nmro_unco_idntfccn_afldo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				cnsctvo_cdgo_ptlga_ctstrfca,	dscrpcn_estdo_ntfccn
	)
	Select		a.nmro_unco_idntfccn,			s.cnsctvo_slctd_atrzcn_srvco,	b.cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				a.cnsctvo_cdgo_ptlga_ctstrfca, b.dscrpcn_estdo_ntfccn 
	From		bdsisalud.dbo.tbAfiliadosMarcados a With(NoLock)
	Inner Join	bdsisalud.dbo.tbEstadosNotificacion_Vigencias b With(NoLock)
	On			a.cnsctvo_cdgo_estdo_ntfccn=b.cnsctvo_cdgo_estdo_ntfccn 
	Inner Join	#TempSolicitudes s 
	On			s.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
	Where		b.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn13
	And			b.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo, @dscrpcn_estdo_ntfccn_prbble)
	And			@fcha_actl Between b.inco_vgnca And b.fn_vgnca
	
	-- 
	Insert Into #tmpNotificacionesAfiliado (
				nmro_unco_idntfccn_afldo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				cnsctvo_cdgo_ptlga_ctstrfca,	dscrpcn_estdo_ntfccn
	)
	Select		a.nmro_unco_idntfccn,			s.cnsctvo_slctd_atrzcn_srvco,	b.cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				a.cnsctvo_cdgo_ptlga_ctstrfca, b.dscrpcn_estdo_ntfccn 
	From		bdsisalud.dbo.tbAfiliadosMarcados a With(NoLock)
	Inner Join	bdsisalud.dbo.tbEstadosNotificacion_Vigencias b With(NoLock)
	On			a.cnsctvo_cdgo_estdo_ntfccn=b.cnsctvo_cdgo_estdo_ntfccn 
	Inner Join	#TempSolicitudes s 
	On			s.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
	Where		b.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn16
	And			b.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo)
	And			@fcha_actl Between b.inco_vgnca And b.fn_vgnca

	-- 
	Insert Into #tmpNotificacionesAfiliado (
				nmro_unco_idntfccn_afldo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				cnsctvo_cdgo_ptlga_ctstrfca,	dscrpcn_estdo_ntfccn
	)
	Select		a.nmro_unco_idntfccn,			s.cnsctvo_slctd_atrzcn_srvco,	b.cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				a.cnsctvo_cdgo_ptlga_ctstrfca, b.dscrpcn_estdo_ntfccn 
	From		bdsisalud.dbo.tbAfiliadosMarcados a With(NoLock)
	Inner Join	bdsisalud.dbo.tbEstadosNotificacion_Vigencias b With(NoLock)
	On			a.cnsctvo_cdgo_estdo_ntfccn=b.cnsctvo_cdgo_estdo_ntfccn 
	Inner Join	#TempSolicitudes s 
	On			s.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
	Where		b.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn23
	And			b.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo, @dscrpcn_estdo_ntfccn_cnfrmdo)
	And			@fcha_actl Between b.inco_vgnca And b.fn_vgnca
	
	-- 
	Insert Into #tmpNotificacionesAfiliado ( 
				nmro_unco_idntfccn_afldo,		cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				cnsctvo_cdgo_ptlga_ctstrfca,	dscrpcn_estdo_ntfccn
	)
	Select		a.nmro_unco_idntfccn,			s.cnsctvo_slctd_atrzcn_srvco,	b.cnsctvo_cdgo_clsfccn_evnto_ntfccn,
				a.cnsctvo_cdgo_ptlga_ctstrfca, b.dscrpcn_estdo_ntfccn 
	From		bdsisalud.dbo.tbAfiliadosMarcados a With(NoLock)
	Inner Join	bdsisalud.dbo.tbEstadosNotificacion_Vigencias b With(NoLock)
	On			a.cnsctvo_cdgo_estdo_ntfccn=b.cnsctvo_cdgo_estdo_ntfccn 
	Inner Join	#TempSolicitudes s 
	On			s.nmro_unco_idntfccn_afldo = a.nmro_unco_idntfccn
	Where		b.cnsctvo_cdgo_clsfccn_evnto_ntfccn = @cnsctvo_cdgo_clsfccn_evnto_ntfccn3  
	And			a.cnsctvo_cdgo_ptlga_ctstrfca = @cnsctvo_cdgo_ptlga_ctstrfca8
	And			b.dscrpcn_estdo_ntfccn In (@dscrpcn_estdo_ntfccn_ntfcdo,@dscrpcn_estdo_ntfccn_cnfrmdo)	

	-- Se cargan los diagnosticos.
	Insert Into		#tmpDiagnosticos (
				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_cdgo_dgnstco
	)
	Select		d.cnsctvo_slctd_atrzcn_srvco,	d.cnsctvo_cdgo_dgnstco
	From		bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios d With(NoLock)
	Inner Join	bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias dv With(NoLock)
	On			dv.cnsctvo_dgnstco_slctd_atrzcn_srvco = d.cnsctvo_dgnstco_slctd_atrzcn_srvco
	Inner Join	#TempSolicitudes s
	On			s.cnsctvo_slctd_atrzcn_srvco = d.cnsctvo_slctd_atrzcn_srvco
	Where		d.cnsctvo_cdgo_tpo_dgnstco = @cnsctvo_cdgo_tpo_dgnstco
	And			@fcha_actl Between dv.inco_vgnca And dv.fn_vgnca;

	Update		a
	Set			cnsctvo_cdgo_tpo_mrca_dgnstco = md.cnsctvo_tpo_mrca_dgnstco
	From		#tmpDiagnosticos a
	Inner Join	bdSisalud.dbo.tbTiposMarcasDiagnosticoxDiagnosticos_Vigencias md With(NoLock)
	On			md.cnsctvo_cdgo_dgnstco = a.cnsctvo_cdgo_dgnstco
	Where		@fcha_actl Between md.inco_vgnca And md.fn_vgnca;

	-- 
	Update		ss
	Set			cnsctvo_cdgo_agrpdr_prstcn = b.cnsctvo_cdgo_agrpdr_prstcn
	From		bdsisalud.dbo.tbDetAgrupadoresPrestaciones_Vigencias a With(NoLock) 
	Inner Join	bdSisalud.dbo.tbAgrupadoresPrestaciones_vigencias b With(NoLock)
 	On			b.cnsctvo_cdgo_agrpdr_prstcn  = a.cnsctvo_cdgo_agrpdr_prstcn
 	Inner Join	bdSisalud.dbo.tbTiposAgrupadoresPrestaciones c With(NoLock)
	On          c.cnsctvo_cdgo_tpo_agrpdr_prstcn=b.cnsctvo_cdgo_tpo_agrpdr_prstcn
	Inner Join	#TempServicioSolicitudes ss 
	On			ss.cnsctvo_cdgo_srvco_slctdo = a.cnsctvo_cdfccn
	Where		@fcha_actl Between a.inco_vgnca And a.fn_vgnca
 	And			@fcha_actl Between b.inco_vgnca And b.fn_vgnca 	

End