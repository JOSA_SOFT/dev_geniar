USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarPrestacionesOPS]    Script Date: 15/05/2017 7:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF object_id('gsa.spASConsultarPrestacionesxSolicitud') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASConsultarPrestacionesxSolicitud AS SELECT 1;'
END
GO
/*----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultarPrestacionesxSolicitud
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos    A\>    
* Descripcion        : <\D 
                           Procedimiento que permite consultar la información de las prestaciones     
					       a partir del consecutivo de la solicitud											  
					   D\>
* Observaciones      : <\O  O\>    
* Parametros         : <\P @id_slctd : Identificador único de la solicitud. P\>  				
* Variables          : <\V  V\>    
* Fecha Creacion     : <\FC 15/05/2016 FC\>    
*----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\DM   DM\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*----------------------------------------------------------------------------------------------------------------------------------------*/

--exec gsa.spASConsultarPrestacionesxSolicitud  1081

ALTER PROCEDURE [gsa].[spASConsultarPrestacionesxSolicitud] 
   @cnsctvo_slctd_atrzcn_srvco udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON

	Declare @ldFechaActual Datetime

	Create Table #tmpPrestaciones(cnsctvo_srvco_slctdo          udtConsecutivo,
	                              cnsctvo_cdgo_tpo_srvco		udtConsecutivo,
								  cdgo_tpo_srvco				udtCodigo     ,
								  dscrpcn_tpo_srvco				udtDescripcion,
								  cnsctvo_cdfccn				udtConsecutivo,
								  cdgo_cdfccn					Char(15)      ,
								  dscrpcn_cdfccn				udtDescripcion,
								  cntdd							Int           ,
							      cnsctvo_cdgo_cdd_rsdnca_afldo	udtConsecutivo,
								  cnsctvo_cdgo_pln				udtConsecutivo,
								  cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
	                             );

	Set @ldFechaActual = getDate()

	--Inserción de información de los servicios
	Insert 
	Into       #tmpPrestaciones (cnsctvo_srvco_slctdo, cnsctvo_cdgo_tpo_srvco    , cnsctvo_cdfccn, 
	                             cntdd               , cnsctvo_slctd_atrzcn_srvco
								)
	Select     sso.cnsctvo_srvco_slctdo, sso.cnsctvo_cdgo_tpo_srvco  , sso.cnsctvo_cdgo_srvco_slctdo, 
	           sso.cntdd_slctda        , a.cnsctvo_slctd_atrzcn_srvco
	From       bdCNA.gsa.tbASSolicitudesAutorizacionServicios a With(NoLock)	
 	Inner Join bdCNA.gsa.tbAsServiciosSolicitados sso With(NoLock)	
	On         a.cnsctvo_slctd_atrzcn_srvco = sso.cnsctvo_slctd_atrzcn_srvco	  
	WHERE      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco


	--Actualización de información adicional.
	Update     a
	Set        cdgo_cdfccn    = b.cdgo_cdfccn   ,
		       dscrpcn_cdfccn = b.dscrpcn_cdfccn
	From       #tmpPrestaciones	a
	Inner Join bdsisalud.dbo.tbCodificaciones b With(NoLock)	
 	On         b.cnsctvo_cdfccn = a.cnsctvo_cdfccn

	--Actualización de tipo prestación.
	Update     a
	Set        dscrpcn_tpo_srvco = b.dscrpcn_tpo_cdfccn,
		       cdgo_tpo_srvco    = b.cdgo_tpo_cdfccn
	From       #tmpPrestaciones a
	Inner Join bdsisalud.dbo.tbTipoCodificacion_vigencias b  With(NoLock)
	On         b.cnsctvo_cdgo_tpo_cdfccn = a.cnsctvo_cdgo_tpo_srvco
    Where      @ldFechaActual Between b.inco_vgnca and b.fn_vgnca


	 --Obtener la ciudad y el plan del afiliado
	 Update     a
	 Set	    cnsctvo_cdgo_pln			  = b.cnsctvo_cdgo_pln,
			    cnsctvo_cdgo_cdd_rsdnca_afldo = b.cnsctvo_cdgo_cdd_rsdnca_afldo
	 From       #tmpPrestaciones a
	 Inner Join BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios b With(NoLock)
	 On         b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco


	 --Se retorna la información.
	 SELECT cnsctvo_srvco_slctdo, cnsctvo_cdgo_tpo_srvco, cdgo_tpo_srvco               , 
	        dscrpcn_tpo_srvco   , cnsctvo_cdfccn        , cdgo_cdfccn                  , 
			dscrpcn_cdfccn      , cntdd                 , cnsctvo_cdgo_cdd_rsdnca_afldo, 
			cnsctvo_cdgo_pln
	 FROM   #tmpPrestaciones
END
