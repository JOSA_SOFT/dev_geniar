Use bdCNA
Go

If(OBJECT_ID('gsa.spASValidarExistenciaReciboCaja') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASValidarExistenciaReciboCaja As Select 1';
End 
Go

/*
-------------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASValidarExistenciaReciboCaja												
* Desarrollado por	: <\A Ing. Carlos Andres Lopez Ramirez A\>  
* Descripcion		: <\D  Procedimientos que consulta la cantidad de recibos de caja generados para la 
						   solicitud de entrada. D\>  												
* Observaciones		: <\O  O\>  													
* Parametros		: <\P  P\>  													
* Variables			: <\V  V\>  													
* Fecha Creacion	: <\FC 04/08/2017 FC\>											
*  															
*------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM  AM\>  													
* Descripcion			: <\DM  DM\>
* Nuevos Parametros	 	: <\PM  PM\>  													
* Nuevas Variables		: <\VM  VM\>  													
* Fecha Modificacion	: <\FM  FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
*/


Alter Procedure gsa.spASValidarExistenciaReciboCaja
	@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
	@cnsctvo_srvco_slctdo			udtConsecutivo,
	@cntdd_rcbs						udtConsecutivo Output
As
Begin
	
	Set Nocount On

	Declare		@tipo_documento_caja	udtConsecutivo;
			
	-- 	
	Create Table #tempOPS (
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
		nmro_unco_ops					udtConsecutivo
	);

	-- 
	Set @tipo_documento_caja = 1;

	-- 
	Insert Into #tempOPS (
				cnsctvo_slctd_atrzcn_srvco,			nmro_unco_ops
	)
	Select		dccr.cnsctvo_slctd_atrzcn_srvco,	dccr.nmro_unco_ops 
	From		bdCNA.gsa.tbAsDetCalculoCuotaRecuperacion dccr With(NoLock)
	Where		dccr.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And			dccr.cnsctvo_srvco_slctdo = @cnsctvo_srvco_slctdo;

	-- 
	Select      @cntdd_rcbs = count(1)
	From        #tempOPS a
	Inner Join  bdCNA.cja.tbRCSoportesDocumentosCaja b With(Nolock) 
	On          b.nmro_unco_ops = a.nmro_unco_ops
	Inner Join  bdCNA.cja.tbRCDocumentosCaja c With(Nolock) 
	On          c.cnsctvo_cdgo_dcmnto_cja = b.cnsctvo_dcmnto_cja
	Where       c.cnsctvo_cdgo_tpo_dcmnto_cja = @tipo_documento_caja;

	--
	Drop Table #tempOPS;

End