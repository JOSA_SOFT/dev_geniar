USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarProcesoLiquidacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASEjecutarProcesoLiquidacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D Consulta los registros necesarios para ejecutar el 
							SP de Liquidación									D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jonathan - SETI SAS								 AM\>
* Descripcion			 : <\DM	Se insertan las solicitudes en la tabla temporal
								#tmpnmroslctds y se ejecuta el SP orquestador de
								liquidación									     DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	01/06/2016										 FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASEjecutarProcesoLiquidacion] 
	@p_cdgs_slctds		NVARCHAR(MAX),
	@mensaje			VARCHAR(1000) OUTPUT,
	@codigo_error		VARCHAR(100) OUTPUT
AS
BEGIN
  SET NOCOUNT ON;
  BEGIN TRY	

	DECLARE @no_es_btch			udtlogico	= 'N', --El proceso se ejecuta de manera Online
			@cdgs_slctds_XML	XML,
			@cdgo_err			CHAR(5)		= 'ERROR',
			@cdgo_ok			CHAR(2)		= 'OK',
			@fcha_entrga		DATE		=  GETDATE();

    CREATE TABLE #tmpnmroslctds (
			id								INT IDENTITY (1, 1),
			cnsctvo_slctd_srvco_sld_rcbda	udtconsecutivo,
			nmro_slctd						udtconsecutivo,
			nmro_slctd_prvdr				udtconsecutivo,
			stdo_slctd						INT DEFAULT 0,
			cntrl_prcso						CHAR(1)
	  );	
	  
	SET @cdgs_slctds_XML = CONVERT(XML, @p_cdgs_slctds);

	BEGIN

		INSERT INTO #tmpNmroSlctds (cnsctvo_slctd_srvco_sld_rcbda)
		SELECT Pref.value('.', 'udtConsecutivo')
		FROM @cdgs_slctds_XML.nodes('(/cdgs_slctds/idnt_slctds/cnsctvo_slctd)') AS xml_slctd (Pref);

		--Se ejecuta el SP orquestador
		EXEC gsa.spASProcesoLiquidacion @fcha_entrga, @no_es_btch, @mensaje OUTPUT, @codigo_error OUTPUT

	END
  END TRY
  BEGIN CATCH
		SET @mensaje =	'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
						'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
						'Message:'   + ERROR_MESSAGE() + CHAR(13) +
						'Procedure:' + ERROR_PROCEDURE();
		SET @codigo_error = @cdgo_err;
  END CATCH
  
  DROP TABLE #tmpnmroslctds;

END;

GO
