USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarUsuarioAltoCosto]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarUsuarioAltoCosto
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que valida el usuario es de alto costo	 D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarUsuarioAltoCosto] @cnsctvo_cdgo_grpo_entrga udtConsecutivo,
@cdgo_grpo_entrga CHAR(4),
@agrpa_prstcn udtLogico
AS

BEGIN
  SET NOCOUNT ON

  DECLARE @fcha_actl datetime
  SET @fcha_actl = GETDATE()

  DECLARE @afirmacion char(1)
  SELECT
    @afirmacion = 'S'

  IF EXISTS (SELECT
      sol.id
    FROM #tmpDatosSolicitudFechaEntrega sol
    WHERE sol.cnsctvo_grpo_entrga IS NULL)
  BEGIN

    --Se actualiza al grupo en caso de que el afiliado sea de alto riesgo.
    UPDATE #tmpDatosSolicitudFechaEntrega
    SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
        cdgo_grpo_entrga = @cdgo_grpo_entrga
    FROM #tmpDatosSolicitudFechaEntrega sol
    INNER JOIN BdRiesgosSalud.dbo.tbReporteCohortesxAfiliado rca --Cohorte por afiliado
      ON rca.nmro_unco_idntfccn_afldo = sol.nmro_unco_idntfccn_afldo
    INNER JOIN bdRiesgosSalud.dbo.tbHistoricoEstadosReporteCohortesxAfiliado hrca
      ON rca.cnsctvo_rprte_chrte_x_afldo = hrca.cnsctvo_rprte_chrte_x_afldo
      AND hrca.vldo = @afirmacion
      AND @fcha_actl BETWEEN hrca.inco_vgnca_estdo AND hrca.fn_vgnca_estdo
    WHERE sol.cnsctvo_grpo_entrga IS NULL;

    --Se determina si se debe realizar agrupación
    IF EXISTS (SELECT
        sol.id
      FROM #tmpDatosSolicitudFechaEntrega sol
      WHERE sol.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga
      AND @agrpa_prstcn = @afirmacion)
    BEGIN
      UPDATE sol
      SET cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga,
          cdgo_grpo_entrga = @cdgo_grpo_entrga
      FROM #tmpDatosSolicitudFechaEntrega sol
      INNER JOIN #tmpDatosSolicitudFechaEntrega tmp
        ON tmp.cnsctvo_slctd_srvco_sld_rcbda = sol.cnsctvo_slctd_srvco_sld_rcbda
      WHERE tmp.cnsctvo_grpo_entrga = @cnsctvo_cdgo_grpo_entrga

    END
  END
END

GO
