USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarHomologarPrestacionesPrestador]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASGrabarHomologarPrestacionesPrestador 												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Grabar  Masiva ó por demanda de la Homologación de Prestaciones
						  del prestador																				D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/29																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASGrabarHomologarPrestacionesPrestador]

/*01*/ @lcUsrioAplcn   UdtUsuario = null

As

Begin
	SET NOCOUNT ON

	Declare @lcUsuario				UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fechaactual			datetime = getdate()

	If @lcUsrioAplcn is not null
	   Begin
	     Set @lcUsuario = @lcUsrioAplcn
	   End 
	   
	 --Actualizamos la(s) homologacion(es) a las tablas definitivas
	 
	 Update ss
	 Set	cnsctvo_cdgo_prstcn_prstdr = t.cnsctvo_cdgo_prstcn_prstdr,
			fcha_ultma_mdfccn = @fechaactual,
			usro_ultma_mdfccn = @lcUsuario
	 From	gsa.tbASServiciosSolicitados ss
	 Inner Join #ServiciosSolicitados t
	 On		t.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	 And	t.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

End

GO
