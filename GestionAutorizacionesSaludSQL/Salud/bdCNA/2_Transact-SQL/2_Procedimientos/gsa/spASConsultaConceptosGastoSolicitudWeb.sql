USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaConceptosGastoSolicitudWeb]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASConsultaConceptosGastoSolicitudWeb
* Desarrollado por   : <\A Jhon W. Olarte A\>    
* Descripcion        : <\D 
                           Procedimiento expuesto que permite consultar la información asociada a los 
            		       conceptos de gasto para las prestaciones asociadas a la solicitud 
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                           cnsctvo_slctd_atrzcn_srvco     
					       nmro_slctd_atrzcn_ss  
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 31/03/2016 FC\>    
*---------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D    
                           Se modifica el pocedimiento para consultar la información asociada a los 
            		       conceptos de gasto para las meicamentos y procedimientod asociadas 
						   a la solicitud 
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/04/2016  FM\>    
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jonathan Chamizo  AM\>    
* Descripcion        : <\D Se actualiza el nombre de la razón social por el nombre de la sucursal en
						   la sesión información del prestador.
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/09/2016  FM\>   
*---------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Jorge ROdriguez  AM\>    
* Descripcion        : <\D Se adiciona ajueste para CC044 para que devuelva el historico de la liquidacion 
						   de las prestaciones que presenten una reliquidacion
                       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 06/09/2016  FM\>   
*--------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASConsultaConceptosGastoSolicitudWeb] 807, null

ALTER PROCEDURE [gsa].[spASConsultaConceptosGastoSolicitudWeb] 
  @cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
  @nmro_slctd_atrzcn_ss       Varchar(16) = Null
AS

BEGIN
  SET NOCOUNT ON

  Declare @ldFechaActual Datetime,
          @lcAux         Char(1),
		  @orgn_mdfccn	 Varchar(100)

  --  Creación de tablas temporales para la consulta de la información asociada a los conceptos de gasto
  Create 
  Table  #tmpconceptosgastos(id                                      udtConsecutivo Identity (1, 1),
                             cnsctvo_cdgo_srvco_slctdo               udtConsecutivo,
							 cnsctvo_prcdmnto_insmo_slctdo           udtConsecutivo,
							 cnsctvo_mdcmnto_slctdo                  udtConsecutivo,
                             dscrpcn_cdfccn                          udtdescripcion,
							 cnsctvo_cdgo_cncpto_gsto                udtConsecutivo,
                             dscrpcn_cncpto_gsto                     udtdescripcion,
                             nmro_unco_ops                           udtConsecutivo,
                             rzn_scl_prstdr                          udtdescripcion,
                             vlr_prstcn                              Decimal(18, 0),
                             vlr_acmldo                              Int           ,
                             fcha_imprsn                             Datetime      ,
                             fcha_utlzcn_dsde                        Datetime      ,
                             fcha_utlzcn_hsta                        Datetime      ,
							 usro_ultma_mdfccn                       udtUsuario    ,
                             cnsctvo_cdgo_estdo                      udtconsecutivo,
                             dscrpcn_estdo_csa                       udtdescripcion,
                             dscrpcn_csa                             udtdescripcion,                             
                             nmro_rdccn_ctm                          udtconsecutivo,
                             nmro_fctra                              Char(20)      ,
                             fcha_rdccn                              Datetime      ,
                             nmro_unco_idntfccn_prstdr               udtconsecutivo,
							 cnsctvo_slctd_atrzcn_srvco              udtconsecutivo,
							 cdgo_intrno_prstdr_atrzdo               udtCodigoIPS  ,
							 nmro_rdccn_cm                           udtconsecutivo,
							 cnsctvo_cdgo_ofcna_cm                   udtconsecutivo,
							 cnsctvo_cdgo_estdo_cncpto_srvco_slctdo  udtConsecutivo,
							 dscrpcn_estdo_srvco_slctdo              udtdescripcion,
							 dscrpcn_cdgo_csa_nvdd                   udtdescripcion,
							 vlr_lqdcn_antrr						 decimal(18, 0),
							 cnsctvo_srvco_slctdo					 udtConsecutivo
                           )
	
  Create 
  Table  #tmphistoricoconceptosgastos(
							 cnsctvo_cdgo_trza_mdfccn	udtconsecutivo,
							 cnsctvo_srvco_slctdo		udtconsecutivo,
							 vlr_antrr					decimal(18, 0)
							)

  Set @ldFechaActual	= getDate()
  Set @lcAux			= ' '
  Set @orgn_mdfccn		= 'tbASConceptosServicioSolicitado.ReliquidacionSolicitud'

  If @nmro_slctd_atrzcn_ss IS NOT NUll
    Begin
	    Select  @cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco		
	    From    bdCNA.gsa.tbAsSolicitudesAutorizacionServicios s WITH(NOLOCK)
	    Where   s.nmro_slctd_atrzcn_ss = @nmro_slctd_atrzcn_ss
	End

   /*Se inserta en la tabla temporal la informacion del servicio con el procedimiento*/
  Insert
  Into       #tmpconceptosgastos(cnsctvo_cdgo_srvco_slctdo             , cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo    , 
                                 vlr_acmldo                            , fcha_imprsn                  , cnsctvo_slctd_atrzcn_srvco,
								 nmro_unco_ops                         , fcha_utlzcn_dsde             , fcha_utlzcn_hsta          ,
								 vlr_prstcn                            , cnsctvo_cdgo_cncpto_gsto     , cdgo_intrno_prstdr_atrzdo ,
								 nmro_rdccn_cm                         , usro_ultma_mdfccn            , cnsctvo_cdgo_ofcna_cm     ,
								 cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_srvco_slctdo
								)
  Select     a.cnsctvo_cdgo_srvco_slctdo             , b.cnsctvo_prcdmnto_insmo_slctdo, null                       , 
             a.vlr_lqdcn_srvco                       , a.fcha_imprsn                  , @cnsctvo_slctd_atrzcn_srvco,
			 s.nmro_unco_ops                         , s.fcha_utlzcn_dsde             , s.fcha_utlzcn_hsta         ,
			 s.vlr_lqdcn                             , s.cnsctvo_cdgo_cncpto_gsto     , s.cdgo_intrno_prstdr_atrzdo,
			 s.nmro_rdccn_cm                         , s.usro_ultma_mdfccn            , s.cnsctvo_cdgo_ofcna_cm    ,
			 s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, a.cnsctvo_srvco_slctdo
  From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
  Inner Join bdcna.gsa.tbASProcedimientosInsumosSolicitados b WITH (NOLOCK)
  On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo    
  Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
  On         s.cnsctvo_prcdmnto_insmo_slctdo = b.cnsctvo_prcdmnto_insmo_slctdo
  Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
  
  /*Se inserta en la tabla temporal la informacion del servicio con el medicamento*/
  Insert
  Into       #tmpconceptosgastos(cnsctvo_cdgo_srvco_slctdo             , cnsctvo_prcdmnto_insmo_slctdo, cnsctvo_mdcmnto_slctdo    , 
                                 vlr_acmldo                            , fcha_imprsn                  , cnsctvo_slctd_atrzcn_srvco,
								 nmro_unco_ops                         , fcha_utlzcn_dsde             , fcha_utlzcn_hsta          ,
								 vlr_prstcn                            , cnsctvo_cdgo_cncpto_gsto     , cdgo_intrno_prstdr_atrzdo ,
								 nmro_rdccn_cm                         , usro_ultma_mdfccn            , cnsctvo_cdgo_ofcna_cm     ,
								 cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, cnsctvo_srvco_slctdo
								)  
  Select     a.cnsctvo_cdgo_srvco_slctdo             , null                      , b.cnsctvo_mdcmnto_slctdo   , 
             a.vlr_lqdcn_srvco                       , a.fcha_imprsn             , @cnsctvo_slctd_atrzcn_srvco,
			 s.nmro_unco_ops                         , s.fcha_utlzcn_dsde        , s.fcha_utlzcn_hsta         ,
			 s.vlr_lqdcn                             , s.cnsctvo_cdgo_cncpto_gsto, s.cdgo_intrno_prstdr_atrzdo,
			 s.nmro_rdccn_cm                         , s.usro_ultma_mdfccn       , s.cnsctvo_cdgo_ofcna_cm    ,
			 s.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo, a.cnsctvo_srvco_slctdo
  From       bdcna.gsa.tbASServiciosSolicitados a WITH (NOLOCK)
  Inner Join bdcna.gsa.tbASMedicamentosSolicitados b WITH (NOLOCK)
  On         b.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo  
  Inner Join bdcna.gsa.tbASConceptosServicioSolicitado s WITH (NOLOCK)
  On         s.cnsctvo_mdcmnto_slctdo = b.cnsctvo_mdcmnto_slctdo
  Where      a.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco

   /*Se actualiza la descripcion del servicio*/
  Update     a
  Set        dscrpcn_cdfccn = c.dscrpcn_cdfccn
  From       #tmpconceptosgastos a
  Inner Join bdsisalud.dbo.tbcodificaciones c WITH (NOLOCK)
  On         c.cnsctvo_cdfccn = a.cnsctvo_cdgo_srvco_slctdo

  /*Se actualiza la descripcion del concepto de gasto*/
  Update     a
  Set        dscrpcn_cncpto_gsto = b.dscrpcn_cncpto_gsto
  From       #tmpconceptosgastos a
  Inner Join bdSiSalud.dbo.tbConceptosGasto_Vigencias b WITH (NOLOCK)
  On         b.cnsctvo_cdgo_cncpto_gsto = a.cnsctvo_cdgo_cncpto_gsto
  Where      @ldFechaActual BETWEEN b.inco_vgnca AND b.fn_vgnca

  
  /*Se actualiza la informacion del prestador*/
  Update     a
  Set        nmro_unco_idntfccn_prstdr = p.nmro_unco_idntfccn_prstdr,
             rzn_scl_prstdr            = p.nmbre_scrsl
  From       #tmpconceptosgastos a 
  Inner Join bdSisalud.dbo.tbDireccionesPrestador p WITH(NOLOCK)
  ON         p.cdgo_intrno = a.cdgo_intrno_prstdr_atrzdo 	   
  Left Join  bdSisalud.dbo.tbips i WITH(NOLOCK)
  ON         i.nmro_unco_idntfccn_prstdr = p.nmro_unco_idntfccn_prstdr
  Where      @ldFechaActual Between p.inco_vgnca and p.fn_vgnca      
  
  
  --Valida si la razon social es una persona natural
  UPDATE     #tmpconceptosgastos
  SET        rzn_scl_prstdr = CONCAT(LTRIM(RTRIM(m.prmr_nmbre_afldo))  , @lcAux, 
	                                 LTRIM(RTRIM(m.sgndo_nmbre_afldo)) , @lcAux, 
							         LTRIM(RTRIM(m.prmr_aplldo))       , @lcAux, 
							         LTRIM(RTRIM(m.sgndo_aplldo))
							        ) 
  From       #tmpconceptosgastos ps
  Inner Join bdSisalud.dbo.tbmedicos m WITH(NOLOCK)
  On         m.nmro_unco_idntfccn_prstdr = ps.nmro_unco_idntfccn_prstdr	
  Where      ps.rzn_scl_prstdr IS NULL

  --Se actualiza el estado de la ops
  Update     #tmpconceptosgastos
  Set        dscrpcn_estdo_srvco_slctdo = b.dscrpcn_estdo_srvco_slctdo
  From       #tmpconceptosgastos a
  Inner Join bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias b WITH (NOLOCK)
  On         b.cnsctvo_cdgo_estdo_srvco_slctdo = a.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
  Where      @ldFechaActual Between b.inco_vgnca and b.fn_vgnca  

  --Se actualiza causa novedad de la prestación de la tabla Traza Modificacion
  Update	 #tmpconceptosgastos 
  Set		 dscrpcn_cdgo_csa_nvdd = c.dscrpcn_cdgo_csa_nvdd
  From		 #tmpconceptosgastos a
  Inner Join BDCna.gsa.tbASTrazaModificacion b WITH (NOLOCK)
  On		 a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
  And        a.cnsctvo_cdgo_srvco_slctdo  = b.cnsctvo_cncpto_prcdmnto_slctdo
  Inner Join BDCna.prm.tbASCausaNovedad_Vigencias c  WITH (NOLOCK)
  On         b.cnsctvo_cdgo_csa_nvdd      = c.cnsctvo_cdgo_csa_nvdd
  Where      @ldFechaActual Between c.inco_vgnca and c.fn_vgnca

  --Se actualiza el valor anterior de la liquidación de las prestaciones que fueron reliquidadas.
   insert into #tmphistoricoconceptosgastos
   (
	cnsctvo_cdgo_trza_mdfccn,	
	cnsctvo_srvco_slctdo,	
	vlr_antrr
	)
	select  max(t.cnsctvo_cdgo_trza_mdfccn) cnsctvo_cdgo_trza_mdfccn ,  
			t.cnsctvo_srvco_slctdo, 
			t.vlr_antrr 
	from BDCna.gsa.tbASTrazaModificacion t WITH (NOLOCK)
	Inner Join #tmpconceptosgastos c
	On c.cnsctvo_srvco_slctdo = t.cnsctvo_srvco_slctdo
	where t.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	and t.orgn_mdfccn = @orgn_mdfccn
	group  by t.cnsctvo_srvco_slctdo , t.vlr_antrr
	order by t.cnsctvo_srvco_slctdo desc

	Update c
	Set vlr_lqdcn_antrr	= hc.vlr_antrr
	From #tmpconceptosgastos c
    Inner Join  #tmphistoricoconceptosgastos hc
	On c.cnsctvo_srvco_slctdo	=  hc.cnsctvo_srvco_slctdo


  --Se retorna el resultado de la consulta.
  SELECT  cnsctvo_cdgo_srvco_slctdo , dscrpcn_cdfccn       , dscrpcn_cncpto_gsto  ,
          nmro_unco_ops             , rzn_scl_prstdr       , vlr_prstcn           ,
          vlr_acmldo                , fcha_imprsn          , fcha_utlzcn_dsde     ,
          fcha_utlzcn_hsta          , nmro_rdccn_cm        , cnsctvo_cdgo_ofcna_cm,
		  dscrpcn_estdo_srvco_slctdo, dscrpcn_cdgo_csa_nvdd, usro_ultma_mdfccn	  ,
		  cdgo_intrno_prstdr_atrzdo , cnsctvo_cdgo_estdo_cncpto_srvco_slctdo	  ,
		  vlr_lqdcn_antrr			
  FROM    #tmpconceptosgastos

  --Se borran las tablas temporales.
  DROP TABLE #tmpconceptosgastos
  DROP TABLE #tmphistoricoconceptosgastos
End

GO
