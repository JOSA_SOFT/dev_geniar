USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASCalcularResultadoDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASCalcularResultadoDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite calcular el resultado del direccionamiento D\>    
					   <\D Determinado por los posibles prestadores, suficiencia y nivel D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASCalcularResultadoDireccionamiento]
@es_btch char(1)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @cteUno int

  SELECT
    @cteUno = 1

  --Se valida si hay prestaciones que tengan asociado más de un posible prestador.
  IF EXISTS (SELECT
      id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino TPD
    GROUP BY TPD.id_slctd_x_prstcn
    HAVING COUNT(id) > @cteUno)
  BEGIN

    --Se establece las de mayor prioridad y suficiencia, quedando en la fila 1
    WITH tmp
    AS (SELECT
      ROW_NUMBER() OVER (PARTITION BY id_slctd_x_prstcn ORDER BY TPD.nvl_prrdd DESC, TPD.sfcnca_ips DESC) AS "fila",
      TPD.id
    FROM #tmpPrestadoresDestino TPD)
    DELETE TPD
      FROM #tmpPrestadoresDestino TPD
      INNER JOIN tmp TP
        ON TPD.id = TP.id
    WHERE TP.fila <> @cteUno
  END

  --Se actualiza el prestador destino a la temporal principal
  UPDATE #tmpInformacionSolicitudPrestacion
  SET cdgo_intrno_ips_dstno = TPD.cdgo_intrno
  FROM #tmpInformacionSolicitudPrestacion TIS
  INNER JOIN #tmpPrestadoresDestino TPD
    ON TPD.id_slctd_x_prstcn = TIS.id
END

GO
