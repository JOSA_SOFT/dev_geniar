USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASRegistrarValidacionEspecialWeb]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASRegistrarValidacionEspecialWeb
* Desarrollado por   : <\A Ing. Victor Hugo Gil Ramos  A\>    
* Descripcion        : <\D 
                           Procedimiento que permite registrar la validacion especial realizada para la 
						   descarga de autorizacion de servicios
					   D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P 
                            @cnsctvo_slctd_atrzcn_srvco      udtConsecutivo              ,
							@cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo              ,
							@nmro_idntfccn_afldo             udtNumeroIdentificacionLargo,
							@nmro_vldcn_espcl                Numeric(18,0)               ,
							@cnsctvo_cdgo_ofcna              udtConsecutivo              ,
							@usro_vldcn_espcl                udtUsuario                  ,
							@accn_vldcn_espcl                udtDescripcion              ,
							@nmro_unco_ops                   udtConsecutivo              ,
							@fcha_vldcn_espcl                Datetime                    ,
							@usro_crcn                       udtUsuario                  ,
							@nmro_unco_idntfccn_afldo        udtConsecutivo  					         
					   P\>
* Variables          : <\V             V\>    
* Fecha Creacion     : <\FC 26/04/2016 FC\>    
*---------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM AM\>    
* Descripcion        : <\D  D\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM FM\>    
*---------------------------------------------------------------------------------------------------------------------------------------*/
 --exec [gsa].[spASRegistrarValidacionEspecialWeb] null, null, null, null, null, null, null, null, null, null, null 

ALTER PROCEDURE [gsa].[spASRegistrarValidacionEspecialWeb] 
  @cnsctvo_slctd_atrzcn_srvco      udtConsecutivo              ,
  @cnsctvo_cdgo_tpo_idntfccn_afldo udtConsecutivo              ,
  @nmro_idntfccn_afldo             udtNumeroIdentificacionLargo,
  @nmro_vldcn_espcl                Numeric(18,0)               ,
  @cnsctvo_cdgo_ofcna              udtConsecutivo              ,
  @usro_vldcn_espcl                udtUsuario                  ,
  @accn_vldcn_espcl                udtDescripcion              ,
  @nmro_unco_ops                   udtConsecutivo              ,
  @fcha_vldcn_espcl                Datetime                    ,
  @usro_crcn                       udtUsuario                  ,
  @nmro_unco_idntfccn_afldo        udtConsecutivo

AS

Begin
   SET NOCOUNT ON

   Declare @ldFechaActual  Datetime 
   
   Set @ldFechaActual = getDate()

   Insert 
   Into  bdCNA.gsa.tbASRegistroValidacionEspecial(cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_idntfccn_afldo, nmro_idntfccn_afldo,
					                              nmro_vldcn_espcl          , cnsctvo_cdgo_ofcna             , usro_vldcn_espcl   ,
					                              accn_vldcn_espcl          , cnsctvo_ops                    , fcha_vldcn_espcl   ,
					                              usro_crcn                 , fcha_crcn                      , usro_ultma_mdfccn  ,
												  fcha_ultma_mdfccn         , nmro_unco_idntfccn_afldo       
												 )
   Values(@cnsctvo_slctd_atrzcn_srvco, @cnsctvo_cdgo_tpo_idntfccn_afldo, @nmro_idntfccn_afldo,
          @nmro_vldcn_espcl          , @cnsctvo_cdgo_ofcna             , @usro_vldcn_espcl   ,
		  @accn_vldcn_espcl          , @nmro_unco_ops                  , @fcha_vldcn_espcl   , 
		  @usro_crcn                 , @ldFechaActual                  , @usro_crcn          ,
		  @ldFechaActual             , @nmro_unco_idntfccn_afldo
         )
End

GO
