USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizaDatosImplementacionExternaBPM]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultaDatosInformacionRequiereOtroAuditor
* Desarrollado por		: <\A Jonathan - SETI SAS  							    A\>
* Descripcion			: <\D Se actualiza el histórico del servicio solicitado
							  con base en la actualización del campo
							  cnsctvo_cdgo_estdo_srvco_slctdo   				D\>
						  <\D 													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P													 \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/04/25									   FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM													AM\>
* Descripcion			 : <\DM													DM\>
* Nuevos Parametros	 	 : <\PM													PM\>
* Nuevas Variables		 : <\VM													VM\>
* Fecha Modificacion	 : <\FM													FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizaDatosImplementacionExternaBPM]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
@id_tsk_prcss				udtConsecutivo

AS
BEGIN

	SET NOCOUNT ON;

	Update sas set nmro_prcso_instnca = @id_tsk_prcss
	From BDCna.gsa.tbASSolicitudesAutorizacionServicios sas
    Where sas.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;
		
END

GO
