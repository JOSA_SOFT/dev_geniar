USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaTiposDocumentoSoportePresolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('[gsa].[spASConsultaTiposDocumentoSoportePresolicitud]') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE [gsa].[spASConsultaTiposDocumentoSoportePresolicitud] AS SELECT 1;'
END
GO

/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultaTiposDocumentoSoporte  
* Desarrollado por : <\A Ing. Victor Hugo Gil Ramos   A\>    
* Descripcion      : <\D 
                         Procedimiento que permite consultar los tipos de documento soporte por prestacion o item de 
						 presupuesto
                      D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 27/07/2016 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\D 
                        Se realizo modificacion del procedimiento para que consulte los documentos de donde se toman
						para mi solicitud
                       D\>
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 01/08/2016  FM\>   
*-----------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaTiposDocumentoSoportePresolicitud] 
@cnsctvo_cdgo_mdlo_dcmnto_sprte udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

	Declare @ldFechaActual  Datetime ;
	
	Set @ldFechaActual  = getDate();  


	Select Distinct  det.cnsctvo_cdgo_mdlo_dcmnto_sprte, doc.cnsctvo_cdgo_dcmnto_sprte,
		             doc.cdgo_dcmnto_sprte             , doc.dscrpcn_dcmnto_sprte          , doc.tmno_mxmo                ,
					 doc.url_ejmplo                    , det.frmts_prmtdo                  , det.oblgtro                  ,
					 ISNULL(det.cntdd_mxma_sprts_crgr,1) cntdd_mxma_sprts_crgr, doc.cnsctvo_cdgo_tpo_imgn  
		  From       bdCNA.sol.tbDocumentosSoporte_Vigencias doc With(NoLock)
		  Inner Join bdSisalud.dbo.tbDetModeloDocumentosSoporte3047 det With(NoLock) 
		  On         det.cnsctvo_cdgo_dcmnto_sprte = doc.cnsctvo_cdgo_dcmnto_sprte 
		  Where      det.cnsctvo_cdgo_mdlo_dcmnto_sprte = @cnsctvo_cdgo_mdlo_dcmnto_sprte
		  And        @ldFechaActual Between det.inco_vgnca And det.fn_vgnca
		  And        @ldFechaActual Between doc.inco_vgnca And doc.fn_vgnca
END
GO
