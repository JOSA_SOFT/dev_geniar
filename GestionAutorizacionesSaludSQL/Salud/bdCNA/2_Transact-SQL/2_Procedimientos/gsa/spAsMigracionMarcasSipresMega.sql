USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsMigracionMarcasSipresMega]    Script Date: 31/01/2017 03:25:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

If(Object_id('gsa.spAsMigracionMarcasSipresMega') Is Null )
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsMigracionMarcasSipresMega As Select 1';
End
Go

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsMigracionMarcasSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Consulta las marcas de tbMarcasXAtencionOPS y consulta los homologos para
									migrarlos a mega D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  
							  P\>  													
* Variables					: <\V  
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.spAsMigracionEstadosSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se quita la instruccion "And cs.cnsctvo_ops = c.cnsctvo_ops" ya que en mega no se esta 
							   calculando este valor y se organizan consultas  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsMigracionMarcasSipresMega]
As
Begin
	
	Set NoCount On

	Declare @fcha_actl					Date;

	-- Tabla donde se cargan las marcas
	Create Table #tempMarcasServiciosSolicitados
	(
		cnsctvo_cdgo_tpo_mrca			UdtConsecutivo,
		cnsctvo_cdgo_tps_mrcs_ops		UdtConsecutivo,
		cnsctvo_atncn_ops				UdtConsecutivo,
		cnsctvo_srvco_slctdo			UdtConsecutivo,
		inco_vgnca						Date,
		fn_vgnca						Date,
		fcha_crcn						Date,
		fcha_fn_vldz_rgstro				Date,
		vldo							UdtLogico,
		usro_crcn						UdtUsuario,
		usro_ultma_mdfccn				UdtUsuario
	);

	Set @fcha_actl = getDate();

	-- Se llena la tabla temporal
	Insert Into #tempMarcasServiciosSolicitados
	(
				cnsctvo_cdgo_tps_mrcs_ops,					cnsctvo_atncn_ops,				inco_vgnca,									
				fn_vgnca,									fcha_crcn,						fcha_fn_vldz_rgstro,						
				vldo,										usro_crcn,						usro_ultma_mdfccn
	)
	Select		cnsctvo_cdgo_tps_mrcs_ops,					mao.cnsctvo_atncn_ops,			inco_vgnca,									
				fn_vgnca,									fcha_crcn,						fcha_fn_vldz_rgstro,						
				vldo,										usro_crcn,						usro_ultma_mdfccn
	From		BdCna.dbo.tbMarcasXAtencionOPS mao With(NoLock)
	Inner Join	#tempEstadosMigrar em
	On			em.cnsctvo_atncn_ops = mao.cnsctvo_atncn_ops

	-- Se obtiene la marca homologa
	Update		mss
	Set			cnsctvo_cdgo_tpo_mrca = tmssv.cnsctvo_cdgo_tpo_mrca
	From		#tempMarcasServiciosSolicitados mss
	Inner Join	prm.tbASTipoMarcaServiciosSolicitados_Vigencias tmssv With(NoLock)
	On			mss.cnsctvo_cdgo_tps_mrcs_ops = tmssv.cnsctvo_cdgo_tps_mrcs_ops
	Where		@fcha_actl Between tmssv.inco_vgnca And tmssv.fn_vgnca

	-- Se obtiene el consecutivo del servicio
	Update		mss
	Set			cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	From		#tempMarcasServiciosSolicitados mss
	Inner Join	#tempEstadosMigrar em
	On			em.cnsctvo_atncn_ops = mss.cnsctvo_atncn_ops
	Inner Join	gsa.tbASServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = em.cnsctvo_slctd_mga
	Inner Join	gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_prcdmnto_insmo_slctdo = em.cnsctvo_prcdmnto_insmo_slctdo
	And			ss.cnsctvo_srvco_slctdo = pis.cnsctvo_srvco_slctdo
	
	-- Se insertan los valores en la tabla 
	Merge gsa.tbASMarcasServiciosSolicitados With(RowLock) As t
	Using (	Select	cnsctvo_srvco_slctdo,	cnsctvo_cdgo_tpo_mrca,					fcha_crcn,
					usro_crcn,				@fcha_actl fcha_ultma_mdfccn,			usro_ultma_mdfccn
			From	#tempMarcasServiciosSolicitados
			Where	cnsctvo_cdgo_tpo_mrca Is Not Null
	) As s
	On(	t.cnsctvo_srvco_slctdo = s.cnsctvo_srvco_slctdo		And
		t.cnsctvo_cdgo_tpo_mrca = s.cnsctvo_cdgo_tpo_mrca
	)
	When Not Matched Then
		Insert 
		(
			cnsctvo_srvco_slctdo,	cnsctvo_cdgo_tpo_mrca,		fcha_crcn,
			usro_crcn,				fcha_ultma_mdfccn,			usro_ultma_mdfccn
		)
		Values(
			s.cnsctvo_srvco_slctdo,	s.cnsctvo_cdgo_tpo_mrca,	s.fcha_crcn,
			s.usro_crcn,			s.fcha_ultma_mdfccn,		s.usro_ultma_mdfccn
		);

	Drop Table #tempMarcasServiciosSolicitados;

	
End