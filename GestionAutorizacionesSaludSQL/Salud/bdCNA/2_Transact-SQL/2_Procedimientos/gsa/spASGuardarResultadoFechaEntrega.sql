USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGuardarResultadoFechaEntrega]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarResultadoFechaEntrega
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite guardar la fecha de entrega acorde al grupo   D\>
					   <\D asignado a las prestaciones de fecha esperada de entrega	 D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGuardarResultadoFechaEntrega] @cnsctvo_prcso udtConsecutivo,
@es_btch char(1)
AS

BEGIN
  SET NOCOUNT ON
  DECLARE @fechaActual datetime

  SET @fechaActual = GETDATE()

  --Se almacena la información resultado
  MERGE [BDCna].[gsa].[tbASResultadoFechaEntrega] WITH (ROWLOCK) AS target
  USING (SELECT
    m.cnsctvo_slctd_srvco_sld_rcbda,
    m.cnsctvo_srvco_slctdo,
    m.cnsctvo_grpo_entrga_fnl cnsctvo_grpo_entrga,
    m. fcha_estmda_entrga_fnl fcha_estmda_entrga,
    m.usro
  FROM #tmpDatosSolicitudFechaEntrega m) AS source (cnsctvo_slctd_srvco_sld_rcbda, cnsctvo_srvco_slctdo, cnsctvo_grpo_entrga, fcha_estmda_entrga, usro)
  ON (target.cnsctvo_slctd_atrzcn_srvco = source.cnsctvo_slctd_srvco_sld_rcbda
  AND target.cnsctvo_srvco_slctdo = source.cnsctvo_srvco_slctdo)
  WHEN MATCHED THEN
  UPDATE SET cnsctvo_cdgo_grpo_entrga = source.cnsctvo_grpo_entrga,
  fcha_estmda_entrga = source.fcha_estmda_entrga,
  fcha_ultma_mdfccn = @fechaActual,
  usro_ultma_mdfccn = source.usro
  WHEN NOT MATCHED THEN
  INSERT (cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo, cnsctvo_cdgo_grpo_entrga, fcha_estmda_entrga, fcha_crcn, usro_crcn, fcha_ultma_mdfccn, usro_ultma_mdfccn)
  VALUES (source.cnsctvo_slctd_srvco_sld_rcbda, source.cnsctvo_srvco_slctdo, source.cnsctvo_grpo_entrga, source.fcha_estmda_entrga, @fechaActual, source.usro, @fechaActual, source.usro)
  ;
END

GO
