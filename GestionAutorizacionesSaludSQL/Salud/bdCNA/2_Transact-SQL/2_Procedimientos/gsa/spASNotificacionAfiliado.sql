USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASNotificacionAfiliado]    Script Date: 09/03/2017 9:59:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASNotificacionAfiliado
* Desarrollado por		: <\A Jonathan Chamizo - SETI SAS  A\>
* Descripcion			: <\D 
                              Orquesta los procedimientos encargados de obtener y procesar 
                              el envío de correo a los afiliados											
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2016/03/25 FC\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez De León	AM\>
* Descripcion			 : <\DM 
                                Se adicionan campos a la tabla temporal que se utilizaran
								para la generación de la plantillas				 	
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2016/03/25 FM\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos	AM\>
* Descripcion			 : <\DM 
                                Se modifica procedimiento adicionando el campo cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
								en la tabla temporal con el fin de poder consultar el email del afiliado asociado a una
								solicitud 				 	
							DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	2017/03/09 FM\>
*------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASNotificacionAfiliado] 
	@slctdes		XML,
	@usrio			VARCHAR(50),
	@estdo_ejccn	VARCHAR(2) OUTPUT,
	@msje_rspsta	NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	Declare	@cnstrccn_trma_xml	XML,
	        @codigoExito		Char(2),
			@codigoError		Char(2),
			@valorCero			Int

	DECLARE @rspsta_xml	TABLE (rspsta XML NOT NULL);	

	--Temporal con los datos básicos de la solicitud
	Create 
	Table #tmpSlctdes(id_sld_rcbda                  Int Identity (1,1),
					  cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo	  ,
					  nmro_slctd					Varchar(16)       ,
					  plntlla						Int			
					 )

	--Temporal con los datos necesario del afiliado
	Create 
	Table  #tmpAflds(id                                       Int Identity (1,1) ,
					 id_sld_rcbda					          udtConsecutivo	 ,				
					 cnsctvo_slctd_srvco_sld_rcbda	          udtConsecutivo	 ,				
					 nmro_slctd_atrzcn_ss			          Varchar(16)		 ,				
					 nmbre_afldo					          Varchar(100)		 ,			
					 cnsctvo_cdgo_tpo_idntfccn_afldo          udtConsecutivo	 ,				
					 nmro_idntfccn_afldo			          udtNumeroIdentificacionLargo,	
					 cnsctvo_cdgo_tpo_cntrto		          udtConsecutivo	 ,				
					 nmro_cntrto					          udtNumeroFormulario,				
					 cnsctvo_cdgo_pln				          udtConsecutivo	 ,				
					 nmro_unco_idntfccn_afldo		          udtConsecutivo	 ,				
					 cnsctvo_cdgo_rngo_slrl			          udtConsecutivo	 ,			
					 cdgo_ips_prmra				              udtCodigoIps		 ,					
					 cnsctvo_bnfcro_cntrto			          udtConsecutivo	 ,			
					 cnsctvo_cdgo_sde_ips_prmra		          udtConsecutivo	 ,			
					 cnsctvo_cdgo_estdo_drcho		          udtConsecutivo	 ,			
					 dscrpcn_csa_drcho				          udtDescripcion	 ,			
					 cdgo_tpo_idntfccn_afldo		          udtCodigo			 ,			
					 dscrpcn_pln					          udtDescripcion	 ,			
					 eml							          udtEmail			 ,		
					 sde_afldo						          udtCodigo			 , 		
					 dscrpcn_sde					          udtDescripcion	 ,				
					 bcc							          udtEmail		     ,				
				     lnea_ncnal						          udtDescripcion	 ,				
					 crreo_atncn_clnte				          udtEmail           ,	
					 cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco udtConsecutivo			
					)

	--Temporal con los datos necesario del proveedor
	Create 
	Table #tmpPrvdor(id			  Int Identity(1,1),	
					 id_sld_rcbda Int			   ,
					 crreo		  udtEmail		  
					)
    
	Set @codigoExito = 'OK'
	Set @codigoError = 'ET'
	Set @valorCero	 =  0

	/*Poblar tabla temporal Solicitudes*/
	Insert 
	Into    #tmpSlctdes(cnsctvo_slctd_srvco_sld_rcbda, nmro_slctd, plntlla)
	SELECT	ISNULL(CAST(dta.query('data(cnsctvo_slctd_srvco_sld)') AS VARCHAR(150)), @valorCero),
			CAST(dta.query('data(nmro_slctd)') AS VARCHAR(16)),
			ISNULL(CAST(dta.query('data(plntlla)') AS VARCHAR(10)), @valorCero)
	From    @slctdes.nodes('info_slctds/cdgos_slctds') AS slctdes(dta);		

	--Se actualiza solo si no se envía el consecutivo de la solicitud, pero si se
	--envía el número de radicación SOS
	Update    TSO
	Set       cnsctvo_slctd_srvco_sld_rcbda = SAS.cnsctvo_slctd_atrzcn_srvco
	From      #tmpSlctdes TSO 
	Left Join bdCNA.tbASSolicitudesAutorizacionServicios SAS WITH (NOLOCK)
	On        SAS.nmro_slctd_atrzcn_ss = TSO.nmro_slctd
	Where     TSO.nmro_slctd IS NOT NULL
	And       TSO.cnsctvo_slctd_srvco_sld_rcbda = @valorCero;		
	
	BEGIN TRY
			
		/*Obtener y cargar en la tabla temporal la información del afiliado*/
		EXEC gsa.spASObtenerDatosNotificacionAfiliado;

		/*Construir la trama XML que será enviada al sp encargado de guardar la norificación
			del afiliado*/
		EXEC gsa.spASConstruirTramaXMLNotificacionAfiliado @usrio, @cnstrccn_trma_xml OUTPUT;

		INSERT INTO @rspsta_xml
		EXEC bdProcesosSalud.dbo.spEMGuardarMensajesCorreoMasivo @cnstrccn_trma_xml;					
				
		SELECT @msje_rspsta = CAST(rspsta AS NVARCHAR(MAX)) FROM @rspsta_xml;	
		SET @estdo_ejccn = @codigoExito

	END TRY
	BEGIN CATCH

		SET @msje_rspsta =	'Number:' + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
							'Line:' + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
							'Message:' + ERROR_MESSAGE() + CHAR(13) +
							'Procedure:' + ERROR_PROCEDURE();
		SET @estdo_ejccn = @codigoError
			
	END CATCH
		
	DROP TABLE #tmpAflds
	DROP TABLE #tmpPrvdor
	DROP TABLE #tmpSlctdes
							
END

