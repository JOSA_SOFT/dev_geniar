USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASActualizaEstados3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASActualizaEstados3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASActualizaEstados3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Se encarga de actualizar los estados de las solicitudes guardadas. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/07/13 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASActualizaEstados3047 
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM se cambia la tabla temporal origen por la tabla #solicitudes_guardadas  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/07/19 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASActualizaEstados3047
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @estdo_aprbda udtConsecutivo
	SET @estdo_aprbda = 7

	UPDATE a
	SET a.cnsctvo_cdgo_estdo_slctd = @estdo_aprbda
	FROM gsa.tbASSolicitudesAutorizacionServicios a with(nolock)
	INNER JOIN #solicitudes_guardadas b ON a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco

	UPDATE a
	SET a.cnsctvo_cdgo_estdo_srvco_slctdo = @estdo_aprbda
	FROM gsa.tbASServiciosSolicitados a with(nolock)
	INNER JOIN #solicitudes_guardadas b ON a.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco
END
GO
