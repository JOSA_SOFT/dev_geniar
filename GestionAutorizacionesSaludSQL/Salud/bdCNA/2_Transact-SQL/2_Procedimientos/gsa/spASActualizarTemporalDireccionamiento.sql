USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarTemporalDireccionamiento]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASActualizarTemporalDireccionamiento
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite actualizar y borrar los prestadores que no  D\>    
					   <\D cumplan los críterios para ser direccionada							 D\>
* Observaciones      : <\O      O\>    
* Parametros         : <\P	    P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 22/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASActualizarTemporalDireccionamiento]
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @negacion char(1) = 'N',
		  @cmple	char(1) = 'S',
		  @vlor_cro int		= 0;
	--Se borran los prestadoresxprestacion que no cumplan la condición, y además ya se tengan asociados posibles prestadores para
    --esas prestaciones
    WITH tmp AS (SELECT
        TCO.id_slctd_x_prstcn
      FROM #tmpPrestadoresDestino TCO
      INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
        ON ISP.id = TCO.id_slctd_x_prstcn
        AND TCO.cmple = @cmple
		WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
	DELETE FROM z
	FROM #tmpPrestadoresDestino z
	INNER JOIN tmp x 
	ON z.id_slctd_x_prstcn = x.id_slctd_x_prstcn
	WHERE z.cmple = @negacion;

    --Se reinician todos nuevamente a no cumplen
    UPDATE #tmpPrestadoresDestino
    SET cmple = @negacion
END

GO
