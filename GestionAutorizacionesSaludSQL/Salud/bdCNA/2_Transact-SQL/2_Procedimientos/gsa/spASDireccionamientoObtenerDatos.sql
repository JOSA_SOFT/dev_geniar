USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASDireccionamientoObtenerDatos]    Script Date: 01/08/2017 11:35:30 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASDireccionamientoObtenerDatos
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que llena las tablas temporales de información para calculo de D\>
					   <\D Direccionamiento												 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*----------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jorge Rodriguez AM\>    
* Descripcion        : <\D  Se modifica sp para que los planes familiar y Quimbaya tengan el mismo 
							direccionamiento del POS según las reglas establecidas actualmente.       D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-11-22  FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Jorge Rodriguez AM\>    
* Descripcion        : <\D  Se hacen ajustes al sp spASDireccionamientoObtenerDatos para que cambie el 
							plan en las tablas temporales del afiliado a POS cuando se trate de un plan 
							Familiar o Quimbaya       
						D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-27  FM\>    
*----------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Angela Sandoval AM\>    
* Descripcion        : <\D  
                           Se realiza ajuste en el procedimiento para que no direccione a ciudades diferentes a la de la sede del 
						   afiliado      
						D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-11  FM\>    
*----------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASDireccionamientoObtenerDatos] @usro udtUsuario
AS
BEGIN
  SET NOCOUNT ON

    DECLARE @fechaActual datetime = GETDATE(),
            @vldo char(1) = 'S',
			@vlrCero int = 0,
            @consecutivoTipoDiagnosticoPrincipal int = 1,
			@consecutivoTiempoBusqueda int = 85,
			@valorTiempoBusquedaMeses numeric(18,0),
			@mrca_msmo_prstdor int = 1, --Se marca el mismo prestador de Hospitalización o Afiliado PAC
			@pln_pac	int	= 2, --tipo plan pac
			@pln_fmlr	int	= 2, --plan familiar
			@pln_qmbya	int	= 6, --plan quimbaya
			@tpo_pln_ps	int	= 1; --Tipo Plan Pos



  --Se consulta el parámetro del tiempo máximo de busqueda de prestaciones anteriores.
	SELECT
      @valorTiempoBusquedaMeses = PG.vlr_prmtro_nmrco
    FROM bdSisalud.dbo.tbParametrosGenerales_vigencias PG WITH (NOLOCK)
    WHERE PG.cnsctvo_cdgo_prmtro_gnrl = @consecutivoTiempoBusqueda
    AND @fechaActual BETWEEN PG.inco_vgnca AND PG.fn_vgnca;

    --Carga toda la informacion de cada solicitud y prestacion
    INSERT INTO #tmpInformacionSolicitudPrestacion (cnsctvo_cdgo_prstcn
    , cdgo_intrno_ips_slctnte
    , cdgo_prstcn
    , cnsctvo_slctd
    , cnsctvo_srvco
    , pln_prstcn
    , nmro_id_pcnte
    , cnsctvo_tpo_id_pcnte
    , nmro_slctd
    , nmro_prvdr
    , ubccn_pcnte
    , clse_atncn_orgn
    , clse_atncn
    , ni_pcnte
    , cnsctvo_cdgo_cdd_afldo
    , cdgo_zna
    , cnsctvo_sde_ips_afldo
    , cdgo_intrno_ips_afldo_prmra
    , cnsctvo_cdgo_cdd_prmra
    , edd_afldo
    , cnsctvo_rsgo_prstcn
    , cdgo_intrno_ips_dstno
    , vlr_trfdo
    , sfcnca_ms
    , sfcnca_ips
    , cnsctvo_dtlle_lsta
    , cnsctvo_cnfgrcn
    , fcha_imprsn
    , cdgo_intrno_prstdr_antrr
    , cntdd_ds_mx
    , usro
    , cnsctvo_dgnstco_prncpl
    , cnsctvo_cdgo_tpo_cntrto
    , nmro_cntrto
    , cnsctvo_cdgo_tpo_idntfccn_afldo
    , nmro_idntfccn
	, fcha_mnma_bsqda
	, cnsctvo_cdgo_tpo_pln
	, cnsctvo_cdgo_frma_atncn)
      SELECT
        SER.cnsctvo_cdgo_srvco_slctdo,
        IPS.cdgo_intrno,
        SRO.cdgo_srvco_slctdo,
        TPR.cnsctvo_slctd_srvco_sld_rcbda,
        SER.cnsctvo_srvco_slctdo,
        AFI.cnsctvo_cdgo_pln,
        AFI.nmro_idntfccn_afldo,
        AFI.cnsctvo_cdgo_tpo_idntfccn_afldo,
        SOL.nmro_slctd_atrzcn_ss,
        SOL.nmro_slctd_prvdr,
        SOL.cnsctvo_cdgo_tpo_ubccn_pcnte,
        SOL.cnsctvo_cdgo_clse_atncn, -- Consecutivo clase de atención original
        @vlrCero,	--Consecutivo clase de atención homologada para el procesamiento.
        AFI.nmro_unco_idntfccn_afldo,
        AFI.cnsctvo_cdgo_cdd_rsdnca_afldo, -- Consecutivo ciudad residencia afiliado.
        @vlrCero,								   --Zona de la ips primaria del afiliado
        AFI.cnsctvo_cdgo_sde_ips_prmra,
        AFI.cdgo_ips_prmra,
        @vlrCero,
        AFI.edd_afldo_ans,
        NULL,
        NULL,
        @vlrCero,
        @vlrCero,
        @vlrCero,
        NULL,
        @vlrCero,
        NULL,
        NULL,
        NULL,
        @usro,
        DSA.cnsctvo_cdgo_dgnstco,
        AFI.cnsctvo_cdgo_tpo_cntrto,
        AFI.nmro_cntrto,
        AFI.cnsctvo_cdgo_tpo_idntfccn_afldo,
        AFI.nmro_idntfccn_afldo,
		DATEADD(month,-@valorTiempoBusquedaMeses,@fechaActual),
		AFI.cnsctvo_cdgo_tpo_pln,
		SOL.cnsctvo_cdgo_frma_atncn
      FROM BDCna.gsa.tbASServiciosSolicitados SER WITH (NOLOCK)
      INNER JOIN #tmpNmroSlctds TPR WITH (NOLOCK)
        ON TPR.cnsctvo_slctd_srvco_sld_rcbda = SER.cnsctvo_slctd_atrzcn_srvco
      INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios SOL WITH (NOLOCK)
        ON SOL.cnsctvo_slctd_atrzcn_srvco = SER.cnsctvo_slctd_atrzcn_srvco
      LEFT JOIN BDCna.gsa.tbASServiciosSolicitadosOriginal SRO WITH (NOLOCK)
        ON SER.cnsctvo_srvco_slctdo = SRO.cnsctvo_srvco_slctdo
      INNER JOIN BDCna.gsa.tbASIPSSolicitudesAutorizacionServicios IPS WITH (NOLOCK)
        ON SER.cnsctvo_slctd_atrzcn_srvco = IPS.cnsctvo_slctd_atrzcn_srvco
      INNER JOIN BDCna.gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios AFI WITH (NOLOCK)
        ON SER.cnsctvo_slctd_atrzcn_srvco = AFI.cnsctvo_slctd_atrzcn_srvco
      LEFT JOIN BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios DSA WITH (NOLOCK)
        ON SER.cnsctvo_slctd_atrzcn_srvco = DSA.cnsctvo_slctd_atrzcn_srvco
        AND DSA.cnsctvo_cdgo_tpo_dgnstco = @consecutivoTipoDiagnosticoPrincipal;

    --Se actualiza información adicional asociada al prestador y tipo, clase atención.
    UPDATE ISP
    SET clse_atncn = CA.cnsctvo_cdgo_clsfccn_atncn, --Clasificación de la atención.
        cdgo_zna = DPR.cnsctvo_cdgo_zna, -- Zona de la IPS primaria del afiliado.
        cnsctvo_sde_ips_afldo = DPR.cnsctvo_cdgo_sde, --Sede de la IPS primaria del afiliado.
        cnsctvo_cdgo_cdd_prmra = DPR.cnsctvo_cdgo_cdd  --Ciudad de la sede primaria del afiliado.
    FROM #tmpInformacionSolicitudPrestacion ISP
    INNER JOIN bdSisalud.dbo.tbDireccionesPrestador DPR WITH (NOLOCK)
      ON DPR.cdgo_intrno = ISP.cdgo_intrno_ips_slctnte 
	  AND @fechaActual BETWEEN DPR.inco_vgnca AND DPR.fn_vgnca
    INNER JOIN bdSisalud.dbo.tbclasesatencion_vigencias CA WITH (NOLOCK)
      ON ISP.clse_atncn_orgn = CA.cnsctvo_cdgo_clse_atncn
	  AND @fechaActual BETWEEN CA.inco_vgnca AND CA.fn_vgnca

    --Se actualiza la información del riesgo asociado a la prestación.
    UPDATE ISP
    SET cnsctvo_rsgo_prstcn = PXR.cnsctvo_cdgo_rsgo_dgnstco
    FROM #tmpInformacionSolicitudPrestacion ISP
    INNER JOIN BDCna.sol.tbCNAPrestacionesxRiesgo PR WITH (NOLOCK)
      ON ISP.cnsctvo_cdgo_prstcn = PR.cnsctvo_prstcn
    INNER JOIN BDCna.sol.tbCNAPrestacionesxRiesgo_Vigencias PXR WITH (NOLOCK)
      ON PR.cnsctvo_cdgo_prstcnxrsgs = PXR.cnsctvo_cdgo_prstcnxrsgs
    WHERE @fechaActual BETWEEN PXR.inco_vgnca AND PXR.fn_vgnca;

    --Se actualiza la información del riesgo asociado al diagnostico principal.
    UPDATE ISP
    SET cnsctvo_rsgo_dgnstco = PXR.cnsctvo_cdgo_rsgo_dgnstco
    FROM #tmpInformacionSolicitudPrestacion ISP
    INNER JOIN bdSisalud.dbo.tbMNRiesgosxDiagnosticos PR WITH (NOLOCK)
      ON ISP.cnsctvo_dgnstco_prncpl = PR.cnsctvo_cdgo_dgnstco
    INNER JOIN bdSisalud.dbo.tbMNRiesgosxDiagnosticos_vigencias PXR WITH (NOLOCK)
      ON PR.cnsctvo_cdgo_rsgo_dgnstco = PXR.cnsctvo_rsgo_pr_dgnstco
    WHERE @fechaActual BETWEEN PXR.inco_vgnca AND PXR.fn_vgnca;

	UPDATE ISP
	SET mrca_msmo_prstdor = @mrca_msmo_prstdor --Marca las solicitudes hospitalarias al mismo prestador
	FROM #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
	INNER JOIN	gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios IHS WITH (NOLOCK) 
	ON			IHS.cnsctvo_slctd_atrzcn_srvco = ISP.cnsctvo_slctd


	--La validación de solicitud por el PAC no aplica para el plan familiar y Quimbaya
	If EXISTS(Select 1 from #tmpInformacionSolicitudPrestacion ISP WHERE ISP.pln_prstcn In(@pln_fmlr, @pln_qmbya))
	BEGIN
		UPDATE ISP
		SET cnsctvo_cdgo_tpo_pln = @tpo_pln_ps,
			pln_prstcn			 = @tpo_pln_ps 
		FROM #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
	END

	--Marca las prestaciones con PAC al mismo prestador
	UPDATE ISP
	SET mrca_msmo_prstdor = @mrca_msmo_prstdor 
	FROM #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
	INNER JOIN	gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios AFI WITH (NOLOCK) 
	ON			AFI.cnsctvo_slctd_atrzcn_srvco = ISP.cnsctvo_slctd
	WHERE		ISP.cnsctvo_cdgo_tpo_pln = @pln_pac
	

	--Direccionar las solicitudes hospitalarias y los afiliados PAC al mismo prestador
	IF EXISTS (SELECT 1 FROM #tmpInformacionSolicitudPrestacion ISP WHERE ISP.mrca_msmo_prstdor=@mrca_msmo_prstdor)
	BEGIN

		INSERT INTO #tmpPrestadoresDestino (id_slctd_x_prstcn
		, cnsctvo_cdgo_prstcn
		, cnsctvo_vgnca_drcnmnto_prstdr
		, cnsctvo_cdgo_dt_lsta_prco
		, sfcnca_ips
		, nvl_prrdd
		, cdgo_intrno
		, cnsctvo_cdfccn
		, cnsctvo_cdgo_cdd
		, nmbre_scrsl
		, vlr_trfdo
		, cnsctvo_cdgo_zna --Zona de la ips (sur, norte, etc..)
		, cnsctvo_cdgo_sde --Sede de la ips primaria (CALI, MANIZALES, ETC)
		, tmpo_dsplzmnto)
		  SELECT
			ISP.id,
			ISP.cnsctvo_cdgo_prstcn,
			0,
			0,
			0,
			0,
			ISP.cdgo_intrno_ips_slctnte, 
			ISP.cnsctvo_cdgo_prstcn, 
			DPP.cnsctvo_cdgo_cdd,
			DPP.nmbre_scrsl,
			DPS.vlr_trfdo,
			DPP.cnsctvo_cdgo_zna,
			DPP.cnsctvo_cdgo_sde,
			@vlrCero
		  FROM #tmpInformacionSolicitudPrestacion ISP
		  INNER JOIN bdSisalud.dbo.tbAsociacionModeloActividad AMA WITH (NOLOCK)
			ON AMA.cdgo_intrno = ISP.cdgo_intrno_ips_slctnte  
			AND AMA.cnsctvo_cdgo_pln = ISP.pln_prstcn
			AND @fechaActual BETWEEN AMA.inco_vgnca AND AMA.fn_vgnca
		  INNER JOIN bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan PSU WITH (NOLOCK)
			ON AMA.cnsctvo_asccn_mdlo_actvdd = PSU.cnsctvo_asccn_mdlo_actvdd
			AND @fechaActual BETWEEN PSU.inco_vgnca AND PSU.fn_vgnca
		  INNER JOIN bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan DPS WITH (NOLOCK)
			ON PSU.cnsctvo_cdgo_lsta_prco = DPS.cnsctvo_cdgo_lsta_prco
			AND @fechaActual BETWEEN DPS.inco_vgnca AND DPS.fn_vgnca
			AND DPS.cnsctvo_prstncn = ISP.cnsctvo_cdgo_prstcn
			AND DPS.cnsctvo_cdgo_tpo_atncn = ISP.cnsctvo_cdgo_frma_atncn --tbLSDetListaPreciosxSucursalxPlan.cnsctvo_cdgo_tpo_atncn = a la forma de atención
		  INNER JOIN bdSisalud.dbo.tbDireccionesPrestador DPP WITH (NOLOCK)
			ON AMA.cdgo_intrno = DPP.cdgo_intrno
			AND @fechaActual BETWEEN DPP.inco_vgnca AND DPP.fn_vgnca
			WHERE ISP.mrca_msmo_prstdor=@mrca_msmo_prstdor;

	END

	IF EXISTS (SELECT 1 FROM #tmpInformacionSolicitudPrestacion ISP WHERE ISP.mrca_msmo_prstdor=@vlrCero)
		BEGIN

			--Tabla de posibles prestadores
			--Se toman los que cumplan básicamente las siguientes condiciones:
			--tengan convenio
			--Que atiendan el plan y edad del afiliado.
			--Que atiendan la prestación.
			--Que se encuentran a una distancia no mayor a la parametrizada vs. la ciudad de residencia del afiliado.
			--Que tengan suficiencia.
			--Que atiendan el tipo de atención.
			INSERT INTO #tmpPrestadoresDestino (id_slctd_x_prstcn
			, cnsctvo_cdgo_prstcn
			, cnsctvo_vgnca_drcnmnto_prstdr
			, cnsctvo_cdgo_dt_lsta_prco
			, sfcnca_ips
			, nvl_prrdd
			, cdgo_intrno
			, cnsctvo_cdfccn
			, cnsctvo_cdgo_cdd
			, nmbre_scrsl
			, vlr_trfdo
			, cnsctvo_cdgo_rsgo_dgnstco --Riesgoxprestacion atendido por el prestador
			, cnsctvo_cdgo_zna --Zona de la ips (sur, norte, etc..)
			, cnsctvo_cdgo_sde --Sede de la ips primaria (CALI, MANIZALES, ETC)
			, nmro_idntfccn_empldr_prstdr
			, tmpo_dsplzmnto)
			  SELECT
				ISP.id,
				ISP.cnsctvo_cdgo_prstcn,
				DPD.cnsctvo_drcnmnto_prstdr,
				DPD.cnsctvo_cdgo_dt_lsta_prco,
				DPD.sfcnca_ips,
				DPD.nvl_prrdd,
				DPD.cdgo_intrno,
				DPD.cnsctvo_cdfccn,
				DPP.cnsctvo_cdgo_cdd,
				DPP.nmbre_scrsl,
				DPS.vlr_trfdo,
				DPD.cnsctvo_cdgo_rsgo_dgnstco,
				DPP.cnsctvo_cdgo_zna,
				DPP.cnsctvo_cdgo_sde,
				DPD.nmro_unco_idntfccn_empldr,
				@vlrCero
			  FROM #tmpInformacionSolicitudPrestacion ISP
			  INNER JOIN bdcna.sol.tbCNADireccionamientoPrestadorDestino DPD WITH (NOLOCK)
				ON ISP.cnsctvo_cdgo_prstcn = DPD.cnsctvo_cdfccn
				AND ISP.pln_prstcn = DPD.cnsctvo_cdgo_pln
				AND ISP.edd_afldo BETWEEN DPD.edd_mnma AND DPD.edd_mxma
				AND @fechaActual BETWEEN DPD.inco_vgnca AND DPD.fn_vgnca
				AND DPD.vldo = @vldo
			  INNER JOIN bdSisalud.dbo.tbAsociacionModeloActividad AMA WITH (NOLOCK)
				ON AMA.cdgo_intrno = DPD.cdgo_intrno
				AND AMA.cnsctvo_cdgo_pln = ISP.pln_prstcn
				AND @fechaActual BETWEEN AMA.inco_vgnca AND AMA.fn_vgnca
			  INNER JOIN bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan PSU WITH (NOLOCK)
				ON AMA.cnsctvo_asccn_mdlo_actvdd = PSU.cnsctvo_asccn_mdlo_actvdd
				AND @fechaActual BETWEEN PSU.inco_vgnca AND PSU.fn_vgnca
			  INNER JOIN bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan DPS WITH (NOLOCK)
				ON PSU.cnsctvo_cdgo_lsta_prco = DPS.cnsctvo_cdgo_lsta_prco
				AND @fechaActual BETWEEN DPS.inco_vgnca AND DPS.fn_vgnca
				AND DPS.cnsctvo_prstncn = ISP.cnsctvo_cdgo_prstcn
				AND DPS.cnsctvo_cdgo_tpo_atncn = ISP.cnsctvo_cdgo_frma_atncn --tbLSDetListaPreciosxSucursalxPlan.cnsctvo_cdgo_tpo_atncn = a la forma de atención
			  INNER JOIN bdSisalud.dbo.tbDireccionesPrestador DPP WITH (NOLOCK)
				ON AMA.cdgo_intrno = DPP.cdgo_intrno
				AND @fechaActual BETWEEN DPP.inco_vgnca AND DPP.fn_vgnca
				and  dpp.cnsctvo_cdgo_sde  =  ISP.cnsctvo_sde_ips_afldo  -- ajuste temporal realizado por Angela 11-01-2017 
																		 -- mientras borramos las otras ciudades
			  WHERE ISP.mrca_msmo_prstdor=@vlrCero;

	 
				--Actualización de información de suficiencia
				;with tmpSuficiencia AS (select TPD.id, STE.cnsctvo_drcnmnto_prstdr,STE.fcha_fn_vgnca,STE.fcha_inco_vgnca,  ISNULL(STE.sfcnca_IPS,@vlrCero) sfcnca_ms, STE.sfcnca_IPS
									FROM  bdcna.sol.tbCNASuficienciaPrestadoresPorMes STE
									INNER JOIN #tmpPrestadoresDestino TPD
									ON TPD.cnsctvo_vgnca_drcnmnto_prstdr = STE.cnsctvo_drcnmnto_prstdr
									AND @fechaActual BETWEEN STE.fcha_inco_vgnca AND STE.fcha_fn_vgnca
									)
				UPDATE  TPD
				SET     sfcnca_ms  = ISNULL(STE.sfcnca_ms, @vlrCero),
						fcha_inco_vgnca_sfcnca = STE.fcha_inco_vgnca,
						fcha_fn_vgnca_sfcnca = STE.fcha_fn_vgnca,        
						cnsctvo_drcnmnto_prstdr = STE.cnsctvo_drcnmnto_prstdr,
						sfcnca_orgnl = STE.sfcnca_IPS
				FROM #tmpPrestadoresDestino TPD
				LEFT JOIN tmpSuficiencia STE
				ON STE.id = TPD.id

				--Se borran los que no apliquen y que sean menores a cero.
				DELETE #tmpPrestadoresDestino
				WHERE sfcnca_ms < @vlrCero		
		END  
	END

