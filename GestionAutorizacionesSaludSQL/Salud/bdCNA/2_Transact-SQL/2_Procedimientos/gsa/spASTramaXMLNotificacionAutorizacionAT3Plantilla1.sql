USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASTramaXMLNotificacionAutorizacionAT3Plantilla1]    Script Date: 24/10/2016 10:09:21 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASTramaXMLNotificacionAutorizacionAT3Plantilla1') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASTramaXMLNotificacionAutorizacionAT3Plantilla1 AS SELECT 1;'
END
GO

/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG     : spASTramaXMLNotificacionAutorizacionAT3Plantilla1
* Desarrollado por : <\A Jorge Rodriguez     A\>    
* Descripcion      : <\D Construcción trama xml para el envío de notificación de afiliados. Plantilla 1   D\>
				  <\D  D\>    
* Observaciones    : <\O O\>    
* Parametros       : <\P P\>    
* Variables        : <\V V\>    
* Fecha Creacion   : <\FC 2016-12-29  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM   FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASTramaXMLNotificacionAutorizacionAT3Plantilla1]
	@plantilla			char, 
	@usrio				VARCHAR(50),
	@cnstrccn_trma_xml	XML OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @anno	VARCHAR(4),
			@mes	VARCHAR(2),
			@dia	VARCHAR(2),
			@hra	VARCHAR(2),
			@mnto	VARCHAR(2),
			@sgndo	VARCHAR(2)

	

	SELECT	@anno	= CONVERT(CHAR(4),DATEPART(YEAR,GETDATE())),
			@mes	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(MONTH,GETDATE())))),2),
			@dia	= RIGHT('00' + LTRIM(RTRIM(CONVERT(CHAR(2),DATEPART(DAY,GETDATE())))),2),
			@hra	= CONVERT(CHAR(2),DATEPART(HOUR,GETDATE())),
			@mnto	= CONVERT(CHAR(2),DATEPART(MINUTE,GETDATE())),
			@sgndo	= CONVERT(CHAR(2),DATEPART(SECOND,GETDATE()))

	SET @cnstrccn_trma_xml = (
		SELECT		TSO.id_sld_rcbda	id,
					TPR.crreo			correo,
					TAF.eml				co,
					TAF.bcc				bcc,
					@plantilla			'plantilla',
					--TSO.plntlla			'plantilla',
					(
						SELECT	'tpo_idntfccn'						'parametro/nombre',
								AFI.cnsctvo_cdgo_tpo_idntfccn_afldo	'parametro/valor',
								NULL,
								'nmbre_afldo'						'parametro/nombre',
								AFI.nmbre_afldo						'parametro/valor',
								NULL,
								'nmro_idntfccn'						'parametro/nombre',
								AFI.nmro_idntfccn_afldo				'parametro/valor',
								NULL,
								'dscrpcn_pln'						'parametro/nombre',
								AFI.dscrpcn_pln						'parametro/valor',
								NULL,
								'rngo_slrl'							'parametro/nombre',
								AFI.cnsctvo_cdgo_rngo_slrl			'parametro/valor',
								NULL,
								'drcho'								'parametro/nombre',
								AFI.dscrpcn_csa_drcho				'parametro/valor',
								NULL,
								'ips_afldo'							'parametro/nombre',
								AFI.cdgo_ips_prmra					'parametro/valor'
						FROM #tmpAflds AFI
						WHERE AFI.id = TAF.id
						FOR XML PATH('parametros'), TYPE
					),
					@dia		'fecha/dia',
					@hra		'fecha/hora',
					@mnto		'fecha/minutos',
					@mes		'fecha/mes',
					@sgndo		'fecha/segundos',
					@anno		'fecha/ano',
					RTRIM(LTRIM(@usrio)) 'usuario'					
		FROM #tmpAflds TAF
		INNER JOIN #tmpSlctdes TSO
					ON TAF.id_sld_rcbda=TSO.id_sld_rcbda
		INNER JOIN #tmpPrvdor TPR
					ON TSO.id_sld_rcbda = TPR.id_sld_rcbda
		FOR XML PATH('mensajeCorreo')	
		)
		
END
GO