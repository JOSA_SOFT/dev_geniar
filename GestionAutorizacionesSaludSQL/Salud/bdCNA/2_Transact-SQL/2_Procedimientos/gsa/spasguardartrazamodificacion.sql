USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spasguardartrazamodificacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGuardarTrazaModificacion
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento que permite almacenar la información de trazabilidad    	  D\>    
  					 : <\D de los cambios ejecutados a la OPS										  D\>
* Observaciones      : <\O   O\>    
* Parametros         : <\P	 P\>    
* Variables          : <\V   V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Se modifica SP para que tenga en cuenta las nuevas columnas que se adicionaron
							 a la tabla durante el insert
							 elmnto_orgn_mdfccn
							 dto_elmnto_orgn_mdfccn
							 dscrpcn_vlr_antrr
							 dscrpcn_vlr_nvo         AM\>    
* Descripcion        : <\D     D\> 
* Nuevas Variables   : <\VM  elmnto_orgn_mdfccn
							 dto_elmnto_orgn_mdfccn
							 dscrpcn_vlr_antrr
							 dscrpcn_vlr_nvo   VM\>    
* Fecha Modificacion : <\FM  29/03/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spasguardartrazamodificacion]
AS
BEGIN
  SET NOCOUNT ON

 INSERT INTO bdcna.gsa.tbastrazamodificacion (cnsctvo_slctd_atrzcn_srvco
  , cnsctvo_srvco_slctdo
  , cnsctvo_cncpto_prcdmnto_slctdo
  , cnsctvo_cdgo_csa_nvdd
  , fcha_mdfccn
  , orgn_mdfccn
  , vlr_antrr
  , vlr_nvo
  , obsrvcns
  , nmro_ip
  , fcha_crcn
  , usro_crcn
  , fcha_ultma_mdfccn
  , usro_ultma_mdfccn
  , elmnto_orgn_mdfccn
  , dto_elmnto_orgn_mdfccn
  ,	dscrpcn_vlr_antrr
  , dscrpcn_vlr_nvo)
    SELECT
      TMD.cnsctvo_slctd_atrzcn_srvco,
      TMD.cnsctvo_srvco_slctdo,
      TMD.cnsctvo_cncpto_prcdmnto_slctdo,
      TMD.cnsctvo_cdgo_mtvo_csa,
      TMD.fcha_mdfccn,
      TMD.orgn_mdfccn,
      TMD.vlr_antrr,
      TMD.vlr_nvo,
      TMD.obsrvcns,
      TMD.nmro_ip,
      TMD.fcha_crcn,
      TMD.usro_crcn,
      TMD.fcha_ultma_mdfccn,
      TMD.usro_ultma_mdfccn,
	  TMD.elmnto_orgn_mdfccn,
	  TMD.dto_elmnto_orgn_mdfccn,
	  TMD.dscrpcn_vlr_antrr,
	  TMD.dscrpcn_vlr_nvo
    FROM #tmptrazamodificacion TMD

END

GO
