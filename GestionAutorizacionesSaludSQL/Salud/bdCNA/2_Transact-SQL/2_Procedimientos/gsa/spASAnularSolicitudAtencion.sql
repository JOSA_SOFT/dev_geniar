Use bdCNA
Go

If(Object_id('gsa.spASAnularSolicitudAtencion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASAnularSolicitudAtencion As Select 1';
End
Go

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASAnularSolicitudAtencion
* Desarrollado por   : <\A Ing. Carlos Andres Lopez Ramirez A\>    
* Descripcion        : <\D Procedimiento que se encarga de validar que solicitudes y atenciones tienen todas sus prestaciones anuladas
						   y cambia el estado a anulado D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion     : <\FC 02/10/2017  FC\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D   D\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

Alter Procedure gsa.spASAnularSolicitudAtencion
As 
Begin
	Set Nocount On

	Declare	@cnsctvo_cdgo_estdo_anldo_mga		udtConsecutivo,
			@cnsctvo_cdgo_estdo_anldo_sprs		udtConsecutivo,
			@fcha_actl							Datetime;

	Create Table #tempSolicitudesNoAnular(
		cnsctvo_slctd_atrzcn_srvco		udtConsecutivo
	)

	-- 
	Create Table #tempAtencionesNoAnular (
		cnsctvo_cdgo_ofcna			udtConsecutivo,
		nuam						udtConsecutivo
	)

	-- 
	Set @cnsctvo_cdgo_estdo_anldo_mga  = 14;
	Set @cnsctvo_cdgo_estdo_anldo_sprs = 3;
	Set @fcha_actl = getDate();

	-- Se recuperan los consecutivos de las solicitudes que no tengan todas las prestaciones anuladas.
	Insert Into  #tempSolicitudesNoAnular (
				cnsctvo_slctd_atrzcn_srvco
	)
	Select		Distinct a.cnsctvo_slctd_atrzcn_srvco 
	From		#tempInformacionSolicitud a
	Inner Join	bdCNA.gsa.tbASServiciosSolicitados b With(NoLock)
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_anldo_mga;

	-- 
	Update		a
	Set			cnsctvo_cdgo_estdo_slctd = @cnsctvo_cdgo_estdo_anldo_mga				
	From		#tempInformacionSolicitud a
	Left Join	#tempSolicitudesNoAnular b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_slctd_atrzcn_srvco Is null

	-- Se anulan las solicitudes que tengan estado anulado.
	Update		a
	Set			cnsctvo_cdgo_estdo_slctd = b.cnsctvo_cdgo_estdo_slctd,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = b.usro_anlcn
	From		bdCNA.gsa.tbASSolicitudesAutorizacionServicios a With(RowLock)
	Inner Join	#tempInformacionSolicitud b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Where		b.cnsctvo_cdgo_estdo_slctd = @cnsctvo_cdgo_estdo_anldo_mga
	
	-- Se recuperan las atenciones que no tengan todas las prestaciones anuladas.
	Insert Into #tempAtencionesNoAnular (
				cnsctvo_cdgo_ofcna,				nuam
	)
	Select		Distinct a.cnsctvo_cdgo_ofcna,	a.nuam
	From		#tempInformacionSolicitud s
	Inner Join	#tempInformacionServicios a
	On			a.cnsctvo_slctd_atrzcn_srvco = s.cnsctvo_slctd_atrzcn_srvco
	Inner Join	BDSisalud.dbo.tbASDatosAdicionalesMegaSipres b With(NoLock)
	On			b.cnsctvo_slctd_mga = a.cnsctvo_slctd_atrzcn_srvco
	And			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	And			b.nuam = a.nuam
	And			b.cnsctvo_prstcn = a.cnsctvo_cdgo_srvco_slctdo
	Inner Join	BDSisalud.dbo.tbProcedimientos c With(NoLock)
	On			c.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	And			c.nuam = b.nuam
	Where		c.cnsctvo_cdgo_estdo != @cnsctvo_cdgo_estdo_anldo_sprs;
	
	--
	Delete		a
	From		#tempAtenciones a
	Inner Join	#tempAtencionesNoAnular b
	On			b.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	And			b.nuam = a.nuam	

	--
	Update		#tempAtenciones
	Set			cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo_anldo_sprs	
	

	-- 
	Update		d
	Set			cnsctvo_cdgo_estdo = c.cnsctvo_cdgo_estdo,
				fcha_ultma_mdfccn = @fcha_actl,
				usro_ultma_mdfccn = a.usro_anlcn
	From		#tempInformacionSolicitud a
	Inner Join	BDSisalud.dbo.tbASDatosAdicionalesMegaSipres b With(NoLock)
	On			b.cnsctvo_slctd_mga = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#tempAtenciones c
	On			c.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna
	And			c.nuam = b.nuam
	Inner Join	BDSisalud.dbo.tbAtencionOps d With(RowLock)
	On			d.cnsctvo_cdgo_ofcna = c.cnsctvo_cdgo_ofcna
	And			d.nuam = c.nuam

	Drop Table #tempSolicitudesNoAnular;
	Drop Table #tempAtencionesNoAnular;

End