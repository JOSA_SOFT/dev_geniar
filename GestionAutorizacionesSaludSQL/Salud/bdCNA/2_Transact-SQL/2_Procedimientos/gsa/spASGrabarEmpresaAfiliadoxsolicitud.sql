USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarEmpresaAfiliadoxsolicitud]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASGrabarEmpresaAfiliadoxsolicitud 												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Grabar las empresas de los afiliados x solicitud											D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P	#TemptbAsInformacionEmpresasxAfiliado = temporal que contiene los datos del afiliado x solicitud P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/05/16																				FC\>  														
*  															
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


ALTER PROCEDURE [gsa].[spASGrabarEmpresaAfiliadoxsolicitud]

/*01*/ @lcUsrioAplcn   UdtUsuario = null

aS


Begin
	SET NOCOUNT ON

	Declare @lcUsuario				UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@fechaactual			datetime = getdate()

	If @lcUsrioAplcn is not null
	   Begin
	     Set @lcUsuario = @lcUsrioAplcn
	   End 
	   
	-- Insertamos las Empresas del afiliado x solicitud

	Insert tbAsInformacionEmpresasxAfiliado
	(
			cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,			cnsctvo_cdgo_tpo_idntfccn_empldr,			nmro_idntfccn_empldr,
			nmro_unco_idntfccn_empldr,							nmbre_empldr,								cnsctvo_scrsl,
			cnsctvo_cdgo_prdcto,								cnsctvo_cdgo_opcn_prncpl,					cnsctvo_cdgo_ofcna,
			inco_vgnca_cbrnza,									fn_vgnca_cbrnza,							slro_bse,
			cnsctvo_cdgo_tpo_cbrnza,							cnsctvo_cdgo_entdd_arp,						fcha_crcn,
			usro_crcn,											fcha_ultma_mdfccn,							usro_ultma_mdfccn
	)
	Select
			t.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco,			t.cnsctvo_cdgo_tpo_idntfccn_empldr,			t.nmro_idntfccn_empldr,
			t.nmro_unco_idntfccn_aprtnte,						t.nmbre_empldr,								t.cnsctvo_scrsl,
			t.cnsctvo_cdgo_prdcto,								t.cnsctvo_cdgo_opcn_prncpl,					t.cnsctvo_cdgo_ofcna,
			t.inco_vgnca_cbrnza,								t.fn_vgnca_cbrnza,							t.slro_bse,
			t.cnsctvo_cdgo_tpo_cbrnza,							t.cnsctvo_cdgo_entdd_arp,					@fechaactual,
			@lcUsuario,											@fechaactual,								@lcUsuario


	From		#TemptbAsInformacionEmpresasxAfiliado t
	Left Join	gsa.tbAsInformacionEmpresasxAfiliado iea with(nolock)
	On			iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco = t.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco
	Where		iea.cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco is null	
	  


End
GO
