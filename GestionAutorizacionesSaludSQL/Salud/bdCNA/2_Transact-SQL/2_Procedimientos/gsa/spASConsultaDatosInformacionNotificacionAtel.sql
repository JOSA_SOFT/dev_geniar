USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionNotificacionAtel]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosConsecutivosNotificaciolAtel
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionNotificacionAtel]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
AS
BEGIN
	SET NOCOUNT ON;

		Declare @cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
			@nmro_idntfccn			   udtNumeroIdentificacionLargo,
			@Nui_Afldo				   udtConsecutivo,
			@si							char(2) = 'SI',
			@ldfcha_consulta			DATETIME,
			@nmro_Ntfccn				varchar(50),
			@cnsctvo_accdnte_trbjo		udtConsecutivo = 4,
			@cnsctvo_enfrmdd_prfsnl		udtConsecutivo = 5,
			@ntfccn_crrda				udtConsecutivo = 12,
			@ntfccn_anlda				udtConsecutivo = 54
				
				
	
	CREATE TABLE #NotificacionesAtel(
		numeroSolicitud						udtConsecutivo,
		cnsctvo_cdgo_tpo_dgnstco			udtConsecutivo,
		dscrpcn_tpo_dgnstco_sld				udtDescripcion,
		cnsctvo_cdgo_dgnstco				udtConsecutivo,
		dscrpcn_dgnstco						udtDescripcion,
		cnsctvo_cdgo_estdo_ntfccn			udtConsecutivo,
		dscrpcn_estdo_ntfccn				udtDescripcion,
		nmro_Ntfccn							varchar(50),
		atl									char(2) default 'NO',
		cnsctvo_cdgo_tpo_idntfccn			udtConsecutivo,
		nmro_idntfccn						udtNumeroIdentificacionLargo,
		nui_Afldo							udtConsecutivo,
		cnsctvo_ntfccn						udtConsecutivo
	);

	CREATE TABLE #tmpNumeroNotificacion(
		cnsctvo_cdgo_dgnstco	udtConsecutivo,
		nmro_ntfccn				udtDescripcion
	)

	Create Table #notificacion(
		nmro_Ntfccn					varchar(50),
		dscrpcn_tpo_dgnstco_sld		udtDescripcion,
		cnsctvo_cdgo_estdo_ntfccn	udtConsecutivo,
		dscrpcn_estdo_ntfccn		udtDescripcion,	
		cnsctvo_ntfccn				udtConsecutivo,
		cnsctvo_slctd_atrzcn_srvco	udtConsecutivo
	)

	SET	@ldfcha_consulta = GETDATE()

	SELECT  @cnsctvo_cdgo_tpo_idntfccn = cnsctvo_cdgo_tpo_idntfccn_afldo, 
			@nmro_idntfccn = nmro_idntfccn_afldo
	FROM gsa.tbASInformacionAfiliadoSolicitudAutorizacionServicios ias WITH (NOLOCK)
	WHERE ias.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;

	Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn);

	INSERT INTO #NotificacionesAtel(
		cnsctvo_cdgo_tpo_dgnstco,		dscrpcn_tpo_dgnstco_sld,			cnsctvo_cdgo_dgnstco,
		dscrpcn_dgnstco,				cnsctvo_cdgo_estdo_ntfccn,			dscrpcn_estdo_ntfccn,
		numeroSolicitud,				nmro_Ntfccn,						cnsctvo_ntfccn
	)
	SELECT dss.cnsctvo_cdgo_tpo_dgnstco,	null, 			dss.cnsctvo_cdgo_dgnstco,
		d.dscrpcn_dgnstco,					null,			null,
		@cnsctvo_slctd_atrzcn_srvco,		null,			null											
	FROM BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios dss WITH(NOLOCK)
	INNER JOIN bdSisalud.dbo.tbDiagnosticos_Vigencias d WITH(NOLOCK) on dss.cnsctvo_cdgo_dgnstco = d.cnsctvo_cdgo_dgnstco
	INNER JOIN bdSisalud.prm.tbTiposDiagnosticosSalud_Vigencias td WITH(NOLOCK) on dss.cnsctvo_cdgo_tpo_dgnstco = td.cnsctvo_cdgo_tpo_dgnstco_sld
	INNER JOIN BDCna.gsa.tbASSolicitudesAutorizacionServicios sas WITH(NOLOCK) on sas.cnsctvo_slctd_atrzcn_srvco = dss.cnsctvo_slctd_atrzcn_srvco
	WHERE dss.cnsctvo_slctd_atrzcn_srvco =  @cnsctvo_slctd_atrzcn_srvco
	And	  @ldfcha_consulta between td.inco_vgnca and td.fn_vgnca
		

	Insert Into #notificacion(
		dscrpcn_tpo_dgnstco_sld,
		cnsctvo_cdgo_estdo_ntfccn,
		dscrpcn_estdo_ntfccn,	
		cnsctvo_ntfccn,
		nmro_Ntfccn,
		cnsctvo_slctd_atrzcn_srvco
	)
	SELECT  top 1 
			cev.dscrpcn_clsfccn_evnto,		am.cnsctvo_cdgo_estdo_ntfccn,		
			env.dscrpcn_estdo_ntfccn,		am.cnsctvo_ntfccn,
			ltrim(rtrim(convert(varchar(20),am.cnsctvo_ntfccn)))+'-'+ltrim(rtrim(h.cdgo_ofcna)) nmro_Ntfccn,	
			@cnsctvo_slctd_atrzcn_srvco										
	FROM #NotificacionesAtel d WITH(NOLOCK)
	INNER JOIN bdSisalud.dbo.tbafiliadosMarcados am WITH(NOLOCK) On am.cnsctvo_cdgo_dgnstco = d.cnsctvo_cdgo_dgnstco
	INNER JOIN bdSisalud.dbo.tbClasificacionEventosNotificacion_Vigencias cev WITH(NOLOCK) On cev.cnsctvo_cdgo_clsfccn_evnto = am.cnsctvo_cdgo_clsfccn_evnto
	INNER JOIN bdSisalud.dbo.tbEstadosNotificacion_Vigencias env WITH(NOLOCK) On am.cnsctvo_cdgo_estdo_ntfccn = env.cnsctvo_cdgo_estdo_ntfccn
	INNER JOIN BDAfiliacionValidador..tboficinas h on h.cnsctvo_cdgo_ofcna=am.cnsctvo_cdgo_ofcna
	And	  am.nmro_unco_idntfccn = @Nui_Afldo
	And	  @ldfcha_consulta between cev.inco_vgnca and cev.fn_vgnca
	And	  @ldfcha_consulta between env.inco_vgnca and env.fn_vgnca
	order by am.cnsctvo_ntfccn desc


	Update natel Set 
					nmro_Ntfccn					= noti.nmro_Ntfccn,
					dscrpcn_tpo_dgnstco_sld		= noti.dscrpcn_tpo_dgnstco_sld,
					cnsctvo_cdgo_estdo_ntfccn	= noti.cnsctvo_cdgo_estdo_ntfccn,
					dscrpcn_estdo_ntfccn		= noti.dscrpcn_estdo_ntfccn,
					cnsctvo_ntfccn				= noti.cnsctvo_ntfccn
	From #NotificacionesAtel natel
	Inner Join #notificacion noti On noti.cnsctvo_slctd_atrzcn_srvco = natel.numeroSolicitud


	Insert Into #tmpNumeroNotificacion(cnsctvo_cdgo_dgnstco, nmro_ntfccn)	
	Select
		dss.cnsctvo_cdgo_dgnstco, ltrim(rtrim(convert(varchar(20),b.cnsctvo_ntfccn)))+'-'+ltrim(rtrim(h.cdgo_ofcna)) 
		FROM bdSisalud.dbo.tbatep  a 
		inner join bdSisalud.dbo.tbactuanotificacion b on  a.cnsctvo_ntfccn=b.cnsctvo_ntfccn and a.cnsctvo_cdgo_ofcna=b.cnsctvo_cdgo_ofcna   
		inner join bdSisalud.dbo.tbafiliadosmarcados d on   a.cnsctvo_ntfccn=d.cnsctvo_ntfccn and a.cnsctvo_cdgo_ofcna=d.cnsctvo_cdgo_ofcna      
		INNER JOIN bdSisalud.dbo.tbDiagnosticos_Vigencias f on f.cnsctvo_cdgo_dgnstco=d.cnsctvo_cdgo_dgnstco
		INNER JOIN BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios dss WITH(NOLOCK) on dss.cnsctvo_cdgo_dgnstco = f.cnsctvo_cdgo_dgnstco
		Inner join BDAfiliacionValidador..tboficinas h on a.cnsctvo_cdgo_ofcna=h.cnsctvo_cdgo_ofcna
		where b.nmro_unco_idntfccn=@Nui_Afldo
		And dss.cnsctvo_slctd_atrzcn_srvco =  @cnsctvo_slctd_atrzcn_srvco
		And  @ldfcha_consulta between f.inco_vgnca and f.fn_vgnca
		And  d.cnsctvo_cdgo_clsfccn_evnto in (@cnsctvo_accdnte_trbjo, @cnsctvo_enfrmdd_prfsnl)
		And  d.cnsctvo_cdgo_estdo_ntfccn not in(@ntfccn_crrda, @ntfccn_anlda)
		
	
	UPDATE NT SET cnsctvo_cdgo_tpo_idntfccn	= @cnsctvo_cdgo_tpo_idntfccn,
				  nmro_idntfccn				= @nmro_idntfccn,
				  Nui_Afldo					= @Nui_Afldo
	FROM #NotificacionesAtel NT
	
	Update NT Set	nmro_Ntfccn = nn.nmro_ntfccn
	FROM #NotificacionesAtel NT
	Inner Join 	#tmpNumeroNotificacion nn On nn.cnsctvo_cdgo_dgnstco = NT.cnsctvo_cdgo_dgnstco

	SELECT 
		cnsctvo_cdgo_tpo_dgnstco,		dscrpcn_tpo_dgnstco_sld,		cnsctvo_cdgo_dgnstco,
		dscrpcn_dgnstco,				cnsctvo_cdgo_estdo_ntfccn,		dscrpcn_estdo_ntfccn,
		nmro_Ntfccn,					atl,							cnsctvo_cdgo_tpo_idntfccn,					
		nmro_idntfccn,					nui_Afldo,						cnsctvo_ntfccn	
	FROM #NotificacionesAtel

	DROP TABLE #NotificacionesAtel;
	DROP TABLE #Notificacion;
	DROP TABLE #tmpNumeroNotificacion;
END

GO
