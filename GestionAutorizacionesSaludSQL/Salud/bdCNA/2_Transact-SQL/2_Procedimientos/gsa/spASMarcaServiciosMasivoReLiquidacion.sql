USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASMarcaServiciosMasivoReLiquidacion]    Script Date: 05/04/2017 13:40:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spASMarcaServiciosMasivoReLiquidacion
* Desarrollado por		: <\A Ing. Jonathan - SETI SAS	A\>
* Descripcion			: <\D 
                              Se marcan los servicios Masivo que se van a
							  reliquidar						
						  D\>
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 15/06/2016 FC\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se realiza modificacion al procedimiento para que actualice la informacion del consecutivo
								del procedimiento o medicamento 
                            DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	22/02/2017 FM\>
*------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Ing. Victor Hugo Gil Ramos AM\>
* Descripcion			 : <\DM	
                                Se realiza modificacion al procedimiento para que no marque las prestaciones que tiene
								numero unico de OPS mayor a 0
                            DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	28/03/2017 FM\>
*------------------------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASMarcaServiciosMasivoReLiquidacion] 
	@fcha_entrga_mrca_rlqudcon DATE,
	@mensaje				   VARCHAR(1000) OUTPUT,
	@codigo_error			   VARCHAR(100) OUTPUT
AS
BEGIN
  SET NOCOUNT ON;

  BEGIN TRY
      Declare @cdgo_error						Char(5)	 ,          
              @mrca_rlqdcn						udtlogico,
		      @prmtrzcn_n_ds_ants_fcha_entrga	Int      ,	
		      @cnsctvo_cdgo_rgla				Int		 ,	--bdSisalud.dbo.tbReglas.cnsctvo_cdgo_rgla
		      @fcha_actl						Datetime ,
		      @fcha_entrga_n_ds_ants_rlqudcon   DATE;		--Fecha de entrega se cumple n días antes - reliquidación	 

	  Set @cdgo_error        = 'ERROR'
	  Set @mrca_rlqdcn       = 'S'
      Set @cnsctvo_cdgo_rgla = 155
	  Set @fcha_actl         = getdate()
	  
	  Select @prmtrzcn_n_ds_ants_fcha_entrga = vlr_rgla 
	  From   bdSisalud.dbo.tbReglas_Vigencias With(NoLock)
	  Where  cnsctvo_cdgo_rgla = @cnsctvo_cdgo_rgla
	  And	 @fcha_actl BETWEEN inco_vgnca AND fn_vgnca;

	  SET @fcha_entrga_n_ds_ants_rlqudcon = DATEADD(DAY,-@prmtrzcn_n_ds_ants_fcha_entrga, @fcha_entrga_mrca_rlqudcon);
	
	  --Se marcan los procedimientos que se deben reliquidar	
	  Update	 CS
	  Set		 cnsctvo_prcdmnto_insmo_slctdo = CSS.cnsctvo_prcdmnto_insmo_slctdo,
				 nmro_unco_ops = CSS.nmro_unco_ops					
	  From		 #cnslddo_slctud CS
	  Inner Join GSA.tbASProcedimientosInsumosSolicitados PIS With(NoLock)
	  On		 PIS.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
	  Inner Join gsa.tbASConceptosServicioSolicitado CSS With(NoLock)
	  On		 CSS.cnsctvo_prcdmnto_insmo_slctdo = PIS.cnsctvo_prcdmnto_insmo_slctdo	  
	 

	  --Se marcan los medicamentos que se deben reliquidar	
	  Update	 CS
	  Set		 cnsctvo_mdcmnto_slctdo = CSS.cnsctvo_mdcmnto_slctdo,
				 nmro_unco_ops = CSS.nmro_unco_ops					
	  From		 #cnslddo_slctud CS
	  Inner Join gsa.tbASMedicamentosSolicitados MS
	  On		 MS.cnsctvo_srvco_slctdo = CS.cnsctvo_srvco_slctdo
	  Inner Join gsa.tbASConceptosServicioSolicitado CSS
	  On		 CSS.cnsctvo_mdcmnto_slctdo = MS.cnsctvo_mdcmnto_slctdo
	  

	  --Se marcan las prestaciones que no tengan una OPS 
	  Update     #cnslddo_slctud
	  Set        mrca_rlqdcn = @mrca_rlqdcn
	  Where      fcha_estmda_entrga IN (@fcha_entrga_mrca_rlqudcon, @fcha_entrga_n_ds_ants_rlqudcon) 
	  And        (nmro_unco_ops = 0 OR nmro_unco_ops IS NULL)
        	
  END TRY
  BEGIN CATCH
	  SET @mensaje = 'Number:'    + CAST(ERROR_NUMBER() AS VARCHAR(20)) + CHAR(13) +
					 'Line:'      + CAST(ERROR_LINE() AS VARCHAR(10)) + CHAR(13) +
					 'Message:'   + ERROR_MESSAGE() + CHAR(13) +
					 'Procedure:' + ERROR_PROCEDURE();
      SET @codigo_error = @cdgo_error;
  END CATCH
END
