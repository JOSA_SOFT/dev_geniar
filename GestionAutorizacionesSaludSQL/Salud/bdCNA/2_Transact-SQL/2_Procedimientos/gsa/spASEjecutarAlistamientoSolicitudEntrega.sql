USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarAlistamientoSolicitudEntrega]    Script Date: 04/04/2017 10:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------  															
* Metodo o PRG 		: spASEjecutarAlistamientoSolicitudEntrega												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Controlador Para ejecución Alistamiento Solicitud para entrega							D\>  												
* Observaciones		: <\O O\>  													
* Parametros		: <\P																							P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/21																				FC\>  														
*  												
*------------------------------------------------------------------------------------------------------------------------    															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------  															
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	  	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/ 


/* 
Declare @mnsjertrno					 UdtDescripcion,
		@cdgortrno					 varchar(2)
exec bdcna.gsa.spASEjecutarAlistamientoSolicitudEntrega null, null, @mnsjertrno output, @cdgortrno output
--exec bdcna.gsa.spASEjecutarAlistamientoSolicitudEntrega 2104, 'geniarjcv', @mnsjertrno output, @cdgortrno output

Select @cdgortrno, @mnsjertrno

*/

ALTER PROCEDURE [gsa].[spASEjecutarAlistamientoSolicitudEntrega]

/*01*/	@cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo = null,
/*02*/  @lcUsrioAplcn				 UdtUsuario = null,
/*03*/  @mnsjertrno					 UdtDescripcion = null output,
/*04*/  @cdgortrno					 varchar(2) = null output

AS

Begin
	SET NOCOUNT ON

	-- Declaramos Variables del Proceso
	Declare @mnsjeexto		UdtDescripcion = 'El Proceso Se Ejecuto Con Exito',
			@mnsjeerr		UdtDescripcion = 'Se Presento un Error En el proceso',
			@cdgoexto		Varchar(2) = 'OK',
			@cdgoerr		Varchar(2) = 'ET'

	-- Paso 1. Homologar prestaciones del prestador de la solicitud
		exec bdcna.gsa.spASEjecutarHomologacionPrestacionesPrestador @cnsctvo_slctd_atrzcn_srvco, @lcUsrioAplcn, @mnsjertrno output, @cdgortrno output 

		if @cdgortrno = @cdgoerr
			Begin
				-- Se presento un error al homologar se cancela el proceso
				Return
			End

	-- Paso 2. Crear Empresa del afiliado de la solicitud
		exec bdcna.gsa.spASEjecutarCreacionEmpresaAfiliadoSolicitud  @cnsctvo_slctd_atrzcn_srvco, @lcUsrioAplcn, @mnsjertrno output, @cdgortrno output
		if @cdgortrno = @cdgoerr
			Begin
				-- Se presento un error al crear Empresa del afiliado de la solicitud se cancela el proceso
				Return
			End

	-- Paso 3. Generar Número(s) Unico(s) OPS de la solicitud
		exec bdcna.gsa.spASEjecutarGenerarNumeroUnicoOpsMasivo @cnsctvo_slctd_atrzcn_srvco, @lcUsrioAplcn, @mnsjertrno output, @cdgortrno output
		if @cdgortrno = @cdgoerr
			Begin
				-- Se presento un error al Generar Número Unico OPS se cancela el proceso
				
				Return
			End
			
	-- Paso 4. Cálculo Cuotas de Recuperación
	    exec bdcna.gsa.spASEjecutarCalculoCuotasRecuperacion @cnsctvo_slctd_atrzcn_srvco, @lcUsrioAplcn, @mnsjertrno output, @cdgortrno output
		if @cdgortrno = @cdgoerr
			Begin
				-- Se presento un error al Calcular Cuotas de Recuperación se cancela el proceso
				Return
			End	
		else
			Begin
				-- El Proceso se realizo con éxito
				Set @mnsjertrno = @mnsjeexto
				Set @cdgortrno = @cdgoexto
			End

	-- Paso 5. Actualizar Estados Solicitud Autorizacion Masivo OPS Virtual 
	   exec bdcna.gsa.spASActualizarEstadoImpresoAutorizacionServicioOPSMasivo @cdgortrno output,@mnsjertrno output
	   if @cdgortrno = @cdgoerr
			Begin
				-- Se presento un error al Calcular Cuotas de Recuperación se cancela el proceso
				Return
			End	
		else
			Begin
				-- El Proceso se realizo con éxito
				Set @mnsjertrno = @mnsjeexto
				Set @cdgortrno = @cdgoexto
			End

	IF	@@error	<> 0
		Begin
			Set @mnsjertrno = @mnsjeerr
			Set @cdgortrno = @cdgoerr
		End
	
End