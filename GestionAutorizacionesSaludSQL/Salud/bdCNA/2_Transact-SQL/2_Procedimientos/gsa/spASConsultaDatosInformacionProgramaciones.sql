USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultaDatosInformacionProgramaciones]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASConsultarDatosProgramaciones
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					A\>
* Descripcion			: <\D 													D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2012/10/04 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASConsultaDatosInformacionProgramaciones]
@cnsctvo_slctd_atrzcn_srvco	udtConsecutivo

AS
BEGIN
	SET NOCOUNT ON;
	
	Declare @ldfcha_consulta  datetime ,
			@cums		int = 5,
			@cups		int = 4,
			@cuos		int = 11;
	
	SET	@ldfcha_consulta = GETDATE()
	
	SELECT  ROW_NUMBER() OVER(ORDER BY ss.cnsctvo_slctd_atrzcn_srvco DESC) cnsctvo_slctd_atrzcn_srvco,
			fe.fcha_rl_entrga , 
			ss.cntdd_slctda , 
			rd.cdgo_intrno , 
			dp.nmbre_scrsl , 
			concat(cf.cdgo_cdfccn, ' ', cf.dscrpcn_cdfccn) cdgo_cdfccn, 
			tcf.dscrpcn_tpo_cdfccn
	From BDCna.gsa.tbASResultadoFechaEntrega fe With(NoLock)
	INNER JOIN BDCna.gsa.tbASServiciosSolicitados ss With(NoLock) on  fe.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
													  and fe.cnsctvo_srvco_slctdo       = ss.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN BDCna.gsa.tbASResultadoDireccionamiento rd With(NoLock) on rd.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
													  and rd.cnsctvo_srvco_slctdo       = ss.cnsctvo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock) ON rd.cdgo_intrno = dp.cdgo_intrno
	INNER JOIN bdSisalud.dbo.tbDireccionesPrestador_Vigencias DPV With(NoLock) ON DPV.cdgo_intrno = DP.cdgo_intrno
	
	INNER JOIN bdSisalud.dbo.tbCodificaciones cf With(NoLock) on cf.cnsctvo_cdfccn = ss.cnsctvo_cdgo_srvco_slctdo
	INNER JOIN bdSisalud.dbo.tbTipoCodificacion_Vigencias tcf With(NoLock) on cf.cnsctvo_cdgo_tpo_cdfccn = tcf.cnsctvo_cdgo_tpo_cdfccn 
	Where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
	And   tcf.cnsctvo_cdgo_tpo_cdfccn in(@cums, @cups, @cuos)
	--And   @ldfcha_consulta between tcf.inco_vgnca and tcf.fn_vgnca
	And   @ldfcha_consulta between DPV.inco_vgnca and DPV.fn_vgnca
END

GO
