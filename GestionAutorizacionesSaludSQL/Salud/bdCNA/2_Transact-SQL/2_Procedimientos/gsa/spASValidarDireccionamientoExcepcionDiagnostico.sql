USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoExcepcionDiagnostico]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoExcepcionDiagnostico
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D excepcion de diagnostico											 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoExcepcionDiagnostico] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @fechaActual datetime = GETDATE(),
          @afirmacion char(1) = 'S',
		  @vlor_cro int = 0;

  --Se valida si hay prestadores que tengan asociadas excepciones sobre  diagnósticos.
  IF EXISTS (SELECT
      TCO.id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
    INNER JOIN bdCna.gsa.tbASExcepcionesDiagnosticosDireccionesPrestador EDP WITH (NOLOCK)
      ON EDP.cdgo_intrno = TCO.cdgo_intrno
      AND ISP.cnsctvo_dgnstco_prncpl = EDP.cnsctvo_cdgo_dgnstco
	  AND TCO.cnsctvo_cdfccn = EDP.cnsctvo_cdfccn
    WHERE @fechaActual BETWEEN EDP.inco_vgnca AND EDP.fn_vgnca
    AND EDP.vldo = @afirmacion
    AND @fechaActual < EDP.fcha_fn_vldz_rgstro
	AND ISP.mrca_msmo_prstdor = @vlor_cro)
  BEGIN
    --Se actualizan los prestadores que si tengan asociadas excepciones sobre diagnosticos dado
    --que son los que seguirán en el proceso.
    UPDATE TCO
    SET cmple = @afirmacion
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
    INNER JOIN bdCna.gsa.tbASExcepcionesDiagnosticosDireccionesPrestador EDP WITH (NOLOCK)
      ON EDP.cdgo_intrno = TCO.cdgo_intrno
      AND ISP.cnsctvo_dgnstco_prncpl = EDP.cnsctvo_cdgo_dgnstco
	  AND TCO.cnsctvo_cdfccn = EDP.cnsctvo_cdfccn
      AND @fechaActual BETWEEN EDP.inco_vgnca AND EDP.fn_vgnca
      AND EDP.vldo = @afirmacion
      AND @fechaActual < EDP.fcha_fn_vldz_rgstro
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro;

    EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
  END

END

GO
