USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsCambiarEstadoConceptosMigracionEstadosSipresMega]    Script Date: 24/05/2017 08:59:42 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsCambiarEstadoConceptosMigracionEstadosSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Cambia el estado de los conceptos de las prestaciones al equivalente al estado en Sipres
									y guarda el historico de cambios en Mega.
							  D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  Entrada:
								   1- @usuario_sistema: usuario del sistema
							  P\>  													
* Variables					: <\V  1- @fcha_actl: Fecha Actual obtenida del sistema.
								   2- @fin_vigencia: Fecha de fin de vigencia utilizada para el historico de 
												     cambio de estado de conceptos
								   3- @vldo_si: Valor por defecto para el cambio de estado que indica la valides del registro
								   4- @cnsctvo_cdgo_mdlo_cra46: codigo que crea mega
								   5- @vlr_cro: Almacena el valor 0
								   6- @cnsctvo_cdgo_estdo8: Codigo de estado que indica que la ops esta impresa en Sipres
								   7- @cnsctvo_cdgo_estdo3: Codigo Sipres que indica que la ops esta anulada.
								   8- @cnsctvo_cdgo_estdo11: Codigo sipres que indica que la ops esta vencida.
								   9- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15: Codigo Mega que indica que la ops esta entregada
								   10- @vlr_uno: Almacena el valor 1
								   11- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14: Codigo Mega que indica que la Ops fue anulada
								   12- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10: Codigo Mega que indica que la ops esta vencida.
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.spAsCambiarEstadoPrestacionesMigracionEstadosSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se modifica prcedimiento para permitir el guardado conceptos.
							   Se cambio referencia de tabla temporal por tabla temporal de conceptos. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/02 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega borrado del historico de los conceptos de las prestaciones migradas y borrado
							   de los conceptos de la tabla tbAsConceptosServiciosSolicitados.
							   Se cambia Merge de guardado en la tabla tbAsConceptosServiciosSolicitados por insert ya que 
							   al eliminarce los conceptos con anterioridad no hay datos para actualizar. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/10 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega borrado del historico de descarga de los conceptos de las prestaciones migradas. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>  													
* Descripcion			: <\DM Se agrega clausula Group By en el Merge. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsCambiarEstadoConceptosMigracionEstadosSipresMega]
			@usro_sstma									UdtUsuario
As
Begin
	Set NoCount On
	
	Declare	@fin_vigencia								Date,
			@vldo_si									Udtlogico,
			@cnsctvo_cdgo_mdlo_cra46					UdtConsecutivo,
			@vlr_cro									Int,
			@cnsctvo_cdgo_estdo8						UdtConsecutivo,
			@cnsctvo_cdgo_estdo3						UdtConsecutivo,
			@cnsctvo_cdgo_estdo11						UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15	UdtConsecutivo,
			@fcha_actl									Date,
			@vlr_uno									Int,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14	UdtConsecutivo,
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10	UdtConsecutivo;

	Set @fin_vigencia = '9999-12-31 00:00:00.000'; 
	Set @vldo_si = 'S';
	Set @cnsctvo_cdgo_mdlo_cra46 = 46;
	Set @vlr_cro = 0;
	Set @cnsctvo_cdgo_estdo8 = 8;
	Set @cnsctvo_cdgo_estdo3 = 3;
	Set @cnsctvo_cdgo_estdo11 = 11;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo15 = 15;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo14 = 14;
	Set @fcha_actl = getDate();
	Set @vlr_uno = 1;
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo10 = 10;

	-- Se elimina el historico de los conceptos para evitar error de llave foranea
	Delete		hecss
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbAsProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	BDCna.gsa.tbASHistoricoEstadosConceptosServicioSolicitado hecss With(NoLock)
	On			hecss.cnsctvo_cncpto_prcdmnto_slctdo = css.cnsctvo_cncpto_prcdmnto_slctdo

	-- 
	Delete		hecss 
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_mdcmnto_slctdo = p.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	Inner Join	BDCna.gsa.tbASHistoricoEstadosConceptosServicioSolicitado hecss With(NoLock)
	On			hecss.cnsctvo_cncpto_prcdmnto_slctdo = css.cnsctvo_cncpto_prcdmnto_slctdo

	-- Elimina el historico de descargas de los conceptos
	Delete		hdf
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbAsProcedimientosInsumosSolicitados pis With(NoLock)
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	BDCna.gsa.tbASHistoricoDescargaFormatos hdf With(NoLock)
	On			hdf.cnsctvo_cncpto_prcdmnto_slctdo = css.cnsctvo_cncpto_prcdmnto_slctdo

	Delete		hdf 
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_mdcmnto_slctdo = p.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
	Inner Join	BDCna.gsa.tbASHistoricoDescargaFormatos hdf With(NoLock)
	On			hdf.cnsctvo_cncpto_prcdmnto_slctdo = css.cnsctvo_cncpto_prcdmnto_slctdo

	-- Se aliminan los conceptos de la tbAsConceptosServiciosSolicitados
	Delete		css
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbAsProcedimientosInsumosSolicitados pis
	On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css
	On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo				
				
	-- 
	Delete		css 
	From		#tempPrestaciones p
	Inner Join	#tempConceptosPrestaciones cp
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_mdcmnto_slctdo = p.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms
	On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css
	On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
				
	-- Guarda la informacion de los conceptos de las prestaciones.
	Insert Into bdCNA.gsa.tbASConceptosServicioSolicitado
	(	
				cnsctvo_prcdmnto_insmo_slctdo,			cnsctvo_mdcmnto_slctdo,					cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				fcha_crcn,								usro_crcn,								fcha_ultma_mdfccn,
				usro_ultma_mdfccn,						cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz
	)
	Select		cp.cnsctvo_prcdmnto_insmo_slctdo,		cp.cnsctvo_mdcmnto_slctdo,				cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				cp.fcha_crcn,							cp.usro_crcn,							cp.fcha_ultma_mdfccn,
				cp.usro_ultma_mdfccn,					cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz
	From		#tempConceptosPrestaciones cp
	Inner Join	#tempPrestaciones p
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_mdcmnto_slctdo = p.cnsctvo_mdcmnto_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	Group By	cp.cnsctvo_prcdmnto_insmo_slctdo,		cp.cnsctvo_mdcmnto_slctdo,				cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				cp.fcha_crcn,							cp.usro_crcn,							cp.fcha_ultma_mdfccn,
				cp.usro_ultma_mdfccn,					cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz


	-- Guarda la informacion de los conceptos de las prestaciones.
	Insert Into bdCNA.gsa.tbASConceptosServicioSolicitado
	(	
				cnsctvo_prcdmnto_insmo_slctdo,			cnsctvo_mdcmnto_slctdo,					cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				fcha_crcn,								usro_crcn,								fcha_ultma_mdfccn,
				usro_ultma_mdfccn,						cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz
	)
	Select		cp.cnsctvo_prcdmnto_insmo_slctdo,		cp.cnsctvo_mdcmnto_slctdo,				cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				cp.fcha_crcn,							cp.usro_crcn,							cp.fcha_ultma_mdfccn,
				cp.usro_ultma_mdfccn,					cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz
	From		#tempConceptosPrestaciones cp
	Inner Join	#tempPrestaciones p
	On			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	And			cp.nuam = p.nuam
	And			cp.cnsctvo_prcdmnto_insmo_slctdo = p.cnsctvo_prcdmnto_insmo_slctdo
	Inner Join	bdCNA.gsa.tbAsServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
	Group By	cp.cnsctvo_prcdmnto_insmo_slctdo,		cp.cnsctvo_mdcmnto_slctdo,				cnsctvo_ops,
				cnsctvo_prstcn,							vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,
				cdgo_intrno_prstdr_atrzdo,				cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,
				cnsctvo_cdgo_ofcna_cm,					vlr_cncpto_cm,							gnrdo,
				fcha_utlzcn_dsde,						fcha_utlzcn_hsta,						nmro_unco_ops,
				cp.fcha_crcn,							cp.usro_crcn,							cp.fcha_ultma_mdfccn,
				cp.usro_ultma_mdfccn,					cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,	ds_vldz
				
	-- 
	Update		cp
	Set			cnsctvo_cncpto_prcdmnto_slctdo = css.cnsctvo_cncpto_prcdmnto_slctdo
	From		#tempConceptosPrestaciones  cp
	Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
	On			css.cnsctvo_prstcn = cp.cnsctvo_prstcn						
	And			css.nmro_unco_ops = cp.nmro_unco_ops							
	And			css.cnsctvo_cdgo_cncpto_gsto = cp.cnsctvo_cdgo_cncpto_gsto

	--Se buscan el historico de conceptos que esta vigente y se cambia la fecha de fin de vigencia
	Update		hcs
	Set			fn_vgnca = @fcha_actl ,
				usro_ultma_mdfccn = @usro_sstma,
				fcha_ultma_mdfccn = @fcha_actl	
	From		BDCna.gsa.tbASHistoricoEstadosConceptosServicioSolicitado hcs With(RowLock)
	Inner Join	#tempConceptosPrestaciones sv
	On			hcs.cnsctvo_cncpto_prcdmnto_slctdo = sv.cnsctvo_cncpto_prcdmnto_slctdo
	Where		@fcha_actl Between hcs.inco_vgnca And hcs.fn_vgnca;

	-- Se inserta el historico del cambio de estado de los conceptos
	Merge BDCna.gsa.tbASHistoricoEstadosConceptosServicioSolicitado With(RowLock) As Target
	Using
	(
		Select		cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
		From		#tempConceptosPrestaciones
		Where		cnsctvo_cncpto_prcdmnto_slctdo Is Not Null
		Group By	cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
	) As Source
	On
	(
		Target.cnsctvo_cncpto_prcdmnto_slctdo = Source.cnsctvo_cncpto_prcdmnto_slctdo			And
		Target.cnsctvo_cdgo_estdo_srvco_slctdo = Source.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
	)
	When Not Matched Then
		Insert 
		(
					cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_cdgo_estdo_srvco_slctdo,		inco_vgnca,
					fn_vgnca,							fcha_fn_vldz_rgstro,					vldo,
					usro_crcn,							fcha_crcn,								usro_ultma_mdfccn,
					fcha_ultma_mdfccn
		)
		Values
		(		
				Source.cnsctvo_cncpto_prcdmnto_slctdo,	Source.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo,		@fcha_actl,
				@fin_vigencia,							@fin_vigencia,										@vldo_si,
				@usro_sstma,							@fcha_actl,											@usro_sstma,
				@fcha_actl
		);

End