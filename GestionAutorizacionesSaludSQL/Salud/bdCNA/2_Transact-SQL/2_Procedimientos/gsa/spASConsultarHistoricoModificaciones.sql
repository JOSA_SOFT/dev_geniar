USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASConsultarHistoricoModificaciones]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASConsultarHistoricoModificaciones') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASConsultarHistoricoModificaciones AS SELECT 1;'
END
GO

/*-----------------------------------------------------------------------------------------------------------------------    
* Metodo o PRG     : spASConsultarHistoricoModificaciones  
* Desarrollado por : <\A Ing. Jorge Rodriguez De León   A\>    
* Descripcion      : <\D Procedimiento que permite consultar el historico de modificaciones de las solicitudes D\>    
* Observaciones  : <\O O\>    
* Parametros     : <\P P\>    
* Variables      : <\V V\>    
* Fecha Creacion : <\FC 2016/03/09 FC\>    
*    
*------------------------------------------------------------------------------------------------------------------------      
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------    
* Modificado Por     : <\AM Jorge Rodriguez  SETI SAS AM\>    
* Descripcion        : <\DM	 Se modifica SP para que tome el contenido del log de trazabilidad que registrará
							 la descripción mas claras DM\>    
* Nuevos Parametros  : <\PM PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 28-03-2017	FM\>    
*-----------------------------------------------------------------------------------------------------------------------*/ 
--exec [gsa].[spASConsultarHistoricoModificaciones]  393890
--exec [gsa].[spASConsultarHistoricoModificaciones]  807
ALTER PROCEDURE  [gsa].[spASConsultarHistoricoModificaciones]  
  @lnConsecutivoSolicitud udtConsecutivo
AS  

Begin 

    SET NOCOUNT ON 

	Declare @ldFechaActual						Datetime,
			@ldprntss_abrto				char(1),
			@ldprntss_crrdo				char(1),
			@ldspco_blnco				char(1),
			@ldcnsctvo_cdgo_tpo_srvco	udtConsecutivo

	Create 
	Table #tmpConsultarHistoricoModificaciones(
											cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
											dscrpcn_elmnto_mdfccn		udtDescripcion,
											nmro_atrzcn					udtConsecutivo,
											cnsctvo_srvco_slctdo		udtConsecutivo,
											dscrpcn_srvco_slctdo		udtDescripcion,
											tpo_mdfccn					udtDescripcion,
											fcha_mdfccn					date,
											vlr_antrr					udtDescripcion,
											dscrpcn_vlr_antrr			udtDescripcion,
											vlr_nvo						udtDescripcion,
											dscrpcn_vlr_nvo				udtDescripcion,
											nmro_atrzcn_antrr			udtConsecutivo,
											usro_mdfccn					udtUsuario,
											cnsctvo_csl_mdfccn			udtConsecutivo,
											dscrpcn_csl_mdfccn			udtDescripcion,
											cnsctvo_cdgo_tpo_srvco		udtConsecutivo,
											obsrvcns					udtObservacion,
											dscrpcn_orgn_mdfccn			udtDescripcion,
											orgn_mdfccn					varchar(100)
											)
	Set @ldFechaActual						= getDate()
	Set @ldcnsctvo_cdgo_tpo_srvco			= 5 --Tipo Prestación CUMS
	Set	@ldprntss_abrto						= '('
	Set @ldprntss_crrdo						= ')'
	Set	@ldspco_blnco						= ' '

	--Se obtienen los registros por solicitud
	Insert into #tmpConsultarHistoricoModificaciones(
													cnsctvo_slctd_atrzcn_srvco	,dscrpcn_elmnto_mdfccn	,cnsctvo_srvco_slctdo,
													tpo_mdfccn					,fcha_mdfccn			,vlr_antrr			 ,
													vlr_nvo						,usro_mdfccn			,cnsctvo_csl_mdfccn	 ,
													obsrvcns					,orgn_mdfccn
													)
	Select		@lnConsecutivoSolicitud	, 
			    elmnto_orgn_mdfccn,
				t.cnsctvo_srvco_slctdo,
				dto_elmnto_orgn_mdfccn
				,t.fcha_crcn					,dscrpcn_vlr_antrr		   ,
				t.dscrpcn_vlr_nvo				,t.usro_crcn					,t.cnsctvo_cdgo_csa_nvdd,
				t.obsrvcns				,orgn_mdfccn
	From		BDCna.gsa.tbASTrazaModificacion t With(NoLock)
	Where		t.cnsctvo_slctd_atrzcn_srvco = @lnConsecutivoSolicitud
	and			t.cnsctvo_slctd_atrzcn_srvco is not null
	and			t.cnsctvo_srvco_slctdo	is not null

	--Se actualiza numero de la OPS de los medicamentos
	Update hm
	Set nmro_atrzcn = c.nmro_unco_ops
	From #tmpConsultarHistoricoModificaciones hm
	Inner Join	BDCna.gsa.tbASServiciosSolicitados s With(NoLock)
	On			hm.cnsctvo_srvco_slctdo				= s.cnsctvo_srvco_slctdo
	Inner Join	BDCna.gsa.tbASMedicamentosSolicitados m With(NoLock)		
	On			s.cnsctvo_srvco_slctdo				= m.cnsctvo_srvco_slctdo
	Inner Join	BDCna.gsa.tbASConceptosServicioSolicitado c With(NoLock)	
	On			c.cnsctvo_mdcmnto_slctdo			= m.cnsctvo_mdcmnto_slctdo
	 
	--Se actualiza numero de la OPS de los procedimientos
	Update hm
	Set nmro_atrzcn = c.nmro_unco_ops
	From  #tmpConsultarHistoricoModificaciones hm
	Inner Join	BDCna.gsa.tbASServiciosSolicitados s With(NoLock)
	On			hm.cnsctvo_srvco_slctdo				= s.cnsctvo_srvco_slctdo
	Inner Join	BDCna.gsa.tbASProcedimientosInsumosSolicitados m With(NoLock)	
	On			s.cnsctvo_srvco_slctdo				= m.cnsctvo_srvco_slctdo
	Inner Join	BDCna.gsa.tbASConceptosServicioSolicitado c With(NoLock)	
	On			c.cnsctvo_prcdmnto_insmo_slctdo		= m.cnsctvo_prcdmnto_insmo_slctdo


	--Se actualiza la descripción de la prestación
	Update		t
	Set			dscrpcn_srvco_slctdo	= c.dscrpcn_srvco_slctdo,
				cnsctvo_cdgo_tpo_srvco	= c.cnsctvo_cdgo_tpo_srvco
	From		#tmpConsultarHistoricoModificaciones t
	Inner Join	BDCna.gsa.tbASServiciosSolicitados c With(NoLock)
	On			c.cnsctvo_srvco_slctdo = t.cnsctvo_srvco_slctdo 

	--Se adiciona Descripción de la prestación.  Tener en cuenta que si es un CUMS debe visualizarse 
	--la descripción de la prestación concatenada con la concentración y la presentación) CC 050
	Update h
	Set	dscrpcn_srvco_slctdo = Concat(h.dscrpcn_srvco_slctdo,@ldspco_blnco, @ldprntss_abrto,
								Rtrim(Ltrim(m.cncntrcn_dss)), @ldspco_blnco, Rtrim(Ltrim(c.dscrpcn_cncntrcn)), @ldspco_blnco,
								Rtrim(Ltrim(m.prsntcn_dss )), @ldspco_blnco, Rtrim(Ltrim(p.dscrpcn_prsntcn)),@ldprntss_crrdo) 

	From		#tmpConsultarHistoricoModificaciones h
	Inner Join	BDCna.gsa.tbASMedicamentosSolicitados m With(NoLock)
	On			m.cnsctvo_srvco_slctdo				= h.cnsctvo_srvco_slctdo
	Inner Join	bdSisalud.dbo.tbConcentracion_Vigencias c With(NoLock)
	On			m.cnsctvo_cdgo_undd_cncntrcn_dss	= c.cnsctvo_vgnca_cncntrcn
	Inner Join	bdSisalud.dbo.tbPresentaciones_Vigencias p With(NoLock)
	On			p.cnsctvo_vgnca_prsntcn				= m.cnsctvo_cdgo_prsntcn_dss
	Where		h.cnsctvo_cdgo_tpo_srvco			 =  @ldcnsctvo_cdgo_tpo_srvco   
	And			@ldFechaActual Between c.inco_vgnca And c.fn_vgnca
	And			@ldFechaActual Between p.inco_vgnca And p.fn_vgnca

	--Se actualiza la descripción de la causal de modificación
	Update		t
	Set			dscrpcn_csl_mdfccn		= c.dscrpcn_cdgo_csa_nvdd
	From		#tmpConsultarHistoricoModificaciones t
	inner Join	BDCna.prm.tbASCausaNovedad_Vigencias c With(NoLock)
	On			t.cnsctvo_csl_mdfccn	= c.cnsctvo_cdgo_csa_nvdd
	Where		@ldFechaActual Between c.inco_vgnca And c.fn_vgnca

	Select		cnsctvo_slctd_atrzcn_srvco	,dscrpcn_elmnto_mdfccn		,nmro_atrzcn			,
				cnsctvo_srvco_slctdo		,dscrpcn_srvco_slctdo		,tpo_mdfccn	,
				fcha_mdfccn					,vlr_antrr					,vlr_nvo,
				nmro_atrzcn_antrr			,usro_mdfccn				,cnsctvo_csl_mdfccn		,
				dscrpcn_csl_mdfccn			,cnsctvo_cdgo_tpo_srvco		,obsrvcns			
				
	From		#tmpConsultarHistoricoModificaciones
	Order By	fcha_mdfccn DESC

	Drop table #tmpConsultarHistoricoModificaciones
	
End
GO
