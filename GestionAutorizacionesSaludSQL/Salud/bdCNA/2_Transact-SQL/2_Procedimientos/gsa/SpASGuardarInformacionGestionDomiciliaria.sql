USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[SpASGuardarInformacionGestionDomiciliaria]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spASGuardarInformacionGestionDOMI
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D													 D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2016/01/18										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
--EXEC [gsa].[SpASGuardarInformacionGestionDomiciliaria] 
ALTER PROCEDURE [gsa].[SpASGuardarInformacionGestionDomiciliaria] 
@infrmcion_prstcion		xml,
@cdgo_rsltdo			udtConsecutivo  output,
@mnsje_rsltdo			varchar(2000)	output
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @lcdscrpcn_tpo_adtra						udtDescripcion;

	SET @lcdscrpcn_tpo_adtra = 'DOMI';
	
	Exec BDCna.gsa.spASGuardarInformacionGestionAuditoria	@infrmcion_prstcion, 
															@lcdscrpcn_tpo_adtra, 
															@cdgo_rsltdo  OUTPUT, 
															@mnsje_rsltdo OUTPUT

END

GO
