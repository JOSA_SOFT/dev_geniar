USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoZonaIpsPrimaria]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoZonaIpsPrimaria
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D Zona de IPS Primaria											 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoZonaIpsPrimaria] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @cmple char(1) = 'S',
		  @vlor_cro int = 0;

  --Se valida la Zona de la IPs del afiliado
  IF EXISTS (SELECT
      TCO.id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_zna = ISP.cdgo_zna
      AND ISP.cdgo_zna IS NOT NULL
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
  BEGIN
    --Se actualizan los prestadores que apliquen el filtro, siempre y cuando se haya encontrado mínimo uno
    --que cumpla el filtro de ciudad para la prestación a evaluar.
    UPDATE TCO
    SET cmple = @cmple
    FROM #tmpPrestadoresDestino TCO
    INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
      ON ISP.id = TCO.id_slctd_x_prstcn
      AND TCO.cnsctvo_cdgo_zna = ISP.cdgo_zna
      AND ISP.cdgo_zna IS NOT NULL
	  WHERE ISP.mrca_msmo_prstdor = @vlor_cro;

    EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
  END
END

GO
