USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASNotificacionDevolucionAfiliadoWeb]    Script Date: 10/03/2017 12:10:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spASNotificacionDevolucionAfiliadoWeb') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spASNotificacionDevolucionAfiliadoWeb AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------------------------------------- 
* Metodo o PRG 			: spASNotificacionDevolucionAfiliadoWeb
* Desarrollado por		: <\A Ing. Victor Hugo Gil Ramos A\>
* Descripcion			: <\D 
                              Procedimiento que permite realizar la orquestacion de los procedimientos
							  que se utilizan para el proceso de envio de notificacion al afiliado 
							  de la devolucion de los servicios desde el ingreso de la solicitud via
							  web							  					  										
						  D\>
* Observaciones			: <\O O\>
* Parametros			: <\P P\>
* Variables				: <\V V\>
* Fecha Creacion		: <\FC 2017/03/06 FC\>
*------------------------------------------------------------------------------------------------------------------------ 
* DATOS DE MODIFICACION
*------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*------------------------------------------------------------------------------------------------------------------------*/
--Exec [gsa].[spASNotificacionDevolucionAfiliadoWeb] 130665, 'vgil'

ALTER PROCEDURE [gsa].[spASNotificacionDevolucionAfiliadoWeb] 
	@cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
	@usrio			            Varchar(50)
AS

BEGIN
	SET NOCOUNT ON

	DECLARE	@cnstrccn_trma_xml	XML, 
	        @plntlla            Int       

	Create
	Table  #tmpRspstaXML(rspsta XML NOT NULL)	

	--Temporal con los datos básicos de la solicitud
	Create 
	Table  #tmpSlctdes(id_sld_rcbda                  udtConsecutivo IDENTITY (1, 1),
					   cnsctvo_slctd_srvco_sld_rcbda udtConsecutivo	               ,
					   nmro_slctd					 Varchar(16)                   ,
					   plntlla						 Int 	
					  )

	--Temporal con los datos necesario del afiliado
	Create 
	Table  #tmpAflds(id                                       udtConsecutivo Identity(1,1),
					 id_sld_rcbda					          udtConsecutivo			  ,
					 cnsctvo_slctd_srvco_sld_rcbda	          udtConsecutivo			  ,
					 nmro_slctd_atrzcn_ss			          Varchar(16)				  ,	
					 nmbre_afldo					          Varchar(100)				  ,
					 cnsctvo_cdgo_tpo_idntfccn_afldo          udtConsecutivo			  ,
					 nmro_idntfccn_afldo			          udtNumeroIdentificacionLargo,	
					 cnsctvo_cdgo_tpo_cntrto		          udtConsecutivo	          ,
					 nmro_cntrto					          udtNumeroFormulario		  ,		
					 cnsctvo_cdgo_pln				          udtConsecutivo			  ,	
					 nmro_unco_idntfccn_afldo		          udtConsecutivo			  ,	
					 cnsctvo_cdgo_rngo_slrl			          udtConsecutivo			  ,	
					 cdgo_ips_prmra					          udtCodigoIps				  ,				
					 cnsctvo_bnfcro_cntrto			          udtConsecutivo			  ,	
					 cnsctvo_cdgo_sde_ips_prmra		          udtConsecutivo			  ,	
					 cnsctvo_cdgo_estdo_drcho		          udtConsecutivo			  , 	
					 dscrpcn_csa_drcho				          Varchar(150)				  ,	
					 cdgo_tpo_idntfccn_afldo		          udtCodigo					  ,	
		             dscrpcn_pln					          Varchar(150)				  ,	
					 eml							          udtEmail					  ,	
					 sde_afldo						          udtCodigo					  ,	
					 dscrpcn_sde					          udtDescripcion			  ,	
					 bcc							          udtEmail					  ,	
					 lnea_ncnal						          udtDescripcion			  ,	
					 crreo_atncn_clnte				          udtEmail		              ,
					 cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco udtConsecutivo				 		
					)
	
	--Temporal con los datos necesario del proveedor
	Create 
	Table  #tmpPrvdor(id		   udtConsecutivo Identity(1,1),
					  id_sld_rcbda udtConsecutivo			   ,		
					  crreo		   udtEmail
					)
					
	Set @plntlla = 5 -- Formato de Devolucion    

	/*Poblar tabla temporal Solicitudes*/
	Insert 
	Into   #tmpSlctdes(cnsctvo_slctd_srvco_sld_rcbda, nmro_slctd, plntlla)
	Select @cnsctvo_slctd_atrzcn_srvco, nmro_slctd_atrzcn_ss, @plntlla
	From   bdCNA.gsa.tbASSolicitudesAutorizacionServicios WITH (NOLOCK)
	Where  cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;		

	
	/*Obtener y cargar en la tabla temporal la información del afiliado*/
	Execute bdCNA.gsa.spASObtenerDatosNotificacionAfiliado

	/*
	 Construir la trama XML para la generacion del formato de devolucion que 
	 será enviada al afiliado
	*/
	Execute bdCNA.gsa.spASTramaXMLNotificacionDevolucionPlantilla5  @usrio, @cnstrccn_trma_xml OUTPUT;


	/*Se registra la solicitud para el envio de correo*/
	Insert Into #tmpRspstaXML
	Execute bdProcesosSalud.dbo.spEMGuardarMensajesCorreoMasivo @cnstrccn_trma_xml;					
				
	Select CAST(rspsta AS NVARCHAR(MAX)) As msje_rspsta From #tmpRspstaXML;	
		
	Drop Table #tmpAflds
	Drop Table #tmpPrvdor
	Drop Table #tmpSlctdes						
End
