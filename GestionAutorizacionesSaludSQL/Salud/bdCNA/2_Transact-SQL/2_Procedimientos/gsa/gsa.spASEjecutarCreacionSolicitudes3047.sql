USE bdCNA
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASEjecutarCreacionSolicitudes3047') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASEjecutarCreacionSolicitudes3047 AS SELECT 1;'
END
GO

/*----------------------------------------------------------------------------------------
* Metodo o PRG : gsa.spASEjecutarCreacionSolicitudes3047
* Desarrollado por : <\A Jorge Andres Garcia Erazo A\>
* Descripcion  : <\D Este sera el SP que sea llamado por el job que se encargue de migrar las solicitudes de 3047
					 a MEGA. D\>    
* Observaciones : <\O O\>    
* Parametros  : <\P En caso de que se vaya a migrar una solicitud en particular se pasa el @cnsctvo_slctd_ops en
					caso de ser null, se realizara masivo  P\>
* Variables   : <\V V\>
* Fecha Creacion : <\FC 2017/06/12 FC\>
* Ejemplo: 
    <\EJ
        Incluir un ejemplo de invocacion al procedimiento almacenado
        EXEC gsa.spASEjecutarCreacionSolicitudes3047 2696
    EJ\>

*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta llamado a sp que actualiza los estados de la solicitud y prestacion DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/13 FM\>    
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM Jorge Andres Garcia Erazo AM\>    
* Descripcion        : <\DM Se ajusta orden de llamado de los pasos para que llame uno de los ultimos sea la actualizacion de estados DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/19 FM\>    
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASEjecutarCreacionSolicitudes3047(
	@cnsctvo_slctd_ops udtConsecutivo = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tpo_prcso			int,
			@cnsctvo_prcso		int,
			@mensajeError		varchar(1000),
			@cdgo_rsltdo		int,
			@mnsje_rsltdo		varchar(1000),
			@cnsctvo_lg			int,
			@rgstro_log_poblar	int,
			@rgstro_log_validar	int,
			@rgstro_lg_grdr_estdos int,
			@rgstro_lg_grdr_fcha_entrga int,
			@rgstro_lg_grdr_drrcnmnto	int,
			@rgstro_lg_grdr_nmro_slctd	int,
			@mnsge_pblr			varchar(1000),
			@codigoExito		char(2),
			@codigoError		char(2),
			@codigoSalir		char(2),
			@estdo_pso1			int,  
			@estdo_pso2			int,  
			@estdo_pso3			int,  
			@estdo_pso4			int,  
			@estdo_pso5			int,  
			@estdo_pso6			int,  
			@estdo_pso7			int,  
			@estdo_pso8			int,  
			@estdo_pso9			int,  
			@estdo_pso10		int,  
			@estdo_pso11		int,  
			@estdo_pso12		int, 
			@estdo_pso13		int,  
			@cnsctvo_estdo_err	int,
			@mnsge_vldr			varchar(1000),
			@mnsge_grddo_slctd	varchar(1000),
			@codigoExitoso		int,
			@cnsctvo_cdgo_tpo_incnsstnca int,
			@rgstro_lg_grdr_slctd	int,
			@rgstro_lg_grdr_afldo int,
			@rgstro_lg_grdr_ips	int,
			@rgstro_lg_grdr_mdco int,
			@rgstro_lg_grdr_dgnstco int,
			@rgstro_lg_grdr_prstcns	int,
			@rgstro_lg_grdr_dts_mgrcn int,
			@mnsge_grddo_afldo	varchar(1000),
			@mnsge_grddo_ips	varchar(1000),
			@mnsge_grddo_mdco	varchar(1000),
			@mnsge_grddo_dgnstco	varchar(1000),
			@mnsge_grddo_prstcns	varchar(1000),
			@mnsge_grddo_estdos	varchar(1000),
			@mnsge_grddo_fcha_entrga varchar(1000),
			@mnsge_grddo_drrcnmnto varchar(1000),
			@mnsge_grddo_nmro_slctd varchar(1000),
			@mnsge_grddo_dts_mgrcn varchar(1000),
			@estdo_ejccn varchar(5),
			@codigoErrorDireccionamigo varchar(5),
			@usro_ejccn_drccnmnto varchar(50),
			@mensaje_error_direccionamiento varchar(2000),
			@msje_rspsta nvarchar(max),
			@throw_error_number int,
			@throw_error_state tinyint,
			@fechaActual datetime,
			@NO char(1),
			@uno int

	CREATE TABLE #solicitudes3047(
		cnsctvo_slctd_ops udtConsecutivo,
		exste_mga char(1) default 'N'
	)

	--Tablas necesarias para los sps que realizan el guardado en MEGA.
	CREATE TABLE #Idsolicitudes		(cnsctvo_slctd_atrzcn_srvco					udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdAfiliados		(cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)			
	CREATE TABLE #IdIPS				(cnsctvo_ips_slctd_atrzcn_srvco				udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdHospitalaria	(cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco	udtConsecutivo NOT NULL, idXML	int NOT NULL)					
	CREATE TABLE #IdMedico			(cnsctvo_mdco_trtnte_slctd_atrzcn_srvco		udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdDiagnostico		(cnsctvo_dgnstco_slctd_atrzcn_srvco			udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServicios		(cnsctvo_srvco_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdServiciosOr		(cnsctvo_dto_srvco_slctdo_orgnl				udtConsecutivo NOT NULL, idXML	int NOT NULL, nmro_prstcn INT)
	CREATE TABLE #IdMedicamentos	(cnsctvo_mdcmnto_slctdo						udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #IdInsumos			(cnsctvo_prcdmnto_insmo_slctdo				udtConsecutivo NOT NULL, idXML	int NOT NULL)
	CREATE TABLE #solicitudes_guardadas (id int identity(1,1) NOT NULL, cnsctvo_slctd_atrzcn_srvco udtconsecutivo NOT NULL, Identificador_sol udtconsecutivo NOT NULL)
		
	CREATE TABLE #Tempo_Solicitudes
	( 
		id_tbla								int identity
		,id									Int  NOT NULL										
		,cnsctvo_cdgo_orgn_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_srvco_slctdo		udtConsecutivo NULL
		,cnsctvo_cdgo_prrdd_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_ubccn_pcnte		udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_slctd				udtConsecutivo NULL
		,jstfccn_clnca						varchar(2000) NULL
		,cnsctvo_cdgo_mdo_cntcto_slctd		udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_trnsccn_srvco_sld	udtConsecutivo NULL
		,nmro_slctd_prvdr					varchar(15) NULL
		,nmro_slctd_pdre					varchar(15) NULL
		,cdgo_eps							char(10) NULL
		,cnsctvo_cdgo_clse_atncn			udtConsecutivo NULL
		,cnsctvo_cdgo_frma_atncn			udtConsecutivo NULL
		,fcha_slctd							datetime NULL
		,anio_slctd							char(4) NULL
		,nmro_slctd_atrzcn_ss				varchar(16) NULL
		,nmro_slctd_ss_rmplzo				varchar(15) NULL
		,cnsctvo_cdgo_ofcna_atrzcn			udtConsecutivo NULL
		,nuam								numeric(18,0) NULL
		,srvco_hsptlzcn						varchar(30) NULL
		,ga_atncn							varchar(30) NULL
		,cnsctvo_prcso_vldcn				udtConsecutivo NULL
		,cnsctvo_prcso_mgrcn				udtConsecutivo NULL
		,mgrda_gstn							int NULL
		,spra_tpe_st						udtLogico NULL
		,dmi								udtLogico NULL
		,obsrvcn_adcnl						UdtObservacion NULL
		,usro_crcn							udtUsuario NULL
		,cdgo_orgn_atncn					udtCodigo NULL
		,cdgo_tpo_srvco_slctdo				udtCodigo NULL
		,cdgo_prrdd_atncn					udtCodigo NULL
		,cdgo_tpo_ubccn_pcnte				udtCodigo NULL
		,cdgo_clse_atncn					char(3) NULL
		,cdgo_frma_atncn					udtCodigo NULL
		,cdgo_mdo_cntcto_slctd				udtCodigo NULL
		,cdgo_tpo_trnsccn_srvco_sld			udtCodigo NULL
		,cnsctvo_cdgo_tpo_orgn_slctd		UdtConsecutivo NULL							
	)

	CREATE TABLE #Tempo_Afiliados 
	( 
		id_tbla										int identity
		,id											Int	NOT NULL
		,cnsctvo_cdgo_tpo_idntfccn_afldo			udtConsecutivo NULL default 0
		,nmro_idntfccn_afldo						udtNumeroIdentificacionLargo NULL
		,cnsctvo_cdgo_dprtmnto_afldo				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cdd_rsdnca_afldo				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cbrtra_sld					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_pln						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_pln							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_estdo_pln						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_sxo							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_vnclcn_afldo				udtConsecutivo NULL default 0
		,nmro_unco_idntfccn_afldo					udtConsecutivo NULL default 0
		,cnsctvo_afldo_slctd_orgn					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_estdo_drcho					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_cntrto					udtConsecutivo NULL default 0
		,nmro_cntrto								udtNumeroFormulario NULL default ''
		,cnsctvo_bnfcro_cntrto						udtConsecutivo NULL default 0
		,cdgo_ips_prmra								udtCodigoIps NULL default ''
		,cnsctvo_cdgo_sde_ips_prmra					udtConsecutivo NULL default 0
		,rcn_ncdo									udtLogico default 'N'
		,prto_mltple								udtLogico default 'N'
		,nmro_hjo_afldo								udtConsecutivo default 0
		,cnsctvo_cdgo_sxo_rcn_ncdo					udtConsecutivo default 0
		,ttla										udtLogico NULL default 'N'
		,cnsctvo_cdgo_chrte							udtConsecutivo NULL default 0
		,cnsctvo_cdgo_rngo_slrl						udtConsecutivo NULL default 0
		,usro_crcn									udtUsuario NULL					
		,prmr_aplldo_afldo							udtApellido NULL
		,sgndo_aplldo_afldo							udtApellido NULL
		,prmr_nmbre_afldo							udtNombre NULL
		,sgndo_nmbre_afldo							udtNombre NULL
		,cdgo_tpo_idntfccn_afldo					udtCodigo NULL
		,fcha_ncmnto_afldo							datetime NULL
		,drccn_afldo								udtDireccion NULL
		,tlfno_afldo								udtTelefono NULL
		,cdgo_dprtmnto_afldo						char(3) NULL
		,dscrpcn_dprtmnto_afldo						udtDescripcion NULL
		,cdgo_cdd_rsdnca_afldo						udtCiudad NULL
		,dscrpcn_cdd_rsdnca_afldo					udtDescripcion NULL
		,tlfno_cllr_afldo							char(30) NULL
		,crro_elctrnco_afldo						udtEmail NULL
		,cdgo_cbrtra_sld							varchar(5) NULL
		,cdgo_pln									udtCodigo NULL
		,cdgo_sxo									udtCodigo NULL
		,cdgo_sxo_rcn_ncdo							udtCodigo NULL
		,cdgo_tpo_pln								udtCodigo NULL
		,cdgo_tpo_vnclcn_afldo						udtCodigo NULL
		,cdgo_estdo_pln								udtCodigo NULL
		,fcha_ncmnto_rcn_ncdo						datetime NULL
		,edd_afldo_ans								UdtConsecutivo
		,edd_afldo_mss								UdtConsecutivo
		,edd_afldo_ds								UdtConsecutivo
	)

	CREATE TABLE #Tempo_IPS		
	(  
		id_tbla										int identity
		,id											Int NOT NULL	
		,cnsctvo_cdgo_tpo_idntfccn_ips_slctnte		udtConsecutivo NULL default 0
		,nmro_idntfccn_ips_slctnte					udtNumeroIdentificacionLargo NULL default ''
		,cdgo_intrno								udtCodigoIps NULL default ''
		,nmro_indctvo_prstdr						udtConsecutivo NULL default 0
		,cnsctvo_cdgo_dprtmnto_prstdr				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_cdd_prstdr					udtConsecutivo NULL default 0
		,cnsctvo_ips_slctd_orgn						udtConsecutivo NULL default 0
		,adscrto									udtLogico NULL default 'N'
		,nmro_unco_idntfccn_prstdr					udtConsecutivo NULL default 0
		,usro_crcn									udtUsuario NULL	
		,nmbre_ips									udtDescripcion NULL
		,nmbre_scrsl								udtDescripcion NULL
		,cdgo_tpo_idntfccn_ips_slctnte				udtCodigo NULL default ''
		,dgto_vrfccn								int NULL
		,cdgo_hbltcn								char(12) NULL
		,drccn_prstdr								udtDireccion NULL
		,tlfno_prstdr								udtTelefono NULL
		,cdgo_dprtmnto_prstdr						char(3) NULL
		,cdgo_cdd_prstdr							udtCiudad NULL																
	)

	CREATE TABLE #Tempo_Medicos 
	(   
		id_tbla									int identity
		,id										Int	NOT NULL
		,cnsctvo_cdgo_tpo_aflcn_mdco_trtnte		udtConsecutivo default 0
		,cnsctvo_cdgo_espcldd_mdco_trtnte		udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_idntfccn_mdco_trtnte	udtConsecutivo NULL default 0
		,nmro_idntfccn_mdco_trtnte				udtNumeroIdentificacionLargo NULL default ''
		,rgstro_mdco_trtnte						varchar(20) NULL default ''
		,cnsctvo_mdco_trtnte_slctd_orgn			udtConsecutivo default 0
		,adscrto								udtLogico NULL default 'N'
		,nmro_unco_idntfccn_mdco				udtConsecutivo NULL default 0
		,usro_crcn								udtUsuario NULL
		,cdgo_espcldd_mdco_trtnte				char(3) NULL default ''
		,cdgo_tpo_idntfccn_mdco_trtnte			char(2) NULL default ''
		,prmr_nmbre_mdco_trtnte					udtNombre NULL
		,sgndo_nmbre_mdco_trtnte				udtNombre NULL
		,prmr_aplldo_mdco_trtnte				udtApellido NULL
		,sgndo_aplldo_mdco_trtnte				udtApellido NULL
		,cdgo_tpo_aflcn_mdco_trtnte				udtCodigo default ''
		,cnsctvo_cdgo_ntfccn					udtConsecutivo NULL
		,cnsctvo_cdgo_ofcna						udtConsecutivo NULL
		,cnsctvo_cdgo_ctc						udtConsecutivo NULL
	)

	CREATE TABLE #Tempo_Diagnostico 
	( 
		id_fila						int identity
		,id							Int NOT NULL
		,nmro_prstcn				INT	default 0	
		,cnsctvo_cdgo_tpo_dgnstco	udtConsecutivo NULL default 0
		,cnsctvo_cdgo_dgnstco		udtConsecutivo NULL
		,cnsctvo_cdgo_cntngnca		udtConsecutivo NULL
		,cnsctvo_cdgo_rcbro			udtConsecutivo NULL default 9
		,usro_crcn					udtUsuario NULL
		,cdgo_dgnstco				udtCodigoDiagnostico NULL
		,cdgo_tpo_dgnstco			char(3) NULL
		,cdgo_cntngnca				Char(3) NULL
		,cdgo_rcbro					char(3) NULL
	)

	CREATE TABLE #Tempo_Prestaciones 
	(	
		id_fila									INT IDENTITY(1,1)
		,id										Int NOT NULL
		,nmro_prstcn							INT default 0
		,cnsctvo_cdgo_srvco_slctdo				udtConsecutivo NULL
		,cnsctvo_cdgo_tpo_srvco					udtConsecutivo NULL
		,dscrpcn_srvco_slctdo					udtDescripcion NULL 
		,cntdd_slctda							int NULL
		,fcha_prstcn_srvco_slctdo				datetime NULL
		,tmpo_trtmnto_slctdo					int NULL
		,cnsctvo_cdgo_undd_tmpo_trtmnto_slctdo	udtConsecutivo NULL default 0
		,vlr_lqdcn_srvco						udtConsecutivo NULL default 0
		,cnsctvo_srvco_slctdo_orgn				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_tpo_atrzcn				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_prstcn_prstdr				udtConsecutivo NULL default 0
		,cnfrmcn_cldd							udtLogico NULL
		,usro_crcn								udtUsuario NULL
		,cdgo_tpo_srvco							udtCodigo NULL
		,cdgo_srvco_slctdo						char(15) NULL
		,cdgo_undd_tmpo_trtmnto_slctdo			udtCodigo NULL
		,cdgo_prstcn_prstdr						udtCodigo NULL
		,cdgo_tpo_atrzcn						udtCodigo NULL								
		,dss									float NULL
		,cnsctvo_cdgo_prsntcn_dss				udtConsecutivo NULL default 0
		,prdcdd_dss								float NULL
		,cnsctvo_cdgo_undd_prdcdd_dss			udtConsecutivo NULL default 0
		,cnsctvo_cdgo_frma_frmctca				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_grpo_trptco				udtConsecutivo NULL default 0
		,cnsctvo_cdgo_prsntcn					udtConsecutivo NULL default 0
		,cnsctvo_cdgo_va_admnstrcn_mdcmnto		udtConsecutivo NULL default 0
		,cnsctvo_mdcmnto_slctdo_orgn			udtConsecutivo NULL default 0
		,cncntrcn_dss							char(20) NULL
		,prsntcn_dss							char(20) NULL
		,cnsctvo_cdgo_undd_cncntrcn_dss			udtConsecutivo NULL
		,cdgo_prsntcn_dss						udtCodigo NULL
		,cdgo_undd_prdcdd_dss					udtCodigo NULL
		,cdgo_frma_frmctca						udtCodigo NULL
		,cdgo_grpo_trptco						char(3) NULL
		,cdgo_prsntcn							udtCodigo NULL
		,cdgo_va_admnstrcn_mdcmnto				udtCodigo NULL
		,cdgo_undd_cncntrcn_dss					char(10) NULL									
		,cnsctvo_cdgo_ltrldd					udtConsecutivo NULL
		,cnsctvo_cdgo_va_accso					udtConsecutivo NULL
		,cdgo_ltrldd							udtCodigo NULL
		,cdgo_va_accso							udtcodigo NULL
		,cnsctvo_cdgo_dt_prgrmcn_fcha_evnto		udtConsecutivo NULL
		,prgrmcn_entrga							udtLogico default 'S'
	)

	SET @codigoExito = '0'
	SET @codigoError = '-1'
	SET @codigoSalir = '-2'
		
	SET @tpo_prcso = 37
		
	SET @codigoExitoso = 2
	SET @cnsctvo_estdo_err = 3
	SET @cnsctvo_cdgo_tpo_incnsstnca = 1
		
	SET @rgstro_log_poblar = 158
	SET @rgstro_log_validar = 159
	SET @rgstro_lg_grdr_slctd = 160
	SET @rgstro_lg_grdr_afldo = 161
	SET @rgstro_lg_grdr_ips	= 162
	SET @rgstro_lg_grdr_mdco = 163
	SET @rgstro_lg_grdr_dgnstco = 164
	SET @rgstro_lg_grdr_prstcns	= 165
	SET @rgstro_lg_grdr_estdos = 166
	SET @rgstro_lg_grdr_fcha_entrga = 167
	SET @rgstro_lg_grdr_drrcnmnto = 168
	SET @rgstro_lg_grdr_nmro_slctd = 169
	SET @rgstro_lg_grdr_dts_mgrcn = 170

	SET @throw_error_number = 50000
	SET @throw_error_state = 0
	SET @fechaActual = getdate()
	SET @NO = 'N'
	SET @uno = 1

	SET @mnsge_pblr = 'Paso 1: Poblar tablas temporales para migracion de 3047 a MEGA'
	SET @mensajeError = 'Se Presento Un Error al Crear la(s) Solicitud(es) de 3047'
	SET @mnsge_vldr = 'Paso 2: Validacion Solicitud 3047 en MEGA'
	SET @mnsge_grddo_slctd = 'Paso 3: Guardar Solicitud 3047 en MEGA'
	SET @mnsge_grddo_afldo = 'Paso 4. Grabar Solicitud Afiliados 3047 en MEGA'
	SET @mnsge_grddo_ips = 'Paso 5: Grabar Solicitud Prestador 3047 en MEGA'
	SET @mnsge_grddo_mdco = 'Paso 6: Grabar Solicitud M�dico 3047 en MEGA'
	SET @mnsge_grddo_dgnstco = 'Paso 7: Grabar Solicitud Diagn�stico 3047 en MEGA'
	SET @mnsge_grddo_prstcns = 'Paso 8: Grabar Solicitud Prestaciones 3047 en MEGA'
	SET @mnsge_grddo_estdos = 'Paso 9: Actualizar estados MEGA de acuerdo a 3047'
	SET @mnsge_grddo_fcha_entrga = 'Paso 10: Actualizar fecha de entrega de acuerdo a 3047'
	SET @mnsge_grddo_drrcnmnto = 'Paso 11: Ejecutar direccionamiento prestador'
	SET @mnsge_grddo_nmro_slctd = 'Paso 12: Generar numero de radicado solicitud'
	SET @mnsge_grddo_dts_mgrcn = 'Paso 13: Llenar tabla datos migracion mega-3047'
	SET @codigoErrorDireccionamigo = 'ET'
	SET @usro_ejccn_drccnmnto = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

	EXEC bdProcesosSalud.dbo.spPRORegistraProceso @tpo_prcso, @usro_ejccn_drccnmnto, @cnsctvo_prcso output
	
	BEGIN TRANSACTION migracion_3047
	BEGIN TRY --Proceso general.
		BEGIN TRY --Paso 1: Proceso de poblar tablas temporales.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_pblr,@rgstro_log_poblar,@cnsctvo_lg output
			EXEC gsa.spASLlenarTablasTemporalesMigracion3047 @cnsctvo_slctd_ops
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso1 = @codigoExitoso;
		END TRY
		BEGIN CATCH
			SET @estdo_pso1 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 2: Proceso de validacion de de existencia de prestaciones en MEGA.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_vldr,@rgstro_log_validar,@cnsctvo_lg output
			--TODO: Llamar proceso almacenado que valida si las solicitudes ya existen en mega.
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso2 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso2 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 3: Proceso de guardado en tabla de mega de solicitudes.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_slctd,@rgstro_lg_grdr_slctd,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudSolicitudes 
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso3 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso3 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 4: Proceso de guardado en tabla de mega de afiiados.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_afldo,@rgstro_lg_grdr_afldo,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudAfiliados
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso4 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso4 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 5: Proceso de guardado en tabla de mega de ips.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_ips,@rgstro_lg_grdr_ips,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudIPS 
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso5 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso5 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 6: Proceso de guardado en tabla de mega de medico.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_mdco,@rgstro_lg_grdr_mdco,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudMedico 
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso6 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso6 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 7:Proceso de guardado en tabla de mega de diagnostico.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_dgnstco,@rgstro_lg_grdr_dgnstco,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudDiagnostico 
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso7 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso7 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 8: Proceso de guardado en tabla de mega de prestaciones.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_prstcns,@rgstro_lg_grdr_prstcns,@cnsctvo_lg output
			EXEC gsa.spASGrabarSolicitudPrestaciones 
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso8 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso8 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 10: Proceso de calculo de actualizacion de fecha de entrega.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_fcha_entrga,@rgstro_lg_grdr_fcha_entrga,@cnsctvo_lg output
			EXEC gsa.spASActualizarFechaEntrega3047
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso10 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso10 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 11: Proceso de calculo de direccionamiento
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_drrcnmnto,@rgstro_lg_grdr_drrcnmnto,@cnsctvo_lg output
			EXEC gsa.spASDireccionarMigracion3047 @usro_ejccn_drccnmnto, @estdo_ejccn OUTPUT, @mensaje_error_direccionamiento OUTPUT, @msje_rspsta OUTPUT
			IF @estdo_ejccn = @codigoErrorDireccionamigo
			BEGIN
				SET @estdo_pso11 = @cnsctvo_estdo_err;
				THROW @throw_error_number,@mensaje_error_direccionamiento,@throw_error_state
			END
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso11 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso11 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		BEGIN TRY --Paso 12: Proceso de calculo de numero de solicitud.
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_nmro_slctd,@rgstro_lg_grdr_nmro_slctd,@cnsctvo_lg output
			EXEC gsa.spASCalcularNumeroSolicitud3047
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso12 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso12 = @cnsctvo_estdo_err;
			THROW
		END CATCH

		BEGIN TRY --Paso 9: Proceso de calculo de estados de la prestacion
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_estdos,@rgstro_lg_grdr_estdos,@cnsctvo_lg output
			EXEC gsa.spASActualizaEstados3047
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso9 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso9 = @cnsctvo_estdo_err;
			THROW
		END CATCH

		BEGIN TRY --Paso 13: Proceso de guardado de datos de migracion
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_dts_mgrcn,@rgstro_lg_grdr_dts_mgrcn,@cnsctvo_lg output
			EXEC gsa.spASGuardarDatosMigradosMega3047
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProcesoExitoso @cnsctvo_lg
			SET @estdo_pso13 = @codigoExitoso
		END TRY
		BEGIN CATCH
			SET @estdo_pso13 = @cnsctvo_estdo_err;
			THROW
		END CATCH
		
		EXEC bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@codigoExitoso
		COMMIT TRANSACTION migracion_3047
	END TRY
	BEGIN CATCH
		SET @mensajeError = concat(@mensajeError, ERROR_PROCEDURE(), ' ', ERROR_MESSAGE() ,  ' Linea: ', cast(ERROR_LINE() as varchar(5))) ;
		SET @cdgo_rsltdo = @codigoError
		SET @mnsje_rsltdo = @mensajeError

		ROLLBACK TRANSACTION migracion_3047
		
		EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_pblr,@rgstro_log_poblar,@cnsctvo_lg output
		EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso1
		IF @estdo_pso1 = @cnsctvo_estdo_err
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
		END
			
		IF @estdo_pso2 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_vldr,@rgstro_log_validar,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso2
			IF @estdo_pso2 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		IF @estdo_pso3 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_slctd,@rgstro_lg_grdr_slctd,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso3
			IF @estdo_pso3 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		IF @estdo_pso4 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_afldo,@rgstro_lg_grdr_afldo,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso4
			IF @estdo_pso4 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso5 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_ips,@rgstro_lg_grdr_ips,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso5
			IF @estdo_pso5 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso6 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_mdco,@rgstro_lg_grdr_mdco,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso6
			IF @estdo_pso6 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso7 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_dgnstco,@rgstro_lg_grdr_dgnstco,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso7
			IF @estdo_pso7 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso8 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_prstcns,@rgstro_lg_grdr_prstcns,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso8
			IF @estdo_pso8 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso9 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_estdos,@rgstro_lg_grdr_estdos,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso9
			IF @estdo_pso9 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso10 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_fcha_entrga,@rgstro_lg_grdr_fcha_entrga,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso10
			IF @estdo_pso10 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso11 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_drrcnmnto,@rgstro_lg_grdr_drrcnmnto,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso11
			IF @estdo_pso11 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		IF @estdo_pso12 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_nmro_slctd,@rgstro_lg_grdr_nmro_slctd,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso12
			IF @estdo_pso12 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END

		IF @estdo_pso13 IS NOT NULL
		BEGIN
			EXEC bdProcesosSalud.dbo.spPRORegistraLogEventoProceso @cnsctvo_prcso, @tpo_prcso,@mnsge_grddo_dts_mgrcn,@rgstro_lg_grdr_dts_mgrcn,@cnsctvo_lg output
			EXEC bdProcesosSalud.dbo.spPROActualizaEstadoLogEventoProceso @cnsctvo_lg,@estdo_pso13
			IF @estdo_pso13 = @cnsctvo_estdo_err
			BEGIN
				EXEC bdProcesosSalud.dbo.spPRORegistrarInconsistenciaProceso @cnsctvo_lg,@cnsctvo_cdgo_tpo_incnsstnca,@mensajeError
			END
		END
		
		EXEC bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@tpo_prcso,@cnsctvo_estdo_err
	END CATCH

	DROP TABLE #Idsolicitudes		
	DROP TABLE #IdAfiliados	
	DROP TABLE #IdIPS
	DROP TABLE #IdHospitalaria
	DROP TABLE #IdMedico								
	DROP TABLE #IdDiagnostico						
	DROP TABLE #IdServicios							
	DROP TABLE #IdServiciosOr		
	DROP TABLE #IdMedicamentos								
	DROP TABLE #IdInsumos
	DROP TABLE #Tempo_Solicitudes
	DROP TABLE #Tempo_Afiliados
	DROP TABLE #Tempo_IPS
	DROP TABLE #Tempo_Medicos
	DROP TABLE #Tempo_Diagnostico
	DROP TABLE #Tempo_Prestaciones
	DROP TABLE #solicitudes3047
	
END
GO
