USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarIdentificadorDocumentoAnexo]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarIdentificadorDocumentoAnexo
* Desarrollado por   : <\A Jonathan Chamizo    A\>    
* Descripcion        : <\D Actualización del identificador del documento anexo de la solicitud obtenido desde
						   VisosService  D\>    
* Observaciones      : <\O  O\>    
* Parametros         : <\P	P\>    
* Variables          : <\V  V\>    
* Fecha Creacion  	 : <\FC 23/02/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\D  D\>  
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM  FM\>   
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASGrabarIdentificadorDocumentoAnexo] 
@cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
@cnsctvo_cdgo_dcmnto udtConsecutivo,
@usro udtUsuario,
@estdo_ejccn varchar(50) OUTPUT
AS
BEGIN
	SET NOCOUNT ON
  
  	DECLARE @Fecha_Actual datetime,
	        @codigoExito varchar(20),
			@codigoNoEncontrado varchar (40),
			@resultado udtConsecutivo;

	SET @Fecha_Actual = Getdate();
	SET @codigoExito = 'Actualizado';
	SET @codigoNoEncontrado = 'ConsecutivoSolicitudNoEncontradoEnAnexo'

	BEGIN

		-- Se consulta si existen anexos con base en el consecutivo de la solicitud
		SELECT @resultado = DAS.cnsctvo_slctd_atrzcn_srvco  FROM BDCna.gsa.tbASDocumentosAnexosServicios DAS WITH (NOLOCK)
		WHERE DAS.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco  

		IF @resultado <> 0
			BEGIN
				UPDATE BDCna.gsa.tbASDocumentosAnexosServicios 
				SET cnsctvo_cdgo_dcmnto = @cnsctvo_cdgo_dcmnto,
					fcha_ultma_mdfccn = @Fecha_Actual,
					usro_ultma_mdfccn = @usro                        
				WHERE cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco;
				SET @estdo_ejccn = @codigoExito;
			END
		ELSE
			BEGIN
				SET @estdo_ejccn = @codigoNoEncontrado;
			END		
	END
END


GO
