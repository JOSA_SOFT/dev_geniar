USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarPrevioProgramacionEntregaMasivo]    Script Date: 05/05/2017 11:53:04 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*--------------------------------------------------------------------------------------
* Método o PRG		:		gsa.spASEjecutarPrevioProgramaciOnEntregaMasivo							
* Desarrollado por	: <\A	Ing. Carlos Andres Lopez Ramirez - qvisiOnclr A\>	
* Descripción		: <\D	Procedimiento que guarda un log de las programaciOnes de entrega 
						    que no liquidaran por parametros	D\>
* ObservaciOnes		: <\O 	O\>	
* Parámetros		: <\P 	P\>	
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/04/18	FC\>
*-----------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*---------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\DM 
                            Se agrega entrada de fechas por parametro de forma temporal.
						DM\> 
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/05/05 FM\>    
*---------------------------------------------------------------------------------------------------------------------------------*/

/**
	Exec [gsa].[spASEjecutarPrevioProgramacionEntregaMasivo] '20170401', '20170501'

	Exec [gsa].[spASEjecutarPrevioProgramacionEntregaMasivo] Null, Null
*/

ALTER Procedure [gsa].[spASEjecutarPrevioProgramacionEntregaMasivo]
		@fcha_inco				Date,
		@fcha_fn				Date
As
Begin

	Set NoCount On

	Declare		@cnsctvo_cdgo_clsfccn_evnto11			udtCOnsecutivo,
				@sn_entrgr								Int,
				@rprgrmdo								Int,
				@fcha_crte_inco							Datetime,
				@fechaactual							Datetime,
				@vlr_prmtro_nmrco						Numeric(18,0),
				@lcCdgo_prmtro_cntdd_mss_frmto			Char(3),
				@vsble_usro								udtLogico,
				@tpo_dto_prmtro							udtLogico,
				@vlr_prmtro_crctr						Char(200),
				@vlr_prmtro_fcha						Datetime,
				@fechacrtelqdccn						Datetime,
				@estdo									Char(1),
				@nui_cmfndi								Int,
				@tpo_ips_drgra							Int,
				@tpo_cntrto_PAC							Int,
				@ValorSI								udtlogico,
				@valorNo								udtlogico,
				@valorUNO								Int,
				@cnsctvo_cdgo_tps_mrcs_evnts_ntfccn1	udtCOnsecutivo,
				@cnsctvo_cdgo_estdo_acta56				udtCOnsecutivo,
				@cnsctvo_cdgo_tpo_mrca_prstdr14			udtCOnsecutivo,
				@cnsctvo_cdgo_tpo_mrca_prstdr21			udtCOnsecutivo,
				@cnsctvo_cdgo_clsfccn_atncn1			udtCOnsecutivo,
				@ValorCERO								Int,
				@cnsctvo_cdgo_tpo_cntrto1				udtConsecutivo,
				@cnsctvo_cdgo_tpo_cntrto3				udtConsecutivo;


	Create Table #tbDetProgramaciOn		  
	(
		id_tbla								int identity,
		cnsctvo_prgrmcn_fcha_evnto			UdtCOnsecutivo,
		cnsctvo_prgrmcn_prstcn				UdtCOnsecutivo,
		cnsctvo_det_prgrmcn_fcha			UdtCOnsecutivo,
		cnsctvo_cdgo_ntfccn					UdtCOnsecutivo,
		cnsctvo_cdgo_ofcna					UdtCOnsecutivo,
		cnsctvo_pr_ntfccn					Varchar(50),
		cnsctvo_prstcn						UdtCOnsecutivo,
		cnsctvo_cdgo_clsfccn_cdfccn			UdtCOnsecutivo,
		fcha_dsde							datetime,
		fcha_hsta							datetime,
		fcha_entrga							datetime,
		fcha_rl_entrga						datetime,
		cnsctvo_cdgo_estds_entrga			UdtCOnsecutivo,
		cdgo_intrno							UdtCodigoIps,
		nmro_unco_idntfccn_prstdr			UdtCOnsecutivo default 0,
		cntdd								int,
		cnsctvo_cdgo_dgnstco				UdtCOnsecutivo,
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto	udtCOnsecutivo,
		mrca_lsta_chk						UdtLogico default 'N',
		nmro_acta							char(50),
		cnsctvo_cdgo_clsfccn_evnto          UdtCOnsecutivo,
		nmro_unco_idntfccn_afldo			UdtCOnsecutivo,
		cnsctvo_cdgo_tpo_cntrto				udtCOnsecutivo,
		nmro_cntrto							char(15),
		cnsctvo_bnfcro						UdtCOnsecutivo,
		cnsctvo_cdgo_pln					UdtCOnsecutivo,
		mrca_ops_vrtl						UdtLogico default 'N',
		mrca_cnvno							UdtLogico default 'N',
		mrca_lqdcn							UdtLogico default 'N',
	) 

	Create Table #tempTbPrevioProgramaciOn
	(
		id									Int Identity,
		fcha_crcn							DateTime,
		dscpcn_csa_incdnca					udtDescripciOn,
		cnsctvo_cdgo_det_prgrmcn_fcha_evnto	udtCOnsecutivo
	)

	Create Table #tmpProgramaciOnesVarias
	(
		nmro_unco_idntfccn_afldo			UdtCOnsecutivo
	)

	

	Set @cnsctvo_cdgo_clsfccn_evnto11			= 11;
	Set @sn_entrgr								= 151;
	Set @rprgrmdo								= 152;
	Set @fechaactual							= GETDATE();
	Set @fcha_crte_inco							= DATEADD(dd,DATEDIFF(dd,0,@fechaactual),0);
	Set @lcCdgo_prmtro_cntdd_mss_frmto			= '88';
	Set @vsble_usro								= 'S';
	Set @tpo_dto_prmtro							= '';
	Set @fechacrtelqdccn						= GETDATE();
	Set @estdo									= 'A';
	Set @nui_cmfndi								= 100419;
	Set @tpo_ips_drgra							= 8;
	Set @tpo_cntrto_PAC							= 2;
	Set @ValorSI								= 'S';
	Set @ValorNo								= 'N';
	Set @valorUNO								= 1;
	Set @cnsctvo_cdgo_tps_mrcs_evnts_ntfccn1	= 1;
	Set @cnsctvo_cdgo_estdo_acta56				= 56;
	Set @cnsctvo_cdgo_tpo_mrca_prstdr14			= 14;
	Set @cnsctvo_cdgo_tpo_mrca_prstdr21			= 21;
	Set @cnsctvo_cdgo_clsfccn_atncn1			= 1;
	Set @ValorCERO								= 0;
	Set @cnsctvo_cdgo_tpo_cntrto1				= 1;
	Set @cnsctvo_cdgo_tpo_cntrto3				= 3;
	

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output

	Set	@fcha_crte_inco		-=	@vlr_prmtro_nmrco
	Set @lcCdgo_prmtro_cntdd_mss_frmto  = '110'

	Exec bdSiSalud.dbo.spTraerParametroGeneral @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @lcCdgo_prmtro_cntdd_mss_frmto, @vsble_usro, @tpo_dto_prmtro, @vlr_prmtro_nmrco Output, @vlr_prmtro_crctr Output, @vlr_prmtro_fcha Output

	Set	@fechacrtelqdccn = bdsisalud.dbo.fnPeCalculaDiasHabiles(@fechacrtelqdccn,@vlr_prmtro_nmrco)
	Set	@fechacrtelqdccn = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,@fechacrtelqdccn),1))

	if((@fcha_inco Is Not Null) And (@fcha_fn Is Not Null))
	Begin
		Set	@fcha_crte_inco		= @fcha_inco;
		Set	@fechacrtelqdccn	= @fcha_fn;
	End

	Insert Into	#tbDetProgramaciOn 
	(              
				cnsctvo_prgrmcn_prstcn,			cnsctvo_cdgo_ntfccn,					cnsctvo_cdgo_ofcna,
				cnsctvo_prstcn,					cnsctvo_cdgo_clsfccn_cdfccn,			fcha_dsde,
				fcha_hsta,						cnsctvo_cdgo_estds_entrga,				cdgo_intrno,
				nmro_unco_idntfccn_prstdr,		cntdd,									cnsctvo_cdgo_dgnstco,
				cnsctvo_pr_ntfccn,				cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		cnsctvo_prgrmcn_fcha_evnto,
				cnsctvo_det_prgrmcn_fcha,		fcha_entrga,							fcha_rl_entrga,
				nmro_acta,						cnsctvo_cdgo_clsfccn_evnto,				nmro_unco_idntfccn_afldo
	)
	Select Distinct
				a.cnsctvo_prgrmcn_prstcn,		a.cnsctvo_cdgo_ntfccn,					a.cnsctvo_cdgo_ofcna,
				a.cnsctvo_prstcn,				a.cnsctvo_cdgo_clsfccn_cdfccn,			b.fcha_dsde,
				b.fcha_hsta,					b.cnsctvo_cdgo_estds_entrga,			e.cdgo_intrno,
				isnull(e.nmro_unco_idntfccn_prstdr,0),	c.cntdd,						f.cnsctvo_cdgo_dgnstco,
				a.cnsctvo_pr_ntfccn,			c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto,	b.cnsctvo_prgrmcn_fcha_evnto,
				c.cnsctvo_det_prgrmcn_fcha,		isnull(c.fcha_entrga,''),				c.fcha_rl_entrga,
				a.cnsctvo_pr_ntfccn,			f.cnsctvo_cdgo_clsfccn_evnto,			f.nmro_unco_idntfccn
	From		BdSiSalud.dbo.tbProgramaciOnPrestaciOn a With(Nolock)	
	Inner Join	bdsisalud.dbo.tbProgramaciOnFechaEvento b	With(Nolock)	
	On			a.cnsctvo_prgrmcn_prstcn = b.cnsctvo_prgrmcn_prstcn 
	Inner Join	BdSiSalud.dbo.tbDetProgramaciOnFechaEvento c	With(Nolock)	
	On			c.cnsctvo_prgrmcn_prstcn = a.cnsctvo_prgrmcn_prstcn  
	And			c.cnsctvo_prgrmcn_fcha_evnto = b.cnsctvo_prgrmcn_fcha_evnto  		
	Inner Join	BdSiSalud.dbo.tbCodificaciones d With(Nolock)	
	On			d.cnsctvo_cdfccn = a.cnsctvo_prstcn
	Inner Join	BdSiSalud.dbo.tbDetProgramaciOnProveedores e	With(Nolock)
	On			c.cnsctvo_prgrmcn_prstcn = e.cnsctvo_prgrmcn_prstcn 
	And			c.cnsctvo_det_prgrmcn_fcha = e.cnsctvo_det_prgrmcn_fcha
	And			c.cnsctvo_prgrmcn_fcha_evnto = e.cnsctvo_prgrmcn_fcha_evnto 	
	Inner Join	BdSiSalud.dbo.tbAfiliadosMarcados f with(nolock)
	On			f.cnsctvo_ntfccn = a.cnsctvo_cdgo_ntfccn
	And			f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna	
	Left Join   bdcna.dbo.TmpPrgmentrgaCnvcnl t1
	On			t1.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = c.cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	Where       c.cnsctvo_cdgo_estds_entrga In (@sn_entrgr, @rprgrmdo)
	And			c.fcha_entrga Between @fcha_crte_inco  And @fechacrtelqdccn 
	And			e.estdo = @estdo
	And			f.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11
	And			t1.cnsctvo_cdgo_det_prgrmcn_fcha_evnto is null
	Order By    isnull(c.fcha_entrga,'');

	-- sisjvg01 - 20170419 - Quitar las programaciOnes de afiliados cOn más de una programación
	
	Insert Into #tmpProgramaciOnesVarias
	(			nmro_unco_idntfccn_afldo )
	Select		nmro_unco_idntfccn_afldo 
	From		#tbDetProgramaciOn
	Group by	nmro_unco_idntfccn_afldo
	Having		count(nmro_unco_idntfccn_afldo) > @ValorUNO;
	
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select		t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Afiliado con  mas de una entrega en el mismo periodo'
	From		#tbDetProgramaciOn t
	Inner Join	#tmpProgramaciOnesVarias t1
	On			t1.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo;

	Delete		t
	From		#tbDetProgramaciOn t
	Inner Join	#tmpProgramaciOnesVarias t1
	On			t1.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo;
	
	-- sismpr01 - 20170412 - Quitar las programaciOnes de entrega que sean de comfAndi y no sean droguerias
	-- 
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select		tdp.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Programacion para IPS de comfandi diferentes a droguerias'
	From		#tbDetProgramaciOn tdp
	Inner Join	bdsisalud.dbo.tbdirecciOnesprestador  dp With (NoLock)
   	On			tdp.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr 
	And			tdp.cdgo_intrno =  dp.cdgo_intrno
	Where		tdp.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi	
	And			dp.cnsctvo_cdgo_tpo_ips			!=	@tpo_ips_drgra;

	Delete		tdp
	From		#tbDetProgramaciOn tdp
	Inner Join	bdsisalud.dbo.tbdirecciOnesprestador  dp With (NoLock)
   	On			tdp.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr 
	And			tdp.cdgo_intrno =  dp.cdgo_intrno
	Where		tdp.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi	
	And			dp.cnsctvo_cdgo_tpo_ips			!=	@tpo_ips_drgra;

	-- sismpr01 - 20170417 - Quitar las programaciOnes de entrega de afiliados que tengan planes complementarios vigentes
	-- 
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	   t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Programaciones para planes complementarios solo Comfandi'
	From       #tbDetProgramaciOn t
	Inner Join BDAfiliaciOnValidador.dbo.tbBeneficiariosValidador bv With (NoLock)
	On         bv.nmro_unco_idntfccn_afldo	= t.nmro_unco_idntfccn_afldo			
	Where      @fechaactual  Between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro
	And		   bv.cnsctvo_cdgo_tpo_cntrto	=	@tpo_cntrto_PAC -- PAC
	And		   t.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi	

	Delete	   t
	From       #tbDetProgramaciOn t
	Inner Join BDAfiliaciOnValidador.dbo.tbBeneficiariosValidador bv With (NoLock)
	On         bv.nmro_unco_idntfccn_afldo	= t.nmro_unco_idntfccn_afldo			
	Where      @fechaactual  Between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro
	And		   bv.cnsctvo_cdgo_tpo_cntrto	=	@tpo_cntrto_PAC -- PAC		
	And		   t.nmro_unco_idntfccn_prstdr	=	@nui_cmfndi	

	-- Recuperamos el tipo, número de cOntrato y cOnsecutivo del afiliado
	-- sismpr01 - 20170417 - se recupera el cOntrato pos y pos subsidiado
	Update     t
	Set        cnsctvo_cdgo_tpo_cntrto	= bv.cnsctvo_cdgo_tpo_cntrto,
			  	nmro_cntrto				= bv.nmro_cntrto,
				cnsctvo_bnfcro			= bv.cnsctvo_bnfcro
	From		#tbDetProgramacion t
	Inner Join	BDAfiliacionValidador.dbo.tbBeneficiariosValidador bv With (NoLock)
	On			bv.nmro_unco_idntfccn_afldo = t.nmro_unco_idntfccn_afldo
	Where		@fechaactual between bv.inco_vgnca_bnfcro And bv.fn_vgnca_bnfcro
	And			bv.cnsctvo_cdgo_tpo_cntrto	in (@cnsctvo_cdgo_tpo_cntrto1,@cnsctvo_cdgo_tpo_cntrto3); --1 POS, 3 POSS

	--Se Eliminan las programaciOnes de entrega para afiliados no vigentes
	Insert Into #tempTbPrevioProgramacion
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	    t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Afiliado no vigente en SOS'
	From		#tbDetProgramacion t
	Where		t.cnsctvo_cdgo_tpo_cntrto	is Null;

	Delete		t
	From		#tbDetProgramaciOn t
	Where		t.cnsctvo_cdgo_tpo_cntrto	is Null;

	Insert Into #tempTbPrevioProgramacion
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	    t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Afiliado no vigente en SOS'
	From		#tbDetProgramaciOn t
	Where		t.nmro_cntrto				is Null;

	Delete		t
	From		#tbDetProgramaciOn t
	Where		t.nmro_cntrto				is Null;

	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	    t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Afiliado no vigente en SOS'
	From		#tbDetProgramaciOn t
	Where		t.cnsctvo_bnfcro			is Null;

	Delete		t
	From		#tbDetProgramaciOn t
	Where		t.cnsctvo_bnfcro			is Null;

	-- sisjvg01- 2017-04-12 - Eliminar las programaciOnes de entrega para afiliados Sin Derechos
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	   t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Afiliado sin derechos a servicio'
	From       #tbDetProgramaciOn t
	Inner Join BDAfiliaciOnValidador.dbo.TbMatrizDerechosValidador bv With (NoLock)
	On         bv.nmro_unco_idntfccn_bnfcro	=			t.nmro_unco_idntfccn_afldo
	And        bv.cnsctvo_cdgo_tpo_cntrto	=			t.cnsctvo_cdgo_tpo_cntrto
	And        bv.nmro_cntrto				=			t.nmro_cntrto
	And		   bv.cnsctvo_bnfcro			=			t.cnsctvo_bnfcro
	Where      @fechaactual					Between		bv.inco_vgnca_estdo_drcho And bv.fn_vgnca_estdo_drcho
	And		   bv.cnsctvo_cdgo_estdo_drcho	In			(	Select	cnsctvo_cdgo_estdo_drcho
															From	BDAfiliaciOnValidador.dbo.tbestadosderechovalidador  With (NoLock)
															Where	drcho  = @ValorNo
															And		@fechaactual Between inco_vgnca	And fn_vgnca
														);

	Delete	   t
	From       #tbDetProgramaciOn t
	Inner Join BDAfiliaciOnValidador.dbo.TbMatrizDerechosValidador bv With (NoLock)
	On         bv.nmro_unco_idntfccn_bnfcro	=			t.nmro_unco_idntfccn_afldo
	And        bv.cnsctvo_cdgo_tpo_cntrto	=			t.cnsctvo_cdgo_tpo_cntrto
	And        bv.nmro_cntrto				=			t.nmro_cntrto
	And		   bv.cnsctvo_bnfcro			=			t.cnsctvo_bnfcro
	Where      @fechaactual					Between		bv.inco_vgnca_estdo_drcho And bv.fn_vgnca_estdo_drcho
	And		   bv.cnsctvo_cdgo_estdo_drcho	In			(	Select	cnsctvo_cdgo_estdo_drcho
															From	BDAfiliaciOnValidador.dbo.tbestadosderechovalidador  With (NoLock)
															Where	drcho  = @ValorNo 
															And		@fechaactual Between inco_vgnca	And fn_vgnca
														);

	-- Recuperamos el plan de los afiliados de la programación de entrega
	Update		t
	Set			cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln
	From        #tbDetProgramaciOn t
	Inner Join	bdSisalud.dbo.tbactuanotificaciOn a With (NoLock)
	On			a.cnsctvo_ntfccn		= t.cnsctvo_cdgo_ntfccn
	And			a.cnsctvo_cdgo_ofcna	= t.cnsctvo_cdgo_ofcna;
			
	-- marcamos las programaciOnes que apliquen para ops Virtual
	Update      t
	Set         mrca_ops_vrtl = @ValorSI
	From        #tbDetProgramaciOn t
	Inner Join  BDSisalud.dbo.tbTiposMarcasxEventosNotificaciOn tmen with(nolock)
	On          tmen.cnsctvo_cdgo_clsfccn_evnto = t.cnsctvo_cdgo_clsfccn_evnto
	Inner Join  BDSisalud.dbo.tbTiposMarcasXSucursal tms with(nolock)
	On          tms.cdgo_intrno = t.cdgo_intrno
	Where		tmen.cnsctvo_cdgo_tps_mrcs_evnts_ntfccn = @cnsctvo_cdgo_tps_mrcs_evnts_ntfccn1
	And         tms.cnsctvo_cdgo_tpo_mrca_prstdr In(@cnsctvo_cdgo_tpo_mrca_prstdr14,@cnsctvo_cdgo_tpo_mrca_prstdr21)
	And         @fechaactual between tmen.inco_vgnca And tmen.fn_vgnca         
	And         @fechaactual between tms.inco_vgnca And tms.fn_vgnca;
			
	---- eliminamos las programaciOnes que no aplican para ops virtual
	--Insert Into #tempTbPrevioProgramaciOn
	--(
	--			cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	--)
	--Select	    t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'programaciones que no aplican para ops virtual'
	--From		#tbDetProgramaciOn t
	--Where		mrca_ops_vrtl = @ValorNo;

	--Delete		t
	--From		#tbDetProgramaciOn t
	--Where		mrca_ops_vrtl = @ValorNo;

	-- eliminamos las programaciOnes que apliquen para ops virtual
	-- pero que tenga la primer programaciOn y la fecha de entrega mayor a la actual
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	    t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'programaciones que apliquen para ops virtual que tenga la primer programacion y la fecha de entrega mayor a la actual'
	From		#tbDetProgramaciOn t
	Where		t.cnsctvo_det_prgrmcn_fcha = @ValorUNO  
	And			t.fcha_entrga > @fechaactual;

	Delete		t
	From		#tbDetProgramaciOn t
	Where		t.cnsctvo_det_prgrmcn_fcha = @ValorUNO  
	And			t.fcha_entrga > @fechaactual;

	-- Eliminamos las programaciOnes cOn acta anulada -- sisjvg01- 2017-04-03
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	   t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Acta del CTC anulado'
	From		#tbDetProgramaciOn t 
	Inner Join	BdSiSalud.dbo.tbCTCRecobro r with(nolock)
	On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
	And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
	And			r.nmro_acta = t.nmro_acta
	Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
	And         r.cnsctvo_cdgo_estdo_acta = @cnsctvo_cdgo_estdo_acta56;

	Delete		t
	From		#tbDetProgramaciOn t 
	Inner Join	BdSiSalud.dbo.tbCTCRecobro r with(nolock)
	On			r.cnsctvo_ntfccn = t.cnsctvo_cdgo_ntfccn
	And			r.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna
	And			r.nmro_acta = t.nmro_acta
	Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
	And         r.cnsctvo_cdgo_estdo_acta = @cnsctvo_cdgo_estdo_acta56;

	-- Marcamos las programaciOnes que tengan cOnvenio vigente y liq. vigentes - sisjvg01- 2017-04-07

	-- Modelo COnvenio Cups
	Update      t
	Set         mrca_cnvno = @ValorSI
	From		#tbDetProgramaciOn t
	Inner Join	bdSisalud.dbo.tbAsociaciOnModeloActividad a With (NoLock)			
	On			a.cdgo_intrno = t.cdgo_intrno
	And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln	
	Inner Join	bdCOntrataciOn.dbo.tbModeloCOnveniosPrestaciOnesxPlan b With(NoLock)
	On			b.cnsctvo_mdlo_cnvno_prstcn_pln = a.cnsctvo_mdlo_cnvno_pln	
	Inner Join	bdCOntrataciOn.dbo.tbDetModeloCOnveniosPrestaciOnes	c	With(NoLock)	
	On			c.cnsctvo_cdgo_mdlo_cnvno_prstcn = b.cnsctvo_cdgo_mdlo_cnvno_prstcn		
	Inner Join	bdSiSalud.dbo.tbDetEquivalencias e	With(NoLock)
	On			e.cnsctvo_det_eqvlnca = c.cnsctvo_det_eqvlnca
	And         e.cnsctvo_cdfccn_b = t.cnsctvo_prstcn 	
	Where		c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
	And         t.fcha_entrga Between e.inco_vgnca And e.fn_vgnca
	And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca
	And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca
	And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca;

			
	-- Modelo COnvenio Medicamentos
	Update      t
	Set         mrca_cnvno = @ValorSI
	From		#tbDetProgramaciOn t
	Inner Join	bdSisalud.dbo.tbAsociaciOnModeloActividad a With (NoLock)			
	On			a.cdgo_intrno = t.cdgo_intrno
	And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln	
	Inner Join	bdCOntrataciOn.dbo.tbModeloCOnveniosMedicamentos b with(nolock)
	On          b.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = a.cnsctvo_mdlo_cnvno_pln	
	Inner Join	bdCOntrataciOn.dbo.tbDetModeloCOnveniosMedicamentos	c with(nolock)
	On          c.cnsctvo_cdgo_mdlo_cnvno_mdcmnto = b.cnsctvo_cdgo_mdlo_cnvno_mdcmnto
    And         c.cnsctvo_cdgo_mdcmnto = t.cnsctvo_prstcn
	Where       c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
	And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca
	And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca
	And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca;

	-- Modelo Cuos 
	Update      t
	Set         mrca_cnvno = @ValorSI
	From		#tbDetProgramaciOn t
	Inner Join	bdSisalud.dbo.tbAsociaciOnModeloActividad a With (NoLock)			
	On			a.cdgo_intrno = t.cdgo_intrno
	And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln	
	Inner Join	bdCOntrataciOn.dbo.tbmodelocOnveniosotrosserviciosxplan b With(NoLock)
	On          b.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs = a.cnsctvo_mdlo_cnvno_pln	
	Inner Join	bdCOntrataciOn.dbo.tbdetmodelocOnveniosotrosservicios	c	With(NoLock)
	On          c.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs = b.cnsctvo_cdgo_mdlo_cnvno_otrs_srvcs
	And			c.cnsctvo_prstcn_pis = t.cnsctvo_prstcn
	Where       c.cnsctvo_cdgo_clsfccn_atncn = @cnsctvo_cdgo_clsfccn_atncn1
	And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca
	And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
	And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca;

	-- Eliminamos las programaciOnes que no tengan cOnvenio vigente
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	   t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Prestaciones sin convenio vigente'
	From		#tbDetProgramaciOn t 
	Where       mrca_cnvno = @ValorNO;

	Delete		t
	From		#tbDetProgramaciOn t 
	Where       mrca_cnvno = @ValorNO;
			
	-- Modelo Liquidación
	Update      t
	Set         mrca_lqdcn = @ValorSI
	From		#tbDetProgramaciOn t
	Inner Join	bdSisalud.dbo.tbAsociaciOnModeloActividad a With (NoLock)			
	On			a.cdgo_intrno = t.cdgo_intrno
	And         a.cnsctvo_cdgo_pln = t.cnsctvo_cdgo_pln    
	Inner Join	bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan b With (NoLock)	
	On			a.cnsctvo_asccn_mdlo_actvdd = b.cnsctvo_asccn_mdlo_actvdd	
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan c With (NoLock)	
	On			b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
	And         c.cnsctvo_prstncn = t.cnsctvo_prstcn
	Where       c.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_clsfccn_atncn1
	And         c.vlr_trfdo > @ValorCERO
	And         t.fcha_entrga Between c.inco_vgnca And c.fn_vgnca	
	And         t.fcha_entrga Between a.inco_vgnca And a.fn_vgnca
	And         t.fcha_entrga Between b.inco_vgnca And b.fn_vgnca;

	-- Eliminamos las programaciOnes que no tengan modelo liquidación
		
	Insert Into #tempTbPrevioProgramaciOn
	(
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto,		fcha_crcn,				dscpcn_csa_incdnca
	)
	Select	   t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,		@fechaactual,			'Prestaciones sin convenio vigente'
	From		#tbDetProgramaciOn t 
	Where       mrca_lqdcn = @ValorNO;

	Delete		t
	From		#tbDetProgramaciOn t 
	Where       mrca_lqdcn = @ValorNO;

	Insert Into bdCNA.gsa.tbASPrevioProgramaciOn
	(
		fcha_crcn,			dscpcn_csa_incdnca,			cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	)
	Select		fcha_crcn,			dscpcn_csa_incdnca,			cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	From		#tempTbPrevioProgramaciOn;

	Drop Table #tmpProgramaciOnesVarias;
	Drop Table #tbDetProgramaciOn;
	Drop Table #tempTbPrevioProgramaciOn;

End