Use BdCna
Go

If(Object_id('gsa.spAsMarcaPrestaciones') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spAsMarcaPrestaciones As Select 1'
End
Go

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsMarcaPrestaciones							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Guara el registro de las marcas de los servicos segun los parametros del prestador D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  1- @lcUsuario:usuario del sistema 
                              P\>  													
* Variables					: <\V  1- @cnsctvo_cdgo_tpo_mrca_prstdr15: 15 - IPS habilitada descarga autorizaciones MEGA
																    (Referencia: bdSisalud.dbo.tbTiposMarcasPrestador)
								   2- @cnsctvo_cdgo_tpo_mrca_prstdr14: IPS recauda copago y cuota moderadora
																	(Referencia: bdSisalud.dbo.tbTiposMarcasPrestador)
								   3- @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11: 11 - Autorizado
																	(Referencia: BdCna.prm.tbASEstadosServiciosSolicitados)
								   4- @vlr_cro: Almacena el valor 0
								   5- @fcha_actl: Almacena la fecha actual, obtenida del sistema
								   6- @cnsctvo_cdgo_tpo_mrca2: 2 - DESCARGA AFILIADO
																   (Referencia: BdCna.prm.tbASTipoMarcaServiciosSolicitados)
								   7- @cnsctvo_cdgo_tpo_mrca5: 5 - DESCARGA IPS
																   (Referencia: BdCna.prm.tbASTipoMarcaServiciosSolicitados)
								   8- @cnsctvo_cdgo_tpo_mrca6: 6 - VENTANILLA
																   (Referencia: BdCna.prm.tbASTipoMarcaServiciosSolicitados)

							  V\>  	
* Metdos o PRG Relacionados	:      1 - BdCna.gsa.spASEjecutarCalculoCuotasRecuperacion
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/11 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM AM\>  													
* Descripcion			: <\DM DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

Alter Procedure gsa.spAsMarcaPrestaciones
			@lcUsuario									UdtUsuario
As
Begin
	Set NoCount On

	Declare @cnsctvo_cdgo_tpo_mrca_prstdr15				UdtConsecutivo, 
			@cnsctvo_cdgo_tpo_mrca_prstdr14				UdtConsecutivo, 
			@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11	UdtConsecutivo,
			@vlr_cro									Int,
			@fcha_actl									Date,
			@cnsctvo_cdgo_tpo_mrca2						UdtConsecutivo,
			@cnsctvo_cdgo_tpo_mrca5						UdtConsecutivo,
			@cnsctvo_cdgo_tpo_mrca6						UdtConsecutivo;

	Set @cnsctvo_cdgo_tpo_mrca_prstdr15 = 15; -- IPS habilitada descarga autorizaciones MEGA
	Set @cnsctvo_cdgo_tpo_mrca_prstdr14 = 14; -- IPS recauda copago y cuota moderadora
	Set @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11 = 11; -- Autorizada
	Set @vlr_cro = 0;
	Set	@fcha_actl = getDate();
	Set @cnsctvo_cdgo_tpo_mrca2 = 2; -- 2 - DESCARGA AFILIADO
	Set @cnsctvo_cdgo_tpo_mrca5 = 5; -- 5 - DESCARGA IPS
	Set @cnsctvo_cdgo_tpo_mrca6 = 6; -- 6 - VENTANILLA

	-- Prestador cobra cuota de recuperacion
	Update		ss
	Set			cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca2
	from		#TempServicioSolicitudes ss With(NoLock)
	Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = ccro.cnsctvo_slctd_atrzcn_srvco
	Inner Join	gsa.tbASConceptosServicioSolicitado		css With(NoLock)
	On			css.nmro_unco_ops = ccro.nmro_unco_ops
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
	On			css.cdgo_intrno_prstdr_atrzdo = dp.cdgo_intrno
	Inner Join	bdSisalud.dbo.tbTiposMarcasXPrestador tmp With(NoLock)
	On			tmp.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr
	Where		@fcha_actl Between tmp.inco_vgnca And tmp.fn_vgnca
	And			@fcha_actl Between dp.inco_vgnca And dp.fn_vgnca
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11
	And			tmp.cnsctvo_cdgo_tpo_mrca_prstdr = @cnsctvo_cdgo_tpo_mrca_prstdr14
	And			ss.nmro_unco_ops != @vlr_cro;


	-- Prestador cobra cuota de recuperacion y prestador descarga autorizacion
	Update		ss
	Set			cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca5
	from		#TempServicioSolicitudes ss With(NoLock)
	Inner Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = ccro.cnsctvo_slctd_atrzcn_srvco
	Inner Join	gsa.tbASConceptosServicioSolicitado		css With(NoLock)
	On			css.nmro_unco_ops = ccro.nmro_unco_ops
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
	On			css.cdgo_intrno_prstdr_atrzdo = dp.cdgo_intrno
	Inner Join	bdSisalud.dbo.tbTiposMarcasXPrestador tmp With(NoLock)
	On			tmp.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr
	Where		@fcha_actl Between tmp.inco_vgnca And tmp.fn_vgnca
	And			@fcha_actl Between dp.inco_vgnca And dp.fn_vgnca
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11
	And			tmp.cnsctvo_cdgo_tpo_mrca_prstdr = @cnsctvo_cdgo_tpo_mrca_prstdr15
	And			ss.cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca2 -- 
	And			ss.nmro_unco_ops != @vlr_cro;

	-- OPS no tiene cobro y prestador descarga autorizacion
	Update		ss
	Set			cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca5
	from		#TempServicioSolicitudes ss With(NoLock)
	Inner Join	gsa.tbASConceptosServicioSolicitado		css With(NoLock)
	On			css.nmro_unco_ops = ss.nmro_unco_ops
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador dp With(NoLock)
	On			css.cdgo_intrno_prstdr_atrzdo = dp.cdgo_intrno
	Inner Join	bdSisalud.dbo.tbTiposMarcasXPrestador tmp With(NoLock)
	On			tmp.nmro_unco_idntfccn_prstdr = dp.nmro_unco_idntfccn_prstdr
	Left Join	gsa.TbAsConsolidadoCuotaRecuperacionxOps ccro With(NoLock)
	On			css.nmro_unco_ops = ccro.nmro_unco_ops
	Where		ccro.nmro_unco_ops Is Null
	And			@fcha_actl Between tmp.inco_vgnca And tmp.fn_vgnca
	And			@fcha_actl Between dp.inco_vgnca And dp.fn_vgnca
	And			css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11
	And			tmp.cnsctvo_cdgo_tpo_mrca_prstdr = @cnsctvo_cdgo_tpo_mrca_prstdr15
	And			ss.nmro_unco_ops != @vlr_cro;

    -- Pone la marca para las prestaciones	que no cumplieron con las condiciones anteriores.
	Update		#TempServicioSolicitudes
	Set			cnsctvo_cdgo_tpo_mrca = @cnsctvo_cdgo_tpo_mrca6
	Where		cnsctvo_cdgo_tpo_mrca Is Null;
		
	-- Guarda el registro de marca
	Merge gsa.tbASMarcasServiciosSolicitados As t
	Using(
		Select		
					cnsctvo_srvco_slctdo,		cnsctvo_cdgo_tpo_mrca,			@fcha_actl fcha_crcn,
					@lcUsuario usro_crcn,		@fcha_actl fcha_ultma_mdfccn,	@lcUsuario usro_ultma_mdfccn
		From		#TempServicioSolicitudes
	) As s
	On (
					t.cnsctvo_srvco_slctdo = s.cnsctvo_srvco_slctdo
	)
	When Not Matched Then
			Insert
			(
					cnsctvo_srvco_slctdo,	cnsctvo_cdgo_tpo_mrca,		fcha_crcn,
					usro_crcn,				fcha_ultma_mdfccn,			usro_ultma_mdfccn
			)
			Values
			(
					s.cnsctvo_srvco_slctdo,		s.cnsctvo_cdgo_tpo_mrca,		s.fcha_crcn,								
					s.usro_crcn,				s.fcha_ultma_mdfccn,			s.usro_ultma_mdfccn
			)
	When Matched Then
			Update
			Set		t.cnsctvo_srvco_slctdo  = s.cnsctvo_srvco_slctdo,			
					t.cnsctvo_cdgo_tpo_mrca = s.cnsctvo_cdgo_tpo_mrca,			
					t.fcha_ultma_mdfccn     = s.fcha_ultma_mdfccn,									
					t.usro_ultma_mdfccn     = s.usro_ultma_mdfccn;

End
