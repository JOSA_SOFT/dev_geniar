USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spASDesasociarProgramacionEntrega]    Script Date: 10/3/2017 11:38:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------ 
* Método o PRG		:		spASDesasociarProgramacionEntrega										 
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez G.												A\>	 
* Descripción		: <\D	Desasociar una  Hospitalizacion a una solicitudOPS						D\>	 
* Observaciones		: <\O																			O\>	 
* Parámetros		: <\P																			P\>	 
* Variables			: <\V																			V\>	 
* Fecha Creación	: <\FC	2016/03/07																FC\> 
*------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACIÓN																		 
*------------------------------------------------------------------------------------------- 
* Modificado Por	: <\AM	Luis Fernando Benavides	AM\> 
* Descripción		: <\DM	1.Cambio estado del detalle de programacion de fecha evento		DM\> 
*					  <\DM	de 155-Anulado a 151-Sin Entregar								DM\> 
*					  <\DM	2.Retiro actualizacion estado de programación fecha entrega		DM\> 
* Nuevos Parámetros	: <\PM	PM\> 
* Nuevas Variables	: <\VM	VM\> 
* Fecha Modificación: <\FM	2016/10/04														FM\>
*-------------------------------------------------------------------------------------------
* DATOS DE MODIFICACIÓN																		 
*------------------------------------------------------------------------------------------- 
* Modificado Por	: <\AM	Ing. Carlos Andres Lopez Ramirez	AM\> 
* Descripción		: <\DM	Se ajusta sp para que funciones con nuevo SP de anulacion DM\> 
* Nuevos Parámetros	: <\PM	PM\> 
* Nuevas Variables	: <\VM	VM\> 
* Fecha Modificación: <\FM	2017/10/04														FM\>
*------------------------------------------------------------------------------------------- */

/*
Declare @cdgo_msg		udtCodigo,
		@msg			UdtDescripcion
exec bdcna.gsa.spASDesasociarProgramacionEntrega 2,null,@cdgo_msg output, @msg output

Select @cdgo_msg,@msg
*/
ALTER procedure [gsa].[spASDesasociarProgramacionEntrega]
As
Begin
	Set nocount On

	Declare @fechaActual							Datetime,
			@lcusuario								UdtUsuario,
			@cnsctvo_cdgo_det_prgrmcn_fcha_evnto	UdtConsecutivo,
			@cnsctvo_cdgo_estds_entrga				UdtConsecutivo,
			@vlr_cro								Int,
			@vlr_no									udtLogico;

	Create Table #tempProgemacionesFechaEvento (
		cnsctvo_cdgo_det_prgrmcn_fcha_evnto		udtConsecutivo
	)

	Set @fechaActual = getDate();
	Set @lcusuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User));
	Set @cnsctvo_cdgo_det_prgrmcn_fcha_evnto = 0;
	Set @cnsctvo_cdgo_estds_entrga = 151;
	Set @vlr_cro = 0;
	Set @vlr_no = 'N';

	-- Paso 1. Recuperamos el cnsctvo_cdgo_dt_prgrmcn_fcha_evnto

	Insert Into #tempProgemacionesFechaEvento (
				cnsctvo_cdgo_det_prgrmcn_fcha_evnto
	)
	Select		Distinct ss.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto
	From		#tempInformacionServicios a
	Inner Join	bdcna.gsa.tbASServiciosSolicitados ss with(nolock)
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	
	-- Paso 2  Actualizamos el estado del detalle programacion fecha evento

	Update		dp
	Set			cnsctvo_cdgo_estds_entrga = @cnsctvo_cdgo_estds_entrga, -- Estado de Notificación 
				cnsctvo_ops = @vlr_cro,
				cnsctvo_cdgo_ofcna_ops = @vlr_cro,
				usro_mdfccn = @lcusuario,
				fcha_ultma_mdfccn = @fechaActual
	From		bdSisalud.dbo.tbDetProgramacionFechaEvento dp With(RowLock)
	Inner Join	#tempProgemacionesFechaEvento a
	On			a.cnsctvo_cdgo_det_prgrmcn_fcha_evnto = dp.cnsctvo_cdgo_det_prgrmcn_fcha_evnto

	-- Paso 4. DesAsociamos la programación de Entrega

	Update		ss
	Set			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = @vlr_cro,
				prgrmcn_entrga = @vlr_no,
				usro_ultma_mdfccn = @lcusuario,
				fcha_ultma_mdfccn = @fechaActual
	From		#tempInformacionServicios a
	Inner Join	BDCna.gsa.tbASServiciosSolicitados ss
	On			ss.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	And			ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
		
End

