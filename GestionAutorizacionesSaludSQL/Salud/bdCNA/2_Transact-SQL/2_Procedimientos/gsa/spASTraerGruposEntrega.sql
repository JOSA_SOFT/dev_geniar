USE BDCna
GO
/****** Object:  StoredProcedure [gsa].[spASTraerGruposEntrega]    Script Date: 16/11/2017 12:07:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('gsa.spASTraerGruposEntrega') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE gsa.spASTraerGruposEntrega AS SELECT 1;'
END
GO
/*----------------------------------------------------------------------------------------
* Metodo o PRG :     GSA.spASTraerGruposEntrega
* Desarrollado por : <\A Ing. Germán Pérez - Geniar S.A.S A\>
* Descripcion  :     <\D Este procedimiento permite recuperar la lista de los grupos de entrega del afiliado D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P @ldFechaActual Fecha a la cual se valida la vigencia del grupo de entrega P\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/09/21 FC\>
* Ejemplo: 
    <\EJ
        EXEC GSA.spASTraerGruposEntrega null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE gsa.spASTraerGruposEntrega
	@ldFechaActual Datetime
AS
BEGIN
	SET NOCOUNT ON;

	Declare @lcVisibleUsuario udtLogico

	Set @lcVisibleUsuario = 'S'

	If @ldFechaActual Is Null
		Set @ldFechaActual = getDate()

    Select     cnsctvo_cdgo_grpo_entrga, cdgo_grpo_entrga, dscrpcn_grpo_entrga
	From       prm.tbASGruposEntrega_Vigencias  with(nolock)
	Where      vsble_usro = @lcVisibleUsuario
	And        @ldFechaActual Between inco_vgnca And fn_vgnca
	Order By   cnsctvo_cdgo_grpo_entrga

END
