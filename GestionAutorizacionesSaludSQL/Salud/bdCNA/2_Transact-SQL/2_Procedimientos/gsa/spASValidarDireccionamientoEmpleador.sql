USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASValidarDireccionamientoEmpleador]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASValidarDireccionamientoEmpleador
* Desarrollado por   : <\A Jois Lombana Sánchez A\>    
* Descripcion        : <\D Procedimiento que permite definir la IPS prestadora del servicio a partir de D\>		
					   <\D el empleador												 D\>         
* Observaciones   	 : <\O      O\>    
* Parametros         : <\P cdgs_slctd P\>    
* Variables          : <\V       V\>    
* Fecha Creacion  	 : <\FC 05/01/2016  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM   AM\>    
* Descripcion        : <\D         D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM    FM\>    
*-----------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASValidarDireccionamientoEmpleador] @agrpa_prstcn udtLogico
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @afirmacion char(1),
          @cantidadRegistros int,
          @indice int,
          @cnsctvo_cdgo_tpo_cntrto int,
          @nmro_cntrto varchar(15),
          @cnsctvo_cdgo_tpo_idntfccn_afldo int,
          @nmro_unco_idntfccn_afldo varchar(23),
          @id int,
          @cnsctvo_emprsa_vp int,
          @fechaActual datetime,
		  @vlor_cro int = 0;

  DECLARE @empleadores TABLE (
    cdgo_tpo_idntfccn varchar(3),
    nmro_idntfccn varchar(23),
    rzn_scl varchar(200),
    idntfccn_cmplta_empldr varchar(30),
    cnsctvo_cdgo_tpo_idntfccn int,
    nmro_unco_idntfccn_aprtnte int,
    prncpl char,
    cnsctvo_scrsl_ctznte int,
    cnsctvo_cdgo_crgo_empldo int,
    cnsctvo_cdgo_clse_aprtnte int,
    cnsctvo_cdgo_tpo_ctznte int,
    inco_vgnca_cbrnza datetime,
    fn_vgnca_cbrnza datetime,
    cnsctvo_cdgo_tpo_cbrnza int,
    cnsctvo_cbrnza int,
    slro_bsco int,
    cnsctvo_cdgo_tpo_cntrto int,
    nmro_cntrto varchar(15),
    cnsctvo_prdcto_scrsl int,
    cnsctvo_scrsl int,
    cnsctvo_cdgo_actvdd_ecnmca int,
    cnsctvo_cdgo_arp int,
    drccn varchar(80),
    tlfno varchar(30),
    cdgo_actvdd_ecnmca char(4),
    dscrpcn_actvdd_ecnmca varchar(150),
    cdgo_crgo char(4),
    dscrpcn_crgo varchar(150),
    cdgo_entdd char(8),
    dscrpcn_entdd varchar(150),
    cdgo_tpo_ctznte char(2),
    dscrpcn_tpo_ctznte varchar(150),
    nmbre_cntcto varchar(140),
    tlfno_cntcto udtTelefono,
    eml_cntcto udtEmail,
    cnsctvo_cdgo_cdd udtconsecutivo,
    cdgo_cdd char(8),
    dscrpcn_cdd udtDescripcion,
    cnsctvo_cdgo_dprtmnto udtConsecutivo,
    cdgo_dprtmnto char(3),
    dscrpcn_dprtmnto udtDescripcion,
    eml udtEmail,
    cdgo_cnvno_cmrcl char(2),
    dscrpcn_cnvno_cmrcl char(150)
  )

  DECLARE @afiliados TABLE(
    id INT IDENTITY(1,1),
    cnsctvo_cdgo_tpo_cntrto INT,
    nmro_cntrto varchar(15),
    cnsctvo_cdgo_tpo_idntfccn_afldo INT,
    nmro_idntfccn varchar(23)
	)

	DECLARE @empleadoresxafiliado TABLE(
    id INT IDENTITY(1,1),
    id_afldo INT,
    nmro_idntfccn_empldr varchar(15)
	)

  SELECT
    @afirmacion = 'S',
    @indice = 1,
    @cnsctvo_emprsa_vp = 45,
    @fechaActual = GETDATE();

  --Se valida que haya mas de 1 prestador asociado a cualquier prestación.
  IF EXISTS (SELECT
      id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino
    GROUP BY id_slctd_x_prstcn
    HAVING COUNT(id_slctd_x_prstcn) > 1)
  BEGIN

	--Se inserta la información por afiliado, solo para los que sea necesario realizar la consulta.
	WITH tmpPrestadores AS (SELECT
      id_slctd_x_prstcn
    FROM #tmpPrestadoresDestino
    GROUP BY id_slctd_x_prstcn
    HAVING COUNT(id_slctd_x_prstcn)>1 )
	INSERT INTO @afiliados (cnsctvo_cdgo_tpo_cntrto,
    nmro_cntrto,
    cnsctvo_cdgo_tpo_idntfccn_afldo,
    nmro_idntfccn)
	SELECT
        sol.cnsctvo_cdgo_tpo_cntrto,
        sol.nmro_cntrto,
        sol.cnsctvo_cdgo_tpo_idntfccn_afldo,
        sol.nmro_idntfccn
      FROM #tmpInformacionSolicitudPrestacion sol
	  INNER JOIN tmpPrestadores tmp
	  ON tmp.id_slctd_x_prstcn = sol.id
	  WHERE sol.mrca_msmo_prstdor = @vlor_cro
      GROUP BY sol.cnsctvo_cdgo_tpo_cntrto,
			   sol.nmro_cntrto,
			   sol.cnsctvo_cdgo_tpo_idntfccn_afldo,
			   sol.nmro_idntfccn;

    SELECT
      @cantidadRegistros = COUNT(sol.id)
    FROM @afiliados sol;

    WHILE @indice <= @cantidadRegistros
    BEGIN
      SELECT
        @cnsctvo_cdgo_tpo_cntrto = sol.cnsctvo_cdgo_tpo_cntrto,
        @nmro_cntrto = sol.nmro_cntrto,
        @cnsctvo_cdgo_tpo_idntfccn_afldo = sol.cnsctvo_cdgo_tpo_idntfccn_afldo,
        @nmro_unco_idntfccn_afldo = sol.nmro_idntfccn,
        @id = sol.id
      FROM @afiliados sol
      WHERE id = @indice;

      IF @id IS NOT NULL
      BEGIN
        /*Se obtienen los datos de empleador*/
        INSERT INTO @empleadores (cdgo_tpo_idntfccn, nmro_idntfccn, rzn_scl, idntfccn_cmplta_empldr, cnsctvo_cdgo_tpo_idntfccn, nmro_unco_idntfccn_aprtnte,
        prncpl, cnsctvo_scrsl_ctznte, cnsctvo_cdgo_crgo_empldo, cnsctvo_cdgo_clse_aprtnte, cnsctvo_cdgo_tpo_ctznte, inco_vgnca_cbrnza, fn_vgnca_cbrnza,
        cnsctvo_cdgo_tpo_cbrnza, cnsctvo_cbrnza, slro_bsco, cnsctvo_cdgo_tpo_cntrto, nmro_cntrto, cnsctvo_prdcto_scrsl, cnsctvo_scrsl,
        cnsctvo_cdgo_actvdd_ecnmca, cnsctvo_cdgo_arp, drccn, tlfno, cdgo_actvdd_ecnmca, dscrpcn_actvdd_ecnmca, cdgo_crgo, dscrpcn_crgo,
        cdgo_entdd, dscrpcn_entdd, cdgo_tpo_ctznte, dscrpcn_tpo_ctznte, nmbre_cntcto, tlfno_cntcto, eml_cntcto, cnsctvo_cdgo_cdd, cdgo_cdd,
        dscrpcn_cdd, cnsctvo_cdgo_dprtmnto, cdgo_dprtmnto, dscrpcn_dprtmnto, eml, cdgo_cnvno_cmrcl, dscrpcn_cnvno_cmrcl)
        EXEC BDAfiliacionValidador.dbo.spPmConsultaDetEmpleadoresAfiliado @cnsctvo_cdgo_tpo_cntrto,
                                                                          @nmro_cntrto,
                                                                          NULL,
                                                                          NULL,
                                                                          @cnsctvo_cdgo_tpo_idntfccn_afldo,
                                                                          @nmro_unco_idntfccn_afldo,
                                                                          NULL,
                                                                          NULL,
                                                                          NULL,
                                                                          NULL;

		INSERT INTO @empleadoresxafiliado(id_afldo, nmro_idntfccn_empldr)
		SELECT DISTINCT
		@id,
		empdor.nmro_idntfccn
		FROM @empleadores empdor
		INNER JOIN bdAfiliacionValidador.dbo.tbMarcasProductosxSucursal mps
		  ON mps.nmro_unco_idntfccn_empldr = empdor.nmro_unco_idntfccn_aprtnte
		INNER JOIN BDAfiliacionValidador.dbo.tbMarcasEspeciales_Vigencias tbev
			ON mps.cnsctvo_cdgo_mrca_espcl = tbev.cnsctvo_mrca_espcl
			AND tbev.cnsctvo_vgnca_mrca_espcl = @cnsctvo_emprsa_vp --VIP
		WHERE @fechaActual BETWEEN tbev.inco_vgnca AND tbev.fn_vgnca;

		DELETE @empleadores
      END

      SET @indice = @indice + 1;
    END;

    --Se valida que el prestador tenga asociado el empleador del afiliado en caso de que sea VIP.
    IF EXISTS (SELECT
        TCO.id_slctd_x_prstcn
      FROM #tmpPrestadoresDestino TCO
      INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
        ON ISP.id = TCO.id_slctd_x_prstcn
	  INNER JOIN @afiliados EMP
	    ON EMP.nmro_cntrto = ISP.nmro_cntrto 
		AND EMP.cnsctvo_cdgo_tpo_cntrto = ISP.cnsctvo_cdgo_tpo_cntrto
		AND EMP.cnsctvo_cdgo_tpo_idntfccn_afldo = ISP.cnsctvo_cdgo_tpo_idntfccn_afldo
		AND EMP.nmro_idntfccn = ISP.nmro_idntfccn
	   INNER JOIN @empleadoresxafiliado EAF 
	    ON EAF.id_afldo = EMP.id
		AND TCO.nmro_idntfccn_empldr_prstdr = EAF.nmro_idntfccn_empldr
		WHERE ISP.mrca_msmo_prstdor = @vlor_cro)
    BEGIN
      --Se actualizan los prestadores que apliquen el filtro.
      UPDATE TCO
      SET cmple = @afirmacion
      FROM #tmpPrestadoresDestino TCO
      INNER JOIN #tmpInformacionSolicitudPrestacion ISP WITH (NOLOCK)
        ON ISP.id = TCO.id_slctd_x_prstcn
	  INNER JOIN @afiliados EMP
	    ON EMP.nmro_cntrto = ISP.nmro_cntrto 
		AND EMP.cnsctvo_cdgo_tpo_cntrto = ISP.cnsctvo_cdgo_tpo_cntrto
		AND EMP.cnsctvo_cdgo_tpo_idntfccn_afldo = ISP.cnsctvo_cdgo_tpo_idntfccn_afldo
		AND EMP.nmro_idntfccn = ISP.nmro_idntfccn
	   INNER JOIN @empleadoresxafiliado EAF 
	    ON EAF.id_afldo = EMP.id
        AND TCO.nmro_idntfccn_empldr_prstdr = EAF.nmro_idntfccn_empldr
	   WHERE ISP.mrca_msmo_prstdor = @vlor_cro;

      --Se borran los prestadoresxprestacion que no cumplan la condición, y además ya se tengan asociados posibles prestadores para
      --esas prestaciones
      EXEC bdCNA.[gsa].[spASActualizarTemporalDireccionamiento]
    END


  END
END

GO
