Use bdCNA
Go

If(Object_id('gsa.spASCrearTrazaModificacionAnulacion') Is Null)
Begin
	Exec sp_executesql N'Create Procedure gsa.spASCrearTrazaModificacionAnulacion As Select 1';
End
Go


/*------------------------------------------------------------------------------------------ 
* M�todo o PRG		:		spASCrearTrazaModificacionAnulacion										 
* Desarrollado por	: <\A	Ing. Juan Carlos V�squez G.	A\>	 
* Descripci�n		: <\D	SP que crea y guarda la traza de modificaciones D\>	 
* Observaciones		: <\O	O\>	 
* Par�metros		: <\P	P\>	 
* Variables			: <\V	V\>	 
* Fecha Creaci�n	: <\FC	2017/10/03	FC\> 
*-------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACI�N																		 
*------------------------------------------------------------------------------------------- 
* Modificado Por	: <\AM  AM\> 
* Descripci�n		: <\DM  DM\> 
* Nuevos Par�metros	: <\PM	PM\> 
* Nuevas Variables	: <\VM	VM\> 
* Fecha Modificaci�n: <\FM  FM\>
*------------------------------------------------------------------------------------------- */


Alter Procedure gsa.spASCrearTrazaModificacionAnulacion
As
Begin
	Set Nocount On

	Declare @fcha_actl						Datetime,
			@dto_elmnto_orgn_mdfccn			udtDescripcion,
			@ldOrgn_mdfccn					udtDescripcion,
			@nmro_ip						Varchar(12),
			@cnsctvo_cdgo_estdo_mga_anldo	udtConsecutivo;

	Set @fcha_actl				= getDate();
	Set @dto_elmnto_orgn_mdfccn = 'Anulaci�n';
	Set @ldOrgn_mdfccn		    = 'tbASConceptosServicioSolicitado.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo';
	Set @nmro_ip				= '127.0.0.1';
	Set @cnsctvo_cdgo_estdo_mga_anldo = 14;

	Insert 
	Into   #tmpTrazaModificacion(	cnsctvo_slctd_atrzcn_srvco, cnsctvo_srvco_slctdo     , cnsctvo_cdgo_mtvo_csa        , 
									fcha_mdfccn               , orgn_mdfccn              , vlr_antrr                    , 
									vlr_nvo                   , usro_mdfccn              , obsrvcns                     , 
									nmro_ip                   , fcha_crcn                , usro_crcn                    , 
									fcha_ultma_mdfccn         , usro_ultma_mdfccn        , elmnto_orgn_mdfccn           , 
									dto_elmnto_orgn_mdfccn    , dscrpcn_vlr_antrr        , dscrpcn_vlr_nvo
			                    )
	Select     a.cnsctvo_slctd_atrzcn_srvco      , b.cnsctvo_srvco_slctdo          , a.cnsctvo_cdgo_mtvo_csa         , 
				@fcha_actl                       , @ldOrgn_mdfccn                  , b.cnsctvo_cdgo_estdo_mga_antrr  , 
				b.cnsctvo_cdgo_estdo_mga	     , a.usro_anlcn                    , a.obsrvcn						 , 
				@nmro_ip                         , @fcha_actl                      , a.usro_anlcn					 , 
				@fcha_actl                       , a.usro_anlcn					   , a.orgn_mdfccn                   , 
				@dto_elmnto_orgn_mdfccn          , c.dscrpcn_estdo_srvco_slctdo    , d.dscrpcn_estdo_srvco_slctdo	
	From		#tempInformacionSolicitud a
	Inner Join	#tempInformacionServicios b
	On			b.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_atrzcn_srvco
	Inner Join	bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias c With(NoLock)
	On			c.cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga_antrr
	Inner Join	bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias	d With(NoLock)
	On			d.cnsctvo_cdgo_estdo_srvco_slctdo = b.cnsctvo_cdgo_estdo_mga
	Where		b.cnsctvo_cdgo_estdo_mga_antrr != @cnsctvo_cdgo_estdo_mga_anldo;
	

	Exec BDCna.gsa.spASGuardarTrazaModificacion;

End

