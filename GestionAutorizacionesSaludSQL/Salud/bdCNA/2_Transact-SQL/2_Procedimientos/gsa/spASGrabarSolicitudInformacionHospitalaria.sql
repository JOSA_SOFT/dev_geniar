USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGrabarSolicitudInformacionHospitalaria]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASGrabarSolicitudInformacionHospitalaria
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D Procedimiento expuesto que permite ingresar información Hospitalaria relacionada
					 : a una solicitud ingresada   D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\D Se ajusta la creación de variables dentro de los Procedimientos 
						para el manejo de los valores constantes? D\>  
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>      
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASGrabarSolicitudInformacionHospitalaria] 
AS
  SET NOCOUNT ON
	  
	DECLARE @Fecha_Actual as datetime
	SET		@Fecha_Actual = Getdate()
	BEGIN
		-- INSERTAR INFORMACIÓN DE Información Hospitalaria EN TABLA DE PROCESO tbASInformacionHospitalariaSolicitudAutorizacionServicios 	
		MERGE INTO			BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServicios
		USING (SELECT		 IDS.cnsctvo_slctd_atrzcn_srvco
							,THO.id
							,THO.ds_estnca	
							,THO.nmro_vsts
							,THO.fcha_ingrso_hsptlzcn			
							,THO.fcha_egrso_hsptlzcn			
							,THO.cnsctvo_cdgo_clse_hbtcn
							,THO.cnsctvo_cdgo_srvco_hsptlzcn	
							,THO.cma	
							,THO.crte_cnta						
							,THO.usro_crcn						
				FROM		#Tempo_Hospitalaria	THO WITH (NOLOCK)		
				INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
				ON			THO.id = IDS.IdXML) AS HOS								
		ON		1 = 0
		WHEN	NOT MATCHED THEN
		INSERT ( cnsctvo_slctd_atrzcn_srvco
				,ds_estnca
				,nmro_vsts
				,fcha_ingrso_hsptlzcn
				,fcha_egrso_hsptlzcn
				,cnsctvo_cdgo_clse_hbtcn
				,cnsctvo_cdgo_srvco_hsptlzcn
				,cma
				,crte_cnta
				,fcha_crcn
				,usro_crcn
				,fcha_ultma_mdfccn
				,usro_ultma_mdfccn
				)
		VALUES (HOS.cnsctvo_slctd_atrzcn_srvco
				,HOS.ds_estnca	
				,HOS.nmro_vsts
				,HOS.fcha_ingrso_hsptlzcn			
				,HOS.fcha_egrso_hsptlzcn			
				,HOS.cnsctvo_cdgo_clse_hbtcn
				,HOS.cnsctvo_cdgo_srvco_hsptlzcn	
				,HOS.cma	
				,HOS.crte_cnta
				,@Fecha_Actual	--fcha_crcn						
				,HOS.usro_crcn	
				,@Fecha_Actual	--fcha_ultma_mdfccn
				,HOS.usro_crcn	--usro_ultma_mdfccn
				)	
		OUTPUT	inserted.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
				,HOS.id
		INTO	#IdHospitalaria;
	   -- INSERTAR INFORMACIÓN DE Información Hospitalaria EN TABLA ORIGINAL tbInformacionHospitalariaSolicitudAutorizacionServiciosOriginal 
		INSERT INTO	BdCNA.gsa.tbASInformacionHospitalariaSolicitudAutorizacionServiciosOriginal(
					 cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
					,ds_estnca
					,cnsctvo_slctd_atrzcn_srvco
					,nmro_vsts
					,fcha_ingrso_hsptlzcn
					,fcha_egrso_hsptlzcn
					,cdgo_clse_hbtcn
					,cma
					,cdgo_srvco_hsptlzcn
					,crte_cnta
					,fcha_crcn
					,usro_crcn
					,fcha_ultma_mdfccn
					,usro_ultma_mdfccn)
		SELECT		 IDH.cnsctvo_infrmcn_hsptlra_slctd_atrzcn_srvco
					,HOS.ds_estnca	
					,IDS.cnsctvo_slctd_atrzcn_srvco
					,HOS.nmro_vsts
					,HOS.fcha_ingrso_hsptlzcn			
					,HOS.fcha_egrso_hsptlzcn	
					,HOS.cdgo_clse_hbtcn
					,HOS.cma				
					,HOS.cdgo_srvco_hsptlzcn	
					,HOS.crte_cnta
					,@Fecha_Actual	--fcha_crcn						
					,HOS.usro_crcn	
					,@Fecha_Actual	--fcha_ultma_mdfccn
					,HOS.usro_crcn	--usro_ultma_mdfccn		
		FROM		#Tempo_Hospitalaria	HOS WITH (NOLOCK)
		INNER JOIN	#IdHospitalaria		IDH WITH (NOLOCK)
		ON			HOS.Id = IDH.idXML
		INNER JOIN	#Idsolicitudes		IDS WITH (NOLOCK)
		ON			HOS.Id = IDS.idXML
		  
	END

GO
