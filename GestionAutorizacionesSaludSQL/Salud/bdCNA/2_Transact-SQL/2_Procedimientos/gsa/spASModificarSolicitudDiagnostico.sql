USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASModificarSolicitudDiagnostico]    Script Date: 7/19/2017 5:33:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASModificarSolicitudDiagnostico
* Desarrollado por   : <\A Jois Lombana    A\>    
* Descripcion        : <\D 
                           Procedimiento expuesto que permite ingresar información de Diagnóstico como 
					 	   modificación de una solicitud especifica 
					    D\>   
* Observaciones      : <\O      O\>    
* Parametros         : <\P		P\>    
* Variables          : <\V      V\>    
* Fecha Creacion  	 : <\FC 09/12/2015  FC\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jois Lombana  AM\>    
* Descripcion        : <\DM Se ajustan no conformidades: 
*					       25 Se crean variables dentro de los Procedimientos para el manejo de 
                              los valores constantes?
*					       30 Se evita el uso de subquerys? En su lugar usar joins 
                       DM\> 
* Nuevas Variables   : <\VM @Fecha_actual VM\>    
* Fecha Modificacion : <\FM 29/12/2015 FM\>    
*-------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Ing. Victor Hugo Gil Ramos  AM\>    
* Descripcion        : <\DM 
                            Se ajusta el procedimiento para que borre el registro de la tabla
							tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias cuando se 
							modifique una solicitud por inconsistencias administrativa
                         DM\> 
* Nuevas Variables   : <\VM  VM\>    
* Fecha Modificacion : <\FM 19/07/2017 FM\>    
*-----------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE [gsa].[spASModificarSolicitudDiagnostico] 
AS
  SET NOCOUNT ON

  BEGIN

	DECLARE @Fecha_Actual Datetime,
	        @fn_vgnca     Datetime
			   
	Set     @Fecha_Actual = Getdate()
	Set     @fn_vgnca     = '9999-12-31'

	
	-- ELIMINAR REGISTRO EXISTENTE TABLA ORIGINAL tbASDiagnosticosSolicitudAutorizacionServiciosOriginal
	DELETE			DGO
	FROM			BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal DGO With(NoLock)
	INNER JOIN		#TMod_Solicitudes	TSO
	ON				DGO.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		
	-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias
	DELETE			DGP
	FROM			BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias DGP With(NoLock)
	INNER JOIN		#TMod_Solicitudes	TSO
	ON				DGP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo		
		
		
	-- ELIMINAR REGISTRO EXISTENTE TABLA DE PROCESO tbASDiagnosticosSolicitudAutorizacionServicios
	DELETE			DGP
	FROM			BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios DGP With(NoLock)
	INNER JOIN		#TMod_Solicitudes	TSO
	ON				DGP.cnsctvo_slctd_atrzcn_srvco = TSO.cnsctvo_slctd_atrzcn_srvco_rcbdo
		
		
	-- INSERTAR INFORMACIÓN DE DIAGNOSTICO EN TABLA DE PROCESO tbASDiagnosticosSolicitudAutorizacionServicios 	
	MERGE INTO BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios
	USING  (SELECT	   IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo, TDI.id                   , TDI.cnsctvo_cdgo_tpo_dgnstco,
					   TDI.cnsctvo_cdgo_dgnstco            , TDI.cnsctvo_cdgo_cntngnca, TDI.cnsctvo_cdgo_rcbro	  ,
					   TDI.usro_crcn			           , TDI.nmro_prstcn			
		    FROM	   #TMod_Diagnostico TDI 		
		    INNER JOIN #TMod_Solicitudes IDS 
		    ON		   TDI.id = IDS.id
		    ) AS DIA
	ON		1 = 0
	WHEN	NOT MATCHED THEN
	INSERT (cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_tpo_dgnstco, cnsctvo_cdgo_dgnstco,
			cnsctvo_cdgo_cntngnca     , cnsctvo_cdgo_rcbro      , fcha_crcn           ,
			usro_crcn                 , fcha_ultma_mdfccn       , usro_ultma_mdfccn	
		   )
	VALUES (DIA.cnsctvo_slctd_atrzcn_srvco_rcbdo, DIA.cnsctvo_cdgo_tpo_dgnstco, DIA.cnsctvo_cdgo_dgnstco,	
			DIA.cnsctvo_cdgo_cntngnca           , DIA.cnsctvo_cdgo_rcbro      , @Fecha_Actual	        ,--fcha_crcn	
			DIA.usro_crcn			            , @Fecha_Actual	              , DIA.usro_crcn	         --usro_ultma_mdfccn
		   )
	OUTPUT	inserted.cnsctvo_dgnstco_slctd_atrzcn_srvco, 
	                 DIA.Id                            , 
	                 DIA.nmro_prstcn                   
    INTO	#IdDiagnostico_Mod;
		 

	-- INSERTAR INFORMACIÓN DE DIAGNOSTICO EN TABLA ORIGINAL tbASDiagnosticosSolicitudAutorizacionServiciosOriginal 
	INSERT 
	INTO	    BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal(cnsctvo_dgnstco_slctd_atrzcn_srvco, cnsctvo_slctd_atrzcn_srvco, cdgo_dgnstco     ,
					                                                             cdgo_tpo_dgnstco                  , cdgo_cntngnca             , cdgo_rcbro       ,
   					                                                             fcha_crcn                         , usro_crcn                 , fcha_ultma_mdfccn,
					                                                             usro_ultma_mdfccn
																			    )
	SELECT		IDD.cnsctvo_dgnstco_slctd_atrzcn_srvco, IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo, DIA.cdgo_dgnstco,
				DIA.cdgo_tpo_dgnstco                  , DIA.cdgo_cntngnca                   , DIA.cdgo_rcbro  ,
				@Fecha_Actual	                      , DIA.usro_crcn                      , @Fecha_Actual   ,--fcha_ultma_mdfccn
				DIA.usro_crcn	--usro_ultma_mdfccn			
	FROM		#TMod_Diagnostico	DIA 
	INNER JOIN	#IdDiagnostico_Mod	IDD 
	ON			DIA.id = IDD.idXML
	AND			DIA.nmro_prstcn = IDD.nmro_prstcn
	INNER JOIN	#TMod_Solicitudes IDS 
	ON			DIA.id = IDS.id

	

	--INSERTAR INFORMACIÓN DE DIAGNOSTICO EN TABLA ORIGINAL tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias 
	INSERT 
	INTO	    BdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias(cnsctvo_dgnstco_slctd_atrzcn_srvco, cnsctvo_slctd_atrzcn_srvco, cnsctvo_cdgo_dgnstco,
					                                                               inco_vgnca                        , fn_vgnca                  , fcha_crcn           , 
																				   usro_crcn                         , fcha_ultma_mdfccn         , usro_ultma_mdfccn
																			      )
	SELECT		IDD.cnsctvo_dgnstco_slctd_atrzcn_srvco, IDS.cnsctvo_slctd_atrzcn_srvco_rcbdo, DIA.cnsctvo_cdgo_dgnstco,
				@Fecha_Actual			              , @fn_vgnca                           , @Fecha_Actual           , 
				DIA.usro_crcn                         , @Fecha_Actual	                    , DIA.usro_crcn	--usro_ultma_mdfccn			
	FROM		#TMod_Diagnostico  DIA 
	INNER JOIN	#IdDiagnostico_Mod IDD 
	ON			DIA.id = IDD.idXML
	AND			DIA.nmro_prstcn = IDD.nmro_prstcn
	INNER JOIN	#TMod_Solicitudes IDS 
	ON			DIA.id = IDS.id
END



