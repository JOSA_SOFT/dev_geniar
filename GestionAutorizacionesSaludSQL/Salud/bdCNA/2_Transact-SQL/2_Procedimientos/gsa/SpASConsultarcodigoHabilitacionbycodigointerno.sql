USE [bdSisalud]
GO
/****** Object:  StoredProcedure [dbo].[SpASConsultarcodigoHabilitacionbycodigointerno]    Script Date: 21/11/2016 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*---------------------------------------------------------------------------------     
* Metodo o PRG     :  SpASConsultarcodigoHabilitacionbycodigointerno
* Desarrollado por : <\A Ing. Angela Sandoval A\>     
* Descripcion      : <\D 
						Consulta el numero de habilitacion desde el codigo interno
					 D\>     
	 
* Parametros       : <\P
						@cdgo_intrno
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2016/11/01 FC\>     
*     
*---------------------------------------------------------------------------------------------*/
-- exec dbo.[SpASConsultarcodigoHabilitacionbycodigointerno] '01480'   
ALTER PROCEDURE [dbo].[SpASConsultarcodigoHabilitacionbycodigointerno]
@cdgo_intrno char(12) --> codigo interno del prestador

AS
Set NoCount On

DECLARE
	@fecha_actual datetime
	
	SET @fecha_actual = getDate()

	
	SELECT dp.cdgo_prstdr_mnstro, dp.nmro_unco_idntfccn_prstdr, dp.nmbre_scrsl
	FROM tbDireccionesPrestador dp with (nolock)-- los prestadores				
	WHERE 
		@fecha_actual between dp.inco_vgnca and dp.fn_vgnca 
		AND cdgo_intrno = @cdgo_intrno
		