USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spAsPoblarTemporalesMigracionEstadosSipresMega]    Script Date: 02/06/2017 05:19:53 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------------------------------------														
* Metodo o PRG 				: spAsPoblarTemporalesMigracionEstadosSipresMega							
* Desarrollado por			: <\A   Ing. Carlos Andres Lopez Ramirez - qvisionclr A\>  
* Descripcion				: <\D	Llena la tabla #tempPrestaciones con la informacion de las solicitudes de sipres y mega
							  D\>  												
* Observaciones				: <\O O\>  													
* Parametros				: <\P  Entrada:
								   1- @usuario_sistema: usuario del sistema
							  P\>  													
* Variables					: <\V  1- @fcha_actl: Fecha Actual obtenida del sistema.
								   2- @cnsctvo_cdgo_mdlo_cra46: codigo que crea mega
								   3- @vlr_cro: Almacena el valor 0
								   4- @cnsctvo_cdgo_estdo8: Codigo de estado que indica que la ops esta impresa en Sipres
								   5- @cnsctvo_cdgo_estdo3: Codigo Sipres que indica que la ops esta anulada.
								   6- @cnsctvo_cdgo_estdo11: Codigo sipres que indica que la ops esta vencida.
								   7- @cnsctvo_cdgo_estdo_srvco_slctdo15: Codigo Mega que indica que la ops esta entregada
								   8- @vlr_uno: Almacena el valor 1
							  V\>  	
* Metdos o PRG Relacionados	:      1- BdCna.spAsMigracionEstadosSipresMega
* Pre-Condiciones			:											
* Post-Condiciones			:
* Fecha Creacion			: <\FC 2017/01/12 FC\>  																										
*------------------------------------------------------------------------------------------------------------------------ 															
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se quita la instruccion "And cs.cnsctvo_ops = c.cnsctvo_ops" ya que en mega no se esta 
							   calculando este valor y se organizan consultas  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/17 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se modifican consultas para recuperar el consecutivo que relaciona las prestaciones
							   de Mega con Sipres de la tabla tbASDatosAdicionalesMegaSipres. 
							   Se agrega tabla temporal para la carga de informacion.
							   Se agrega update para recuperar el cnsctvo_slctd_mga  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/01/30 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agregan consultas para cargar las prestaciones con estados No Autorizada y
							   No Tramitada en sipres  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/07 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega select para cargar datos de prestaciones impresas de tbAtencionOps  DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/13 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega group by en carga de conceptos para evitar que se carguen registros duplicados. 
							   Se agrega consulta para obtener las solicitudes con prestaciones en estado cobrada. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/14 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega update para obtener la fecha de impresion de tbConceptosOps DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/15 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega update para poner el nmro_unco_ops = 0 para los conceptos que
							   carguen este campo en Null DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agregan validaciones a las tablas tbAutorizacionesOPS y tbAutorizacionesOPSDetalle
							   para buscar las prestaciones que ya fueron utilizadas, es decir, las prestaciones a las
							   cuales ya se les presto el servicio, y cambiar su estado a Usada. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/02/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se quitan referencias de la tabla temporal #tempSolicitudes, se agrega carga de datos de 
							   las prestaciones cuyo numero de ops aparece en las tablas tbAutorizacionesOPS y
							   tbAutorizacionesOPSDetalle detalle. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/04/27 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agrega Delete para las prestaciones anuladas de forma que, si la prestacion aparece 
							   con estado anulada en el historico, esta no migres DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/03 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se organizan Join para mejorar los tiempos de consulta. 
							   Se agrega condicional a consulta de OPS usadas DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/24 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se realiza ajuste para evitar que las prestaciones impresas en mega
							   cambien a estado procesada. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/25 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se extiende condicion para no migracion de prestaciones anuladas por sipres a mega  para
							   que no permita migrar ningun estado cuando en mega la prestacion este anulada, No Autorizada
							   o devuelta. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/05/31 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------													
* Modificado Por		: <\AM Ing. Carlos Andres Lopez Ramirez AM\>  													
* Descripcion			: <\DM Se agregan sentencias para eliminar de la temporal de conceptos los conceptos duplicados
							   Se agrega condicion para evitar la migracion de prestaciones con oficina erronea. DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM VM\>  													
* Fecha Modificacion	: <\FM 2017/06/02 FM\>  													
*-----------------------------------------------------------------------------------------------------------------------*/

ALTER Procedure [gsa].[spAsPoblarTemporalesMigracionEstadosSipresMega]
			@fcha_mgra									Date
As
Begin
	Set NoCount On

	Declare @cnsctvo_cdgo_mdlo_cra46					UdtConsecutivo,
			@vlr_cro									Int,
			@cnsctvo_cdgo_estdo7						UdtConsecutivo,
			@cnsctvo_cdgo_estdo8						UdtConsecutivo,
			@cnsctvo_cdgo_estdo2						UdtConsecutivo,
			@cnsctvo_cdgo_estdo3						UdtConsecutivo,
			@cnsctvo_cdgo_estdo4						UdtConsecutivo,
			@cnsctvo_cdgo_estdo11						UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo15			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo6			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo8			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo10			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo12			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo13			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo14			UdtConsecutivo,
			@cnsctvo_cdgo_estdo_srvco_slctdo16			UdtConsecutivo,
			@fcha_actl									Date,
			@vlr_uno									Int;

	-- Se crea temporal para guardar los estados que permiten vencimiento
	Create Table #tempEstados
	(
		cnsctvo_cdgo_estdo_srvco_slctdo				UdtConsecutivo
	);

	Create Table #tempServiciosValor 
	(
			nuam							UdtConsecutivo,
			cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
			cnsctvo_prstcn					UdtConsecutivo,
			vlr_lqdcn_srvco					Decimal(18, 0) Default 0						
	)

	Create Table #tempPrestacionesUsadas
	(
				cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,			
				cnsctvo_cdgo_srvco_slctdo		udtConsecutivo,	
				cnsctvo_cdgo_estdo_srvco_slctdo	udtConsecutivo,	
				cnsctvo_cdgo_estdo				udtConsecutivo, 				
				cntdd_slctda					Int,						
				cnsctvo_cdgo_ofcna				udtConsecutivo,					
				nuam							udtConsecutivo,
				cnsctvo_srvco_slctdo			udtConsecutivo,				
				fcha_ultma_mdfccn				Date,					
				usro_ultma_mdfccn				udtUsuario								
	)

		-- 
		Set @cnsctvo_cdgo_mdlo_cra46 = 46;
		Set @vlr_cro = 0;
		Set @cnsctvo_cdgo_estdo7 = 7;
		Set @cnsctvo_cdgo_estdo8 = 8;
		Set @cnsctvo_cdgo_estdo2 = 2;
		Set @cnsctvo_cdgo_estdo3 = 3;
		Set @cnsctvo_cdgo_estdo4 = 4;
		Set @cnsctvo_cdgo_estdo11 = 11;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo15 = 15;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo6 = 6;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo8 = 8;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo10 = 10;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo12 = 12;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo13 = 13;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo14 = 14;
		Set @cnsctvo_cdgo_estdo_srvco_slctdo16 = 16;
		Set @fcha_actl = getDate();
		Set @vlr_uno = 1;

		

		--Tabla de estados
		--(Referencia: Select * From BdCna.prm.tbAsEstadosServiciosSolicitados_Vigencias)
		Insert Into #tempEstados
		(
					cnsctvo_cdgo_estdo_srvco_slctdo
		)
		Select		cnsctvo_cdgo_estdo_srvco_slctdo
		From		bdCNA.prm.tbASEstadosServiciosSolicitados_Vigencias With(NoLock)
		Where		@fcha_actl BetWeen inco_vgnca And fn_vgnca

		-- 
		Delete 
		From		#tempEstados 
		Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo6

		Delete 
		From		#tempEstados 
		Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo8

		Delete 
		From		#tempEstados 
		Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo10

		Delete 
		From		#tempEstados 
		Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo12

		Delete 
		From		#tempEstados 
		Where		cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo14

				
		-- 
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		And			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Inner Join	#tempEstados e
		On			ss.cnsctvo_cdgo_estdo_srvco_slctdo = e.cnsctvo_cdgo_estdo_srvco_slctdo
		Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo11 -- Sipres Vencida por no tramite
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		
		-- Llena la tabla con las solicitudes entregadas en Sipres
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					co.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		And			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		Inner Join	bdSisalud.dbo.tbConceptosOps co With(NoLock)
		On			co.nuam = p.nuam
		And			co.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			co.cnsctvo_prstcn = p.cnsctvo_prstcn
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Where		co.fcha_ultma_mdfccn > = @fcha_mgra	
		And			co.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8	-- sipres Entregada	
		And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_srvco_slctdo13 -- @cnsctvo_cdgo_estdo_srvco_slctdo15 Se cambia como medida temporal para evitar la impresion de solicitudes impresas en sipres que migran a mega. 	
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					co.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn	

		-- 
		--Insert Into #tempPrestaciones
		--(
		--			cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
		--			cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
		--			cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		--)
		--Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
		--			p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
		--			ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		--From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		--Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		--On			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		--And			dams.nuam = p.nuam
		--And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		--Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		--On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		--And			ao.nuam = dams.nuam
		--Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		--On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		--And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		--Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		--And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8	-- sipres Entregada	
		--And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_srvco_slctdo13 -- @cnsctvo_cdgo_estdo_srvco_slctdo15 Se cambia como medida temporal para evitar la impresion de solicitudes impresas en sipres que migran a mega. 			
		--Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
		--			p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
		--			ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn

		-- Se cargan las solicitudes cobradas en Sipres
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo7	-- sipres Cobrada	
		And			ss.cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_srvco_slctdo12 -- Mega Cobrada
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn

		-- Carga las prestaciones en estado no autorizada
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo2 -- Sipres No Autorizada
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn


		-- Carga las prestaciones en estado no tramitada
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo4 -- Sipres Vencida por no tramite
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		

		-- Llena la tabla con las solicitudes anuladas en Sipres	
		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo, 				
					cntdd_slctda,						cnsctvo_cdgo_ofcna,					nuam,
					cnsctvo_srvco_slctdo,				fcha_ultma_mdfccn,					usro_ultma_mdfccn													
		)
		Select		dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			dams.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			dams.nuam = p.nuam
		And			dams.cnsctvo_prstcn = p.cnsctvo_prstcn
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_prstcn
		Where		ao.fcha_ultma_mdfccn > = @fcha_mgra	
		And			ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo3	-- sipres Anulada	
		Group By	dams.cnsctvo_slctd_mga ,			p.cnsctvo_prstcn,					ao.cnsctvo_cdgo_estdo,
					p.cntdd_prstcn,						p.cnsctvo_cdgo_ofcna,				p.nuam,
					ss.cnsctvo_srvco_slctdo,			p.fcha_ultma_mdfccn,				p.usro_ultma_mdfccn	

		-- Carga las prestaciones que ya fueron utilizadas, es decir las que ya se les presto el servicio al afiliado.
				Insert Into #tempPrestacionesUsadas
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo, 				
					cnsctvo_cdgo_ofcna,					nuam,								cnsctvo_srvco_slctdo,				
					fcha_ultma_mdfccn,					usro_ultma_mdfccn											
		)
		Select		dams.cnsctvo_slctd_mga ,			dams.cnsctvo_prstcn,				@cnsctvo_cdgo_estdo_srvco_slctdo16,
					dams.cnsctvo_cdgo_ofcna,			dams.nuam,							ss.cnsctvo_srvco_slctdo,			
					ao.fcha_crcn,						ao.usro_crcn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = dams.cnsctvo_prstcn
		Inner Join	bdCNA.gsa.tbASProcedimientosInsumosSolicitados pis With(NoLock)
		On			pis.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		On			css.cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
		Inner join	bdSisalud.dbo.tbAutorizacionesOPSDetalle det  With(nolock) 
		On			det.nmro_unco_ops = css.nmro_unco_ops
		Inner Join	bdSisalud.dbo.tbAutorizacionesOPS ao With(NoLock)
		On			ao.cdgo_intrno_prstdr = css.cdgo_intrno_prstdr_atrzdo 
		And			det.cnsctvo_cdgo_atrzcn_ops=ao.cnsctvo_cdgo_atrzcn_ops
		Where		det.ops_atrzda =  @vlr_uno
		And			ao.fcha_crcn >= @fcha_mgra
		Group By	dams.cnsctvo_slctd_mga ,			dams.cnsctvo_prstcn,				dams.cnsctvo_cdgo_ofcna,			
					dams.nuam,							ss.cnsctvo_srvco_slctdo,			ao.fcha_crcn,						
					ao.usro_crcn

		-- 
		Insert Into #tempPrestacionesUsadas
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo, 				
					cnsctvo_cdgo_ofcna,					nuam,								cnsctvo_srvco_slctdo,				
					fcha_ultma_mdfccn,					usro_ultma_mdfccn					
		)
		Select		dams.cnsctvo_slctd_mga ,			dams.cnsctvo_prstcn,				@cnsctvo_cdgo_estdo_srvco_slctdo16,
					dams.cnsctvo_cdgo_ofcna,			dams.nuam,							ss.cnsctvo_srvco_slctdo,			
					ao.fcha_crcn,						ao.usro_crcn
		From		bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		Inner Join	bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		And			ss.cnsctvo_cdgo_srvco_slctdo = dams.cnsctvo_prstcn
		Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
		On			ms.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo
		Inner Join	bdCNA.gsa.tbASConceptosServicioSolicitado css With(NoLock)
		On			css.cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
		Inner join	bdSisalud.dbo.tbAutorizacionesOPSDetalle det  With(nolock) 
		On			det.nmro_unco_ops = css.nmro_unco_ops
		Inner Join	bdSisalud.dbo.tbAutorizacionesOPS ao With(NoLock)
		On			ao.cdgo_intrno_prstdr = css.cdgo_intrno_prstdr_atrzdo 
		And			det.cnsctvo_cdgo_atrzcn_ops=ao.cnsctvo_cdgo_atrzcn_ops
		Where		det.ops_atrzda =  @vlr_uno
		And			ao.fcha_crcn >= @fcha_mgra
		Group By	dams.cnsctvo_slctd_mga ,			dams.cnsctvo_prstcn,				dams.cnsctvo_cdgo_ofcna,			
					dams.nuam,							ss.cnsctvo_srvco_slctdo,			ao.fcha_crcn,						
					ao.usro_crcn

		-- Si la prestacion esta usada y se cargo 
		Delete		p 
		From		#tempPrestaciones p
		Inner Join	#tempPrestacionesUsadas pu
		On			pu.nuam = p.nuam
		And			pu.cnsctvo_cdgo_ofcna =  p.cnsctvo_cdgo_ofcna
		And			pu.cnsctvo_cdgo_srvco_slctdo = p.cnsctvo_cdgo_srvco_slctdo
		And			pu.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco


		Insert Into #tempPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo, 				
					cnsctvo_cdgo_ofcna,					nuam,								cnsctvo_srvco_slctdo,				
					fcha_ultma_mdfccn,					usro_ultma_mdfccn	
		)
		Select		cnsctvo_slctd_atrzcn_srvco,			cnsctvo_cdgo_srvco_slctdo,			cnsctvo_cdgo_estdo_srvco_slctdo, 				
					cnsctvo_cdgo_ofcna,					nuam,								cnsctvo_srvco_slctdo,				
					fcha_ultma_mdfccn,					usro_ultma_mdfccn	
		From		#tempPrestacionesUsadas


		Delete		p 
		From		#tempPrestaciones p
		Inner Join	bdCNa.gsa.tbASSolicitudesAutorizacionServicios sas With(NoLock)
		On			sas.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
		And			sas.cnsctvo_cdgo_ofcna_atrzcn != p.cnsctvo_cdgo_ofcna


		-- Se evita la migracion de las prestaciones que en el historico aparescan como anuladas.
		Delete		p
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbASHistoricoEstadosServicioSolicitado hess With(NoLock)
		On			hess.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
		Where		hess.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo14

		Delete		p
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbASHistoricoEstadosServicioSolicitado hess With(NoLock)
		On			hess.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
		Where		hess.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo6

		Delete		p
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbASHistoricoEstadosServicioSolicitado hess With(NoLock)
		On			hess.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
		Where		hess.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo8

		-- Se evita la migracion de prestaciones de sipres a mega si estas ya fueron impresas en mega.
		Delete		p
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbASHistoricoEstadosServicioSolicitado hess With(NoLock)
		On			hess.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo
		Where		hess.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo15
		And			p.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8

		-- 
		Update		pr
		Set			cnsctvo_cdgo_estdo = ao.cnsctvo_cdgo_estdo,
					cntdd_slctda = p.cntdd_prstcn,
					cntdd_atrzda = p.cntdd_prstcn_rl
		From		#tempPrestaciones pr
		Inner Join	bdSisalud.dbo.tbASDatosAdicionalesMegaSipres dams With(NoLock)
		On			pr.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		Inner Join	bdSisalud.dbo.tbAtencionOps ao With(NoLock)
		On			ao.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		And			ao.nuam = dams.nuam				
		Inner Join	bdSisalud.dbo.tbProcedimientos p With(NoLock)
		On			p.nuam = dams.nuam
		And			p.cnsctvo_prstcn = dams.cnsctvo_prstcn
		And			p.cnsctvo_cdgo_ofcna = dams.cnsctvo_cdgo_ofcna
		Inner Join	BDCna.gsa.tbASServiciosSolicitados ss With(NoLock)
		On			ss.cnsctvo_srvco_slctdo = pr.cnsctvo_srvco_slctdo
		And			ss.cnsctvo_slctd_atrzcn_srvco = dams.cnsctvo_slctd_mga
		Where		ao.cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8
		And			pr.cnsctvo_cdgo_estdo Is Null

		-- 
		Update		p
		Set			cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbAsProcedimientosInsumosSolicitados pis With(NoLock)
		On			pis.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo

		--
		Update		p
		Set			cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
		From		#tempPrestaciones p
		Inner Join	bdCNA.gsa.tbASMedicamentosSolicitados ms With(NoLock)
		On			ms.cnsctvo_srvco_slctdo = p.cnsctvo_srvco_slctdo


		
		-- Calcula el estado Mega menos para las prestaciones que ya fueron usadas.
		Update		p
		Set			cnsctvo_cdgo_estdo_srvco_slctdo = essv.cnsctvo_cdgo_estdo_srvco_slctdo
		From		#tempPrestaciones p
		Inner Join	bdCNA.prm.tbAsEstadosServiciosSolicitados_Vigencias essv With(NoLock)
		On			essv.cnsctvo_cdgo_estdo_hmlgdo_sprs = p.cnsctvo_cdgo_estdo 
		Where		p.cnsctvo_cdgo_estdo_srvco_slctdo  Is Null


		-- Temporal mientras se define la impersion en mega de prestaciones migradas.
		Update		#tempPrestaciones
		Set			cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo13
		Where		cnsctvo_cdgo_estdo = @cnsctvo_cdgo_estdo8
		And			cnsctvo_cdgo_estdo_srvco_slctdo != @cnsctvo_cdgo_estdo_srvco_slctdo16
		
		-- 
		Insert Into #tempConceptosPrestaciones
		(
					cnsctvo_slctd_atrzcn_srvco,				nuam,								cnsctvo_ops,								
					cnsctvo_prcdmnto_insmo_slctdo,			cnsctvo_mdcmnto_slctdo,				cnsctvo_prstcn,							
					vlr_lqdcn,								cnsctvo_cdgo_cncpto_gsto,			cdgo_intrno_prstdr_atrzdo,				
					cnsctvo_cdgo_grpo_imprsn,				nmro_rdccn_cm,						cnsctvo_cdgo_ofcna_cm,					
					vlr_cncpto_cm,							gnrdo,								fcha_utlzcn_dsde,						
					fcha_utlzcn_hsta,						nmro_unco_ops,						cnsctvo_cdgo_estdo_sprs,				
					fcha_crcn,								usro_crcn,							fcha_ultma_mdfccn,						
					usro_ultma_mdfccn
		)
		Select		
					p.cnsctvo_slctd_atrzcn_srvco,			co.nuam,								co.cnsctvo_ops,
					p.cnsctvo_prcdmnto_insmo_slctdo,		p.cnsctvo_mdcmnto_slctdo,				co.cnsctvo_prstcn,
					co.vlr_rfrnca,							co.cnsctvo_cdgo_cncpto_gsto,			co.cdgo_prstdr,
					co.cnsctvo_cdgo_grpo_imprsn,			co.nmro_rdccn_cm,						co.cnsctvo_cdgo_ofcna_cm,
					co.vlr_cncpto_cm,						co.gnrdo,								co.fcha_utlzcn_dsde,
					co.fcha_utlzcn_hsta,					co.nmro_unco_ops,						co.cnsctvo_cdgo_estdo,
					co.fcha_ultma_mdfccn,					co.usro_ultma_mdfccn,					co.fcha_ultma_mdfccn,
					co.usro_ultma_mdfccn					
		From		#tempPrestaciones p
		Inner Join	bdSisalud.dbo.tbConceptosOps co With(NoLock)
		On			co.nuam = p.nuam
		And			co.cnsctvo_cdgo_ofcna = p.cnsctvo_cdgo_ofcna
		And			co.cnsctvo_prstcn = p.cnsctvo_cdgo_srvco_slctdo
		Group By	p.cnsctvo_slctd_atrzcn_srvco,			co.nuam,								co.cnsctvo_ops,
					p.cnsctvo_prcdmnto_insmo_slctdo,		p.cnsctvo_mdcmnto_slctdo,				co.cnsctvo_prstcn,
					co.vlr_rfrnca,							co.cnsctvo_cdgo_cncpto_gsto,			co.cdgo_prstdr,
					co.cnsctvo_cdgo_grpo_imprsn,			co.nmro_rdccn_cm,						co.cnsctvo_cdgo_ofcna_cm,
					co.vlr_cncpto_cm,						co.gnrdo,								co.fcha_utlzcn_dsde,
					co.fcha_utlzcn_hsta,					co.nmro_unco_ops,						co.cnsctvo_cdgo_estdo,
					co.fcha_ultma_mdfccn,					co.usro_ultma_mdfccn,					co.fcha_ultma_mdfccn,
					co.usro_ultma_mdfccn		
		
		Delete		cp	
		From		#tempConceptosPrestaciones cp
		Left Join	bdCNA.gsa.tbASConceptosServicioSolicitado css
		On			css.cnsctvo_prcdmnto_insmo_slctdo = cp.cnsctvo_prcdmnto_insmo_slctdo
		And			css.nmro_unco_ops = cp.nmro_unco_ops
		Where		css.nmro_unco_ops Is Null

		Delete		cp	
		From		#tempConceptosPrestaciones cp
		Left Join	bdCNA.gsa.tbASConceptosServicioSolicitado css
		On			css.cnsctvo_mdcmnto_slctdo = cp.cnsctvo_mdcmnto_slctdo
		And			css.nmro_unco_ops = cp.nmro_unco_ops
		Where		css.nmro_unco_ops Is Null

		-- 
		Insert Into		#tempServiciosValor
		(
					nuam,	cnsctvo_slctd_atrzcn_srvco,		cnsctvo_prstcn,		vlr_lqdcn_srvco	
		)
		Select		nuam,	cnsctvo_slctd_atrzcn_srvco,		cnsctvo_prstcn,		Sum(vlr_lqdcn)
		From		#tempConceptosPrestaciones	
		Group By	nuam,	cnsctvo_slctd_atrzcn_srvco,		cnsctvo_prstcn

		-- 
		Update		p
		Set			fcha_imprsn = co.fcha_imprsn
		From		#tempPrestaciones p
		Inner Join	bdSisalud.dbo.tbConceptosOps co
		On			p.nuam = co.nuam
		And			p.cnsctvo_cdgo_ofcna = co.cnsctvo_cdgo_ofcna

		-- 
		Update		p
		Set			vlr_lqdcn_srvco = cp.vlr_lqdcn_srvco
		From		#tempPrestaciones p
		Inner Join	#tempServiciosValor cp
		On			cp.nuam = p.nuam
		And			cp.cnsctvo_slctd_atrzcn_srvco = p.cnsctvo_slctd_atrzcn_srvco
		And			cp.cnsctvo_prstcn = p.cnsctvo_cdgo_srvco_slctdo
		
		-- 
		Update		a
		Set			ds_vldz = b. ds_vldz
		FROM		#tempConceptosPrestaciones a 
		inner join	bdSisalud.dbo.tbConceptosGasto_Vigencias b 
		on			a.cnsctvo_cdgo_cncpto_gsto = b.cnsctvo_cdgo_cncpto_gsto

		-- 
		Update		cp
		Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = essv.cnsctvo_cdgo_estdo_srvco_slctdo
		From		#tempConceptosPrestaciones cp
		Inner Join	bdCNA.prm.tbAsEstadosServiciosSolicitados_Vigencias essv With(NoLock)
		On			essv.cnsctvo_cdgo_estdo_hmlgdo_sprs = cp.cnsctvo_cdgo_estdo_sprs

		-- 
		Update		cp
		Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = p.cnsctvo_cdgo_estdo_srvco_slctdo
		From		#tempConceptosPrestaciones cp
		Inner Join	#tempPrestaciones p
		On			p.cnsctvo_prcdmnto_insmo_slctdo = cp.cnsctvo_prcdmnto_insmo_slctdo
		Where		p.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo16

		-- 
		Update		cp
		Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = p.cnsctvo_cdgo_estdo_srvco_slctdo
		From		#tempConceptosPrestaciones cp
		Inner Join	#tempPrestaciones p
		On			p.cnsctvo_mdcmnto_slctdo = cp.cnsctvo_mdcmnto_slctdo
		Where		p.cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo16

		-- Temporal mientras se define la impresion de prestaciones migradas en mega.
		Update		#tempConceptosPrestaciones
		Set			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo13
		Where		cnsctvo_cdgo_estdo_sprs = @cnsctvo_cdgo_estdo8
		And			cnsctvo_cdgo_estdo_cncpto_srvco_slctdo != @cnsctvo_cdgo_estdo_srvco_slctdo16

		-- 
		Update		#tempConceptosPrestaciones
		Set			cnsctvo_cdgo_ofcna_cm = @vlr_cro
		Where		cnsctvo_cdgo_ofcna_cm Is Null

		-- 
		Update		#tempConceptosPrestaciones
		Set			nmro_rdccn_cm = @vlr_cro
		Where		nmro_rdccn_cm Is Null

		-- 
		Update	#tempConceptosPrestaciones
		Set		nmro_unco_ops = @vlr_cro
		Where	nmro_unco_ops Is Null	

		-- 
		Drop Table #tempEstados;
		Drop Table #tempServiciosValor;

End


