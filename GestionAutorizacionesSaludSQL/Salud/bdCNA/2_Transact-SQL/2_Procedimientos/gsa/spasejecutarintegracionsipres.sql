USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEjecutarIntegracionSipres]    Script Date: 21/07/2017 03:31:22 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG       : spASEjecutarIntegracionSipres
* Desarrollado por   : <\A Jhon Olarte     A\>    
* Descripcion        : <\D Procedimiento orquestador que permite realizar la integración de MEGA  D\>    
					   <\D con la base de datos de SIPRES											D\>    
* Observaciones      : <\O      O\>    
* Parametros         : <\P @fcha_inco: Fecha de incicio de busqueda de información.				P\>    
* Variables          : <\V       V\>    
* Fecha Creacion     : <\FC 02/05/2016  FC\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM Jonathan Chamizo Quina  AM\>    
* Descripcion        : <\D Se adiciona script para validar si existe el SP en la BD. En caso contrario
					 :     crea con la instrucción AS Select 1											D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM 2016-09-20   FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento adicionando los campos de estado en la tabla del concepto
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-19  FM\>    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Victor Hugo Gil Ramos AM\>    
* Descripcion        : <\D Se modifica el procedimiento para adicionar en la tabla temporal de solicitudes el estado de la atencion, con el estado del concepto o 
                           el estado de la solicitud cuando no se tengan conceptos de gasto asociados
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-10-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Juan Carlos Vásquez García - sisjvg01 AM\>    
* Descripcion        : <\D Se modifica el procedimiento para agregar el atributo cnsctvo_cdgo_tpo_orgn_slctd - para el manejo de la nueva funcionalidad
							de Envio de OPS Virtual correspondiente a solicitudes creadas por proceso automático de programación de entrega 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2016-12-20  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agregan campos en la tabla temporal #tmpinformacionconceptos 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-18  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agregan campos en la tabla temporal #tmpinformacionsolicitud 
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-27  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D Se agrega tabla temporal #tempInformacionEmpresasxAfiliado para migrar la informacion de la empresa
						   asociada a la solicitud
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-01-30  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
							Se agrega log para la ejecucion de la migracion de las cuotas de recuperacion y las marcas
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-04-04  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
							Se agrega log y ejecucion de sp para migracion de medicos no adscritos.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-04-05  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
							Se agrega campo cnsctvo_det_prgrmcn_fcha a la tabla temporal #tmpinformacionservicios
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-04-10  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
							Se modifica el registro del log de procesos, se agregan variables para determinar 
							el paso en el cual falla el proceso, y se agrega en el catch el registro del log 
							para la ejecucion fallida.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-05-09  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez - qvisionclr AM\>    
* Descripcion        : <\D 
							Se agregan pasos de proceso independientes para migracion de cuotas, medicos, y marcas.
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-05-09  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por     : <\AM  Ing. Carlos Andres Lopez Ramirez AM\>    
* Descripcion        : <\D 
							Se agregan campos para la informacion de hospitalizacion a la tabla temporal #tmpinformacionsolicitud
					   D\> 
* Nuevas Variables   : <\VM   VM\>    
* Fecha Modificacion : <\FM  2017-06-27  FM\>    
*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*
	
	Exec bdcna.gsa.spasejecutarintegracionsipres '20170515'

*/

ALTER Procedure [gsa].[spASEjecutarIntegracionSipres]
@fcha_inco	Datetime

AS

Begin
	Set NoCount On

	DECLARE @cnsctvo_prcso Int,
          @cnsctvo_cdgo_tpo_prcso Int,
          @prcso_crga_mga Int,
          @prcso_crga_otrs Int,
          @prcso_crga_sprs Int,
		  @prcso_mgra_cts Int,
		  @prcso_mgra_mrcs	Int,
		  @prcso_mgra_mdcs	Int,
          @cnsctvo_lg Int,
          @usro udtusuario,
          @mensajeerror VARCHAR(2000),
		  @cnsctvo_prcso_exto Int,
		  @cnsctvo_prcso_err Int,
		  @paso1				Int,
		  @paso2				Int,
		  @paso3				Int,
		  @paso4				Int,
		  @paso5				Int,
		  @paso6				Int,
		  @errr					Int,
		  @cdgo_exto			Int, 
		  @vlr_si				udtLogico;

	CREATE TABLE  #tmpinformacionsolicitud
	(
		id                           udtConsecutivo IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco   udtConsecutivo,
		cnsctvo_cdgo_ofcna           udtConsecutivo,
		nuam                         Numeric(18, 0),
		fcha_slctd                   Datetime      ,
		cdgo_intrno                  Char(8)       ,
		nmro_unco_idntfccn_prstdr    Int           ,
		nmro_unco_idntfccn_mdco      Int           ,
		cnsctvo_cdgo_cntngnca        udtConsecutivo,
		fcha_estmda_entrga           Datetime      ,
		cnsctvo_cdgo_clse_atncn      udtConsecutivo,
		cnsctvo_cdgo_frma_atncn      udtConsecutivo,
		obsrvcn_adcnl                Varchar(250)  ,
		fcha_crcn                    Datetime      ,
		usro_crcn                    udtusuario    ,
		usro_ultma_mdfccn            udtusuario    , 
		fcha_ultma_mdfccn            Datetime      ,
		cnsctvo_cdgo_dgnstco         udtConsecutivo,
		cnsctvo_cdgo_ofcna_atrzcn    udtConsecutivo,
		cnsctvo_cdgo_rcbro           udtConsecutivo,
		cnsctvo_cdgo_tpo_ubccn_pcnte udtConsecutivo,
		nmro_slctd_prvdr             Int           ,
		cnsctvo_cdgo_srvco_hsptlzcn  udtConsecutivo,
		cnsctvo_cdgo_mrbldd          udtConsecutivo,
		cnsctvo_cdgo_tpo_atncn       udtConsecutivo,
		cnsctvo_cdgo_estdo_mega      udtConsecutivo,
		cnsctvo_cdgo_estdo           udtConsecutivo,
		hra_dgta                     Char(10)      ,
		nmro_atncn                   Int           ,
		hra_slctd_orgn               Char(8)       ,
		cnsctvo_cdgo_mdo_cntcto      udtConsecutivo,
		fcha_imprsn                  Datetime      ,
		tpo_prstcn                   Char(1) DEFAULT 'N',
		cnsctvo_cdgo_grpo_entrga     udtConsecutivo,
		nmro_vsts                    Int           ,
		fcha_ingrso_hsptlzcn		 DateTime	   ,
		fcha_egrso_hsptlzcn			 DateTime	   ,
		ds_estnca                    Int Default 0 ,
		crte_cnta					 udtConsecutivo Default 0,
		cnsctvo_cdgo_clse_hbtcn		 udtConsecutivo,
		cnsctvo_cdgo_estdo_atncn     udtConsecutivo,
		cnsctvo_atncn_ops			 UdtConsecutivo,
		cnsctvo_cdgo_tpo_orgn_slctd  UdtConsecutivo,
		cnsctvo_cdgo_agrpdr_prstcn	 UdtConsecutivo
	)

	Create Table #tmpinformacionafiliado 
	(
		id Int IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco Int,
		nmro_unco_idntfccn_afldo Int,
		cnsctvo_cdgo_pln Int,
		cnsctvo_cdgo_sde_ips_prmra Int,
		cnsctvo_cdgo_tpo_idntfccn_afldo Int,
		nmro_idntfccn_afldo udtnumeroidentificacion,
		prmr_aplldo_afldo udtapellido Default '',
		sgndo_aplldo_afldo udtapellido Default '',
		prmr_nmbre_afldo udtnombre Default '',
		sgndo_nmbre_afldo udtnombre Default '',
		cnsctvo_bnfcro_cntrto Int,
		nmro_cntrto CHAR(15),
		cnsctvo_cdgo_tpo_cntrto Int,
		cnsctvo_cdgo_sxo Int,
		cnsctvo_cdgo_tpo_undd Int,
		edd_afldo_ans Int,
		smns_ctzds Int,
		cnsctvo_cdgo_tpo_vnclcn_afldo Int,
		cnsctvo_cdgo_rngo_slrl Int,
		fcha_ultma_mdfccn DATETIME,
		cdgo_ips_prmra udtcodigoips,
		fcha_ncmnto_afldo DATETIME,
		drccn_afldo udtdireccion,
		cnsctvo_cdgo_cdd_rsdnca_afldo Int,
		tlfno_afldo udttelefono,
		tlfno_cllr_afldo udttelefono,
		crro_elctrnco_afldo udtemail,
		cnsctvo_cdgo_ofcna udtcodigoips,
		cnsctvo_cdgo_prntscs Int Default 0,
		cnsctvo_cdgo_tpo_cbrnza Int Default 0,
		fcha_fn_vgnca DATETIME,
		fcha_inco_vgnca DATETIME,
		nmro_unco_idntfccn_aprtnt Int,
		rzn_scl CHAR(35) Default ''
	)

	Create Table #tempInformacionEmpresasxAfiliado
	(
		cnsctvo_cdgo_tpo_idntfccn_empldr			UdtConsecutivo,
		nmro_unco_idntfccn_empldr					Int,
		nmro_idntfccn_empldr						udtNumeroIdentificacionLargo,
		nmbre_empldr								Varchar(200),
		cnsctvo_scrsl								UdtConsecutivo,
		cnsctvo_cdgo_ofcna							UdtConsecutivo,
		inco_vgnca_cbrnza							Date,
		fn_vgnca_cbrnza								Date,
		slro_bse									Int,
		cnsctvo_cdgo_tpo_cbrnza						UdtConsecutivo,
		cnsctvo_cdgo_entdd_arp						UdtConsecutivo,
		fcha_ultma_mdfccn							Date,
		usro_ultma_mdfccn							UdtUsuario,
		nuam										Numeric(18, 0),		
		cnsctvo_infrmcn_afldo_slctd_atrzcn_srvco	UdtConsecutivo					
	)

	Create Table #tmpinformacionservicios 
	(
		id Int IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco Int,
		cnsctvo_srvco_slctdo Int,
		fcha_imprsn DATETIME,
		cnsctvo_cdgo_srvco_slctdo Int,
		cntdd_slctda Int,
		cnsctvo_cdgo_ltrldd Int Default 0,
		fcha_ultma_mdfccn DATETIME,
		cnsctvo_cdgo_va_accso Int Default 0,
		usro_ultma_mdfccn udtusuario,
		cnsctvo_cdgo_prsntcn Int,
		prstcn_cptda udtlogico,
		cnsctvo_cdgo_dt_prgrmcn_fcha_evnto Int,
		cnsctvo_cdgo_tpo_srvco Int,
		cnsctvo_mdcmnto_slctdo Int,
		cnsctvo_prcdmnto_slctdo Int,
		vlr_rfrnca DECIMAL(10, 0) Default 0,
		cpgo_lqudcn DECIMAL(8, 0) Default 0,
		cnsctvo_cdgo_estdo_mega Int,
		cnsctvo_cdgo_estdo Int,
		cnsctvo_cdgo_ntfccn Int,
		cnsctvo_cdgo_ofcna Int,
		nuam Int,
		nmro_acta Int,
		cnsctvo_prgrmcn_prstcn Int,
		cnsctvo_prgrmcn_fcha_evnto Int,
		cnsctvo_det_prgrmcn_fcha	Int,
		rqre_prvdr CHAR(1),
		cnsctvo_cdgo_agrpdr_prstcn Int,
		cnsctvo_cdgo_rcbro Int,
		id_agrpdr Int
	)

	Create Table  #tmpinformacionconceptos
	(
		id                         udtConsecutivo IDENTITY (1, 1),
		cnsctvo_slctd_atrzcn_srvco udtConsecutivo,
		cnsctvo_srvco_slctdo       udtConsecutivo,
		cnsctvo_ops                Int,
		cnsctvo_cdgo_cncpto_gsto   udtConsecutivo,
		fcha_ultma_mdfccn          Datetime,
		cnsctvo_cdgo_grpo_imprsn   udtConsecutivo,
		nmro_rdccn_cm              Int,
		cnsctvo_cdgo_ofcna_cm      udtConsecutivo,
		nmro_unco_ops              Int,
		vlr_cncpto_cm              Float,
		gnrdo                      udtlogico,
		fcha_utlzcn_dsde           Datetime,
		fcha_utlzcn_hsta           Datetime,
		cnsctvo_cdgo_estdo         udtConsecutivo,
		cnsctvo_cdgo_estdo_mega    udtConsecutivo,
		vlr_lqdcn                  Decimal(18,0) Default 0,
		nmro_unco_idntfccn_prstdr  Int,									-- qvisionclr 2017/01/18
		cdgo_intrno                Char(8)
	 )

	Set @prcso_crga_mga = 101
	Set @prcso_crga_otrs = 102
	Set @prcso_crga_sprs = 103
	Set @prcso_mgra_cts = 148;
	Set @prcso_mgra_mrcs = 149;
	Set @prcso_mgra_mdcs = 150;
	Set @cnsctvo_prcso_exto = 2
	Set @cnsctvo_prcso_err= 3
	Set @cnsctvo_cdgo_tpo_prcso = 30 --Tipo de proceso para integración Sipres
	Set @usro = SUBSTRING(system_user, CHARINDEX('\', system_user) + 1, LEN(system_user))
	Set @paso1 = -2
	Set @paso2 = -2
	Set @paso3 = -2
	Set @paso4 = -2
	Set @paso5 = -2
	Set @paso6 = -2
	Set @errr = -1;
	Set @cdgo_exto = 0;
	Set @vlr_si = 'S';

	Begin Try	    
		
		--PASO 0  Crear Proceso
		Exec bdprocesossalud.dbo.spproregistraproceso @cnsctvo_cdgo_tpo_prcso,
													@usro,
													@cnsctvo_prcso OUTPUT
		--PASO 1  Cargar información MEGA
		Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 1. Inicia proceso de carga de información MEGA.',
																   @prcso_crga_mga,
																   @cnsctvo_lg OUTPUT
			Exec gsa.spasobtenerinformacionintegracionsipresmega @fcha_inco
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
						
			Set @paso1 = @cdgo_exto;
      End Try
      Begin Catch
				Select
				@mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
				'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
				'Message:', ERROR_MESSAGE(), CHAR(13),
				'Procedure:', ERROR_PROCEDURE())
				Set @paso1 = @errr;				
				Throw
      End Catch

      --PASO 2  Cargar información otros sistemas
      Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 2. Inicia proceso de carga de información Otros Sistemas.',
																   @prcso_crga_otrs,
																   @cnsctvo_lg OUTPUT
			Exec gsa.spasobtenerinformacionintegracionsipresotros
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		
			Set @paso2 = @cdgo_exto;

      End Try
      Begin Catch
			RollBack
			Select
			  @mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
			  'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
			  'Message:', ERROR_MESSAGE(), CHAR(13),
			  'Procedure:', ERROR_PROCEDURE())
			Set @paso2 = @errr;
			
			Throw
      End Catch

       --PASO 3  Carga de información a Sipres
	  Begin Transaction migracion_sipres
      Begin Try
		
        Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
                                                               @cnsctvo_cdgo_tpo_prcso,
                                                               'Paso 3. Inicia proceso de carga al sistema sipres.',
                                                               @prcso_crga_sprs,
                                                               @cnsctvo_lg OUTPUT
		Exec gsa.spascargarinformacionsipres @vlr_si;		
		Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		Set @paso3 = @cdgo_exto;

      End Try
      Begin Catch
		
			Select
			  @mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
			  'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
			  'Message:', ERROR_MESSAGE(), CHAR(13),
			  'Procedure:', ERROR_PROCEDURE());
			Set @paso3 = @errr;
			
			Throw
      End Catch

	  Begin Try
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
                                                               @cnsctvo_cdgo_tpo_prcso,
                                                               'Paso 4. Migración cuotas de recuperación.',
                                                               @prcso_mgra_cts,
                                                               @cnsctvo_lg OUTPUT
		-- Migra las cuotas de recuperacion
		Exec bdCNA.gsa.spAsMigracionCuotasDeRecuperacionSipres
		Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		Set @paso4 = @cdgo_exto;

      End Try
      Begin Catch
		
	    Select
          @mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
          'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
          'Message:', ERROR_MESSAGE(), CHAR(13),
          'Procedure:', ERROR_PROCEDURE());
			Set @paso4 = @errr;
			
        Throw
      End Catch
	  
	  Begin Try
				Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																	   @cnsctvo_cdgo_tpo_prcso,
																	   'Paso 5. Migración de marcas.',
																	   @prcso_mgra_mrcs,
																	   @cnsctvo_lg OUTPUT
			-- Migra las marcas de los servicios solicitados
			Exec BdCna.gsa.spASMigrarMarcasSipres  
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
			Set @paso5 = @cdgo_exto;

      End Try
      Begin Catch
			
			Select
			  @mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
			  'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
			  'Message:', ERROR_MESSAGE(), CHAR(13),
			  'Procedure:', ERROR_PROCEDURE());
			Set @paso5 = @errr;
			
			Throw
      End Catch

	  Begin Try
			Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
																   @cnsctvo_cdgo_tpo_prcso,
																   'Paso 6. Migracion de medicos no adscritos.',
																   @prcso_mgra_mdcs,
																   @cnsctvo_lg OUTPUT
			-- Migra informacion del medico
			Exec bdCNA.gsa.spAsMigracionMedicoGenericoSipres
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
			Set @paso6 = @cdgo_exto;
      End Try
      Begin Catch
			
			Select
			  @mensajeerror = concat('Number:', CAST(ERROR_NUMBER() AS CHAR), CHAR(13),
			  'Line:', CAST(ERROR_LINE() AS CHAR), CHAR(13),
			  'Message:', ERROR_MESSAGE(), CHAR(13),
			  'Procedure:', ERROR_PROCEDURE());
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																	  @mensajeerror;
			Set @paso6 = @errr;
			Throw
      End Catch

		-- Registramos el fin del proceso Automático log de procesos
		Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@cnsctvo_cdgo_tpo_prcso,@cnsctvo_prcso_exto		
	
		Commit Transaction migracion_sipres;
		
  End Try
  Begin Catch	
		RollBack Transaction migracion_sipres;	

		RAISERROR (@mensajeerror, 16, 2) WITH SETERROR
		-- Registramos el fin del proceso Automático log de procesos con error.
		Exec bdProcesosSalud.dbo.spPROActualizaEstadoProceso @cnsctvo_prcso,@cnsctvo_cdgo_tpo_prcso,@cnsctvo_prcso_err	
		--Envío de notificación
		Exec bdProcesosSalud.dbo.spPROEnviarNotificacion @cnsctvo_prcso, @cnsctvo_cdgo_tpo_prcso, @cnsctvo_prcso_err

		-- Paso 1
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																'Paso 1. Inicia proceso de carga de información MEGA.',
																@prcso_crga_mga,
																@cnsctvo_lg OUTPUT
		if(@paso1 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso1 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada	@cnsctvo_lg,
																				@mensajeerror;
		End

		-- Paso 2
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																'Paso 2. Inicia proceso de carga de información Otros Sistemas.',
																@prcso_crga_otrs,
																@cnsctvo_lg OUTPUT
		if(@paso2 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso2 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada	@cnsctvo_lg,
																				@mensajeerror;
		End

		-- Paso 3
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
                                                               @cnsctvo_cdgo_tpo_prcso,
                                                               'Paso 3. Inicia proceso de carga al sistema sipres.',
                                                               @prcso_crga_sprs,
                                                               @cnsctvo_lg OUTPUT
		if(@paso3 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso3 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada	@cnsctvo_lg,
																				@mensajeerror;
		End

		-- Paso 4
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso @cnsctvo_prcso,
                                                               @cnsctvo_cdgo_tpo_prcso,
                                                              'Paso 4. Migración cuotas de recuperación.',
                                                               @prcso_mgra_cts,
                                                               @cnsctvo_lg OUTPUT
		if(@paso4 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso4 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																		  @mensajeerror;
		End

		-- Paso 5
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																'Paso 5. Migración de marcas.',
																@prcso_mgra_mrcs,
																@cnsctvo_lg OUTPUT
		If(@paso5 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso5 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																		  @mensajeerror;
		End

		-- Paso 6
		Exec bdprocesossalud.dbo.spproregistralogeventoproceso	@cnsctvo_prcso,
																@cnsctvo_cdgo_tpo_prcso,
																'Paso 6. Migracion de medicos no adscritos.',
																@prcso_mgra_mdcs,
																@cnsctvo_lg OUTPUT
		If(@paso6 = @cdgo_exto)
		Begin
			Exec bdprocesossalud.dbo.spproactualizaestadologeventoprocesoexitoso @cnsctvo_lg
		End
		Else
		If(@paso6 = @errr)
		Begin
			Exec bdprocesossalud.dbo.spproregistrarinconsistencianocontrolada @cnsctvo_lg,
																		  @mensajeerror;
		End
	
		
	
  End Catch
End

