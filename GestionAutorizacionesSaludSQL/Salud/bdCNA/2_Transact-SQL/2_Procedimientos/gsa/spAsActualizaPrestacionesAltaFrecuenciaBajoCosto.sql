USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spAsActualizaPrestacionesAltaFrecuenciaBajoCosto]    Script Date: 11/01/2017 4:04:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spAsActualizaPrestacionesAltaFrecuenciaBajoCosto') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spAsActualizaPrestacionesAltaFrecuenciaBajoCosto AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spAsActualizaPrestacionesAltaFrecuenciaBajoCosto
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS						 A\>
* Descripcion			: <\D 
								Cuando el servicio de direccionamiento no pueda direccionar el CUPS 
								a un prestador y el estado de la prestación es “aprobada” el sistema 
								debe cambiar el estado automáticamente a “En auditoría” para   enviar 
								el caso al BPM del auditor													
                          D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2017/02/08 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spAsActualizaPrestacionesAltaFrecuenciaBajoCosto] 394114, 'user1', null, null
AlTER PROCEDURE [gsa].[spAsActualizaPrestacionesAltaFrecuenciaBajoCosto] 
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@usuario_sistema				udtUsuario,
@cdgo_rsltdo					udtConsecutivo  output,
@mnsje_rsltdo					varchar(2000)	output

AS

BEGIN
	SET NOCOUNT ON;
	Declare @codigo_ok									int,
			@codigo_error								int,
			@cnsctvo_cdgo_estdo_srvco_slctdo			udtConsecutivo,
			@mensaje_ok									udtDescripcion,
			@mensaje_error								varchar(2000),
			@fecha_actual								datetime,
			@valor_uno									int,
			@valor_cero									int;;
	
		Create table #tmpPrestaciones(
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo,
			indcdr_drccnmnto				int default 0,
			indcdr_fcha_entrga				int default 0
		)

	Set @cnsctvo_cdgo_estdo_srvco_slctdo = 5  -- Estado EN AUDITORIA
	Set @codigo_error					 = -1  -- Error SP
	Set @codigo_ok						 = 0  -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente'
	Set @fecha_actual					 = getDate()
	Set @valor_uno						 = 1
	Set @valor_cero						 = 0

	BEGIN TRY
			
			-- Obtener todas las prestaciones marcadas de acceso directo para la solicitud
			Insert into #tmpPrestaciones(
				cnsctvo_slctd_atrzcn_srvco,
				cnsctvo_srvco_slctdo
			)
			Select 
				@cnsctvo_slctd_atrzcn_srvco,
				ss.cnsctvo_srvco_slctdo
			From BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
			Where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			
			-- Se verifican cuales prestaciones no presentan direccionamiento
			Update ss
			Set	indcdr_drccnmnto = @valor_uno
			From #tmpPrestaciones ss
			Inner Join BDCna.gsa.tbASResultadoDireccionamiento rd  WITH (NOLOCK)
			On rd.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

			-- Se verifican cuales prestaciones no presentan fecha entrega
			Update ss
			Set	indcdr_fcha_entrga = @valor_uno
			From #tmpPrestaciones ss
			Inner Join BDCna.gsa.tbASResultadoFechaEntrega rf WITH (NOLOCK)
			On rf.cnsctvo_srvco_slctdo = ss.cnsctvo_srvco_slctdo

			--Se actualizan los estados a Auditoria de las prestaciones que no
			--tienen fecha entrega y/o direcciomiento 
			Update ss
			Set cnsctvo_cdgo_estdo_srvco_slctdo = @cnsctvo_cdgo_estdo_srvco_slctdo,
				usro_ultma_mdfccn				= @usuario_sistema,
				fcha_ultma_mdfccn				= @fecha_actual
			From BDCna.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
			inner Join #tmpPrestaciones t
			On ss.cnsctvo_srvco_slctdo  = t.cnsctvo_srvco_slctdo
			where t.indcdr_drccnmnto	= @valor_cero
			Or	  t.indcdr_fcha_entrga	= @valor_cero

			--Se determina en la respuesta si al menos encontró una prestación sin marcada
			Set @cdgo_rsltdo = @codigo_ok
			If exists(Select 1 from #tmpPrestaciones t
					  Where t.indcdr_drccnmnto	 = @valor_cero
					  Or	t.indcdr_fcha_entrga = @valor_cero)
			Begin
				Set @cdgo_rsltdo = @valor_uno				
			End
			Set @mnsje_rsltdo	 = @mensaje_ok

			
	END TRY
	BEGIN CATCH
		SET @mensaje_error = Concat(	    'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
											'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
											'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
											'Procedure:' , ERROR_PROCEDURE()
										);

		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH
	
	Drop table #tmpPrestaciones
END
