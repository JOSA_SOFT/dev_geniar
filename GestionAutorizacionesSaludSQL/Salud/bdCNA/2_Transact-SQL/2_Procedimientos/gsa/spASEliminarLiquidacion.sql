USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASEliminarLiquidacion]    Script Date: 12/09/2016 11:50:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG     		: spASEliminarLiquidacion
* Desarrollado por		: <\A Jonathan - SETI SAS			  					A\>
* Descripcion			: <\D Orquestador para ejecutar el proceso encargado
							  de eliminar la liquidación    					D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 01/06/2016										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASEliminarLiquidacion]
	@usr_lgn			udtusuario
AS
BEGIN
  SET NOCOUNT ON; 

	DECLARE @mrca_rlqdcn					udtlogico	= 'S';	 

	INSERT INTO #tmpSolicitudAnulacionCuotasRecuperacion
	(
		cnsctvo_slctd_atrzcn_srvco,
		cnsctvo_cdgo_srvco_slctdo,
		nmro_unco_ops	
	)
	SELECT  CS.cnsctvo_slctd_atrzcn_srvco,
			CS.cnsctvo_cdgo_srvco_slctdo,
			CS.nmro_unco_ops
	FROM	#cnslddo_slctud CS
	WHERE   CS.mrca_rlqdcn = @mrca_rlqdcn AND CS.nmro_unco_ops IS NOT NULL;

	BEGIN
					
		--Devolver los copagos y cuotas de recuperación. 
		EXEC gsa.spASEjecutarAnulacionCuotasRecuperacion @usr_lgn;

		--Eliminar los conceptos de gasto asociados con las prestaciones a reliquidar
		EXEC gsa.spASEliminarConceptosGastoLiquidacion;

		--Se actualiza el estado de la prestación y el valor de la liquidación
		EXEC gsa.spASActualizarPrestacionLiquidacion @usr_lgn;

	END					

END;

GO
