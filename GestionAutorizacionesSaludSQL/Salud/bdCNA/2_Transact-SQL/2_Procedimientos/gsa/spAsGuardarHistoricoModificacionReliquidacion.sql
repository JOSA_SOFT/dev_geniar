USE [BDCna]
GO
/****** Object:  StoredProcedure [gsa].[spAsGuardarHistoricoModificacionReliquidacion]    Script Date: 10/02/2017 4:04:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (object_id('gsa.spAsGuardarHistoricoModificacionReliquidacion') IS NULL)
BEGIN
	EXEC sp_executesql N'CREATE PROCEDURE gsa.spAsGuardarHistoricoModificacionReliquidacion AS SELECT 1;'
END
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG 			: spAsGuardarHistoricoModificacionReliquidacion
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS						 A\>
* Descripcion			: <\D 
								El Sistema guarda por cada prestación modificada en el histórico de modificaciones la siguiente información:
								- Elemento: Solicitud.
								- Número de autorización: Número de radicado de la Solicitud. 
								- Prestación: Código de la prestación.
								- Tipo de modificación:  Reliquidación de Solicitud.
								- Valor Anterior: Valor de liquidación anterior del concepto de gasto.        
								- Valor Nuevo: N/A
								- Responsable Modificación: Usuario del sistema que realiza la modificación.        
								- Fecha Modificación: Fecha actual del sistema.  
								- Observaciones: Observaciones de la modificación.
								- Razón Modificación (causal): Causal por la que se modifica la fecha de entrega.													
                          D\>						 
* Observaciones			: <\O  O\>
* Parametros			: <\P  P\>
* Variables				: <\V  V\>
* Fecha Creacion		: <\FC 2017/02/10 FC\>
*-----------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION
*-----------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		 : <\AM	Jorge Rodriguez - SETI SAS		AM\>
* Descripcion			 : <\DM	Se modifica SP para guardar el origen de la modificación
								que sea mas claro para mostrarlo en la pantalla
								Historico Modificaciones		DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM 2017/03/29	FM\>
*------------------------------------------------------------------------------------------------------------------------------------------*/
--exec [gsa].[spAsGuardarHistoricoModificacionReliquidacion] 807, 56, 'Prueba de reliquidacion', 'user1', null, null
AlTER PROCEDURE [gsa].[spAsGuardarHistoricoModificacionReliquidacion] 
@cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
@cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
@obsrvcn						udtObservacion,
@usuario_sistema				udtUsuario,
@cdgo_rsltdo					udtConsecutivo  output,
@mnsje_rsltdo					varchar(2000)	output

AS

BEGIN
	SET NOCOUNT ON;
	Declare @codigo_ok									int,
			@codigo_error								int,
			@mensaje_ok									udtDescripcion,
			@mensaje_error								varchar(2000),
			@fecha_actual								datetime,
			@valor_cero									int,
			@ip											varchar(15),
			@ldOrgn_mdfccn								varchar(100),
			@elmnto_orgn_mdfccn							udtdescripcion,
			@dto_elmnto_orgn_mdfccn						udtdescripcion;;
	
		Create table #tmpPrestacionesReliquidar(
			cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
			cnsctvo_srvco_slctdo			udtConsecutivo,
			cnsctvo_prcdmnto_insmo_slctdo	udtConsecutivo,
			cnsctvo_mdcmnto_slctdo			udtConsecutivo,
			vlr_lqdcn						decimal(18, 0),
			cnsctvo_cdgo_estdo_srvco_slctdo udtConsecutivo
		)

		Create 
		Table #tmpTrazaModificacion (cnsctvo_slctd_atrzcn_srvco		udtConsecutivo,
									cnsctvo_srvco_slctdo			udtConsecutivo,
									cnsctvo_cncpto_prcdmnto_slctdo	udtConsecutivo,
									cnsctvo_cdgo_mtvo_csa			udtConsecutivo,
									fcha_mdfccn						datetime,
									orgn_mdfccn						varchar(100),
									vlr_antrr						varchar(20),
									vlr_nvo							varchar(20),
									usro_mdfccn						udtUsuario,
									obsrvcns						udtObservacion,
									nmro_ip							varchar(12),
									fcha_crcn						datetime,
									usro_crcn						udtUsuario,
									fcha_ultma_mdfccn				datetime,
									usro_ultma_mdfccn				udtUsuario,
									elmnto_orgn_mdfccn				udtDescripcion,
									dto_elmnto_orgn_mdfccn			udtDescripcion,
									dscrpcn_vlr_antrr				udtDescripcion,
									dscrpcn_vlr_nvo					udtDescripcion
									)

	Set @codigo_error					 = -1  -- Error SP
	Set @codigo_ok						 = 0   -- Codigo de mensaje Ok
	Set @mensaje_ok						 = 'Proceso ejecutado satisfactoriamente'
	Set @fecha_actual					 = getDate()
	Set @valor_cero						 = 0
	Set @ip								 = '127.0.0.1'
	Set @ldOrgn_mdfccn					 = 'Solicitud-Reliquidación'
	Set @ldOrgn_mdfccn					 = 'tbASConceptosServicioSolicitado.ReliquidacionSolicitud'
	Set @elmnto_orgn_mdfccn				= 'Solicitud'
	Set	@dto_elmnto_orgn_mdfccn			= 'Reliquidación'


	BEGIN TRY
			
			-- Obtener todas las prestaciones de la solicitud de servicios
			Insert into #tmpPrestacionesReliquidar(
				cnsctvo_slctd_atrzcn_srvco,
				cnsctvo_srvco_slctdo
			)
			Select 
				@cnsctvo_slctd_atrzcn_srvco,
				ss.cnsctvo_srvco_slctdo
			From BdCNA.gsa.tbASServiciosSolicitados ss WITH (NOLOCK)
			Where ss.cnsctvo_slctd_atrzcn_srvco = @cnsctvo_slctd_atrzcn_srvco
			
			-- Se obtiene el consecutivo del procedimiento asociado a la prestación
			 Update a 
			 Set    cnsctvo_prcdmnto_insmo_slctdo = pis.cnsctvo_prcdmnto_insmo_slctdo
			 From   #tmpPrestacionesReliquidar a
			 Inner Join  BDCna.gsa.tbASProcedimientosInsumosSolicitados pis WITH (NOLOCK)
			 On    pis.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			 -- Se obtiene el consecutivo del medicamento asociado a la prestación 
			 Update a 
			 Set    cnsctvo_mdcmnto_slctdo = ms.cnsctvo_mdcmnto_slctdo
			 From   #tmpPrestacionesReliquidar a
			 Inner Join  BDCna.gsa.tbASMedicamentosSolicitados ms WITH (NOLOCK)
			 On ms.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo

			 --Se obtiene el valor de la liquidación para cada una de las prestaciones de la tabla tbASConceptosServicioSolicitado
			 Update a
			 Set	vlr_lqdcn						= css.vlr_lqdcn,
					cnsctvo_cdgo_estdo_srvco_slctdo	= css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
			 From #tmpPrestacionesReliquidar a
			 Inner Join BDCna.gsa.tbASConceptosServicioSolicitado css WITH (NOLOCK)
			 On  css.cnsctvo_prcdmnto_insmo_slctdo = a.cnsctvo_prcdmnto_insmo_slctdo
			 And a.cnsctvo_prcdmnto_insmo_slctdo is not null

			 Update a
			 Set	vlr_lqdcn						= css.vlr_lqdcn,
					cnsctvo_cdgo_estdo_srvco_slctdo	= css.cnsctvo_cdgo_estdo_cncpto_srvco_slctdo
			 From #tmpPrestacionesReliquidar a
			 Inner Join BDCna.gsa.tbASConceptosServicioSolicitado css WITH (NOLOCK)
			 On  css.cnsctvo_mdcmnto_slctdo = a.cnsctvo_mdcmnto_slctdo
			 And a.cnsctvo_mdcmnto_slctdo is not null

			 --Se hace el llamado SP spASGuardarTrazaModificacion para que guarde la traza de la gestión
			Insert Into #tmpTrazaModificacion 
			(cnsctvo_slctd_atrzcn_srvco,	  cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
			 cnsctvo_cdgo_mtvo_csa,			  fcha_mdfccn,						orgn_mdfccn,					
			 vlr_antrr,						  vlr_nvo,							usro_mdfccn,					
			 obsrvcns,						  nmro_ip,							fcha_crcn,						
			 usro_crcn,						  fcha_ultma_mdfccn,				usro_ultma_mdfccn,
			 elmnto_orgn_mdfccn,			  dto_elmnto_orgn_mdfccn,			dscrpcn_vlr_antrr,
			 dscrpcn_vlr_nvo
			)
			Select
			 cnsctvo_slctd_atrzcn_srvco,	  cnsctvo_srvco_slctdo,				null,
			 @cnsctvo_cdgo_mtvo_csa,		  @fecha_actual,					@ldOrgn_mdfccn,				
			 vlr_lqdcn,						  @valor_cero,						@usuario_sistema,
			 @obsrvcn,						  @ip,								@fecha_actual,
			 @usuario_sistema,				  @fecha_actual,					@usuario_sistema,
			 @elmnto_orgn_mdfccn,		      @dto_elmnto_orgn_mdfccn,			vlr_lqdcn,
			 @valor_cero
			From #tmpPrestacionesReliquidar
			Where cnsctvo_prcdmnto_insmo_slctdo is not null	and vlr_lqdcn is not null


			Insert Into #tmpTrazaModificacion 
			(cnsctvo_slctd_atrzcn_srvco,	  cnsctvo_srvco_slctdo,				cnsctvo_cncpto_prcdmnto_slctdo,
			 cnsctvo_cdgo_mtvo_csa,			  fcha_mdfccn,						orgn_mdfccn,					
			 vlr_antrr,						  vlr_nvo,							usro_mdfccn,					
			 obsrvcns,						  nmro_ip,							fcha_crcn,						
			 usro_crcn,						  fcha_ultma_mdfccn,				usro_ultma_mdfccn,
			 elmnto_orgn_mdfccn,			  dto_elmnto_orgn_mdfccn,			dscrpcn_vlr_antrr,
			 dscrpcn_vlr_nvo
			)
			Select
			 cnsctvo_slctd_atrzcn_srvco,	  cnsctvo_srvco_slctdo,				cnsctvo_prcdmnto_insmo_slctdo,
			 @cnsctvo_cdgo_mtvo_csa,		  @fecha_actual,					@ldOrgn_mdfccn,				
			 vlr_lqdcn,						  @valor_cero,						@usuario_sistema,
			 @obsrvcn,						  @ip,								@fecha_actual,
			 @usuario_sistema,				  @fecha_actual,					@usuario_sistema,
			 @elmnto_orgn_mdfccn,		      @dto_elmnto_orgn_mdfccn,			vlr_lqdcn,
			 @valor_cero	
			From #tmpPrestacionesReliquidar
			Where cnsctvo_mdcmnto_slctdo is not null and vlr_lqdcn is not null	

			Exec BDCna.gsa.spASGuardarTrazaModificacion;

			Set @cdgo_rsltdo	 = @codigo_ok				
			Set @mnsje_rsltdo	 = @mensaje_ok

			
	END TRY
	BEGIN CATCH
		SET @mensaje_error = Concat(	    'Number:'    , CAST(ERROR_NUMBER() AS VARCHAR(20)) , CHAR(13) ,
											'Line:'      , CAST(ERROR_LINE() AS VARCHAR(10)) , CHAR(13) ,
											'Message:'   , ERROR_MESSAGE() , CHAR(13) ,
											'Procedure:' , ERROR_PROCEDURE()
										);

		SET @cdgo_rsltdo  =  @codigo_error;
		SET @mnsje_rsltdo =  @mensaje_error
	END CATCH

	Drop table #tmpPrestacionesReliquidar
	Drop table #tmpTrazaModificacion
END
