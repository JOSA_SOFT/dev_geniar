USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASPVencConsultaLegalAfiliado]    Script Date: 12/09/2016 11:50:45 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*----------------------------------------------------------------------------------
* Metodo o PRG 			: spCNAPVencConsultaLegalAfiliado
* Desarrollado por		: <\A Ing. Jorge Rodriguez - SETI SAS  					 A\>
* Descripcion			: <\D Consulta tutelas o desacatos asociados al  afiliado.D\>
						  <\D .													D\>
* Observaciones			: <\O  													O\>
* Parametros			: <\P \>
* Variables				: <\V  													V\>
* Fecha Creacion		: <\FC 2015/12/02 										FC\>
*
*---------------------------------------------------------------------------------  
* DATOS DE MODIFICACION
*---------------------------------------------------------------------------------
* Modificado Por		 : <\AM	AM\>
* Descripcion			 : <\DM	DM\>
* Nuevos Parametros	 	 : <\PM	PM\>
* Nuevas Variables		 : <\VM	VM\>
* Fecha Modificacion	 : <\FM	FM\>
*---------------------------------------------------------------------------------*/
ALTER PROCEDURE [gsa].[spASPVencConsultaLegalAfiliado]
	@cnsctvo_cdgo_tpo_idntfccn udtConsecutivo,
	@nmro_idntfccn varchar(17),
	@Nui_Afldo	UdtConsecutivo = null,
	@legal varchar(40) output
AS
BEGIN
SET NOCOUNT ON;
	BEGIN
		Declare	@clasifevento int = 8,
				@notificado   int = 14,
				@confirmado	  int = 15,
				@ms_desacato	  varchar(20) = 'DESACATO',
				@ms_tutela		  varchar(20) = 'TUTELA',
				@ms_normal		  varchar(20) = 'NORMAL';
		
		   /* Nota: Esta consulta devuelve si el acuerdo legal es TUTELA, DESACATO o NORMAL. Los casos
		   DERECHO DE PETICION y SANCION actualmente no se tiene forma de obtenerse de la BD */

		if @Nui_Afldo is null
		begin
			Set @Nui_Afldo = dbo.fnObtenerNumeroUnicoIdentificacion(@cnsctvo_cdgo_tpo_idntfccn, @nmro_idntfccn)
		end
	
		if @Nui_Afldo is not null
		begin
			If exists(Select  'x' desacato From  bdSisalud.dbo.tbafiliadosmarcados aa with(nolock)
								Inner join bdSisalud.dbo.tbtutela cc with(nolock) On   aa.cnsctvo_ntfccn    = cc.cnsctvo_ntfccn 
																				  And   aa.cnsctvo_cdgo_ofcna   = cc.cnsctvo_cdgo_ofcna
								Inner join bdSisalud.dbo.tbMNDesacato bb with(nolock) On   aa.cnsctvo_ntfccn    = bb.cnsctvo_ntfccn 
																				  And   aa.cnsctvo_cdgo_ofcna   = bb.cnsctvo_cdgo_ofcna
							 Where  aa.cnsctvo_cdgo_clsfccn_evnto = @clasifevento
							 And   aa.cnsctvo_cdgo_estdo_ntfccn in (@notificado,@confirmado) 
							 And   aa.nmro_unco_idntfccn = @Nui_Afldo )
			Begin
				Set @legal = @ms_desacato;
			End


			if @legal is null
			begin
				If Exists ( Select  'x' From  bdSisalud.dbo.tbafiliadosmarcados aa with(nolock)
									Inner join bdSisalud.dbo.tbtutela cc with(nolock) On   aa.cnsctvo_ntfccn    = cc.cnsctvo_ntfccn 
																					  And   aa.cnsctvo_cdgo_ofcna   = cc.cnsctvo_cdgo_ofcna
								  Where  aa.cnsctvo_cdgo_clsfccn_evnto = @clasifevento
								  And   aa.cnsctvo_cdgo_estdo_ntfccn in (@notificado,@confirmado) 
								  And aa.nmro_unco_idntfccn = @Nui_Afldo)
				begin
					Set @legal = @ms_tutela
				end
			end
		
		end


		if @legal is null
		begin
			Set @legal = @ms_normal
		end


	END
END

GO
