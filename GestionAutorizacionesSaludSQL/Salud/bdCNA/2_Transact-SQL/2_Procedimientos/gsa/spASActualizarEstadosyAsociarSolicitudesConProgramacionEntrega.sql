USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASActualizarEstadosyAsociarSolicitudesConProgramacionEntrega]    Script Date: 7/5/2017 9:13:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------------------------------------------------------
* Método o PRG		:		gsa.spASActualizarEstadosyAsociarSolicitudesConProgramacionEntrega							
* Desarrollado por	: <\A	Ing. Juan Carlos Vásquez García	 A\>	
* Descripción		: <\D	
                            Actualiza los Estados y asocia las solicitudes creadas por el proceso
							automático con programación	de entrega 								
					  D\>
* Observaciones		: <\O O\>	
* Parámetros		: <\P 	
                            #tbDetProgramacion = temporal contiene las programaciones de entrega 
							#Tempo_Solicitudes = temporal contiene las solicitudes creadas por programación entrega
							#Tempo_Prestaciones = temporal contiene las prestaciones con programación entrega	
					  P\>	
* Variables			: <\V	V\>	
* Fecha Creación	: <\FC	2016/03/31 FC\>
*---------------------------------------------------------------------------------------------------------------------------
* Modificado Por	: <\AM  Ing. Juan Carlos Vásquez García	AM\>      
* Descripción		: <\DM	
                            Por Ops Virtual se modifica procedimiento para incluir la actualización
							del consecutivo generico en la tabla tbASMedicoTratanteSolicitudAutorizacionServicios
							en caso de que el médico sea Generico	
					   DM\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2017-04-05 FM\>
*---------------------------------------------------------------------------------------------------------------------------
* Modificado Por	: <\AM  Ing. Victor Hugo Gil ramos	AM\>      
* Descripción		: <\DM	
                            Se realiza modificacion en el procedimiento para que actualice el consecutivo el recobro
							asociado en la programacion de entrega a la tabla de servicios de mega
					   DM\>
* Nuevos Parámetros : <\PM	PM\>      
* Nuevas Variables	: <\VM	VM\>      
* Fecha Modificación: <\FM  2017-07-05 FM\>
*---------------------------------------------------------------------------------------------------------------------------*/

ALTER PROCEDURE  [gsa].[spASActualizarEstadosyAsociarSolicitudesConProgramacionEntrega]

/*01*/ @usro_prcso	UdtUsuario = null

AS 

Begin

	SET NOCOUNT ON

	-- Declaramos variables del proceso
	Declare	@fechaactual						Datetime = getdate(),
			@lcUsuario							udtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User)),
			@cnsctvo_cdgo_estdo_slctd3			udtConsecutivo = 3, -- Estado '3-En Proceso'
			@cnsctvo_cdgo_estdo_srvco_slctdo7	udtConsecutivo = 7, -- Estado '7-Aprobado'
			@cnsctvo_cdgo_estds_entrga170		udtConsecutivo = 170,  -- Estado '170-Asociado OPS',
			@nmro_unco_idntfccn_mdco100961		udtConsecutivo = 100961, -- Consecutivo Médico Genérico3
			@cnsctvo_cdgo_clsfccn_evnto11		udtConsecutivo = 11, -- evento ctc
			@clse_prstdr						udtLogico = 'M',
			@estdo								udtLogico = 'A'

	If @usro_prcso is not null
	  Begin
		 Set @lcUsuario = @usro_prcso
	  End

	-- Actualizamos el estado del detalle programación fecha evento
	Update		p
	Set			cnsctvo_cdgo_estds_entrga = @cnsctvo_cdgo_estds_entrga170, -- Estado '170-Asociado OPS'
				fcha_ultma_mdfccn         = @fechaactual                 ,
				usro_mdfccn               = @lcUsuario
	From		BDSiSalud.dbo.tbDetProgramacionFechaEvento p With(NoLock)
	Inner Join	#tbDetProgramacion t
	On			t.cnsctvo_det_prgrmcn_fcha = p.cnsctvo_det_prgrmcn_fcha
	And			t.cnsctvo_prgrmcn_prstcn = p.cnsctvo_prgrmcn_prstcn
	And         t.cnsctvo_prgrmcn_fcha_evnto = p.cnsctvo_prgrmcn_fcha_evnto
	And			t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = p.cnsctvo_cdgo_det_prgrmcn_fcha_evnto

	-- Actualizamos el estado de la solicitud Creada

	Update		sol
	Set			cnsctvo_cdgo_estdo_slctd    = @cnsctvo_cdgo_estdo_slctd3   , /* Estado '3-En Proceso' */
				cnsctvo_cdgo_tpo_orgn_slctd = t.cnsctvo_cdgo_tpo_orgn_slctd, /* 2-Prog.Autom. prg. Entrega, 3-Proc.Autom.Ops Virtual*/
				fcha_ultma_mdfccn           = @fechaactual                 ,
				usro_ultma_mdfccn           = @lcUsuario
	From		bdCNA.gsa.tbASSolicitudesAutorizacionServicios sol With(NoLock)
	Inner Join	#Tempo_Solicitudes t
	On			t.nmro_slctd_atrzcn_ss = sol.nmro_slctd_atrzcn_ss
	Inner Join  #Idsolicitudes i
	on			i.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco

	-- Actualizamos la asociación de la programación de entrega y el estado de la solicitud creada
	Update		ss
	Set			cnsctvo_cdgo_dt_prgrmcn_fcha_evnto = t.cnsctvo_cdgo_dt_prgrmcn_fcha_evnto,
				prgrmcn_entrga                     = 'S'                                 ,
				cnsctvo_cdgo_estdo_srvco_slctdo    = @cnsctvo_cdgo_estdo_srvco_slctdo7   , -- Estado '7-Aprobado'
				fcha_ultma_mdfccn                  = @fechaactual                        ,
				usro_ultma_mdfccn                  = @lcUsuario
	From		bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	Inner Join  bdCNA.gsa.tbASSolicitudesAutorizacionServicios sol With(NoLock)
	On			sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	Inner Join	#Tempo_Solicitudes ts
	On			ts.nmro_slctd_atrzcn_ss = sol.nmro_slctd_atrzcn_ss
	Inner Join  #Tempo_Prestaciones t
	On			t.id = ts.id
	Inner Join  #Idsolicitudes i
	on			i.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco

	-- Actualizamos el consecutivo generico para los médicos genericos de la programacion de entrega --sisjvg01- 2017-04-05
	Update		mt
	Set         cnsctvo_gnrco = g.cnsctvo_gnrco
	From        #tbDetProgramacion t
	Inner Join  bdsisalud.dbo.tbGenericosDetalleCTC g  With(NoLock)
	On			g.cnsctvo_ntfccn     = t.cnsctvo_cdgo_ntfccn And
	            g.cnsctvo_cdgo_ofcna = t.cnsctvo_cdgo_ofcna  And
				g.cnsctvo_cdgo_ctc   = t.nmro_acta           				
	Inner Join  #Tempo_Medicos t1
	On			t1.id = t.id_tbla
	Inner Join  #Idsolicitudes i
	On			i.idxml = t1.id
	Inner Join  gsa.tbASMedicoTratanteSolicitudAutorizacionServicios mt With(NoLock)
	On          mt.cnsctvo_slctd_atrzcn_srvco = i.cnsctvo_slctd_atrzcn_srvco
	Where		t.cnsctvo_cdgo_clsfccn_evnto = @cnsctvo_cdgo_clsfccn_evnto11 -- ctc
	And         t1.nmro_unco_idntfccn_mdco   = @nmro_unco_idntfccn_mdco100961
	And         g.clse_prstdr                = @clse_prstdr  
	And         g.estdo                      = @estdo

	--Se actualiza el consecutivo del recobro en la tabla gsa.tbASServiciosSolicitados 

	Update     ss
    Set        cnsctvo_cdgo_rcbro = d.cnsctvo_cdgo_rcbro
    From	   bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios sol With(NoLock)
	On		   sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco    
	Inner Join #Idsolicitudes i
	On		   i.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco
	Inner Join BDCna.gsa.tbASDiagnosticosSolicitudAutorizacionServicios d With(NoLock)
	On         d.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco  

	--Se actualiza el codigo del recobro en la tabla gsa.tbASServiciosSolicitadosOriginal

	Update     a
    Set        cdgo_rcbro = dia.cdgo_rcbro
    From       bdCNA.gsa.tbASServiciosSolicitadosOriginal a	With(NoLock)
	Inner Join bdCNA.gsa.tbASServiciosSolicitados ss With(NoLock)
	On         ss.cnsctvo_srvco_slctdo = a.cnsctvo_srvco_slctdo
	Inner Join bdCNA.gsa.tbASSolicitudesAutorizacionServicios sol With(NoLock)
	On		   sol.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco    
	Inner Join #Idsolicitudes i
	On		   i.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco
	Inner Join bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServicios d With(NoLock)
	On         d.cnsctvo_slctd_atrzcn_srvco = sol.cnsctvo_slctd_atrzcn_srvco  
	Inner Join bdCNA.gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal dia With(NoLock)
    On         dia.cnsctvo_dgnstco_slctd_atrzcn_srvco = d.cnsctvo_dgnstco_slctd_atrzcn_srvco
   
				
End