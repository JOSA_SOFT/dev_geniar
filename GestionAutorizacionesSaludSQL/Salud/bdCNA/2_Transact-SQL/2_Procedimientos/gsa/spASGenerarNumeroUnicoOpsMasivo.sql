USE [bdCNA]
GO
/****** Object:  StoredProcedure [gsa].[spASGenerarNumeroUnicoOpsMasivo]    Script Date: 16/06/2017 08:33:05 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------													
* Metodo o PRG 		: spASGenerarNumeroUnicoOpsMasivo												
* Desarrollado por	: <\A Ing. Juan Carlos Vásquez García															A\>  
* Descripcion		: <\D Generar El Número Unico de OPS Modelo Mega												D\>  												
* Observaciones		: <\O Se recibe como parámetro una tabla temporal para ejecutar el cálculo						O\>  													
* Parametros		: <\P	#tbConceptosOps																			P\>  													
* Variables			: <\V V\>  													
* Fecha Creacion	: <\FC 2016/04/21																				FC\>  														
*  															
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------															
* DATOS DE MODIFICACION  															
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------														
* Modificado Por		: <\AM Ing. Juan Carlos Vásquez GarciaAM\>  													
* Descripcion			: <\DM Se modifica para generar número unico OPS de acuerdo a el agrupador de grupos de impresión
								1. Si el agrupador es 'S' entonces se debe generar una OPS por cada grupo de impresión
								2. Si el agrupador es 'N' entonces se debe generar una OPS por cada servicio de la solicitud(es) DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM agrpdr_imprsn																					VM\>  													
* Fecha Modificacion	: <\FM 2016/06/17			FM\>  													
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez y Angela SandovalAM\>  													
* Descripcion			: <\DM Se modifica la marca de principal para cuando tiene  varios conceptos todos con diferente numeros unicos ops DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM agrpdr_imprsn																					VM\>  													
* Fecha Modificacion	: <\FM 2016/11/04			FM\>  													
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  															
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 															
* Modificado Por		: <\AM Ing. Carlos Andres Lopez y Angela SandovalAM\>  													
* Descripcion			: <\DM Se ajusta proceso para que tenga el cuenta el recobro por prestaciones al generar el numero de OPS,
							   de forma que, si existen varias prestaciones con el mismo grupo de impresion marcadas con agrupador 'S'
							   y distinto recobro, se genere un numero de OPS para cada recobro, control de cambio 068 DM\>  												
* Nuevos Parametros	 	: <\PM PM\>  													
* Nuevas Variables		: <\VM agrpdr_imprsn																					VM\>  													
* Fecha Modificacion	: <\FM 2017/06/16			FM\>  													
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

ALTER PROCEDURE [gsa].[spASGenerarNumeroUnicoOpsMasivo]

/*01*/ @lcUsrioAplcn		UdtUsuario = null

AS

BEGIN
	SET NOCOUNT ON
	Declare	
	@cnsctvo_cdgo_ofcna							UdtConsecutivo = 0,  -- consecutivo oficina
	@cnsctvo_cdgo_cncpto						UdtConsecutivo = 33, -- consecutivo concepto
	@lnmroatncn									UdtConsecutivo = 0 ,-- Número Unico OPS
	@lnDigitoVerificacion						Int = 0,
	@lnNumeroAutorizacion						Int = 0,
	@minimo										Int = 0,
	@maximo										Int = 0,
	@cnsctvo_cdgo_estdo_cncpto_srvco_slctdo11   Int = 11, -- Estado Servicios '11- Autorizada',
	@ValorSI									char(1) = 'S',
	@ValorNO									char(1) = 'N',
	@ValorCERO									Int = 0,
	@ValorUNO									Int = 1,
	@cnsctvo_cdgo_grpo_imprsn14					Int = 14, -- Grupo Impresion '14-Derechos de Sala'
	@cnsctvo_cdgo_grpo_imprsn29					Int = 29, -- Grupo Impresion '29-Derechos de Sala e Insumos Proc. Especiales'
	@cnsctvo_cdgo_grpo_imprsn47					Int = 47, -- Grupo Impresion '47-Insumos'
	@fechaactual								Datetime = getdate(),
	@lcUsuario									UdtUsuario = Substring(System_User,CharIndex('\',System_User) + 1,Len(System_User))

	If @lcUsrioAplcn is not null
		Begin
			Set @lcUsuario = @lcUsrioAplcn
		End

	-- Creamos tabla temporal por grupos de impresión
	Create table #tbGruposImpresion
	(
		id								Int identity,
		cnsctvo_slctd_atrzcn_srvco 		UdtConsecutivo,
		grpo_imprsn						UdtConsecutivo,
		cdgo_intrno						UdtCodigoIPS,
		nmro_unco_ops					UdtConsecutivo,
		agrpdr_imprsn					UdtLogico,
		cnsctvo_cdgo_rcbro				UdtConsecutivo
	)

	-- Creamos tabla temporal por grupo de prestaciones
	Create table #tbGruposPrestacion
	(
		id								Int identity,
		cnsctvo_slctd_atrzcn_srvco 		UdtConsecutivo,
		cnsctvo_cncpto_prcdmnto_slctdo	UdtConsecutivo,
		cnsctvo_prstcn_slctda			UdtConsecutivo,
		grpo_imprsn						UdtConsecutivo,
		cdgo_intrno						UdtCodigoIPS,
		nmro_unco_ops					UdtConsecutivo,
		agrpdr_imprsn					UdtLogico default 'N',
		cnsctvo_cdgo_rcbro				UdtConsecutivo
	)


	-- Creamos tabla temporal para el cálculo del máximo dia de validez x solicitud
	Create table #tbMaximoDiasValidez
	(
		id							Int identity,
		cnsctvo_slctd_atrzcn_srvco  UdtConsecutivo,
		ds_vldz						Int
	) 
	
	Create table #TempAgrpcnServiciosXConcepto_Ops 
	(
		id_tbla							int identity,
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
		cnsctvo_prstcn_slctda			UdtConsecutivo,
		cntdd_cncptos_x_srvco			int default 0,
		cntdd_ops_x_srvco				int default 0,
		cntdd_ops_x_srvco_dplcds		int default 0
	)
	
	Create table #TempAgrpcnServiciosxOps_Dplcdas
	(
		id_tbla							int identity,
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
		cnsctvo_prstcn_slctda			UdtConsecutivo,
		nmro_unco_ops					UdtConsecutivo,
		cntdd_ops_x_srvco_dplcds		int default 0
	)

	Create table #TempAgrpcnServiciosxOps_No_Dplcdas
	(
		id_tbla							int identity,
		cnsctvo_slctd_atrzcn_srvco		UdtConsecutivo,
		cnsctvo_prstcn_slctda			UdtConsecutivo,
		nmro_unco_ops					UdtConsecutivo,
		cntdd_ops_x_srvco_dplcds		int default 0
	)

	-- Poblamos tabla grupo de impresiones
	Insert Into #tbGruposImpresion
	(
				cnsctvo_slctd_atrzcn_srvco,				grpo_imprsn,			cdgo_intrno,
				cnsctvo_cdgo_rcbro
	)
	Select		
				cnsctvo_slctd_atrzcn_srvco,				grpo_imprsn,			cdgo_intrno,
				cnsctvo_cdgo_rcbro
	From		#tbConceptosOps
	Group by	cnsctvo_slctd_atrzcn_srvco, grpo_imprsn, cnsctvo_cdgo_rcbro ,cdgo_intrno
	Order By    cnsctvo_slctd_atrzcn_srvco, grpo_imprsn, cnsctvo_cdgo_rcbro,cdgo_intrno

	-- Actualizamos el agrupador de impresión
	Update		t
	Set			agrpdr_imprsn = iif(giv.agrpdr_imprsn=@ValorSI,@ValorSI,@ValorNO)
	From		#tbGruposImpresion t
	Inner Join	bdSisalud.dbo.tbGrupoImpresion_Vigencias giv with(nolock)
	On			giv.cnsctvo_cdgo_grpo_imprsn = t.grpo_imprsn
	Where		@fechaactual between giv.inco_vgnca and giv.fn_vgnca
	And			giv.vsble_usro = @ValorSI

	-- Poblamos tabla grupo prestaciones
	Insert Into #tbGruposPrestacion
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_prstcn_slctda,				grpo_imprsn,
				cdgo_intrno,							cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_cdgo_rcbro
	)
	Select		
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_prstcn_slctda,				grpo_imprsn,
				cdgo_intrno,							cnsctvo_cncpto_prcdmnto_slctdo,		cnsctvo_cdgo_rcbro
	From		#tbConceptosOps
	Group by	cnsctvo_slctd_atrzcn_srvco,				cnsctvo_prstcn_slctda,				grpo_imprsn,
				cnsctvo_cdgo_rcbro,						cdgo_intrno,						cnsctvo_cncpto_prcdmnto_slctdo
	Order By    cnsctvo_slctd_atrzcn_srvco, cnsctvo_prstcn_slctda

	-- Actualizamos el agrupador de impresión
	Update		t
	Set			agrpdr_imprsn = iif(giv.agrpdr_imprsn=@ValorSI,@ValorSI,@ValorNO)
	From		#tbGruposPrestacion t
	Inner Join	bdSisalud.dbo.tbGrupoImpresion_Vigencias giv with(nolock)
	On			giv.cnsctvo_cdgo_grpo_imprsn = t.grpo_imprsn
	Where		@fechaactual between giv.inco_vgnca and giv.fn_vgnca
	And			giv.vsble_usro = @ValorSI
	

	-- obtenemos dias validez, se toma el de mayor duracion de todos los conceptos
	
	Insert      #tbMaximoDiasValidez
	(
				cnsctvo_slctd_atrzcn_srvco,				ds_vldz
	)
	Select		b.cnsctvo_slctd_atrzcn_srvco,			max(c.ds_vldz)        
	From		#tbConceptosOps b with (nolock)                         
	Inner Join	bdSisalud.dbo.tbConceptosGasto_Vigencias c  with (nolock)      
	On			c.cnsctvo_cdgo_cncpto_gsto = b.cnsctvo_cdgo_cncpto_gsto
	Where       @fechaactual between c.inco_vgnca And c.fn_vgnca
	Group By	b.cnsctvo_slctd_atrzcn_srvco


	-- Actualizamos el día de validez en la tabla temporal
	Update		b        
	SET			ds_vldz = mdv.ds_vldz
	From		#tbConceptosOps b 
	Inner Join  #tbMaximoDiasValidez mdv
	On			mdv.cnsctvo_slctd_atrzcn_srvco = b.cnsctvo_slctd_atrzcn_srvco


	-- Ciclo para Generar Consecutivo Ops por grupo de Impresión marcados con el agrupador de impresión 'S'
	-- Obtenemos las variables del ciclo

	select @minimo = min(id) from #tbGruposImpresion
	select @maximo = max(id) from #tbGruposImpresion
	
	
	Do while (@minimo <= @maximo And @minimo > 0 )

		Begin
			if exists(	select	1 
						from	#tbGruposImpresion 
						where	id = @minimo
						And		agrpdr_imprsn = @ValorSI)
				Begin

/*					SET TRANSACTION ISOLATION LEVEL REPEATABLE READ 
					-- Obtenemos el último consecutivo de la oficina
					Select  @lnmroatncn = ultmo_vlr          
					From	bdsisalud.dbo.tbConsecutivosPorOficina with(nolock)      
					Where	cnsctvo_cdgo_cncpto	= @cnsctvo_cdgo_cncpto          
					And		cnsctvo_cdgo_ofcna	= @cnsctvo_cdgo_ofcna   

					Set @lnmroatncn = @lnmroatncn + 1 
	
					-- Actualizamos el consecutivo de la oficina
					Update	bdsisalud.dbo.tbConsecutivosPorOficina                  
					Set		ultmo_vlr = @lnmroatncn                  
					Where	cnsctvo_cdgo_cncpto = @cnsctvo_cdgo_cncpto
					And		cnsctvo_cdgo_ofcna = @cnsctvo_cdgo_ofcna
*/
					-- obtenemos el numero de la atención
					Set @lnmroatncn = 0
					exec bdSiSalud.dbo.spPMgeneraconsecutivoatencion1 @cnsctvo_cdgo_ofcna,@cnsctvo_cdgo_cncpto,@lnmroatncn output 

					-- Generamos el Número Unico de la OPS

					Set @lnDigitoVerificacion = bdAfiliacionValidador.dbo.fnCalculaDigitoVerificacionModulo11(@lnmroatncn)    
					Set @lnNumeroAutorizacion = RTRIM(LTRIM(CAST(@lnmroatncn as CHAR(09)))) +  RTRIM(LTRIM(CAST(@lnDigitoVerificacion as CHAR(1))))

					-- Actualizamos el Número Unico OPS Generado para el grupo de impresión

					Update  #tbGruposImpresion
					Set		nmro_unco_ops = @lnNumeroAutorizacion
					Where	id = @minimo

				End

			Set @minimo += 1

		End

	-- Actualizamos tabla temporal con el Consecutivo OPS generado por grupo de impresión
	Update		t1
	Set			nmro_unco_ops = t2.nmro_unco_ops,
				agrpdr_imprsn = @ValorSI
	From		#tbConceptosOps t1
	Inner Join	#tbGruposImpresion t2
	On			t2.grpo_imprsn = t1.grpo_imprsn
	And         t2.cdgo_intrno = t1.cdgo_intrno
	And			t1.cnsctvo_cdgo_rcbro = t2.cnsctvo_cdgo_rcbro
	And			t2.cnsctvo_slctd_atrzcn_srvco = t1.cnsctvo_slctd_atrzcn_srvco
	And			t2.agrpdr_imprsn = @ValorSI

	-- Ciclo para Generar Consecutivo Ops por prestacion marcados con el agrupador de impresión 'N'
	-- Obtenemos las variables del ciclo

	select @minimo = min(id) from #tbGruposPrestacion
	select @maximo = max(id) from #tbGruposPrestacion
	
	
	Do while (@minimo <= @maximo And @minimo > 0 )

		Begin
			if exists(	select	1 
						from	#tbGruposPrestacion 
						where	id = @minimo
						And		agrpdr_imprsn = @ValorNO)
				Begin
/*
					SET TRANSACTION ISOLATION LEVEL REPEATABLE READ 
					-- Obtenemos el último consecutivo de la oficina
					Select  @lnmroatncn = ultmo_vlr          
					From	bdsisalud.dbo.tbConsecutivosPorOficina with(nolock)      
					Where	cnsctvo_cdgo_cncpto	= @cnsctvo_cdgo_cncpto          
					And		cnsctvo_cdgo_ofcna	= @cnsctvo_cdgo_ofcna   

					Set @lnmroatncn = @lnmroatncn + 1 
	
					-- Actualizamos el consecutivo de la oficina
					Update	bdsisalud.dbo.tbConsecutivosPorOficina                  
					Set		ultmo_vlr = @lnmroatncn                  
					Where	cnsctvo_cdgo_cncpto = @cnsctvo_cdgo_cncpto
					And		cnsctvo_cdgo_ofcna = @cnsctvo_cdgo_ofcna
*/
					-- obtenemos el numero de la atención
					Set @lnmroatncn = 0
					exec bdSiSalud.dbo.spPMgeneraconsecutivoatencion1 @cnsctvo_cdgo_ofcna,@cnsctvo_cdgo_cncpto,@lnmroatncn output 

					-- Generamos el Número Unico de la OPS

					Set @lnDigitoVerificacion = bdAfiliacionValidador.dbo.fnCalculaDigitoVerificacionModulo11(@lnmroatncn)    
					Set @lnNumeroAutorizacion = RTRIM(LTRIM(CAST(@lnmroatncn as CHAR(09)))) +  RTRIM(LTRIM(CAST(@lnDigitoVerificacion as CHAR(1))))

					-- Actualizamos el Número Unico OPS Generado para la prestación
					Update  #tbGruposPrestacion
					Set		nmro_unco_ops = @lnNumeroAutorizacion
					Where	id = @minimo

				End

				Set @minimo += 1
		End

	-- Actualizamos tabla temporal con el Consecutivo OPS generado por grupo de Prestación
	Update		t1
	Set			nmro_unco_ops = t2.nmro_unco_ops,
				agrpdr_imprsn = @ValorSI
	From		#tbConceptosOps t1
	Inner Join	#tbGruposPrestacion t2
	On			t2.grpo_imprsn = t1.grpo_imprsn
	And         t2.cdgo_intrno = t1.cdgo_intrno
	And			t1.cnsctvo_cdgo_rcbro = t2.cnsctvo_cdgo_rcbro
	And			t2.cnsctvo_slctd_atrzcn_srvco = t1.cnsctvo_slctd_atrzcn_srvco
	And			t2.cnsctvo_cncpto_prcdmnto_slctdo = t1.cnsctvo_cncpto_prcdmnto_slctdo
	And			t2.agrpdr_imprsn = @ValorNO

	-- Agrupamos los servicios x concepto-ops 
	Insert into #TempAgrpcnServiciosXConcepto_Ops
	(
				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_prstcn_slctda, 			cntdd_cncptos_x_srvco,							
				cntdd_ops_x_srvco
	)
	select 
				ss.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_prstcn_slctda,		count(distinct ss.cnsctvo_cdgo_cncpto_gsto),	
				count(distinct ss.nmro_unco_ops)
	From	    #tbConceptosOps ss
	Group By    ss.cnsctvo_slctd_atrzcn_srvco,ss.cnsctvo_prstcn_slctda

	-- Agrupamos los servicios x ops duplicadas
	Insert Into #TempAgrpcnServiciosxOps_Dplcdas
	(

				cnsctvo_slctd_atrzcn_srvco,				cnsctvo_prstcn_slctda,			ss.nmro_unco_ops,		
				cntdd_ops_x_srvco_dplcds
	)
	select 
				ss.cnsctvo_slctd_atrzcn_srvco,			ss.cnsctvo_prstcn_slctda,		ss.nmro_unco_ops,		
				count(distinct ss.nmro_unco_ops)
	From	    #tbConceptosOps ss
	Group By    ss.cnsctvo_slctd_atrzcn_srvco,ss.cnsctvo_prstcn_slctda,ss.nmro_unco_ops
	having		count(ss.nmro_unco_ops)> 1

	Insert Into #TempAgrpcnServiciosxOps_No_Dplcdas
	(

				cnsctvo_slctd_atrzcn_srvco,		cnsctvo_prstcn_slctda,	   cntdd_ops_x_srvco_dplcds
	)
	select 
				ss.cnsctvo_slctd_atrzcn_srvco,   ss.cnsctvo_prstcn_slctda, count(ss.cnsctvo_prstcn_slctda)  
	From		#tbConceptosOps ss
	Group By    ss.cnsctvo_slctd_atrzcn_srvco,ss.cnsctvo_prstcn_slctda
	having		count(ss.cnsctvo_prstcn_slctda)> 1


    -- Criterio 1. Actualizamos los servicios con ops principal para aquellos que tienen un solo concepto de gasto y Número Unico (OPS)
	Update		ss
	Set			nmro_unco_prncpl_cta_rcprcn = @ValorSI
	From		#tbConceptosOps ss	
	Inner Join	#TempAgrpcnServiciosXConcepto_Ops t1	
	On			t1.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			t1.cnsctvo_prstcn_slctda = ss.cnsctvo_prstcn_slctda	
	Where		t1.cntdd_cncptos_x_srvco = @ValorUNO
	And			t1.cntdd_ops_x_srvco = @ValorUNO	
	
	 -- Criterio 2. Actualizamos los servicios con ops principal para aquellos que tienen más de un concepto de gasto y un solo Número Unico (OPS)
	Update		ss
	Set			nmro_unco_prncpl_cta_rcprcn = @ValorSI
	From		#tbConceptosOps ss	
	Inner Join	#TempAgrpcnServiciosXConcepto_Ops t1	
	On			t1.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			t1.cnsctvo_prstcn_slctda = ss.cnsctvo_prstcn_slctda	
	Where		t1.cntdd_cncptos_x_srvco > @ValorUNO
	And			t1.cntdd_ops_x_srvco = @ValorUNO
	And			ss.nmro_unco_prncpl_cta_rcprcn = @ValorNO	
	
	-- Criterio 3. Actualizamos los servicios con ops principal para aquellos que tienen más de un concepto de gasto y mas de un Número Unico (OPS)
	Update		ss
	Set			nmro_unco_prncpl_cta_rcprcn = @ValorSI
	From		#tbConceptosOps ss	
	Inner Join	#TempAgrpcnServiciosxOps_Dplcdas t1	
	On			t1.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			t1.cnsctvo_prstcn_slctda = ss.cnsctvo_prstcn_slctda	
	And			t1.nmro_unco_ops = ss.nmro_unco_ops
	Inner Join  bdSisalud.dbo.tbGrupoImpresion_Vigencias giv with(nolock)
	On			giv.cnsctvo_cdgo_grpo_imprsn = ss.grpo_imprsn
	And			(giv.agrpdr_imprsn = @ValorSI or giv.cnsctvo_cdgo_grpo_imprsn in(@cnsctvo_cdgo_grpo_imprsn14,@cnsctvo_cdgo_grpo_imprsn29,@cnsctvo_cdgo_grpo_imprsn47))
	Where		cntdd_ops_x_srvco_dplcds > @ValorCERO
	And			ss.nmro_unco_prncpl_cta_rcprcn = @ValorNO	
	
	-- Criterio 4. Actualizamos los servicios con ops principal para aquellos que tienen más de un concepto de gasto y mas de un Número Unico (OPS) No repetido
	Update		#tbConceptosOps
	Set			nmro_unco_prncpl_cta_rcprcn = @ValorSI
	where       grpo_imprsn=(Select top 1 ss.grpo_imprsn
	From		#tbConceptosOps ss	
	Inner Join	#TempAgrpcnServiciosxOps_No_Dplcdas t1	
	On			t1.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
	And			t1.cnsctvo_prstcn_slctda = ss.cnsctvo_prstcn_slctda	
	Inner Join  bdSisalud.dbo.tbGrupoImpresion_Vigencias giv with(nolock)
	On			giv.cnsctvo_cdgo_grpo_imprsn = ss.grpo_imprsn
	And			(giv.agrpdr_imprsn = @ValorSI or giv.cnsctvo_cdgo_grpo_imprsn in(@cnsctvo_cdgo_grpo_imprsn14,@cnsctvo_cdgo_grpo_imprsn29,@cnsctvo_cdgo_grpo_imprsn47))
	Where		cntdd_ops_x_srvco_dplcds > @ValorCERO
	And			ss.nmro_unco_prncpl_cta_rcprcn = @ValorNO)		
	

	Drop Table #tbGruposImpresion
	Drop Table #tbMaximoDiasValidez
	Drop Table #TempAgrpcnServiciosXConcepto_Ops
	Drop Table #TempAgrpcnServiciosxOps_Dplcdas
	Drop Table #tbGruposPrestacion
	Drop Table #TempAgrpcnServiciosxOps_No_Dplcdas

END
