USE [bdCNA]
GO
/****** Object:  StoredProcedure [cpw].[spCPWConsultarSolicitudesNUI]    Script Date: 09/12/2016 03:49:30 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Metodo o PRG     :  [cpw].[spCPWConsultarSolicitudesNUI]
* Desarrollado por : <\A Ing. Jorge ANDres Garcia Erazo - Geniar SAS A\>     
* Descripcion      : <\D Consulta las prestaciones de un afiliado a partir del nui de acuerdo a las reglas establecidas 
						 por el caso de uso  D\>     
* Observaciones    : <\O O\>     
* Parametros       : <\P 
						 @nui: numero unico identificacion del afiliado
						 @consecutivoLog: log del maestro del log.
					 P\>     
* Variables        : <\V										  V\>     
* Fecha Creacion   : <\FC 2013/01/24 FC\>     
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------  
* Modificado Por		:	<\AM Jorge Andres Garcia Erazo - Geniar S.A.S AM\>    
* Descripcion			:	<\DM Control de cambio 01 proyecto SOMA lineas: 429-443 DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM VM\>    
* Fecha Modificacion	:	<\FM  2013/09/16 FM\>  
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------  
* Modificado Por		:	<\AM Jorge Andres Garcia Erazo - Geniar S.A.S AM\>    
* Descripcion			:	<\DM Control de cambio 02 proyecto SOMA lineas:   DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM VM\>    
* Fecha Modificacion	:	<\FM  2013/09/20 FM\>       
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------  
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		:	<\AM Jorge Andres Garcia Erazo - Geniar S.A.S AM\>    
* Descripcion			:	<\DM Desarrollo de contingencia ya que en ambiente de produccion no esta el cambio 
								 que en tbsolicitudops guarde el nui del afiliado, se agrega or a consulta de 3047
								 para que cuando no este el nui busque por numero y tipo de identificacion DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM VM\>    
* Fecha Modificacion	:	<\FM  2013/10/17 FM\>      
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* Modificado Por		:	<\AM Jorge Andres Garcia Erazo - Geniar S.A.S AM\>    
* Descripcion			:	<\DM Correccion de la consulta del calculo de la fecha probable para los estados de 3047 DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM VM\>    
* Fecha Modificacion	:	<\FM  2013/10/22 FM\>     
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION    
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Modificado Por		:	<\AM Julian Hernandez Gomez - SETI AM\>    
* Descripcion			:	<\DM Se agregan campos nuevos de salida fcha_utlzcn_hsta, cpgo_pgo, cta_mdrdra DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM fcha_utlzcn_hsta, cpgo_pgo, cta_mdrdra VM\>    
* Fecha Modificacion	:	<\FM  2015/07/29 FM\>     
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Modificado Por		:	<\AM Angela Sandoval y Carlos Andrés López Ramírez AM\>    
* Descripcion			:	<\DM Se atualiza para que se vean los valores de copagos y cuotas moderadoras de mega DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM @cnsctvo_cdgo_cncpto_pgo1 VM\>    
* Fecha Modificacion	:	<\FM 2016/11/23 FM\>     
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION  
*------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Modificado Por		:	<\AM Carlos Andres Lopez Ramirez AM\>    
* Descripcion			:	<\DM Se agrega distinct en consulta que retornaba datos duplicados DM\>    
* Nuevos Parametros		:	<\PM PM\>    
* Nuevas Variables		:	<\VM VM\>    
* Fecha Modificacion	:	<\FM 2016/12/09 FM\>     
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*
Declare @nui Int, @consecutivoLog int,@flag int
Set @nui = 33904312
Set @consecutivoLog = 0
set @flag=0
EXEC [cpw].[spCPWConsultarSolicitudesNUI] @nui, @consecutivoLog,@flag
*/
ALTER PROCEDURE [cpw].[spCPWConsultarSolicitudesNUI](
	@nui Int, 
	@consecutivoLog int,
	@flag int = 0
)
AS

SET NOCOUNT ON
/*
Declare @nui Int, 
		@consecutivoLog int,@flag int

	Set @nui = 31811510
	Set @consecutivoLog = 1
	set @flag=1*/
	
--Variables generales para las consultas de prestaciones y estados.

Declare @valorReglaDias int,		@cnsctvo_cdgo_rgla int,		@fechaActual datetime,
		@fechaInicio datetime,		@fuenteSipres char(1),		@fuente3047 char(1),
		@cerrado varchar(10),		@abierto varchar(10),		@valorDiasRespuesta int,
		@cnsctvo_cdgo_rgla_res int,	@aplca_ds_rgla char(1),		@estadoImpreso int,
		@csa_dvlcn_capita int,		@logPrestaciones char(1),	@rgstra_lg char(1),
		@rprgrma_fcha_rspsta char(1),@marcadescarga  int

Declare @ips_observacion char(8), @max_id int, @contador int,	@observaciones nvarchar(250),
		@nom_ips_observacion varchar(100), @telefono_ips_obser varchar(50), @dire_ips_obser varchar(100),
		@fecha_proba_obser varchar(150), @fecha_program_obser varchar(150), @variable_si char(1),
		@anulada int, @nmro_idntfccn_afldo varchar(20),  @cnsctvo_cdgo_tpo_idntfccn_afldo int

--Variables para los diferentes tipos de calculo de fecha.

Declare	@calculoFechaImpr int,	@calculoFechaProgram int,	@calculoFechaParam int,
		@calculoFechaRegla int,	@calculoFechaNoMostrar int,	@ips_primaria varchar(15),
		@ips_capita varchar(15), @dir_Prestador varchar(15), @fecha_programacion varchar(20),
		@fecha_actual varchar(15), @fecha_probable varchar(20), @tel_prestador varchar(15),
		@pres_destino varchar(15), @fechaRecalculada varchar(150), @vncda char(1), @variable_no char(1)

--Variables para la consulta de presolicitudes.
DECLARE @cnsctvo_estdo_prslctd_crrdo int,
		@cnsctvo_estdo_prslctd_anldo int,
		@dscrpcn_estdo_no_atrzdo varchar(50),
		@obsrvcn_no_atrzdo varchar(150)

-- modificado por angela sandoval
Declare @cnsctvo_cdgo_cncpto_pgo1 int, 
@cnsctvo_cdgo_cncpto_pgo2 int

set @cnsctvo_cdgo_cncpto_pgo1 = 1
set @cnsctvo_cdgo_cncpto_pgo2 = 2

Set @cnsctvo_estdo_prslctd_crrdo = 10
Set @cnsctvo_estdo_prslctd_anldo = 16
Set @dscrpcn_estdo_no_atrzdo = 'EN AUDITORIA'
Set @obsrvcn_no_atrzdo = 'Acérquese al SIAU más cercano por la respuesta a su solicitud'		
		
Create table #prestacionesMiSolicitud(
	id int identity,
	cnsctvo_cdgo_ofcna udtConsecutivo,
	nuam numeric(9,0),
	cnsctvo_slctd_prstcn int,
	cnsctvo_slctd_ops int,
	fcha datetime,
	dscrpcn_cdfccn varchar(150),
	estdo_sn_lqdr int,
	prstdr_dstno char(8),
	estdo_lqddo int,
	tpo_srvco varchar(250),
	estdo_3047 int,
	estdo_hmlgdo_afldo udtDescripcion,
	cnsctvo_cdgo_estdo_hmlgdo_afldo int,
	estdo_abrto_crrdo varchar(10),
	fcha_prbble_rspsta varchar(150),
	fcha_imprsn datetime,
	fcha_incl_imprsn datetime,
	fcha_fnl_imprsn datetime,
	cnsctvo_cdgo_pln int,
	cnsctvo_cdfccn int,
	ds_prgrmcn_fcha_prbble numeric(4,0),
	tpo_clclo_fcha int,
	ips_mstrr char(8),
	txto_ips varchar(250),
	csa_dvlcn int,
	rgstra_lg char(1),
	obsrvcn_afldo nvarchar(250),
	rprgrma_fcha_rspsta char(1),
	tlfno_ips udtTelefono,
	drccn_ips udtDireccion,
	fcha_prbble_rspsta_date datetime,
	cnsctvo_vgnca_cnslta_prstcns_wb int,
	fcha_utlzcn_hsta datetime,
	cpgo_pgo float,
	cta_mdrdra float,

	cnsctvo_cdgo_estdo_atncn udtConsecutivo, /* jh*/
	estdo_prstcn_ss udtConsecutivo, /* jh*/
	cnsctvo_cdgo_estdo_atrzcn udtConsecutivo, /* jh*/
)

Create table #prestacionesMiSolicitudConceptoDeGasto(
	cnsctvo_cdgo_ofcna int,
	nuam numeric(18,0),
	cnsctvo_prstcn int,
	cnsctvo_cdgo_cncpto_gsto int,
	cdgo_prstdr char(8),
	cncpto_prncpl char(1) default 'N',
	cdgo_intrno_hsptlro char(8),
	fcha_imprsn datetime
)

Create table #prestacionesMiSolicitudUnConceptoPpal(
	cnsctvo_cdgo_ofcna int,
	nuam numeric(18,0),
	cnsctvo_prstcn int,
	cdgo_prstdr char(8),
	cncpto_prncpl char(1),
	fcha_imprsn datetime
)

CREATE TABLE #prestaciones(
	id int identity,
	cnsctvo_cdfccn int,
)

DECLARE @prestadoresCapitacion TABLE (
	cdgo_intrno char(8),
	cnsctvo_cdgo_cdd int,
	cnsctvo_cdfccn int,
	nmro_unco_idntfccn_prstdr int,
	nmbre_prstdr varchar(250)
)


--Valores de los parametros generales.

Set @fechaActual			= getdate()
Set @cnsctvo_cdgo_rgla		= 84
Set @cnsctvo_cdgo_rgla_res	= 85
Set @fuenteSipres			= '1'
Set @fuente3047				= '2'
Set @cerrado				= 'CERRADO'
Set @abierto				= 'ABIERTO'
Set @aplca_ds_rgla			= 'S'
Set @estadoImpreso			= 8
Set @csa_dvlcn_capita		= 29
Set @rgstra_lg				= 'S'
Set @rprgrma_fcha_rspsta	= 'S'
Set @contador				= 1
Set @variable_si			= 'S'
Set @vncda					= 'S'
Set @variable_no			= 'N'
Set @anulada				= 3
set @marcadescarga          = 2 --angela

--Constantes que seran reemplazadas.

SET @ips_primaria			= 'ips_primaria'
SET @ips_capita				= 'ips_capita'
SET @dir_Prestador			= 'dir_Prestador'
SET @fecha_programacion		= 'fecha_programacion'
SET @fecha_actual			= 'fecha_actual'	
SET @fecha_probable			= 'fecha_probable'
SET @tel_prestador			= 'tel_prestador'
SET @pres_destino			= 'pres_destino'

--Valores de los calculos de fecha

Set @calculoFechaImpr		= 1
Set @calculoFechaProgram	= 2
Set @calculoFechaParam		= 3
Set @calculoFechaRegla		= 4
Set @calculoFechaNoMostrar	= 5

Select  @valorReglaDias = cast(b.vlr_rgla as int)
From bdsisalud..tbreglas a with(nolock) 
	 inner join bdsisalud..tbreglas_vigencias b with(nolock)
	 on a.cnsctvo_cdgo_rgla = b.cnsctvo_cdgo_rgla
Where  @fechaActual between b.inco_vgnca and b.fn_vgnca
And a.cnsctvo_cdgo_rgla = @cnsctvo_cdgo_rgla

Select @valorDiasRespuesta = cast(b.vlr_rgla as int)
From bdsisalud..tbreglas a with(nolock) 
	 inner join bdsisalud..tbreglas_vigencias b with(nolock)
	 on a.cnsctvo_cdgo_rgla = b.cnsctvo_cdgo_rgla
Where  @fechaActual between b.inco_vgnca and b.fn_vgnca
And a.cnsctvo_cdgo_rgla = @cnsctvo_cdgo_rgla_res

Select @ips_observacion=a.cdgo_intrno, @nom_ips_observacion=b.nmbre_scrsl, @telefono_ips_obser=b.tlfno, @dire_ips_obser=b.drccn,
	   @nmro_idntfccn_afldo=a.nmro_idntfccn,  @cnsctvo_cdgo_tpo_idntfccn_afldo=a.cnsctvo_cdgo_tpo_idntfccn 
from bdafiliacionvalidador..tbbeneficiariosvalidador a with(nolock)
	 inner join bdsisalud..tbdireccionesprestador b with(nolock) ON a.cdgo_intrno = b.cdgo_intrno
where a.nmro_unco_idntfccn_afldo = @nui

--Se hace todo este proceso para quitarle las horas, minutos segundos etc. y quede solo la fecha.

Set @fechaInicio = CONVERT(datetime,CONVERT(varchar,@fechaActual-@valorReglaDias , 101))

--Esta es la nueva fecha recalculada en caso de que sea necesario recalcular

SET @fechaRecalculada = CONVERT(varchar,DATEADD(day,isnull(@valorDiasRespuesta,0),@fechaActual),103)


--Inserta todas las solicitudes que estan en proceso de liquidacion en la tabla tbconceptosops el cual tiene todos los conceptos
--de gasto que tienen las prestaciones.

Insert into #prestacionesMiSolicitudConceptoDeGasto (cnsctvo_cdgo_ofcna,nuam,cnsctvo_prstcn,cnsctvo_cdgo_cncpto_gsto,cncpto_prncpl,cdgo_prstdr,fcha_imprsn)
Select b.cnsctvo_cdgo_ofcna
	   ,b.nuam
	   ,b.cnsctvo_prstcn
	   ,d.cnsctvo_cdgo_cncpto_gsto
	   ,isnull(g.cncpto_prncpl,'N')
	   ,d.cdgo_prstdr
	   ,d.fcha_imprsn
From bdsisalud..tbatencionops a with(nolock) 
	 inner join bdsisalud..tbprocedimientos b with(nolock) ON a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna and a.nuam = b.nuam
	 inner join bdsisalud..tbconceptosops d with(nolock) ON b.cnsctvo_cdgo_ofcna = d.cnsctvo_cdgo_ofcna and b.nuam = d.nuam and b.cnsctvo_prstcn = d.cnsctvo_prstcn
	 inner join bdsisalud..tbconceptosgasto_vigencias g with(nolock) on d.cnsctvo_cdgo_cncpto_gsto = g.cnsctvo_cdgo_cncpto_gsto
Where a.nmro_unco_idntfccn_afldo = @nui
And d.cnsctvo_cdgo_estdo != @anulada
And a.fcha_ultma_mdfccn between @fechaInicio and @fechaActual

--A partir de la tabla anterior toma las prestaciones que tengan un concepto de gasto principal, si solo tiene un concepto de gasto
--Que no sea principal lo tomara de igual manera almacena esta informacion en otra tabla temporal.

Insert into #prestacionesMiSolicitudUnConceptoPpal(cnsctvo_cdgo_ofcna,nuam,cnsctvo_prstcn,cdgo_prstdr,cncpto_prncpl,fcha_imprsn)
Select	cnsctvo_cdgo_ofcna,
		nuam,
		cnsctvo_prstcn,
		cdgo_prstdr,
		cncpto_prncpl,
		fcha_imprsn
from #prestacionesMiSolicitudConceptoDeGasto
where cncpto_prncpl = @variable_si
group by cnsctvo_cdgo_ofcna,nuam,cnsctvo_prstcn,cdgo_prstdr,cncpto_prncpl,fcha_imprsn
having count(cnsctvo_prstcn) = 1

Insert into #prestacionesMiSolicitudUnConceptoPpal(cnsctvo_cdgo_ofcna,nuam,cnsctvo_prstcn,cdgo_prstdr,cncpto_prncpl,fcha_imprsn)
Select	cnsctvo_cdgo_ofcna,
		nuam,
		cnsctvo_prstcn,
		cdgo_prstdr,
		cncpto_prncpl,
		fcha_imprsn
from #prestacionesMiSolicitudConceptoDeGasto
where cncpto_prncpl = @variable_no
group by cnsctvo_cdgo_ofcna,nuam,cnsctvo_prstcn,cdgo_prstdr,cncpto_prncpl,fcha_imprsn
having count(cnsctvo_prstcn) = 1


--Elimino de la tabla que tiene todos los conceptos de gasto las prestaciones que fueron tomadas con 1 concepto de gasto principal o que solo 
--tengan un concepto de gasto.
--Quedan en la tabla que contienen los conceptos de gasto donde estan los que tienen varios principales o varios conceptos de gasto que no son 
--principales.

delete from #prestacionesMiSolicitudConceptoDeGasto
where exists (
	select *
	from #prestacionesMiSolicitudUnConceptoPpal b 
	where #prestacionesMiSolicitudConceptoDeGasto.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna 
	and #prestacionesMiSolicitudConceptoDeGasto.nuam = b.nuam 
	and #prestacionesMiSolicitudConceptoDeGasto.cnsctvo_prstcn = b.cnsctvo_prstcn)

--Se inserta en la tabla temporal todas las prestaciones que esten en conceptosops teniendo en cuenta todas las validaciones de los conceptos de
--gasto.

Insert into #prestacionesMiSolicitud (cnsctvo_cdgo_ofcna,nuam,fcha,dscrpcn_cdfccn,estdo_sn_lqdr,prstdr_dstno,
								estdo_lqddo,tpo_srvco,fcha_imprsn,fcha_incl_imprsn,fcha_fnl_imprsn,cnsctvo_cdgo_pln,
								cnsctvo_cdfccn,ips_mstrr, cnsctvo_cdgo_estdo_atncn /* jh*/,cnsctvo_cdgo_estdo_atrzcn)
Select Distinct	--qvisionclr  2016/12/09
		a.cnsctvo_cdgo_ofcna
	   ,a.nuam
	   ,b.fcha_expdcn
	   ,c.dscrpcn_cdfccn
	   ,b.cnsctvo_cdgo_estdo as estdo_sn_lqdr
	   ,a.cdgo_prstdr as prstdr_dstno
	   ,b.cnsctvo_cdgo_estdo as estdo_lqddo
	   ,e.dscrpcn_agrpdr_prstcn as tpo_srvco
	   ,CONVERT(varchar, a.fcha_imprsn ,112) as fcha_imprsn
	   ,f.fcha_incl_imprsn
	   ,f.fcha_fnl_imprsn
	   ,b.cnsctvo_cdgo_pln
	   ,a.cnsctvo_prstcn
	   ,a.cdgo_prstdr
	   ,cnsctvo_cdgo_tps_mrcs_ops as cnsctvo_cdgo_estdo_atncn /* jh*/
	   ,cnsctvo_cdgo_estdo_atncn as cnsctvo_cdgo_estdo_atrzcn   /* jh*/

from #prestacionesMiSolicitudUnConceptoPpal a
	 inner join bdsisalud.dbo.tbatencionops b with(nolock) on a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna and a.nuam = b.nuam
	 inner join bdsisalud.dbo.tbcodificaciones c with(nolock) on a.cnsctvo_prstcn = c.cnsctvo_cdfccn
	 inner join bdsisalud.dbo.tbCupsServicios d with(nolock) ON d.cnsctvo_prstcn = a.cnsctvo_prstcn
	 inner join bdsisalud.dbo.tbAgrupadorPrestacion_Vigencias e with(nolock) ON e.cnsctvo_cdgo_agrpdr_prstcn = d.cnsctvo_agrpdr_prstcn
	 left join bdsisalud.dbo.tbsolicitudopsprestaciones f with(nolock) ON f.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna and f.nuam = a.nuam
	 left join tbHistoricoEstadosAtencionOPS h with(nolock) ON h.cnsctvo_atncn_ops = b.cnsctvo_atncn_ops and @fechaActual between h.inco_vgnca and h.fn_vgnca and h.vldo = 'S' /* jh*/  
	 left join tbMarcasXAtencionOPS i with(nolock) ON i.cnsctvo_atncn_ops = b.cnsctvo_atncn_ops /*20150717 sislbr01*/
	 And i.cnsctvo_cdgo_tps_mrcs_ops = @marcadescarga And @fechaActual between i.inco_vgnca And i.fn_vgnca And i.vldo = 'S' 


--Se toman todas las solicitudes de SIPRES de acuerdo al nui. No se tiene en cuenta conceptosops ya que en el select anterior se tiene 
--toda la informacion de tbconceptosops y se saca de este select para solo tomar los que no esten en tbconceptosops

Insert into #prestacionesMiSolicitud (cnsctvo_cdgo_ofcna,nuam,fcha,dscrpcn_cdfccn,estdo_sn_lqdr,prstdr_dstno,
								estdo_lqddo,tpo_srvco,fcha_imprsn,fcha_incl_imprsn,fcha_fnl_imprsn,cnsctvo_cdgo_pln,
								cnsctvo_cdfccn,ips_mstrr, cnsctvo_cdgo_estdo_atncn /* jh*/,cnsctvo_cdgo_estdo_atrzcn)
Select distinct 
		b.cnsctvo_cdgo_ofcna
	   ,b.nuam
	   ,a.fcha_expdcn
	   ,c.dscrpcn_cdfccn
	   ,a.cnsctvo_cdgo_estdo as estdo_sn_lqdr
	   ,null as prstdr_dstno
	   ,a.cnsctvo_cdgo_estdo as estdo_lqddo
	   ,f.dscrpcn_agrpdr_prstcn as tpo_srvco
	   ,null as fcha_imprsn
	   ,g.fcha_incl_imprsn
	   ,g.fcha_fnl_imprsn
	   ,a.cnsctvo_cdgo_pln
	   ,b.cnsctvo_prstcn
	   ,null as ips_mstrr
	   ,i.cnsctvo_cdgo_tps_mrcs_ops as cnsctvo_cdgo_estdo_atncn /* jh*/
	   ,h.cnsctvo_cdgo_estdo_atncn as cnsctvo_cdgo_estdo_atrzcn   /* jh*/

From bdsisalud..tbatencionops a with(nolock) 
	 inner join bdsisalud..tbprocedimientos b with(nolock) ON a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna and a.nuam = b.nuam
	 inner join bdsisalud..tbcodificaciones c with(nolock) ON b.cnsctvo_prstcn = c.cnsctvo_cdfccn 
	 inner join bdsisalud..tbCupsServicios e with(nolock) ON e.cnsctvo_prstcn = c.cnsctvo_cdfccn
	 inner join bdsisalud..tbAgrupadorPrestacion_Vigencias f with(nolock) ON f.cnsctvo_cdgo_agrpdr_prstcn = e.cnsctvo_agrpdr_prstcn
	 left join bdsisalud..tbsolicitudopsprestaciones g with(nolock) ON g.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna and g.nuam = a.nuam
	 left join tbHistoricoEstadosAtencionOPS h with(nolock) ON h.cnsctvo_atncn_ops = a.cnsctvo_atncn_ops and @fechaActual between h.inco_vgnca and h.fn_vgnca and h.vldo = 'S' /* jh*/  
	 left join tbMarcasXAtencionOPS i with(nolock) ON i.cnsctvo_atncn_ops = a.cnsctvo_atncn_ops /*20150717 sislbr01*/
	 And i.cnsctvo_cdgo_tps_mrcs_ops = @marcadescarga And @fechaActual between i.inco_vgnca And i.fn_vgnca And i.vldo = 'S' 

Where a.nmro_unco_idntfccn_afldo = @nui
And a.cnsctvo_cdgo_estdo != @anulada
And b.fcha_ultma_mdfccn between @fechaInicio and @fechaActual
And not exists(
	Select *
	from #prestacionesMiSolicitudUnConceptoPpal h
	where h.cnsctvo_cdgo_ofcna = a.cnsctvo_cdgo_ofcna
	and h.nuam = a.nuam
)

--Se toman las solicitudes de 3047 que no esten bajo ninguna manera en SIPRES de acuerdo a la consulta anterior.

Insert into #prestacionesMiSolicitud (fcha,dscrpcn_cdfccn,estdo_3047,tpo_srvco,cnsctvo_cdgo_pln,cnsctvo_cdfccn,csa_dvlcn,ips_mstrr,
								cnsctvo_slctd_prstcn,cnsctvo_slctd_ops,fcha_incl_imprsn,fcha_fnl_imprsn)
Select a.fcha_crcn 
	   ,b.dscrpcn_prstcn 
	   ,b.cnsctvo_cdgo_estdo_prstcn_slctd
	   ,d.dscrpcn_agrpdr_prstcn
	   ,a.cnsctvo_cdgo_pln
	   ,b.cnsctvo_cdfccn
	   ,e.cnsctvo_csa_dvlcn
	   ,(CASE WHEN e.cnsctvo_csa_dvlcn = @csa_dvlcn_capita THEN a.cdgo_intrno ELSE NULL END) as ips_mstrr
	   ,b.cnsctvo_slctd_prstcn
	   ,b.cnsctvo_slctd_ops
	   ,b.fcha_incl_imprsn
	   ,b.fcha_fnl_imprsn
From bdsisalud..tbsolicitudops a with(nolock) 
	 inner join bdsisalud..tbsolicitudopsprestaciones b with(nolock) on a.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops
	 inner join bdsisalud..tbCupsServicios c with(nolock) on b.cnsctvo_cdfccn = c.cnsctvo_prstcn
	 inner join bdsisalud..tbAgrupadorPrestacion_Vigencias d with(nolock) on c.cnsctvo_agrpdr_prstcn = d.cnsctvo_cdgo_agrpdr_prstcn
	 left join bdsisalud..tbValidacionSolicitudops e with(nolock) on (e.cnsctvo_slctd_ops = b.cnsctvo_slctd_ops and e.cnsctvo_cdfccn = b.cnsctvo_cdfccn and e.cnsctvo_csa_dvlcn = @csa_dvlcn_capita)
where(a.nmro_unco_idntfccn_afldo = @nui or (a.nmro_idntfccn_pcnte = @nmro_idntfccn_afldo and a.cnsctvo_cdgo_tpo_idntfccn_pcnte = @cnsctvo_cdgo_tpo_idntfccn_afldo))
and (b.fcha_ultma_mdfccn between @fechaInicio and @fechaActual or b.fcha_ultma_mdfccn is null)
and not exists (
	Select nuam,cnsctvo_cdgo_ofcna
	From #prestacionesMiSolicitud presipre
	Where b.cnsctvo_cdgo_ofcna = presipre.cnsctvo_cdgo_ofcna
	and b.nuam = presipre.nuam
)

Insert into #prestaciones(cnsctvo_cdfccn)
Select cnsctvo_cdfccn
From #prestacionesMiSolicitud
Where csa_dvlcn = @csa_dvlcn_capita

Insert into @prestadoresCapitacion(cdgo_intrno,cnsctvo_cdgo_cdd,cnsctvo_cdfccn,nmro_unco_idntfccn_prstdr)
exec bdsisalud.dbo.SpVWConsultaConvenioCapitacionMasivo @nui

Update a
Set nmbre_prstdr = b.rzn_scl
From @prestadoresCapitacion a 
	 inner join bdsisalud.dbo.tbips b with(nolock) on a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr

Update a
Set nmbre_prstdr = nmbre_scrsl
From @prestadoresCapitacion a 
	 inner join bdsisalud.dbo.tbdireccionesprestador b with(nolock) on a.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr
	 inner join bdsisalud.dbo.tbmedicos c with(nolock) on c.nmro_unco_idntfccn_prstdr = b.nmro_unco_idntfccn_prstdr
where a.nmbre_prstdr is not null 

Update a 
Set txto_ips = b.nmbre_prstdr
From #prestacionesMiSolicitud a
	inner join @prestadoresCapitacion b on a.cnsctvo_cdfccn = b.cnsctvo_cdfccn and a.csa_dvlcn = @csa_dvlcn_capita

--Actualizacion de los dias probables para la prestacion en el plan

UPDATE pres
SET pres.ds_prgrmcn_fcha_prbble = a.ds_prgrmcn_fcha_prbble
FROM #prestacionesMiSolicitud pres 
	 inner join bdsisalud..tbcupsserviciosxplanes a with(nolock) ON pres.cnsctvo_cdfccn = a.cnsctvo_prstcn and pres.cnsctvo_cdgo_pln = a.cnsctvo_cdgo_pln 


--Actualizacion de los estados que tienen que ver con SIPRES

UPDATE pres 
SET pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	pres.rgstra_lg = a.rgstra_lg, 
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb,
	pres.rprgrma_fcha_rspsta = a.rprgrma_fcha_rspsta,
	pres.obsrvcn_afldo = a.obsrvcn,
	pres.estdo_abrto_crrdo	= CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END	
	, pres.estdo_prstcn_ss = a.estdo_prstcn_ss /* jh*/						  
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_sn_lqdr = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.fnte = @fuenteSipres
AND a.vncda != @vncda
AND pres.cnsctvo_cdgo_ofcna is not null
AND pres.nuam is not null
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca

--Actualizacion de los estados que tienen que ver con 3047

UPDATE pres 
SET pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	rgstra_lg = a.rgstra_lg,
	pres.obsrvcn_afldo = a.obsrvcn,
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb,
	pres.rprgrma_fcha_rspsta = a.rprgrma_fcha_rspsta,
	estdo_abrto_crrdo = CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END 
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_3047 = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.fnte = @fuente3047
AND a.vncda != @vncda
AND a.csl_dvlcn != @csa_dvlcn_capita
AND (pres.csa_dvlcn is null or pres.csa_dvlcn != @csa_dvlcn_capita)
AND pres.cnsctvo_cdgo_ofcna is null
AND pres.nuam is null
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca

--Actualizacion de los estados que tienen que ver con 3047 y estan devueltos por capitacion

UPDATE pres 
SET pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	rgstra_lg = a.rgstra_lg,
	pres.obsrvcn_afldo = a.obsrvcn,
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb,
	pres.rprgrma_fcha_rspsta = a.rprgrma_fcha_rspsta,
	estdo_abrto_crrdo = CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END 
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON (pres.estdo_3047 = a.estdo_prstcn_ss)
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE pres.cnsctvo_cdgo_ofcna is null
AND pres.nuam is null
AND pres.csa_dvlcn = @csa_dvlcn_capita
AND a.csl_dvlcn = @csa_dvlcn_capita
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca

--Se eliminan todos los registros que no hayan sido homologados con la tabla parametro
DELETE FROM #prestacionesMiSolicitud WHERE estdo_hmlgdo_afldo IS NULL

--Actualizacion del campo de la fecha problable de entrega. tiene en cuenta todos los casos excepto el caso 4 ya que para este caso debe 
--calcular la fecha de cualquiera de los lo otros y si el 4 esta seteado le debe sumar a lo que calculo esto se hace para el caso de SIPRES

UPDATE pres 
SET tpo_clclo_fcha = a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco,
	pres.cnsctvo_vgnca_cnslta_prstcns_wb = a.cnsctvo_vgnca_cnslta_prstcns_wb,
	estdo_abrto_crrdo	= CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END, 
	fcha_prbble_rspsta	= (CASE WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaImpr THEN CONVERT(varchar, pres.fcha_imprsn,103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaProgram THEN CONVERT(varchar, pres.fcha_incl_imprsn, 103)+' - '+CONVERT(varchar, pres.fcha_fnl_imprsn, 103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaParam THEN CONVERT(varchar, (pres.fcha+isnull(pres.ds_prgrmcn_fcha_prbble,0)),103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaNoMostrar THEN NULL
						  END),
	fcha_prbble_rspsta_date = CASE WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaImpr THEN pres.fcha_imprsn
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaParam THEN pres.fcha+isnull(pres.ds_prgrmcn_fcha_prbble,0)
							   ELSE NULL
							  END
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_sn_lqdr = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca
AND a.fnte = @fuenteSIPRES

--Actualizacion del campo de la fecha problable de entrega. tiene en cuenta todos los casos excepto el caso 4 ya que para este caso debe 
--calcular la fecha de cualquiera de los lo otros y si el 4 esta seteado le debe sumar a lo que calculo esto se hace para el caso de 3047

UPDATE pres 
SET tpo_clclo_fcha = a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco,
	pres.cnsctvo_vgnca_cnslta_prstcns_wb = a.cnsctvo_vgnca_cnslta_prstcns_wb,
	estdo_abrto_crrdo	= CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END, 
	fcha_prbble_rspsta	= (CASE WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaImpr THEN CONVERT(varchar, pres.fcha_imprsn,103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaProgram THEN CONVERT(varchar, pres.fcha_incl_imprsn, 103)+' - '+CONVERT(varchar, pres.fcha_fnl_imprsn, 103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaParam THEN CONVERT(varchar, (pres.fcha+isnull(pres.ds_prgrmcn_fcha_prbble,0)),103)
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaNoMostrar THEN NULL
						  END),
	fcha_prbble_rspsta_date = CASE WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaImpr THEN pres.fcha_imprsn
							   WHEN a.cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco = @calculoFechaParam THEN pres.fcha+isnull(pres.ds_prgrmcn_fcha_prbble,0)
							   ELSE NULL
							  END
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_3047 = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca
AND a.fnte = @fuente3047



update pres
set	pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	rgstra_lg = a.rgstra_lg,
	pres.obsrvcn_afldo = a.obsrvcn,
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb,
	pres.rprgrma_fcha_rspsta = a.rprgrma_fcha_rspsta,
	estdo_abrto_crrdo = CASE WHEN a.abrto = @variable_si THEN @abierto ELSE @cerrado END 
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.cnsctvo_cdgo_estdo_atrzcn= a.estdo_prstcn_ss
	inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE pres.cnsctvo_cdgo_estdo_atncn=@marcadescarga  and pres.estdo_prstcn_ss!=@estadoImpreso  -- valida que la autorizacion este marcada para descargar 
and  @fechaActual between a.inco_vgnca and a.fn_vgnca 
AND @fechaActual between d.inco_vgnca and d.fn_vgnca

--se realizar este update para el caso de aplicar la regla de sumar el numero de dias a la fecha probable calculada en el paso anterior, no tiene en cuenta 
--el rango de fechas de programacion para el caso de SIPRES

UPDATE pres 
SET pres.fcha_prbble_rspsta	= CONVERT(varchar,DATEADD(day,isnull(@valorDiasRespuesta,0),fcha_prbble_rspsta_date),103)
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_sn_lqdr = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.aplca_ds_rgla = @aplca_ds_rgla
AND cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco <> @calculoFechaProgram 
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca
AND fcha_prbble_rspsta_date is not null
AND a.fnte = @fuenteSIPRES

--se realizar este update para el caso de aplicar la regla de sumar el numero de dias a la fecha probable calculada en el paso anterior, no tiene en cuenta 
--el rango de fechas de programacion para el caso de SIPRES

UPDATE pres 
SET pres.fcha_prbble_rspsta	= CONVERT(varchar,DATEADD(day,isnull(@valorDiasRespuesta,0),fcha_prbble_rspsta_date),103)
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON pres.estdo_sn_lqdr = a.estdo_prstcn_ss
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.aplca_ds_rgla = @aplca_ds_rgla
AND cnsctvo_cdgo_clclo_fcha_dspnbldd_srvco <> @calculoFechaProgram 
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca
AND fcha_prbble_rspsta_date is not null
AND a.fnte = @fuenteSIPRES

--Se actualiza los estados de la tabla temporal por las configuraciones de las que esten vencidas para SIPRES

UPDATE pres 
SET pres.obsrvcn_afldo = a.obsrvcn,
	pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON (pres.estdo_sn_lqdr = a.estdo_prstcn_ss)
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.vncda = @vncda
AND a.fnte = @fuenteSIPRES
AND fcha_prbble_rspsta_date < @fechaActual
AND fcha_prbble_rspsta_date is not null
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca 

--Se actualiza los estados de la tabla temporal por las configuraciones de las que esten vencidas para 3047

UPDATE pres 
SET pres.obsrvcn_afldo = a.obsrvcn,
	pres.estdo_hmlgdo_afldo = c.dscrpcn_estdo_cnslta_wb,
	pres.cnsctvo_cdgo_estdo_hmlgdo_afldo = c.cnsctvo_cdgo_estdo_cnslta_wb
FROM #prestacionesMiSolicitud pres 
	 inner join cpw.tbCPWConsultaPrestacionesWeb_vigencias a with(nolock) ON (pres.estdo_3047 = a.estdo_prstcn_ss)
	 inner join	cpw.tbCPWConsultaPrestacionesWeb b with(nolock) ON a.cnsctvo_cdgo_cnslta_prstcns_wb = b.cnsctvo_cdgo_cnslta_prstcns_wb
	 inner join cpw.tbCPWEstadosConsultaWeb c with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = a.cnsctvo_cdgo_estdo_cnslta_wb
	 inner join cpw.tbCPWEstadosConsultaWeb_vigencias d with(nolock) ON c.cnsctvo_cdgo_estdo_cnslta_wb = d.cnsctvo_cdgo_estdo_cnslta_wb
WHERE a.vncda = @vncda
AND a.fnte = @fuente3047
AND fcha_prbble_rspsta_date < @fechaActual
AND fcha_prbble_rspsta_date is not null
AND @fechaActual between a.inco_vgnca and a.fn_vgnca
AND @fechaActual between d.inco_vgnca and d.fn_vgnca 


--Calculo de la regla a la fecha de respuesta calculada anteriormente (suma de dias a la fecha ya calculada) solo aplica para las 
--fechas que no son rango de fechas.

UPDATE pres 
SET pres.fcha_prbble_rspsta = @fechaRecalculada
FROM #prestacionesMiSolicitud pres
WHERE pres.rprgrma_fcha_rspsta = @rprgrma_fcha_rspsta
AND fcha_prbble_rspsta_date < @fechaActual
AND pres.tpo_clclo_fcha != @calculoFechaProgram
AND fcha_prbble_rspsta_date is not null

--Se actualiza la informacion adicional de las IPS ya sea que capiten o que esten impresas.

UPDATE pres 
SET pres.txto_ips = a.nmbre_scrsl,
	pres.tlfno_ips = a.tlfno,
	pres.drccn_ips = a.drccn
FROM #prestacionesMiSolicitud pres 
	 inner join bdsisalud.dbo.tbdireccionesprestador a with(nolock) ON pres.ips_mstrr = a.cdgo_intrno
Where (pres.csa_dvlcn is null or pres.csa_dvlcn != @csa_dvlcn_capita)

--Actualizacion de la observacion cuando encuentra el campo IPS primaria en la observacion. (nombre IPS)

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@ips_primaria,@nom_ips_observacion),obsrvcn_afldo)
From #prestacionesMiSolicitud pres
WHERE obsrvcn_afldo like '%'+@ips_primaria+'%'

--Actualizacion de la observacion cuando encuentra el campo IPS primaria en la observacion (direccion IPS).

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@dir_Prestador,@dire_ips_obser),obsrvcn_afldo)
From #prestacionesMiSolicitud pres
WHERE obsrvcn_afldo like '%'+@ips_primaria+'%'

--Actualizacion de la observacion cuando encuentra el campo IPS primaria en la observacion (telefono IPS).

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@tel_prestador,@telefono_ips_obser),obsrvcn_afldo)
From #prestacionesMiSolicitud pres
WHERE obsrvcn_afldo like '%'+@ips_primaria+'%'

--Cambio de observacaciones para el caso de que sea cuando la IPS esta devuelta por capitacion y/o este impresa ya 
--que estos se guardaron en la tabla temporal.

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@ips_capita,ltrim(rtrim(txto_ips))),obsrvcn_afldo)
From #prestacionesMiSolicitud pres
WHERE obsrvcn_afldo like '%'+@ips_capita+'%'


--Actualizacion para el caso de la ips en donde fue impresa la solicitud.

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@pres_destino,ltrim(rtrim(txto_ips))),obsrvcn_afldo)
From #prestacionesMiSolicitud pres
WHERE obsrvcn_afldo like '%'+@pres_destino+'%'

--Actualizaciones de direccion y telefono del prestador esto se hace por fila 
--Este reemplazo se hace en todas las filas sin importar que

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@dir_Prestador,ltrim(rtrim(drccn_ips))),obsrvcn_afldo)
From #prestacionesMiSolicitud pres

Update pres
Set obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@tel_prestador,ltrim(rtrim(tlfno_ips))),obsrvcn_afldo)
From #prestacionesMiSolicitud pres

UPDATE #prestacionesMiSolicitud
SET obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@fecha_programacion,fcha_prbble_rspsta),obsrvcn_afldo)

UPDATE #prestacionesMiSolicitud
SET obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@fecha_actual,@fechaActual),obsrvcn_afldo)
	
UPDATE #prestacionesMiSolicitud
SET obsrvcn_afldo = isnull(replace(obsrvcn_afldo,@fecha_probable,fcha_prbble_rspsta),obsrvcn_afldo)

-- actualiza en la tabla temporal el copago y la cuota moderadora de MEGA directamente
Update     #prestacionesMiSolicitud
Set        cpgo_pgo         = r.cpgo_pgo, 
           cta_mdrdra       = r.cta_mdrdra, 
		   fcha_utlzcn_hsta = d.fcha_utlzcn_hsta
FROM       #prestacionesMiSolicitud b
inner join bdsisalud..tbconceptosops d with(nolock) ON b.cnsctvo_cdgo_ofcna = d.cnsctvo_cdgo_ofcna and b.nuam = d.nuam
left  join bdsisalud..tbDetalleCuotasRecuperacion r ON b.cnsctvo_cdgo_ofcna=r.cnsctvo_cdgo_ofcna AND b.nuam=r.nuam

-- actualiza en la tabla temporal el copago y la cuota moderadora de MEGA directamente
Update #prestacionesMiSolicitud
set    cta_mdrdra = (select     Sum(c.vlr_lqdcn_cta_rcprcn_srvco_ops) 
					 FROM       #prestacionesMiSolicitud b with(nolock) 
					 inner join bdsisalud.dbo.tbactua a 
					 on         a.cnsctvo_cdgo_ofcna = b.cnsctvo_cdgo_ofcna And 
					            a.nuam = b.nuam
					 inner join bdCNA.gsa.tbASConsolidadoCuotaRecuperacionxOps c 
					 On         c.cnsctvo_slctd_atrzcn_srvco = a.cnsctvo_slctd_mga
					 where      cnsctvo_cdgo_cncpto_pgo = 1 --@cnsctvo_cdgo_cncpto_pgo1  -- Cuota moderadora
					 group by   a.cnsctvo_slctd_mga)
Where cta_mdrdra = 0 Or cta_mdrdra is null

-- actualiza en la tabla temporal el copago y la cuota moderadora de MEGA directamente
Update #prestacionesMiSolicitud
set cpgo_pgo = (select sum(c.vlr_lqdcn_cta_rcprcn_srvco_ops) 
				FROM       #prestacionesMiSolicitud b with(nolock) 
				inner join bdsisalud.dbo.tbactua a 
				on         a.cnsctvo_cdgo_ofcna =b.cnsctvo_cdgo_ofcna And 
				           a.nuam = b.nuam
				inner join bdCNA.gsa.tbASConsolidadoCuotaRecuperacionxOps c 
				On         c.cnsctvo_slctd_atrzcn_srvco=a.cnsctvo_slctd_mga
				where      cnsctvo_cdgo_cncpto_pgo = 2--@cnsctvo_cdgo_cncpto_pgo2  -- Copago
				group by   a.cnsctvo_slctd_mga)
where cpgo_pgo = 0 Or cpgo_pgo is null

--Registro en el log las prestaciones que deban registrarse en el log.
INSERT INTO cpw.tbCPWDetLogConsultaPrestaciones(
	cnsctvo_lg_cnslta_prstcns
	,cnsctvo_slctd_prstcn
	,cnsctvo_slctd_ops
	,cnsctvo_cdfccn
	,fcha_dsde_prbble_rspsta
	,fcha_fn_prbble_rspsta
	,cnsctvo_cdgo_estdo_cnslta_wb
	,obsrvcn_afldo
	,nuam
	,cnsctvo_cdgo_ofcna
)
SELECT DISTINCT
	   @consecutivoLog,
	   pres.cnsctvo_slctd_prstcn,
	   pres.cnsctvo_slctd_ops,
	   pres.cnsctvo_cdfccn,
	   (CASE WHEN pres.fcha_incl_imprsn is not null THEN pres.fcha_incl_imprsn ELSE fcha_prbble_rspsta_date END ),
	   (CASE WHEN pres.fcha_fnl_imprsn is not null THEN pres.fcha_fnl_imprsn ELSE NULL END ),
	   pres.cnsctvo_cdgo_estdo_hmlgdo_afldo,
	   pres.obsrvcn_afldo,
	   pres.nuam,
	   pres.cnsctvo_cdgo_ofcna
FROM #prestacionesMiSolicitud pres
WHERE rgstra_lg = @rgstra_lg
and fcha_prbble_rspsta_date is not null

--Select *
--from #prestacionesMiSolicitud
IF @flag = 1
BEGIN 
SELECT DISTINCT
		tpo_clclo_fcha,
		fcha,
		dscrpcn_cdfccn,
		tpo_srvco,
		estdo_hmlgdo_afldo,
		estdo_abrto_crrdo,
		fcha_prbble_rspsta,
		(txto_ips+isnull(' - '+drccn_ips+' - '+tlfno_ips,'')) as txto_ips,
		obsrvcn_afldo,
		fcha_utlzcn_hsta,
		isnull(cpgo_pgo,0) as cpgo_pgo,
	    isnull(cta_mdrdra,0) as cta_mdrdra,
		cnsctvo_slctd_ops,
		cnsctvo_slctd_prstcn,
		cnsctvo_cdfccn,
		nuam,
		cnsctvo_cdgo_ofcna,
		cnsctvo_cdgo_estdo_atncn, /* jh*/
		estdo_prstcn_ss /* jh*/
		,null [cnsctvo_prslctd]
FROM #prestacionesMiSolicitud
UNION ALL
SELECT 
		null tpo_clclo_fcha,
		pres.[fcha_crcn] fcha,
		null dscrpcn_cdfccn,
		null tpo_srvco,
		est.[dscrpcn_estdo_prstcn_slctd] as estdo_hmlgdo_afldo,
		null estdo_abrto_crrdo,
		null fcha_prbble_rspsta,
		null txto_ips,
		pres.[obsrvcns] as obsrvcn_afldo,
		null fcha_utlzcn_hsta,
		null cpgo_pgo,
	    null cta_mdrdra,
		null cnsctvo_slctd_ops,
		null cnsctvo_slctd_prstcn,
		neg.[cnsctvo_cdfccn] cnsctvo_cdfccn,
		null nuam,
		null cnsctvo_cdgo_ofcna,
		pres.[cnsctvo_cdgo_estdo_prslctd] as cnsctvo_cdgo_estdo_atncn,
		null estdo_prstcn_ss /* jh*/
		,pres.[cnsctvo_prslctd]
	  from BDCna.dbo.tbPreSolicitudes pres With(NoLock)
	  INNER JOIN BDSisalud.dbo.tbSolicitudOPSEstadosPrestacion est With(NoLock) on
	  est.cnsctvo_cdgo_estdo_prstcn_slctd = pres.cnsctvo_cdgo_estdo_prslctd
	  INNER JOIN  BDAfiliacionValidador.dbo.tbPlanes_Vigencias afi With(NoLock) on
	  afi.cnsctvo_cdgo_pln = pres.cnsctvo_cdgo_pln
	  INNER JOIN  Bdcna.dbo.tbOrigenesSolicitudesSalud_Vigencias ori With(NoLock) on
	  ori.cnsctvo_cdgo_orgn_slctd_sld = pres.cnsctvo_cdgo_orgn_prslctd
	  LEFT JOIN   [BDCna].[dbo].[tbPrestacionesNegadasPreSolicitud] neg With(NoLock) on
	  neg.[cnsctvo_prslctd] = pres.[cnsctvo_prslctd]	
	  LEFT JOIN   [BDCna].[dbo].[tbPrestacionesAutorizadasPreSolicitud] AUT With(NoLock) on
	  AUT.[cnsctvo_prslctd] = pres.[cnsctvo_prslctd]	
	  where pres.[nmro_unco_idntfccn_afldo] =    @nui and
	  getdate() between afi.[inco_vgnca] and afi.[fn_vgnca] and
	  getdate() between ori.[inco_vgnca] and ori.[fn_vgnca] 
	  and pres.cnsctvo_cdgo_estdo_prslctd not in (@cnsctvo_estdo_prslctd_crrdo,@cnsctvo_estdo_prslctd_anldo)   -- nO muestra las cerradas, ni las anuladas
	  and neg.[cnsctvo_prslctd] is null
	  and AUT.[cnsctvo_prslctd] is null
union all
SELECT 
		null tpo_clclo_fcha,
		pres.[fcha_crcn] fcha,
		'' dscrpcn_cdfccn,
		null tpo_srvco,
		@dscrpcn_estdo_no_atrzdo,
		null estdo_abrto_crrdo,
		null fcha_prbble_rspsta,
		null txto_ips,
		@obsrvcn_no_atrzdo,
		null fcha_utlzcn_hsta,
		null cpgo_pgo,
	    null cta_mdrdra,
		null cnsctvo_slctd_ops,
		null cnsctvo_slctd_prstcn,
		neg.[cnsctvo_cdfccn] cnsctvo_cdfccn,
		null nuam,
		null cnsctvo_cdgo_ofcna,
		pres.[cnsctvo_cdgo_estdo_prslctd],
		null estdo_prstcn_ss /* jh*/
		,pres.[cnsctvo_prslctd]
	  from BDCna.dbo.tbPreSolicitudes pres With(NoLock)
	  INNER JOIN BDSisalud.dbo.tbSolicitudOPSEstadosPrestacion est With(NoLock) on
	  est.cnsctvo_cdgo_estdo_prstcn_slctd = pres.cnsctvo_cdgo_estdo_prslctd
	  INNER JOIN  BDAfiliacionValidador.dbo.tbPlanes_Vigencias afi With(NoLock) on
	  afi.cnsctvo_cdgo_pln = pres.cnsctvo_cdgo_pln
	  INNER JOIN  Bdcna.dbo.tbOrigenesSolicitudesSalud_Vigencias ori With(NoLock) on
	  ori.cnsctvo_cdgo_orgn_slctd_sld = pres.cnsctvo_cdgo_orgn_prslctd
	  inner JOIN   [BDCna].[dbo].[tbPrestacionesNegadasPreSolicitud] neg With(NoLock) on
	  neg.[cnsctvo_prslctd] = pres.[cnsctvo_prslctd]	
	  --inner JOIN   [bdSisalud].[dbo].[tbCodificaciones] cod With(NoLock) on
	  --neg.[cnsctvo_cdfccn] = cod.[cnsctvo_cdfccn]
	  where pres.[nmro_unco_idntfccn_afldo] =   @nui and
	  getdate() between afi.[inco_vgnca] and afi.[fn_vgnca] and
	  getdate() between ori.[inco_vgnca] and ori.[fn_vgnca] 
union all
SELECT 
		null tpo_clclo_fcha,
		pres.[fcha_crcn] fcha,
		cod.[dscrpcn_cdfccn] dscrpcn_cdfccn,
		null tpo_srvco,
		@dscrpcn_estdo_no_atrzdo,
		null estdo_abrto_crrdo,
		null fcha_prbble_rspsta,
		null txto_ips,
		@obsrvcn_no_atrzdo,
		null fcha_utlzcn_hsta,
		null cpgo_pgo,
	    null cta_mdrdra,
		null cnsctvo_slctd_ops,
		null cnsctvo_slctd_prstcn,
		neg.[cnsctvo_cdfccn] cnsctvo_cdfccn,
		null nuam,
		null cnsctvo_cdgo_ofcna,
		pres.[cnsctvo_cdgo_estdo_prslctd],
		null estdo_prstcn_ss /* jh*/
		,pres.[cnsctvo_prslctd]
	  from BDCna.dbo.tbPreSolicitudes pres With(NoLock)
	  INNER JOIN BDSisalud.dbo.tbSolicitudOPSEstadosPrestacion est With(NoLock) on
	  est.cnsctvo_cdgo_estdo_prstcn_slctd = pres.cnsctvo_cdgo_estdo_prslctd
	  INNER JOIN  BDAfiliacionValidador.dbo.tbPlanes_Vigencias afi With(NoLock) on
	  afi.cnsctvo_cdgo_pln = pres.cnsctvo_cdgo_pln
	  INNER JOIN  Bdcna.dbo.tbOrigenesSolicitudesSalud_Vigencias ori With(NoLock) on
	  ori.cnsctvo_cdgo_orgn_slctd_sld = pres.cnsctvo_cdgo_orgn_prslctd
	  inner JOIN   [BDCna].[dbo].[tbPrestacionesNegadasPreSolicitud] neg With(NoLock) on
	  neg.[cnsctvo_prslctd] = pres.[cnsctvo_prslctd]	
	  inner JOIN   [bdSisalud].[dbo].[tbCodificaciones] cod With(NoLock) on
	  neg.[cnsctvo_cdfccn] = cod.[cnsctvo_cdfccn]
	  where pres.[nmro_unco_idntfccn_afldo] =   @nui and
	  getdate() between afi.[inco_vgnca] and afi.[fn_vgnca] and
	  getdate() between ori.[inco_vgnca] and ori.[fn_vgnca] 

ORDER BY fcha DESC
END

ELSE
BEGIN 
SELECT DISTINCT
		tpo_clclo_fcha,
		fcha,
		dscrpcn_cdfccn,
		tpo_srvco,
		estdo_hmlgdo_afldo,
		estdo_abrto_crrdo,
		fcha_prbble_rspsta,
		(txto_ips+isnull(' - '+drccn_ips+' - '+tlfno_ips,'')) as txto_ips,
		obsrvcn_afldo,
		fcha_utlzcn_hsta,
		isnull(cpgo_pgo,0) as cpgo_pgo,
	    isnull(cta_mdrdra,0) as cta_mdrdra,
		cnsctvo_slctd_ops,
		cnsctvo_slctd_prstcn,
		cnsctvo_cdfccn,
		nuam,
		cnsctvo_cdgo_ofcna,
		cnsctvo_cdgo_estdo_atncn, /* jh*/
		estdo_prstcn_ss /* jh*/
FROM #prestacionesMiSolicitud
ORDER BY fcha DESC
END 

DROP TABLE #prestacionesMiSolicitud
DROP TABLE #prestacionesMiSolicitudConceptoDeGasto
DROP TABLE #prestacionesMiSolicitudUnConceptoPpal
DROP TABLE #prestaciones