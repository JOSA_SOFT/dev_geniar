USE BDCna
GO

ALTER TABLE cja.tbRCEstadosDocumentosCaja_Vigencias
ADD cnsctvo_cdgo_tpo_dcmnto_cja udtConsecutivo NULL
GO

ALTER TABLE cja.tbRCEstadosDocumentosCaja_Vigencias ADD CONSTRAINT FK_tbRCEstadosDocumentosCaja_Vigencias_tbRCTiposDocumentosCaja
	FOREIGN KEY (cnsctvo_cdgo_tpo_dcmnto_cja) REFERENCES cja.tbRCTiposDocumentosCaja (cnsctvo_cdgo_tpo_dcmnto_cja) ON DELETE No Action ON UPDATE No Action
GO

UPDATE cja.tbRCEstadosDocumentosCaja_Vigencias
SET cnsctvo_cdgo_tpo_dcmnto_cja = 1
WHERE cnsctvo_vgnca_estdo_dcmnto_cja = 1
GO

UPDATE cja.tbRCEstadosDocumentosCaja_Vigencias
SET cnsctvo_cdgo_tpo_dcmnto_cja = 2
WHERE cnsctvo_vgnca_estdo_dcmnto_cja = 2
OR cnsctvo_vgnca_estdo_dcmnto_cja = 3
GO

ALTER TABLE cja.tbRCEstadosDocumentosCaja_Vigencias
ALTER COLUMN cnsctvo_cdgo_tpo_dcmnto_cja udtConsecutivo NOT NULL
GO
