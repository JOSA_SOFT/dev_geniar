USE [BDCna]
GO

ALTER TABLE gsa.tbASSolicitudesAutorizacionServicios
Add  cnsctvo_cdgo_tpo_orgn_slctd UdtConsecutivo not null default 1

GO

ALTER TABLE gsa.tbASSolicitudesAutorizacionServicios  WITH CHECK ADD  CONSTRAINT [FK_tbASSolicitudesAutorizacionServicios_tbAsTipoOrigenSolicitud] FOREIGN KEY([cnsctvo_cdgo_tpo_orgn_slctd])
REFERENCES prm.tbAsTipoOrigenSolicitud([cnsctvo_cdgo_tpo_orgn_slctd])
GO 
