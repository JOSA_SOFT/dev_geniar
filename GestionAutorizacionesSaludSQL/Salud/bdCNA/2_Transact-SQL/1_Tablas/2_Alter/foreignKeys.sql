USE [bdcna]
GO

/* Create Foreign Key Constraints */

ALTER TABLE cja.[tbRCDatosContacto] ADD CONSTRAINT [FK_tbDatosContacto_tbBancos]
	FOREIGN KEY ([cnsctvo_cdgo_bnco]) REFERENCES cja.[tbRCBancos] ([cnsctvo_cdgo_bnco]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDatosContacto] ADD CONSTRAINT [FK_tbDatosContacto_tbMetodosDevolucionDinero]
	FOREIGN KEY ([cnsctvo_cdgo_mtdo_dvlcn_dnro]) REFERENCES cja.[tbRCMetodosDevolucionDinero] ([cnsctvo_cdgo_mtdo_dvlcn_dnro]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDenominaciones_Vigencias] ADD CONSTRAINT [FK_tbDenominaciones_Vigencias_tbDenominaciones]
	FOREIGN KEY ([cnsctvo_cdgo_dnmncn]) REFERENCES cja.[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDenominaciones_Vigencias] ADD CONSTRAINT [FK_tbDenominaciones_Vigencias_tbTiposDenominacion]
	FOREIGN KEY ([cnsctvo_cdgo_tpo_dnmncn]) REFERENCES cja.[tbRCTiposDenominacion] ([cnsctvo_cdgo_tpo_dnmncn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDetalleDenominaciones] ADD CONSTRAINT [FK_tbDetalleDenominaciones_tbDenominaciones]
	FOREIGN KEY ([cnsctvo_cdgo_dnmncn]) REFERENCES cja.[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDetalleDenominaciones] ADD CONSTRAINT [FK_tbDetalleDenominaciones_tbMovimientosCaja]
	FOREIGN KEY ([cnsctvo_mvmnto_cja]) REFERENCES cja.[tbRCMovimientosCaja] ([cnsctvo_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCEstadosDocumentosCaja_Vigencias] ADD CONSTRAINT [FK_tbEstadosDocumentosCaja_Vigencias_tbEstadosDocumentosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_estdo_dcmnto_cja]) REFERENCES cja.[tbRCEstadosDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCEstadosMovimientosCaja_Vigencias] ADD CONSTRAINT [FK_tbEstadosMovimientosCaja_Vigencias_tbEstadosMovimientosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_estdo]) REFERENCES cja.[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCHistoricoEstadosMovimientosCaja] ADD CONSTRAINT [FK_tbHistoricoEstadosMovimientosCaja_tbEstadosMovimientosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_estdo_mvmnto_cja]) REFERENCES cja.[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCHistoricoEstadosMovimientosCaja] ADD CONSTRAINT [FK_tbHistoricoEstadosMovimientosCaja_tbMovimientosCaja]
	FOREIGN KEY ([cnsctvo_mvmnto_cja]) REFERENCES cja.[tbRCMovimientosCaja] ([cnsctvo_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMetodosDevolucionDinero_Vigencias] ADD CONSTRAINT [FK_tbMetodosDevolucionDinero_Vigencias_tbMetodosDevolucionDinero]
	FOREIGN KEY ([cnsctvo_cdgo_mtdo_dvlcn_dnro]) REFERENCES cja.[tbRCMetodosDevolucionDinero] ([cnsctvo_cdgo_mtdo_dvlcn_dnro]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCajaxCausalesDescuadre] ADD CONSTRAINT [FK_tbMovimientosCajaxCausalesDescuadre_tbCausalesDescuadre]
	FOREIGN KEY ([cnsctvo_cdgo_csl_dscdre]) REFERENCES cja.[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCajaxCausalesDescuadre] ADD CONSTRAINT [FK_tbMovimientosCajaxCausalesDescuadre_tbMovimientosCaja]
	FOREIGN KEY ([cnsctvo_mvmnto_cja]) REFERENCES cja.[tbRCMovimientosCaja] ([cnsctvo_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCTiposDenominacion_Vigencias] ADD CONSTRAINT [FK_tbTiposDenominacion_Vigencias_tbTiposDenominacion]
	FOREIGN KEY ([cnsctvo_cdgo_tpo_dnmncn]) REFERENCES cja.[tbRCTiposDenominacion] ([cnsctvo_cdgo_tpo_dnmncn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCTiposDocumentosCaja_Vigencias] ADD CONSTRAINT [FK_tbTiposDocumentos_Vigencias_tbTiposDocumentos]
	FOREIGN KEY ([cnsctvo_cdgo_tpo_dcmnto_cja]) REFERENCES cja.[tbRCTiposDocumentosCaja] ([cnsctvo_cdgo_tpo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCBancos_Vigencias] ADD CONSTRAINT [FK_tbBancos_Vigencias_tbBancos]
	FOREIGN KEY ([cnsctvo_cdgo_bnco]) REFERENCES cja.[tbRCBancos] ([cnsctvo_cdgo_bnco]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCCausalesDescuadre_Vigencias] ADD CONSTRAINT [FK_tbCausalesDescuadre_Vigencias_tbCausalesDescuadre]
	FOREIGN KEY ([cnsctvo_cdgo_csl_dscdre]) REFERENCES cja.[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCCierreOficina] ADD CONSTRAINT [FK_tbRCCierreOficina_tbRCCoordinadorAsixOficina]
	FOREIGN KEY ([cnsctvo_cdgo_crdndr_asi]) REFERENCES cja.[tbRCCoordinadorAsixOficina] ([cnsctvo_cdgo_crdndr_asi]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCaja] ADD CONSTRAINT [FK_tbMovimientosCaja_tbEstadosMovimientosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_estdo]) REFERENCES cja.[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCaja] ADD CONSTRAINT [FK_tbRCMovimientosCaja_tbRCCierreOficina]
	FOREIGN KEY ([cnsctvo_crre_ofcna]) REFERENCES  cja.[tbRCCierreOficina] ([cnsctvo_crre_ofcna]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCCoordinadorAsixOficina_Vigencias] ADD CONSTRAINT [FK_tbRCCoordinadorAsixOficina_Vigencias_tbRCCoordinadorAsixOficina]
	FOREIGN KEY ([cnsctvo_cdgo_crdndr_asi]) REFERENCES cja.[tbRCCoordinadorAsixOficina] ([cnsctvo_cdgo_crdndr_asi]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCSoportesDocumentosCaja] ADD CONSTRAINT [FK_tbRCSoportesDocumentosCaja_tbRCDocumentosCaja]
	FOREIGN KEY ([cnsctvo_dcmnto_cja]) REFERENCES cja.[tbRCDocumentosCaja] ([cnsctvo_cdgo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosRelacionados] ADD CONSTRAINT [FK_tbRCDocumentosRelacionados_tbRCDocumentosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_dcmnto_cja_rlcndo]) REFERENCES  cja.[tbRCDocumentosCaja] ([cnsctvo_cdgo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosCaja] ADD CONSTRAINT [FK_tbRCDocumentosCaja_tbRCDatosContacto]
	FOREIGN KEY ([cnsctvo_dto_cntcto]) REFERENCES  cja.[tbRCDatosContacto] ([cnsctvo_dto_cntcto]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosCaja] ADD CONSTRAINT [FK_tbRCDocumentosCaja_tbRCDetMovimientosCaja]
	FOREIGN KEY ([cnsctvo_dtlle_mvmnto_cja]) REFERENCES cja.[tbRCDetMovimientosCaja] ([cnsctvo_dtlle_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosCaja] ADD CONSTRAINT [FK_tbRCDocumentosCaja_tbRCTiposDocumentosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_tpo_dcmnto_cja]) REFERENCES cja.[tbRCTiposDocumentosCaja] ([cnsctvo_cdgo_tpo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosCaja] ADD CONSTRAINT [FK_tbRCNotasCredito_tbRCConceptosGeneracionNotasCredito]
	FOREIGN KEY ([cnsctvo_cdgo_cncpto_gnrcn]) REFERENCES cja.[tbRCConceptosGeneracionNotasCredito] ([cnsctvo_cdgo_cncpto_gnrcn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDocumentosCaja] ADD CONSTRAINT [FK_tbRCNotasCredito_tbRCEstadosDocumentosCaja]
	FOREIGN KEY ([cnsctvo_cdgo_estdo_dcmnto_cja]) REFERENCES cja.[tbRCEstadosDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCDetMovimientosCaja] ADD CONSTRAINT [FK_tbDetMovimientosCaja_tbMovimientosCaja]
	FOREIGN KEY ([cnsctvo_mvmnto_cja]) REFERENCES cja.[tbRCMovimientosCaja] ([cnsctvo_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] ADD CONSTRAINT [FK_tbRCConceptosGeneracionNotasCredito_Vigencias_tbRCConceptosGeneracionNotasCredito]
	FOREIGN KEY ([cnsctvo_cdgo_cncpto_gnrcn]) REFERENCES cja.[tbRCConceptosGeneracionNotasCredito] ([cnsctvo_cdgo_cncpto_gnrcn]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCajaxCausalesDescuadre] ADD CONSTRAINT [FK_tbMovimientosCajaxCausalesDescuadre_tbCausalesDescuadre]
	FOREIGN KEY ([cnsctvo_cdgo_csl_dscdre]) REFERENCES cja.[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre]) ON DELETE No Action ON UPDATE No Action
GO

ALTER TABLE cja.[tbRCMovimientosCajaxCausalesDescuadre] ADD CONSTRAINT [FK_tbMovimientosCajaxCausalesDescuadre_tbMovimientosCaja]
	FOREIGN KEY ([cnsctvo_mvmnto_cja]) REFERENCES cja.[tbRCMovimientosCaja] ([cnsctvo_mvmnto_cja]) ON DELETE No Action ON UPDATE No Action
GO