Use BdCna
Go
	
-- Crea el campo en la tabla tbASTipoMarcaServiciosSolicitados_Vigencias
Alter Table BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias Add cnsctvo_cdgo_tps_mrcs_ops udtConsecutivo Null

-- Crea llave foranea
	
ALTER TABLE [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias]  WITH NOCHECK ADD  CONSTRAINT [FK_tbASTipoMarcaServiciosSolicitados_Vigencias_tbTiposMarcasOPS] FOREIGN KEY([cnsctvo_cdgo_tps_mrcs_ops])
REFERENCES [dbo].[tbTiposMarcasOPS] ([cnsctvo_cdgo_tps_mrcs_ops])

GO

ALTER TABLE [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] NOCHECK CONSTRAINT [FK_tbASTipoMarcaServiciosSolicitados_Vigencias_tbTiposMarcasOPS]
GO