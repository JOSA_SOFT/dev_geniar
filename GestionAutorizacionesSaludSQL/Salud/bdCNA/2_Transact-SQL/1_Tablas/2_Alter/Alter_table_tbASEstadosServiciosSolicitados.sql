USE bdCNA
GO

ALTER TABLE prm.tbASEstadosServiciosSolicitados
ADD mrca_estdo_trnstro udtLogico;

ALTER TABLE prm.tbASEstadosServiciosSolicitados_Vigencias
ADD mrca_estdo_trnstro udtLogico;