Use [BDCna]

Go

Alter Table BDCna.prm.tbASCausaNovedad_Vigencias Drop column tme_stmp 

Alter Table BDCna.prm.tbASCausaNovedad_Vigencias Add  rqre_nmro_atrzcn_ips CHAR(1) DEFAULT 'N' not null

Alter Table BDCna.prm.tbASCausaNovedad_Vigencias Add tme_stmp timestamp not null

GO