	
	Use bdCNA

	Go


	--
	Alter Table gsa.tbASServiciosSolicitados
	Add cnsctvo_cdgo_rcbro udtConsecutivo Null

	--
	Alter Table gsa.tbASServiciosSolicitadosOriginal
	Add cdgo_rcbro Char(3) Null

	--
	Update		ss
	Set			cnsctvo_cdgo_rcbro = dsas.cnsctvo_cdgo_rcbro
	From		gsa.tbASServiciosSolicitados ss With(RowLock)
	Inner Join	gsa.tbASDiagnosticosSolicitudAutorizacionServicios dsas With(NoLock)
	On			dsas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco

	--
	Update		ss
	Set			cdgo_rcbro = dsas.cdgo_rcbro
	From		gsa.tbASServiciosSolicitadosOriginal ss With(RowLock)
	Inner Join	gsa.tbASDiagnosticosSolicitudAutorizacionServiciosOriginal dsas With(NoLock)
	On			dsas.cnsctvo_slctd_atrzcn_srvco = ss.cnsctvo_slctd_atrzcn_srvco
