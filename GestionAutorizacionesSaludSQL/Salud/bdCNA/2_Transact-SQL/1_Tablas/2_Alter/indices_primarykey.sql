USE [bdcna]
GO

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE cja.[tbRCDatosContacto] 
 ADD CONSTRAINT [PK_tbDatosContacto]
	PRIMARY KEY CLUSTERED ([cnsctvo_dto_cntcto])
GO

CREATE INDEX [IXFK_tbDatosContacto_tbBancos] 
 ON cja.[tbRCDatosContacto] ([cnsctvo_cdgo_bnco] ASC)
GO

CREATE INDEX [IXFK_tbDatosContacto_tbMetodosDevolucionDinero] 
 ON cja.[tbRCDatosContacto] ([cnsctvo_cdgo_mtdo_dvlcn_dnro] ASC)
GO

ALTER TABLE cja.[tbRCDenominaciones] 
 ADD CONSTRAINT [PK_tbDenominaciones]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_dnmncn])
GO

ALTER TABLE cja.[tbRCDenominaciones_Vigencias] 
 ADD CONSTRAINT [PK_tbDenominacionesVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_dnmncn])
GO

CREATE INDEX [IXFK_tbDenominaciones_Vigencias_tbDenominaciones] 
 ON cja.[tbRCDenominaciones_Vigencias] ([cnsctvo_cdgo_dnmncn] ASC)
GO

CREATE INDEX [IXFK_tbDenominaciones_Vigencias_tbTiposDenominacion] 
 ON cja.[tbRCDenominaciones_Vigencias] ([cnsctvo_cdgo_tpo_dnmncn] ASC)
GO

ALTER TABLE cja.[tbRCDetalleDenominaciones] 
 ADD CONSTRAINT [PK_tbDetalleDenominaciones]
	PRIMARY KEY CLUSTERED ([cnsctvo_dtlle_dnmncn])
GO

CREATE INDEX [IXFK_tbDetalleDenominaciones_tbDenominaciones] 
 ON cja.[tbRCDetalleDenominaciones] ([cnsctvo_cdgo_dnmncn] ASC)
GO

CREATE INDEX [IXFK_tbDetalleDenominaciones_tbMovimientosCaja] 
 ON cja.[tbRCDetalleDenominaciones] ([cnsctvo_mvmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCEstadosDocumentosCaja] 
 ADD CONSTRAINT [PK_tbEstadosDocumentosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_estdo_dcmnto_cja])
GO

ALTER TABLE cja.[tbRCEstadosDocumentosCaja_Vigencias] 
 ADD CONSTRAINT [PK_tbEstadosDocumentosCajaVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_estdo_dcmnto_cja])
GO

CREATE INDEX [IXFK_tbEstadosDocumentosCaja_Vigencias_tbEstadosDocumentosCaja] 
 ON cja.[tbRCEstadosDocumentosCaja_Vigencias] ([cnsctvo_cdgo_estdo_dcmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCEstadosMovimientosCaja] 
 ADD CONSTRAINT [PK_tbEstadosMovimientosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_estdo])
GO

ALTER TABLE cja.[tbRCEstadosMovimientosCaja_Vigencias] 
 ADD CONSTRAINT [PK_tbEstadosMovimientosCajaVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_estdo])
GO

CREATE INDEX [IXFK_tbEstadosMovimientosCaja_Vigencias_tbEstadosMovimientosCaja] 
 ON cja.[tbRCEstadosMovimientosCaja_Vigencias] ([cnsctvo_cdgo_estdo] ASC)
GO

ALTER TABLE cja.[tbRCHistoricoEstadosMovimientosCaja] 
 ADD CONSTRAINT [PK_tbHistoricoEstadosMovimientosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_hstrco_estdo_mvmnto_cja])
GO

CREATE INDEX [IXFK_tbHistoricoEstadosMovimientosCaja_tbEstadosMovimientosCaja] 
 ON cja.[tbRCHistoricoEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo_mvmnto_cja] ASC)
GO

CREATE INDEX [IXFK_tbHistoricoEstadosMovimientosCaja_tbMovimientosCaja] 
 ON cja.[tbRCHistoricoEstadosMovimientosCaja] ([cnsctvo_mvmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCMetodosDevolucionDinero] 
 ADD CONSTRAINT [PK_tbMetodosDevolucionDinero]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_mtdo_dvlcn_dnro])
GO

ALTER TABLE cja.[tbRCMetodosDevolucionDinero_Vigencias] 
 ADD CONSTRAINT [PK_tbMetodosDevolucionDineroVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_mtdo_dvlcn_dnro])
GO

CREATE INDEX [IXFK_tbMetodosDevolucionDinero_Vigencias_tbMetodosDevolucionDinero] 
 ON cja.[tbRCMetodosDevolucionDinero_Vigencias] ([cnsctvo_cdgo_mtdo_dvlcn_dnro] ASC)
GO

ALTER TABLE cja.[tbRCTiposDenominacion] 
 ADD CONSTRAINT [PK_tbTiposDenominacion]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_tpo_dnmncn])
GO

ALTER TABLE cja.[tbRCTiposDenominacion_Vigencias] 
 ADD CONSTRAINT [PK_tbTiposDenominacionVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_tpo_dnmncn])
GO

CREATE INDEX [IXFK_tbTiposDenominacion_Vigencias_tbTiposDenominacion] 
 ON cja.[tbRCTiposDenominacion_Vigencias] ([cnsctvo_cdgo_tpo_dnmncn] ASC)
GO

ALTER TABLE cja.[tbRCTiposDocumentosCaja] 
 ADD CONSTRAINT [PK_tbTiposDocumentos]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_tpo_dcmnto_cja])
GO

ALTER TABLE cja.[tbRCTiposDocumentosCaja_Vigencias] 
 ADD CONSTRAINT [PK_tbTiposDocumentosVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_tpo_dcmnto_cja])
GO

CREATE INDEX [IXFK_tbTiposDocumentos_Vigencias_tbTiposDocumentos] 
 ON cja.[tbRCTiposDocumentosCaja_Vigencias] ([cnsctvo_cdgo_tpo_dcmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCBancos] 
 ADD CONSTRAINT [PK_tbBancos]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_bnco])
GO

ALTER TABLE cja.[tbRCBancos_Vigencias] 
 ADD CONSTRAINT [PK_tbBancosVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_bnco])
GO

CREATE INDEX [IXFK_tbBancos_Vigencias_tbBancos] 
 ON cja.[tbRCBancos_Vigencias] ([cnsctvo_cdgo_bnco] ASC)
GO

ALTER TABLE cja.[tbRCCausalesDescuadre] 
 ADD CONSTRAINT [PK_tbCausalesDescuadre]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_csl_dscdre])
GO

ALTER TABLE cja.[tbRCCausalesDescuadre_Vigencias] 
 ADD CONSTRAINT [PK_tbCausalesDescuadreVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_csl_dscdre])
GO

CREATE INDEX [IXFK_tbCausalesDescuadre_Vigencias_tbCausalesDescuadre] 
 ON cja.[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_cdgo_csl_dscdre] ASC)
GO

ALTER TABLE cja.[tbRCCierreOficina] 
 ADD CONSTRAINT [PK_tbCierreOficina]
	PRIMARY KEY CLUSTERED ([cnsctvo_crre_ofcna])
GO

CREATE INDEX [IXFK_tbRCCierreOficina_tbRCCoordinadorAsixOficina] 
 ON cja.[tbRCCierreOficina] ([cnsctvo_cdgo_crdndr_asi] ASC)
GO

ALTER TABLE cja.[tbRCConceptosGeneracionNotasCredito] 
 ADD CONSTRAINT [PK_tbConceptosGeneracionNotasCredito]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_cncpto_gnrcn])
GO

ALTER TABLE cja.[tbRCMovimientosCaja] 
 ADD CONSTRAINT [PK_tbMovimientosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_mvmnto_cja])
GO

CREATE INDEX [IXFK_tbMovimientosCaja_tbEstadosMovimientosCaja] 
 ON cja.[tbRCMovimientosCaja] ([cnsctvo_cdgo_estdo] ASC)
GO

CREATE INDEX [IXFK_tbRCMovimientosCaja_tbRCCierreOficina] 
 ON cja.[tbRCMovimientosCaja] ([cnsctvo_crre_ofcna] ASC)
GO

ALTER TABLE cja.[tbRCCoordinadorAsixOficina] 
 ADD CONSTRAINT [PK_tbRCCoordinadorAsixOficina]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_crdndr_asi])
GO

ALTER TABLE cja.[tbRCCoordinadorAsixOficina_Vigencias] 
 ADD CONSTRAINT [PK_tbRCCoordinadorAsixOficinaVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_crdndr_asi])
GO

CREATE INDEX [IXFK_tbRCCoordinadorAsixOficina_Vigencias_tbRCCoordinadorAsixOficina] 
 ON cja.[tbRCCoordinadorAsixOficina_Vigencias] ([cnsctvo_cdgo_crdndr_asi] ASC)
GO

ALTER TABLE cja.[tbRCSoportesDocumentosCaja] 
 ADD CONSTRAINT [PK_tbRCSoportesDocumentosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_sprte_dcmnto])
GO

CREATE INDEX [IXFK_tbRCSoportesDocumentosCaja_tbRCDocumentosCaja] 
 ON cja.[tbRCSoportesDocumentosCaja] ([cnsctvo_dcmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCDocumentosRelacionados] 
 ADD CONSTRAINT [PK_tbRCDocumentosRelacionados]
	PRIMARY KEY CLUSTERED ([cnsctvo_dcmnto_rlcndo])
GO

CREATE INDEX [IXFK_tbRCDocumentosRelacionados_tbRCDocumentosCaja] 
 ON cja.[tbRCDocumentosRelacionados] ([cnsctvo_cdgo_dcmnto_cja_rlcndo] ASC)
GO

ALTER TABLE cja.[tbRCDocumentosCaja] 
 ADD CONSTRAINT [PK_tbRCNotasCredito]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_dcmnto_cja])
GO

CREATE INDEX [IXFK_tbRCDocumentosCaja_tbRCDatosContacto] 
 ON cja.[tbRCDocumentosCaja] ([cnsctvo_dto_cntcto] ASC)
GO

CREATE INDEX [IXFK_tbRCDocumentosCaja_tbRCDetMovimientosCaja] 
 ON cja.[tbRCDocumentosCaja] ([cnsctvo_dtlle_mvmnto_cja] ASC)
GO

CREATE INDEX [IXFK_tbRCDocumentosCaja_tbRCTiposDocumentosCaja] 
 ON cja.[tbRCDocumentosCaja] ([cnsctvo_cdgo_tpo_dcmnto_cja] ASC)
GO

CREATE INDEX [IXFK_tbRCNotasCredito_tbRCConceptosGeneracionNotasCredito] 
 ON cja.[tbRCDocumentosCaja] ([cnsctvo_cdgo_cncpto_gnrcn] ASC)
GO

CREATE INDEX [IXFK_tbRCNotasCredito_tbRCEstadosDocumentosCaja] 
 ON cja.[tbRCDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCDetMovimientosCaja] 
 ADD CONSTRAINT [PK_tbDetMovimientosCaja]
	PRIMARY KEY CLUSTERED ([cnsctvo_dtlle_mvmnto_cja])
GO

CREATE INDEX [IXFK_tbDetMovimientosCaja_tbMovimientosCaja] 
 ON cja.[tbRCDetMovimientosCaja] ([cnsctvo_mvmnto_cja] ASC)
GO

ALTER TABLE cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] 
 ADD CONSTRAINT [PK_tbConceptosGeneracionNotasCreditoVigencias]
	PRIMARY KEY CLUSTERED ([cnsctvo_vgnca_cncpto_gnrcn])
GO

CREATE INDEX [IXFK_tbConceptosGeneracionNotasCredito_Vigencias_tbConceptosGeneracionNotasCredito] 
 ON cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] ([cnsctvo_cdgo_cncpto_gnrcn] ASC)
GO

CREATE INDEX [IXFK_tbConceptosGeneracionNotasCredito_Vigencias_tbConceptosGeneracionNotasCredito_02] 
 ON cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] ([cnsctvo_cdgo_cncpto_gnrcn] ASC)
GO

CREATE INDEX [IXFK_tbRCConceptosGeneracionNotasCredito_Vigencias_tbRCConceptosGeneracionNotasCredito] 
 ON cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] ([cnsctvo_cdgo_cncpto_gnrcn] ASC)
GO

ALTER TABLE cja.[tbRCMovimientosCajaxCausalesDescuadre] 
 ADD CONSTRAINT [PK_tbMovimientosCajaxCausalesDescuadre]
	PRIMARY KEY CLUSTERED ([cnsctvo_cdgo_mvmnto_cja_x_csl_dscdre])
GO

CREATE INDEX [IXFK_tbMovimientosCajaxCausalesDescuadre_tbCausalesDescuadre] 
 ON cja.[tbRCMovimientosCajaxCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre] ASC)
GO

CREATE INDEX [IXFK_tbMovimientosCajaxCausalesDescuadre_tbMovimientosCaja] 
 ON cja.[tbRCMovimientosCajaxCausalesDescuadre] ([cnsctvo_mvmnto_cja] ASC)
GO