Use bdCNA
Go

ALTER TABLE [gsa].[tbASServiciosSolicitados]  WITH CHECK ADD  CONSTRAINT [FK_tbASServiciosSolicitados_TbAsCausasNoCobroCuotaRecuperacion] FOREIGN KEY([cnsctvo_cdgo_csa_no_cbro_cta_rcprcn])
REFERENCES [prm].[TbAsCausasNoCobroCuotaRecuperacion] ([cnsctvo_cdgo_csa_no_cbro_cta_rcprcn])
GO

ALTER TABLE [gsa].[tbASServiciosSolicitados] CHECK CONSTRAINT [FK_tbASServiciosSolicitados_TbAsCausasNoCobroCuotaRecuperacion]
GO


