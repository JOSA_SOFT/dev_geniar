USE [BDCna]
Go

  CREATE TABLE [prm].[tbASFlujosCargos](
    [cnsctvo_cdgo_fljo_crgo] [dbo].[udtConsecutivo] NOT NULL,
    [cdgo_fljo_crgo] udtCodigo NOT NULL,
    [dscrpcn_cdgo_fljo_crgo] [dbo].[udtDescripcion] NOT NULL,
    [fcha_crcn] [datetime] NOT NULL,
    [usro_crcn] [dbo].[udtUsuario] NOT NULL,
    [vsble_usro] [dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbASFlujosCargos] PRIMARY KEY CLUSTERED
(
    [cnsctvo_cdgo_fljo_crgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

Go


CREATE TABLE [prm].[tbASFlujosCargos_Vigencias](
    [cnsctvo_vgnca_cdgo_fljo_crgo] [dbo].[udtConsecutivo] NOT NULL,
    [cnsctvo_cdgo_fljo_crgo] [dbo].[udtConsecutivo] NOT NULL,
    [cdgo_fljo_crgo] udtCodigo NOT NULL,
    [dscrpcn_cdgo_fljo_crgo] [dbo].[udtDescripcion] NOT NULL,
    [inco_vgnca] [datetime] NOT NULL,
    [fn_vgnca] [datetime] NOT NULL,
    [fcha_crcn] [datetime] NOT NULL,
    [usro_crcn] [dbo].[udtUsuario] NOT NULL,
    [obsrvcns] [dbo].[udtObservacion] NOT NULL,
    [vsble_usro] [dbo].[udtLogico] NOT NULL,
    [grpo_adtr_prcso]	[dbo].[udtDescripcion],
    [tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbASFlujosCargos_Vigencias] PRIMARY KEY CLUSTERED
(
    [cnsctvo_vgnca_cdgo_fljo_crgo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

Go

ALTER TABLE [prm].[tbASFlujosCargos_Vigencias]  WITH NOCHECK ADD  CONSTRAINT [FK_tbASFlujosCargos_Vigencias_tbASFlujosCargos] FOREIGN KEY([cnsctvo_cdgo_fljo_crgo])
REFERENCES [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo])
GO

ALTER TABLE [prm].[tbASFlujosCargos_Vigencias] CHECK CONSTRAINT [FK_tbASFlujosCargos_Vigencias_tbASFlujosCargos]
GO
