USE [BDCna]
GO

CREATE TABLE [prm].[tbASPlantillasNotificaciones](
	[cnsctvo_cdgo_plntlla_ntfccn] 	[udtConsecutivo] NOT NULL,
	[cdgo_plntlla_ntfccn] [udtCodigo] NOT NULL,
	[dscrpcn_plntlla_ntfccn] [udtDescripcion] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [udtUsuario] NOT NULL,
	[vsble_usro] [dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbASPlantillasNotificaciones] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_cdgo_plntlla_ntfccn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [prm].[tbASPlantillasNotificaciones_Vigencias](
	[cnsctvo_vgnca_cdgo_plntlla_ntfccn] [udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_plntlla_ntfccn] [udtConsecutivo] NOT NULL,
	[cdgo_plntlla_ntfccn] [udtCodigo] NOT NULL,
	[dscrpcn_plntlla_ntfccn] [udtDescripcion] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [udtUsuario] NOT NULL,
	[obsrvcns] [udtObservacion] NOT NULL,
	[vsble_usro] [udtLogico] NOT NULL,
	[nmbre_sp] [udtDescripcion] NULL,
	[tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbASPlantillasNotificaciones_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_cdgo_plntlla_ntfccn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [prm].[tbASPlantillasNotificaciones_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbASPlantillasNotificaciones_Vigencias_tbASPlantillasNotificaciones] FOREIGN KEY([cnsctvo_cdgo_plntlla_ntfccn])
REFERENCES [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn])
GO

ALTER TABLE [prm].[tbASPlantillasNotificaciones_Vigencias] CHECK CONSTRAINT [FK_tbASPlantillasNotificaciones_Vigencias_tbASPlantillasNotificaciones]
GO
