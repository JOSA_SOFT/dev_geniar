USE [bdCNA]
GO

CREATE TABLE [prm].[tbASRelUbicacionPacientexClaseAtencion](
	[cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn] [dbo].[udtConsecutivo] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuarioWeb] NOT NULL,
	[cnsctvo_cdgo_tpo_ubccn_pcnte] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_clse_atncn] [dbo].[udtConsecutivo] NOT NULL,
 CONSTRAINT [PK_tbASRelUbicacionPacientexClaseAtencion] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias](
    [cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn] [dbo].[udtConsecutivo] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuarioWeb] NOT NULL,
	[cnsctvo_cdgo_tpo_ubccn_pcnte] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_clse_atncn] [dbo].[udtConsecutivo] NOT NULL,
	[tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbASRelUbicacionPacientexClaseAtencion_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

Go

ALTER TABLE [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbASRelUbicacionPacientexClaseAtencion_Vigencias_tbASRelUbicacionPacientexClaseAtencion] FOREIGN KEY([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn])
REFERENCES [prm].[tbASRelUbicacionPacientexClaseAtencion] ([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn])
GO

ALTER TABLE [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] CHECK CONSTRAINT [FK_tbASRelUbicacionPacientexClaseAtencion_Vigencias_tbASRelUbicacionPacientexClaseAtencion]
GO

--Drop Table [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias]
--Drop Table [prm].[tbASRelUbicacionPacientexClaseAtencion]
