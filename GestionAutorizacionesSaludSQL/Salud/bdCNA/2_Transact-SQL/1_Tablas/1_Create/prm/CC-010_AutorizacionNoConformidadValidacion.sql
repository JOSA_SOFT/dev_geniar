USE [BDCna]
Go

  CREATE TABLE [prm].[tbASAutorizaNoConformidadValidacion](
    [cnsctvo_cdgo_atrzcn_no_cnfrmdd_vldcn] [dbo].[udtConsecutivo] NOT NULL,
    [cdgo_atrzcn_no_cnfrmdd_vldcn] udtCodigo NOT NULL,
    [dscrpcn_cdgo_atrzcn_no_cnfrmdd_vldcn] [dbo].[udtDescripcion] NOT NULL,
    [fcha_crcn] [datetime] NOT NULL,
    [usro_crcn] [dbo].[udtUsuario] NOT NULL,
    [vsble_usro] [dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbASAutorizaNoConformidadValidacion] PRIMARY KEY CLUSTERED
(
    [cnsctvo_cdgo_atrzcn_no_cnfrmdd_vldcn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

Go


CREATE TABLE [prm].[tbASAutorizaNoConformidadValidacion_Vigencias](
    [cnsctvo_vgnca_cdgo_atrzcn_no_cnfrmdd_vldcn] [dbo].[udtConsecutivo] NOT NULL,
    [cnsctvo_cdgo_atrzcn_no_cnfrmdd_vldcn] [dbo].[udtConsecutivo] NOT NULL,
    [cdgo_atrzcn_no_cnfrmdd_vldcn] udtCodigo NOT NULL,
    [dscrpcn_cdgo_atrzcn_no_cnfrmdd_vldcn] [dbo].[udtDescripcion] NOT NULL,
    [inco_vgnca] [datetime] NOT NULL,
    [fn_vgnca] [datetime] NOT NULL,
    [fcha_crcn] [datetime] NOT NULL,
    [usro_crcn] [dbo].[udtUsuario] NOT NULL,
    [obsrvcns] [dbo].[udtObservacion] NOT NULL,
    [vsble_usro] [dbo].[udtLogico] NOT NULL,
    [cnsctvo_cdgo_no_cnfrmdd_vldcn]	[dbo].[udtDescripcion],
    [tme_stmp] [timestamp] NOT NULL,
 CONSTRAINT [PK_tbASAutorizaNoConformidadValidacion_Vigencias] PRIMARY KEY CLUSTERED
(
    [cnsctvo_vgnca_cdgo_atrzcn_no_cnfrmdd_vldcn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

Go

ALTER TABLE [prm].[tbASAutorizaNoConformidadValidacion_Vigencias]  WITH NOCHECK ADD  CONSTRAINT [FK_tbASAutorizaNoConformidadValidacion_Vigencias_tbASAutorizaNoConformidadValidacion] FOREIGN KEY([cnsctvo_cdgo_atrzcn_no_cnfrmdd_vldcn])
REFERENCES [prm].[tbASAutorizaNoConformidadValidacion] ([cnsctvo_cdgo_atrzcn_no_cnfrmdd_vldcn])
GO

ALTER TABLE [prm].[tbASAutorizaNoConformidadValidacion_Vigencias] CHECK CONSTRAINT [FK_tbASAutorizaNoConformidadValidacion_Vigencias_tbASAutorizaNoConformidadValidacion]