use bdcna
GO

CREATE TABLE prm.tbAsTipoOrigenSolicitud(
    [cnsctvo_cdgo_tpo_orgn_slctd]						[dbo].[udtConsecutivo] NOT NULL,
	[cdgo_tpo_orgn_slctd]								[dbo].[udtCodigo] NOT NULL,
	[dscrpcn_cdgo_tpo_orgn_slctd]						[dbo].[udtDescripcion] NOT NULL,
	[fcha_crcn]											[datetime] NOT NULL,
	[usro_crcn]											[dbo].[udtUsuario] NOT NULL,
	[vsble_usro]										[dbo].[udtLogico] NOT NULL,
 CONSTRAINT [PK_tbAsTipoOrigenSolicitud] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_cdgo_tpo_orgn_slctd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE prm.tbAsTipoOrigenSolicitud_Vigencias(
	[cnsctvo_vgnca_cdgo_tpo_orgn_slctd]					[dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_tpo_orgn_slctd]						[dbo].[udtConsecutivo] NOT NULL,
	[cdgo_tpo_orgn_slctd]								[dbo].[udtCodigo] NOT NULL,
	[dscrpcn_cdgo_tpo_orgn_slctd]						[dbo].[udtDescripcion] NOT NULL,
	[inco_vgnca]										datetime NOT NULL,
	[fn_vgnca]											datetime NOT NULL,
	[fcha_crcn]											[datetime] NOT NULL,
	[usro_crcn]											[dbo].[udtUsuario] NOT NULL,
	[obsrvcns]											[dbo].[udtObservacion] NULL,
	[vsble_usro]										[dbo].[udtLogico] NOT NULL,
	[tme_stmp]											timestamp NOT NULL
	CONSTRAINT [PK_tbAsTipoOrigenSolicitud_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_cdgo_tpo_orgn_slctd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE prm.tbAsTipoOrigenSolicitud_Vigencias  WITH CHECK ADD  CONSTRAINT [FK_tbAsTipoOrigenSolicitud_Vigencias_tbAsTipoOrigenSolicitud] FOREIGN KEY([cnsctvo_cdgo_tpo_orgn_slctd])
REFERENCES prm.tbAsTipoOrigenSolicitud([cnsctvo_cdgo_tpo_orgn_slctd])
GO 
SET ANSI_PADDING OFF
GO

