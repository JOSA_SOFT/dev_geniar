USE [bdcna]
GO

/* Create Table Comments */

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Dato Contacto para la devolucion del dinero a los afiliados', 'Schema', [cja], 'table', [tbRCDatosContacto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [cnsctvo_dto_cntcto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbMetodosDevolucionDinero', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [cnsctvo_cdgo_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Numero de la cuenta para realizar la consignacion', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [nmro_cnta]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbBancos', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [cnsctvo_cdgo_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tipo de cuenta: A C', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [tpo_cnta]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Nombre del titular de la cuenta', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [ttlr]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Numero de telefono de contacto', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [nmro_tlfno]
GO

EXEC sp_addextendedproperty 'MS_Description', 'email de contacto del afiliado', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [eml]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCDatosContacto], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbMovimientosCajaxCausalesDescuadre', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [cnsctvo_vgnca_mvmnto_cja_x_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbMovimientosCajaxCausalesDescuadre', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [cnsctvo_cdgo_mvmnto_cja_x_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [cnsctvo_cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Denominacion', 'Schema', [cja], 'table', [tbRCDenominaciones]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [cnsctvo_cdgo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del denominacion', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [cdgo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del denominacion', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [dscrpcn_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCDenominaciones], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbDenominaciones', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [cnsctvo_vgnca_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbDenominaciones', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [cnsctvo_cdgo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del denominacion', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [cdgo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del denominacion', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [dscrpcn_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbTiposDenominacion', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [cnsctvo_cdgo_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCDenominaciones_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Detalle Denominacion', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [cnsctvo_dtlle_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbMovimientosCaja', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbDenominaciones', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [cnsctvo_cdgo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Cantidad recibida por la denominación', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [cntdd]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCDetalleDenominaciones], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Estado Documento Caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [cnsctvo_cdgo_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del estado documento caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [cdgo_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del estado documento caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [dscrpcn_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbEstadosDocumentos Caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [cnsctvo_vgnca_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbEstadosDocumentosCaja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [cnsctvo_cdgo_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del estado documento caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [cdgo_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del estado documento caja', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [dscrpcn_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCEstadosDocumentosCaja_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Estado', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [cnsctvo_cdgo_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del estado', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [cdgo_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del estado', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [dscrpcn_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbEstadosMovimientosCaja', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [cnsctvo_vgnca_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbEstadosMovimientosCaja', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [cnsctvo_cdgo_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del estado', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [cdgo_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del estado', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [dscrpcn_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCEstadosMovimientosCaja_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla historico de estados de la tabla tbMovimientosCaja', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [cnsctvo_hstrco_estdo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbMovimientosCaja', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbEstadosMovimientosCaja', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [cnsctvo_cdgo_estdo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro en el historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro en el historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha hasta la cual fue valido este registro en el caso que este haya sido marcado como No Valido', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [fcha_fn_vldz_rgstro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro debe ser tomado en cuenta como valido o no para los procesos, es equivalente a un borrado logico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [vldo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro de historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCHistoricoEstadosMovimientosCaja], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Metodo Devolucion Dinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [cnsctvo_cdgo_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del metodo devolucion dinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [cdgo_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del metodo devolucion dinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [dscrpcn_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbMetodosDevolucionDinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [cnsctvo_vgnca_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbMetodosDevolucionDinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [cnsctvo_cdgo_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del metodo devolucion dinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [cdgo_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del metodo devolucion dinero', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [dscrpcn_mtdo_dvlcn_dnro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCMetodosDevolucionDinero_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla relacion entre las tablas parametro tbMovimientosCaja y tbCausalesDescuadre', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre], 'column', [cnsctvo_cdgo_mvmnto_cja_x_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre], 'column', [cnsctvo_cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCajaxCausalesDescuadre], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbConceptosGeneracionNotasCredito', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [cnsctvo_vgnca_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbConceptosGeneracionNotasCredito', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [cnsctvo_cdgo_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del concepto generacion', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [cdgo_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del concepto generacion', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [dscrpcn_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Tipo Denominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [cnsctvo_cdgo_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del tipo denominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [cdgo_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del tipo denominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [dscrpcn_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCTiposDenominacion], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbTiposDenominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [cnsctvo_vgnca_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbTiposDenominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [cnsctvo_cdgo_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del tipo denominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [cdgo_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del tipo denominacion', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [dscrpcn_tpo_dnmncn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCTiposDenominacion_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Tipo Documento', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [cnsctvo_cdgo_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del tipo documento', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [cdgo_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del tipo documento', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [dscrpcn_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbTiposDocumentosCaja', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [cnsctvo_vgnca_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbTiposDocumentos', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [cnsctvo_cdgo_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del tipo documento', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [cdgo_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del tipo documento', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [dscrpcn_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCTiposDocumentosCaja_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Banco', 'Schema', [cja], 'table', [tbRCBancos]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCBancos], 'column', [cnsctvo_cdgo_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del banco', 'Schema', [cja], 'table', [tbRCBancos], 'column', [cdgo_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del banco', 'Schema', [cja], 'table', [tbRCBancos], 'column', [dscrpcn_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCBancos], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCBancos], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCBancos], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbBancos', 'Schema', [cja], 'table', [tbRCBancos_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [cnsctvo_vgnca_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbBancos', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [cnsctvo_cdgo_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del banco', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [cdgo_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del banco', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [dscrpcn_bnco]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCBancos_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Causal Descuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [cnsctvo_cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del causal descuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del causal descuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [dscrpcn_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCCausalesDescuadre], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbCausalesDescuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [cnsctvo_vgnca_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbCausalesDescuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [cnsctvo_cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del causal descuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [cdgo_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del causal descuadre', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [dscrpcn_csl_dscdre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCCausalesDescuadre_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Cierre Oficina', 'Schema', [cja], 'table', [tbRCCierreOficina]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [cnsctvo_crre_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Total efectivo caja', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [ttl_efctvo_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Total recibido oficina', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [ttl_rcbdo_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Base oficina', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [bse_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Faltante oficina', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [fltnte_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Sobrante oficina', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [sbrnte_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCCoordinadorAsixOficina', 'Schema', [cja], 'table', [tbRCCierreOficina], 'column', [cnsctvo_cdgo_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Concepto Generacion', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [cnsctvo_cdgo_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del concepto generacion', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [cdgo_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del concepto generacion', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [dscrpcn_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCConceptosGeneracionNotasCredito], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Movimiento de apertura y cierre de las Cajas ', 'Schema', [cja], 'table', [tbRCMovimientosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Base de dinero entregada', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [bse]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de apertura de la caja', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [fcha_aprtra]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de cierre de la caja', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [fcha_crre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Oficina en la que se realiza la apertura', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [cnsctvo_cdgo_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbEstadosMovimientosCaja', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [cnsctvo_cdgo_estdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Dirección IP del equipo de donde se realiza la apertura', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [drccn_ip]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Dinero faltante al cierre de caja', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [fltnte_crre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Dinero sobrante al cierre de caja', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [sbrnte_crre]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Total en efectivo en caja al momento del cierre', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [ttl_efctvo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Total en caja calculado por el sistema', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [ttl_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Total notas creditos al realizar el cierre', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [ttl_nta_crdto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCCierreOficina', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [cnsctvo_crre_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCMovimientosCaja], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra los parametros correspondientes a Coordinador Asi', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [cnsctvo_cdgo_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del coordinador asi', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [cdgo_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del coordinador asi', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [dscrpcn_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Vigencias de la tabla tbRCCoordinadorAsixOficina', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [cnsctvo_vgnca_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla tbRCCoordinadorAsixOficina', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [cnsctvo_cdgo_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Codigo del coordinador asi', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [cdgo_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Descripcion del coordinador asi', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [dscrpcn_crdndr_asi]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Consecutivo codigo de la oficina asociada al usuario', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [cnsctvo_cdgo_ofcna]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Login del usuario coordinador de ASi en la oficina', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [lgn_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Inicio de vigencia del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [inco_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fin de vigencia del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [fn_vgnca]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observaciones', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [obsrvcns]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Indica si el registro es visible para seleccion al usuario', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [vsble_usro]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Time Stamp', 'Schema', [cja], 'table', [tbRCCoordinadorAsixOficina_Vigencias], 'column', [tme_stmp]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Soporte Documento', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [cnsctvo_sprte_dcmnto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCDocumentosCaja', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [cnsctvo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'numero unico ops para la que se genera el recibo de caja o nota credito', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [nmro_unco_ops]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCSoportesDocumentosCaja], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra documentos que se asocian a un recibo de caja cuando hay pago con notas credito o quedan excedentes de pago', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla Documentos Relacionados', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [cnsctvo_dcmnto_rlcndo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCDocumentosCaja Consecutivo codigo del recibo al que se le hizo el cruce', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [cnsctvo_cdgo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCDocumento Consecutivo codigo de la Nota credito con la que se realizo el cruce', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [cnsctvo_cdgo_dcmnto_cja_rlcndo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCDocumentosRelacionados], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra Documentos de caja, s tales como notas credito y recibos de caja', 'Schema', [cja], 'table', [tbRCDocumentosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_cdgo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Numero unico de identificacion del afiliado al que se le genera la nota credito', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [nmro_unco_idntfccn_afldo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCConceptosGeneracionNotasCredito', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_cdgo_cncpto_gnrcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Valor documento caja', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [vlr_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCTiposDocumentosCaja', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_cdgo_tpo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCEstadosDocumentosCaja', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_cdgo_estdo_dcmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCDatosContacto', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_dto_cntcto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbRCDetMovimientosCaja', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [cnsctvo_dtlle_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Numero de documento generado  Secuencia para Recibos de caja y Notas credito', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [nmro_dcmnto]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Observación ingresada al momento de crear e documento', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [obsrvcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCDocumentosCaja], 'column', [usro_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Tabla que administra el Detalle diario de los Movimiento Caja, recibos de caja y notas de credito asociadas a una caja', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave primaria de la tabla', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [cnsctvo_dtlle_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Llave foranea de la tabla tbMovimientosCaja', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [cnsctvo_mvmnto_cja]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Valor del movimiento de la caja', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [vlr_dcmnto ]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Valor en efectivo recibido', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [vlr_rcbdo]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de creacion del registro', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [fcha_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de creacion del registro', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [usro_crcn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Fecha de ultima modificacion del registro de historico', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [fcha_ultma_mdfccn]
GO

EXEC sp_addextendedproperty 'MS_Description', 'Usuario de ultima modificacion del registro historico', 'Schema', [cja], 'table', [tbRCDetMovimientosCaja], 'column', [usro_ultma_mdfccn]
GO
