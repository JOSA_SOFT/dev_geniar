	USE [BDCna]
	go
	--drop table gsa.tbTmpAsOPSConsultadas
	--drop table gsa.tbTmpAsTotalOPSConsultadas
	CREATE TABLE gsa.tbTmpAsOPSConsultadas (
		id							int Identity (1,1),
		cnsctvo_cdgo_tpo_idntfccn	udtConsecutivo,
		cdgo_tpo_idntfccn			udtTipoIdentificacion,
		nmro_idntfccn				udtNumeroIdentificacion,
		cnsctvo_cdgo_pln			udtConsecutivo, 
		dscrpcn_pln					udtDescripcion,
		cnsctvo_cdgo_tpo_afldo		udtConsecutivo,
		fcha_estmda_entrga_fnl		datetime,
		fcha_expdcn					datetime,
		cnsctvo_cdgo_estdo			udtConsecutivo,
		cnsctvo_cdgo_sde			udtConsecutivo,
		cnsctvo_cdgo_grpo_entrga	udtConsecutivo,
		cnsctvo_prstcn				udtConsecutivo,
		cdgo_cdfccn					char(11),
		dscrpcn_agrpdr_prstcn		udtObservacion,
		vlr_rfrnca					numeric(14, 2),
		cnsctvo_vgnca_agrpdr_prstcn udtConsecutivo,
		nmbre						varchar(200),
		nmro_ops					varchar(100),
		dscrpcn_cdfccn				varchar(500),
		ttla						udtLogico default 'N',
		nmro_unco_idntfccn_afldo	udtConsecutivo,
		incpcdd						udtLogico default 'N',
		cdgo_grpo_entrga			varchar(10), 
		dscrpcn_grpo_entrga			udtDescripcion default ' ',
		pln_pc						udtLogico default 'N',
		dscrpcn_estdo_mga			udtDescripcion default ' ',
		dscrpcn_clsfccn_evnto		udtDescripcion default ' ',
		fcha_entrga					datetime,
		cnsctvo_cdgo_ofcna			udtConsecutivo,
		nuam						numeric(18,0),
		cnsctvo_cdfccn				udtConsecutivo,
		rclclr						udtLogico default 'N',
		usro 						udtUsuario not null,
		cnsctvo_atncn_ops			udtConsecutivo,
		cnsctvo_slctd_mga			udtConsecutivo,		
		fcha_ultma_mdfccn			datetime

	)
	go
		CREATE  TABLE gsa.tbTmpAsTotalOPSConsultadas (
		cnsctvo_vgnca_agrpdr_prstcn udtConsecutivo,
		dscrpcn_agrpdr_prstcn		udtDescripcion,
		cntdd_ops_cnsltds			numeric(9,2) ,
		vlr_ops_cnsltds				numeric(14,2),
		prctj_cntdd_ops_cnsltds		numeric(9,2),
		prctj_vlr_ops_cnsltds		numeric(9,2),
		cntdd_ops_mdfcds			numeric(9,2),
		vlr_ops_mdfcds				numeric(14,2),
		prctj_cntdd_ops_mdfcds		numeric(9,2),
		prctj_vlr_ops_mdfcds		numeric(9,2),
		vlr_ops_rclclo				numeric(14,2) default 0,
		cntdd_ops_rclclo			int default 0,
		usro 						udtUsuario not null
		
	)
	go
	
CREATE UNIQUE NONCLUSTERED INDEX idx_tmp_id ON gsa.tbTmpAsOPSConsultadas
	(
	id
	) 
	Go
CREATE NONCLUSTERED INDEX idx_tmp_uro ON gsa.tbTmpAsOPSConsultadas
	(
	usro
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_cdgo_ofcna ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_cdgo_ofcna
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_cdfccn ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_cdfccn
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_prstcn ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_prstcn
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_cdgo_pln ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_cdgo_pln
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_cdgo_sde ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_cdgo_sde
	) 
GO
CREATE NONCLUSTERED INDEX idx_cnsctvo_vgnca_agrpdr_prstcn ON gsa.tbTmpAsOPSConsultadas
	(
	cnsctvo_vgnca_agrpdr_prstcn
	) 
GO
CREATE NONCLUSTERED INDEX idx_nuam ON gsa.tbTmpAsOPSConsultadas
	(
	nuam
	) 