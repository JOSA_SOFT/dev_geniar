USE [bdCNA]
GO

/****** Object:  Table [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias]    Script Date: 23/05/2017 03:26:13 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias](
	[cnsctvo_vgnca_dgnstco_slctd_atrzcn_srvco] [dbo].[udtConsecutivo] IDENTITY(1,1) NOT NULL,
	[cnsctvo_dgnstco_slctd_atrzcn_srvco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_slctd_atrzcn_srvco] [dbo].[udtConsecutivo] NOT NULL,
	[cnsctvo_cdgo_dgnstco] [dbo].[udtConsecutivo] NOT NULL,
	[inco_vgnca] [datetime] NOT NULL,
	[fn_vgnca] [datetime] NOT NULL,
	[fcha_crcn] [datetime] NOT NULL,
	[usro_crcn] [dbo].[udtUsuario] NOT NULL,
	[fcha_ultma_mdfccn] [datetime] NOT NULL,
	[usro_ultma_mdfccn] [dbo].[udtUsuario] NOT NULL,
 CONSTRAINT [PK_tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias] PRIMARY KEY CLUSTERED 
(
	[cnsctvo_vgnca_dgnstco_slctd_atrzcn_srvco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias_tbASDiagnosticosSolicitudAutorizacionServicios] FOREIGN KEY([cnsctvo_dgnstco_slctd_atrzcn_srvco])
REFERENCES [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios] ([cnsctvo_dgnstco_slctd_atrzcn_srvco])
GO

ALTER TABLE [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias] CHECK CONSTRAINT [FK_tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias_tbASDiagnosticosSolicitudAutorizacionServicios]
GO

ALTER TABLE [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias]  WITH CHECK ADD  CONSTRAINT [FK_tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias_tbASSolicitudesAutorizacionServicios] FOREIGN KEY([cnsctvo_slctd_atrzcn_srvco])
REFERENCES [gsa].[tbASSolicitudesAutorizacionServicios] ([cnsctvo_slctd_atrzcn_srvco])
GO

ALTER TABLE [gsa].[tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias] CHECK CONSTRAINT [FK_tbASDiagnosticosSolicitudAutorizacionServicios_Vigencias_tbASSolicitudesAutorizacionServicios]
GO

