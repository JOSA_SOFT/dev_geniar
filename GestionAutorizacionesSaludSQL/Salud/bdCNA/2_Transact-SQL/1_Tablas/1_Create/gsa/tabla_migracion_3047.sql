USE bdCNA
GO

CREATE TABLE gsa.tbASDatosMigracion3047Mega(
	cnsctvo_dts_mgrcn			udtConsecutivo identity,
	cnsctvo_slctd_atrzcn_srvco	udtConsecutivo,
	cnsctvo_slctd_prstcn		udtConsecutivo,
	fcha_crcn					datetime NOT NULL,
	usro_crcn					udtUsuario NOT NULL,
	fcha_ultma_mdfccn			datetime NOT NULL,
	usro_ultma_mdfccn			udtUsuario NOT NULL,
	CONSTRAINT [PK_tbASDatosMigracion3047Mega] PRIMARY KEY CLUSTERED (cnsctvo_dts_mgrcn ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE gsa.tbASDatosMigracion3047Mega  
WITH CHECK ADD CONSTRAINT FK_tbASDatosMigracion3047Mega_tbASSolicitudesAutorizacionServicios 
FOREIGN KEY(cnsctvo_slctd_atrzcn_srvco) REFERENCES gsa.tbASSolicitudesAutorizacionServicios (cnsctvo_slctd_atrzcn_srvco)
GO
