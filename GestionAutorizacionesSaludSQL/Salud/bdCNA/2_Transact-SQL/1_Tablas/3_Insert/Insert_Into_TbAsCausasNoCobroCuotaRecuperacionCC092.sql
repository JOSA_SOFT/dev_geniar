
Use bdCNA
Go

Begin
	
	Declare @fcha_actl									Date,
			@fn_vgnca									Date,
			@vsble_usro									udtLogico,
			@vsble_pntlla								udtLogico,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn	udtConsecutivo,
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn		udtConsecutivo,
			@dscrpcn_csa_no_cbro_cta_rcprcn				udtDescripcion,
			@usro_crcn									udtUsuario,
			@cdgo_csa_no_cbro_cta_rcprcn				Char(2),
			@obsrvcns									udtObservacion;

	Set @fcha_actl = getDate();
	Set @fn_vgnca = '99991231';
	Set @vsble_usro = 'S';
	Set @vsble_pntlla = 'S';
	Set @cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn = 1; -- Exoneración Cuotas de Recuperación
	Set @obsrvcns = '.'
	Set @usro_crcn = 'qvisionclr';

	--
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 21;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Exento por Tutela';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_pntlla
	)


	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 22;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Paciente paga en la IPS';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_pntlla
	)

	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 23;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Medicamento de una misma solicitud, solo cobra 1 cuota por solicitud';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_pntlla
	)

End
