USE [BDCna]
GO


Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime

Set     @fcha_actl  = GETDATE()
Set     @usro       = 'sismigra01'
Set     @obsrvcns   = '.'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'



INSERT [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1', N'Plantilla 1', @fcha_actl, @usro, N'S')
INSERT [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2', N'Plantilla 2 Autorización SIAU', @fcha_actl, @usro, N'S')
INSERT [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'4', N'Plantilla 4 Negación', @fcha_actl, @usro, N'S')
INSERT [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (5, N'5', N'Plantilla 5 Devolucion', @fcha_actl, @usro, N'S')
INSERT [prm].[tbASPlantillasNotificaciones] ([cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (6, N'6', N'Plantilla 6 Devolucion Transcripciones', @fcha_actl, @usro, N'S')

INSERT [prm].[tbASPlantillasNotificaciones_Vigencias] ([cnsctvo_vgnca_cdgo_plntlla_ntfccn], [cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [nmbre_sp]) VALUES (1, 1, N'1 ', N'Plantilla 1', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Plantilla 1', N'S', N'spASTramaXMLNotificacionAutorizacionAT3Plantilla1')
INSERT [prm].[tbASPlantillasNotificaciones_Vigencias] ([cnsctvo_vgnca_cdgo_plntlla_ntfccn], [cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [nmbre_sp]) VALUES (2, 2, N'2 ', N'Plantilla 2 Autorización SIAU', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Plantilla 2 Autorización SIAU', N'S', N'spASTramaXMLNotificacionAutorizacionSIAUDescargaPlantilla2')
INSERT [prm].[tbASPlantillasNotificaciones_Vigencias] ([cnsctvo_vgnca_cdgo_plntlla_ntfccn], [cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [nmbre_sp]) VALUES (4, 4, N'4 ', N'Plantilla 4 Negación',@fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Plantilla 4 Negación', N'S', N'spASTramaXMLNotificacionNegacionPlantilla4')
INSERT [prm].[tbASPlantillasNotificaciones_Vigencias] ([cnsctvo_vgnca_cdgo_plntlla_ntfccn], [cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [nmbre_sp]) VALUES (5, 5, N'5 ', N'Plantilla 5 Devolucion', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Plantilla 5 Devolucion', N'S', N'spASTramaXMLNotificacionDevolucionPlantilla5')

INSERT [prm].[tbASPlantillasNotificaciones_Vigencias] ([cnsctvo_vgnca_cdgo_plntlla_ntfccn], [cnsctvo_cdgo_plntlla_ntfccn], [cdgo_plntlla_ntfccn], [dscrpcn_plntlla_ntfccn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [nmbre_sp]) VALUES (6, 6, N'6 ', N'Plantilla 6 Devolucion Transcripciones', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, N'Plantilla 6 Devolucion Transcripciones', N'S', N'spASTramaXMLNotificacionDevolucionPlantillaTrancripcion5')
