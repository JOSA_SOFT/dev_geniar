USE [bdCNA]
GO


Declare @fcha_crcn Datetime,
        @usro_crcn udtUsuario,
		@fn_vgnca  Datetime


Set @fcha_crcn = GETDATE()
Set @usro_crcn = 'qvisionvgr'
Set @fn_vgnca = '9999-12-31'


INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (1, @fcha_crcn, @usro_crcn, 1, 1)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (1, 1, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 1, 1)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (2, @fcha_crcn, @usro_crcn, 3, 2)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (2, 2, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 3, 2)


INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (3, @fcha_crcn, @usro_crcn, 2, 3)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (3, 3, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 2, 3)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (4, @fcha_crcn, @usro_crcn, 3, 4)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (4, 4, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 3, 4)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (5, @fcha_crcn, @usro_crcn, 1, 7)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (5, 5, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 1, 7)

INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion]([cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
VALUES (6, @fcha_crcn, @usro_crcn, 3, 8)


INSERT INTO [prm].[tbASRelUbicacionPacientexClaseAtencion_Vigencias] ([cnsctvo_vgnca_rlcn_tpo_ubccn_pcnte_clse_atncn], [cnsctvo_rlcn_tpo_ubccn_pcnte_clse_atncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [cnsctvo_cdgo_tpo_ubccn_pcnte], [cnsctvo_cdgo_clse_atncn])
Values (6, 6, @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, 3, 8)



--Select * From bdsisalud.dbo.tbPMTipoUbicacionPaciente_Vigencias
--Select * from bdsisalud.dbo.tbClasesAtencion_Vigencias -- 2