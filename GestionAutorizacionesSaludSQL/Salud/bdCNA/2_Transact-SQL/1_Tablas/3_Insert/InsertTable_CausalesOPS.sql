use bdCNA

Go

Declare @fcha_crcn					Date,
		@fn_vgnca					Date,
		@usro_crcn					udtUsuario,
		@vsble_usro					Char(1),
		@rqre_nmro_atrzcn_ips       udtLogico,
		@cnsctvo_cdgo_tpo_csa_nvdd	udtConsecutivo

Set	@fcha_crcn                 = getDate();
Set @usro_crcn                 = 'qvisionvgr';
Set @vsble_usro                = 'S';
Set @fn_vgnca                  = '99991231';
Set @rqre_nmro_atrzcn_ips      = 'N';
Set @cnsctvo_cdgo_tpo_csa_nvdd = 2


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (76, N'76', N'Usuario no toma el servicio', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (76, 76, N'76', N'Usuario no toma el servicio', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (77, N'77', N'Cambio de ops por perdida', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (77, 77, N'77', N'Cambio de ops por perdida', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (78, N'78', N'Cambio de ops previa por vencimiento', @fcha_crcn, @usro_crcn, N'S')

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (78, 78, N'78', N'Cambio de ops previa por vencimiento', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (79, N'79', N'Error funcionalidad MEGA', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (79, 79, N'79', N'Error funcionalidad MEGA', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (80, N'80', N'Cambio de ops por prestador', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (80, 80, N'80', N'Cambio de ops por prestador', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (81, N'81', N'Cambio de ops por hallazgo quirúrgico', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (81, 81, N'81', N'Cambio de ops por hallazgo quirúrgico', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (82, N'82', N'Medicamento agostado o desabastecido', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (82, 82, N'82', N'Medicamento agostado o desabastecido', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (83, N'83', N'Prestación PAF', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (83, 83, N'83', N'Prestación PAF', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (84, N'84', N'OPS ya Impresa', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (84, 84, N'84', N'OPS ya Impresa', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (85, N'85', N'Prestación gestionada por VIA 3047', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (85, 85, N'85', N'Prestación gestionada por VIA 3047', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (86, N'86', N'IPS no acepta codigo validado', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (86, 86, N'86', N'IPS no acepta codigo validado', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (87, N'87', N'Servicio se encuentra en PGP', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (87, 87, N'87', N'Servicio se encuentra en PGP', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (88, N'88', N'Servicio se encuentra en CAPITACION', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (88, 88, N'88', N'Servicio se encuentra en CAPITACION', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (89, N'89', N'La prestación es de Acceso directo', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (89, 89, N'89', N'La prestación es de Acceso directo', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)
