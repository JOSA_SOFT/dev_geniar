
Use bdCNA
Go

Begin
	
	Declare @fcha_actl									Date,
			@fn_vgnca									Date,
			@vsble_usro									udtLogico,
			@vsble_en_pntlla							udtLogico,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn	udtConsecutivo,
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn		udtConsecutivo,
			@dscrpcn_csa_no_cbro_cta_rcprcn				udtDescripcion,
			@usro_crcn									udtUsuario,
			@cdgo_csa_no_cbro_cta_rcprcn				Char(2),
			@obsrvcns									udtObservacion;

	Set @fcha_actl = getDate();
	Set @fn_vgnca = '99991231';
	Set @vsble_usro = 'S';
	Set @vsble_en_pntlla = 'S';
	Set @cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn = 1; -- Exoneraci�n Cuotas de Recuperaci�n
	Set @obsrvcns = '.'
	Set @usro_crcn = 'qvisionclr';

	--
	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 24;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'El servicio con marca de evento de alto costo regimen contributivo, no cancela cuota de recuperaci�n';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)


	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 25;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Medicamento asociado a evento de alto costo regimen contributivo';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)

	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 26;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Afiliado mayor a 18 a�os con tipo discapacidad fisica o sensorial regimen Subsidiado';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)

	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 27;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Afiliado menor de 18 a�os con tipo discapacidad fisica o sensorial regimen Subsidiado';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)


	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 28;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Afiliado menor a 1 a�o regimen subsidiado';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)

	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 29;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Afiliado menor de 18 a�os con Cancer, regimen contributivo';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)


	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 30;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Afiliado menor de 18 a�os con Cancer regimen subsidiado';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)

	Set @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn = 31;
	Set @cdgo_csa_no_cbro_cta_rcprcn = Convert(Char(2), @cnsctvo_cdgo_csa_no_cbro_cta_rcprcn)
	Set @dscrpcn_csa_no_cbro_cta_rcprcn = 'Solicitud con diagnostico de alto costo, afiliado del regimen subsidiado';


	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion
	(
			cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		cdgo_csa_no_cbro_cta_rcprcn,
			dscrpcn_csa_no_cbro_cta_rcprcn,				fcha_crcn,
			usro_crcn,									vsble_usro
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,		@cdgo_csa_no_cbro_cta_rcprcn,
			@dscrpcn_csa_no_cbro_cta_rcprcn,			@fcha_actl,
			@usro_crcn,									@vsble_usro
	)

	Insert Into bdCNA.prm.TbAsCausasNoCobroCuotaRecuperacion_Vigencias
	(
			cnsctvo_vgnca_cdgo_csa_no_cbro_cta_rcprcn,		cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			cdgo_csa_no_cbro_cta_rcprcn,					dscrpcn_csa_no_cbro_cta_rcprcn,
			inco_vgnca,										fn_vgnca,
			fcha_crcn,										usro_crcn,
			obsrvcns,										vsble_usro,
			cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		vsble_pntlla
	)
	Values
	(
			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,			@cnsctvo_cdgo_csa_no_cbro_cta_rcprcn,
			@cdgo_csa_no_cbro_cta_rcprcn,					@dscrpcn_csa_no_cbro_cta_rcprcn,
			@fcha_actl,										@fn_vgnca,
			@fcha_actl,										@usro_crcn,
			@obsrvcns,										@vsble_usro,
			@cnsctvo_cdgo_tipo_csa_no_bro_cta_rcprcn,		@vsble_en_pntlla
	)

End
