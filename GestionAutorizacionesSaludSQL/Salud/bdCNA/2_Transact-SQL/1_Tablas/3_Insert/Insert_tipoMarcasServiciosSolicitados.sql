USE [BDCna]
GO

Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@obsrvcns_2 udtObservacion,  
		@obsrvcns_3 udtObservacion, 
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime

Set     @fcha_actl  = GETDATE()
Set     @usro       = 'qvisionvgr'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'
Set     @obsrvcns   = '.'
Set     @obsrvcns_2 = 'Descargue su autorizacion en la pagina web wwww.sos.com.co, en la opcion pagina virtual'
Set     @obsrvcns_3 = 'Acerquese al SIAU mas cercano a reclamar su autorizacion'


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1', N'MARCAS ATEL', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1', N'MARCAS ATEL', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2', N'DESCARGA AFILIADO', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2', N'DESCARGA AFILIADO', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns_2, @vsble_usro)


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'3', N'ENVIAR CORREO', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (3, 3, N'3', N'ENVIAR CORREO', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'4', N'OPS VIRTUAL', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (4, 4, N'4', N'OPS VIRTUAL', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (5, N'5', N'DESCARGA IPS', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (5, 5, N'5', N'DESCARGA IPS', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro)


INSERT [prm].[tbASTipoMarcaServiciosSolicitados] ([cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (6, N'6', N'VENTANILLA', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias] ([cnsctvo_vgnca_cdgo_tpo_mrca], [cnsctvo_cdgo_tpo_mrca], [cdgo_tpo_mrca], [dscrpcn_tpo_mrca], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (6, 6, N'6', N'VENTANILLA', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns_3, @vsble_usro)


--2 descarga afiliado, 3 enviar correo, 4 ops virtual, 5 descarga ips, 7 ventanila

--Select * From [prm].tbASTipoMarcaServiciosSolicitados
--Select * From [prm].[tbASTipoMarcaServiciosSolicitados_Vigencias]




