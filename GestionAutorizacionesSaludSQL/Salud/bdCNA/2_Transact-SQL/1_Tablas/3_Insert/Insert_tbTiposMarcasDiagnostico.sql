USE BDSisalud
GO

	Declare	@fcha_actl		Datetime,
			@usro			udtUsuario,
			@vsble_usro		udtLogico,
			@inco_vgnca		Date,
			@fn_vgnca		Date;


	Set @fcha_actl = getDate();
	Set @usro = 'qvisionclr';
	Set @vsble_usro = 'S';
	Set @inco_vgnca = Convert(Date, @fcha_actl);
	Set @fn_vgnca = '99991231';

	INSERT INTO dbo.tbTiposMarcasDiagnostico (
			 cnsctvo_cdgo_tpo_mrca_dgnstco ,cdgo_tpo_mrca_dgnstco
			,dscrpcn_tpo_mrca_dgnstco	   ,fcha_crcn
			,usro_crcn					   ,vsble_usro)
     VALUES
           (1							   ,'01'
           ,'ALTO COSTO'				   ,@fcha_actl
           ,@usro						   ,@vsble_usro)


	INSERT INTO dbo.tbTiposMarcasDiagnostico_Vigencias
           (cnsctvo_vgnca_tpo_mrca_dgnstco         ,cnsctvo_cdgo_tpo_mrca_dgnstco
           ,cdgo_tpo_mrca_dgnstco				   ,dscrpcn_tpo_mrca_dgnstco
           ,inco_vgnca							   ,fn_vgnca
           ,fcha_crcn							   ,usro_crcn
           ,obsrvcns							   ,vsble_usro)
     VALUES
           (1									   ,1
           ,'01'								   ,'ALTO COSTO'
           ,@inco_vgnca							   ,@fn_vgnca
           ,@fcha_actl							   ,@usro
           ,'.'									   ,@vsble_usro)
GO


