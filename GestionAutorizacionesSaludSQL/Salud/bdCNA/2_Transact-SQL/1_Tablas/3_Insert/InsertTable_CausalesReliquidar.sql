use bdCNA

Go

Declare @fcha_crcn					Date,
		@fn_vgnca					Date,
		@usro_crcn					udtUsuario,
		@vsble_usro					Char(1),
		@rqre_nmro_atrzcn_ips       udtLogico,
		@cnsctvo_cdgo_tpo_csa_nvdd	udtConsecutivo

Set	@fcha_crcn                 = getDate();
Set @usro_crcn                 = 'qvisionvgr';
Set @vsble_usro                = 'S';
Set @fn_vgnca                  = '99991231';
Set @rqre_nmro_atrzcn_ips      = 'N';
Set @cnsctvo_cdgo_tpo_csa_nvdd = 9


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (72, N'72', N'Direccionamiento por concepto de gasto por prestador', @fcha_crcn, @usro_crcn, @vsble_usro)


INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (72, 72, N'72', N'Direccionamiento por concepto de gasto por prestador', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (73, N'73', N'Requiere conceptos adicionales', @fcha_crcn, @usro_crcn, @vsble_usro)


INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (73, 73, N'73', N'Requiere conceptos adicionales', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (74, N'74', N'Cambio por modelo de contratación', @fcha_crcn, @usro_crcn, N'S')


INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (74, 74, N'74', N'Cambio por modelo de contratación', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (75, N'75', N'Direccionamiento a otro prestador', @fcha_crcn, @usro_crcn, @vsble_usro)


INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (75, 75, N'75', N'Direccionamiento a otro prestador', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


