USE [BDCna]
GO
-- 1. Poblamos tbTiposProceso

USE BDCna
go
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (0,'00','Vacio',getdate(),'sisjvg01','N')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (1,'01','Proceso Convencional',getdate(),'sisjvg01','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (2,'02','Proceso Automático Programación Entrega',getdate(),'sisjvg01','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (3,'03','Proceso Automático OPS Virtual de Prog. Entrega',getdate(),'sisjvg01','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (4,'04','Proceso Automático 3047',getdate(),'sisjvg01','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud](cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,fcha_crcn,usro_crcn,vsble_usro) values (99999,'99','No Existe',getdate(),'sisjvg01','N')


-- 2. Poblamos tbTiposProceso_Vigencias

INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (0,0,'00','Vacio',getdate(),'99991231',getdate(),'sisjvg01','Vacio','N')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (1,1,'01','Proceso Convencional',getdate(),'99991231',getdate(),'sisjvg01','origen creación de la solicitud','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (2,2,'02','Proceso Automático Programación Entrega',getdate(),'99991231',getdate(),'sisjvg01','origen creación de la solicitud','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (3,3,'03','Proceso Automático OPS Virtual de Prog. Entrega',getdate(),'99991231',getdate(),'sisjvg01','origen creación de la solicitud','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (4,4,'04','Proceso Automático 3047',getdate(),'99991231',getdate(),'sisjvg01','origen creación de la solicitud','S')
INSERT INTO [prm].[tbAsTipoOrigenSolicitud_Vigencias](cnsctvo_vgnca_cdgo_tpo_orgn_slctd,cnsctvo_cdgo_tpo_orgn_slctd,cdgo_tpo_orgn_slctd,dscrpcn_cdgo_tpo_orgn_slctd,inco_vgnca,fn_vgnca,fcha_crcn,usro_crcn,obsrvcns,vsble_usro) values (99999,99999,'99','No Existe',getdate(),'99991231',getdate(),'sisjvg01','No Existe','N')

