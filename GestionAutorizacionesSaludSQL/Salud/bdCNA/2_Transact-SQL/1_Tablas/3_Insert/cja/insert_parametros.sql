USE [BDCna]
GO
INSERT [cja].[tbRCMetodosDevolucionDinero] ([cnsctvo_cdgo_mtdo_dvlcn_dnro], [cdgo_mtdo_dvlcn_dnro], [dscrpcn_mtdo_dvlcn_dnro], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Consignación', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCMetodosDevolucionDinero] ([cnsctvo_cdgo_mtdo_dvlcn_dnro], [cdgo_mtdo_dvlcn_dnro], [dscrpcn_mtdo_dvlcn_dnro], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Cheque', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Generado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Pendiente de cobro', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja] ([cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'3 ', N'Cobrado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCTiposDocumentosCaja] ([cnsctvo_cdgo_tpo_dcmnto_cja], [cdgo_tpo_dcmnto_cja], [dscrpcn_tpo_dcmnto_cja], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Recibo de caja', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCTiposDocumentosCaja] ([cnsctvo_cdgo_tpo_dcmnto_cja], [cdgo_tpo_dcmnto_cja], [dscrpcn_tpo_dcmnto_cja], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Nota Credito', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCConceptosGeneracionNotasCredito] ([cnsctvo_cdgo_cncpto_gnrcn], [cdgo_cncpto_gnrcn], [dscrpcn_cncpto_gnrcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Anulacion OPS', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCConceptosGeneracionNotasCredito] ([cnsctvo_cdgo_cncpto_gnrcn], [cdgo_cncpto_gnrcn], [dscrpcn_cncpto_gnrcn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Excedente por cambio de OPS', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Abierto', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Cerrado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'3 ', N'Anulado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja] ([cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'4 ', N'Cierre Administrativo', CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'1000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'2000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'3 ', N'5000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'4 ', N'10000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (5, N'5 ', N'50000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (6, N'6 ', N'100000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (7, N'7 ', N'50', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (8, N'8 ', N'100', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (9, N'9 ', N'200', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (10, N'10', N'500', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (11, N'11', N'1000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones] ([cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (12, N'12', N'20000', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCTiposDenominacion] ([cnsctvo_cdgo_tpo_dnmncn], [cdgo_tpo_dnmncn], [dscrpcn_tpo_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'Billetes', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCTiposDenominacion] ([cnsctvo_cdgo_tpo_dnmncn], [cdgo_tpo_dnmncn], [dscrpcn_tpo_dnmncn], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'Monedas', CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'1000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'2000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (3, 3, N'3 ', N'5000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (4, 4, N'4 ', N'10000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (5, 5, N'5 ', N'50000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (6, 6, N'6 ', N'100000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (7, 7, N'7 ', N'50', 2, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (8, 8, N'8 ', N'100', 2, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (9, 9, N'9 ', N'200', 2, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (10, 10, N'10', N'500', 2, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (11, 11, N'11', N'1000', 2, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCDenominaciones_Vigencias] ([cnsctvo_vgnca_dnmncn], [cnsctvo_cdgo_dnmncn], [cdgo_dnmncn], [dscrpcn_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (12, 12, N'12', N'20000', 1, CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja_Vigencias] ([cnsctvo_vgnca_estdo_dcmnto_cja], [cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Generado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja_Vigencias] ([cnsctvo_vgnca_estdo_dcmnto_cja], [cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Pendiente de cobro', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosDocumentosCaja_Vigencias] ([cnsctvo_vgnca_estdo_dcmnto_cja], [cnsctvo_cdgo_estdo_dcmnto_cja], [cdgo_estdo_dcmnto_cja], [dscrpcn_estdo_dcmnto_cja], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (3, 3, N'3 ', N'Cobrado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inical', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja_Vigencias] ([cnsctvo_vgnca_estdo], [cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Abierto', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja_Vigencias] ([cnsctvo_vgnca_estdo], [cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Cerrado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja_Vigencias] ([cnsctvo_vgnca_estdo], [cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (3, 3, N'3 ', N'Anulado', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCEstadosMovimientosCaja_Vigencias] ([cnsctvo_vgnca_estdo], [cnsctvo_cdgo_estdo], [cdgo_estdo], [dscrpcn_estdo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (4, 4, N'4 ', N'Cierre Administrativo', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCMetodosDevolucionDinero_Vigencias] ([cnsctvo_vgnca_mtdo_dvlcn_dnro], [cnsctvo_cdgo_mtdo_dvlcn_dnro], [cdgo_mtdo_dvlcn_dnro], [dscrpcn_mtdo_dvlcn_dnro], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Consignación', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCMetodosDevolucionDinero_Vigencias] ([cnsctvo_vgnca_mtdo_dvlcn_dnro], [cnsctvo_cdgo_mtdo_dvlcn_dnro], [cdgo_mtdo_dvlcn_dnro], [dscrpcn_mtdo_dvlcn_dnro], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Cheque', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'1 ', N'copago no corresponde', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'2 ', N'cancela en ips', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'3 ', N'cambio de ops previa', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'4 ', N'cambio de ops por vencimiento', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (5, N'5 ', N'convenio comercial', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (6, N'6 ', N'usuario con plan complementario', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (7, N'7 ', N'credito epsa', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (8, N'8 ', N'varios mtos en la misma formula', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre] ([cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (9, N'9 ', N'no cancela copago alto costo', CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'S')
GO
INSERT [cja].[tbRCTiposDenominacion_Vigencias] ([cnsctvo_vgnca_tpo_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [cdgo_tpo_dnmncn], [dscrpcn_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Billetes', CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCTiposDenominacion_Vigencias] ([cnsctvo_vgnca_tpo_dnmncn], [cnsctvo_cdgo_tpo_dnmncn], [cdgo_tpo_dnmncn], [dscrpcn_tpo_dnmncn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Monedas', CAST(N'2016-06-22 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-06-22 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCTiposDocumentosCaja_Vigencias] ([cnsctvo_vgnca_tpo_dcmnto_cja], [cnsctvo_cdgo_tpo_dcmnto_cja], [cdgo_tpo_dcmnto_cja], [dscrpcn_tpo_dcmnto_cja], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Recibo de caja', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCTiposDocumentosCaja_Vigencias] ([cnsctvo_vgnca_tpo_dcmnto_cja], [cnsctvo_cdgo_tpo_dcmnto_cja], [cdgo_tpo_dcmnto_cja], [dscrpcn_tpo_dcmnto_cja], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Nota Credito', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'copago no corresponde', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'cancela en ips', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (3, 3, N'3 ', N'cambio de ops previa', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (4, 4, N'4 ', N'cambio de ops por vencimiento', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (5, 5, N'5 ', N'convenio comercial', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (6, 6, N'6 ', N'usuario con plan complementario', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (7, 7, N'7 ', N'credito epsa', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (8, 8, N'8 ', N'varios mtos en la misma formula', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCCausalesDescuadre_Vigencias] ([cnsctvo_vgnca_csl_dscdre], [cnsctvo_cdgo_csl_dscdre], [cdgo_csl_dscdre], [dscrpcn_csl_dscdre], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (9, 9, N'9 ', N'no cancela copago alto costo', CAST(N'2016-10-26 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-10-26 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCConceptosGeneracionNotasCredito_Vigencias] ([cnsctvo_vgnca_cncpto_gnrcn], [cnsctvo_cdgo_cncpto_gnrcn], [cdgo_cncpto_gnrcn], [dscrpcn_cncpto_gnrcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (1, 1, N'1 ', N'Anulacion OPS', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
INSERT [cja].[tbRCConceptosGeneracionNotasCredito_Vigencias] ([cnsctvo_vgnca_cncpto_gnrcn], [cnsctvo_cdgo_cncpto_gnrcn], [cdgo_cncpto_gnrcn], [dscrpcn_cncpto_gnrcn], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro]) VALUES (2, 2, N'2 ', N'Excedente por cambio de OPS', CAST(N'2016-05-27 00:00:00.000' AS DateTime), CAST(N'9999-12-31 00:00:00.000' AS DateTime), CAST(N'2016-05-27 00:00:00.000' AS DateTime), N'Sistemas                      ', N'Creacion Inicial', N'S')
GO
