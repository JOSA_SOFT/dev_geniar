use bdCNA

Go

Declare @fcha_crcn					Date,
		@fn_vgnca					Date,
		@usro_crcn					udtUsuario,
		@vsble_usro					Char(1),
		@rqre_nmro_atrzcn_ips       udtLogico,
		@cnsctvo_cdgo_tpo_csa_nvdd	udtConsecutivo

Set	@fcha_crcn                 = getDate();
Set @usro_crcn                 = 'qvisionvgr';
Set @vsble_usro                = 'S';
Set @fn_vgnca                  = '99991231';
Set @rqre_nmro_atrzcn_ips      = 'N';
Set @cnsctvo_cdgo_tpo_csa_nvdd = 1


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (90, N'90', N'Prestación PAF', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (90, 90, N'90', N'Prestación PAF', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (91, N'91', N'OPS ya Impresa', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (91, 91, N'91', N'OPS ya Impresa', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (92, N'92', N'Prestación gestionada por VIA 3047', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (92, 92, N'92', N'Prestación gestionada por VIA 3047', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (93, N'93', N'IPS no acepta codigo validado', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (93, 93, N'93', N'IPS no acepta codigo validado', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (94, N'94', N'Servicio se encuentra en PGP', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (94, 94, N'94', N'Servicio se encuentra en PGP', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (95, N'95', N'Servicio se encuentra en CAPITACION', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (95, 95, N'95', N'Servicio se encuentra en CAPITACION', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)


INSERT [prm].[tbASCausaNovedad] ([cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [fcha_crcn], [usro_crcn], [vsble_usro]) 
VALUES (96, N'96', N'La prestación es de Acceso directo', @fcha_crcn, @usro_crcn, @vsble_usro)

INSERT [prm].[tbASCausaNovedad_Vigencias] ([cnsctvo_vgnca_cdgo_csa_nvdd], [cnsctvo_cdgo_csa_nvdd], [cdgo_csa_nvdd], [dscrpcn_cdgo_csa_nvdd], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [vsble_usro], [cnsctvo_cdgo_tpo_csa_nvdd], [rqre_nmro_atrzcn_ips]) 
VALUES (96, 96, N'96', N'La prestación es de Acceso directo', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd, @rqre_nmro_atrzcn_ips)
