﻿	Use BDCna
	Go
	Declare		@fcha_crcn					date,
				@vsble_usro					udtLogico,
				@fn_vgnca					date,
				@usro_crcn					udtUsuario,
				@cnsctvo_cdgo_tpo_csa_nvdd	udtConsecutivo,
				@mx_cnsctvo					udtConsecutivo;

	Set		@fcha_crcn = getDate();
	Set		@vsble_usro = 'S';
	Set		@fn_vgnca = '9999-12-31';
	Set		@usro_crcn = 'qvisionclr';
	Set		@cnsctvo_cdgo_tpo_csa_nvdd = 7
	
	
	Select	@mx_cnsctvo = Max(a.cnsctvo_cdgo_csa_nvdd) + 1 From BDCna.prm.tbASCausaNovedad a	

	Insert Into BDCna.prm.tbASCausaNovedad(cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, fcha_crcn, usro_crcn, vsble_usro)
	Values (@mx_cnsctvo, @mx_cnsctvo, 'Cambio de prioridad por complicación en el estado de salud', @fcha_crcn, @usro_crcn, @vsble_usro)

	Insert Into BDCna.prm.tbASCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_csa_nvdd, cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, inco_vgnca, fn_vgnca, fcha_crcn, usro_crcn, vsble_usro, cnsctvo_cdgo_tpo_csa_nvdd)
	Values (@mx_cnsctvo, @mx_cnsctvo, @mx_cnsctvo, 'Cambio de prioridad por complicación en el estado de salud', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd)

	-------------------------------------------------------------------------------------------------------------------------------------------------------
	
	Select	@mx_cnsctvo = Max(a.cnsctvo_cdgo_csa_nvdd) + 1 From BDCna.prm.tbASCausaNovedad a
	
	Insert Into BDCna.prm.tbASCausaNovedad(cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, fcha_crcn, usro_crcn, vsble_usro)
	Values(@mx_cnsctvo, @mx_cnsctvo, 'Oportunidad para el acceso al servicio en las IPS', @fcha_crcn, @usro_crcn, @vsble_usro)

	Insert Into BDCna.prm.tbASCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_csa_nvdd, cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, inco_vgnca, fn_vgnca, fcha_crcn, usro_crcn, vsble_usro, cnsctvo_cdgo_tpo_csa_nvdd)
	Values (@mx_cnsctvo, @mx_cnsctvo, @mx_cnsctvo,'Oportunidad para el acceso al servicio en las IPS', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd)
	
	-------------------------------------------------------------------------------------------------------------------------------------------------------

	Select	@mx_cnsctvo = Max(a.cnsctvo_cdgo_csa_nvdd) + 1 From BDCna.prm.tbASCausaNovedad a
	
	Insert Into BDCna.prm.tbASCausaNovedad(cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, fcha_crcn, usro_crcn, vsble_usro)
	Values(@mx_cnsctvo, @mx_cnsctvo, 'Solicitud del Usuario', @fcha_crcn, @usro_crcn, @vsble_usro)

	Insert Into BDCna.prm.tbASCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_csa_nvdd, cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, inco_vgnca, fn_vgnca, fcha_crcn, usro_crcn, vsble_usro, cnsctvo_cdgo_tpo_csa_nvdd)
	Values (@mx_cnsctvo, @mx_cnsctvo, @mx_cnsctvo, 'Solicitud del Usuario', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd)

	-------------------------------------------------------------------------------------------------------------------------------------------------------

	Select	@mx_cnsctvo = Max(a.cnsctvo_cdgo_csa_nvdd) + 1 From BDCna.prm.tbASCausaNovedad a
	
	Insert Into BDCna.prm.tbASCausaNovedad(cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, fcha_crcn, usro_crcn, vsble_usro)
	Values(@mx_cnsctvo, @mx_cnsctvo, 'Insuficiencia económica para el pago de cuota de recuperación', @fcha_crcn, @usro_crcn, @vsble_usro)

	Insert Into BDCna.prm.tbASCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_csa_nvdd, cnsctvo_cdgo_csa_nvdd, cdgo_csa_nvdd, dscrpcn_cdgo_csa_nvdd, inco_vgnca, fn_vgnca, fcha_crcn, usro_crcn, vsble_usro, cnsctvo_cdgo_tpo_csa_nvdd)
	Values (@mx_cnsctvo, @mx_cnsctvo, @mx_cnsctvo, 'Insuficiencia económica para el pago de cuota de recuperación', @fcha_crcn, @fn_vgnca, @fcha_crcn, @usro_crcn, @vsble_usro, @cnsctvo_cdgo_tpo_csa_nvdd)

Go

	