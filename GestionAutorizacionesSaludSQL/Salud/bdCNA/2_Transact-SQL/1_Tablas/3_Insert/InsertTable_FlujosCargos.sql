USE [BDCna]
GO

Declare @fcha_crcn  Datetime      ,
        @usro_crcn  udtUsuario    ,
		@vsble_usro udtLogico     ,
		@fn_vgca    Datetime      ,
		@obsrvcns   udtObservacion


Set @fcha_crcn  = getDate()
Set @usro_crcn  = 'sismigra01'
Set @vsble_usro = 'S'
Set @fn_vgca    = '9999-12-31'
Set @obsrvcns   = '.'


INSERT INTO [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (4, N'04', N'Auditoria medica por riesgo', @fcha_crcn, @usro_crcn, @vsble_usro)
INSERT INTO [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (4, 4, N'04', N'Auditoria medica por riesgo', @fcha_crcn, @fn_vgca, @fcha_crcn, @usro_crcn, @obsrvcns, @vsble_usro, N'4000017 Auditor Medicamentos')


INSERT INTO [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (5, N'05', N'Auditoria medica por riesgo', @fcha_crcn, @usro_crcn, @vsble_usro)
INSERT INTO [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (5, 5, N'05', N'Auditoria medica por riesgo', @fcha_crcn, @fn_vgca, @fcha_crcn, @usro_crcn, @obsrvcns, @vsble_usro, N'4000018 Enfermera')


INSERT INTO [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (6, N'06', N'Auditoria medica por riesgo', @fcha_crcn, @usro_crcn, @vsble_usro)
INSERT INTO [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (6, 6, N'06', N'Auditoria medica por riesgo', @fcha_crcn, @fn_vgca, @fcha_crcn, @usro_crcn, @obsrvcns, @vsble_usro, N'4000019 Auditor Odontologico')


--Select * from [prm].[tbASFlujosCargos]
--Select * from [prm].[tbASFlujosCargos_Vigencias] 

