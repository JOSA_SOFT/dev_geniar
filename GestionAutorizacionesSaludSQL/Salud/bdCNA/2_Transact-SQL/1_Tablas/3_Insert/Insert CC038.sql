USE [BDCna]
GO
Declare @fcha_actl  Datetime,
        @usro       udtUsuario,
		@obsrvcns   udtObservacion,
		@vsble_usro udtLogico,
		@fn_vgncia  Datetime

Set     @fcha_actl  = GETDATE()
Set     @usro       = 'sismigra01'
Set     @vsble_usro = 'S'
Set     @fn_vgncia  = '9999-12-31'
Set     @obsrvcns   = '.'

INSERT [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (1, N'01', N'DOMI', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (2, N'02', N'Medicina del trabajo', @fcha_actl, @usro, @vsble_usro)

INSERT [prm].[tbASFlujosCargos] ([cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [fcha_crcn], [usro_crcn], [vsble_usro]) VALUES (3, N'03', N'Auditoria medico por riesgo', @fcha_actl, @usro, @vsble_usro)



INSERT [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (1, 1, N'01', N'DOMI', @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro, N'DOMI')

INSERT [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (2, 2, N'02', N'Medicina del trabajo', @fcha_actl, @fn_vgncia, @fcha_actl, @usro,@obsrvcns,@vsble_usro, N'Medicina del trabajo')

INSERT [prm].[tbASFlujosCargos_Vigencias] ([cnsctvo_vgnca_cdgo_fljo_crgo], [cnsctvo_cdgo_fljo_crgo], [cdgo_fljo_crgo], [dscrpcn_cdgo_fljo_crgo], [inco_vgnca], [fn_vgnca], [fcha_crcn], [usro_crcn], [obsrvcns], [vsble_usro], [grpo_adtr_prcso]) VALUES (3, 3, N'03', N'Auditoria medico por riesgo',  @fcha_actl, @fn_vgncia, @fcha_actl, @usro, @obsrvcns, @vsble_usro, N'Auditoria medico por riesgo')

GO
