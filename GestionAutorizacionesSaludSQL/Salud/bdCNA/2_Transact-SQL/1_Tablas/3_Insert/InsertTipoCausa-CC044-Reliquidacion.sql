USE[BDCna]
Declare @max_consecutivo_tipo_causa				int = 0,
		@max_consecutivo_tipo_causa_vigencia	int = 0,
		@max_consecutivo_causa					int	= 0,
		@max_consecutivo_causa_vigencia			int	= 0;

Set @max_consecutivo_tipo_causa = (Select max(cnsctvo_cdgo_tpo_csa_nvdd) from prm.tbASTipoCausaNovedad);


INSERT INTO prm.tbASTipoCausaNovedad(cnsctvo_cdgo_tpo_csa_nvdd, 
									 cdgo_tpo_csa_nvdd, 
									 dscrpcn_cdgo_tpo_csa_nvdd, 
									 fcha_crcn, 
									 usro_crcn, 
									 vsble_usro)
							 VALUES(
									(@max_consecutivo_tipo_causa+1), 
									(@max_consecutivo_tipo_causa+1), 
									'Causales reliquidación de solicitudes', 
									GETDATE(), 'sisaps01', 'S'
									);

							
Set @max_consecutivo_tipo_causa_vigencia = (Select max(cnsctvo_vgnca_cdgo_tpo_csa_nvdd) from prm.tbASTipoCausaNovedad_Vigencias);
INSERT INTO prm.tbASTipoCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_tpo_csa_nvdd
											  ,cnsctvo_cdgo_tpo_csa_nvdd
											  ,cdgo_tpo_csa_nvdd
											  ,dscrpcn_cdgo_tpo_csa_nvdd
											  ,inco_vgnca
											  ,fn_vgnca
											  ,fcha_crcn
											  ,usro_crcn
											  ,obsrvcns
											  ,vsble_usro)
										VALUES(@max_consecutivo_tipo_causa_vigencia+1, @max_consecutivo_tipo_causa+1, (@max_consecutivo_tipo_causa+1),'Causales reliquidación de solicitudes', GETDATE(), '9999-12-31', GETDATE(), 'sisaps01','.', 'S');										

Set @max_consecutivo_causa = (Select max(cnsctvo_cdgo_csa_nvdd) from prm.tbASCausaNovedad);
INSERT INTO prm.tbASCausaNovedad
           (cnsctvo_cdgo_csa_nvdd
           ,cdgo_csa_nvdd
           ,dscrpcn_cdgo_csa_nvdd
           ,fcha_crcn
           ,usro_crcn
           ,vsble_usro)
     VALUES
           (@max_consecutivo_causa+1
           ,@max_consecutivo_causa+1
           ,'Cambio liquidacion de convenio'
           ,getDate()
           ,'seti'
           ,'S');

INSERT INTO prm.tbASCausaNovedad
           (cnsctvo_cdgo_csa_nvdd
           ,cdgo_csa_nvdd
           ,dscrpcn_cdgo_csa_nvdd
           ,fcha_crcn
           ,usro_crcn
           ,vsble_usro)
     VALUES
           (@max_consecutivo_causa+2
           ,@max_consecutivo_causa+2
           ,'Requiere honorarios adicionales'
           ,getDate()
           ,'seti'
           ,'S');	

		   
Set @max_consecutivo_causa_vigencia = (Select max(cnsctvo_vgnca_cdgo_csa_nvdd) from prm.tbASCausaNovedad_Vigencias);		   
INSERT INTO prm.tbASCausaNovedad_Vigencias
           (cnsctvo_vgnca_cdgo_csa_nvdd
           ,cnsctvo_cdgo_csa_nvdd
           ,cdgo_csa_nvdd
           ,dscrpcn_cdgo_csa_nvdd
           ,inco_vgnca
           ,fn_vgnca
           ,fcha_crcn
           ,usro_crcn
           ,vsble_usro
           ,cnsctvo_cdgo_tpo_csa_nvdd
           ,rqre_nmro_atrzcn_ips)
     VALUES
           (@max_consecutivo_causa_vigencia+1
           ,@max_consecutivo_causa+1
           ,@max_consecutivo_causa+1
           ,'Cambio liquidacion de convenio'
           ,getDate()
           ,'9999-12-31'
           ,getDate()
           ,'seti'
           ,'S'
           ,@max_consecutivo_tipo_causa+1
           ,'N');

INSERT INTO prm.tbASCausaNovedad_Vigencias
           (cnsctvo_vgnca_cdgo_csa_nvdd
           ,cnsctvo_cdgo_csa_nvdd
           ,cdgo_csa_nvdd
           ,dscrpcn_cdgo_csa_nvdd
           ,inco_vgnca
           ,fn_vgnca
           ,fcha_crcn
           ,usro_crcn
           ,vsble_usro
           ,cnsctvo_cdgo_tpo_csa_nvdd
           ,rqre_nmro_atrzcn_ips)
     VALUES
           (@max_consecutivo_causa_vigencia+2
           ,@max_consecutivo_causa+2
           ,@max_consecutivo_causa+2
           ,'Requiere honorarios adicionales'
           ,getDate()
           ,'9999-12-31'
           ,getDate()
           ,'seti'
           ,'S'
           ,@max_consecutivo_tipo_causa+1
           ,'N');		   