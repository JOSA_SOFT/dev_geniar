Use bdCNA
Go

	Declare @fecha_actual		Date,
			@vsble_usro			UdtLogico,
			@fn_vgnca			Date,
			@usro_crcn			UdtUsuario;

	Set @fecha_actual = getDate();
	Set @vsble_usro = 'S';
	Set @fn_vgnca = '99991231';
	Set	@usro_crcn = 'qvisionclr';

Insert Into bdCNA.dbo.tbTiposMarcasOPS
(cnsctvo_cdgo_tps_mrcs_ops,	cdgo_tps_mrcs_ops,	dscrpcn_tps_mrcs_ops,	fcha_crcn,		usro_crcn,	vsble_usro)
Values
(5,							5, 					'Acceso directo',		@fecha_actual,	@usro_crcn, @vsble_usro)

Insert Into bdCNA.dbo.tbTiposMarcasOPS_Vigencias
(cnsctvo_vgnca_tps_mrcs_ops,	cnsctvo_cdgo_tps_mrcs_ops,	cdgo_tps_mrcs_ops,	dscrpcn_tps_mrcs_ops,	inco_vgnca,		fn_vgnca,	fcha_crcn,		usro_crcn,	obsrvcns,	vsble_usro)
Values
(5,								5,							5, 					'Acceso Directo',		@fecha_actual,	@fn_vgnca,	@fecha_actual,	@usro_crcn, '.',		@vsble_usro)

