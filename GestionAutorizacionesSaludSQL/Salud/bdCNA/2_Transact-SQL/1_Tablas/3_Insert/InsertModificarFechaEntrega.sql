USE[BDCna]

go

INSERT INTO prm.tbASTipoCausaNovedad(cnsctvo_cdgo_tpo_csa_nvdd, cdgo_tpo_csa_nvdd, dscrpcn_cdgo_tpo_csa_nvdd, 
									 fcha_crcn, usro_crcn, vsble_usro)
							VALUES(7, '07', 'Causales cambio de fecha de entrega', GETDATE(), 'sisaps01', 'S');
							

INSERT INTO prm.tbASTipoCausaNovedad_Vigencias(cnsctvo_vgnca_cdgo_tpo_csa_nvdd
											  ,cnsctvo_cdgo_tpo_csa_nvdd
											  ,cdgo_tpo_csa_nvdd
											  ,dscrpcn_cdgo_tpo_csa_nvdd
											  ,inco_vgnca
											  ,fn_vgnca
											  ,fcha_crcn
											  ,usro_crcn
											  ,obsrvcns
											  ,vsble_usro)
										VALUES(7, 7, '07','Causales cambio de fecha de entrega', GETDATE(), '9999-12-31', GETDATE(), 'sisaps01','.', 'S');										

Go
