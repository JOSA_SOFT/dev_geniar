use bdCNA

Go

--Select * From bdsisalud.dbo.tbPMTipoUbicacionPaciente_Vigencias
--Select * from bdsisalud.dbo.tbClasesAtencion_Vigencias -- 2
--Select * from prm.tbASRelUbicacionPacientexClaseAtencion_Vigencias

Update  bdcna.gsa.tbASSolicitudesAutorizacionServicios
Set     cnsctvo_cdgo_tpo_ubccn_pcnte = 1
Where   cnsctvo_cdgo_clse_atncn = 1 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0

Update bdcna.gsa.tbASSolicitudesAutorizacionServicios 
Set    cnsctvo_cdgo_tpo_ubccn_pcnte = 3
Where  cnsctvo_cdgo_clse_atncn = 2 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0

Update bdcna.gsa.tbASSolicitudesAutorizacionServicios 
Set    cnsctvo_cdgo_tpo_ubccn_pcnte = 2
Where  cnsctvo_cdgo_clse_atncn = 3 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0

Update bdcna.gsa.tbASSolicitudesAutorizacionServicios 
Set    cnsctvo_cdgo_tpo_ubccn_pcnte = 3
Where  cnsctvo_cdgo_clse_atncn = 4 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0

Update bdcna.gsa.tbASSolicitudesAutorizacionServicios 
Set    cnsctvo_cdgo_tpo_ubccn_pcnte = 1
Where  cnsctvo_cdgo_clse_atncn = 7 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0

Update bdcna.gsa.tbASSolicitudesAutorizacionServicios 
Set    cnsctvo_cdgo_tpo_ubccn_pcnte = 3
Where  cnsctvo_cdgo_clse_atncn = 8 And cnsctvo_cdgo_tpo_ubccn_pcnte = 0


