USE bdCNA
GO

-- tbASEstadosServiciosSolicitados
UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 0;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 1;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 2;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 3;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 4;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 5;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 6;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 7;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 8;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 9;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 10;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 11;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 12;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 13;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 14;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 15;

UPDATE prm.tbASEstadosServiciosSolicitados
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_cdgo_estdo_srvco_slctdo = 16;


-- tbASEstadosServiciosSolicitados_Vigencias
UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 0;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 1;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 2;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 3;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 4;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 5;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 6;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 7;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 8;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 9;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 10;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'S'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 11;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 12;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 13;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 14;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 15;

UPDATE prm.tbASEstadosServiciosSolicitados_Vigencias
SET    mrca_estdo_trnstro = 'N'
WHERE  cnsctvo_vgnca_estdo_srvco_slctdo = 16;