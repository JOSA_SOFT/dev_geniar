Use bdCna

Go

-- Se llenan los campos con los valores homologos.
Update BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias
Set cnsctvo_cdgo_tps_mrcs_ops = 1 -- Descarga IPS
Where	cnsctvo_cdgo_tpo_mrca = 5 -- Descarga IPS

Update BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias
Set cnsctvo_cdgo_tps_mrcs_ops = 2 -- Descarga Afiliado
Where	cnsctvo_cdgo_tpo_mrca = 2 -- Descarga afiliado

Update BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias
Set cnsctvo_cdgo_tps_mrcs_ops = 3 -- Enviar correo
Where	cnsctvo_cdgo_tpo_mrca = 3 -- Enviar correo

Update BdCna.prm.tbASTipoMarcaServiciosSolicitados_Vigencias
Set cnsctvo_cdgo_tps_mrcs_ops = 5 -- Acceso Directo
Where cnsctvo_cdgo_tpo_mrca = 7 -- Acceso directo

Update prm.tbasestadosserviciossolicitados_vigencias
Set cnsctvo_cdgo_estdo_hmlgdo_sprs = 1 -- Ingresada Sipres
Where	cnsctvo_cdgo_estdo_srvco_slctdo = 7 -- Aprobada Mega