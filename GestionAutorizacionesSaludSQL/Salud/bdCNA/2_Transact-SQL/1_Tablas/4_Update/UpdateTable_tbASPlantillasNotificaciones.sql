use bdCNA

Go

Update bdCNA.prm.tbASPlantillasNotificaciones_Vigencias
Set    cnsctvo_cdgo_plntlla_mnsje = 1
Where  cnsctvo_cdgo_plntlla_ntfccn = 1


Update bdCNA.prm.tbASPlantillasNotificaciones_Vigencias
Set    cnsctvo_cdgo_plntlla_mnsje = 2
Where  cnsctvo_cdgo_plntlla_ntfccn = 2

Update bdCNA.prm.tbASPlantillasNotificaciones_Vigencias
Set    cnsctvo_cdgo_plntlla_mnsje = 4
Where  cnsctvo_cdgo_plntlla_ntfccn = 4

Update bdCNA.prm.tbASPlantillasNotificaciones_Vigencias
Set    cnsctvo_cdgo_plntlla_mnsje = 5
Where  cnsctvo_cdgo_plntlla_ntfccn = 5

Update bdCNA.prm.tbASPlantillasNotificaciones_Vigencias
Set    cnsctvo_cdgo_plntlla_mnsje = 9
Where  cnsctvo_cdgo_plntlla_ntfccn = 6
