use bdCNA

Go

Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Error al generarla'       
Where  cnsctvo_cdgo_csa_nvdd = 1

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Error al generarla'       
Where  cnsctvo_cdgo_csa_nvdd = 1


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Error en la programación'       
Where  cnsctvo_cdgo_csa_nvdd = 2

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Error en la programación'       
Where  cnsctvo_cdgo_csa_nvdd = 2


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Error Digitacion'       
Where  cnsctvo_cdgo_csa_nvdd = 3

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Error Digitacion'       
Where  cnsctvo_cdgo_csa_nvdd = 3


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Afiliado Fallecido'       
Where  cnsctvo_cdgo_csa_nvdd = 4

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Afiliado Fallecido'       
Where  cnsctvo_cdgo_csa_nvdd = 4


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Causales de asignacion automatica por modificacion OPS'       
Where  cnsctvo_cdgo_csa_nvdd = 50

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Causales de asignacion automatica por modificacion OPS'       
Where  cnsctvo_cdgo_csa_nvdd = 50


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio direccionamiento'       
Where  cnsctvo_cdgo_csa_nvdd = 51

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio direccionamiento'       
Where  cnsctvo_cdgo_csa_nvdd = 51


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Reliquidacion'       
Where  cnsctvo_cdgo_csa_nvdd = 52

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Reliquidacion'       
Where  cnsctvo_cdgo_csa_nvdd = 52


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio fecha entrega'       
Where  cnsctvo_cdgo_csa_nvdd = 53

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio fecha entrega'       
Where  cnsctvo_cdgo_csa_nvdd = 53


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Afiliado sin derecho'       
Where  cnsctvo_cdgo_csa_nvdd = 54

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Afiliado sin derecho'       
Where  cnsctvo_cdgo_csa_nvdd = 54


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Usuario no toma el servicio'       
Where  cnsctvo_cdgo_csa_nvdd = 55

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Usuario no toma el servicio'       
Where  cnsctvo_cdgo_csa_nvdd = 55


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por perdida'       
Where  cnsctvo_cdgo_csa_nvdd = 56

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por perdida'       
Where  cnsctvo_cdgo_csa_nvdd = 56	


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops previa por vencimiento'       
Where  cnsctvo_cdgo_csa_nvdd = 57

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops previa por vencimiento'       
Where  cnsctvo_cdgo_csa_nvdd = 57	


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Error funcionalidad MEGA'       
Where  cnsctvo_cdgo_csa_nvdd = 58

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Error funcionalidad MEGA'       
Where  cnsctvo_cdgo_csa_nvdd = 58	
	

Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por prestador'       
Where  cnsctvo_cdgo_csa_nvdd = 59

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por prestador'       
Where  cnsctvo_cdgo_csa_nvdd = 59	
	

Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por hallazgo quirúrgico'       
Where  cnsctvo_cdgo_csa_nvdd = 60

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Cambio de ops por hallazgo quirúrgico'       
Where  cnsctvo_cdgo_csa_nvdd = 60	


Update prm.tbASCausaNovedad
Set    dscrpcn_cdgo_csa_nvdd = 'Medicamento agostado o desabastecido'       
Where  cnsctvo_cdgo_csa_nvdd = 61

Update prm.tbASCausaNovedad_Vigencias
Set    dscrpcn_cdgo_csa_nvdd = 'Medicamento agostado o desabastecido'       
Where  cnsctvo_cdgo_csa_nvdd = 61	
	
	


