
USE [bdSisalud]
GO

Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  DISABLE trigger all

Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 4
Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 17
Update css set cnsctvo_cdgo_fljo_crgo = 2 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 18
Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 19
Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 20
Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 28
Update css set cnsctvo_cdgo_fljo_crgo = 3 from [bdSisalud].[dbo].[tbCargosSOS_Vigencias] css where cnsctvo_cdgo_crgo_ss = 29

Alter Table bdSisalud.dbo.tbCargosSOS_Vigencias  ENABLE trigger all
GO
