USE [bdCNA]
GO

Update prm.tbASEstadosServiciosSolicitados_Vigencias
Set    mrca_lqdcn_prstcn = 'N'
Where  cnsctvo_cdgo_estdo_srvco_slctdo = 11

go

Update BDCna.prm.tbASFlujosCargos
Set    dscrpcn_cdgo_fljo_crgo = 'Auditoria medica por riesgo'
Where  cnsctvo_cdgo_fljo_crgo = 3

go

Update BDCna.prm.tbASFlujosCargos_Vigencias
Set    dscrpcn_cdgo_fljo_crgo = 'Auditoria medica por riesgo'
Where  cnsctvo_cdgo_fljo_crgo = 3

go