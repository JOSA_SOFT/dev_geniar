USE [BDCna]
GO
GRANT EXECUTE ON gsa.spAsConsultaDatosGestionAutorizacionPrestacionesDevueltas TO autsalud_rol;
GRANT EXECUTE ON gsa.spAsGuardarGestionAutorizaNoConformidadPrestacion TO autsalud_rol;
GRANT EXECUTE ON gsa.spASValidarReglasFlujosBPM TO autSalud_rol;
GRANT EXECUTE ON gsa.spASconsultaDatosSolicitudModificarFechaEntrega TO autsalud_rol;
GRANT EXECUTE ON gsa.spASGuardarInformacionFechaEntrega TO autsalud_rol;
GRANT EXECUTE ON gsa.spASRegistrarCausalesDevolucionPresolicitud TO autsalud_rol;
GRANT EXECUTE ON dbo.spPTLConsultarCausalesNegacionPreSolicitud TO autsalud_rol;

GRANT EXECUTE ON gsa.spasobtenerinformacionintegracionsipresotros TO autsalud_rol;
GRANT EXECUTE ON gsa.spascargarinformacionsipres TO autsalud_rol;
GRANT EXECUTE ON gsa.spasobtenerinformacionintegracionsipresmega TO autsalud_rol;
GRANT EXECUTE ON gsa.spasejecutarintegracionsipres TO autsalud_rol;
/*sislbr01*/
GRANT EXECUTE ON dbo.spPMCrearHistoricoEstadosAtencionOPSMasivo TO autsalud_rol;
GRANT EXECUTE ON gsa.spASGrabarGenerarNumeroUnicoOpsMasivo TO autsalud_rol
/*Permiso procedimiento validacion caja abierta*/
GRANT EXECUTE ON cja.spRCValidaAperturaCaja TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerConceptoPagoOPS TO autsalud_rol;


/*Permiso procedimiento validacion caja abierta*/
GRANT EXECUTE ON cja.spRCAperturaCaja TO autsalud_rol;

/*Permiso procedimiento de consultar caja abierta*/
GRANT EXECUTE ON cja.spRCConsultarCajaAbierta TO autsalud_rol;

/*Permiso procedimiento de consultar denominaciones*/
GRANT EXECUTE ON cja.spRCConsultarDenominaciones TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerContratosGrupoFamiliar TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerNotasCreditosGrupoFamiliar TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerNUIGrupoFamiliar TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerInformacionOPSValor TO autsalud_rol;
GRANT EXECUTE ON cja.spRCActualizarEstadoDocumentoCaja TO autsalud_rol;
GRANT EXECUTE ON cja.spRCInsertarDocRelacionado TO autsalud_rol;
GRANT EXECUTE ON cja.spRCGuardarDocumentoCaja TO autsalud_rol;

GRANT EXECUTE ON cja.spRCObtenerBancos TO autsalud_rol;
GRANT EXECUTE ON cja.spRCObtenerMetodoDevolucion TO autsalud_rol;
GRANT EXECUTE ON cja.spRCGuardarDatosContacto TO autsalud_rol;

GRANT EXECUTE ON  [cja].[spRCGuardarReciboCaja]  TO autsalud_rol;

/*Permiso procedimiento de sumar notas credito estado cobrado*/
GRANT EXECUTE ON cja.spRCObtenerTotalesCierre TO autsalud_rol;
GRANT EXECUTE ON cja.spRCGenerarNotaCreditoAnulacion TO autsalud_rol;
GRANT EXECUTE ON cja.spRCGenerarInformeCierre TO autsalud_rol;

/*Permiso procedimiento de consultar causales descuadre*/
GRANT EXECUTE ON cja.spRCConsultaCausalesDescuadrePorCaja TO autsalud_rol;
GRANT EXECUTE ON cja.spRCConsultarCausalesDescuadre TO autsalud_rol;

/*Permiso procedimiento que cierra la caja*/
GRANT EXECUTE ON cja.spRCCierreCaja TO autsalud_rol;

/*Permiso procedimiento de consultar coordinador*/
GRANT EXECUTE ON cja.spRCConsultarCoordinador TO autsalud_rol;

/*Permiso procedimiento de consultar oficina*/
GRANT EXECUTE ON cja.spRCConsultarOficina TO autsalud_rol;

/*Permiso procedimiento de consulta movimientos caja por una oficina*/
GRANT EXECUTE ON cja.spRCConsultarMovimientosCajaPorOficina TO autsalud_rol;

/*Permiso procedimiento para cerrar oficina*/
GRANT EXECUTE ON cja.spRCCierreOficina TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaRecibosCajaUsuario TO autsalud_rol;
GRANT EXECUTE ON cja.spRCGuardarDetMovimientoCaja TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaNotasCreditoRecibo TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaOPSValorNotasCredito TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaInformesDetalle TO autsalud_rol;
/*Permiso procedimiento de consulta movimientos caja por cierre oficina*/
GRANT EXECUTE ON cja.spRCConsultarMovimientosCajaPorCierrerOficina TO autsalud_rol;

/*Permiso procedimiento de consulta oficina por movimiento actual*/
GRANT EXECUTE ON cja.spRCConsultarOficinaPorMovimientoActual TO autsalud_rol;

/*Permiso procedimiento de consulta grupos de OPS y su valor*/
GRANT EXECUTE ON cja.spRCConsultaOPSValorNotasCredito TO autsalud_rol;

GRANT EXECUTE ON cja.spRCGuardarReciboCajaOrquestador TO autsalud_rol;

GRANT EXECUTE ON cja.spRCBuscadorUsuarioWeb TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaAfiliadoID TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaPrestacionesNumeroOPS TO autsalud_rol;
 
GRANT EXECUTE ON cja.spRCObtenerCausalesExoneracion TO autsalud_rol;

GRANT EXECUTE ON cja.spRCObtenerEstadoDocumentos TO autsalud_rol;

GRANT EXECUTE ON cja.spRCGenerarInformeNotaCredito TO autsalud_rol;

GRANT EXECUTE ON cja.spRCObtenerSoportesDocumento TO autsalud_rol;

GRANT EXECUTE ON cja.spRCConsultaDatosContacto TO autsalud_rol;

GRANT EXECUTE ON cja.spRCActualizarNotaCredito TO autsalud_rol;

GRANT EXECUTE ON cja.spRCActualizarContacto TO autsalud_rol;

GRANT EXECUTE ON gsa.spASObtenerOficinas TO autsalud_rol;

GRANT EXECUTE ON cja.spRCObtenerReciboCajaSolicitud TO autsalud_rol;