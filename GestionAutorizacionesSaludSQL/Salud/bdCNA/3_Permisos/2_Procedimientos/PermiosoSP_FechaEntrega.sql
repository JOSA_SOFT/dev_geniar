use bdCNA

Go

GRANT EXECUTE on gsa.spASEjecutarCambioFechaEntregaOPS to autsalud_rol
GRANT EXECUTE on gsa.spASTraerEstadosMega to autsalud_rol
GRANT EXECUTE on gsa.spASTraerGruposEntrega to autsalud_rol
GRANT EXECUTE on gsa.spASCalcularDetalleCambioFechaEntregaOPS to autsalud_rol
GRANT EXECUTE on gsa.spASRecalcularDetalleCambioFechaEntregaOPS  to autsalud_rol
