USE [BDCna]

GO

GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDatosContacto] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDenominaciones] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDenominaciones_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDetalleDenominaciones] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCEstadosDocumentosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCEstadosDocumentosCaja_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCEstadosMovimientosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCEstadosMovimientosCaja_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCHistoricoEstadosMovimientosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCMetodosDevolucionDinero] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCMetodosDevolucionDinero_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCConceptosGeneracionNotasCredito_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCTiposDenominacion] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCTiposDenominacion_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCTiposDocumentosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCTiposDocumentosCaja_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCBancos] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCBancos_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCCausalesDescuadre] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCCausalesDescuadre_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCCierreOficina] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCConceptosGeneracionNotasCredito] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCMovimientosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCCoordinadorAsixOficina] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCCoordinadorAsixOficina_Vigencias] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCSoportesDocumentosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDocumentosRelacionados] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDocumentosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCDetMovimientosCaja] TO autsalud_webusr
GRANT SELECT, INSERT, UPDATE ON cja.[tbRCMovimientosCajaxCausalesDescuadre] TO autsalud_webusr
GRANT CONTROL, UPDATE ON [cja].[seqRCNotasCredito] TO autsalud_webusr
GRANT CONTROL, UPDATE ON [cja].[seqRCRecibosCaja] TO autsalud_webusr

