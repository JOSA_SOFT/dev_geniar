use BDAfiliacionValidador
go

grant select,insert,update on dbo.tbDocumentosSoportesxAfiliadoValidador to [autsalud_rol] 

go

use BdCna
go

grant select,insert,update on prm.tbAsTipoOrigenSolicitud to [autsalud_rol] 
grant select,insert,update on prm.tbAsTipoOrigenSolicitud to [autsalud_rol] 

grant select,insert,update on prm.tbAsTipoOrigenSolicitud_Vigencias to [autsalud_rol] 
grant select,insert,update on prm.tbAsTipoOrigenSolicitud_Vigencias to [autsalud_rol] 

GRANT SELECT, INSERT, UPDATE ON OBJECT::dbo.tbPrestacionesNegadasPreSolicitud TO autSalud_rol;
GRANT SELECT ON OBJECT::prm.tbASFlujosCargos_Vigencias TO autsalud_rol;

GRANT SELECT, INSERT, UPDATE ON OBJECT::prm.tbASAutorizaNoConformidadValidacion TO autSalud_rol;                                            
GRANT SELECT, INSERT, UPDATE ON OBJECT::prm.tbASAutorizaNoConformidadValidacion_Vigencias TO autSalud_rol;

   															
