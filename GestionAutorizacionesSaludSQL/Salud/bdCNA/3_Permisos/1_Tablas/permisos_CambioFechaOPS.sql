USE BdCNA 
GO
 GRANT SELECT on  gsa.tbASResultadoFechaEntrega				to [autsalud_rol]
 GRANT UPDATE ON  gsa.tbASResultadoFechaEntrega				to [autsalud_rol]
 GRANT INSERT ON  gsa.tbASResultadoFechaEntrega				to [autsalud_rol]
 GRANT INSERT ON  gsa.tbASTrazaModificacion					to [autsalud_rol]
