USE [bdContratacion]
GO
/****** Object:  StoredProcedure [dbo].[spLSLiquidacionMasivaListaPrecios]    Script Date: 6/27/2017 7:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*--------------------------------------------------------------------------------------------------------------------------------- 
* Metodo o PRG:			spLSLiquidacionMasivaListaPrecios
* Desarrollado por:		<\A	Ing. Samuel Muñoz	A\>
* Descripcion:			<\D	Liquida prestaciones basada en convenios tarificado en un lista de Precios de forma masiva	D\>
* Parametros:			<\P   	P\>
* Fecha Creacion:		<\FC	25/05/2013	FC\>
*----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza la modificacion del procedimiento en donde se inserta el nivel de atencion del prestador
							 en las tablas temporales de los servicios y conceptos a liquidar	
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Fecha Modificacion:	<\FM 05/12/2016		FM\>
*-----------------------------------------------------------------------------------------------------------------------------------
* DATOS DE MODIFICACION 
*-----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Carlos Andres Lopez		AM\>
* Descripcion:			<\DM 
                             Se agrega Join para cruzar con tabla bdContratacion.dbo.tbModeloConveniosPrestacionesXPlan 
							 necesario para sacar las vigencias correctas.
							 Se agrega condicional para servicios  NO QX ya que si no hay lista de precios no se debe liquidar.
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM @valor_si
							 @valor_no
							 @frma_lqdcn4
							 @frma_lqdcn2
							 @valor_cero
							 @cnsctvo_cdgo_nvl_atncn0
							 @cnsctvo_cdgo_tpo_cdfccn11
							 @cnsctvo_cdgo_tpo_mdlo1
							 @valor_cien
							 @valor_uno VM\>
* Fecha Modificacion:	<\FM 30/12/2016		FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Carlos Andres Lopez		AM\>
* Descripcion:			<\DM 
                             Se retira Join agregado en modificacion anterior
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 13/01/2016		FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza modificacion del procedimiento para que los valores del Detalle de Lista Precios
							 se calculen teniendo en cuenta la lista vigente para el modelo de prestacion y se puedan 
							 tomar los paquetes en la liquidacion
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 06/06/2016		FM\>
*---------------------------------------------------------------------------------------------------------------------------------- 
* DATOS DE MODIFICACION 
*----------------------------------------------------------------------------------------------------------------------------------    
* Modificado Por:		<\AM Ing. Victor Hugo Gil Ramos		AM\>
* Descripcion:			<\DM 
                             Se realiza modificacion del procedimiento para que agrupe las prestaciones x item de presupuesto
							 con el fin de que realice la liquidacion correctamente
                         DM\>
* Nuevos Parametros:	<\PM		PM\>
* Nuevas Variables:	    <\VM VM\>
* Fecha Modificacion:	<\FM 22/06/2016		FM\>
*----------------------------------------------------------------------------------------------------------------------------------
* Estructuracion del Store Procedure
*
* 1.  Creación de Tablas Temporales
* 2.  Población de las Tablas Temporales de Trabajo
* 3.  Liquida las Prestaciones que se Prestaran por Paquetes
* 4.  Liquida las Prestaciones Qx que SI se liquidan por Modelo de Conceptos
* 5.  Liquida las Prestaciones Qx que NO se liquidan por Modelo de Conceptos 
* 6.  Liquida las Prestaciones NO Qx, Medicamentos y Otros Servicios que NO se liquidan por Modelo de Conceptos
* 7.  Liquida las Prestaciones NO Qx, Medicamentos y Otros Servicios que SI se liquidan por Modelo de Conceptos
* 8.  Liquida los Conceptos de Prestaciones Qx pero que NO son Contratadas por Conceptos.
* 9.  Liquida los Conceptos que NO estaban Incluidos en los Paquetes
* 10. Actualización de los cursores de Origen
*  
* Estructura de los cursores de entrada
*
*	Table #tbServiciosALiquidarOrigen(
*   cnsctvo_srvco_lqdr			Int, 
*	cnsctvo_cdgo_cps			Int, 
*	cnsctvo_cdgo_tpo_atncn		Int,	
*	bltrl						Char(1), 
*	msma_va						Char(1),
*	cntdd						Int, 
*	rmpe_pqte					Char(1),
*	espcldd						Int, 
*	vlr_lqdcn					Int,
*	agrpdr_srvco_lqdr			Int,
*	cdgo_intrno_prstdr			Char(8),
*	fcha_lqdcn					Datetime,
*	cnsctvo_cdgo_pln			Int,
*	rsltdo_lqdcn				Int)
*
*
*	Table #tbConceptosServiciosALiquidarOrigen(
*	cnsctvo_cncpto_lqdr			Int, 
*	cnsctvo_srvco_lqdr			Int,	
*	cnsctvo_cdgo_cncpto_gsto	Int, 
*	vlr_lqdcn_cncpto			Int,
*	cnsctvo_cdgo_clse_hbtcn		Int,
*	rsltdo_lqdcn				Int)
*
*----------------------------------------------------------------------------------------------------------------------------------*/
ALTER Procedure [dbo].[spLSLiquidacionMasivaListaPrecios]
--
As
Begin

	Set NoCount On

	Declare		@MaximoConsecutivoConcepto	 Int,
				@MaximoConsecutivoPrestacion Int,
				@cnsctvo_cdgo_nvl			 udtConsecutivo,
				@valor_si					 Udtlogico,
				@valor_no					 UdtLogico,
				@frma_lqdcn4				 Int,--Paquetes
				@frma_lqdcn2				 Int,--Modelo de conceptos
				@valor_cero					 Int,
				@cnsctvo_cdgo_nvl_atncn0	 UdtConsecutivo,
				@cnsctvo_cdgo_tpo_cdfccn11   UdtConsecutivo,
				@cnsctvo_cdgo_tpo_mdlo1		 UdtConsecutivo, --Prestaciones
				@valor_cien					 Int,
				@valor_uno					 Int,
				@cnsctvo_cdgo_tpo_atncn      udtConsecutivo,
				@cndcnl_cmplto               Char(2)       ,
				@opcnl_cmplto                Char(2)       ,
				@cndcnl_incmplto             Char(2)       ,
				@tpo_prcntje_lqdcn           udtConsecutivo;

	/*
	==================================================================================================
	Sección para Pruebas
	==================================================================================================
	CREATE TABLE #tbserviciosaliquidarorigen (
		cnsctvo_srvco_lqdr				udtconsecutivo,
		cnsctvo_cdgo_cps				udtconsecutivo,
		cnsctvo_cdgo_tpo_atncn			udtconsecutivo,
		bltrl							CHAR(1),
		msma_va							CHAR(1),
		cntdd							INT,
		rmpe_pqte						CHAR(1),
		espcldd							INT,
		vlr_lqdcn						INT,
		agrpdr_srvco_lqdr				INT,
		cdgo_intrno_prstdr				CHAR(8),
		fcha_lqdcn						DATETIME,
		cnsctvo_cdgo_pln				udtconsecutivo,
		rsltdo_lqdcn					INT,
		cnsctvo_pqte					INT,
		cnsctvo_cdgo_dt_lsta_prco		udtconsecutivo  DEFAULT 0,
		cnsctvo_cdgo_lsta_prco			udtconsecutivo  DEFAULT 0
	);


	Create Table #tbConceptosServiciosALiquidarOrigen
	(
		cnsctvo_cncpto_lqdr				udtconsecutivo,
		cnsctvo_srvco_lqdr				udtconsecutivo,
		cnsctvo_cdgo_cncpto_gsto		udtconsecutivo,
		vlr_lqdcn_cncpto				INT,
		cnsctvo_cdgo_clse_hbtcn			udtconsecutivo,
		rsltdo_lqdcn					INT				DEFAULT 0,
		cnsctvo_cdgo_cncpts_dt_lsta_prco udtconsecutivo DEFAULT 0
	)


	--Debe llenar los datos de las estructuras temporal de la prestacion y concepto de gasto prestacion
	Insert 
	Into	#tbServiciosALiquidarOrigen(
			cnsctvo_srvco_lqdr,				cnsctvo_cdgo_cps,			cnsctvo_cdgo_tpo_atncn,	
			bltrl,							msma_va,					cntdd,	
			rmpe_pqte,						espcldd ,					vlr_lqdcn,
			agrpdr_srvco_lqdr,				cdgo_intrno_prstdr,			fcha_lqdcn,
			cnsctvo_cdgo_pln,				rsltdo_lqdcn,				cnsctvo_pqte)
	Values	(1,	27900,	1,	@valor_no,	@valor_no,	1,	@valor_no,	9,	0,	69232,	91369,   	'2016-12-30 10:34:11.433',	9,	0,	0)

	-- Concepto Gasto por cada concepto de gasto se incrementa el campo cnsctvo_cncpto_lqdr y el servicio al que pertenece el concepto gasto 

	Insert 
	Into	#tbConceptosServiciosALiquidarOrigen(
			cnsctvo_cncpto_lqdr,			cnsctvo_srvco_lqdr,			cnsctvo_cdgo_cncpto_gsto,		
			vlr_lqdcn_cncpto,				cnsctvo_cdgo_clse_hbtcn,	rsltdo_lqdcn)
	Values	(1,	1,	1,	0,	0, 0),
			(2,	1,	3,	0,	0, 0),
			(3,	1,	11,	0,	0, 0),
			(4,	1,	19,	0,	0, 0),
			(5,	1,	21,	0,	0, 0)

	--Insert 
	--Into	#tbConceptosServiciosALiquidarOrigen(
	--		cnsctvo_cncpto_lqdr,			cnsctvo_srvco_lqdr,			cnsctvo_cdgo_cncpto_gsto,		
	--		vlr_lqdcn_cncpto,				cnsctvo_cdgo_clse_hbtcn,	rsltdo_lqdcn)
	--Values	(2,								1,							21,									
	--		     0,								0,							0 )

	=====================================================================================================
	* Inicializacion de variables
	*=====================================================================================================*/

	Set @MaximoConsecutivoConcepto	 = 0   ;
	Set @MaximoConsecutivoPrestacion = 0   ;
	Set @valor_si                    = 'S' ;
	Set @valor_no                    = 'N' ;
	Set @frma_lqdcn4                 = 4   ;
	Set @frma_lqdcn2                 = 2   ;
	Set @valor_cero                  = 0   ;
	Set	@cnsctvo_cdgo_nvl_atncn0     = 0   ;
	Set @cnsctvo_cdgo_tpo_cdfccn11   = 11  ;
	Set @cnsctvo_cdgo_tpo_mdlo1      = 1   ;
	Set	@valor_cien                  = 100 ;
	Set @valor_uno                   = 1   ;
	Set @cnsctvo_cdgo_tpo_atncn      = 3   ;
	Set @cndcnl_cmplto               = 'CC'; -- CC-Condicional Completo;
	Set @opcnl_cmplto                = 'OC'; -- OC-Opcional Completo
	Set @cndcnl_incmplto             = 'CI'; -- Condicional Incompleto
	Set @tpo_prcntje_lqdcn           = 2   ;


	/*=====================================================================================================
	 * 1.  Creación de Tablas Temporales
	 *=====================================================================================================*/

	Create 
	Table  #tbGruposALiquidar(id_agrpdr_srvco_lqdr    udtConsecutivo Identity(1,1),
	                          agrpdr_srvco_lqdr		  udtConsecutivo          ,
		                      cdgo_intrno_prstdr	  udtCodigoIPS            ,
		                      fcha_lqdcn			  Datetime                , 
		                      cnsctvo_cdgo_pln		  udtConsecutivo          ,
		                      cnsctvo_cdgo_nvl_prstdr udtConsecutivo Default 0,
		                      vlr_incl_jrrqa		  Int            Default 0,
	                          Primary Key (id_agrpdr_srvco_lqdr)
							 )

	Create 
	Table  #tbServiciosALiquidar(cnsctvo_srvco_lqdr	     	     udtConsecutivo             , -- Llave Primaria
		                         cnsctvo_cdgo_cps			     udtConsecutivo             , -- Codigo CUPS del Servicio
		                         cnsctvo_cdgo_tpo_atncn		     udtConsecutivo             , -- Tipo de Atención
		                         bltrl						     udtCodigo                  , -- (S/N) Para defnir si la prestación es bilateral
		                         msma_va					     udtCodigo                  , -- (S/N) Para definir si la prestación es por la misma via
		                         cntdd		  				     Int                        , -- Cantidad 
		                         rmpe_pqte					     udtCodigo                  , -- Indica si el paquete se rompe o no S
		                         espcldd					     udtConsecutivo             , -- Especialidad
		                         vlr_lqdcn					     Float                      , -- Valor de la liquidación, es el campo en el que se retorna el resultado de la liquidación.
		                         agrpdr_srvco_lqdr			     udtConsecutivo             , -- Valor que indica que la prestación pertenece a un mismo grupo de atención a liquidar
		                         fcha_lqdcn					     Datetime                   ,
		                         cnsctvo_cdgo_lsta_prco		     udtConsecutivo	Default 0  ,
		                         qrrgco						     udtCodigo		Default 'N',
		                         cnsctvo_cdgo_sccn 	             udtConsecutivo	Default 0  ,
		                         frma_lqdcn					     Int			Default 0  ,
		                         cnsctvo_pqte				     udtConsecutivo	Default 0  ,
		                         msma_espcldd			         udtCodigo		Default 'S',
		                         cnsctvo_cdgo_dt_lsta_prco	     udtConsecutivo	Default 0  ,
		                         cnsctvo_cdgo_tpo_mdlo		     udtConsecutivo	Default 0  ,
		                         jrrqa					         Int			Default 0  ,
		                         lddo						     udtCodigo		Default 'N',
		                         cnsctvo_rgstro_pqte		     udtConsecutivo	Default 0  ,  
		                         cnsctvo_cdgo_nvl_atncn		     udtConsecutivo	Default 0  ,
		                         rsltdo_lqdcn				     Int		    Default 0  ,
								 cdgo_intrno_prstdr	             udtCodigoIPS            ,
	                             Primary Key (cnsctvo_srvco_lqdr)
								)

	Create 
	Table  #tbConceptosServiciosALiquidar(cnsctvo_cncpto_lqdr				udtConsecutivo              , -- Llave Primaria
		                                  cnsctvo_srvco_lqdr				udtConsecutivo              , -- Llave Foranea
		                                  cnsctvo_cdgo_cncpto_gsto			udtConsecutivo              , -- Consecutivo del concepto de gasto a liquidar
		                                  vlr_lqdcn_cncpto					Float                       , -- Valor de la liquidación del concepto
		                                  cnsctvo_cdgo_clse_hbtcn			udtConsecutivo              ,
		                                  fctr_msma_va						Float		    Default 1.00,
		                                  cnsctvo_cdgo_cncpts_dt_lsta_prco	udtConsecutivo	Default 0   ,
		                                  cntmpldo_pqte						udtLogico	    Default 'N' ,
		                                  cnsctvo_cdgo_trfa					udtConsecutivo	Default 0   ,
		                                  cnsctvo_cdgo_nvl_atncn			udtConsecutivo	Default 0   ,
		                                  rsltdo_lqdcn						Int			    Default 0   ,
										  cnsctvo_prstcn_pqte 	            udtConsecutivo		
	                                      Primary Key (cnsctvo_cncpto_lqdr)
										 )			

	Create 
	Table  #tbLSDetListaPreciosxSucursalxPlan(cnsctvo_cdgo_dt_lsta_prco	udtConsecutivo,
		                                      cnsctvo_cdgo_lsta_prco	udtConsecutivo,
		                                      cnsctvo_cdgo_tpo_atncn	udtConsecutivo,
		                                      cnsctvo_prstncn			udtConsecutivo,
		                                      cnsctvo_cdgo_tpo_cdfccn	udtConsecutivo,
	                                          vlr_trfdo					Float,
		                                      vlr_trfdo_bltrl			Float,
		                                      cnsctvo_prstncn_pqte		udtConsecutivo,
                                              inco_vgnca				Datetime,
                                              fn_vgnca					Datetime,
                                              cnsctvo_cdgo_frma_lqdcn	udtConsecutivo,
                                              cnsctvo_cdgo_nvl_atncn	udtConsecutivo,
                                              cnsctvo_srvco_lqdr		udtConsecutivo,
                                              cnsctvo_asccn_mdlo_actvdd	udtConsecutivo,
                                              cnsctvo_cdgo_tpo_mdlo		udtConsecutivo,
											  cnsctvo_cdgo_pln          udtConsecutivo,
	                                          Primary Key (cnsctvo_srvco_lqdr,
											               cnsctvo_cdgo_lsta_prco, 
											               cnsctvo_cdgo_dt_lsta_prco,
														   cnsctvo_cdgo_pln         
														   )
											 )

	Create 
	Table  #tbConceptosNoIncluidos(cnsctvo_cncpto_lqdr				udtConsecutivo,	-- Llave Primaria
                                   cnsctvo_srvco_lqdr				udtConsecutivo,	-- Llave Foranea
                                   cnsctvo_cdgo_cncpto_gsto			udtConsecutivo, -- Consecutivo del concepto de gasto a liquidar
                                   vlr_lqdcn_cncpto					Float         , -- Valor de la liquidación del concepto
                                   cnsctvo_cdgo_clse_hbtcn			udtConsecutivo,
                                   fctr_msma_va						Float         ,
                                   cnsctvo_cdgo_cncpts_dt_lsta_prco	udtConsecutivo,
                                   cntmpldo_pqte                    udtLogico     ,
                                   cnsctvo_cdgo_trfa                udtConsecutivo,
                                   cnsctvo_cdgo_nvl_atncn			udtConsecutivo,			
	                               Primary Key (cnsctvo_cncpto_lqdr)
								  )

	-- Paquetes que esten vigentes y pertenezcan a los Prestadores

	Create 
	Table  #tbPaquetes (cnsctvo_pqte			udtConsecutivo,					
		                cnsctvo_prstcn			udtConsecutivo,
		                nmro_agrpdr				udtConsecutivo,
		                cnsctvo_cps_pqte		udtConsecutivo,					
		                cnsctvo_cdgo_lsta_prco	udtConsecutivo,			
		                agrpdr_srvco_lqdr		udtConsecutivo,
		                cntdd_grps_pqte			Int     Default 0,
		                tpo_pqte				Char(2) Default 'OC',						
		                cntdd_grps_prsnts		Int		Default 0
					   )
    
	Create 
	Table  #tbPrestadorLista(cnsctvo_cdgo_lsta_prco    udtConsecutivo, 
			                 cnsctvo_cdgo_tpo_mdlo     udtConsecutivo,
			                 inco_vgnca                Datetime      ,
			                 fn_vgnca                  Datetime      ,
			                 cdgo_intrno               udtCodigoIPS  ,
			                 cnsctvo_cdgo_pln          udtConsecutivo,
							 cnsctvo_asccn_mdlo_actvdd udtConsecutivo
	                        )
	Create 
	Table  #tbPaquetesConConceptos(cnsctvo_pqte udtConsecutivo)

	Create 
	Table  #tbConceptosServiciosALiquidarSinPaquete(cnsctvo_srvco_lqdr udtConsecutivo)

	Create 
	Table  #tbServiciosALiquidarPaquete(cnsctvo_srvco_lqdr udtConsecutivo)

	Create 
	Table  #tbProcedimientosJerarquizados(jrrqa				 Int           ,
		                                  cnsctvo_cdgo_cps	 udtConsecutivo,
		                                  bltrl				 udtLogico     ,
		                                  msma_va			 udtLogico     ,
		                                  cntdd				 Int           ,
		                                  cnsctvo_lsta_prco	 udtConsecutivo,
		                                  qrrgco			 udtLogico     ,
		                                  cnsctvo_cdgo_trfa	 udtConsecutivo,
		                                  vlr_lqdcn			 Float         ,
		                                  cnsctvo_srvco_lqdr udtConsecutivo,
										  agrpdr_srvco_lqdr  udtConsecutivo
	                                     )
		

/*=================================================================================================
 * 2.  Población de las Tablas Temporales de Trabajo
 *=================================================================================================*/

    Insert 
	Into		#tbGruposALiquidar(agrpdr_srvco_lqdr, cdgo_intrno_prstdr,
	                               fcha_lqdcn       , cnsctvo_cdgo_pln
								  )
	Select		Distinct agrpdr_srvco_lqdr,	cdgo_intrno_prstdr,			
	                     fcha_lqdcn       , cnsctvo_cdgo_pln
	From		#tbServiciosALiquidarOrigen

	-- Select * from #tbGruposALiquidar	

	--Inserta los servicios a liquidar
	Insert 
	Into	#tbServiciosALiquidar(cnsctvo_srvco_lqdr, cnsctvo_cdgo_cps, cnsctvo_cdgo_tpo_atncn,
				                  bltrl             , msma_va         , cntdd                 ,
				                  rmpe_pqte         , espcldd         , vlr_lqdcn             ,
				                  agrpdr_srvco_lqdr , fcha_lqdcn      , cdgo_intrno_prstdr
							     )
	Select	cnsctvo_srvco_lqdr, cnsctvo_cdgo_cps, cnsctvo_cdgo_tpo_atncn,
			bltrl             , msma_va         , cntdd                 ,
			rmpe_pqte         , espcldd         , vlr_lqdcn             ,						
	        agrpdr_srvco_lqdr , fcha_lqdcn      , cdgo_intrno_prstdr
	From	#tbServiciosALiquidarOrigen

	-- Select * from #tbServiciosALiquidarOrigen
		
	Create NonClustered Index IX_Primario On #tbServiciosALiquidar(cnsctvo_cdgo_cps, cnsctvo_cdgo_lsta_prco, cnsctvo_cdgo_tpo_atncn)

	Insert 
	Into	#tbConceptosServiciosALiquidar(cnsctvo_cncpto_lqdr,	cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto,
				                           vlr_lqdcn_cncpto   , cnsctvo_cdgo_clse_hbtcn
										  )
	Select	cnsctvo_cncpto_lqdr, cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto,
		    vlr_lqdcn_cncpto   , cnsctvo_cdgo_clse_hbtcn
	From	#tbConceptosServiciosALiquidarOrigen	

	 --Select * from #tbConceptosServiciosALiquidar

	 -- Actualiza el nivel del prestador
	Update		#tbGruposALiquidar
	Set			cnsctvo_cdgo_nvl_prstdr = b.cnsctvo_cdgo_nvl
	From		#tbGruposALiquidar a
	Inner Join	bdSisalud.dbo.tbDireccionesPrestador b With(NoLock)
	On          b.cdgo_intrno = a.cdgo_intrno_prstdr

	Update		#tbServiciosALiquidar
	Set			cnsctvo_cdgo_nvl_atncn = c.cnsctvo_cdgo_nvl_prstdr
	From	    #tbServiciosALiquidar b		
	Inner Join	#tbGruposALiquidar c	
	On          c.agrpdr_srvco_lqdr = b.agrpdr_srvco_lqdr

	Update		#tbConceptosServiciosALiquidar
	Set			cnsctvo_cdgo_nvl_atncn = b.cnsctvo_cdgo_nvl_atncn
	From		#tbConceptosServiciosALiquidar a
	Inner Join	#tbServiciosALiquidar b	
	On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr

    --obtener la lista vigente para el modelo de prestacion(nueva version) 26/05/2027
	Insert 
	Into       #tbPrestadorLista(cnsctvo_cdgo_lsta_prco   , cnsctvo_cdgo_tpo_mdlo, inco_vgnca      ,
			                     fn_vgnca                 , cdgo_intrno          , cnsctvo_cdgo_pln,
								 cnsctvo_asccn_mdlo_actvdd
                                )
	Select     Distinct	a3.cnsctvo_cdgo_lsta_prco   , a3.cnsctvo_cdgo_tpo_mdlo, a3.inco_vgnca      ,
					    a3.fn_vgnca                 , a1.cdgo_intrno          , a1.cnsctvo_cdgo_pln,
						a1.cnsctvo_asccn_mdlo_actvdd
	From	   #tbServiciosALiquidar e
	Inner Join #tbGruposALiquidar f							
	On		   f.agrpdr_srvco_lqdr = e.agrpdr_srvco_lqdr
	Inner Join bdSisalud.dbo.tbAsociacionModeloActividad a1 With(NoLock)
	On         a1.cnsctvo_cdgo_pln = f.cnsctvo_cdgo_pln   And
	           a1.cdgo_intrno      = f.cdgo_intrno_prstdr                     
	Inner Join bdContratacion.dbo.tbLSListaPreciosxSucursalxPlan a2 With(NoLock)	
	On         a2.cnsctvo_asccn_mdlo_actvdd = a1.cnsctvo_asccn_mdlo_actvdd
	Inner Join bdContratacion.dbo.tbLSListaPrecios a3 With(NoLock)	
	On         a3.cnsctvo_cdgo_lsta_prco = a2.cnsctvo_cdgo_lsta_prco		
    Where	   e.fcha_lqdcn Between a1.inco_vgnca And a1.fn_vgnca --Quick 2013-00001-000016608
	And		   e.fcha_lqdcn Between a2.inco_vgnca And a2.fn_vgnca
	And		   e.fcha_lqdcn Between a3.inco_vgnca And a3.fn_vgnca

	--Actualizar en los servicios la lista del modelo
	Insert 
	Into        #tbLSDetListaPreciosxSucursalxPlan(cnsctvo_cdgo_dt_lsta_prco, cnsctvo_cdgo_lsta_prco   , cnsctvo_cdgo_tpo_atncn,
	                                               cnsctvo_prstncn          , cnsctvo_cdgo_tpo_cdfccn  , vlr_trfdo             ,
					                               vlr_trfdo_bltrl          , cnsctvo_prstncn_pqte     , inco_vgnca            , 
											       fn_vgnca                 , cnsctvo_cdgo_frma_lqdcn  , cnsctvo_cdgo_nvl_atncn,
												   cnsctvo_srvco_lqdr       , cnsctvo_asccn_mdlo_actvdd, cnsctvo_cdgo_tpo_mdlo  ,
												   cnsctvo_cdgo_pln
			 	                                  )
	Select		Distinct c.cnsctvo_cdgo_dt_lsta_prco, c.cnsctvo_cdgo_lsta_prco   , c.cnsctvo_cdgo_tpo_atncn,	
	            c.cnsctvo_prstncn                   , c.cnsctvo_cdgo_tpo_cdfccn  , c.vlr_trfdo             ,
				c.vlr_trfdo_bltrl                   , c.cnsctvo_prstncn_pqte     , c.inco_vgnca            , 
				c.fn_vgnca                          , c.cnsctvo_cdgo_frma_lqdcn  , c.cnsctvo_cdgo_nvl_atncn,
				e.cnsctvo_srvco_lqdr                , b.cnsctvo_asccn_mdlo_actvdd, b.cnsctvo_cdgo_tpo_mdlo ,
				b.cnsctvo_cdgo_pln
	From		#tbServiciosALiquidar e
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan c	With(NoLock)	
	On	        c.cnsctvo_prstncn = e.cnsctvo_cdgo_cps
	And		    (
	             c.cnsctvo_cdgo_tpo_atncn = e.cnsctvo_cdgo_tpo_atncn Or 
				 c.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn
				)
	Inner Join	#tbPrestadorLista b 
	On          b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco  And
	            b.cdgo_intrno            = e.cdgo_intrno_prstdr
	Where		e.fcha_lqdcn Between c.inco_vgnca And c.fn_vgnca


	-- Se actualiza la Lista Precio 
    Update		#tbServiciosALiquidar
	Set			cnsctvo_cdgo_lsta_prco = b.cnsctvo_cdgo_lsta_prco ,
				cnsctvo_cdgo_tpo_mdlo  = b.cnsctvo_cdgo_tpo_mdlo  ,
				frma_lqdcn			   = c.cnsctvo_cdgo_frma_lqdcn		
    From		#tbServiciosALiquidar e
	Inner Join	#tbLSDetListaPreciosxSucursalxPlan c	
	On	        c.cnsctvo_prstncn = e.cnsctvo_cdgo_cps 
	And         (
				 c.cnsctvo_cdgo_tpo_atncn = e.cnsctvo_cdgo_tpo_atncn Or 
				 c.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn
				)-- Ambito Especifico o Todos
	And         (
	             c.cnsctvo_cdgo_nvl_atncn = e.cnsctvo_cdgo_nvl_atncn Or 
	             c.cnsctvo_cdgo_nvl_atncn = @valor_cero 
				) --Prestacion Estancia/o Todos
	Inner Join	#tbPrestadorLista b	
	On          b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
	Where		e.fcha_lqdcn Between c.inco_vgnca And c.fn_vgnca


	-- Calcula que Prestaciones son Qx
	Update		#tbServiciosALiquidar
	Set			cnsctvo_cdgo_sccn = f.cnsctvo_cdgo_sccn
	From		#tbServiciosALiquidar a 
	Inner Join	bdSiSalud.dbo.tbCupsServicios b	With(NoLock)		
	On          b.cnsctvo_prstcn = a.cnsctvo_cdgo_cps 
	Inner Join	bdSiSalud.dbo.tbCategoria_vigencias c With(NoLock)
	On          c.cnsctvo_cdgo_ctgra = b.cnsctvo_cdgo_ctgra 
	Inner Join	bdSiSalud.dbo.tbSubgrupos_vigencias d With(NoLock)
	On          d.cnsctvo_cdgo_sbgrpo = c.cnsctvo_cdgo_sbgrpo 
	Inner Join	bdSiSalud.dbo.tbGrupos_vigencias e With(NoLock)		
	On          e.cnsctvo_cdgo_grpo = d.cnsctvo_cdgo_grpo 
	Inner Join	bdSiSalud.dbo.tbCapitulos_vigencias f With(NoLock)
	On          f.cnsctvo_cdgo_cptlo = e.cnsctvo_cdgo_cptlo
	Where		a.cnsctvo_cdgo_tpo_mdlo	= @cnsctvo_cdgo_tpo_mdlo1 -- Prestaciones
	And			a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca
	And			a.fcha_lqdcn Between c.inco_vgnca And c.fn_vgnca
	And			a.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca
	And			a.fcha_lqdcn Between e.inco_vgnca And e.fn_vgnca
	And			a.fcha_lqdcn Between f.inco_vgnca And f.fn_vgnca

-- Select * from #tbServiciosALiquidar

	Update		#tbServiciosALiquidar
	Set			qrrgco				= @valor_si
	Where		cnsctvo_cdgo_sccn	= 5

	-- Actualiza el Codigo de Paquete para aquellas prestaciones que desde la solicitud indican que son Paquetes y de entrada los conceptos que 
	-- hallan sido enviados se consideran como del paquete para que luego no trate de liquidarlos por conceptos, pues la prestacion en si es un
	-- paquete

	Update		#tbServiciosALiquidar
	Set			cnsctvo_pqte = cnsctvo_cdgo_cps
	From		#tbServiciosALiquidar 
	Where		frma_lqdcn = @frma_lqdcn4 -- Paquetes

	Update		#tbConceptosServiciosALiquidar
	Set			cntmpldo_pqte = @valor_si
	From 		#tbConceptosServiciosALiquidar a
	Inner Join	#tbServiciosALiquidar c	
	On          c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr	
	Where		c.frma_lqdcn = @frma_lqdcn4 -- Paquete


	-- Calcula Paquetes que esten vigentes y pertenezcan al Prestador 
	Insert 
	Into		#tbPaquetes(cnsctvo_pqte    , cnsctvo_prstcn        , nmro_agrpdr      ,
				            cnsctvo_cps_pqte, cnsctvo_cdgo_lsta_prco, agrpdr_srvco_lqdr
						   )
	Select		Distinct b.cnsctvo_pqte, b.cnsctvo_prstcn        , b.nmro_agrpdr      ,
				c.cnsctvo_det_trfa     , f.cnsctvo_cdgo_lsta_prco, a.agrpdr_srvco_lqdr
	From		#tbServiciosALiquidar a 
	Inner Join	bdContratacion.dbo.tbPaqueteDetalle b With(NoLock) 
	On          b.cnsctvo_prstcn = a.cnsctvo_cdgo_cps
	Inner Join	bdSisalud.dbo.tbDetTarifas c With(NoLock) 
	On          c.cnsctvo_rgstro = b.cnsctvo_pqte
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan d With(NoLock)  
	On          d.cnsctvo_prstncn = c.cnsctvo_det_trfa 
	And         (
				 d.cnsctvo_cdgo_tpo_atncn = a.cnsctvo_cdgo_tpo_atncn Or 
				 d.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn
	            )
	Inner Join	#tbPrestadorLista f	
	On          f.cnsctvo_cdgo_lsta_prco = d.cnsctvo_cdgo_lsta_prco
	Inner Join  #tbServiciosALiquidarOrigen	e
	On          e.cnsctvo_cdgo_pln   = f.cnsctvo_cdgo_pln   And
	            e.cdgo_intrno_prstdr = f.cdgo_intrno  
	Where		a.rmpe_pqte	  = @valor_no    -- Si el paquete debe mantenerse
	And			a.frma_lqdcn != @frma_lqdcn4 -- Paquetes
	And			a.fcha_lqdcn Between c.inco_vgnca And c.fn_vgnca
	And			a.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca


	-- Elimino aquellos paquetes que no contengan al menos uno de los conceptos solicitados
	Insert Into #tbPaquetesConConceptos(cnsctvo_pqte)
	Select		d.cnsctvo_pqte
	From		#tbConceptosServiciosALiquidar	a
	Inner Join	#tbServiciosALiquidar c	
	On	        c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Inner Join	#tbPaquetes e
	On	        e.cnsctvo_prstcn = c.cnsctvo_cdgo_cps
	Inner Join	bdContratacion.dbo.tbPaqueteIncluye d	
    On	        d.cnsctvo_pqte   = e.cnsctvo_pqte             And 
	            d.cnsctvo_inclye = a.cnsctvo_cdgo_cncpto_gsto
	Where		d.cnsctvo_cdgo_tpo_cdfccn	= @cnsctvo_cdgo_tpo_cdfccn11

	Delete		#tbPaquetes
	From		#tbPaquetes	a
	Left Join	#tbPaquetesConConceptos	b	
	On	        b.cnsctvo_pqte = a.cnsctvo_pqte
	Where		b.cnsctvo_pqte Is Null
		
/*================================================================================================
 *  3.  Liquida las Prestaciones que se Prestaran por Paquetes
 *================================================================================================*/

	-- Calculo el tipo de Paquete Opcional o Condicional

	-- Calcula cuantos grupos diferentes de prestaciones conforman un paquete (cada grupo indica que las 
	-- prestaciones son incluyentes entre si)

	Update		a
	Set			cntdd_grps_pqte	= b.cntdd_grps_pqte
	From		#tbPaquetes a
	Inner Join	(Select	  cnsctvo_pqte, Count (Distinct nmro_agrpdr) As cntdd_grps_pqte
				 From	  bdContratacion.dbo.tbPaqueteDetalle With(NoLock)
				 Group by cnsctvo_pqte
				)b 
	On          b.cnsctvo_pqte = a.cnsctvo_pqte
	
	-- Si un paquete tiene mas de un grupo es porque requiere que exista mas de una prestación para que sea valido,
	-- por esto incialmente supondremos que el paquete si bien es condicional, este estará incompleto.

	Update	#tbPaquetes
	Set		tpo_pqte = @cndcnl_incmplto -- Condicional Incompleto
	Where	cntdd_grps_pqte	> @valor_uno

	
	--Calculo cantidad de Grupo Presentes

	Update		#tbPaquetes
	Set			cntdd_grps_prsnts = b.cntdd_grps_prsnts
	From		#tbPaquetes a 
	Inner Join (Select	agrpdr_srvco_lqdr, cnsctvo_pqte, Count(Distinct nmro_agrpdr) As cntdd_grps_prsnts
				From	#tbPaquetes a
				Group by agrpdr_srvco_lqdr, cnsctvo_pqte
			    ) b	
	On	        b.agrpdr_srvco_lqdr = a.agrpdr_srvco_lqdr And 
	            b.cnsctvo_pqte      = a.cnsctvo_pqte
 	
	-- Si el numero de prestaciones encontradas corresponde al numero de prestaciones requeridas se puede decir
	-- que el paquete es condicional y esta completo

	Update	#tbPaquetes
	Set	    tpo_pqte = @cndcnl_cmplto -- Condicional Completo
	Where	cntdd_grps_pqte = cntdd_grps_prsnts
	
	-- Actualiza Consecutivo Paquete y Forma de Liquidacion de los Paquetes
	-- Se actualiza primero el paquete Opcional Completo y Luego el Condicional Completo pues tiene prioridad el Condicional Completo
	-- (que es el dato que al final quedaria)
	Update		#tbServiciosALiquidar
	Set			frma_lqdcn				= @frma_lqdcn4, -- Paquetes
				cnsctvo_pqte			= b.cnsctvo_cps_pqte,
				cnsctvo_rgstro_pqte		= b.cnsctvo_pqte,
				cnsctvo_cdgo_lsta_prco	= b.cnsctvo_cdgo_lsta_prco
	From		#tbServiciosALiquidar a 
	Inner Join	#tbPaquetes b 
	On          b.cnsctvo_prstcn = a.cnsctvo_cdgo_cps
	Where		b.tpo_pqte = @opcnl_cmplto -- OC-Opcional Completo


	Update		#tbServiciosALiquidar
	Set			frma_lqdcn				= @frma_lqdcn4, -- Paquetes
				cnsctvo_pqte			= b.cnsctvo_cps_pqte,
				cnsctvo_rgstro_pqte		= b.cnsctvo_pqte,
				cnsctvo_cdgo_lsta_prco	= b.cnsctvo_cdgo_lsta_prco
	From		#tbServiciosALiquidar a 
	Inner Join	#tbPaquetes b 
	On          b.cnsctvo_prstcn = a.cnsctvo_cdgo_cps
	Where		b.tpo_pqte = @cndcnl_cmplto -- CC-Condicional Completo


	-- Como el paquete tiene una jerarquia mayor se le asignara como valor inicial de jerarquia al grupo el valor 1, es decir
	-- el resto de procedimientos tendran un jerarquia menor a este, si es que hay mas.

	Update	   #tbGruposALiquidar
	Set		   vlr_incl_jrrqa = @valor_uno
	From	   #tbGruposALiquidar a
	Inner Join #tbServiciosALiquidar b 
    On         b.agrpdr_srvco_lqdr = a.agrpdr_srvco_lqdr 
	Where	   b.frma_lqdcn = @frma_lqdcn4

	-- Liquido las prestaciones que pertenecen a un Paquete
    Update	   #tbServiciosALiquidar
	Set        vlr_lqdcn				 = b.vlr_trfdo,
			   lddo						 = @valor_si,
			   cnsctvo_cdgo_dt_lsta_prco = b.cnsctvo_cdgo_dt_lsta_prco,
			   jrrqa					 = @valor_uno,  -- Los paquetes siempre tiene la mayor jerarquia
			   rsltdo_lqdcn				 = @valor_uno
	From	   #tbServiciosALiquidar a
	Inner Join bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)  
	On         b.cnsctvo_prstncn        = a.cnsctvo_pqte
	And        b.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco
    And        (
		        b.cnsctvo_cdgo_tpo_atncn = a.cnsctvo_cdgo_tpo_atncn Or
				b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn 
			   )
	Where	   a.frma_lqdcn = @frma_lqdcn4 --Paquete
	And        a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca

	
	-- Inserto el concepto que me agrupa los conceptos para el paquete
	Select	   @MaximoConsecutivoConcepto = Max(cnsctvo_cncpto_lqdr) From  #tbConceptosServiciosALiquidar
	       	 
	Insert		
	Into	   #tbConceptosServiciosALiquidar(cnsctvo_cncpto_lqdr    ,		
				                              cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto, vlr_lqdcn_cncpto                ,			
				                              cnsctvo_cdgo_clse_hbtcn, fctr_msma_va            , cnsctvo_cdgo_cncpts_dt_lsta_prco,
				                              cntmpldo_pqte          , cnsctvo_cdgo_trfa       , cnsctvo_cdgo_nvl_atncn          ,
				                              rsltdo_lqdcn           , cnsctvo_prstcn_pqte
											 )
	Select	   Distinct Row_Number() Over(Order By c.cnsctvo_srvco_lqdr) + @MaximoConsecutivoConcepto,	
			   c.cnsctvo_srvco_lqdr, d.cnsctvo_cdgo_cncpto_gsto, d.vlr_trfdo                       ,								
			   @valor_cero         , @valor_cien               , d.cnsctvo_cdgo_cncpts_dt_lsta_prco,			
			   @valor_si           , @valor_cero               , d.cnsctvo_cdgo_nvl_atncn          ,
			   @valor_uno          , c.cnsctvo_pqte
	From	   #tbServiciosALiquidar c
	Inner Join bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)  
	On         b.cnsctvo_prstncn        = c.cnsctvo_pqte
	And        b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
    And        (
		        b.cnsctvo_cdgo_tpo_atncn = c.cnsctvo_cdgo_tpo_atncn Or
				b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn 
			   )	
	Inner Join bdContratacion.dbo.tbLSConceptosDetListaPreciosxSucursalxPlan d	With(NoLock)
	On         d.cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco
	Where	   c.frma_lqdcn = @frma_lqdcn4 --Paquete
	And   	   c.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca
	And   	   c.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca	

	-- Actualizo si los conceptos solicitados en la prestación estan contemplados en el paquete	
	Update		#tbConceptosServiciosALiquidar
	Set			cntmpldo_pqte = @valor_si
	From 		#tbConceptosServiciosALiquidar a 
	Inner Join	#tbServiciosALiquidar c	
	On	        c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr	
	Inner Join	bdContratacion.dbo.tbPaqueteIncluye d With(NoLock)	
	On	        d.cnsctvo_pqte   = c.cnsctvo_rgstro_pqte      And 
	            d.cnsctvo_inclye = a.cnsctvo_cdgo_cncpto_gsto
	Where		c.frma_lqdcn			  = @frma_lqdcn4 --Paquete
	And         d.cnsctvo_cdgo_tpo_cdfccn = @cnsctvo_cdgo_tpo_cdfccn11 
	
	-- Inserto las prestaciones que no han sido considerados totalmente en el paquete con sus respectivos conceptos
	Insert 
	Into    #tbConceptosServiciosALiquidarSinPaquete(cnsctvo_srvco_lqdr)
	Select	Distinct cnsctvo_srvco_lqdr
	From	#tbConceptosServiciosALiquidar
	Where   cntmpldo_pqte = @valor_no

		
	-- Inserto las prestaciones que no han sido considerados totalmente en el paquete con sus respectivos conceptos	
	Select	@MaximoConsecutivoPrestacion = Max(cnsctvo_srvco_lqdr) From  #tbServiciosALiquidar	
	
	Insert	
	Into 	   #tbServiciosALiquidar(cnsctvo_srvco_lqdr   , cnsctvo_cdgo_cps      , cnsctvo_cdgo_tpo_atncn   ,
			                         bltrl                , msma_va               , cntdd                    ,
			                         rmpe_pqte            , espcldd               , vlr_lqdcn                ,
			                         agrpdr_srvco_lqdr    , fcha_lqdcn            , cnsctvo_cdgo_lsta_prco   ,	
		                             qrrgco               , cnsctvo_cdgo_sccn     , frma_lqdcn               ,
			                         cnsctvo_pqte         , msma_espcldd          , cnsctvo_cdgo_dt_lsta_prco,								
			                         cnsctvo_cdgo_tpo_mdlo, jrrqa                 , lddo                     ,
			                         cnsctvo_rgstro_pqte  , cnsctvo_cdgo_nvl_atncn
									)
	Select	   a.cnsctvo_srvco_lqdr + @MaximoConsecutivoPrestacion,	a.cnsctvo_cdgo_cps    , a.cnsctvo_cdgo_tpo_atncn   ,
			   a.bltrl                                            , a.msma_va             , a.cntdd                    ,
			   a.rmpe_pqte                                        , a.espcldd             , a.vlr_lqdcn                ,
			   a.agrpdr_srvco_lqdr                                , a.fcha_lqdcn          , a.cnsctvo_cdgo_lsta_prco   ,		
			   a.qrrgco                                           , a.cnsctvo_cdgo_sccn   , @frma_lqdcn2               ,		
			   @valor_cero                                        , a.msma_espcldd        , a.cnsctvo_cdgo_dt_lsta_prco,	
			   a.cnsctvo_cdgo_tpo_mdlo                            , a.jrrqa               , @valor_no                  ,
			   @valor_cero                                        , cnsctvo_cdgo_nvl_atncn
	From	   #tbServiciosALiquidar a
	Inner Join #tbConceptosServiciosALiquidarSinPaquete	b	
	On	       b.cnsctvo_srvco_lqdr	= a.cnsctvo_srvco_lqdr
	Where	   a.frma_lqdcn = @frma_lqdcn4 --Paquete
	
	Insert 
	Into	#tbServiciosALiquidarPaquete (cnsctvo_srvco_lqdr)
	Select  cnsctvo_srvco_lqdr 
	From	#tbServiciosALiquidar
	Where	frma_lqdcn = @frma_lqdcn4

		
	-- Se recalcula el ultimo concepto pues se ingresaron los conceptos agrupadores de los paquetes
	Select	@MaximoConsecutivoConcepto = Max(cnsctvo_cncpto_lqdr) From #tbConceptosServiciosALiquidar
	
	Insert	
	Into	   #tbConceptosServiciosALiquidar(cnsctvo_cncpto_lqdr             , cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto,
	                                          vlr_lqdcn_cncpto                , cnsctvo_cdgo_clse_hbtcn, fctr_msma_va            ,		
		 	                                  cnsctvo_cdgo_cncpts_dt_lsta_prco, cntmpldo_pqte          , cnsctvo_cdgo_trfa       , 
											  cnsctvo_cdgo_nvl_atncn          , cnsctvo_prstcn_pqte
											 )
	Select	   (a.cnsctvo_cncpto_lqdr + @MaximoConsecutivoConcepto), (a.cnsctvo_srvco_lqdr + @MaximoConsecutivoPrestacion), a.cnsctvo_cdgo_cncpto_gsto,	
			   a.vlr_lqdcn_cncpto                                  , a.cnsctvo_cdgo_clse_hbtcn                            , a.fctr_msma_va            ,
			   a.cnsctvo_cdgo_cncpts_dt_lsta_prco                  , a.cntmpldo_pqte                                      , a.cnsctvo_cdgo_trfa       ,
			   a.cnsctvo_cdgo_nvl_atncn                            , a.cnsctvo_prstcn_pqte
	From	   #tbConceptosServiciosALiquidar a
	Inner Join #tbServiciosALiquidarPaquete b 
	On	       b.cnsctvo_srvco_lqdr	= a.cnsctvo_srvco_lqdr
	Where	   a.cntmpldo_pqte      = @valor_no

/*=============================================================================================
 *   4.  Liquida las Prestaciones Qx que SI se liquidan por Modelo de Conceptos
 *============================================================================================*/

	-- Verifico si existen Qx a liquidar por Modelo de Conceptos
	If Exists (Select 1 From #tbServiciosALiquidar Where qrrgco = @valor_si And frma_lqdcn = @frma_lqdcn2)
	 Begin

		-- Actualizo el detalle de la lista de precios	
		Update		#tbServiciosALiquidar
		Set			cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco,
					vlr_lqdcn					= b.vlr_trfdo,
					lddo						= @valor_si,
					rsltdo_lqdcn				= @valor_uno
		From		#tbServiciosALiquidar a 
		Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)	
		On	        b.cnsctvo_prstncn        = a.cnsctvo_cdgo_cps 
		And         b.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco
		And         (
		             b.cnsctvo_cdgo_tpo_atncn = a.cnsctvo_cdgo_tpo_atncn Or 
		             b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn 
					)
		And         (
		             b.cnsctvo_cdgo_nvl_atncn = a.cnsctvo_cdgo_nvl_atncn Or 
		             b.cnsctvo_cdgo_nvl_atncn = @valor_cero
				    )--Prestacion Estancia/o Todos --Ambito Especifico o Todos
		Where		a.qrrgco	 = @valor_si
		And			a.frma_lqdcn = @frma_lqdcn2 -- Modelo de Conceptos
		And         a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca


		-- Realiza la primera liquidación para Qx que se liquidan segun un modelo de conceptos
		Update		#tbConceptosServiciosALiquidar
		Set			vlr_lqdcn_cncpto					= d.vlr_trfdo,
					cnsctvo_cdgo_cncpts_dt_lsta_prco	= d.cnsctvo_cdgo_cncpts_dt_lsta_prco,
					cnsctvo_cdgo_trfa					= d.cnsctvo_cdgo_trfa,
					rsltdo_lqdcn						= @valor_uno							
	    From		#tbConceptosServiciosALiquidar a
		Inner Join	#tbServiciosALiquidar c	
		On          c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
		Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b	With(NoLock)
		On	        b.cnsctvo_prstncn        = c.cnsctvo_cdgo_cps
		And         b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
		And         (
		             b.cnsctvo_cdgo_tpo_atncn = c.cnsctvo_cdgo_tpo_atncn Or 
		             b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn
					)
		And         (
		             b.cnsctvo_cdgo_nvl_atncn = c.cnsctvo_cdgo_nvl_atncn Or 
					 b.cnsctvo_cdgo_nvl_atncn = @valor_cero
					) --Prestacion Estancia/o Todos -- Ambito Especifico o Todos
		Inner Join	bdContratacion.dbo.tbLSConceptosDetListaPreciosxSucursalxPlan d	With(NoLock)
		On	        d.cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco
		And         d.cnsctvo_cdgo_cncpto_gsto  = a.cnsctvo_cdgo_cncpto_gsto
		And	        d.cnsctvo_cdgo_clse_hbtcn   = a.cnsctvo_cdgo_clse_hbtcn
		And         (
		             d.cnsctvo_cdgo_nvl_atncn = a.cnsctvo_cdgo_nvl_atncn Or 
		             d.cnsctvo_cdgo_nvl_atncn = @valor_cero 
					) --Prestacion Estancia/o Todos																					
		Where		c.bltrl	     = @valor_no
		And			c.qrrgco	 = @valor_si
		And			c.frma_lqdcn = @frma_lqdcn2 -- Modelo de Conceptos
		And   		c.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca
		And   		c.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca -- Quick 2012-029-007693
 
		-- Primera liquidación para procedimientos bilaterales
		Update		#tbConceptosServiciosALiquidar
		Set			vlr_lqdcn_cncpto					= d.vlr_trfdo_bltrl,
					cnsctvo_cdgo_cncpts_dt_lsta_prco	= d.cnsctvo_cdgo_cncpts_dt_lsta_prco,
					cnsctvo_cdgo_trfa					= d.cnsctvo_cdgo_trfa,
					rsltdo_lqdcn						= @valor_uno										
		From		#tbConceptosServiciosALiquidar a
		Inner Join	#tbServiciosALiquidar		   c					
		On          c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr	
		Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b	With(NoLock) 
		On	        b.cnsctvo_prstncn        = c.cnsctvo_cdgo_cps
		And         b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
		And         (
		             b.cnsctvo_cdgo_tpo_atncn = c.cnsctvo_cdgo_tpo_atncn Or
		             b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn
					)
		And         (
		             b.cnsctvo_cdgo_nvl_atncn = c.cnsctvo_cdgo_nvl_atncn Or 
		             b.cnsctvo_cdgo_nvl_atncn = @valor_cero 
					)-- Ambito Especifico o Todos
	    Inner Join	bdContratacion.dbo.tbLSConceptosDetListaPreciosxSucursalxPlan d	With(NoLock)
		On	        d.cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco And 
		            d.cnsctvo_cdgo_cncpto_gsto  = a.cnsctvo_cdgo_cncpto_gsto  And 
					d.cnsctvo_cdgo_clse_hbtcn   = a.cnsctvo_cdgo_clse_hbtcn
		Where		c.bltrl		 = @valor_si
		And			c.qrrgco	 = @valor_si
		And			c.frma_lqdcn = @frma_lqdcn2 --Modelo de Conceptos
		And   		c.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca
		AND			c.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca -- Quick 2012-029-007693
	
		-- La jerarquia es determinada por el valor de la liquidación de cada prestación Qx, esto para cada uno de los agrupadores de prestaciones
		Insert 
		Into   #tbProcedimientosJerarquizados(jrrqa,
		                                      cnsctvo_cdgo_cps , bltrl            , msma_va           ,
											  cntdd            , cnsctvo_lsta_prco, qrrgco            ,
											  cnsctvo_cdgo_trfa, vlr_lqdcn        , cnsctvo_srvco_lqdr,
											  agrpdr_srvco_lqdr
		                                     )
		Select Row_Number() Over(Partition By agrpdr_srvco_lqdr Order By vlr_lqdcn Desc),	
			   cnsctvo_cdgo_cps  , bltrl                 , msma_va           , 
			   cntdd             , cnsctvo_cdgo_lsta_prco, qrrgco            ,		
			   @valor_uno        , vlr_lqdcn             , cnsctvo_srvco_lqdr,
			   agrpdr_srvco_lqdr
		From   #tbServiciosALiquidar
		Where  frma_lqdcn = @frma_lqdcn2 --Modelo de Conceptos
		And	   qrrgco	  = @valor_si
		
		
		Update		#tbServiciosALiquidar
		Set			jrrqa = b.jrrqa + c.vlr_incl_jrrqa
		From		#tbServiciosALiquidar a 
		Inner Join	#tbGruposALiquidar c   
		On	        c.agrpdr_srvco_lqdr = a.agrpdr_srvco_lqdr
		Inner Join	#tbProcedimientosJerarquizados b	
		On	        b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr And 
		            b.agrpdr_srvco_lqdr  = a.agrpdr_srvco_lqdr

		
		-- Recalculo el valor de cada concepto según el factor de la jerarquia
		Update		#tbConceptosServiciosALiquidar
		Set			fctr_msma_va = (c.prcntje / @valor_cien)
		From		#tbConceptosServiciosALiquidar a 
		Inner Join	#tbServiciosALiquidar b						
		On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
		Inner Join	bdSiSalud.dbo.tbPorcentajesLiquidacion c With(NoLock)
		On          c.cnsctvo_cdgo_trfa        = a.cnsctvo_cdgo_trfa        And 
		            c.cnsctvo_cdgo_cncpto_gsto = a.cnsctvo_cdgo_cncpto_gsto	And
		            c.ordn			           =  b.jrrqa             
		Where		c.cnsctvo_cdgo_tpo_prcntje_lqdcn = @valor_uno -- Porcentaje Misma Via	
		And			b.msma_va	                     = @valor_si
		And         c.inco_vgnca                    <= b.fcha_lqdcn
		And         c.fn_vgnca	                    >= b.fcha_lqdcn	
		

		Update		#tbConceptosServiciosALiquidar
		Set			fctr_msma_va = (c.prcntje / @valor_cien)
		From		#tbConceptosServiciosALiquidar a 
		Inner Join	#tbServiciosALiquidar b						
		On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
		Inner Join	bdSiSalud.dbo.tbPorcentajesLiquidacion c	
		On          c.cnsctvo_cdgo_trfa        = a.cnsctvo_cdgo_trfa   And 
		            c.ordn					   = b.jrrqa			   And
					c.cnsctvo_cdgo_cncpto_gsto = a.cnsctvo_cdgo_cncpto_gsto 
		Where		c.cnsctvo_cdgo_tpo_prcntje_lqdcn = @tpo_prcntje_lqdcn -- Porcentaje Diferente Via	
		And         b.msma_va						 = @valor_no
		And         c.inco_vgnca				    <= b.fcha_lqdcn	
		And         c.fn_vgnca					    >= b.fcha_lqdcn  

		Update		#tbConceptosServiciosALiquidar
		Set			vlr_lqdcn_cncpto = (vlr_lqdcn_cncpto * fctr_msma_va)
		From		#tbConceptosServiciosALiquidar

		Update		#tbServiciosALiquidar
		Set			vlr_lqdcn	 = b.vlr_lqdcn_cncpto_ttl,
					lddo		 = @valor_si             ,
					rsltdo_lqdcn = @valor_uno
		From		#tbServiciosALiquidar a
		Inner Join	(Select	Sum(vlr_lqdcn_cncpto) As vlr_lqdcn_cncpto_ttl, cnsctvo_srvco_lqdr
					 From		#tbConceptosServiciosALiquidar 
					 Group by	cnsctvo_srvco_lqdr
					)b 
		On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
		Where		a.qrrgco			   = @valor_si
		And			a.frma_lqdcn		   = @frma_lqdcn2 -- Modelo de Conceptos
		And			b.vlr_lqdcn_cncpto_ttl > @valor_cero

	 End	-- Fin Si encuentra Qx a Liquidar por Conceptos

	 
/*====================================================================================================================
 * 5.  Liquida las Prestaciones NO Qx, Medicamentos y Otros Servicios que NO se liquidan por Modelo de Conceptos
 *====================================================================================================================*/
   	Update		#tbServiciosALiquidar
	Set			vlr_lqdcn					= (b.vlr_trfdo * a.cntdd),
				lddo						= @valor_si,
				cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco,
				rsltdo_lqdcn				= @valor_uno
	From		#tbServiciosALiquidar a 
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)	
	On	        b.cnsctvo_prstncn        = a.cnsctvo_cdgo_cps       And 
	            b.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco And
				b.cnsctvo_cdgo_tpo_atncn = a.cnsctvo_cdgo_tpo_atncn	
	Where		a.qrrgco				  = @valor_no  
	And			a.frma_lqdcn			 != @frma_lqdcn2 -- Modelo de Conceptos
	And			a.cnsctvo_cdgo_lsta_prco != @valor_cero
	And			a.lddo					  = @valor_no
	And         b.cnsctvo_cdgo_tpo_atncn != @cnsctvo_cdgo_tpo_atncn
	And         a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca

	-- Se actualiza cuando el tipo de atencion aplica para ambos
	Update		#tbServiciosALiquidar
	Set			vlr_lqdcn				  = (b.vlr_trfdo * a.cntdd)    ,
				lddo					  = @valor_si                  ,
				cnsctvo_cdgo_dt_lsta_prco = b.cnsctvo_cdgo_dt_lsta_prco,
				rsltdo_lqdcn			  = @valor_uno
	From		#tbServiciosALiquidar a
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)	
	On          b.cnsctvo_prstncn         = a.cnsctvo_cdgo_cps  And 
	            b.cnsctvo_cdgo_lsta_prco  = a.cnsctvo_cdgo_lsta_prco -- Ambito Especifico o Todos
	Where		a.qrrgco			      = @valor_no
	And			a.frma_lqdcn		     != @frma_lqdcn2 -- Modelo de Conceptos
	And			a.cnsctvo_cdgo_lsta_prco != @valor_cero
	And			a.lddo				      = @valor_no
	And         b.cnsctvo_cdgo_tpo_atncn  = @cnsctvo_cdgo_tpo_atncn
	And			a.vlr_lqdcn               = @valor_cero
	And         a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca

/*====================================================================================================================
 * 6.  Liquida las Prestaciones Qx que NO se liquidan por Modelo de Conceptos 
 *====================================================================================================================*/
 	Update		#tbServiciosALiquidar
	Set			vlr_lqdcn					= (b.vlr_trfdo * a.cntdd),
				lddo						= @valor_si,
				cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco,
				rsltdo_lqdcn				= @valor_uno
	From		#tbServiciosALiquidar a 
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b With(NoLock)	
	On	        b.cnsctvo_prstncn        = a.cnsctvo_cdgo_cps      
	And         b.cnsctvo_cdgo_lsta_prco = a.cnsctvo_cdgo_lsta_prco     
	And         (
	             b.cnsctvo_cdgo_tpo_atncn = a.cnsctvo_cdgo_tpo_atncn Or 
				 b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn 
				)				
    Where		a.qrrgco				  = @valor_si 
	And			a.frma_lqdcn			 != @frma_lqdcn2 -- Modelo de Conceptos
	And			a.cnsctvo_cdgo_lsta_prco != @valor_cero
	And			a.lddo					  = @valor_no
	And         a.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca
	
	-- La siguiente instrucción se incluye para actualizar valores a conceptos de NO QX, esto se hace para mantener compatibilidad con Sipres, 
	-- ya que este asume que los NO Qx tambien tienen Conceptos

	Update		#tbConceptosServiciosALiquidar
	Set			vlr_lqdcn_cncpto	 = c.vlr_lqdcn,
				rsltdo_lqdcn		 = @valor_uno				
	From		#tbConceptosServiciosALiquidar a 
	Inner Join  #tbServiciosALiquidar c			
	On          c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Inner Join (Select	 Max(cnsctvo_cncpto_lqdr) As cnsctvo_srvco_lqdr_mxmo, cnsctvo_srvco_lqdr 
				From	 #tbConceptosServiciosALiquidar
				Group by cnsctvo_srvco_lqdr
			    ) b	
	On          b.cnsctvo_srvco_lqdr      = a.cnsctvo_srvco_lqdr  And 
	            b.cnsctvo_srvco_lqdr_mxmo = a.cnsctvo_cncpto_lqdr
	Where		c.qrrgco			      = @valor_no
	And			c.frma_lqdcn		     != @frma_lqdcn2 -- Modelo de Conceptos
	And			c.frma_lqdcn		     != @frma_lqdcn4 -- Paquetes
	And			c.cnsctvo_cdgo_lsta_prco != @valor_cero	-- Si no tiene lista de precios no se puede liquidar qvisionclr 2016/12/30	

/*==================================================================================================================
 * 7.  Liquida los Conceptos de Prestaciones Qx pero que NO son Contratadas por Conceptos
 *==================================================================================================================*/

	Update		#tbConceptosServiciosALiquidar
	Set			vlr_lqdcn_cncpto	 = c.vlr_lqdcn,
				rsltdo_lqdcn		 = @valor_uno
	From		#tbConceptosServiciosALiquidar a 
	Inner Join  #tbServiciosALiquidar c			
	On          c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Inner Join (Select	 Max(cnsctvo_cncpto_lqdr) As cnsctvo_srvco_lqdr_mxmo, cnsctvo_srvco_lqdr 
				From	 #tbConceptosServiciosALiquidar
				Group by cnsctvo_srvco_lqdr
			    ) b	
   On           b.cnsctvo_srvco_lqdr      = a.cnsctvo_srvco_lqdr  And 
                b.cnsctvo_srvco_lqdr_mxmo = a.cnsctvo_cncpto_lqdr
	Where		c.qrrgco	              = @valor_si
	And			c.frma_lqdcn             != @frma_lqdcn2 -- Modelo de Conceptos
	And			c.frma_lqdcn             != @frma_lqdcn4 -- Paquetes
	And			c.cnsctvo_cdgo_lsta_prco != @valor_cero	 -- Si no tiene lista de precios no se puede liquidar qvisionclr 2016/12/30
		
/*==================================================================================================================
 * 8.  Liquida las Prestaciones NO Qx, Medicamentos y Otros Servicios que SI se liquidan por Modelo de Conceptos
 *==================================================================================================================*/

	Update		#tbConceptosServiciosALiquidar
	Set			vlr_lqdcn_cncpto					= d.vlr_trfdo,
				cnsctvo_cdgo_cncpts_dt_lsta_prco	= d.cnsctvo_cdgo_cncpts_dt_lsta_prco,
				rsltdo_lqdcn						= @valor_uno							
	From		#tbConceptosServiciosALiquidar a
	Inner Join	#tbServiciosALiquidar c	
	On	        c.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Inner Join	bdContratacion.dbo.tbLSDetListaPreciosxSucursalxPlan b	With(NoLock)
	On	        b.cnsctvo_prstncn        = c.cnsctvo_cdgo_cps 
	And         b.cnsctvo_cdgo_lsta_prco = c.cnsctvo_cdgo_lsta_prco
	And	        (
	             b.cnsctvo_cdgo_tpo_atncn = c.cnsctvo_cdgo_tpo_atncn  Or  
	             b.cnsctvo_cdgo_tpo_atncn = @cnsctvo_cdgo_tpo_atncn 
				)
	And	        (
	             b.cnsctvo_cdgo_nvl_atncn = c.cnsctvo_cdgo_nvl_atncn Or 
				 b.cnsctvo_cdgo_nvl_atncn = @valor_cero 
			    )																					-- Ambito Especifico o Todos
	Inner Join	bdContratacion.dbo.tbLSConceptosDetListaPreciosxSucursalxPlan d	With(NoLock)
	On	        d.cnsctvo_cdgo_dt_lsta_prco	= b.cnsctvo_cdgo_dt_lsta_prco And 
	            d.cnsctvo_cdgo_cncpto_gsto  = a.cnsctvo_cdgo_cncpto_gsto  And	
				d.cnsctvo_cdgo_clse_hbtcn   = a.cnsctvo_cdgo_clse_hbtcn
 	Where		c.qrrgco			      = @valor_no
	And			c.frma_lqdcn		      = @frma_lqdcn2 -- Modelo de Conceptos
	And			c.cnsctvo_cdgo_lsta_prco != @valor_cero
	And			c.lddo				      = @valor_no
	And         c.fcha_lqdcn Between b.inco_vgnca And b.fn_vgnca
	And         c.fcha_lqdcn Between d.inco_vgnca And d.fn_vgnca

/*==================================================================================================================
 * 9.  Liquida los Conceptos que NO estaban Incluidos en los Paquetes
 *==================================================================================================================*/
 
	Insert 
	Into		#tbConceptosNoIncluidos(cnsctvo_cncpto_lqdr             , cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto,
				                        vlr_lqdcn_cncpto                , cnsctvo_cdgo_clse_hbtcn, fctr_msma_va            ,
				                        cnsctvo_cdgo_cncpts_dt_lsta_prco, cntmpldo_pqte          , cnsctvo_cdgo_trfa       ,
				                        cnsctvo_cdgo_nvl_atncn
									   )
	Select		a.cnsctvo_cncpto_lqdr             , a.cnsctvo_srvco_lqdr     , a.cnsctvo_cdgo_cncpto_gsto,
				a.vlr_lqdcn_cncpto                , a.cnsctvo_cdgo_clse_hbtcn, a.fctr_msma_va            ,
				a.cnsctvo_cdgo_cncpts_dt_lsta_prco,	a.cntmpldo_pqte          , a.cnsctvo_cdgo_trfa       ,
				a.cnsctvo_cdgo_nvl_atncn
	From		#tbConceptosServiciosALiquidar a
	Inner Join	#tbServiciosALiquidar b	
	On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Where		a.cnsctvo_cncpto_lqdr > @MaximoConsecutivoConcepto
	And			a.cntmpldo_pqte		  = @valor_no


	Update		#tbConceptosServiciosALiquidar
	Set			vlr_lqdcn_cncpto                 = b.vlr_lqdcn_cncpto,
				cnsctvo_cdgo_cncpts_dt_lsta_prco = b.cnsctvo_cdgo_cncpts_dt_lsta_prco,
				rsltdo_lqdcn					 = @valor_uno
	From		#tbConceptosServiciosALiquidar a
	Inner Join 	#tbConceptosNoIncluidos b		
	On          (b.cnsctvo_cncpto_lqdr - @MaximoConsecutivoConcepto) = a.cnsctvo_cncpto_lqdr
	
	-- Actualiza el valor de la prestacion
	Update		#tbServiciosALiquidar
	Set			vlr_lqdcn				=  b.vlr_lqdcn_cncpto_ttl,
				lddo					= @valor_si,
				rsltdo_lqdcn			= @valor_uno
	From		#tbServiciosALiquidar a 
	Inner Join	(Select	  Sum(vlr_lqdcn_cncpto) As vlr_lqdcn_cncpto_ttl, cnsctvo_srvco_lqdr
				 From	  #tbConceptosServiciosALiquidar 
				 Group by cnsctvo_srvco_lqdr
				) b 
	On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr
	Where		a.qrrgco			   = @valor_no
	And			a.frma_lqdcn		   = @frma_lqdcn2 -- Modelo de Conceptos
	And			b.vlr_lqdcn_cncpto_ttl > @valor_cero
	And			a.lddo				   = @valor_no

/*==================================================================================================================
 * 10. Actualización de los cursores de Origen
 *==================================================================================================================*/

	Update		#tbServiciosALiquidarOrigen
	Set			vlr_lqdcn				  = a.vlr_lqdcn,
				cnsctvo_pqte			  = a.cnsctvo_pqte,		
				rsltdo_lqdcn			  = a.rsltdo_lqdcn,
				cnsctvo_cdgo_lsta_prco	  = a.cnsctvo_cdgo_lsta_prco,
				cnsctvo_cdgo_dt_lsta_prco = a.cnsctvo_cdgo_dt_lsta_prco
	From		#tbServiciosALiquidar a
	Inner Join	#tbServiciosALiquidarOrigen b 
	On          b.cnsctvo_srvco_lqdr = a.cnsctvo_srvco_lqdr

	Select		@MaximoConsecutivoPrestacion = Max(cnsctvo_srvco_lqdr) From  #tbServiciosALiquidarOrigen	

	Insert 
	Into		#tbServiciosALiquidarOrigen(cnsctvo_srvco_lqdr,	cnsctvo_cdgo_cps      ,	cnsctvo_cdgo_tpo_atncn   ,
				                            bltrl             ,	msma_va               ,	cntdd                    , 
				                            rmpe_pqte         ,	espcldd               ,	vlr_lqdcn                ,
				                            agrpdr_srvco_lqdr ,	cdgo_intrno_prstdr    ,	fcha_lqdcn               ,
				                            cnsctvo_cdgo_pln  ,	cnsctvo_cdgo_lsta_prco, cnsctvo_cdgo_dt_lsta_prco,		
				                            cnsctvo_pqte      ,	rsltdo_lqdcn
										   )
	Select		a.cnsctvo_srvco_lqdr, a.cnsctvo_cdgo_cps      ,	a.cnsctvo_cdgo_tpo_atncn   ,
				a.bltrl             , a.msma_va               ,	a.cntdd                    , 
				a.rmpe_pqte         , a.espcldd               ,	a.vlr_lqdcn                ,
				a.agrpdr_srvco_lqdr , b.cdgo_intrno_prstdr    ,	b.fcha_lqdcn               ,
				b.cnsctvo_cdgo_pln  , a.cnsctvo_cdgo_lsta_prco,	a.cnsctvo_cdgo_dt_lsta_prco,	
				a.cnsctvo_pqte      , a.rsltdo_lqdcn				
	From		#tbServiciosALiquidar a
	Inner Join	#tbGruposALiquidar b		
	On          b.agrpdr_srvco_lqdr = a.agrpdr_srvco_lqdr
	Where		a.cnsctvo_srvco_lqdr > @MaximoConsecutivoPrestacion
	
	Update		#tbConceptosServiciosALiquidarOrigen
	Set			vlr_lqdcn_cncpto					= a.vlr_lqdcn_cncpto,
				rsltdo_lqdcn						= a.rsltdo_lqdcn,
				cnsctvo_cdgo_cncpts_dt_lsta_prco	= a.cnsctvo_cdgo_cncpts_dt_lsta_prco
	From		#tbConceptosServiciosALiquidar a
	Inner Join	#tbConceptosServiciosALiquidarOrigen b On a.cnsctvo_cncpto_lqdr = b.cnsctvo_cncpto_lqdr

	Select		@MaximoConsecutivoConcepto = Max(cnsctvo_cncpto_lqdr) From  #tbConceptosServiciosALiquidarOrigen	

	Insert
	Into		#tbConceptosServiciosALiquidarOrigen(cnsctvo_cncpto_lqdr, cnsctvo_srvco_lqdr     , cnsctvo_cdgo_cncpto_gsto        ,
				                                     vlr_lqdcn_cncpto   , cnsctvo_cdgo_clse_hbtcn, cnsctvo_cdgo_cncpts_dt_lsta_prco,
				                                     rsltdo_lqdcn       , cnsctvo_prstcn_pqte
													)
	Select		a.cnsctvo_cncpto_lqdr, a.cnsctvo_srvco_lqdr     , a.cnsctvo_cdgo_cncpto_gsto        ,
				a.vlr_lqdcn_cncpto   , a.cnsctvo_cdgo_clse_hbtcn, a.cnsctvo_cdgo_cncpts_dt_lsta_prco,
				a.rsltdo_lqdcn       , a.cnsctvo_prstcn_pqte
	From		#tbConceptosServiciosALiquidar a
	Where		a.cnsctvo_cncpto_lqdr > @MaximoConsecutivoConcepto


	--Select * From #tbServiciosALiquidarOrigen
	--Select * From #tbConceptosServiciosALiquidarOrigen
	--Select * From #tbConceptoGastosTmp_1

	Drop Table #tbGruposALiquidar
	Drop Table #tbServiciosALiquidar
	Drop Table #tbConceptosServiciosALiquidar
	Drop Table #tbConceptosNoIncluidos
	Drop Table #tbPaquetes
	Drop Table #tbLSDetListaPreciosxSucursalxPlan
	Drop Table #tbConceptosServiciosALiquidarSinPaquete
	Drop Table #tbServiciosALiquidarPaquete
	Drop Table #tbPaquetesConConceptos
	Drop Table #tbPrestadorLista
	Drop Table #tbProcedimientosJerarquizados
End