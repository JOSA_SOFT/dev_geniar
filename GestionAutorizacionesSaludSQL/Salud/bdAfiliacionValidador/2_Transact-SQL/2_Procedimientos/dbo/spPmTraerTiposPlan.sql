USE BDAfiliacionValidador
GO
/****** Object:  StoredProcedure [dbo].[spPmTraerTiposPlan]    Script Date: 12/10/2017 04:05:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF object_id('dbo.spPmTraerTiposPlan') IS NULL
BEGIN
    EXEC sp_executesql N'CREATE PROCEDURE dbo.spPmTraerTiposPlan AS SELECT 1;'
END
GO


/*----------------------------------------------------------------------------------------
* Metodo o PRG :     dbo.spPmTraerTiposPlan
* Desarrollado por : <\A Ing. Germán Pérez - Geniar S.A.S A\>
* Descripcion  :     <\D Este procedimiento permite recuperar la lista de los tipos de plan D\>    
* Observaciones :    <\O O\>    
* Parametros  :      <\P @ldFechaActual Fecha a la cual se valida la vigencia del plan P\>
* Variables   :      <\V V\>
* Fecha Creacion :   <\FC 2017/09/21 FC\>
* Ejemplo: 
    <\EJ
        EXEC dbo.spPmTraerTiposPlan null
    EJ\>
*------------------------------------------------------------------------------------------------------------------------ 
* Modificado Por     : <\AM  AM\>    
* Descripcion        : <\DM  DM/>
* Nuevos Parametros  : <\PM  PM\>    
* Nuevas Variables   : <\VM VM\>    
* Fecha Modificacion : <\FM 2017/06/12 FM\>   
*-----------------------------------------------------------------------------------------*/
ALTER PROCEDURE dbo.spPmTraerTiposPlan
	@ldFechaActual Datetime
AS
BEGIN
	SET NOCOUNT ON;

	Declare @lcVisibleUsuario udtLogico

	Set @lcVisibleUsuario = 'S'

	If @ldFechaActual Is Null
		Set @ldFechaActual = getDate()

    Select     a.cnsctvo_cdgo_tpo_pln, a.cdgo_tpo_pln, a.dscrpcn_tpo_pln
	From       dbo.tbTiposPlan_vigencias a With (Nolock)		
	Where      a.vsble_usro = @lcVisibleUsuario
	And        @ldFechaActual Between a.inco_vgnca And a.fn_vgnca
	Order By   a.cnsctvo_cdgo_tpo_pln
END
