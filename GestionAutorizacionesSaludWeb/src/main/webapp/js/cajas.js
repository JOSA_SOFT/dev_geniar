function formatearMonedaOtroComponente(claseOrigen, claseDestino) {
	var num = jQuery("." + claseOrigen).val();
	if (!isNaN(num)) {
		var numero = '$'
				+ num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
		jQuery("." + claseDestino).html(numero);
	}
}