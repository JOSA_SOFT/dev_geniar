<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>SOS | Login</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/css/estilo_sos-v2.css">

	</head>

	<body>
		<div class="header"></div>
		<form name="logonForm"
			action="j_security_check" method="post">
			<b><br /> <br />
				<center>
					<table align="center">
						<tr height="25px" style="vertical-align: top">
							<td colspan="2" align="center">
								<b>Login de Usuario</b>
							</td>
						</tr>
						<tr>
							<td>
								Usuario:
							</td>
							<td>
								<input type="text" id="j_username" name="j_username" />
							</td>
						</tr>
						<tr>
							<td>
								Password:
							</td>
							<td>
								<input type="password" id="j_password" name="j_password" />
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<br />
								<input type="submit" value="Entrar" class="aceptar" />
							</td>
						</tr>
					</table>
				</center> </b>
		</form>
	</body>
</html>
