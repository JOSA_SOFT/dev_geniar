package com.sos.gestionautorizacionessaludweb.util;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * JSF utilities.
 * 
 * @author sisddc01
 * @version 4/09/2012
 */
public class FacesUtils {

	/** The build properties. */
	private static Properties buildProperties = null;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(FacesUtils.class);

	private FacesUtils() {

	}

	/**
	 * Mostrar mensaje.
	 *
	 * @param color
	 *            the color
	 * @param mensaje
	 *            the mensaje
	 */
	public static void mostrarMensaje(String color, String mensaje) {
		MessagesPopupBean messagesPopupBean = (MessagesPopupBean) FacesUtils.getManagedBean("messagesPopupBean");
		messagesPopupBean.setColor(color);
		messagesPopupBean.showError(mensaje);
	}

	public static String getUserName() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getExternalContext().getUserPrincipal().getName();
	}

	/**
	 * Send redirect.
	 *
	 * @param url
	 *            the url
	 */
	public static void sendRedirect(String url) {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();

		try {

			ec.redirect(url);

		} catch (Exception ex) {
			LOG.error(ex);
			addErrorMessage(ex.getMessage());
		}
	}

	/**
	 * Get servlet context.
	 * 
	 * @return the servlet context
	 */
	public static ServletContext getServletContext() {
		return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	}

	/**
	 * Gets the external context.
	 * 
	 * @return the external context
	 */
	public static ExternalContext getExternalContext() {
		FacesContext fc = FacesContext.getCurrentInstance();
		return fc.getExternalContext();
	}

	/**
	 * Gets the http session.
	 * 
	 * @param create
	 *            the create
	 * @return the http session
	 */
	public static HttpSession getHttpSession(boolean create) {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(create);
	}

	/**
	 * Get managed bean based on the bean name.
	 * 
	 * @param beanName
	 *            the bean name
	 * @return the managed bean associated with the bean name
	 */
	public static Object getManagedBean(String beanName) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ELContext elc = fc.getELContext();
		ExpressionFactory ef = fc.getApplication().getExpressionFactory();
		ValueExpression ve = ef.createValueExpression(elc, getJsfEl(beanName), Object.class);
		return ve.getValue(elc);
	}

	/**
	 * Remove the managed bean based on the bean name.
	 * 
	 * @param beanName
	 *            the bean name of the managed bean to be removed
	 */
	public static void resetManagedBean(String beanName) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ELContext elc = fc.getELContext();
		ExpressionFactory ef = fc.getApplication().getExpressionFactory();
		ef.createValueExpression(elc, getJsfEl(beanName), Object.class).setValue(elc, null);
	}

	/**
	 * Store the managed bean inside the session scope.
	 * 
	 * @param beanName
	 *            the name of the managed bean to be stored
	 * @param managedBean
	 *            the managed bean to be stored
	 */
	public static void setManagedBeanInSession(String beanName, Object managedBean) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(beanName, managedBean);
	}

	/**
	 * Store the managed bean inside the session scope.
	 * 
	 * @param beanName
	 *            the name of the managed bean to be stored
	 * @param managedBean
	 *            the managed bean to be stored
	 */
	public static Object getManagedBeanInSession(String beanName) {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(beanName);
	}

	/**
	 * Store the managed bean inside the request scope.
	 * 
	 * @param beanName
	 *            the name of the managed bean to be stored
	 * @param managedBean
	 *            the managed bean to be stored
	 */
	public static void setManagedBeanInRequest(String beanName, Object managedBean) {
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(beanName, managedBean);
	}

	/**
	 * Get parameter value from request scope.
	 * 
	 * @param name
	 *            the name of the parameter
	 * @return the parameter value
	 */
	public static String getRequestParameter(String name) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
	}

	/**
	 * Get named request map object value from request map.
	 * 
	 * @param name
	 *            the name of the key in map
	 * @return the key value if any, null otherwise.
	 */
	public static Object getRequestMapValue(String name) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(name);
	}

	/**
	 * Gets the action attribute value from the specified event for the given
	 * name. Action attributes are specified by &lt;f:attribute /&gt;.
	 * 
	 * @param event
	 *            the event
	 * @param name
	 *            the name
	 * @return the action attribute
	 */
	public static String getActionAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	/**
	 * Gets the builds the attribute.
	 * 
	 * @param name
	 *            the name
	 * @return the builds the attribute
	 */
	public static String getBuildAttribute(String name) {
		if (buildProperties != null)
			return buildProperties.getProperty(name, "unknown");
		InputStream is = null;
		try {
			is = getServletContext().getResourceAsStream("/WEB-INF/buildversion.properties");
			buildProperties = new java.util.Properties();
			buildProperties.load(is);
		} catch (Exception e) {
			LOG.error(e);
			is = null;
			buildProperties = null;
			return "unknown";
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception t) {
					LOG.error(t);
				}
			}
		}
		return buildProperties.getProperty(name, "unknown");
	}

	/**
	 * Get parameter value from the web.xml file
	 * 
	 * @param parameter
	 *            name to look up
	 * @return the value of the parameter
	 */
	public static String getFacesParameter(String parameter) {
		// Get the servlet context based on the faces context
		ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

		// Return the value read from the parameter
		return sc.getInitParameter(parameter);
	}

	/**
	 * Obtiene request parameter force id.
	 * 
	 * @param name
	 *            the name
	 * @return request parameter force id
	 */
	public static String getRequestParameterForceId(final String name) {
		String keyFound = (String) CollectionUtils.find(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().keySet(),
				new Predicate() {
					public boolean evaluate(Object pParamNameObj) {
						String paramName = pParamNameObj.toString();
						int index = paramName.lastIndexOf(":");
						if (index > 0) {
							paramName = paramName.substring(index + 1);
						}
						return name.equals(paramName);
					}
				});
		keyFound = StringUtils.isBlank(keyFound) ? "" : keyFound;
		return getRequestParameter(keyFound);
	}

	/**
	 * Add information message.
	 * 
	 * @param msg
	 *            the information message
	 */
	public static void addInfoMessage(String msg) {
		addInfoMessage(null, msg);
	}

	/**
	 * Add information message to a specific client.
	 * 
	 * @param clientId
	 *            the client id
	 * @param msg
	 *            the information message
	 */
	public static void addInfoMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
	}

	/**
	 * Add information message to a specific client.
	 * 
	 * @param clientId
	 *            the client id
	 * @param msg
	 *            the information message
	 */
	public static void addWarnMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
	}

	/**
	 * Add error message.
	 * 
	 * @param msg
	 *            the error message
	 */
	public static void addErrorMessage(String msg) {
		addErrorMessage(null, msg);
	}

	/**
	 * Add error message to a specific client.
	 * 
	 * @param clientId
	 *            the client id
	 * @param msg
	 *            the error message
	 */
	public static void addErrorMessage(String clientId, String msg) {
		FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}

	/**
	 * Gets the jsf el.
	 * 
	 * @param value
	 *            the value
	 * @return the jsf el
	 */
	private static String getJsfEl(String value) {
		return "#{" + value + "}";
	}

	/**
	 * Finds component with the given id.
	 * 
	 * @param c
	 *            component check children of.
	 * @param id
	 *            id of component to search for.
	 * @return found component if any.
	 */
	public static UIComponent findComponent(UIComponent c, String id) {
		if (id.equals(c.getId())) {
			return c;
		}
		Iterator<UIComponent> kids = c.getFacetsAndChildren();
		while (kids.hasNext()) {
			UIComponent found = findComponent(kids.next(), id);
			if (found != null) {
				return found;
			}
		}
		return null;
	}

	// *******************************************

	/**
	 * Gets the session parameter.
	 * 
	 * @param name
	 *            the name
	 * @return the session parameter
	 */
	public static Object getSessionParameter(String name) {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(name);
	}

	/**
	 * Sets the session parameter.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void setSessionParameter(String key, Object value) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
	}

	/**
	 * Removes the session parameter.
	 * 
	 * @param key
	 *            the key
	 */
	public static void removeSessionParameter(String key) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
	}

	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	@SuppressWarnings("unused")
	private static Application getApplication() {
		ApplicationFactory appFactory = (ApplicationFactory) FactoryFinder
				.getFactory("javax.faces.application.ApplicationFactory");
		return appFactory.getApplication();
	}

	/**
	 * Gets the servlet request.
	 * 
	 * @return the servlet request
	 */
	private static HttpServletRequest getServletRequest() {
		return ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
	}

	/**
	 * Gets the http request.
	 * 
	 * @return the http request
	 */
	public static HttpServletRequest getHttpRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	/**
	 * Gets the message by key.
	 * 
	 * @param key
	 *            the key
	 * @return the message by key
	 */
	public static String getMessageByKey(String key) {
		try {
			ResourceBundle resourceBundle = FacesContext.getCurrentInstance().getApplication()
					.getResourceBundle(FacesContext.getCurrentInstance(), "msgs");
			return resourceBundle.getString(key);
		} catch (Exception e) {
			LOG.error(e);
		}
		return key;
	}

	/**
	 * Gets the url actual.
	 * 
	 * @return the url actual
	 */
	public static String getUrlActual() {
		return getServletRequest().getRequestURL().toString();
	}

	/**
	 * Gets the direccion remota.
	 * 
	 * @return the direccion remota
	 */
	public static String getDireccionRemota() {
		return getServletRequest().getRemoteAddr();
	}

	/**
	 * Gets the url aplicacion.
	 * 
	 * @return the url aplicacion
	 */
	public static String getUrlAplicacion() {
		HttpServletRequest peticion = getServletRequest();

		StringBuffer url = new StringBuffer();
		int port = peticion.getServerPort();
		if (port < 0) {
			port = 80;
		}
		String scheme = peticion.getScheme();
		url.append(scheme);
		url.append("://");
		url.append(peticion.getServerName());
		if (((scheme.equals("http")) && (port != 80)) || ((scheme.equals("https")) && (port != 443))) {
			url.append(':');
			url.append(port);
		}
		url.append(peticion.getContextPath());
		return url.toString();
	}

	/**
	 * Limpiar sesion.
	 */
	public static void limpiarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
	}

	/**
	 * Convert to select items.
	 *
	 * @param <T>
	 *            tipo generico
	 * @param objects
	 *            the objects
	 * @param valueName
	 *            the value name
	 * @param descripcionName
	 *            the descripcion name
	 * @param clazz
	 *            the clazz
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public static <T> List<SelectItem> convertToSelectItems(List<T> objects, String valueName, String descripcionName,
			Class<? extends T> clazz) throws Exception {
		ArrayList<SelectItem> listSelectItem = new ArrayList<SelectItem>(objects.size());
		Field valueField = clazz.getDeclaredField(valueName);
		Field descripcionField = clazz.getDeclaredField(descripcionName);
		valueField.setAccessible(true);
		descripcionField.setAccessible(true);
		for (T t : objects) {
			Object value = valueField.get(t);
			Object dValue = descripcionField.get(t);
			String descripcion = dValue == null ? null : String.valueOf(dValue);
			listSelectItem.add(new SelectItem(value, descripcion, descripcion));
		}

		return listSelectItem;
	}

	/**
	 * Obtiene la ip de la maquina donde se realiza la peticion
	 * 
	 * @return
	 * @author Fabian Caicedo - Geniar SAS
	 */
	public static String obtenerIPCliente() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String ipAddress = req.getHeader("X-FORWARDED-FOR");
		if (ipAddress != null) {
			ipAddress = ipAddress.replaceFirst(",.*", ""); // cares only about
															// the first IP if
															// there is a list
		} else {
			ipAddress = req.getRemoteAddr();
		}
		return ipAddress;
	}

	/**
	 * Obtener string en formato XML.
	 *
	 * @param descripcion
	 *            the descripcion
	 * @return the string in XML format
	 */
	public static String descripcionToXMLFormat(String descripcion) {
		String[] arrayDescripcion = descripcion.split(ConstantesWeb.VALOR_MAS);
		StringBuilder sb = new StringBuilder(ConstantesWeb.INICIO_XML_PRESTACIONES);

		for (String prestacion : arrayDescripcion) {
			sb.append(ConstantesWeb.VALUE_XML_PRESTACION).append(prestacion).append(ConstantesWeb.CLOSE_XML_PRESTACION);
		}

		sb.append(ConstantesWeb.FIN_XML_PRESTACIONES);

		return sb.toString();
	}

	/**
	 * Obtener string en formato XML.
	 *
	 * @param tipos
	 *            the tipos
	 * @return the tipos in XML format
	 */
	public static String tiposToXMLFormat(String tipos, String splitVariable) {
		String[] arrayTipos = tipos.split(splitVariable);
		StringBuilder sb = new StringBuilder(ConstantesWeb.INICIO_XML_TIPOS);

		for (String tipo : arrayTipos) {
			sb.append(ConstantesWeb.VALUE_XML_TIPO).append(tipo).append(ConstantesWeb.CLOSE_XML_TIPO);
		}

		sb.append(ConstantesWeb.FIN_XML_TIPOS);

		return sb.toString();
	}

	/**
	 * Obtener string en formato XML.
	 *
	 * @param datos
	 * @return the tipos in XML format
	 */
	public static String cadenaToXMLFormat(String datos, String splitVariable, String coleccion, String nombreObjeto) {
		if (datos != null && !datos.isEmpty()) {
			String[] arrayTipos = datos.split(splitVariable);
			StringBuilder sb = new StringBuilder(1000);

			sb.append("<").append(coleccion).append(">");
			for (String tipo : arrayTipos) {
				sb.append("<").append(nombreObjeto).append(">").append("<dto>").append(tipo).append("</dto>")
						.append("</").append(nombreObjeto).append(">");
			}
			sb.append("</").append(coleccion).append(">");

			return sb.toString();
		}
		return null;
	}
}