package com.sos.gestionautorizacionessaludweb.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/**
 * The Class FilesUtil.
 */
public class FilesUtil {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(FilesUtil.class);

	/** The Constant BUFFER_SIZE. */
	private static final int BUFFER_SIZE = 1024;

	/**
	 * Zippear.
	 *
	 * @param pFile
	 *            the file
	 * @param pZipFile
	 *            the zip file
	 * @throws Exception
	 *             the exception
	 */
	public void zippear(String pFile, String pZipFile) {
		// objetos en memoria
		FileInputStream fis = null;
		FileOutputStream fos = null;
		ZipOutputStream zipos = null;

		// buffer
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			// fichero a comprimir
			fis = new FileInputStream(pFile);
			// fichero contenedor del zip
			fos = new FileOutputStream(pZipFile);
			// fichero comprimido
			zipos = new ZipOutputStream(fos);

			String nameFile = pFile.replace("D:/", "");

			ZipEntry zipEntry = new ZipEntry(nameFile);
			zipos.putNextEntry(zipEntry);
			int len = 0;
			// zippear
			while ((len = fis.read(buffer, 0, BUFFER_SIZE)) != -1)
				zipos.write(buffer, 0, len);
			// volcar la memoria al disco
			zipos.flush();
		} catch (Exception e) {
			LOG.error(e);
		} finally {
			// cerramos los files
			try {
				zipos.close();
				fis.close();
				fos.close();
			} catch (IOException e) {
				LOG.error(e);
			}

		} // end try
	} // end Zippear

	/**
	 * Zip directorio.
	 *
	 * @param dir
	 *            the dir
	 * @param destino
	 *            the destino
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 */
	// Crea un zip desde un directorio.
	public static void zipDirectorio(String dir, String destino) {
		ZipOutputStream out = null;
		try {
			// Revisa que el directorio sea direcorio y lee sus archivos.
			File d = new File(dir);
			if (!d.isDirectory())
				throw new IllegalArgumentException(dir + " no es un directorio.");

			String[] entries = d.list();
			byte[] buffer = new byte[4096]; // Crea un buffer para copiar
			int bytesRead;
			out = new ZipOutputStream(new FileOutputStream(destino));

			for (int i = 0; i < entries.length; i++) {
				File f = new File(d, entries[i]);
				if (f.isDirectory())
					continue; // Ignora el directorio

				FileInputStream in = new FileInputStream(f);
				ZipEntry entry = new ZipEntry(f.getName()); // Crea una entrada zip
				// para cada archivo,
				out.putNextEntry(entry);

				while ((bytesRead = in.read(buffer)) != -1)
					out.write(buffer, 0, bytesRead);
				in.close();
			}
		} catch (Exception e) {
			LOG.error(e);
		} 
		finally {
			// cerramos los files
			try {
				out.close();
			} catch (IOException e) {
				LOG.error(e);
			}

		}
	}
	
	/**
	 * Recibe el directorio local donde se encuentran los archivos generados, un array con los nombres de los archivos 
	 * y un String con el nombre del archivo de salida
	 * @param directory
	 * @param files
	 * @param fileOutputName
	 * @throws IOException 
	 */
	public static ByteArrayOutputStream appendFiles(File directory, String[] files, String fileOutputName) throws IOException {
		
		if (files == null || files.length == 0) {
			return null;
		}
		FileOutputStream fos = null;
		BufferedOutputStream os = null;
		ByteArrayOutputStream baos = null;
		List<byte[]> lstPdfByte = new ArrayList<byte[]>();
			
		try {
			fos = new FileOutputStream(fileOutputName);
			os = new BufferedOutputStream(fos);			
			lstPdfByte = cargarListaPdf(lstPdfByte, directory, files);					
			concatPDFs(lstPdfByte, os);
			baos = getFile(fileOutputName);
			os.close();
			fos.close();
		} catch (DocumentException e) {
			throw new IOException(e.getCause());
		} finally {
			
		}
		return baos;
	}
	
	/**
	 * Recibe una URL o URI y retorna un objecto ByteArrayOutputStream 
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	private static ByteArrayOutputStream getFile(String dir) throws IOException {
		FileInputStream fis = new FileInputStream(dir);
		BufferedInputStream bis = new BufferedInputStream(fis);
		ByteArrayOutputStream buffer = getBuffer(bis);
		fis.close();
		bis.close();
		
		return buffer;
	}
	
	/**
	 * Recibe los nombres de los archivos la ruta donde se encuentran y retorna una lista de archivos como
	 * byte array.
	 * @param lstPdfByte
	 * @param directory
	 * @param files
	 * @return
	 * @throws IOException
	 */
	private static List<byte[]> cargarListaPdf(List<byte[]> lstPdfByte, File directory, String[] files) throws IOException {
		
		for (String fileName : files) {
			ByteArrayOutputStream buffer = getFile(directory.getPath() + File.separator + fileName);
			lstPdfByte.add(buffer.toByteArray());
			buffer.close();			
		}
		return lstPdfByte;
	}
	
	/**
	 * Lee los datos del BufferedInputStream y agrega la informacion a un ByteArrayOutputStream
	 * @param bis
	 * @return
	 * @throws IOException
	 */
	private static ByteArrayOutputStream getBuffer(BufferedInputStream bis) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		byte[] bytes = new byte[2048];
		int bytesRead;			
		while ((bytesRead = bis.read(bytes)) != -1) {
			buffer.write(bytes, 0, bytesRead);
		}			
		buffer.flush();
		return buffer;
	}
	
	
	/**
	 * Une una lista de PDFs en uno solo
	 * 
	 * @author <a href="mailto:jhon.trejos@premize.com">Jhon Trejos - Premize
	 *         S.A.S</a>
	 * @since 30/11/2015
	 * @param streamOfPDFFiles
	 * @param path
	 * @throws DocumentException 
	 * @throws FileNotFoundException
	 */
	private static void concatPDFs(List<byte[]> pdfs,
			OutputStream outputStream) throws IOException, DocumentException {
				
		Document document = new Document();

		try {
			List<PdfReader> readers = new ArrayList<PdfReader>();
			for (byte[] pdfStream : pdfs) {
				PdfReader pdfReader = new PdfReader(pdfStream);
				readers.add(pdfReader);
			}

			PdfWriter writer = PdfWriter.getInstance(document, outputStream);

			document.open();
			PdfContentByte cb = writer.getDirectContent();
			
			for (PdfReader pdfReader : readers) {
				
				for (int pageOfCurrentReaderPDF = 1; 
						pageOfCurrentReaderPDF <= pdfReader.getNumberOfPages();
						pageOfCurrentReaderPDF++){
					
					Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
					document.setPageSize(rectangle);
					document.newPage();

					PdfImportedPage page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
					
					switch (rectangle.getRotation()) {
						case 0:
							cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
							break;
						case 90:
							cb.addTemplate(page, 0, -1f, 1f, 0, 0, rectangle.getHeight());
							break;
						case 180:
							cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
							break;
						case 270:
							cb.addTemplate(page, 0, 1.0F, -1.0F, 0, rectangle.getWidth(), 0);
							break;		
						default: /*no-op*/
							break;
					}
									
					// Paginar
					cb.beginText();
					cb.endText();
				}
			}
			
			document.close();
			outputStream.flush();
		} finally {
			if (document.isOpen()) {
				document.close();
			}
		}
	}
	

	/**
	 * Compress the given directory with all its files.
	 *
	 * @param directory
	 *            the directory
	 * @param files
	 *            the files
	 * @return the byte[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static byte[] zipFiles(File directory, String[] files) throws IOException {

		if (files == null || files.length == 0) {
			return new byte[0];
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(10*1024*files.length);
		ZipOutputStream zos = new ZipOutputStream(baos);
		byte[] bytes = new byte[2048];

		for (String fileName : files) {
			FileInputStream fis = new FileInputStream(directory.getPath() + File.separator + fileName);
			BufferedInputStream bis = new BufferedInputStream(fis);

			zos.putNextEntry(new ZipEntry(fileName));

			int bytesRead;
			while ((bytesRead = bis.read(bytes)) != -1) {
				zos.write(bytes, 0, bytesRead);
			}
			zos.closeEntry();
			bis.close();
			fis.close();
		}
		zos.flush();
		baos.flush();
		zos.close();
		baos.close();
		return baos.toByteArray();
	}

	/**
	 * Delete directory files.
	 *
	 * @param source
	 *            the source
	 * @throws Exception
	 *             the exception
	 */
	public static void deleteDirectoryFiles(File source) {
		if (null == source) {
			LOG.error("source can't be null.");
		}

		if (!source.exists()) {
			LOG.error("The directory '" + source.getAbsolutePath() + "' does not exist");
		}

		String[] filelist = source.list();

		for (String filelist_element : filelist) {
			File current_file = new File(source.getAbsolutePath() + File.separator + filelist_element);
			deleteFile(current_file);
		}
	}

	/**
	 * Delete file.
	 *
	 * @param file
	 *            the file
	 */
	public static void deleteFile(File file) {
		if (null == file) {
			LOG.error("file can't be null.");
		}

		file.delete();
	}

}
