package com.sos.gestionautorizacionessaludweb.util;

import java.util.ArrayList;
import java.util.List;






import javax.faces.model.SelectItem;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.ips.modelo.IPSVO;
import com.sos.medico.modelo.MedicoVO;

/**
 * Class Utilitaria para el ingreso de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * para los diagnosticos
 * @author Ing. Victor Hugo Gil
 * @version 26/07/2016
 *
 */
public class IngresoSolicitudesMedicoPrestadorUtil {
	
	private IngresoSolicitudesMedicoPrestadorUtil(){
		/*clase utilizaría constructor privado para evitar instancias */
	}
	
	
	/** 
	 * Metodo que permite realizar la validacion del registro medico
	 */
	public static void validaRegistroMedico(String txtConfirmarRegistroMedico, MedicoVO medicoVO) throws LogicException{
		if (!txtConfirmarRegistroMedico.equals(medicoVO.getRegistroMedico())) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_CONSULTA_MEDICO), ErrorType.PARAMETRO_ERRADO);
		}
	}	
	
	/** 
	 * Metodo que permite asinar prestador adscrito
	 */
	public static boolean asignarSiEsAdscritoPrestador(IPSVO ipsVO){
		boolean aux = false;
		
		if (ipsVO != null && ipsVO.getConsecGenerico() == 0) {
			aux = true;
		}
		return aux;
	} 
	
	public static boolean validarPopUpPrestadoresMayor(List<IPSVO> listaIPSVO){
		boolean aux = false;
		if (listaIPSVO != null && !listaIPSVO.isEmpty() && listaIPSVO.size() > ConstantesWeb.TAMANO_LISTA) {
			aux = true;
		}		
		return aux;
	}
	

	public static boolean validarPopUpPrestadoresIgual(List<IPSVO> listaIPSVO){
		boolean aux = false;
		if (listaIPSVO != null && !listaIPSVO.isEmpty() && listaIPSVO.size() == ConstantesWeb.TAMANO_LISTA)  {
			aux = true;
		}		
		return aux;
	}
	
	/** 
	 * Metodo que permite asinar prestador adscrito
	 */
	public static void validacionPrestadorNoAdscrito(IPSVO ipsVO, CiudadVO ciudadVO) throws LogicException {
		if (ipsVO.getConsecCodigoTipoId() == 0 || ipsVO.getConsecCodigoTipoId() == ConstantesWeb.VALOR_INICIAL) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_TIPO_IDENTIFICACION), ErrorType.PARAMETRO_ERRADO);
		}
		if (ipsVO.getConsecCodigoTipoIps() == 0 || ipsVO.getConsecCodigoTipoIps() == ConstantesWeb.VALOR_INICIAL) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTADOR), ErrorType.PARAMETRO_ERRADO);
		}
		if (ciudadVO.getConsecutivoCodigoCiudad() == null || ciudadVO.getConsecutivoCodigoCiudad().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SELECCIONAR_CIUDAD), ErrorType.PARAMETRO_ERRADO);
		}
	}	
	
	/** 
	 * Metodo que permite consultar las ciudades
	 * @throws LogicException 
	 */
	public static List<CiudadVO> buscarCiudades(CiudadVO ciudadVO, RegistrarSolicitudController registrarSolicitudesController) throws LogicException {		
		List<CiudadVO> listaCiudadesVO = new ArrayList<CiudadVO>();
		
		if(ciudadVO != null && ciudadVO.getCodigoCiudad() != null && !ciudadVO.getCodigoCiudad().trim().isEmpty()){
			listaCiudadesVO = registrarSolicitudesController.consultaCiudadxCodigo(ciudadVO.getCodigoCiudad());
		}
		
		if(ciudadVO != null && ciudadVO.getDescripcionCiudad() != null && !ciudadVO.getDescripcionCiudad().trim().isEmpty()){
			listaCiudadesVO = registrarSolicitudesController.consultaCiudadxDescripcion(ciudadVO.getDescripcionCiudad());
		}
		
		if(listaCiudadesVO == null || listaCiudadesVO.isEmpty()){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		
		return listaCiudadesVO;
	}	
	
	/**
	 * metodo que buscar la descripcion de la especialidad del medico
    */	
	public static String buscarDescripcionEspecialidadMedico(List<SelectItem> listaEspecialidadesMedico, MedicoVO medicoVO){
		String descripcionEspecialidadMedico = null;
		List<SelectItem> listaEspecialidades = listaEspecialidadesMedico;
		for (SelectItem item : listaEspecialidades) {
			if (medicoVO.getConsecCodTipoEspe() == Long.parseLong(item.getValue().toString())) {
				descripcionEspecialidadMedico = item.getLabel();
				break;
			}
		}
		
		return descripcionEspecialidadMedico;		
	}
}
