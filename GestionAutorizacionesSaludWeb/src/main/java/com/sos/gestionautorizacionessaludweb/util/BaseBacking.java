/**
 * 
 */
package com.sos.gestionautorizacionessaludweb.util;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

import com.sos.web.jsf.FacesQueryAction;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseBacking.
 * 
 * @param <T>
 *            the generic type
 * @author Oscar Iv�n Prado Perdomo - Expert Information
 * @since 21/09/2011 Clase BaseBacking Descripcion Modificaciones Autor Fecha
 *        Modificacion
 */

public class BaseBacking<T> extends FacesQueryAction<T> {

	/**
	 * M�todo que direcciona a una p�gina dada la regla de navegaci�n.
	 * 
	 * @param navigationRule
	 *            the navigation rule
	 */
	protected void redirectTo(String navigationRule) { 
		FacesContext context = FacesContext.getCurrentInstance();
		NavigationHandler navigation = context.getApplication().getNavigationHandler();
		navigation.handleNavigation(context, "", navigationRule);
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getExternalContext().getUserPrincipal().getName();
	}
}
