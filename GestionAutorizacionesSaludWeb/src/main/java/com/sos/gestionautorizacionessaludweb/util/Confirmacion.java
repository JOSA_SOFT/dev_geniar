/*
 * @author Jorge Andres Garcia Erazo - Geniar S.A.S
 * @since 04/09/2013
 * @version 1.0
 */
package com.sos.gestionautorizacionessaludweb.util;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

/**
 * Class Confirmacion.
 *
 * @author sisddc01
 * @version 4/09/2012
 */
public class Confirmacion implements Serializable {
	
	/** Constante serialVersionUID. */
	private static final long serialVersionUID = 1659790765389230395L;

	/** show. */
	private boolean show;

	/** message. */
	private String message;

	/** response bean. */
	private String responseBean;

	/**
	 * Show confirmation.
	 *
	 * @param message the message
	 * @param responseBean the response bean
	 */
	public void showConfirmation(String message, String responseBean) {
		this.responseBean = responseBean;
		this.message = message;
		this.show = true;
	}

	/**
	 * Send confirmation answer.
	 *
	 * @return string
	 */
	public String sendConfirmationAnswer() {
		Confirmable backingBean = (Confirmable) FacesUtils
				.getManagedBean(responseBean);
		return backingBean.listenerConfirmation(true);
	}

	/**
	 * Close.
	 *
	 * @param eve the eve
	 */
	public void close(ActionEvent eve) {
		this.show = false;
		this.responseBean = "";
		this.message = "";
	}

	/**
	 * Comprueba si es show.
	 *
	 * @return true, si es show
	 */
	public boolean isShow() {
		return show;
	}

	/**
	 * Obtiene message.
	 *
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Establece show.
	 *
	 * @param show Nuevo show
	 */
	public void setShow(boolean show) {
		this.show = show;
	}

}
