package com.sos.gestionautorizacionessaludweb.util;

/**
 * Class ConstantesData Clase donde se relacionan las constantes que maneja la
 * aplicacion para el proyecto data
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/03/2016
 *
 */
public class ConstantesWeb {
	public static final String PLNS = "plns";
	public static final String PLN = "pln";
	public static final String AFLDS = "aflds";
	public static final String AFLD = "afld";
	public static final String ESTDS = "estds";
	public static final String ESTDO = "estdo";
	public static final String SDS = "sds";
	public static final String SDE = "sde";
	public static final String GRP_ENTREGAS = "grpo_entrgs";
	public static final String GRP_ENTREGA = "grpo_entrga";

	public static final String ERROR_EXCEPCION_INESPERADA = "ERROR_EXCEPCION_INESPERADA";

	/**
	 * Atributo que determina el color del msg
	 */
	public static final String DIAGNOSTICO_PPAL = "PRINCIPAL";

	/**
	 * Descripcion que se envia al modificar el campo recobro.
	 */
	public static final String OBSERVACION_MODIFICACION_RECOBRO = "Actualizacion de campo recobro";

	/**
	 * Atributo que determina el color del msg
	 */
	public static final String COLOR_ROJO = "red";

	/**
	 * Atributo que determina el color del msg
	 */
	public static final String COLOR_AZUL = "blue";

	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_SOLICITUD = 1;

	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_MOD_FECHA = 7;
	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_RELIQ = 9;
	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_OPS = 2;

	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_PRESTACIONES_PAFPOC = 8;
	/**
	 * Atributo que determina el consecutvo del tipo motivo causa
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_MOD_DIRECCIONAMIENTO = 3;

	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int CONSECUTIVO_CUPS = 4;

	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final int CONSECUTIVO_CUMS = 5;

	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final String DESCRIPCION_CUMS = "CUMS";

	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final String DESCRIPCION_MEDICAMENTOS = "MEDICAMENTOS";
	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final String DESCRIPCION_CUPS = "CUPS";

	/**
	 * Atributo que determina el consecutvo de insumo
	 */
	public static final int CONSECUTIVO_CUOS = 9;

	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_NO_AUTORIZADA = 8;
	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_APROBADA = 7;
	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_NEGADA = 9;
	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_EN_REMISION_ADMINISTRATIVA = 9;
	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_ANULADA = 14;
	/** Atributo que determina el codigo estado **/
	public static final int CODIGO_ESTADO_DEVUELTA = 3;
	/** codigo estado en Auditoria **/
	public static final int CODIGO_ESTADO_AUDITORIA = 5;
	/** codigo estado en Ingresada **/
	public static final int CODIGO_ESTADO_INGRESADA = 2;

	/**
	 * Atributo que determina una cadenas vacia
	 */
	public static final String CADENA_VACIA = "";

	/**
	 * Atributo que determina si el tipo de prestacion
	 */
	public static final int TIPO_PRESTACION = 1;

	/**
	 * Atributo que determina un valor en SI
	 */
	public static final String SI = "SI";

	/**
	 * Atributo que determina un valor en NO
	 */
	public static final String NO = "NO";

	/**
	 * Atributo que determina si es recien nacido
	 */
	public static final String RECIEN_NACIDO = "1";

	/**
	 * Atributo que determina el nombre del Bean de Inconsistencias
	 */
	public static final String NOMBRE_BEAN_INCONSISTENCIAS = "gestionInconsistenciasBean";

	/**
	 * Atributo que define el mensaje para pacientes de alto riesto
	 */
	public static final String MENSAJE_ALTO_RIESGO = "PACIENTE ALTO RIESGO";

	/**
	 * 
	 */
	public static final String MENSAJE_GUARDAR_RECOBRO_OK = "GUARDAR_RECOBRO_OK";

	/**
	 * Atributo para el valor boolean true
	 */
	public static final String EQUIVALENTE_VERDADERO = "1";

	/**
	 * Atributo para el valor boolean false
	 */
	public static final String EQUIVALENTE_FALSO = "0";

	/**
	 * Atributo para el valor boolean true
	 */
	public static final String CONSULTA_PRESTADOR_COD_INTERNO = "I";

	/**
	 * Atributo para el valor content disposition
	 */
	public static final String CONTENT_DISPOSITION = "Content-disposition";

	/**
	 * Atributo para el valor attachment filename
	 */
	public static final String ATTACHMENT_FILENAME = "attachment; filename=";

	/**
	 * Atributo para el valor cache control
	 */
	public static final String CACHE_CONTROL = "Cache-Control";

	/**
	 * Atributo para el valor must revalidate
	 */
	public static final String MUST_REVALIDATE = "must-revalidate";

	/**
	 * Atributo para el valor post check
	 */
	public static final String POST_CHECK = "post-check=0";

	/**
	 * Atributo para el valor pre check
	 */
	public static final String PRE_CHECK = "pre-check=0";

	/**
	 * Atributo que determina el medio de contacto EPS
	 */
	public static final String MEDIO_CONTACTO_ASI = "01";

	/**
	 * Atributo que determina si el medio de contacto es IPS
	 */
	public static final String MEDIO_CONTACTOS_IPS = "02";

	/**
	 * Atributo que determina si el medio de contacto es Afiliado
	 */
	public static final String MEDIO_CONTACTOS_AFILIADO = "04";

	/**
	 * Atributo que determina si el medio de contacto es Programación Entrega
	 */
	public static final String MEDIO_CONTACTOS_PROG_ENTREGA = "PROGRAMACION ENTREGA";

	/**
	 * Formato de fechas dd/MM/yyyy
	 */
	public static final String FORMATO_FECHA_1 = "dd/MM/yyyy";

	/**
	 * Atributo que determina el nombre del parametro con id de la tarea que
	 * llega en la URL
	 */
	public static final String TASK_ID = "task_id";

	/**
	 * Atributo que determina el nombre del Bean del cliente Rest-BPM
	 */
	public static final String NOMBRE_BEAN_BPM_REST = "clienteRestBpmBean";

	/**
	 * Atributo que determina el nombre del parametro que contiene el id de la
	 * instancia
	 */
	public static final String SOLICITUD_ID = "id_solicitud";

	/**
	 * Atributo que determina el nombre del parametro que contiene el id de la
	 * instancia
	 */
	public static final String TERMINA_TAREA = "1";

	/**
	 * Atributo que determina el nombre del parametro que contiene el id de la
	 * instancia
	 */
	public static final String LIBERAR_TAREA = "2";

	/**
	 * Atributo que determina el nombre del parametro que contiene el id de la
	 * instancia
	 */
	public static final String REASIGNAR_TAREA = "3";

	/**
	 * Atributo que determina el nombre del parametro que contiene el cons de la
	 * devolucion de la tarea presolicitud
	 */
	public static final String DEVOLVER_TAREA = "4";

	/**
	 * Atributo que determina la Url del servicio rest que permite terminar la
	 * tarea humana externa
	 */
	public static final String PROPIEDAD_GESTION_SOLICITUD_URL = "PROPIEDAD_GESTION_SOLICITUD_URL";

	/**
	 * Atributo que determina la Ip del sevidor BPM. Se utilizar en la app Web
	 * para verificar conectividad
	 */
	public static final String PROPIEDAD_ACTION_ANULAR_INSTANCIA = "?action=terminate&parts=all";

	/**
	 * Atributo que determina el usuario a autenticar en el servidor BPM para
	 * terminar la instancia
	 */
	public static final String PROPIEDAD_GESTION_SOLICITUD_USUARIO = "PROPIEDAD_GESTION_SOLICITUD_USUARIO";

	/**
	 * Atributo que determina el password a autenticar en el servidor BPM para
	 * terminar la instancia
	 */
	public static final String PROPIEDAD_GESTION_SOLICITUD_PASSW = "PROPIEDAD_GESTION_SOLICITUD_PASSW";

	/**
	 * Atributo que determina la aplicaciÃ³n en el properties Query
	 */
	public static final String APLICACION_GESTION_SALUD = "APLICACION_GESTION_SALUD";

	/**
	 * Atributo que determina la clasificaciÃ³n en el properties Query
	 */
	public static final String CLASIFICACION_GESTION_SOLICITUD = "CLASIFICACION_GESTION_SOLICITUD";

	/**
	 * Atributo que determina la tarea a ejecutar en el bpm
	 */
	public static final String ACTION_SOLICITUD = "action";

	/**
	 * Atributo que determina el estado en que debe quedar la tarea
	 */
	public static final String ESTADO_ACTION = "finish";

	/**
	 * Atributo que determina el estado en que debe quedar la tarea
	 */
	public static final String ESTADO_TERMINATE = "terminate";

	/**
	 * Codigo tipo de atencion hospitalizacion
	 */
	public static final Integer CODIGO_ORIGEN_ATENCION_HOSPITALIZACION = 2;

	/**
	 * Atributo que determina la Ip del sevidor BPM. Se utilizar en la app Web
	 * para verificar conectividad
	 */
	public static final String PROPIEDAD_GESTION_SOLICITUD_IP = "PROPIEDAD_GESTION_SOLICITUD_IP";

	/**
	 * Atributo que determina la Ip del sevidor BPM. Se utilizar en la app Web
	 * para verificar conectividad
	 */
	public static final String PROPIEDAD_GESTION_RESTO_URL = "task";

	/**
	 * Atributo que determina la Ip del sevidor BPM. Se utilizar en la app Web
	 * para verificar conectividad
	 */
	public static final String PROPIEDAD_GESTION_RESTO_ANULAR_INSTANCIA_URL = "process";

	/**
	 * Atributo que determina la descripcion del estado del afiliado en activo
	 */
	public static final String ESTADO_ACTIVO = "ACTIVO";

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final String ESTADO_INACTIVO = "INACTIVO";

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_ANULADO = 14;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_LIQUIDADO = 9;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_COBRADO = 12;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_APROBADO = 7;

	/**
	 * Atributo que determina COD_ESTADO_PROGRAMACION_ENTREGA_SIN_ENTREGAR
	 */
	public static final Integer COD_ESTADO_PROGRAMACION_ENTREGA_SIN_ENTREGAR = 151;
	/**
	 * Atributo que determina COD_ESTADO_PROGRAMACION_ENTREGA_ENTREGADO
	 */
	public static final Integer COD_ESTADO_PROGRAMACION_ENTREGA_ENTREGADO = 153;
	/**
	 * Atributo que determina COD_ESTADO_PROGRAMACION_ENTREGA_REPROGRAMADO
	 */
	public static final Integer COD_ESTADO_PROGRAMACION_ENTREGA_REPROGRAMADO = 152;
	/**
	 * Atributo que determina COD_ESTADO_PROGRAMACION_ENTREGA_ASOCIADO_OPS
	 */
	public static final Integer COD_ESTADO_PROGRAMACION_ENTREGA_ASOCIADO_OPS = 170;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_DEVUELTO = 6;
	/**
	 * Atributo que determina la descripcion del COD_ESTADO_ENTREGADA
	 */
	public static final Integer COD_ESTADO_ENTREGADA = 15;

	/**
	 * Atributo que indica la descripcion del estado procesada.
	 */
	public static final Integer COD_ESTADO_PROCESADA = 13;

	/**
	 * Atributo que determina la descripcion del estado del afiliado en inactivo
	 */
	public static final Integer COD_ESTADO_AUTORIZADO = 11;

	/**
	 * Atributo que determina la descripcion COD_ESTADO_NO_AUTORIZADO
	 */
	public static final Integer COD_ESTADO_NO_AUTORIZADO = 8;

	/**
	 * Atributo que determina el codigo del estado activo
	 */
	public static final String CODIGO_ESTADO_ACTIVO = "ACTIVO";

	/**
	 * Atributo separador
	 */
	public static final String CARACTER_SEPARADOR = "-";

	/**
	 * Atributo URI
	 */
	public static final String CARACTER_URI = "/";

	/**
	 * Atributo que determina el nombre de la actividad para el Adufitor
	 * Integral
	 */
	public static final String GRUPO_AUDITORIA = "Auditoria medico por riesgo";

	/**
	 * Atributo que determina el consecutivo del grupo auditor de medicina del
	 * trabajo
	 */
	public static final Integer CONS_MEDICINA_TRABAJO = 2;

	/**
	 * Atributo que determina el consecutivo del grupo auditor de contratacion
	 */
	public static final Integer CONS_INCONSISTENCIA_PRESTADOR = 1;

	/**
	 * Atributo que determina el consecutivo del grupo auditor de Auditoria
	 * Integral
	 */
	public static final Integer CONS_AUDITORIA_INTEGRAL = 4;

	/**
	 * Atributo que determina el consecutivo del grupo auditor de Domiciliaria
	 */
	public static final Integer CONS_DOMI = 3;

	/**
	 * Atributo que determina el nombre del bean para el usuario
	 */
	public static final String USUARIO_BEAN = "usuarioBean";

	/**
	 * Atributo que determina el nombre del bean para el menu
	 */
	public static final String MENU_BEAN = "menuBean";

	/**
	 * Atributo que determina el nombre del Alto riesgo
	 */
	public static final String ALTO_RIESGO = "Alto Riesgo";

	/**
	 * Atributo que determina el formato de la fecha
	 */
	public static final String FORMATO_FECHA = "yyyy/MM/dd";

	/**
	 * Atributo que determina el Tipo de estado para las causas
	 */
	public static final String TIPO_ESTADO_NEGOCIO = "TIPO_ESTADO_NEGOCIO";

	/**
	 * Atributo que determina el parametro del servicio
	 */
	public static final String PARAMS = "params";

	/**
	 * Atributo que determina el parametro de la ruta del servicio
	 */
	public static final String PARAMS_PATH = "{\"respuestaAppWeb\":{\"numeroSolicitud\":\"";

	/**
	 * Atributo que determina el parametro de la ruta del servicio
	 */
	public static final String PARAMS_PATH_2 = "\",\"tipoAuditor\":\"";

	/**
	 * Atributo que determina el parametro de la ruta del servicio
	 */
	public static final String PARAMS_PATH_RESULT = "\",\"result\":\"";

	/**
	 * Atributo que determina el parametro de la ruta del servicio
	 */
	public static final String PARAMS_PATH_CIERRE = "\"}}";

	/**
	 * Atributo que determina la excepcion de la validacion de campos
	 * obligatorios
	 */
	public static final String ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION = "ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION";

	/**
	 * Atributo que determina la excepcion de la validacion de campos
	 * obligatorios
	 */
	public static final String ERROR_VALIDACION_TIPO_DIAGNOSTICO = "ERROR_VALIDACION_TIPO_DIAGNOSTICO";

	public static final String ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTACION = "ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTACION";

	/**
	 * Atributo que determina la excepcion de la validacion de campos
	 * obligatorios
	 */
	public static final String ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO = "ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO";

	/**
	 * Atributo que determina la excepcion de la validacion de campos
	 * obligatorios
	 */
	public static final String ERROR_VALIDACION_DIAGNOSTICO_PRINCIPAL = "ERROR_VALIDACION_DIAGNOSTICO_PRINCIPAL";

	/**
	 * Atributo que determina la excepcion de la validacion de campos
	 * obligatorios
	 */
	public static final String ERROR_VALIDACION_DIAGNOSTICO = "ERROR_VALIDACION_DIAGNOSTICO";

	/**
	 * Atributo que determina el parametro de diagnostico principal
	 */
	public static final int DIAGNOSTICO_PRINCIPAL = 1;

	/**
	 * Atributo que determina el parametro para la excepcion devuelta
	 */
	public static final String EXCEPCION_SOLICITUD_DEVUELTA = "EXCEPCION_SOLICITUD_DEVUELTA";

	/**
	 * Atributo que determina el parametro para la excepcion devuelta
	 */
	public static final String EXCEPCION_SOLICITUD_AUDITORIA = "EXCEPCION_SOLICITUD_AUDITORIA";

	/**
	 * Atributo que determina el parametro para la excepcion inconsistencias
	 * adminitrativas
	 */
	public static final String EXCEPCION_SOLICITUD_INCONSISTENCIAS_ADMINISTRATIVAS = "EXCEPCION_SOLICITUD_INCONSISTENCIAS_ADMINISTRATIVAS";

	/**
	 * Atributo que determina el parametro para la excepcion inconsistencias
	 */
	public static final String EXCEPCION_SOLICITUD_INCONSISTENCIAS = "EXCEPCION_SOLICITUD_INCONSISTENCIAS";

	/**
	 * Atributo que determina el parametro para varias excepcion
	 */
	public static final String EXCEPCION_SOLICITUD_MIX = "EXCEPCION_SOLICITUD_MIX";

	/**
	 * Atributo que determina el parametro para solicitud creada
	 */
	public static final String EXCEPCION_SOLICITUD_CREADA = "EXCEPCION_SOLICITUD_CREADA";

	/**
	 * Atributo que determina el parametro para varias excepcion devueltas
	 */
	public static final String EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS = "EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS";

	/**
	 * Atributo que determina el parametro para excepcion del llamado al bpm
	 */
	public static final String EXCEPCION_SERVICIO_BPM_OK = "EXCEPCION_SERVICIO_BPM_OK";

	/**
	 * Atributo que determina el parametro para excepcion del llamado al bpm
	 */
	public static final String EXCEPCION_SERVICIO_BPM = "EXCEPCION_SERVICIO_BPM";

	/**
	 * Atributo que determina el parametro para excepcion del llamado al bpm
	 */
	public static final String EXCEPCION_CONSULTA_MEDICO = "EXCEPCION_CONSULTA_MEDICO";

	/**
	 * Atributo que determina el mensaje para medico
	 */
	public static final String MENSAJE_MEDICO = "El m\u00E9dico con n\u00FAmero de identificaci\u00F3n ";

	/**
	 * Atributo que determina el mensaje para el prestador
	 */
	public static final String MENSAJE_PRESTADOR = "El prestador con n\u00FAmero de identificaci\u00F3n ";

	/**
	 * Atributo que determina el mensaje para registro medico
	 */
	public static final String MENSAJE_REGISTRO_MEDICO = " y registro m\u00E9dico ";

	/**
	 * Atributo que determina el mensaje para la razon social
	 */
	public static final String MENSAJE_RAZON_SOCIAL = " y raz\u00F3n social ";

	/**
	 * Atributo que determina el mensaje para registro medico
	 */
	public static final String MENSAJE_EXISTE = " ya existe";

	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int TAMANO_LISTA = 1;

	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int VALOR_INICIAL = -1;

	/**
	 * Atributo que determina el parametro para la validacion del tipo de
	 * identificacion
	 */
	public static final String ERROR_VALIDACION_TIPO_IDENTIFICACION = "ERROR_VALIDACION_TIPO_IDENTIFICACION";

	/**
	 * Atributo que determina el parametro para la validacion del tipo de
	 * prestador
	 */
	public static final String ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTADOR = "ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTADOR";

	/**
	 * Atributo que determina el parametro para la validacion de la ciudad
	 */
	public static final String ERROR_VALIDACION_SELECCIONAR_CIUDAD = "ERROR_VALIDACION_SELECCIONAR_CIUDAD";

	/**
	 * Atributo que determina el parametro del codigo interno
	 */
	public static final String CODIGO_INTERNO = "5999";

	/**
	 * Atributo que determina el parametro de validacion de documentos soporte
	 */
	public static final String EXCEPCION_VALIDACION_DOCUMENTOS_SOPORTE = "EXCEPCION_VALIDACION_DOCUMENTOS_SOPORTE";

	/**
	 * Atributo que determina el parametro de validacion del tamaÃ±o documentos
	 * soporte
	 */
	public static final String EXCEPCION_VALIDACION_TAMANO_DOCUMENTOS_SOPORTE = "EXCEPCION_VALIDACION_TAMANO_DOCUMENTOS_SOPORTE";

	/**
	 * Atributo que determina el parametro de validacion del tamaÃ±o documentos
	 * soporte
	 */
	public static final String TAMANO_DOCUMENTOS_SOPORTE = "Mb";

	/**
	 * Atributo que determina el parametro de validacion de las fechas de
	 * hospitalizacion
	 */
	public static final String ERROR_VALIDACION_FECHAS_HOSPITALIZACION = "ERROR_VALIDACION_FECHAS_HOSPITALIZACION";

	/**
	 * Atributo que determina el parametro de validacion del corte cuenta
	 */
	public static final String EXCEPCION_CORTE_CUENTA = "EXCEPCION_CORTE_CUENTA";

	/**
	 * Atributo que determina la excepcion datos obligatorios
	 */
	public static final String EXCEPCION_DATOS_OBLIGATORIOS = "EXCEPCION_DATOS_OBLIGATORIOS";

	/**
	 * Atributo que determina la excepcion datos obligatorios
	 */
	public static final String EXCEPCION_CONSULTA_OFICINA = "EXCEPCION_CONSULTA_OFICINA";

	/**
	 * Atributo que determina la excepcion datos obligatorios
	 */
	public static final String EXCEPCION_SELECCION_GRUPO_FAMILIAR = "EXCEPCION_SELECCION_GRUPO_FAMILIAR";

	/**
	 * Atributo que determina la excepcion para una solicitud
	 */
	public static final String EXCEPCION_BUSQUEDA_SOLICITUD = "EXCEPCION_BUSQUEDA_SOLICITUD";

	/**
	 * Atributo que determina la excepcion datos obligatorios hospitalizacion
	 */
	public static final String EXCEPCION_HOSPITALIZACION_DATOS_OBLIGATORIOS = "EXCEPCION_HOSPITALIZACION_DATOS_OBLIGATORIOS";

	/**
	 * Atributo que determina la excepcion estancia hospitalizacion
	 */
	public static final String EXCEPCION_VALIDACION_HOSPITALIZACION_ESTANCIA = "EXCEPCION_VALIDACION_HOSPITALIZACION_ESTANCIA";

	/**
	 * Atributo que determina el parametro del consecutivo del estado en
	 * auditoria
	 */
	public static final String CONSECUTIVO_AUDITORIA = "CONSECUTIVO_AUDITORIA";

	/**
	 * Atributo que determina el parametro para el tipo de motivo de
	 * direccionamiento
	 */
	public static final int MOTIVOS_DIRECCIONAMIENTO = 1;

	/**
	 * Atributo que determina el parametro para el tipo de motivo de
	 * direccionamiento
	 */
	public static final int TIPO_MOTIVOS_DIRECCIONAMIENTO = 3;

	/**
	 * Atributo que determina el parametro para el tipo de motivo para crear una
	 * prestacion
	 */
	public static final int TIPO_MOTIVOS_CREAR_PRESTACION = 5;

	/**
	 * Atributo que determina el parametro para el tipo de motivo para la
	 * devolucion de una pretacion
	 */
	public static final int TIPO_MOTIVOS_DEVOLUCION = 6;

	/**
	 * Atributo que determina el parametro para el tipo de motivo de fecha
	 * entrega
	 */
	public static final int MOTIVOS_FECHA_ENTREGA = 2;

	/**
	 * Atributo que determina el parametro para el tipo de motivo de fecha
	 * entrega
	 */
	public static final int TIPO_MOTIVOS_FECHA_ENTREGA = 4;

	/**
	 * Atributo que determina el consecutivo del estado aprobado de la
	 * prestaciÃ³n
	 */
	public static final int CONS_ESTADO_APROBADO = 7;

	/**
	 * Atributo que determina el consecutivo del estado no autorizado de la
	 * prestaciÃ³n
	 */
	public static final int CONS_ESTADO_NO_AUTORIZADO = 8;
	/**
	 * Atributo que determina ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO
	 */
	public static final String ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO = "ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO";

	/**
	 * Atributo que determina ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO
	 */
	public static final String ERROR_MSG_NUMEROIPS_CAMPO_OBLIGATORIO = "ERROR_MSG_NUMEROIPS_CAMPO_OBLIGATORIO";

	/**
	 * Atributo que determina ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO
	 */
	public static final String ERROR_MSG_NUMEROIPS_FORMATO_INVALIDO = "ERROR_MSG_NUMEROIPS_FORMATO_INVALIDO";
	/**
	 * Atributo que determina ERROR_LONGITUD_MIN_MAX_OBSERVACIONES
	 */
	public static final String ERROR_LONGITUD_MIN_MAX_OBSERVACIONES = "ERROR_LONGITUD_MIN_MAX_OBSERVACIONES";
	/**
	 * Atributo que determina ERROR_FECHA_MOD_NULA
	 */
	public static final String ERROR_FECHA_MOD_NULA = "ERROR_FECHA_MOD_NULA";
	/**
	 * Atributo que determina ERROR_FECHA_INVALIDA
	 */
	public static final String ERROR_FECHA_INVALIDA = "ERROR_FECHA_INVALIDA";
	/**
	 * Atributo que determina EXITO_ANULACION_SOLICITUD
	 */
	public static final String EXITO_ANULACION_SOLICITUD = "EXITO_ANULACION_SOLICITUD";
	/**
	 * Atributo que determina EXITO_ANULACION_OPS
	 */
	public static final String EXITO_ANULACION_OPS = "EXITO_ANULACION_OPS";

	/**
	 * Atributo que determina EXITO_ANULACION_OPS
	 */
	public static final String EXITO_REGISTRO_REASIGNACION = "EXITO_REGISTRO_REASIGNACION";

	/**
	 * Atributo que determina EXITO_CAMBIO_DIRECCIONAMIENTO_OPS
	 */
	public static final String EXITO_CAMBIO_DIRECCIONAMIENTO_OPS = "EXITO_CAMBIO_DIRECCIONAMIENTO_OPS";

	/**
	 * Atributo que determina EXITO_CAMBIO_FECHA_ENTREGA
	 */
	public static final String EXITO_CAMBIO_FECHA_ENTREGA = "EXITO_CAMBIO_FECHA_ENTREGA";

	/**
	 * Atributo que determina EXITO_GESTION_AUTORIZA_PRESTACION
	 */
	public static final String EXITO_RELIQUIDAR = "EXITO_RELIQUIDAR";

	/**
	 * Atributo que determina EXITO_GESTION_AUTORIZA_PRESTACION
	 */
	public static final String EXITO_GESTION_AUTORIZA_PRESTACION = "EXITO_GESTION_AUTORIZA_PRESTACION";

	/**
	 * Atributo que determina el id del estado de inconsistencias en malla
	 */
	public static final int CONS_ESTADO_INCONSISTENCIAS_MALLA = 1;

	/**
	 * Atributo que determina el id del estado de inconsistencias en malla
	 */
	public static final int CONS_ESTADO_AUDITORIA_MALLA = 2;

	/**
	 * Atributo que determina si se visualiza la observacion en el reporte OPS
	 */
	public static final int OBSERVACION_OPS_OK = 1;

	/**
	 * Atributo que determina si se visualiza la observacion en el reporte OPS
	 */
	public static final int OBSERVACION_OPS_NOK = 0;

	/**
	 * Atributo que determina el complemento del nombre del archivo
	 */
	public static final String CARACTER_NOMBRE_ARCHIVO_OPS = "_OPS_";
	/**
	 * Atributo que determina el complemento del nombre del archivo
	 */
	public static final String ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR = "ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR";

	/**
	 * Atributo que determina si se asocia una prestacion
	 */
	public static final String ERROR_ASOCIAR_PRESTACION = "ERROR_ASOCIAR_PRESTACION";

	/**
	 * Atributo que determina la validacion de la minima fecha de solicitud
	 */
	public static final String MINIMA_FECHA_CONSULTA_SOLICITUD = "minimaFechaConsultaSolicitud";

	/**
	 * Atributo que determina la validacion de las fechas para la consulta
	 */
	public static final String ERROR_VALIDACION_FECHAS_CONSULTA_SOLICITUD = "ERROR_VALIDACION_FECHAS_CONSULTA_SOLICITUD";

	/**
	 * Atributo que determina el ERROR_MISMO_PRETADOR_DIRECCIONAMIENTO
	 */
	public static final String ERROR_MISMO_PRETADOR_DIRECCIONAMIENTO = "ERROR_MISMO_PRETADOR_DIRECCIONAMIENTO";

	/**
	 * Atributo que determina el ERROR_NUMERO_PRETADOR_DIRECCIONAMIENTO
	 */
	public static final String ERROR_NUMERO_PRETADOR_DIRECCIONAMIENTO = "ERROR_NUMERO_PRETADOR_DIRECCIONAMIENTO";
	/**
	 * Atributo que determina el ERROR_CONSULTA_PARAMETRO_FECHA
	 */
	public static final String ERROR_CONSULTA_PARAMETRO_FECHA = "ERROR_CONSULTA_PARAMETRO_FECHA";
	/**
	 * Atributo que determina el ERROR_LIQUIDACION_FECHA
	 */
	public static final String ERROR_LIQUIDACION_FECHA = "ERROR_LIQUIDACION_SERVICE";

	/**
	 * Atributo que determina el Usuario que viene del BPM
	 */
	public static final String USER_BPM = "user_bpm";

	/**
	 * Atributo que determina el tipo de auditor
	 */
	public static final String TIPO_AUDITOR = "t";

	/**
	 * Atributo que determina la pantalla de Inconsistencias
	 */
	public static final String ID_INCONSISTENCIAS = "0";

	/**
	 * Atributo que determina la pantalla de contratacion
	 */
	public static final String ID_CONTRATACION = "1";

	/**
	 * Atributo que determina la pantalla de medicina
	 */
	public static final String ID_MEDICINA = "2";

	/**
	 * Atributo que determina la pantalla de domiciliaria
	 */
	public static final String ID_DOMICILIARIA = "3";

	/**
	 * Atributo que determina la pantalla de Auditoria
	 */
	public static final String ID_AUDITORIA = "4";

	/**
	 * Atributo que determina el error para la validacion de las prestaciones
	 * seleccionadas
	 */
	public static final String ERROR_VALIDACION_PRESTACION_ADICIONADA = "ERROR_VALIDACION_PRESTACION_ADICIONADA";

	/**
	 * Atributo que determina la excepecion para el diagnostico principal
	 */
	public static final String EXCEPCION_VALIDACION_DIAGNOSTICO_PRINCIPAL = "EXCEPCION_VALIDACION_DIAGNOSTICO_PRINCIPAL";

	/**
	 * Atributo que determina error de validacion para el tipo de identificacion
	 * medico
	 */
	public static final String ERROR_VALIDACION_TIPO_IDENTIFICACION_MEDICO = "ERROR_VALIDACION_TIPO_IDENTIFICACION_MEDICO";

	/**
	 * Atributo que determina error de validacion para el numero de
	 * identificacion medico
	 */
	public static final String ERROR_VALIDACION_NUMERO_IDENTIFICACION_MEDICO = "ERROR_VALIDACION_NUMERO_IDENTIFICACION_MEDICO";

	/**
	 * Atributo que determina error de validacion para el primer nombre del
	 * medico
	 */
	public static final String ERROR_VALIDACION_PRIMER_NOMBRE_MEDICO = "ERROR_VALIDACION_PRIMER_NOMBRE_MEDICO";

	/**
	 * Atributo que determina error de validacion para el primer apellido del
	 * medico
	 */
	public static final String ERROR_VALIDACION_PRIMER_APELLIDO_MEDICO = "ERROR_VALIDACION_PRIMER_APELLIDO_MEDICO";

	/**
	 * Atributo que determina error de validacion para el primer apellido del
	 * medico
	 */
	public static final String ERROR_VALIDACION_ESPECIALIDAD_MEDICO = "ERROR_VALIDACION_ESPECIALIDAD_MEDICO";

	/**
	 * Atributo que determina error de validacion para el tipo de identificacion
	 * de la IPS
	 */
	public static final String ERROR_VALIDACION_TIPO_IDENTIFICACION_PRESTADOR = "ERROR_VALIDACION_TIPO_IDENTIFICACION_PRESTADOR";

	/**
	 * Atributo que determina error de validacion para el numero de
	 * identificacion de la IPS
	 */
	public static final String ERROR_VALIDACION_NUMERO_IDENTIFICACION_PRESTADOR = "ERROR_VALIDACION_NUMERO_IDENTIFICACION_PRESTADOR";

	/**
	 * Atributo que determina error de validacion para el codigo interno de la
	 * IPS
	 */
	public static final String ERROR_VALIDACION_CODIGO_INTERNO = "ERROR_VALIDACION_CODIGO_INTERNO";

	/**
	 * Atributo que determina error de validacion para la razon social de la IPS
	 */
	public static final String ERROR_VALIDACION_RAZON_SOCIAL = "ERROR_VALIDACION_RAZON_SOCIAL";

	/**
	 * Atributo que determina error de validacion de la ciudad IPS
	 */
	public static final String ERROR_VALIDACION_CIUDAD = "ERROR_VALIDACION_CIUDAD";

	/**
	 * Atributo que determina error de validacion de la prestacion
	 */
	public static final String EXCEPCION_VALIDACION_PRESTACION = "EXCEPCION_VALIDACION_PRESTACION";

	/**
	 * Atributo que determina error de validacion del procedimiento asociado a
	 * la hospitalizacion
	 */
	public static final String EXCEPCION_VALIDACION_PROCEDIMIENTO_HOSPITALIZACION = "EXCEPCION_VALIDACION_PROCEDIMIENTO_HOSPITALIZACION";

	/**
	 * Atributo que determina error de validacion del afiliado
	 */
	public static final String ERROR_VALIDACION_AFILIADO = "ERROR_VALIDACION_AFILIADO";

	/**
	 * Atributo que determina error de archivos para borrar
	 */
	public static final String ERROR_ARCHIVOS_BORRAR = "ERROR_ARCHIVOS_BORRAR";

	/**
	 * Atributo que determina error tope maximo
	 */
	public static final String ERROR_ARCHIVOS_MAXIMO_TOPE = "ERROR_ARCHIVOS_MAXIMO_TOPE";

	/**
	 * Atributo que determina error validar inconsistencia
	 */
	public static final String ERROR_VALIDAR_INCONSISTENCIA = "ERROR_VALIDAR_INCONSISTENCIA";

	/**
	 * Atributo que determina error valor inconsistencia
	 */
	public static final String ERROR_VALOR_INCONSISTENCIA = "ERROR_VALOR_INCONSISTENCIA";

	/**
	 * Atributo que determina el ERROR_NUMERO_PRETADOR_DIRECCIONAMIENTO
	 */
	public static final String MEDICAMENTO = "MEDICAMENTO";

	/**
	 * Atributo que determina el mensaje de existencia del afiliado
	 */
	public static final String AFILIADO_NO_EXISTE = "El afiliado no existe";

	/**
	 * Atributo que determina la excepcion de la validacion de plan
	 */
	public static final String ERROR_VALIDACION_PLAN = "ERROR_VALIDACION_PLAN";

	/**
	 * Atributo que determina la excepcion de la validacion fecha de consulta
	 */
	public static final String ERROR_VALIDACION_FECHA_CONSULTA = "ERROR_VALIDACION_FECHA_CONSULTA";

	/**
	 * Atributo que determina la excepcion de la validacion especial de oficina
	 */
	public static final String ERROR_VALIDACION_ESPECIAL_OFICINA = "ERROR_VALIDACION_ESPECIAL_OFICINA";

	/**
	 * Atributo que determina la excepcion de la validacion especial de afiliado
	 */
	public static final String ERROR_VALIDACION_ESPECIAL_AFILIADO = "ERROR_VALIDACION_ESPECIAL_AFILIADO";

	/**
	 * Atributo que determina la excepcion de servicio de visos
	 */
	public static final String ERROR_EXCEPCION_SERVICIO_VISOS = "ERROR_EXCEPCION_SERVICIO_VISOS";
	/**
	 * Atributo que determina el
	 * ERROR_VALIDACION_ESPECIAL_AFILIADO_MIN_MAX_CARACTERES
	 */
	public static final String ERROR_VALIDACION_ESPECIAL_AFILIADO_MIN_MAX_CARACTERES = "ERROR_VALIDACION_ESPECIAL_AFILIADO_MIN_MAX_CARACTERES";
	/**
	 * Atributo que determina el ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_GESTION
	 */
	public static final String ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_GESTION = "ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_GESTION";

	/**
	 * Atributo que determina el ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO
	 */
	public static final String ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO = "ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO";
	/**
	 * Atributo que determina el
	 * ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER
	 */
	public static final String ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER = "ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER";
	/**
	 * Atributo que determina el ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO
	 */
	public static final String ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO = "ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO";
	/**
	 * Atributo que determina el ERROR_VALIDACION__PRESTACION_SIN_DATOS
	 */
	public static final String ERROR_VALIDACION__PRESTACION_SIN_DATOS = "ERROR_VALIDACION__PRESTACION_SIN_DATOS";
	/**
	 * Atributo que determina el
	 * ERROR_VALIDACION_ESPECIAL_AFILIADO_CAMPO_OBLIGATORIO
	 */
	public static final String ERROR_VALIDACION_ESPECIAL_AFILIADO_CAMPO_OBLIGATORIO = "ERROR_VALIDACION_ESPECIAL_AFILIADO_CAMPO_OBLIGATORIO";
	/**
	 * Atributo que determina el
	 * ERROR_VALIDACION_ESPECIAL_AFILIADO_CAMPO_OBLIGATORIO
	 */
	public static final String ACCION_EJECUTADA_DESCARGA_OPS = " OPS DESCARGADA";
	/**
	 * Atributo que determina el
	 * EXCEPCION_VALIDACION_NUM_VERIFICACION_INCORRECTO
	 */
	public static final String EXCEPCION_VALIDACION_NUM_VERIFICACION_INCORRECTO = "EXCEPCION_VALIDACION_NUM_VERIFICACION_INCORRECTO";
	/**
	 * Atributo que determina el ERROR_EXCEPCION_DESCARGA_OPS_ESTADO_FECHA
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_OPS_ESTADO_FECHA = "ERROR_EXCEPCION_DESCARGA_OPS_ESTADO_FECHA";
	/**
	 * Atributo que determina el EXCEPCION_VALIDACION_VIGENCIA
	 */
	public static final String EXCEPCION_VALIDACION_VIGENCIA = "EXCEPCION_VALIDACION_VIGENCIA";
	/**
	 * Atributo que determina el ERROR_SERVICIO_DIRECCIONAMIENTO
	 */
	public static final String ERROR_SERVICIO_DIRECCIONAMIENTO = "Las prestaciones no pueden ser direccionadas al prestador, no se encontró un prestador parametrizado. Nro. Radicado: ";
	/**
	 * Atributo que determina el
	 * ERROR_EXCEPCION_GENERAR_PROGRAMACION_AFILIADO_SIN_DERECHOS
	 */
	public static final String ERROR_EXCEPCION_GENERAR_PROGRAMACION_AFILIADO_SIN_DERECHOS = "ERROR_EXCEPCION_GENERAR_PROGRAMACION_AFILIADO_SIN_DERECHOS";
	/**
	 * Atributo que determina la marca de calidad
	 */
	public static final String MARCA_CALIDAD = "N";
	/**
	 * Atributo que determina la ERROR_EXCEPCION_DESCARGA_OPS_FUERA_VIGENCIA
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_OPS_FUERA_VIGENCIA = "ERROR_EXCEPCION_DESCARGA_OPS_FUERA_VIGENCIA";
	/**
	 * Atributo que determina la EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION
	 */
	public static final String EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION = "EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION";
	/**
	 * Atributo que determina el COD_MARCA_INCONSISTENCIA
	 */
	public static final int COD_MARCA_INCONSISTENCIA = 3;
	/**
	 * Atributo que determina el
	 * ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_ESTADO
	 */
	public static final String ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_ESTADO = "ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_ESTADO";

	/**
	 * Atributo que determina el ERROR_EXCEPCION_DESCARGA_FECHA_INVALIDA
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_FECHA_INVALIDA = "ERROR_EXCEPCION_DESCARGA_FECHA_INVALIDA";
	/**
	 * Atributo que determina el ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO = "ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO";
	/**
	 * Atributo que determina el
	 * ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_FECHA
	 */
	public static final String ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_FECHA = "ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_FECHA";
	/**
	 * Atributo que determina el
	 * EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION
	 */
	public static final String EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION = "EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION";

	/**
	 * Atributo que determina el ERROR_EXCEPCION_DESCARGA_VENCIDA
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_VENCIDA = "ERROR_EXCEPCION_DESCARGA_VENCIDA";
	/**
	 * Atributo que determina el ERROR_CARGA_PARAMETROS
	 */
	public static final String ERROR_CARGA_PARAMETROS = "ERROR_CARGA_PARAMETROS";
	/**
	 * Atributo que determina el EXCEPCION_PROGRAMACION_SIN_RESULTADO
	 */
	public static final String EXCEPCION_PROGRAMACION_SIN_RESULTADO = "EXCEPCION_PROGRAMACION_SIN_RESULTADO";
	/**
	 * Atributo que determina el SUFIJO_NOMBRE_REPORTE
	 */
	public static final String SUFIJO_NOMBRE_REPORTE = "_DEV_";
	/**
	 * Atributo que determina el EXCEPCION_AFILIADO_SIN_PROGRAMACION
	 */
	public static final String EXCEPCION_AFILIADO_SIN_PROGRAMACION = "EXCEPCION_AFILIADO_SIN_PROGRAMACION";
	/**
	 * Atributo que determina el SUFIJO_NOMBRE_REPORTE
	 */
	public static final String ERROR_VALIDACION_ALTERNATIVA = "ERROR_VALIDACION_ALTERNATIVA";
	/**
	 * Atributo que determina el MINIMA_FECHA_CONSULTA_PROGRAMACION
	 */
	public static final String MINIMA_FECHA_CONSULTA_PROGRAMACION = "minimaFechaConsultaProgramacion";
	/**
	 * Atributo que determina el SUFIJO_NOMBRE_REPORTE_NEGACION
	 */
	public static final String SUFIJO_NOMBRE_REPORTE_NEGACION = "_NEG_";

	/**
	 * Atributo que determina si se asocia una prestacion
	 */
	public static final String ERROR_CONSULTAR_PRESTADOR = "ERROR_CONSULTAR_PRESTADOR";

	/**
	 * Atributo que Error de validacion para el campo de auditor
	 */
	public static final String ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO = "ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO";

	/**
	 * Atributo para el error en la transferencia del archivo adjunto.
	 */
	public static final String ERROR_TRANSFERENCIA_ARCHIVO = "ERROR_TRANSFERENCIA_ARCHIVO";

	/**
	 * Atributo para determinar el nombre del Bean de Consulta Afiliado.
	 */
	public static final String CONSULTA_AFILIADO_BEAN = "consultaAfiliadoBean";

	/**
	 * Atributo para determinar el nombre del Bean de Ingresar Solicitud.
	 */
	public static final String INGRESAR_SOLICITUD_BEAN = "ingresoSolicitudBean";
	/**
	 * Atributo para determinar ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO_OPS
	 */
	public static final String ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO_OPS = "ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO_OPS";
	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion de la OPS
	 */
	public static final String NUMERO_UNICO_OPS = "numeroUnicoOPS";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion de la OPS
	 */
	public static final String USUARIO_CONSULTA = "usuarioConsulta";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion de la OPS
	 */
	public static final String USUARIO_SISTEMA = "usuarioSistema";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion de la OPS
	 */
	public static final String OBSERVACION_ADICIONAL = "observacionAdicional";

	/**
	 * Atributo para ERROR_VALIDACION_FECHAS_CONSULTA_PROGRAMACION
	 */
	public static final String ERROR_VALIDACION_FECHAS_CONSULTA_PROGRAMACION = "ERROR_VALIDACION_FECHAS_CONSULTA_PROGRAMACION";
	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion de la OPS
	 */
	public static final String CONSECUTIVO_SOLICITUD = "consecutivoSolicitud";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion del formato de devolucion
	 */
	public static final String RUTA_LOGO = "rutaLogo";
	/**
	 * Atributo para determinar EXCEPCION_DATOS_OBLIGATORIOS_BUSQUEDA_PRESTACION
	 */
	public static final String EXCEPCION_DATOS_OBLIGATORIOS_BUSQUEDA_PRESTACION = "EXCEPCION_DATOS_OBLIGATORIOS_BUSQUEDA_PRESTACION";
	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion del formato de negacion
	 */
	public static final String CONSECUTIVO_SERVICIO_NEGADO = "consecutivoServicioNegado";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion del formato de negacion
	 */
	public static final String RUTA_FUNDAMENTO_LEGAL = "rutaSubReporteNegacionFundamentoLegal";

	/**
	 * Atributo para determinar el string del parametro enviado para le
	 * generacion del formato de negacion
	 */
	public static final String RUTA_REPORTE_ALTERNATIVAS = "rutaSubReporteNegacionAlternativa";

	/**
	 * Atributo para determinar int para consultar los parametros generales para
	 * validacion del corte cuuenta
	 */
	public static final int PARAMETRO_GENERAL_CORTE_CUENTA_MINIMO = 80;

	/**
	 * Atributo para determinar int para consultar los parametros generales para
	 * validacion del corte cuuenta
	 */
	public static final int PARAMETRO_GENERAL_CORTE_CUENTA_MAXIMO = 81;

	/**
	 * Atributo para determinar int para consultar los parametros generales la
	 * generacion del formato
	 */
	public static final int PARAMETRO_GENERAL_GENERACION_FORMATO = 88;

	/**
	 * Atributo para determinar int para consultar el tamaño minimo de las
	 * observaciones
	 */
	public static final int VALOR_MINIMO_OBSERVACIONES = 10;

	/**
	 * Atributo para determinar int para consultar el tamaño minimo de las
	 * observaciones
	 */
	public static final String EXPRESION_REGULAR_NUMERO_AUTORIZACION_IPS = "[0-9-]{3,20}";

	/**
	 * Atributo para determinar int para consultar el tamaño maximo de las
	 * observaciones
	 */
	public static final int VALOR_MAXIMO_OBSERVACIONES = 250;

	/**
	 * Atributo para determinar la navegacion
	 */

	public static final String IR = "IR_SOLICITUD";

	/**
	 * Atributo que determina el tipo de documentos soporte para mi solicitud.
	 */
	public static final int TIPO_DOCUMENTO_SOPORTE_MI_SOLICITUD = 4;

	/**
	 * Atributo para determinar si la solicitud tiene tutela
	 */
	public static final int TUTELA_SI = 1;

	/**
	 * Atributo para determinar si la solicitud no tiene tutela
	 */
	public static final int TUTELA_NO = 2;

	/**
	 * Atributo para determinar el tamaño de las listas
	 */
	public static final int LISTAS_SIZE = 1;

	/**
	 * Atributo para asignar la opcion del menu de programacion de entrega
	 */
	public static final String OPCION_PROGRAMACION_ENTREGA = "Programación de Entrega";

	/**
	 * Atributo para asignar la opcion del menu de programacion de entrega
	 */
	public static final String OPCION_INGRESAR_SOLICITUD = "Ingresar Solicitud";

	/**
	 * Atributo que determina la excepcion de la validacion del afiliado
	 */
	public static final String EXCEPCION_VALIDACION_CANTIDAD_MINIMA_NOMBRE_APELLIDO = "EXCEPCION_VALIDACION_CANTIDAD_MINIMA_NOMBRE_APELLIDO";

	/**
	 * Atributo que determina la excepcion de la validacion del afiliado
	 */
	public static final String EXCEPCION_VALIDACION_TIPO_NUMERO_IDENTIFICACION = "EXCEPCION_VALIDACION_TIPO_NUMERO_IDENTIFICACION";

	/**
	 * Atributo que determina la minima fecha de consulta
	 */
	public static final String MINIMA_FECHA_CONSULTA = "minimaFechaConsulta";

	/**
	 * Atributo que determina la minima fecha de consulta
	 */
	public static final String MINIMA_FECHA_SOLICITUD = "minimaFechaSolicitud";

	/**
	 * Atributo que determina si aplica lateralidad
	 */
	public static final String APLICA_LATERALIDAD = "N";

	/**
	 * Atributo que determina el tamaño del archivo permitido
	 */
	public static final Integer TAMANO_ARCHIVO = 1048576;

	/**
	 * Atributo que determina la escala del archivo permitido
	 */
	public static final Integer ESCALA = 2;

	/**
	 * Atributo para determinar la cantidad de archivos repetidos
	 */
	public static final int CANTIDAD_ARCHIVOS_REPETIDOS = 1;

	/**
	 * Atributo para determinar la ubicacion paciente
	 */
	public static final int UBICACION_PACIENTE_HOSPITALIZACION = 3;

	/**
	 * Atributo para validacion del resultado de liquidacion
	 */
	public static final int RESULTADO_LIQUIDACION = 1;

	/**
	 * Atributo que determina si es parto multiple
	 */
	public static final int PARTO_MULTIPLE = 1;

	/**
	 * Atributo que determina una cadena en blanco
	 */
	public static final String CADENA_EN_BLANCO = " ";

	/**
	 * Atributo que determina separador de cadena
	 */
	public static final String SEPARADOR_CADENA = ": ";

	/**
	 * Atributo que determina lateralidad
	 */
	public static final int NO_APLICA = 4;

	/**
	 * Atributo que determina la ruta del archivo
	 */
	public static final String RUTA_ARCHIVO = "rutaArchivo";

	/**
	 * Atributo que determina el separador de slash
	 */
	public static final String SEPARADOR_SLASH = "file.separator";

	/**
	 * Atributo que determina el caracter doble slash
	 */
	public static final String CARACTER_SLASH_DOBLE = "\\";

	/**
	 * Atributo que determina el caracter slash
	 */
	public static final String CARACTER_SLASH = "/";

	/**
	 * Atributo que determina error de la lsita de soportes
	 */
	public static final String ERROR_LISTA_SOPORTES = "ERROR_LISTA_SOPORTES";

	/**
	 * Atributo que determina error cuando no existe direccionamiento
	 */
	public static final String ERROR_VALIDACION_DIRECCIONAMIENTO_REQUERIDO = "ERROR_VALIDACION_DIRECCIONAMIENTO_REQUERIDO";

	/**
	 * Atributo que determina que no se debe gestionar estado en Auditoria por
	 * parte del auditor
	 */
	public static final String ERROR_VALIDACION_GESTION_ESTADO_AUDITORIA = "ERROR_VALIDACION_GESTION_ESTADO_AUDITORIA";

	/**
	 * Atributo que determina error cuando no existe la fecha de entrega
	 */
	public static final String ERROR_VALIDACION_FECHA_REQUERIDO = "ERROR_VALIDACION_FECHA_REQUERIDO";

	/**
	 * Atributo que determina error cuando no existe la Sucursal
	 */
	public static final String ERROR_VALIDACION_SUCURSAL_REQUERIDO = "ERROR_VALIDACION_SUCURSAL_REQUERIDO";

	/**
	 * Atributo que determina error cuando no existe el Codigo Interno
	 */
	public static final String ERROR_VALIDACION_CODIGO_INTERNO_REQUERIDO = "ERROR_VALIDACION_CODIGO_INTERNO_REQUERIDO";

	/**
	 * Atributo que determina error cuando no existe la Identificacion
	 */
	public static final String ERROR_VALIDACION_IDENTIFICACION_REQUERIDO = "ERROR_VALIDACION_IDENTIFICACION_REQUERIDO";

	public static final String USUARIO_CLAVE_INVALIDA_DESCARGA = "USUARIO_CLAVE_INVALIDA_DESCARGA";
	public static final String USUARIO_DESCARGA_PERFIL_COORDINADOR_ERROR = "USUARIO_DESCARGA_PERFIL_COORDINADOR_ERROR";

	public static final String USUARIO_INVALIDO = "USUARIO_INVALIDO";

	/**
	 * Atributo que determina el plan POS
	 */
	public static final int PLAN_POS = 1;
	/**
	 * Atributo que determina el plan POS SUBSIDIADO
	 */
	public static final int PLAN_POS_SUBSIDIADO = 9;

	public static final int VALOR_UNO = 1;
	/**
	 * Mnesaje que indica que la prestación de acceso directo se devolvió
	 * satisfactoriamente
	 */
	public static final String PRESTACION_ACCESO_DIRECTO_DEVUELTAS = "PRESTACION_ACCESO_DIRECTO_DEVUELTAS";

	public static final String ERROR_LIQUIDACION_PRESTACION = "ERROR_LIQUIDACION_PRESTACION";

	/**
	 * Atributo que determina el nombre del Bean de GestionAuditoriaIntegralBean
	 */
	public static final String NOMBRE_BEAN_AUDITORIA_INTEGRAL = "gestionAuditoriaIntegralBean";

	/**
	 * Atributo que determina el consecutivo del no recobro Atributo el cual
	 * indica que la validacion de la caja es igual a 'Caja Cerrada'
	 */
	public static final int RETORNO_CAJA_CERRADA = 0;

	/**
	 * Atributo el cual indica que la validacion de la caja es igual a 'Caja
	 * Abierta'
	 */
	public static final int RETORNO_CAJA_ABIERTA = 1;

	/**
	 * Atributo el cual indica que la validacion de la caja es igual a 'Caja
	 * Atributo que indica el tab de detalle de OPS
	 */
	public static final String TAB_DETALLE_OPS = "detalleOPSTab";

	/**
	 * Atributo que indica el tab de generar recibo
	 */
	public static final String TAB_GENERAR_RECIBO = "generarReciboTab";
	/**
	 * Atributo que indica el tab de generar recibo
	 */
	public static final String TAB_GENERAR_NOTA_CREDITO = "generarNotasCreditoTab";

	/**
	 * Atributo que indica el tab de notas credito
	 */
	public static final String TAB_NOTAS_CREDITO = "notasCreditoTab";
	/**
	 * Abierta con Fecha diferente a la actual'
	 */
	public static final int RETORNO_CAJA_ABIERTA_DESTIEMPO = 2;

	/**
	 * Atributo para reemplazar en la cadena de validaciones
	 */
	public static final String CAJA_REPLACE_NOMBRE = "[Nombre]";

	/**
	 * Mensaje para la busqueda
	 */
	public static final String CAJA_DILIGENCIAR_OFICINA = "Por favor diligencie los campos obligatorios para la búsqueda";

	/**
	 * Debe ingresar oficina
	 */
	public static final String CAJA_INGRESAR_OFICINA = "Debe ingresar oficina";

	/**
	 * Debe consultar la oficina
	 */
	public static final String ERROR_CAJA_CONSULTAR_OFICINA = "Debe consultar la oficina";

	/**
	 * public static final String URL_CIERRE_CAJA = "/view/caja/cierre.jsf";
	 * Modulo de caja
	 */
	public static final int MODULO_CAJA = 4000;

	/**
	 * Consecutivo Tipo Billete
	 */
	public static final int DENOMINACION_TIPO_BILLETE = 1;

	/**
	 * Consecutivo Tipo Moneda
	 */
	public static final int DENOMINACION_TIPO_MONEDA = 2;

	/**
	 * Consecutivo estado abierto caja
	 */
	public static final int CONSECUTIVO_ESTADO_APERTURA = 1;

	/**
	 * Validacion causal valor
	 */
	public static final String ERROR_VALIDACION_VALOR_CAUSAL = "Valor: Error de validación: El numero debe estar entre 1 y 99.999.999";

	/**
	 * Validacion causal no seleccionada
	 */
	public static final String ERROR_VALIDACION_CAUSAL_SELECCIONADA = "Error de validación: Seleccione Causal";

	/**
	 * Validacion causal repetida
	 */
	public static final String ERROR_VALIDACION_CAUSAL_REPETIDA = "Causales: Error de validación: La causal ya fue añadida";

	/**
	 * Validacion cuadre caja
	 */
	public static final String ERROR_VALIDACION_CUADRE_CAJA = "Cuadre: Error de validación: La sumatoria de los valores de las causales debe ser igual al descuadre";

	/**
	 * Mensaje de validacion de caracteres.
	 */
	public static final String MENSAJE_ERROR_DESCRIPCION_GENERAR_NOTA_CREDITO = "La descripción no puede superar los 250 caracteres";

	/**
	 * Consecutivo tipo documeto nota credito
	 */
	public static final int CODIGO_TIPO_DOCUMENTO_CAJA_NOTA_CREDITO = 2;

	/**
	 * Consecutivo estado documento generado.
	 */
	public static final int CODIGO_ESTADO_DOCUMENTO_CAJA_GENERADO = 1;

	/**
	 * Consecutivo estado documento pendiente de cobro (notas credito).
	 */
	public static final int CODIGO_ESTADO_DOCUMENTO_CAJA_PENDIENTE_COBRO = 2;

	/**
	 * Consecutivo estado documento cobrado (notas credito).
	 */
	public static final int CODIGO_ESTADO_DOCUMENTO_CAJA_COBRADA = 3;

	/**
	 * Consecutivo tipo nota credito excedente
	 */
	public static final int CODIGO_CONCEPTO_NOTA_CAJA_EXCEDENTE = 2;

	/**
	 * Consecutivo tipo nota credito anulacion OPS
	 */
	public static final int CODIGO_CONCEPTO_NOTA_CAJA_ANULACION = 1;

	/**
	 * Mensaje de exito creacion nota credito
	 */
	public static final String MENSAJE_EXITO_NOTA_CREDITO_CAJA = "Se ha generado una Nota Credito por valor de ";

	/**
	 * Consecutivo parametro general limite de dinero a devolver debe confirmar
	 * con que codigo queda en pruebas y produccion.
	 */
	public static final String PARAMETRO_LIMITE_VALOR_CAJA = "PARAMETRO_LIMITE_VALOR_CAJA";

	/**
	 * Tipo devolucion consignacion bancaria
	 */
	public static final int CONSECUTIVO_CODIGO_TIPO_DEVOLUCION_BANCO = 1;

	/**
	 * Nombre del tab de datos del banco.
	 */
	public static final String TAB_DATOS_BANCO = "datosBancoNotaCreditoTab";

	/**
	 * Clave para validacion de combos de banco.
	 */
	public static final String COMBO_BANCO = "banco";

	/**
	 * Clave de validacion para combo de metodo de devolucion
	 */
	public static final String COMBO_METODO_DEVOLUCION = "metodoDevolucion";

	/**
	 * Label para label de tipo de cuenta de ahorro
	 */
	public static final String COMBO_CUENTA_AHORRO_LABEL = "Ahorro";

	/**
	 * Clave para combo de cuenta de ahorro
	 */
	public static final String COMBO_CUENTA_AHORRO_VALOR = "A";

	/**
	 * Clave para combo de cuenta corriente
	 */
	public static final String COMBO_CUENTA_CORRIENTE_VALOR = "C";

	/**
	 * Label para label de cuenta corriente.
	 */
	public static final String COMBO_CUENTA_CORRIENTE_LABEL = "Corriente";

	/**
	 * Consecutivo tipo contizante
	 */
	public static final int CONSECUTIVO_CODIGO_COTIZANTE = 1;

	/**
	 * Mayoria de edad (anhos)
	 */
	public static final int NUMERO_MAYOR_EDAD = 18;

	/**
	 * Cantidad caracteres permitido numero de cuenta
	 */
	public static final int CANTIDAD_CARACTERES_NUMERO_CUENTA = 100;

	/**
	 * Mensaje de validacion para seleccion de metodo de devolucion
	 */
	public static final String METODO_DEVOLUCION_OBLIGATORIO = "Debe seleccionar un metodo de devolucion";

	/**
	 * Mensaje de validacion para cantidad de caracteres el numero de cuenta.
	 */
	public static final String NUMERO_CUENTA_CANTIDAD_CARACTERES = "Numero de cuenta debe ser menor a 100 caracteres";

	/**
	 * Mensaje de validacion numero de cuenta obligatorio.
	 */
	public static final String NUMERO_CUENTA_OBLIGATORIO = "Numero de cuenta es obligatorio";

	/**
	 * Mensaje de validacion de banco obligatorio.
	 */
	public static final String BANCO_OBLIGATORIO = "El banco es obligatorio";

	/**
	 * Mensaje de que el titular es obligatorio.
	 */
	public static final String TITULAR_OBLIGATORIO = "El titular es obligatorio";

	/**
	 * Mensaje de que el telefono es obligatorio
	 */
	public static final String NUMERO_TELEFONO_OBLIGATORIO = "El numero telefonico es obligatorio";

	/**
	 * Mensaje de que el correo es obligatorio
	 */
	public static final String CORREO_ELECTRONICO_OBLIGATORIO = "El correo electronico es obligatorio";

	/**
	 * Mensaje de que el formato de correo no es valido
	 */
	public static final String FORMATO_ELECTRONICO_OBLIGATORIO = "El formato del correo electronico no es valido";

	/**
	 * Mensaje de error de los filtros obligatorios
	 */
	public static final String MENSAJE_FILTROS_REPORTE = "La Fecha desde y hasta son obligatorias";

	/**
	 * Mensaje de error de la fecha inicial mayor a la final
	 */
	public static final String MENSAJE_FECHAS_REPORTE = "La fecha inicial no debe ser superior a la actual";

	/**
	 * Mensaje de error de consulta que no arroja resultados
	 */
	public static final String CONSULTA_NO_ARROJO_RESULTADOS = "La consulta no arrojo resultados";

	/**
	 * Mensaje de caja que no tiene descuadre
	 */
	public static final String CONSULTA_NO_TIENE_CAUSALES_DESCUADRE = "La caja no tiene causales de descuadre";

	/**
	 * Cerro caja con exito
	 */
	public static final String INFO_CERRAR_EXITO = "Cerro caja con exito";

	/**
	 * Asigno valor recibido con exito
	 */
	public static final String INFO_ASIGNAR_VALOR = "Se asgino el valor recibido con exito";

	/**
	 * Descripcion estado cerrado administrativo
	 */
	public static final String DESCRIPCION_ESTADO_CERRADO_ADMIN = "Cierre administrativo";

	/**
	 * Valor recibido diferente de total en caja
	 */
	public static final String ERROR_VALOR_RECIBIDO = "El Total efectivo en caja es  diferente al recibido";

	/**
	 * No esta asignado como coordinador actualmente
	 */
	public static final String ERROR_COORDINADOR = "No esta asignado como coordinador actualmente";

	/**
	 * Error total recibido no coincide con total cierre
	 */
	public static final String ERROR_TOTAL_RECIBIDO = "El valor Total Recibido oficina no es igual al valor Total  Cierre de oficina";

	/**
	 * Mensaje de exito cierre oficina
	 */
	public static final String INFO_CIERRE_EXITOSO = "Se ha realizado el cierre de la oficina exitosamente";
	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String FECHA_INICIAL_INFORME_CIERRE_CAJA = "FECHA_INICIAL";

	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String FECHA_FINAL_INFORME_CIERRE_CAJA = "FECHA_FINAL";

	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String USUARIO_INFORME_CIERRE_CAJA = "USUARIO";

	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String RUTA_LOGO_SOS = "RUTA_LOGO";

	/**
	 * 
	 */
	public static final String NOMBRE_ARCHIVO_INFORME_CIERRE_CAJA = "InformeCierreCaja_";

	/**
	 * Error de validacion de caja abierta
	 */
	public static final String ERROR_NO_EXISTE_CAJA = "Debe tener una caja abierta";

	/**
	 * Error de validacion de caja abierta
	 */
	public static final String ERROR_EXISTE_CAJA_APERTURADA = "Tiene una caja abierta";

	/**
	 * Parametro Informe cierre oficina OFICINA_USER
	 */
	public static final String PARAM_OFICINA_USER = "OFICINA_USER";

	/**
	 * Parametro Informe cierre oficina OFICINA_RESPONSABLE
	 */
	public static final String PARAM_OFICINA_RESPONSABLE = "OFICINA_RESPONSABLE";
	public static final String NOMBRE_ARCHIVO_RECIBO_CAJA = "ReciboCaja_";
	/**
	 * Mensaje de Error cuando no se encuentra las OPS
	 */
	public static final String ERROR_OPS_NOT_FOUND = "No se encuentran OPS Seleccionadas!";

	/**
	 * No se selecciona ninguna OPS
	 */
	public static final String ERROR_OPS_NO_SELECCIONADO = "Debe seleccionar al menos una OPS";

	/**
	 * Parametro Informe cierre oficina FECHA
	 */
	public static final String PARAM_FECHA = "FECHA";

	/**
	 * Parametro Informe cierre oficina OFICINA_DESC
	 */
	public static final String PARAM_OFICINA_DESC = "OFICINA_DESC";

	/**
	 * Parametro Informe cierre oficina SUMA_TOTAL_RECIBIDO
	 */
	public static final String PARAM_SUMA_TOTAL_RECIBIDO = "SUMA_TOTAL_RECIBIDO";

	/**
	 * Parametro Informe cierre oficina SUMA_BASE
	 */
	public static final String PARAM_SUMA_BASE = "SUMA_BASE";

	/**
	 * Parametro Informe cierre oficina SUMA_FALTANTE
	 */
	public static final String PARAM_SUMA_FALTANTE = "SUMA_FALTANTE";

	/**
	 * Parametro Informe cierre oficina SUMA_SOBRANTE
	 */
	public static final String PARAM_SUMA_SOBRANTE = "SUMA_SOBRANTE";

	/**
	 * Parametro Informe cierre oficina TOTAL_CIERRE_OFICINA
	 */
	public static final String PARAM_TOTAL_CIERRE_OFICINA = "TOTAL_CIERRE_OFICINA";

	/**
	 * Parametro Informe cierre oficina CNS_CIERRE_OFICINA
	 */
	public static final String PARAM_CNS_CIERRE_OFICINA = "CNS_CIERRE_OFICINA";

	/**
	 * Parametro Informe cierre oficina FALTANTE
	 */
	public static final String PARAM_FALTANTE = "FALTANTE";

	/**
	 * Parametro Informe cierre oficina SUMA_BASE_COLUMNA
	 */
	public static final String PARAM_SUMA_BASE_COLUMNA = "SUMA_BASE_COLUMNA";

	/**
	 * Parametro Informe cierre oficina SOBRANTE
	 */
	public static final String PARAM_SOBRANTE = "SOBRANTE";

	/**
	 * Nombre de archivo para generacion del pdf
	 */
	public static final String NOMBRE_ARCHIVO_INFORME_CIERRE_OFICINA = "InformeCierreOficina_";

	/**
	 * Mensaje error para la generacion del informe
	 */
	public static final String ERROR_JASPER = "Error generando el informe";

	/**
	 * Parametro Informe cierre oficina TOTAL_EFECTIVO_CAJA
	 */
	public static final String PARAM_TOTAL_EFECTIVO_CAJA = "TOTAL_EFECTIVO_CAJA";

	/**
	 */
	public static final String FECHA_CIERRE = "FECHA_CIERRE";

	/**
	 */
	public static final String LOGIN = "LOGIN";

	/**
	 */
	public static final String OFICINA = "OFICINA";

	/**
	 */
	public static final String USUARIO = "USUARIO";

	/**
	 * 
	 */
	public static final String IDENTIFICACION_OBLIGATORIA = "El tipo y numero de identificacion es obligatorio";

	public static final String MENSAJE_ERROR_NUMERO_OPS = "El numero de OPS  y el numero recibo es obligatorio";

	/**
	 * Nombre de archivo para generacion del pdf
	 */
	public static final String NOMBRE_ARCHIVO_INFO_CIERRE_CAJA = "InformeCierreCaja_";

	/**
	 * Nombre de archivo para generacion del pdf
	 */
	public static final String NOMBRE_ARCHIVO_REPORTE_NOTA_CREDITO = "Reporte_Nota_Credito_";

	/**
	 * Parametro Informe cierre oficina BASE
	 */
	public static final String PARAM_BASE = "BASE";

	/**
	 * Parametro Informe cierre oficina LIST_BILLETES
	 */
	public static final String PARAM_LIST_BILLETES = "LIST_BILLETES";

	/**
	 * Parametro Informe cierre oficina LIST_CAUSALES
	 */
	public static final String PARAM_LIST_CAUSALES = "LIST_CAUSALES";

	/**
	 * Parametro Informe cierre oficina LIST_MONEDAS
	 */
	public static final String PARAM_LIST_MONEDAS = "LIST_MONEDAS";

	/**
	 * Parametro Informe cierre oficina OFICINA_NOMBRE
	 */
	public static final String PARAM_OFICINA_NOMBRE = "OFICINA_NOMBRE";

	/**
	 * Parametro Informe cierre oficina RESPONSABLE
	 */
	public static final String PARAM_RESPONSABLE = "RESPONSABLE";

	/**
	 * Parametro Informe cierre oficina TOTAL_CAJA
	 */
	public static final String PARAM_TOTAL_CAJA = "TOTAL_CAJA";

	/**
	 * Parametro Informe cierre oficina TOTAL_EFECTIVO_RECAUDADO
	 */
	public static final String PARAM_TOTAL_EFECTIVO_RECAUDADO = "TOTAL_EFECTIVO_RECAUDADO";

	/**
	 * Parametro Informe cierre oficina TOTAL_NOTAS_CREDITO
	 */
	public static final String PARAM_TOTAL_NOTAS_CREDITO = "TOTAL_NOTAS_CREDITO";

	/**
	 * Parametro Informe cierre oficina TOTAL_RECAUDADO
	 */
	public static final String PARAM_TOTAL_RECAUDADO = "TOTAL_RECAUDADO";

	/**
	 * Parametro Informe cierre oficina USER_LOGIN
	 */
	public static final String PARAM_USER_LOGIN = "USER_LOGIN";

	/**
	 * Parametro Informe
	 */
	public static final String PARAM_NUMERO_NOTA_CREDITO = "NUMERO_NOTA_CREDITO";

	/**
	 * Parametro Informe
	 */
	public static final String PARAM_TIPO_E_IDENTIFICACION = "TIPO_E_IDENTIFICACION";

	/**
	 * Parametro Informe NOMBRE
	 */
	public static final String PARAM_NOMBRE = "NOMBRE";

	/**
	 * Parametro Informe OFICINA
	 */
	public static final String PARAM_OFICINA = "OFICINA";

	/**
	 * Parametro Informe VALOR_LETRAS
	 */
	public static final String PARAM_VALOR_LETRAS = "VALOR_LETRAS";

	/**
	 * Parametro Informe TIPO_CUENTA
	 */
	public static final String PARAM_TIPO_CUENTA = "TIPO_CUENTA";

	/**
	 * Parametro Informe NUMERO_CUENTA
	 */
	public static final String PARAM_NUMERO_CUENTA = "NUMERO_CUENTA";

	/**
	 * Parametro Informe NOMBRE_BANCO
	 */
	public static final String PARAM_NOMBRE_BANCO = "NOMBRE_BANCO";

	/**
	 * Parametro Informe NOMBRE_USUARIO_ATENCION
	 */
	public static final String PARAM_NOMBRE_USUARIO_ATENCION = "NOMBRE_USUARIO_ATENCION";

	/**
	 * Mensaje error apertura
	 */
	public static final String TAMANOERROR = "El tamaño [Nombre] de lo ingresado en el campo no es valido";

	/**
	 * Mensaje error campo diligenciado
	 */
	public static final String VACIOERROR = "Por favor diligencie los campos obligatorios para la búsqueda: [Nombre]";

	/**
	 * Codigo oficina
	 */
	public static final String CODIGO_OFICINA = "codigo oficina";

	/**
	 * base
	 */
	public static final int CONSECUTIVO_RECOBRO_NO_RECOBRO = 9;
	public static final String BASE_APERTURA = "base";

	/**
	 * Error codigo oficina Atributo que determina el consecutivo del no recobro
	 */
	public static final int CONSECUTIVO_CODIGO_MEDIO_CONTACTO_IPS = 2;
	public static final String ERROR_CODIGO_OFICINA = "del codigo de la oficina";

	/**
	 * Descripcion oficina Atributo que determina el consecutivo del no recobro
	 */
	public static final String ERROR_DESC_OFICINA = "descripcion oficina";
	public static final String ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO = "ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO";

	/**
	 * Error des oficina Atributo que determina el logout
	 */
	public static final String ERROR_CONSUL_OFICINA = "de la descripcion de la oficina";

	public static final String NUMERO_MAYOR_A_999999999 = "El numero es mayor de 999'999.999";

	public static final String NO_ES_POSIBLE_CONVERTIR_NUMERO = "no es posible convertirlo";

	public static final String NUMERO_DEBE_SER_POSITIVO = "El numero debe ser positivo";

	public static final double NUMERO_LIMITE = 999999999;

	public static final String UN_MILLON = "UN MILLON ";

	public static final String MILLONES = "MILLONES ";

	public static final String UN_MIL = "UN MIL ";

	public static final String MILES = "MILES ";

	public static final String MIL = "MIL ";

	public static final String UN = "UN ";

	public static final String CERO = "CERO ";

	public static final String PESOS = "PESOS ";

	public static final String LONGITUD_MAXIMA = "La longitud maxima debe ser 3 digitos";

	public static final String NUMERO_CIEN = "100";

	public static final String CIEN = "CIEN ";

	public static final String Y = "Y ";

	public static final String NUMERO_IDENTIFICACION = "NUMERO_IDENTIFICACION";

	public static final String TIPO_IDENTIFICACION = "TIPO_IDENTIFICACION";

	public static final String NUI_AFILIADO = "NUI_AFILIADO";

	public static final String OFICINA_ID = "OFICINA_ID";

	public static final String FECHA_INICIAL = "FECHA_INICIAL";

	public static final String FECHA_FINAL = "FECHA_FINAL";

	public static final String OPS = "OPS";

	public static final String RECIBO = "RECIBO";

	public static final String PATH_CONSULTAR_AFILIADO = "Consultar Afiliado";

	public static final String TAB_COMPROBANTES = "comprobanteReciboTab";

	public static final String RUTA_APERTURA_CAJA = "/view/caja/apertura.jsf";

	public static final String RUTA_CIERRE_CAJA = "/view/caja/cierre.jsf";

	public static final String CONSULTA_SOLICITUD = "CONSULTA_SOLICITUD";

	public static final String INGRESO_SOLICITUD = "INGRESO_SOLICITUD";

	public static final String PROGRAMACION_ENTREGA = "PROGRAMACION_ENTREGA";
	public static final String VALOR_LOGOUT = "logout";
	/**
	 * Atributo que determina el logout bpm Mensaje de Error cuando valor
	 * recibido es menor o igual que cero
	 */
	public static final String VALOR_LOGOUT_BPM = "logoutBPM";
	public static final String ERROR_RECIBIDO_ZERO = "El valor recibido debe ser mayor que cero";

	public static final String ERROR_FECHA_VACIA = "La fecha es obligatoria";

	public static final String ERROR_USUARIO_LOGIN_OBLIGATORIO = "El login o el usuario son obligaotrio";

	public static final String CONSECUTIVO_SEDE = "CONSECUTIVO_SEDE";

	public static final int CONSECUTIVO_CUOTA_MODERADORA = 1;

	public static final String CLAVE_MENSAJE_PAGO_IPS = "El pago del copago o cuota moderadora esta a cargo de la IPS";

	public static final String MENSAJE_CAUSAS_NO_PAGO = "Se presentaron las siguientes causales de no pago: ";

	public static final String ACCION_CREAR_NOTA_CREDITO = "ACCION_CREAR_NOTA_CREDITO";

	public static final String ACCION_ACTUALIZAR_NOTA_CREDITO = "ACCION_ACTUALIZAR_NOTA_CREDITO";

	public static final String CIERRE_CAJA_BEAN = "cierreCajaBean";

	public static final String GENERAR_RECIBO_BEAN = "generarReciboBean";

	public static final String GENERAR_NOTA_CREDITO_BEAN = "generarNotaCreditoBean";

	public static final String CONSULTAR_SOLICITUD_BEAN = "consultarSolicitudBean";

	public static final String INGRESO_SOLICITUD_BEAN = "ingresoSolicitudBean";

	public static final String CONSULTA_PROGRAACION_ENTREGA_BEAN = "consultarProgramacionEntregaBean";

	public static final String MENSAJE_ERROR_VALOR_A_PAGAR_MENOR_SUMATORIA = "El valor recibido no cubre el valor total a cancelar";
	public static final String FORMATO_DINERO = "$#,##0";
	public static final Integer OFICINA_SOS = 4;

	/**
	 * Atributo para el valor boolean false
	 */
	public static final String CORTE_CUENTA_INI = "0";

	/**
	 * Atributo para el valor de la clase de atencion HOSP_URG
	 */
	public static final int CLASE_ATENCION_HOSP_URG = 4;

	public static final String FECHA_APERTURA = "FECHA_APERTURA";

	public static final String ERROR_SERVICIO_AUTENTICACION = "ERROR_SERVICIO_AUTENTICACION";

	public static final String GENERAR_PRESTADOR_FILTRO_BEAN = "prestadorFiltroBean";

	public static final String DOS = "DOS  ";
	public static final String TRES = "TRES ";
	public static final String CUATRO = "CUATRO  ";
	public static final String CINCO = "CINCO  ";
	public static final String SEIS = "SEIS  ";
	public static final String SIETE = "SIETE  ";
	public static final String OCHO = "OCHO  ";
	public static final String NUEVE = "NUEVE  ";
	public static final String DIEZ = "DIEZ ";
	public static final String ONCE = "ONCE  ";
	public static final String DOCE = "DOCE  ";
	public static final String TRECE = "TRECE  ";
	public static final String CATORCE = "CATORCE  ";
	public static final String QUINCE = "QUINCE  ";
	public static final String DIECISEIS = "DIECISEIS ";
	public static final String DIECISIETE = "DIECISIETE  ";
	public static final String DIECIOCHO = "DIECIOCHO  ";
	public static final String DIECINUEVE = "DIECINUEVE  ";
	public static final String VEINTE = "VEINTE ";
	public static final String VENTI = "VENTI  ";
	public static final String TREINTA = "TREINTA  ";
	public static final String CUARENTA = "CUARENTA ";
	public static final String CINCUENTA = "CINCUENTA  ";
	public static final String SESENTA = "SESENTA  ";
	public static final String SETENTA = "SETENTA  ";
	public static final String OCHENTA = "OCHENTA  ";
	public static final String NOVENTA = "NOVENTA ";
	public static final String CIENTO = "CIENTO  ";
	public static final String DOSCIENTOS = "DOSCIENTOS ";
	public static final String TRESCIENTOS = "TRESCIENTOS  ";
	public static final String CUATROCIENTOS = "CUATROCIENTOS  ";
	public static final String QUINIENTOS = "QUINIENTOS  ";
	public static final String SEISCIENTOS = "SEISCIENTOS ";
	public static final String SETECIENTOS = "SETECIENTOS  ";
	public static final String OCHOCIENTOS = "OCHOCIENTOS  ";
	public static final String NOVECIENTOS = "NOVECIENTOS  ";
	public static final String INFORME_GENERAL_RECIBO_BEAN = "informeGeneralReciboCajaBean";
	public static final String ERROR_NO_EXISTE_PRESTADOR = "ERROR_NO_EXISTE_PRESTADOR";
	public static final String ES_USUARIOS_IPS = "ES_USUARIOS_IPS";

	public static final int CANTIDAD_MINIMA_LETRA_AUTOCOMPLETAR = 3;
	public static final String ERROR_NO_PRESTACION_ENCONTRADA = "ERROR_NO_PRESTACION_ENCONTRADA";
	public static final String ERROR_SELECCION_FILTRO_ADICIONAL = "ERROR_SELECCION_FILTRO_ADICIONAL";
	public static final String ERROR_SELECCION_FILTRO_VACIO = "ERROR_SELECCION_FILTRO_VACIO";
	public static final String ERROR_CIUDAD_NO_SELECCIONADA = "ERROR_CIUDAD_NO_SELECCIONADA";
	public static final String VACIO = "";
	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int CANTIDAD_PROCEDIMIENTOS_HOSPITALIZACION = 5;

	public static final String CONFIRMACION_BEAN = "confirmacionBean";

	public static final String EJB_RESOLVER_BEAN = "EJBResolverInstance";

	public static final int PERFIL_COORDINADORA = 246;

	public static final String ES_COORDINADOR_ASI = "ES_COORDINADOR_ASI";

	public static final String SEPARADOR = "@";

	/**
	 * Constante que indica el nombre con el cual se recuperara el mensaje de
	 * exito al cambiar el direccionamiento de una prestacion sin OPS
	 */
	public static final String EXITO_CAMBIO_DIRECCIONAMIENTO_PRESTACION_SIN_OP = "EXITO_CAMBIO_DIRECCIONAMIENTO_PRESTACION_SIN_OP";

	/**
	 * Constante que indica el nombre con el cual se recuperara el mensaje de
	 * error cuando se trate de modificar conceptos con numero de OPS en 0 y que
	 * almenos uno de los otros conceptos asociados a la prestacion tenga numero
	 * de OPS
	 */
	public static final String ERROR_DEBE_RELIQUIDAR = "ERROR_DEBE_RELIQUIDAR";

	/**
	 * Constante que indica el mensaje a mostrar cuando la consulta de las
	 * prestaciones de una OPS no retorne resultado.
	 */
	public static final String ERROR_PRESTACION_CONCEPTO_NO_ENCONTRADA = "ERROR_PRESTACION_CONCEPTO_NO_ENCONTRADA";

	public static final String ERROR_ESTADO_NO_ENCONTRADO = "ERROR_ESTADO_NO_ENCONTRADO";

	public static final Integer COD_ESTADO_USADA = 16;

	public static final String ERROR_VALIDACION_DEBE_SELECCIONAR_CAUSAL_NO_COBRO = "ERROR_VALIDACION_DEBE_SELECCIONAR_CAUSAL_NO_COBRO";

	public static final int VALOR_CERO = 0;

	public static final String ERROR_DESCARGA_RECIBO_CAJA = "ERROR_DESCARGA_RECIBO_CAJA";

	public static final String FECHA_REPORTE = "FECHA_REPORTE";

	public static final String DEBE_SELECCIONAR_PRESTADOR = "DEBE_SELECCIONAR_PRESTADOR";
	public static final String DEBE_SELECCIONAR_PRESTACION = "DEBE_SELECCIONAR_PRESTACION";
	public static final String DEBE_SPECIFICAR_DESCRIPCION_PRESTACION = "ERROR_VALIDACION_DESCRIPCION_PRESTACION";

	public static final String VALOR_MAS = "\\+";
	public static final String INICIO_XML_PRESTACIONES = "<prestaciones>";
	public static final String VALUE_XML_PRESTACION = "<prestacion value=\"";
	public static final String CLOSE_XML_PRESTACION = "\" />";
	public static final String FIN_XML_PRESTACIONES = "</prestaciones>";

	public static final String VERSION = "Version 1.0";
	/**
	 * Nombre del perfil del coordinador.
	 */
	public static final String PERFIL_COORDINADOR = "Coordinador de Prestaciones Medicas";
	/**
	 * Atributo que determina el consecutvo del tipo motivo causa para la
	 * anulacion de prestaciones
	 */
	public static final int CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_PRESTACION = 10;

	/**
	 * Nombre del perfil del Administrador CNA.
	 */
	public static final String PERFIL_ADMINISTRADOR_CNA = "AdministradorCNA";

	public static final String EXCEPCION_PERFIL_USUARIO = "EXCEPCION_PERFIL_USUARIO";

	public static final String MESSAGES_POPUP_BEAN = "messagesPopupBean";

	public static final String ERROR_CONSULTAR_CONCEPTOS = "ERROR_CONSULTAR_CONCEPTOS";

	public static final String MENSAJE_GUARDAR_PRESTACION_OK = "MENSAJE_GUARDAR_PRESTACION_OK";

	public static final String OBSERVACION_MODIFICACION_PRESTACION = "Actualizacion de datos";

	public static final String ERROR_BUSQUEDA_CIUDAD = "ERROR_BUSQUEDA_CIUDAD";

	/**
	 * Variable que indica el mensaje de anulacion de prestacion exitosa.
	 */
	public static final String EXITO_ANULACION_PRESTACION = "EXITO_ANULACION_PRESTACION";

	public static final String EXCEPCION_RECIBO_CAJA_EXISTENTE = "EXCEPCION_RECIBO_CAJA_EXISTENTE";

	public static final String EXCEPCION_CIUDAD_REQUERIDA = "EXCEPCION_CIUDAD_REQUERIDA";

	public static final String EXCEPCION_PERSTADORES_NO_ENCONTRADOS = "EXCEPCION_PERSTADORES_NO_ENCONTRADOS";

	public static final String EXCEPTION_CAMPO_OPS_VACIO = "El campo número OPS no puede estar vacio";

	public static final String EXCEPTION_OPS_INVALIDA = "El número OPS no existe";

	public static final String ERROR_VALIDACION_REQUIERE_DIAGNOSTICO_PPAL_OBLIGATORIO = "ERROR_VALIDACION_REQUIERE_DIAGNOSTICO_PPAL_OBLIGATORIO";

	public static final String ERROR_VALIDACION_REQUIERE_MINIMO_OSB = "ERROR_VALIDACION_REQUIERE_MINIMO_OSB";

	public static final String ERROR_VALIDACION_REQUIERE_MINIMO_JUSTIFICACION = "ERROR_VALIDACION_REQUIERE_MINIMO_JUSTIFICACION";

	public static final String ERROR_VALIDACION_REQUIERE_DATOS_NEGACION_OBLIGATORIO = "ERROR_VALIDACION_REQUIERE_DATOS_NEGACION_OBLIGATORIO";

	public static final String ERROR_VALIDACION_REQUIERE_CAUSAS_OBLIGATORIO = "ERROR_VALIDACION_REQUIERE_CAUSAS_OBLIGATORIO";

	public static final String ERROR_VALIDACION_ESTADOS_GESTION_SOLICITUD = "ERROR_VALIDACION_ESTADOS_GESTION_SOLICITUD";

	public static final String OPCION_CAMBIAR_FECHA_ENTREGA = "Cambiar Fecha Entrega";

	public static final String OPCION_CAMBIAR_DIRECCIONAMIENTO = "Cambiar Direccionamiento";

	public static final String ORIGEN_ANULACION_PRESTACION = "Prestacion";

	public static final String ORIGEN_ANULACION_SOLICITUD = "Solicitud";
	public static final String VALOR_COMA = ",";
	public static final String INICIO_XML_TIPOS = "<tipos>";
	public static final String VALUE_XML_TIPO = "<tipo value=\"";
	public static final String CLOSE_XML_TIPO = "\" />";
	public static final String FIN_XML_TIPOS = "</tipos>";

	public static final String DEBE_SELECCIONAR_FECHA_ENTREGA_HASTA = "DEBE_SELECCIONAR_FECHA_ENTREGA_HASTA";
	public static final String DEBE_SELECCIONAR_FECHA_ENTREGA_DESDE = "DEBE_SELECCIONAR_FECHA_ENTREGA_DESDE";
	public static final String DEBE_SELECCIONAR_FECHA_CREACION_HASTA = "DEBE_SELECCIONAR_FECHA_CREACION_HASTA";
	public static final String DEBE_SELECCIONAR_FECHA_CREACION_DESDE = "DEBE_SELECCIONAR_FECHA_CREACION_DESDE";
	public static final String DEBE_SELECCIONAR_CRITERIOS_OBLIGATORIOS = "DEBE_SELECCIONAR_CRITERIOS_OBLIGATORIOS";
	public static final String ERROR_VALIDACION_FECHAS = "ERROR_VALIDACION_FECHAS";
	public static final String ERROR_VALIDACION_FECHAS2 = "ERROR_VALIDACION_FECHAS";
	public static final String FECHA_CREACION = "Fecha Creación";
	public static final String FECHA_ENTREGA = "Fecha Estimada Entrega";
	public static final String FECHA_ENTREGA_INVALIDA = "FECHA_ENTREGA_INVALIDA";
	public static final String OPS_MAS_RECIENTE = "opsMas";
	public static final String VALOR = "valor";
	public static final String MENSAJE_CAMBIO_FECHA_SUCCESS = "MENSAJE_CAMBIO_FECHA_SUCCESS";
	public static final String MENSAJE_ERROR_PORCENTAJE_INVALIDO = "MENSAJE_ERROR_PORCENTAJE_INVALIDO";

	private ConstantesWeb() {
		/* clase utilizaría constructor privado para evitar instancias */
	}
}
