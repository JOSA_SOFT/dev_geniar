package com.sos.gestionautorizacionessaludweb.util;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;

/**
 * Esta clase se encarga guardar los datos del usuario en pantalla y tener el
 * metodo de cerrar sesion del aplicativo.
 *
 * @author Julian Marin - Geniar SAS
 * @since 26/11/2013
 * @version 1.0
 */
public class UsuarioBean extends BaseBacking<String> implements Serializable {
	private static final long serialVersionUID = -7147167527895140770L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(UsuarioBean.class);
	/** The user name. */
	private String userName;
	private List<ParametrosGeneralesVO> listaParametrosGenerales;

	@PostConstruct
	public void init() {
		//Metodo vacio
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.cna.esperados.base.BaseBacking#getUserName()
	 */
	@Override
	public String getUserName() {
		if (userName == null) {
			try {
				userName = getNombreUsuario();
			} catch (Exception e) {
				userName = "Guest";
				LOG.error(e);
			}
		}
		return userName;
	}

	/**
	 * Metodo encargado de eliminar el contenido de la sesion.
	 *
	 * @return regla de navegacion
	 * @author Jorge Andres Garcia Erazo - Geniar SAS
	 * @since 25/09/2012
	 */
	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			request.getSession().invalidate();
		} catch (Exception e) {
			LOG.error(e);
			ControladorExcepcionesLogic.validarExcepcion(e);
		}
		return ConstantesWeb.VALOR_LOGOUT;
	}

	/**
	 * Metodo encargado de eliminar el contenido de la sesion.
	 *
	 * @return regla de navegacion
	 * @author Jorge Andres Garcia Erazo - Geniar SAS
	 * @since 25/09/2012
	 */
	public String logoutBPM() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			request.getSession().invalidate();
		} catch (Exception e) {
			LOG.error(e);
			ControladorExcepcionesLogic.validarExcepcion(e);
		}
		return ConstantesWeb.VALOR_LOGOUT_BPM;
	}
	
	public List<ParametrosGeneralesVO> getListaParametrosGenerales() {
		try {
			CargarCombosController cargarCombosController = new CargarCombosController();
			if (listaParametrosGenerales == null || listaParametrosGenerales.isEmpty()) {
				listaParametrosGenerales = cargarCombosController.consultaParametrosGenerales();
			}
		} catch (LogicException e) {
			LOG.error(e);
		} catch (Exception e) {
			LOG.error(e);
		}
		return listaParametrosGenerales;
	}

}
