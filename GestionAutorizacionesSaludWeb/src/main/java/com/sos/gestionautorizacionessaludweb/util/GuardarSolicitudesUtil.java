package com.sos.gestionautorizacionessaludweb.util;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import co.com.sos.enterprise.inicioinstancia.v1.InicioInstanciaService;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.IniciarInstanciaVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OrigenAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PrioridadAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoServicioVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.AtencionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.EspecialidadVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.TipoIPSVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.IniciarInstanciaBpmService;
import com.sos.gestionautorizacionessaludejb.bean.impl.LiquidacionService;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.ips.modelo.IPSVO;
import com.sos.medico.modelo.MedicoVO;


/**
 * Class Utilitaria para la consulta de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * para los diagnosticos
 * @author Ing. Victor Hugo Gil
 * @version 29/07/2016
 *
 */
public class GuardarSolicitudesUtil {
	
	private static final Logger LOG = Logger.getLogger(GuardarSolicitudesUtil.class);
	private static IniciarInstanciaBpmService instanciaBpmService;
	private static LiquidacionService liquidacionService;
	
	private GuardarSolicitudesUtil(){
		/*clase utilizaría constructor privado para evitar instancias */	
	}
	
	public static IniciarInstanciaVO instanciarBPM(SolicitudVO solicitud) throws LogicException {
		IniciarInstanciaVO iniciarInstanciaVO = null;
		try {
			instanciaBpmService = new IniciarInstanciaBpmService();
			InicioInstanciaService inicioInstanciaService = instanciaBpmService.crearInicioInstanciaService();
			iniciarInstanciaVO = instanciaBpmService.resultadoInicioInstanciaService(instanciaBpmService.inicioInstanciaService(inicioInstanciaService, solicitud, FacesUtils.getUserName()), FacesUtils.getUserName());
			LOG.info(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SERVICIO_BPM_OK));

		} catch (Exception e) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SERVICIO_BPM), e, LogicException.ErrorType.DATO_NO_EXISTE);	
		}
		return iniciarInstanciaVO;
	}
	
	/**
	 * metodo que permite validar el objeto direccionamiento
	 * @throws LogicException
	 */
	public static boolean validarDireccionamientoSolicitud(DireccionamientoSolicitudVO direccionamientoSolicitudVO) {
		boolean aux = false;
		if(direccionamientoSolicitudVO != null && direccionamientoSolicitudVO.getServiceErrorVO() != null && direccionamientoSolicitudVO.getServiceErrorVO().getlAdvertenciaVO() != null && !direccionamientoSolicitudVO.getServiceErrorVO().getlAdvertenciaVO().isEmpty()){
			aux = true;
		}		
		return aux;
	}
	
	/**
	 * Metodo que permite validar las inconsistencias administrativas
	 * @throws LogicException
	 */
	public static void grabarSolicitudValInconsistencias(List<InconsistenciasResponseServiceVO> listaInconsistenciasAdministrativas) throws LogicException{
		for (InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO : listaInconsistenciasAdministrativas) {
			if (!inconsistenciasResponseServiceVO.isConfirmada() && !inconsistenciasResponseServiceVO.isResuelta()) {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_VALIDAR_INCONSISTENCIA) + inconsistenciasResponseServiceVO.getMensaje(), ErrorType.PARAMETRO_ERRADO);
			}
			if (inconsistenciasResponseServiceVO.isConfirmada() && inconsistenciasResponseServiceVO.isResuelta()) {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_VALOR_INCONSISTENCIA) + inconsistenciasResponseServiceVO.getMensaje(), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}
	
	/**
	 * Metodo que permite validar los adjuntos en la transcripcion de la solicitud
	 * @throws LogicException
	 */
	public static void grabarSolicitudValSoportesTranscripcion(List<SoporteVO> listaSoporteVOs)throws LogicException{
		if(listaSoporteVOs.isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_LISTA_SOPORTES), ErrorType.PARAMETRO_ERRADO);
		}else if(listaSoporteVOs.get(0).getData() == null){
			throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_LISTA_SOPORTES), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * Metodo que permite validar los datos obligatorios de IPS y Medicos
	 * @throws LogicException
	 */
	public static void grabarSolicitudValidarObligatorios(MedicoVO medicoVO, IPSVO ipsVO, List<PrestacionDTO> listaPrestacionesSolicitadas) throws LogicException{

		/**validaciones medico*/
		Utilidades.validarDatosObligatoriosInt(medicoVO.getConsecCodTipoId(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_TIPO_IDENTIFICACION_MEDICO));
		Utilidades.validarDatosObligatoriosString(medicoVO.getNumeroIdentificacion(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_NUMERO_IDENTIFICACION_MEDICO));
		Utilidades.validarDatosObligatoriosString(medicoVO.getPrimerNombre(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRIMER_NOMBRE_MEDICO));
		Utilidades.validarDatosObligatoriosString(medicoVO.getPrimerApellido(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRIMER_APELLIDO_MEDICO));
		Utilidades.validarDatosObligatoriosString(medicoVO.getDescripcionEspe(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESPECIALIDAD_MEDICO));

		/**validaciones ips*/
		Utilidades.validarDatosObligatoriosInt(ipsVO.getConsecCodigoTipoId(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_TIPO_IDENTIFICACION_PRESTADOR));
		Utilidades.validarDatosObligatoriosString(ipsVO.getNumeroIdentificacion(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_NUMERO_IDENTIFICACION_PRESTADOR));
		Utilidades.validarDatosObligatoriosString(ipsVO.getCodigoInterno(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CODIGO_INTERNO));
		Utilidades.validarDatosObligatoriosString(ipsVO.getRazonSocial(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_RAZON_SOCIAL));
		Utilidades.validarDatosObligatoriosString(ipsVO.getDescripcionCiudad(), Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CIUDAD));

		Utilidades.validarDatosObligatoriosList(listaPrestacionesSolicitadas, Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_PRESTACION));
	}
	
	/**
	 * Metodo que permite validar los datos de la hospitalizacion
	 * @throws LogicException
	 */
	public static void grabarSolicitudValHospitalizacion(List<PrestacionDTO> listaPrestacionesSolicitadas, boolean deshabilitarSelectTipoPrestacion, boolean deshabilitarBtnHospitalizacion, HospitalizacionVO hospitalizacionVO) throws LogicException{
		if (!deshabilitarBtnHospitalizacion && deshabilitarSelectTipoPrestacion && listaPrestacionesSolicitadas != null && listaPrestacionesSolicitadas.size() > ConstantesWeb.CANTIDAD_PROCEDIMIENTOS_HOSPITALIZACION) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_PROCEDIMIENTO_HOSPITALIZACION), ErrorType.PARAMETRO_ERRADO);
		}
		if (!deshabilitarBtnHospitalizacion && deshabilitarSelectTipoPrestacion && (validarHospitalizacionConsecutivos(hospitalizacionVO) || validarCamposHospitalizacion(hospitalizacionVO))) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_HOSPITALIZACION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		if (!deshabilitarBtnHospitalizacion && deshabilitarSelectTipoPrestacion && listaPrestacionesSolicitadas != null && !listaPrestacionesSolicitadas.isEmpty()) {
			for (PrestacionDTO prestacionDTO : listaPrestacionesSolicitadas) {
				if (prestacionDTO.getProcedimientosVO() != null && ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO.equalsIgnoreCase(prestacionDTO.getProcedimientosVO().getEstanciaProcedimiento())) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_HOSPITALIZACION_ESTANCIA), ErrorType.PARAMETRO_ERRADO);
				}
				if (prestacionDTO.getMedicamentosVO() != null) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_HOSPITALIZACION_ESTANCIA), ErrorType.PARAMETRO_ERRADO);
				}
			}
		}
	}
	
	/**
	 * Metodo que permite validar el consecutivo de hospitalizacion
	 * @throws LogicException
	 */
	public static boolean validarHospitalizacionConsecutivos(HospitalizacionVO hospitalizacionVO){
		if(hospitalizacionVO.getServicioHospitalizacionVO().getConsecutivoSevicioHospitalizacion() == null || hospitalizacionVO.getTipoHabitacionVO().getConsecutivoClaseHabitacion() == null){
			return true;
		}

		return false;
	}
	
	/**
	 * Metodo que permite validar los campos de hospitalizacion
	 * @throws LogicException
	 */

	public static boolean validarCamposHospitalizacion(HospitalizacionVO hospitalizacionVO){
		if(hospitalizacionVO.getFechaIngreso() == null || hospitalizacionVO.getFechaEgreso() == null){
			return true;
		}

		return false;
	}
	
	
	/**
	 * Metodo que permite validar los campos de diagnostico
	 * @throws LogicException
	 */
	public static void grabarSolicitudValidarDiagnosticos(List<DiagnosticoDTO> listaDiagnosticosSeleccionados) throws LogicException{

		Utilidades.validarDatosObligatoriosList(listaDiagnosticosSeleccionados, Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_DIAGNOSTICO_PRINCIPAL));

		if (listaDiagnosticosSeleccionados != null && !listaDiagnosticosSeleccionados.isEmpty()) {
			boolean encontroPrincipal = false;
			for (DiagnosticoDTO diagnosticoDTO : listaDiagnosticosSeleccionados) {
				if (diagnosticoDTO.getTiposDiagnosticosVO().getConsecutivoTipoDiagnostico().compareTo(ConstantesWeb.DIAGNOSTICO_PRINCIPAL) == 0) {
					encontroPrincipal = true;
					break;
				}
			}
			if (!encontroPrincipal) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_DIAGNOSTICO_PRINCIPAL), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}	
	
	/**
	 * metodo que ejecuta el servicio de liquidacion
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static LiquidacionSolicitudVO ejecutarLiquidacion(SolicitudVO solicitud) throws LogicException {
		LiquidacionSolicitudVO liquidacionSolicitudVO = null;
		liquidacionService = new LiquidacionService();
		co.com.sos.liquidacion.v1.LiquidacionService liquidacionServiceExc;
		try {
			liquidacionServiceExc = liquidacionService.crearLiquidacionService();
			liquidacionSolicitudVO = liquidacionService.consultaResultadoLiquidacion(liquidacionService.obtenerLiquidacionService(liquidacionServiceExc, solicitud, FacesUtils.getUserName()));
		} catch (ServiceException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (LogicException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		return liquidacionSolicitudVO;
	}
	
	/**
	 * Metodo permite adicionar el medico para guardar la solicitud
	 * @return
	 * @throws LogicException 
	 */
	public static com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO grabarSolicitudMedico(com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO medicoModelo , TiposIdentificacionVO tiposIdentificacionVO, MedicoVO medicoVO) {
		com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO medicoVOModelo = medicoModelo;
		
		tiposIdentificacionVO.setCodigoTipoIdentificacion(medicoVO.getCodigoTipoId());
		tiposIdentificacionVO.setConsecutivoTipoIdentificacion((int) medicoVO.getConsecCodTipoId());
		medicoVOModelo.setTiposIdentificacionMedicoVO(tiposIdentificacionVO);

		EspecialidadVO especialidadVO = new EspecialidadVO();
		especialidadVO.setConsecutivoCodigoEspecialidad((int) medicoVO.getConsecCodTipoEspe());
		especialidadVO.setDescripcionEspecialidad(medicoVO.getDescripcionEspe());

		medicoVOModelo.setEspecialidadMedicoVO(especialidadVO);
		medicoVOModelo.setNumeroIdentificacionMedico(medicoVO.getNumeroIdentificacion());
		medicoVOModelo.setNumeroUnicoIdentificacionMedico(Long.toString(medicoVO.getNumeroUnicoIdMedico()));
		medicoVOModelo.setRegistroMedico(medicoVO.getRegistroMedico());
		medicoVOModelo.setPrimerNombreMedico(medicoVO.getPrimerNombre());
		medicoVOModelo.setSegundoNombreMedico(medicoVO.getSegundoNombre());
		medicoVOModelo.setPrimerApellidoMedico(medicoVO.getPrimerApellido());
		medicoVOModelo.setSegundoApellidoMedico(medicoVO.getSegundoApellido());		
		
		return medicoVOModelo;
	}
	
	/**
	 * Metodo permite adicionar el medico para guardar la solicitud
	 * @return
	 * @throws ServiceException 
	 * @throws LogicException 
	 */
	public static void validarRespuestaGuardarSolicitud(SolicitudVO solicitud) throws ServiceException{
		if (solicitud != null && solicitud.getServiceErrorVO() != null) {
			throw new co.eps.sos.service.exception.ServiceException(solicitud.getServiceErrorVO().getEstadoTerminacion() + ConstantesWeb.SEPARADOR_CADENA + solicitud.getServiceErrorVO().getCodigoError() + ConstantesWeb.CADENA_EN_BLANCO + solicitud.getServiceErrorVO().getMensajeError());
		}
	}
	
	/**
	 * Metodo permite adicionar el prestado para guardar la solicitud
	 * @param prestadorVO
	 */
	public static PrestadorVO grabarSolicitudValPrestador(PrestadorVO prestadorVO, IPSVO ipsVO, Boolean prestadorAdscrito, CiudadVO ciudadVO){
		
		PrestadorVO prestador = prestadorVO;
		
		TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
		tiposIdentificacionVO.setCodigoTipoIdentificacion(ipsVO.getCodigoTipoId());
		tiposIdentificacionVO.setConsecutivoTipoIdentificacion((int) ipsVO.getConsecCodigoTipoId());
		
		prestador.setTiposIdentificacionPrestadorVO(tiposIdentificacionVO);
		prestador.setNumeroIdentificacionPrestador(ipsVO.getNumeroIdentificacion());
		prestador.setCodigoInterno(ipsVO.getCodigoInterno());
		prestador.setRazonSocial(ipsVO.getRazonSocial());
		prestador.setPrestadorAdscrito(prestadorAdscrito ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
		prestador.setNombreSucursal(ipsVO.getSucursal());
		prestador.setCiudadVO(ciudadVO);

		TipoIPSVO tipoIPSVO = new TipoIPSVO();
		tipoIPSVO.setCodigoTipoIps(ipsVO.getCodigoTipoId());
		tipoIPSVO.setConsecutivoCodigoTipoIps((int) ipsVO.getConsecCodigoTipoIps());
		
		prestador.setTipoIPSVO(tipoIPSVO);
		prestador.setDireccionPrestador(ipsVO.getDireccion());
		prestador.setNumeroTelefonicoPrestador(ipsVO.getTelefono());
		
		return prestador;
	}
	
	
	/**
	 * metodo que permite crear el objeto de la atencion
	 */	
	
	public static AtencionVO crearAtencionVO(){
		AtencionVO atencionVO = new AtencionVO();		
		atencionVO.setClaseAtencionVO(new ClaseAtencionVO());
		atencionVO.setTipoServicioVO(new TipoServicioVO());
		atencionVO.setPrioridadAtencionVO(new PrioridadAtencionVO());
		atencionVO.setOrigenAtencionVO(new OrigenAtencionVO());
		atencionVO.setUbicacionPacienteVO(new UbicacionPacienteVO());			
		atencionVO.setFechaSolicitudProfesional(Calendar.getInstance().getTime());
		
		return atencionVO;
	}
	
	
	/**
	 * metodo que permite crear el objeto de la hospitalizacion
	 */	
	public static HospitalizacionVO crearHospitalizacionVO(){
		HospitalizacionVO hospitalizacionVO = new HospitalizacionVO();
		hospitalizacionVO.setServicioHospitalizacionVO(new ServicioHospitalizacionVO());
		hospitalizacionVO.setTipoHabitacionVO(new ClaseHabitacionVO());
		hospitalizacionVO.setCorteCuenta(ConstantesWeb.CORTE_CUENTA_INI);
		return hospitalizacionVO;
	}

}
