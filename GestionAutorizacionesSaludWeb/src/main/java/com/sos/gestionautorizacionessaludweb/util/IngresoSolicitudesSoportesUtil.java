package com.sos.gestionautorizacionessaludweb.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class Utilitaria para la consulta de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * para los soportes
 * @author Ing. Victor Hugo Gil
 * @version 01/08/2016
 *
 */
public class IngresoSolicitudesSoportesUtil {
	
	private IngresoSolicitudesSoportesUtil(){
		/*clase utilizaría constructor privado para evitar instancias */	
	}
	
	
	/**
	 * Validacion de tamano del archivo
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static void validacionTamanoArchivo(SoporteVO soporteVO) throws LogicException {

		if (soporteVO.getDocumentoSoporteVO() != null && soporteVO.getDocumentoSoporteVO().getTamanoMaximo() != null && soporteVO.getTamanoArchivo() != null) {
			Integer tmnMaximo = Integer.parseInt(soporteVO.getDocumentoSoporteVO().getTamanoMaximo());
			if (tmnMaximo.compareTo(soporteVO.getTamanoArchivo()) < 0) {
				BigDecimal valorMaximoEnMb = new BigDecimal(Double.valueOf(soporteVO.getDocumentoSoporteVO().getTamanoMaximo()) / ConstantesWeb.TAMANO_ARCHIVO);
				valorMaximoEnMb = valorMaximoEnMb.setScale(ConstantesWeb.ESCALA, RoundingMode.HALF_UP);
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_TAMANO_DOCUMENTOS_SOPORTE) + valorMaximoEnMb + ConstantesWeb.TAMANO_DOCUMENTOS_SOPORTE,
						ErrorType.PARAMETRO_ERRADO);
			}
		}
	}
	
	/**
	 * Validacion de archivos repetidos
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static void validacionArchivosRepetidos(List<SoporteVO> listaSoporteVOs, SoporteVO soporteVO) throws LogicException {
		int contador = 0;
		List<SoporteVO> listaSoporte = listaSoporteVOs;
		SoporteVO soporte = soporteVO;
		
		for (SoporteVO soporteVORev : listaSoporte) {
			if (soporteVORev.getNombreSoporte() != null && soporte.getNombreSoporte() != null && soporteVORev.getNombreSoporte().equalsIgnoreCase(soporte.getNombreSoporte())) {
				contador++;
			}
			if (contador > 1) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_DOCUMENTOS_SOPORTE) + soporte.getNombreSoporte(), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}
	
	/**
	 * Validacion de archivos repetidos
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static boolean buscarArchivosRepetidos(List<SoporteVO> listaSoporteVOs, SoporteVO soporteSeleccionado) {
		boolean encontroVarios = false;
		int contador = 0;
		
		List<SoporteVO> listaSoporte = listaSoporteVOs;
		SoporteVO soporte = soporteSeleccionado;
		
		for (SoporteVO soporteVO : listaSoporte) {
			if (soporte.getConsecutivoCodigoDocumentoSoporte().compareTo(soporteVO.getDocumentoSoporteVO().getConsecutivoDocumento()) == 0) {
				contador++;
				if (contador > ConstantesWeb.CANTIDAD_ARCHIVOS_REPETIDOS) {
					encontroVarios = true;
					break;
				}
			}
		}
		return encontroVarios;
	}
	
	/**
	 * Validacion de maxima cantidad de archivos
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static void validarMaximoDocuementos(Integer maxDocumentos) throws LogicException{
		if (maxDocumentos <= 1) {
			throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ARCHIVOS_MAXIMO_TOPE), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * metodo que permite agrupar los documentos soporte
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static List<DocumentoSoporteVO> agruparDocumentosSoporte(List<DocumentoSoporteVO> listaDocumentoSoporte) throws LogicException {
		Long tempConsecutivoDocumento = null;
		List<DocumentoSoporteVO> listaDocumento = listaDocumentoSoporte;
		List<DocumentoSoporteVO> listTemp = new ArrayList<DocumentoSoporteVO>();
		
		for (DocumentoSoporteVO documentoSoporteVO : listaDocumento) {
			if (tempConsecutivoDocumento == null) {
				listTemp.add(documentoSoporteVO);
			}
			if (tempConsecutivoDocumento != null && tempConsecutivoDocumento.compareTo(documentoSoporteVO.getConsecutivoDocumento()) != 0) {
				listTemp.add(documentoSoporteVO);
			}
			tempConsecutivoDocumento = documentoSoporteVO.getConsecutivoDocumento();
		}
		return listTemp;
	}
	
	/**
	 * metodo que permite crear la lista de soportes
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static List<SoporteVO> crearListaSoportes(List<DocumentoSoporteVO> listaDocumentoSoporte, List<SoporteVO> listaSoporte) throws LogicException {
		boolean encontro;
		List<SoporteVO> listaTempRemove = new ArrayList<SoporteVO>();
		for (SoporteVO soporteVO : listaSoporte) {
			encontro = false;
			for (DocumentoSoporteVO documentoSoporteVO : listaDocumentoSoporte) {
				if (soporteVO.getConsecutivoCodigoDocumentoSoporte().compareTo(documentoSoporteVO.getConsecutivoDocumento()) == 0) {
					encontro = true;
					break;
				}
			}
			if (!encontro) {
				listaTempRemove.add(soporteVO);
			}
		}

		listaSoporte.removeAll(listaTempRemove);

		for (DocumentoSoporteVO documentoSoporteVO : listaDocumentoSoporte) {
			encontro = false;
			for (SoporteVO soporteVO : listaSoporte) {
				if (soporteVO.getConsecutivoCodigoDocumentoSoporte().compareTo(documentoSoporteVO.getConsecutivoDocumento()) == 0) {
					encontro = true;
					break;
				}
			}
			if (!encontro) {
				SoporteVO soporteVO = new SoporteVO();
				soporteVO.setRutaFisica(ConstantesWeb.CADENA_VACIA);
				soporteVO.setConsecutivoCodigoDocumentoSoporte(documentoSoporteVO.getConsecutivoDocumento());
				soporteVO.setDescripcionDocumentoSoporte(documentoSoporteVO.getNombreDocumento());
				soporteVO.setDocumentoSoporteVO(documentoSoporteVO);
				soporteVO.setSubidoAVisos(false);
				listaSoporte.add(soporteVO);
			}
		}
		return listaSoporte;
	}

}
