package com.sos.gestionautorizacionessaludweb.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConsultaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoFormatosVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.GeneracionAutorizacionServicioController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.bean.ConsultaAfiliadoBean;
import com.sos.gestionautorizacionessaludweb.bean.FechaEntregaBean;
import com.sos.ips.modelo.IPSVO;
import com.sos.util.jsf.FacesUtil;

/**
 * Class Utilitaria para la consulta de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre la consulta de solicitudes
 * @author Ing. Victor Hugo Gil
 * @version 25/07/2016
 *
 */
public class ConsultaSolicitudesUtil {
	
	private static final String CONSULTA_AFILIADO_BEAN = "consultaAfiliadoBean";
	
	private ConsultaSolicitudesUtil(){
		/*clase utilizaría constructor privado para evitar instancias */		
	}	
		
	/**
	 * Validacion de campos obligatorios para el cambio de direccionamiento
	 * @throws LogicException
	 */
	public static void validarObligatoriosCambioDireccionamientoOPS(PrestadorVO prestadorOPS, String codInternoPrestadorNuevo, String obsDireccionamientoOPS, Integer causalDireccionamientoOPS) throws LogicException{
		if (prestadorOPS != null && prestadorOPS.getCodigoInterno() != null && prestadorOPS.getCodigoInterno().equals(codInternoPrestadorNuevo)) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MISMO_PRETADOR_DIRECCIONAMIENTO), ErrorType.PARAMETRO_ERRADO);
		}

		if(!Utilidades.validarCamposVacios(codInternoPrestadorNuevo) || causalDireccionamientoOPS == null || !Utilidades.validarCamposVacios(obsDireccionamientoOPS)){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
		}

		if (Utilidades.validarObservaciones(obsDireccionamientoOPS)) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_LONGITUD_MIN_MAX_OBSERVACIONES), ErrorType.PARAMETRO_ERRADO);
		}
	}
	

	/**
	 * Validacion de campos obligatorios para la gestion Autorizacion Prestacion Negada
	 * @throws LogicException
	 */
	public static void validarObligatoriosGestionAutorizacionPrestacionNegada(String numeroAutorizacionPrestacionIPS, String codInternoPrestador, String obsDireccionamientoAutorizacion, Integer causalAutorizacionPrestacion, boolean habilitaGestionNumeroAutorizacionIPS) throws LogicException{
		
		if((habilitaGestionNumeroAutorizacionIPS==false) && (!Utilidades.validarExpresionRegular(numeroAutorizacionPrestacionIPS, 
															 ConstantesWeb.EXPRESION_REGULAR_NUMERO_AUTORIZACION_IPS))){
			
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_NUMEROIPS_FORMATO_INVALIDO), ErrorType.PARAMETRO_ERRADO);
		}
		
		if(!Utilidades.validarCamposVacios(codInternoPrestador) || causalAutorizacionPrestacion == null || !Utilidades.validarCamposVacios(obsDireccionamientoAutorizacion)){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
		}


		if (Utilidades.validarObservaciones(obsDireccionamientoAutorizacion)) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_LONGITUD_MIN_MAX_OBSERVACIONES), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	
	/**
	 * Validacion de campos obligatorios para el cambio de fecha de entrega
	 * @throws LogicException
	 */
	public static void validarObligatoriosModFechaEntrega(String obsFechaEntrega, Integer causalFechaEntrega) throws LogicException{

			if(causalFechaEntrega == null || !Utilidades.validarCamposVacios(obsFechaEntrega)){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
			}

			if (Utilidades.validarObservaciones(obsFechaEntrega)) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_LONGITUD_MIN_MAX_OBSERVACIONES), ErrorType.PARAMETRO_ERRADO);
			}
	
	}
	/**
	 * Validacion todas las fechas Vacias cambio de fecha de entrega
	 * @throws LogicException
	 */
	public static void validarVacioModFechaEntrega(List<PrestacionDTO> prestaciones) throws LogicException{

		boolean vacio=true;	
		for (PrestacionDTO dto : prestaciones) {
			if(dto.getFechaEntregaModificada()!=null|| Utilidades.validarCamposVacios(dto.getFechaEntregaModificada())){
				vacio=false;
				break;
			}
		}	
		if(vacio){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_FECHA_MOD_NULA), ErrorType.PARAMETRO_ERRADO);
		}
	}
	/**
	 * Validacion de fecha No es valida para el cambio de fecha de entrega
	 * @throws LogicException
	 */	
	public static void validarModFechaEntregaInvalida(List<PrestacionDTO> prestaciones, Date fechaActual) throws LogicException{
		for (PrestacionDTO dto : prestaciones) {
			
			if(dto.getFechaEntregaModificada()!=null|| Utilidades.validarCamposVacios(dto.getFechaEntregaModificada())){
				if(Utilidades.esfechaMenorActual(fechaActual, dto.getFechaEntregaModificada())){
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_FECHA_INVALIDA), ErrorType.PARAMETRO_ERRADO);
				}
				if(dto.getFechaEstimadaEntrega()!=null|| Utilidades.validarCamposVacios(dto.getFechaEstimadaEntrega())){
					if(Utilidades.esfechaIgualActual(dto.getFechaEstimadaEntrega(), dto.getFechaEntregaModificada())){
						throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_FECHA_INVALIDA), ErrorType.PARAMETRO_ERRADO);
					}
				}
			}		
		}	
	}	
	/**
	 * Validacion si fecha supera parámetro del sistema para el cambio de fecha de entrega
	 * @throws LogicException
	 */
	public static boolean isModFechaEntregaMayorParam(List<PrestacionDTO> prestaciones, Date fechaActual) throws LogicException{
		boolean flag=false;
		int paramFecha;
		try{
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			paramFecha=consultarSolicitudController.consultaParametroFechaMayorActual();

			for (PrestacionDTO dto : prestaciones) {
				if(dto.getFechaEntregaModificada()!=null|| Utilidades.validarCamposVacios(dto.getFechaEntregaModificada())){
					if(Utilidades.esfechaMayorNMeses(fechaActual, dto.getFechaEntregaModificada(),paramFecha)){
						flag=true;
					}
				}	
			}	
		} catch (ConnectionProviderException e) {
			throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_CONSULTA_PARAMETRO_FECHA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		} 
		return flag;	
	}		
	/**
	 * Validacion fecha de entrega modificada igual a Actual
	 * @throws LogicException
	 */	
	public static boolean isFechaModEntregaActual(List<PrestacionDTO> prestaciones, Date fechaActual) throws LogicException{	
		boolean flag=false;

		for (PrestacionDTO dto : prestaciones) {
			if(dto.getFechaEntregaModificada()!=null|| Utilidades.validarCamposVacios(dto.getFechaEntregaModificada())){
				if(Utilidades.esfechaIgualActual(fechaActual, dto.getFechaEntregaModificada())){
					flag=true;
					break;
				}
			}
		}		
		return flag;	
	}	
	
	public static void liquidarPrestacionModFechaEntrega(Integer numSolicitud,String numRadicado) throws LogicException{
		SolicitudVO solicitud= new SolicitudVO();
		ServiceErrorVO srvError= null;
		solicitud.setConsecutivoSolicitud(numSolicitud);
		solicitud.setNumeroSolicitudSOS(numRadicado);
		LiquidacionSolicitudVO liquidacionSolicitudVO = null;

		liquidacionSolicitudVO = GuardarSolicitudesUtil.ejecutarLiquidacion(solicitud);
 
		if (liquidacionSolicitudVO!=null) {
			GeneracionAutorizacionServicioController generacionAutorizacionServicio = new GeneracionAutorizacionServicioController();		
			srvError=generacionAutorizacionServicio.generarNumeroUnicoAutorizacionServicio(numSolicitud, FacesUtils.getUserName());	
			if(!FuncionesAppWeb.validateServiceError(srvError)){
				throw new LogicException(srvError.getMensajeError(), ErrorType.PARAMETRO_ERRADO);			
			}
		} 	
	}	
	
	/**
	 * Validacion de campos causas anulacion
	 * @throws LogicException
	 */
	public static void validacionCausalAnulacionOPS(Integer causalAnulacionOPS, String obsAnulacionOPS, ConceptosGastoVO conceptosGastoVOSel) throws LogicException {
		if(causalAnulacionOPS == null || obsAnulacionOPS == null || conceptosGastoVOSel.getNumeroUnicoAutorizacionOps() == null){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
		}

		validacionTamanoCampoObservacion(obsAnulacionOPS);
	}
	
	public static void validacionCausalAnulacionSolicitud(Integer causalAnulacionSolicitud, String obsAnulacionSolicitud) throws LogicException {
		if(causalAnulacionSolicitud == null || obsAnulacionSolicitud == null || obsAnulacionSolicitud.trim().equals(ConstantesWeb.CADENA_VACIA)){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
		}
		validacionTamanoCampoObservacion(obsAnulacionSolicitud);		
	}
	
	/**
	 * Valida el tamaño de la observacion
	 */
	private static void validacionTamanoCampoObservacion(String obsAnulacion) throws LogicException {
		
		if (obsAnulacion.length() < ConstantesWeb.VALOR_MINIMO_OBSERVACIONES || obsAnulacion.length() > ConstantesWeb.VALOR_MAXIMO_OBSERVACIONES) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_LONGITUD_MIN_MAX_OBSERVACIONES), ErrorType.PARAMETRO_ERRADO);
		}		
	}
	
	/**
	 * Metodo que permite extraer el numero de identificacion del afiliado del ojeto bean
	 * @param 
	 * @throws LogicException
	 */
	public static ConsultaSolicitudVO validarConsultarSolicitudes(String opcionBusquedaSeleccionada, ConsultaSolicitudVO consultaSolicitudVO) throws LogicException{
		AfiliadoVO afiliadoVOTemp = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getAfiliadoVO();
		ConsultaSolicitudVO consultaSolicitud = consultaSolicitudVO;
		
		
		if (afiliadoVOTemp != null && afiliadoVOTemp.getNumeroIdentificacion() != null) {
			consultaSolicitud.setNumeroUnicoIdentificacionAfiliado(afiliadoVOTemp.getNumeroUnicoIdentificacion());
			
		}
		if(opcionBusquedaSeleccionada == null){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);				
		}
		
		return consultaSolicitud;
	}
	
	/**
	 * metodo que permite la validacion de las fechas a utilizar en la consulta de las solicitudes
	 * @throws LogicException
	 */
	public static void validarFechas(Date fechaConsultaInicial, Date fechaConsultaFinal) throws LogicException {
		if (fechaConsultaInicial == null || fechaConsultaFinal == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		if (fechaConsultaFinal.compareTo(fechaConsultaInicial) < 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS_CONSULTA_SOLICITUD), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * metodo que permite la validacion de los campos para la consulta de las solicitudes por fecha
	 * @throws LogicException
	 */
	public static ConsultaSolicitudVO validarConsultaSolicitudesxFecha(Date fechaConsultaInicial, Date fechaConsultaFinal,  Integer consecutivoPlan, IPSVO ipsVO, Integer consecutivoPrestacion, ConsultaSolicitudVO consultaSolicitudVO) throws LogicException {
		ConsultaSolicitudVO consultaSolicitud = consultaSolicitudVO;
		ConsultaSolicitudesUtil.validarFechas(fechaConsultaInicial, fechaConsultaFinal);
		
		consultaSolicitud.setFechaDesde(fechaConsultaInicial);
		consultaSolicitud.setFechaHasta(fechaConsultaFinal);
		
		if(consecutivoPlan!=null){
			consultaSolicitud.setConsecutivoPlan(consecutivoPlan);
		}
		
		if(ipsVO != null && ipsVO.getConsecCodigoTipoId() > 0 && ipsVO.getNumeroIdentificacion() != null) {
			consultaSolicitud.setConsecutivoTipoIdentificacionPrestador((int)ipsVO.getConsecCodigoTipoId());
			consultaSolicitud.setNumeroIdentificacionPrestador(ipsVO.getNumeroIdentificacion());
		}
		
		if(consecutivoPrestacion != null) {
			consultaSolicitud.setConsecutivoServicioSolicitado(consecutivoPrestacion);
		}		
		
		return consultaSolicitud;
	}
	
	/**
 * Metod utilizado para liquidar las prestaciones autorizadas manualmente
 * @param idSolicitud
 * @param idRadicadoSolicitud
 * @return
 * @throws LogicException
 */
public static boolean liquidarPrestacionesPaFPocCapita(Integer idSolicitud,String idRadicadoSolicitud) throws LogicException{
	boolean resultadoProceso = false;
	ServiceErrorVO validadorError= null;
	SolicitudVO solicitudServicio= new SolicitudVO();
	solicitudServicio.setConsecutivoSolicitud(idSolicitud);
	solicitudServicio.setNumeroSolicitudSOS(idRadicadoSolicitud);
	LiquidacionSolicitudVO liquidacionSolicitudVO = null;
	
	FechaEntregaBean entregaBean = new FechaEntregaBean();
	entregaBean.ejecutarFechaEntrega(solicitudServicio);

	liquidacionSolicitudVO = GuardarSolicitudesUtil.ejecutarLiquidacion(solicitudServicio);
	
	if (liquidacionSolicitudVO!=null) {
		GeneracionAutorizacionServicioController generacionAutorizacionServicio = new GeneracionAutorizacionServicioController();		
		validadorError=generacionAutorizacionServicio.generarNumeroUnicoAutorizacionServicio(idSolicitud, FacesUtils.getUserName());	
		if(FuncionesAppWeb.validateServiceError(validadorError)){
			resultadoProceso = true;			
		}
	}
	return resultadoProceso;
}

	
	/**
	 * metodo que permite la validacion de los campos para la consulta de las solicitudes por numero de solicitud
	 * @throws LogicException
	 */
	public static ConsultaSolicitudVO validarConsultaSolicitudesxNumeroSolicitud(String numeroSolicitud, IPSVO ipsVO, ConsultaSolicitudVO consultaSolicitudVO) throws LogicException {
		ConsultaSolicitudVO consultaSolicitud = consultaSolicitudVO;
				
		if (numeroSolicitud == null || numeroSolicitud.isEmpty()) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		consultaSolicitud.setNumeroSolicitudProveedor(numeroSolicitud);
		
		if (ipsVO != null && ipsVO.getConsecCodigoTipoId() > 0 && ipsVO.getNumeroIdentificacion() != null) {
			consultaSolicitud.setConsecutivoTipoIdentificacionPrestador(Integer.parseInt(Long.toString(ipsVO.getConsecCodigoTipoId())));
			consultaSolicitud.setNumeroIdentificacionPrestador(ipsVO.getNumeroIdentificacion());
		}	
		
		return consultaSolicitud;
	}
	
	/**
	 * metodo que permite la validacion de los campos para la consulta de las solicitudes por numero de OPS
	 * @throws LogicException
	 */
	public static ConsultaSolicitudVO validarConsultaSolicitudesxOPS(String numeroAutorizacion, ConsultaSolicitudVO consultaSolicitudVO) throws LogicException {
		ConsultaSolicitudVO consultaSolicitud = consultaSolicitudVO;
				
		if (numeroAutorizacion == null || numeroAutorizacion.isEmpty()) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		consultaSolicitud.setNumeroAutorizacionOPS(Integer.parseInt(numeroAutorizacion));
		
		return consultaSolicitud;
	}
	
	/**
	 * metodo que permite la validacion de los campos para la consulta de las solicitudes por numero de radicado
	 * @throws LogicException
	 */
	public static ConsultaSolicitudVO validarConsultaSolicitudesxRadicado(String numeroRadicado, ConsultaSolicitudVO consultaSolicitudVO) throws LogicException {
		ConsultaSolicitudVO consultaSolicitud = consultaSolicitudVO;
				
		if (numeroRadicado == null || numeroRadicado.isEmpty()) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS), ErrorType.PARAMETRO_ERRADO);
		}
		consultaSolicitud.setNumeroRadicadoSolicitud(numeroRadicado);
		
		return consultaSolicitud;
	}
	
	/**
	 * metodo que permite la validacion del numero maximo de meses para descargar
	 * @throws LogicException
	 */
	public static Integer valorMaximoMesesDescarga(List<ParametrosGeneralesVO> listaParametrosGeneralesVOs) throws LogicException {
		Integer numMaximoMesesDescarga = null;
		for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGeneralesVOs) {
			if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == ConstantesWeb.PARAMETRO_GENERAL_GENERACION_FORMATO) {
				numMaximoMesesDescarga = parametrosGeneralesVO.getValorParametroGeneral().intValue();
			}
		}
		
		return numMaximoMesesDescarga;
	}	
	
	/**
	 * metodo que permite la validacion de la lista de parametros generales
	 * @throws LogicException
	 */
	
	public static List<ParametrosGeneralesVO> validarConsultaParametrosGenerales(List<ParametrosGeneralesVO> listaParametrosGeneralesVOs){
		
		List<ParametrosGeneralesVO> listaParametrosGenerales = listaParametrosGeneralesVOs;
		
		if (listaParametrosGenerales == null) {
			listaParametrosGenerales = new ArrayList<ParametrosGeneralesVO>();
		}
		
		return listaParametrosGenerales;
	}	
	
	/**
	 * metodo que permite la validacion del historico de descargas
	 * @throws LogicException
	 */
	
	public static void validarListaHistoricoDescargas(List<HistoricoFormatosVO> listaHistoricoFormatosVO) throws LogicException{
		if (listaHistoricoFormatosVO == null || listaHistoricoFormatosVO.isEmpty()) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICO_DESCARGA), LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Metodo que permite consultar los motivos causa
	 * @throws LogicException
	 */
	
	public static List<SelectItem> consultaMotivosCausa(int motivoCausa) throws LogicException{
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaItems = obtenerListaSelectMotivoscausas(consultarSolicitudController.consultarMotivosCausas(motivoCausa));			
		} catch (ConnectionProviderException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVO_CAUSA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (LogicException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVO_CAUSA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}		
		return listaItems;		
	}

	/**
	 * Metodo para convertir una lista de valores en una lista desplegable
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectMotivoscausas(List<MotivoCausaVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (MotivoCausaVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivoCodMotivoCausa(), vo.getDesMotivoCausa());
				listaItems.add(item);
			}
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar los motivos causa. Devuelve el objeto VO para validaciones de datos
	 * @throws LogicException
	 */
	
	public static List<MotivoCausaVO> obtenerMotivosCausa(int motivoCausa) throws LogicException {
		List<MotivoCausaVO> listaMotivosCausa = new ArrayList<MotivoCausaVO>();
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaMotivosCausa = consultarSolicitudController.consultarMotivosCausas(motivoCausa);			
		} catch (ConnectionProviderException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVO_CAUSA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (LogicException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVO_CAUSA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}		
		return listaMotivosCausa;		
	}	
	
	public static void validacionCausalAnulacionPrestacion(Integer causalAnulacion, String obsAnulacion, PrestacionDTO prestacionSel) throws LogicException {
		if(causalAnulacion == null || obsAnulacion == null || prestacionSel.getConsecutivoServicioSolicitado() == null){
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
		}

		validacionTamanoCampoObservacion(obsAnulacion);
	}
}
