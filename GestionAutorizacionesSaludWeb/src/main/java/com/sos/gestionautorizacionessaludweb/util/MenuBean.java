package com.sos.gestionautorizacionessaludweb.util;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

import co.com.sos.logic.vo.PrestadorVO;
import co.com.sos.logic.vo.UsuarioVO;
import co.com.sos.logic.voa.UsuarioVOA;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.ModuloVO;
import com.sos.gestionautorizacionessaluddata.model.ServicioVO;
import com.sos.gestionautorizacionessaludejb.controller.MenuController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.util.jsf.FacesUtil;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;

/**
 * Class MenuBean
 * 
 * @author Jerson Viveros
 * @version 1.0
 */
public class MenuBean implements Serializable{
	private static final long serialVersionUID = -3604978039031547472L;
	
	private static final Logger LOGGER = Logger.getLogger(UsuarioBean.class);

	/**
	 * La lista de procesos se usa para pintar todos los procesos a los que
	 * tiene acceso el usuario
	 */
	private List<ModuloVO> modulos;

	/** Opcion seleccionada por el usuario */
	private String opcion;
	
	private boolean usuarioIps;
	
	private PrestadorVO prestadorVO;

	/**
	 * Inicia el menu dinamico, consulta en base de datos las tablas
	 * TbModulos y tbServicios
	 */
	@PostConstruct
	public void init() {
		boolean coordinadorASI = false;
		MenuController mcmc = new MenuController();
		try {
			String usr = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario();
			setModulos(mcmc.getModulos(usr));
			for (ModuloVO moduloVO : modulos) {
				if (ConstantesWeb.PERFIL_COORDINADORA == moduloVO.getConsecutivoCodigoPerfil()){
					coordinadorASI = true;
				}
				for (ServicioVO servicioVO : moduloVO.getServicios()) {				
					servicioVO.setDeshabilitado(opcionInactiva(servicioVO.getAction().trim()));
				}
			}
			UsuarioVO usuarioConsultaVO = new UsuarioVO();
			usuarioConsultaVO.setLogin(FacesUtils.getUserName());

			UsuarioVOA usuarioVoa = new UsuarioVOA();
			UsuarioVO usuarioResultadoVO = usuarioVoa. consultarUsuario(usuarioConsultaVO);
				
			prestadorVO = usuarioResultadoVO.getPrestadorAsociado();
			
			usuarioIps = prestadorVO.getCodInternoPrestador()==null?false:true;
			
			FacesUtils.setSessionParameter(ConstantesWeb.ES_USUARIOS_IPS, usuarioIps);
			FacesUtils.setSessionParameter(ConstantesWeb.ES_COORDINADOR_ASI, coordinadorASI);

		} catch (LogicException e) {
			LOGGER.error(e.getMessage(), e);
			mostrarMensajes(e.getMessage(), "blue");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			mostrarMensajes(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	
	private boolean opcionInactiva(String opcion) {
		return ConstantesWeb.OPCION_INGRESAR_SOLICITUD.equals(opcion) 
				|| ConstantesWeb.OPCION_PROGRAMACION_ENTREGA.equals(opcion)
				|| ConstantesWeb.OPCION_CAMBIAR_DIRECCIONAMIENTO.equals(opcion)
				|| ConstantesWeb.OPCION_CAMBIAR_FECHA_ENTREGA.equals(opcion);
	}

	/**
	 * Permite realizar la navegacion del menu dinamico
	 * 
	 * @return regla de navegacion
	 */
	public String navegar() {
		return getOpcion();
	}

	/**
	 * Metodo encargado de mostrar los mensajes.
	 *
	 * @param mensaje
	 *            the mensaje
	 * @param color
	 *            the color
	 */
	private void mostrarMensajes(String mensaje, String color) {
		MessagesPopupBean messagesPopupBean = (MessagesPopupBean) FacesUtils.getManagedBean(ConstantesWeb.MESSAGES_POPUP_BEAN);
		messagesPopupBean.setColor(color);
		messagesPopupBean.showError(mensaje);
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setModulos(List<ModuloVO> modulos) {
		this.modulos = modulos;
	}

	public List<ModuloVO> getModulos() {
		return modulos;
	}

	public boolean isUsuarioIps() {
		return usuarioIps;
	}

	public void setUsuarioIps(boolean usuarioIps) {
		this.usuarioIps = usuarioIps;
	}

	public PrestadorVO getPrestadorVO() {
		return prestadorVO;
	}

	public void setPrestadorVO(PrestadorVO prestadorVO) {
		this.prestadorVO = prestadorVO;
	}

}
