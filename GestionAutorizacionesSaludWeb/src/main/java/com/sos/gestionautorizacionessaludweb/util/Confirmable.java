/*
 * @author Jorge Andres Garcia Erazo - Geniar S.A.S
 * @since 04/09/2013
 * @version 1.0
 */
package com.sos.gestionautorizacionessaludweb.util;

// TODO: Auto-generated Javadoc
/**
 * Interface Confirmable.
 *
 * @author sisddc01
 * @version 4/09/2012
 */
public interface Confirmable {

	/**
	 * Listener confirmation.
	 *
	 * @param ejecute the ejecute
	 * @return string
	 */
	String listenerConfirmation(boolean ejecute);
}
