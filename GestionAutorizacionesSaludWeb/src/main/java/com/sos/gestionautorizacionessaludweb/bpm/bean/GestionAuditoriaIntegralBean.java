package com.sos.gestionautorizacionessaludweb.bpm.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.AntecedentesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.GestionesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.HistoricoPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.UsuarioVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosNegacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.GestionAuditorDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.AtencionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionAuditoriaIntegralController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionContratacionController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionDomiciliariaController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionInconsistenciasController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionMedicinaTrabajoController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.PrestadorController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.SolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.gestionautorizacionessaludweb.bean.IngresoSolicitudBean;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesSoportesUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

/**
 * Class GestionAuditoriaIntegralBean
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionAuditoriaIntegralBean implements Serializable {
	private static final long serialVersionUID = 4830125360038567345L;
	/**
	 * Variable instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionAuditoriaIntegralBean.class);
	/**
	 * Variable gestion de mensajes
	 */
	

	/**
	 * Controller
	 */
	private static final String COLOR_ROJO = "red";

	/**
	 * VO's
	 */
	private PrestacionesAprobadasVO prestacionesAprobadasDTOTemp; 
	private PrestacionesAprobadasVO prestacionesAprobadasDTOCUMS;
	private DireccionamientoVO direccionamientoDTOCUMS;
	private DireccionamientoVO cambioFecha;
	private DireccionamientoVO cambioDireccionamiento;
	private MedicamentosVO medicamentoVOSeleccionado;
	private DiagnosticosVO diagnosticoVoSeleccionado;
	private SolicitudBpmVO solicitudVO;
	private AfiliadoVO afiliadoVO;
	private PrestacionesAprobadasVO prestacionNueva;
	private AlternativasVO alternativaSel;
	private DocumentoSoporteVO documentoSoporteVOSeleccionadoAuditoria;
	private HospitalizacionVO hospitalizacion; 
	private DocumentoSoporteVO documentoSoporteSeleccionadoGestion;
	private SoporteVO soporteSeleccionadoGestion;

	/**
	 * Listas desplegables
	 */
	private List<SelectItem> listaHallazgosCUMS;
	private List<SelectItem> listaEstadosCUMS;
	private List<SelectItem> lstMedicosAuditores;
	private List<UsuarioVO> lstMedicosAuditoresTemp;
	private List<SelectItem> lstRiesgos;
	private List<SelectItem> listaCausasCUMS;
	private List<SelectItem> listaFundamentosCUMS;
	private List<SelectItem> listaTiposPrestaciones;
	private List<SelectItem> listaViasAccesoMedicamento;
	private List<SelectItem> listaLateralidades;
	private List<SelectItem> listaDosisMedicamentos;
	private List<SelectItem> listaFrecuenciaMedicamentos;
	private List<SelectItem> lstMotivos;
	private List<SelectItem> lstMotivosPrestacion;
	private List<SelectItem> lstPrestadoresDireccionamiento;
	private List<SelectItem> lstGruposAuditores;
	private List<SelectItem> lstRecobros;
	private List<SelectItem> lstPrestacionesReasignar = new ArrayList<SelectItem>();

	/**
	 * Listas de valores
	 */
	private List<AlternativasVO> listaAlternativasCUMS;
	private List<EmpleadorVO> listaEmpleadores;
	private List<ConveniosCapitacionVO> listaConveniosCapacitacion;
	private List<DiagnosticosVO> listaDiagnosticosEncontrados;
	private List<DiagnosticoDTO> listaDiagnosticosSeleccionados;
	private List<GestionesPrestacionVO> lstGestionesPrestacion;
	private List<HistoricoPrestacionVO> lstHistoricosPrestacion;
	private List<AntecedentesPrestacionVO> lstAntecedentesPrestacion;
	private List<PrestacionesAprobadasVO> lstPrestacionesSolicitadas;
	private List<PrestacionesAprobadasVO> lstPrestaciones;
	private List<MedicamentosVO> listaMedicamentosEncontrados;
	private List<DocumentoSoporteVO> listaDocumentosAuditoria;
	private List<ProgramacionEntregaVO> lstProgramacionEntrega;
	private List<ConceptosGastoVO> lstConceptosGastos; 
	private List<ProcedimientosVO> listaCUPSEncontrados;
	private List<SoporteVO> listaSoporteGestionVOs = new ArrayList<SoporteVO>();
	private List<DocumentoSoporteVO> listaDocumentoSoporteGestionVOs = new ArrayList<DocumentoSoporteVO>();
	private ConveniosCapitacionVO convenioCapacitacionSeleccionado;
	private List<PrestacionDTO> listaPrestacionesSolicitadasGestion = new ArrayList<PrestacionDTO>();
	private List<CiudadVO> listaCiudadesDireccionamiento = new ArrayList<CiudadVO>();
	
	/**
	 * Variables del MB
	 */
	private Date fechaConsulta;
	private int TIPO_ESTADO_NEGOCIO;
	private String pestanaSeleccionada;
	private String txtCodigoDiagnostico;
	private String txtDescripcionDiagnostico;
	private boolean checkDiagnosticoPpal;
	private boolean checkDiagnosticoSOS;
	private DiagnosticoDTO diagnosticoDTOSeleccionado;
	private boolean mostrarPopupDiagnosticos;
	private boolean verPestañaCums; 
	private boolean archivosObligatoriosFaltantes = false;
	private String alternativa;
	private Integer consecutivoPrestacion;
	private String codigoPrestacion;
	private String descripcionPrestacion;
	private Integer tipoPrestacionSeleccionado;
	private boolean mostrarPopupMedicamentos;
	private boolean mostrarPopupAdicionarPrestacion;
	private boolean mostrarPopupDocumentosSoporteGestion;
	private boolean mostrarPopupConfirmacionDocumentosGestion = false;
	private boolean mostrarPopupVerDetalleSolicitud;
	private boolean mostrarPopupVerDetalleAfiliado;
	private boolean mostrarPopupHistoricoPrestaciones;
	private boolean mostrarPopupHospitalizacion;
	private boolean mostrarConfirmacionGuardar;
	private boolean causaRequerida;
	private boolean validacionEstadoAprobado;
	private boolean identificacionDirRequerido;
	private boolean codigoInternoRequerido;
	private boolean sucursalRequerido;
	private boolean fechaEsperadosRequerido;
	private boolean fundamentoLegalRequerido=false;
	private boolean auditorMedicoRequerido=false;
	private boolean alternativaRequerido=false;
	private boolean observacionOPSRequerida=false;
	private boolean mostrarPopupVerGestiones=false;
	private boolean mostrarPopupVerAntecedentes=false;
	private boolean mostrarPopupDocumentosSoporteAuditoria = false;
	private boolean mostrarPopupConfirmacionBorrarArchivoGestion = false;
	private boolean mostrarPopupReliquidar;
	private Integer prestacionesReasignar;
	private String observacionesReasignar;
	private Integer consecutivoPrestacionGlobal;
	private Integer idSolicitud;
	private String idTask;
	private String userBpm;
	private String tipoAuditor;
	private String descGrupoAuditor;
	private boolean mostrarPopupCambiarDireccionamiento;
	private boolean mostrarPopupCambiarFechaDireccionamiento;
	private String CUMS;
	private String MEDICAMENTOS;
	private String CUPS;
	private boolean mostrarHospitalizacion;
	private static final String PARAMETRO_CUPS = "consecutivoCups";
	private static final String PARAMETRO_CUMS = "consecutivoCums";
	private int consecutivoCUMS;
	private int consecutivoCUPS;
	private Integer maximoDocumentosGestion = 0;
	private boolean mostrarPopupCUPS = false;
	private ProcedimientosVO procedimientoVOSeleccionado;
	private String fechaSolicitud;
	private String altoRiesgo;
	
	/**
	 * DTO's
	 */
	private GestionAuditorDTO gestionAuditorDTO;
	private DatosNegacionDTO datosNegacionDTO;

	private ClienteRestBpmBean clienteRestBpmBean;
	private IngresoSolicitudBean ingresoSolicitudBean;
	
	
	private List<SelectItem> lstCausalesNoCobroCuota;
	private boolean noCobraCuotaRecuperacion;
	
	private CiudadVO ciudad;
	private boolean mostrarPopupCiudades;
	
	private boolean activarFiltroCiudad;

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {

		try{
			iniciarVariables();

			idTask					=(String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
			String idSolicitudStr	=(String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
			clienteRestBpmBean		=(ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
			tipoAuditor 			= (String) FacesUtil.getRequestParameter(ConstantesWeb.TIPO_AUDITOR);
			userBpm 				= (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);
			ingresoSolicitudBean	= (IngresoSolicitudBean) FacesUtil.getBean(ConstantesWeb.INGRESAR_SOLICITUD_BEAN);	
			
			
			if(tipoAuditor != null && tipoAuditor.equals(ConstantesWeb.ID_AUDITORIA)){
				this.idSolicitud = new Integer(idSolicitudStr);
				
				
				GestionInconsistenciasController gestionInconsistenciasController = new GestionInconsistenciasController(); 
				CargarCombosController cargarCombosController = new CargarCombosController();
				GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController(); 
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				GestionContratacionController gestionContratacionController = new GestionContratacionController();
				
				List<Object> afiliadoYSolicitud = gestionInconsistenciasController.consultaAfiliadoYSolicitud(idSolicitud);
				afiliadoVO = (AfiliadoVO) afiliadoYSolicitud.get(0); 
				solicitudVO = (SolicitudBpmVO) afiliadoYSolicitud.get(1);
				lstRiesgos = FuncionesAppWeb.obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());
	
				convenioCapacitacionSeleccionado = new ConveniosCapitacionVO();
				SimpleDateFormat formatoFecha = new SimpleDateFormat(ConstantesWeb.FORMATO_FECHA);
				fechaSolicitud = formatoFecha.format(solicitudVO.getFechaSolicitud());
				altoRiesgo=afiliadoVO.isAltoRiesgo()?ConstantesWeb.ALTO_RIESGO:"";
				
				listaDiagnosticosSeleccionados = gestionMedicinaTrabajoController.consultaDiagnosticos(idSolicitud);
				if (ConstantesWeb.CODIGO_ORIGEN_ATENCION_HOSPITALIZACION.equals(solicitudVO.getCodigoOrigenAtencion())) {
					mostrarHospitalizacion = true;
				}
	
				lstGruposAuditores = FuncionesAppWeb.obtenerListaSelectGruposAuditores(gestionDomiciliariaController.consultarGruposAuditores(idSolicitud, ConstantesWeb.CONS_AUDITORIA_INTEGRAL));
				cambioFecha = new DireccionamientoVO();
				cambioDireccionamiento = new DireccionamientoVO();
				gestionAuditorDTO = new GestionAuditorDTO();
				datosNegacionDTO = new DatosNegacionDTO();
				lstPrestacionesSolicitadas = gestionContratacionController.consultaPrestacionesAprobadas(idSolicitud, ConstantesWeb.CADENA_VACIA);
				actualizarListadoDocumentosSolicitud();
				actualizarDocumentosSoporteGestion();

				llenarListadoPrestacionesSolicitadas();
				obtenerCausalesNoCobroCuota();
				
			}

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void llenarListadoPrestacionesSolicitadas() {
		
		for( PrestacionesAprobadasVO vo : lstPrestacionesSolicitadas){
			SelectItem item = new SelectItem(vo.getConscodigoPrestacionGSA(), vo.getCodigoPrestacion() + ConstantesWeb.CADENA_EN_BLANCO + vo.getDescripcion());
			lstPrestacionesReasignar.add(item);
		}
		
	}

	public void verHospitalizacion(){
		try{
			ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
			hospitalizacion = consultarSolicitudControllerAuditoria.consultaDetalleSolicitud(
					solicitudVO.getNumeroRadicado()).get(0).getHospitalizacionVO();
			mostrarPopupHospitalizacion=true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void cerrarHospitalizacion(){
		mostrarPopupHospitalizacion=false;
	}

	public void verHistoricoPrestaciones(){
		try{
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			lstHistoricosPrestacion = consultaAfiliadoController.consultarHistoricosPrestacion(
					afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion(), afiliadoVO.getNumeroIdentificacion());
			mostrarPopupHistoricoPrestaciones=true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void limpiarRiesgo(){
		try{
			if(!afiliadoVO.isAltoRiesgo()){
				lstRiesgos = new ArrayList<SelectItem>();
				afiliadoVO.setConsecutivoCodRiesgo(0);
			}else{
				CargarCombosController cargarCombosController = new CargarCombosController();
				lstRiesgos = FuncionesAppWeb.obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void cerrarHistoricoPrestaciones(){
		mostrarPopupHistoricoPrestaciones=false;
	}

	public void verSolicitudDetallada(){
		try{
			SolicitudController solicitudController = new SolicitudController();
			mostrarPopupVerDetalleSolicitud=true;
			SolicitudBpmVO solicitudBpmVO = solicitudController.consultarDetalleSolicitudBPM(idSolicitud);
			solicitudVO.setMedicoSolicitante(solicitudBpmVO.getMedicoSolicitante());
			solicitudVO.setPrestadorSolicitante(solicitudBpmVO.getPrestadorSolicitante());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo cerrar popup  */
	public void cerrarPopupVerSolicitudDetallada(){
		mostrarPopupVerDetalleSolicitud=false;
	}

	/**  Metodo cerrar popup  */
	public void cerrarPopupDocumentosSoporte(){
		mostrarPopupDocumentosSoporteAuditoria=false;
	}

	/**  Metodo abrir popup  */
	public void verPopupSoportesAuditoria(){
		listaDocumentosAuditoria = consultaDocumentosSoportexSolicitud(solicitudVO);
		mostrarPopupDocumentosSoporteAuditoria = true;
	}
	
	/**
	 * Metodo utilizado para consultar el listado de los documentos soportes
	 * @param solicitudVO
	 * @return
	 */
	public List<DocumentoSoporteVO> consultaDocumentosSoportexSolicitud(SolicitudBpmVO solicitudVO){
		List<DocumentoSoporteVO> listaIdDocumentosAuditoria = new ArrayList<DocumentoSoporteVO>();
		try{
			ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
			ControladorVisosServiceEJB controladorVisosServiceEJBAuditoria = new ControladorVisosServiceEJB();
			listaIdDocumentosAuditoria = consultarSolicitudControllerAuditoria.consultaDocumentosSoportexSolicitud(solicitudVO.getNumeroSolicitud());
			controladorVisosServiceEJBAuditoria.crearConexionVisosService();
			listaDocumentosAuditoria = controladorVisosServiceEJBAuditoria.consultarDocumentoAnexoxIdDocumento(listaIdDocumentosAuditoria.get(0).getConsecutivoDocumentoSoporteVisos());
			for(int i=0;i<listaDocumentosAuditoria.size();i ++){
				listaDocumentosAuditoria.get(i).setDescripcionTipoDocumentoSoporte(listaIdDocumentosAuditoria.get(i).getDescripcionTipoDocumentoSoporte());
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		
		return listaDocumentosAuditoria;
	}

	/**  Metodo Descargar documentos soportes  */
	public void descargarSoporteAuditoria(){
		try {
			FuncionesAppWeb.descargarSoporteAuditoria(documentoSoporteVOSeleccionadoAuditoria.getNombreDocumento(), documentoSoporteVOSeleccionadoAuditoria.getDatosArchivoSoporte());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void validacionEstadoAprobado() throws LogicException{
		
		observacionOPSRequerida = gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.CONS_ESTADO_APROBADO ?true:false;
		if(gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.CONS_ESTADO_APROBADO)
		{
			identificacionDirRequerido=true;
			codigoInternoRequerido=true;
			sucursalRequerido=true;
			fechaEsperadosRequerido=true;
			causaRequerida=false;
		}else{
			identificacionDirRequerido=false;
			codigoInternoRequerido=false;
			sucursalRequerido=false;
			fechaEsperadosRequerido=false;
			causaRequerida=true;
		}
	}
	
	
	/**  Metodo Cargar info de las causas de la malla  */
	public void cambiarCausas(){
		try {
			validacionEstadoAprobado();
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.CONS_ESTADO_NO_AUTORIZADO){
				fundamentoLegalRequerido =  true;
				auditorMedicoRequerido = true;
				alternativaRequerido = true;
			}else{
				fundamentoLegalRequerido =  false;
				auditorMedicoRequerido = false;
				alternativaRequerido = false;
			}
			gestionAuditorDTO.setJustificacion(null);
			consecutivoPrestacionGlobal = Integer.parseInt(prestacionesAprobadasDTOTemp.getConscodigoPrestacionGSA().trim());
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != ConstantesWeb.CODIGO_ESTADO_AUDITORIA ){
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				listaCausasCUMS = FuncionesAppWeb.obtenerListaSelectCausas(gestionDomiciliariaController.consultaCausas(gestionAuditorDTO.getConsecutivoEstadoPrestacion(), consecutivoPrestacionGlobal));
			}else{
				listaCausasCUMS = new ArrayList<SelectItem>();
				causaRequerida = false;
			}
 		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	/**  Metodo para cerrar Poppup  */
	public void cerrarDetalleAfiliado() {
		mostrarPopupVerDetalleAfiliado=false;
	}

	/**  Cargar detalle del afiliado  */
	public void cargarDetalleAfiliado() {
		try{
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController.consultaRespuestaServicioAfiliado(
					afiliadoVO.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion(),
					afiliadoVO.getNumeroIdentificacion(), afiliadoVO.getPlan().getDescripcionPlan(),
					fechaConsulta, FacesUtils.getUserName());

			if (resultadoConsultaAfiliadoDatosAdicionales != null && resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() != 0) {

				afiliadoVO = FuncionesAppWeb.datosAdicionalesAfiliadoToAfiliadoVO(resultadoConsultaAfiliadoDatosAdicionales.getAfiliado(), afiliadoVO);
				listaEmpleadores = consultaAfiliadoController.consultaInformacionEmpleadorAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
				listaConveniosCapacitacion = consultaAfiliadoController.consultaConveniosCapitacion(resultadoConsultaAfiliadoDatosAdicionales);
			} else if (resultadoConsultaAfiliadoDatosAdicionales != null && resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() == 0) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_AFILIADO_NO_EXISTE"), ErrorType.PARAMETRO_ERRADO);
			}
			mostrarPopupVerDetalleAfiliado=true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}


	/**  Metodo para Inicializar variables  */
	private void iniciarVariables() {
		try{

			consecutivoCUMS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUMS));
			consecutivoCUPS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUPS));

			listaEmpleadores 				= new ArrayList<EmpleadorVO>();
			listaConveniosCapacitacion	 	= new ArrayList<ConveniosCapitacionVO>();
			lstProgramacionEntrega 			= new ArrayList<ProgramacionEntregaVO>();
			listaMedicamentosEncontrados 	= new ArrayList<MedicamentosVO>();
			listaDocumentosAuditoria 		= new ArrayList<DocumentoSoporteVO>();
			listaDiagnosticosEncontrados 	= new ArrayList<DiagnosticosVO>();
			listaDiagnosticosSeleccionados 	= new ArrayList<DiagnosticoDTO>();
			lstGestionesPrestacion 			= new ArrayList<GestionesPrestacionVO>();	
			listaAlternativasCUMS			= new ArrayList<AlternativasVO>();
			lstHistoricosPrestacion 		= new ArrayList<HistoricoPrestacionVO>();
			lstAntecedentesPrestacion		= new ArrayList<AntecedentesPrestacionVO>();
			lstPrestacionesSolicitadas 		= new ArrayList<PrestacionesAprobadasVO>();
			lstPrestaciones					= new ArrayList<PrestacionesAprobadasVO>();
			lstConceptosGastos				= new ArrayList<ConceptosGastoVO>();
			listaCUPSEncontrados 			= new ArrayList<ProcedimientosVO>();
			listaCiudadesDireccionamiento	= new ArrayList<CiudadVO>();

			lstRiesgos 						= new ArrayList<SelectItem>();
			listaTiposPrestaciones 			= new ArrayList<SelectItem>();
			listaHallazgosCUMS 				= new ArrayList<SelectItem>();
			listaEstadosCUMS 				= new ArrayList<SelectItem>();
			lstMedicosAuditores				= new ArrayList<SelectItem>();
			lstMedicosAuditoresTemp			= new ArrayList<UsuarioVO>();
			listaCausasCUMS					= new ArrayList<SelectItem>();
			listaFundamentosCUMS			= new ArrayList<SelectItem>();
			listaTiposPrestaciones			= new ArrayList<SelectItem>();
			listaViasAccesoMedicamento		= new ArrayList<SelectItem>();
			listaLateralidades				= new ArrayList<SelectItem>();
			listaDosisMedicamentos			= new ArrayList<SelectItem>();
			listaFrecuenciaMedicamentos		= new ArrayList<SelectItem>();
			lstMotivos						= new ArrayList<SelectItem>();
			lstMotivosPrestacion			= new ArrayList<SelectItem>();
			lstPrestadoresDireccionamiento	= new ArrayList<SelectItem>();
			lstGruposAuditores				= new ArrayList<SelectItem>();
			lstRecobros						= new ArrayList<SelectItem>();
			lstCausalesNoCobroCuota 		= new ArrayList<SelectItem>();
			

			TIPO_ESTADO_NEGOCIO = Integer.parseInt(Messages.getValorParametro(ConstantesWeb.TIPO_ESTADO_NEGOCIO));
			CUMS = Messages.getValorParametro(ConstantesWeb.DESCRIPCION_CUMS);
			CUPS = Messages.getValorParametro(ConstantesWeb.DESCRIPCION_CUPS);
			MEDICAMENTOS = Messages.getValorParametro(ConstantesWeb.DESCRIPCION_MEDICAMENTOS);

			fechaConsulta 				= new Date(); 
			prestacionNueva 			= new PrestacionesAprobadasVO();
			gestionAuditorDTO	 		= new GestionAuditorDTO();
			datosNegacionDTO 			= new DatosNegacionDTO();
			hospitalizacion 			= new HospitalizacionVO();
			procedimientoVOSeleccionado = new ProcedimientosVO();
			ciudad 						= new CiudadVO();

		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	private void validacionEstadoDevuelto() {
		verPestañaCums = gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.COD_ESTADO_DEVUELTO?false:true;
	}
	
	/**  Metodo Guardar los cambios  */
	public void guardar(){
		try{
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				validacionEstadoAprobado = gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.CONS_ESTADO_APROBADO ?true:false;
			}
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_AUDITORIA ){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_GESTION_ESTADO_AUDITORIA), ErrorType.PARAMETRO_ERRADO);
			}
			if (validacionEstadoAprobado && direccionamientoDTOCUMS == null){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DIRECCIONAMIENTO_REQUERIDO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (validacionEstadoAprobado && direccionamientoDTOCUMS.getFechaEsperado() == null ){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHA_REQUERIDO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (validacionEstadoAprobado && direccionamientoDTOCUMS.getCodigoTipoIdentificacion().equals("")){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_IDENTIFICACION_REQUERIDO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (validacionEstadoAprobado && direccionamientoDTOCUMS.getCodigoInterno().equals("")){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CODIGO_INTERNO_REQUERIDO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (validacionEstadoAprobado && direccionamientoDTOCUMS.getSucursal().equals("")){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SUCURSAL_REQUERIDO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (listaDiagnosticosSeleccionados.isEmpty() || !existeDiagnosticoPpal()) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_DIAGNOSTICO_PPAL_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if(noCobraCuotaRecuperacion && prestacionesAprobadasDTOCUMS.getConsecutivoCausaNoCobroCuota() == null) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DEBE_SELECCIONAR_CAUSAL_NO_COBRO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if(noCobraCuotaRecuperacion && prestacionesAprobadasDTOCUMS.getConsecutivoCausaNoCobroCuota().intValue() == 0) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DEBE_SELECCIONAR_CAUSAL_NO_COBRO), ErrorType.PARAMETRO_ERRADO);
			}

			if(gestionAuditorDTO.getJustificacion() != null){
				if (gestionAuditorDTO.getJustificacion().length()<10) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_MINIMO_JUSTIFICACION), ErrorType.PARAMETRO_ERRADO);
				}
			}

			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_APROBADA) {
					if (gestionAuditorDTO.getJustificacion().length()<10) {
						throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_MINIMO_OSB), ErrorType.PARAMETRO_ERRADO);
					}
				}
			}

			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NO_AUTORIZADA) {
					if (datosNegacionDTO.getLstFundamentoLegal().isEmpty() 
							||	datosNegacionDTO.getAuditorMedico().getNumeroUnicoIdentificacionMedico() == null
							|| listaAlternativasCUMS.isEmpty()) {
						throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_DATOS_NEGACION_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
					}else{
						validarUsuarioAuditor();
					}
				}
			}

			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NEGADA
						|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_EN_REMISION_ADMINISTRATIVA
						|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NO_AUTORIZADA
						|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_ANULADA
						|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_DEVUELTA) {
					
					if (gestionAuditorDTO.getLstCausas().isEmpty()) {
						throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_CAUSAS_OBLIGATORIO), ErrorType.PARAMETRO_ERRADO);
					}
				}
			}
			
			/** Se valida si hay prestaciones en estado, auditoria, ingresadas y aprobadas sin direccionamiento **/
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				if ((gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_APROBADA && direccionamientoDTOCUMS == null) ||
				     gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_AUDITORIA ||
					 gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_INGRESADA){
					
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESTADOS_GESTION_SOLICITUD), ErrorType.PARAMETRO_ERRADO);
				}
			}
			
			if(!noCobraCuotaRecuperacion) {
				prestacionesAprobadasDTOCUMS.setConsecutivoCausaNoCobroCuota(0);
			}

			datosNegacionDTO.setLstAlternativas(listaAlternativasCUMS);
			
			if(prestacionesAprobadasDTOCUMS == null){
				prestacionesAprobadasDTOCUMS = new PrestacionesAprobadasVO();
			}
			
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null){
				validacionEstadoDevuelto();
			}
			
			gestionAuditorDTO.setDosis(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsPosologiaCUMS()));
			gestionAuditorDTO.setPosologiaCUMSGeneral(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsecutivoCodigoDosis()));
			gestionAuditorDTO.setCadaGeneral(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getCada()));
			gestionAuditorDTO.setFrecuenciaGeneral(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsecutivoFrecuencia()));
			gestionAuditorDTO.setCantidadSOS(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getCantidad()));
			gestionAuditorDTO.setConsRecobro(Utilidades.validarNullint(prestacionesAprobadasDTOCUMS.getConsecutivoRecobro()));
			gestionAuditorDTO.setCausalNoCobroCuota(Utilidades.validarNullint(prestacionesAprobadasDTOCUMS.getConsecutivoCausaNoCobroCuota()));
			
			solicitudVO.setLstDiagnosticos(listaDiagnosticosSeleccionados);
			solicitudVO.setlSoporteVO(listaSoporteGestionVOs);
			
			GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
			GestionContratacionController gestionContratacionController = new GestionContratacionController();
			
			gestionAuditoriaIntegralController.guardarGestionAuditoriaIntegral(afiliadoVO, solicitudVO, 
					lstPrestaciones, consecutivoPrestacionGlobal, gestionAuditorDTO, datosNegacionDTO, 
					userBpm);
			
			SolicitudVO solicitud = new SolicitudVO();
			solicitud.setNumeroSolicitudSOS(solicitudVO.getNumeroRadicado());
			solicitud.setConsecutivoSolicitud(solicitudVO.getNumeroSolicitud());
			
			AtencionVO atencionVO = new AtencionVO();
			atencionVO.setFechaSolicitudProfesional(solicitudVO.getFechaSolicitud());
			listaSoporteGestionVOs = ingresoSolicitudBean.guardarDocumentosVisos(afiliadoVO, solicitud, atencionVO, listaSoporteGestionVOs);
			
			
			lstPrestacionesSolicitadas = gestionContratacionController.consultaPrestacionesAprobadas(idSolicitud, ConstantesWeb.CADENA_VACIA);
			
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
			
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		mostrarConfirmacionGuardar = false;
	}

	private void validarUsuarioAuditor(){
		String usuarioId;
		String consecutivo;
		for(int i=0; i<lstMedicosAuditoresTemp.size();i++){
			usuarioId = datosNegacionDTO.getAuditorMedico().getNumeroUnicoIdentificacionMedico();
			consecutivo = lstMedicosAuditoresTemp.get(i).getConsecutivo().toString();
			if(usuarioId.equals(consecutivo)){
				datosNegacionDTO.getAuditorMedico().setNumeroIdentificacionMedico(lstMedicosAuditoresTemp.get(i).getLogin());
				break;
			}
		}
	}
	
	public void verConfirmacionGuardar(){
		if(listaAlternativasCUMS.size()<0 && gestionAuditorDTO.getConsecutivoEstadoPrestacion().intValue() == ConstantesWeb.CONS_ESTADO_NO_AUTORIZADO){
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.ERROR_VALIDACION_ALTERNATIVA) , ConstantesWeb.COLOR_ROJO);
		}else{
			mostrarConfirmacionGuardar=true;
		}
	}


	/**  Metodo para reasignar la solicitud  */
	public String reasignar(){
		String url = "";
		try{
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.REASIGNAR_TAREA, descGrupoAuditor);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/**  Metodo para asociar programacion  */
	public void asociarprogramacion(){
		try{
			if (!lstProgramacionEntrega.isEmpty()) {
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				gestionDomiciliariaController.asociarProgramacionEntrega(idSolicitud, prestacionesAprobadasDTOTemp.getConscodigoPrestacion(),
						afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion(), afiliadoVO.getNumeroIdentificacion(), userBpm);
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
			}else{
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_ASOCIAR_PROGRAMACION_SIN_PROGRAMACION"), ErrorType.DATO_NO_EXISTE);
			}
			
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo para terminar la solicitu  */
	public String terminarTarea(){
		String url = "";
		try{
			guardar();
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.TERMINA_TAREA, "-");
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}


	/**  Metodo para liberar la solicitu  */
	public String liberarTarea(){
		String url = "";
		try{
			clienteRestBpmBean.liberarTarea(idTask, idSolicitud.toString(), ConstantesWeb.LIBERAR_TAREA, "-"); 
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/**  Metodo para limpiar el formulario busqueda prestacion  */
	public void limpiarBusquedaPrestacion() {
		prestacionNueva.setTipoPrestacion(null);
		prestacionNueva.setConscodigoPrestacion(null);
		prestacionNueva.setDescripcion(null);
	}

	/**  Metodo para limpiar el formulario agregar prestacion  */
	public void limpiarPanelAgregarPrestacion() {
		prestacionNueva = new PrestacionesAprobadasVO();
	}

	/**  Metodo para buscar un diagnostico  */
	public void buscarDiagnostico() {
		try {
			diagnosticoVoSeleccionado = null;
			listaDiagnosticosEncontrados = null;
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			if (txtCodigoDiagnostico != null && !txtCodigoDiagnostico.trim().equals("")) {
				if (txtCodigoDiagnostico.length()>1) {
					listaDiagnosticosEncontrados = registrarSolicitudesController.consultaDiagnosticosxCodigo(txtCodigoDiagnostico.trim());
				}else {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_MIN_CODIGO"), ErrorType.PARAMETRO_ERRADO);
				}
			} else if (txtDescripcionDiagnostico != null && !txtDescripcionDiagnostico.trim().equals("")) {
				if (txtDescripcionDiagnostico.length()>4) {
					listaDiagnosticosEncontrados = registrarSolicitudesController.consultaDiagnosticosxDescripcion(txtDescripcionDiagnostico);
				}else {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_MIN_CARACTER"), ErrorType.PARAMETRO_ERRADO);
				}
			} else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_SEL_ALGUNO"), ErrorType.PARAMETRO_ERRADO);
			}

			txtCodigoDiagnostico = "";
			txtDescripcionDiagnostico = "";

			if (listaDiagnosticosEncontrados.size() == 1) {
				mostrarPopupDiagnosticos = false;
				diagnosticoVoSeleccionado = listaDiagnosticosEncontrados.get(0);
				txtCodigoDiagnostico = diagnosticoVoSeleccionado.getCodigoDiagnostico();
				txtDescripcionDiagnostico = diagnosticoVoSeleccionado.getDescripcionDiagnostico();
			}else {
				mostrarPopupDiagnosticos = true;
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo cerrar busqueda diagnostico  */
	public void cerrarPopupDiagnostico() {
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	/**  Metodo seleccionar busqueda diagnostico  */
	public void seleccionarDiagnostico() {
		txtCodigoDiagnostico = diagnosticoVoSeleccionado.getCodigoDiagnostico();
		txtDescripcionDiagnostico = diagnosticoVoSeleccionado.getDescripcionDiagnostico();
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	/**  Metodo adicionar un diagnostico en busqueda diagnostico  */
	public void adicionarDiagnostico() {
		try {

			if (diagnosticoVoSeleccionado == null) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO"), ErrorType.PARAMETRO_ERRADO);
			}
			
			diagnosticoVoSeleccionado.setCodigoDiagnostico(diagnosticoVoSeleccionado.getCodigoDiagnostico().trim());
			
			for (DiagnosticoDTO obj : listaDiagnosticosSeleccionados) {
				if (diagnosticoVoSeleccionado.getCodigoDiagnostico().equals(obj.getCodigo().trim())) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO"), ErrorType.PARAMETRO_ERRADO);
				}
				
				if (checkDiagnosticoPpal) {
					if(obj.getPrincipal().equals(ConstantesWeb.SI)
							|| obj.getPrincipal().toUpperCase().equals(ConstantesWeb.DIAGNOSTICO_PPAL)){
						throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_PRINCIPAL"), ErrorType.PARAMETRO_ERRADO);
					}
				}
				
				if (checkDiagnosticoSOS) {
					if(obj.getAsignadoSOS().equals(ConstantesWeb.SI)){
						throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_ASIGNADO_SOS"), ErrorType.PARAMETRO_ERRADO);
					}
				}
			}

			DiagnosticoDTO diagnosticoDTO = new DiagnosticoDTO();
			diagnosticoDTO.setDiagnosticosVO(diagnosticoVoSeleccionado);
			diagnosticoDTO.setAsignadoSOS(checkDiagnosticoSOS==true?ConstantesWeb.SI:ConstantesWeb.NO);
			diagnosticoDTO.setPrincipal(checkDiagnosticoPpal==true?ConstantesWeb.SI:ConstantesWeb.NO);
			diagnosticoDTO.setCodigo(diagnosticoVoSeleccionado.getCodigoDiagnostico());
			diagnosticoDTO.setDescripcion(diagnosticoVoSeleccionado.getDescripcionDiagnostico());

			listaDiagnosticosSeleccionados.add(diagnosticoDTO);

			diagnosticoVoSeleccionado = null;
			txtCodigoDiagnostico = "";
			txtDescripcionDiagnostico = "";
			checkDiagnosticoPpal = false;
			checkDiagnosticoSOS = false;

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo eliminar un diagnostico en busqueda diagnostico  */
	public void elimiarDiagnosticoSeleccionado() {
		listaDiagnosticosSeleccionados.remove(diagnosticoDTOSeleccionado);
	}

	private void validacionCausaRequerida(){
		if (prestacionesAprobadasDTOTemp.getConsEstadoPrestacion() != ConstantesWeb.CODIGO_ESTADO_APROBADA &&
			prestacionesAprobadasDTOTemp.getConsEstadoPrestacion() != ConstantesWeb.CODIGO_ESTADO_AUDITORIA) {
			causaRequerida=true;
			identificacionDirRequerido=false;
			codigoInternoRequerido=false;
			sucursalRequerido=false;
			fechaEsperadosRequerido=false;
		}else{
			identificacionDirRequerido=true;
			codigoInternoRequerido=true;
			sucursalRequerido=true;
			fechaEsperadosRequerido=true;
			causaRequerida=false;
		}
	}
	
	/**  Metodo Cargar info prestacion seleccionada  */
	public void cargarPrestacion(){
		try {
			noCobraCuotaRecuperacion = false;
			CargarCombosController cargarCombosController = new CargarCombosController();
			GestionInconsistenciasController gestionInconsistenciasController = new GestionInconsistenciasController(); 
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			PrestacionController prestacionesBPMController = new PrestacionController();
			GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
			PrestadorController prestadorController = new PrestadorController();
			
			consecutivoPrestacionGlobal = Integer.parseInt(prestacionesAprobadasDTOTemp.getConscodigoPrestacionGSA().trim());

			validacionCausaRequerida();
			/**Panel Solicitud*/
			prestacionesAprobadasDTOCUMS = prestacionesAprobadasDTOTemp;
			gestionAuditorDTO.setConsecutivoEstadoPrestacion(prestacionesAprobadasDTOTemp.getConsEstadoPrestacion());
			
			gestionAuditorDTO.setCantidadSOS(prestacionesAprobadasDTOTemp.getCantidad());
			if(prestacionesAprobadasDTOTemp.getLateralidad() != null){
				gestionAuditorDTO.setConsLateralidad(prestacionesAprobadasDTOTemp.getConsLateralidad());
			}
			gestionAuditorDTO.setViaAcceso(prestacionesAprobadasDTOTemp.getViaAcceso());
		
			if(prestacionesAprobadasDTOTemp.getRecobro() != null) {
				gestionAuditorDTO.setConsRecobro(solicitudVO.getConsRecobro());
			}
			
			if(prestacionesAprobadasDTOTemp.getConsecutivoCausaNoCobroCuota() != null 
					&& prestacionesAprobadasDTOTemp.getConsecutivoCausaNoCobroCuota().intValue() != 0) {
				noCobraCuotaRecuperacion = true;
			}
			
			listaDosisMedicamentos = CargarCombosUtil.obtenerListaSelectDosisMedicamentos(null, cargarCombosController);
			listaFrecuenciaMedicamentos = CargarCombosUtil.obtenerListaSelectFrecuenciaMedicamentos(null, cargarCombosController);
			
			listaHallazgosCUMS = gestionInconsistenciasController.consultaIncosistencias(idSolicitud, ConstantesWeb.CONS_ESTADO_AUDITORIA_MALLA) ;
			/**Panel Direccionamiento*/
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud, consecutivoPrestacionGlobal);
			/**Cunsulta de Estados**/
			listaEstadosCUMS = gestionDomiciliariaController.consultaEstados();
			
			/**Fundamentos*/
			listaFundamentosCUMS = FuncionesAppWeb.obtenerListaSelectFundamentoLegal(gestionDomiciliariaController.consultaFundamentos(idSolicitud, consecutivoPrestacionGlobal));
			/**Alternativas**/
			listaAlternativasCUMS = gestionDomiciliariaController.consultaAlternativas(consecutivoPrestacionGlobal);
			lstAntecedentesPrestacion = prestacionesBPMController.consultarAntecedentesPrestacion(idSolicitud, consecutivoPrestacionGlobal);
			lstGestionesPrestacion = prestacionesBPMController.consultarGestionesPrestacion(consecutivoPrestacionGlobal);
			
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadorController.consultarPrestadoresDireccionamiento(
					prestacionesAprobadasDTOTemp.getConscodigoPrestacion(), 
					new Integer(prestacionesAprobadasDTOTemp.getPlanAfiliado()), 
					prestacionesAprobadasDTOTemp.getConsecutivoCiudad(),
					idSolicitud));
			
			lstMedicosAuditoresTemp = gestionDomiciliariaController.consultarMedicosAuditores(userBpm);
			
			lstMedicosAuditores = FuncionesAppWeb.obtenerListaSelectMedicosAuditores(lstMedicosAuditoresTemp);
			lstProgramacionEntrega= gestionDomiciliariaController.consultarProgramacionEntrega(afiliadoVO.getNumeroUnicoIdentificacion(), new Integer(prestacionesAprobadasDTOTemp.getCodigoTipoPrestacion()), prestacionesAprobadasDTOTemp.getConscodigoPrestacion());
			lstConceptosGastos = gestionAuditoriaIntegralController.consultarConceptoGastos(consecutivoPrestacionGlobal);
						
			listaLateralidades = CargarCombosUtil.obtenerListaSelectLateralidadesProcedimientos(null, cargarCombosController);
			listaViasAccesoMedicamento = CargarCombosUtil.obtenerListaSelectViasAccesoMedicamento(null, cargarCombosController);
			
			lstRecobros = gestionAuditoriaIntegralController.consultaRecobros(idSolicitud);			
			gestionAuditorDTO.setJustificacion(null);
			verPestañaCums = true;
			
			/**Causas. Se consultan las causas para estados diferentes a Auditoria*/
			if(gestionAuditorDTO.getConsecutivoEstadoPrestacion() != ConstantesWeb.CODIGO_ESTADO_AUDITORIA ){
				listaCausasCUMS = FuncionesAppWeb.obtenerListaSelectCausas(gestionDomiciliariaController.consultaCausas(gestionAuditorDTO.getConsecutivoEstadoPrestacion(), consecutivoPrestacionGlobal));
				causaRequerida=false;
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void verGestiones(){
		mostrarPopupVerGestiones=true;
	}
	
	public void cerrarVerGestiones(){
		mostrarPopupVerGestiones=false;
	}

	public void verAntecedentes(){
		mostrarPopupVerAntecedentes=true;
	}
	
	public void cerrarVerAntecedentes(){
		mostrarPopupVerAntecedentes=false;
	}
	/**  Metodo para agregar una prestacion  */
	public void adicionarPrestacion() {      
		try {

			if (tipoPrestacionSeleccionado!=null) {
				if (tipoPrestacionSeleccionado.compareTo(consecutivoCUMS) == 0) {
					if (medicamentoVOSeleccionado == null) {
						throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_SELECCIONAR_PRESTACION"), ErrorType.PARAMETRO_ERRADO);
					}
					validarPrestacionRepetidaMedicamento();
				} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUPS) == 0) {
					if (procedimientoVOSeleccionado == null) {
						throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_SELECCIONAR_PRESTACION"), ErrorType.PARAMETRO_ERRADO);
					}
					validarPrestacionRepetidaProcedimientos();
				}
				prestacionNueva.setCodigoTipoPrestacion(tipoPrestacionSeleccionado.toString());
			}

			if (prestacionNueva.getCodigoTipoPrestacion() == null
					|| prestacionNueva.getCantidad()==null
					|| prestacionNueva.getConsecutivoMotivo()==null) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION"), ErrorType.PARAMETRO_ERRADO);
			}
			
			prestacionNueva.setCodigoPrestacion(codigoPrestacion);
			
			prestacionNueva.setRequiereAuditoria("SI");
			prestacionNueva.setEstadoPrestacion("AUDITORIA");
			prestacionNueva.setVerDetalle(false);
			prestacionNueva.setTipoPrestacion(getDescTipoPrestacion(prestacionNueva.getCodigoTipoPrestacion()));
			
			prestacionNueva.setConsTipoPrestacion(tipoPrestacionSeleccionado);
			prestacionNueva.setConscodigoPrestacion(consecutivoPrestacion);
			prestacionNueva.setDescripcion(descripcionPrestacion);
			
			guardarPrestacion();
			lstPrestacionesSolicitadas.add(prestacionNueva);
			lstPrestaciones.add(prestacionNueva);
			prestacionNueva = new PrestacionesAprobadasVO(); 
			mostrarPopupAdicionarPrestacion = false;
			consecutivoPrestacion = null;
			codigoPrestacion = null;
			descripcionPrestacion = null;
			
			GestionContratacionController gestionContratacionController = new GestionContratacionController();
			lstPrestacionesSolicitadas = gestionContratacionController.consultaPrestacionesAprobadas(idSolicitud, ConstantesWeb.CADENA_VACIA);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void guardarPrestacion() throws LogicException{
		GestionAuditoriaIntegralController gestionAuditoriaIntegralController;
		try{
			gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
		} catch(Exception e){
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		gestionAuditoriaIntegralController.guardarPrestacionAuditoriaIntegral(prestacionNueva, solicitudVO.getNumeroSolicitud(), userBpm);
	}
	
	private void validarPrestacionRepetidaMedicamento() throws LogicException{
		medicamentoVOSeleccionado.setCodigoCodificacionMedicamento(medicamentoVOSeleccionado.getCodigoCodificacionMedicamento().trim());
		for (PrestacionesAprobadasVO obj : lstPrestacionesSolicitadas) {
			if (medicamentoVOSeleccionado.getCodigoCodificacionMedicamento().equals(obj.getCodigoPrestacion().trim())) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_ADICIONADA"), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}

	private void validarPrestacionRepetidaProcedimientos() throws LogicException{
		procedimientoVOSeleccionado.setCodigoCodificacionProcedimiento(procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento().trim());
		for (PrestacionesAprobadasVO obj : lstPrestacionesSolicitadas) {
			if (procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento().equals(obj.getCodigoPrestacion().trim())) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_ADICIONADA"), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}

	/**  Metodo para obtener la descripcion del tipo prestacion  */
	private String getDescTipoPrestacion(String codigoTipoPrestacion){
		String descripcion = "";
		for (SelectItem object : listaTiposPrestaciones) {
			if (object.getValue()!=null && codigoTipoPrestacion.equals(object.getValue().toString())) {
				descripcion = object.getLabel();
				break;
			}
		}
		return descripcion;

	}

	/**  Metodo para buscar la prestacion */
	public void buscarPrestacion() {
		try {
			if (tipoPrestacionSeleccionado!=null) {
				if (tipoPrestacionSeleccionado.compareTo(consecutivoCUMS) == 0) {
					buscarPrestacionMedicamentos();
				} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUPS) == 0) {
					buscarPrestacionCUPS();
				}
				prestacionNueva.setCodigoTipoPrestacion(tipoPrestacionSeleccionado.toString());
			}else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_BUSQUEDA_CAMPOS_OBLIGATORIOS_PRESTACION"), ErrorType.PARAMETRO_ERRADO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo para buscar la prestacion CUPS  */
	public void buscarPrestacionCUPS() {
		try {
			mostrarPopupCUPS = false;
			
			Utilidades.validarAdicionarPrestacion(codigoPrestacion, descripcionPrestacion);			
			com.sos.gestionautorizacionessaludejb.controller.PrestacionController prestacionesController = new com.sos.gestionautorizacionessaludejb.controller.PrestacionController();
			listaCUPSEncontrados = prestacionesController.consultarPrestacionesCUPSPorParametros(new ProcedimientosVO(codigoPrestacion, descripcionPrestacion));

			FuncionesAppWeb funcionesAppWeb = new FuncionesAppWeb();
			funcionesAppWeb.validarAsociacionPrestacion(listaCUPSEncontrados);						

			if(funcionesAppWeb.isMostrarPopupCUPS()){
				mostrarPopupCUPS = true;
			}else{

				mostrarPopupCUPS = false;	
				procedimientoVOSeleccionado = funcionesAppWeb.getProcedimientoVOSeleccionado();
				consecutivoPrestacion = funcionesAppWeb.getProcedimientoVOSeleccionado().getConsecutivoCodificacionProcedimiento();
				codigoPrestacion = funcionesAppWeb.getProcedimientoVOSeleccionado().getCodigoCodificacionProcedimiento();
				descripcionPrestacion = funcionesAppWeb.getProcedimientoVOSeleccionado().getDescripcionCodificacionProcedimiento();
			}		
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo para seleccionar la prestacion CUPS  */
	public void seleccionPopupCUPS() {
		if (procedimientoVOSeleccionado != null) {
			consecutivoPrestacion = procedimientoVOSeleccionado.getConsecutivoCodificacionProcedimiento();
			codigoPrestacion = procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento();
			descripcionPrestacion = procedimientoVOSeleccionado.getDescripcionCodificacionProcedimiento();
			mostrarPopupCUPS = false;
			listaCUPSEncontrados = null;
		}
	}


	/**  Metodo para buscar la prestacion  */
	public void buscarPrestacionMedicamentos() {
		try {
			mostrarPopupMedicamentos = false;
			com.sos.gestionautorizacionessaludejb.controller.PrestacionController prestacionesController = new com.sos.gestionautorizacionessaludejb.controller.PrestacionController();
			if (codigoPrestacion != null && !codigoPrestacion.trim().equals("")) {
				if (codigoPrestacion.length()>1) {
					listaMedicamentosEncontrados = prestacionesController.consultarPrestacionesMedicamentosPorParametros(new MedicamentosVO(codigoPrestacion, descripcionPrestacion));
				}else {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO"), ErrorType.PARAMETRO_ERRADO);
				}
			} else if (descripcionPrestacion != null && !descripcionPrestacion.trim().equals("")) {
				if (descripcionPrestacion.length()>4) {
					listaMedicamentosEncontrados = prestacionesController.consultarPrestacionesMedicamentosPorParametros(new MedicamentosVO(codigoPrestacion, descripcionPrestacion));
				}else {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER"), ErrorType.PARAMETRO_ERRADO);
				}
			} else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO"), ErrorType.PARAMETRO_ERRADO);
			}

			if (listaMedicamentosEncontrados.size() > 1) {
				mostrarPopupMedicamentos = true;
			} else if (listaMedicamentosEncontrados.size() == 1) {

				mostrarPopupMedicamentos = false;
				medicamentoVOSeleccionado = listaMedicamentosEncontrados.get(0);
				consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
				codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
				descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
			} else if (listaMedicamentosEncontrados == null || listaMedicamentosEncontrados.isEmpty()) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION__PRESTACION_SIN_DATOS"), ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo cerrar popup prestaciones  */
	public void cerrarPopupMedicamentos() {
		mostrarPopupMedicamentos = false;
		listaMedicamentosEncontrados = null;
	}

	/**  Metodo para cerrar el popup la prestacion CUPS  */
	public void cerrarPopupCUPS() {
		mostrarPopupCUPS = false;
		listaCUPSEncontrados = null;
	}

	/**  Metodo abrir popup prestaciones  */
	public void abrirPopupAdicionarPrestaciones(){
		try{
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			CargarCombosController cargarCombosController = new CargarCombosController();
			listaTiposPrestaciones = registrarSolicitudesController.obtenerListaSelectTipoPrestaciones(null,0);
			listaLateralidades = CargarCombosUtil.obtenerListaSelectLateralidadesProcedimientos(null, cargarCombosController);
			listaViasAccesoMedicamento = CargarCombosUtil.obtenerListaSelectViasAccesoMedicamento(null, cargarCombosController);
			listaDosisMedicamentos = CargarCombosUtil.obtenerListaSelectDosisMedicamentos(null, cargarCombosController);
			listaFrecuenciaMedicamentos = CargarCombosUtil.obtenerListaSelectFrecuenciaMedicamentos(null, cargarCombosController);
			
			
			cargarMotivos(ConstantesWeb.TIPO_MOTIVOS_CREAR_PRESTACION);

			mostrarPopupAdicionarPrestacion = true;
			prestacionNueva = new PrestacionesAprobadasVO(); 
			codigoPrestacion = null;
			descripcionPrestacion = null;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo abrir popup agregar prestaciones  */
	public void cerrarPopupAdicionarPrestaciones(){
		mostrarPopupAdicionarPrestacion = false;
		prestacionNueva = new PrestacionesAprobadasVO(); 

	}

	/**  Metodo seleccionar prestacion  */
	public void seleccionPopupMedicamentos() {
		if (medicamentoVOSeleccionado != null) {
			consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
			codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
			descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
			mostrarPopupMedicamentos = false;
			listaMedicamentosEncontrados = null;
		}
	}

	/**  Metodo cambiar fecha de direccionamiento  */
	public void cambiarFechaDireccionamiento(){
		try {
			PrestacionController prestacionesBPMController = new PrestacionController();
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			
			if (cambioFecha.getMotivos().isEmpty() || cambioFecha.getFechaEsperado() == null
					|| (cambioFecha.getJustificacion() == null || 
					ConstantesWeb.CADENA_VACIA.equals(cambioFecha.getJustificacion().trim()))) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO"), ErrorType.PARAMETRO_ERRADO);
			}

			if (cambioFecha.getJustificacion().trim().length()<10) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_CAMBIO_DIRECCIONAMIENTO_JUSTIFACION_MIN"), ErrorType.PARAMETRO_ERRADO);
			}
			
			prestacionesBPMController.guardarCambioFechaDireccionamiento(cambioFecha, consecutivoPrestacionGlobal, ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			cambioFecha = new DireccionamientoVO();
			mostrarPopupCambiarFechaDireccionamiento = false;
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud, consecutivoPrestacionGlobal);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo cambiar direccionamiento  */
	public void cambiarDireccionamiento(){
		try {
			PrestacionController prestacionesBPMController = new PrestacionController();
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			
			if (cambioDireccionamiento.getMotivos().isEmpty() || cambioDireccionamiento.getConsecutivoPrestador() == null
					|| cambioDireccionamiento.getJustificacion() == null  || 
					ConstantesWeb.CADENA_VACIA.equals(cambioDireccionamiento.getJustificacion().trim())) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO"), ErrorType.PARAMETRO_ERRADO);
			}

			if (cambioDireccionamiento.getJustificacion().trim().length()<10) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_CAMBIO_DIRECCIONAMIENTO_JUSTIFACION_MIN"), ErrorType.PARAMETRO_ERRADO);
			}

			prestacionesBPMController.guardarDireccionamiento(cambioDireccionamiento, consecutivoPrestacionGlobal, ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud, consecutivoPrestacionGlobal);

			mostrarPopupCambiarDireccionamiento = false;
			activarFiltroCiudad = false;
			ciudad = new CiudadVO();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo agregar una alternativa  */
	public void adicionarAlternativa() {      
		try {
			
			if (!ConstantesWeb.CADENA_VACIA.equals(alternativa.trim())) {
				listaAlternativasCUMS.add(new AlternativasVO(alternativa, null));
				alternativa = null; 
				if(listaAlternativasCUMS.size() == 5){
					alternativaRequerido=false;
					throw new LogicException(Messages.getValorExcepcion("ERROR_CATIDAD_ALTERNATIVAS"), ErrorType.PARAMETRO_ERRADO);
				}
			}else{
				throw new LogicException(Messages.getValorExcepcion("ERROR_ALTERNATIVA_VACIO"), ErrorType.PARAMETRO_ERRADO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	public void cargarMotivos(Integer tipoMotivo){
		try{
			PrestacionController prestacionesBPMController = new PrestacionController();
			lstMotivos = FuncionesAppWeb.obtenerListaMotivos(prestacionesBPMController.consultarMotivosDireccionamiento(tipoMotivo));
			lstMotivosPrestacion = lstMotivos;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**  Metodo eliminar una alternativa  */
	public void eliminarAlternativa() {      
		listaAlternativasCUMS.remove(alternativaSel);
		alternativaRequerido=true;
	}

	/**  Metodo abrir popup prestaciones  */
	public void abrirPopupCambiarDireccionamiento(){
		cargarMotivos(ConstantesWeb.TIPO_MOTIVOS_DIRECCIONAMIENTO);
		cambioDireccionamiento = new DireccionamientoVO();
		mostrarPopupCambiarDireccionamiento = true;
		PrestadorController prestadorController;
		try {
			prestadorController = new PrestadorController();
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadorController.consultarPrestadoresDireccionamiento(
					prestacionesAprobadasDTOCUMS.getConscodigoPrestacion(), 
					new Integer(prestacionesAprobadasDTOTemp.getPlanAfiliado()), 
					prestacionesAprobadasDTOCUMS.getConsecutivoCiudad(),
					idSolicitud));
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		
	}

	/**  Metodo abrir popup prestaciones  */
	public void abrirPopupCambiarFechaDireccionamiento(){
		cargarMotivos(ConstantesWeb.TIPO_MOTIVOS_FECHA_ENTREGA);
		mostrarPopupCambiarFechaDireccionamiento = true;
	}

	/**  Metodo abrir popup prestaciones  */
	public void cerrarPopupCambiarDireccionamiento(){
		mostrarPopupCambiarDireccionamiento = false;
		cambioDireccionamiento = new DireccionamientoVO();
		activarFiltroCiudad = false;
		ciudad = new CiudadVO();
	}

	/**  Metodo abrir popup prestaciones  */
	public void cerrarPopupCambiarFechaDireccionamiento(){
		mostrarPopupCambiarFechaDireccionamiento = false;
		cambioFecha = new DireccionamientoVO();
	}

	/** Metodo que valida si hay diagnostico ppal **/
	private boolean existeDiagnosticoPpal(){
		boolean resultado = false;
		for (DiagnosticoDTO object : listaDiagnosticosSeleccionados) {
			if (object.getPrincipal().equals(ConstantesWeb.SI) 
					|| object.getPrincipal().equals(ConstantesWeb.DIAGNOSTICO_PPAL)) {
				resultado = true;
				break;
			}
		}
		return resultado;
	}

	private void actualizarDocumentosSoporteGestion() throws LogicException {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();			
			listaDocumentoSoporteGestionVOs = IngresoSolicitudesSoportesUtil.agruparDocumentosSoporte(
																			registrarSolicitudesController.consultaTiposSoportexPrestacion(listaPrestacionesSolicitadasGestion));
			listaSoporteGestionVOs = IngresoSolicitudesSoportesUtil.crearListaSoportes
																    (listaDocumentoSoporteGestionVOs, listaSoporteGestionVOs);

		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_VALIDADOR_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
	
	private void actualizarListadoDocumentosSolicitud(){
		for(PrestacionesAprobadasVO prestacionAprobadasDto: lstPrestacionesSolicitadas){
			PrestacionDTO prestacionDto = new PrestacionDTO();
			prestacionDto.setCodigoCodificacionPrestacion(prestacionAprobadasDto.getCodigoPrestacion());
			prestacionDto.setConsecutivoItemPresupuesto(ConstantesWeb.VALOR_UNO);
			TipoCodificacionVO tipoCodificacion = new TipoCodificacionVO();
			tipoCodificacion.setConsecutivoCodigoTipoCodificacion(Integer.parseInt(prestacionAprobadasDto.getCodigoTipoPrestacion()));
			prestacionDto.setTipoCodificacionVO(tipoCodificacion);
			listaPrestacionesSolicitadasGestion.add(prestacionDto);
		}
	}
	
	public void addicionarSoporteGestion() {
		try {
			if (documentoSoporteSeleccionadoGestion != null) {
				maximoDocumentosGestion = documentoSoporteSeleccionadoGestion.getTempCantidadMaximaSoportes() != null ? documentoSoporteSeleccionadoGestion.getTempCantidadMaximaSoportes() : 0;
		
				IngresoSolicitudesSoportesUtil.validarMaximoDocuementos(maximoDocumentosGestion);

				SoporteVO soporteGestionVO;
				if (maximoDocumentosGestion.compareTo(0) != 0) {

					Date fechaActual = new Date(System.currentTimeMillis());
					soporteGestionVO = new SoporteVO();
					soporteGestionVO.setFechaCreacion(fechaActual);
					soporteGestionVO.setUsuarioCreacion(FacesUtils.getUserName());
					soporteGestionVO.setFechaUltimaModificacion(fechaActual);
					soporteGestionVO.setUsuarioUltimaModificacion(FacesUtils.getUserName());
					soporteGestionVO.setRutaFisica(ConstantesWeb.CADENA_VACIA);
					soporteGestionVO.setNumeroUnicIdentifUsuario(ConstantesWeb.VALOR_UNO);
					soporteGestionVO.setConsecutivoCodigoDocumentoSoporte(documentoSoporteSeleccionadoGestion.getConsecutivoDocumento());
					soporteGestionVO.setDescripcionDocumentoSoporte(documentoSoporteSeleccionadoGestion.getNombreDocumento());
					soporteGestionVO.setDocumentoSoporteVO(documentoSoporteSeleccionadoGestion);
					listaSoporteGestionVOs.add(soporteGestionVO);

					maximoDocumentosGestion--;
					documentoSoporteSeleccionadoGestion.setTempCantidadMaximaSoportes(maximoDocumentosGestion);
				}
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	public void listenerUpload(UploadEvent event) {
		try {
			
			if(documentoSoporteSeleccionadoGestion != null){
				UploadItem item = event.getUploadItem();
				byte[] data = Utilidades.extractBytes(item.getFile().getAbsolutePath());

				maximoDocumentosGestion = documentoSoporteSeleccionadoGestion.getTempCantidadMaximaSoportes() != null ? documentoSoporteSeleccionadoGestion.getTempCantidadMaximaSoportes() : 0;
				
				if (maximoDocumentosGestion.compareTo(0) != 0) {
					soporteSeleccionadoGestion.setNombreSoporte(item.getFileName());
					soporteSeleccionadoGestion.setRutaFisica(ConstantesWeb.CADENA_VACIA);
					soporteSeleccionadoGestion.setData(data);
					soporteSeleccionadoGestion.setTamanoArchivo(item.getFileSize());
					soporteSeleccionadoGestion.setSubidoAVisos(false);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}	
	
	
	public void preBorrarArchivoCargado() {
		mostrarPopupConfirmacionBorrarArchivoGestion = false;
		boolean encontroVariosArchivos;
		if (soporteSeleccionadoGestion != null && soporteSeleccionadoGestion.getNombreSoporte() != null && soporteSeleccionadoGestion.getData() != null) {
			mostrarPopupConfirmacionBorrarArchivoGestion = true;
		} else {
			encontroVariosArchivos = IngresoSolicitudesSoportesUtil.buscarArchivosRepetidos(listaSoporteGestionVOs, soporteSeleccionadoGestion);
			if (encontroVariosArchivos) {
				listaSoporteGestionVOs.remove(soporteSeleccionadoGestion);
				if (soporteSeleccionadoGestion.getDocumentoSoporteVO() != null) {
					soporteSeleccionadoGestion.getDocumentoSoporteVO().setTempCantidadMaximaSoportes(soporteSeleccionadoGestion.getDocumentoSoporteVO().getTempCantidadMaximaSoportes() + ConstantesWeb.CANTIDAD_ARCHIVOS_REPETIDOS);
				}
			} else {
				MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.ERROR_ARCHIVOS_BORRAR), "blue");
			}
		}
	}
	
	public void borrarArchivoCargadoGestion() {
		boolean encontroVarios = IngresoSolicitudesSoportesUtil.buscarArchivosRepetidos(listaSoporteGestionVOs, soporteSeleccionadoGestion);
		if (encontroVarios) {
			listaSoporteGestionVOs.remove(soporteSeleccionadoGestion);
			if (soporteSeleccionadoGestion.getDocumentoSoporteVO() != null) {
				soporteSeleccionadoGestion.getDocumentoSoporteVO().setTempCantidadMaximaSoportes(soporteSeleccionadoGestion.getDocumentoSoporteVO().getTempCantidadMaximaSoportes() + ConstantesWeb.CANTIDAD_ARCHIVOS_REPETIDOS);
			}
		} else {
			soporteSeleccionadoGestion.setNombreSoporte(null);
			soporteSeleccionadoGestion.setRutaFisica("");
			soporteSeleccionadoGestion.setData(null);
			soporteSeleccionadoGestion.setTamanoArchivo(null);
		}
	}
	
	public void aceptarDocumentosSoporte() {
		try {
			mostrarPopupConfirmacionDocumentosGestion = false;
			validacionesDocumentoSoporte();
			if (archivosObligatoriosFaltantes) {
				mostrarPopupConfirmacionDocumentosGestion = true;
			} else {
				mostrarPopupDocumentosSoporteGestion = false;
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
		
	}
	
	
	public void aceptarDocumentosFaltantesGestion() {
		try {
			validacionesDocumentoSoporte();
			mostrarPopupDocumentosSoporteGestion = false;
			mostrarPopupConfirmacionDocumentosGestion = false;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	private void validacionesDocumentoSoporte() throws LogicException {
		archivosObligatoriosFaltantes = false;
		for (SoporteVO soporteVO : listaSoporteGestionVOs) {
			// Validacion de si existe archivo cuando el sooprte es
			// obligatorio
			if (soporteVO.getNombreSoporte() == null || soporteVO.getData() == null) {
				archivosObligatoriosFaltantes = true;
			}

			// Validacion de archivos repetidos
			IngresoSolicitudesSoportesUtil.validacionArchivosRepetidos(listaSoporteGestionVOs, soporteVO);

			// Validacion de tamano del archivo
			IngresoSolicitudesSoportesUtil.validacionTamanoArchivo(soporteVO);
		}
	}
	
	public String guardarProcesoReasignacionPrestacion(){
		String url ="";
		try {
			if(descGrupoAuditor != null){
				ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(observacionesReasignar, prestacionesReasignar);
				GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
				
				gestionAuditoriaIntegralController.guardarProcesoReasignacionPrestacionAuditoriaIntegral(observacionesReasignar, prestacionesReasignar, userBpm, descGrupoAuditor);
				mostrarPopupReliquidar = false;
				url = reasignar();
			}else {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO), ConstantesWeb.COLOR_ROJO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	public void cancelarDocumentosFaltantes() {
		mostrarPopupDocumentosSoporteGestion = true;
		mostrarPopupConfirmacionDocumentosGestion = false;
	}

	public void mostrarPopupDocumentosSoporteAuditor() {
		mostrarPopupDocumentosSoporteGestion = true;
	}

	public void ocultarPopupDocumentosSoporteAuditor() {
		mostrarPopupDocumentosSoporteGestion = false;
	}	
	
	public void cancelarBorrarArchivoCargado() {
		mostrarPopupConfirmacionBorrarArchivoGestion = false;
	}
	
	/**
	 * Metodo que carga la informacion de las causales de no cobro de cuota de recuperacion.
	 */
	public void obtenerCausalesNoCobroCuota() {
		GestionAuditoriaIntegralController gestionAuditoriaIntegralController;
		try {
			gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
			lstCausalesNoCobroCuota = gestionAuditoriaIntegralController.consultarCausalesNoCobroCuota();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}
	
	/**
	 * Metodo que realiza la busqueda de la ciudad de acuerdo al codigo o descripcion ingresada
	 */
	public void buscarCiudad() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupCiudades = false;
			listaCiudadesDireccionamiento = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudad, registrarSolicitudesController);			
			ciudad = new CiudadVO();
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesDireccionamiento)){
				mostrarPopupCiudades = true;
			}else{
				mostrarPopupCiudades = false;
				ciudad = listaCiudadesDireccionamiento.get(0);
			}	
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}	
	
	/**
	 * Cierra popup con listado de ciudades.
	 */
	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		
	}
	
	/**
	 * Metodo que recalcula los direccionamientos de acuerdo a la ciudad.
	 */
	public void recalcularDireccionamientos() {		
		try {
			int consecutivoCiudad = ciudad.getConsecutivoCodigoCiudad() == null? ConstantesWeb.VALOR_CERO: ciudad.getConsecutivoCodigoCiudad().intValue();
			if(consecutivoCiudad == ConstantesWeb.VALOR_CERO) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_BUSQUEDA_CIUDAD),
						ErrorType.PARAMETRO_ERRADO);
			}
			lstPrestadoresDireccionamiento.clear();
			PrestadorController prestadorController = new PrestadorController();
			List<PrestadorVO> prestadores = prestadorController
					.consultarPrestadoresDireccionamiento(prestacionesAprobadasDTOCUMS.getConscodigoPrestacion(), 
					afiliadoVO.getPlan().getConsectivoPlan(), ciudad.getConsecutivoCodigoCiudad(), solicitudVO.getNumeroSolicitud());
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadores);
	
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}
	
	public void limpiarCiudad() {
		if(!activarFiltroCiudad) 
			ciudad = new CiudadVO();
	}
	
	//-------- Get y Set ------------//

	/**
	 * @return the prestacionesAprobadasDTOTemp
	 */
	public PrestacionesAprobadasVO getPrestacionesAprobadasDTOTemp() {
		return prestacionesAprobadasDTOTemp;
	}

	/**
	 * @param prestacionesAprobadasDTOTemp the prestacionesAprobadasDTOTemp to set
	 */
	public void setPrestacionesAprobadasDTOTemp(
			PrestacionesAprobadasVO prestacionesAprobadasDTOTemp) {
		this.prestacionesAprobadasDTOTemp = prestacionesAprobadasDTOTemp;
	}

	/**
	 * @return the prestacionesAprobadasDTOCUMS
	 */
	public PrestacionesAprobadasVO getPrestacionesAprobadasDTOCUMS() {
		return prestacionesAprobadasDTOCUMS;
	}

	/**
	 * @param prestacionesAprobadasDTOCUMS the prestacionesAprobadasDTOCUMS to set
	 */
	public void setPrestacionesAprobadasDTOCUMS(
			PrestacionesAprobadasVO prestacionesAprobadasDTOCUMS) {
		this.prestacionesAprobadasDTOCUMS = prestacionesAprobadasDTOCUMS;
	}

	/**
	 * @return the direccionamientoDTOCUMS
	 */
	public DireccionamientoVO getDireccionamientoDTOCUMS() {
		return direccionamientoDTOCUMS;
	}

	/**
	 * @param direccionamientoDTOCUMS the direccionamientoDTOCUMS to set
	 */
	public void setDireccionamientoDTOCUMS(
			DireccionamientoVO direccionamientoDTOCUMS) {
		this.direccionamientoDTOCUMS = direccionamientoDTOCUMS;
	}

	/**
	 * @return the cambioFecha
	 */
	public DireccionamientoVO getCambioFecha() {
		return cambioFecha;
	}

	/**
	 * @param cambioFecha the cambioFecha to set
	 */
	public void setCambioFecha(DireccionamientoVO cambioFecha) {
		this.cambioFecha = cambioFecha;
	}

	/**  Metodo cerrar popup Reliquidar */
	public void cerrarPopupReasignarSolicitud(){
		mostrarPopupReliquidar = false;
	}
	
	/**  Metodo Abrir popup ReasignarSolicitud */
	public void abrirPopupReasignarSolicitud(){
		mostrarPopupReliquidar = true;
	}
	
	/**
	 * @return the cambioDireccionamiento
	 */
	public DireccionamientoVO getCambioDireccionamiento() {
		return cambioDireccionamiento;
	}

	/**
	 * @param cambioDireccionamiento the cambioDireccionamiento to set
	 */
	public void setCambioDireccionamiento(DireccionamientoVO cambioDireccionamiento) {
		this.cambioDireccionamiento = cambioDireccionamiento;
	}

	/**
	 * @return the medicamentoVOSeleccionado
	 */
	public MedicamentosVO getMedicamentoVOSeleccionado() {
		return medicamentoVOSeleccionado;
	}

	/**
	 * @param medicamentoVOSeleccionado the medicamentoVOSeleccionado to set
	 */
	public void setMedicamentoVOSeleccionado(
			MedicamentosVO medicamentoVOSeleccionado) {
		this.medicamentoVOSeleccionado = medicamentoVOSeleccionado;
	}

	/**
	 * @return the diagnosticoVoSeleccionado
	 */
	public DiagnosticosVO getDiagnosticoVoSeleccionado() {
		return diagnosticoVoSeleccionado;
	}

	/**
	 * @param diagnosticoVoSeleccionado the diagnosticoVoSeleccionado to set
	 */
	public void setDiagnosticoVoSeleccionado(
			DiagnosticosVO diagnosticoVoSeleccionado) {
		this.diagnosticoVoSeleccionado = diagnosticoVoSeleccionado;
	}

	/**
	 * @return the solicitudVO
	 */
	public SolicitudBpmVO getSolicitudVO() {
		return solicitudVO;
	}

	/**
	 * @param solicitudVO the solicitudVO to set
	 */
	public void setSolicitudVO(SolicitudBpmVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	/**
	 * @return the datosBasicosAfiliado
	 */
	public AfiliadoVO getDatosBasicosAfiliado() {
		return afiliadoVO;
	}

	/**
	 * @param datosBasicosAfiliado the datosBasicosAfiliado to set
	 */
	public void setDatosBasicosAfiliado(AfiliadoVO datosBasicosAfiliado) {
		this.afiliadoVO = datosBasicosAfiliado;
	}

	/**
	 * @return the prestacionNueva
	 */
	public PrestacionesAprobadasVO getPrestacionNueva() {
		return prestacionNueva;
	}

	/**
	 * @param prestacionNueva the prestacionNueva to set
	 */
	public void setPrestacionNueva(PrestacionesAprobadasVO prestacionNueva) {
		this.prestacionNueva = prestacionNueva;
	}

	/**
	 * @return the alternativaSel
	 */
	public AlternativasVO getAlternativaSel() {
		return alternativaSel;
	}

	/**
	 * @param alternativaSel the alternativaSel to set
	 */
	public void setAlternativaSel(AlternativasVO alternativaSel) {
		this.alternativaSel = alternativaSel;
	}

	/**
	 * @return the documentoSoporteVOSeleccionadoAuditoria
	 */
	public DocumentoSoporteVO getDocumentoSoporteVOSeleccionadoAuditoria() {
		return documentoSoporteVOSeleccionadoAuditoria;
	}

	/**
	 * @param documentoSoporteVOSeleccionadoAuditoria the documentoSoporteVOSeleccionadoAuditoria to set
	 */
	public void setDocumentoSoporteVOSeleccionadoAuditoria(
			DocumentoSoporteVO documentoSoporteVOSeleccionadoAuditoria) {
		this.documentoSoporteVOSeleccionadoAuditoria = documentoSoporteVOSeleccionadoAuditoria;
	}

	/**
	 * @return the listaHallazgosCUMS
	 */
	public List<SelectItem> getListaHallazgosCUMS() {
		return listaHallazgosCUMS;
	}

	/**
	 * @param listaHallazgosCUMS the listaHallazgosCUMS to set
	 */
	public void setListaHallazgosCUMS(List<SelectItem> listaHallazgosCUMS) {
		this.listaHallazgosCUMS = listaHallazgosCUMS;
	}

	/**
	 * @return the listaEstadosCUMS
	 */
	public List<SelectItem> getListaEstadosCUMS() {
		return listaEstadosCUMS;
	}

	/**
	 * @param listaEstadosCUMS the listaEstadosCUMS to set
	 */
	public void setListaEstadosCUMS(List<SelectItem> listaEstadosCUMS) {
		this.listaEstadosCUMS = listaEstadosCUMS;
	}

	/**
	 * @return the lstMedicosAuditores
	 */
	public List<SelectItem> getLstMedicosAuditores() {
		return lstMedicosAuditores;
	}

	/**
	 * @param lstMedicosAuditores the lstMedicosAuditores to set
	 */
	public void setLstMedicosAuditores(List<SelectItem> lstMedicosAuditores) {
		this.lstMedicosAuditores = lstMedicosAuditores;
	}

	/**
	 * @return the lstRiesgos
	 */
	public List<SelectItem> getLstRiesgos() {
		return lstRiesgos;
	}

	/**
	 * @param lstRiesgos the lstRiesgos to set
	 */
	public void setLstRiesgos(List<SelectItem> lstRiesgos) {
		this.lstRiesgos = lstRiesgos;
	}

	/**
	 * @return the listaCausasCUMS
	 */
	public List<SelectItem> getListaCausasCUMS() {
		return listaCausasCUMS;
	}

	/**
	 * @param listaCausasCUMS the listaCausasCUMS to set
	 */
	public void setListaCausasCUMS(List<SelectItem> listaCausasCUMS) {
		this.listaCausasCUMS = listaCausasCUMS;
	}

	/**
	 * @return the listaFundamentosCUMS
	 */
	public List<SelectItem> getListaFundamentosCUMS() {
		return listaFundamentosCUMS;
	}

	/**
	 * @param listaFundamentosCUMS the listaFundamentosCUMS to set
	 */
	public void setListaFundamentosCUMS(List<SelectItem> listaFundamentosCUMS) {
		this.listaFundamentosCUMS = listaFundamentosCUMS;
	}

	/**
	 * @return the listaTiposPrestaciones
	 */
	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	/**
	 * @param listaTiposPrestaciones the listaTiposPrestaciones to set
	 */
	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	/**
	 * @return the listaViasAccesoMedicamento
	 */
	public List<SelectItem> getListaViasAccesoMedicamento() {
		return listaViasAccesoMedicamento;
	}

	/**
	 * @param listaViasAccesoMedicamento the listaViasAccesoMedicamento to set
	 */
	public void setListaViasAccesoMedicamento(
			List<SelectItem> listaViasAccesoMedicamento) {
		this.listaViasAccesoMedicamento = listaViasAccesoMedicamento;
	}

	/**
	 * @return the listaLateralidades
	 */
	public List<SelectItem> getListaLateralidades() {
		return listaLateralidades;
	}

	/**
	 * @param listaLateralidades the listaLateralidades to set
	 */
	public void setListaLateralidades(List<SelectItem> listaLateralidades) {
		this.listaLateralidades = listaLateralidades;
	}

	/**
	 * @return the listaDosisMedicamentos
	 */
	public List<SelectItem> getListaDosisMedicamentos() {
		return listaDosisMedicamentos;
	}

	/**
	 * @param listaDosisMedicamentos the listaDosisMedicamentos to set
	 */
	public void setListaDosisMedicamentos(List<SelectItem> listaDosisMedicamentos) {
		this.listaDosisMedicamentos = listaDosisMedicamentos;
	}

	/**
	 * @return the listaFrecuenciaMedicamentos
	 */
	public List<SelectItem> getListaFrecuenciaMedicamentos() {
		return listaFrecuenciaMedicamentos;
	}

	/**
	 * @param listaFrecuenciaMedicamentos the listaFrecuenciaMedicamentos to set
	 */
	public void setListaFrecuenciaMedicamentos(
			List<SelectItem> listaFrecuenciaMedicamentos) {
		this.listaFrecuenciaMedicamentos = listaFrecuenciaMedicamentos;
	}

	/**
	 * @return the lstMotivos
	 */
	public List<SelectItem> getLstMotivos() {
		return lstMotivos;
	}

	/**
	 * @param lstMotivosPrestacion the lstMotivosPrestacion to set
	 */
	public void setLstMotivos(List<SelectItem> lstMotivos) {
		this.lstMotivos = lstMotivos;
	}

	/**
	 * @return the lstMotivosPrestacion
	 */
	public List<SelectItem> getLstMotivosPrestacion() {
		return lstMotivosPrestacion;
	}

	/**
	 * @param lstMotivos the lstMotivos to set
	 */
	public void setLstMotivosPrestacion(List<SelectItem> lstMotivosPrestacion) {
		this.lstMotivosPrestacion = lstMotivosPrestacion;
	}

	/**
	 * @return the lstPrestadoresDireccionamiento
	 */
	public List<SelectItem> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

	/**
	 * @param lstPrestadoresDireccionamiento the lstPrestadoresDireccionamiento to set
	 */
	public void setLstPrestadoresDireccionamiento(
			List<SelectItem> lstPrestadoresDireccionamiento) {
		this.lstPrestadoresDireccionamiento = lstPrestadoresDireccionamiento;
	}

	/**
	 * @return the listaAlternativasCUMS
	 */
	public List<AlternativasVO> getListaAlternativasCUMS() {
		return listaAlternativasCUMS;
	}

	/**
	 * @param listaAlternativasCUMS the listaAlternativasCUMS to set
	 */
	public void setListaAlternativasCUMS(List<AlternativasVO> listaAlternativasCUMS) {
		this.listaAlternativasCUMS = listaAlternativasCUMS;
	}

	/**
	 * @return the listaEmpleadores
	 */
	public List<EmpleadorVO> getListaEmpleadores() {
		return listaEmpleadores;
	}

	/**
	 * @param listaEmpleadores the listaEmpleadores to set
	 */
	public void setListaEmpleadores(List<EmpleadorVO> listaEmpleadores) {
		this.listaEmpleadores = listaEmpleadores;
	}

	/**
	 * @return the listaConveniosCapacitacion
	 */
	public List<ConveniosCapitacionVO> getListaConveniosCapacitacion() {
		return listaConveniosCapacitacion;
	}

	/**
	 * @param listaConveniosCapacitacion the listaConveniosCapacitacion to set
	 */
	public void setListaConveniosCapacitacion(
			List<ConveniosCapitacionVO> listaConveniosCapacitacion) {
		this.listaConveniosCapacitacion = listaConveniosCapacitacion;
	}

	/**
	 * @return the listaDiagnosticosEncontrados
	 */
	public List<DiagnosticosVO> getListaDiagnosticosEncontrados() {
		return listaDiagnosticosEncontrados;
	}

	/**
	 * @param listaDiagnosticosEncontrados the listaDiagnosticosEncontrados to set
	 */
	public void setListaDiagnosticosEncontrados(
			List<DiagnosticosVO> listaDiagnosticosEncontrados) {
		this.listaDiagnosticosEncontrados = listaDiagnosticosEncontrados;
	}

	/**
	 * @return the listaDiagnosticosSeleccionados
	 */
	public List<DiagnosticoDTO> getListaDiagnosticosSeleccionados() {
		return listaDiagnosticosSeleccionados;
	}

	/**
	 * @param listaDiagnosticosSeleccionados the listaDiagnosticosSeleccionados to set
	 */
	public void setListaDiagnosticosSeleccionados(
			List<DiagnosticoDTO> listaDiagnosticosSeleccionados) {
		this.listaDiagnosticosSeleccionados = listaDiagnosticosSeleccionados;
	}

	/**
	 * @return the lstGestionesPrestacion
	 */
	public List<GestionesPrestacionVO> getLstGestionesPrestacion() {
		return lstGestionesPrestacion;
	}

	/**
	 * @param lstGestionesPrestacion the lstGestionesPrestacion to set
	 */
	public void setLstGestionesPrestacion(
			List<GestionesPrestacionVO> lstGestionesPrestacion) {
		this.lstGestionesPrestacion = lstGestionesPrestacion;
	}

	/**
	 * @return the lstHistoricosPrestacion
	 */
	public List<HistoricoPrestacionVO> getLstHistoricosPrestacion() {
		return lstHistoricosPrestacion;
	}

	/**
	 * @param lstHistoricosPrestacion the lstHistoricosPrestacion to set
	 */
	public void setLstHistoricosPrestacion(
			List<HistoricoPrestacionVO> lstHistoricosPrestacion) {
		this.lstHistoricosPrestacion = lstHistoricosPrestacion;
	}

	/**
	 * @return the lstAntecedentesPrestacion
	 */
	public List<AntecedentesPrestacionVO> getLstAntecedentesPrestacion() {
		return lstAntecedentesPrestacion;
	}

	/**
	 * @param lstAntecedentesPrestacion the lstAntecedentesPrestacion to set
	 */
	public void setLstAntecedentesPrestacion(
			List<AntecedentesPrestacionVO> lstAntecedentesPrestacion) {
		this.lstAntecedentesPrestacion = lstAntecedentesPrestacion;
	}

	/**
	 * @return the lstPrestacionesSolicitadas
	 */
	public List<PrestacionesAprobadasVO> getLstPrestacionesSolicitadas() {
		return lstPrestacionesSolicitadas;
	}

	/**
	 * @param lstPrestacionesSolicitadas the lstPrestacionesSolicitadas to set
	 */
	public void setLstPrestacionesSolicitadas(
			List<PrestacionesAprobadasVO> lstPrestacionesSolicitadas) {
		this.lstPrestacionesSolicitadas = lstPrestacionesSolicitadas;
	}

	/**
	 * @return the listaMedicamentosEncontrados
	 */
	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	/**
	 * @param listaMedicamentosEncontrados the listaMedicamentosEncontrados to set
	 */
	public void setListaMedicamentosEncontrados(
			List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	/**
	 * @return the listaDocumentosAuditoria
	 */
	public List<DocumentoSoporteVO> getListaDocumentosAuditoria() {
		return listaDocumentosAuditoria;
	}

	/**
	 * @param listaDocumentosAuditoria the listaDocumentosAuditoria to set
	 */
	public void setListaDocumentosAuditoria(
			List<DocumentoSoporteVO> listaDocumentosAuditoria) {
		this.listaDocumentosAuditoria = listaDocumentosAuditoria;
	}
	
	/**
	 * @return the diagnosticoDTOSeleccionado
	 */
	public DiagnosticoDTO getDiagnosticoDTOSeleccionado() {
		return diagnosticoDTOSeleccionado;
	}

	/**
	 * @param diagnosticoDTOSeleccionado the diagnosticoDTOSeleccionado to set
	 */
	public void setDiagnosticoDTOSeleccionado(
			DiagnosticoDTO diagnosticoDTOSeleccionado) {
		this.diagnosticoDTOSeleccionado = diagnosticoDTOSeleccionado;
	}

	/**
	 * @return the fechaConsulta
	 */
	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	/**
	 * @param fechaConsulta the fechaConsulta to set
	 */
	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	/**
	 * @return the tIPO_ESTADO_NEGOCIO
	 */
	public int getTIPO_ESTADO_NEGOCIO() {
		return TIPO_ESTADO_NEGOCIO;
	}

	/**
	 * @param tIPO_ESTADO_NEGOCIO the tIPO_ESTADO_NEGOCIO to set
	 */
	public void setTIPO_ESTADO_NEGOCIO(int tIPO_ESTADO_NEGOCIO) {
		TIPO_ESTADO_NEGOCIO = tIPO_ESTADO_NEGOCIO;
	}

	/**
	 * @return the cUMS
	 */
	public String getCUMS() {
		return CUMS;
	}

	/**
	 * @param cUMS the cUMS to set
	 */
	public void setCUMS(String cUMS) {
		CUMS = cUMS;
	}

	/**
	 * @return the pestanaSeleccionada
	 */
	public String getPestanaSeleccionada() {
		return pestanaSeleccionada;
	}

	/**
	 * @param pestanaSeleccionada the pestanaSeleccionada to set
	 */
	public void setPestanaSeleccionada(String pestanaSeleccionada) {
		this.pestanaSeleccionada = pestanaSeleccionada;
	}

	/**
	 * @return the txtCodigoDiagnostico
	 */
	public String getTxtCodigoDiagnostico() {
		return txtCodigoDiagnostico;
	}

	/**
	 * @param txtCodigoDiagnostico the txtCodigoDiagnostico to set
	 */
	public void setTxtCodigoDiagnostico(String txtCodigoDiagnostico) {
		this.txtCodigoDiagnostico = txtCodigoDiagnostico;
	}

	/**
	 * @return the txtDescripcionDiagnostico
	 */
	public String getTxtDescripcionDiagnostico() {
		return txtDescripcionDiagnostico;
	}

	/**
	 * @param txtDescripcionDiagnostico the txtDescripcionDiagnostico to set
	 */
	public void setTxtDescripcionDiagnostico(String txtDescripcionDiagnostico) {
		this.txtDescripcionDiagnostico = txtDescripcionDiagnostico;
	}

	/**
	 * @return the checkDiagnosticoPpal
	 */
	public boolean isCheckDiagnosticoPpal() {
		return checkDiagnosticoPpal;
	}

	/**
	 * @param checkDiagnosticoPpal the checkDiagnosticoPpal to set
	 */
	public void setCheckDiagnosticoPpal(boolean checkDiagnosticoPpal) {
		this.checkDiagnosticoPpal = checkDiagnosticoPpal;
	}

	/**
	 * @return the checkDiagnosticoSOS
	 */
	public boolean isCheckDiagnosticoSOS() {
		return checkDiagnosticoSOS;
	}

	/**
	 * @param checkDiagnosticoSOS the checkDiagnosticoSOS to set
	 */
	public void setCheckDiagnosticoSOS(boolean checkDiagnosticoSOS) {
		this.checkDiagnosticoSOS = checkDiagnosticoSOS;
	}
	

	/**
	 * @return the mostrarPopupDiagnosticos
	 */
	public boolean isMostrarPopupDiagnosticos() {
		return mostrarPopupDiagnosticos;
	}

	/**
	 * @param mostrarPopupDiagnosticos the mostrarPopupDiagnosticos to set
	 */
	public void setMostrarPopupDiagnosticos(boolean mostrarDiagnosticos) {
		this.mostrarPopupDiagnosticos = mostrarDiagnosticos;
	}

	/**
	 * @return the verPestañaCums
	 */
	public boolean isVerPestañaCums() {
		return verPestañaCums;
	}

	/**
	 * @param verPestañaCums the verPestañaCums to set
	 */
	public void setVerPestañaCums(boolean verPestanaCums) {
		this.verPestañaCums = verPestanaCums;
	}

	/**
	 * @return the alternativa
	 */
	public String getAlternativa() {
		return alternativa;
	}

	/**
	 * @param alternativa the alternativa to set
	 */
	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}

	/**
	 * @return the consecutivoPrestacion
	 */
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	/**
	 * @param consecutivoPrestacion the consecutivoPrestacion to set
	 */
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	/**
	 * @return the codigoPrestacion
	 */
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	/**
	 * @param codigoPrestacion the codigoPrestacion to set
	 */
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	/**
	 * @return the descripcionPrestacion
	 */
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	/**
	 * @param descripcionPrestacion the descripcionPrestacion to set
	 */
	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	/**
	 * @return the tipoPrestacionSeleccionado
	 */
	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	/**
	 * @param tipoPrestacionSeleccionado the tipoPrestacionSeleccionado to set
	 */
	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	/**
	 * @return the mostrarPopupMedicamentos
	 */
	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	/**
	 * @param mostrarPopupMedicamentos the mostrarPopupMedicamentos to set
	 */
	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	/**
	 * @return the mostrarPopupAdicionarPrestacion
	 */
	public boolean isMostrarPopupAdicionarPrestacion() {
		return mostrarPopupAdicionarPrestacion;
	}

	/**
	 * @param mostrarPopupAdicionarPrestacion the mostrarPopupAdicionarPrestacion to set
	 */
	public void setMostrarPopupAdicionarPrestacion(
			boolean mostrarPopupAdicionarPrestacion) {
		this.mostrarPopupAdicionarPrestacion = mostrarPopupAdicionarPrestacion;
	}

	/**
	 * @return the mostrarPopupDocumentosSoporteAuditoria
	 */
	public boolean isMostrarPopupDocumentosSoporteAuditoria() {
		return mostrarPopupDocumentosSoporteAuditoria;
	}

	/**
	 * @param mostrarPopupDocumentosSoporteAuditoria the mostrarPopupDocumentosSoporteAuditoria to set
	 */
	public void setMostrarPopupDocumentosSoporteAuditoria(
			boolean mostrarPopupDocumentosSoporteAuditoria) {
		this.mostrarPopupDocumentosSoporteAuditoria = mostrarPopupDocumentosSoporteAuditoria;
	}

	/**
	 * @return the consecutivoPrestacionGlobal
	 */
	public Integer getConsecutivoPrestacionGlobal() {
		return consecutivoPrestacionGlobal;
	}

	/**
	 * @param consecutivoPrestacionGlobal the consecutivoPrestacionGlobal to set
	 */
	public void setConsecutivoPrestacionGlobal(Integer consecutivoPrestacionGlobal) {
		this.consecutivoPrestacionGlobal = consecutivoPrestacionGlobal;
	}

	/**
	 * @return the consecutivoSolicitudGlobal
	 */
	public Integer getConsecutivoSolicitudGlobal() {
		return idSolicitud;
	}

	/**
	 * @param consecutivoSolicitudGlobal the consecutivoSolicitudGlobal to set
	 */
	public void setConsecutivoSolicitudGlobal(Integer consecutivoSolicitudGlobal) {
		this.idSolicitud = consecutivoSolicitudGlobal;
	}

	/**
	 * @return the gestionAuditorDTO
	 */
	public GestionAuditorDTO getGestionAuditorDTO() {
		return gestionAuditorDTO;
	}

	/**
	 * @param gestionAuditorDTO the gestionAuditorDTO to set
	 */
	public void setGestionAuditorDTO(GestionAuditorDTO gestionAuditorDTO) {
		this.gestionAuditorDTO = gestionAuditorDTO;
	}

	/**
	 * @return the datosNegacionDTO
	 */
	public DatosNegacionDTO getDatosNegacionDTO() {
		return datosNegacionDTO;
	}

	/**
	 * @param datosNegacionDTO the datosNegacionDTO to set
	 */
	public void setDatosNegacionDTO(DatosNegacionDTO datosNegacionDTO) {
		this.datosNegacionDTO = datosNegacionDTO;
	}

	/**
	 * @return the lstProgramacionEntrega
	 */
	public List<ProgramacionEntregaVO> getLstProgramacionEntrega() {
		return lstProgramacionEntrega;
	}

	/**
	 * @param lstProgramacionEntrega the lstProgramacionEntrega to set
	 */
	public void setLstProgramacionEntrega(
			List<ProgramacionEntregaVO> lstProgramacionEntrega) {
		this.lstProgramacionEntrega = lstProgramacionEntrega;
	}

	/**
	 * @return the lstGruposAuditores
	 */
	public List<SelectItem> getLstGruposAuditores() {
		return lstGruposAuditores;
	}

	/**
	 * @param lstGruposAuditores the lstGruposAuditores to set
	 */
	public void setLstGruposAuditores(List<SelectItem> lstGruposAuditores) {
		this.lstGruposAuditores = lstGruposAuditores;
	}

	/**
	 * @return the idSolicitud
	 */
	public Integer getIdSolicitud() {
		return idSolicitud;
	}

	/**
	 * @param idSolicitud the idSolicitud to set
	 */
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	/**
	 * @return the idTask
	 */
	public String getIdTask() {
		return idTask;
	}

	/**
	 * @param idTask the idTask to set
	 */
	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	/**
	 * @return the descGrupoAuditor
	 */
	public String getDescGrupoAuditor() {
		return descGrupoAuditor;
	}

	/**
	 * @param descGrupoAuditor the descGrupoAuditor to set
	 */
	public void setDescGrupoAuditor(String descGrupoAuditor) {
		this.descGrupoAuditor = descGrupoAuditor;
	}

	/**
	 * @return the mostrarPopupCambiarDireccionamiento
	 */
	public boolean isMostrarPopupCambiarDireccionamiento() {
		return mostrarPopupCambiarDireccionamiento;
	}

	/**
	 * @param mostrarPopupCambiarDireccionamiento the mostrarPopupCambiarDireccionamiento to set
	 */
	public void setMostrarPopupCambiarDireccionamiento(
			boolean mostrarPopupCambiarDireccionamiento) {
		this.mostrarPopupCambiarDireccionamiento = mostrarPopupCambiarDireccionamiento;
	}

	/**
	 * @return the mostrarPopupCambiarFechaDireccionamiento
	 */
	public boolean isMostrarPopupCambiarFechaDireccionamiento() {
		return mostrarPopupCambiarFechaDireccionamiento;
	}

	/**
	 * @param mostrarPopupCambiarFechaDireccionamiento the mostrarPopupCambiarFechaDireccionamiento to set
	 */
	public void setMostrarPopupCambiarFechaDireccionamiento(
			boolean mostrarPopupCambiarFechaDireccionamiento) {
		this.mostrarPopupCambiarFechaDireccionamiento = mostrarPopupCambiarFechaDireccionamiento;
	}

	/**
	 * @return the cUPS
	 */
	public String getCUPS() {
		return CUPS;
	}

	/**
	 * @param cUPS the cUPS to set
	 */
	public void setCUPS(String cUPS) {
		CUPS = cUPS;
	}

	/**
	 * @return the hospitalizacion
	 */
	public HospitalizacionVO getHospitalizacion() {
		return hospitalizacion;
	}

	/**
	 * @param hospitalizacion the hospitalizacion to set
	 */
	public void setHospitalizacion(HospitalizacionVO hospitalizacion) {
		this.hospitalizacion = hospitalizacion;
	}

	/**
	 * @return the lstRecobros
	 */
	public List<SelectItem> getLstRecobros() {
		return lstRecobros;
	}

	/**
	 * @param lstRecobros the lstRecobros to set
	 */
	public void setLstRecobros(List<SelectItem> lstRecobros) {
		this.lstRecobros = lstRecobros;
	}

	/**
	 * @return the lstPrestaciones
	 */
	public List<PrestacionesAprobadasVO> getLstPrestaciones() {
		return lstPrestaciones;
	}

	/**
	 * @param lstPrestaciones the lstPrestaciones to set
	 */
	public void setLstPrestaciones(List<PrestacionesAprobadasVO> lstPrestaciones) {
		this.lstPrestaciones = lstPrestaciones;
	}

	/**
	 * @return the mEDICAMENTOS
	 */
	public String getMEDICAMENTOS() {
		return MEDICAMENTOS;
	}

	/**
	 * @param mEDICAMENTOS the mEDICAMENTOS to set
	 */
	public void setMEDICAMENTOS(String mEDICAMENTOS) {
		MEDICAMENTOS = mEDICAMENTOS;
	}

	/**
	 * @return the mostrarHospitalizacion
	 */
	public boolean isMostrarHospitalizacion() {
		return mostrarHospitalizacion;
	}

	/**
	 * @param mostrarHospitalizacion the mostrarHospitalizacion to set
	 */
	public void setMostrarHospitalizacion(boolean mostrarHospitalizacion) {
		this.mostrarHospitalizacion = mostrarHospitalizacion;
	}

	/**
	 * @return the lstConceptosGastos
	 */
	public List<ConceptosGastoVO> getLstConceptosGastos() {
		return lstConceptosGastos;
	}

	/**
	 * @param lstConceptosGastos the lstConceptosGastos to set
	 */
	public void setLstConceptosGastos(List<ConceptosGastoVO> lstConceptosGastos) {
		this.lstConceptosGastos = lstConceptosGastos;
	}

	/**
	 * @return the listaCUPSEncontrados
	 */
	public List<ProcedimientosVO> getListaCUPSEncontrados() {
		return listaCUPSEncontrados;
	}

	/**
	 * @param listaCUPSEncontrados the listaCUPSEncontrados to set
	 */
	public void setListaCUPSEncontrados(List<ProcedimientosVO> listaCUPSEncontrados) {
		this.listaCUPSEncontrados = listaCUPSEncontrados;
	}

	/**
	 * @return the consecutivoCUMS
	 */
	public int getConsecutivoCUMS() {
		return consecutivoCUMS;
	}

	/**
	 * @param consecutivoCUMS the consecutivoCUMS to set
	 */
	public void setConsecutivoCUMS(int consecutivoCUMS) {
		this.consecutivoCUMS = consecutivoCUMS;
	}

	/**
	 * @return the consecutivoCUPS
	 */
	public int getConsecutivoCUPS() {
		return consecutivoCUPS;
	}

	/**
	 * @param consecutivoCUPS the consecutivoCUPS to set
	 */
	public void setConsecutivoCUPS(int consecutivoCUPS) {
		this.consecutivoCUPS = consecutivoCUPS;
	}

	/**
	 * @return the mostrarPopupCUPS
	 */
	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	/**
	 * @param mostrarPopupCUPS the mostrarPopupCUPS to set
	 */
	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	/**
	 * @return the procedimientoVOSeleccionado
	 */
	public ProcedimientosVO getProcedimientoVOSeleccionado() {
		return procedimientoVOSeleccionado;
	}

	/**
	 * @param procedimientoVOSeleccionado the procedimientoVOSeleccionado to set
	 */
	public void setProcedimientoVOSeleccionado(
			ProcedimientosVO procedimientoVOSeleccionado) {
		this.procedimientoVOSeleccionado = procedimientoVOSeleccionado;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public String getAltoRiesgo() {
		return altoRiesgo;
	}

	public void setAltoRiesgo(String altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}

	public boolean getMostrarPopupVerDetalleSolicitud() {
		return mostrarPopupVerDetalleSolicitud;
	}

	public void setMostrarPopupVerDetalleSolicitud(
			boolean mostrarPopupVerDetalleSolicitud) {
		this.mostrarPopupVerDetalleSolicitud = mostrarPopupVerDetalleSolicitud;
	}

	public boolean getMostrarPopupVerDetalleAfiliado() {
		return mostrarPopupVerDetalleAfiliado;
	}

	public void setMostrarPopupVerDetalleAfiliado(
			boolean mostrarPopupVerDetalleAfiliado) {
		this.mostrarPopupVerDetalleAfiliado = mostrarPopupVerDetalleAfiliado;
	}

	public boolean getMostrarPopupHistoricoPrestaciones() {
		return mostrarPopupHistoricoPrestaciones;
	}

	public void setMostrarPopupHistoricoPrestaciones(
			boolean mostrarPopupHistoricoPrestaciones) {
		this.mostrarPopupHistoricoPrestaciones = mostrarPopupHistoricoPrestaciones;
	}

	public boolean isMostrarPopupHospitalizacion() {
		return mostrarPopupHospitalizacion;
	}

	public void setMostrarPopupHospitalizacion(boolean mostrarPopupHospitalizacion) {
		this.mostrarPopupHospitalizacion = mostrarPopupHospitalizacion;
	}

	public ConveniosCapitacionVO getConvenioCapacitacionSeleccionado() {
		return convenioCapacitacionSeleccionado;
	}

	public void setConvenioCapacitacionSeleccionado(
			ConveniosCapitacionVO convenioCapacitacionSeleccionado) {
		this.convenioCapacitacionSeleccionado = convenioCapacitacionSeleccionado;
	}

	public boolean getMostrarConfirmacionGuardar() {
		return mostrarConfirmacionGuardar;
	}

	public void setMostrarConfirmacionGuardar(boolean mostrarConfirmacionGuardar) {
		this.mostrarConfirmacionGuardar = mostrarConfirmacionGuardar;
	}

	public boolean isCausaRequerida() {
		return causaRequerida;
	}

	public void setCausaRequerida(boolean causaRequerida) {
		this.causaRequerida = causaRequerida;
	}

	public boolean isFundamentoLegalRequerido() {
		return fundamentoLegalRequerido;
	}

	public void setFundamentoLegalRequerido(boolean fundamentoLegalRequerido) {
		this.fundamentoLegalRequerido = fundamentoLegalRequerido;
	}

	public boolean isAuditorMedicoRequerido() {
		return auditorMedicoRequerido;
	}

	public void setAuditorMedicoRequerido(boolean auditorMedicoRequerido) {
		this.auditorMedicoRequerido = auditorMedicoRequerido;
	}

	public boolean isAlternativaRequerido() {
		return alternativaRequerido;
	}

	public void setAlternativaRequerido(boolean alternativaRequerido) {
		this.alternativaRequerido = alternativaRequerido;
	}

	public boolean getObservacionOPSRequerida() {
		return observacionOPSRequerida;
	}

	public void setObservacionOPSRequerida(boolean observacionOPSRequerida) {
		this.observacionOPSRequerida = observacionOPSRequerida;
	}

	public boolean isMostrarPopupVerGestiones() {
		return mostrarPopupVerGestiones;
	}

	public void setMostrarPopupVerGestiones(boolean mostrarPopupVerGestiones) {
		this.mostrarPopupVerGestiones = mostrarPopupVerGestiones;
	}

	public boolean isMostrarPopupVerAntecedentes() {
		return mostrarPopupVerAntecedentes;
	}

	public void setMostrarPopupVerAntecedentes(boolean mostrarPopupVerAntecedentes) {
		this.mostrarPopupVerAntecedentes = mostrarPopupVerAntecedentes;
	}

	public boolean isIdentificacionDirRequerido() {
		return identificacionDirRequerido;
	}

	public void setIdentificacionDirRequerido(boolean identificacionDirRequerido) {
		this.identificacionDirRequerido = identificacionDirRequerido;
	}

	public boolean isCodigoInternoRequerido() {
		return codigoInternoRequerido;
	}

	public void setCodigoInternoRequerido(boolean codigoInternoRequerido) {
		this.codigoInternoRequerido = codigoInternoRequerido;
	}

	public boolean isSucursalRequerido() {
		return sucursalRequerido;
	}

	public void setSucursalRequerido(boolean sucursalRequerido) {
		this.sucursalRequerido = sucursalRequerido;
	}

	public boolean isFechaEsperadosRequerido() {
		return fechaEsperadosRequerido;
	}

	public void setFechaEsperadosRequerido(boolean fechaEsperadosRequerido) {
		this.fechaEsperadosRequerido = fechaEsperadosRequerido;
	}


	public boolean isMostrarPopupDocumentosSoporteGestion() {
		return mostrarPopupDocumentosSoporteGestion;
	}

	public void setMostrarPopupDocumentosSoporteGestion(
			boolean mostrarPopupDocumentosSoporteGestion) {
		this.mostrarPopupDocumentosSoporteGestion = mostrarPopupDocumentosSoporteGestion;
	}

	public boolean isMostrarPopupConfirmacionDocumentosGestion() {
		return mostrarPopupConfirmacionDocumentosGestion;
	}

	public void setMostrarPopupConfirmacionDocumentosGestion(
			boolean mostrarPopupConfirmacionDocumentosGestion) {
		this.mostrarPopupConfirmacionDocumentosGestion = mostrarPopupConfirmacionDocumentosGestion;
	}

	public SoporteVO getSoporteSeleccionadoGestion() {
		return soporteSeleccionadoGestion;
	}

	public void setSoporteSeleccionadoGestion(SoporteVO soporteSeleccionadoGestion) {
		this.soporteSeleccionadoGestion = soporteSeleccionadoGestion;
	}

	public List<SoporteVO> getListaSoporteGestionVOs() {
		return listaSoporteGestionVOs;
	}

	public void setListaSoporteGestionVOs(List<SoporteVO> listaSoporteGestionVOs) {
		this.listaSoporteGestionVOs = listaSoporteGestionVOs;
	}

	public DocumentoSoporteVO getDocumentoSoporteSeleccionadoGestion() {
		return documentoSoporteSeleccionadoGestion;
	}

	public void setDocumentoSoporteSeleccionadoGestion(
			DocumentoSoporteVO documentoSoporteSeleccionadoGestion) {
		this.documentoSoporteSeleccionadoGestion = documentoSoporteSeleccionadoGestion;
	}

	public boolean isMostrarPopupConfirmacionBorrarArchivoGestion() {
		return mostrarPopupConfirmacionBorrarArchivoGestion;
	}

	public void setMostrarPopupConfirmacionBorrarArchivoGestion(
			boolean mostrarPopupConfirmacionBorrarArchivoGestion) {
		this.mostrarPopupConfirmacionBorrarArchivoGestion = mostrarPopupConfirmacionBorrarArchivoGestion;
	}

	public boolean isMostrarPopupReliquidar() {
		return mostrarPopupReliquidar;
	}

	public void setMostrarPopupReliquidar(boolean mostrarPopupReliquidar) {
		this.mostrarPopupReliquidar = mostrarPopupReliquidar;
	}

	public List<SelectItem> getLstPrestacionesReasignar() {
		return lstPrestacionesReasignar;
	}

	public void setLstPrestacionesReasignar(
			List<SelectItem> lstPrestacionesReasignar) {
		this.lstPrestacionesReasignar = lstPrestacionesReasignar;
	}

	public String getObservacionesReasignar() {
		return observacionesReasignar;
	}

	public void setObservacionesReasignar(String observacionesReasignar) {
		this.observacionesReasignar = observacionesReasignar;
	}

	public Integer getPrestacionesReasignar() {
		return prestacionesReasignar;
	}

	public void setPrestacionesReasignar(Integer prestacionesReasignar) {
		this.prestacionesReasignar = prestacionesReasignar;
	}

	public List<SelectItem> getLstCausalesNoCobroCuota() {
		return lstCausalesNoCobroCuota;
	}

	public boolean isNoCobraCuotaRecuperacion() {
		return noCobraCuotaRecuperacion;
	}

	public void setNoCobraCuotaRecuperacion(boolean noCobraCuotaRecuperacion) {
		this.noCobraCuotaRecuperacion = noCobraCuotaRecuperacion;
	}

	public CiudadVO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadVO ciudad) {
		this.ciudad = ciudad;
	}

	public List<CiudadVO> getListaCiudadesDireccionamiento() {
		return listaCiudadesDireccionamiento;
	}

	public void setListaCiudadesDireccionamiento(
			List<CiudadVO> listaCiudadesDireccionamiento) {
		this.listaCiudadesDireccionamiento = listaCiudadesDireccionamiento;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public boolean isActivarFiltroCiudad() {
		return activarFiltroCiudad;
	}

	public void setActivarFiltroCiudad(boolean activarFiltroCiudad) {
		this.activarFiltroCiudad = activarFiltroCiudad;
	}



}
