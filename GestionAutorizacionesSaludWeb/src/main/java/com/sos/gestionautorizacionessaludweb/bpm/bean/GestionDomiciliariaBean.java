package com.sos.gestionautorizacionessaludweb.bpm.bean;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.AntecedentesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.GestionesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.HistoricoPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.UsuarioVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosNegacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.GestionAuditorDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionAuditoriaIntegralController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionContratacionController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionDomiciliariaController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionInconsistenciasController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionMedicinaTrabajoController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.PrestadorController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.SolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

/**
 * Class GestionDomiciliariaBean
 * 
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionDomiciliariaBean implements Serializable {
	private static final long serialVersionUID = 1249453608676726443L;
	/**
	 * Variable instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionDomiciliariaBean.class);
	private static final int LONGITUD_1 = 1;
	private static final int LONGITUD_4 = 4;
	/**
	 * Variable gestion de mensajes
	 */
	

	/**
	 * VO's
	 */
	private PrestacionesAprobadasVO prestacionesAprobadasDTOTemp;
	private PrestacionesAprobadasVO prestacionesAprobadasDTOCUMS;
	private DireccionamientoVO direccionamientoDTOCUMS;
	private DireccionamientoVO cambioFecha;
	private DireccionamientoVO cambioDireccionamiento;
	private MedicamentosVO medicamentoVOSeleccionado;
	private DiagnosticosVO diagnosticoVoSeleccionadoDomi;
	private SolicitudBpmVO solicitudVO;
	private AfiliadoVO afiliadoVO;
	private PrestacionesAprobadasVO prestacionNuevaDomi;
	private AlternativasVO alternativaSel;
	private DocumentoSoporteVO documentoSoporteVOSeleccionadoAuditoria;
	private HospitalizacionVO hospitalizacionDomi;

	/**
	 * Listas desplegables
	 */
	private List<SelectItem> listaHallazgosCUMS;
	private List<SelectItem> listaLateralidades;
	private List<SelectItem> listaDosisMedicamentos;
	private List<SelectItem> listaFrecuenciaMedicamentos;
	private List<SelectItem> listaEstadosCUMS;
	private List<SelectItem> lstMedicosAuditores;
	private List<UsuarioVO> lstMedicosAuditoresTemp;
	private List<SelectItem> listaFundamentosCUMS;
	private List<SelectItem> listaTiposPrestaciones;
	private List<SelectItem> listaViasAccesoMedicamento;
	private List<SelectItem> lstPrestadoresDireccionamiento;
	private List<SelectItem> lstGruposAuditores;
	private List<SelectItem> lstRiesgos;
	private List<SelectItem> listaCausasCUMS;
	private List<SelectItem> lstMotivos;
	private List<SelectItem> lstMotivosPrestacionesDomi;
	private List<SelectItem> lstPrestacionesReasignarDomi = new ArrayList<SelectItem>();

	/**
	 * Listas de valores
	 */
	private List<AlternativasVO> listaAlternativasCUMS;
	private List<EmpleadorVO> listaEmpleadores;
	private List<ConveniosCapitacionVO> listaConveniosCapacitacion;
	private List<DiagnosticosVO> listaDiagnosticosEncontrados;
	private List<DiagnosticoDTO> listaDiagnosticosSeleccionados;
	private List<GestionesPrestacionVO> lstGestionesPrestacion;
	private List<HistoricoPrestacionVO> lstHistoricosPrestacionDomi;
	private List<AntecedentesPrestacionVO> lstAntecedentesPrestacion;
	private List<PrestacionesAprobadasVO> lstPrestacionesSolicitadasDomi;
	private List<PrestacionesAprobadasVO> lstPrestaciones;
	private List<MedicamentosVO> listaMedicamentosEncontrados;
	private List<DocumentoSoporteVO> listaDocumentosDomi;
	private List<ProgramacionEntregaVO> lstProgramacionEntrega;
	private ConveniosCapitacionVO convenioCapacitacionSeleccionado;
	private List<CiudadVO> listaCiudadesDireccionamiento = new ArrayList<CiudadVO>();

	/**
	 * DTO's
	 */
	private GestionAuditorDTO gestionAuditorDTO;
	private DatosNegacionDTO datosNegacionDTO;
	private ClienteRestBpmBean clienteRestBpmBean;

	/**
	 * Variables del MB
	 */
	private Date fechaConsulta;
	private int TIPO_ESTADO_NEGOCIO;
	private String CUMS;
	private boolean mostrarPopupHospitalizacionDomi;
	private Integer consecutivoPrestacionGlobal;
	private boolean checkDiagnosticoPpalDomi;
	private boolean checkDiagnosticoSOSDomi;
	private DiagnosticoDTO diagnosticoDTOSeleccionado;
	private boolean mostrarPopupDiagnosticos;
	private String codigoPrestacion;
	private String pestanaSeleccionada;
	private String txtCodigoDiagnostico;
	private String descripcionPrestacion;
	private Integer tipoPrestacionSeleccionado;
	private boolean mostrarPopupMedicamentos;
	private boolean mostrarPopupAdicionarPrestacion;
	private boolean mostrarPopupDocumentosSoporteAuditoriaDomi;
	private boolean verPestañaCums;
	private String txtDescripcionDiagnostico;
	private String alternativa;
	private Integer consecutivoPrestacion;
	private Integer idSolicitud;
	private String idTask;
	private String userBpmDomi;
	private String tipoAuditor;
	private String descGrupoAuditor;
	private boolean mostrarPopupCambiarDireccionamientoDomi;
	private boolean mostrarPopupCambiarFechaDireccionamientoDomi;
	private boolean mostrarBotonHospitalizacionDomi;
	private boolean mostrarHistoricoPrestacionesDomi;
	private boolean mostrarVerDetalleSolicitudDomi;
	private boolean mostrarVerDetalleAfiliadoDomi;
	private boolean mostrarConfirmacionGuardarDomi;
	private boolean causaRequeridaDomi;
	private boolean fundamentoLegalRequeridoDomi = false;
	private boolean auditorMedicoRequeridoDomi = false;
	private boolean alternativaRequeridoDomi = false;
	private boolean observacionOPSRequeridaDomi = false;
	private boolean mostrarPopupVerGestionesDomi = false;
	private boolean mostrarPopupVerAntecedentesDomi = false;
	private boolean mostrarPopupReliquidarDomi;
	private String altoRiesgo;
	private String fechaSolicitud;
	private Integer prestacionesReasignar;
	private String observacionesReasignar;
	
	private CiudadVO ciudad;
	private boolean mostrarPopupCiudades;	
	private boolean activarFiltroCiudad;

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {

		try {
			iniciarVariables();
			String idSolicitudStr = (String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
			idTask = (String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
			clienteRestBpmBean = (ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
			userBpmDomi = (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);
			tipoAuditor = (String) FacesUtil.getRequestParameter(ConstantesWeb.TIPO_AUDITOR);
			userBpmDomi = (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);

			this.idSolicitud = new Integer(idSolicitudStr);
			if (tipoAuditor.equals(ConstantesWeb.ID_DOMICILIARIA)) {

				GestionInconsistenciasController gestionInconsistenciasController = new GestionInconsistenciasController();
				CargarCombosController cargarCombosController = new CargarCombosController();
				GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController();
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				GestionContratacionController gestionContratacionController = new GestionContratacionController();

				List<Object> afiliadoYSolicitud = gestionInconsistenciasController.consultaAfiliadoYSolicitud(idSolicitud);
				afiliadoVO = (AfiliadoVO) afiliadoYSolicitud.get(0);
				solicitudVO = (SolicitudBpmVO) afiliadoYSolicitud.get(1);

				convenioCapacitacionSeleccionado = new ConveniosCapitacionVO();
				SimpleDateFormat formatoFecha = new SimpleDateFormat(ConstantesWeb.FORMATO_FECHA);
				fechaSolicitud = formatoFecha.format(solicitudVO.getFechaSolicitud());

				lstRiesgos = FuncionesAppWeb
						.obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());

				altoRiesgo = afiliadoVO.isAltoRiesgo() == true ? ConstantesWeb.ALTO_RIESGO : "";

				cambioFecha = new DireccionamientoVO();
				cambioDireccionamiento = new DireccionamientoVO();
				gestionAuditorDTO = new GestionAuditorDTO();
				datosNegacionDTO = new DatosNegacionDTO();

				listaDiagnosticosSeleccionados = gestionMedicinaTrabajoController.consultaDiagnosticos(idSolicitud);

				if (ConstantesWeb.CODIGO_ORIGEN_ATENCION_HOSPITALIZACION
						.equals(solicitudVO.getCodigoOrigenAtencion())) {
					mostrarBotonHospitalizacionDomi = true;
				}

				lstGruposAuditores = FuncionesAppWeb.obtenerListaSelectGruposAuditores(
						gestionDomiciliariaController.consultarGruposAuditores(idSolicitud, ConstantesWeb.CONS_DOMI));

				lstPrestacionesSolicitadasDomi = gestionContratacionController
						.consultaPrestacionesAprobadas(idSolicitud, ConstantesWeb.DESCRIPCION_CUMS);
				llenarListadoPrestacionesSolicitadasDomi();
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	public void verGestionesDomi() {
		mostrarPopupVerGestionesDomi = true;
	}

	public void cerrarVerGestionesDomi() {
		mostrarPopupVerGestionesDomi = false;
	}

	public void verAntecedentesDomi() {
		mostrarPopupVerAntecedentesDomi = true;
	}

	public void cerrarVerAntecedentesDomi() {
		mostrarPopupVerAntecedentesDomi = false;
	}

	public void verConfirmacionGuardarDomi() {
		mostrarConfirmacionGuardarDomi = true;
	}

	public void verSolicitudDetalladaDomi() {
		try {
			mostrarVerDetalleSolicitudDomi = true;
			SolicitudController solicitudController = new SolicitudController();
			SolicitudBpmVO solicitudBpmVODomi = solicitudController.consultarDetalleSolicitudBPM(idSolicitud);
			solicitudVO.setMedicoSolicitante(solicitudBpmVODomi.getMedicoSolicitante());
			solicitudVO.setPrestadorSolicitante(solicitudBpmVODomi.getPrestadorSolicitante());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo cerrar popup */
	public void cerrarVerSolicitudDetalladaDomi() {
		mostrarVerDetalleSolicitudDomi = false;
	}

	public void verHospitalizacionDomi() {
		try {
			ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
			hospitalizacionDomi = consultarSolicitudControllerAuditoria
					.consultaDetalleSolicitud(solicitudVO.getNumeroRadicado()).get(0).getHospitalizacionVO();
			mostrarPopupHospitalizacionDomi = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void cerrarHospitalizacionDomi() {
		mostrarPopupHospitalizacionDomi = false;
	}

	public void verHistoricoPrestacionesDomi() {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			lstHistoricosPrestacionDomi = consultaAfiliadoController.consultarHistoricosPrestacion(
					afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion(),
					afiliadoVO.getNumeroIdentificacion());
			mostrarHistoricoPrestacionesDomi = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void limpiarRiesgoDomi() {
		lstRiesgos = new ArrayList<SelectItem>();
		try {
			if (!afiliadoVO.isAltoRiesgo()) {
				afiliadoVO.setConsecutivoCodRiesgo(0);
			} else {
				CargarCombosController cargarCombosController = new CargarCombosController();
				lstRiesgos = FuncionesAppWeb
						.obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarHistoricoPrestacionesDomi() {
		mostrarHistoricoPrestacionesDomi = false;
	}

	/** Metodo cerrar popup */
	public void cerrarPopupDocumentosSoporte() {
		mostrarPopupDocumentosSoporteAuditoriaDomi = false;
	}

	/** Metodo abrir popup */
	public void verPopupSoportesAuditoriaDomi() {
		try {
			ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
			ControladorVisosServiceEJB controladorVisosServiceEJBAuditoria = new ControladorVisosServiceEJB();
			List<DocumentoSoporteVO> listaIdDocumentosDomi = consultarSolicitudControllerAuditoria
					.consultaDocumentosSoportexSolicitud(solicitudVO.getNumeroSolicitud());
			controladorVisosServiceEJBAuditoria.crearConexionVisosService();
			listaDocumentosDomi = controladorVisosServiceEJBAuditoria.consultarDocumentoAnexoxIdDocumento(
					listaIdDocumentosDomi.get(0).getConsecutivoDocumentoSoporteVisos());
			for (int i = 0; i < listaDocumentosDomi.size(); i++) {
				listaDocumentosDomi.get(i).setDescripcionTipoDocumentoSoporte(
						listaIdDocumentosDomi.get(i).getDescripcionTipoDocumentoSoporte());
			}
			mostrarPopupDocumentosSoporteAuditoriaDomi = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo Descargar documentos soportes */
	public void descargarSoporteAuditoria() {
		try {
			FuncionesAppWeb.descargarSoporteAuditoria(documentoSoporteVOSeleccionadoAuditoria.getNombreDocumento(),
					documentoSoporteVOSeleccionadoAuditoria.getDatosArchivoSoporte());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Cargar detalle del afiliado */
	public void cargarDetalleAfiliadoDomi() {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController
					.consultaRespuestaServicioAfiliado(
							afiliadoVO.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion(),
							afiliadoVO.getNumeroIdentificacion(), afiliadoVO.getPlan().getDescripcionPlan(),
							fechaConsulta, FacesUtils.getUserName());

			if (resultadoConsultaAfiliadoDatosAdicionales != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() != 0) {

				afiliadoVO = FuncionesAppWeb.datosAdicionalesAfiliadoToAfiliadoVO(
						resultadoConsultaAfiliadoDatosAdicionales.getAfiliado(), afiliadoVO);
				listaEmpleadores = consultaAfiliadoController
						.consultaInformacionEmpleadorAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
				listaConveniosCapacitacion = consultaAfiliadoController
						.consultaConveniosCapitacion(resultadoConsultaAfiliadoDatosAdicionales);
			} else if (resultadoConsultaAfiliadoDatosAdicionales != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() == 0) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_AFILIADO_NO_EXISTE"),
						ErrorType.PARAMETRO_ERRADO);
			}
			mostrarVerDetalleAfiliadoDomi = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para cerrar Poppup */
	public void cerrarDetalleAfiliadoDomi() {
		mostrarVerDetalleAfiliadoDomi = false;
	}

	/**
	 * 
	 */
	private void validacionEstadoDevueltoDomi() {
		verPestañaCums = gestionAuditorDTO.getConsecutivoEstadoPrestacion()
				.intValue() == ConstantesWeb.COD_ESTADO_DEVUELTO ? false : true;
	}

	/** Metodo para Inicializar variables */
	private void iniciarVariables() {
		try {
			lstRiesgos = new ArrayList<SelectItem>();
			listaEmpleadores = new ArrayList<EmpleadorVO>();
			listaConveniosCapacitacion = new ArrayList<ConveniosCapitacionVO>();
			lstProgramacionEntrega = new ArrayList<ProgramacionEntregaVO>();
			listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
			listaDocumentosDomi = new ArrayList<DocumentoSoporteVO>();
			listaDiagnosticosEncontrados = new ArrayList<DiagnosticosVO>();
			listaDiagnosticosSeleccionados = new ArrayList<DiagnosticoDTO>();
			lstGestionesPrestacion = new ArrayList<GestionesPrestacionVO>();
			listaAlternativasCUMS = new ArrayList<AlternativasVO>();
			lstHistoricosPrestacionDomi = new ArrayList<HistoricoPrestacionVO>();
			lstAntecedentesPrestacion = new ArrayList<AntecedentesPrestacionVO>();
			lstPrestacionesSolicitadasDomi = new ArrayList<PrestacionesAprobadasVO>();
			lstPrestaciones = new ArrayList<PrestacionesAprobadasVO>();
			listaCiudadesDireccionamiento	= new ArrayList<CiudadVO>();

			listaTiposPrestaciones = new ArrayList<SelectItem>();
			listaHallazgosCUMS = new ArrayList<SelectItem>();
			listaEstadosCUMS = new ArrayList<SelectItem>();
			lstMedicosAuditores = new ArrayList<SelectItem>();
			lstMedicosAuditoresTemp = new ArrayList<UsuarioVO>();
			lstRiesgos = new ArrayList<SelectItem>();
			listaCausasCUMS = new ArrayList<SelectItem>();
			listaFundamentosCUMS = new ArrayList<SelectItem>();
			listaTiposPrestaciones = new ArrayList<SelectItem>();
			listaViasAccesoMedicamento = new ArrayList<SelectItem>();
			listaLateralidades = new ArrayList<SelectItem>();
			listaDosisMedicamentos = new ArrayList<SelectItem>();
			listaFrecuenciaMedicamentos = new ArrayList<SelectItem>();
			lstMotivos = new ArrayList<SelectItem>();
			lstMotivosPrestacionesDomi = new ArrayList<SelectItem>();
			lstPrestadoresDireccionamiento = new ArrayList<SelectItem>();
			lstGruposAuditores = new ArrayList<SelectItem>();

			TIPO_ESTADO_NEGOCIO = Integer.parseInt(Messages.getValorParametro("TIPO_ESTADO_NEGOCIO"));
			CUMS = Messages.getValorParametro(ConstantesWeb.DESCRIPCION_CUMS);
			fechaConsulta = new Date();
			prestacionNuevaDomi = new PrestacionesAprobadasVO();
			gestionAuditorDTO = new GestionAuditorDTO();
			datosNegacionDTO = new DatosNegacionDTO();
			ciudad = new CiudadVO(); 

		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	private void validarGuardar() throws LogicException {
		if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null) {
			if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_APROBADA) {
				if (gestionAuditorDTO.getJustificacion().length() < 10) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_MINIMO_OSB"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
		}
		if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null) {
			if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NEGADA
					|| gestionAuditorDTO
							.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_EN_REMISION_ADMINISTRATIVA
					|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NO_AUTORIZADA
					|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_ANULADA
					|| gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_DEVUELTA) {
				if (gestionAuditorDTO.getLstCausas().isEmpty()) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_CAUSAS_OBLIGATORIO"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
		}
		if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null) {
			if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CODIGO_ESTADO_NO_AUTORIZADA) {
				if (datosNegacionDTO.getLstFundamentoLegal().isEmpty()
						|| datosNegacionDTO.getAuditorMedico().getNumeroUnicoIdentificacionMedico() == null
						|| listaAlternativasCUMS.isEmpty()) {
					throw new LogicException(
							Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_DATOS_NEGACION_OBLIGATORIO"),
							ErrorType.PARAMETRO_ERRADO);
				} else {
					validarUsuarioAuditor();
				}
			}
		}
	}

	/** Metodo Guardar los cambios */
	public void guardar() {
		try {
			if (listaDiagnosticosSeleccionados.isEmpty() || !existeDiagnosticoPpal()) {
				throw new LogicException(
						Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_DIAGNOSTICO_PPAL_OBLIGATORIO"),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (gestionAuditorDTO.getJustificacion() != null) {
				if (gestionAuditorDTO.getJustificacion().length() < 10) {
					throw new LogicException(
							Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_MINIMO_JUSTIFICACION"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}

			validarGuardar();

			datosNegacionDTO.setLstAlternativas(listaAlternativasCUMS);
			if (prestacionesAprobadasDTOCUMS == null) {
				prestacionesAprobadasDTOCUMS = new PrestacionesAprobadasVO();
			}

			if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() != null) {
				validacionEstadoDevueltoDomi();
			}

			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			GestionContratacionController gestionContratacionController = new GestionContratacionController();

			gestionAuditorDTO
					.setDosis(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsPosologiaCUMS()));
			gestionAuditorDTO.setPosologiaCUMSGeneral(
					Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsecutivoCodigoDosis()));
			gestionAuditorDTO.setCadaGeneral(Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getCada()));
			gestionAuditorDTO.setFrecuenciaGeneral(
					Utilidades.validarNullCadena(prestacionesAprobadasDTOCUMS.getConsecutivoFrecuencia()));
			solicitudVO.setLstDiagnosticos(listaDiagnosticosSeleccionados);
			gestionDomiciliariaController.guardarGestionDomiciliaria(afiliadoVO, solicitudVO, lstPrestaciones,
					consecutivoPrestacionGlobal, gestionAuditorDTO, datosNegacionDTO,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			lstPrestacionesSolicitadasDomi = gestionContratacionController.consultaPrestacionesAprobadas(idSolicitud,
					ConstantesWeb.DESCRIPCION_CUMS);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para reasignar la solicitud */
	public String reasignar() {
		String url = "";
		try {
			if (descGrupoAuditor != null) {
				clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.REASIGNAR_TAREA,
						descGrupoAuditor);
				url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
			} else {
				throw new LogicException(
						Messages.getValorExcepcion("ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO"),
						ErrorType.PARAMETRO_ERRADO);
			}

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/** Metodo para asociar programacion */
	public void asociarprogramacion() {
		try {
			if (!lstProgramacionEntrega.isEmpty()) {
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				gestionDomiciliariaController.asociarProgramacionEntrega(idSolicitud,
						prestacionesAprobadasDTOTemp.getConscodigoPrestacion(),
						afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion(),
						afiliadoVO.getNumeroIdentificacion(), userBpmDomi);
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
			} else {
				throw new LogicException(
						Messages.getValorExcepcion("ERROR_VALIDACION_ASOCIAR_PROGRAMACION_SIN_PROGRAMACION"),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	private void validarUsuarioAuditor() {
		String consecutivoDomi;
		String usuarioId;
		for (int i = 0; i < lstMedicosAuditoresTemp.size(); i++) {
			usuarioId = datosNegacionDTO.getAuditorMedico().getNumeroUnicoIdentificacionMedico();
			consecutivoDomi = lstMedicosAuditoresTemp.get(i).getConsecutivo().toString();
			if (usuarioId.equals(consecutivoDomi)) {
				datosNegacionDTO.getAuditorMedico()
						.setNumeroIdentificacionMedico(lstMedicosAuditoresTemp.get(i).getLogin());
			}
		}
	}

	/** Metodo para terminar la solicitu */
	public String terminarTarea() {
		String url = "";
		try {
			guardar();

			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.TERMINA_TAREA,
					ConstantesWeb.CARACTER_SEPARADOR);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
		return url;
	}

	/** Metodo para liberar la solicitu */
	public String liberarTarea() {
		String url = "";
		try {

			clienteRestBpmBean.liberarTarea(idTask, idSolicitud.toString(), ConstantesWeb.LIBERAR_TAREA,
					ConstantesWeb.CARACTER_SEPARADOR);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
		return url;
	}

	/** Metodo para limpiar el formulario busqueda prestacion */
	public void limpiarBusquedaPrestacion() {
		prestacionNuevaDomi.setTipoPrestacion(null);
		prestacionNuevaDomi.setConscodigoPrestacion(null);
		prestacionNuevaDomi.setDescripcion(null);
	}

	/** Metodo para limpiar el formulario agregar prestacion */
	public void limpiarPanelAgregarPrestacion() {
		prestacionNuevaDomi = new PrestacionesAprobadasVO();
	}

	/** Metodo cerrar busqueda diagnostico */
	public void cerrarPopupDiagnostico() {
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	private void gestionarbuscarDiagnostico() throws LogicException {
		RegistrarSolicitudController registrarSolicitudesController;
		try {
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}

		if (txtCodigoDiagnostico != null && !txtCodigoDiagnostico.trim().equals("")) {
			if (txtCodigoDiagnostico.length() > LONGITUD_1) {
				listaDiagnosticosEncontrados = registrarSolicitudesController
						.consultaDiagnosticosxCodigo(txtCodigoDiagnostico.trim());
			} else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_MIN_CODIGO"),
						ErrorType.PARAMETRO_ERRADO);
			}
		} else if (txtDescripcionDiagnostico != null && !txtDescripcionDiagnostico.trim().equals("")) {
			if (txtDescripcionDiagnostico.length() > LONGITUD_4) {
				listaDiagnosticosEncontrados = registrarSolicitudesController
						.consultaDiagnosticosxDescripcion(txtDescripcionDiagnostico);
			} else {
				throw new LogicException(
						Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_MIN_CARACTER"),
						ErrorType.PARAMETRO_ERRADO);
			}
		} else {
			throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_BUSQUEDA_SEL_ALGUNO"),
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	/** Metodo para buscar un diagnostico */
	public void buscarDiagnostico() {
		try {
			listaDiagnosticosEncontrados = null;
			diagnosticoVoSeleccionadoDomi = null;

			gestionarbuscarDiagnostico();

			txtDescripcionDiagnostico = "";
			txtCodigoDiagnostico = "";

			if (listaDiagnosticosEncontrados.size() == 1) {
				diagnosticoVoSeleccionadoDomi = listaDiagnosticosEncontrados.get(0);
				txtCodigoDiagnostico = diagnosticoVoSeleccionadoDomi.getCodigoDiagnostico();
				txtDescripcionDiagnostico = diagnosticoVoSeleccionadoDomi.getDescripcionDiagnostico();
				mostrarPopupDiagnosticos = false;
			} else {
				mostrarPopupDiagnosticos = true;
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo seleccionar busqueda diagnostico */
	public void seleccionarDiagnostico() {
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
		txtCodigoDiagnostico = diagnosticoVoSeleccionadoDomi.getCodigoDiagnostico();
		txtDescripcionDiagnostico = diagnosticoVoSeleccionadoDomi.getDescripcionDiagnostico();

	}

	/**
	 * 
	 */
	private void validacionCausaRequeridaDomi() {
		if (prestacionesAprobadasDTOTemp.getConsEstadoPrestacion() != ConstantesWeb.CODIGO_ESTADO_APROBADA) {
			causaRequeridaDomi = true;
		} else {
			causaRequeridaDomi = false;
		}
	}

	private void gestionarDiagnosricos() throws LogicException {
		diagnosticoVoSeleccionadoDomi.setCodigoDiagnostico(diagnosticoVoSeleccionadoDomi.getCodigoDiagnostico().trim());
		for (DiagnosticoDTO obj : listaDiagnosticosSeleccionados) {
			if (diagnosticoVoSeleccionadoDomi.getCodigoDiagnostico().equals(obj.getCodigo().trim())) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO"),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (checkDiagnosticoPpalDomi) {
				if (obj.getPrincipal().equals(ConstantesWeb.SI)
						|| obj.getPrincipal().toUpperCase().equals(ConstantesWeb.DIAGNOSTICO_PPAL)) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_PRINCIPAL"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
			if (checkDiagnosticoSOSDomi) {
				if (obj.getAsignadoSOS().equals(ConstantesWeb.SI)) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_DIAGNOSTICO_ASIGNADO_SOS"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
		}
	}

	/** Metodo adicionar un diagnostico en busqueda diagnostico */
	public void adicionarDiagnostico() {
		try {
			if (diagnosticoVoSeleccionadoDomi == null) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO"),
						ErrorType.PARAMETRO_ERRADO);
			}
			gestionarDiagnosricos();
			DiagnosticoDTO diagnosticoDTODomi = new DiagnosticoDTO();
			diagnosticoDTODomi.setDiagnosticosVO(diagnosticoVoSeleccionadoDomi);
			diagnosticoDTODomi.setCodigo(diagnosticoVoSeleccionadoDomi.getCodigoDiagnostico());
			diagnosticoDTODomi.setAsignadoSOS(checkDiagnosticoSOSDomi == true ? ConstantesWeb.SI : ConstantesWeb.NO);
			diagnosticoDTODomi.setPrincipal(checkDiagnosticoPpalDomi == true ? ConstantesWeb.SI : ConstantesWeb.NO);
			diagnosticoDTODomi.setDescripcion(diagnosticoVoSeleccionadoDomi.getDescripcionDiagnostico());
			listaDiagnosticosSeleccionados.add(diagnosticoDTODomi);
			diagnosticoVoSeleccionadoDomi = null;
			checkDiagnosticoPpalDomi = false;
			checkDiagnosticoSOSDomi = false;
			txtCodigoDiagnostico = "";
			txtDescripcionDiagnostico = "";
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/** Metodo eliminar un diagnostico en busqueda diagnostico */
	public void elimiarDiagnosticoSeleccionado() {
		listaDiagnosticosSeleccionados.remove(diagnosticoDTOSeleccionado);
	}

	/** Metodo Cargar info prestacion seleccionada */
	public void cargarPrestacion() {
		try {
			consecutivoPrestacionGlobal = Integer
					.parseInt(prestacionesAprobadasDTOTemp.getConscodigoPrestacionGSA().trim());

			validacionCausaRequeridaDomi();
			pestanaSeleccionada = CUMS;
			/** Panel Solicitud */
			prestacionesAprobadasDTOCUMS = prestacionesAprobadasDTOTemp;

			CargarCombosController cargarCombosController = new CargarCombosController();
			GestionInconsistenciasController gestionInconsistenciasController = new GestionInconsistenciasController();
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			PrestacionController prestacionController = new PrestacionController();
			PrestadorController prestadorController = new PrestadorController();

			listaDosisMedicamentos = CargarCombosUtil.obtenerListaSelectDosisMedicamentos(null, cargarCombosController);
			listaFrecuenciaMedicamentos = CargarCombosUtil.obtenerListaSelectFrecuenciaMedicamentos(null,
					cargarCombosController);

			listaHallazgosCUMS = gestionInconsistenciasController.consultaIncosistencias(idSolicitud,
					ConstantesWeb.CONS_ESTADO_AUDITORIA_MALLA);
			/** Panel Direccionamiento */
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud,
					consecutivoPrestacionGlobal);
			/** Cunsulta de Estados **/
			listaEstadosCUMS = gestionDomiciliariaController.consultaEstados();
			/** Causas */
			gestionAuditorDTO.setConsecutivoEstadoPrestacion(prestacionesAprobadasDTOTemp.getConsEstadoPrestacion());
			listaCausasCUMS = FuncionesAppWeb.obtenerListaSelectCausas(gestionDomiciliariaController
					.consultaCausas(gestionAuditorDTO.getConsecutivoEstadoPrestacion(), consecutivoPrestacionGlobal));

			/** Fundamentos */
			listaFundamentosCUMS = FuncionesAppWeb.obtenerListaSelectFundamentoLegal(
					gestionDomiciliariaController.consultaFundamentos(idSolicitud, consecutivoPrestacionGlobal));
			/** Alternativas **/
			listaAlternativasCUMS = gestionDomiciliariaController.consultaAlternativas(consecutivoPrestacionGlobal);
			lstAntecedentesPrestacion = prestacionController.consultarAntecedentesPrestacion(idSolicitud,
					consecutivoPrestacionGlobal);
			lstGestionesPrestacion = prestacionController.consultarGestionesPrestacion(consecutivoPrestacionGlobal);
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadorController.consultarPrestadoresDireccionamiento(
					prestacionesAprobadasDTOTemp.getConscodigoPrestacion(), 
					new Integer(prestacionesAprobadasDTOTemp.getPlanAfiliado()), 
					prestacionesAprobadasDTOTemp.getConsecutivoCiudad(),
					idSolicitud));

			lstMedicosAuditoresTemp = gestionDomiciliariaController.consultarMedicosAuditores(userBpmDomi);
			lstMedicosAuditores = FuncionesAppWeb.obtenerListaSelectMedicosAuditores(lstMedicosAuditoresTemp);

			lstProgramacionEntrega = gestionDomiciliariaController.consultarProgramacionEntrega(
					afiliadoVO.getNumeroUnicoIdentificacion(),
					new Integer(prestacionesAprobadasDTOTemp.getCodigoTipoPrestacion()),
					prestacionesAprobadasDTOTemp.getConscodigoPrestacion());

			verPestañaCums = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo Cargar info de las causas de la malla */
	public void cambiarCausas() {
		try {
			causaRequeridaDomi = gestionAuditorDTO
					.getConsecutivoEstadoPrestacion() == ConstantesWeb.CONS_ESTADO_APROBADO ? false : true;
			observacionOPSRequeridaDomi = gestionAuditorDTO.getConsecutivoEstadoPrestacion()
					.intValue() == ConstantesWeb.CONS_ESTADO_APROBADO ? true : false;
			if (gestionAuditorDTO.getConsecutivoEstadoPrestacion() == ConstantesWeb.CONS_ESTADO_NO_AUTORIZADO) {
				fundamentoLegalRequeridoDomi = true;
				auditorMedicoRequeridoDomi = true;
				alternativaRequeridoDomi = true;
			} else {
				fundamentoLegalRequeridoDomi = false;
				auditorMedicoRequeridoDomi = false;
				alternativaRequeridoDomi = false;
			}

			consecutivoPrestacionGlobal = Integer
					.parseInt(prestacionesAprobadasDTOTemp.getConscodigoPrestacionGSA().trim());
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			listaCausasCUMS = FuncionesAppWeb.obtenerListaSelectCausas(gestionDomiciliariaController
					.consultaCausas(gestionAuditorDTO.getConsecutivoEstadoPrestacion(), consecutivoPrestacionGlobal));
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para agregar una prestacion */
	public void adicionarPrestacion() {
		try {

			if (medicamentoVOSeleccionado == null) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_SELECCIONAR_PRESTACION"),
						ErrorType.PARAMETRO_ERRADO);
			}

			if (prestacionNuevaDomi.getCodigoTipoPrestacion() == null || prestacionNuevaDomi.getCantidad() == null
					|| prestacionNuevaDomi.getConsecutivoMotivo() == null) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION"),
						ErrorType.PARAMETRO_ERRADO);
			}
			medicamentoVOSeleccionado.setCodigoCodificacionMedicamento(
					medicamentoVOSeleccionado.getCodigoCodificacionMedicamento().trim());
			for (PrestacionesAprobadasVO obj : lstPrestacionesSolicitadasDomi) {
				if (medicamentoVOSeleccionado.getCodigoCodificacionMedicamento()
						.equals(obj.getCodigoPrestacion().trim())) {
					throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_ADICIONADA"),
							ErrorType.PARAMETRO_ERRADO);
				}
			}

			prestacionNuevaDomi.setCodigoPrestacion(codigoPrestacion);
			prestacionNuevaDomi.setConscodigoPrestacion(consecutivoPrestacion);
			prestacionNuevaDomi.setDescripcion(descripcionPrestacion);
			prestacionNuevaDomi.setRequiereAuditoria("SI");
			prestacionNuevaDomi.setEstadoPrestacion("AUDITORIA");
			prestacionNuevaDomi.setVerDetalle(false);
			prestacionNuevaDomi.setTipoPrestacion(getDescTipoPrestacion(prestacionNuevaDomi.getCodigoTipoPrestacion()));
			prestacionNuevaDomi.setConsTipoPrestacion(Integer.parseInt(prestacionNuevaDomi.getCodigoTipoPrestacion()));
			guardarPrestacionDomi();

			lstPrestacionesSolicitadasDomi.add(prestacionNuevaDomi);
			lstPrestaciones.add(prestacionNuevaDomi);
			prestacionNuevaDomi = new PrestacionesAprobadasVO();
			mostrarPopupAdicionarPrestacion = false;
			consecutivoPrestacion = null;
			codigoPrestacion = null;
			descripcionPrestacion = null;
			GestionContratacionController gestionContratacionController = new GestionContratacionController();
			lstPrestacionesSolicitadasDomi = gestionContratacionController.consultaPrestacionesAprobadas(idSolicitud,
					ConstantesWeb.DESCRIPCION_CUMS);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void guardarPrestacionDomi() throws LogicException {
		GestionAuditoriaIntegralController gestionAuditoriaIntegralControllerDomi;
		try {
			gestionAuditoriaIntegralControllerDomi = new GestionAuditoriaIntegralController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		gestionAuditoriaIntegralControllerDomi.guardarPrestacionAuditoriaIntegral(prestacionNuevaDomi, idSolicitud,
				userBpmDomi);
	}

	/** Metodo para obtener la descripcion del tipo prestacion */
	private String getDescTipoPrestacion(String codigoTipoPrestacion) {
		String descripcion = "";
		for (SelectItem object : listaTiposPrestaciones) {
			if (object.getValue() != null && codigoTipoPrestacion.equals(object.getValue().toString())) {
				descripcion = object.getLabel();
				break;
			}
		}
		return descripcion;

	}

	/** Metodo cerrar popup prestaciones */
	public void cerrarPopupMedicamentos() {
		mostrarPopupMedicamentos = false;
		listaMedicamentosEncontrados = null;
	}

	/** Metodo para buscar la prestacion */
	public void buscarPrestacionMedicamentos() {
		try {
			com.sos.gestionautorizacionessaludejb.controller.PrestacionController prestacionControllerEjb = new com.sos.gestionautorizacionessaludejb.controller.PrestacionController();
			mostrarPopupMedicamentos = false;
			if (codigoPrestacion != null && !codigoPrestacion.trim().equals("")) {
				if (codigoPrestacion.length() > 1) {
					listaMedicamentosEncontrados = prestacionControllerEjb
							.consultarPrestacionesMedicamentosPorParametros(
									new MedicamentosVO(codigoPrestacion, descripcionPrestacion));
				} else {

					throw new LogicException(
							Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO"),
							ErrorType.PARAMETRO_ERRADO);
				}
			} else if (descripcionPrestacion != null && !descripcionPrestacion.trim().equals("")) {
				if (descripcionPrestacion.length() > 4) {
					listaMedicamentosEncontrados = prestacionControllerEjb
							.consultarPrestacionesMedicamentosPorParametros(
									new MedicamentosVO(codigoPrestacion, descripcionPrestacion));
				} else {

					throw new LogicException(
							Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER"),
							ErrorType.PARAMETRO_ERRADO);
				}
			} else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO"),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (listaMedicamentosEncontrados.size() > 1) {
				mostrarPopupMedicamentos = true;
			} else if (listaMedicamentosEncontrados.size() == 1) {
				medicamentoVOSeleccionado = listaMedicamentosEncontrados.get(0);
				consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
				codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
				descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
				mostrarPopupMedicamentos = false;
			} else if (listaMedicamentosEncontrados == null || listaMedicamentosEncontrados.isEmpty()) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_VALIDACION__PRESTACION_SIN_DATOS"),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/** Metodo abrir popup prestaciones */
	public void abrirPopupAdicionarPrestaciones() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			CargarCombosController cargarCombosController = new CargarCombosController();
			
			listaTiposPrestaciones = FuncionesAppWeb.obtenerListaSelectTiposPrestacionGD(
					registrarSolicitudesController.obtenerListaSelectTipoPrestaciones(null, 0));

			listaLateralidades = CargarCombosUtil.obtenerListaSelectLateralidadesProcedimientos(null,
					cargarCombosController);
			listaViasAccesoMedicamento = CargarCombosUtil.obtenerListaSelectViasAccesoMedicamento(null,
					cargarCombosController);
			listaDosisMedicamentos = CargarCombosUtil.obtenerListaSelectDosisMedicamentos(null, cargarCombosController);
			listaFrecuenciaMedicamentos = CargarCombosUtil.obtenerListaSelectFrecuenciaMedicamentos(null,
					cargarCombosController);

			cargarMotivosDomiciliaria(ConstantesWeb.TIPO_MOTIVOS_CREAR_PRESTACION);

			mostrarPopupAdicionarPrestacion = true;
			codigoPrestacion = null;
			descripcionPrestacion = null;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/** Metodo seleccionar prestacion */
	public void seleccionPopupMedicamentos() {
		if (medicamentoVOSeleccionado != null) {
			consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
			descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
			codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
			listaMedicamentosEncontrados = null;
			mostrarPopupMedicamentos = false;
		}
	}

	/** Metodo abrir popup agregar prestaciones */
	public void cerrarPopupAdicionarPrestaciones() {
		mostrarPopupAdicionarPrestacion = false;
		prestacionNuevaDomi = new PrestacionesAprobadasVO();
	}

	/** Metodo cambiar direccionamiento */
	public void cambiarDireccionamiento() {
		try {

			if (cambioDireccionamiento.getMotivos().isEmpty()
					|| cambioDireccionamiento.getConsecutivoPrestador() == null
					|| cambioDireccionamiento.getJustificacion() == null
					|| ConstantesWeb.CADENA_VACIA.equals(cambioDireccionamiento.getJustificacion().trim())) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO"),
						ErrorType.PARAMETRO_ERRADO);
			}

			if (cambioDireccionamiento.getJustificacion().trim().length() < 10) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_CAMBIO_DIRECCIONAMIENTO_JUSTIFACION_MIN"),
						ErrorType.PARAMETRO_ERRADO);
			}
			PrestacionController prestacionController = new PrestacionController();
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			
			prestacionController.guardarDireccionamiento(cambioDireccionamiento, consecutivoPrestacionGlobal,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud,
					consecutivoPrestacionGlobal);
			cambioDireccionamiento = new DireccionamientoVO();
			mostrarPopupCambiarDireccionamientoDomi = false;
			activarFiltroCiudad = false;
			ciudad = new CiudadVO();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/** Metodo cambiar fecha de direccionamiento */
	public void cambiarFechaDireccionamiento() {
		try {
			if (cambioFecha.getMotivos().isEmpty() || cambioFecha.getFechaEsperado() == null
					|| (cambioFecha.getJustificacion() == null
							|| ConstantesWeb.CADENA_VACIA.equals(cambioFecha.getJustificacion().trim()))) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_MSG_GENERICO_CAMPO_OBLIGATORIO"),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (cambioFecha.getJustificacion().trim().length() < 10) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_CAMBIO_DIRECCIONAMIENTO_JUSTIFACION_MIN"),
						ErrorType.PARAMETRO_ERRADO);
			}
			
			PrestacionController prestacionController = new PrestacionController();
			GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
			
			prestacionController.guardarCambioFechaDireccionamiento(cambioFecha, consecutivoPrestacionGlobal,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			mostrarPopupCambiarFechaDireccionamientoDomi = false;
			cambioFecha = new DireccionamientoVO();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion("EXITO_MSG_GENERICO"), ConstantesWeb.COLOR_AZUL);
			direccionamientoDTOCUMS = gestionDomiciliariaController.consultaDireccionamiento(idSolicitud,
					consecutivoPrestacionGlobal);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/** Metodo agregar una alternativa */
	public void adicionarAlternativa() {
		try {
			if (!ConstantesWeb.CADENA_VACIA.equals(alternativa.trim())) {
				listaAlternativasCUMS.add(new AlternativasVO(alternativa, null));
				alternativa = null;
				if (listaAlternativasCUMS.size() == 5) {
					alternativaRequeridoDomi = false;
					throw new LogicException(Messages.getValorExcepcion("ERROR_CATIDAD_ALTERNATIVAS"),
							ErrorType.PARAMETRO_ERRADO);
				}

			} else {
				throw new LogicException(Messages.getValorExcepcion("ERROR_ALTERNATIVA_VACIO"),
						ErrorType.PARAMETRO_ERRADO);
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}

	}

	/** Metodo eliminar una alternativa */
	public void eliminarAlternativa() {
		listaAlternativasCUMS.remove(alternativaSel);
		alternativaRequeridoDomi = true;
	}

	public void cargarMotivosDomiciliaria(Integer tipoMotivo) {
		try {
			PrestacionController prestacionController = new PrestacionController();
			lstMotivos = FuncionesAppWeb
					.obtenerListaMotivos(prestacionController.consultarMotivosDireccionamiento(tipoMotivo));
			lstMotivosPrestacionesDomi = lstMotivos;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo que valida si hay diagnostico ppal **/
	private boolean existeDiagnosticoPpal() {
		boolean resultado = false;
		for (DiagnosticoDTO object : listaDiagnosticosSeleccionados) {
			if (object.getPrincipal().equals(ConstantesWeb.SI)
					|| object.getPrincipal().equals(ConstantesWeb.DIAGNOSTICO_PPAL)) {
				resultado = true;
				break;
			}
		}
		return resultado;
	}

	private void llenarListadoPrestacionesSolicitadasDomi() {

		for (PrestacionesAprobadasVO vo : lstPrestacionesSolicitadasDomi) {
			SelectItem item = new SelectItem(vo.getConscodigoPrestacionGSA(),
					vo.getCodigoPrestacion() + ConstantesWeb.CADENA_EN_BLANCO + vo.getDescripcion());
			lstPrestacionesReasignarDomi.add(item);
		}

	}

	/** Metodo cerrar popup direccionamiento */
	public void cerrarPopupCambiarDireccionamientoDomi() {
		mostrarPopupCambiarDireccionamientoDomi = false;
		cambioDireccionamiento = new DireccionamientoVO();
		activarFiltroCiudad = false;
		ciudad = new CiudadVO();
	}

	/** Metodo abrir popup direccionamiento */
	public void abrirPopupCambiarDireccionamientoDomi() {
		cargarMotivosDomiciliaria(ConstantesWeb.TIPO_MOTIVOS_DIRECCIONAMIENTO);
		mostrarPopupCambiarDireccionamientoDomi = true;
		PrestadorController prestadorController;
		try {
			prestadorController = new PrestadorController();
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadorController.consultarPrestadoresDireccionamiento(
					prestacionesAprobadasDTOCUMS.getConscodigoPrestacion(), 
					new Integer(prestacionesAprobadasDTOTemp.getPlanAfiliado()), 
					prestacionesAprobadasDTOCUMS.getConsecutivoCiudad(),
					idSolicitud));
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo abrir popup cambiar fecha */
	public void abrirPopupCambiarFechaDireccionamientoDomi() {
		cargarMotivosDomiciliaria(ConstantesWeb.TIPO_MOTIVOS_FECHA_ENTREGA);
		mostrarPopupCambiarFechaDireccionamientoDomi = true;
	}

	public String guardarProcesoReasignacionPrestacionDomi() {
		String url = "";
		try {
			if (descGrupoAuditor != null) {
				GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
				
				ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(observacionesReasignar,
						prestacionesReasignar);
				gestionAuditoriaIntegralController.guardarProcesoReasignacionPrestacionAuditoriaIntegral(
						observacionesReasignar, prestacionesReasignar, userBpmDomi, descGrupoAuditor);
				mostrarPopupReliquidarDomi = false;
				url = reasignar();
			} else {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO),
						ConstantesWeb.COLOR_ROJO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/** Metodo cerrar popup cambiar fecha */
	public void cerrarPopupCambiarFechaDireccionamientoDomi() {
		mostrarPopupCambiarFechaDireccionamientoDomi = false;
		cambioFecha = new DireccionamientoVO();
	}

	public void cerrarPopupReasignarSolicitud() {
		mostrarPopupReliquidarDomi = false;
	}

	public void abrirPopupReasignarSolicitudDomi() {
		mostrarPopupReliquidarDomi = true;
	}
	
	/**
	 * Metodo que realiza la busqueda de la ciudad de acuerdo al codigo o descripcion ingresada
	 */
	public void buscarCiudad() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupCiudades = false;
			listaCiudadesDireccionamiento = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudad, registrarSolicitudesController);			
			ciudad = new CiudadVO();
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesDireccionamiento)){
				mostrarPopupCiudades = true;
			}else{
				mostrarPopupCiudades = false;
				ciudad = listaCiudadesDireccionamiento.get(0);
			}	
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}	
	
	/**
	 * Cierra popup con listado de ciudades.
	 */
	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		
	}
	
	/**
	 * Metodo que recalcula los direccionamientos de acuerdo a la ciudad.
	 */
	public void recalcularDireccionamientos() {
		try {
			int consecutivoCiudad = ciudad.getConsecutivoCodigoCiudad() == null? ConstantesWeb.VALOR_CERO: ciudad.getConsecutivoCodigoCiudad().intValue();
			if(consecutivoCiudad == ConstantesWeb.VALOR_CERO) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_BUSQUEDA_CIUDAD),
						ErrorType.PARAMETRO_ERRADO);
			}
			lstPrestadoresDireccionamiento.clear();
			PrestadorController prestadorController = new PrestadorController();
			List<PrestadorVO> prestadores = prestadorController
					.consultarPrestadoresDireccionamiento(prestacionesAprobadasDTOCUMS.getConscodigoPrestacion(), 
					afiliadoVO.getPlan().getConsectivoPlan(), ciudad.getConsecutivoCodigoCiudad(), solicitudVO.getNumeroSolicitud());
			lstPrestadoresDireccionamiento = FuncionesAppWeb.obtenerListaSelectPrestadoresDireccionamiento(prestadores);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * @return the prestacionesAprobadasDTOTemp
	 */
	public PrestacionesAprobadasVO getPrestacionesAprobadasDTOTemp() {
		return prestacionesAprobadasDTOTemp;
	}

	/**
	 * @return the prestacionesAprobadasDTOCUMS
	 */
	public PrestacionesAprobadasVO getPrestacionesAprobadasDTOCUMS() {
		return prestacionesAprobadasDTOCUMS;
	}

	/**
	 * @param prestacionesAprobadasDTOCUMS
	 *            the prestacionesAprobadasDTOCUMS to set
	 */
	public void setPrestacionesAprobadasDTOCUMS(PrestacionesAprobadasVO prestacionesAprobadasDTOCUMS) {
		this.prestacionesAprobadasDTOCUMS = prestacionesAprobadasDTOCUMS;
	}

	/**
	 * @param prestacionesAprobadasDTOTemp
	 *            the prestacionesAprobadasDTOTemp to set
	 */
	public void setPrestacionesAprobadasDTOTemp(PrestacionesAprobadasVO prestacionesAprobadasDTOTemp) {
		this.prestacionesAprobadasDTOTemp = prestacionesAprobadasDTOTemp;
	}

	/**
	 * @return the direccionamientoDTOCUMS
	 */
	public DireccionamientoVO getDireccionamientoDTOCUMS() {
		return direccionamientoDTOCUMS;
	}

	/**
	 * @return the cambioFecha
	 */
	public DireccionamientoVO getCambioFecha() {
		return cambioFecha;
	}

	/**
	 * @param cambioFecha
	 *            the cambioFecha to set
	 */
	public void setCambioFecha(DireccionamientoVO cambioFecha) {
		this.cambioFecha = cambioFecha;
	}

	/**
	 * @param direccionamientoDTOCUMS
	 *            the direccionamientoDTOCUMS to set
	 */
	public void setDireccionamientoDTOCUMS(DireccionamientoVO direccionamientoDTOCUMS) {
		this.direccionamientoDTOCUMS = direccionamientoDTOCUMS;
	}

	/**
	 * @return the cambioDireccionamiento
	 */
	public DireccionamientoVO getCambioDireccionamiento() {
		return cambioDireccionamiento;
	}

	/**
	 * @return the medicamentoVOSeleccionado
	 */
	public MedicamentosVO getMedicamentoVOSeleccionado() {
		return medicamentoVOSeleccionado;
	}

	/**
	 * @param medicamentoVOSeleccionado
	 *            the medicamentoVOSeleccionado to set
	 */
	public void setMedicamentoVOSeleccionado(MedicamentosVO medicamentoVOSeleccionado) {
		this.medicamentoVOSeleccionado = medicamentoVOSeleccionado;
	}

	/**
	 * @return the diagnosticoVoSeleccionado
	 */
	public DiagnosticosVO getDiagnosticoVoSeleccionado() {
		return diagnosticoVoSeleccionadoDomi;
	}

	/**
	 * @param cambioDireccionamiento
	 *            the cambioDireccionamiento to set
	 */
	public void setCambioDireccionamiento(DireccionamientoVO cambioDireccionamiento) {
		this.cambioDireccionamiento = cambioDireccionamiento;
	}

	/**
	 * @return the solicitudVO
	 */
	public SolicitudBpmVO getSolicitudVO() {
		return solicitudVO;
	}

	/**
	 * @param solicitudVO
	 *            the solicitudVO to set
	 */
	public void setSolicitudVO(SolicitudBpmVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	/**
	 * @param diagnosticoVoSeleccionado
	 *            the diagnosticoVoSeleccionado to set
	 */
	public void setDiagnosticoVoSeleccionado(DiagnosticosVO diagnosticoVoSeleccionado) {
		this.diagnosticoVoSeleccionadoDomi = diagnosticoVoSeleccionado;
	}

	/**
	 * @return the datosBasicosAfiliado
	 */
	public AfiliadoVO getDatosBasicosAfiliado() {
		return afiliadoVO;
	}

	/**
	 * @return the prestacionNueva
	 */
	public PrestacionesAprobadasVO getPrestacionNueva() {
		return prestacionNuevaDomi;
	}

	/**
	 * @param prestacionNueva
	 *            the prestacionNueva to set
	 */
	public void setPrestacionNueva(PrestacionesAprobadasVO prestacionNueva) {
		this.prestacionNuevaDomi = prestacionNueva;
	}

	/**
	 * @param datosBasicosAfiliado
	 *            the datosBasicosAfiliado to set
	 */
	public void setDatosBasicosAfiliado(AfiliadoVO datosBasicosAfiliado) {
		this.afiliadoVO = datosBasicosAfiliado;
	}

	/**
	 * @return the alternativaSel
	 */
	public AlternativasVO getAlternativaSel() {
		return alternativaSel;
	}

	/**
	 * @return the documentoSoporteVOSeleccionadoAuditoria
	 */
	public DocumentoSoporteVO getDocumentoSoporteVOSeleccionadoAuditoria() {
		return documentoSoporteVOSeleccionadoAuditoria;
	}

	/**
	 * @param documentoSoporteVOSeleccionadoAuditoria
	 *            the documentoSoporteVOSeleccionadoAuditoria to set
	 */
	public void setDocumentoSoporteVOSeleccionadoAuditoria(DocumentoSoporteVO documentoSoporteVOSeleccionadoAuditoria) {
		this.documentoSoporteVOSeleccionadoAuditoria = documentoSoporteVOSeleccionadoAuditoria;
	}

	/**
	 * @param alternativaSel
	 *            the alternativaSel to set
	 */
	public void setAlternativaSel(AlternativasVO alternativaSel) {
		this.alternativaSel = alternativaSel;
	}

	/**
	 * @return the listaHallazgosCUMS
	 */
	public List<SelectItem> getListaHallazgosCUMS() {
		return listaHallazgosCUMS;
	}

	/**
	 * @param listaHallazgosCUMS
	 *            the listaHallazgosCUMS to set
	 */
	public void setListaHallazgosCUMS(List<SelectItem> listaHallazgosCUMS) {
		this.listaHallazgosCUMS = listaHallazgosCUMS;
	}

	/**
	 * @param listaEstadosCUMS
	 *            the listaEstadosCUMS to set
	 */
	public void setListaEstadosCUMS(List<SelectItem> listaEstadosCUMS) {
		this.listaEstadosCUMS = listaEstadosCUMS;
	}

	/**
	 * @return the lstMedicosAuditores
	 */
	public List<SelectItem> getLstMedicosAuditores() {
		return lstMedicosAuditores;
	}

	/**
	 * @return the listaEstadosCUMS
	 */
	public List<SelectItem> getListaEstadosCUMS() {
		return listaEstadosCUMS;
	}

	/**
	 * @param lstMedicosAuditores
	 *            the lstMedicosAuditores to set
	 */
	public void setLstMedicosAuditores(List<SelectItem> lstMedicosAuditores) {
		this.lstMedicosAuditores = lstMedicosAuditores;
	}

	/**
	 * @return the lstRiesgos
	 */
	public List<SelectItem> getLstRiesgos() {
		return lstRiesgos;
	}

	/**
	 * @return the listaCausasCUMS
	 */
	public List<SelectItem> getListaCausasCUMS() {
		return listaCausasCUMS;
	}

	/**
	 * @param listaCausasCUMS
	 *            the listaCausasCUMS to set
	 */
	public void setListaCausasCUMS(List<SelectItem> listaCausasCUMS) {
		this.listaCausasCUMS = listaCausasCUMS;
	}

	/**
	 * @return the listaFundamentosCUMS
	 */
	public List<SelectItem> getListaFundamentosCUMS() {
		return listaFundamentosCUMS;
	}

	/**
	 * @param lstRiesgos
	 *            the lstRiesgos to set
	 */
	public void setLstRiesgos(List<SelectItem> lstRiesgos) {
		this.lstRiesgos = lstRiesgos;
	}

	/**
	 * @param listaFundamentosCUMS
	 *            the listaFundamentosCUMS to set
	 */
	public void setListaFundamentosCUMS(List<SelectItem> listaFundamentosCUMS) {
		this.listaFundamentosCUMS = listaFundamentosCUMS;
	}

	/**
	 * @return the listaTiposPrestaciones
	 */
	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	/**
	 * @return the listaViasAccesoMedicamento
	 */
	public List<SelectItem> getListaViasAccesoMedicamento() {
		return listaViasAccesoMedicamento;
	}

	/**
	 * @param listaViasAccesoMedicamento
	 *            the listaViasAccesoMedicamento to set
	 */
	public void setListaViasAccesoMedicamento(List<SelectItem> listaViasAccesoMedicamento) {
		this.listaViasAccesoMedicamento = listaViasAccesoMedicamento;
	}

	/**
	 * @return the listaLateralidades
	 */
	public List<SelectItem> getListaLateralidades() {
		return listaLateralidades;
	}

	/**
	 * @param listaLateralidades
	 *            the listaLateralidades to set
	 */
	public void setListaLateralidades(List<SelectItem> listaLateralidades) {
		this.listaLateralidades = listaLateralidades;
	}

	/**
	 * @param listaTiposPrestaciones
	 *            the listaTiposPrestaciones to set
	 */
	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	/**
	 * @return the listaDosisMedicamentos
	 */
	public List<SelectItem> getListaDosisMedicamentos() {
		return listaDosisMedicamentos;
	}

	/**
	 * @param listaDosisMedicamentos
	 *            the listaDosisMedicamentos to set
	 */
	public void setListaDosisMedicamentos(List<SelectItem> listaDosisMedicamentos) {
		this.listaDosisMedicamentos = listaDosisMedicamentos;
	}

	/**
	 * @return the listaFrecuenciaMedicamentos
	 */
	public List<SelectItem> getListaFrecuenciaMedicamentos() {
		return listaFrecuenciaMedicamentos;
	}

	/**
	 * @return the lstMotivos
	 */
	public List<SelectItem> getLstMotivos() {
		return lstMotivos;
	}

	/**
	 * @param lstMotivos
	 *            the lstMotivos to set
	 */
	public void setLstMotivos(List<SelectItem> lstMotivos) {
		this.lstMotivos = lstMotivos;
	}

	/**
	 * @return the lstPrestadoresDireccionamiento
	 */
	public List<SelectItem> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

	/**
	 * @param listaFrecuenciaMedicamentos
	 *            the listaFrecuenciaMedicamentos to set
	 */
	public void setListaFrecuenciaMedicamentos(List<SelectItem> listaFrecuenciaMedicamentos) {
		this.listaFrecuenciaMedicamentos = listaFrecuenciaMedicamentos;
	}

	/**
	 * @param lstPrestadoresDireccionamiento
	 *            the lstPrestadoresDireccionamiento to set
	 */
	public void setLstPrestadoresDireccionamiento(List<SelectItem> lstPrestadoresDireccionamiento) {
		this.lstPrestadoresDireccionamiento = lstPrestadoresDireccionamiento;
	}

	/**
	 * @return the listaAlternativasCUMS
	 */
	public List<AlternativasVO> getListaAlternativasCUMS() {
		return listaAlternativasCUMS;
	}

	/**
	 * @param listaAlternativasCUMS
	 *            the listaAlternativasCUMS to set
	 */
	public void setListaAlternativasCUMS(List<AlternativasVO> listaAlternativasCUMS) {
		this.listaAlternativasCUMS = listaAlternativasCUMS;
	}

	/**
	 * @return the listaEmpleadores
	 */
	public List<EmpleadorVO> getListaEmpleadores() {
		return listaEmpleadores;
	}

	/**
	 * @param listaEmpleadores
	 *            the listaEmpleadores to set
	 */
	public void setListaEmpleadores(List<EmpleadorVO> listaEmpleadores) {
		this.listaEmpleadores = listaEmpleadores;
	}
	
	/**
	 * @return the listaConveniosCapacitacion
	 */
	public List<ConveniosCapitacionVO> getListaConveniosCapacitacion() {
		return listaConveniosCapacitacion;
	}

	/**
	 * @param listaConveniosCapacitacion
	 *            the listaConveniosCapacitacion to set
	 */
	public void setListaConveniosCapacitacion(List<ConveniosCapitacionVO> listaConveniosCapacitacion) {
		this.listaConveniosCapacitacion = listaConveniosCapacitacion;
	}

	/**
	 * @return the listaDiagnosticosEncontrados
	 */
	public List<DiagnosticosVO> getListaDiagnosticosEncontrados() {
		return listaDiagnosticosEncontrados;
	}

	/**
	 * @return the listaDiagnosticosSeleccionados
	 */
	public List<DiagnosticoDTO> getListaDiagnosticosSeleccionados() {
		return listaDiagnosticosSeleccionados;
	}

	/**
	 * @param listaDiagnosticosSeleccionados
	 *            the listaDiagnosticosSeleccionados to set
	 */
	public void setListaDiagnosticosSeleccionados(List<DiagnosticoDTO> listaDiagnosticosSeleccionados) {
		this.listaDiagnosticosSeleccionados = listaDiagnosticosSeleccionados;
	}

	/**
	 * @return the lstGestionesPrestacion
	 */
	public List<GestionesPrestacionVO> getLstGestionesPrestacion() {
		return lstGestionesPrestacion;
	}

	/**
	 * @param lstGestionesPrestacion
	 *            the lstGestionesPrestacion to set
	 */
	public void setLstGestionesPrestacion(List<GestionesPrestacionVO> lstGestionesPrestacion) {
		this.lstGestionesPrestacion = lstGestionesPrestacion;
	}

	/**
	 * @param listaDiagnosticosEncontrados
	 *            the listaDiagnosticosEncontrados to set
	 */
	public void setListaDiagnosticosEncontrados(List<DiagnosticosVO> listaDiagnosticosEncontrados) {
		this.listaDiagnosticosEncontrados = listaDiagnosticosEncontrados;
	}

	/**
	 * @return the lstHistoricosPrestacion
	 */
	public List<HistoricoPrestacionVO> getLstHistoricosPrestacion() {
		return lstHistoricosPrestacionDomi;
	}

	/**
	 * @param lstHistoricosPrestacion
	 *            the lstHistoricosPrestacion to set
	 */
	public void setLstHistoricosPrestacion(List<HistoricoPrestacionVO> lstHistoricosPrestacion) {
		this.lstHistoricosPrestacionDomi = lstHistoricosPrestacion;
	}

	/**
	 * @return the lstAntecedentesPrestacion
	 */
	public List<AntecedentesPrestacionVO> getLstAntecedentesPrestacion() {
		return lstAntecedentesPrestacion;
	}

	/**
	 * @param lstAntecedentesPrestacion
	 *            the lstAntecedentesPrestacion to set
	 */
	public void setLstAntecedentesPrestacion(List<AntecedentesPrestacionVO> lstAntecedentesPrestacion) {
		this.lstAntecedentesPrestacion = lstAntecedentesPrestacion;
	}

	/**
	 * @param lstPrestacionesSolicitadas
	 *            the lstPrestacionesSolicitadas to set
	 */
	public void setLstPrestacionesSolicitadas(List<PrestacionesAprobadasVO> lstPrestacionesSolicitadas) {
		this.lstPrestacionesSolicitadasDomi = lstPrestacionesSolicitadas;
	}

	/**
	 * @return the listaMedicamentosEncontrados
	 */
	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	/**
	 * @param listaMedicamentosEncontrados
	 *            the listaMedicamentosEncontrados to set
	 */
	public void setListaMedicamentosEncontrados(List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	public List<DocumentoSoporteVO> getListaDocumentosDomi() {
		return listaDocumentosDomi;
	}

	/**
	 * @return the lstPrestacionesSolicitadas
	 */
	public List<PrestacionesAprobadasVO> getLstPrestacionesSolicitadas() {
		return lstPrestacionesSolicitadasDomi;
	}

	public void setListaDocumentosDomi(List<DocumentoSoporteVO> listaDocumentosDomi) {
		this.listaDocumentosDomi = listaDocumentosDomi;
	}

	/**
	 * @return the fechaConsulta
	 */
	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	/**
	 * @return the cUMS
	 */
	public String getCUMS() {
		return CUMS;
	}

	/**
	 * @param fechaConsulta
	 *            the fechaConsulta to set
	 */
	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	/**
	 * @param cUMS
	 *            the cUMS to set
	 */
	public void setCUMS(String cUMS) {
		CUMS = cUMS;
	}

	/**
	 * @param tIPO_ESTADO_NEGOCIO
	 *            the tIPO_ESTADO_NEGOCIO to set
	 */
	public void setTIPO_ESTADO_NEGOCIO(int tIPO_ESTADO_NEGOCIO) {
		TIPO_ESTADO_NEGOCIO = tIPO_ESTADO_NEGOCIO;
	}

	/**
	 * @return the pestanaSeleccionada
	 */
	public String getPestanaSeleccionada() {
		return pestanaSeleccionada;
	}

	/**
	 * @param pestanaSeleccionada
	 *            the pestanaSeleccionada to set
	 */
	public void setPestanaSeleccionada(String pestanaSeleccionada) {
		this.pestanaSeleccionada = pestanaSeleccionada;
	}

	/**
	 * @return the txtDescripcionDiagnostico
	 */
	public String getTxtDescripcionDiagnostico() {
		return txtDescripcionDiagnostico;
	}

	/**
	 * @return the tIPO_ESTADO_NEGOCIO
	 */
	public int getTIPO_ESTADO_NEGOCIO() {
		return TIPO_ESTADO_NEGOCIO;
	}

	/**
	 * @return the txtCodigoDiagnostico
	 */
	public String getTxtCodigoDiagnostico() {
		return txtCodigoDiagnostico;
	}

	/**
	 * @param txtCodigoDiagnostico
	 *            the txtCodigoDiagnostico to set
	 */
	public void setTxtCodigoDiagnostico(String txtCodigoDiagnostico) {
		this.txtCodigoDiagnostico = txtCodigoDiagnostico;
	}

	/**
	 * @param txtDescripcionDiagnostico
	 *            the txtDescripcionDiagnostico to set
	 */
	public void setTxtDescripcionDiagnostico(String txtDescripcionDiagnostico) {
		this.txtDescripcionDiagnostico = txtDescripcionDiagnostico;
	}

	/**
	 * @return the diagnosticoDTOSeleccionado
	 */
	public DiagnosticoDTO getDiagnosticoDTOSeleccionado() {
		return diagnosticoDTOSeleccionado;
	}

	/**
	 * @param diagnosticoDTOSeleccionado
	 *            the diagnosticoDTOSeleccionado to set
	 */
	public void setDiagnosticoDTOSeleccionado(DiagnosticoDTO diagnosticoDTOSeleccionado) {
		this.diagnosticoDTOSeleccionado = diagnosticoDTOSeleccionado;
	}

	/**
	 * @return the mostrarPopupDiagnosticos
	 */
	public boolean isMostrarPopupDiagnosticos() {
		return mostrarPopupDiagnosticos;
	}

	/**
	 * @param mostrarPopupDiagnosticos
	 *            the mostrarPopupDiagnosticos to set
	 */
	public void setMostrarPopupDiagnosticos(boolean mostrarPopupDiagnosticos) {
		this.mostrarPopupDiagnosticos = mostrarPopupDiagnosticos;
	}

	/**
	 * @return the verPestañaCums
	 */
	public boolean isVerPestañaCums() {
		return verPestañaCums;
	}

	/**
	 * @return the consecutivoPrestacion
	 */
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	/**
	 * @return the codigoPrestacion
	 */
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	/**
	 * @param verPestañaCums
	 *            the verPestañaCums to set
	 */
	public void setVerPestañaCums(boolean verPestañaCums) {
		this.verPestañaCums = verPestañaCums;
	}

	/**
	 * @return the alternativa
	 */
	public String getAlternativa() {
		return alternativa;
	}

	/**
	 * @param consecutivoPrestacion
	 *            the consecutivoPrestacion to set
	 */
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	/**
	 * @param codigoPrestacion
	 *            the codigoPrestacion to set
	 */
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	/**
	 * @return the descripcionPrestacion
	 */
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	/**
	 * @param mostrarPopupMedicamentos
	 *            the mostrarPopupMedicamentos to set
	 */
	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	/**
	 * @param alternativa
	 *            the alternativa to set
	 */
	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}

	/**
	 * @param descripcionPrestacion
	 *            the descripcionPrestacion to set
	 */
	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	/**
	 * @param tipoPrestacionSeleccionado
	 *            the tipoPrestacionSeleccionado to set
	 */
	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	/**
	 * @return the mostrarPopupMedicamentos
	 */
	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	/**
	 * @return the tipoPrestacionSeleccionado
	 */
	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	/**
	 * @return the mostrarPopupAdicionarPrestacion
	 */
	public boolean isMostrarPopupAdicionarPrestacion() {
		return mostrarPopupAdicionarPrestacion;
	}

	public boolean getMostrarPopupDocumentosSoporteAuditoriaDomi() {
		return mostrarPopupDocumentosSoporteAuditoriaDomi;
	}

	public void setMostrarPopupDocumentosSoporteAuditoriaDomi(boolean mostrarPopupDocumentosSoporteAuditoriaDomi) {
		this.mostrarPopupDocumentosSoporteAuditoriaDomi = mostrarPopupDocumentosSoporteAuditoriaDomi;
	}

	/**
	 * @return the consecutivoPrestacionGlobal
	 */
	public Integer getConsecutivoPrestacionGlobal() {
		return consecutivoPrestacionGlobal;
	}

	/**
	 * @param consecutivoPrestacionGlobal
	 *            the consecutivoPrestacionGlobal to set
	 */
	public void setConsecutivoPrestacionGlobal(Integer consecutivoPrestacionGlobal) {
		this.consecutivoPrestacionGlobal = consecutivoPrestacionGlobal;
	}

	/**
	 * @return the datosNegacionDTO
	 */
	public DatosNegacionDTO getDatosNegacionDTO() {
		return datosNegacionDTO;
	}

	/**
	 * @return the consecutivoSolicitudGlobal
	 */
	public Integer getConsecutivoSolicitudGlobal() {
		return idSolicitud;
	}

	/**
	 * @param consecutivoSolicitudGlobal
	 *            the consecutivoSolicitudGlobal to set
	 */
	public void setConsecutivoSolicitudGlobal(Integer consecutivoSolicitudGlobal) {
		this.idSolicitud = consecutivoSolicitudGlobal;
	}

	/**
	 * @param mostrarPopupAdicionarPrestacion
	 *            the mostrarPopupAdicionarPrestacion to set
	 */
	public void setMostrarPopupAdicionarPrestacion(boolean mostrarPopupAdicionarPrestacion) {
		this.mostrarPopupAdicionarPrestacion = mostrarPopupAdicionarPrestacion;
	}

	/**
	 * @return the gestionAuditorDTO
	 */
	public GestionAuditorDTO getGestionAuditorDTO() {
		return gestionAuditorDTO;
	}

	/**
	 * @param lstProgramacionEntrega
	 *            the lstProgramacionEntrega to set
	 */
	public void setLstProgramacionEntrega(List<ProgramacionEntregaVO> lstProgramacionEntrega) {
		this.lstProgramacionEntrega = lstProgramacionEntrega;
	}

	/**
	 * @param gestionAuditorDTO
	 *            the gestionAuditorDTO to set
	 */
	public void setGestionAuditorDTO(GestionAuditorDTO gestionAuditorDTO) {
		this.gestionAuditorDTO = gestionAuditorDTO;
	}

	/**
	 * @param lstGruposAuditores
	 *            the lstGruposAuditores to set
	 */
	public void setLstGruposAuditores(List<SelectItem> lstGruposAuditores) {
		this.lstGruposAuditores = lstGruposAuditores;
	}

	/**
	 * @param datosNegacionDTO
	 *            the datosNegacionDTO to set
	 */
	public void setDatosNegacionDTO(DatosNegacionDTO datosNegacionDTO) {
		this.datosNegacionDTO = datosNegacionDTO;
	}

	/**
	 * @return the lstProgramacionEntrega
	 */
	public List<ProgramacionEntregaVO> getLstProgramacionEntrega() {
		return lstProgramacionEntrega;
	}

	/**
	 * @return the lstGruposAuditores
	 */
	public List<SelectItem> getLstGruposAuditores() {
		return lstGruposAuditores;
	}

	/**
	 * @return the idSolicitud
	 */
	public Integer getIdSolicitud() {
		return idSolicitud;
	}

	/**
	 * @param idSolicitud
	 *            the idSolicitud to set
	 */
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	/**
	 * @param idTask
	 *            the idTask to set
	 */
	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	/**
	 * @return the descGrupoAuditor
	 */
	public String getDescGrupoAuditor() {
		return descGrupoAuditor;
	}

	/**
	 * @param descGrupoAuditor
	 *            the descGrupoAuditor to set
	 */
	public void setDescGrupoAuditor(String descGrupoAuditor) {
		this.descGrupoAuditor = descGrupoAuditor;
	}

	/**
	 * @return the mostrarPopupCambiarDireccionamiento
	 */
	public boolean isMostrarPopupCambiarDireccionamientoDomi() {
		return mostrarPopupCambiarDireccionamientoDomi;
	}

	/**
	 * @param mostrarPopupCambiarDireccionamiento
	 *            the mostrarPopupCambiarDireccionamiento to set
	 */
	public void setMostrarPopupCambiarDireccionamientoDomi(boolean mostrarPopupCambiarDireccionamiento) {
		this.mostrarPopupCambiarDireccionamientoDomi = mostrarPopupCambiarDireccionamiento;
	}

	/**
	 * @return the idTask
	 */
	public String getIdTask() {
		return idTask;
	}

	/**
	 * @return the mostrarPopupCambiarFechaDireccionamiento
	 */
	public boolean isMostrarPopupCambiarFechaDireccionamientoDomi() {
		return mostrarPopupCambiarFechaDireccionamientoDomi;
	}

	/**
	 * @param mostrarPopupCambiarFechaDireccionamiento
	 *            the mostrarPopupCambiarFechaDireccionamiento to set
	 */
	public void setMostrarPopupCambiarFechaDireccionamientoDomi(boolean mostrarPopupCambiarFechaDireccionamiento) {
		this.mostrarPopupCambiarFechaDireccionamientoDomi = mostrarPopupCambiarFechaDireccionamiento;
	}

	/**
	 * @return the hospitalizacion
	 */
	public HospitalizacionVO getHospitalizacion() {
		return hospitalizacionDomi;
	}

	/**
	 * @param hospitalizacion
	 *            the hospitalizacion to set
	 */
	public void setHospitalizacion(HospitalizacionVO hospitalizacion) {
		this.hospitalizacionDomi = hospitalizacion;
	}

	/**
	 * @return the lstPrestaciones
	 */
	public List<PrestacionesAprobadasVO> getLstPrestaciones() {
		return lstPrestaciones;
	}

	/**
	 * @param lstPrestaciones
	 *            the lstPrestaciones to set
	 */
	public void setLstPrestaciones(List<PrestacionesAprobadasVO> lstPrestaciones) {
		this.lstPrestaciones = lstPrestaciones;
	}

	/**
	 * @param mostrarHospitalizacion
	 *            the mostrarHospitalizacion to set
	 */
	public void setMostrarHospitalizacion(boolean mostrarHospitalizacion) {
		this.mostrarBotonHospitalizacionDomi = mostrarHospitalizacion;
	}

	public String getAltoRiesgo() {
		return altoRiesgo;
	}

	public void setAltoRiesgo(String altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public boolean getMostrarPopupHospitalizacionDomi() {
		return mostrarPopupHospitalizacionDomi;
	}

	public void setMostrarPopupHospitalizacionDomi(boolean mostrarPopupHospitalizacionDomi) {
		this.mostrarPopupHospitalizacionDomi = mostrarPopupHospitalizacionDomi;
	}

	public void setMostrarHistoricoPrestacionesDomi(boolean mostrarHistoricoPrestacionesDomi) {
		this.mostrarHistoricoPrestacionesDomi = mostrarHistoricoPrestacionesDomi;
	}

	public boolean getMostrarVerDetalleSolicitudDomi() {
		return mostrarVerDetalleSolicitudDomi;
	}

	public void setMostrarVerDetalleSolicitudDomi(boolean mostrarVerDetalleSolicitudDomi) {
		this.mostrarVerDetalleSolicitudDomi = mostrarVerDetalleSolicitudDomi;
	}

	/**
	 * @return the mostrarHospitalizacion
	 */
	public boolean isMostrarHospitalizacion() {
		return mostrarBotonHospitalizacionDomi;
	}

	public boolean getMostrarBotonHospitalizacionDomi() {
		return mostrarBotonHospitalizacionDomi;
	}

	public void setMostrarBotonHospitalizacionDomi(boolean mostrarBotonHospitalizacionDomi) {
		this.mostrarBotonHospitalizacionDomi = mostrarBotonHospitalizacionDomi;
	}

	public boolean isMostrarVerDetalleAfiliadoDomi() {
		return mostrarVerDetalleAfiliadoDomi;
	}

	public void setMostrarVerDetalleAfiliadoDomi(boolean mostrarVerDetalleAfiliadoDomi) {
		this.mostrarVerDetalleAfiliadoDomi = mostrarVerDetalleAfiliadoDomi;
	}

	public ConveniosCapitacionVO getConvenioCapacitacionSeleccionado() {
		return convenioCapacitacionSeleccionado;
	}

	public void setConvenioCapacitacionSeleccionado(ConveniosCapitacionVO convenioCapacitacionSeleccionado) {
		this.convenioCapacitacionSeleccionado = convenioCapacitacionSeleccionado;
	}

	public boolean isMostrarConfirmacionGuardarDomi() {
		return mostrarConfirmacionGuardarDomi;
	}

	public void setMostrarConfirmacionGuardarDomi(boolean mostrarConfirmacionGuardarDomi) {
		this.mostrarConfirmacionGuardarDomi = mostrarConfirmacionGuardarDomi;
	}

	public boolean getMostrarHistoricoPrestacionesDomi() {
		return mostrarHistoricoPrestacionesDomi;
	}

	public List<SelectItem> getLstMotivosPrestacionesDomi() {
		return lstMotivosPrestacionesDomi;
	}

	public void setLstMotivosPrestacionesDomi(List<SelectItem> lstMotivosPrestacionesDomi) {
		this.lstMotivosPrestacionesDomi = lstMotivosPrestacionesDomi;
	}

	public boolean getCausaRequeridaDomi() {
		return causaRequeridaDomi;
	}

	public void setCausaRequeridaDomi(boolean causaRequeridaDomi) {
		this.causaRequeridaDomi = causaRequeridaDomi;
	}

	public boolean getFundamentoLegalRequeridoDomi() {
		return fundamentoLegalRequeridoDomi;
	}

	public boolean getAuditorMedicoRequeridoDomi() {
		return auditorMedicoRequeridoDomi;
	}

	public void setAuditorMedicoRequeridoDomi(boolean auditorMedicoRequeridoDomi) {
		this.auditorMedicoRequeridoDomi = auditorMedicoRequeridoDomi;
	}

	public boolean getAlternativaRequeridoDomi() {
		return alternativaRequeridoDomi;
	}

	public void setAlternativaRequeridoDomi(boolean alternativaRequeridoDomi) {
		this.alternativaRequeridoDomi = alternativaRequeridoDomi;
	}

	public boolean getObservacionOPSRequeridaDomi() {
		return observacionOPSRequeridaDomi;
	}

	public void setObservacionOPSRequeridaDomi(boolean observacionOPSRequeridaDomi) {
		this.observacionOPSRequeridaDomi = observacionOPSRequeridaDomi;
	}

	public String getUserBpmDomi() {
		return userBpmDomi;
	}

	public void setUserBpmDomi(String userBpmDomi) {
		this.userBpmDomi = userBpmDomi;
	}

	public String getTipoAuditor() {
		return tipoAuditor;
	}

	public void setFundamentoLegalRequeridoDomi(boolean fundamentoLegalRequeridoDomi) {
		this.fundamentoLegalRequeridoDomi = fundamentoLegalRequeridoDomi;
	}

	public void setTipoAuditor(String tipoAuditor) {
		this.tipoAuditor = tipoAuditor;
	}

	public boolean isMostrarPopupVerGestionesDomi() {
		return mostrarPopupVerGestionesDomi;
	}

	public boolean isMostrarPopupVerAntecedentesDomi() {
		return mostrarPopupVerAntecedentesDomi;
	}

	public void setMostrarPopupVerAntecedentesDomi(boolean mostrarPopupVerAntecedentesDomi) {
		this.mostrarPopupVerAntecedentesDomi = mostrarPopupVerAntecedentesDomi;
	}

	public void setMostrarPopupVerGestionesDomi(boolean mostrarPopupVerGestionesDomi) {
		this.mostrarPopupVerGestionesDomi = mostrarPopupVerGestionesDomi;
	}

	public boolean isCheckDiagnosticoPpalDomi() {
		return checkDiagnosticoPpalDomi;
	}

	public void setCheckDiagnosticoPpalDomi(boolean checkDiagnosticoPpalDomi) {
		this.checkDiagnosticoPpalDomi = checkDiagnosticoPpalDomi;
	}

	public boolean isCheckDiagnosticoSOSDomi() {
		return checkDiagnosticoSOSDomi;
	}

	public void setCheckDiagnosticoSOSDomi(boolean checkDiagnosticoSOSDomi) {
		this.checkDiagnosticoSOSDomi = checkDiagnosticoSOSDomi;
	}

	public boolean isMostrarPopupReliquidarDomi() {
		return mostrarPopupReliquidarDomi;
	}

	public void setMostrarPopupReliquidarDomi(boolean mostrarPopupReliquidarDomi) {
		this.mostrarPopupReliquidarDomi = mostrarPopupReliquidarDomi;
	}

	public List<SelectItem> getLstPrestacionesReasignarDomi() {
		return lstPrestacionesReasignarDomi;
	}

	public void setLstPrestacionesReasignarDomi(List<SelectItem> lstPrestacionesReasignarDomi) {
		this.lstPrestacionesReasignarDomi = lstPrestacionesReasignarDomi;
	}

	public Integer getPrestacionesReasignar() {
		return prestacionesReasignar;
	}

	public void setPrestacionesReasignar(Integer prestacionesReasignar) {
		this.prestacionesReasignar = prestacionesReasignar;
	}

	public String getObservacionesReasignar() {
		return observacionesReasignar;
	}

	public void setObservacionesReasignar(String observacionesReasignar) {
		this.observacionesReasignar = observacionesReasignar;
	}

	public CiudadVO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadVO ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public boolean isActivarFiltroCiudad() {
		return activarFiltroCiudad;
	}

	public void setActivarFiltroCiudad(boolean activarFiltroCiudad) {
		this.activarFiltroCiudad = activarFiltroCiudad;
	}

	public List<CiudadVO> getListaCiudadesDireccionamiento() {
		return listaCiudadesDireccionamiento;
	}

	public void setListaCiudadesDireccionamiento(
			List<CiudadVO> listaCiudadesDireccionamiento) {
		this.listaCiudadesDireccionamiento = listaCiudadesDireccionamiento;
	}
}
