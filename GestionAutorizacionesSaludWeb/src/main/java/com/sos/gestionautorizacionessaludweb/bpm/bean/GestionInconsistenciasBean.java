package com.sos.gestionautorizacionessaludweb.bpm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionInconsistenciasController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

/**
 * Class GestionInconsistenciasBean
 * @author ing. Rafael Cano 
 * @version 09/02/2016
 *
 */

public class GestionInconsistenciasBean implements Serializable {
	private static final long serialVersionUID = 1861165812978867098L;

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionInconsistenciasBean.class);

	private String idTask;
	private String idSolicitud;

	/**
	 * Variable gestion de mensajes
	 */
	



	/**Informacion del Afiliado**/
	private AfiliadoVO datosBasicosAfiliado;

	/**Informacion del Solicitud**/
	private SolicitudBpmVO solicitudVO;

	/**Informacion de las Incosistencias**/
	private List<SelectItem> listaInconsistencias;

	private ClienteRestBpmBean clienteRestBpmBean=(ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
	private String tipoAuditor;
	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {
			idTask=(String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
			idSolicitud=(String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
			tipoAuditor = (String) FacesUtil.getRequestParameter(ConstantesWeb.TIPO_AUDITOR);

			GestionInconsistenciasController gestionInconsistenciasController  = new GestionInconsistenciasController();
			listaInconsistencias = new ArrayList<SelectItem>();
			List<Object> afiliadoYSolicitud = gestionInconsistenciasController.consultaAfiliadoYSolicitud(Integer.parseInt(idSolicitud));
			datosBasicosAfiliado = (AfiliadoVO) afiliadoYSolicitud.get(0);
			solicitudVO = (SolicitudBpmVO) afiliadoYSolicitud.get(1);
			validarEstadoSolicitud();
			if(tipoAuditor.equals(ConstantesWeb.ID_INCONSISTENCIAS)){
				listaInconsistencias = gestionInconsistenciasController.consultaIncosistencias(Integer.parseInt(idSolicitud), ConstantesWeb.CONS_ESTADO_INCONSISTENCIAS_MALLA);
				if(listaInconsistencias.isEmpty() || listaInconsistencias == null){
					MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INCONSISTENCIAS), ConstantesWeb.COLOR_ROJO);
				}
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void validarEstadoSolicitud() {
		if(ConstantesWeb.CODIGO_ESTADO_ACTIVO.equals(datosBasicosAfiliado.getDescripcionEstadoAfiliado())){
			datosBasicosAfiliado.setDescripcionEstadoAfiliado(ConstantesWeb.ESTADO_ACTIVO);
		}else{
			datosBasicosAfiliado.setDescripcionEstadoAfiliado(ConstantesWeb.ESTADO_INACTIVO);
		}
	}

	/**
	 * Metodo que se encarga de terminar la tarea
	 */
	public String terminarTarea(){
		String url="";
		try {
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud, ConstantesWeb.TERMINA_TAREA, ConstantesWeb.CARACTER_SEPARADOR);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/**
	 * Metodo que se encarga de terminar liberar la tarea
	 */
	public String liberarTarea(){
		String url="";
		try {
			clienteRestBpmBean.liberarTarea(idTask, idSolicitud, ConstantesWeb.LIBERAR_TAREA, ConstantesWeb.CARACTER_SEPARADOR);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	//-- Get y Set --//
	
	public String getIdTask() {
		return idTask;
	}

	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public AfiliadoVO getDatosBasicosAfiliado() {
		return datosBasicosAfiliado;
	}

	public void setDatosBasicosAfiliado(AfiliadoVO datosBasicosAfiliado) {
		this.datosBasicosAfiliado = datosBasicosAfiliado;
	}

	public SolicitudBpmVO getSolicitudVO() {
		return solicitudVO;
	}

	public void setSolicitudVO(SolicitudBpmVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	public List<SelectItem> getListaInconsistencias() {
		return listaInconsistencias;
	}

	public void setListaInconsistencias(List<SelectItem> listaInconsistencias) {
		this.listaInconsistencias = listaInconsistencias;
	}
}
