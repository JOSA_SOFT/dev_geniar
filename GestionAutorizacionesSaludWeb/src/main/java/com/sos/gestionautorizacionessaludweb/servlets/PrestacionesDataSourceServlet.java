package com.sos.gestionautorizacionessaludweb.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaBodegaPoliticaController;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;

public class PrestacionesDataSourceServlet extends HttpServlet implements Serializable {
	private static final long serialVersionUID = 1027338367224439073L;
	private static final String PARAMETRO_TIPO_PRESTACION = "tp";
	private static final String PARAMETRO_FILTRO_TEXTO = "q";
	private static final String PARAMETRO_FORMATO_RESPUESTA = "fr";
	private static final String PARAMETRO_FORMATO_RESPUESTA_AUTOCOMPLETAR = "ac";
	private static final String ENCODING_UTF_8 = "UTF-8";
	private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
	private static final String IDENTIFICADOR_BUSQUEDA_CODIGO = "@";
	private static final String PARAMETRO_CUPS_NORMA = "cn";

	private static final Logger LOGGER = Logger.getLogger(PrestacionesDataSourceServlet.class);

	public PrestacionesDataSourceServlet() {
		super();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType(CONTENT_TYPE_APPLICATION_JSON);
		response.setCharacterEncoding(ENCODING_UTF_8);

		try {
			String paramTipoPrestacion = request.getParameter(PARAMETRO_TIPO_PRESTACION);
			String paramFiltro = request.getParameter(PARAMETRO_FILTRO_TEXTO);
			String paramCupsNorma = request.getParameter(PARAMETRO_CUPS_NORMA);
			
			List<PrestacionDTO> listaPrestacionDTO = realizarBusquedaPrestacion(paramTipoPrestacion, paramFiltro, paramCupsNorma);

			Gson gson = new Gson();
			JsonWriter writer = new JsonWriter(response.getWriter());
			writer.beginArray();

			String paramFormatoRespuesta = request.getParameter(PARAMETRO_FORMATO_RESPUESTA);
			if (PARAMETRO_FORMATO_RESPUESTA_AUTOCOMPLETAR.equals(paramFormatoRespuesta)) {
				procesarListadoPrestacion(listaPrestacionDTO, gson, writer);
			}
			writer.endArray();
			writer.flush();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			response.setStatus(500);
		}
	}

	private List<PrestacionDTO> realizarBusquedaPrestacion(String paramTipoPrestacion, String paramFiltro, String paramCupsNorma)
			throws LogicException {
		List<PrestacionDTO> listaPrestacionDTO = new ArrayList<PrestacionDTO>();
		try {
			if (paramTipoPrestacion != null && paramFiltro != null && !ConstantesWeb.CADENA_VACIA.equals(paramFiltro)) {
				String primerCaracter = paramFiltro.substring(0, 1);

				if (IDENTIFICADOR_BUSQUEDA_CODIGO.equals(primerCaracter)) {
					ConsultaBodegaPoliticaController consultaBodegaController;

					consultaBodegaController = new ConsultaBodegaPoliticaController();

					listaPrestacionDTO = consultaBodegaController.consultaInformacionPrestacionesxTipo(
							Integer.valueOf(paramTipoPrestacion), paramFiltro.substring(1, paramFiltro.length()), null, paramCupsNorma != null ? Integer.valueOf(paramCupsNorma) : null );
				} else {
					ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
					listaPrestacionDTO = consultaBodegaController.consultaInformacionPrestacionesxTipo(
							Integer.valueOf(paramTipoPrestacion), null, paramFiltro, paramCupsNorma != null ? Integer.valueOf(paramCupsNorma) : null );
				}
			}
		} catch (Exception e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} 
		return listaPrestacionDTO;
	}

	private void procesarListadoPrestacion(List<PrestacionDTO> listaPrestacionDTO, Gson gson, JsonWriter writer) {
		if (listaPrestacionDTO != null) {
			for (PrestacionDTO prestacionDTO : listaPrestacionDTO) {
				PrestacionesVO vo = new PrestacionesVO(prestacionDTO.getConsecutivoCodificacionPrestacion(),
						prestacionDTO.getCodigoCodificacionPrestacion(),
						prestacionDTO.getDescripcionCodificacionPrestacion());
				gson.toJson(vo, PrestacionesVO.class, writer);
			}
		}
	}
}
