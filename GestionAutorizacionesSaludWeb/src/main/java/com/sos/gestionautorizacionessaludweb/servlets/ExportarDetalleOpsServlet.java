package com.sos.gestionautorizacionessaludweb.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.geniar.generadorExcel.util.Delegado.reportesToExcel.DelegadoReportesToExcel;
import com.geniar.generadorExcel.util.modelo.reportesToExcel.ReporteExcel;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.DetalleOpsVO;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.bean.ModificaFechasEntregaOPSBean;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;

public class ExportarDetalleOpsServlet extends HttpServlet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ExportarDetalleOpsServlet.class);
	private static final String ENCABEZADOS = "Tipo ID,No ID,Plan,Cotizante,No. OPS,Cod. Prestación,Desc. Prestación,Grupo de entrega afiliado,Tutela,"
			+ "PAC,Incapacitado,Estado Mega/Sipres,Fecha Creación,Riesgo del afiliado,Fecha Estimada Entrega";
	private static final String CONTENTTYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	private static final String HEADER1 = "Content-disposition";
	private static final String HEADER2 = "attachment;filename=DetalleOPSCambioFechaEntrega.xls";
	private static final String CE = "iso-8859-1";
	private static final String NOMBREHOJA = "DetalleCambioFechaEntregaOPS";
	private static final String NAMEBEAN_MODIFICARFECHAENTREGA = "modificaFechasEntregaOPSBean";

	public ExportarDetalleOpsServlet() {
		super();
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			performTask(request, response);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public synchronized void performTask(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ByteArrayOutputStream byteArrayOutputStream = null;

		ServletOutputStream toClient = null;

		try {

			HttpSession session = request.getSession();
			ModificaFechasEntregaOPSBean modificaFechasEntregaOPSBean = (ModificaFechasEntregaOPSBean) session
					.getAttribute(NAMEBEAN_MODIFICARFECHAENTREGA);
			List<DetalleOpsVO> detalleOps = modificaFechasEntregaOPSBean.getColeccionOpsVO().getlDetalleOps();
			if (detalleOps != null && !detalleOps.isEmpty()) {
				List<String[]> datosFilasExcel = new ArrayList<String[]>();

				for (int i = 0; i < detalleOps.size(); i++) {

					DetalleOpsVO prestacion = detalleOps.get(i);
					String[] datosColumnas = new String[15];

					String fechaCreacion = prestacion.getFechaExpedicion() != null
							? Utilidades.obtenerStringDeDate(prestacion.getFechaExpedicion(),
									ConstantesWeb.CARACTER_SEPARADOR, false)
							: ConstantesWeb.VACIO;
					String fechaEstimadaEntrega = prestacion.getFechaEstimadaEntrega() != null
							? Utilidades.obtenerStringDeDate(prestacion.getFechaEstimadaEntrega(),
									ConstantesWeb.CARACTER_SEPARADOR, false)
							: ConstantesWeb.VACIO;

					datosColumnas[0] = (prestacion.getTipoIdentificacion() != null)
							? prestacion.getTipoIdentificacion().trim() : ConstantesWeb.VACIO;
					datosColumnas[1] = (prestacion.getNumeroIdentificacion() != null)
							? prestacion.getNumeroIdentificacion().trim() : ConstantesWeb.VACIO;
					datosColumnas[2] = (prestacion.getDescripcionPlan() != null)
							? prestacion.getDescripcionPlan().trim() : ConstantesWeb.VACIO;
					datosColumnas[3] = (prestacion.getNombreApellido() != null) ? prestacion.getNombreApellido().trim()
							: ConstantesWeb.VACIO;
					String opsRadicado = prestacion.getNumeroOps() != null ? prestacion.getNumeroOps().trim()
							: ConstantesWeb.VACIO;
					datosColumnas[4] = opsRadicado;
					datosColumnas[5] = (prestacion.getCodigoCodificacion() != null)
							? prestacion.getCodigoCodificacion().trim() : ConstantesWeb.VACIO;
					datosColumnas[6] = (prestacion.getDescripcionCodificacion() != null)
							? prestacion.getDescripcionCodificacion().trim() : ConstantesWeb.VACIO;

					String grupoEntrega = prestacion.getDescripcionGrupoEntrega() != null
							? prestacion.getDescripcionGrupoEntrega().trim() : ConstantesWeb.VACIO;
					datosColumnas[7] = grupoEntrega;

					datosColumnas[8] = (prestacion.getTutela() != null) ? prestacion.getTutela().trim()
							: ConstantesWeb.VACIO;
					datosColumnas[9] = (prestacion.getPlanPac() != null) ? prestacion.getPlanPac().trim()
							: ConstantesWeb.VACIO;
					datosColumnas[10] = (prestacion.getIncapacidad() != null) ? prestacion.getIncapacidad().trim()
							: ConstantesWeb.VACIO;
					String estadoMegaSipres = prestacion.getDescripcionEstado() != null
							? prestacion.getDescripcionEstado().trim() : ConstantesWeb.VACIO;
					datosColumnas[11] = estadoMegaSipres;
					datosColumnas[12] = (fechaCreacion != null) ? fechaCreacion.trim() : ConstantesWeb.VACIO;
					datosColumnas[13] = (prestacion.getDescripcionClasificacionEvento() != null)
							? prestacion.getDescripcionClasificacionEvento().trim() : ConstantesWeb.VACIO;
					datosColumnas[14] = (fechaEstimadaEntrega != null) ? fechaEstimadaEntrega.trim()
							: ConstantesWeb.VACIO;

					// se ingresa cada fila o registro encontrado
					// con sus respectivos datos en la colección.
					datosFilasExcel.add(datosColumnas);
				}

				ReporteExcel reporteExcel = new ReporteExcel();
				response.setContentType(CONTENTTYPE);
				response.setHeader(HEADER1, HEADER2);
				response.setCharacterEncoding(CE);

				reporteExcel.setNombreHoja(NOMBREHOJA);

				DelegadoReportesToExcel delegadoReportesToExcel = new DelegadoReportesToExcel();
				byteArrayOutputStream = delegadoReportesToExcel
						.generacionReportesExcelByteArrayOutputStream(ENCABEZADOS, null, datosFilasExcel, reporteExcel);

			}

			toClient = response.getOutputStream();
			byteArrayOutputStream.writeTo(toClient);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
}
