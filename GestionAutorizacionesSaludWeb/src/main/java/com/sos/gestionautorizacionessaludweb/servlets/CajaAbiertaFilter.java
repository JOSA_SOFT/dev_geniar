package com.sos.gestionautorizacionessaludweb.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.gestionautorizacionessaludejb.controller.ValidaAperturaCajaController;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;

import co.eps.sos.dataccess.exception.DataAccessException;

/**
 * Se encarga de redirigir de acuerdo al estado de la caja
 * 
 * @author GENIAR
 *
 */
public class CajaAbiertaFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(CajaAbiertaFilter.class);

	/**
	 * Filtro que recibe la petición, si se está llamando desde una página que
	 * requiere tener una caja abierta en al fecha actual. Dependiendo el
	 * resultado realiza la redireccion. 0: Si no se encuentra abierta la caja
	 * 1: Si la caja está abierta 2: Si la caja está abierta pero una fecha
	 * diferente a la actual.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {

		try {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			HttpSession session = req.getSession(false);
			boolean isIPS = (Boolean) session.getAttribute(ConstantesWeb.ES_USUARIOS_IPS);
			
			String contexto = req.getContextPath();
			String userName = req.getUserPrincipal().getName();

			if (!isIPS) {
				ValidaAperturaCajaController controller = new ValidaAperturaCajaController();
				int result = controller.validaAperturaCaja(userName);

				// Se valida el resultado obtenido del proceso almacenado para
				// redirigir.
				switch (result) {
				case ConstantesWeb.RETORNO_CAJA_ABIERTA:
					chain.doFilter(request, response);
					break;
				case ConstantesWeb.RETORNO_CAJA_CERRADA:
					res.sendRedirect(contexto + ConstantesWeb.RUTA_APERTURA_CAJA);
					break;
				case ConstantesWeb.RETORNO_CAJA_ABIERTA_DESTIEMPO:
					res.sendRedirect(contexto + ConstantesWeb.RUTA_CIERRE_CAJA);
					break;
				default:
					chain.doFilter(request, response);
				}
			} else {
				chain.doFilter(request, response);
			}
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (NullPointerException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} catch (ServletException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// Este metodo es necesario para la implementacion, de la interfaz mas
		// no se usa en a clase.
	}

	@Override
	public void destroy() {
		// Este metodo es necesario para la implementacion, de la interfaz mas
		// no se usa en a clase.
	}

}
