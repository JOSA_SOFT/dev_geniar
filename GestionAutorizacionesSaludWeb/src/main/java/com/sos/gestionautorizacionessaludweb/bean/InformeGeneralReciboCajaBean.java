package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.AfiliadoNombresVO;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesDescuadreValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeGeneralReciboVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeNotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroReporteCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteCajaDetalladoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.controller.ReciboCajaController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;

import co.eps.sos.dataccess.exception.DataAccessException;

/**
 * Bean que se encarga de anuar la presentacion del informe general
 * 
 * @author GENIAR<jorge.garcia@geniar.net>
 */
public class InformeGeneralReciboCajaBean implements Serializable {
	private static final long serialVersionUID = -6992464891036734259L;
	private Date fechaInicial;
	private Date fechaFinal;
	private static final String COLOR_ROJO = ConstantesWeb.COLOR_ROJO;
	private static final Logger LOG = Logger.getLogger(InformeGeneralReciboCajaBean.class);
	private AperturaCajaBean apertura;
	private String login;
	private boolean habilitarDescarga;
	private OficinasVO oficinaSeleccionada;
	private List<InformeGeneralReciboVO> informe;
	private int consecutivoMovimientoCaja;
	private boolean verCausales;
	private List<CausalesDescuadreValorVO> listaDescuadreMovimientoCaja;
	private Integer consecutivoTipoIdentificacion;
	private String numeroIdentificacionAfiliado;
	private AfiliadoNombresVO afiliado;
	private Integer numeroRecibo;
	private Integer numeroOPS;
	private String nombreCompleto;
	private List<ReporteCajaDetalladoVO> reporteCajaDetallado;
	private boolean habilitarDescargaDetallado;
	private String usuario;
	private List<SelectItem> listaSedes;
	private int consecutivoSede = -1;
	private boolean mostrarModalUsuarios = false;
	private List<UsuarioWebVO> listaUsuarios;
	private UsuarioWebVO usuarioSeleccionado;
	private InformeGeneralReciboVO filaSeleccionada;
	private List<SelectItem> listaEstadosNotaCredito;
	private Integer estadoNotaSeleccionado;
	private List<InformeNotasCreditoVO> reporteNotaCredito;
	private InformeNotasCreditoVO notaCreditoSeleccionada;

	/**
	 * Construtor de la clase
	 */
	public InformeGeneralReciboCajaBean() {
		try {
			listaSedes = CargarCombosUtil.obtenerListaSelectParametros(null, new CargarCombosController(),
					ConstantesEJB.COMBO_SEDES);

			listaEstadosNotaCredito = CargarCombosUtil.obtenerListaSelectParametros(null, new CargarCombosController(),
					ConstantesEJB.COMBO_ESTADOS_NOTA_CREDITO);

			listaDescuadreMovimientoCaja = new ArrayList<CausalesDescuadreValorVO>();
			listaUsuarios = new ArrayList<UsuarioWebVO>();

			apertura = (AperturaCajaBean) FacesUtils.getManagedBean("aperturaCajaBean");
			habilitarDescarga = true;
			habilitarDescargaDetallado = true;
			if (apertura == null) {
				apertura = new AperturaCajaBean();
			}
		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeError(e);
		} catch (IOException e) {
			this.mostrarMensajeError(e);
		}

	}

	/**
	 * Valida cuando se presiona en la busqueda de oficina
	 * 
	 * @return true si ambos campos estan vacios, false si estan llenos.
	 */
	private boolean validarCamposBusquedaOficina() {
		if (Utilidades.validarNullCadena(apertura.getCodigoOficina()).equals(ConstantesWeb.CADENA_VACIA)
				&& Utilidades.validarNullCadena(apertura.getDescripcionOficina()).equals(ConstantesWeb.CADENA_VACIA)) {
			return true;
		}
		return false;
	}

	/**
	 * Metodo que se encarga de consultar las oficinas.
	 * 
	 * @return
	 */
	public String consultarOficina() {
		if (!this.validarCamposBusquedaOficina()) {
			try {
				CargarCombosController cargarCombosController = new CargarCombosController();
				String auxCodigoOficina = apertura.getCodigoOficina() != null
						&& !apertura.getCodigoOficina().equals(ConstantesWeb.CADENA_VACIA) ? apertura.getCodigoOficina()
								: null;
				String auxDescripcionOficina = apertura.getDescripcionOficina() != null
						&& !apertura.getDescripcionOficina().equals(ConstantesWeb.CADENA_VACIA)
								? apertura.getDescripcionOficina() : null;

				List<OficinasVO> listOficinas = cargarCombosController.consultarOficinas(auxCodigoOficina,
						auxDescripcionOficina, ConstantesWeb.OFICINA_SOS, null, new Date());
				procesarResultadoConsultaOficina(listOficinas);
			} catch (LogicException e) {
				this.mostrarMensajeError(e);
			} catch (ConnectionProviderException e) {
				this.mostrarMensajeError(e);
			} catch (IOException e) {
				this.mostrarMensajeError(e);
			}
		} else {
			MostrarMensaje.mostrarMensaje(ConstantesWeb.CAJA_INGRESAR_OFICINA,COLOR_ROJO);
		}
		return null;
	}

	private void procesarResultadoConsultaOficina(List<OficinasVO> listOficinas) {
		if (listOficinas != null && !listOficinas.isEmpty() && listOficinas.size() == 1) {
			oficinaSeleccionada = listOficinas.get(0);
			apertura.setCodigoOficina(oficinaSeleccionada.getCodigoOficina());
			apertura.setDescripcionOficina(oficinaSeleccionada.getDescripcionOficina());
			apertura.setOficinaVOseleccionada(oficinaSeleccionada);
		} else {
			apertura.setListOficinas(listOficinas);
			apertura.setMostrarModalOficinas(true);
		}
	}

	private boolean validarCampos() {
		if (fechaFinal == null || fechaInicial == null) {
			this.mostrarMensajeError(ConstantesWeb.MENSAJE_FILTROS_REPORTE);
			return false;
		}
		if (fechaInicial.getTime() > fechaFinal.getTime()) {
			this.mostrarMensajeError(ConstantesWeb.MENSAJE_FECHAS_REPORTE);
			return false;
		}
		return true;
	}

	/**
	 * Realiza la consulta del reporte con los datos ingresados.
	 * 
	 * @return
	 */
	public String consultarInforme() {
		Integer oficina = null;
		Integer sede = null;
		try {
			if (this.validarCampos()) {
				if (oficinaSeleccionada != null && oficinaSeleccionada.getConsecutivoCodigoOficina() <= 0) {
					oficina = oficinaSeleccionada.getConsecutivoCodigoOficina();
				}

				if (!Utilidades.validarCamposVacios(login)) {
					login = null;
				}

				if (consecutivoSede != -1) {
					sede = consecutivoSede;
				}

				ReciboCajaController controlador = new ReciboCajaController();
				informe = controlador.generarInformeCierre(fechaInicial, fechaFinal, oficina, login, sede);
				if (informe != null && !informe.isEmpty()) {
					habilitarDescarga = false;
				} else {
					this.mostrarMensajeError(ConstantesWeb.CONSULTA_NO_ARROJO_RESULTADOS);
				}
			} else {
				habilitarDescarga = true;
			}
		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		}
		return null;
	}

	/**
	 * Limpia el formulario
	 * 
	 * @return
	 */
	public String limpiarDatos() {
		fechaFinal = null;
		fechaInicial = null;
		oficinaSeleccionada = null;
		apertura.setCodigoOficina(ConstantesWeb.CADENA_VACIA);
		apertura.setDescripcionOficina(ConstantesWeb.CADENA_VACIA);
		apertura.setOficinaVOseleccionada(null);
		login = "";
		apertura.setMostrarModalOficinas(false);
		habilitarDescarga = true;
		habilitarDescargaDetallado = true;
		informe = new ArrayList<InformeGeneralReciboVO>();
		afiliado = new AfiliadoNombresVO();
		numeroIdentificacionAfiliado = "";
		consecutivoTipoIdentificacion = null;
		numeroRecibo = null;
		numeroOPS = null;
		reporteCajaDetallado = new ArrayList<ReporteCajaDetalladoVO>();
		consecutivoSede = -1;
		nombreCompleto = "";
		usuario = "";
		estadoNotaSeleccionado = null;
		reporteNotaCredito = new ArrayList<InformeNotasCreditoVO>();
		return null;
	}

	/**
	 * Valida el campo obligatorio de la fecha para el reporte detallado.
	 * 
	 * @return
	 */
	private boolean validarCamposInformeDetallado() {
		if (fechaInicial == null) {
			this.mostrarMensajeError(ConstantesWeb.ERROR_FECHA_VACIA);
			return false;
		}
		return true;
	}

	private void formarParametrosReporteRecibos(ParametroReporteCierreCajaVO entradaReporte) {
		if (numeroOPS != null && numeroOPS > 0) {
			entradaReporte.setNumeroOps(numeroOPS);
		}

		if (numeroRecibo != null && numeroRecibo > 0) {
			entradaReporte.setNumeroRecibo(numeroRecibo);
		}
	}

	private void formarParametrosReporteUbicacion(ParametroReporteCierreCajaVO entradaReporte) {
		if (oficinaSeleccionada != null && oficinaSeleccionada.getConsecutivoCodigoOficina() == 0) {
			entradaReporte.setConsecutivoCodigoOficina(oficinaSeleccionada.getConsecutivoCodigoOficina());
		}
		if (consecutivoSede != -1) {
			entradaReporte.setConsecutivoCodigoSede(consecutivoSede);
		}
	}

	/**
	 * Seleccion del usuario del modal
	 * 
	 * @param event
	 */
	public void seleccionarUsuario(ActionEvent event) {
		this.usuario = usuarioSeleccionado.getNombreCompleto();
		this.login = usuarioSeleccionado.getLgnUsro();
	}

	private void formarParametrosReportePersona(ParametroReporteCierreCajaVO entradaReporte) {
		if (Utilidades.validarCamposVacios(login)) {
			entradaReporte.setLogin(login);
		}
		if (afiliado != null && afiliado.getNumeroUnicoIdentificacionInfoAfiliado() != null) {
			entradaReporte.setNui(afiliado.getNumeroUnicoIdentificacionInfoAfiliado());
		}
	}

	private ParametroReporteCierreCajaVO formarParametrosReporte() {
		ParametroReporteCierreCajaVO entradaReporte = new ParametroReporteCierreCajaVO();
		entradaReporte.setFechaInicial(fechaInicial);
		this.formarParametrosReporteRecibos(entradaReporte);
		this.formarParametrosReporteUbicacion(entradaReporte);
		this.formarParametrosReportePersona(entradaReporte);
		return entradaReporte;
	}

	/**
	 * limpia los campos.
	 */
	public void limpiarCampos() {
		this.usuario = ConstantesWeb.CADENA_VACIA;
		this.login = ConstantesWeb.CADENA_VACIA;
		this.listaUsuarios = new ArrayList<UsuarioWebVO>();
	}

	/**
	 * Valida que al menos el usuario o el login no sean vacios.
	 * 
	 * @return
	 */
	public boolean validarCamposBusquedaUsuario() {
		if (Utilidades.validarCamposVacios(usuario) || Utilidades.validarCamposVacios(login)) {
			return true;
		}
		return false;
	}

	/**
	 * Reaiza la consulta de usuario a partir del login y nombre de usuario
	 * 
	 * @param event
	 * @throws LogicException
	 */
	public void consultarUsuario(ActionEvent event) {
		try {
			if (this.validarCamposBusquedaUsuario()) {
				GestionCajaController control = new GestionCajaController();
				listaUsuarios = new ArrayList<UsuarioWebVO>();
				if (Utilidades.validarCamposVacios(usuario)) {
					listaUsuarios = control.consultarUsuariosLogin(usuario, null);
				}
				if (Utilidades.validarCamposVacios(login)) {
					listaUsuarios = control.consultarUsuariosLogin(null, login);
				}
				if (listaUsuarios == null || listaUsuarios.isEmpty()) {
					this.mostrarMensajeError(ConstantesWeb.CONSULTA_NO_ARROJO_RESULTADOS);
					mostrarModalUsuarios = false;
				} else if (listaUsuarios.size() == 1) {
					mostrarModalUsuarios = false;
					usuario = listaUsuarios.get(0).getNombreCompleto();
					login = listaUsuarios.get(0).getLgnUsro();
				} else {
					mostrarModalUsuarios = true;
				}
			} else {
				this.mostrarMensajeError(ConstantesWeb.ERROR_USUARIO_LOGIN_OBLIGATORIO);
			}
		} catch (Exception e) {
			this.mostrarMensajeError(e);
		}
	}

	/**
	 * Almacena los datos del VO en los campos
	 * 
	 * @return
	 */
	public String seleccionUsuario() {
		usuario = usuarioSeleccionado.getNombreCompleto();
		login = usuarioSeleccionado.getLgnUsro();
		return null;
	}

	/**
	 * Metodo que se encarga de ralizar la consulta del reporte.
	 * 
	 * @return
	 */
	public String consultarInformeDetalle() {
		try {
			if (this.validarCamposInformeDetallado()) {
				GestionCajaController control;

				control = new GestionCajaController();

				ParametroReporteCierreCajaVO entradaReporte = this.formarParametrosReporte();

				reporteCajaDetallado = control.consultarReporteCajaDetallado(entradaReporte);

				if (reporteCajaDetallado == null || (reporteCajaDetallado.isEmpty())) {
					this.mostrarMensajeError(ConstantesWeb.CONSULTA_NO_ARROJO_RESULTADOS);
					habilitarDescargaDetallado = true;
				} else {
					habilitarDescargaDetallado = false;
				}
			}
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeError(e);
		} catch (DataAccessException e) {
			this.mostrarMensajeError(e);
		}
		return null;
	}

	/**
	 * Generar el informe jasper en excel
	 * 
	 * @return
	 */
	public String exportarInforme() {
		GenerarReporteBean reporte = new GenerarReporteBean();
		try {
			Integer pOficina = null;
			String pLogin = null;
			Integer pSede = null;

			if (oficinaSeleccionada != null && oficinaSeleccionada.getConsecutivoCodigoOficina() != null) {
				pOficina = oficinaSeleccionada.getConsecutivoCodigoOficina();
			}

			if (Utilidades.validarCamposVacios(login)) {
				pLogin = login;
			}

			if (consecutivoSede != -1) {
				pSede = consecutivoSede;
			}

			reporte.generarReporteCierreCaja(fechaInicial, fechaFinal, pOficina, pLogin, pSede);
		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		}
		return null;
	}

	/**
	 * Exporta el reporte detallado a excel
	 * 
	 * @return
	 */
	public String exportarInformeDetallado() {
		GenerarReporteBean reporte = new GenerarReporteBean();
		ParametroReporteCierreCajaVO entradaReporte = this.formarParametrosReporte();
		try {
			reporte.generarReporteCierreCajaDetallado(entradaReporte);
		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String verDetalleCausales() {
		ReciboCajaController controlador;
		try {
			controlador = new ReciboCajaController();
			listaDescuadreMovimientoCaja = controlador.obtenerCausalesDescuadreCaja(consecutivoMovimientoCaja);
			if (listaDescuadreMovimientoCaja == null || listaDescuadreMovimientoCaja.isEmpty()) {
				verCausales = false;
				this.mostrarMensajeError(ConstantesWeb.CONSULTA_NO_TIENE_CAUSALES_DESCUADRE);
			} else {
				verCausales = true;
			}

		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		}

		return null;
	}

	private boolean validarCamposAfiliado() {
		if (!Utilidades.validarCamposVacios(numeroIdentificacionAfiliado)
				|| !Utilidades.validarCamposVacios(consecutivoTipoIdentificacion)) {
			this.mostrarMensajeError(ConstantesWeb.IDENTIFICACION_OBLIGATORIA);
			return false;
		}
		return true;
	}

	/**
	 * Consulta del afiliado
	 * 
	 * @return
	 */
	public String consultarAfiliado() {
		try {
			if (this.validarCamposAfiliado()) {
				afiliado = this.buscarAfiliado();
				if (afiliado == null) {
					this.mostrarMensajeError(ConstantesWeb.AFILIADO_NO_EXISTE);
				} else {
					nombreCompleto = afiliado.getNombreCompleto();
				}
			}
		} catch (LogicException e) {
			this.mostrarMensajeError(e);
		}

		return null;
	}

	private AfiliadoNombresVO buscarAfiliado() throws LogicException {
		GestionCajaController control;
		AfiliadoNombresVO obtenerAfiliado;
		try {
			control = new GestionCajaController();
			obtenerAfiliado = control.obtenerAfiliado(this.consecutivoTipoIdentificacion,
					this.numeroIdentificacionAfiliado);
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (DataAccessException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return obtenerAfiliado;
	}

	private void mostrarMensajeError(Exception e) {
		LOG.error(e.getMessage(), e);
		MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
	}

	private void mostrarMensajeError(String mensajeError) {
		MostrarMensaje.mostrarMensaje(mensajeError, COLOR_ROJO);
	}

	/**
	 * 
	 * @return
	 */
	public String consultarNotasCreditoGeneradas() {
		Integer nuiAux = null;
		Integer estadoAux = null;
		if (this.validarCampos()) {
			if (afiliado != null) {
				nuiAux = afiliado.getNumeroUnicoIdentificacionInfoAfiliado();
			}

			if (estadoNotaSeleccionado != -1) {
				estadoAux = estadoNotaSeleccionado;
			}

			GestionCajaController controller;
			try {
				controller = new GestionCajaController();
				reporteNotaCredito = controller.consultarReporteNotasCredito(fechaInicial, fechaFinal, nuiAux,
						estadoAux);
				if (reporteNotaCredito.isEmpty()) {
					mostrarMensajeError(ConstantesWeb.CONSULTA_NO_ARROJO_RESULTADOS);
				}
			} catch (ConnectionProviderException e) {
				this.mostrarMensajeError(e);
			} catch (DataAccessException e) {
				this.mostrarMensajeError(e);
			}
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String consultarDatosNotaCredito() {
		try {
			GenerarNotaCreditoBean notaCreditoBean = (GenerarNotaCreditoBean) FacesUtils
					.getManagedBean("generarNotaCreditoBean");

			GenerarReciboBean recibo = (GenerarReciboBean) FacesUtils.getManagedBean("generarReciboBean");

			if (recibo == null) {
				recibo = new GenerarReciboBean();
			}

			if (notaCreditoBean == null) {
				notaCreditoBean = new GenerarNotaCreditoBean();
			}

			GestionCajaController controller = new GestionCajaController();

			OPSCajaVO opsValor = controller
					.obtenerInformacionOPSValor(notaCreditoSeleccionada.getConsecutivoCodigoDocumento());

			AfiliadoVO afiliadoVO = obtenerInformacionAfiliado();
			obtenerDatosContacto(notaCreditoBean, controller);
			datosReciboCaja(recibo, afiliadoVO);
			datosNotaCredito(notaCreditoBean, opsValor, afiliadoVO);

		} catch (ConnectionProviderException e) {
			this.mostrarMensajeError(e);
		} catch (DataAccessException e) {
			this.mostrarMensajeError(e);
		}
		return null;
	}

	private void datosNotaCredito(GenerarNotaCreditoBean notaCreditoBean, OPSCajaVO opsValor, AfiliadoVO afiliadoVO) {
		notaCreditoBean.setOps(opsValor.getOps());
		notaCreditoBean.setValorADevolverNotaCredito(notaCreditoSeleccionada.getValorDocumento());
		notaCreditoBean.setNumeroIdentificacionAfiliado(notaCreditoSeleccionada.getNumeroIdentificacionAfiliado());
		notaCreditoBean.setConsecutivoTipoIdentificacionAfiliado(
				notaCreditoSeleccionada.getConsecutivoCodigoTipoDocumentoAfiliado());
		notaCreditoBean.setAccionARealizar(ConstantesWeb.CADENA_VACIA);

		notaCreditoBean.setAfiliado(afiliadoVO);

		notaCreditoBean.setConsecutivoDocumentoCaja(notaCreditoSeleccionada.getConsecutivoCodigoDocumento());
		notaCreditoBean.setObservacionNotaCredito(notaCreditoSeleccionada.getObservacion());
		notaCreditoBean.setNumeroNotaCredito(notaCreditoSeleccionada.getNumeroDocumento());

		if (notaCreditoSeleccionada
				.getConsecutivoEstadoDocumento() == ConstantesWeb.CODIGO_ESTADO_DOCUMENTO_CAJA_COBRADA) {
			notaCreditoBean.setDeshabilitarBotonGenerarNotaCredito(true);
			notaCreditoBean.setDeshabilitarBotonGenerarNotaCreditoBanco(true);
		} else {
			notaCreditoBean.setDeshabilitarBotonGenerarNotaCredito(false);
			notaCreditoBean.setDeshabilitarBotonGenerarNotaCreditoBanco(false);
		}
	}

	private void datosReciboCaja(GenerarReciboBean recibo, AfiliadoVO afiliadoVO) {
		recibo.setMostrarModalReciboCaja(true);
		recibo.setMostrarTabGenerarNotasCredito(true);
		recibo.setMostrarGenerarRecibo(false);
		recibo.setTabSeleccionado(ConstantesWeb.TAB_GENERAR_NOTA_CREDITO);
		recibo.setNuiAfiliado(notaCreditoSeleccionada.getNuiAfililado());
		recibo.setDesabilitarNotaCredito(true);
		recibo.setAfiliado(afiliadoVO);
		recibo.setProvieneInformeNotaCredito(true);

		if (notaCreditoSeleccionada
				.getConsecutivoEstadoDocumento() == ConstantesWeb.CODIGO_ESTADO_DOCUMENTO_CAJA_COBRADA) {
			if (notaCreditoSeleccionada.getConsecutivoContacto() > 0) {
				recibo.setMostrarTabDatosBancoNotaCredito(true);
				recibo.setTabSeleccionado(ConstantesWeb.TAB_DATOS_BANCO);
			} else {
				recibo.setMostrarTabDatosBancoNotaCredito(false);
			}
		}
	}

	private void obtenerDatosContacto(GenerarNotaCreditoBean notaCreditoBean, GestionCajaController controller)
			throws DataAccessException {
		if (notaCreditoSeleccionada.getConsecutivoContacto() > 0) {
			DatosContactoVO datoContacto = controller
					.obtenerInformacionDatoContacto(notaCreditoSeleccionada.getConsecutivoContacto());
			notaCreditoBean.setConsecutivoBanco(String.valueOf(datoContacto.getConsecutivoBanco()));
			notaCreditoBean.setConsectivoMetodoDevolucion(datoContacto.getConsecutivoMetodoDevolucion());
			notaCreditoBean.setCorreoElectronico(datoContacto.getCorreoElectronico());
			notaCreditoBean.setNumeroCuenta(datoContacto.getNumeroCuenta());
			notaCreditoBean.setNumeroTelefono(datoContacto.getTelefono());
			notaCreditoBean.setTipoCuenta(datoContacto.getTipoCuenta());
			
			String titularAux = datoContacto.getTitular();
			if (titularAux != null && !titularAux.equals(ConstantesWeb.CADENA_VACIA)){
				String[] titularArray = titularAux.split(ConstantesWeb.SEPARADOR);
				notaCreditoBean.setTitular(titularArray[0]);
			} else {
				notaCreditoBean.setTitular("");
			}
			
			notaCreditoBean.setConsecutivoContacto(notaCreditoSeleccionada.getConsecutivoContacto());
		} else {
			notaCreditoBean.setConsecutivoBanco("-1");
			notaCreditoBean.setConsectivoMetodoDevolucion(-1);
			notaCreditoBean.setCorreoElectronico(ConstantesWeb.CADENA_VACIA);
			notaCreditoBean.setNumeroCuenta(ConstantesWeb.CADENA_VACIA);
			notaCreditoBean.setNumeroTelefono(ConstantesWeb.CADENA_VACIA);
			notaCreditoBean.setTipoCuenta(ConstantesWeb.CADENA_VACIA);
			notaCreditoBean.setTitular(null);
			notaCreditoBean.setConsecutivoContacto(notaCreditoSeleccionada.getConsecutivoContacto());
		}
	}

	private AfiliadoVO obtenerInformacionAfiliado() {
		AfiliadoVO afiliadoVO = new AfiliadoVO();
		TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
		tiposIdentificacionVO.setCodigoTipoIdentificacion(notaCreditoSeleccionada.getTipoIdentificacionAfiliado());
		afiliadoVO.setNumeroIdentificacion(notaCreditoSeleccionada.getNumeroIdentificacionAfiliado());
		afiliadoVO.setNombreCompleto(notaCreditoSeleccionada.getNombreCompletoAfiliado());

		afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
		return afiliadoVO;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isHabilitarDescarga() {
		return habilitarDescarga;
	}

	public void setHabilitarDescarga(boolean habilitarDescarga) {
		this.habilitarDescarga = habilitarDescarga;
	}

	public List<InformeGeneralReciboVO> getInforme() {
		return informe;
	}

	public void setInforme(List<InformeGeneralReciboVO> informe) {
		this.informe = informe;
	}

	public static String getColorRojo() {
		return COLOR_ROJO;
	}

	public int getConsecutivoMovimientoCaja() {
		return consecutivoMovimientoCaja;
	}

	public void setConsecutivoMovimientoCaja(int consecutivoMovimientoCaja) {
		this.consecutivoMovimientoCaja = consecutivoMovimientoCaja;
	}

	public boolean isVerCausales() {
		return verCausales;
	}

	public void setVerCausales(boolean verCausales) {
		this.verCausales = verCausales;
	}

	public List<CausalesDescuadreValorVO> getListaDescuadreMovimientoCaja() {
		return listaDescuadreMovimientoCaja;
	}

	public void setListaDescuadreMovimientoCaja(List<CausalesDescuadreValorVO> listaDescuadreMovimientoCaja) {
		this.listaDescuadreMovimientoCaja = listaDescuadreMovimientoCaja;
	}

	public Integer getConsecutivoTipoIdentificacion() {
		return consecutivoTipoIdentificacion;
	}

	public void setConsecutivoTipoIdentificacion(Integer consecutivoTipoIdentificacion) {
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
	}

	public String getNumeroIdentificacionAfiliado() {
		return numeroIdentificacionAfiliado;
	}

	public void setNumeroIdentificacionAfiliado(String numeroIdentificacionAfiliado) {
		this.numeroIdentificacionAfiliado = numeroIdentificacionAfiliado;
	}

	public Integer getNumeroRecibo() {
		return numeroRecibo;
	}

	public void setNumeroRecibo(Integer numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}

	public Integer getNumeroOPS() {
		return numeroOPS;
	}

	public void setNumeroOPS(Integer numeroOPS) {
		this.numeroOPS = numeroOPS;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public List<ReporteCajaDetalladoVO> getReporteCajaDetallado() {
		return reporteCajaDetallado;
	}

	public void setReporteCajaDetallado(List<ReporteCajaDetalladoVO> reporteCajaDetallado) {
		this.reporteCajaDetallado = reporteCajaDetallado;
	}

	public boolean isHabilitarDescargaDetallado() {
		return habilitarDescargaDetallado;
	}

	public void setHabilitarDescargaDetallado(boolean habilitarDescargaDetallado) {
		this.habilitarDescargaDetallado = habilitarDescargaDetallado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<SelectItem> getListaSedes() {
		return listaSedes;
	}

	public void setListaSedes(List<SelectItem> listaSedes) {
		this.listaSedes = listaSedes;
	}

	public int getConsecutivoSede() {
		return consecutivoSede;
	}

	public void setConsecutivoSede(int consecutivoSede) {
		this.consecutivoSede = consecutivoSede;
	}

	public boolean isMostrarModalUsuarios() {
		return mostrarModalUsuarios;
	}

	public void setMostrarModalUsuarios(boolean mostrarModalUsuarios) {
		this.mostrarModalUsuarios = mostrarModalUsuarios;
	}

	public List<UsuarioWebVO> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioWebVO> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public UsuarioWebVO getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(UsuarioWebVO usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public InformeGeneralReciboVO getFilaSeleccionada() {
		return filaSeleccionada;
	}

	public void setFilaSeleccionada(InformeGeneralReciboVO filaSeleccionada) {
		this.filaSeleccionada = filaSeleccionada;
	}

	public List<SelectItem> getListaEstadosNotaCredito() {
		return listaEstadosNotaCredito;
	}

	public void setListaEstadosNotaCredito(List<SelectItem> listaEstadosNotaCredito) {
		this.listaEstadosNotaCredito = listaEstadosNotaCredito;
	}

	public Integer getEstadoNotaSeleccionado() {
		return estadoNotaSeleccionado;
	}

	public void setEstadoNotaSeleccionado(Integer estadoNotaSeleccionado) {
		this.estadoNotaSeleccionado = estadoNotaSeleccionado;
	}

	public List<InformeNotasCreditoVO> getReporteNotaCredito() {
		return reporteNotaCredito;
	}

	public void setReporteNotaCredito(List<InformeNotasCreditoVO> reporteNotaCredito) {
		this.reporteNotaCredito = reporteNotaCredito;
	}

	public InformeNotasCreditoVO getNotaCreditoSeleccionada() {
		return notaCreditoSeleccionada;
	}

	public void setNotaCreditoSeleccionada(InformeNotasCreditoVO notaCreditoSeleccionada) {
		this.notaCreditoSeleccionada = notaCreditoSeleccionada;
	}
}
