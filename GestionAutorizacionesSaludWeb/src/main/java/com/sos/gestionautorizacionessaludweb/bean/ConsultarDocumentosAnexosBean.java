package com.sos.gestionautorizacionessaludweb.bean;


import java.util.List;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.visos.service.to.containers.IndicesContainer;

/**
 * Class ConsultarDocumentosAnexosBean
 * @author ing. Victor Hugo Gil Ramos
 * @version 03/02/2016
 *
 */
public class ConsultarDocumentosAnexosBean {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultarDocumentosAnexosBean.class);	
	private ControladorVisosServiceEJB controladorVisosServiceEJB;	
	
	
	private List<DocumentoSoporteVO> listaDocumentosSoporte ;
	
	public ConsultarDocumentosAnexosBean(){
		controladorVisosServiceEJB = new ControladorVisosServiceEJB();
		try {
			controladorVisosServiceEJB.crearConexionVisosService();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}	
	}	
	
	public List<DocumentoSoporteVO> consultarDocumentos(IndicesContainer indices){		
		try {
			listaDocumentosSoporte = controladorVisosServiceEJB.consultarDocumentoAnexoxIndices(indices);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_SERVICIO_VISOS), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}		
		return listaDocumentosSoporte;		
	}
	
	public List<DocumentoSoporteVO> consultarDocumentosxIdDocumento(long idDocumento){		
		try {
			listaDocumentosSoporte = controladorVisosServiceEJB.consultarDocumentoAnexoxIdDocumento(idDocumento);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_SERVICIO_VISOS), "red");
		} catch (Exception e) {
			LOG.error(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), "red");
		}		
		return listaDocumentosSoporte;		
	}
}
