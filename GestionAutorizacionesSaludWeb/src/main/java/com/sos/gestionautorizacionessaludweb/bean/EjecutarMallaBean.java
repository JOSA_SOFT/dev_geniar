package com.sos.gestionautorizacionessaludweb.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import co.com.sos.consultarresultadomalla.v1.ConsultarResultadoMallaResType;
import co.com.sos.ejecutarmalla.v1.EjecutarMallaResType;
import co.com.sos.enterprise.malla.v1.MallaService;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.EjecutarMalla;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;

/**
 * Class que permite ejecutar el servicio de malla 
 * @author Ing. Victor Hugo Gil
 * @version 01/08/2016
 */
public class EjecutarMallaBean {
	
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(EjecutarMallaBean.class);

	private List<PrestacionDTO> listaPrestacionesInconsistenciasMallaService;	
	
	/**
	 * metodo que ejecuta el servicio de malla
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public InconsistenciasVO ejecutarServicioMalla(MallaService mallaService, EjecutarMalla ejecutarMalla, SolicitudVO solicitud) throws LogicException {
		
		InconsistenciasVO inconsistenciasVO = new InconsistenciasVO();

		try {
			LOG.debug("INICIO ejecutarServicioMalla BEAN");
			EjecutarMallaResType ejecutarMallaResType = ejecutarMalla.ejecutarMallaService(mallaService, solicitud, FacesUtils.getUserName());
			LOG.debug("FIN ejecutarServicioMalla BEAN");
			LOG.debug("INICIO consultarResultadoInconsistenciasMalla BEAN");
			inconsistenciasVO = ejecutarMalla.consultarResultadoInconsistenciasMalla(ejecutarMallaResType, null);
			LOG.debug("FIN consultarResultadoInconsistenciasMalla BEAN");
			listaPrestacionesInconsistenciasMallaService = inconsistenciasVO.getListaPrestaciones();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			try {
				LOG.debug("INICIO consultarResultadoMalla BEAN");
				ConsultarResultadoMallaResType consultarResultadoMallaResType = ejecutarMalla.consultarResultadoMalla(mallaService, solicitud, FacesUtils.getUserName());
				LOG.debug("FIN consultarResultadoMalla BEAN");
				inconsistenciasVO = ejecutarMalla.consultarResultadoInconsistenciasMalla(null, consultarResultadoMallaResType);
				listaPrestacionesInconsistenciasMallaService = inconsistenciasVO.getListaPrestaciones();
			} catch (Exception ex) {
				LOG.error(ex.getMessage(), ex);
				throw new LogicException(ex.getMessage(), ErrorType.INDEFINIDO);
			}
		}
		
		return inconsistenciasVO;
	}
	
		
	public List<PrestacionDTO> getListaPrestacionesInconsistenciasMallaService() {
		if (listaPrestacionesInconsistenciasMallaService == null){
			listaPrestacionesInconsistenciasMallaService = new ArrayList<PrestacionDTO>();
		}
		return listaPrestacionesInconsistenciasMallaService;
	}

	public void setListaPrestacionesInconsistenciasMallaService(
			List<PrestacionDTO> listaPrestacionesInconsistenciasMallaService) {
		this.listaPrestacionesInconsistenciasMallaService = listaPrestacionesInconsistenciasMallaService;
	}

}
