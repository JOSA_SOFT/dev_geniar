package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;

/**
 * Clase para apertura de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 */
public class AperturaCajaBean implements Serializable {
	private static final long serialVersionUID = 5483618631629782546L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger
			.getLogger(ConsultaAfiliadoBean.class);

	private Date fechaActual;
	private String userLogin;
	private String codigoOficina;
	private String descripcionOficina;
	private BigInteger base;
	private OficinasVO oficinaVOseleccionada;
	private List<String> listErrores;
	private List<OficinasVO> listOficinas;
	private boolean mostrarConfirmacionUno;
	private boolean mostrarModalError;
	private boolean mostrarModalOficinas;
	private String valorConfirmacionCadena;

	/**
	 * Post Construct
	 */
	@PostConstruct
	public void init() {
		try {
			fechaActual = new Date();
			userLogin = FacesUtils.getUserName();
			listOficinas = new ArrayList<OficinasVO>();
			codigoOficina = "";
			descripcionOficina = "";
			oficinaVOseleccionada = null;
			base = null;
			listErrores = null;
			mostrarConfirmacionUno = false;
			mostrarModalError = false;
			mostrarModalOficinas = false;
		} catch (Exception e) {
			this.mostrarMensajeExcepcionApertura(e);
		}
	}

	/**
	 * Limpia campos
	 */
	public void limpiarCampos() {
		codigoOficina = "";
		descripcionOficina = "";
		oficinaVOseleccionada = null;
		base = null;
		listErrores = null;
		mostrarConfirmacionUno = false;
		mostrarModalError = false;
		mostrarModalOficinas = false;
	}

	/**
	 * Realiza la consulta de las oficinas por los campos seleccionados
	 */
	public void consultarOficinas() {
		if (!validarCamposBusquedaOficina()) {
			try {
				CargarCombosController cargarCombosController = new CargarCombosController();
				if (!Utilidades.validarNullCadena(codigoOficina).equals(
						ConstantesWeb.CADENA_VACIA)
						&& Utilidades.validarCamposVacios(codigoOficina)) {
					listOficinas = cargarCombosController
							.consultarOficinas(codigoOficina, null, ConstantesWeb.OFICINA_SOS, null, new Date());
				} else if (!Utilidades.validarNullCadena(descripcionOficina)
						.equals(ConstantesWeb.CADENA_VACIA)
						&& Utilidades.validarCamposVacios(descripcionOficina)) {
					listOficinas = cargarCombosController
							.consultarOficinas(null, descripcionOficina, ConstantesWeb.OFICINA_SOS, null, new Date());
				}
				if (listOficinas.size() == 1) {
					codigoOficina = listOficinas.get(0).getCodigoOficina();
					descripcionOficina = listOficinas.get(0)
							.getDescripcionOficina();
					oficinaVOseleccionada = listOficinas.get(0);
				} else {
					mostrarModalOficinas = true;
				}

			} catch (LogicException e) {
				this.mostrarMensajeExcepcionApertura(e);
			} catch (Exception e) {
				this.mostrarMensajeExcepcionApertura(e);
			}
		}
	}

	private void mostrarMensajeExcepcionApertura(Exception e) {
		LOG.error(e.getMessage(), e);
		List<String> errorTemp = new ArrayList<String>();
		errorTemp.add(e.getMessage());
		mostrarModalError(errorTemp);
	}

	/**
	 * Selecciona una oficina y setea los campos
	 */
	public void seleccionarOficina() {
		codigoOficina = oficinaVOseleccionada.getCodigoOficina().trim();
		descripcionOficina = oficinaVOseleccionada.getDescripcionOficina()
				.trim();
		listOficinas = null;
		mostrarModalOficinas = false;
	}

	/**
	 * Metodo para abrir caja
	 * 
	 * @return
	 */
	public String abrirCaja() {
		mostrarConfirmacionUno = false;
		mostrarModalError = false;
		mostrarModalOficinas = false;
		try {
			GestionCajaController cajaController = new GestionCajaController();
			if (this.oficinaVOseleccionada == null) {
				List<String> errorTemp = new ArrayList<String>();
				errorTemp.add(ConstantesWeb.ERROR_CAJA_CONSULTAR_OFICINA);
				mostrarModalError(errorTemp);
			} else {

				if (cajaController.consultarMovimientoCajaAbierta(userLogin)
						.isEmpty()) {
					cajaController.abrirCaja(this.base.doubleValue(),
							this.userLogin == null ? ConstantesWeb.CADENA_VACIA
									: userLogin, this.oficinaVOseleccionada
									.getConsecutivoCodigoOficina(),
							ConstantesWeb.CONSECUTIVO_ESTADO_APERTURA,
							FacesUtils.obtenerIPCliente(), null);
				} else {
					List<String> errorTemp = new ArrayList<String>();
					errorTemp.add(ConstantesWeb.ERROR_EXISTE_CAJA_APERTURADA);
					mostrarModalError(errorTemp);
					return "";
				}
			}
		} catch (DataAccessException e) {
			this.mostrarMensajeExcepcionApertura(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeExcepcionApertura(e);
		}
		return ConstantesWeb.PATH_CONSULTAR_AFILIADO;
	}

	/**
	 * Valida todos los campos
	 */
	public void validarCaja() {
		List<String> errores = new ArrayList<String>();

		errores.addAll(validarOficina());
		errores.addAll(validarBase());

		mostrarModalErrorBeforeConfirmacionUno(errores);
	}

	/**
	 * Valida cuando se preciona en la busqueda
	 * 
	 * @return true si
	 */
	private boolean validarCamposBusquedaOficina() {
		if ((Utilidades.validarNullCadena(codigoOficina).equals(
				ConstantesWeb.CADENA_VACIA) || !Utilidades
				.validarCamposVacios(codigoOficina))
				&& (Utilidades.validarNullCadena(descripcionOficina).equals(
						ConstantesWeb.CADENA_VACIA) || !Utilidades
						.validarCamposVacios(descripcionOficina))) {
			List<String> errorTemp = new ArrayList<String>();
			errorTemp.add(ConstantesWeb.CAJA_INGRESAR_OFICINA);
			mostrarModalError(errorTemp);
			return true;
		}
		return false;
	}

	/**
	 * Valida los campos de oficina
	 */
	private List<String> validarOficina() {
		List<String> errores = new ArrayList<String>();
		if (Utilidades.validarNullCadena(codigoOficina).equals(
				ConstantesWeb.CADENA_VACIA)
				|| !Utilidades.validarCamposVacios(codigoOficina)) {
			errores.add(ConstantesWeb.VACIOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.CODIGO_OFICINA));
		}
		if (!Utilidades.validarNullCadena(codigoOficina).equals(
				ConstantesWeb.CADENA_VACIA)
				&& codigoOficina.length() > 5) {
			errores.add(ConstantesWeb.TAMANOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.ERROR_CODIGO_OFICINA));
		}
		if (Utilidades.validarNullCadena(descripcionOficina).equals(
				ConstantesWeb.CADENA_VACIA)
				|| !Utilidades.validarCamposVacios(descripcionOficina)) {
			errores.add(ConstantesWeb.VACIOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.ERROR_DESC_OFICINA));
		}
		if (!Utilidades.validarNullCadena(descripcionOficina).equals(
				ConstantesWeb.CADENA_VACIA)
				&& (descripcionOficina.length() < 5 || descripcionOficina
						.length() > 150)) {
			errores.add(ConstantesWeb.TAMANOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.ERROR_CONSUL_OFICINA));
		}
		return errores;
	}

	/**
	 * Valida el campo base
	 */
	private List<String> validarBase() {
		List<String> errores = new ArrayList<String>();
		if (Utilidades.validarNullCadena(base).equals(
				ConstantesWeb.CADENA_VACIA)) {
			errores.add(ConstantesWeb.VACIOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.BASE_APERTURA));
		}
		if (!Utilidades.validarNullCadena(base).equals(
				ConstantesWeb.CADENA_VACIA)
				&& base.toString().length() > 6) {
			errores.add(ConstantesWeb.TAMANOERROR.replace(
					ConstantesWeb.CAJA_REPLACE_NOMBRE,
					ConstantesWeb.BASE_APERTURA));
		}
		return errores;
	}

	/**
	 * Muestra el modal error despues de validar los campos
	 */
	private void mostrarModalErrorBeforeConfirmacionUno(List<String> errores) {
		if (errores.isEmpty()) {
			mostrarConfirmacionUno = true;
			mostrarModalError = false;
			DecimalFormat myFormatter = new DecimalFormat(ConstantesWeb.FORMATO_DINERO);
			valorConfirmacionCadena = myFormatter.format(base);
		} else {
			listErrores = errores;
			mostrarConfirmacionUno = false;
			mostrarModalError = true;
		} 
	}

	/**
	 * Mostrar modal error
	 * 
	 * @param errores
	 */
	private void mostrarModalError(List<String> errores) {
		if (!errores.isEmpty()) {
			listErrores = errores;
			mostrarConfirmacionUno = false;
			mostrarModalOficinas = false;
			mostrarModalError = true;
		}
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getCodigoOficina() {
		return codigoOficina;
	}

	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}

	public String getDescripcionOficina() {
		return descripcionOficina;
	}

	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}

	public BigInteger getBase() {
		return base;
	}

	public void setBase(BigInteger base) {
		this.base = base;
	}

	public boolean isMostrarConfirmacionUno() {
		return mostrarConfirmacionUno;
	}

	public void setMostrarConfirmacionUno(boolean mostrarConfirmacionUno) {
		this.mostrarConfirmacionUno = mostrarConfirmacionUno;
	}

	public boolean isMostrarModalError() {
		return mostrarModalError;
	}

	public void setMostrarModalError(boolean mostrarModalError) {
		this.mostrarModalError = mostrarModalError;
	}

	public List<String> getListErrores() {
		return listErrores;
	}

	public void setListErrores(List<String> listErrores) {
		this.listErrores = listErrores;
	}

	public List<OficinasVO> getListOficinas() {
		return listOficinas;
	}

	public void setListOficinas(List<OficinasVO> listOficinas) {
		this.listOficinas = listOficinas;
	}

	public boolean isMostrarModalOficinas() {
		return mostrarModalOficinas;
	}

	public void setMostrarModalOficinas(boolean mostrarModalOficinas) {
		this.mostrarModalOficinas = mostrarModalOficinas;
	}

	public OficinasVO getOficinaVOseleccionada() {
		return oficinaVOseleccionada;
	}

	public void setOficinaVOseleccionada(OficinasVO oficinaVOseleccionada) {
		this.oficinaVOseleccionada = oficinaVOseleccionada;
	}

	public String getValorConfirmacionCadena() {
		return valorConfirmacionCadena;
	}
}