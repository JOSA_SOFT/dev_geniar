package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.clientews.BpmClient;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;

public class ClienteRestBpmBean implements Serializable{
	private static final long serialVersionUID = -8423187019813548752L;
	private static final Logger LOG = Logger.getLogger(ClienteRestBpmBean.class);
    private String nombre;
    private String resultado;

    
    static {
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
        	@Override
            public boolean verify(String hostname, SSLSession session) {
            	String ipLocation= "";
        		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();
        		PropiedadVO propiedad = new PropiedadVO();
	       		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesWeb.APLICACION_GESTION_SALUD));
	       		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesWeb.CLASIFICACION_GESTION_SOLICITUD));
	       		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesWeb.PROPIEDAD_GESTION_SOLICITUD_IP));
	       		try {
					ipLocation = propertyQuery.consultarPropiedad(propiedad);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
	       		return (hostname.equals(ipLocation)); 
            }
        });
    }  
    

    public void reasignarTarea(String idTask, String idSolicitud, String accion, String tipoAuditor) throws LogicException {
    	BpmClient client = new BpmClient(idTask, idSolicitud);
    	try{	    	
	    	client.crearBpmService();
	        client.enviarBPM(accion, tipoAuditor, idSolicitud);
        } catch (LogicException e) {
        	LOG.error(e.getMessage(), e);
		} catch(Exception e){
			LOG.error(e.getMessage(), e);
    		throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), e, ErrorType.INDEFINIDO);
    	} finally{
			client.close();
		} 
    }

    
    public void liberarTarea(String idTask, String idSolicitud, String accion, String tipoAuditor) {
        BpmClient client = new BpmClient(idTask, idSolicitud);
        try {
			client.crearBpmService();
			client.enviarBPM(accion, tipoAuditor, idSolicitud);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
		} finally{
			client.close();
		} 
    }
    
    public void anularInstanciaWeb(String idInstanciaProceso) {
        BpmClient client = new BpmClient(idInstanciaProceso);
        try {
			client.crearBpmServiceAnularInstancia();
			client.enviarBPMAnularInstancia();
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
		}finally{
			client.close();
		}    	
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
