package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ValueChangeEvent;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesExentosVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.NotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.controller.ObtenerInformacionOpsValorController;
import com.sos.gestionautorizacionessaludejb.controller.ReciboCajaController;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConversorNumeroLetras;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;

import co.eps.sos.dataccess.exception.DataAccessException;
import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

/**
 * Clase encargada de manejar el modal de generar recibos de caja
 * 
 * @author GENIAR <jorge.garcia@geniar.net>
 */
public class GenerarReciboBean implements Serializable {

	private static final long serialVersionUID = 2558229770678165801L;
	// Objetos para el log.
	private static final Logger LOG = Logger.getLogger(GenerarReciboBean.class);
	private static final String COLOR_ROJO = ConstantesWeb.COLOR_ROJO;

	private boolean mostrarModalReciboCaja;
	private int numeroOPS;
	private transient DatosSolicitudVO solicitudSeleccionada;
	private String tabSeleccionado;
	private boolean mostrarTabDetalleOPS = false;
	private boolean mostrarTabNotasCredito = false;
	private transient List<PrestacionDTO> listaPrestaciones;
	private List<ConceptoPagoOpsVO> conceptosPago;
	private InformacionOpsValorVO informacionOps;
	private boolean showPopupGenRecibo;
	private Date fechaActual;
	private List<InformacionOpsValorVO> listInformacion = new ArrayList<InformacionOpsValorVO>();

	private ReciboCajaVO recibo;
	private boolean mostrarTabGenerarNotasCredito;
	private Double totalApagar;
	private Double saldoAFavor;
	private List<DocumentoCajaVO> listaNotasCredito;
	private double valorRecibidoAux = 0;
	private ResultadoConsultaAfiliadoDatosAdicionales datosAdicionales;
	private boolean mostrarTabDatosBancoNotaCredito;
	private double diferenciaUltimoNotaCredito = 0;
	private int nuevoCodigoCaja = 0;
	private int nuiAfiliado;
	private boolean mostrarComprobanteRecibo = false;
	private boolean desabilitarDescargaReciboCaja = true;
	private boolean desabilitarNotaCredito = true;
	private boolean reciboCajaGenerado = false;
	private String beanSolicitud;
	private List<InformacionOpsValorVO> opsSeleccionadas;
	private String causales;
	private OPSCajaVO opsCajas;
	private boolean mostrarGenerarRecibo = true;
	private transient AfiliadoVO afiliado;

	private boolean desabilitarBotonGenerar = false;
	
	private boolean provieneInformeNotaCredito = false;
	
	private boolean entregaDineroNota = false;

	/**
	 * Inicialización de variables y listas
	 */
	public GenerarReciboBean() {
		opsSeleccionadas = new ArrayList<InformacionOpsValorVO>();
		recibo = new ReciboCajaVO();
		recibo.setSumatoriaValorOps(0);
		recibo.setSumatoriaNotasCredito(0);
		fechaActual = new Date();
		listaNotasCredito = new ArrayList<DocumentoCajaVO>();
		listInformacion = new ArrayList<InformacionOpsValorVO>();
	}

	private void mostrarMensajeErrorRecibo(Exception e) {
		LOG.error(e.getMessage(), e);
		MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
	}

	private void causalesNoPago() throws LogicException {
		GestionCajaController gestion;
		try {
			gestion = new GestionCajaController();
			List<CausalesExentosVO> causalesNoPago = gestion
					.obtenerCausalesNoPago(solicitudSeleccionada.getNumeroSolicitud());
			if (causalesNoPago != null && !causalesNoPago.isEmpty()) {
				procesarCausalesNoPago(causalesNoPago);
			}
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (DataAccessException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}

	private void procesarCausalesNoPago(List<CausalesExentosVO> causalesNoPago) {
			StringBuilder builder = new StringBuilder(200);
			builder.append(ConstantesWeb.CADENA_VACIA);
			for (CausalesExentosVO causal : causalesNoPago) {
				builder.append(causal.getDescripcion() + ",");
			}
			causales = builder.toString();		
	}

	/**
	 * Obtiene la información de las OPS que tiene pendientes el
	 * afiliado/beneficiario
	 * 
	 * 
	 */
	public void consultarInformacion() {
		try {
			ObtenerInformacionOpsValorController controller = new ObtenerInformacionOpsValorController();
			double sumatoriaValorOps = 0.0;
			opsCajas = controller.obtenerInformacionOPSValor(this.getSolicitudSeleccionada().getNumeroSolicitud());
			recibo.setValorRecibido(0.0);
			recibo.setValorDevolver(0.0);

			listInformacion = opsCajas.getOps();

			this.causalesNoPago();

			if (listInformacion != null && !listInformacion.isEmpty()) {
				showPopupGenRecibo = true;

				for (InformacionOpsValorVO model : listInformacion) {
					sumatoriaValorOps += model.getValueOPS();
				}
				recibo.setSumatoriaValorOps(sumatoriaValorOps);
			} else {
				showPopupGenRecibo = false;
			}
		} catch (DataAccessException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (LogicException e) {
			this.mostrarMensajeErrorRecibo(e);
		}
	}

	private void prepararNotaCredito() {
		tabSeleccionado = ConstantesWeb.TAB_GENERAR_NOTA_CREDITO;
		mostrarTabGenerarNotasCredito = true;
		desabilitarNotaCredito = false;

		GenerarNotaCreditoBean notaCreditoBean = (GenerarNotaCreditoBean) FacesUtils
				.getManagedBean(ConstantesWeb.GENERAR_NOTA_CREDITO_BEAN);
		if (notaCreditoBean == null) {
			notaCreditoBean = new GenerarNotaCreditoBean();
		}
		
		notaCreditoBean.setEntregaDineroNotaCredito(entregaDineroNota);
		notaCreditoBean.setDeshabilitarBotonGenerarNotaCredito(false);
		notaCreditoBean.setDeshabilitarBotonGenerarNotaCreditoBanco(false);
		notaCreditoBean.setConsecutivoBanco("-1");
		notaCreditoBean.setConsectivoMetodoDevolucion(-1);
		notaCreditoBean.setCorreoElectronico(ConstantesWeb.CADENA_VACIA);
		notaCreditoBean.setNumeroCuenta("");
		notaCreditoBean.setNumeroTelefono("");
		
		notaCreditoBean.setTipoCuenta("");
		notaCreditoBean.setTitular(null);

		notaCreditoBean.setOps(this.getOpsSeleccionadas());
		notaCreditoBean.setValorADevolverNotaCredito(saldoAFavor);
		notaCreditoBean
				.setNumeroIdentificacionAfiliado(solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion());
		notaCreditoBean.setConsecutivoTipoIdentificacionAfiliado(solicitudSeleccionada.getAfiliadoVO()
				.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion());

		notaCreditoBean.setAfiliado(solicitudSeleccionada.getAfiliadoVO());

		notaCreditoBean.setAccionARealizar(ConstantesWeb.ACCION_CREAR_NOTA_CREDITO);
		nuiAfiliado = datosAdicionales.getAfiliado().getNui();
	}

	private void organizarNotasCreditoAConsumir() {
		int posNotaModificar = -1;
		for (DocumentoCajaVO nota : listaNotasCredito) {
			if (nota.isSelected())
				posNotaModificar++;
		}
		if (posNotaModificar != -1 && diferenciaUltimoNotaCredito > 0) {
			double valor = listaNotasCredito.get(posNotaModificar).getValorAfiliad() - diferenciaUltimoNotaCredito;

			if (valor < 0) {
				valor = listaNotasCredito.get(posNotaModificar).getValorAfiliad();
			}
			listaNotasCredito.get(posNotaModificar).setValorAfiliad(valor);
		}
	}

	private List<ConceptoPagoOpsVO> calcularTotales(List<InformacionOpsValorVO> listaOps) {
		List<ConceptoPagoOpsVO> conceptos = new ArrayList<ConceptoPagoOpsVO>();
		Double sumCopago = 0.0;
		Double sumModeradora = 0.0;
		boolean cuotaModeradora = false;
		boolean copago = false;
		String nombreCuota = "";
		String nombreCopago = "";
		for (InformacionOpsValorVO ops : listaOps) {
			for (ConceptoPagoOpsVO concepto : ops.getListaConceptos()) {
				if (concepto.getConsecutivoCodigoConcepto() == ConstantesWeb.CONSECUTIVO_CUOTA_MODERADORA) {
					cuotaModeradora = true;
					nombreCuota = concepto.getNombreConceptoPago();
					sumModeradora += concepto.getValorConceptoPago();
				} else {
					copago = true;
					nombreCopago = concepto.getNombreConceptoPago();
					sumCopago += concepto.getValorConceptoPago();
				}
			}
		}

		if (cuotaModeradora) {
			ConceptoPagoOpsVO concepto = new ConceptoPagoOpsVO();
			concepto.setNombreConceptoPago(nombreCuota);
			concepto.setValorConceptoPago(sumModeradora);
			conceptos.add(concepto);
		}

		if (copago) {
			ConceptoPagoOpsVO concepto = new ConceptoPagoOpsVO();
			concepto.setNombreConceptoPago(nombreCopago);
			concepto.setValorConceptoPago(sumCopago);
			conceptos.add(concepto);
		}
		return conceptos;
	}

	/**
	 * Metodo para generar los recibos, envios al SP y se genera PDF
	 * 
	 * @return
	 */
	public String generarReciboCaja() {
		this.getOpsSeleccionadas();
		try {

			if (recibo == null) {
				throw new LogicException(ConstantesWeb.ERROR_RECIBIDO_ZERO, LogicException.ErrorType.DATO_NO_EXISTE);
			}

			if (recibo.getValorRecibido() <= 0 && (saldoAFavor != null && saldoAFavor == 0)
					&& recibo.getValorEfectivo() > 0) {
				throw new LogicException(ConstantesWeb.ERROR_RECIBIDO_ZERO, LogicException.ErrorType.DATO_NO_EXISTE);
			}

			double totalRecibidoAux = recibo.getSumatoriaNotasCredito() + recibo.getValorRecibido();
			if (recibo.getSumatoriaValorOps() > totalRecibidoAux) {
				throw new LogicException(ConstantesWeb.MENSAJE_ERROR_VALOR_A_PAGAR_MENOR_SUMATORIA,
						LogicException.ErrorType.DATO_NO_EXISTE);
			}

			this.validacionSeleccionOPSYNotaCredito();

			ReciboCajaController reciboCajaController = new ReciboCajaController();
			nuevoCodigoCaja = reciboCajaController.guardarReciboCaja(datosAdicionales.getAfiliado().getNui(),
					listaNotasCredito, recibo, FacesUtils.getUserName(), opsSeleccionadas);
			desabilitarBotonGenerar = true;
		} catch (Exception e) {
			this.mostrarMensajeErrorRecibo(e);
		}

		return null;
	}

	private void validacionSeleccionOPSYNotaCredito() throws LogicException {
		if (saldoAFavor > 0) {
			this.prepararNotaCredito();
		} else {
			mostrarComprobanteRecibo = true;
			tabSeleccionado = ConstantesWeb.TAB_COMPROBANTES;
			desabilitarNotaCredito = true;
		}
		desabilitarDescargaReciboCaja = false;
		this.organizarNotasCreditoAConsumir();
	}

	private List<InformacionOpsValorVO> getOpsSeleccionadas() {
		opsSeleccionadas = listInformacion;

		for (InformacionOpsValorVO info : listInformacion) {
			if (info.isSelected()) {
				opsSeleccionadas.add(info);
			}
		}
		return opsSeleccionadas;
	}

	public void setOpsSeleccionadas(List<InformacionOpsValorVO> opsSeleccionadas) {
		this.opsSeleccionadas = opsSeleccionadas;
	}

	/**
	 * Se encarga de realizar la consulta del detalle de las OPS.
	 * 
	 * @return
	 */
	public String consultaOPS() {
		try {
			ReciboCajaController reciboCajaController = new ReciboCajaController();
			GestionCajaController gestionCaja = new GestionCajaController();
			listaPrestaciones = gestionCaja.obtenerPrestacionesNumeroUnicoOPS(numeroOPS);
			conceptosPago = reciboCajaController.obtenerConceptoPagoOPS(numeroOPS);

			tabSeleccionado = ConstantesWeb.TAB_DETALLE_OPS;
			mostrarTabDetalleOPS = true;
		} catch (LogicException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (Exception e) {
			this.mostrarMensajeErrorRecibo(e);
		}
		return null;
	}

	/**
	 * Abre tab de notas créditos con la información a consultar
	 * 
	 * @return
	 */
	public String mostrarTabNotasCredito() {
		tabSeleccionado = ConstantesWeb.TAB_NOTAS_CREDITO;
		mostrarTabNotasCredito = true;
		return null;
	}

	/**
	 * Cuando cierra el tab debe volver al tab principal (Generar Recibo)
	 * 
	 * @return
	 */
	public String cerrarTab() {
		tabSeleccionado = ConstantesWeb.TAB_GENERAR_RECIBO;
		mostrarTabDetalleOPS = false;
		mostrarTabNotasCredito = false;
		mostrarTabGenerarNotasCredito = false;
		
		if (provieneInformeNotaCredito){
			mostrarModalReciboCaja = false;
		}
		return null;
	}

	private void consultaDatosAdicionalesAfiliado(List<PlanVO> listaPlan) throws LogicException {
		try {
			if (listaPlan != null && !listaPlan.isEmpty()) {
				ReciboCajaController reciboCajaController = new ReciboCajaController();
				String descPlan = buscarDescripcionPlan(
						solicitudSeleccionada.getAfiliadoVO().getPlan().getConsectivoPlan(), listaPlan);

				datosAdicionales = new ConsultaAfiliadoController().consultaRespuestaServicioAfiliado(
						solicitudSeleccionada.getAfiliadoVO().getTipoIdentificacionAfiliado()
								.getCodigoTipoIdentificacion(),
						solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion(), descPlan,
						solicitudSeleccionada.getFechaSolicitud(), FacesUtils.getUserName());

				NotasCreditoVO objetoNotasCredito = reciboCajaController.obtenerNotasCredito(datosAdicionales.getAfiliado().getNui());
				if (objetoNotasCredito != null){
					listaNotasCredito = objetoNotasCredito.getNotas();
					entregaDineroNota = ConstantesWeb.SI.equals(objetoNotasCredito.getEntregaDinero());
				}
				
				double sumatoriaNotasCredito = 0;
				for (DocumentoCajaVO nota : listaNotasCredito) {
					sumatoriaNotasCredito += nota.getValorAfiliad();
				}
				recibo.setSumatoriaNotasCredito(sumatoriaNotasCredito);
			}
		} catch (ConnectionProviderException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (IOException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.INDEFINIDO);
		} catch (DataAccessException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}

	/**
	 * Según el objeto de solicitud seleccionada, se obtienen las notas créditos
	 * asociadas al afiliado y demás información básica requerida
	 */
	private void procesarSolicitud() {

		if (solicitudSeleccionada != null)
			try {
				mostrarGenerarRecibo = true;
				this.consultarInformacion();
				List<PlanVO> listaPlan = new CargarCombosController().consultarListaPlanes();
				desabilitarNotaCredito = false;
				desabilitarBotonGenerar = false;
				mostrarTabGenerarNotasCredito = false;
				mostrarTabDatosBancoNotaCredito = false;
				mostrarComprobanteRecibo = false;

				this.consultaDatosAdicionalesAfiliado(listaPlan);
			} catch (LogicException e) {
				this.mostrarMensajeErrorRecibo(e);
			} catch (ConnectionProviderException e) {
				this.mostrarMensajeErrorRecibo(e);
			} catch (IOException e) {
				this.mostrarMensajeErrorRecibo(e);
			}
	}

	/**
	 * Guarda la información y se envía a generar el documento
	 */
	public void saveButton() {
		boolean isSelected = false;
		for (InformacionOpsValorVO vo : listInformacion) {
			if (vo.isSelected()) {
				isSelected = true;
				break;
			}
		}

		if (!isSelected)
			try {
				throw new LogicException(ConstantesWeb.ERROR_OPS_NO_SELECCIONADO,
						LogicException.ErrorType.DATO_NO_EXISTE);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			}
	}

	/**
	 * Realiza calculo sobre las notas créditos que se van a requerir para
	 * cubrir el valor total de las OPS y se actualiza el campo efectivo y saldo
	 * a favor
	 */
	public void seleccionOPS() {

		double sumatoriaValorOps = recibo.getSumatoriaValorOps();
		double valorEfectivoAux;
		double sumatoriaNotasCredito = recibo.getSumatoriaNotasCredito();

		valorEfectivoAux = sumatoriaValorOps - sumatoriaNotasCredito;

		if (valorEfectivoAux < 0) {
			recibo.setSaldoFavor(valorEfectivoAux * -1);
			saldoAFavor = valorEfectivoAux * -1;
			valorEfectivoAux = 0;
		} else {
			saldoAFavor = 0.0;
			recibo.setSaldoFavor(0);
		}

		recibo.setSumatoriaValorOps(sumatoriaValorOps);
		recibo.setValorEfectivo(valorEfectivoAux);
		recibo.setTotalPagar(sumatoriaValorOps);

		calcularNotasCredito();
	}

	/**
	 * Metodo encargado de calcular las notas creditos necesarias para cubrir el
	 * valor de la ops
	 */
	private void calcularNotasCredito() {
		double suma = 0;
		// Quito la selección de todas las listas
		for (int cont = 0; cont < listaNotasCredito.size(); cont++) {
			listaNotasCredito.get(cont).setSelected(false);
		}

		for (int cont = 0; cont < listaNotasCredito.size(); cont++) {
			DocumentoCajaVO nota = listaNotasCredito.get(cont);
			if (suma < recibo.getSumatoriaValorOps()) {
				suma += nota.getValorAfiliad();
				listaNotasCredito.get(cont).setSelected(true);
			} else {
				diferenciaUltimoNotaCredito = suma - recibo.getSumatoriaValorOps();
			}
		}
	}

	/**
	 * Obtenemos la descripción del plan que pertenece el afiliado/beneficiario
	 * 
	 * @param consecutivoPlan
	 *            id del consecutivo del plan
	 * @param listaPlan
	 *            List<SelectItem> lista con los planes
	 * @return String con la descripción del plan encontrado o vacio si no
	 *         encontró el plan a buscar en la lista enviada
	 */
	private String buscarDescripcionPlan(int consecutivoPlan, List<PlanVO> listaPlan) {
		String descPlan = "";
		for (PlanVO item : listaPlan) {
			int tempConsec = item.getConsectivoPlan();
			if (tempConsec == consecutivoPlan) {
				descPlan = item.getDescripcionPlan();
				break;
			}
		}
		return descPlan;
	}

	/**
	 * Calcula el valor a devolver
	 * 
	 * @param evt
	 */
	public void calcularValorDevolver(ValueChangeEvent evt) {
		recibo.setValorDevolver((Double.parseDouble(evt.getNewValue().toString())) - recibo.getValorEfectivo());

		if (recibo.getValorEfectivo() <= 0 || recibo.getValorDevolver() < 0)
			recibo.setValorDevolver(0);

	}

	public List<ReciboCajaJasperVO> generarComprobanteReciboPorSolicitud(int consecutivoSolicitud) throws LogicException{
		List<ReciboCajaJasperVO> lista = new ArrayList<ReciboCajaJasperVO>();
		try {
			GestionCajaController gestionCajaController = new GestionCajaController();
			ReciboCajaJasperVO reporteJasper = gestionCajaController.obtenerReciboCajaSolicitud(consecutivoSolicitud);
			reporteJasper.setNumeroEnLetra(ConversorNumeroLetras.convertirNumeroLetras(reporteJasper.getTotalPagar()));
			
			if (reporteJasper.getIdDocumento() > 0){
				lista.add(reporteJasper);
			}
		} catch (Exception e) {
			throw new LogicException(e, LogicException.ErrorType.INDEFINIDO);
		}
		return lista;
	}
	
	/**
	 * Metodo que se encarga de generar el PDF del recibo de caja
	 * 
	 * @return
	 */
	public String generarComprobanteReciboCaja() {
		ReciboCajaJasperVO caja = new ReciboCajaJasperVO();
		recibo.setTotalPagar(recibo.getSumatoriaValorOps());
		try {
			caja.setDocumentos(listaNotasCredito);
			caja.setIdDocumento(nuevoCodigoCaja);

			caja.setIdentificacionAfiliado(
					solicitudSeleccionada.getAfiliadoVO().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion()
							+ "-" + solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion());
			caja.setNombreAfiliado(solicitudSeleccionada.getAfiliadoVO().getNombreCompleto());
			caja.setNumeroEnLetra(ConversorNumeroLetras.convertirNumeroLetras(recibo.getTotalPagar()));
			GestionCajaController gestionCajaController = new GestionCajaController();
			String codigoOficina;

			OficinasVO oficinaVO = gestionCajaController.consultarOficinaPorMovimientoActual(FacesUtils.getUserName());
			codigoOficina = oficinaVO != null ? oficinaVO.getDescripcionOficina() : "";

			caja.setOficina(codigoOficina);
			List<ConceptoPagoOpsVO> totales = this.calcularTotales(this.opsSeleccionadas);
			caja.setConceptosPago(totales);

			caja.setUsuarioCreador(FacesUtils.getUserName());
			caja.setTotalPagar(recibo.getTotalPagar());
			caja.setSumatoriaNotasCredito(recibo.getSumatoriaNotasCredito());
			caja.setValorEfectivo(recibo.getValorRecibido());
			if (recibo.getTotalPagar() <= 0)
				recibo.setValorDevolver(0);

			caja.setValorDevolver(recibo.getValorDevolver());

			List<ReciboCajaJasperVO> lista = new ArrayList<ReciboCajaJasperVO>();
			lista.add(caja);
			GenerarReporteBean reporteBean = new GenerarReporteBean();

			reporteBean.generarReciboCaja(lista, ConstantesWeb.NOMBRE_ARCHIVO_RECIBO_CAJA + new Date().getTime(), new Date());

			reciboCajaGenerado = true;
		} catch (DataAccessException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (LogicException e) {
			this.mostrarMensajeErrorRecibo(e);
		} catch (LoginException e) {
			this.mostrarMensajeErrorRecibo(e);
		}
		return null;
	}

	/**
	 * Metodo que limpia los objetos al terminar el proceso.
	 * 
	 * @return
	 */
	public String salir() {
		recibo = new ReciboCajaVO();
		mostrarGenerarRecibo = true;
		mostrarTabNotasCredito = false;
		showPopupGenRecibo = false;
		reciboCajaGenerado = false;
		mostrarTabDetalleOPS = false;
		desabilitarNotaCredito = true;
		mostrarModalReciboCaja = false;
		mostrarTabNotasCredito = false;
		mostrarComprobanteRecibo = false;
		desabilitarDescargaReciboCaja = true;
		mostrarTabGenerarNotasCredito = false;
		mostrarTabDatosBancoNotaCredito = false;
		totalApagar = 0.0;
		saldoAFavor = 0.0;
		nuevoCodigoCaja = 0;
		recibo = new ReciboCajaVO();
		diferenciaUltimoNotaCredito = 0;
		listaPrestaciones = new ArrayList<PrestacionDTO>();
		conceptosPago = new ArrayList<ConceptoPagoOpsVO>();
		listaNotasCredito = new ArrayList<DocumentoCajaVO>();
		listInformacion = new ArrayList<InformacionOpsValorVO>();
		datosAdicionales = new ResultadoConsultaAfiliadoDatosAdicionales();
		return null;
	}

	/**
	 * Se encarga de redireccionar al metodo de descarga de la OPSs
	 * 
	 * @return
	 */
	public String generarComprobantes() {
		if (ConstantesWeb.CONSULTA_SOLICITUD.equalsIgnoreCase(this.beanSolicitud)) {
			ConsultarSolicitudBean consultaSolicitudBean = (ConsultarSolicitudBean) FacesUtils
					.getManagedBean(ConstantesWeb.CONSULTAR_SOLICITUD_BEAN);
			consultaSolicitudBean.descargar();
		}
		if (ConstantesWeb.INGRESO_SOLICITUD.equalsIgnoreCase(this.beanSolicitud)) {
			IngresoSolicitudBean ingresoSolicitudBean = (IngresoSolicitudBean) FacesUtils
					.getManagedBean(ConstantesWeb.INGRESO_SOLICITUD_BEAN);
			ingresoSolicitudBean.setMostrarPopupGenerarOPS(true);
		}
		if (ConstantesWeb.PROGRAMACION_ENTREGA.equalsIgnoreCase(this.beanSolicitud)) {
			ConsultarProgramacionEntregaBean programacionEntregaBean = (ConsultarProgramacionEntregaBean) FacesUtils
					.getManagedBean(ConstantesWeb.CONSULTA_PROGRAACION_ENTREGA_BEAN);
			try {
				programacionEntregaBean.validarOPSDescarga();
			} catch (LogicException e) {
				this.mostrarMensajeErrorRecibo(e);
			}
		}
		return null;
	}

	/******* SETTER&GETTER *********/
	public boolean isMostrarModalReciboCaja() {
		return mostrarModalReciboCaja;
	}

	public void setMostrarModalReciboCaja(boolean mostrarModalReciboCaja) {
		this.mostrarModalReciboCaja = mostrarModalReciboCaja;
	}

	public int getNumeroOPS() {
		return numeroOPS;
	}

	public void setNumeroOPS(int numeroOPS) {
		this.numeroOPS = numeroOPS;
	}

	public String getTabSeleccionado() {
		return tabSeleccionado;
	}

	public void setTabSeleccionado(String tabSeleccionado) {
		this.tabSeleccionado = tabSeleccionado;
	}

	public boolean isMostrarTabDetalleOPS() {
		return mostrarTabDetalleOPS;
	}

	public void setMostrarTabDetalleOPS(boolean mostrarTabDetalleOPS) {
		this.mostrarTabDetalleOPS = mostrarTabDetalleOPS;
	}

	public boolean isMostrarTabNotasCredito() {
		return mostrarTabNotasCredito;
	}

	public void setMostrarTabNotasCredito(boolean mostrarTabNotasCredito) {
		this.mostrarTabNotasCredito = mostrarTabNotasCredito;
	}

	public List<PrestacionDTO> getListaPrestaciones() {
		return listaPrestaciones;
	}

	public void setListaPrestaciones(List<PrestacionDTO> listaPrestaciones) {
		this.listaPrestaciones = listaPrestaciones;
	}

	public List<ConceptoPagoOpsVO> getConceptosPago() {
		return conceptosPago;
	}

	public void setConceptosPago(List<ConceptoPagoOpsVO> conceptosPago) {
		this.conceptosPago = conceptosPago;
	}

	public void setInformacionOps(InformacionOpsValorVO informacionOps) {
		this.informacionOps = informacionOps;
	}

	public InformacionOpsValorVO getInformacionOps() {
		return informacionOps;
	}

	public void setSolicitudSeleccionada(DatosSolicitudVO solicitudSeleccionada) {
		provieneInformeNotaCredito = false;
		this.solicitudSeleccionada = solicitudSeleccionada;
		this.afiliado = solicitudSeleccionada.getAfiliadoVO();
		procesarSolicitud();
		this.seleccionOPS();
	}

	public DatosSolicitudVO getSolicitudSeleccionada() {
		return solicitudSeleccionada;
	}

	public void setShowPopupGenRecibo(boolean showPopupGenRecibo) {
		this.showPopupGenRecibo = showPopupGenRecibo;
	}

	public boolean isShowPopupGenRecibo() {
		return showPopupGenRecibo;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setListInformacion(List<InformacionOpsValorVO> listInformacion) {
		this.listInformacion = listInformacion;
	}

	public List<InformacionOpsValorVO> getListInformacion() {
		return listInformacion;
	}

	public void setRecibo(ReciboCajaVO recibo) {
		this.recibo = recibo;
	}

	public ReciboCajaVO getRecibo() {
		return recibo;
	}

	public boolean isMostrarTabGenerarNotasCredito() {
		return mostrarTabGenerarNotasCredito;
	}

	public void setMostrarTabGenerarNotasCredito(boolean mostrarTabGenerarNotasCredito) {
		this.mostrarTabGenerarNotasCredito = mostrarTabGenerarNotasCredito;
	}

	public Double getSaldoAfavor() {
		return totalApagar;
	}

	public void setSaldoAfavor(Double saldoAfavor) {
		this.totalApagar = saldoAfavor;
	}

	public boolean isMostrarTabDatosBancoNotaCredito() {
		return mostrarTabDatosBancoNotaCredito;
	}

	public void setMostrarTabDatosBancoNotaCredito(boolean mostrarTabDatosBancoNotaCredito) {
		this.mostrarTabDatosBancoNotaCredito = mostrarTabDatosBancoNotaCredito;
	}

	public void setListaNotasCredito(List<DocumentoCajaVO> listaNotasCredito) {
		this.listaNotasCredito = listaNotasCredito;
	}

	public List<DocumentoCajaVO> getListaNotasCredito() {
		return listaNotasCredito;
	}

	public void setValorRecibidoAux(double valorRecibidoAux) {
		this.valorRecibidoAux = valorRecibidoAux;
	}

	public double getValorRecibidoAux() {
		return valorRecibidoAux;
	}

	public int getNuiAfiliado() {
		return nuiAfiliado;
	}

	public boolean isMostrarComprobanteRecibo() {
		return mostrarComprobanteRecibo;
	}

	public void setMostrarComprobanteRecibo(boolean mostrarComprobanteRecibo) {
		this.mostrarComprobanteRecibo = mostrarComprobanteRecibo;
	}

	public boolean isDesabilitarDescargaReciboCaja() {
		return desabilitarDescargaReciboCaja;
	}

	public void setDesabilitarDescargaReciboCaja(boolean desabilitarDescargaReciboCaja) {
		this.desabilitarDescargaReciboCaja = desabilitarDescargaReciboCaja;
	}

	public boolean isDesabilitarNotaCredito() {
		return desabilitarNotaCredito;
	}

	public void setDesabilitarNotaCredito(boolean desabilitarNotaCredito) {
		this.desabilitarNotaCredito = desabilitarNotaCredito;
	}

	public boolean isReciboCajaGenerado() {
		return reciboCajaGenerado;
	}

	public void setBeanSolicitud(String beanSolicitud) {
		this.beanSolicitud = beanSolicitud;
	}

	public boolean isDesabilitarBotonGenerar() {
		return desabilitarBotonGenerar;
	}

	public void setDesabilitarBotonGenerar(boolean desabilitarBotonGenerar) {
		this.desabilitarBotonGenerar = desabilitarBotonGenerar;
	}

	public String getCausales() {
		return causales;
	}

	public OPSCajaVO getOpsCajas() {
		return opsCajas;
	}

	public boolean isMostrarGenerarRecibo() {
		return mostrarGenerarRecibo;
	}

	public void setMostrarGenerarRecibo(boolean mostrarGenerarRecibo) {
		this.mostrarGenerarRecibo = mostrarGenerarRecibo;
	}

	public void setNuiAfiliado(int nuiAfiliado) {
		this.nuiAfiliado = nuiAfiliado;
	}

	public AfiliadoVO getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(AfiliadoVO afiliado) {
		this.afiliado = afiliado;
	}

	public boolean isProvieneInformeNotaCredito() {
		return provieneInformeNotaCredito;
	}

	public void setProvieneInformeNotaCredito(boolean provieneInformeNotaCredito) {
		this.provieneInformeNotaCredito = provieneInformeNotaCredito;
	}

	public void setEntregaDineroNota(boolean entregaDineroNota) {
		this.entregaDineroNota = entregaDineroNota;
	}
}