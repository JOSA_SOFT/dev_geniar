package com.sos.gestionautorizacionessaludweb.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.sos.authentication.serviceclient.proxy.AuthenticationServiceClient;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.propertyquery.serviceproxy.PropertyQueryServiceProxy;

public class ServicioAutenticacionBean {
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ServicioAutenticacionBean.class);	
	
	public boolean validarUsuarioServicioAutenticacion(String usuario, String contrasena){
		boolean usrValido = false;
		try {
			PropertyQueryServiceProxy propertyQueryServiceProxy = new PropertyQueryServiceProxy();
			com.sos.authentication.serviceclient.SOSAuthenticationService sosAuthenticationService = AuthenticationServiceClient.getInstance(propertyQueryServiceProxy);
			usrValido = sosAuthenticationService.login(usuario, contrasena);
	
		}catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesWeb.ERROR_SERVICIO_AUTENTICACION), ConstantesWeb.CADENA_VACIA));
			LOG.error(ex.getMessage(), ex);		
		}
		return usrValido;
	}
}
