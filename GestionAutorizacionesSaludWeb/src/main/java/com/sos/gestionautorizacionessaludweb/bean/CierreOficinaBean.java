package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.CierreOficinaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.CoordinadorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeCierreOficinaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.TotalCierreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;

/**
 * Bean de cierre de oficina
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class CierreOficinaBean implements Serializable {
	private static final long serialVersionUID = 9046112663172299185L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger
			.getLogger(ConsultaAfiliadoBean.class);
	private static final String ERROR_EXCEPCION_INESPERADA = ConstantesWeb.ERROR_EXCEPCION_INESPERADA;
	private List<String> listErrores;
	private Date fechaActual;
	private Double valorRecibido;
	private Double totalCaja;
	private Double totalFaltanteOficina;
	private Double totalSobranteOficina;
	private Double totalBaseOficina;
	private Double totalBaseColumna;
	private Double totalRecibidoOficina;
	private Double totalCierreOficina;
	private Double totalDescuadre;
	private List<MovimientoCajaVO> listMovimientoCajaCierreAdmin;
	private List<MovimientoCajaVO> listMovimientoCajaVO;
	private InformeCierreOficinaVO informeCierreOficinaVO;
	private MovimientoCajaVO movimientoCajaSeleccionado;
	private CoordinadorVO coordinadorVO;
	private UsuarioWebVO usuarioWebVO;
	private boolean mostrarModalError;
	private boolean mostrarModalValorRecibido;
	private boolean recargaPaginaOficina = false;

	/**
	 * Post Construct
	 */
	@PostConstruct
	public void init() {
		movimientoCajaSeleccionado = new MovimientoCajaVO();
		listMovimientoCajaCierreAdmin = new ArrayList<MovimientoCajaVO>();
		String userLogin = FacesUtils.getUserName();
		fechaActual = new Date();
		try {
			GestionCajaController cajaController = new GestionCajaController();
			usuarioWebVO = cajaController.consultarUsuarioWeb(userLogin,
					ConstantesWeb.MODULO_CAJA);

			coordinadorVO = cajaController.consultarCoordinador(userLogin);

			if (!Utilidades.validarNullCadena(coordinadorVO).equals(
					ConstantesWeb.CADENA_VACIA)) {
				coordinadorVO.setPrmrNmbreUsro(usuarioWebVO.getPrmrNmbreUsro());
				coordinadorVO.setSgndoNmbreUsro(usuarioWebVO
						.getSgndoNmbreUsro());
				coordinadorVO.setPrmrAplldoUsro(usuarioWebVO
						.getPrmrAplldoUsro());
				coordinadorVO.setSgndoAplldoUsro(usuarioWebVO
						.getSgndoAplldoUsro());

				listMovimientoCajaVO = cajaController
						.consultarMovimientosCajaPorOficina(coordinadorVO
								.getCnsctvoCdgoOfcna());
				calcularTotalCajaAbierta();
			}
			calcularTotales();
			recargaPaginaOficina = true;
		} catch (DataAccessException e) {
			this.mostrarMensajeErrorCierreOficina(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeErrorCierreOficina(e);
		}
	}
	
	public void reloadPageOficina(ActionEvent event) {
		if (recargaPaginaOficina) {
			this.init();
		}
	}

	private void mostrarMensajeErrorCierreOficina(Exception e) {
		LOG.error(e.getMessage(), e);
		List<String> errorTemp = new ArrayList<String>();
		errorTemp.add(ERROR_EXCEPCION_INESPERADA);
		mostrarModalError(errorTemp);
	}

	private void mostrarExceptcionPantallaCierreOficina(Exception e) {
		LOG.error(e.getMessage(), e);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR,
						ConstantesWeb.ERROR_EXCEPCION_INESPERADA,
						ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
	}

	/**
	 * Evento del boton imprimir
	 */
	public void imprimir() {
		generarInformeCierreOficina();
	}

	/**
	 * Cierra la oficina
	 */
	public void cierreOficina() {
		totalDescuadre = (this.totalRecibidoOficina == null ? 0.0
				: totalRecibidoOficina)
				- (this.totalCierreOficina == null ? 0.0
						: this.totalCierreOficina);
		if (!Utilidades.validarNullCadena(coordinadorVO).equals(
				ConstantesWeb.CADENA_VACIA)) {
			if (!Utilidades.validarNullCadena(totalCierreOficina).equals(
					ConstantesWeb.CADENA_VACIA)
					&& ((totalCierreOficina.intValue() + ConstantesWeb.CADENA_VACIA)
							.length() > 8 || totalCierreOficina.intValue() < 1)) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL,
								ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL));
				totalCierreOficina = null;
			} else {
				if (!Utilidades.validarNullCadena(totalCierreOficina).equals(
						ConstantesWeb.CADENA_VACIA)
						&& totalCierreOficina.equals(totalRecibidoOficina)) {
					cambiarUsuarioUltimaModificacion();
					llamarSPCierreOficina();
				} else {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									ConstantesWeb.ERROR_TOTAL_RECIBIDO,
									ConstantesWeb.ERROR_TOTAL_RECIBIDO));
				}
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							ConstantesWeb.ERROR_COORDINADOR,
							ConstantesWeb.ERROR_COORDINADOR));
		}
	}

	/**
	 * Evento cuando se oprime el boton re recibido
	 */
	public void recibidoOficina() {
		if (!Utilidades.validarNullCadena(valorRecibido).equals(
				ConstantesWeb.CADENA_VACIA)
				&& ((valorRecibido.intValue() + ConstantesWeb.CADENA_VACIA)
						.length() > 8 || valorRecibido.intValue() < 1)) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL,
							ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL));
		} else {
			if (movimientoCajaSeleccionado.getCnsctvoCdgoEstdo().intValue() == 1) {
				cerrarCaja(movimientoCajaSeleccionado);
			} else {
				if (!movimientoCajaSeleccionado.getTtlCja().equals(
						valorRecibido)) {

					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									ConstantesWeb.ERROR_VALOR_RECIBIDO,
									ConstantesWeb.ERROR_VALOR_RECIBIDO));
				} else {
					mostrarModalValorRecibido = false;
					movimientoCajaSeleccionado.setTotalRecibido(valorRecibido);
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									ConstantesWeb.INFO_ASIGNAR_VALOR,
									ConstantesWeb.INFO_ASIGNAR_VALOR));
				}
			}
		}
		valorRecibido = null;
		calcularTotales();
	}

	/**
	 * Genera el informe del cierre oficina
	 */
	private void generarInformeCierreOficina() {
		if (!Utilidades.validarNullCadena(informeCierreOficinaVO).equals(
				ConstantesWeb.CADENA_VACIA)) {
			GenerarReporteBean reporte = new GenerarReporteBean();
			try {
				reporte.generarInformeCierreOficina(informeCierreOficinaVO);
			} catch (LogicException e) {
				LOG.error(e.getMessage(), e);
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								ConstantesWeb.ERROR_JASPER,
								ConstantesWeb.ERROR_JASPER));
			}
		}
	}

	/**
	 * Cambiar el usuario de ultima modificacion de los registros
	 */
	private void cambiarUsuarioUltimaModificacion() {
		for (MovimientoCajaVO movimientoCajaVO : listMovimientoCajaVO) {
			movimientoCajaVO.setUsroUltmaMdfccn(FacesUtils.getUserName());
		}
	}

	/**
	 * Llama SP para cierre de oficina
	 */
	private void llamarSPCierreOficina() {
		CierreOficinaVO cierreOficinaVO = new CierreOficinaVO();
		cierreOficinaVO.setTtlEfctvoCja(totalCaja);
		cierreOficinaVO.setTtlRcbdoOfcna(totalRecibidoOficina);
		cierreOficinaVO.setBseOfcna(totalBaseOficina);

		Double descuadre = (this.totalRecibidoOficina == null ? 0.0
				: totalRecibidoOficina)
				- (this.totalCierreOficina == null ? 0.0
						: this.totalCierreOficina);

		if (descuadre.doubleValue() < 0.0) {
			cierreOficinaVO.setFltnteOfcna(descuadre * -1);
			cierreOficinaVO.setSbrnteOfcna(0.0);
		} else if (descuadre.doubleValue() > 0.0) {
			cierreOficinaVO.setSbrnteOfcna(descuadre);
			cierreOficinaVO.setFltnteOfcna(0.0);
		} else {
			cierreOficinaVO.setFltnteOfcna(0.0);
			cierreOficinaVO.setSbrnteOfcna(0.0);
		}

		cierreOficinaVO.setUsroCrcn(FacesUtils.getUserName());
		cierreOficinaVO.setCnsctvoCdgoCrdndrAsi(coordinadorVO
				.getCnsctvoCdgoCrdndrAsi());
		try {
			GestionCajaController cajaController = new GestionCajaController();
			Integer consecutivoCierreOficina = cajaController.cierreOficina(
					cierreOficinaVO, obtenerMovimientosCerrados(),
					listMovimientoCajaCierreAdmin);
			if (!Utilidades.validarNullCadena(consecutivoCierreOficina).equals(
					ConstantesWeb.CADENA_VACIA)) {
				cierreOficinaVO.setCnsctvoCrreOfcna(consecutivoCierreOficina);
				InformeCierreOficinaVO informeCierreOficinaTemp = new InformeCierreOficinaVO();
				informeCierreOficinaTemp.setCierreOficinaVO(cierreOficinaVO);
				informeCierreOficinaTemp.setCoordinadorVO(coordinadorVO);
				informeCierreOficinaTemp.setTotalBaseTabla(totalBaseColumna);
				informeCierreOficinaTemp
						.setTotalCierreOficina(totalCierreOficina);
				informeCierreOficinaTemp.setTotalEfectivoCajaTabla(totalCaja);
				informeCierreOficinaTemp
						.setTotalFaltanteTabla(totalFaltanteOficina);
				informeCierreOficinaTemp
						.setTotalSobranteTabla(totalSobranteOficina);
				this.informeCierreOficinaVO = informeCierreOficinaTemp;
			}

			limpiarCampos();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							ConstantesWeb.INFO_CIERRE_EXITOSO,
							ConstantesWeb.INFO_CIERRE_EXITOSO));
		} catch (LogicException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		} catch (ConnectionProviderException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		}
	}

	/**
	 * Limpia los campos luego de cerrar oficina
	 */
	private void limpiarCampos() {
		listMovimientoCajaCierreAdmin = new ArrayList<MovimientoCajaVO>();
		listMovimientoCajaVO = new ArrayList<MovimientoCajaVO>();
		totalRecibidoOficina = 0.0;
		totalCierreOficina = null;
		totalBaseOficina = 0.0;
		totalFaltanteOficina = 0.0;
		totalSobranteOficina = 0.0;
		totalCaja = 0.0;
	}

	/**
	 * Calcula el campo ttlCja de una caja abierta
	 * 
	 * @param movimientoEstadoAbierto
	 */
	private void calcularTotalCajaAbierta() {
		TotalCierreVO totalesCierreVO;
		try {
			GestionCajaController cajaController = new GestionCajaController();
			for (MovimientoCajaVO movimientoCajaVO : listMovimientoCajaVO) {
				if (movimientoCajaVO.getCnsctvoCdgoEstdo().intValue() == 1) {
					totalesCierreVO = cajaController
							.obtenerTotalesCierre(movimientoCajaVO
									.getCnsctvoMvmntoCja());
					Double totalCajaCierre = totalesCierreVO.getSumReciboCaja()
							+ movimientoCajaVO.getBse()
							- totalesCierreVO.getSumNotaCredito();
					movimientoCajaVO.setTtlCja(totalCajaCierre);
				}
			}
		} catch (DataAccessException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		} catch (ConnectionProviderException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		}
	}

	/**
	 * Cierra una caja en estado abierto
	 */
	private void cerrarCaja(MovimientoCajaVO movimientoCajaSeleccionado) {
		try {
			GestionCajaController cajaController = new GestionCajaController();
			TotalCierreVO totalesCierreVO = cajaController
					.obtenerTotalesCierre(movimientoCajaSeleccionado
							.getCnsctvoMvmntoCja());
			Double totalCajaCierre = totalesCierreVO.getSumReciboCaja()
					+ movimientoCajaSeleccionado.getBse()
					- totalesCierreVO.getSumNotaCredito();

			if (!totalCajaCierre.equals(valorRecibido)) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								ConstantesWeb.ERROR_VALOR_RECIBIDO,
								ConstantesWeb.ERROR_VALOR_RECIBIDO));
			} else {
				Double descuadre = valorRecibido - totalCajaCierre;
				if (descuadre.doubleValue() < 0.0) {
					movimientoCajaSeleccionado.setFltnteCrre(descuadre
							.doubleValue() * -1);
				} else if (descuadre.doubleValue() > 0.0) {
					movimientoCajaSeleccionado.setSbrnteCrre(descuadre);
				}
				movimientoCajaSeleccionado.setCnsctvoCdgoEstdo(4);
				movimientoCajaSeleccionado
						.setDscrpcnEstdo(ConstantesWeb.DESCRIPCION_ESTADO_CERRADO_ADMIN);
				movimientoCajaSeleccionado.setTtlEfctvo(valorRecibido);
				movimientoCajaSeleccionado.setTotalRecibido(valorRecibido);
				movimientoCajaSeleccionado.setFchaCrre(new Date());
				mostrarModalValorRecibido = false;
				listMovimientoCajaCierreAdmin.add(movimientoCajaSeleccionado);
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								ConstantesWeb.INFO_ASIGNAR_VALOR,
								ConstantesWeb.INFO_ASIGNAR_VALOR));
			}
		} catch (DataAccessException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		} catch (ConnectionProviderException e) {
			this.mostrarExceptcionPantallaCierreOficina(e);
		}
	}

	/**
	 * Obtiene movimientos caja que ya estaban cerrados
	 * 
	 * @return
	 */
	private List<MovimientoCajaVO> obtenerMovimientosCerrados() {
		List<MovimientoCajaVO> listTemp = new ArrayList<MovimientoCajaVO>();
		for (MovimientoCajaVO movimientoCajaVO : listMovimientoCajaVO) {
			if (movimientoCajaVO.getCnsctvoCdgoEstdo() == 2) {
				listTemp.add(movimientoCajaVO);
			}
		}
		return listTemp;
	}

	/**
	 * Calcula los totales de la tabla
	 */
	private void calcularTotales() {
		totalBaseColumna = 0.0;
		totalCaja = 0.0;
		totalRecibidoOficina = 0.0;
		totalBaseOficina = 0.0;
		totalFaltanteOficina = 0.0;
		totalSobranteOficina = 0.0;
		if (!Utilidades.validarNullCadena(listMovimientoCajaVO).equals(
				ConstantesWeb.CADENA_VACIA)) {
			for (MovimientoCajaVO movimientoCajaVO : listMovimientoCajaVO) {
				totalCaja += movimientoCajaVO.getTtlCja();
				totalRecibidoOficina += movimientoCajaVO.getTotalRecibido() == null ? 0.0
						: movimientoCajaVO.getTotalRecibido();
				if (!Utilidades.validarNullCadena(
						movimientoCajaVO.getTotalRecibido()).equals(
						ConstantesWeb.CADENA_VACIA)) {
					totalBaseOficina += movimientoCajaVO.getBse();
				}
				if (movimientoCajaVO.getCnsctvoCdgoEstdo().intValue() == 2) {
					totalBaseColumna += movimientoCajaVO.getBse();
				}
				totalFaltanteOficina += movimientoCajaVO.getFltnteCrre();
				totalSobranteOficina += movimientoCajaVO.getSbrnteCrre();
			}
		}
	}

	/**
	 * Mostrar modal error
	 * 
	 * @param errores
	 */
	private void mostrarModalError(List<String> errores) {
		if (!errores.isEmpty()) {
			listErrores = errores;
			mostrarModalError = true;
		}
	}

	public List<String> getListErrores() {
		return listErrores;
	}

	public void setListErrores(List<String> listErrores) {
		this.listErrores = listErrores;
	}

	public boolean isMostrarModalError() {
		return mostrarModalError;
	}

	public void setMostrarModalError(boolean mostrarModalError) {
		this.mostrarModalError = mostrarModalError;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public CoordinadorVO getCoordinadorVO() {
		return coordinadorVO;
	}

	public void setCoordinadorVO(CoordinadorVO coordinadorVO) {
		this.coordinadorVO = coordinadorVO;
	}

	public UsuarioWebVO getUsuarioWebVO() {
		return usuarioWebVO;
	}

	public void setUsuarioWebVO(UsuarioWebVO usuarioWebVO) {
		this.usuarioWebVO = usuarioWebVO;
	}

	public List<MovimientoCajaVO> getListMovimientoCajaVO() {
		return listMovimientoCajaVO;
	}

	public void setListMovimientoCajaVO(
			List<MovimientoCajaVO> listMovimientoCajaVO) {
		this.listMovimientoCajaVO = listMovimientoCajaVO;
	}

	public MovimientoCajaVO getMovimientoCajaSeleccionado() {
		return movimientoCajaSeleccionado;
	}

	public void setMovimientoCajaSeleccionado(
			MovimientoCajaVO movimientoCajaSeleccionado) {
		this.movimientoCajaSeleccionado = movimientoCajaSeleccionado;
	}

	public Double getValorRecibido() {
		return valorRecibido;
	}

	public void setValorRecibido(Double valorRecibido) {
		this.valorRecibido = valorRecibido;
	}

	public boolean isMostrarModalValorRecibido() {
		return mostrarModalValorRecibido;
	}

	public void setMostrarModalValorRecibido(boolean mostrarModalValorRecibido) {
		this.mostrarModalValorRecibido = mostrarModalValorRecibido;
	}

	public Double getTotalFaltanteOficina() {
		return totalFaltanteOficina;
	}

	public void setTotalFaltanteOficina(Double totalFaltanteOficina) {
		this.totalFaltanteOficina = totalFaltanteOficina;
	}

	public Double getTotalSobranteOficina() {
		return totalSobranteOficina;
	}

	public void setTotalSobranteOficina(Double totalSobranteOficina) {
		this.totalSobranteOficina = totalSobranteOficina;
	}

	public Double getTotalBaseOficina() {
		return totalBaseOficina;
	}

	public void setTotalBaseOficina(Double totalBaseOficina) {
		this.totalBaseOficina = totalBaseOficina;
	}

	public Double getTotalRecibidoOficina() {
		return totalRecibidoOficina;
	}

	public void setTotalRecibidoOficina(Double totalRecibidoOficina) {
		this.totalRecibidoOficina = totalRecibidoOficina;
	}

	public Double getTotalCaja() {
		return totalCaja;
	}

	public void setTotalCaja(Double totalCaja) {
		this.totalCaja = totalCaja;
	}

	public Double getTotalCierreOficina() {
		return totalCierreOficina;
	}

	public void setTotalCierreOficina(Double totalCierreOficina) {
		this.totalCierreOficina = totalCierreOficina;
	}

	public List<MovimientoCajaVO> getListMovimientoCajaCierreAdmin() {
		return listMovimientoCajaCierreAdmin;
	}

	public void setListMovimientoCajaCierreAdmin(
			List<MovimientoCajaVO> listMovimientoCajaCierreAdmin) {
		this.listMovimientoCajaCierreAdmin = listMovimientoCajaCierreAdmin;
	}

	public Double getTotalDescuadre() {
		return totalDescuadre;
	}

	public void setTotalDescuadre(Double totalDescuadre) {
		this.totalDescuadre = totalDescuadre;
	}

	public Double getTotalBaseColumna() {
		return totalBaseColumna;
	}

	public void setTotalBaseColumna(Double totalBaseColumna) {
		this.totalBaseColumna = totalBaseColumna;
	}
}