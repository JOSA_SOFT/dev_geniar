package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.ips.modelo.IPSVO;

/**
 * Bean para el filtro por prestador
 * 
 * @author geniarjos
 * @since 24/03/2017
 */
public class PrestadorFiltroBean implements Serializable{
	private static final long serialVersionUID = -6237372282746458414L;
	private static final Logger LOG = Logger.getLogger(PrestadorFiltroBean.class);
	private static final String COLOR_ROJO = "red";
	private List<SelectItem> listaTiposIdentificacion = new ArrayList<SelectItem>();
	
	private boolean mostrarPopupPrestadores;
	private IPSVO ipsVO = new IPSVO();
	private List<IPSVO> listaIPSVO = new ArrayList<IPSVO>();
	
	private transient EJBResolverInstance ejbs;

	@PostConstruct
	public void init() {
		CargarCombosController cargarCombosController;
		try {
			cargarCombosController = new CargarCombosController();
			listaTiposIdentificacion = CargarCombosUtil.obtenerListaSelectTiposIdentificacion(null,
					cargarCombosController);

		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);

		}
	}
	
	private EJBResolverInstance getEjbs(){
		if (ejbs == null){
			ejbs = (EJBResolverInstance)FacesUtils.getManagedBean(ConstantesWeb.EJB_RESOLVER_BEAN);
		} 
		return ejbs;
	}

	/**
	 * Busca las ips según el tipo y numero de identificaion o codigo del
	 * prestador
	 * 
	 * @author geniarjos Geniar S.A.S
	 */
	public void buscarIPS() {
		try {
			mostrarPopupPrestadores = false;

			listaIPSVO = getEjbs().getIpsEJB().consultarIpsPorParametros(ipsVO);
			if (listaIPSVO == null || listaIPSVO.isEmpty()) {
				listaIPSVO = getEjbs().getIpsEJB().consultarConsultoriosPorParametros(ipsVO);
			}
			if (IngresoSolicitudesMedicoPrestadorUtil.validarPopUpPrestadoresMayor(listaIPSVO)) {
				mostrarPopupPrestadores = true;
			}

			if (IngresoSolicitudesMedicoPrestadorUtil.validarPopUpPrestadoresIgual(listaIPSVO)) {
				mostrarPopupPrestadores = false;
				ipsVO = listaIPSVO.get(0);
			}
			if (listaIPSVO == null || listaIPSVO.isEmpty()) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
						COLOR_ROJO);
			}

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);

		}
	}

	public void seleccionarIPS() {
		mostrarPopupPrestadores = false;
		listaIPSVO = null;
	}

	public void cerrarPopupIPS() {
		mostrarPopupPrestadores = false;
		listaIPSVO = null;
	}

	public List<SelectItem> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public void setListaTiposIdentificacion(List<SelectItem> listaTiposIdentificacion) {
		this.listaTiposIdentificacion = listaTiposIdentificacion;
	}

	public IPSVO getIpsVO() {
		return ipsVO;
	}

	public void setIpsVO(IPSVO ipsVO) {
		this.ipsVO = ipsVO;
	}

	public void setMostrarPopupPrestadores(boolean mostrarPopupPrestadores) {
		this.mostrarPopupPrestadores = mostrarPopupPrestadores;
	}

	public boolean isMostrarPopupPrestadores() {
		return mostrarPopupPrestadores;
	}

	public void setListaIPSVO(List<IPSVO> listaIPSVO) {
		this.listaIPSVO = listaIPSVO;
	}

	public List<IPSVO> getListaIPSVO() {
		return listaIPSVO;
	}
}
