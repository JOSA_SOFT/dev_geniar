package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.dto.modificarfechaentregaops.CriteriosConsultaOps;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.ColeccionOpsVO;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.TotalResumenOpsVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaBodegaPoliticaController;
import com.sos.gestionautorizacionessaludejb.controller.ModificaFechasEntregaOPSController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;

public class ModificaFechasEntregaOPSBean implements Serializable {
	private static final long serialVersionUID = -5569222007305205599L;

	private static final Logger LOG = Logger.getLogger(ModificaFechasEntregaOPSBean.class);
	private static final String COLOR_ROJO = ConstantesWeb.COLOR_ROJO;

	private Integer tipoPrestacionSeleccionado;
	private Integer consecutivoPrestacion;

	private String planesSeleccionados;
	private String afiliadosSeleccionados;
	private String estadosSeleccionados;
	private String sedesSeleccionados;
	private String gruposSeleccionados;
	private String descPrestacion;
	private String opsAModificar;
	private String valorAModificar;
	private String valorModificarOPS;
	private String valorLimite;
	private String prioridad;

	private Date fechaEntregaDesde;
	private Date fechaEntregaHasta;
	private Date fechaCreacionDesde;
	private Date fechaCreacionHasta;
	private Date nuevaFechaEntrega;

	private boolean incapacidadFecha = false;

	private List<SelectItem> listaPlanes;
	private List<SelectItem> listaAfiliados;
	private List<SelectItem> listaEstadosMega;
	private List<SelectItem> listaSedes;
	private List<SelectItem> listaGruposEntrega;
	private List<SelectItem> listaTiposPrestaciones;
	private List<TotalResumenOpsVO> listaTotalResumenOpsTmpVO;
	private List<TotalResumenOpsVO> listaTotalResumenOpsVO;
	private ColeccionOpsVO coleccionOpsVO;
	private boolean showValor;
	private boolean modificarFecha;
	private boolean hasBusquedaOPS;

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {
			hasBusquedaOPS = false;
			modificarFecha = false;
			showValor = true;
			ModificaFechasEntregaOPSController modificaFechasOPSController = new ModificaFechasEntregaOPSController();
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();

			listaPlanes = new ArrayList<SelectItem>();
			listaAfiliados = new ArrayList<SelectItem>();
			listaEstadosMega = new ArrayList<SelectItem>();
			listaSedes = new ArrayList<SelectItem>();
			listaGruposEntrega = new ArrayList<SelectItem>();
			listaTiposPrestaciones = new ArrayList<SelectItem>();

			listaPlanes = modificaFechasOPSController.obtenerListaTiposPlan(null);
			listaAfiliados = modificaFechasOPSController.obtenerListaTiposAfiliado(null);
			listaEstadosMega = modificaFechasOPSController.obtenerListaEstadosMega(null);
			CargarCombosController combos = new CargarCombosController();
			listaSedes = combos.obtenerListaSedes(null);
			listaGruposEntrega = modificaFechasOPSController.obtenerListaGruposEntrega(null);
			listaTiposPrestaciones = consultaBodegaController.obtenerListaSelectTipoPrestacionesBodegaPolit(null, 2);
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (Exception e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	public void exportExcel() {
		String url = "/ExportarDetalleOpsServlet";
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			context.getExternalContext().dispatch(url);
		} catch (Exception e) {
			manejarExcepcion(e, e.getMessage());
		} finally {
			context.responseComplete();
		}
	}

	public void habilitarCampos() {
		if (valorAModificar.equals(ConstantesWeb.VALOR))
			showValor = true;
		else
			showValor = false;
	}

	public void recalcularValores() {
		try {
			ModificaFechasEntregaOPSController modificaFechasOPSController = new ModificaFechasEntregaOPSController();
			Double valorBase = (valorLimite != null && !valorLimite.isEmpty()) ? Double.parseDouble(valorLimite) : 0;
			Integer valorPrioridad = (prioridad != null && !prioridad.isEmpty()
					&& prioridad.equals(ConstantesWeb.OPS_MAS_RECIENTE)) ? ConstantesWeb.VALOR_UNO
							: ConstantesWeb.VALOR_CERO;
			boolean continuar = true;
			if (!valorAModificar.equals(ConstantesWeb.VALOR)) {
				valorBase = ConstantesData.VALOR_CERO_D;
				for (int i = 0; i < listaTotalResumenOpsTmpVO.size(); i++) {
					TotalResumenOpsVO val1 = listaTotalResumenOpsVO.get(i);
					TotalResumenOpsVO val2 = listaTotalResumenOpsTmpVO.get(i);

					continuar = validarPorcentajes(val1, val2);
				}
			}
			if (continuar) {
				List<TotalResumenOpsVO> listaTotalResumenOpsTmp = modificaFechasOPSController.recalcularListaResumenOPS(
						listaTotalResumenOpsVO, valorBase, valorPrioridad, FacesUtils.getUserName());

				listaTotalResumenOpsVO = new ArrayList<TotalResumenOpsVO>();
				listaTotalResumenOpsVO.addAll(listaTotalResumenOpsTmp);

				calcularTotalAModificar();
			}
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (Exception e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	private boolean validarPorcentajes(TotalResumenOpsVO val1, TotalResumenOpsVO val2) {
		if (val1.getPorcentajeCantidadOpsAModificar() != null && val2.getPorcentajeCantidadOpsAModificar() != null) {
			float fval1 = val1.getPorcentajeCantidadOpsAModificar().floatValue();
			float fval2 = val2.getPorcentajeCantidadOpsAModificar().floatValue();
			if (fval1 < ConstantesData.VALOR_CERO || fval1 > fval2) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.MENSAJE_CAMBIO_FECHA_SUCCESS),
						ConstantesWeb.COLOR_ROJO);
				return false;

			}
		}
		return true;
	}

	public void consultarResumenOps() {
		try {
			if (validateFields()) {
				ModificaFechasEntregaOPSController modificaFechasOPSController = new ModificaFechasEntregaOPSController();

				CriteriosConsultaOps criteriosConsultaOps = new CriteriosConsultaOps();
				criteriosXml(criteriosConsultaOps);
				criteriosConsultaOps.setFechaCreacionIni(fechaCreacionDesde);
				criteriosConsultaOps.setFechaCreacionFin(fechaCreacionHasta);
				criteriosConsultaOps.setFechaEntregaIni(fechaEntregaDesde);
				criteriosConsultaOps.setFechaEntregaFin(fechaEntregaHasta);
				criteriosConsultaOps.setPrestacion(consecutivoPrestacion);
				criteriosConsultaOps.setUsuario(FacesUtils.getUserName());

				this.listaTotalResumenOpsVO = modificaFechasOPSController
						.consultarListaResumenOPS(criteriosConsultaOps);

				if (listaTotalResumenOpsVO != null && !listaTotalResumenOpsVO.isEmpty()) {
					initListTemporal();

					this.listaTotalResumenOpsTmpVO.addAll(listaTotalResumenOpsVO);
					calcularTotalAModificar();
					hasBusquedaOPS = true;
				} else
					hasBusquedaOPS = false;
			}
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (Exception e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	private void initListTemporal() {
		if (listaTotalResumenOpsTmpVO == null)
			listaTotalResumenOpsTmpVO = new ArrayList<TotalResumenOpsVO>();
		else
			listaTotalResumenOpsTmpVO.clear();
	}

	private void calcularTotalAModificar() {
		int cantidadOpsModificar = ConstantesData.VALOR_CERO;
		Double valorOpsModificarTtl = ConstantesData.VALOR_CERO_D;
		for (TotalResumenOpsVO resumen : listaTotalResumenOpsVO) {
			if (!resumen.getDescripcionAgrupador().equals(ConstantesData.TOTAL_LABEL)) {
				cantidadOpsModificar += resumen.getCantidadOpsAModificar();
				valorOpsModificarTtl += resumen.getValorOpsAModificar();

			}
		}
		valorModificarOPS = String.valueOf(valorOpsModificarTtl);
		opsAModificar = String.valueOf(cantidadOpsModificar);
	}

	public void guardar() {
		try {
			if (nuevaFechaEntrega == null || nuevaFechaEntrega.getTime() < (new Date()).getTime()) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.FECHA_ENTREGA_INVALIDA),
						COLOR_ROJO);
				return;
			}
			String userLogin = FacesUtils.getUserName();
			ModificaFechasEntregaOPSController modificaFechasOPSController = new ModificaFechasEntregaOPSController();

			if (this.listaTotalResumenOpsVO != null && !this.listaTotalResumenOpsVO.isEmpty()) {
				modificaFechasOPSController.cambiarFechasEntrega(nuevaFechaEntrega, userLogin);
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.MENSAJE_CAMBIO_FECHA_SUCCESS),
						ConstantesWeb.COLOR_AZUL);
			}
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (Exception e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	public void asignarNuevaFechaHabilitar() {
		if (listaTotalResumenOpsVO != null && !listaTotalResumenOpsVO.isEmpty())
			modificarFecha = !modificarFecha;
	}

	private boolean validateFields() {
		boolean fecha = true;
		if (fechaEntregaDesde == null && fechaCreacionDesde == null)
			fecha = false;

		boolean fecha1 = validarFechaEntrega();
		boolean fecha2 = validarFechaCreacion();
		if (!(fecha1 && fecha2))
			fecha = false;

		if (!fecha && (estadosSeleccionados == null || estadosSeleccionados.isEmpty())) {
			MostrarMensaje.mostrarMensaje(
					Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_CRITERIOS_OBLIGATORIOS), COLOR_ROJO);
			return false;
		}

		return true;
	}

	private boolean validarFechaCreacion() {
		if (fechaCreacionDesde != null || fechaCreacionHasta != null) {
			if (fechaCreacionDesde != null && fechaCreacionHasta == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_FECHA_CREACION_HASTA), COLOR_ROJO);
				return false;
			}

			if (fechaCreacionDesde == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_FECHA_CREACION_DESDE), COLOR_ROJO);
				return false;
			}
			if (!validarRangoFecha(fechaCreacionDesde, fechaCreacionHasta, ConstantesWeb.FECHA_CREACION))
				return false;

		}
		return true;
	}

	private boolean validarFechaEntrega() {
		if (fechaEntregaDesde != null || fechaEntregaHasta != null) {
			if (fechaEntregaDesde != null && fechaEntregaHasta == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_FECHA_ENTREGA_HASTA), COLOR_ROJO);
				return false;
			}

			if (fechaEntregaDesde == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_FECHA_ENTREGA_DESDE), COLOR_ROJO);
				return false;
			}

			if (!validarRangoFecha(fechaEntregaDesde, fechaEntregaHasta, ConstantesWeb.FECHA_ENTREGA))
				return false;
		}
		return true;
	}

	private boolean validarRangoFecha(Date ini, Date fin, String campo) {
		if (ini.getTime() > fin.getTime()) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS)
					+ ConstantesWeb.CADENA_EN_BLANCO + campo, COLOR_ROJO);
			return false;
		}

		if (fin.getTime() < ini.getTime()) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS2)
					+ ConstantesWeb.CADENA_EN_BLANCO + campo, COLOR_ROJO);
			return false;
		}
		return true;
	}

	/**
	 * Permite setear los parametros de los combos, convertidos a xml
	 * 
	 * @param criteriosConsultaOps
	 */
	private void criteriosXml(CriteriosConsultaOps criteriosConsultaOps) {

		String planes = FacesUtils.cadenaToXMLFormat(planesSeleccionados, ConstantesWeb.VALOR_COMA, ConstantesWeb.PLNS,
				ConstantesWeb.PLN);
		String afiliados = FacesUtils.cadenaToXMLFormat(afiliadosSeleccionados, ConstantesWeb.VALOR_COMA,
				ConstantesWeb.AFLDS, ConstantesWeb.AFLD);
		String estados = FacesUtils.cadenaToXMLFormat(estadosSeleccionados, ConstantesWeb.VALOR_COMA,
				ConstantesWeb.ESTDS, ConstantesWeb.ESTDO);
		String sedes = FacesUtils.cadenaToXMLFormat(sedesSeleccionados, ConstantesWeb.VALOR_COMA, ConstantesWeb.SDS,
				ConstantesWeb.SDE);
		String gruposEntrega = FacesUtils.cadenaToXMLFormat(sedesSeleccionados, ConstantesWeb.VALOR_COMA,
				ConstantesWeb.GRP_ENTREGAS, ConstantesWeb.GRP_ENTREGA);

		criteriosConsultaOps.setPlanes(planes);
		criteriosConsultaOps.setAfiliados(afiliados);
		criteriosConsultaOps.setEstados(estados);
		criteriosConsultaOps.setSedes(sedes);
		criteriosConsultaOps.setGruposEntrega(gruposEntrega);
	}

	private void manejarExcepcion(Exception e, String message) {
		LOG.error(e.getMessage(), e);
		MostrarMensaje.mostrarMensaje(message, COLOR_ROJO);
	}

	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	public String getPlanesSeleccionados() {
		return planesSeleccionados;
	}

	public void setPlanesSeleccionados(String planesSeleccionados) {
		this.planesSeleccionados = planesSeleccionados;
	}

	public String getAfiliadosSeleccionados() {
		return afiliadosSeleccionados;
	}

	public void setAfiliadosSeleccionados(String afiliadosSeleccionados) {
		this.afiliadosSeleccionados = afiliadosSeleccionados;
	}

	public String getEstadosSeleccionados() {
		return estadosSeleccionados;
	}

	public void setEstadosSeleccionados(String estadosSeleccionados) {
		this.estadosSeleccionados = estadosSeleccionados;
	}

	public String getSedesSeleccionados() {
		return sedesSeleccionados;
	}

	public void setSedesSeleccionados(String sedesSeleccionados) {
		this.sedesSeleccionados = sedesSeleccionados;
	}

	public String getGruposSeleccionados() {
		return gruposSeleccionados;
	}

	public void setGruposSeleccionados(String gruposSeleccionados) {
		this.gruposSeleccionados = gruposSeleccionados;
	}

	public String getDescPrestacion() {
		return descPrestacion;
	}

	public void setDescPrestacion(String descPrestacion) {
		this.descPrestacion = descPrestacion;
	}

	public String getOpsAModificar() {
		return opsAModificar;
	}

	public void setOpsAModificar(String opsAModificar) {
		this.opsAModificar = opsAModificar;
	}

	public String getValorAModificar() {
		return this.valorAModificar;
	}

	public void setValorAModificar(String valorAModificar) {
		this.valorAModificar = valorAModificar;
	}

	public String getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(String valorLimite) {
		this.valorLimite = valorLimite;
	}

	public Date getFechaEntregaDesde() {
		return fechaEntregaDesde;
	}

	public void setFechaEntregaDesde(Date fechaEntregaDesde) {
		this.fechaEntregaDesde = fechaEntregaDesde;
	}

	public Date getFechaEntregaHasta() {
		return fechaEntregaHasta;
	}

	public void setFechaEntregaHasta(Date fechaEntregaHasta) {
		this.fechaEntregaHasta = fechaEntregaHasta;
	}

	public Date getFechaCreacionDesde() {
		return fechaCreacionDesde;
	}

	public void setFechaCreacionDesde(Date fechaCreacionDesde) {
		this.fechaCreacionDesde = fechaCreacionDesde;
	}

	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

	public Date getNuevaFechaEntrega() {
		return nuevaFechaEntrega;
	}

	public void setNuevaFechaEntrega(Date nuevaFechaEntrega) {
		this.nuevaFechaEntrega = nuevaFechaEntrega;
	}

	public boolean isIncapacidadFecha() {
		return incapacidadFecha;
	}

	public void setIncapacidadFecha(boolean incapacidadFecha) {
		this.incapacidadFecha = incapacidadFecha;
	}

	public List<SelectItem> getListaPlanes() {
		return listaPlanes;
	}

	public void setListaPlanes(List<SelectItem> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}

	public List<SelectItem> getListaAfiliados() {
		return listaAfiliados;
	}

	public void setListaAfiliados(List<SelectItem> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}

	public List<SelectItem> getListaEstadosMega() {
		return listaEstadosMega;
	}

	public void setListaEstadosMega(List<SelectItem> listaEstadosMega) {
		this.listaEstadosMega = listaEstadosMega;
	}

	public List<SelectItem> getListaSedes() {
		return listaSedes;
	}

	public void setListaSedes(List<SelectItem> listaSedes) {
		this.listaSedes = listaSedes;
	}

	public List<SelectItem> getListaGruposEntrega() {
		return listaGruposEntrega;
	}

	public void setListaGruposEntrega(List<SelectItem> listaGruposEntrega) {
		this.listaGruposEntrega = listaGruposEntrega;
	}

	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	public List<TotalResumenOpsVO> getListaTotalResumenOpsVO() {
		return listaTotalResumenOpsVO;
	}

	public void setListaTotalResumenOpsVO(List<TotalResumenOpsVO> listaTotalResumenOpsVO) {
		this.listaTotalResumenOpsVO = listaTotalResumenOpsVO;
	}

	public ColeccionOpsVO getColeccionOpsVO() {
		return coleccionOpsVO;
	}

	public void setColeccionOpsVO(ColeccionOpsVO coleccionOpsVO) {
		this.coleccionOpsVO = coleccionOpsVO;
	}

	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	public void setShowValor(boolean showValor) {
		this.showValor = showValor;
	}

	public boolean isShowValor() {
		return showValor;
	}

	public void setValorModificarOPS(String valorModificarOPS) {
		this.valorModificarOPS = valorModificarOPS;
	}

	public String getValorModificarOPS() {
		return valorModificarOPS;
	}

	public void setModificarFecha(boolean modificarFecha) {
		this.modificarFecha = modificarFecha;
	}

	public boolean isModificarFecha() {
		return modificarFecha;
	}

	public void setHasBusquedaOPS(boolean hasBusquedaOPS) {
		this.hasBusquedaOPS = hasBusquedaOPS;
	}

	public boolean isHasBusquedaOPS() {
		return hasBusquedaOPS;
	}
}
