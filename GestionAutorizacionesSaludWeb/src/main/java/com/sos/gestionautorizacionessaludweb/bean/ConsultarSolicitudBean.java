package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.ModuloVO;
import com.sos.gestionautorizacionessaluddata.model.ServicioVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EstadoDerechoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConsultaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DetalleSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.GestionAutorizacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoFormatosVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoModificacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoTareaVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.PrestacionPisVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosAdicionalesPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaluddata.util.ConstantesData;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionMedicinaTrabajoController;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.GeneracionAutorizacionServicioController;
import com.sos.gestionautorizacionessaludejb.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.controller.PrestadorController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.SolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MenuBean;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.ips.modelo.IPSVO;
import com.sos.util.jsf.FacesUtil;


public class ConsultarSolicitudBean implements Serializable {
	private static final long serialVersionUID = 8125482373932803634L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultarSolicitudBean.class);
	private static final String COLOR_ROJO = "red";
	private static final String COLOR_AZUL = "blue";
	private static final String FILTRO_FECHA = "fechas";
	private static final String FILTRO_RADICADO = "radicado";
	private static final String FILTRO_SOLICITUD = "solicitud";
	private static final String FILTRO_AUTORIZACION = "autorizacion";
	private static final String PARAMETRO_CUPS = "consecutivoCups";
	private static final String PARAMETRO_CUMS = "consecutivoCums";
	private static final String PARAMETRO_CUOS = "consecutivoCuos";

	private transient EJBResolverInstance ejbs;

	IngresoSolicitudBean ingresoSolicitudBean;
	ClienteRestBpmBean clienteRestBpmBean;

	private List<SelectItem> listaTiposIdentificacion = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposPrestaciones = new ArrayList<SelectItem>();
	private List<SelectItem> listaPlanes = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausas = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasModFechaEntrega = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasReliquidar = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasOPS = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasPrestacion = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasDireccionamientoOPS = new ArrayList<SelectItem>();
	private List<SelectItem> lstMotivosCausasPrestacionesPAFPOC = new ArrayList<SelectItem>();

	private boolean mostrarPopupPrestadores = false;

	private AfiliadoVO afiliadoVO = new AfiliadoVO();
	private List<IPSVO> listaIPSVO = new ArrayList<IPSVO>();

	private List<ProcedimientosVO> listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
	private List<MedicamentosVO> listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
	private List<PrestacionPisVO> listaPisEncontrados = new ArrayList<PrestacionPisVO>();
	private List<DatosSolicitudVO> listaSolicitudes = new ArrayList<DatosSolicitudVO>();
	private List<DiagnosticoDTO> listaDiagnosticoDTO = new ArrayList<DiagnosticoDTO>();
	private List<DocumentoSoporteVO> listaDocumentoSoporteVO = new ArrayList<DocumentoSoporteVO>();
	private List<PrestacionDTO> listaPrestacionDTO = new ArrayList<PrestacionDTO>();
	private List<DatosAdicionalesPrestacionDTO> listaInformacionAdicionalPrestacionDTO = new ArrayList<DatosAdicionalesPrestacionDTO>();	
	private List<PrestacionDTO> listaPrestacionFechaEntrega = new ArrayList<PrestacionDTO>();
	private List<InconsistenciasResponseServiceVO> listaInconsistenciasMallaService = new ArrayList<InconsistenciasResponseServiceVO>();
	
	private List<HistoricoFormatosVO> listaHistoricoFormatosVO = new ArrayList<HistoricoFormatosVO>();
	private List<HistoricoModificacionVO> listaHistoricoModificacionVO = new ArrayList<HistoricoModificacionVO>();
	private List<ConceptosGastoVO> listaConceptosGastoVO = new ArrayList<ConceptosGastoVO>();
	private List<PrestacionDTO> lstPrestacionesOPS = new ArrayList<PrestacionDTO>();
	private List<PrestadorVO> lstPrestadoresDireccionamiento = new ArrayList<PrestadorVO>();
	private List<AutorizacionServicioVO> listaAutorizacionServicioVOs;
	private List<MotivoCausaVO> listaMotivosCausaVo;
	private List<CiudadVO> listaCiudadesDireccionamiento = new ArrayList<CiudadVO>();

	private IPSVO ipsVO = new IPSVO();
	private ProcedimientosVO procedimientoVOSeleccionado;
	private MedicamentosVO medicamentoVOSeleccionado;
	private PrestacionPisVO pisVOSeleccionado;
	private DatosSolicitudVO solicitudSeleccionada;
	private PrestacionDTO prestacionSeleccionada;
	private DetalleSolicitudVO detalleSolicitudVO;
	private DocumentoSoporteVO documentoSoporteVOSeleccionado;
	private InconsistenciasVO inconsistenciasVO = new InconsistenciasVO();
	private ConceptosGastoVO conceptosGastoVOSel = new ConceptosGastoVO();
	private PrestadorVO prestadorOPS = new PrestadorVO();

	private Integer consecutivoPlan;
	private Integer tipoPrestacionSeleccionado;
	private Date fechaConsultaInicial;
	private Date fechaConsultaFinal;
	private Date fechaActual;
	private String numeroRadicado;
	private String numeroSolicitud;
	private String numeroAutorizacion;
	private String opcionBusquedaSeleccionada;
	private Integer consecutivoPrestacion;
	private String codigoPrestacion;
	private String descripcionPrestacion;
	private String medioContacto;
	private boolean habilitarFiltroFechas;
	private boolean habilitarFiltroRadicado;
	private boolean habilitarFiltroSolicitud;
	private boolean habilitarFiltroAutorizacion;
	private boolean mostrarPopupMedicamentos = false;
	private boolean mostrarPopupCUPS = false;
	private boolean mostrarPopupPis = false;
	private boolean mostrarPopupHospitalizacion = false;
	private boolean mostrarPopupDocumentosSoporte = false;
	private boolean mostrarPopupRespuestaMalla = false;
	private boolean mostrarPopupHistoricoDescargas = false;
	private boolean mostrarPopupHistoricoModificacion = false;

	private int consecutivoCUMS;
	private int consecutivoCUPS;
	private int consecutivoCUOS;

	private Integer causalAnulacionSolicitud;
	private Integer causalModFechaEntrega;
	private Integer causalReliquidar;
	private String obsAnulacionSolicitud;
	private String obsModificarFechaEntrega;
	private String obsReliquidar;
	private Integer causalAnulacionOPS;
	private String obsAnulacionOPS;
	private Integer causalDireccionamientoOPS;
	private String obsDireccionamientoOPS;
	private String codInternoPrestadorNuevo;
	private String perfil;

	
	private boolean mostrarPopupAnularSolicitud;
	private boolean mostrarPopupAnularOPS;
	private boolean mostrarPopupModFechaEntrega;
	private boolean mostrarPopupReliquidar;
	private boolean mostrarConfirmarModFecha;

	private boolean mostrarPopupModificarOPS;
	private boolean mostrarPopupcambiarDirOPS;

	private boolean mostrarBtnAnular;
	private boolean mostrarBtnModificar;
	private boolean mostrarBtnModificarFechaEntrega = false;

	private boolean permisoAnularSolicitudOPS = false;

	private boolean mostrarPopupGenerarOPS;
	private boolean generarConObservaciones;
	private boolean valEspecial;
	private ValidacionEspecialVO validacionEspecialVO;
	private List<AutorizacionServicioVO> listaAutorizacionServicioVO;
	private boolean mostrarPopupGenerarDevolucion;
	private boolean mostrarPopupGenerarNegacion;
	private Integer numMaximoMesesDescarga;
	private boolean mostrarPopupAutorizarNegacion;
	private String numeroAutorizacionPrestacionIPS;
	private Integer causalAutorizacionPrestacion;
	private String obsDireccionamientoAutorizacion;
	private boolean habilitaGestionNumeroAutorizacionIPS;
	private boolean mostrarLinkAutorizar;
	private boolean habilitaDatosAutorizaPrestacion;

	private boolean visualizarPopupAccesoDirecto;
	private boolean visualizarPopupValidarAccesoDirecto;
	private String usuarioDescarga;
	private String usuarioDescargaPass;

	private List<ContingenciaRecobroVO> listaContingenciaRecobroVOs = new ArrayList<ContingenciaRecobroVO>();
	private List<SelectItem> listaContingenciaRecobros = new ArrayList<SelectItem>();
	private boolean editarRecobro;
	private boolean mostrarBtnEditarRecobro;
	private Integer consecutivoContingenciaRecobro = ConstantesWeb.VALOR_INICIAL;
	private boolean mostrarDescarga;
	
	private boolean modificarDireccionamientoConOPS;
	private boolean mostrarConfirmarCambiarDireccionamientoConcepto;
	
	//Va
	private boolean mostrarPopupCambiarRecobro;
	private boolean mostrarLinkRecobro;
	
	private List<SelectItem> lstCausalesNoCobroCuota = new ArrayList<SelectItem>();
	
	private boolean modificarInformacionPrestacion;
	
	private boolean cambiarDireccionamientoSinOPS;
	private boolean mostrarPopupcambiarDirSinOPS;
	private boolean cambiarCausalesNoCobro;
	
	private CiudadVO ciudad = new CiudadVO();
	private boolean mostrarPopupCiudades;	
	private boolean activarFiltroCiudad;
	
	private boolean mostrarPopupAnularPrestacion;
	private Integer causalAnulacionPrestacion;
	private String observacionAnulacionPrestacion;
	private boolean habilitarEditarPrestacion;
	private boolean modificarInformacionRecobro;
	private boolean mostrarOptionSolicitudes;
	

	

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {

			fechaActual = Calendar.getInstance().getTime();
			consecutivoCUMS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUMS));
			consecutivoCUPS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUPS));
			consecutivoCUOS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUOS));
			clienteRestBpmBean = (ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
			mostrarDescarga = !(Boolean)FacesUtils.getSessionParameter(ConstantesWeb.ES_COORDINADOR_ASI);
			
			List<ParametrosGeneralesVO> listaParametrosGeneralesVOs = ((UsuarioBean) FacesUtils
					.getManagedBean("usuarioBean")).getListaParametrosGenerales();
			listaParametrosGeneralesVOs = ConsultaSolicitudesUtil.validarConsultaParametrosGenerales(listaParametrosGeneralesVOs);						
			obtenerListaParametrosGenerales(listaParametrosGeneralesVOs);
			afiliadoVO.setPlan(new PlanVO());
			CargarCombosController cargarCombosController = new CargarCombosController();
			listaTiposIdentificacion = CargarCombosUtil.obtenerListaSelectTiposIdentificacion(null,
					cargarCombosController);
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			listaTiposPrestaciones = registrarSolicitudesController.obtenerListaSelectTipoPrestaciones(null, 1);
			listaPlanes = cargarCombosController.obtenerListaPlanesConsecutivoDescripcion(null);
			consultarPerfilUsuario();
			mostrarOptionSolicitudes = ConstantesWeb.PERFIL_ADMINISTRADOR_CNA.equalsIgnoreCase(perfil.trim())
										|| ConstantesWeb.PERFIL_COORDINADOR.equalsIgnoreCase(perfil.trim());
			lstMotivosCausas = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_SOLICITUD);
			lstMotivosCausasOPS = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_OPS);
			lstMotivosCausasPrestacion = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_ANULAR_PRESTACION);
			lstMotivosCausasDireccionamientoOPS = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_MOD_DIRECCIONAMIENTO);
			lstMotivosCausasModFechaEntrega = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_MOD_FECHA);
			lstMotivosCausasReliquidar = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_RELIQ);
			lstMotivosCausasPrestacionesPAFPOC = ConsultaSolicitudesUtil
					.consultaMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_PRESTACIONES_PAFPOC);

			numMaximoMesesDescarga = ConsultaSolicitudesUtil.valorMaximoMesesDescarga(listaParametrosGeneralesVOs);			
			ingresoSolicitudBean = (IngresoSolicitudBean) FacesUtil.getBean(ConstantesWeb.INGRESAR_SOLICITUD_BEAN);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}
	
	private EJBResolverInstance getEjbs(){
		if (ejbs == null){
			ejbs = (EJBResolverInstance)FacesUtils.getManagedBean(ConstantesWeb.EJB_RESOLVER_BEAN);
		} 
		return ejbs;
	}

	/**
	 * metodo que permite obyener los parametros generales
	 */
	private static void obtenerListaParametrosGenerales(List<ParametrosGeneralesVO> listaParametrosGeneralesVOs) {
		for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGeneralesVOs) {
			if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == 87) {
				int cantidadMeses = Integer.parseInt(parametrosGeneralesVO.getValorParametroGeneral().toString());
				Calendar fechaMinima = Calendar.getInstance();
				fechaMinima.add(Calendar.MONTH, -cantidadMeses);
				FacesUtils.setSessionParameter(ConstantesWeb.MINIMA_FECHA_CONSULTA_SOLICITUD,
						fechaMinima.getTimeInMillis());
			}
		}
	}

	public void seleccionFiltroBusqueda() {
		habilitarFiltroFechas = false;
		habilitarFiltroRadicado = false;
		habilitarFiltroSolicitud = false;
		habilitarFiltroAutorizacion = false;
		if (opcionBusquedaSeleccionada.equals(FILTRO_FECHA)) {
			habilitarFiltroFechas = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_RADICADO)) {
			habilitarFiltroRadicado = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_SOLICITUD)) {
			habilitarFiltroSolicitud = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_AUTORIZACION)) {
			habilitarFiltroAutorizacion = true;
		}
		limpiarCampos();
	}

	private void limpiarCampos() {
		ipsVO = new IPSVO();
		tipoPrestacionSeleccionado = null;
		consecutivoPrestacion = null;
		codigoPrestacion = "";
		descripcionPrestacion = "";
		numeroRadicado = "";
		numeroSolicitud = "";
		numeroAutorizacion = "";
		fechaConsultaInicial = null;
		fechaConsultaFinal = null;
		consecutivoPlan = null;
	}

	public void limpiarDatos() {
		limpiarCampos();
		inactivarMenu(true);
		listaSolicitudes = new ArrayList<DatosSolicitudVO>();
	}

	/**
	 * Metodo que permite realizar la consulta de las solicitudes
	 */
	public void consultarSolicitudes() {
		try {
			ConsultaSolicitudVO consultaSolicitudVO = new ConsultaSolicitudVO();
			consultaSolicitudVO = ConsultaSolicitudesUtil.validarConsultarSolicitudes(opcionBusquedaSeleccionada, consultaSolicitudVO);

			if (opcionBusquedaSeleccionada.equals(FILTRO_FECHA)) {
				listaSolicitudes = consultarSolicitudesFecha(consultaSolicitudVO);
			} else if (opcionBusquedaSeleccionada.equals(FILTRO_RADICADO)) {
				listaSolicitudes = consultarSolicitudesRadicado(consultaSolicitudVO);
			} else if (opcionBusquedaSeleccionada.equals(FILTRO_SOLICITUD)) {
				listaSolicitudes = consultarSolicitudesSolicitud(consultaSolicitudVO);
			} else if (opcionBusquedaSeleccionada.equals(FILTRO_AUTORIZACION)) {
				listaSolicitudes = consultarSolicitudesAutorizacion(consultaSolicitudVO);
			} else {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_BUSQUEDA_SOLICITUD),
						ErrorType.PARAMETRO_ERRADO);
			}
			SolicitudController solicitudController = new SolicitudController();
			listaSolicitudes = solicitudController.agruparSolicitudes(listaSolicitudes);
			inhabilitarDescargaOPSSiCoordinador();
			llenarTooltip();

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}
	
	/**
	 * Metodo que inhabilita la descarga de OPS para el coordinador.
	 */
	private void inhabilitarDescargaOPSSiCoordinador() {
		if(listaSolicitudes != null && !listaSolicitudes.isEmpty()) {
			for(DatosSolicitudVO solicitud: listaSolicitudes) {
				List<PrestacionDTO> tempListaPrestaciones = solicitud.getlPrestaciones();
				for(PrestacionDTO prestaDTO: tempListaPrestaciones) {
					boolean solicitudDescagar = prestaDTO.isHabilitaDescarga();
					solicitudDescagar = solicitudDescagar && mostrarDescarga;
					prestaDTO.setHabilitaDescarga(solicitudDescagar);					
				}
			}
		}
	}

	/**
	 * Llenar tooltip auditoria
	 * 
	 * @throws LogicException
	 */
	private void llenarTooltip() throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
		for (DatosSolicitudVO solicitud : listaSolicitudes) {
			if (ConstantesData.COD_ESTADO_AUDITORIA.equals(solicitud.getPrestacionDTO().getConsecutivoEstado())) {
				List<HistoricoTareaVO> lstvo = consultarSolicitudController
						.consultaHistoricoTareasxSolicitud(solicitud.getNumeroSolicitud());
				if (!lstvo.isEmpty()) {
					solicitud.setNombreGrupoRiesgo(lstvo.get(0).getNombreGrupoRiesgo());
					solicitud.setNombreTarea(lstvo.get(0).getNombreTarea());
					solicitud.setUsuarioTarea(lstvo.get(0).getUsuarioTarea());
				}
			}
		}
	}

	private ConsultarSolicitudController generarConsultarSolicitudController() throws LogicException {
		ConsultarSolicitudController consultarSolicitudController;
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		return consultarSolicitudController;
	}

	/**
	 * Metodo que permite realizar la consulta de solicitudes por radicado
	 * 
	 * @param consultaSolicitudVO
	 * @throws LogicException
	 */
	private List<DatosSolicitudVO> consultarSolicitudesRadicado(ConsultaSolicitudVO consultaSolicitudVO)
			throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();

		ConsultaSolicitudVO consultar = ConsultaSolicitudesUtil
				.validarConsultaSolicitudesxRadicado(numeroRadicado.trim(), consultaSolicitudVO);
		return consultarSolicitudController.consultaSolicitud(consultar);
	}

	/**
	 * Metodo que permite realizar la consulta de solicitudes por numero de
	 * autorizacion
	 * 
	 * @param consultaSolicitudVO
	 * @throws LogicException
	 */
	private List<DatosSolicitudVO> consultarSolicitudesAutorizacion(ConsultaSolicitudVO consultaSolicitudVO)
			throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
		ConsultaSolicitudVO consultar = ConsultaSolicitudesUtil
				.validarConsultaSolicitudesxOPS(numeroAutorizacion.trim(), consultaSolicitudVO);
		return consultarSolicitudController.consultaSolicitud(consultar);
	}

	/**
	 * Metodo que permite realizar la consulta de solicitudes por fechas
	 * 
	 * @param consultaSolicitudVO
	 * @throws LogicException
	 */
	private List<DatosSolicitudVO> consultarSolicitudesFecha(ConsultaSolicitudVO consultaSolicitudVO)
			throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
		ConsultaSolicitudVO consultar = ConsultaSolicitudesUtil.validarConsultaSolicitudesxFecha(fechaConsultaInicial,
				fechaConsultaFinal, consecutivoPlan, ipsVO, consecutivoPrestacion, consultaSolicitudVO);
		return consultarSolicitudController.consultaSolicitud(consultar);
	}

	/**
	 * Metodo que permite realizar la consulta de solicitudes por numero de
	 * solicitud
	 * 
	 * @param consultaSolicitudVO
	 * @throws LogicException
	 */
	private List<DatosSolicitudVO> consultarSolicitudesSolicitud(ConsultaSolicitudVO consultaSolicitudVO)
			throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
		ConsultaSolicitudVO consultar = ConsultaSolicitudesUtil
				.validarConsultaSolicitudesxNumeroSolicitud(numeroSolicitud.trim(), ipsVO, consultaSolicitudVO);
		return consultarSolicitudController.consultaSolicitud(consultar);
	}

	/**
	 * Metodo validacion especial
	 */
	public void validacionEspecial() {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			validacionEspecialVO = consultaAfiliadoController.validarNumeroAutorizacionAfiliado(
					solicitudSeleccionada.getAfiliadoVO().getTipoIdentificacionAfiliado()
							.getConsecutivoTipoIdentificacion(),
					solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion());
			valEspecial = true;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void nuevaBusquedaIPS() {
		ipsVO = new IPSVO();
	}

	public void buscarIPS() {
		try {
			mostrarPopupPrestadores = false;
			listaIPSVO = getEjbs().getIpsEJB().consultarIpsPorParametros(ipsVO);

			if (listaIPSVO == null || listaIPSVO.isEmpty()) {
				listaIPSVO = getEjbs().getIpsEJB().consultarConsultoriosPorParametros(ipsVO);
			}

			if (listaIPSVO != null && !listaIPSVO.isEmpty()) {
				if (listaIPSVO.size() == 1) {
					mostrarPopupPrestadores = false;
					ipsVO = listaIPSVO.get(0);
				} else {
					mostrarPopupPrestadores = true;
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_CONSULTAR_PRESTADOR),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void seleccionarIPS() {
		try {
			mostrarPopupPrestadores = false;
			listaIPSVO = null;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void cerrarPopupIPS() {
		try {
			mostrarPopupPrestadores = false;
			listaIPSVO = null;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void nuevaConsultaPrestacion() {
		try {
			tipoPrestacionSeleccionado = null;
			consecutivoPrestacion = null;
			codigoPrestacion = "";
			descripcionPrestacion = "";
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void cambioTipoPrestacion() {
		consecutivoPrestacion = null;
		codigoPrestacion = "";
		descripcionPrestacion = "";
	}

	public void buscarPrestacion() {
		try {
			if (tipoPrestacionSeleccionado == null) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (tipoPrestacionSeleccionado.compareTo(consecutivoCUMS) == 0) {
				buscarPrestacionMedicamentos();
			} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUPS) == 0) {
				buscarPrestacionCUPS();
			} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUOS) == 0) {
				buscarPrestacionPis();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void buscarPrestacionCUPS() {
		try {
			mostrarPopupCUPS = false;
			Utilidades.validarAdicionarPrestacion(codigoPrestacion, descripcionPrestacion);
			PrestacionController prestacionController = new PrestacionController();
			listaCUPSEncontrados = prestacionController.consultarPrestacionesCUPSPorParametros(
					new ProcedimientosVO(codigoPrestacion, descripcionPrestacion));

			FuncionesAppWeb funcionesConsulta = new FuncionesAppWeb();
			funcionesConsulta.validarAsociacionPrestacion(listaCUPSEncontrados);

			if (funcionesConsulta.isMostrarPopupCUPS()) {
				mostrarPopupCUPS = true;
			} else {
				mostrarPopupCUPS = false;
				procedimientoVOSeleccionado = funcionesConsulta.getProcedimientoVOSeleccionado();
				consecutivoPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getConsecutivoCodificacionProcedimiento();
				codigoPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getCodigoCodificacionProcedimiento();
				descripcionPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getDescripcionCodificacionProcedimiento();
			}

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void seleccionPopupCUPS() {
		if (procedimientoVOSeleccionado != null) {
			consecutivoPrestacion = procedimientoVOSeleccionado.getConsecutivoCodificacionProcedimiento();
			codigoPrestacion = procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento();
			descripcionPrestacion = procedimientoVOSeleccionado.getDescripcionCodificacionProcedimiento();
			mostrarPopupCUPS = false;
			listaCUPSEncontrados = null;
		}
	}

	public void cerrarPopupCUPS() {
		mostrarPopupCUPS = false;
		listaCUPSEncontrados = null;
	}

	public void buscarPrestacionMedicamentos() {
		try {
			mostrarPopupMedicamentos = false;
			Utilidades.validarAdicionarPrestacion(codigoPrestacion, descripcionPrestacion);
			PrestacionController prestacionController = new PrestacionController();
			listaMedicamentosEncontrados = prestacionController.consultarPrestacionesMedicamentosPorParametros(
					new MedicamentosVO(codigoPrestacion, descripcionPrestacion));

			if (listaMedicamentosEncontrados != null && !listaMedicamentosEncontrados.isEmpty()) {
				if (listaMedicamentosEncontrados.size() == 1) {
					mostrarPopupMedicamentos = false;
					medicamentoVOSeleccionado = listaMedicamentosEncontrados.get(0);
					consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
					codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
					descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
				} else {
					mostrarPopupMedicamentos = true;
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ASOCIAR_PRESTACION),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void seleccionPopupMedicamentos() {
		if (medicamentoVOSeleccionado != null) {
			consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
			codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
			descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
			mostrarPopupMedicamentos = false;
			listaMedicamentosEncontrados = null;
		}
	}

	public void cerrarPopupMedicamentos() {
		mostrarPopupMedicamentos = false;
		listaMedicamentosEncontrados = null;
	}

	public void buscarPrestacionPis() {
		try {
			mostrarPopupPis = false;
			PrestacionController prestacionController = new PrestacionController();
			listaPisEncontrados = prestacionController.consultarPrestacionesCUOSPorParametros(
					new PrestacionPisVO(codigoPrestacion, descripcionPrestacion));

			if (listaPisEncontrados != null && !listaPisEncontrados.isEmpty()) {
				if (listaPisEncontrados.size() == 1) {
					mostrarPopupPis = false;
					pisVOSeleccionado = listaPisEncontrados.get(0);
					consecutivoPrestacion = pisVOSeleccionado.getConsecutivoPrestacionPis();
					codigoPrestacion = pisVOSeleccionado.getCodigoPrestacionPis();
					descripcionPrestacion = pisVOSeleccionado.getDescripcionPrestacionPis();
				} else {
					mostrarPopupPis = true;
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ASOCIAR_PRESTACION),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void seleccionPopupPis() {
		if (pisVOSeleccionado != null) {
			consecutivoPrestacion = pisVOSeleccionado.getConsecutivoPrestacionPis();
			codigoPrestacion = pisVOSeleccionado.getCodigoPrestacionPis();
			descripcionPrestacion = pisVOSeleccionado.getDescripcionPrestacionPis();
			mostrarPopupPis = false;
			listaPisEncontrados = null;
		}
	}

	public void cerrarPopupPis() {
		mostrarPopupPis = false;
		listaPisEncontrados = null;
	}

	public String seleccionSolicitud() {
		String navega = ConstantesWeb.CADENA_VACIA;

		try {
			mostrarConfirmarModFecha = false;
			mostrarLinkAutorizar = false;
			habilitaDatosAutorizaPrestacion = false;
			mostrarBtnEditarRecobro = false;
			mostrarLinkRecobro = false;
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController();

			detalleSolicitudVO = consultarSolicitudController.consultaDetalleSolicitud(solicitudSeleccionada.getNumeroRadicado()).get(0);
			listaDiagnosticoDTO = gestionMedicinaTrabajoController.consultaDiagnosticos(solicitudSeleccionada.getNumeroSolicitud());
			listaPrestacionDTO = consultarSolicitudController.consultaInformacionPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			listaInformacionAdicionalPrestacionDTO = consultarSolicitudController.consultaInformacionAdicionalPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());			
			consultarConceptosGasto(consultarSolicitudController);
			medioContacto = consultarSolicitudController.removeDiacriticalMarks(detalleSolicitudVO.getDescripcionMedioContacto());

			SolicitudVO solicitudVO = new SolicitudVO();
			solicitudVO.setConsecutivoSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			solicitudVO.setNumeroSolicitudSOS(solicitudSeleccionada.getNumeroRadicado());

			perfil = consultarSolicitudController.consultarPerfilUsuario(
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());

			if (!ConstantesWeb.COD_ESTADO_ANULADO.equals(solicitudSeleccionada.getConsecutivoEstadoSolicitud())
					&& ConstantesWeb.PERFIL_COORDINADOR.equalsIgnoreCase(perfil)) {
				mostrarBtnAnular = false;
			} else {
				mostrarBtnAnular = true;
			}

			if (ConstantesWeb.PERFIL_ADMINISTRADOR_CNA.equalsIgnoreCase(perfil) && !ConstantesWeb.MEDIO_CONTACTOS_PROG_ENTREGA.equals(medioContacto)) {
				mostrarBtnModificarFechaEntrega = false;
			} else {
				mostrarBtnModificarFechaEntrega = true;
			}

			if (ConstantesWeb.PERFIL_COORDINADOR.equalsIgnoreCase(perfil)) {
				habilitaDatosAutorizaPrestacion = false;
			} else {
				habilitaDatosAutorizaPrestacion = true;
			}
			
			listaSolicitudes.clear();
			consultarDocumentosSoporte();            
			activarRecobroPrestacion();
			activarAnulacionPrestacion();
			validarConceptosGastos();
			activarCambiarDireccionamientoPrestacion();
			cargarListaComboRecobro();
			navega = ConstantesWeb.IR;

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

		return navega;
	}

	/**
	 * Metodo que valida el estado de las prestaciones y pone el atributo cambiarDireccionamiento
	 * en true o false dependiendo del resultado.
	 */
	private void activarCambiarDireccionamientoPrestacion() {
		for(PrestacionDTO prestacion: listaPrestacionDTO) {
			boolean activarDir= !ConstantesWeb.MEDIO_CONTACTOS_PROG_ENTREGA.equals(medioContacto);
			activarDir = activarDir && (ConstantesWeb.COD_ESTADO_LIQUIDADO.intValue() == prestacion.getConsecutivoEstado().intValue()
						|| ConstantesWeb.COD_ESTADO_APROBADO.intValue() == prestacion.getConsecutivoEstado().intValue()
						|| ConstantesWeb.COD_ESTADO_AUTORIZADO.intValue() == prestacion.getConsecutivoEstado().intValue());
			activarDir = activarDir && (ConstantesWeb.PERFIL_COORDINADOR.equalsIgnoreCase(perfil)
										|| ConstantesWeb.PERFIL_ADMINISTRADOR_CNA.equalsIgnoreCase(perfil));
			prestacion.setCambiarDireccionamiento(activarDir);
		}
	}
	
	/**
	 * Metodo que pone el estado habilitado a para la opcion de recobro por prestacion
	 */
	private void activarRecobroPrestacion() {
		try{
			for(PrestacionDTO prestacion: listaPrestacionDTO) {
				boolean permitir = ConstantesWeb.COD_ESTADO_ENTREGADA.intValue() != prestacion.getConsecutivoEstado().intValue();
				permitir = permitir	&& (ConstantesWeb.COD_ESTADO_ANULADO.intValue() != prestacion.getConsecutivoEstado().intValue());
				permitir = permitir	&& (ConstantesWeb.COD_ESTADO_PROCESADA.intValue() != prestacion.getConsecutivoEstado().intValue());
				permitir = permitir && (ConstantesWeb.COD_ESTADO_USADA.intValue() != prestacion.getConsecutivoEstado().intValue());
				permitir = permitir && (ConstantesWeb.COD_ESTADO_AUTORIZADO.intValue() != prestacion.getConsecutivoEstado().intValue());
				prestacion.setHabilitarCambiarRecobro(permitir);			
			}	
		}catch(NullPointerException e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_ESTADO_NO_ENCONTRADO), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Metodo que consulta los conceptos de gasto, recibe como parametros un objeto ConsultarSolicitudController
	 * @param consultarSolicitudController
	 */
	private void consultarConceptosGasto(ConsultarSolicitudController consultarSolicitudController) {
		try {
			listaConceptosGastoVO = consultarSolicitudController
					.consultaConceptosGastosxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			activarRecobroPrestacion();
			validarConceptosGastos();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_CONSULTAR_CONCEPTOS), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void cargarListaComboRecobro() {
		DiagnosticoDTO diagnosticoPrincipal = obtenerDiagnosticoPrincipal();
		DiagnosticosVO diagnosticoVOPrincipal;
		try {
			if (diagnosticoPrincipal != null) {
				ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
				List<DiagnosticosVO> lDiagnosticosVO = consultarSolicitudController
						.consultaDiagnosticosxCodigo(diagnosticoPrincipal.getCodigo());
				if (lDiagnosticosVO != null && !lDiagnosticosVO.isEmpty()) {
					diagnosticoVOPrincipal = lDiagnosticosVO.get(0);
					diagnosticoPrincipal.setDiagnosticosVO(diagnosticoVOPrincipal);
					listaContingenciaRecobroVOs = consultarSolicitudController.consultaRelContingenciasRecobros(
							diagnosticoVOPrincipal.getConsecutivoCodigoContingencia());
					obtenerConsecutivoContingenciaRecobro();
					listaContingenciaRecobros = consultarSolicitudController
							.obtenerListaSelectRelContingenciasRecobros(listaContingenciaRecobroVOs);
				} else {
					throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS),
							LogicException.ErrorType.DATO_NO_EXISTE);
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS),
						LogicException.ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	private void obtenerConsecutivoContingenciaRecobro() {
		for (ContingenciaRecobroVO cr : listaContingenciaRecobroVOs) {
			if (detalleSolicitudVO.getDescripcionRecobro().equals(cr.getDescripcionRecobro()))
				consecutivoContingenciaRecobro = cr.getConsecutivoCodigoRecobro();
		}
	}

	/**
	 * 
	 * @return
	 */
	private DiagnosticoDTO obtenerDiagnosticoPrincipal() {
		DiagnosticoDTO diagnosticoPrincipal = null;
		for (DiagnosticoDTO diagnosticoVar : listaDiagnosticoDTO) {
			if (diagnosticoVar.getPrincipal().equals(ConstantesWeb.DIAGNOSTICO_PPAL))
				diagnosticoPrincipal = diagnosticoVar;
		}
		return diagnosticoPrincipal;
	}

	private void validarConceptosGastos() {
		for (ConceptosGastoVO vo : listaConceptosGastoVO) {
			if (!ConstantesWeb.COD_ESTADO_ANULADO.equals(vo.getConsecutivoEstadoServicioSolicitado())
					&& permisoAnularSolicitudOPS) {				
				vo.setVerAnular(true);
				if (!ConstantesWeb.MEDIO_CONTACTOS_PROG_ENTREGA.equals(medioContacto)
						&& ConstantesWeb.COD_ESTADO_LIQUIDADO.equals(vo.getConsecutivoEstadoServicioSolicitado())
						|| ConstantesWeb.COD_ESTADO_APROBADO.equals(vo.getConsecutivoEstadoServicioSolicitado())
						|| ConstantesWeb.COD_ESTADO_AUTORIZADO.equals(vo.getConsecutivoEstadoServicioSolicitado())) {

					vo.setVerModificar(true);
				} else {
					vo.setVerModificar(false);
				}

				if (ConstantesWeb.COD_ESTADO_COBRADO.equals(vo.getConsecutivoEstadoServicioSolicitado())) {
					mostrarBtnAnular = true;
					vo.setVerAnular(false);
				}

			} else {
				vo.setVerAnular(false);
				vo.setVerModificar(false);
			}
		}
	}

	private void consultarDocumentosSoporte() {
		ControladorVisosServiceEJB controladorVisosServiceEJB;
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			List<DocumentoSoporteVO> listaIdDocumentos = consultarSolicitudController.consultaDocumentosSoportexSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			controladorVisosServiceEJB = new ControladorVisosServiceEJB();
			controladorVisosServiceEJB.crearConexionVisosService();
			if (listaIdDocumentos != null && !listaIdDocumentos.isEmpty()) {
				listaDocumentoSoporteVO = controladorVisosServiceEJB.consultarDocumentoAnexoxIdDocumento(
						listaIdDocumentos.get(0).getConsecutivoDocumentoSoporteVisos());
			}
			for (DocumentoSoporteVO documentoSoporteVO : listaDocumentoSoporteVO) {
				for (DocumentoSoporteVO documentoSoporteVO2 : listaIdDocumentos) {
					if (documentoSoporteVO.getConsecutivoDocumento()
							.compareTo(documentoSoporteVO2.getConsecutivoDocumento()) == 0) {
						documentoSoporteVO.setDescripcionTipoDocumentoSoporte(
								documentoSoporteVO2.getDescripcionTipoDocumentoSoporte());
						break;
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public void consultarMalla(){
		try {			
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaInconsistenciasMallaService = consultarSolicitudController.consultarResultadoMallayGestionAuditor(solicitudSeleccionada.getNumeroSolicitud());		
			mostrarPopupRespuestaMalla = true;
			
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);			
		}
	}

	public void consultarHistoricoDescargas() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaHistoricoFormatosVO = consultarSolicitudController.consultaHistoricoDescarga(solicitudSeleccionada.getNumeroSolicitud());
			mostrarPopupHistoricoDescargas = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void consultarHistoricoModificaciones() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaHistoricoModificacionVO = consultarSolicitudController.consultaHistoricoModificaciones(solicitudSeleccionada.getNumeroSolicitud());
			mostrarPopupHistoricoModificacion = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private boolean afiliadoSinDerechos() {
		return ConstantesEJB.getCodigosEstadosSinDerecho().contains(solicitudSeleccionada.getAfiliadoVO().getConsecutivoEstadoAfiliado());
	}

	private void consultarEstadoDerecho() throws LogicException {
		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
		EstadoDerechoVO estadoDerechoVO = consultarSolicitudController.validacionEstadoDerecho(solicitudSeleccionada.getAfiliadoVO().getNumeroUnicoIdentificacion(), 
				                                                                               fechaActual
				                                                                              );
		solicitudSeleccionada.getAfiliadoVO().setConsecutivoEstadoAfiliado(estadoDerechoVO.getConsecutivoCodigoEstadoDerecho());
	}

	private void validarOPSDescarga() throws LogicException {

		GeneracionAutorizacionServicioController generacionAutorizacionServicioController;
		try {
			generacionAutorizacionServicioController = new GeneracionAutorizacionServicioController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}

		listaAutorizacionServicioVO = generacionAutorizacionServicioController.consultaVigenciaEstadoOPSWeb(
				solicitudSeleccionada.getNumeroSolicitud(), prestacionSeleccionada.getConsecutivoServicioSolicitado());
		if (listaAutorizacionServicioVO != null && !listaAutorizacionServicioVO.isEmpty()) {
			if (ConstantesWeb.COD_ESTADO_ENTREGADA.equals(prestacionSeleccionada.getConsecutivoEstado())) {
				if (!ConstantesWeb.COD_ESTADO_ENTREGADA.equals(listaAutorizacionServicioVO.get(0).getConsecutivoCodigoEstadoOPS())) {
					throw new LogicException(
							Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_OPS_ESTADO_FECHA),
							ErrorType.PARAMETRO_ERRADO);
				}
				if (Utilidades.esfechaMenorActual(fechaActual, listaAutorizacionServicioVO.get(0).getFechaVencimientoHolgura())) {
					throw new LogicException(
							Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_OPS_FUERA_VIGENCIA),
							ErrorType.PARAMETRO_ERRADO);
				}
				mostrarPopupGenerarOPS = true;
			} else {
				mostrarPopupGenerarOPS = true;
			}
		}
	}

	public void validarAccesoDirecto() {
		iniciarCajaBean();
		if (ingresoSolicitudBean.validarMarcaAccesoDirecto(prestacionSeleccionada.getConsecutivoServicioSolicitado())) {
			if (!ingresoSolicitudBean.esPlanPOS(solicitudSeleccionada.getAfiliadoVO().getPlan().getConsectivoPlan())) {
				if (!ingresoSolicitudBean.excedeSumaPrestacionesSMLV()) {
					visualizarPopupAccesoDirecto = true;
					return;
				} else {
					mostrarReciboCaja();
				}
			} else {
				visualizarPopupAccesoDirecto = true;
				return;
			}
		} else {
			mostrarReciboCaja();
		}

	}

	public void validarDescargaOPS() {		
			visualizarPopupValidarAccesoDirecto = false;
			visualizarPopupAccesoDirecto = false;
			mostrarReciboCaja();			
	}

	/**
	 * 
	 * @return
	 * @throws LogicException
	 */
	public boolean validarPerfilCoordinador() throws LogicException {
		RegistrarSolicitudController registrarSolicitudesController;
		try {
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		return registrarSolicitudesController.validarPerfilCordinador(usuarioDescarga);
	}
	

	public void devolverServicioSolicitado() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			registrarSolicitudesController.devolverSevicioSolicitado(solicitudSeleccionada.getNumeroSolicitud(),
					solicitudSeleccionada.getAfiliadoVO().getPlan().getConsectivoPlan(), FacesUtils.getUserName());

			mostrarPopupGenerarOPS = false;
			visualizarPopupAccesoDirecto = false;
			mostrarPopupGenerarDevolucion = false;
			mostrarPopupGenerarNegacion = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.PRESTACION_ACCESO_DIRECTO_DEVUELTAS) + ' '
					+ solicitudSeleccionada.getNumeroSolicitud(), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * 
	 */
	public void descargar() {
		try {
			if (prestacionSeleccionada.isValidacionConceptoCero() == true) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO),
						ErrorType.PARAMETRO_ERRADO);
			}

			if (ConstantesWeb.COD_ESTADO_AUTORIZADO.equals(prestacionSeleccionada.getConsecutivoEstado())|| 
				ConstantesWeb.COD_ESTADO_ENTREGADA.equals(prestacionSeleccionada.getConsecutivoEstado())) 
			{
				consultarEstadoDerecho(); 
				
				if (afiliadoSinDerechos()) {
					validacionEspecial();
				}
				
				validarOPSDescarga();
				
			} else if (ConstantesWeb.COD_ESTADO_DEVUELTO.equals(prestacionSeleccionada.getConsecutivoEstado())) {
				mostrarPopupGenerarDevolucion = true;
			} else if (ConstantesWeb.COD_ESTADO_NO_AUTORIZADO.equals(prestacionSeleccionada.getConsecutivoEstado())) {
				mostrarPopupGenerarNegacion = true;
			}

		} catch (LogicException e) {
			mostrarPopupGenerarOPS = false;
			mostrarPopupGenerarDevolucion = false;
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			mostrarPopupGenerarOPS = false;
			mostrarPopupGenerarDevolucion = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo que se utiliza para la generacion del reporte de devolucion
	 * 
	 * @throws LogicException
	 */
	public void generarReporteDevolucion() {
		try {

			validarFechaDescargaDevolucion();
			GenerarReporteBean generarReporteBean = new GenerarReporteBean();
			List<PrestacionDTO> lstPrestacionDTO = new ArrayList<PrestacionDTO>();
			lstPrestacionDTO.add(prestacionSeleccionada);
			mostrarPopupGenerarDevolucion = false;
			generarReporteBean.formatoDevolucion(solicitudSeleccionada.getNumeroSolicitud(),
					solicitudSeleccionada.getAfiliadoVO().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion(),
					solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion(),
					solicitudSeleccionada.getNumeroRadicado(), lstPrestacionDTO);
		} catch (LogicException e) {
			mostrarPopupGenerarOPS = false;
			mostrarPopupGenerarDevolucion = false;
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			mostrarPopupGenerarOPS = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	private void validarFechaDescargaDevolucion() throws LogicException {
		Date fechaDescarga = null;

		ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();

		listaHistoricoFormatosVO = consultarSolicitudController
				.consultaHistoricoDescargaFormatos(solicitudSeleccionada.getNumeroSolicitud());

		for (HistoricoFormatosVO vo : listaHistoricoFormatosVO) {
			if (vo.getConsecutivoPrestacion().equals(prestacionSeleccionada.getConsecutivoServicioSolicitado())) {
				fechaDescarga = vo.getFechaDescargaFormato();
				break;
			}
		}
		if (fechaDescarga != null) {
			if (Utilidades.esfechaMayorNMesesActual(fechaActual, fechaDescarga, numMaximoMesesDescarga)) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_VENCIDA),
						ErrorType.PARAMETRO_ERRADO);
			}
		}
	}

	/**
	 * Metodo que se utiliza para la generacion del reporte de devolucion
	 * 
	 * @throws LogicException
	 */
	public void generarReporteNegacion() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaHistoricoFormatosVO = consultarSolicitudController
					.consultaHistoricoDescargaFormatos(solicitudSeleccionada.getNumeroSolicitud());
			for (HistoricoFormatosVO vo : listaHistoricoFormatosVO) {
				if (vo.getConsecutivoPrestacion().equals(prestacionSeleccionada.getConsecutivoServicioSolicitado())) {
					throw new LogicException(
							Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION),
							ErrorType.PARAMETRO_ERRADO);
				}
			}

			GenerarReporteBean generarReporteBean = new GenerarReporteBean();
			List<PrestacionDTO> lstPrestacionDTO = new ArrayList<PrestacionDTO>();
			lstPrestacionDTO.add(prestacionSeleccionada);

			generarReporteBean.formatoNegacionSolicitud(solicitudSeleccionada.getNumeroSolicitud(),
					solicitudSeleccionada.getAfiliadoVO().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion(),
					solicitudSeleccionada.getAfiliadoVO().getNumeroIdentificacion(),
					solicitudSeleccionada.getNumeroRadicado(), lstPrestacionDTO);
		} catch (LogicException e) {
			mostrarPopupGenerarNegacion = false;
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			mostrarPopupGenerarNegacion = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	/**
	 * Metodo que se utiliza para la generacion del reporte de la OPS
	 * 
	 * @throws LogicException
	 */
	public void generarOPS() throws LogicException {
		try {

			GenerarReporteBean generarReporteBean = new GenerarReporteBean();
			List<PrestacionDTO> lstPrestacionDTO = new ArrayList<PrestacionDTO>();
			lstPrestacionDTO.add(prestacionSeleccionada);

			generarReporteBean.formatoOPSConsulta(solicitudSeleccionada.getAfiliadoVO(),
					solicitudSeleccionada.getNumeroRadicado(), solicitudSeleccionada.getNumeroSolicitud(),
					lstPrestacionDTO, listaAutorizacionServicioVO, generarConObservaciones);
			guardarLogAuditoriaValEspecial();
			consultarSolicitudes();
			mostrarPopupGenerarOPS = false;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo cerrar popup generar OPS solicitud */
	public void cerrarMostrarPopupGenerarOPS() {
		mostrarPopupGenerarOPS = false;
	}

	/**
	 * Guardar log de auditoria si hizo validacion especial
	 * 
	 * @throws LogicException
	 */
	private void guardarLogAuditoriaValEspecial() throws LogicException {
		if (valEspecial) {
			GeneracionAutorizacionServicioController generacionAutorizacionServicioController = new GeneracionAutorizacionServicioController();
			validacionEspecialVO.setAfiliadoVO(solicitudSeleccionada.getAfiliadoVO());
			validacionEspecialVO.setConsecutivoSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			for (AutorizacionServicioVO autorizacionServicioVO : listaAutorizacionServicioVOs) {
				validacionEspecialVO.setNumeroUnicoAutorizacion(autorizacionServicioVO.getNumeroUnicoAutorizacion());
				generacionAutorizacionServicioController.registrarValidacionEspecialAfiliado(validacionEspecialVO,
						((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(),
						autorizacionServicioVO.getNumeroUnicoAutorizacion()
								+ ConstantesWeb.ACCION_EJECUTADA_DESCARGA_OPS);
			}

		}
	}

	/**
	 * 
	 */
	public void descargarSoporte() {

		ExternalContext extCont = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse httpSerRes = (HttpServletResponse) extCont.getResponse();
		httpSerRes.setHeader(ConstantesWeb.CONTENT_DISPOSITION,
				ConstantesWeb.ATTACHMENT_FILENAME + documentoSoporteVOSeleccionado.getNombreDocumento());
		httpSerRes.setHeader(ConstantesWeb.CACHE_CONTROL,
				ConstantesWeb.MUST_REVALIDATE + "," + ConstantesWeb.POST_CHECK + "," + ConstantesWeb.PRE_CHECK);

		httpSerRes.setContentLength((int) documentoSoporteVOSeleccionado.getDatosArchivoSoporte().length);
		try {
			ServletOutputStream serOutStream = httpSerRes.getOutputStream();
			serOutStream.write(documentoSoporteVOSeleccionado.getDatosArchivoSoporte(), 0,
					(int) documentoSoporteVOSeleccionado.getDatosArchivoSoporte().length);
			serOutStream.flush();
			serOutStream.close();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

		FacesContext.getCurrentInstance().responseComplete();

	}

	/** Metodo abrir popup anular solicitud */
	public void abrirPopupAnularSolicitud() {
		mostrarPopupAnularSolicitud = true;
		causalAnulacionSolicitud = null;
		obsAnulacionSolicitud = null;

	}

	/** Metodo abrir popup anular solicitud */
	public void cerrarPopupAnularSolicitud() {
		mostrarPopupAnularSolicitud = false;
		causalAnulacionSolicitud = null;
		obsAnulacionSolicitud = null;
	}

	/** Metodo abrir popup modificar Fecha Entrega */
	public void abrirPopupModificarFechaEntrega() {
		consultaPrestacionesAprobadasModFechaEntrega();
	}

	/** Metodo cerrar popup modificar Fecha Entrega */
	public void cerrarPopupModificarFechaEntrega() {
		mostrarPopupModFechaEntrega = false;
		obsModificarFechaEntrega = null;
		causalModFechaEntrega = null;
	}

	/*
	 * Metodo para refactorizar contenido de procedimientos
	 * abrirPopupModificarFechaEntrega y abrirPopupReliquidar
	 */
	public void consultaPrestacionesAprobadasModFechaEntrega() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			listaPrestacionFechaEntrega = consultarSolicitudController.consultaPrestacionesAprobadasModFechaEntrega(
					solicitudSeleccionada.getNumeroSolicitud(), solicitudSeleccionada.getNumeroRadicado());
			mostrarPopupModFechaEntrega = true;
			obsModificarFechaEntrega = null;
			causalModFechaEntrega = null;

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo abrir popup Reliquidar */
	public void abrirPopupReliquidar() {
		consultaPrestacionesReliquidar();
	}

	/** Metodo cerrar popup Reliquidar */
	public void cerrarPopupReliquidar() {
		mostrarPopupReliquidar = false;
		obsReliquidar = null;
		causalReliquidar = null;
	}

	public void consultaPrestacionesReliquidar() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			consultarSolicitudController.consultaEstadoPrestReliquidar(solicitudSeleccionada.getNumeroSolicitud());
			mostrarPopupReliquidar = true;
			obsReliquidar = null;
			causalReliquidar = null;

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo abrir popup anular OPS */
	public void abrirPopupAnularOPS() {
		mostrarPopupAnularOPS = true;
		causalAnulacionOPS = null;
		obsAnulacionOPS = null;

	}

	/** Metodo abrir popup anular OPS */
	public void cerrarPopupAnularOPS() {
		mostrarPopupAnularOPS = false;
		causalAnulacionOPS = null;
		obsAnulacionOPS = null;
	}

	/** Metodo abrir popup autorizar prestaciones */
	public void abrirPopupAutorizarNegacion() {
		mostrarPopupAutorizarNegacion = true;

		numeroAutorizacionPrestacionIPS = null;
		causalAutorizacionPrestacion = null;
		obsDireccionamientoAutorizacion = null;
		codInternoPrestadorNuevo = null;
		habilitaGestionNumeroAutorizacionIPS = true;

		try {
			lstPrestadoresDireccionamiento.clear();
			List<PrestadorVO> listaPrestadores;
			PrestadorController prestadorController = new PrestadorController();
			listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(
					prestacionSeleccionada.getConsecutivoCodificacionPrestacion(),
					solicitudSeleccionada.getAfiliadoVO().getPlan().getConsectivoPlan(),
					solicitudSeleccionada.getAfiliadoVO().getCiudadResidencia().getConsecutivoCodigoCiudad(),
					solicitudSeleccionada.getNumeroSolicitud());
			lstPrestadoresDireccionamiento.addAll(listaPrestadores);
			listaMotivosCausaVo = ConsultaSolicitudesUtil
					.obtenerMotivosCausa(ConstantesWeb.CONSECUTIVO_TIPO_MOTIVO_CAUSA_PRESTACIONES_PAFPOC);
			
			if (prestacionSeleccionada.isHabilitaGestionDatosAutorizacion()) {
				ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
				GestionAutorizacionVO gestionAutorizacionVO = consultarSolicitudController
						.obtenerGestionAutorizacionPrestacionNegada(solicitudSeleccionada.getNumeroSolicitud(),
								prestacionSeleccionada.getConsecutivoServicioSolicitado());
				if (gestionAutorizacionVO != null) {
					numeroAutorizacionPrestacionIPS = gestionAutorizacionVO.getNumeroAtorizacionPrestacionIPS();
					causalAutorizacionPrestacion = gestionAutorizacionVO.getConsecutivoCodigoCausaNovedad();
					obsDireccionamientoAutorizacion = gestionAutorizacionVO.getObservaciones();
					codInternoPrestadorNuevo = gestionAutorizacionVO.getCodigoInterno();
					habilitaGestionNumeroAutorizacionIPS = true;
					seleccionarPrestadorDir();
				}
			} else {
				if (habilitaDatosAutorizaPrestacion) {
					prestacionSeleccionada.setHabilitaGestionDatosAutorizacion(true);
				} else {
					prestacionSeleccionada.setHabilitaGestionDatosAutorizacion(false);
				}
			}

			if (lstPrestadoresDireccionamiento.isEmpty()) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR),
						ErrorType.PARAMETRO_ERRADO);
			}
			
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			lstPrestacionesOPS = consultarSolicitudController.consultarPrestaciones(solicitudSeleccionada.getNumeroSolicitud());
			prestacionSeleccionada = lstPrestacionesOPS.get(obtenerPrestacionSeleccionada());
			lstPrestacionesOPS.clear();
			lstPrestacionesOPS.add(prestacionSeleccionada);
			obtenerPrestadorOPS();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);

		}
	}

	/**
	 * Metodo para validar si el motivo seleccionado requiere numero
	 * autorización IPS
	 */
	public void validaMotivoCausasNumeroAutorizacionIPS() {
		habilitaGestionNumeroAutorizacionIPS = true;

		if (listaMotivosCausaVo != null && !listaMotivosCausaVo.isEmpty()) {
			for (MotivoCausaVO vo : listaMotivosCausaVo) {
				if (vo.getConsecutivoCodMotivoCausa().equals(causalAutorizacionPrestacion)
						&& vo.isRequiereNumeroAutorizacionIps()) {
					habilitaGestionNumeroAutorizacionIPS = false;
					break;
				}
			}
		}
		numeroAutorizacionPrestacionIPS = null;
	}

	
	/**
	 * Metodo que verifica si la lista de las prestaciones asociadas a un concepto es null
	 * o vacia. Si es null crea la instancia, si es vacia la limpia.
	 */
	private void limpiarListaPrestaciones() {
		if(lstPrestacionesOPS == null) {
			lstPrestacionesOPS = new ArrayList<PrestacionDTO>();
		}else {
			lstPrestacionesOPS.clear();
		}
	}
	
	/**  Metodo abrir popup modificar OPS  */
	public void abrirPopupModificarOPS() {
		modificarDireccionamientoConOPS = false;
		cambiarDireccionamientoSinOPS = false;
		limpiarListaPrestaciones();
		try {
			int consecutivoInt = (conceptosGastoVOSel.getNumeroUnicoAutorizacionOps() == null? ConstantesWeb.VALOR_CERO: 
									conceptosGastoVOSel.getNumeroUnicoAutorizacionOps().intValue());
			if(ConstantesWeb.VALOR_CERO == consecutivoInt) {
				lstPrestacionesOPS = obtenerPrestacionConcepto(conceptosGastoVOSel);
				if(lstPrestacionesOPS == null || lstPrestacionesOPS.isEmpty()) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_PRESTACION_CONCEPTO_NO_ENCONTRADA), ErrorType.PARAMETRO_ERRADO);
				}				
				if(tieneOPSConceptoPrestacion(lstPrestacionesOPS.get(0))) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_DEBE_RELIQUIDAR), ErrorType.PARAMETRO_ERRADO);
				} else {
					cambiarDireccionamientoSinOPS = true;
				}
			}else {
				modificarDireccionamientoConOPS = true;
			}
			mostrarPopupModificarOPS = true;
		}catch(LogicException e) {
			mostrarPopupModificarOPS = false;
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para cerrar popup Acceso directo */
	public void cerrarPopupAccesoDirecto() {
		visualizarPopupAccesoDirecto = false;
	}

	/** Metodo abrir popup modificar OPS */
	public void cerrarPopupModificarOPS() {
		mostrarPopupModificarOPS = false;
	}

	/** Metodo abrir popup cambiar direccionamiento OPS */
	public void abrirPopupCambiarDirOPS() {
		try {

			codInternoPrestadorNuevo = null;
			causalDireccionamientoOPS = null;
			obsDireccionamientoOPS = null;
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			lstPrestacionesOPS = consultarSolicitudController.consultarPrestacionesOPS(solicitudSeleccionada.getNumeroSolicitud(), conceptosGastoVOSel.getNumeroUnicoAutorizacionOps());
			prestadorOPS = consultarSolicitudController.consultarPrestadorCodigoInterno(conceptosGastoVOSel.getCodigoInterno(), ConstantesWeb.CONSULTA_PRESTADOR_COD_INTERNO);

			List<PrestadorVO> listaPrestadores;
			lstPrestadoresDireccionamiento.clear();
			PrestadorController prestadorController = new PrestadorController();
			for (PrestacionDTO dto : lstPrestacionesOPS) {
				listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(
						dto.getConsecutivoCodificacionPrestacion(), dto.getConsecutivoPlan(),
						dto.getConsecutivoCiudad(), solicitudSeleccionada.getNumeroSolicitud());
				lstPrestadoresDireccionamiento.addAll(listaPrestadores);
			}

			if (lstPrestadoresDireccionamiento.isEmpty()) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR),
						ErrorType.PARAMETRO_ERRADO);
			}

			mostrarPopupcambiarDirOPS = true;
			mostrarPopupModificarOPS = false;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	public void abrirPopupCambiarDireccionamientoPrestacion() {
		codInternoPrestadorNuevo = null;
		causalDireccionamientoOPS = null;
		obsDireccionamientoOPS = null;
		try {			
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			PrestadorController prestadorController = new PrestadorController();
			lstPrestacionesOPS = consultarSolicitudController.consultarPrestaciones(solicitudSeleccionada.getNumeroSolicitud());
			prestacionSeleccionada = lstPrestacionesOPS.get(obtenerPrestacionSeleccionada());
			lstPrestacionesOPS.clear();
			lstPrestacionesOPS.add(prestacionSeleccionada);
			obtenerPrestadorOPS();			
			lstPrestadoresDireccionamiento = prestadorController.consultarPrestadoresDireccionamiento(prestacionSeleccionada.getConsecutivoCodificacionPrestacion(), 
																						prestacionSeleccionada.getConsecutivoPlan(), 
																						prestacionSeleccionada.getConsecutivoCiudad(),
																		                solicitudSeleccionada.getNumeroSolicitud());
			if (lstPrestadoresDireccionamiento.isEmpty()) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR), ErrorType.PARAMETRO_ERRADO);
			}
			mostrarPopupcambiarDirSinOPS = true;
			cambiarDireccionamientoSinOPS = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	private int obtenerPrestacionSeleccionada() {
		int indice = ConstantesWeb.VALOR_CERO;
		for(PrestacionDTO prestacion: lstPrestacionesOPS) {
			if(prestacion.getConsecutivoServicioSolicitado().intValue() == prestacionSeleccionada.getConsecutivoServicioSolicitado().intValue()) {
				indice = lstPrestacionesOPS.indexOf(prestacion);
			}
		}
		return indice;
	}
	
	
	private void obtenerPrestadorOPS() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			for(ConceptosGastoVO concepto: listaConceptosGastoVO) {
				if(concepto.getConsecutivoCodigoServicioSolicitado().intValue() == prestacionSeleccionada.getConsecutivoCodificacionPrestacion().intValue()) {
					prestadorOPS = consultarSolicitudController.consultarPrestadorCodigoInterno(concepto.getCodigoInterno(), ConstantesWeb.CONSULTA_PRESTADOR_COD_INTERNO);
				}
				continue;
			}
		}catch(LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}
	
	/**
	 * Metodo usado para establecer los valores de las variables y datos requeridos para mostrar el popup de cambio de direccionamiento
	 * cuando la prestacion no tiene conceptos con numero de ops
	 */
	public void abrirPopupCambiarDireccionamientoConceptosSinOPS() {
		try {
			codInternoPrestadorNuevo = null;
			causalDireccionamientoOPS = null;
			obsDireccionamientoOPS = null;
			ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
			prestadorOPS = consultarSolicitudController.consultarPrestadorCodigoInterno(conceptosGastoVOSel.getCodigoInterno(), ConstantesWeb.CONSULTA_PRESTADOR_COD_INTERNO);
			List<PrestadorVO> listaPrestadores;
			lstPrestadoresDireccionamiento.clear();
			PrestadorController prestadorController = new PrestadorController();
			for (PrestacionDTO dto : lstPrestacionesOPS) {
				listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(dto.getConsecutivoCodificacionPrestacion(), 
						                                                                    dto.getConsecutivoPlan(), 
						                                                                    dto.getConsecutivoCiudad(),
						                                                                    solicitudSeleccionada.getNumeroSolicitud());
				lstPrestadoresDireccionamiento.addAll(listaPrestadores);
			}

			if (lstPrestadoresDireccionamiento.isEmpty()) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_MSG_NO_EXISTE_CONVENIO_PRESTADOR), ErrorType.PARAMETRO_ERRADO);
			}

			mostrarPopupcambiarDirSinOPS = true;
			mostrarPopupModificarOPS = false;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * Metodo que busca la prestacion a la que pertenece un concepto
	 * @return
	 */
	private List<PrestacionDTO> obtenerPrestacionConcepto(ConceptosGastoVO concepto) {
		List<PrestacionDTO>	listaPrestacionesConcepto = new ArrayList<PrestacionDTO>();
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			lstPrestacionesOPS = consultarSolicitudController.consultarPrestaciones(solicitudSeleccionada.getNumeroSolicitud());		
			for(PrestacionDTO prestacionConcepto: lstPrestacionesOPS) {
				if(concepto.getConsecutivoCodigoServicioSolicitado().intValue() == prestacionConcepto.getConsecutivoCodificacionPrestacion().intValue()) {
					listaPrestacionesConcepto.add(prestacionConcepto);
				}
			}
		}catch(LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return listaPrestacionesConcepto;
	}
	
	private boolean tieneOPSConceptoPrestacion(PrestacionDTO prestacion) {
		boolean tieneOPS = false;
		for(ConceptosGastoVO cgVO: listaConceptosGastoVO) {
			if(cgVO.getConsecutivoCodigoServicioSolicitado().intValue() == prestacion.getConsecutivoCodificacionPrestacion().intValue()) {
				int consecutivoInt = (cgVO.getNumeroUnicoAutorizacionOps()== null? ConstantesWeb.VALOR_CERO: cgVO.getNumeroUnicoAutorizacionOps().intValue());
				if(ConstantesWeb.VALOR_CERO != consecutivoInt) {
					tieneOPS = true;
				}
			}
		}
		return tieneOPS;
	}

	
	/**
	 * Metodo para anular la solicitud
	 */
	public void anularSolicitud() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			ConsultaSolicitudesUtil.validacionCausalAnulacionSolicitud(causalAnulacionSolicitud, obsAnulacionSolicitud);
			consultarSolicitudController.anularSolicitud(solicitudSeleccionada, listaPrestacionDTO,
					causalAnulacionSolicitud, obsAnulacionSolicitud,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(), ConstantesWeb.ORIGEN_ANULACION_SOLICITUD);

			if (solicitudSeleccionada.getIdInstanciaProcesoBPM() != null
					&& solicitudSeleccionada.getIdInstanciaProcesoBPM().intValue() != ConstantesWeb.VALOR_CERO) {
				clienteRestBpmBean.anularInstanciaWeb(solicitudSeleccionada.getIdInstanciaProcesoBPM().toString());
			}
			for (ConceptosGastoVO vo : listaConceptosGastoVO) {
				vo.setVerAnular(false);
				vo.setVerModificar(false);
			}
			mostrarBtnAnular = true;
			seleccionSolicitud();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_ANULACION_SOLICITUD,
					solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);
			mostrarPopupAnularSolicitud = false;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	

	public void seleccionarPrestadorDir() {
		for (PrestadorVO vo : lstPrestadoresDireccionamiento) {
			vo.setNuevoCodigoInterno(null);
			if (vo.getCodigoInterno().equals(codInternoPrestadorNuevo)) {
				vo.setNuevoCodigoInterno(ConstantesWeb.EQUIVALENTE_VERDADERO);
			}
		}
	}

	/**
	 * Metodo para guardar cambios de la OPS
	 */
	public void guardarCambioDireccionamientoOPS() {
		mostrarConfirmarCambiarDireccionamientoConcepto = false;
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			ConsultaSolicitudesUtil.validarObligatoriosCambioDireccionamientoOPS(prestadorOPS, codInternoPrestadorNuevo,obsDireccionamientoOPS, causalDireccionamientoOPS);
			consultarSolicitudController.guardarModificacionesOPS(solicitudSeleccionada.getNumeroSolicitud(),conceptosGastoVOSel.getNumeroUnicoAutorizacionOps(), codInternoPrestadorNuevo,causalDireccionamientoOPS, obsDireccionamientoOPS,((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_CAMBIO_DIRECCIONAMIENTO_OPS,conceptosGastoVOSel.getNumeroUnicoAutorizacionOps().toString()), ConstantesWeb.COLOR_AZUL);
			mostrarPopupcambiarDirOPS = false;
			consultarConceptosGasto(consultarSolicitudController);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Guarda el cambio de direccionamiento de prestaciones sin numero de OPS
	 */
	public void guardarCambioDireccionamientoPrestacionesSinOps() {
		mostrarConfirmarCambiarDireccionamientoConcepto = false;
		try {
			ConsultaSolicitudesUtil.validarObligatoriosCambioDireccionamientoOPS(prestadorOPS, codInternoPrestadorNuevo, obsDireccionamientoOPS, causalDireccionamientoOPS);			
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			consultarSolicitudController.
					guardarCambioDireccionamientoSolicitudSinOPS(solicitudSeleccionada.getNumeroSolicitud(), lstPrestacionesOPS.get(0).getConsecutivoServicioSolicitado(), 
																	codInternoPrestadorNuevo,	causalDireccionamientoOPS, obsDireccionamientoOPS, 
																	((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			seleccionSolicitud();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_CAMBIO_DIRECCIONAMIENTO_PRESTACION_SIN_OP, 
					new String(lstPrestacionesOPS.get(0).getCodigoCodificacionPrestacion() + 
							ConstantesWeb.CADENA_EN_BLANCO + 
							lstPrestacionesOPS.get(0).getDescripcionCodificacionPrestacion())), 
					ConstantesWeb.COLOR_AZUL);
			mostrarPopupcambiarDirOPS = false;	
			cerrarPopupCambiarDirSinOPS();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Metodo para guardar la traza de la gestión autorización de las prestaciones negadas por la malla
	 * Metodo para guardar la traza de la gestión autorización de las
	 * prestaciones negadas por la malla
	 */
	public void guardarGestionAutorizacionPrestacionNegada() {
		int resultado = -1;
		try {
			mostrarPopupAutorizarNegacion = false;
			prestacionSeleccionada.setHabilitaGestionDatosAutorizacion(true);
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();

			ConsultaSolicitudesUtil.validarObligatoriosGestionAutorizacionPrestacionNegada(
					numeroAutorizacionPrestacionIPS, codInternoPrestadorNuevo, obsDireccionamientoAutorizacion,
					causalAutorizacionPrestacion, habilitaGestionNumeroAutorizacionIPS);
			resultado = consultarSolicitudController.guardarGestionAutorizacionPrestacionNegada(
					solicitudSeleccionada.getNumeroSolicitud(),
					prestacionSeleccionada.getConsecutivoServicioSolicitado(), causalAutorizacionPrestacion,
					obsDireccionamientoAutorizacion, numeroAutorizacionPrestacionIPS, codInternoPrestadorNuevo,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());

			ConsultaSolicitudesUtil.liquidarPrestacionesPaFPocCapita(solicitudSeleccionada.getNumeroSolicitud(),
					solicitudSeleccionada.getNumeroRadicado());

			if (resultado == ConstantesWeb.VALOR_CERO) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_GESTION_AUTORIZA_PRESTACION,
						prestacionSeleccionada.getCodigoCodificacionPrestacion()), ConstantesWeb.COLOR_AZUL);
			}

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_LIQUIDACION_PRESTACION), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	/**
	 * Metodo para guardar cambios de la Fecha de Entrega de la Solicitud
	 */
	public void guardarFechaEntregaModificada() {
		try {
			mostrarConfirmarModFecha = false;
			ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(obsModificarFechaEntrega, causalModFechaEntrega);
			ConsultaSolicitudesUtil.validarVacioModFechaEntrega(listaPrestacionFechaEntrega);
			ConsultaSolicitudesUtil.validarModFechaEntregaInvalida(listaPrestacionFechaEntrega, fechaActual);
			mostrarConfirmarModFecha = ConsultaSolicitudesUtil.isModFechaEntregaMayorParam(listaPrestacionFechaEntrega,
					fechaActual);
			if (!mostrarConfirmarModFecha) {
				modificarFecha();
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo para confirmar cambios de la Fecha de Entrega de la Solicitud
	 */
	public void modificarFecha() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			consultarSolicitudController.guardarCambioFechaEntrega(listaPrestacionFechaEntrega,
					solicitudSeleccionada.getNumeroSolicitud(), solicitudSeleccionada.getNumeroRadicado(),
					causalModFechaEntrega, obsModificarFechaEntrega, FacesUtils.getUserName());
			mostrarPopupModFechaEntrega = false;
			seleccionSolicitud();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_CAMBIO_FECHA_ENTREGA,
					solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);			
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo para guardar cambios de la Reliquidación
	 */
	public void guardarReliquidar() {
		try {
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(obsReliquidar, causalReliquidar);
			consultarSolicitudController.guardarReliquidacion(solicitudSeleccionada.getNumeroSolicitud(),
					causalReliquidar, obsReliquidar, FacesUtils.getUserName());
			ConsultaSolicitudesUtil.liquidarPrestacionModFechaEntrega(solicitudSeleccionada.getNumeroSolicitud(),
					solicitudSeleccionada.getNumeroRadicado());
			seleccionSolicitud();
			mostrarPopupReliquidar = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_RELIQUIDAR,
					solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);						
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Se encarga de validar si debe mostrar el recibo de caja o no.
	 * 
	 * @param event
	 *            Evento del faces
	 * @return void
	 */
	public String mostrarReciboCaja() {
		// Agregar valiaciones que dirijan a descarga a OPS
		if (ConstantesWeb.COD_ESTADO_AUTORIZADO.intValue() == prestacionSeleccionada.getConsecutivoEstado().intValue()) {
			this.generarReciboCaja();
		} else {
			this.descargar();
		}
		return null;
	}

	private void generarReciboCaja() {
		GenerarReciboBean reciboBean = iniciarCajaBean();
		reciboBean.setBeanSolicitud(ConstantesWeb.CONSULTA_SOLICITUD);
		reciboBean.setSolicitudSeleccionada(solicitudSeleccionada);

		OPSCajaVO opsCajas = reciboBean.getOpsCajas();

		boolean muestraMensajeCausales = mostrarMensajeCausalesNoPago(reciboBean);

		if (ConstantesWeb.SI.equalsIgnoreCase(opsCajas.getMuestraMensajeIPS())) {
			MostrarMensaje.mostrarMensaje(ConstantesWeb.CLAVE_MENSAJE_PAGO_IPS, COLOR_AZUL);
		}

		if (reciboBean.getListInformacion() == null || reciboBean.getListInformacion().isEmpty()) {
			this.descargar();
		} else {
			if ((muestraMensajeCausales && reciboBean.getListInformacion() != null
					&& reciboBean.getListInformacion().isEmpty())
					|| ConstantesWeb.NO.equalsIgnoreCase(opsCajas.getMuestraReciboCaja())) {
				this.descargar();
			} else {
				reciboBean.setMostrarModalReciboCaja(true);
				reciboBean.setTabSeleccionado(ConstantesWeb.TAB_GENERAR_RECIBO);
			}
		}
	}

	private GenerarReciboBean iniciarCajaBean() {
		GenerarReciboBean reciboBean = (GenerarReciboBean) FacesUtils.getManagedBean("generarReciboBean");
		if (reciboBean == null) {
			reciboBean = new GenerarReciboBean();
		}
		reciboBean.setMostrarModalReciboCaja(false);
		return reciboBean;
	}

	private boolean mostrarMensajeCausalesNoPago(GenerarReciboBean reciboBean) {
		boolean resp = false;
		String causales = reciboBean.getCausales();
		if (Utilidades.validarCamposVacios(causales)) {
			resp = true;
			MostrarMensaje.mostrarMensaje(ConstantesWeb.MENSAJE_CAUSAS_NO_PAGO + causales, COLOR_AZUL);
		}
		return resp;
	}

	public String descargarReciboCaja() {
		try {
			GenerarReciboBean cajaBean = iniciarCajaBean();
			List<ReciboCajaJasperVO> datosJasper = cajaBean.generarComprobanteReciboPorSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			if (datosJasper != null && !datosJasper.isEmpty()){
				GenerarReporteBean reporteBean = new GenerarReporteBean();
				reporteBean.generarReciboCaja(datosJasper, ConstantesWeb.NOMBRE_ARCHIVO_RECIBO_CAJA + new Date().getTime(), datosJasper.get(0).getFechaReporte());
			}else{
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_DESCARGA_RECIBO_CAJA), COLOR_ROJO);
			}
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return null;
	}
	
	public void activarModificarInformacionPrestacion() {
		ConsultarSolicitudController consultarSolicitudController;
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
			modificarInformacionPrestacion = true;		
			modificarInformacionRecobro = modificarInformacionPrestacion && prestacionSeleccionada.isHabilitarCambiarRecobro();
			permisoAnularSolicitudOPS = consultarSolicitudController.consultarPermisoAnularSolicitudOPS(
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			cambiarCausalesNoCobro = (modificarInformacionPrestacion && permisoAnularSolicitudOPS);
		}  catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * Metodo que realiza la busqueda de la ciudad de acuerdo al codigo o descripcion ingresada
	 */
	public void buscarCiudad() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupCiudades = false;
			listaCiudadesDireccionamiento = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudad, registrarSolicitudesController);			
			ciudad = new CiudadVO();
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesDireccionamiento)){
				mostrarPopupCiudades = true;
			}else{
				mostrarPopupCiudades = false;
				ciudad = listaCiudadesDireccionamiento.get(0);
			}	
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}	
	
	/**
	 * Cierra popup con listado de ciudades.
	 */
	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		
	}
	
	/**
	 * Metodo que recalcula los direccionamientos de acuerdo a la ciudad.
	 */
	public void recalcularDireccionamientos() {		
		try {
			int consecutivoCiudad = ciudad.getConsecutivoCodigoCiudad() == null? ConstantesWeb.VALOR_CERO: ciudad.getConsecutivoCodigoCiudad().intValue();
			if(consecutivoCiudad == ConstantesWeb.VALOR_CERO) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_BUSQUEDA_CIUDAD),
						ErrorType.PARAMETRO_ERRADO);
			}
			lstPrestadoresDireccionamiento.clear();
			PrestadorController prestadorController = new PrestadorController();
			for (PrestacionDTO dto : lstPrestacionesOPS) {
			List<PrestadorVO>	listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(dto.getConsecutivoCodificacionPrestacion(), 
						                                                                    dto.getConsecutivoPlan(), 
						                                                                    ciudad.getConsecutivoCodigoCiudad(),
						                                                                    solicitudSeleccionada.getNumeroSolicitud());
				lstPrestadoresDireccionamiento.addAll(listaPrestadores);
			}
			if(lstPrestadoresDireccionamiento == null || lstPrestadoresDireccionamiento.isEmpty()) {
				throw new  LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_PERSTADORES_NO_ENCONTRADOS),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}
	
		/** Metodo abrir popup modificar OPS */
	public void cerrarPopupAutorizarNegacion() {
		mostrarPopupAutorizarNegacion = false;
		activarFiltroCiudad = false;	
		ciudad = new CiudadVO();
	}
	
	/** Metodo abrir popup cambiar direccionamiento OPS */
	public void cerrarPopupCambiarDirOPS() {
		mostrarPopupcambiarDirOPS = false;
		activarFiltroCiudad = false;	
		ciudad = new CiudadVO();
	}
	
	
	public void cerrarPopupCambiarDirSinOPS() {
		cambiarDireccionamientoSinOPS = false;
		activarFiltroCiudad = false;	
		ciudad = new CiudadVO();
		
	}
	
	/**
	 * Cambia el estado de la variable que indica si el popup de anular prestacion se debe mostrar
	 */
	public void abrirPopupAnularPrestacion() {
		mostrarPopupAnularPrestacion = true;
	}
	
	/**
	 * Cambia el estado de la variable que indica si el popup de anular prestacion se debe ocultar
	 */
	public void cerrarPopupAnularPrestacion() {
		mostrarPopupAnularPrestacion = false;
	}
	

	public void anularPrestacion() {
		try {
			List<PrestacionDTO> prestacionesAnular = new ArrayList<PrestacionDTO>();
			prestacionesAnular.add(prestacionSeleccionada);
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			ConsultaSolicitudesUtil.validacionCausalAnulacionPrestacion(causalAnulacionPrestacion, 
					observacionAnulacionPrestacion, prestacionSeleccionada);
			consultarSolicitudController.anularSolicitud(solicitudSeleccionada, 
					prestacionesAnular,	causalAnulacionPrestacion, observacionAnulacionPrestacion,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(), ConstantesWeb.ORIGEN_ANULACION_PRESTACION);			
			listaPrestacionDTO = consultarSolicitudController
					.consultaInformacionPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			activarAnulacionPrestacion();		
			listaInformacionAdicionalPrestacionDTO = consultarSolicitudController.consultaInformacionAdicionalPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			seleccionSolicitud();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXITO_ANULACION_PRESTACION,
					prestacionSeleccionada.getCodigoCodificacionPrestacion(),
					prestacionSeleccionada.getDescripcionCodificacionPrestacion()), ConstantesWeb.COLOR_AZUL);
			mostrarPopupAnularPrestacion = false;
			mostrarBtnModificar = false;
			causalAnulacionPrestacion = ConstantesWeb.VALOR_CERO;
			observacionAnulacionPrestacion = "";
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void activarAnulacionPrestacion() {
		try{
			for(PrestacionDTO prestacion: listaPrestacionDTO) {
				boolean permitir = (ConstantesWeb.COD_ESTADO_ANULADO.intValue() != prestacion.getConsecutivoEstado().intValue())
						&& (ConstantesWeb.COD_ESTADO_USADA.intValue() != prestacion.getConsecutivoEstado().intValue())
						&& (ConstantesWeb.COD_ESTADO_PROCESADA.intValue() != prestacion.getConsecutivoEstado().intValue());
				permitir = permitir && ConstantesWeb.PERFIL_COORDINADOR.equalsIgnoreCase(perfil);
				prestacion.setHabilitarAnulacionPrestacion(permitir);			
			}	
		}catch(NullPointerException e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_ESTADO_NO_ENCONTRADO), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	public void cancelarModificarInformacionPrestacion() {
		modificarInformacionPrestacion = false;
		modificarInformacionRecobro = false;
		cambiarCausalesNoCobro = (modificarInformacionPrestacion && permisoAnularSolicitudOPS);
	}
	
	
	public void validarExistenciaReciboCaja() {
		ConsultarSolicitudController consultarSolicitudController;
		if(!prestacionSeleccionada.isNoCobroCuotaRecuperacion()) {
			return;
		}
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
			Integer resultadoValidacion = consultarSolicitudController.validarExistenciaReciboCaja(solicitudSeleccionada.getNumeroSolicitud(), prestacionSeleccionada.getConsecutivoServicioSolicitado());
			if(resultadoValidacion.intValue() > ConstantesWeb.VALOR_CERO) {
				prestacionSeleccionada.setNoCobroCuotaRecuperacion(false);
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_RECIBO_CAJA_EXISTENTE), ConstantesWeb.COLOR_ROJO);
			} else {
				prestacionSeleccionada.setNoCobroCuotaRecuperacion(true);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void activarFechaEntregaDireccionamiento() {
		try {			
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			detalleSolicitudVO = consultarSolicitudController.consultaDetalleSolicitud(solicitudSeleccionada.getNumeroRadicado()).get(0);
			listaPrestacionDTO = consultarSolicitudController.consultaInformacionPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			listaInformacionAdicionalPrestacionDTO = consultarSolicitudController.consultaInformacionAdicionalPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());			
			consultarConceptosGasto(consultarSolicitudController);
			medioContacto = consultarSolicitudController.removeDiacriticalMarks(detalleSolicitudVO.getDescripcionMedioContacto());
			SolicitudVO solicitudVO = new SolicitudVO();
			solicitudVO.setConsecutivoSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			solicitudVO.setNumeroSolicitudSOS(solicitudSeleccionada.getNumeroRadicado());	
			consultarPerfilUsuario();
			
			if (ConstantesWeb.PERFIL_ADMINISTRADOR_CNA.equals(perfil) && !ConstantesWeb.MEDIO_CONTACTOS_PROG_ENTREGA.equals(medioContacto)) {
				mostrarBtnModificarFechaEntrega = false;
			} else {
				mostrarBtnModificarFechaEntrega = true;
			}				
			activarCambiarDireccionamientoPrestacion();
			inactivarMenu(false);
		}catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void consultarPerfilUsuario() {
		ConsultarSolicitudController consultarSolicitudController;
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
			perfil = consultarSolicitudController.consultarPerfilUsuario(
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);			
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_PERFIL_USUARIO), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} 
		
	}
	
	private void inactivarMenu(boolean activo) {
		List<ModuloVO> modulos = ((MenuBean) FacesUtil.getBean(ConstantesWeb.MENU_BEAN)).getModulos();
		if (modulos != null) {
			for (ModuloVO moduloVO : modulos) {
				cambiarEstadoOpciones(activo, moduloVO);
			}
		}
	}
	
	private void cambiarEstadoOpciones(boolean activo, ModuloVO moduloVO) {
		for (ServicioVO servicioVO : moduloVO.getServicios()) {
			if (ConstantesWeb.OPCION_CAMBIAR_DIRECCIONAMIENTO.equals(servicioVO.getAction().trim()) 
					|| ConstantesWeb.OPCION_CAMBIAR_FECHA_ENTREGA.equals(servicioVO.getAction().trim())) {
				servicioVO.setDeshabilitado(activo);				
			}
				
		}
	}

	public List<SelectItem> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public void setListaTiposIdentificacion(List<SelectItem> listaTiposIdentificacion) {
		this.listaTiposIdentificacion = listaTiposIdentificacion;
	}

	public List<SelectItem> getListaPlanes() {
		return listaPlanes;
	}

	public void setListaPlanes(List<SelectItem> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}

	/**
	 * @return the consecutivoPlan
	 */
	public Integer getConsecutivoPlan() {
		return consecutivoPlan;
	}

	/**
	 * @param consecutivoPlan
	 *            the consecutivoPlan to set
	 */
	public void setConsecutivoPlan(Integer consecutivoPlan) {
		this.consecutivoPlan = consecutivoPlan;
	}

	/**
	 * @return the afiliadoVO
	 */
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	/**
	 * @param afiliadoVO
	 *            the afiliadoVO to set
	 */
	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public Date getFechaConsultaInicial() {
		return fechaConsultaInicial;
	}

	public void setFechaConsultaInicial(Date fechaConsultaInicial) {
		this.fechaConsultaInicial = fechaConsultaInicial;
	}

	public Date getFechaConsultaFinal() {
		return fechaConsultaFinal;
	}

	public void setFechaConsultaFinal(Date fechaConsultaFinal) {
		this.fechaConsultaFinal = fechaConsultaFinal;
	}

	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	public List<IPSVO> getListaIPSVO() {
		return listaIPSVO;
	}

	public void setListaIPSVO(List<IPSVO> listaIPSVO) {
		this.listaIPSVO = listaIPSVO;
	}

	public IPSVO getIpsVO() {
		return ipsVO;
	}

	public void setIpsVO(IPSVO ipsVO) {
		this.ipsVO = ipsVO;
	}

	public boolean isMostrarPopupPrestadores() {
		return mostrarPopupPrestadores;
	}

	public void setMostrarPopupPrestadores(boolean mostrarPopupPrestadores) {
		this.mostrarPopupPrestadores = mostrarPopupPrestadores;
	}

	public String getOpcionBusquedaSeleccionada() {
		return opcionBusquedaSeleccionada;
	}

	public void setOpcionBusquedaSeleccionada(String opcionBusquedaSeleccionada) {
		this.opcionBusquedaSeleccionada = opcionBusquedaSeleccionada;
	}

	public boolean isHabilitarFiltroFechas() {
		return habilitarFiltroFechas;
	}

	public void setHabilitarFiltroFechas(boolean habilitarFiltroFechas) {
		this.habilitarFiltroFechas = habilitarFiltroFechas;
	}

	public boolean isHabilitarFiltroRadicado() {
		return habilitarFiltroRadicado;
	}

	public void setHabilitarFiltroRadicado(boolean habilitarFiltroRadicado) {
		this.habilitarFiltroRadicado = habilitarFiltroRadicado;
	}

	public boolean isHabilitarFiltroSolicitud() {
		return habilitarFiltroSolicitud;
	}

	public void setHabilitarFiltroSolicitud(boolean habilitarFiltroSolicitud) {
		this.habilitarFiltroSolicitud = habilitarFiltroSolicitud;
	}

	public boolean isHabilitarFiltroAutorizacion() {
		return habilitarFiltroAutorizacion;
	}

	public void setHabilitarFiltroAutorizacion(boolean habilitarFiltroAutorizacion) {
		this.habilitarFiltroAutorizacion = habilitarFiltroAutorizacion;
	}

	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	public List<ProcedimientosVO> getListaCUPSEncontrados() {
		return listaCUPSEncontrados;
	}

	public void setListaCUPSEncontrados(List<ProcedimientosVO> listaCUPSEncontrados) {
		this.listaCUPSEncontrados = listaCUPSEncontrados;
	}

	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	public void setListaMedicamentosEncontrados(List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	public ProcedimientosVO getProcedimientoVOSeleccionado() {
		return procedimientoVOSeleccionado;
	}

	public void setProcedimientoVOSeleccionado(ProcedimientosVO procedimientoVOSeleccionado) {
		this.procedimientoVOSeleccionado = procedimientoVOSeleccionado;
	}

	public MedicamentosVO getMedicamentoVOSeleccionado() {
		return medicamentoVOSeleccionado;
	}

	public void setMedicamentoVOSeleccionado(MedicamentosVO medicamentoVOSeleccionado) {
		this.medicamentoVOSeleccionado = medicamentoVOSeleccionado;
	}

	public boolean isMostrarPopupPis() {
		return mostrarPopupPis;
	}

	public void setMostrarPopupPis(boolean mostrarPopupPis) {
		this.mostrarPopupPis = mostrarPopupPis;
	}

	public List<PrestacionPisVO> getListaPisEncontrados() {
		return listaPisEncontrados;
	}

	public void setListaPisEncontrados(List<PrestacionPisVO> listaPisEncontrados) {
		this.listaPisEncontrados = listaPisEncontrados;
	}

	public PrestacionPisVO getPisVOSeleccionado() {
		return pisVOSeleccionado;
	}

	public void setPisVOSeleccionado(PrestacionPisVO pisVOSeleccionado) {
		this.pisVOSeleccionado = pisVOSeleccionado;
	}

	public List<DatosSolicitudVO> getListaSolicitudes() {
		return listaSolicitudes;
	}

	public void setListaSolicitudes(List<DatosSolicitudVO> listaSolicitudes) {
		this.listaSolicitudes = listaSolicitudes;
	}

	public DatosSolicitudVO getSolicitudSeleccionada() {
		return solicitudSeleccionada;
	}

	public void setSolicitudSeleccionada(DatosSolicitudVO solicitudSeleccionada) {
		this.solicitudSeleccionada = solicitudSeleccionada;
	}

	/**
	 * @return the detalleSolicitudVO
	 */
	public DetalleSolicitudVO getDetalleSolicitudVO() {
		return detalleSolicitudVO;
	}

	/**
	 * @param detalleSolicitudVO
	 *            the detalleSolicitudVO to set
	 */
	public void setDetalleSolicitudVO(DetalleSolicitudVO detalleSolicitudVO) {
		this.detalleSolicitudVO = detalleSolicitudVO;
	}

	/**
	 * @return the listaDiagnosticoDTO
	 */
	public List<DiagnosticoDTO> getListaDiagnosticoDTO() {
		return listaDiagnosticoDTO;
	}

	/**
	 * @param listaDiagnosticoDTO
	 *            the listaDiagnosticoDTO to set
	 */
	public void setListaDiagnosticoDTO(List<DiagnosticoDTO> listaDiagnosticoDTO) {
		this.listaDiagnosticoDTO = listaDiagnosticoDTO;
	}

	/**
	 * @return the listaDocumentoSoporteVO
	 */
	public List<DocumentoSoporteVO> getListaDocumentoSoporteVO() {
		return listaDocumentoSoporteVO;
	}

	/**
	 * @param listaDocumentoSoporteVO
	 *            the listaDocumentoSoporteVO to set
	 */
	public void setListaDocumentoSoporteVO(List<DocumentoSoporteVO> listaDocumentoSoporteVO) {
		this.listaDocumentoSoporteVO = listaDocumentoSoporteVO;
	}

	public DocumentoSoporteVO getDocumentoSoporteVOSeleccionado() {
		return documentoSoporteVOSeleccionado;
	}

	public void setDocumentoSoporteVOSeleccionado(DocumentoSoporteVO documentoSoporteVOSeleccionado) {
		this.documentoSoporteVOSeleccionado = documentoSoporteVOSeleccionado;
	}

	/**
	 * @return the mostrarPopupHospitalizacion
	 */
	public boolean isMostrarPopupHospitalizacion() {
		return mostrarPopupHospitalizacion;
	}

	/**
	 * @param mostrarPopupHospitalizacion
	 *            the mostrarPopupHospitalizacion to set
	 */
	public void setMostrarPopupHospitalizacion(boolean mostrarPopupHospitalizacion) {
		this.mostrarPopupHospitalizacion = mostrarPopupHospitalizacion;
	}

	/**
	 * @return the mostrarPopupDocumentosSoporte
	 */
	public boolean isMostrarPopupDocumentosSoporte() {
		return mostrarPopupDocumentosSoporte;
	}

	/**
	 * @param mostrarPopupDocumentosSoporte
	 *            the mostrarPopupDocumentosSoporte to set
	 */
	public void setMostrarPopupDocumentosSoporte(boolean mostrarPopupDocumentosSoporte) {
		this.mostrarPopupDocumentosSoporte = mostrarPopupDocumentosSoporte;
	}

	/**
	 * @return the listaPrestacionDTO
	 */
	public List<PrestacionDTO> getListaPrestacionDTO() {
		return listaPrestacionDTO;
	}

	/**
	 * @param listaPrestacionDTO
	 *            the listaPrestacionDTO to set
	 */
	public void setListaPrestacionDTO(List<PrestacionDTO> listaPrestacionDTO) {
		this.listaPrestacionDTO = listaPrestacionDTO;
	}
	
	/**
	 * @return the listaInformacionAdicionalPrestacionDTO
	 */
	public List<DatosAdicionalesPrestacionDTO> getListaInformacionAdicionalPrestacionDTO() {
		return listaInformacionAdicionalPrestacionDTO;
	}

	/**
	 * @param listaInformacionAdicionalPrestacionDTO
	 *            the listaInformacionAdicionalPrestacionDTO to set
	 */
	public void setListaInformacionAdicionalPrestacionDTO(
			List<DatosAdicionalesPrestacionDTO> listaInformacionAdicionalPrestacionDTO) {
		this.listaInformacionAdicionalPrestacionDTO = listaInformacionAdicionalPrestacionDTO;
	}

	/**
	 * @return the listaPrestacionFechaEntrega
	 */
	public List<PrestacionDTO> getListaPrestacionFechaEntrega() {
		return listaPrestacionFechaEntrega;
	}

	/**
	 * @param listaPrestacionFechaEntrega
	 *            the listaPrestacionFechaEntrega to set
	 */
	public void setListaPrestacionFechaEntrega(List<PrestacionDTO> listaPrestacionFechaEntrega) {
		this.listaPrestacionFechaEntrega = listaPrestacionFechaEntrega;
	}

	/**
	 * @return the listaInconsistenciasMallaService
	 */
	public List<InconsistenciasResponseServiceVO> getListaInconsistenciasMallaService() {
		return listaInconsistenciasMallaService;
	}

	/**
	 * @param listaInconsistenciasMallaService
	 *            the listaInconsistenciasMallaService to set
	 */
	public void setListaInconsistenciasMallaService(
			List<InconsistenciasResponseServiceVO> listaInconsistenciasMallaService) {
		this.listaInconsistenciasMallaService = listaInconsistenciasMallaService;
	}

	/**
	 * @return the mostrarPopupRespuestaMalla
	 */
	public boolean isMostrarPopupRespuestaMalla() {
		return mostrarPopupRespuestaMalla;
	}

	/**
	 * @param mostrarPopupRespuestaMalla
	 *            the mostrarPopupRespuestaMalla to set
	 */
	public void setMostrarPopupRespuestaMalla(boolean mostrarPopupRespuestaMalla) {
		this.mostrarPopupRespuestaMalla = mostrarPopupRespuestaMalla;
	}

	/**
	 * @return the listaHistoricoFormatosVO
	 */
	public List<HistoricoFormatosVO> getListaHistoricoFormatosVO() {
		return listaHistoricoFormatosVO;
	}

	/**
	 * @param listaHistoricoFormatosVO
	 *            the listaHistoricoFormatosVO to set
	 */
	public void setListaHistoricoFormatosVO(List<HistoricoFormatosVO> listaHistoricoFormatosVO) {
		this.listaHistoricoFormatosVO = listaHistoricoFormatosVO;
	}

	/**
	 * @return the listaHistoricoModificacionVO
	 */
	public List<HistoricoModificacionVO> getListaHistoricoModificacionVO() {
		return listaHistoricoModificacionVO;
	}

	/**
	 * @param listaHistoricoModificacionVO
	 *            the listaHistoricoModificacionVO to set
	 */
	public void setListaHistoricoModificacionVO(List<HistoricoModificacionVO> listaHistoricoModificacionVO) {
		this.listaHistoricoModificacionVO = listaHistoricoModificacionVO;
	}

	/**
	 * @return the mostrarPopupHistoricoDescargas
	 */
	public boolean isMostrarPopupHistoricoDescargas() {
		return mostrarPopupHistoricoDescargas;
	}

	/**
	 * @param mostrarPopupHistoricoDescargas
	 *            the mostrarPopupHistoricoDescargas to set
	 */
	public void setMostrarPopupHistoricoDescargas(boolean mostrarPopupHistoricoDescargas) {
		this.mostrarPopupHistoricoDescargas = mostrarPopupHistoricoDescargas;
	}

	/**
	 * @return the mostrarPopupHistoricoModificacion
	 */
	public boolean isMostrarPopupHistoricoModificacion() {
		return mostrarPopupHistoricoModificacion;
	}

	/**
	 * @param mostrarPopupHistoricoModificacion
	 *            the mostrarPopupHistoricoModificacion to set
	 */
	public void setMostrarPopupHistoricoModificacion(boolean mostrarPopupHistoricoModificacion) {
		this.mostrarPopupHistoricoModificacion = mostrarPopupHistoricoModificacion;
	}

	/**
	 * @return the listaConceptosGastoVO
	 */
	public List<ConceptosGastoVO> getListaConceptosGastoVO() {
		return listaConceptosGastoVO;
	}

	/**
	 * @param listaConceptosGastoVO
	 *            the listaConceptosGastoVO to set
	 */
	public void setListaConceptosGastoVO(List<ConceptosGastoVO> listaConceptosGastoVO) {
		this.listaConceptosGastoVO = listaConceptosGastoVO;
	}

	/**
	 * @return the fechaActual
	 */
	public Date getFechaActual() {
		return fechaActual;
	}

	/**
	 * @param fechaActual
	 *            the fechaActual to set
	 */
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	/**
	 * @return the causalAnulacionSolicitud
	 */
	public Integer getCausalAnulacionSolicitud() {
		return causalAnulacionSolicitud;
	}

	/**
	 * @param causalAnulacionSolicitud
	 *            the causalAnulacionSolicitud to set
	 */
	public void setCausalAnulacionSolicitud(Integer causalAnulacionSolicitud) {
		this.causalAnulacionSolicitud = causalAnulacionSolicitud;
	}

	/**
	 * @return the causalModFechaEntrega
	 */
	public Integer getCausalModFechaEntrega() {
		return causalModFechaEntrega;
	}

	/**
	 * @param causalModFechaEntrega
	 *            the causalModFechaEntrega to set
	 */
	public void setCausalModFechaEntrega(Integer causalModFechaEntrega) {
		this.causalModFechaEntrega = causalModFechaEntrega;
	}

	/**
	 * @return the causalReliquidar
	 */
	public Integer getCausalReliquidar() {
		return causalReliquidar;
	}

	/**
	 * @param causalReliquidar
	 *            the causalReliquidar to set
	 */
	public void setCausalReliquidar(Integer causalReliquidar) {
		this.causalReliquidar = causalReliquidar;
	}

	/**
	 * @return the obsAnulacionSolicitud
	 */
	public String getObsAnulacionSolicitud() {
		return obsAnulacionSolicitud;
	}

	/**
	 * @param obsAnulacionSolicitud
	 *            the obsAnulacionSolicitud to set
	 */
	public void setObsAnulacionSolicitud(String obsAnulacionSolicitud) {
		this.obsAnulacionSolicitud = obsAnulacionSolicitud;
	}

	/**
	 * @return the obsModificarFechaEntrega
	 */
	public String getObsModificarFechaEntrega() {
		return obsModificarFechaEntrega;
	}

	/**
	 * @param obsModificarFechaEntrega
	 *            the obsModificarFechaEntrega to set
	 */
	public void setObsModificarFechaEntrega(String obsModificarFechaEntrega) {
		this.obsModificarFechaEntrega = obsModificarFechaEntrega;
	}

	/**
	 * @return the obsReliquidar
	 */
	public String getObsReliquidar() {
		return obsReliquidar;
	}

	/**
	 * @param obsReliquidar
	 *            the obsReliquidar to set
	 */
	public void setObsReliquidar(String obsReliquidar) {
		this.obsReliquidar = obsReliquidar;
	}

	/**
	 * @return the mostrarPopupAnularSolicitud
	 */
	public boolean isMostrarPopupAnularSolicitud() {
		return mostrarPopupAnularSolicitud;
	}

	/**
	 * @param mostrarPopupAnularSolicitud
	 *            the mostrarPopupAnularSolicitud to set
	 */
	public void setMostrarPopupAnularSolicitud(boolean mostrarPopupAnularSolicitud) {
		this.mostrarPopupAnularSolicitud = mostrarPopupAnularSolicitud;
	}

	/**
	 * @return the mostrarPopupModFechaEntrega
	 */
	public boolean isMostrarPopupModFechaEntrega() {
		return mostrarPopupModFechaEntrega;
	}

	/**
	 * @param mostrarPopupModFechaEntrega
	 *            the mostrarPopupModFechaEntrega to set
	 */
	public void setMostrarPopupModFechaEntrega(boolean mostrarPopupModFechaEntrega) {
		this.mostrarPopupModFechaEntrega = mostrarPopupModFechaEntrega;
	}

	/**
	 * @return the mostrarConfirmarModFecha
	 */
	public boolean isMostrarConfirmarModFecha() {
		return mostrarConfirmarModFecha;
	}

	/**
	 * @param mostrarConfirmarModFecha
	 *            the mostrarConfirmarModFecha to set
	 */
	public void setMostrarConfirmarModFecha(boolean mostrarConfirmarModFecha) {
		this.mostrarConfirmarModFecha = mostrarConfirmarModFecha;
	}

	/**
	 * @return the mostrarReliquidar
	 */
	public boolean isMostrarPopupReliquidar() {
		return mostrarPopupReliquidar;
	}

	/**
	 * @param mostrarReliquidar
	 *            the mostrarReliquidar to set
	 */
	public void setMostrarPopupReliquidar(boolean mostrarPopupReliquidar) {
		this.mostrarPopupReliquidar = mostrarPopupReliquidar;
	}

	/**
	 * @return the lstMotivosCausas
	 */
	public List<SelectItem> getLstMotivosCausas() {
		return lstMotivosCausas;
	}

	/**
	 * @param lstMotivosCausas
	 *            the lstMotivosCausas to set
	 */
	public void setLstMotivosCausas(List<SelectItem> lstMotivosCausas) {
		this.lstMotivosCausas = lstMotivosCausas;
	}

	/**
	 * @return the conceptosGastoVOSel
	 */
	public ConceptosGastoVO getConceptosGastoVOSel() {
		return conceptosGastoVOSel;
	}

	/**
	 * @param conceptosGastoVOSel
	 *            the conceptosGastoVOSel to set
	 */
	public void setConceptosGastoVOSel(ConceptosGastoVO conceptosGastoVOSel) {
		this.conceptosGastoVOSel = conceptosGastoVOSel;
	}

	/**
	 * @return the lstMotivosCausasOPS
	 */
	public List<SelectItem> getLstMotivosCausasOPS() {
		return lstMotivosCausasOPS;
	}

	/**
	 * @param lstMotivosCausasOPS
	 *            the lstMotivosCausasOPS to set
	 */
	public void setLstMotivosCausasOPS(List<SelectItem> lstMotivosCausasOPS) {
		this.lstMotivosCausasOPS = lstMotivosCausasOPS;
	}

	/**
	 * @return the lstMotivosCausasModFechaEntrega
	 */
	public List<SelectItem> getLstMotivosCausasModFechaEntrega() {
		return lstMotivosCausasModFechaEntrega;
	}

	/**
	 * @param lstMotivosCausasModFechaEntrega
	 *            the lstMotivosCausasModFechaEntrega to set
	 */
	public void setLstMotivosCausasModFechaEntrega(List<SelectItem> lstMotivosCausasModFechaEntrega) {
		this.lstMotivosCausasModFechaEntrega = lstMotivosCausasModFechaEntrega;
	}

	/**
	 * @return the lstMotivosCausasReliquidar
	 */
	public List<SelectItem> getLstMotivosCausasReliquidar() {
		return lstMotivosCausasReliquidar;
	}

	/**
	 * @param lstMotivosCausasReliquidar
	 *            the lstMotivosCausasReliquidar to set
	 */
	public void setLstMotivosCausasReliquidar(List<SelectItem> lstMotivosCausasReliquidar) {
		this.lstMotivosCausasReliquidar = lstMotivosCausasReliquidar;
	}

	/**
	 * @return the causalAnulacionOPS
	 */
	public Integer getCausalAnulacionOPS() {
		return causalAnulacionOPS;
	}

	/**
	 * @param causalAnulacionOPS
	 *            the causalAnulacionOPS to set
	 */
	public void setCausalAnulacionOPS(Integer causalAnulacionOPS) {
		this.causalAnulacionOPS = causalAnulacionOPS;
	}

	/**
	 * @return the obsAnulacionOPS
	 */
	public String getObsAnulacionOPS() {
		return obsAnulacionOPS;
	}

	/**
	 * @param obsAnulacionOPS
	 *            the obsAnulacionOPS to set
	 */
	public void setObsAnulacionOPS(String obsAnulacionOPS) {
		this.obsAnulacionOPS = obsAnulacionOPS;
	}

	/**
	 * @return the mostrarPopupAnularOPS
	 */
	public boolean isMostrarPopupAnularOPS() {
		return mostrarPopupAnularOPS;
	}

	/**
	 * @param mostrarPopupAnularOPS
	 *            the mostrarPopupAnularOPS to set
	 */
	public void setMostrarPopupAnularOPS(boolean mostrarPopupAnularOPS) {
		this.mostrarPopupAnularOPS = mostrarPopupAnularOPS;
	}

	/**
	 * @return the mostrarPopupModificarOPS
	 */
	public boolean isMostrarPopupModificarOPS() {
		return mostrarPopupModificarOPS;
	}

	/**
	 * @param mostrarPopupModificarOPS
	 *            the mostrarPopupModificarOPS to set
	 */
	public void setMostrarPopupModificarOPS(boolean mostrarPopupModificarOPS) {
		this.mostrarPopupModificarOPS = mostrarPopupModificarOPS;
	}

	/**
	 * @return the mostrarPopupcambiarDirOPS
	 */
	public boolean isMostrarPopupcambiarDirOPS() {
		return mostrarPopupcambiarDirOPS;
	}

	/**
	 * @param mostrarPopupcambiarDirOPS
	 *            the mostrarPopupcambiarDirOPS to set
	 */
	public void setMostrarPopupcambiarDirOPS(boolean mostrarPopupcambiarDirOPS) {
		this.mostrarPopupcambiarDirOPS = mostrarPopupcambiarDirOPS;
	}

	/**
	 * @return the lstMotivosCausasDireccionamientoOPS
	 */
	public List<SelectItem> getLstMotivosCausasDireccionamientoOPS() {
		return lstMotivosCausasDireccionamientoOPS;
	}

	/**
	 * @param lstMotivosCausasDireccionamientoOPS
	 *            the lstMotivosCausasDireccionamientoOPS to set
	 */
	public void setLstMotivosCausasDireccionamientoOPS(List<SelectItem> lstMotivosCausasDireccionamientoOPS) {
		this.lstMotivosCausasDireccionamientoOPS = lstMotivosCausasDireccionamientoOPS;
	}

	/**
	 * @return the lstPrestacionesOPS
	 */
	public List<PrestacionDTO> getLstPrestacionesOPS() {
		return lstPrestacionesOPS;
	}

	/**
	 * @param lstPrestacionesOPS
	 *            the lstPrestacionesOPS to set
	 */
	public void setLstPrestacionesOPS(List<PrestacionDTO> lstPrestacionesOPS) {
		this.lstPrestacionesOPS = lstPrestacionesOPS;
	}

	/**
	 * @return the lstPrestadoresDireccionamiento
	 */
	public List<PrestadorVO> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

	/**
	 * @param lstPrestadoresDireccionamiento
	 *            the lstPrestadoresDireccionamiento to set
	 */
	public void setLstPrestadoresDireccionamiento(List<PrestadorVO> lstPrestadoresDireccionamiento) {
		this.lstPrestadoresDireccionamiento = lstPrestadoresDireccionamiento;
	}

	/**
	 * @return the prestadorOPS
	 */
	public PrestadorVO getPrestadorOPS() {
		return prestadorOPS;
	}

	/**
	 * @param prestadorOPS
	 *            the prestadorOPS to set
	 */
	public void setPrestadorOPS(PrestadorVO prestadorOPS) {
		this.prestadorOPS = prestadorOPS;
	}

	/**
	 * @return the causalDireccionamientoOPS
	 */
	public Integer getCausalDireccionamientoOPS() {
		return causalDireccionamientoOPS;
	}

	/**
	 * @param causalDireccionamientoOPS
	 *            the causalDireccionamientoOPS to set
	 */
	public void setCausalDireccionamientoOPS(Integer causalDireccionamientoOPS) {
		this.causalDireccionamientoOPS = causalDireccionamientoOPS;
	}

	/**
	 * @return the obsDireccionamientoOPS
	 */
	public String getObsDireccionamientoOPS() {
		return obsDireccionamientoOPS;
	}

	/**
	 * @param obsDireccionamientoOPS
	 *            the obsDireccionamientoOPS to set
	 */
	public void setObsDireccionamientoOPS(String obsDireccionamientoOPS) {
		this.obsDireccionamientoOPS = obsDireccionamientoOPS;
	}

	/**
	 * @return the codInternoPrestadorNuevo
	 */
	public String getCodInternoPrestadorNuevo() {
		return codInternoPrestadorNuevo;
	}

	/**
	 * @param codInternoPrestadorNuevo
	 *            the codInternoPrestadorNuevo to set
	 */
	public void setCodInternoPrestadorNuevo(String codInternoPrestadorNuevo) {
		this.codInternoPrestadorNuevo = codInternoPrestadorNuevo;
	}

	/**
	 * @return the mostrarBtnAnular
	 */
	public boolean isMostrarBtnAnular() {
		return mostrarBtnAnular;
	}

	/**
	 * @param mostrarBtnAnular
	 *            the mostrarBtnAnular to set
	 */
	public void setMostrarBtnAnular(boolean mostrarBtnAnular) {
		this.mostrarBtnAnular = mostrarBtnAnular;
	}

	/**
	 * @return the mostrarBtnFechaEntrega
	 */
	public boolean isMostrarBtnModificarFechaEntrega() {
		return mostrarBtnModificarFechaEntrega;
	}

	/**
	 * @param mostrarBtnFechaEntrega
	 *            the mostrarBtnFechaEntrega to set
	 */
	public void setMostrarBtnModificarFechaEntrega(boolean mostrarBtnModificarFechaEntrega) {
		this.mostrarBtnModificarFechaEntrega = mostrarBtnModificarFechaEntrega;
	}

	/**
	 * @return the mostrarBtnModificar
	 */
	public boolean isMostrarBtnModificar() {
		return mostrarBtnModificar;
	}

	/**
	 * @param mostrarBtnModificar
	 *            the mostrarBtnModificar to set
	 */
	public void setMostrarBtnModificar(boolean mostrarBtnModificar) {
		this.mostrarBtnModificar = mostrarBtnModificar;
	}

	/**
	 * @return the permisoAnularSolicitudOPS
	 */
	public boolean isPermisoAnularSolicitudOPS() {
		return permisoAnularSolicitudOPS;
	}

	/**
	 * @param permisoAnularSolicitudOPS
	 *            the permisoAnularSolicitudOPS to set
	 */
	public void setPermisoAnularSolicitudOPS(boolean permisoAnularSolicitudOPS) {
		this.permisoAnularSolicitudOPS = permisoAnularSolicitudOPS;
	}

	/**
	 * @return the prestacionSeleccionada
	 */
	public PrestacionDTO getPrestacionSeleccionada() {
		return prestacionSeleccionada;
	}

	/**
	 * @param prestacionSeleccionada
	 *            the prestacionSeleccionada to set
	 */
	public void setPrestacionSeleccionada(PrestacionDTO prestacionSeleccionada) {
		this.prestacionSeleccionada = prestacionSeleccionada;
	}

	/**
	 * @return the mostrarPopupGenerarOPS
	 */
	public boolean isMostrarPopupGenerarOPS() {
		return mostrarPopupGenerarOPS;
	}

	/**
	 * @param mostrarPopupGenerarOPS
	 *            the mostrarPopupGenerarOPS to set
	 */
	public void setMostrarPopupGenerarOPS(boolean mostrarPopupGenerarOPS) {
		this.mostrarPopupGenerarOPS = mostrarPopupGenerarOPS;
	}

	/**
	 * @return the generarConObservaciones
	 */
	public boolean isGenerarConObservaciones() {
		return generarConObservaciones;
	}

	/**
	 * @param generarConObservaciones
	 *            the generarConObservaciones to set
	 */
	public void setGenerarConObservaciones(boolean generarConObservaciones) {
		this.generarConObservaciones = generarConObservaciones;
	}

	/**
	 * @return the listaAutorizacionServicioVOs
	 */
	public List<AutorizacionServicioVO> getListaAutorizacionServicioVOs() {
		return listaAutorizacionServicioVOs;
	}

	/**
	 * @param listaAutorizacionServicioVOs
	 *            the listaAutorizacionServicioVOs to set
	 */
	public void setListaAutorizacionServicioVOs(List<AutorizacionServicioVO> listaAutorizacionServicioVOs) {
		this.listaAutorizacionServicioVOs = listaAutorizacionServicioVOs;
	}

	/**
	 * @return the inconsistenciasVO
	 */
	public InconsistenciasVO getInconsistenciasVO() {
		return inconsistenciasVO;
	}

	/**
	 * @param inconsistenciasVO
	 *            the inconsistenciasVO to set
	 */
	public void setInconsistenciasVO(InconsistenciasVO inconsistenciasVO) {
		this.inconsistenciasVO = inconsistenciasVO;
	}

	/**
	 * @return the mostrarPopupGenerarDevolucion
	 */
	public boolean isMostrarPopupGenerarDevolucion() {
		return mostrarPopupGenerarDevolucion;
	}

	/**
	 * @param mostrarPopupGenerarDevolucion
	 *            the mostrarPopupGenerarDevolucion to set
	 */
	public void setMostrarPopupGenerarDevolucion(boolean mostrarPopupGenerarDevolucion) {
		this.mostrarPopupGenerarDevolucion = mostrarPopupGenerarDevolucion;
	}

	/**
	 * @return the mostrarPopupGenerarNegacion
	 */
	public boolean isMostrarPopupGenerarNegacion() {
		return mostrarPopupGenerarNegacion;
	}

	/**
	 * @param mostrarPopupGenerarNegacion
	 *            the mostrarPopupGenerarNegacion to set
	 */
	public void setMostrarPopupGenerarNegacion(boolean mostrarPopupGenerarNegacion) {
		this.mostrarPopupGenerarNegacion = mostrarPopupGenerarNegacion;
	}

	/**
	 * @return the mostrarPopupAutorizarNegacion
	 */
	public boolean isMostrarPopupAutorizarNegacion() {
		return mostrarPopupAutorizarNegacion;
	}

	/**
	 * @param mostrarPopupAutorizarNegacion
	 *            the mostrarPopupAutorizarNegacion to set
	 */
	public void setMostrarPopupAutorizarNegacion(boolean mostrarPopupAutorizarNegacion) {
		this.mostrarPopupAutorizarNegacion = mostrarPopupAutorizarNegacion;
	}

	/**
	 * @return the NumeroAutorizacionPrestacionIPS
	 */
	public String getNumeroAutorizacionPrestacionIPS() {
		return numeroAutorizacionPrestacionIPS;
	}

	/**
	 * @param NumeroAutorizacionPrestacionIPS
	 *            the NumeroAutorizacionPrestacionIPS to set
	 */
	public void setNumeroAutorizacionPrestacionIPS(String numeroAutorizacionPrestacionIPS) {
		this.numeroAutorizacionPrestacionIPS = numeroAutorizacionPrestacionIPS;
	}

	/**
	 * @return the ObsDireccionamientoAutorizacion
	 */
	public String getObsDireccionamientoAutorizacion() {
		return obsDireccionamientoAutorizacion;
	}

	/**
	 * @param ObsDireccionamientoAutorizacion
	 *            the ObsDireccionamientoAutorizacion to set
	 */
	public void setObsDireccionamientoAutorizacion(String obsDireccionamientoAutorizacion) {
		this.obsDireccionamientoAutorizacion = obsDireccionamientoAutorizacion;
	}

	/**
	 * @return the CausalAutorizacionPrestacion
	 */
	public Integer getCausalAutorizacionPrestacion() {
		return causalAutorizacionPrestacion;
	}

	/**
	 * @param CausalAutorizacionPrestacion
	 *            the CausalAutorizacionPrestacion to set
	 */
	public void setCausalAutorizacionPrestacion(Integer causalAutorizacionPrestacion) {
		this.causalAutorizacionPrestacion = causalAutorizacionPrestacion;
	}

	/**
	 * @return the lstMotivosCausasPrestacionesPAFPOC
	 */
	public List<SelectItem> getLstMotivosCausasPrestacionesPAFPOC() {
		return lstMotivosCausasPrestacionesPAFPOC;
	}

	/**
	 * @param lstMotivosCausasPrestacionesPAFPOC
	 *            the lstMotivosCausasPrestacionesPAFPOC to set
	 */
	public void setLstMotivosCausasPrestacionesPAFPOC(List<SelectItem> lstMotivosCausasPrestacionesPAFPOC) {
		this.lstMotivosCausasPrestacionesPAFPOC = lstMotivosCausasPrestacionesPAFPOC;
	}

	/**
	 * @return the listaMotivosCausaVo
	 */
	public List<MotivoCausaVO> getListaMotivosCausaVo() {
		return listaMotivosCausaVo;
	}

	/**
	 * 
	 */
	public void abrirPopupValidarAccesoDirecto() {
		visualizarPopupValidarAccesoDirecto = true;
		visualizarPopupAccesoDirecto = false;

	}

	/**
	 * 
	 */
	public void cerrarPopupValidarAccesoDirecto() {
		visualizarPopupValidarAccesoDirecto = false;
	}

	public boolean isVisualizarPopupAccesoDirecto() {
		return visualizarPopupAccesoDirecto;
	}

	public void setVisualizarPopupAccesoDirecto(boolean visualizarPopupAccesoDirecto) {
		this.visualizarPopupAccesoDirecto = visualizarPopupAccesoDirecto;
	}

	public boolean isVisualizarPopupValidarAccesoDirecto() {
		return visualizarPopupValidarAccesoDirecto;
	}

	public void setVisualizarPopupValidarAccesoDirecto(boolean visualizarPopupValidarAccesoDirecto) {
		this.visualizarPopupValidarAccesoDirecto = visualizarPopupValidarAccesoDirecto;
	}

	public String getUsuarioDescarga() {
		return usuarioDescarga;
	}

	public void setUsuarioDescarga(String usuarioDescarga) {
		this.usuarioDescarga = usuarioDescarga;
	}

	public String getUsuarioDescargaPass() {
		return usuarioDescargaPass;
	}

	public void setUsuarioDescargaPass(String usuarioDescargaPass) {
		this.usuarioDescargaPass = usuarioDescargaPass;
	}

	/**
	 * @param listaMotivosCausaVo
	 *            the listaMotivosCausaVo to set
	 */
	public void setListaMotivosCausaVo(List<MotivoCausaVO> listaMotivosCausaVo) {
		this.listaMotivosCausaVo = listaMotivosCausaVo;
	}

	/**
	 * @return the habilitaGestionNumeroAutorizacionIPS
	 */
	public boolean isHabilitaGestionNumeroAutorizacionIPS() {
		return habilitaGestionNumeroAutorizacionIPS;
	}

	/**
	 * @param habilitaGestionNumeroAutorizacionIPS
	 *            the habilitaGestionNumeroAutorizacionIPS to set
	 */
	public void setHabilitaGestionNumeroAutorizacionIPS(boolean habilitaGestionNumeroAutorizacionIPS) {
		this.habilitaGestionNumeroAutorizacionIPS = habilitaGestionNumeroAutorizacionIPS;
	}

	/**
	 * @return the isMostrarLinkAutorizar
	 */
	public boolean isMostrarLinkAutorizar() {
		return mostrarLinkAutorizar;
	}

	/**
	 * @param mostrarLinkAutorizar
	 *            the mostrarLinkAutorizar to set
	 */
	public void setMostrarLinkAutorizar(boolean mostrarLinkAutorizar) {
		this.mostrarLinkAutorizar = mostrarLinkAutorizar;
	}

	public void setEditarRecobro(boolean editarRecobro) {
		this.editarRecobro = editarRecobro;
	}

	public boolean getEditarRecobro() {
		return editarRecobro;
	}

	public void activarRecobro() {
		this.editarRecobro = !editarRecobro;
	}

	public void inactivarRecobro() {
		obtenerConsecutivoContingenciaRecobro();
		this.editarRecobro = !editarRecobro;
	}

	public List<ContingenciaRecobroVO> getListaContingenciaRecobroVOs() {
		return listaContingenciaRecobroVOs;
	}

	public void setListaContingenciaRecobroVOs(List<ContingenciaRecobroVO> listaContingenciaRecobroVOs) {
		this.listaContingenciaRecobroVOs = listaContingenciaRecobroVOs;
	}

	public List<SelectItem> getListaContingenciaRecobros() {
		return listaContingenciaRecobros;
	}

	public void setListaContingenciaRecobros(List<SelectItem> listaContingenciaRecobros) {
		this.listaContingenciaRecobros = listaContingenciaRecobros;
	}

	public Integer getConsecutivoContingenciaRecobro() {
		return consecutivoContingenciaRecobro;
	}

	public void setConsecutivoContingenciaRecobro(Integer consecutivoContingenciaRecobro) {
		this.consecutivoContingenciaRecobro = consecutivoContingenciaRecobro;
	}

	public void guardarEditarRecobro() {
		DiagnosticoDTO diagnosticoPrincipal = obtenerDiagnosticoPrincipal();
		DiagnosticosVO diagnosticoVOPrincipal = diagnosticoPrincipal.getDiagnosticosVO();
		try {
			ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
			consultarSolicitudController.guardarEditarRecobro(solicitudSeleccionada.getNumeroSolicitud(),
					consecutivoContingenciaRecobro, diagnosticoVOPrincipal.getConsecutivoCodigoContingencia(),
					ConstantesWeb.OBSERVACION_MODIFICACION_RECOBRO,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			this.editarRecobro = !editarRecobro;
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.MENSAJE_GUARDAR_RECOBRO_OK,
					solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public boolean isMostrarBtnEditarRecobro() {
		return mostrarBtnEditarRecobro;
	}

	public boolean isMostrarDescarga() {
		return mostrarDescarga;
	}

	public boolean isModificarDireccionamientoConOPS() {
		return modificarDireccionamientoConOPS;
	}
	
	public void mostrarConfirmarCambioDireccionamientoConcepto() {
		mostrarConfirmarCambiarDireccionamientoConcepto = true;
	}
	
	public void ocultarConfirmarCambioDireccionamientoConcepto() {
		mostrarConfirmarCambiarDireccionamientoConcepto = false;
	}

	public boolean isMostrarConfirmarCambiarDireccionamientoConcepto() {
		return mostrarConfirmarCambiarDireccionamientoConcepto;
	}

	public void setMostrarBtnEditarRecobro(boolean mostrarBtnEditarRecobro) {
		this.mostrarBtnEditarRecobro = mostrarBtnEditarRecobro;
	}

	public boolean isMostrarPopupCambiarRecobro() {
		return mostrarPopupCambiarRecobro;
	}

	public void setMostrarPopupCambiarRecobro(boolean mostrarPopupCambiarRecobro) {
		this.mostrarPopupCambiarRecobro = mostrarPopupCambiarRecobro;
	}
	
	
	public void mostrarCambiarRecobroPopup() {
		ConsultarSolicitudController consultarSolicitudController;
		try {
			consultarSolicitudController = generarConsultarSolicitudController();
			lstCausalesNoCobroCuota = consultarSolicitudController.consultarCausalesNoCobroCuota();
			this.mostrarPopupCambiarRecobro = true;
			habilitarEditarPrestacion = (ConstantesWeb.COD_ESTADO_ANULADO.intValue() != prestacionSeleccionada.getConsecutivoEstado().intValue()
					&& ConstantesWeb.COD_ESTADO_ENTREGADA.intValue() != prestacionSeleccionada.getConsecutivoEstado().intValue()
					&& ConstantesWeb.COD_ESTADO_PROCESADA.intValue() != prestacionSeleccionada.getConsecutivoEstado().intValue()
					&& ConstantesWeb.COD_ESTADO_COBRADO.intValue() != prestacionSeleccionada.getConsecutivoEstado().intValue());
			prestacionSeleccionada.setNoCobroCuotaRecuperacion(false);
			cambiarCausalesNoCobro = (modificarInformacionPrestacion && permisoAnularSolicitudOPS && habilitarEditarPrestacion);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void cerrarCambiarRecobroPopup() {
		modificarInformacionPrestacion = false;
		mostrarPopupCambiarRecobro = false;
		modificarInformacionRecobro = false;
	}
	
	/**
	 * 
	 */
	public void guardarModificacionRecobroxPrestacion() {
		DiagnosticoDTO diagnosticoPrincipal = obtenerDiagnosticoPrincipal();
		DiagnosticosVO diagnosticoVOPrincipal = diagnosticoPrincipal.getDiagnosticosVO();
		try {
			ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
			consultarSolicitudController.guardarModificacionRecobroxPrestacion(
					prestacionSeleccionada.getConsecutivoServicioSolicitado(), 
					solicitudSeleccionada.getNumeroSolicitud(),
					prestacionSeleccionada.getConsecutivoRecobro(),
					ConstantesWeb.OBSERVACION_MODIFICACION_RECOBRO,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(),
					diagnosticoVOPrincipal.getConsecutivoCodigoContingencia());
			this.mostrarPopupCambiarRecobro = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.MENSAJE_GUARDAR_RECOBRO_OK, solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);
			listaPrestacionDTO = consultarSolicitudController
					.consultaInformacionPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			activarRecobroPrestacion();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	public void guardarModificacionPrestacion() {
		DiagnosticoDTO diagnosticoPrincipal = obtenerDiagnosticoPrincipal();
		DiagnosticosVO diagnosticoVOPrincipal = diagnosticoPrincipal.getDiagnosticosVO();
		try {
			ConsultarSolicitudController consultarSolicitudController = generarConsultarSolicitudController();
			consultarSolicitudController.guardarModificacionPrestacion(
					prestacionSeleccionada.getConsecutivoServicioSolicitado(), 
					solicitudSeleccionada.getNumeroSolicitud(),
					prestacionSeleccionada.getConsecutivoRecobro(),
					ConstantesWeb.OBSERVACION_MODIFICACION_PRESTACION,
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(),
					diagnosticoVOPrincipal.getConsecutivoCodigoContingencia(),
					prestacionSeleccionada.getConsecutivoCausaNoCobroCuota());
			this.mostrarPopupCambiarRecobro = false;
			modificarInformacionRecobro = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.MENSAJE_GUARDAR_PRESTACION_OK, solicitudSeleccionada.getNumeroRadicado().toString()), ConstantesWeb.COLOR_AZUL);
			listaPrestacionDTO = consultarSolicitudController
					.consultaInformacionPrestacionesxSolicitud(solicitudSeleccionada.getNumeroSolicitud());
			activarRecobroPrestacion();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	public void limpiarCiudad() {
		if(!activarFiltroCiudad) 
			ciudad = new CiudadVO();
	}

	public boolean isMostrarLinkRecobro() {
		return mostrarLinkRecobro;
	}

	public void setMostrarLinkRecobro(boolean mostrarLinkRecobro) {
		this.mostrarLinkRecobro = mostrarLinkRecobro;
	}

	public List<SelectItem> getLstCausalesNoCobroCuota() {
		return lstCausalesNoCobroCuota;
	}

	public boolean isModificarInformacionPrestacion() {
		return modificarInformacionPrestacion;
	}
	
	public boolean isCambiarDireccionamientoSinOPS() {
		return cambiarDireccionamientoSinOPS;
	}

	public boolean isMostrarPopupcambiarDirSinOPS() {
		return mostrarPopupcambiarDirSinOPS;
	}

	public void setMostrarPopupcambiarDirSinOPS(boolean mostrarPopupcambiarDirSinOPS) {
		this.mostrarPopupcambiarDirSinOPS = mostrarPopupcambiarDirSinOPS;
	}

	public boolean isCambiarCausalesNoCobro() {
		return cambiarCausalesNoCobro;
	}

	public void setCambiarCausalesNoCobro(boolean cambiarCausalesNoCobro) {
		this.cambiarCausalesNoCobro = cambiarCausalesNoCobro;
	}

	public CiudadVO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadVO ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public boolean isActivarFiltroCiudad() {
		return activarFiltroCiudad;
	}

	public void setActivarFiltroCiudad(boolean activarFiltroCiudad) {
		
		this.activarFiltroCiudad = activarFiltroCiudad;
	}

	public boolean isMostrarPopupAnularPrestacion() {
		return mostrarPopupAnularPrestacion;
	}

	public void setMostrarPopupAnularPrestacion(boolean mostrarPopupAnularPrestacion) {
		this.mostrarPopupAnularPrestacion = mostrarPopupAnularPrestacion;
	}

	public Integer getCausalAnulacionPrestacion() {
		return causalAnulacionPrestacion;
	}

	public void setCausalAnulacionPrestacion(Integer causalAnulacionPrestacion) {
		this.causalAnulacionPrestacion = causalAnulacionPrestacion;
	}

	public String getObservacionAnulacionPrestacion() {
		return observacionAnulacionPrestacion;
	}

	public void setObservacionAnulacionPrestacion(
			String observacionAnulacionPrestacion) {
		this.observacionAnulacionPrestacion = observacionAnulacionPrestacion;
	}

	public List<CiudadVO> getListaCiudadesDireccionamiento() {
		return listaCiudadesDireccionamiento;
	}

	public void setListaCiudadesDireccionamiento(
			List<CiudadVO> listaCiudadesDireccionamiento) {
		this.listaCiudadesDireccionamiento = listaCiudadesDireccionamiento;
	}

	public List<SelectItem> getLstMotivosCausasPrestacion() {
		return lstMotivosCausasPrestacion;
	}

	public void setLstMotivosCausasPrestacion(
			List<SelectItem> lstMotivosCausasPrestacion) {
		this.lstMotivosCausasPrestacion = lstMotivosCausasPrestacion;
	}

	public boolean isHabilitarEditarPrestacion() {
		return habilitarEditarPrestacion;
	}

	public void setHabilitarEditarPrestacion(boolean habilitarEditarPrestacion) {
		this.habilitarEditarPrestacion = habilitarEditarPrestacion;
	}

	public boolean isModificarInformacionRecobro() {
		return modificarInformacionRecobro;
	}

	public void setModificarInformacionRecobro(boolean modificarInformacionRecobro) {
		this.modificarInformacionRecobro = modificarInformacionRecobro;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public boolean isMostrarOptionSolicitudes() {
		return mostrarOptionSolicitudes;
	}

	public void setMostrarOptionSolicitudes(boolean mostrarOptionSolicitudes) {
		this.mostrarOptionSolicitudes = mostrarOptionSolicitudes;
	}
}