package com.sos.gestionautorizacionessaludweb.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.ips.modelo.IPSVO;
import com.sos.util.jsf.FacesUtil;

public class IngresoSolicitudIpsBean {

	private List<ProcedimientosVO> listaCUPSEncontrados= new ArrayList<ProcedimientosVO>();
	private List<MedicamentosVO> listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
	
	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {

	}
	
	
	public List<ProcedimientosVO> consultarPrestacionesCUPSIps(ProcedimientosVO procedimientosVO, IPSVO ipsVO, int consPlan)  throws LogicException, ServiceException{
		PrestacionController prestacionController = new PrestacionController();
		listaCUPSEncontrados = prestacionController.consultarPrestacionesCUPSIps(procedimientosVO, ipsVO, consPlan);	
		
		return listaCUPSEncontrados;
	}

	
	public List<MedicamentosVO> consultarPrestacionesCUMSIps(MedicamentosVO medicamentoVO, IPSVO ipsVO, int consPlan)  throws LogicException, ServiceException{
		PrestacionController prestacionController = new PrestacionController();
		listaMedicamentosEncontrados = prestacionController.consultarPrestacionesMedicamentosIps(medicamentoVO, ipsVO, consPlan);
		
		return listaMedicamentosEncontrados;
	}
	
	
	/**
	 * @throws Exception 
	 * 
	 */
	public void gestionRespuestaGrabarSolicitud() throws LogicException{
		
		IngresoSolicitudBean ingresobean = ((IngresoSolicitudBean) FacesUtil.getBean(ConstantesWeb.INGRESAR_SOLICITUD_BEAN));
		
		if (!ingresobean.getListaInconsistenciasDevueltasMallaService().isEmpty() && ingresobean.getListaPrestacionesInconsistencias().isEmpty() && ingresobean.getListaInconsistenciasAdministrativas().isEmpty()
				&& ingresobean.getListaPrestacionesAuditoria().isEmpty() && ingresobean.getListaPrestacionesAprobadas().isEmpty()) {

			ingresobean.setMostrarPopupSolicitudDevuelta(true);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_DEVUELTA) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!ingresobean.getListaPrestacionesAuditoria().isEmpty() && ingresobean.getListaPrestacionesInconsistencias().isEmpty() && ingresobean.getListaInconsistenciasAdministrativas().isEmpty()	&& ingresobean.getListaInconsistenciasDevueltasMallaService().isEmpty()) {

			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_AUDITORIA) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!ingresobean.getListaInconsistenciasAdministrativas().isEmpty()) {

			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_INCONSISTENCIAS_ADMINISTRATIVAS) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
			ingresobean.setDeshabilitarBtnGuardar(false);

		} else if (!ingresobean.getListaPrestacionesInconsistencias().isEmpty() && ingresobean.getListaPrestacionesAuditoria().isEmpty() && ingresobean.getListaInconsistenciasAdministrativas().isEmpty()	&& ingresobean.getListaInconsistenciasDevueltasMallaService().isEmpty()) {

			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_INCONSISTENCIAS) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!ingresobean.getListaPrestacionesInconsistencias().isEmpty() || !ingresobean.getListaPrestacionesAuditoria().isEmpty()) {

			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!ingresobean.getListaInconsistenciasDevueltasMallaService().isEmpty() && !ingresobean.getListaPrestacionesAprobadas().isEmpty() && ingresobean.getListaPrestacionesInconsistencias().isEmpty() && ingresobean.getListaPrestacionesAuditoria().isEmpty() && ingresobean.getListaInconsistenciasAdministrativas().isEmpty()) {
			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
			
		} else if (ingresobean.getListaInconsistenciasAdministrativas().isEmpty() && ingresobean.getListaPrestacionesInconsistencias().isEmpty() && ingresobean.getListaPrestacionesAuditoria().isEmpty()	&& ingresobean.getListaInconsistenciasDevueltasMallaService().isEmpty()) {

			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_CREADA) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), "blue");
		} else {
			ingresobean.instanciarBPM();
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX) + ingresobean.getSolicitud().getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		}
	}

	
}
