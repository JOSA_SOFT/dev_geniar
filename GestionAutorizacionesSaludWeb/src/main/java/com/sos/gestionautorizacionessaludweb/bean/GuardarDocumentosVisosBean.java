package com.sos.gestionautorizacionessaludweb.bean;

import java.math.BigInteger;
import java.util.List;

import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.AtencionVO;
import com.sos.gestionautorizacionessaludejb.controller.GestionarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.visos.service.vo.Indice;

/**
 * Class GuardarDocumentosVisosBean Clase bean que guarda los documentos soporte
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 21/04/2015
 *
 */
public class GuardarDocumentosVisosBean {

	public List<SoporteVO> guardarDocumentos(AfiliadoVO afiliadoVO, SolicitudVO solicitud, AtencionVO atencionVO, List<SoporteVO> listaSoporteVOs) throws LogicException {

		DigitalizarDocumentosAnexosBean digitalizarDocumentosAnexosBean = new DigitalizarDocumentosAnexosBean();
		Integer[] codigosIndices = digitalizarDocumentosAnexosBean.getIndicesDocumentosAnexos();
		String[] indicesRemitidos = new String[codigosIndices.length];
		indicesRemitidos[0] = afiliadoVO.getNumeroIdentificacion();
		indicesRemitidos[1] = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim();
		indicesRemitidos[2] = atencionVO.getNumeroSolicitud() != null ? Integer.toString(atencionVO.getNumeroSolicitud()) : ConstantesWeb.CADENA_VACIA;
		indicesRemitidos[3] = solicitud.getNumeroSolicitudSOS();
		indicesRemitidos[4] = Utilidades.obtenerStringDeDate(atencionVO.getFechaSolicitudProfesional(), ConstantesWeb.FORMATO_FECHA_1, true);
		indicesRemitidos[5] = Integer.toString(solicitud.getConsecutivoSolicitud());
		Indice[] indices = Utilidades.procesarIndices(indicesRemitidos, codigosIndices);
		long consecutivoVisos = digitalizarDocumentosAnexosBean.anexarDocumentos(listaSoporteVOs, indices, solicitud.getConsecutivoSolicitud());
		BigInteger identificadorDocumento = new BigInteger(Long.toString(consecutivoVisos));
		BigInteger consecutivoSolicitud = new BigInteger(solicitud.getConsecutivoSolicitud().toString());

		GestionarSolicitudController gestionarSolicitudController = new GestionarSolicitudController();
		try {
			if (consecutivoVisos != 0) {
				gestionarSolicitudController.grabarIdentificadorDocumentoAnexos(FacesUtils.getUserName(), consecutivoSolicitud, identificadorDocumento);
				for (SoporteVO soporteVO : listaSoporteVOs) {
					soporteVO.setSubidoAVisos(true);
				}
			}
		} catch (ServiceException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VISOS_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GRABAR_IDENTIFICADOR_DOCUMENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return listaSoporteVOs;
	}

}
