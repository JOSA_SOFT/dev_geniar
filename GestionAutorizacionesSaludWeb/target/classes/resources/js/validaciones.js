/**
 * Valida que el valor de un campo de texto sea un n�mero entero
 * 
 * @param e
 *            Evento del teclado sobre el campo de texto que se quiere validar
 */
function esEntero(e) {
	var TAB_KEY = 9;
	var CTRL = e.ctrlKey;
	var V_KEY = 118;
	var V_LOWERCASE_KEY = 86;
	var HOME_KEY = 36;
	var END_KEY = 35;
	var DELETE_KEY = 46;
	var unicode = e.charCode ? e.charCode : e.keyCode;

	if (unicode == TAB_KEY || (CTRL && unicode == V_KEY)
			|| (CTRL && unicode == V_LOWERCASE_KEY) || unicode == HOME_KEY
			|| unicode == END_KEY || unicode == DELETE_KEY) {
		return true;
	}
	if (esBackSpace(e) == false) {
		if (unicode < 48 || unicode > 57)
			return false;
	}
	return true;
}

/**
 * Valida si el valor del campo es num�rico y es menor a 3
 * 
 * @param e
 *            Evento del teclado sobre el campo de texto que se quiere validar
 * @param campo
 *            Campo de texto donde se gener� el evento de teclado
 * @param longitud
 *            N�mero de d�gitos que se quiere validar
 */
function esEnteroLongitudCalificador(e, campo, longitud) {
	var backSpace = esBackSpace(e);
	if (backSpace == false && esEntero(e)) {
		if (campo.value > 3) {
			return false;
		} else {
			return esEnteroLongitud(e, campo, longitud);
		}
		return true;
	}
}

/**
 * Valida que el valor de un campo de texto sea un n�mero entero y que su
 * longitud no exceda un n�mero dado de d�gitos
 * 
 * @param e
 *            Evento del teclado sobre el campo de texto que se quiere validar
 * @param campo
 *            Campo de texto donde se gener� el evento de teclado
 * @param longitud
 *            N�mero de d�gitos que se quiere validar
 */
function esEnteroLongitud(e, campo, longitud) {
	var backSpace = esBackSpace(e);
	if (backSpace == false && esEntero(e))
		return esLongitud(e, campo, longitud - 1);
	return backSpace;
}

/**
 * Valida que el valor de un campo de texto tenga una longitud m�xima dada
 * 
 * @param campo
 *            Campo de texto
 * @param longitud
 *            N�mero de caracteres a validar
 */
function esLongitud(e, campo, longitud) {
	var backSpace = esBackSpace(e);
	if (backSpace == false)
		return campo.value.length <= longitud;
	return true;
}

/**
 * Indica si la tecla presionada en un evento dado es la tecla backspace
 */
function esBackSpace(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return unicode == 8;
}

/**
 * Indica si la tecla presionada en un evento dado es la tecla backspace
 */
function esEspacio(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return (unicode == 32 || unicode == 13);
}

/**
 * Indica si el valor ingresado en un campo de texto es permitido para no dejar
 * vacio el mismo
 * 
 * @param campo
 *            El campo de texto
 * @param e
 *            El evento de ingreso de un caracter al campo de texto
 */
function noVacio(campo, e) {
	if (esBackSpace(e) == false) {
		var longitudTexto = campo.value.length;
		if (esEspacio(e)
				&& (longitudTexto == 0 || campo.value.charAt(longitudTexto - 1) == ' ')) {
			return false;
		}
	}
	return true;
}

/**
 * Indica si la tecla presionada en un evento dado es la tecla utilizada para
 * definir los n�meros flotantes
 */
function esCaracterFlotante(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return caracterFlotante() == String.fromCharCode(unicode)
}

/**
 * Retorna el caracter con el que se definen los n�meros flotantes
 */
function caracterFlotante() {
	return ",";
}
/**
 * Retorna el caracter con el que se definen los n�meros negativos
 */
function caracterNegativo() {
	return "-";
}
/**
 * Retorna el caracter con el que se definen el espacio
 */
function caracterEspacio() {
	return " ";
}
/**
 * Retorna el caracter con el que se definen los puntos
 */
function caracterPunto() {
	return ".";
}
/**
 * Valida que el valor de un campo de texto sea un n�mero flotante
 * 
 * @param campo
 *            Campo de texto donde se gener� el evento de teclado
 * @param e
 *            Evento del teclado sobre el campo de texto que se quiere validar
 */
function esFlotante(campo, e, enteros, decimales) {
	if (esBackSpace(e))
		return true;
	// numero de decimales actuales
	var nuDecimales = 0;
	var nuEnteros = 0;
	var indexCaracterFlotante = campo.value.indexOf(caracterFlotante());
	if (indexCaracterFlotante != -1) {
		nuDecimales = campo.value.length - indexCaracterFlotante - 1;
		nuEnteros = indexCaracterFlotante - 1;
	} else
		nuEnteros = campo.value.length;

	if (esEntero(e) && nuEnteros < enteros && nuDecimales < decimales)
		return true;
	if (esCaracterFlotante(e) && indexCaracterFlotante == -1)
		return true;
	return false;
}
function esCalificacionValida(campo, e, enteros, decimales) {

	if (esBackSpace(e))
		return true;
	// numero de decimales actuales
	var nuDecimales = 0;
	var nuEnteros = 0;
	var indexCaracterFlotante = campo.value.indexOf(caracterPunto());
	if (indexCaracterFlotante != -1) {
		nuDecimales = campo.value.length - indexCaracterFlotante - 1;
		nuEnteros = indexCaracterFlotante - 1;
	} else
		nuEnteros = campo.value.length;

	if (esEntero(e) && nuEnteros < enteros && nuDecimales < decimales)
		return true;
	if (esCaracterPunto(e) && indexCaracterFlotante == -1)
		return true;
	return false;

}
/**
 * Valida que el valor de un campo numero identifiacion sea valido para un tipo
 * de identificacion diferente a C.C
 * 
 * @param campo
 *            Campo de texto donde se gener� el evento de teclado
 * @param e
 *            Evento del teclado sobre el campo de texto que se quiere validar
 */
function esNumIdNoCCValido(campo, e, longitud) {
	if (esBackSpace(e)) {
		return true;
	}
	if (esCaracterFlotante(e) == false && esCaracterNegativo(e) == false
			&& esCaracterEspacio(e) == false && esCaracterPunto(e) == false) {
		return esLongitud(e, campo, longitud - 1);
	} else {
		return false;
	}

}
/**
 * Indica si la tecla presionada es "-"
 */
function esCaracterNegativo(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return caracterNegativo() == String.fromCharCode(unicode)
}
/**
 * Indica si la tecla presionada es "."
 */
function esCaracterPunto(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return caracterPunto() == String.fromCharCode(unicode)
}
/**
 * Indica si la tecla presionada es un espacio
 */
function esCaracterEspacio(e) {
	var unicode = e.charCode ? e.charCode : e.keyCode
	return caracterEspacio() == String.fromCharCode(unicode)
}
/**
 * Valida la sintaxis de la descripcion de un item
 */
function esDescripcionItem(campo, e) {
	if (esBackSpace(e))
		return true;

	if (esEspacio(e)) {
		// no permite espacios en blanco a la izquierda
		if (campo.value.length == 0)
			return false;
		// no permite m�s de un espacio en blanco
		if (campo.value.charAt(campo.value.length - 1) == ' ')
			return false;
	} else if (esCadenaValida(String.fromCharCode(e.charCode).toUpperCase(),
			"QWERTYUIOPASDFGHJKL�ZXCVBNM_()-")) {
		// la primera letra de todas las palabras es may�scula
		if (campo.value.length == 0
				|| campo.value.charAt(campo.value.length - 1) == ' ')
			campo.value += String.fromCharCode(e.charCode).toUpperCase();
		else
			campo.value += String.fromCharCode(e.charCode).toLowerCase();
		return false;

	}
	return true;
}

/**
 * Indica si una cadena de texto contiene solo un conjunto de caracteres dado
 * 
 * @param text
 *            La cadena de texto
 * @param caracteresValidos
 *            Conjunto de caracteres v�lidos
 */
function esCadenaValida(text, caracteresValidos) {
	for (i = 0; i < text.length; i++) {
		var caracter = text.charAt(i);
		if (caracteresValidos.indexOf(caracter) == -1)
			return false;
	}
	return true;
}

/**
 * Valida que el caracter digitado en un evento de teclado no sea un d�gito
 * 
 * @param e
 *            Evento de teclado
 */
function noDigitos(event) {
	return !esCadenaValida(getCaracterEvento(event), "1234567890");
}

/**
 * Valida la sintaxis del nombre de un item
 */
function esNombreItem(campo, event) {
	return noDigitos(event) && esDescripcionItem(campo, event);
}

/**
 * Retorna el caracter que representa la tecla orpimida y registrada en un
 * evento
 * 
 * @param event
 *            El evento generado
 */
function getCaracterEvento(event) {
	var browser = whichBrs();
	if (browser == "Firefox")
		return String.fromCharCode(event.charCode).toUpperCase();
	else
		return String.fromCharCode(event.keyCode).toUpperCase();
}

// Browser Detection Javascript
// copyright 1 February 2003, by Stephen Chapman, Felgall Pty Ltd

// You have permission to copy and use this javascript provided that
// the content of the script is not changed in any way.

function whichBrs() {
	var agt = navigator.userAgent.toLowerCase();
	if (agt.indexOf("opera") != -1)
		return 'Opera';
	if (agt.indexOf("staroffice") != -1)
		return 'Star Office';
	if (agt.indexOf("webtv") != -1)
		return 'WebTV';
	if (agt.indexOf("beonex") != -1)
		return 'Beonex';
	if (agt.indexOf("chimera") != -1)
		return 'Chimera';
	if (agt.indexOf("netpositive") != -1)
		return 'NetPositive';
	if (agt.indexOf("phoenix") != -1)
		return 'Phoenix';
	if (agt.indexOf("firefox") != -1)
		return 'Firefox';
	if (agt.indexOf("safari") != -1)
		return 'Safari';
	if (agt.indexOf("skipstone") != -1)
		return 'SkipStone';
	if (agt.indexOf("msie") != -1)
		return 'Internet Explorer';
	if (agt.indexOf("netscape") != -1)
		return 'Netscape';
	if (agt.indexOf("mozilla/5.0") != -1)
		return 'Mozilla';
	if (agt.indexOf('\/') != -1) {
		if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
			return navigator.userAgent.substr(0, agt.indexOf('\/'));
		} else
			return 'Netscape';
	} else if (agt.indexOf(' ') != -1)
		return navigator.userAgent.substr(0, agt.indexOf(' '));
	else
		return navigator.userAgent;
}

function tipoIdentificacion(e, idt) {
	var ti = document.getElementById(idt).value;

	if (ti == "1") {
		return esEntero(e);
	} else {
		return true;
	}
}
/**
 * Funcion que cuenta el n�mero de guiones de un campo, no permite m�s de uno
 */
function esGuion(event, id, longitud) {
	var unicode = event.charCode ? event.charCode : event.keyCode
	var ti = document.getElementById(id).value;
	var cont = 0;
	if (ti.length <= longitud) {
		if (esEntero(event)) {
			return true;
		} else {
			if (unicode == 45) {
				for (i = 0; i < ti.length; i++) {
					var c = ti.charAt(i);
					if (c == "-") {
						cont = cont + 1;
					}
				}
				if (cont >= 1) {
					return false;
				}
			} else {
				return false;
			}
		}
	} else {
		return false;
	}
	return true;
}

// Every single key press action will call this function.
function shouldCancelbackspace(e) {
	var key;
	var readOnly = false;
	if (e) {
		key = e.which ? e.which : e.keyCode;
		if (key == null || (key != 8 && key != 13)) { // return when the key
			// is not backspace key.
			return false;
		}
	} else {
		return false;
	}

	if (e.srcElement) { // in IE
		tag = e.srcElement.tagName.toUpperCase();
		type = e.srcElement.type;
		readOnly = e.srcElement.readOnly;
		if (type == null) { // Type is null means the mouse focus on a non-form
			// field. disable backspace button
			return true;
		} else {
			type = e.srcElement.type.toUpperCase();
		}
	} else { // in FF
		tag = e.target.nodeName.toUpperCase();
		type = (e.target.type) ? e.target.type.toUpperCase() : "";
	}

	// we don't want to cancel the keypress (ever) if we are in an input/text
	// area
	if (tag == 'INPUT' || type == 'TEXT' || type == 'TEXTAREA') {
		if (readOnly == true) // if the field has been dsabled, disbale the
			// back space button
			return true;
		if (((tag == 'INPUT' && type == 'RADIO') || (tag == 'INPUT' && type == 'CHECKBOX'))
				&& (key == 8 || key == 13)) {
			return true; // the mouse is on the radio button/checkbox,
			// disable the backspace button
		}
		return false;
	}

	// if we are not in one of the above things, then we want to cancel (true)
	// if backspace
	return (key == 8 || key == 13);
}

if (typeof window.event != 'undefined') { // IE
	document.onkeydown = function() {
		return !shouldCancelbackspace(event);
	};
} else { // FF
	document.onkeypress = function(e) {
		return !shouldCancelbackspace(e);
	};
}

// Función que convierte a mayusculas la información ingresada en las cajas de
// texto y textAreas
function pasoAMayusculas(campo) {
	campo.value = campo.value.toUpperCase();
	campo.value = campo.value.replace(/^\\s*|\\s*$/g, '');
	return campo.value;
}

// Formatea el numero con comas y decimales
function asignarFormatoMoneda(obj) {
	if (obj != null && obj != '') {
		valor = obj.value;
		valor += '';
		var x = valor.split('.');
		var x1 = x[0];
		var x2 = x.length > 1 ? '.' + x[1] : '';
		var x5 = "";
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		if (x2.length > 2)
			x5 = x2.substring(0, 3);

		obj.value = x1 + x5;
	}
}

// Formatea el numero con comas y decimales
function asignarFormatoPorcent(obj) {
	if (obj != null && obj != '' && obj.value != '') {
		valor = obj.value;
		var x = valor.split('.');
		var format = "";
		var result = "";
		if (x.length == 2) {
			var x1 = x[0];
			if (x1 == '') {
				x1 = 0;
			}
			var x2 = x[1];
			if (x2 == '') {
				result = '00';
			} else {
				var x2num = parseFloat(x2);
				result = Math.round(x2num * 100) / 100;
			}
			format = x1 + '.' + result;
		} else if (x.length == 1) {
			format = valor + '.00';
		}
		obj.value = format;
	}
}

// Formatea el numero con comas y decimales
function asignarFormato(tipoFormato, obj) {
	if (obj != null && obj != '') {
		valor = obj.value;
		switch (tipoFormato) {
		case "PORC": // porcentaje
			// asignarFormatoPorcent(obj);
			break;
		case "MONEDA": // moneda miles con ',' y decimales con
			// '.'
			asignarFormatoMoneda(obj);
			break;
		}
	}
}

// Remueve los caracteres no numericos
function quitarFormatoMoneda(obj) {
	if (obj != null && obj != '') {
		var str = obj.value;
		str += '';
		var rgx = /^\d|\.|-$/;
		var out = '';
		for (var i = 0; i < str.length; i++) {
			if (rgx.test(str.charAt(i))) {
				if (!((str.charAt(i) == '.' && out.indexOf('.') != -1) || (str
						.charAt(i) == '-' && out.length != 0))) {
					out += str.charAt(i);
				}
			}
		}
		obj.value = out;
	}
}

// -------------------------------
// Funcion para validar el ingreso de datos por teclado
// -------------------------------

function validarTecla(event, li_tipo, objeto) {
	var li_kc = getKeyCode(event);
	var CTRL = event.ctrlKey;
	var V_KEY = 118;
	var V_LOWERCASE_KEY = 86;

	if ((CTRL && li_kc == V_KEY) || (CTRL && li_kc == V_LOWERCASE_KEY)) {
		return true;
	}

	switch (li_tipo) {
	case "ALF": // Evita caracteres de control, ", ', &, %, ?, ?, `
		if (li_kc == 39 || li_kc == 34 || li_kc < 32 || li_kc == 38
				|| li_kc == 37 || li_kc == 63 || li_kc == 18 || li_kc == 192)
			continuarEvento(event, false);
		break;
	case "ESP": // Evita caracteres de control, ", ', &, %, ? y espacios
		if (li_kc == 32 || li_kc == 39 || li_kc == 34 || li_kc < 32
				|| li_kc == 38 || li_kc == 37 || li_kc == 63)
			continuarEvento(event, false);
		break;
	case "FEC": // Solo permite numeros y /
		if ((li_kc < 48 || li_kc > 57) && (li_kc != 47))
			continuarEvento(event, false);
		validaFormatoFecha(objeto, event);
		break;
	case "NUM": // Solo permite numeros
		if (li_kc < 48 || li_kc > 57)
			continuarEvento(event, false);
		break;
	case "ENT": // Solo permite numeros y -
		if ((li_kc < 48 || li_kc > 57) && (li_kc != 45))
			continuarEvento(event, false);
		break;
	case "DEC": // Solo permite numeros, - y punto .
		if ((li_kc < 48 || li_kc > 57) && (li_kc != 45) && (li_kc != 46))
			continuarEvento(event, false);
		break;
	case "DECP": // Solo permite numeros, y punto .
		if ((li_kc < 48 || li_kc > 57) && (li_kc != 46))
			continuarEvento(event, false);
		break;
	case "NOM": // Evita caracteres de control
		if (li_kc < 32)
			continuarEvento(event, false);
		break;
	case "HOR": // Solo permite numeros y :
		if (li_kc < 48 || li_kc > 58)
			continuarEvento(event, false);
		break;
	case "ARC": // No permite /\:*?"<>|
		if ((li_kc >= 33 && li_kc <= 45) || (li_kc > 46 && li_kc <= 47)
				|| (li_kc >= 58 && li_kc <= 63)
				|| (li_kc >= 123 && li_kc <= 127) || (li_kc == 32))
			continuarEvento(event, false);
		break;
	case "EMA": // Evita caracteres de control, ", ', &, %, ? , ESP
		if (li_kc == 39 || li_kc == 34 || li_kc <= 32 || li_kc == 38
				|| li_kc == 37 || li_kc == 63)
			continuarEvento(event, false);
		break;
	case "ALM": // Evita caracteres de control, ", ', &, %, ?
		if (li_kc == 39 || li_kc == 34 || li_kc < 32 || li_kc == 38
				|| li_kc == 63)
			continuarEvento(event, false);
		break;
	}
}

function getKeyCode(evt) {
	if (window.event)
		return evt.keyCode;
	else
		return evt.which;
}

function continuarEvento(evt, continuar) {
	if (evt.keyCode) {
		evt.returnValue = false;
	} else if (evt.preventDefault && !continuar) {
		evt.preventDefault();
		evt.stopPropagation();
	}
	return continuar;
}

function validaFormatoFecha(objeto, event) {

	if (event.keyCode == 008) {
		return true;
	} else {
		var pK = event ? objeto.which : window.event.keyCode;
		if (pK == 8) {
			objeto.value = substr(0, objeto.value.length - 1);
			return;
		}
		var dt = objeto.value;
		var da = dt.split('/');
		for (var a = 0; da.length > a; a++) {
			if (da[a] != +da[a])
				da[a] = da[a].substr(0, da[a].length - 1);
		}
		if (da[0] > 31) {
			da[1] = da[0].substr(da[0].length - 1, 1);
			da[0] = '0' + da[0].substr(0, da[0].length - 1);
		}
		if (da[1] > 12) {
			da[2] = da[1].substr(da[1].length - 1, 1);
			da[1] = '0' + da[1].substr(0, da[1].length - 1);
		}
		if (da[2] > 9999)
			da[1] = da[2].substr(0, da[2].length - 1);
		dt = da.join('/');
		if (dt.length == 2 || dt.length == 5)
			dt += '/';

		objeto.value = dt;
	}

}

function validateAlphaNumeric(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	var regex = /^[0-9a-zA-Z]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault)
			theEvent.preventDefault();
	}
}
function validateDecimalNumericSlash(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	var regex = /[0-9/]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault)
			theEvent.preventDefault();
	}
}
function validateDecimalNumeric(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	var regex = /[0-9]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault)
			theEvent.preventDefault();
	}
}
function validateNumeric(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	if (isNaN(String.fromCharCode(evt.keyCode))) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault)
			theEvent.preventDefault();
	}
}
function formatoMiles(input) {
	var num = input.value.replace(/\./g, '');
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		input.value = num;
	}
}