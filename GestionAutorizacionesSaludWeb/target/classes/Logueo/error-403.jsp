<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Error 403</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="<%= basePath %>css/estilo_sos-v2.css"/>
  </head>
  <body>
   	<center>
	   <div class="header"></div>
	   <br/>
	   <b>
	   No tiene autorización para entrar en la aplicación
	   </b>
	   <br/><br/>
	   <form action="<%= basePath %>Logueo/login.jsp">
	   		<input type="submit" value="OK" class="aceptar">
	   </form>
	 </center>
  </body>
</html>
