function formatSelect(repo) {
	if (repo.loading) {
		return repo.text;
	}
	return repo.codigo + " - " + repo.descripcion;
}

function formatTipoPlanSelection(tipoPlan) {

	return tipoPlan.codigo + " - " + tipoPlan.descripcion;
}

function limpiarCampoTipoPlan() {
	jQuery(".selectTipoPlan").each(function() {
		jQuery(this).val("");
	});
}

function formatTipoAfiliadoSelection(tipoAfiliado) {

	return tipoAfiliado.codigo + " - " + tipoAfiliado.descripcion;
}

function limpiarCampoTipoAfiliado() {
	jQuery(".selectTipoAfiliado").each(function() {
		jQuery(this).val("");
	});
}

function formatEstadosMegaSelection(estadosMega) {

	return estadosMega.codigo + " - " + estadosMega.descripcion;
}

function limpiarCampoEstadosMega() {

	jQuery(".selectEstadosMega").each(function() {
		jQuery(this).val("");
	});
}

function formatSedeSelection(sede) {

	return sede.codigo + " - " + sede.descripcion;
}

function limpiarCampoSede() {
	jQuery(".selectSede").each(function() {
		jQuery(this).val("");
	});
}

function formatGruposEntregaSelection(gruposEntrega) {
	return gruposEntrega.codigo + " - " + gruposEntrega.descripcion;
}

function limpiarCampoGruposEntrega() {
	jQuery(".selectGruposEntrega").each(function() {
		jQuery(this).val("");
	});
}
function initListasTipoPlan() {
	jQuery.ajax({
		url : './../../MultiSelectServlet',
		dataType : 'json',
		data : {
			fr : 'tp'
		},
		success : function(responseText) {
			jQuery("#selTipoPlan").select2({
				data : responseText,
				language : "es",
				templateResult : formatSelect,
				templateSelection : formatTipoPlanSelection
			});
		}
	});
}
function initListasTipoAfiliado() {
	jQuery.ajax({
		url : './../../MultiSelectServlet',
		dataType : 'json',
		data : {
			fr : 'ta'
		},
		success : function(responseText) {
			jQuery(".selectTipoAfiliado").select2({
				data : responseText,
				language : "es",
				templateResult : formatSelect,
				templateSelection : formatTipoAfiliadoSelection
			});
		}
	});
}
function initListasEstadosMega() {
	jQuery.ajax({
		url : './../../MultiSelectServlet',
		dataType : 'json',
		data : {
			fr : 'em'
		},
		success : function(responseText) {
			jQuery(".selectEstadosMega").select2({
				data : responseText,
				language : "es",
				templateResult : formatSelect,
				templateSelection : formatEstadosMegaSelection
			});
		}
	});

}
function initListasSede() {
	jQuery.ajax({
		url : './../../MultiSelectServlet',
		dataType : 'json',
		data : {
			fr : 'se'
		},
		success : function(responseText) {
			jQuery(".selectSede").select2({
				data : responseText,
				language : "es",
				templateResult : formatSelect,
				templateSelection : formatSedeSelection
			});
		}
	});

}
function initListasGruposEntrega() {
	jQuery.ajax({
		url : './../../MultiSelectServlet',
		dataType : 'json',
		data : {
			fr : 'ge'
		},
		success : function(responseText) {
			jQuery(".selectGrupoEntrega").select2({
				data : responseText,
				language : "es",
				templateResult : formatSelect,
				templateSelection : formatGruposEntregaSelection
			});
		}
	});

}

function setValues() {
	jQuery(".consTipoPlan").val(jQuery(".selTipoPlan").val());
	jQuery(".consTipoAfiliado").val(jQuery(".selectTipoAfiliado").val());
	jQuery(".consEstadosMega").val(jQuery(".selectEstadosMega").val());
	jQuery(".consSede").val(jQuery(".selectSede").val());
	jQuery(".consGruposEntrega").val(jQuery(".selectGrupoEntrega").val());

}

jQuery(document).ready(function() {
	initListasTipoPlan();
	initListasTipoAfiliado();
	initListasEstadosMega();
	initListasSede();
	initListasGruposEntrega();
});
