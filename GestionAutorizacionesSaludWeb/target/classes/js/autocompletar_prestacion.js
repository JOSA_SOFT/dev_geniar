function formatPrestacion(repo) {
	if (repo.loading) {
		return repo.text;
	}
	return repo.codigo + " - " + repo.descripcion;
}

function formatPrestacionSelection(prestacion) {
	jQuery(".consPrestacion").val(prestacion.id);
	return prestacion.codigo + " - " + prestacion.descripcion;
}

function limpiarCampo(){
	//jQuery("#prestacion").val("");
	jQuery(".selectPrestacion").each(function(){
		jQuery(this).val("");
	});
}

function initListas(){
	jQuery(".selectPrestacion").each(function(){
		var id = jQuery(this).data("id");
		jQuery(this).select2({
			ajax : {
				url : "./../../PrestacionesDataSourceServlet",
				type : 'POST',
				dataType : 'json',
				delay : 250,
				data : function(params) {
					return {
						q : params.term, // search term
						page : params.page,
						fr : 'ac',
						tp : jQuery('.tipoPrestacionClass'+id).val(),
						cn: jQuery('#formConsultarSolicitud\\:filtraCupsNorma'+id).is(":checked") ? 1 : 0
					};
				},
				processResults : function(data, params) {
					// Indica que se puede utilizar desplazamiento infinito
					params.page = params.page || 1;
					return {
						results : data,
						pagination : {
							more : (params.page * 30) < data.total_count
						}
					};
				},
				cache : true
			},
			language : "es",
			minimumInputLength : 3,
			templateResult : formatPrestacion,
			templateSelection : formatPrestacionSelection
		});
	});
}

jQuery(document).ready(function() {
	initListas();
});