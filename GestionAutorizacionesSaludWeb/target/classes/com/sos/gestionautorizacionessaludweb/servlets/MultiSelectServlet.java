package com.sos.gestionautorizacionessaludweb.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.EstadoMegaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GrupoEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ModificaFechasEntregaOPSController;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

public class MultiSelectServlet extends HttpServlet implements Serializable {
	private static final long serialVersionUID = 1027338367224439099L;
	private static final String PARAMETRO_TIPO_PLAN = "tp";
	private static final String PARAMETRO_TIPO_AFILIADO = "ta";
	private static final String PARAMETRO_ESTADOS_MEGA = "em";
	private static final String PARAMETRO_SEDE = "se";
	private static final String PARAMETRO_GRUPO_ENTREGA = "ge";

	private static final String PARAMETRO_FORMATO_RESPUESTA = "fr";

	private static final String ENCODING_UTF_8 = "UTF-8";
	private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

	private static final Logger LOGGER = Logger.getLogger(MultiSelectServlet.class);

	public MultiSelectServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType(CONTENT_TYPE_APPLICATION_JSON);
		response.setCharacterEncoding(ENCODING_UTF_8);

		try {
			ModificaFechasEntregaOPSController modificaFechasEntregaOPSController = new ModificaFechasEntregaOPSController();
			String parametroRespuesta = request.getParameter(PARAMETRO_FORMATO_RESPUESTA);

			Gson gson = new Gson();
			JsonWriter writer = new JsonWriter(response.getWriter());
			writer.beginArray();

			Date fechaActual = Utilidades.getFechaActual();
			// se valida que quien hizo la solicitud
			if (PARAMETRO_TIPO_PLAN.equals(parametroRespuesta)) {
				List<PlanVO> lPlan = modificaFechasEntregaOPSController.consultarListaTiposPlan(fechaActual);
				procesarListadoTipoPlan(lPlan, gson, writer);

			} else if (PARAMETRO_TIPO_AFILIADO.equals(parametroRespuesta)) {

				List<AfiliadoVO> lTipoAfiliado = modificaFechasEntregaOPSController.consultarListaTiposAfiliado(null,
						fechaActual, null, null);
				procesarListadoTipoAfiliado(lTipoAfiliado, gson, writer);
			} else if (PARAMETRO_ESTADOS_MEGA.equals(parametroRespuesta)) {
				List<EstadoMegaVO> lEstadosMega = modificaFechasEntregaOPSController
						.consultarListaEstadosMega(fechaActual);
				procesarListadoEstadosMega(lEstadosMega, gson, writer);
			} else if (PARAMETRO_SEDE.equals(parametroRespuesta)) {
				CargarCombosController combos = new CargarCombosController();
				List<ParametroVO> lSede = combos.consultaSedes(null, fechaActual, null, null);
				procesarListadoSede(lSede, gson, writer);
			} else if (PARAMETRO_GRUPO_ENTREGA.equals(parametroRespuesta)) {

				List<GrupoEntregaVO> lGrupoEntrega = modificaFechasEntregaOPSController
						.consultarListaGruposEntrega(fechaActual);
				procesarListadoGrupoEntrega(lGrupoEntrega, gson, writer);
			}

			writer.endArray();
			writer.flush();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			response.setStatus(500);
		}
	}

	private void procesarListadoTipoPlan(List<PlanVO> lTipoPlan, Gson gson, JsonWriter writer) {
		if (lTipoPlan != null) {
			for (PlanVO vo : lTipoPlan) {
				AutocompletarVO vo2 = new AutocompletarVO(vo.getConsectivoPlan(), vo.getCodigoPlan(),
						vo.getDescripcionPlan());
				gson.toJson(vo2, AutocompletarVO.class, writer);
			}
		}
	}

	private void procesarListadoTipoAfiliado(List<AfiliadoVO> lTipoAfiliado, Gson gson, JsonWriter writer) {
		if (lTipoAfiliado != null) {
			for (AfiliadoVO vo : lTipoAfiliado) {
				AutocompletarVO vo2 = new AutocompletarVO(vo.getConsecutivoTipoAfiliado(), vo.getCodigoTipoAfiliado(),
						vo.getDescripcionTipoAfiliado());
				gson.toJson(vo2, AutocompletarVO.class, writer);
			}
		}
	}

	private void procesarListadoEstadosMega(List<EstadoMegaVO> lEstadoMega, Gson gson, JsonWriter writer) {
		if (lEstadoMega != null) {
			for (EstadoMegaVO vo : lEstadoMega) {
				AutocompletarVO vo2 = new AutocompletarVO(vo.getConsecutivoCodigoEstadoMega(), vo.getCodigoEstadoMega(),
						vo.getDescripcionEstadoMega());

				gson.toJson(vo2, AutocompletarVO.class, writer);
			}
		}
	}

	private void procesarListadoSede(List<ParametroVO> lSedes, Gson gson, JsonWriter writer) {
		if (lSedes != null) {
			for (ParametroVO vo : lSedes) {
				AutocompletarVO vo2 = new AutocompletarVO(vo.getConsecutivo(), vo.getCodigo(), vo.getDescipcion());
				gson.toJson(vo2, AutocompletarVO.class, writer);
			}
		}
	}

	private void procesarListadoGrupoEntrega(List<GrupoEntregaVO> lGrupoEntrega, Gson gson, JsonWriter writer) {
		if (lGrupoEntrega != null) {
			for (GrupoEntregaVO vo : lGrupoEntrega) {
				AutocompletarVO vo2 = new AutocompletarVO(vo.getConsecutivoCodigoGrupoEntrega(),
						vo.getCodigoGrupoEntrega(), vo.getDescripcionGrupoEntrega());
				gson.toJson(vo2, AutocompletarVO.class, writer);
			}
		}
	}
}
