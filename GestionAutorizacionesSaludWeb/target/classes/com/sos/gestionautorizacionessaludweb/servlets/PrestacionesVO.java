package com.sos.gestionautorizacionessaludweb.servlets;

import java.io.Serializable;

public class PrestacionesVO implements Serializable {
	private static final long serialVersionUID = 5784108220596788693L;
	private Integer id;
	private String codigo;
	private String descripcion;
	
	public PrestacionesVO(Integer id, String codigo, String descripcion) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
