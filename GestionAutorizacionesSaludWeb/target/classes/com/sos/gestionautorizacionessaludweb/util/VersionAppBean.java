package com.sos.gestionautorizacionessaludweb.util;


import java.io.Serializable;
import java.net.InetAddress;


/**
 * The Class VersionAppBean.
 * 
 * @author hcuenca
 * @version 28/11/2014
 * @type: Clase
 * @name: VersionAppBean
 * @description: Clase encargada de ser la controladora de aspectos relacionados
 *               con la version dela aplicacion
 */
public class VersionAppBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7316100275043010544L;

	/**
	 * The version app.
	 * 
	 * @author hcuenca
	 * @version 28/11/2014
	 * @type: Atributo
	 * @name: versionApp
	 * @description: Variable de clase encargada de capturar y almacenar el
	 *               valor de la version de la App
	 */
	private String versionApp;


	/**
	 * Instantiates a new version app bean.
	 * 
	 * @author hcuenca
	 * @version 11/10/2014
	 * @type: Constructor
	 * @name: GenerarFormatosComunicadosBean
	 * @description: Constructor por defecto, de la clase
	 *               GenerarFormatosComunicadosBean
	 */
	public VersionAppBean() {

	}

	/**
	 * Gets the version app.
	 * 
	 * @author hcuenca
	 * @version 28/11/2014
	 * @return String: Retorna el valor del atributo de clase: versionApp
	 * @type: Comportamiento
	 * @name: getVersionApp
	 * @description: Metodo get del atributo de clase: versionApp
	 */
	public String getVersionApp() {
		versionApp = ConstantesWeb.VERSION + "-"+ getHostName();		
		return versionApp;
	}

	/**
	 * Sets the version app.
	 * 
	 * @author hcuenca
	 * @version 28/11/2014
	 * @param versionApp
	 *            the new version app
	 * @type: Comportamiento
	 * @name: setVersionApp
	 * @description: Metodo set del atributo de clase: versionApp
	 */
	public void setVersionApp(String versionApp) {
		this.versionApp = versionApp;
	}

	
	
	public String getHostName() {
		String host="";
		try{
			host = InetAddress.getLocalHost().getHostName();
		} catch (final Exception e) { /* no-op */
			host = "Not Found";
		}
		return host;
	}
}
