package com.sos.gestionautorizacionessaludweb.util;

import org.apache.log4j.Logger;
import com.sos.excepciones.LogicException;

public class ControladorExcepcionesLogic {

	/** La Constante log. */
	private static final Logger log = Logger.getLogger(ControladorExcepcionesLogic.class);

	public static void validarExcepcion(Object ex) {

		String mensaje = ((Throwable) ex).getMessage();

		if (ex instanceof LogicException) {
			FacesUtils.mostrarMensaje("red", mensaje);
			log.debug(((LogicException) ex).getMessage());
		} else {
			if (ex instanceof Exception) {
				FacesUtils.mostrarMensaje("red", "Excepcion Inesperada");
				log.error(((Exception) ex).getMessage(), (Throwable) ex);
			}
		}
	}

}
