package com.sos.gestionautorizacionessaludweb.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TutelasAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class Utilitaria para la consulta de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * @author Ing. Victor Hugo Gil
 * @version 26/07/2016
 *
 */
public class IngresoSolicitudesUtil {
	
	private IngresoSolicitudesUtil(){
		/*clase utilizaría constructor privado para evitar instancias */		
	}
		
	/**
	 * metodo que permite la validacion de la lista de parametros generales
	 * @throws LogicException
	 */
	
	public static List<ParametrosGeneralesVO> validarConsultaParametrosGenerales(List<ParametrosGeneralesVO> listaParametrosGeneralesVOs){
		
		List<ParametrosGeneralesVO> listaParametrosGenerales = listaParametrosGeneralesVOs;
		
		if (listaParametrosGenerales == null) {
			listaParametrosGenerales = new ArrayList<ParametrosGeneralesVO>();
		}		
		return listaParametrosGenerales;
	}	
	
	/**
	 * metodo que permite la validacion del valor minimo de corte para hospitalizacion
	 * @throws LogicException
	 */
	public static int obtenerValoresCorteCuentaMinimo(List<ParametrosGeneralesVO> listaParametrosGenerales) throws LogicException {
		Integer minimoCorteHospitalizacion = null;
		for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGenerales) {
			if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == ConstantesWeb.PARAMETRO_GENERAL_CORTE_CUENTA_MINIMO) {
				minimoCorteHospitalizacion = parametrosGeneralesVO.getValorParametroGeneral().intValue();
			}
		}		
		return minimoCorteHospitalizacion;
	}
	
	/**
	 * metodo que permite la validacion del valor minimo de corte para hospitalizacion
	 * @throws LogicException
	 */
	public static int obtenerValoresCorteCuentaMaximo(List<ParametrosGeneralesVO> listaParametrosGenerales) throws LogicException {
		Integer maximoCorteHospitalizacion = null;
		for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGenerales) {
			if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == ConstantesWeb.PARAMETRO_GENERAL_CORTE_CUENTA_MAXIMO) {
				maximoCorteHospitalizacion = parametrosGeneralesVO.getValorParametroGeneral().intValue();
			}
		}		
		return maximoCorteHospitalizacion;
	}
	
	/**
	 * metodo que permite la validacion del valor minimo de corte para hospitalizacion
	 * @throws LogicException
	 */
	public static int aplicaTutela(List<TutelasAfiliadoVO> listaTutelasAfiliadoVO) throws LogicException {
		Integer tutela = null;
		
		if (listaTutelasAfiliadoVO != null && !listaTutelasAfiliadoVO.isEmpty()) {
			tutela = ConstantesWeb.TUTELA_SI;
		} else {
			tutela = ConstantesWeb.TUTELA_NO;
		}
			
		return tutela;
	}	
	
	/**
	 * metodo que permite la validacion del tamaño de la lista de medicos
	 * @throws LogicException
	 */
	public static boolean validarTamanoLista(java.util.List<?> lista){
		boolean aux = false;
		
		if(lista != null && !lista.isEmpty()){
			if(lista.size() > ConstantesWeb.LISTAS_SIZE){
				aux = true;
			}
		}
		return aux;	
	}
		
	/** Metodo que permite realizar la validacion de las listas */
	public static boolean validarDatosList(java.util.List<?> lista){
		boolean aux = false;
		
		if (lista != null && !lista.isEmpty()) {
			 aux = true;
		}
		return aux;
	}	
	
	/** Metodo que permite validar la fecha de egreso
	 *  @throws LogicException
	 */
	public static void validarFechaEgreso(HospitalizacionVO hospitalizacionVO) throws LogicException{
		if (hospitalizacionVO.getFechaEgreso().compareTo(hospitalizacionVO.getFechaIngreso()) < 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS_HOSPITALIZACION), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/** Metodo que permite validar la fecha de egreso
	 *  @throws LogicException
	 */	
	public static void validarCorteCuentaMinimo(BigInteger intcorteCuenta, int minimoCorteHospitalizacion) throws LogicException{
		if (intcorteCuenta.intValue() <= minimoCorteHospitalizacion) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_CORTE_CUENTA) + minimoCorteHospitalizacion, ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/** Metodo que permite validar un concepto en cero para la liquidacion de una prestacion
	 *  @throws LogicException
	 */	
	public static boolean validarConceptoCero(List<ConceptosGastoVO> listaConcepto) throws LogicException{
		boolean esConceptoCero = false;
		for(ConceptosGastoVO concepto : listaConcepto){
			if(concepto.getValorPrestacion() == 0){
				esConceptoCero = true;
				break;
			}
		}
		return esConceptoCero;
	}
}
