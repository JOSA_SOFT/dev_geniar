package com.sos.gestionautorizacionessaludweb.util;

import java.util.ArrayList;
import java.util.List;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;


/**
 * Class Utilitaria para el ingreso de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * para los diagnosticos
 * @author Ing. Victor Hugo Gil
 * @version 26/07/2016
 *
 */
public class IngresoSolicitudesDiagnosticosUtil {
	
	private IngresoSolicitudesDiagnosticosUtil(){
		/*clase utilizaría constructor privado para evitar instancias */		
	}	
	
	/**
	 * metodo que permite llenar la lista de los diagnosticos
	 * @throws LogicException
	 */
	public static List<DiagnosticosVO> llenarListaDiagnosticoPorCodigo(String txtCodigoDiagnostico, String txtDescripcionDiagnostico, RegistrarSolicitudController registrarSolicitudesController) throws LogicException {
		List<DiagnosticosVO> listaDiagnosticosEncontrados = new ArrayList<DiagnosticosVO>();
				
		if (txtCodigoDiagnostico != null && !txtCodigoDiagnostico.isEmpty()) {
			listaDiagnosticosEncontrados = registrarSolicitudesController.consultaDiagnosticosxCodigo(txtCodigoDiagnostico);
		} else if (txtDescripcionDiagnostico != null && !txtDescripcionDiagnostico.isEmpty()) {
			listaDiagnosticosEncontrados = registrarSolicitudesController.consultaDiagnosticosxDescripcion(txtDescripcionDiagnostico);
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO), ErrorType.PARAMETRO_ERRADO);
		}
		
		return listaDiagnosticosEncontrados;
	}
	

	/**
	 * metodo que permite obtener validar el tamaño de la lista de llos diagnosticos
	 * @throws LogicException
	 */
	public static boolean validarListaDiagnosticosEncontrados(List<DiagnosticosVO> listaDiagnosticosEncontrados) {
		
		List<DiagnosticosVO> listaDiagnosticos = listaDiagnosticosEncontrados;
		boolean aux = false;
		
		if(listaDiagnosticos != null && !listaDiagnosticos.isEmpty()){
			if(listaDiagnosticosEncontrados.size() > ConstantesWeb.LISTAS_SIZE){
				aux = true;
			}
		}
		return aux;
	}	

	/**
	 * metodo que permite validar el tipo de diagnostico
	 * @throws LogicException
	 */
	public static void validarTipoDiagnostico(Integer consecutivoTipoDiagnostico) throws LogicException {
		if (consecutivoTipoDiagnostico == null || consecutivoTipoDiagnostico.intValue() == ConstantesWeb.VALOR_INICIAL || consecutivoTipoDiagnostico.intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_TIPO_DIAGNOSTICO), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * metodo que permite validar el consecutivo del diagnostico
	 * @throws LogicException
	 */
	public static void validarConsecutivoDiagnostico(DiagnosticosVO diagnosticoVoSeleccionado) throws LogicException {
		if (diagnosticoVoSeleccionado == null || diagnosticoVoSeleccionado.getConsecutivoCodigoDiagnostico() == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SELECCIONAR_DIAGNOSTICO), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * metodo que permite validar el listado de diagnosticos
	 * @throws LogicException
	 */
	public static void validarlistaDeDiagnosticosSeleccionados(List<DiagnosticoDTO> listaDiagnosticosSeleccionados, Integer consecutivoTipoDiagnostico, DiagnosticosVO diagnosticoVoSeleccionado) throws LogicException {
		for (DiagnosticoDTO diagnosticoDTOAux : listaDiagnosticosSeleccionados) {
			if (consecutivoTipoDiagnostico.compareTo(ConstantesWeb.DIAGNOSTICO_PRINCIPAL) == 0 && diagnosticoDTOAux.getTiposDiagnosticosVO().getConsecutivoTipoDiagnostico().compareTo(ConstantesWeb.DIAGNOSTICO_PRINCIPAL) == 0) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DIAGNOSTICO_PRINCIPAL), ErrorType.PARAMETRO_ERRADO);
			}

			if (diagnosticoVoSeleccionado.getConsecutivoCodigoDiagnostico().compareTo(diagnosticoDTOAux.getDiagnosticosVO().getConsecutivoCodigoDiagnostico()) == 0) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DIAGNOSTICO), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}
	
	/**
	 * metodo que permite adicionar los tipos de diagnostico
	 * @throws LogicException
	 */
	public static TiposDiagnosticosVO adicionarTipoDiagnostico(Integer consecutivoTipoDiagnostico, List<TiposDiagnosticosVO> listaTipoDiagnosticoVO, TiposDiagnosticosVO tiposDiagnosticosAdicionales){
		TiposDiagnosticosVO tiposDiagnosticos = tiposDiagnosticosAdicionales;
		List<TiposDiagnosticosVO> listaTipoDiagnostico = listaTipoDiagnosticoVO;
		
		for (TiposDiagnosticosVO tiposDiagnosticosVOAux : listaTipoDiagnostico) {
			if (consecutivoTipoDiagnostico.compareTo(tiposDiagnosticosVOAux.getConsecutivoTipoDiagnostico()) == 0) {
				tiposDiagnosticos.setConsecutivoTipoDiagnostico(consecutivoTipoDiagnostico);
				tiposDiagnosticos.setDescripcionTipoDiagnostico(tiposDiagnosticosVOAux.getDescripcionTipoDiagnostico());
				break;
			}
		}
		return tiposDiagnosticos;
	}	
	
	/**
	 * metodo que inicializa la hospitalizacion
	 * @throws LogicException
	 */
	public static HospitalizacionVO inicializarHospitalizacion() {				
		HospitalizacionVO hospitalizacionVO = new HospitalizacionVO();
		hospitalizacionVO.setServicioHospitalizacionVO(new ServicioHospitalizacionVO());
		hospitalizacionVO.setTipoHabitacionVO(new ClaseHabitacionVO());		
		hospitalizacionVO.setCorteCuenta(ConstantesWeb.CORTE_CUENTA_INI);
		return hospitalizacionVO;
	}
}
