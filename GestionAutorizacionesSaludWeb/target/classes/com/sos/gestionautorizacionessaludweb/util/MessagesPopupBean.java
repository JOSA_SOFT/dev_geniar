/*
 * @author Jorge Andres Garcia Erazo - Geniar S.A.S
 * @since 04/09/2013
 * @version 1.0
 */
package com.sos.gestionautorizacionessaludweb.util;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;



/**
 * Class MessagesPopupBean.
 *
 * @author sisddc01
 * @version 4/09/2012
 */
public class MessagesPopupBean implements Serializable {

	/** Constante serialVersionUID. */
	private static final long serialVersionUID = 3463993965199399525L;

	/** show. */
	private boolean show;

	/** title. */
	private String title;

	/** msgs. */
	private String[] msgs;

	/** info. */
	private boolean info;

	/** url. */
	private String url;
	
	/** The color. */
	private String color = "blue";
	
	/** show. */
	private boolean botonAceptar;

	/**
	 * Instancia nueva de messages popup bean.
	 */
	public MessagesPopupBean() {
		//Metodo en blanco
	}

	/**
	 * Show info.
	 *
	 * @param msgs the msgs
	 */
	public void showInfo(String msgs) {
		this.msgs = new String[] { msgs };
		info = true;
		show = true;
	}

	/**
	 * Show info.
	 *
	 * @param msgs the msgs
	 * @param url the url
	 */
	public void showInfo(String msgs, String url) {
		this.msgs = new String[] { msgs };
		info = true;
		show = true;
		this.url = url;
	}

	/**
	 * Show info.
	 *
	 * @param msgs the msgs
	 */
	public void showInfo(String[] msgs) {
		this.msgs = msgs;
		info = true;
		show = true;
	}

	/**
	 * Show info.
	 *
	 * @param msgs the msgs
	 */
	public void showInfo(List<String> msgs) {
		this.msgs = new String[msgs.size()];
		for (int i = 0; i < msgs.size(); i++) {
			this.msgs[i] = msgs.get(i);
		}
		info = true;
		show = true;
	}

	/**
	 * Show error.
	 *
	 * @param msgs the msgs
	 */
	public void showError(String msgs) {
		this.msgs = new String[] { msgs };
		info = false;
		show = true;
	}

	/**
	 * Show error.
	 *
	 * @param msgs the msgs
	 */
	public void showError(String[] msgs) {
		this.msgs = msgs;
		info = false;
		show = true;
	}

	/**
	 * Show error.
	 *
	 * @param msgs the msgs
	 */
	public void showError(List<String> msgs) {
		this.msgs = new String[msgs.size()];
		for (int i = 0; i < msgs.size(); i++) {
			this.msgs[i] = msgs.get(i);
		}
		info = false;
		show = true;
	}

	/**
	 * Close.
	 *
	 * @param event the event
	 */
	public void close(ActionEvent event) {
		msgs = null;
		title = "";
		info = false;
		show = false;
		((Confirmacion) FacesUtils.getManagedBean(ConstantesWeb.CONFIRMACION_BEAN))
				.close(null);

	}

	/**
	 * Close.
	 *
	 * @return string
	 */
	public String close() {
		msgs = null;
		title = "";
		info = false;
		show = false;
		((Confirmacion) FacesUtils.getManagedBean(ConstantesWeb.CONFIRMACION_BEAN))
				.close(null);
		return url;
	}

	/**
	 * Comprueba si es show.
	 *
	 * @return true, si es show
	 */
	public boolean isShow() {
		return show;
	}

	/**
	 * Establece show.
	 *
	 * @param show Nuevo show
	 */
	public void setShow(boolean show) {
		this.show = show;
	}

	/**
	 * Obtiene title.
	 *
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Establece title.
	 *
	 * @param title Nuevo title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Obtiene msgs.
	 *
	 * @return msgs
	 */
	public String[] getMsgs() {
		return msgs;
	}

	/**
	 * Establece msgs.
	 *
	 * @param msgs Nuevo msgs
	 */
	public void setMsgs(String[] msgs) {
		this.msgs = msgs;
	}

	/**
	 * Comprueba si es info.
	 *
	 * @return true, si es info
	 */
	public boolean isInfo() {
		return info;
	}

	/**
	 * Establece info.
	 *
	 * @param info Nuevo info
	 */
	public void setInfo(boolean info) {
		this.info = info;
	}

	/**
	 * Obtiene url.
	 *
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Establece url.
	 *
	 * @param url Nuevo url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Sets the color.
	 *
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the botonAceptar
	 */
	public boolean isBotonAceptar() {
		return botonAceptar;
	}

	/**
	 * @param botonAceptar the botonAceptar to set
	 */
	public void setBotonAceptar(boolean botonAceptar) {
		this.botonAceptar = botonAceptar;
	}
	
	
}
