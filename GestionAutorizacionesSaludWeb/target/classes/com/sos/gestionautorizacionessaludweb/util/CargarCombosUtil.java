package com.sos.gestionautorizacionessaludweb.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OpcionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OrigenAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PrioridadAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoServicioVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;

/**
 * Class Utilitaria para el ingreso de solicitudes
 * Clase donde se consulta la informacion de los combos
 * @author Ing. Victor Hugo Gil
 * @version 26/07/2016
 *
 */
public class CargarCombosUtil {
	
	private CargarCombosUtil(){
		/*clase utilizaría constructor privado para evitar instancias */
	}
		
	/**
	 * Metodo que permite consultar la informacion de los diagnosticos
	 * @param 
	 * @throws LogicException
	 */	
	public static List<TiposDiagnosticosVO> obtenerDiagnostico(CargarCombosController cargarCombosController) throws LogicException{
		return cargarCombosController.consultaTipoDiagnostico();			
	}
	
	/**
	 * Metodo que permite consultar la informacion de la clase atencion
	 * @param 
	 * @throws LogicException
	 */	
	
	public static List<ClaseAtencionVO> obtenerClaseAtencion(CargarCombosController cargarCombosController) throws LogicException{
		return cargarCombosController.consultaClaseAtencion();				
	}
	
	/**
	 * Metodo que permite consultar la informacion de los generos
	 * @param 
	 * @throws LogicException
	 */	
	
	public static List<SelectItem> obtenerListaSelectGeneros(List<GeneroVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<GeneroVO>   listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaListaGenero();
		}else {
			listaVosResult = listaVos;
		}		
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (GeneroVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoGenero(), vo.getDescripcionGenero());
			listaItems.add(item);
		}			
		return listaItems;
	}
	
	/**
	 * Metodo que permite extraer la informacion de los tipos de identificacion
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectTiposIdentificacion(List<TiposIdentificacionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<TiposIdentificacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultarTiposIdentificacion();
		}else {
			listaVosResult = listaVos;
		}		
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (TiposIdentificacionVO tiposIdentificacionVO : listaVosResult) {
			item = new SelectItem(tiposIdentificacionVO.getConsecutivoTipoIdentificacion(), tiposIdentificacionVO.getCodigoTipoIdentificacion(), tiposIdentificacionVO.getDescripcionTipoIdentificacion());
			listaItems.add(item);
		}	
			
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de las opciones
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectOpcion(List<OpcionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<OpcionVO> listaVosResult = null;		
			
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaListaOpcion();
		}else {
			listaVosResult = listaVos;
		} 			
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (OpcionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoOpcion(), vo.getDescripcionOpcion());
			listaItems.add(item);
		}
		
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la clase atencion
	 * @param 
	 * @throws LogicException
	 */
	
	public static List<SelectItem> obtenerListaSelectClaseAtencion(List<ClaseAtencionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<ClaseAtencionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaClaseAtencion();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (ClaseAtencionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoClaseAtencion(), vo.getDescripcionClaseAtencion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion del origen de la atencion
	 * @param 
	 * @throws LogicException
	 */
	
	public static List<SelectItem> obtenerListaSelectOrigenAtencion(List<OrigenAtencionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<OrigenAtencionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaOrigenAtencion();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (OrigenAtencionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoOrigenAtencion(), vo.getDescripcionOrigenAtencion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de lso tipos de servicio
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectTipoServicio(List<TipoServicioVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {	
		List<TipoServicioVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaTipoServicio();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (TipoServicioVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoTipoServicio(), vo.getDescripcionTipoServicio());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la prioridad de atencion
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectPrioridadAtencion(List<PrioridadAtencionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {	
		List<PrioridadAtencionVO> listaVosResult = null;
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaPrioridadAtencion();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (PrioridadAtencionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoPrioridadAtencion(), vo.getDescripcionPrioridadAtencion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de los tipos de diagnostico
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectTipoDiagnostico(List<TiposDiagnosticosVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<TiposDiagnosticosVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaTipoDiagnostico();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (TiposDiagnosticosVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoTipoDiagnostico(), vo.getDescripcionTipoDiagnostico());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de los servicios de hospitalizacion
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectServicioHospitalizacion(List<ServicioHospitalizacionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<ServicioHospitalizacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaServicioHospitalizacion();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (ServicioHospitalizacionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoSevicioHospitalizacion(), vo.getDescripcionSevicioHospitalizacion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la clase de habitacion
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectClaseHabitacion(List<ClaseHabitacionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<ClaseHabitacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaClaseHabitacion();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (ClaseHabitacionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoClaseHabitacion(), vo.getDescripcionClaseHabitacion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la dosis del medicamento
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectDosisMedicamentos(List<DosisVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<DosisVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaDosisMedicamentos();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (DosisVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoDosis(), vo.getDescripcionDosis());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la frecuencia del medicamento
	 * @param 
	 * @throws LogicException
	 */
	
	public static List<SelectItem> obtenerListaSelectFrecuenciaMedicamentos(List<FrecuenciaVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<FrecuenciaVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaFrecuenciaMedicamentos();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (FrecuenciaVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoFrecuencia(), vo.getDescripcionFrecuencia());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	
	/**
	 * Metodo que permite consultar la informacion de los tipos de prestacion
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectTipoPrestaciones(List<TipoCodificacionVO> listaVos, int validacionCuos, RegistrarSolicitudController registrarSolicitudesController) throws LogicException {
		List<TipoCodificacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {			
			listaVosResult = registrarSolicitudesController.consultaTiposCodificacion(validacionCuos);			
		}
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (TipoCodificacionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoTipoCodificacion(), vo.getDescripcionTipoCodificacion());
			listaItems.add(item);
		}
		return listaItems;
	}
	

	/**
	 * Metodo que permite consultar la informacion de las lateralidades
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectLateralidadesProcedimientos(List<LateralidadesVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<LateralidadesVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaLateralidadesProcedimientos();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (LateralidadesVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoLaterialidad(), vo.getDescripcionLateralidad());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de las vias de acceso
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectViasAccesoMedicamento(List<ViaAccesoVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<ViaAccesoVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaViasAccesoMedicamento();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ViaAccesoVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoViaAcceso(), vo.getDescripcionViaAcceso());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la concentracion del medicamento
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectConcentracionMedicamento(List<ConcentracionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<ConcentracionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaConcentracionMedicamento();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ConcentracionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoConcentracion(), vo.getDescripcionConcentracion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de la presentacion del medicamento
	 * @param 
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectPresentacionMedicamento(List<PresentacionVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<PresentacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaPresentacionMedicamento();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (PresentacionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoPresentacion(), vo.getDescripcionPresentacion());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que permite consultar la informacion de causales de descuadre
	 * @param listaVos
	 * @param cargarCombosController
	 * @return
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectCausalesDescuadre(List<CausalDescuadreVO> listaVos, CargarCombosController cargarCombosController) throws LogicException {
		List<CausalDescuadreVO> listaVosResult;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = cargarCombosController.consultaCausalesDescuadre();
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (CausalDescuadreVO vo : listaVosResult) {
			item = new SelectItem(vo.getCnsctvoCdgoCslDscdre(), vo.getDscrpcnCslDscdre());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Consulta los combos basados en el objetos parametro 
	 * @param listaVos
	 * @param cargarCombosController
	 * @param listaACargar
	 * @return
	 * @throws LogicException
	 */
	public static List<SelectItem> obtenerListaSelectParametros(List<ParametroVO> listaVos, CargarCombosController cargarCombosController, String listaACargar) throws LogicException {
		List<ParametroVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			if (listaACargar.equalsIgnoreCase(ConstantesEJB.COMBO_BANCO)){
				listaVosResult = cargarCombosController.consultaBanco();
			}
			if (listaACargar.equalsIgnoreCase(ConstantesEJB.COMBO_METODO_DEVOLUCION)){
				listaVosResult = cargarCombosController.consultaMetodosDevolucion();
			}
			if (listaACargar.equalsIgnoreCase(ConstantesEJB.COMBO_SEDES)){
				listaVosResult = cargarCombosController.consultaSedes(null, new Date(), null, null);
			}
			
			if (listaACargar.equalsIgnoreCase(ConstantesEJB.COMBO_ESTADOS_NOTA_CREDITO)){
				listaVosResult = cargarCombosController.consultaEstadosNotaCredito(null, new Date(), null, ConstantesEJB.USUARIO_VISIBLE, ConstantesEJB.CONSECUTIVO_CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO);
			}
			
		}else {
			listaVosResult = listaVos;
		}
		
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ParametroVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivo(), vo.getDescipcion());
			listaItems.add(item);
		}
		return listaItems;
	}
}
