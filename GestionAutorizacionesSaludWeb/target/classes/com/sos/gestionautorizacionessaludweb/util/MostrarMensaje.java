package com.sos.gestionautorizacionessaludweb.util;

/**
 * Class MostrarMensaje Clase utilitaria que permite mostrar los mensajes por
 * medio de un Popup
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 07/01/2016
 *
 */

public class MostrarMensaje {
	private MostrarMensaje() {
		// Constructor privado.
	}

	/**
	 * Metodo encargado de mostrar los mensajes.
	 * 
	 * @param mensaje:
	 *            the mensaje
	 * @param color:
	 *            the color
	 */

	public static void mostrarMensaje(String mensaje, String color) {
		MessagesPopupBean messagesPopupBean = (MessagesPopupBean) FacesUtils.getManagedBean("messagesPopupBean");
		messagesPopupBean.setColor(color);
		messagesPopupBean.showError(mensaje);
	}

}
