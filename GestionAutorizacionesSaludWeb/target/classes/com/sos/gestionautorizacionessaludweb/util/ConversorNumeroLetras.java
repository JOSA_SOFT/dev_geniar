package com.sos.gestionautorizacionessaludweb.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.sos.excepciones.LogicException;

/**
 * Clave para escribir numero en letras
 * 
 * @author Fabian Caicedo - Geniar SAS
 */
public class ConversorNumeroLetras {
	private static final String[] UNIDADES = { ConstantesWeb.CADENA_VACIA,
			ConstantesWeb.UN, ConstantesWeb.DOS, ConstantesWeb.TRES,
			ConstantesWeb.CUATRO, ConstantesWeb.CINCO, ConstantesWeb.SEIS,
			ConstantesWeb.SIETE, ConstantesWeb.OCHO, ConstantesWeb.NUEVE,
			ConstantesWeb.DIEZ, ConstantesWeb.ONCE, ConstantesWeb.DOCE,
			ConstantesWeb.TRECE, ConstantesWeb.CATORCE, ConstantesWeb.QUINCE,
			ConstantesWeb.DIECISEIS, ConstantesWeb.DIECISIETE,
			ConstantesWeb.DIECIOCHO, ConstantesWeb.DIECINUEVE,
			ConstantesWeb.VEINTE };

	private static final String[] DECENAS = { ConstantesWeb.VENTI,
			ConstantesWeb.TREINTA, ConstantesWeb.CUARENTA,
			ConstantesWeb.CINCUENTA, ConstantesWeb.SESENTA,
			ConstantesWeb.SETENTA, ConstantesWeb.OCHENTA,
			ConstantesWeb.NOVENTA, ConstantesWeb.CIEN };

	private static final String[] CENTENAS = { ConstantesWeb.CIENTO,
			ConstantesWeb.DOSCIENTOS, ConstantesWeb.TRESCIENTOS,
			ConstantesWeb.CUATROCIENTOS, ConstantesWeb.QUINIENTOS,
			ConstantesWeb.SEISCIENTOS, ConstantesWeb.SETECIENTOS,
			ConstantesWeb.OCHOCIENTOS, ConstantesWeb.NOVECIENTOS };

	/**
	 * Evita la creación una nueva instancia de la clase usando la palabra
	 * reservada <code>new</code>.
	 */
	private ConversorNumeroLetras() {
	}

	/**
	 * Convierte a letras un numero de la forma $123,456.32
	 * 
	 * @param number
	 *            Numero en representacion texto
	 * @throws NumberFormatException
	 *             Si valor del numero no es valido (fuera de rango o )
	 * @return Numero en letras
	 */
	public static String convertirNumeroLetras(String number)
			throws LogicException {
		return convertirNumeroLetras(Double.parseDouble(number));
	}

	private static void validarNumeroLegal(double doubleNumberAux)
			throws LogicException {
		// Validamos que sea un numero legal
		if (doubleNumberAux > ConstantesWeb.NUMERO_LIMITE) {
			throw new LogicException(ConstantesWeb.NUMERO_MAYOR_A_999999999
					+ ConstantesWeb.NO_ES_POSIBLE_CONVERTIR_NUMERO,
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}

		if (doubleNumberAux < 0) {
			throw new LogicException(ConstantesWeb.NUMERO_DEBE_SER_POSITIVO,
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	private static int descomponerTrioMillones(String[] splitNumber,
			StringBuilder converted) {
		int millon = Integer.parseInt(String.valueOf(obtenerDigito(
				splitNumber[0], 8))
				+ String.valueOf(obtenerDigito(splitNumber[0], 7))
				+ String.valueOf(obtenerDigito(splitNumber[0], 6)));
		if (millon == 1)
			converted.append(ConstantesWeb.UN_MILLON);
		else if (millon > 1)
			converted.append("")
					.append(convertirNumero(String.valueOf(millon)))
					.append(ConstantesWeb.MILLONES);
		return millon;
	}

	private static int descomponerTrioMiles(String[] splitNumber, int millon,
			StringBuilder converted) {
		int miles = Integer.parseInt(String.valueOf(obtenerDigito(
				splitNumber[0], 5))
				+ String.valueOf(obtenerDigito(splitNumber[0], 4))
				+ String.valueOf(obtenerDigito(splitNumber[0], 3)));
		if (millon >= 1) {
			if (miles >= 1)
				converted.append(convertirNumero(String.valueOf(miles)))
						.append(ConstantesWeb.MIL);
		} else {
			if (miles == 1)
				converted.append(ConstantesWeb.UN_MIL);

			if (miles > 1)
				converted.append("")
						.append(convertirNumero(String.valueOf(miles)))
						.append(ConstantesWeb.MIL);
		}
		return miles;
	}

	private static int descomponerTrioCientos(String[] splitNumber, int millon,
			int miles, StringBuilder converted) {
		int cientos = Integer.parseInt(String.valueOf(obtenerDigito(
				splitNumber[0], 2))
				+ String.valueOf(obtenerDigito(splitNumber[0], 1))
				+ String.valueOf(obtenerDigito(splitNumber[0], 0)));
		if (miles >= 1 || millon >= 1) {
			if (cientos >= 1)
				converted.append(convertirNumero(String.valueOf(cientos)));
		} else {
			if (cientos == 1)
				converted.append(ConstantesWeb.UN);
			if (cientos > 1)
				converted.append("").append(
						convertirNumero(String.valueOf(cientos)));
		}
		return cientos;
	}

	/**
	 * Convierte un numero en representacion numerica a uno en representacion de
	 * texto. El numero es valido si esta entre 0 y 999'999.999
	 * 
	 * @param doubleNumber
	 *            Numero a convertir
	 * @return Numero convertido a texto
	 * @throws LogicException
	 * @throws NumberFormatException
	 *             Si el numero esta fuera del rango
	 */
	public static String convertirNumeroLetras(double doubleNumber)
			throws LogicException {
		StringBuilder converted = new StringBuilder(200);
		String patternThreeDecimalPoints = "#.###";
		double doubleNumberAux;

		DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
		format.setRoundingMode(RoundingMode.DOWN);
		// formateamos el numero, para ajustarlo a el formato de tres puntos
		// decimales
		String formatedDouble = format.format(doubleNumber);
		doubleNumberAux = Double.parseDouble(formatedDouble);

		// Validamos que sea un numero legal
		validarNumeroLegal(doubleNumberAux);

		String[] splitNumber = String.valueOf(doubleNumberAux)
				.replace('.', '#').split("#");

		// Descompone el trio de millones
		int millon = descomponerTrioMillones(splitNumber, converted);

		// Descompone el trio de miles
		int miles = descomponerTrioMiles(splitNumber, millon, converted);

		// Descompone el ultimo trio de unidades
		int cientos = descomponerTrioCientos(splitNumber, millon, miles,
				converted);

		if (millon + miles + cientos == 0)
			converted.append(ConstantesWeb.CERO);

		converted.append(ConstantesWeb.PESOS);
		return converted.toString();
	}

	/**
	 * Convierte los trios de numeros que componen las unidades, las decenas y
	 * las centenas del numero.
	 * 
	 * @param number
	 *            Numero a convetir en digitos
	 * @return Numero convertido en letras
	 */
	private static String convertirNumero(String number) {

		if (number.length() > 3)
			throw new NumberFormatException(ConstantesWeb.LONGITUD_MAXIMA);

		// Caso especial con el 100
		if (number.equals(ConstantesWeb.NUMERO_CIEN)) {
			return ConstantesWeb.CIEN;
		}

		StringBuilder output = new StringBuilder(200);
		if (obtenerDigito(number, 2) != 0)
			output.append(CENTENAS[obtenerDigito(number, 2) - 1]);

		int k = Integer.parseInt(String.valueOf(obtenerDigito(number, 1))
				+ String.valueOf(obtenerDigito(number, 0)));

		if (k <= 20)
			output.append(UNIDADES[k]);
		else if (k > 30 && obtenerDigito(number, 0) != 0)
			output.append(DECENAS[obtenerDigito(number, 1) - 2])
					.append(ConstantesWeb.Y)
					.append(UNIDADES[obtenerDigito(number, 0)]);
		else
			output.append(DECENAS[obtenerDigito(number, 1) - 2]).append(
					UNIDADES[obtenerDigito(number, 0)]);

		return output.toString();
	}

	/**
	 * Retorna el digito numerico en la posicion indicada de derecha a izquierda
	 * 
	 * @param origin
	 *            Cadena en la cual se busca el digito
	 * @param position
	 *            Posicion de derecha a izquierda a retornar
	 * @return Digito ubicado en la posicion indicada
	 */
	private static int obtenerDigito(String origin, int position) {
		if (origin.length() > position && position >= 0)
			return origin.charAt(origin.length() - position - 1) - 48;
		return 0;
	}
}