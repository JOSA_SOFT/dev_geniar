package com.sos.gestionautorizacionessaludweb.util;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ServiceLocator {

	
	@SuppressWarnings("unchecked")
	public static <T> T locateService(String serviceProperties) throws Exception{
		Hashtable<String, String> settings = new Hashtable<String, String>();
		settings.put(Context.INITIAL_CONTEXT_FACTORY,"com.ibm.ws.naming.util.WsnInitCtxFactory");
		//settings.put(Context.PROVIDER_URL,"rmi://localhost:2809");
		settings.put(Context.PROVIDER_URL,"iiop://172.16.188.22:2820");//HADES DESARROLLO
		//settings.put(Context.PROVIDER_URL,"iiop://172.16.188.22:9812");//HADES PRUEBAS
		//settings.put(Context.SECURITY_PRINCIPAL,"wpsadmin");
		//settings.put(Context.SECURITY_CREDENTIALS,"wpsadmin");
		Object service;
		try {
			Context initialContext = new InitialContext(settings);
			Object home= initialContext.lookup("ejb/com/sos/visos/service/ejb/remote/VisosServiceEJBHome");
			service = Class.forName("com.sos.visos.service.ejb.remote.VisosServiceEJBHome").getMethod("create").invoke(home);
		} catch (Exception e) {
			throw new Exception(e);
		}
	    //VisosServiceEJB visosService = home.create();
		//return new VisosServiceImpl(); 
		return  (T)service;
	}
	
	
}
