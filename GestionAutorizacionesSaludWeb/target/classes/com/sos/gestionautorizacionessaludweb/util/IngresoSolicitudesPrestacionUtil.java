package com.sos.gestionautorizacionessaludweb.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class Utilitaria para la consulta de solicitudes
 * Clase donde se relacionan las utilidades (Validaciones) sobre el ingreso de solicitudes
 * @author Ing. Victor Hugo Gil
 * @version 26/07/2016
 *
 */
public class IngresoSolicitudesPrestacionUtil {
	
	private IngresoSolicitudesPrestacionUtil(){
		/*clase utilizaría constructor privado para evitar instancias */		
	}
	
	public static void validarPrestacionesSolicitadas(MedicamentosVO medicamentoVO, List<PrestacionDTO> listaPrestacionesSolicitadas) throws LogicException {
		String codigoCodificacionMedicamento = medicamentoVO.getCodigoCodificacionMedicamento().trim();		
		
		for (PrestacionDTO prestacionDTO : listaPrestacionesSolicitadas) {
			if (!codigoCodificacionMedicamento.isEmpty() && prestacionDTO.getCodigoCodificacionPrestacion().equals(medicamentoVO.getCodigoCodificacionMedicamento())) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRESTACION_ADICIONADA), ErrorType.PARAMETRO_ERRADO);
			}
		}
	}

	public static void validarConsecutivoFrecuencia(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getFrecuenciaVO() == null || medicamentoVO.getFrecuenciaVO().getConsecutivoFrecuencia() == null
				|| medicamentoVO.getFrecuenciaVO().getConsecutivoFrecuencia().compareTo(0) == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarHoraFrecuencia(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getHoraFrecuencia() == null || medicamentoVO.getHoraFrecuencia().compareTo(BigDecimal.ZERO) == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarDosis(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getDosisVO() == null || medicamentoVO.getDosisVO().getConsecutivoCodigoDosis() == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarCantidadDosis(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getCantidadDosis() == null || medicamentoVO.getCantidadDosis().compareTo(BigDecimal.ZERO) == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarCodificacionMedicamento(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getDescripcionCodificacionMedicamento() == null || medicamentoVO.getDescripcionCodificacionMedicamento().trim().isEmpty()) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarCantidadMedicamento(MedicamentosVO medicamentoVO) throws LogicException {
		if (medicamentoVO.getCantidadMedicamento() == null || medicamentoVO.getCantidadMedicamento().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarConsecutivoCodificacionMedicamento(MedicamentosVO medicamentoVO)throws LogicException {
		if (medicamentoVO.getConsecutivoCodificacionMedicamento() != null && medicamentoVO.getConsecutivoCodificacionMedicamento().compareTo(0) != 0 && (medicamentoVO.getCodigoCodificacionMedicamento() == null || medicamentoVO.getCodigoCodificacionMedicamento().trim().isEmpty())) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	
	public static void validarConsecutivoAcceso(ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getConsecutivoViaAcceso() == null || procedimientosVO.getConsecutivoViaAcceso().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarDescripcionCodificacion(ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getDescripcionCodificacionProcedimiento() == null || procedimientosVO.getDescripcionCodificacionProcedimiento().trim().isEmpty()) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarConsecutivoLateralidad(ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getConsecutivoLaterialidad() == null || procedimientosVO.getConsecutivoLaterialidad().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarCantidadProcedimientos(ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getCantidadProcedimiento() == null || procedimientosVO.getCantidadProcedimiento().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}

	public static void validarConsecutivoCodificacionProcedimiento(ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getConsecutivoCodificacionProcedimiento() != null && procedimientosVO.getConsecutivoCodificacionProcedimiento().compareTo(0) != 0
				&& (procedimientosVO.getCodigoCodificacionProcedimiento() == null || procedimientosVO.getCodigoCodificacionProcedimiento().trim().isEmpty())) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_PRESTACION), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	
	public static boolean validarListaPrestacionEncontradosVacia(List<?> lista) {
		boolean aux = false;
		
		if(lista == null || lista.isEmpty()){
			aux = true;
		}		
		return aux;
	}	
	
	/**
	 * metodo que valida el codigo de codificacion de la prestacion
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static void validarCodigoCodificacionProcedimiento(PrestacionDTO prestacionDTO, ProcedimientosVO procedimientosVO) throws LogicException {
		if (procedimientosVO.getCodigoCodificacionProcedimiento() != null && !procedimientosVO.getCodigoCodificacionProcedimiento().trim().isEmpty()&& prestacionDTO.getCodigoCodificacionPrestacion().equals(procedimientosVO.getCodigoCodificacionProcedimiento())) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRESTACION_ADICIONADA), ErrorType.PARAMETRO_ERRADO);
		}
	}
	
	/**
	 * metodo que permite ingresar la descripcion de la via de acceso
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static ViaAccesoVO ingresarDescripcionViaAcceso(List<SelectItem> listaViasAccesoMedicamento, ViaAccesoVO viaAccesoVO, ProcedimientosVO procedimientosVO) {
		ViaAccesoVO viaAcceso = viaAccesoVO;
		List<SelectItem> listaViasAccesoMed = listaViasAccesoMedicamento;
		ProcedimientosVO procedimientos = procedimientosVO;
		
		for (SelectItem item : listaViasAccesoMed) {
			if (Integer.parseInt(item.getValue().toString()) == procedimientos.getConsecutivoViaAcceso().intValue()) {
				viaAcceso.setDescripcionViaAcceso(item.getLabel());
				break;
			}
		}
		return viaAcceso;
	}
	
	/**
	 * metodo que permite ingresar la descripcion de la lateralidad
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */
	public static LateralidadesVO ingresarDescripcionLateralidad(LateralidadesVO lateralidadesVO, List<SelectItem> listaLateralidades, ProcedimientosVO procedimientosVO) {
		
		LateralidadesVO lateralidades = lateralidadesVO;
		List<SelectItem> listaLateralid = listaLateralidades;
		ProcedimientosVO procedimientos = procedimientosVO;
		
		for (SelectItem item : listaLateralid) {
			if (Integer.parseInt(item.getValue().toString()) == procedimientos.getConsecutivoLaterialidad()) {
				lateralidades.setDescripcionLateralidad(item.getLabel());
				break;
			}
		}
		return lateralidades;
	}
	
	/**
	 * metodo que permite consultar la descripcion de la dosis
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */	
	public static PrestacionDTO agregarDescripcionDosis(PrestacionDTO prestacionDTO, List<SelectItem> listaDosisMedicamentos, MedicamentosVO medicamentoVO) {
		
		PrestacionDTO prestacion = prestacionDTO;
		List<SelectItem> listaDosisMedica = listaDosisMedicamentos;
		MedicamentosVO medicamento = medicamentoVO;
		
		for (SelectItem item : listaDosisMedica) {
			if (Integer.parseInt(item.getValue().toString()) == medicamento.getDosisVO().getConsecutivoCodigoDosis().intValue()) {
				prestacion.getDosisVO().setDescripcionDosis(item.getLabel());
				break;
			}
		}
		return prestacion;
	}
	
	/**
	 * metodo que permite validar el cambio de prestacion CUMS
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */		
	public static boolean cambioDeTipoPrestacionCUMS(int consecutivoCUMS, Integer tipoPrestacionSeleccionado) {
		
		boolean mostrarPanelMedicamentos = false;
		
		if (tipoPrestacionSeleccionado != null && tipoPrestacionSeleccionado.intValue() == consecutivoCUMS) {
			mostrarPanelMedicamentos = true;
		}
		
		return mostrarPanelMedicamentos;
		
	}
	
	/**
	 * metodo que permite validar el cambio de prestacion CUPS
	 * 
	 * @param soporteVO
	 * @throws LogicException
	 */	
	
	public static boolean cambioDeTipoPrestacionCUPS(int consecutivoCUPS, Integer tipoPrestacionSeleccionado) {
		boolean mostrarPanelCUPS = false;		
		if (tipoPrestacionSeleccionado != null && tipoPrestacionSeleccionado.intValue() == consecutivoCUPS) {
			mostrarPanelCUPS = true;
		}
		
		return mostrarPanelCUPS;
	}
	
	
	/**
	 * metodo que permite crear el objeto de medicamentos
	 * 	
	 */	
	public static MedicamentosVO crearMedicamentosVO(){
		MedicamentosVO medicamentoVO = new MedicamentosVO();
		medicamentoVO.setDosisVO(new DosisVO());
		medicamentoVO.setFrecuenciaVO(new FrecuenciaVO());
		medicamentoVO.setConcentracionVO(new ConcentracionVO());
		medicamentoVO.setPresentacionVO(new PresentacionVO());
		
		return medicamentoVO;
	}		
	
	/**
	 * metodo que buscar la descripcion de la concentracion del medicamento
	 * 	
	 */	
	public static String buscarDescripcionConcentracionMedicamento(List<SelectItem> listaConcentracionesMedicamento, MedicamentosVO medicamentoVO){
		String descripcionConcetracion = null;
		List<SelectItem> listaConcentraciones = listaConcentracionesMedicamento;
		MedicamentosVO medicamento = medicamentoVO;
		
		for (SelectItem item : listaConcentraciones) {
			if (Integer.parseInt(item.getValue().toString()) == medicamento.getConcentracionVO().getConsecutivoConcentracion()) {
				descripcionConcetracion = item.getLabel();
				break;
			}
		}
		return descripcionConcetracion;
	}
	
	/**
	 * metodo que buscar la descripcion de la presentacion del medicamento
	 * 	
	 */	
	public static String buscarDescripcionPresentacionMedicamento(List<SelectItem> listaPresentacionesMedicamento, MedicamentosVO medicamentoVO){
		
		String descripcionPresentacion = null;
		List<SelectItem> listaPresentacion = listaPresentacionesMedicamento;
		MedicamentosVO medicamento = medicamentoVO;
		
		for (SelectItem item : listaPresentacion) {
			if (medicamento.getPresentacionVO().getConsecutivoCodigoPresentacion().compareTo(new BigInteger(item.getValue().toString())) == 0) {
				descripcionPresentacion = item.getLabel();
				break;
			}
		}
		
		return descripcionPresentacion;		
	}
	
	/**
	 * metodo que buscar la descripcion del tipo de prestacion
    */	
	public static String buscarDescripcionTipoPrestacion(List<SelectItem> listaTiposPrestaciones, Integer tipoPrestacionSeleccionado){
		String descripcionTipoCodificacion = null;
		List<SelectItem> listaTiposPrestacion = listaTiposPrestaciones;
		
		for (SelectItem item : listaTiposPrestacion) {
			if (Integer.parseInt(item.getValue().toString()) == tipoPrestacionSeleccionado.intValue()) {
				descripcionTipoCodificacion = item.getLabel();
				break;
			}
		}		
		return descripcionTipoCodificacion;
	}
		
}
