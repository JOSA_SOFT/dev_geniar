package com.sos.gestionautorizacionessaludweb.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import sos.validador.bean.resultado.adicionales.DatosAdicionalesAfiliado;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.UsuarioVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.CausasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.FundamentoLegalVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.GrupoAuditorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.MotivoPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgoAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.util.jsf.FacesUtil;


/**
 * Class Utilitaria para funciones web
 * Clase donde se relacionan las funciones mas utilizadas en los MB
 * @author Julian Hernandez
 * @version 14/03/2016
 *
 */
public class FuncionesAppWeb {
	
	/**
	 * Variable instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(FuncionesAppWeb.class);
	
	private boolean mostrarPopupCUPS;
	private ProcedimientosVO procedimientoVOSeleccionado;

	/**
	 * 
	 * @param datosAdicionalesAfiliado
	 * @param afiliadoVO
	 * @return
	 */
	public static final AfiliadoVO datosAdicionalesAfiliadoToAfiliadoVO(DatosAdicionalesAfiliado datosAdicionalesAfiliado, AfiliadoVO afiliadoVO ){
		afiliadoVO.setDireccionResidencia(datosAdicionalesAfiliado.getDireccion());
		afiliadoVO.setTelefono(datosAdicionalesAfiliado.getTelefono());
		afiliadoVO.setFechaNacimiento(stringToDate(datosAdicionalesAfiliado.getFechaNacimiento(), ConstantesWeb.FORMATO_FECHA_1));
		afiliadoVO.setEdadAnos(datosAdicionalesAfiliado.getEdadAnos());
		afiliadoVO.setEdadMeses(datosAdicionalesAfiliado.getEdadMeses());
		afiliadoVO.setEdadDias(datosAdicionalesAfiliado.getEdadDias());
		afiliadoVO.setFechaInicioVigencia(stringToDate(datosAdicionalesAfiliado.getInicioVigencia(),ConstantesWeb.FORMATO_FECHA_1));
		afiliadoVO.setFechaFinVigencia(stringToDate(datosAdicionalesAfiliado.getFinVigencia(),ConstantesWeb.FORMATO_FECHA_1));
		afiliadoVO.setDescripcionTipoAfiliado(datosAdicionalesAfiliado.getTipoAfiliado());
		afiliadoVO.setSemanasAfiliacionPACSOS(datosAdicionalesAfiliado.getSemanasPacSos());
		afiliadoVO.setSemanasAfiliacionPOSSOS(datosAdicionalesAfiliado.getSemanasAfiliacionPosSOS());
		afiliadoVO.setCodigoIPSPrimaria(datosAdicionalesAfiliado.getCodigoIpsPrimaria());
		afiliadoVO.setDescripcionIPSPrimaria(datosAdicionalesAfiliado.getDescripcionIpsPrimaria());
		afiliadoVO.setSemanasAfiliacionPOSAnt(datosAdicionalesAfiliado.getSemanasAfiliacionPosAnterior());
		afiliadoVO.setSemanasAfiliacionPACAnt(datosAdicionalesAfiliado.getSemanasPacAnterior());
		afiliadoVO.setDescripcionRangoSalarial(datosAdicionalesAfiliado.getRangoSalarial());
		return afiliadoVO;
	}

	/**
	 * 
	 * @param fechaString
	 * @param formato
	 * @return
	 */
	public static final Date stringToDate(String fechaString, String formato){
		Date fechaDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		try {
			fechaDate = formatter.parse(fechaString);
		} catch (ParseException e) {
			LOG.error(e.getMessage(),e);
		}
		return fechaDate;
	}

	/**
	 * 
	 * @param fechaString
	 * @param formato
	 * @return
	 */
	public static final String dateToString(Date fecha){
		String fechaString = ConstantesWeb.CADENA_VACIA;
		SimpleDateFormat formatter = new SimpleDateFormat(ConstantesEJB.FORMATO_FECHA_DIA);
		fechaString = formatter.format(fecha);
		return fechaString;
	}
	
	
	/**
	 * metodo que permite realizar la validacion del error que retorna lo servicios
	 */
	public static final boolean validateServiceError(ServiceErrorVO serviceErrorVO){
		return serviceErrorVO != null && ConstantesEJB.OK_SERVICE.equals(serviceErrorVO.getCodigoError());
	}
	
	public void validarAsociacionPrestacion(List<ProcedimientosVO> listaCUPSEncontrados) throws LogicException{
		if (listaCUPSEncontrados != null && !listaCUPSEncontrados.isEmpty()) {
			if (listaCUPSEncontrados.size() == 1) {
				mostrarPopupCUPS = false;
				procedimientoVOSeleccionado = listaCUPSEncontrados.get(0);				
			} else {
				mostrarPopupCUPS = true;
			}	
		} else {
			throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ASOCIAR_PRESTACION), ErrorType.DATO_NO_EXISTE);
		}	
	}	
	
	
	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	public ProcedimientosVO getProcedimientoVOSeleccionado() {
		return procedimientoVOSeleccionado;
	}

	public void setProcedimientoVOSeleccionado(
			ProcedimientosVO procedimientoVOSeleccionado) {
		this.procedimientoVOSeleccionado = procedimientoVOSeleccionado;
	}	
	
	/**  Metodo Descargar documentos soportes  */
	public static void descargarSoporteAuditoria(String nombreDocumento, byte[] datosArchivoSoporte) throws LogicException{

		ExternalContext external = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse response = (HttpServletResponse) external.getResponse();
		response.setHeader(ConstantesWeb.CONTENT_DISPOSITION,ConstantesWeb.ATTACHMENT_FILENAME + nombreDocumento);
		response.setHeader(ConstantesWeb.CACHE_CONTROL,ConstantesWeb.MUST_REVALIDATE+","+ ConstantesWeb.POST_CHECK +","+ ConstantesWeb.PRE_CHECK);

		response.setContentLength((int) datosArchivoSoporte.length);
		try {
			ServletOutputStream servletOutputStream = response.getOutputStream();
			servletOutputStream.write(datosArchivoSoporte, 0, (int) datosArchivoSoporte.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new LogicException(e.getMessage(), ErrorType.PARAMETRO_ERRADO);
		}
		FacesContext.getCurrentInstance().responseComplete();
	}
	
	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectRiesgosAfiliado(List<RiesgoAfiliadoVO> listaVos){
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (RiesgoAfiliadoVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivoCodRiesgoDiagnostico(), vo.getDescRiesgoDiagnostico());
				listaItems.add(item);
			}
		}
		return listaItems;
	}


	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectMotivos(List<MotivoPrestacionVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (MotivoPrestacionVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivoCodMotivoPrestacion(), vo.getDesMotivoPrestacion());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}
	
	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaMotivos(List<MotivoCausaVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (MotivoCausaVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivoCodMotivoCausa(), vo.getDesMotivoCausa());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}

	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectGruposAuditores(List<GrupoAuditorVO> listaVos)  {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (GrupoAuditorVO vo : listaVos) {
				item = new SelectItem(vo.getDescGrupoAuditor(), vo.getDescGrupoAuditor());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}

	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectPrestadoresDireccionamiento(List<PrestadorVO> listaVos){
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (PrestadorVO vo : listaVos) {
				item = new SelectItem(vo.getCodigoInterno().toString().trim(), vo.getNombreSucursal());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}

	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectMedicosAuditores(List<UsuarioVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (UsuarioVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivo(), vo.getNombre());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}

	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectCausas(List<CausasVO> listaVos)  {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (CausasVO vo : listaVos) {
				item = new SelectItem(vo.getCodigocausa(), vo.getCausa());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}

	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectFundamentoLegal(List<FundamentoLegalVO> listaVos)  {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (FundamentoLegalVO vo : listaVos) {
				item = new SelectItem(vo.getCodigoFudamento(), vo.getFundamentoLegal());
				listaItems.add(item);
			} 
		}
		return listaItems;
	}
	
	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public static List<SelectItem> obtenerListaSelectTiposPrestacionGD(List<SelectItem> listaVos)  {
		if (listaVos != null && !listaVos.isEmpty()) {
			for (SelectItem vo : listaVos) {
				if (!ConstantesWeb.MEDICAMENTO.equals(vo.getLabel())) {
					listaVos.remove(vo);
				}
			}
		}
		return listaVos;
	}
	
	/**
	 * 
	 * @param numeroSinFormato
	 * @return
	 */
	public static String formatearMiles(Integer numeroSinFormato){
		DecimalFormat formatea = new DecimalFormat("###,###.##");
		return formatea.format(numeroSinFormato);
	}
	
	public static void gestionTarea(String idTask, String idSolicitud, String accion) throws LogicException{
		ClienteRestBpmBean clienteRestBpmBean=(ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
		if(accion.equals(ConstantesWeb.LIBERAR_TAREA)){
			clienteRestBpmBean.liberarTarea(idTask, idSolicitud, accion, "-"); 
		}else{
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud, accion, "-");
		}
	}	
}
