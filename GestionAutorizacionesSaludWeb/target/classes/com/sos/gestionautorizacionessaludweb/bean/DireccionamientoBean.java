package com.sos.gestionautorizacionessaludweb.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.DireccionamientoService;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;

/**
 * Class que permite ejecutar el servicio de direccionamiento 
 * @author Ing. Victor Hugo Gil
 * @version 02/08/2016
 */
public class DireccionamientoBean {
	

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(DireccionamientoBean.class);	
	
	
	public DireccionamientoSolicitudVO ejecutarDireccionamiento(SolicitudVO solicitud) {

		DireccionamientoSolicitudVO direccionamientoSolicitud = null;
		DireccionamientoService direccionamientoService = new DireccionamientoService();
		co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService direccionamientoServiceExc;
		try {
			direccionamientoServiceExc = direccionamientoService.crearDireccionamientoService();
			direccionamientoSolicitud  = direccionamientoService.consultaResultadoDireccionamiento(direccionamientoService.ejecutarDireccionamiento(direccionamientoServiceExc, solicitud, FacesUtils.getUserName()), null);

		} catch (LogicException logicExcepction) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorExcepcion(ConstantesEJB.EXCEPCION_SERVICIO_DIRECCIONAMIENTO), ConstantesWeb.CADENA_VACIA));
			LOG.error(logicExcepction.getMessage(), logicExcepction);
		} catch (co.eps.sos.service.exception.ServiceException e) {
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			try {
				direccionamientoServiceExc = direccionamientoService.crearDireccionamientoService();
				direccionamientoSolicitud  = direccionamientoService.consultaResultadoDireccionamiento(null, direccionamientoService.consultaDireccionamiento(direccionamientoServiceExc, solicitud, FacesUtils.getUserName()));
			} catch (LogicException logicExcepction) {				
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorExcepcion(ConstantesEJB.EXCEPCION_SERVICIO_DIRECCIONAMIENTO), ConstantesWeb.CADENA_VACIA));
				LOG.error(logicExcepction.getMessage(), logicExcepction);				
			} catch (co.eps.sos.service.exception.ServiceException ser) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), ConstantesWeb.CADENA_VACIA));
				LOG.error(ser.getMessage(), ser);
			} catch (Exception ex) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), ConstantesWeb.CADENA_VACIA));
				LOG.error(ex.getMessage(), ex);
			}
			LOG.error(e.getMessage(), e);
		}

		return direccionamientoSolicitud;
	}

}
