package com.sos.gestionautorizacionessaludweb.bean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroReporteCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeCierreOficinaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteNotaCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.GenerarReporte;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FilesUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.sosjasper.generador.FormatType;
import com.sos.util.jsf.FacesUtil;

/**
 * Class GenerarReporteBean
 * Clase bean que permite la generacion de los reportes en la aplicacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 09/02/2016
 *
 */
public class GenerarReporteBean {
	
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GenerarReporteBean.class);
	private GenerarReporte generarReporte;
	

	private File tempDirectory;
	private ExternalContext externalContext;
	private String generatedFilesUrl;
	private HttpServletResponse httpServletResponse;
	private ConsultarSolicitudController consultarSolicitudController;

	public GenerarReporteBean() {

		try {
			generarReporte = new GenerarReporte();
			externalContext = FacesContext.getCurrentInstance().getExternalContext();
			tempDirectory     = FileUtils.getTempDirectory();
			generatedFilesUrl = tempDirectory.getAbsolutePath() + File.separator + ConstantesEJB.GENERATED_FILES;
			httpServletResponse = (HttpServletResponse) externalContext.getResponse();
			consultarSolicitudController = new ConsultarSolicitudController();
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}			
	}
	
	
	/**Metodo que permite genera el formato de autorizacion del servicio OPS */
	public void formatoOPS(AfiliadoVO afiliadoVO, SolicitudVO solicitud, List<PrestacionDTO> lstPrestacionDTO, List<AutorizacionServicioVO> listaAutorizacionServicioVO, boolean generarConObservaciones) {
				
		String nameArchivo = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim() +
				             afiliadoVO.getNumeroIdentificacion().trim() +
				             ConstantesWeb.CARACTER_NOMBRE_ARCHIVO_OPS +
				             solicitud.getNumeroSolicitudSOS();
		
		try {
			//Descargar a Ruta Local (Temporal)
			File archivoTemporal = new File(tempDirectory, ConstantesEJB.GENERATED_FILES + File.separator + nameArchivo);					
			String pathGeneracionArchivo = archivoTemporal.getAbsolutePath();									
			
			byte[] zip = null;
			File pathGeneratedFiles = null;

			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ConstantesWeb.OBSERVACION_ADICIONAL, generarConObservaciones? ConstantesWeb.OBSERVACION_OPS_OK: ConstantesWeb.OBSERVACION_OPS_NOK);
			params.put(ConstantesWeb.USUARIO_CONSULTA, FacesUtils.getUserName());
			if (listaAutorizacionServicioVO != null && listaAutorizacionServicioVO.size() > 1) {
				for (AutorizacionServicioVO autorizacionServicioVO : listaAutorizacionServicioVO) {
					String nombreArchivoGenerado =  pathGeneracionArchivo + ConstantesEJB.CADENA_COMODIN_NOMBRE + autorizacionServicioVO.getNumeroUnicoAutorizacion() + ConstantesEJB.ARCHIVO_PDF;					
					params.put(ConstantesWeb.NUMERO_UNICO_OPS, autorizacionServicioVO.getNumeroUnicoAutorizacion());
					generarReporte.generarReporteAutorizacionServicioMasivo(params, FormatType.PDF, nombreArchivoGenerado, generatedFilesUrl);
				
					// Guarda historico de descargas
					consultarSolicitudController.guardarHistoricoDescargaFormato(solicitud.getConsecutivoSolicitud(), autorizacionServicioVO.getNumeroUnicoAutorizacion(), 
							lstPrestacionDTO, ConstantesEJB.COD_FORMATO_OPS, nameArchivo, 
							((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
				}
				
				pathGeneratedFiles = new File(generatedFilesUrl);
				String[] entries = pathGeneratedFiles.list();
				zip = FilesUtil.zipFiles(pathGeneratedFiles, entries);
				generarReporte.descargaFileZip(httpServletResponse, zip, nameArchivo);

				if (pathGeneratedFiles != null && pathGeneratedFiles.isDirectory()) {
					FilesUtil.deleteDirectoryFiles(pathGeneratedFiles);
				}
			} else {
				params.put(ConstantesWeb.NUMERO_UNICO_OPS, listaAutorizacionServicioVO.get(0).getNumeroUnicoAutorizacion());
				ByteArrayOutputStream byteOutPutStream = generarReporte.generarReporteAutorizacionServicioPDF(params, FormatType.PDF);
				generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream, nameArchivo);
				
				// Guarda historico de descargas
				consultarSolicitudController.guardarHistoricoDescargaFormato(solicitud.getConsecutivoSolicitud(), listaAutorizacionServicioVO.get(0).getNumeroUnicoAutorizacion(), 
						lstPrestacionDTO, ConstantesEJB.COD_FORMATO_OPS, nameArchivo, 
						((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			}
			
			FacesContext.getCurrentInstance().responseComplete();

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	/**Metodo que permite genera el formato de devolucion 
	 * @param string */
	public void formatoDevolucion(Integer consecutivoSolicitud, String tipodocAfiliado, String numDocumentoAfiliado, String numRadicado,  List<PrestacionDTO> lstPrestacionDTO){
		
		String pathLogoSuperSalud;
		String nameArchivo = null;
		
		try {		
			//Se extrae la ruta compartida donde se encuentra la ruta del logo de supersalud
			String propiedadRutaRecursos = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
			pathLogoSuperSalud = generarReporte.obtenerRutaReporteJasper(propiedadRutaRecursos);
	
	        //Se extrae la ruta compartida donde se encuentra la ruta del logo de supersalud
			String propiedadJasper = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SUPER_SALUD);
			pathLogoSuperSalud = pathLogoSuperSalud + generarReporte.obtenerRutaReporteJasper(propiedadJasper);
			
			nameArchivo = tipodocAfiliado + numDocumentoAfiliado +ConstantesWeb.SUFIJO_NOMBRE_REPORTE+numRadicado;
			
			Map<String, Object> params = new HashMap<String, Object>();			
			params.put(ConstantesWeb.USUARIO_SISTEMA, FacesUtils.getUserName());
			params.put(ConstantesWeb.CONSECUTIVO_SOLICITUD, consecutivoSolicitud);
			params.put(ConstantesWeb.RUTA_LOGO, pathLogoSuperSalud);
			
			/** Se llama al metodo que genera el formato de devoluciones */
			ByteArrayOutputStream byteOutPutStream = generarReporte.generarReporteDevolucionPDF(params, FormatType.PDF);
			generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream, nameArchivo);
			FacesContext.getCurrentInstance().responseComplete();
			// Guarda historico de descargas
			LOG.info(" ENTRA consultarSolicitudController.guardarHistoricoDescargaFormato "+consecutivoSolicitud);
			consultarSolicitudController.guardarHistoricoDescargaFormato(consecutivoSolicitud, null, lstPrestacionDTO, 
					ConstantesEJB.COD_FORMATO_DEVOLUCION, nameArchivo, 
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			LOG.info(" SALE consultarSolicitudController.guardarHistoricoDescargaFormato "+consecutivoSolicitud);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}			
	}
	
	/**Metodo que permite genera el formato de autorizacion del servicio OPS */
	public void formatoOPSConsulta(AfiliadoVO afiliadoVO, String numRadicado, Integer consecutivoSolicitud, List<PrestacionDTO> lstPrestacionDTO, List<AutorizacionServicioVO> listaAutorizacionServicioVO, boolean generarConObservaciones) {
				
		String nameArchivo = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim() + 
				             afiliadoVO.getNumeroIdentificacion().trim() +
				             ConstantesWeb.CARACTER_NOMBRE_ARCHIVO_OPS +
				             numRadicado;	 
		
		try {			
			//Descargar a Ruta Local (Temporal)
			File archivoTemporal = new File(tempDirectory, ConstantesEJB.GENERATED_FILES + File.separator + nameArchivo);					
			String pathGeneracionArchivo = archivoTemporal.getAbsolutePath();									
			
			File pathGeneratedFiles = null;
			byte[] zip = null;

			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ConstantesWeb.USUARIO_CONSULTA, FacesUtils.getUserName());
			params.put(ConstantesWeb.OBSERVACION_ADICIONAL, generarConObservaciones? ConstantesWeb.OBSERVACION_OPS_OK: ConstantesWeb.OBSERVACION_OPS_NOK);
			
			if (listaAutorizacionServicioVO != null && listaAutorizacionServicioVO.size() > 1) {
				for (AutorizacionServicioVO autorizacionServicioVO : listaAutorizacionServicioVO) {
					String nombreArchivoGenerado =  pathGeneracionArchivo + ConstantesEJB.CADENA_COMODIN_NOMBRE + autorizacionServicioVO.getNumeroUnicoAutorizacion() + ConstantesEJB.ARCHIVO_PDF;					
					params.put(ConstantesWeb.NUMERO_UNICO_OPS, autorizacionServicioVO.getNumeroUnicoAutorizacion());
					generarReporte.generarReporteAutorizacionServicioMasivo(params, FormatType.PDF, nombreArchivoGenerado, generatedFilesUrl);
					
					// Guarda historico de descargas
					consultarSolicitudController.guardarHistoricoDescargaFormato(consecutivoSolicitud, autorizacionServicioVO.getNumeroUnicoAutorizacion(), 
							lstPrestacionDTO, ConstantesEJB.COD_FORMATO_OPS, nameArchivo, 
							((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
					
				}
				String nombreArchivoCompleto = pathGeneracionArchivo + ConstantesEJB.ARCHIVO_PDF;
				pathGeneratedFiles = new File(tempDirectory + File.separator + ConstantesEJB.GENERATED_FILES);
				String[] entries = pathGeneratedFiles.list();
				ByteArrayOutputStream byteOutPutStream = FilesUtil.appendFiles(pathGeneratedFiles, entries, nombreArchivoCompleto);
				generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream, nameArchivo);
				//zip = FilesUtil.zipFiles(pathGeneratedFiles, entries);
				//generarReporte.descargaFileZip(httpServletResponse, zip, nameArchivo);
				FacesContext.getCurrentInstance().responseComplete();
				byteOutPutStream.close();
				if (pathGeneratedFiles != null && pathGeneratedFiles.isDirectory()) {
					FilesUtil.deleteDirectoryFiles(pathGeneratedFiles);
				}
			} else {
				params.put(ConstantesWeb.NUMERO_UNICO_OPS, listaAutorizacionServicioVO.get(0).getNumeroUnicoAutorizacion());
				ByteArrayOutputStream byteOutPutStream = generarReporte.generarReporteAutorizacionServicioPDF(params, FormatType.PDF);
				generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream, nameArchivo);
				FacesContext.getCurrentInstance().responseComplete();
				// Guarda historico de descargas
				consultarSolicitudController.guardarHistoricoDescargaFormato(consecutivoSolicitud, listaAutorizacionServicioVO.get(0).getNumeroUnicoAutorizacion(), 
						lstPrestacionDTO, ConstantesEJB.COD_FORMATO_OPS, nameArchivo, 
						((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			}				
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}		
	}	
	
	/**Metodo que permite genera el formato de negacion */
	public void formatoNegacionSolicitud(Integer consecutivoSolicitud, String tipodocAfiliado, String numDocumentoAfiliado, String numRadicado, List<PrestacionDTO> lstPrestacionDTO){
		
		String nameArchivo = null;
		String subReporteFundamentos;
		String subReporteAlternativas;
		
		try {		
			
			//Se extrae la ruta compartida donde se encuentra la ruta del sub reporte del negacion para los fundamentos
			String propiedadRutaRecursos = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
			subReporteFundamentos = generarReporte.obtenerRutaReporteJasper(propiedadRutaRecursos);
					       
			//Se extrae la ruta compartida donde se encuentra la ruta del sub reporte del negacion para los fundamentos
			String propiedadJasper = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_NEGACION_SUB_FUNDAMENTOS);
			subReporteFundamentos = subReporteFundamentos + generarReporte.obtenerRutaReporteJasper(propiedadJasper);
					
			
			//Se extrae la ruta compartida donde se encuentra la ruta del sub reporte del negacion para las alternativas
			propiedadRutaRecursos = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
			subReporteAlternativas = generarReporte.obtenerRutaReporteJasper(propiedadRutaRecursos);
		       
			//Se extrae la ruta compartida donde se encuentra la ruta del sub reporte del negacion para las alternativas
			propiedadJasper = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_NEGACION_SUB_ALTERNATIVAS);
			subReporteAlternativas = subReporteAlternativas + generarReporte.obtenerRutaReporteJasper(propiedadJasper);
						
			nameArchivo = tipodocAfiliado + numDocumentoAfiliado + ConstantesWeb.SUFIJO_NOMBRE_REPORTE_NEGACION + numRadicado;
						
			Map<String, Object> params = new HashMap<String, Object>();			
			params.put(ConstantesWeb.CONSECUTIVO_SOLICITUD, consecutivoSolicitud);
			params.put(ConstantesWeb.CONSECUTIVO_SERVICIO_NEGADO, lstPrestacionDTO.get(0).getConsecutivoServicioSolicitado());
			params.put(ConstantesWeb.USUARIO_SISTEMA, FacesUtils.getUserName().trim());
			params.put(ConstantesWeb.RUTA_FUNDAMENTO_LEGAL, subReporteFundamentos);
			params.put(ConstantesWeb.RUTA_REPORTE_ALTERNATIVAS, subReporteAlternativas);	
			
			/** Se llama al metodo que genera el formato de devoluciones */
			ByteArrayOutputStream byteOutPutStream = generarReporte.generarReporteNegacionPDF(params, FormatType.PDF);
			generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream, nameArchivo);
			FacesContext.getCurrentInstance().responseComplete();
			
			// Guarda historico de descargas
			consultarSolicitudController.guardarHistoricoDescargaFormato(consecutivoSolicitud, null, lstPrestacionDTO, 
					ConstantesEJB.COD_FORMATO_NEGACION, nameArchivo, 
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}			
	}
	/**
	 * Gennera el informe del cierre oficina
	 * 
	 * @param informeCierreOficinaVO
	 * @throws LogicException
	 */
	public void generarInformeCierreOficina(
			InformeCierreOficinaVO informeCierreOficinaVO)
			throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_INFORME_CIERRE_OFICINA
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ConstantesWeb.PARAM_OFICINA_USER, informeCierreOficinaVO
				.getCoordinadorVO().getLgnUsro());
		params.put(ConstantesWeb.PARAM_OFICINA_RESPONSABLE,
				informeCierreOficinaVO.getCoordinadorVO().getNombreCompleto());
		params.put(ConstantesWeb.PARAM_FECHA, new Date());
		params.put(ConstantesWeb.PARAM_OFICINA_DESC, informeCierreOficinaVO
				.getCoordinadorVO().getDscrpcnOfcna());
		params.put(ConstantesWeb.PARAM_SUMA_TOTAL_RECIBIDO,
				informeCierreOficinaVO.getCierreOficinaVO().getTtlRcbdoOfcna());
		params.put(ConstantesWeb.PARAM_SUMA_BASE, informeCierreOficinaVO
				.getCierreOficinaVO().getBseOfcna());
		params.put(ConstantesWeb.PARAM_SUMA_FALTANTE,
				informeCierreOficinaVO.getTotalFaltanteTabla());
		params.put(ConstantesWeb.PARAM_SUMA_SOBRANTE,
				informeCierreOficinaVO.getTotalSobranteTabla());
		params.put(ConstantesWeb.PARAM_TOTAL_CIERRE_OFICINA,
				informeCierreOficinaVO.getTotalCierreOficina());
		params.put(ConstantesWeb.PARAM_CNS_CIERRE_OFICINA,
				informeCierreOficinaVO.getCierreOficinaVO()
						.getCnsctvoCrreOfcna());
		params.put(ConstantesWeb.PARAM_FALTANTE, informeCierreOficinaVO
				.getCierreOficinaVO().getFltnteOfcna());
		params.put(ConstantesWeb.PARAM_SUMA_BASE_COLUMNA,
				informeCierreOficinaVO.getTotalBaseTabla());
		params.put(ConstantesWeb.PARAM_SOBRANTE, informeCierreOficinaVO
				.getCierreOficinaVO().getSbrnteOfcna());
		params.put(ConstantesWeb.PARAM_TOTAL_EFECTIVO_CAJA,
				informeCierreOficinaVO.getTotalEfectivoCajaTabla());
		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarInformeCierreOficina(params, FormatType.PDF);
		generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}

	/**
	 * Genera el excel del reporte
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param consecutivoOficina
	 * @param usuario
	 * @throws LogicException
	 */
	public void generarReporteCierreCaja(Date fechaInicial, Date fechaFinal,
			Integer consecutivoOficina, String usuario, Integer consecutivoSede) throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_INFORME_CIERRE_CAJA
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put(ConstantesWeb.FECHA_INICIAL_INFORME_CIERRE_CAJA,fechaInicial);
		params.put(ConstantesWeb.FECHA_FINAL_INFORME_CIERRE_CAJA, fechaFinal);
		params.put(ConstantesWeb.OFICINA, consecutivoOficina);
		params.put(ConstantesWeb.USUARIO_INFORME_CIERRE_CAJA, usuario);
		params.put(ConstantesWeb.CONSECUTIVO_SEDE, consecutivoSede);
		
		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarReporteInformeCierreCaja(params, FormatType.XLS);
		generarReporte.descargaFileExcel(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}

	/**
	 * Generar el reporte de cierre de caja detallado
	 * 
	 * @param fechaCierre
	 * @param login
	 * @param oficina
	 * @param nombreUsuario
	 * @throws LogicException
	 */
	public void generarReporteInformeDetalleCierreCaja(Date fechaCierre,
			String login, String oficina, String nombreUsuario, Date fechaApertura)
			throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_INFORME_CIERRE_CAJA
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ConstantesWeb.FECHA_CIERRE, fechaCierre);
		params.put(ConstantesWeb.LOGIN, login);
		params.put(ConstantesWeb.OFICINA, oficina);
		params.put(ConstantesWeb.USUARIO, nombreUsuario);
		params.put(ConstantesWeb.FECHA_APERTURA, fechaApertura);
		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarReporteInformeDetalleCierreCaja(params, FormatType.PDF);
		generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}

	/**
	 * Genera reporte de nota credito
	 * 
	 * @param reporteNotaCreditoVO
	 * @throws LogicException
	 */
	public void generarReporteNotaCredito(
			ReporteNotaCreditoVO reporteNotaCreditoVO) throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_REPORTE_NOTA_CREDITO
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();

		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarReporteNotaCredito(params, reporteNotaCreditoVO);
		generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}

	/**
	 * Genera informe de cierre caja
	 * 
	 * @param informeCierreCajaVO
	 * @throws LogicException
	 * @throws JRException
	 * @throws FileNotFoundException
	 */
	public void generarInformeCierreCaja(InformeCierreCajaVO informeCierreCajaVO)
			throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_INFO_CIERRE_CAJA
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();

		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarInformeCierreCaja(params, informeCierreCajaVO);
		generarReporte.descargaFilePdf(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}

	/**
	 * Generación de recibo de caja
	 * 
	 * @param lista
	 * @param nombreArchivo
	 * @throws LogicException
	 * @throws FileNotFoundException
	 * @throws JRException
	 * @throws Exception
	 */
	public void generarReciboCaja(List<ReciboCajaJasperVO> lista,
			String nombreArchivo, Date fechaReporte) throws LoginException {
		try {

			GenerarReporte reporte = new GenerarReporte();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ConstantesWeb.FECHA_REPORTE, fechaReporte);
			ByteArrayOutputStream byteOutPutStream = reporte.generarReciboCaja(
					lista, params);

			generarReporte.descargaFilePdf(httpServletResponse,
					byteOutPutStream, nombreArchivo);
			FacesContext.getCurrentInstance().responseComplete();

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			throw new LoginException(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param parametro
	 * @throws LogicException
	 */
	public void generarReporteCierreCajaDetallado(ParametroReporteCierreCajaVO parametro) throws LogicException {
		String nombreArchivo = ConstantesWeb.NOMBRE_ARCHIVO_INFORME_CIERRE_CAJA
				+ new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ConstantesWeb.NUI_AFILIADO, parametro.getNui());
		params.put(ConstantesWeb.OFICINA_ID, parametro.getConsecutivoCodigoOficina());
		params.put(ConstantesWeb.FECHA_INICIAL, parametro.getFechaInicial());
		params.put(ConstantesWeb.OPS, parametro.getNumeroOps());
		params.put(ConstantesWeb.RECIBO, parametro.getNumeroRecibo());
		params.put(ConstantesWeb.LOGIN, parametro.getLogin());
		params.put(ConstantesWeb.CONSECUTIVO_SEDE, parametro.getConsecutivoCodigoSede());
		
		ByteArrayOutputStream byteOutPutStream = generarReporte
				.generarReporteInformeDetalleCaja(params, FormatType.XLS);
		generarReporte.descargaFileExcel(httpServletResponse, byteOutPutStream,
				nombreArchivo);
		FacesContext.getCurrentInstance().responseComplete();
	}
}