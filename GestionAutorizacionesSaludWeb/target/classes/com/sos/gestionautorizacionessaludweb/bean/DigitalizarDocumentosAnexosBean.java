package com.sos.gestionautorizacionessaludweb.bean;

import java.util.List;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaParametros;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.visos.service.vo.Indice;

/**
 * Class DigitalizarDocumentosAnexosBean
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 25/01/2016
 *
 */
public class DigitalizarDocumentosAnexosBean {
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(DigitalizarDocumentosAnexosBean.class);	

	private ControladorVisosServiceEJB controladorVisosServiceEJB;

	

	/**
	 * Variable que calcula y almacena el parametro de digitalizador
	 */
	private String digitalizadorDocumento;

	/**
	 * Variable que calcula y almacena el parametro de tipo de documento
	 */
	private String tipoDocumento;

	/**
	 * Variable utilizada para controlar si el proceso de carga de archivo se
	 * completo correctamente o no
	 */
	private boolean archivoCargado;

	/**
	 * Variable que almacena el listado de codigos de indices
	 */
	private Integer[] indicesDocumentosAnexos;

	public DigitalizarDocumentosAnexosBean() {
		controladorVisosServiceEJB = new ControladorVisosServiceEJB();
		try {
			controladorVisosServiceEJB.crearConexionVisosService();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo que permite crear un documento en visos Se deben enviar el List
	 * con los documentos soporte y el arreglo de indices
	 * 
	 * @throws LogicException
	 */
	public long anexarDocumentos(List<SoporteVO> lDocumentoSoporte, Indice[] indices, Integer consecutivoSolicitud) throws LogicException {
		this.archivoCargado = false;
		long idDocumentoAnexado = 0;
		/* Invocacion a metodo de cargarDocumento servicio EJB de Visos */
		idDocumentoAnexado = controladorVisosServiceEJB.guardarDocumentosSoporte(lDocumentoSoporte,// informacion
																									// soportes
				indices,// indices[codigo][valor]
				getDigitalizadorDocumento(),// digitalizador
				getTipoDocumento(), // tipo documento
				consecutivoSolicitud);
		archivoCargado = true;
		return idDocumentoAnexado;
	}

	public Integer[] getIndicesDocumentosAnexos() throws LogicException {

		if (indicesDocumentosAnexos == null) {
			indicesDocumentosAnexos = (Integer[]) ConsultaParametros.obtenerListaValoresFormateados("parametro.digitalizarAnexo.codigosIndices", "int", ",");
		}
		return indicesDocumentosAnexos;
	}

	public void setIndicesDocumentosAnexos(Integer[] indicesDocumentosAnexos) {
		this.indicesDocumentosAnexos = indicesDocumentosAnexos;
	}

	public String getDigitalizadorDocumento() throws LogicException {

		if (digitalizadorDocumento == null) {
			digitalizadorDocumento = ConsultaParametros.obtenerValorParametro("parametro.digitalizarAnexo.digitalizador");
		}
		return digitalizadorDocumento;
	}

	public void setDigitalizadorDocumento(String digitalizadorDocumento) {
		this.digitalizadorDocumento = digitalizadorDocumento;
	}

	public String getTipoDocumento() {

		if (tipoDocumento == null) {
			tipoDocumento = ConsultaParametros.obtenerValorParametro("parametro.digitalizarAnexo.tipoDocumento");
		}

		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public boolean isArchivoCargado() {
		return archivoCargado;
	}

	public void setArchivoCargado(boolean archivoCargado) {
		this.archivoCargado = archivoCargado;
	}
}
