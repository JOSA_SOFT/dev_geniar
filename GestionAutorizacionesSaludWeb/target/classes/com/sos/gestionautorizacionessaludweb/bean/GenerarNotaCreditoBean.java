package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeOPSVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteNotaCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoFamiliarVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.controller.ReciboCajaController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConversorNumeroLetras;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;

import co.eps.sos.dataccess.exception.DataAccessException;

/**
 * Se encarga de manejar la generacion de nota credito
 * 
 * @author GENIAR
 */
public class GenerarNotaCreditoBean implements Serializable {
	private static final long serialVersionUID = -8847430021793897883L;

	private String consecutivoBanco;
	private Integer consectivoMetodoDevolucion;
	private String tipoCuenta;
	private String titular;

	private String correoElectronico;
	private GenerarReciboBean recibo;
	private boolean mostrarModalCruce;
	private boolean mostrarPanelBancos;

	private static final String COLOR_ROJO = ConstantesWeb.COLOR_ROJO;
	private List<InformacionOpsValorVO> ops = new ArrayList<InformacionOpsValorVO>();
	private static final Logger LOG = Logger.getLogger(GenerarNotaCreditoBean.class);

	private List<SelectItem> listaMetodoDevolucion;
	private List<SelectItem> listaTipoCuenta;
	private List<SelectItem> listaTitulares;
	private List<SelectItem> listaBancos;

	private Double valorADevolverNotaCredito;
	private String observacionNotaCredito;
	private Long parametroMontoLimite;

	private String numeroTelefono;
	private String decisionModal;
	private String numeroCuenta;

	private String numeroIdentificacionAfiliado;
	private int consecutivoTipoIdentificacionAfiliado;
	private Integer numeroNotaCredito;
	private DatosContactoVO contacto = new DatosContactoVO();
	private boolean deshabilitarBotonGenerarNotaCredito = false;
	private boolean deshabilitarBotonGenerarNotaCreditoBanco = false;

	private transient AfiliadoVO afiliado;
	private String accionARealizar;
	private int consecutivoContacto;
	private int consecutivoDocumentoCaja;
	
	private boolean entregaDineroNotaCredito = false;

	/**
	 * Constructor del bean
	 */
	public GenerarNotaCreditoBean() {
		recibo = (GenerarReciboBean) FacesUtils.getManagedBean(ConstantesWeb.GENERAR_RECIBO_BEAN);
		try {
			listaMetodoDevolucion = CargarCombosUtil.obtenerListaSelectParametros(null, new CargarCombosController(),
					ConstantesWeb.COMBO_METODO_DEVOLUCION);
			listaBancos = CargarCombosUtil.obtenerListaSelectParametros(null, new CargarCombosController(),
					ConstantesWeb.COMBO_BANCO);
			listaTipoCuenta = new ArrayList<SelectItem>();
			listaTitulares = new ArrayList<SelectItem>();
			SelectItem item;
			SelectItem item2;
			item = new SelectItem(ConstantesWeb.COMBO_CUENTA_AHORRO_VALOR, ConstantesWeb.COMBO_CUENTA_AHORRO_LABEL);
			item2 = new SelectItem(ConstantesWeb.COMBO_CUENTA_CORRIENTE_VALOR,
					ConstantesWeb.COMBO_CUENTA_CORRIENTE_LABEL);
			listaTipoCuenta.add(item);
			listaTipoCuenta.add(item2);
		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		} catch (IOException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
	}

	private void mostrarMensajeExcepcionNotaCredito(Exception e) {
		LOG.error(e.getMessage(), e);
		MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
	}

	private void mostrarMensajeExcepcionNotaCredito(String mensajeError) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, mensajeError, ConstantesWeb.CADENA_VACIA));
	}

	/**
	 * Forma el objeto de infrmacion de contacto bancario.
	 * 
	 * @return
	 */
	private DatosContactoVO formarDatoContacto() {
		contacto.setConsecutivoBanco(Integer.parseInt(consecutivoBanco));
		contacto.setConsecutivoMetodoDevolucion(consectivoMetodoDevolucion);
		contacto.setCorreoElectronico(correoElectronico);
		contacto.setNumeroCuenta(numeroCuenta);
		contacto.setTelefono(numeroTelefono);

		if (!Utilidades.validarCamposVacios(tipoCuenta)) {
			contacto.setTipoCuenta(null);
		} else {
			contacto.setTipoCuenta(tipoCuenta);
		}

		if (!Utilidades.validarCamposVacios(titular)) {
			contacto.setTitular("");
		} else {
			SelectItem itemTitular = encontrarTitularSeleccionado(titular);
			contacto.setTitular(itemTitular.getValue().toString()+ConstantesWeb.SEPARADOR+itemTitular.getLabel());
		}
		return contacto;
	}

	/**
	 * Valida los datos de contacto de la persona.
	 * 
	 * @return
	 */
	private String validarContacto() {
		String mensajeError = "";
		if (Utilidades.validarNullCadena(numeroTelefono).equals(ConstantesWeb.CADENA_VACIA)) {
			mensajeError += ConstantesWeb.NUMERO_TELEFONO_OBLIGATORIO + " ";
		}
		if (Utilidades.validarNullCadena(correoElectronico).equals(ConstantesWeb.CADENA_VACIA)) {
			mensajeError += ConstantesWeb.CORREO_ELECTRONICO_OBLIGATORIO + " ";
		} else {
			if (!Utilidades.validarCorreoElectronico(correoElectronico)) {
				mensajeError += ConstantesWeb.FORMATO_ELECTRONICO_OBLIGATORIO + " ";
			}
		}
		return mensajeError;
	}

	/**
	 * Validacion de los cambpos de contacto bancario
	 * 
	 * @return
	 */
	private String validarCampos() {
		String respuesta = "";
		if (consectivoMetodoDevolucion == -1) {
			respuesta += ConstantesWeb.METODO_DEVOLUCION_OBLIGATORIO + " ";
		}
		if (consectivoMetodoDevolucion == 1) {
			respuesta += this.validarContacto();
		}
		return respuesta;
	}

	/**
	 * Guarda y valida la nota credito con los datos de contacto bancario.
	 * 
	 * @return
	 */
	public String guardarNotaCreditoContacto() {
		contacto = this.formarDatoContacto();
		try {
			String mensajeError = this.validarCampos();
			if (Utilidades.validarNullCadena(mensajeError).equals(ConstantesWeb.CADENA_VACIA)) {
				this.guardarDocumento(valorADevolverNotaCredito,
						ConstantesWeb.CODIGO_ESTADO_DOCUMENTO_CAJA_PENDIENTE_COBRO, contacto);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						mensajeError, ConstantesWeb.MENSAJE_EXITO_NOTA_CREDITO_CAJA + valorADevolverNotaCredito));
				recibo.setTabSeleccionado(ConstantesWeb.TAB_COMPROBANTES);
				recibo.setDesabilitarNotaCredito(false);
				recibo.setMostrarComprobanteRecibo(true);
				deshabilitarBotonGenerarNotaCreditoBanco = true;
				contacto = new DatosContactoVO();
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, mensajeError, ConstantesWeb.CADENA_VACIA));
			}

		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
		return null;
	}

	/**
	 * 
	 * @param reporteNotaCreditoVO
	 */
	private void generarReporteNotaCredito(ReporteNotaCreditoVO reporteNotaCreditoVO) {
		GenerarReporteBean generarReporteBean = new GenerarReporteBean();
		try {
			generarReporteBean.generarReporteNotaCredito(reporteNotaCreditoVO);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			this.mostrarMensajeExcepcionNotaCredito(e.getMessage());
		}
	}

	/**
	 * 
	 * @param saldoAFavor
	 * @param estadoNota
	 * @param contacto
	 * @throws LogicException
	 * @throws ConnectionProviderException
	 * @throws DataAccessException
	 */
	private void guardarDocumento(double saldoAFavor, int estadoNota, DatosContactoVO contacto) throws LogicException {
		ReciboCajaController reciboCaja = new ReciboCajaController();

		DocumentoCajaVO notaCredito = generarNotaCreditoDocumento(saldoAFavor, estadoNota);

		if (ConstantesWeb.ACCION_CREAR_NOTA_CREDITO.equals(accionARealizar)) {
			String guardaDetalle = decisionModal.equalsIgnoreCase(ConstantesWeb.SI) ? ConstantesWeb.NO
					: ConstantesWeb.SI;
			DocumentoCajaVO documentoCajaVO = reciboCaja.guardarDocumentoCaja(notaCredito, contacto, ops,
					guardaDetalle);
			numeroNotaCredito = documentoCajaVO.getNumeroDocumento();
		} else {
			notaCredito.setPkDocCaja(consecutivoDocumentoCaja);
			try {
				generarNotaCreditoConfirmacionBanco(contacto, notaCredito);
			} catch (LogicException e) {
				throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
			}
		}
	}

	private void generarNotaCreditoConfirmacionBanco(DatosContactoVO contacto, DocumentoCajaVO notaCredito)
			throws LogicException {
		try {
			GestionCajaController control;
			control = new GestionCajaController();
			if (consecutivoContacto <= 0 && decisionModal.equalsIgnoreCase(ConstantesWeb.SI)) {
				consecutivoContacto = control.guardarDatosContacto(contacto, FacesUtils.getUserName());
			} else {
				if (decisionModal.equalsIgnoreCase(ConstantesWeb.SI)) {
					contacto.setConsecutivoDatoContacto(consecutivoContacto);
					control.guardarDatosContacto(contacto, FacesUtils.getUserName());

				} else {
					recibo.setMostrarGenerarRecibo(false);
				}
			}
			notaCredito.setDatoContacto(consecutivoContacto);
			control.actualizarNotaCredito(notaCredito);
			actualizarContactoInformeGeneral();
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (DataAccessException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

	}

	private void actualizarContactoInformeGeneral() {
		InformeGeneralReciboCajaBean informe = (InformeGeneralReciboCajaBean) FacesUtils
				.getManagedBean(ConstantesWeb.INFORME_GENERAL_RECIBO_BEAN);
		if (informe != null && informe.getNotaCreditoSeleccionada() != null) {
			informe.getNotaCreditoSeleccionada().setConsecutivoContacto(consecutivoContacto);
		}
	}

	private DocumentoCajaVO generarNotaCreditoDocumento(double saldoAFavor, int estadoNota) {
		DocumentoCajaVO notaCredito = new DocumentoCajaVO();
		notaCredito.setObservacion(this.observacionNotaCredito);
		notaCredito.setEstadoDocumento(estadoNota);
		notaCredito.setNuiAfiliado(recibo.getNuiAfiliado());
		notaCredito.setConceptoGeneracion(ConstantesWeb.CODIGO_CONCEPTO_NOTA_CAJA_EXCEDENTE);
		notaCredito.setValorAfiliad(saldoAFavor);
		notaCredito.setTipoDocumento(ConstantesWeb.CODIGO_TIPO_DOCUMENTO_CAJA_NOTA_CREDITO);
		notaCredito.setDatoContacto(null);
		notaCredito.setDetalleMovimientoCaja(null);
		notaCredito.setUsuarioCreacion(FacesUtils.getUserName());
		return notaCredito;
	}

	/**
	 * Guarda la nota credito
	 * 
	 * @return
	 */
	public String guardarNotaCredito() {
		try {
			if (decisionModal.equalsIgnoreCase(ConstantesWeb.SI)) {
				this.mostrarModalBanco();
			} else {
				this.mostrarModalReciboCaja();
				this.guardarDocumento(valorADevolverNotaCredito, ConstantesWeb.CODIGO_ESTADO_DOCUMENTO_CAJA_COBRADA,
						null);
				recibo.setTabSeleccionado(ConstantesWeb.TAB_COMPROBANTES);
				recibo.setDesabilitarNotaCredito(true);
				recibo.setMostrarComprobanteRecibo(true);

				DecimalFormat myFormatter = new DecimalFormat(ConstantesWeb.FORMATO_DINERO);
				String valorFormatoDinero =  myFormatter.format(valorADevolverNotaCredito);
				
				MostrarMensaje.mostrarMensaje(ConstantesWeb.MENSAJE_EXITO_NOTA_CREDITO_CAJA + " " + valorFormatoDinero,ConstantesWeb.COLOR_AZUL);
			}
		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
		mostrarModalCruce = false;
		return null;
	}

	private void mostrarModalReciboCaja() {
		recibo.setMostrarModalReciboCaja(true);
	}

	/**
	 * Obtiene el parametro limite consumiendo el bean usuario que realiza la
	 * consulta.
	 * 
	 * @throws IOException
	 * @throws ConnectionProviderException
	 * @throws LogicException
	 */
	private void obtenerParametroLimite() throws LogicException {
		try {
			CargarCombosController cargarCombosController = new CargarCombosController();
			List<ParametrosGeneralesVO> parametros = cargarCombosController.consultaParametrosGenerales();
			for (ParametrosGeneralesVO parametro : parametros) {
				if (parametro.getConsecutivoCodigoParametroGeneral() == Integer
						.valueOf(Messages.getValorParametro(ConstantesWeb.PARAMETRO_LIMITE_VALOR_CAJA))) {
					parametroMontoLimite = parametro.getValorParametroGeneral();
					break;
				}
			}
		} catch (ConnectionProviderException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (IOException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}
	
	private SelectItem encontrarTitularSeleccionado(String titular){
		SelectItem titularCombo = new SelectItem("","");
		for (SelectItem selectItem : listaTitulares) {
			if (selectItem.getValue().toString().equals(titular)){
				titularCombo = selectItem;
			}
		}
		return titularCombo;
	}

	/**
	 * Crea el arreglo de titulares
	 * 
	 * @throws ConnectionProviderException
	 * @throws IOException
	 * @throws LogicException
	 */
	private void formarListadoTitulares() throws LogicException {
		List<GrupoFamiliarVO> grupoFamiliar;
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			grupoFamiliar = consultaAfiliadoController
					.consultaGrupoFamiliarAfiliado(consecutivoTipoIdentificacionAfiliado, numeroIdentificacionAfiliado);
		} catch (Exception e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

		listaTitulares = new ArrayList<SelectItem>();
		for (GrupoFamiliarVO persona : grupoFamiliar) {
			if (persona.getAfiliadoGrupoFamiliar().getConsecutivoTipoAfiliado() == ConstantesWeb.CONSECUTIVO_CODIGO_COTIZANTE) {
				SelectItem item = new SelectItem(
						persona.getAfiliadoGrupoFamiliar().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim()
								+ persona.getAfiliadoGrupoFamiliar().getNumeroIdentificacion().trim(),
						persona.getAfiliadoGrupoFamiliar().getNombreCompleto().trim());
				if (!this.validarExistenciaAfiilado(listaTitulares, item)) {
					listaTitulares.add(item);
				}
			}
		}
	}

	private boolean validarExistenciaAfiilado(List<SelectItem> lista, SelectItem item) {
		boolean igual = false;
		for (SelectItem aux : lista) {
			if (aux.getValue().toString().equals(item.getValue().toString())) {
				igual = true;
				break;
			}
		}
		return igual;
	}

	/**
	 * Metodo que se encarga de registrar la nota credito
	 * 
	 * @return
	 */
	public String generarNotaCredito() {
		try {
			if (Utilidades.validarNullCadena(observacionNotaCredito) != "" && observacionNotaCredito.length() > 250) {
				MostrarMensaje.mostrarMensaje(ConstantesWeb.MENSAJE_ERROR_DESCRIPCION_GENERAR_NOTA_CREDITO,
						ConstantesWeb.COLOR_ROJO);
			} else {
				this.obtenerParametroLimite();

				if ((valorADevolverNotaCredito) > parametroMontoLimite || !entregaDineroNotaCredito) {
					this.decisionModal = ConstantesWeb.SI;
					this.mostrarModalBanco();
				} else {
					mostrarModalCruce = true;
					recibo.setMostrarModalReciboCaja(false);
				}
				deshabilitarBotonGenerarNotaCredito = true;
			}
		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
		return null;
	}

	private void mostrarModalBanco() throws LogicException {
		this.mostrarModalReciboCaja();
		this.formarListadoTitulares();
		recibo.setTabSeleccionado(ConstantesWeb.TAB_DATOS_BANCO);
		recibo.setMostrarTabDatosBancoNotaCredito(true);
		recibo.setDesabilitarNotaCredito(false);
	}

	/**
	 * 
	 * @param gestionCajaController
	 * @return
	 * @throws DataAccessException
	 */
	private String obtenerOficina(GestionCajaController gestionCajaController) throws DataAccessException {
		String oficina = ConstantesWeb.CADENA_VACIA;
		OficinasVO consultarOficina = gestionCajaController
				.consultarOficinaPorMovimientoActual(FacesUtils.getUserName());
		if (consultarOficina != null) {
			oficina = consultarOficina.getDescripcionOficina();
		}
		return oficina;
	}

	/**
	 * Obtiene nombre del banco
	 * 
	 * @return
	 */
	private String nombreBanco() {
		for (SelectItem selectItem : listaBancos) {
			if (selectItem.getValue().toString().equals(String.valueOf(this.consecutivoBanco))) {
				return selectItem.getLabel();
			}
		}
		return null;
	}

	/**
	 * Obtiene el tipo de cuenta.
	 * 
	 * @return
	 */
	private String nombreTipoCuenta() {
		for (SelectItem selectItem : listaTipoCuenta) {
			if (selectItem.getValue().toString().equals(this.tipoCuenta)) {
				return selectItem.getLabel();
			}
		}
		return null;
	}

	/**
	 * Metodo que se encarga de generar el PDF de la nota credito
	 * 
	 * @return
	 */
	public String generarComprobanteNotaCredito() {
		try {
			ReporteNotaCreditoVO reporteNotaCreditoVO = new ReporteNotaCreditoVO();
			reporteNotaCreditoVO.setFecha(new Date());
			reporteNotaCreditoVO.setNombre(afiliado.getNombreCompleto());

			reporteNotaCreditoVO.setNombreBanco(this.nombreBanco());

			GestionCajaController gestionCajaController = new GestionCajaController();
			UsuarioWebVO usuarioWebVO = gestionCajaController.consultarUsuarioWeb(FacesUtils.getUserName(),
					ConstantesWeb.MODULO_CAJA);
			reporteNotaCreditoVO.setNombreUsuarioAtencion(usuarioWebVO.getNombreCompleto());

			reporteNotaCreditoVO.setNumeroCuenta(numeroCuenta);
			reporteNotaCreditoVO.setNumeroNotaCredito(numeroNotaCredito);
			reporteNotaCreditoVO.setTipoCuenta(this.nombreTipoCuenta());

			reporteNotaCreditoVO.setOficina(this.obtenerOficina(gestionCajaController));

			reporteNotaCreditoVO
					.setTipoEidentificacion(afiliado.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion()
							+ ConstantesWeb.CADENA_EN_BLANCO + afiliado.getNumeroIdentificacion());

			reporteNotaCreditoVO.setValorLetras(ConversorNumeroLetras.convertirNumeroLetras(
					Double.parseDouble(valorADevolverNotaCredito.intValue() + ConstantesWeb.CADENA_VACIA)));

			List<InformeOPSVO> list = gestionCajaController.consultarGrupoOPSconValor(numeroNotaCredito);
			reporteNotaCreditoVO.setListOPS(list);
			double total = 0.0;
			for (InformeOPSVO informeOPSVO : list) {
				total += informeOPSVO.getValor() == null ? 0.0 : informeOPSVO.getValor().doubleValue();
			}
			reporteNotaCreditoVO.setSumaTotalOPSs(total);

			this.generarReporteNotaCredito(reporteNotaCreditoVO);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		} catch (DataAccessException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
		return null;
	}

	public Double getValorADevolverNotaCredito() {
		return valorADevolverNotaCredito;
	}

	public void setValorADevolverNotaCredito(Double valorADevolverNotaCredito) {
		this.valorADevolverNotaCredito = valorADevolverNotaCredito;
	}

	public String getObservacionNotaCredito() {
		return observacionNotaCredito;
	}

	public void setObservacionNotaCredito(String observacionNotaCredito) {
		this.observacionNotaCredito = observacionNotaCredito;
	}

	public List<InformacionOpsValorVO> getOps() {
		return ops;
	}

	public void setOps(List<InformacionOpsValorVO> ops) {
		this.ops = new ArrayList<InformacionOpsValorVO>();
		this.ops = ops;
	}

	public boolean isMostrarModalCruce() {
		return mostrarModalCruce;
	}

	public void setMostrarModalCruce(boolean mostrarModalCruce) {
		this.mostrarModalCruce = mostrarModalCruce;
	}

	public String getDecisionModal() {
		return decisionModal;
	}

	public void setDecisionModal(String decisionModal) {
		this.decisionModal = decisionModal;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public List<SelectItem> getListaMetodoDevolucion() {
		return listaMetodoDevolucion;
	}

	public void setListaMetodoDevolucion(List<SelectItem> listaMetodoDevolucion) {
		this.listaMetodoDevolucion = listaMetodoDevolucion;
	}

	public List<SelectItem> getListaTipoCuenta() {
		return listaTipoCuenta;
	}

	public void setListaTipoCuenta(List<SelectItem> listaTipoCuenta) {
		this.listaTipoCuenta = listaTipoCuenta;
	}

	public List<SelectItem> getListaTitulares() {
		return listaTitulares;
	}

	public void setListaTitulares(List<SelectItem> listaTitulares) {
		this.listaTitulares = listaTitulares;
	}

	public List<SelectItem> getListaBancos() {
		return listaBancos;
	}

	public void setListaBancos(List<SelectItem> listaBancos) {
		this.listaBancos = listaBancos;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public boolean isMostrarPanelBancos() {
		return mostrarPanelBancos;
	}

	public void setMostrarPanelBancos(boolean mostrarPanelBancos) {
		this.mostrarPanelBancos = mostrarPanelBancos;
	}

	public String getConsecutivoBanco() {
		return consecutivoBanco;
	}

	public void setConsecutivoBanco(String consecutivoBanco) {
		this.consecutivoBanco = consecutivoBanco;
	}

	public Integer getConsectivoMetodoDevolucion() {
		return consectivoMetodoDevolucion;
	}

	public void setConsectivoMetodoDevolucion(Integer consectivoMetodoDevolucion) {
		this.consectivoMetodoDevolucion = consectivoMetodoDevolucion;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public void setParametroMontoLimite(Long parametroMontoLimite) {
		this.parametroMontoLimite = parametroMontoLimite;
	}

	public void setNumeroIdentificacionAfiliado(String numeroIdentificacionAfiliado) {
		this.numeroIdentificacionAfiliado = numeroIdentificacionAfiliado;
	}

	public void setConsecutivoTipoIdentificacionAfiliado(int consecutivoTipoIdentificacionAfiliado) {
		this.consecutivoTipoIdentificacionAfiliado = consecutivoTipoIdentificacionAfiliado;
	}

	public boolean isDeshabilitarBotonGenerarNotaCredito() {
		return deshabilitarBotonGenerarNotaCredito;
	}

	public void setDeshabilitarBotonGenerarNotaCredito(boolean deshabilitarBotonGenerarNotaCredito) {
		this.deshabilitarBotonGenerarNotaCredito = deshabilitarBotonGenerarNotaCredito;
	}

	public boolean isDeshabilitarBotonGenerarNotaCreditoBanco() {
		return deshabilitarBotonGenerarNotaCreditoBanco;
	}

	public void setDeshabilitarBotonGenerarNotaCreditoBanco(boolean deshabilitarBotonGenerarNotaCreditoBanco) {
		this.deshabilitarBotonGenerarNotaCreditoBanco = deshabilitarBotonGenerarNotaCreditoBanco;
	}

	public DatosContactoVO getContacto() {
		return contacto;
	}

	public void setContacto(DatosContactoVO contacto) {
		this.contacto = contacto;
	}

	public AfiliadoVO getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(AfiliadoVO afiliado) {
		try {
			this.formarListadoTitulares();
		} catch (LogicException e) {
			this.mostrarMensajeExcepcionNotaCredito(e);
		}
		this.afiliado = afiliado;
	}

	public String getAccionARealizar() {
		return accionARealizar;
	}

	public void setAccionARealizar(String accionARealizar) {
		this.accionARealizar = accionARealizar;
	}

	public int getConsecutivoContacto() {
		return consecutivoContacto;
	}

	public void setConsecutivoContacto(int consecutivoContacto) {
		this.consecutivoContacto = consecutivoContacto;
	}

	public int getConsecutivoDocumentoCaja() {
		return consecutivoDocumentoCaja;
	}

	public void setConsecutivoDocumentoCaja(int consecutivoDocumentoCaja) {
		this.consecutivoDocumentoCaja = consecutivoDocumentoCaja;
	}

	public Integer getNumeroNotaCredito() {
		return numeroNotaCredito;
	}

	public void setNumeroNotaCredito(Integer numeroNotaCredito) {
		this.numeroNotaCredito = numeroNotaCredito;
	}

	public void setEntregaDineroNotaCredito(boolean entregaDineroNotaCredito) {
		this.entregaDineroNotaCredito = entregaDineroNotaCredito;
	}
}
