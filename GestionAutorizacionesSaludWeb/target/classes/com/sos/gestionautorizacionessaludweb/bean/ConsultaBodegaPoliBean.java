package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.AtributosPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioIpsDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXPlanXCargoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosAdicionalesCumsDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosBasicosBodegaPoliticaDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoExcepcionesDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoNormalDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.HomologacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.MarcasPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PagoFijoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PoliticasAuthPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PrestacionPorPlanesDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.TiempoEntregaDTO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaBodegaPoliticaController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.ips.modelo.IPSVO;

public class ConsultaBodegaPoliBean implements Serializable {
	private static final long serialVersionUID = -1876905275590370299L;
	private static final Logger LOG = Logger.getLogger(ConsultarSolicitudBean.class);
	private static final String COLOR_ROJO = "red";
	private static final String FILTRO_CONVENIO = "convenio";
	private static final String FILTRO_DIRECCIONAMIENTO_NORMAL = "direccionamientoNormal";
	private static final String FILTRO_DIRECCIONAMIENTO_RIESGO = "direccionamientoRiesgo";
	private static final String FILTRO_HOMOLOGO = "homologo";
	private static final String FILTRO_PAGOS_FIJOS = "pagosfijos";

	private List<SelectItem> listaTiposPrestaciones = new ArrayList<SelectItem>();
	private List<SelectItem> listaPrestaciones = new ArrayList<SelectItem>();
	private List<PrestacionDTO> listaPrestacionDTO = new ArrayList<PrestacionDTO>();
	private List<CiudadVO> listaCiudadesVO = new ArrayList<CiudadVO>();
	private List<PrestacionPorPlanesDTO> prestacionPlanes;
	private List<MarcasPrestacionDTO> marcasList;
	private List<AtributosPrestacionDTO> atributosPrestacion;

	private List<PoliticasAuthPrestacionDTO> politicasAuthList;
	private List<TiempoEntregaDTO> tiempoEntreList;
	private Integer tipoPrestacionSeleccionado;

	private boolean mostrarFiltroConvenio = false;
	private boolean mostrarFiltroDireccionamientoNormal = false;
	private boolean mostrarFiltroDireccionamientoRiesgo = false;
	private boolean mostrarFiltroHomologo = false;
	private boolean mostrarFiltroPagosFijos = false;

	private DatosBasicosBodegaPoliticaDTO datosDTO;
	private Integer consecutivoPrestacion;
	private String descPrestacion;
	private CiudadVO ciudadVO = new CiudadVO();
	private List<CupsXRiesgoDTO> cupsXRiesgoList;
	private List<CupsXPlanXCargoDTO> cupsXPlanXCargoList;
	private List<PagoFijoDTO> listaPagosFijos;
	private DatosAdicionalesCumsDTO datosAdicionalesCUMS;
	private boolean showCums = false;
	private boolean showCUPS = false;
	private String opcionBusquedaSeleccionada;
	private boolean filtraCupsNorma = false;
	/**
	 * Convenios de la prestacion
	 */
	private PrestadorFiltroBean prestadorFiltro;
	private Integer consecutivoPlan;
	private Integer consecutivoCiudad;

	private List<SelectItem> listaPlanes;
	private List<SelectItem> listaCiudades;
	private List<CiudadVO> listCiudadesV;
	private List<ConvenioPrestacionDTO> conveniosPrestacionesList;
	private String codigoPrestacion;
	private boolean mostrarPopupCiudades;
	private String tipoConsultaPrestacionSeleccionado;
	/**
	 * Direccionamiento Normal
	 */
	private List<DireccionamientoNormalDTO> direccionamientoNormalList;
	/**
	 * Direccionamiento por riesgo
	 */
	private List<DireccionamientoXRiesgoDTO> direccionamientoXRiesgoList;
	/**
	 * Direccionamiento excepciones
	 */
	private List<DireccionamientoExcepcionesDTO> direccionamientoExcepcionesList;
	/**
	 * Homologos
	 */
	private List<HomologacionDTO> homologosList;

	private List<ConvenioIpsDTO> convenioIpsList;

	private boolean cups;

	@PostConstruct
	public void init() {
		try {
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			conveniosPrestacionesList = new ArrayList<ConvenioPrestacionDTO>();
			direccionamientoNormalList = new ArrayList<DireccionamientoNormalDTO>();
			direccionamientoXRiesgoList = new ArrayList<DireccionamientoXRiesgoDTO>();
			direccionamientoExcepcionesList = new ArrayList<DireccionamientoExcepcionesDTO>();
			homologosList = new ArrayList<HomologacionDTO>();
			listaCiudades = new ArrayList<SelectItem>();
			listaPlanes = new ArrayList<SelectItem>();
			listCiudadesV = new ArrayList<CiudadVO>();
			prestadorFiltro = (PrestadorFiltroBean) FacesUtils
					.getManagedBean(ConstantesWeb.GENERAR_PRESTADOR_FILTRO_BEAN);
			CargarCombosController comboPlanController = new CargarCombosController();

			prestacionPlanes = new ArrayList<PrestacionPorPlanesDTO>();
			atributosPrestacion = new ArrayList<AtributosPrestacionDTO>();
			listaPrestacionDTO = new ArrayList<PrestacionDTO>();
			listaTiposPrestaciones = consultaBodegaController.obtenerListaSelectTipoPrestacionesBodegaPolit(null, 2);
			listaTiposPrestaciones = listaTiposPrestaciones == null ? new ArrayList<SelectItem>()
					: listaTiposPrestaciones;
			listaPlanes = comboPlanController.obtenerListaPlanesConsecutivoDescripcion(null);
			tipoConsultaPrestacionSeleccionado = "noips";
			convenioIpsList = new ArrayList<ConvenioIpsDTO>();
			listaPagosFijos = new ArrayList<PagoFijoDTO>();
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (Exception e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	/**
	 * Selecciona el tipo de prestaciones
	 * 
	 * @author geniarjos
	 * @since 24/03/2017
	 */
	public void seleccionTipoPrestacion() {

		if (tipoPrestacionSeleccionado == ConstantesWeb.CONSECUTIVO_CUPS) {
			cups = true;
		} else
			cups = false;

		codigoPrestacion = descPrestacion = "";

		listaPrestaciones = new ArrayList<SelectItem>();
	}

	/**
	 * Busca las prestaciones
	 * 
	 * @author geniarjos
	 * @since 24/03/2017
	 */
	public void buscarPrestacion() {
		try {

			if (tipoPrestacionSeleccionado == null && consecutivoPrestacion == null) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR),
						COLOR_ROJO);
				return;
			}
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			this.setDatosDTO(consultaBodegaController.consultaInformacionDatosBasicosPrestacion(consecutivoPrestacion,
					tipoPrestacionSeleccionado));
			prestacionPlanes = consultaBodegaController.consultaInformacionPrestacionPorPlanes(consecutivoPrestacion,
					tipoPrestacionSeleccionado);
			atributosPrestacion = consultaBodegaController.consultaAtributosPrestacion(consecutivoPrestacion);
			marcasList = consultaBodegaController.consultaMarcasPrestacion(consecutivoPrestacion);
			politicasAuthList = consultaBodegaController.consultaAuthPrestacion(consecutivoPrestacion);
			tiempoEntreList = consultaBodegaController.consultaInformacionTiemposEntrega(consecutivoPrestacion);
			conveniosPrestacionesList = new ArrayList<ConvenioPrestacionDTO>();
			direccionamientoExcepcionesList = new ArrayList<DireccionamientoExcepcionesDTO>();
			direccionamientoNormalList = new ArrayList<DireccionamientoNormalDTO>();
			direccionamientoXRiesgoList = new ArrayList<DireccionamientoXRiesgoDTO>();
			homologosList = new ArrayList<HomologacionDTO>();
			cupsXPlanXCargoList = new ArrayList<CupsXPlanXCargoDTO>();
			cupsXRiesgoList = new ArrayList<CupsXRiesgoDTO>();
			prestadorFiltro.setIpsVO(new IPSVO());
			showCums = false;
			showCUPS = false;

			if (tipoPrestacionSeleccionado.equals(ConstantesWeb.CONSECUTIVO_CUMS)) {

				showCums = true;
			}
			if (tipoPrestacionSeleccionado.equals(ConstantesWeb.CONSECUTIVO_CUPS)) {

				showCUPS = true;
			}

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		}
	}

	/**
	 * Busca los datos adiconales de los cums segun la prestacion seleccionada
	 * 
	 * @param evt
	 */
	public void buscarCumsDatosAdicionales(ActionEvent evt) {
		try {
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			datosAdicionalesCUMS = consultaBodegaController.consultaDatosCums(consecutivoPrestacion);
		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		}
	}

	/**
	 * Busca los datos adiconales de los cups segun la prestacion seleccionada
	 * 
	 * @param evt
	 */
	public void buscarCupsDatosAdicionales(ActionEvent evt) {
		try {
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			cupsXPlanXCargoList = consultaBodegaController.consultaCupsXPlanXCargo(consecutivoPrestacion);
			cupsXRiesgoList = consultaBodegaController.consultaCupsXRiesgo(consecutivoPrestacion);
		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_NO_EXISTE_PRESTADOR));
		}
	}

	/**
	 * Consulta los convenios según los filtros.
	 */
	public void buscarConveniosPorPrestador() {
		try {

			if (consecutivoPlan != null && (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null)
					&& consecutivoPrestacion == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_SELECCION_FILTRO_ADICIONAL), COLOR_ROJO);
				return;

			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			conveniosPrestacionesList = consultaBodegaController.consultaConveniosPrestacion(tipoPrestacionSeleccionado,
					consecutivoPlan, consecutivoPrestacion,
					ciudadVO == null ? null : ciudadVO.getConsecutivoCodigoCiudad(),
					prestadorFiltro.getIpsVO() != null
							&& !prestadorFiltro.getIpsVO().getCodigoInterno().equalsIgnoreCase(ConstantesWeb.VACIO)
									? prestadorFiltro.getIpsVO().getCodigoInterno() : null);

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	public void consultarTodos() {
		if (ciudadVO != null && ciudadVO.getConsecutivoCodigoCiudad() != null) {
			buscarDireccionamientoNormal();
			buscarDireccionamientoXRiesgo();
			buscarHomologos();
			buscarConveniosPorPrestador();
		}

		if (mostrarFiltroHomologo && (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null))
			buscarHomologos();
		if (mostrarFiltroConvenio && (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null))
			buscarConveniosPorPrestador();

	}

	// Direccionamiento Normal
	/**
	 * Consulta el direccionamiento normal
	 */
	public void buscarDireccionamientoNormal() {
		try {
			if (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_CIUDAD_NO_SELECCIONADA),
						COLOR_ROJO);
				return;
			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			direccionamientoNormalList = consultaBodegaController.consultaDireccionamientoNormal(consecutivoPrestacion,
					ciudadVO.getConsecutivoCodigoCiudad());

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	/**
	 * Consulta el direccionamiento por riesgo
	 */
	public void buscarDireccionamientoXRiesgo() {
		try {
			if (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_CIUDAD_NO_SELECCIONADA),
						COLOR_ROJO);
				return;
			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			direccionamientoXRiesgoList = consultaBodegaController
					.consultaDireccionamientoXRiesgo(consecutivoPrestacion, ciudadVO.getConsecutivoCodigoCiudad());

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	/**
	 * Consulta el direccionamiento normal
	 */
	public void buscarDireccionamientoExcepciones(ActionEvent event) {
		try {
			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			direccionamientoExcepcionesList = consultaBodegaController
					.consultaDireccionamientoExcepcion(consecutivoPrestacion);

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	/**
	 * Consulta los homolgoos
	 */
	public void buscarHomologos() {
		try {
			boolean codInternoVacio = false;
			boolean codCiudadVacio = false;
			if (prestadorFiltro.getIpsVO() == null || prestadorFiltro.getIpsVO().getCodigoInterno() == null
					|| prestadorFiltro.getIpsVO().getCodigoInterno().isEmpty())
				codInternoVacio = true;
			if (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null)
				codCiudadVacio = true;

			if (codInternoVacio && codCiudadVacio) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_SELECCION_FILTRO_VACIO),
						COLOR_ROJO);
				return;
			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			homologosList = consultaBodegaController.consultaHomologo(consecutivoPrestacion,
					ciudadVO != null ? ciudadVO.getConsecutivoCodigoCiudad() : null,
					prestadorFiltro.getIpsVO() != null ? prestadorFiltro.getIpsVO().getCodigoInterno() : null);

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		listaCiudadesVO = null;
	}

	public void seleccionFiltroBusqueda() {
		mostrarFiltroConvenio = false;
		mostrarFiltroDireccionamientoNormal = false;
		mostrarFiltroDireccionamientoRiesgo = false;
		mostrarFiltroHomologo = false;
		mostrarFiltroPagosFijos = false;
		if (opcionBusquedaSeleccionada.equals(FILTRO_CONVENIO)) {
			mostrarFiltroConvenio = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_DIRECCIONAMIENTO_NORMAL)) {
			mostrarFiltroDireccionamientoNormal = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_DIRECCIONAMIENTO_RIESGO)) {
			mostrarFiltroDireccionamientoRiesgo = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_HOMOLOGO)) {
			mostrarFiltroHomologo = true;
		} else if (opcionBusquedaSeleccionada.equals(FILTRO_PAGOS_FIJOS)) {
			mostrarFiltroPagosFijos = true;
		}
		limpiarCampos();
	}

	public void limpiarCampos() {
		prestadorFiltro.setIpsVO(new IPSVO());
		prestadorFiltro.setListaIPSVO(null);
		ciudadVO = new CiudadVO();
	}

	public void buscarCiudad() {
		try {
			mostrarPopupCiudades = false;
			listaCiudadesVO = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudadVO,
					new RegistrarSolicitudController());
			ciudadVO = new CiudadVO();
			if (IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesVO)) {
				mostrarPopupCiudades = true;
			} else {
				mostrarPopupCiudades = false;
				ciudadVO = listaCiudadesVO.get(0);
			}

		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (IOException e) {
			manejarExcepcion(e, e.getMessage());
		}
	}

	public void consultarConveniosIps() {

		try {
			if (prestadorFiltro.getIpsVO() == null
					|| prestadorFiltro.getIpsVO().getCodigoInterno().equalsIgnoreCase(ConstantesWeb.VACIO)) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.DEBE_SELECCIONAR_PRESTADOR),
						COLOR_ROJO);
				return;
			}

			// validar el tipo de prestacion
			if (tipoPrestacionSeleccionado == null) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_SELECCIONAR_TIPO_PRESTACION),
						COLOR_ROJO);
				return;
			}

			if (descPrestacion == null || "".equals(descPrestacion)) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.DEBE_SPECIFICAR_DESCRIPCION_PRESTACION), COLOR_ROJO);
				return;
			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();
			convenioIpsList = consultaBodegaController.consultaConvenioIps(
					FacesUtils.descripcionToXMLFormat(descPrestacion), this.tipoPrestacionSeleccionado,
					this.filtraCupsNorma ? 1 : 0, prestadorFiltro.getIpsVO().getCodigoInterno(),
					ciudadVO.getConsecutivoCodigoCiudad());
		} catch (LogicException e) {
			manejarExcepcion(e, e.getMessage());
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	public void consultarPagosFijos() {
		try {
			if ((prestadorFiltro.getIpsVO() == null
					|| prestadorFiltro.getIpsVO().getCodigoInterno().equalsIgnoreCase(ConstantesWeb.VACIO))
					&& (ciudadVO == null || ciudadVO.getConsecutivoCodigoCiudad() == null)) {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesEJB.ERROR_CONSULTA_PAGOS_FIJOS_PARAMETROS), COLOR_ROJO);
				return;
			}

			ConsultaBodegaPoliticaController consultaBodegaController = new ConsultaBodegaPoliticaController();

			listaPagosFijos = consultaBodegaController.consultaPagosFijos(consecutivoPrestacion,
					ciudadVO.getConsecutivoCodigoCiudad(), consecutivoPlan,
					(prestadorFiltro.getIpsVO() == null
							|| prestadorFiltro.getIpsVO().getCodigoInterno().equalsIgnoreCase(ConstantesWeb.VACIO))
									? null : prestadorFiltro.getIpsVO().getCodigoInterno());

		} catch (LogicException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (ConnectionProviderException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		} catch (IOException e) {
			manejarExcepcion(e, Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
		}
	}

	private void manejarExcepcion(Exception e, String message) {
		LOG.error(e.getMessage(), e);
		MostrarMensaje.mostrarMensaje(message, COLOR_ROJO);
	}

	// Setter Getter

	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	public void setDatosDTO(DatosBasicosBodegaPoliticaDTO datosDTO) {
		this.datosDTO = datosDTO;
	}

	public DatosBasicosBodegaPoliticaDTO getDatosDTO() {
		return datosDTO;
	}

	public void setPrestacionPlanes(List<PrestacionPorPlanesDTO> prestacionPlanes) {
		this.prestacionPlanes = prestacionPlanes;
	}

	public List<PrestacionPorPlanesDTO> getPrestacionPlanes() {

		return prestacionPlanes;
	}

	public List<MarcasPrestacionDTO> getMarcasList() {
		return marcasList;
	}

	public void setMarcasList(List<MarcasPrestacionDTO> marcasList) {
		this.marcasList = marcasList;
	}

	public List<PoliticasAuthPrestacionDTO> getPoliticasAuthList() {
		return politicasAuthList;
	}

	public void setPoliticasAuthList(List<PoliticasAuthPrestacionDTO> politicasAuthList) {
		this.politicasAuthList = politicasAuthList;
	}

	public List<TiempoEntregaDTO> getTiempoEntreList() {
		return tiempoEntreList;
	}

	public void setTiempoEntreList(List<TiempoEntregaDTO> tiempoEntreList) {
		this.tiempoEntreList = tiempoEntreList;
	}

	public void setListaPrestacionDTO(List<PrestacionDTO> listaPrestacionDTO) {
		this.listaPrestacionDTO = listaPrestacionDTO;
	}

	public List<PrestacionDTO> getListaPrestacionDTO() {
		return listaPrestacionDTO;
	}

	public void setListaPrestaciones(List<SelectItem> listaPrestaciones) {
		this.listaPrestaciones = listaPrestaciones;
	}

	public List<SelectItem> getListaPrestaciones() {
		return listaPrestaciones;
	}

	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	public void setDescPrestacion(String descPrestacion) {
		this.descPrestacion = descPrestacion;
	}

	public String getDescPrestacion() {
		return descPrestacion;
	}

	public PrestadorFiltroBean getPrestadorFiltro() {
		return prestadorFiltro;
	}

	public void setPrestadorFiltro(PrestadorFiltroBean prestadorFiltro) {
		this.prestadorFiltro = prestadorFiltro;
	}

	public Integer getConsecutivoPlan() {
		return consecutivoPlan;
	}

	public void setConsecutivoPlan(Integer consecutivoPlan) {
		this.consecutivoPlan = consecutivoPlan;
	}

	public Integer getConsecutivoCiudad() {
		return consecutivoCiudad;
	}

	public void setConsecutivoCiudad(Integer consecutivoCiudad) {
		this.consecutivoCiudad = consecutivoCiudad;
	}

	public List<SelectItem> getListaPlanes() {
		return listaPlanes;
	}

	public void setListaPlanes(List<SelectItem> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}

	public List<SelectItem> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(List<SelectItem> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public List<CiudadVO> getListCiudadesV() {
		return listCiudadesV;
	}

	public void setListCiudadesV(List<CiudadVO> listCiudadesV) {
		this.listCiudadesV = listCiudadesV;
	}

	public List<ConvenioPrestacionDTO> getConveniosPrestacionesList() {
		return conveniosPrestacionesList;
	}

	public void setConveniosPrestacionesList(List<ConvenioPrestacionDTO> conveniosPrestacionesList) {
		this.conveniosPrestacionesList = conveniosPrestacionesList;
	}

	public void setDireccionamientoNormalList(List<DireccionamientoNormalDTO> direccionamientoNormalList) {
		this.direccionamientoNormalList = direccionamientoNormalList;
	}

	public List<DireccionamientoNormalDTO> getDireccionamientoNormalList() {
		return direccionamientoNormalList;
	}

	public void setDireccionamientoXRiesgoList(List<DireccionamientoXRiesgoDTO> direccionamientoXRiesgoList) {
		this.direccionamientoXRiesgoList = direccionamientoXRiesgoList;
	}

	public List<DireccionamientoXRiesgoDTO> getDireccionamientoXRiesgoList() {
		return direccionamientoXRiesgoList;
	}

	public void setDireccionamientoExcepcionesList(
			List<DireccionamientoExcepcionesDTO> direccionamientoExcepcionesList) {
		this.direccionamientoExcepcionesList = direccionamientoExcepcionesList;
	}

	public List<DireccionamientoExcepcionesDTO> getDireccionamientoExcepcionesList() {
		return direccionamientoExcepcionesList;
	}

	public void setHomologosList(List<HomologacionDTO> homologosList) {
		this.homologosList = homologosList;
	}

	public List<HomologacionDTO> getHomologosList() {
		return homologosList;
	}

	public void setCups(boolean cups) {
		this.cups = cups;
	}

	public boolean isCups() {
		return cups;
	}

	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	public void setCupsXRiesgoList(List<CupsXRiesgoDTO> cupsXRiesgoList) {
		this.cupsXRiesgoList = cupsXRiesgoList;
	}

	public List<CupsXRiesgoDTO> getCupsXRiesgoList() {
		return cupsXRiesgoList;
	}

	public void setCupsXPlanXCargoList(List<CupsXPlanXCargoDTO> cupsXPlanXCargoList) {
		this.cupsXPlanXCargoList = cupsXPlanXCargoList;
	}

	public List<CupsXPlanXCargoDTO> getCupsXPlanXCargoList() {
		return cupsXPlanXCargoList;
	}

	public void setDatosAdicionalesCUMS(DatosAdicionalesCumsDTO datosAdicionalesCUMS) {
		this.datosAdicionalesCUMS = datosAdicionalesCUMS;
	}

	public DatosAdicionalesCumsDTO getDatosAdicionalesCUMS() {
		return datosAdicionalesCUMS;
	}

	public void setShowCums(boolean showCums) {
		this.showCums = showCums;
	}

	public boolean isShowCums() {
		return showCums;
	}

	public void setShowCUPS(boolean showCUPS) {
		this.showCUPS = showCUPS;
	}

	public boolean isShowCUPS() {
		return showCUPS;
	}

	public void setListaCiudadesVO(List<CiudadVO> listaCiudadesVO) {
		this.listaCiudadesVO = listaCiudadesVO;
	}

	public List<CiudadVO> getListaCiudadesVO() {
		return listaCiudadesVO;
	}

	public void setCiudadVO(CiudadVO ciudadVO) {
		this.ciudadVO = ciudadVO;
	}

	public CiudadVO getCiudadVO() {
		return ciudadVO;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarFiltroConvenio(boolean mostrarFiltroConvenio) {
		this.mostrarFiltroConvenio = mostrarFiltroConvenio;
	}

	public boolean isMostrarFiltroConvenio() {
		return mostrarFiltroConvenio;
	}

	public void setMostrarFiltroDireccionamientoNormal(boolean mostrarFiltroDireccionamientoNormal) {
		this.mostrarFiltroDireccionamientoNormal = mostrarFiltroDireccionamientoNormal;
	}

	public boolean isMostrarFiltroDireccionamientoNormal() {
		return mostrarFiltroDireccionamientoNormal;
	}

	public void setMostrarFiltroDireccionamientoRiesgo(boolean mostrarFiltroDireccionamientoRiesgo) {
		this.mostrarFiltroDireccionamientoRiesgo = mostrarFiltroDireccionamientoRiesgo;
	}

	public boolean isMostrarFiltroDireccionamientoRiesgo() {
		return mostrarFiltroDireccionamientoRiesgo;
	}

	public void setOpcionBusquedaSeleccionada(String opcionBusquedaSeleccionada) {
		this.opcionBusquedaSeleccionada = opcionBusquedaSeleccionada;
	}

	public String getOpcionBusquedaSeleccionada() {
		return opcionBusquedaSeleccionada;
	}

	public void setMostrarFiltroHomologo(boolean mostrarFiltroHomologo) {
		this.mostrarFiltroHomologo = mostrarFiltroHomologo;
	}

	public boolean isMostrarFiltroHomologo() {
		return mostrarFiltroHomologo;
	}

	public String getTipoConsultaPrestacionSeleccionado() {
		return this.tipoConsultaPrestacionSeleccionado;
	}

	public void setTipoConsultaPrestacionSeleccionado(String tipoConsultaPrestacionSeleccionado) {
		this.tipoConsultaPrestacionSeleccionado = tipoConsultaPrestacionSeleccionado;
	}

	public boolean isFiltraCupsNorma() {
		return filtraCupsNorma;
	}

	public void setFiltraCupsNorma(boolean filtraCupsNorma) {
		this.filtraCupsNorma = filtraCupsNorma;
	}

	public int getTipoPrestacionCups() {
		return ConstantesWeb.CONSECUTIVO_CUPS;
	}

	public List<AtributosPrestacionDTO> getAtributosPrestacion() {
		return atributosPrestacion;
	}

	public void setAtributosPrestacion(List<AtributosPrestacionDTO> atributosPrestacion) {
		this.atributosPrestacion = atributosPrestacion;
	}

	public List<ConvenioIpsDTO> getConvenioIpsList() {
		return convenioIpsList;
	}

	public void setConvenioIpsList(List<ConvenioIpsDTO> convenioIpsList) {
		this.convenioIpsList = convenioIpsList;
	}

	public boolean isMostrarFiltroPagosFijos() {
		return mostrarFiltroPagosFijos;
	}

	public void setMostrarFiltroPagosFijos(boolean mostrarFiltroPagosFijos) {
		this.mostrarFiltroPagosFijos = mostrarFiltroPagosFijos;
	}

	public List<PagoFijoDTO> getListaPagosFijos() {
		return listaPagosFijos;
	}

	public void setListaPagosFijos(List<PagoFijoDTO> listaPagosFijos) {
		this.listaPagosFijos = listaPagosFijos;
	}

}
