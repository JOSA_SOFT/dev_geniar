package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TutelasAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.AdvertenciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasVO;
import com.sos.gestionautorizacionessaluddata.model.malla.MarcaInconsistenciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.AtencionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciasVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RecienNacidoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.FechaEntregaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.PrestacionFechaEntregaVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.EjecutarMalla;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.GestionarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.GuardarSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesDiagnosticosUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesPrestacionUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesSoportesUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MenuBean;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.ips.modelo.IPSVO;
import com.sos.medico.modelo.MedicoVO;
import com.sos.util.jsf.FacesUtil;

import co.com.sos.enterprise.malla.v1.MallaService;
import co.com.sos.enterprise.solicitud.v1.SolicitudService;
import co.com.sos.grabarsolicitud.v1.GrabarSolicitudResType;
import co.eps.sos.dataccess.exception.DataAccessException;

public class IngresoSolicitudBean implements Serializable {
	private static final long serialVersionUID = 3611491016577537400L;

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(IngresoSolicitudBean.class);

	private static final String USUARIO_BEAN = "usuarioBean";
	private static final String CONSULTA_AFILIADO_BEAN = "consultaAfiliadoBean";
	private static final String COLOR_AZUL = "blue";

	private transient EJBResolverInstance ejbs;
		
	private Date fechaActual;
	private String txtCodigoDiagnostico;
	private String txtDescripcionDiagnostico;
	private String txtContingencia;
	private String txtConfirmarRegistroMedico;
	private String txtCodigoCiudad;
	private String txtDescripcionCiudad;
	private String txtObservaciones;
	private boolean mostrarPopupSolicitudDevuelta = false;
	private boolean mostrarPopupConfirmacionBorrarArchivo = false;
	private boolean archivosObligatoriosFaltantes = false;
	private boolean mostrarPopupConfirmacionDocumentos = false;
	private boolean mostrarPopupCiudades = false;
	private boolean mostrarPopupDocumentosSoporte = false;
	private boolean mostrarPopupDiagnosticos = false;
	private boolean mostrarPopupConfirmarRegistrarMedico = false;
	private boolean mostrarPopupConfirmarRegistrarPrestador = false;
	private boolean mostrarPopupConfirmarRegistrarMedicamento = false;
	private boolean mostrarPopupConfirmarRegistrarCUP = false;
	private boolean mostrarPopupMedicos = false;
	private boolean mostrarPopupHospitalizacion = false;
	private boolean mostrarPopupCorteCuenta = false;
	private boolean mostrarPopupRegistrarMedicoNoAds = false;
	private boolean mostrarPopupRegistrarPrestador = false;
	private boolean mostrarPopupRegistrarMedicamento = false;
	private boolean mostrarPopupRegistrarCUP = false;
	private boolean mostrarPopupPrestadores = false;
	private boolean mostrarPopupMedicamentos = false;
	private boolean mostrarPopupCUPS = false;
	private boolean mostrarPanelMedicamentos = false;
	private boolean mostrarPanelCUPS = false;
	private boolean mostrarPopupEjecutarBPM = false;
	private boolean mostrarPopupGenerarOPS = false;
	private boolean esRecienNacido = false;
	private boolean esPartoMultiple = false;
	private boolean esModificacionPrestacion = false;
	private boolean deshabilitarBtnDocumentos = true;
	private boolean deshabilitarCamposMedico = false;
	private boolean deshabilitarBtnHospitalizacion = true;
	private boolean deshabilitarSelectTipoPrestacion = false;
	private boolean deshabilitarSelectLateralidadCUP = false;
	private boolean deshabilitarRecobro = true;
	private boolean deshabilitarBtnGuardar = false;
	private boolean superaTopeSoat;	
	private boolean indDescargaDevolucion;
	private boolean indDescargaOPS;
	private Boolean medicoAdscrito = null;
	private Boolean prestadorAdscrito = null;
	
	private Integer consecutivoTipoDiagnostico = ConstantesWeb.VALOR_INICIAL;
	private Integer consecutivoContingenciaRecobro = ConstantesWeb.VALOR_INICIAL;
	private Integer tipoPrestacionSeleccionado;
	private Integer recienNacido;
	private Integer tutela;
	private Integer partoMultiple;
	private Integer domi;
	
	private List<SelectItem> listaGeneros = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposIdentificacion = new ArrayList<SelectItem>();
	private List<SelectItem> listaOpciones = new ArrayList<SelectItem>();
	private List<SelectItem> listaClasesAtencion = new ArrayList<SelectItem>();
	private List<SelectItem> listaOrigenesAtencion = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposServicio = new ArrayList<SelectItem>();
	private List<SelectItem> listaPrioridadesAtencion = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposDiagnostico = new ArrayList<SelectItem>();
	private List<SelectItem> listaServiciosHospitalizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listaClasesHabitacion = new ArrayList<SelectItem>();
	private List<SelectItem> listaDosisMedicamentos = new ArrayList<SelectItem>();
	private List<SelectItem> listaFrecuenciaMedicamentos = new ArrayList<SelectItem>();
	private List<SelectItem> listaLateralidadesProcedimientos = new ArrayList<SelectItem>();
	private List<SelectItem> listaViasAccesoMedicamento = new ArrayList<SelectItem>();
	private List<SelectItem> listaContingenciaRecobros = new ArrayList<SelectItem>();
	private List<SelectItem> listaEspecialidadesMedico = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposIps = new ArrayList<SelectItem>();
	private List<SelectItem> listaTiposPrestaciones = new ArrayList<SelectItem>();
	private List<SelectItem> listaLateralidades = new ArrayList<SelectItem>();
	private List<SelectItem> listaConcentracionesMedicamento = new ArrayList<SelectItem>();
	private List<SelectItem> listaPresentacionesMedicamento = new ArrayList<SelectItem>();
	private List<ClaseAtencionVO> listaClaseAtencionVOs = new ArrayList<ClaseAtencionVO>();
	private List<TiposDiagnosticosVO> listaTipoDiagnosticoVO = new ArrayList<TiposDiagnosticosVO>();
	private List<DiagnosticosVO> listaDiagnosticosEncontrados = new ArrayList<DiagnosticosVO>();
	private List<DiagnosticoDTO> listaDiagnosticosSeleccionados = new ArrayList<DiagnosticoDTO>();
	private List<ContingenciaRecobroVO> listaContingenciaRecobroVOs = new ArrayList<ContingenciaRecobroVO>();
	private List<MedicamentosVO> listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
	private List<ProcedimientosVO> listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
	private List<MedicoVO> listaMedicosEncontrados = new ArrayList<MedicoVO>();
	private List<IPSVO> listaIPSVO = new ArrayList<IPSVO>();
	private List<CiudadVO> listaCiudadesVO = new ArrayList<CiudadVO>();
	private List<PrestacionDTO> listaPrestacionesSolicitadas = new ArrayList<PrestacionDTO>();
	private List<PrestacionDTO> listaPrestacionesAprobadas = new ArrayList<PrestacionDTO>();
	private List<DocumentoSoporteVO> listaDocumentoSoporteVOs = new ArrayList<DocumentoSoporteVO>();
	private List<SoporteVO> listaSoporteVOs = new ArrayList<SoporteVO>();
	private List<PrestacionDTO> listaPrestacionesInconsistenciasMallaService = new ArrayList<PrestacionDTO>();
	private List<InconsistenciasResponseServiceVO> listaInconsistenciasAdministrativas = new ArrayList<InconsistenciasResponseServiceVO>();
	private List<PrestacionDTO> listaPrestacionesAuditoria = new ArrayList<PrestacionDTO>();
	private List<PrestacionDTO> listaPrestacionesInconsistencias = new ArrayList<PrestacionDTO>();
	private List<InconsistenciasResponseServiceVO> listaInconsistenciasDevueltasMallaService = new ArrayList<InconsistenciasResponseServiceVO>();
	private List<ParametrosGeneralesVO> listaParametrosGenerales = ((UsuarioBean) FacesUtils.getManagedBean(USUARIO_BEAN)).getListaParametrosGenerales();
	private List<TutelasAfiliadoVO> listaTutelasAfiliadoVO = ((ConsultaAfiliadoBean) FacesUtils.getManagedBean(CONSULTA_AFILIADO_BEAN)).getListaTutelasAfiliado();
	private DiagnosticosVO diagnosticoVoSeleccionado;
	private DiagnosticoDTO diagnosticoDTOSeleccionado;
	private MedicoVO medicoVO = new MedicoVO();
	private IPSVO ipsVO = new IPSVO();
	private CiudadVO ciudadVO = new CiudadVO();
	private MedicamentosVO medicamentoVO = new MedicamentosVO();
	private ProcedimientosVO procedimientosVO = new ProcedimientosVO();
	private RecienNacidoVO recienNacidoVO = new RecienNacidoVO();
	private AtencionVO atencionVO = new AtencionVO();
	private HospitalizacionVO hospitalizacionVO = new HospitalizacionVO();
	private PrestacionDTO prestacionSeleccionada;
	private DocumentoSoporteVO documentoSoporteSeleccionado;
	private SoporteVO soporteSeleccionado;
	private FechaEntregaSolicitudVO fechaEntregaSolicitudVO;
	private byte[] imgData;
	
	private Integer maxDocumentos = 0;
	private SolicitudVO solicitud;
	private AfiliadoVO afiliadoVO;
	private String numeroSolicitudSOS;
	private InconsistenciasVO inconsistenciasVO = new InconsistenciasVO();
	private NumberFormat decimalFormat = NumberFormat.getNumberInstance(new Locale("es"));
	int corteCuenta;
	private int maximoCorteHospitalizacion;
	private int minimoCorteHospitalizacion;
	private int consecutivoCUMS;
	private int consecutivoCUPS;
	private boolean generarConObservaciones = false;	
	private List<AutorizacionServicioVO> listaAutorizacionServicioVO;

	private boolean mostrarPopupDevolucionOPS;

	private boolean errorServDireccionamiento = false;

	/**
	 * variables necesarias para la transcripción
	 */
	private int marcaTranscribir = 1;
	private String idTask;
	private String idSolicitud;
	private boolean deshabilitarBtnTerminar=true;
	private boolean deshabilitarBtnLeberar=false;
	private boolean deshabilitarBtnDevolver=false;

	/**IPS**/

	private boolean verIps=false;
	
	
	private boolean mostrarPopupAccesoDirecto;
	private boolean mostrarPopupValidarAccesoDirecto;
	private String usrDescarga;
	private String usrDescargaPass;
	private BigDecimal valorPrestacionesMarcadas;

	private boolean mostrarModalReciboCaja;
	
	
	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {
			fechaActual = Calendar.getInstance().getTime();
			consecutivoCUMS = ConstantesWeb.CONSECUTIVO_CUMS;
			consecutivoCUPS = ConstantesWeb.CONSECUTIVO_CUPS;
			valorPrestacionesMarcadas = new BigDecimal(0);
		
			hospitalizacionVO = GuardarSolicitudesUtil.crearHospitalizacionVO();
			atencionVO = GuardarSolicitudesUtil.crearAtencionVO();			
			medicamentoVO = IngresoSolicitudesPrestacionUtil.crearMedicamentosVO();			
			
			recienNacidoVO.setGeneroVO(new GeneroVO());	
			
			cargarListaCombos();		
			
			listaParametrosGenerales = IngresoSolicitudesUtil.validarConsultaParametrosGenerales(listaParametrosGenerales);			
			minimoCorteHospitalizacion = IngresoSolicitudesUtil.obtenerValoresCorteCuentaMinimo(listaParametrosGenerales);
			maximoCorteHospitalizacion = IngresoSolicitudesUtil.obtenerValoresCorteCuentaMaximo(listaParametrosGenerales);			
			tutela = IngresoSolicitudesUtil.aplicaTutela(listaTutelasAfiliadoVO);		
			if(marcaTranscribir!=0 ){
				deshabilitarBtnGuardar = false;
			}else{
				deshabilitarBtnGuardar = true;
			}
			
			/**IPS**/
			validaIPS();
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	private EJBResolverInstance getEjbs(){
		if (ejbs == null){
			ejbs = (EJBResolverInstance)FacesUtils.getManagedBean(ConstantesWeb.EJB_RESOLVER_BEAN);
		} 
		return ejbs;
	}
	
	/**
	 * Metodo que permite validar si es o no IPS
	 */
	
	public boolean validaIPS(){
		try{
			if (((MenuBean) FacesUtil.getBean(ConstantesWeb.MENU_BEAN)).isUsuarioIps()){
				verIps = false;
				String s = ((MenuBean) FacesUtil.getBean(ConstantesWeb.MENU_BEAN)).getPrestadorVO().getCodInternoPrestador();
				ipsVO= getEjbs().getIpsEJB().consultarIpsPorCodigoInterno(s);
				prestadorAdscrito =  IngresoSolicitudesMedicoPrestadorUtil.asignarSiEsAdscritoPrestador(ipsVO);
			}else{
				verIps = true;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
		return verIps;
	}
	
	/**
	 * Metodo que permite cargas lista de combos
	 * @param 
	 * @throws LogicException
	 */
	private void cargarListaCombos(){
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			CargarCombosController cargarCombosController = new CargarCombosController();
			
			listaTipoDiagnosticoVO = CargarCombosUtil.obtenerDiagnostico(cargarCombosController);				
			listaClaseAtencionVOs = CargarCombosUtil.obtenerClaseAtencion(cargarCombosController);			
			listaGeneros = CargarCombosUtil.obtenerListaSelectGeneros(null, cargarCombosController);
			listaTiposIdentificacion = CargarCombosUtil.obtenerListaSelectTiposIdentificacion(null, cargarCombosController);
			listaOpciones = CargarCombosUtil.obtenerListaSelectOpcion(null, cargarCombosController);
			listaClasesAtencion = CargarCombosUtil.obtenerListaSelectClaseAtencion(listaClaseAtencionVOs, cargarCombosController);
			listaOrigenesAtencion = CargarCombosUtil.obtenerListaSelectOrigenAtencion(null, cargarCombosController);
			listaTiposServicio = CargarCombosUtil.obtenerListaSelectTipoServicio(null, cargarCombosController);
			listaPrioridadesAtencion = CargarCombosUtil.obtenerListaSelectPrioridadAtencion(null, cargarCombosController);
			listaTiposDiagnostico = CargarCombosUtil.obtenerListaSelectTipoDiagnostico(listaTipoDiagnosticoVO, cargarCombosController);
			listaServiciosHospitalizacion = CargarCombosUtil.obtenerListaSelectServicioHospitalizacion(null, cargarCombosController);
			listaClasesHabitacion = CargarCombosUtil.obtenerListaSelectClaseHabitacion(null, cargarCombosController);
			listaDosisMedicamentos = CargarCombosUtil.obtenerListaSelectDosisMedicamentos(null, cargarCombosController);
			listaFrecuenciaMedicamentos = CargarCombosUtil.obtenerListaSelectFrecuenciaMedicamentos(null, cargarCombosController);
			listaTiposPrestaciones = CargarCombosUtil.obtenerListaSelectTipoPrestaciones(null, 0, registrarSolicitudesController);
			listaLateralidades = CargarCombosUtil.obtenerListaSelectLateralidadesProcedimientos(null, cargarCombosController);
			listaViasAccesoMedicamento = CargarCombosUtil.obtenerListaSelectViasAccesoMedicamento(null, cargarCombosController);
			 		
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
		
	}

	public void seleccionClaseAtencion() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			
			deshabilitarBtnHospitalizacion = true;
			deshabilitarSelectTipoPrestacion = false;
			for (ClaseAtencionVO claseAtencionVO : listaClaseAtencionVOs) {
				if (claseAtencionVO.getConsecutivoClaseAtencion().compareTo(atencionVO.getClaseAtencionVO().getConsecutivoClaseAtencion()) == 0) {
					atencionVO.setClaseAtencionVO(claseAtencionVO);
					break;
				}
			}
			List<UbicacionPacienteVO> listaUbicacionPacienteVOs = registrarSolicitudesController.consultaUbicacionPacientexClaseAtencion(atencionVO.getClaseAtencionVO().getConsecutivoClaseAtencion());
			if (listaUbicacionPacienteVOs != null && !listaUbicacionPacienteVOs.isEmpty()) {
				UbicacionPacienteVO ubicacionPacienteVO = listaUbicacionPacienteVOs.get(0);
				if (ubicacionPacienteVO != null) {
					atencionVO.setUbicacionPacienteVO(ubicacionPacienteVO);
				}
				if (ubicacionPacienteVO != null && ubicacionPacienteVO.getConsecutivoUbicacionPaciente().compareTo(ConstantesWeb.UBICACION_PACIENTE_HOSPITALIZACION) == 0) {
					deshabilitarBtnHospitalizacion = false;
					deshabilitarSelectTipoPrestacion = true;
					tipoPrestacionSeleccionado = consecutivoCUPS;
				} else {
					
					tipoPrestacionSeleccionado = null;
					hospitalizacionVO = IngresoSolicitudesDiagnosticosUtil.inicializarHospitalizacion();					
				}
			} else {
				atencionVO.setUbicacionPacienteVO(new UbicacionPacienteVO());
				tipoPrestacionSeleccionado = null;
				hospitalizacionVO = IngresoSolicitudesDiagnosticosUtil.inicializarHospitalizacion();				
			}
			seleccionTipoPrestacion();
		} catch (LogicException e) {
			atencionVO.setUbicacionPacienteVO(new UbicacionPacienteVO());
			tipoPrestacionSeleccionado = null;
			hospitalizacionVO = IngresoSolicitudesDiagnosticosUtil.inicializarHospitalizacion();
			
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}	

	public void buscarDiagnostico() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			
			diagnosticoVoSeleccionado = null;
			listaDiagnosticosEncontrados = null;			
			listaDiagnosticosEncontrados = IngresoSolicitudesDiagnosticosUtil.llenarListaDiagnosticoPorCodigo(txtCodigoDiagnostico.trim(), txtDescripcionDiagnostico.trim(), registrarSolicitudesController);
			txtCodigoDiagnostico      = ConstantesWeb.CADENA_VACIA;
			txtDescripcionDiagnostico = ConstantesWeb.CADENA_VACIA;
			
			if (IngresoSolicitudesDiagnosticosUtil.validarListaDiagnosticosEncontrados(listaDiagnosticosEncontrados)) {
				mostrarPopupDiagnosticos = true;
			} else {
				mostrarPopupDiagnosticos = false;
				diagnosticoVoSeleccionado = listaDiagnosticosEncontrados.get(0);
				txtCodigoDiagnostico      = diagnosticoVoSeleccionado.getCodigoDiagnostico();
				txtDescripcionDiagnostico = diagnosticoVoSeleccionado.getDescripcionDiagnostico();
			}
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}	
	
	public void seleccionarDiagnostico() {
		txtCodigoDiagnostico = diagnosticoVoSeleccionado.getCodigoDiagnostico();
		txtDescripcionDiagnostico = diagnosticoVoSeleccionado.getDescripcionDiagnostico();
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	public void cerrarPopupDiagnostico() {
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	public void cerrarMostrarPopupGenerarOPS() {
		mostrarPopupGenerarOPS = false;
	}
	
	/**
	 * metodo que permite adicionar los diagnosticos
	 * @throws LogicException
	 */
	public void adicionarDiagnostico() {
		try {			
			IngresoSolicitudesDiagnosticosUtil.validarTipoDiagnostico(consecutivoTipoDiagnostico);
			IngresoSolicitudesDiagnosticosUtil.validarConsecutivoDiagnostico(diagnosticoVoSeleccionado);
			IngresoSolicitudesDiagnosticosUtil.validarlistaDeDiagnosticosSeleccionados(listaDiagnosticosSeleccionados, consecutivoTipoDiagnostico, diagnosticoVoSeleccionado);
			
			DiagnosticoDTO diagnosticoDTO = new DiagnosticoDTO();
			diagnosticoDTO.setDiagnosticosVO(diagnosticoVoSeleccionado);
			
			TiposDiagnosticosVO tiposDiagnosticosAdi = new TiposDiagnosticosVO();			
			tiposDiagnosticosAdi = IngresoSolicitudesDiagnosticosUtil.adicionarTipoDiagnostico(consecutivoTipoDiagnostico, listaTipoDiagnosticoVO, tiposDiagnosticosAdi);			
			diagnosticoDTO.setTiposDiagnosticosVO(tiposDiagnosticosAdi);
			
			diagnosticoDTO = validarDiagnosticoPrincipal(diagnosticoDTO);
			ContingenciaRecobroVO recobroVO = new ContingenciaRecobroVO();
			diagnosticoDTO.setRecobroVO(recobroVO);
			listaDiagnosticosSeleccionados.add(diagnosticoDTO);
			diagnosticoVoSeleccionado = null;
			
			txtCodigoDiagnostico = ConstantesWeb.CADENA_VACIA;
			txtDescripcionDiagnostico = ConstantesWeb.CADENA_VACIA;
			consecutivoTipoDiagnostico = ConstantesWeb.VALOR_INICIAL;
						
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	private DiagnosticoDTO validarDiagnosticoPrincipal(DiagnosticoDTO diagnosticoDTO)throws DataAccessException, LogicException {
		
		
		RegistrarSolicitudController registrarSolicitudesController;
		try {
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (IOException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
		if (consecutivoTipoDiagnostico == ConstantesWeb.DIAGNOSTICO_PRINCIPAL) {
			ContingenciasVO contingenciasVO = registrarSolicitudesController.consultaContingencias(diagnosticoVoSeleccionado.getConsecutivoCodigoContingencia()).get(0);
			diagnosticoDTO.setContingenciasVO(contingenciasVO);
			txtContingencia = contingenciasVO.getDescripcionContingencia();
			listaContingenciaRecobroVOs = registrarSolicitudesController.consultaRelContingenciasRecobros(diagnosticoVoSeleccionado.getConsecutivoCodigoContingencia());
			listaContingenciaRecobros = registrarSolicitudesController.obtenerListaSelectRelContingenciasRecobros(listaContingenciaRecobroVOs);
			deshabilitarRecobro = false;
		}
		return diagnosticoDTO;
	}

	
	public void elimiarDiagnosticoSeleccionado() {
		if (diagnosticoDTOSeleccionado.getTiposDiagnosticosVO().getConsecutivoTipoDiagnostico().intValue() == ConstantesWeb.DIAGNOSTICO_PRINCIPAL) {
			txtContingencia = ConstantesWeb.CADENA_VACIA;
			listaContingenciaRecobroVOs = new ArrayList<ContingenciaRecobroVO>();
			listaContingenciaRecobros = new ArrayList<SelectItem>();
			deshabilitarRecobro = true;
		}
		listaDiagnosticosSeleccionados.remove(diagnosticoDTOSeleccionado);
	}

	public void buscarMedico() {
		try {
			mostrarPopupMedicos = false;
			medicoAdscrito = null;
			listaMedicosEncontrados = getEjbs().getMedicoEJB().consultarMedicoPorParametros(medicoVO);
			validarMostrarPopUpMedicos();
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * metodo que permite mostrar popup de medicos
	 * @throws LogicException
	 */
	private void validarMostrarPopUpMedicos() throws LogicException {
		if (listaMedicosEncontrados == null || listaMedicosEncontrados.isEmpty()) {
			mostrarPopupConfirmarRegistrarMedico = true;
			return;
		}
		
		if(IngresoSolicitudesUtil.validarTamanoLista(listaMedicosEncontrados)){
			mostrarPopupMedicos = true;
		}else{
			mostrarPopupMedicos = false;
			medicoVO = listaMedicosEncontrados.get(0);
			Utilidades.quitarEspaciosMedico(medicoVO);
			validarMedicoAdscrito();
		}	
	}

	/**
	 * metodo que permite validar medico adscrito
	 * @throws LogicException
	 */
	private void validarMedicoAdscrito() throws LogicException {
		if (medicoVO.getConsecGenerico() == 0) {
			medicoAdscrito = true;
			/**IPS**/
			if(verIps){
				RegistrarSolicitudController registrarSolicitudesController;
				try {
					registrarSolicitudesController = new RegistrarSolicitudController();
					listaIPSVO = registrarSolicitudesController.consultaIPSMedico((int) medicoVO.getNumeroUnicoIdMedico());
				} catch (ConnectionProviderException e) {
					throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
				} catch (IOException e) {
					throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
				}
			}
			
			
			if(IngresoSolicitudesUtil.validarDatosList(listaIPSVO)) {
				mostrarPopupPrestadores = true;
				/**IPS**/
				if(!verIps){
					mostrarPopupPrestadores = false;
				}
			}
		} else {
			medicoAdscrito = false;
		}
	}

	public void aceptarConfirmacionRegistarMedicoNoAds() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			
			mostrarPopupRegistrarMedicoNoAds = true;
			txtConfirmarRegistroMedico = ConstantesWeb.CADENA_VACIA;
			mostrarPopupConfirmarRegistrarMedico = false;
			listaEspecialidadesMedico = registrarSolicitudesController.obtenerListaSelectEspecialidadesMedico(null);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarConfirmacionRegistrarMedicoNoAds() {
		mostrarPopupConfirmarRegistrarMedico = false;
	}

	public void seleccionarMedico() {
		try {
			mostrarPopupMedicos = false;
			Utilidades.quitarEspaciosMedico(medicoVO);
			validarMedicoAdscrito();
			listaMedicosEncontrados = null;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarPopupMedicos() {
		mostrarPopupMedicos = false;
		listaMedicosEncontrados = null;
	}

	public void nuevaBusquedaMedico() {
		medicoAdscrito = null;
		medicoVO = new MedicoVO();
		listaMedicosEncontrados = null;
		deshabilitarCamposMedico = false;
	}

	public void registrarMedicoNoAdscrito() {
		String descripcionEspecialidadMedico = null;
		
		try {
			IngresoSolicitudesMedicoPrestadorUtil.validaRegistroMedico(txtConfirmarRegistroMedico, medicoVO);
			List<MedicoVO> listaMedicos = new ArrayList<MedicoVO>();
						
			if (medicoVO != null && medicoVO.getRegistroMedico() != null && !medicoVO.getRegistroMedico().trim().isEmpty()) {
				listaMedicos.addAll(Arrays.asList(getEjbs().getMedicoEJB().consultarMedicoPorRegistro(medicoVO.getRegistroMedico().trim()).getDatosMedico()));
			}
			
			if (medicoVO != null && medicoVO.getConsecCodTipoId() > 0 && medicoVO.getNumeroIdentificacion() != null && !medicoVO.getNumeroIdentificacion().trim().isEmpty()) {
				listaMedicos.addAll(Arrays.asList(getEjbs().getMedicoEJB().consultarMedicoPorId(medicoVO.getConsecCodTipoId(), medicoVO.getNumeroIdentificacion().trim()).getDatosMedico()));
			}
						
			Utilidades.validarListaMedicoNoAdscrito(listaMedicos, ConstantesWeb.MENSAJE_MEDICO + medicoVO.getNumeroIdentificacion() + ConstantesWeb.MENSAJE_REGISTRO_MEDICO + medicoVO.getRegistroMedico() + ConstantesWeb.MENSAJE_EXISTE);
			descripcionEspecialidadMedico = IngresoSolicitudesMedicoPrestadorUtil.buscarDescripcionEspecialidadMedico(listaEspecialidadesMedico, medicoVO);
			
			if(descripcionEspecialidadMedico != null){
				medicoVO.setDescripcionEspe(descripcionEspecialidadMedico);
			}			
			
			medicoAdscrito = false;
			mostrarPopupRegistrarMedicoNoAds = false;
			deshabilitarCamposMedico = true;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}


	public void cerrarPopupRegistrarMedico() {
		mostrarPopupRegistrarMedicoNoAds = false;
	}

	public void buscarIPS() {
		try {
			mostrarPopupPrestadores = false;
			prestadorAdscrito = null;
			
			listaIPSVO = getEjbs().getIpsEJB().consultarIpsPorParametros(ipsVO);
			if(listaIPSVO == null || listaIPSVO.isEmpty()){
				listaIPSVO = getEjbs().getIpsEJB().consultarConsultoriosPorParametros(ipsVO);
			}
						
			if(IngresoSolicitudesMedicoPrestadorUtil.validarPopUpPrestadoresMayor(listaIPSVO)){
				mostrarPopupPrestadores = true;
			}
			
			if(IngresoSolicitudesMedicoPrestadorUtil.validarPopUpPrestadoresIgual(listaIPSVO)){
				mostrarPopupPrestadores = false;
				ipsVO = listaIPSVO.get(0);
				prestadorAdscrito =  IngresoSolicitudesMedicoPrestadorUtil.asignarSiEsAdscritoPrestador(ipsVO);
			}
			
			if (listaIPSVO == null || listaIPSVO.isEmpty()) {
				mostrarPopupConfirmarRegistrarPrestador = true;
			}
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	public void aceptarConfirmacionRegistarPrestador() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupRegistrarPrestador = true;
			mostrarPopupConfirmarRegistrarPrestador = false;
			ciudadVO = new CiudadVO();
			listaTiposIps = registrarSolicitudesController.obtenerListaSelectTiposIps(null);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarConfirmacionRegistrarPrestador() {
		mostrarPopupConfirmarRegistrarPrestador = false;
	}

	public void seleccionarIPS() {
		prestadorAdscrito = null;
		prestadorAdscrito =  IngresoSolicitudesMedicoPrestadorUtil.asignarSiEsAdscritoPrestador(ipsVO);
		mostrarPopupPrestadores = false;
		listaIPSVO = null;
	}

	public void cerrarPopupIPS() {
		mostrarPopupPrestadores = false;
		listaIPSVO = null;
	}

	public void nuevaBusquedaIPS() {
		prestadorAdscrito = null;
		ipsVO = new IPSVO();
		listaIPSVO = null;
		ciudadVO = new CiudadVO();
	}

	public void registrarPrestadorNoAdscrito() {
		try {
			IngresoSolicitudesMedicoPrestadorUtil.validacionPrestadorNoAdscrito(ipsVO, ciudadVO);			
			List<IPSVO> listaIPS = null;
			if (ipsVO != null && ipsVO.getConsecCodigoTipoId() > 0 && ipsVO.getNumeroIdentificacion() != null && !ipsVO.getNumeroIdentificacion().trim().isEmpty()) {
				listaIPS = Arrays.asList(getEjbs().getIpsEJB().consultarIpsPorIdentificacionVigencia(ipsVO.getConsecCodigoTipoId(), ipsVO.getNumeroIdentificacion()).getDatosIPS());
			}
			if (listaIPS != null && !listaIPS.isEmpty()) {
				throw new LogicException(
						ConstantesWeb.MENSAJE_PRESTADOR + ipsVO.getNumeroIdentificacion() + ConstantesWeb.MENSAJE_RAZON_SOCIAL + ipsVO.getRazonSocial() + ConstantesWeb.MENSAJE_EXISTE,
						ErrorType.PARAMETRO_ERRADO);
			}
			ipsVO.setCodigoCiudad(ciudadVO.getCodigoCiudad());
			ipsVO.setDescripcionCiudad(ciudadVO.getDescripcionCiudad());
			ipsVO.setCodigoInterno(ConstantesWeb.CODIGO_INTERNO);
			prestadorAdscrito = false;
			mostrarPopupRegistrarPrestador = false;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ConstantesWeb.CADENA_VACIA));
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}	

	public void cerrarPopupRegistrarPrestador() {
		ipsVO = new IPSVO();
		mostrarPopupRegistrarPrestador = false;
	}

	public void buscarCiudad() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupCiudades = false;
			listaCiudadesVO = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudadVO, registrarSolicitudesController);			
			ciudadVO = new CiudadVO();
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesVO)){
				mostrarPopupCiudades = true;
			}else{
				mostrarPopupCiudades = false;
				ciudadVO = listaCiudadesVO.get(0);
			}	
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		listaCiudadesVO = null;
	}

	public void seleccionTipoPrestacion() {
		mostrarPanelCUPS = IngresoSolicitudesPrestacionUtil.cambioDeTipoPrestacionCUPS(consecutivoCUPS, tipoPrestacionSeleccionado);
		mostrarPanelMedicamentos = IngresoSolicitudesPrestacionUtil.cambioDeTipoPrestacionCUMS(consecutivoCUMS, tipoPrestacionSeleccionado);
		
		medicamentoVO = new MedicamentosVO();
		medicamentoVO = IngresoSolicitudesPrestacionUtil.crearMedicamentosVO();		
		procedimientosVO = new ProcedimientosVO();
	}

	public void buscarPrestacionMedicamentos() {
		try {
			mostrarPopupMedicamentos = false;
			/**IPS**/

			IngresoSolicitudIpsBean ingresoSolicitudIpsBean = new IngresoSolicitudIpsBean();
			
			afiliadoVO = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getAfiliadoVO();
			listaMedicamentosEncontrados=ingresoSolicitudIpsBean.consultarPrestacionesCUMSIps(medicamentoVO, ipsVO, afiliadoVO.getPlan().getConsectivoPlan());
		
			if (IngresoSolicitudesUtil.validarTamanoLista(listaMedicamentosEncontrados)) {
				mostrarPopupMedicamentos = true;
			
			} else if(IngresoSolicitudesUtil.validarDatosList(listaMedicamentosEncontrados)){
				mostrarPopupMedicamentos = false;
				medicamentoVO = listaMedicamentosEncontrados.get(0);			
			}
			
			if (IngresoSolicitudesPrestacionUtil.validarListaPrestacionEncontradosVacia(listaMedicamentosEncontrados)) {
				medicamentoVO = IngresoSolicitudesPrestacionUtil.crearMedicamentosVO();	
			    mostrarPopupConfirmarRegistrarMedicamento = true;
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void confirmarRegistroMedicamento() {
		try {
			CargarCombosController cargarCombosController = new CargarCombosController();
			mostrarPopupConfirmarRegistrarMedicamento = false;
			mostrarPopupRegistrarMedicamento = true;
			medicamentoVO.setCodigoCodificacionMedicamento(ConstantesWeb.CADENA_VACIA);
			listaConcentracionesMedicamento = CargarCombosUtil.obtenerListaSelectConcentracionMedicamento(null, cargarCombosController); 
			listaPresentacionesMedicamento = CargarCombosUtil.obtenerListaSelectPresentacionMedicamento(null, cargarCombosController);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public void cerrarPopupConfirmarRegistroMedicamento() {
		mostrarPopupConfirmarRegistrarMedicamento = false;
	}

	public void cerrarPopupMedicamentos() {
		mostrarPopupMedicamentos = false;
		listaMedicamentosEncontrados = null;
	}

	
	/**
	 * metodo que permite registra el medicamentos
     */	
	public void registrarMedicamento() {
		mostrarPopupRegistrarMedicamento = false;
		String descripcionConcetracion = null;
		String descripcionPresentacion = null;
		
		if (medicamentoVO.getConcentracionVO().getConsecutivoConcentracion() != ConstantesWeb.VALOR_INICIAL) {
			descripcionConcetracion = IngresoSolicitudesPrestacionUtil.buscarDescripcionConcentracionMedicamento(listaConcentracionesMedicamento, medicamentoVO);
			if (descripcionConcetracion != null){
				medicamentoVO.getConcentracionVO().setDescripcionConcentracion(descripcionConcetracion);
			}		
		}
		
		if (medicamentoVO.getPresentacionVO().getConsecutivoCodigoPresentacion().compareTo(BigInteger.valueOf(ConstantesWeb.VALOR_INICIAL)) != 0) {
			descripcionPresentacion = IngresoSolicitudesPrestacionUtil.buscarDescripcionPresentacionMedicamento(listaPresentacionesMedicamento, medicamentoVO);
			if (descripcionPresentacion != null){
				medicamentoVO.getPresentacionVO().setDescripcionPresentacion(descripcionPresentacion);
			}			
		}
	}

	public void cerrarRegistrarMedicamento() {
		mostrarPopupRegistrarMedicamento = false;
	}

	public void buscarPrestacionCUPS() {
		try {
			deshabilitarSelectLateralidadCUP = false;
			mostrarPopupCUPS = false;
			
			/**IPS**/
			IngresoSolicitudIpsBean ingresoSolicitudIpsBean = new IngresoSolicitudIpsBean();
			
			afiliadoVO = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getAfiliadoVO();
			listaCUPSEncontrados=ingresoSolicitudIpsBean.consultarPrestacionesCUPSIps(procedimientosVO, ipsVO, afiliadoVO.getPlan().getConsectivoPlan());
			
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCUPSEncontrados)){
				mostrarPopupCUPS = true;
			}else if(IngresoSolicitudesUtil.validarDatosList(listaCUPSEncontrados)){
				mostrarPopupCUPS = false;
				procedimientosVO = listaCUPSEncontrados.get(0);
				if (ConstantesWeb.APLICA_LATERALIDAD.equalsIgnoreCase(procedimientosVO.getAplicaLaterialidad())) {
					deshabilitarSelectLateralidadCUP = true;
					procedimientosVO.setConsecutivoLaterialidad(ConstantesWeb.NO_APLICA);
				}
			}		
			
		    if (IngresoSolicitudesPrestacionUtil.validarListaPrestacionEncontradosVacia(listaCUPSEncontrados)) {
				procedimientosVO = new ProcedimientosVO();
				mostrarPopupConfirmarRegistrarCUP = true;
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cerrarPopupCUPS() {

		if (procedimientosVO != null && procedimientosVO.getAplicaLaterialidad() != null && ConstantesWeb.APLICA_LATERALIDAD.equalsIgnoreCase(procedimientosVO.getAplicaLaterialidad())) {
			deshabilitarSelectLateralidadCUP = true;
			procedimientosVO.setConsecutivoLaterialidad(ConstantesWeb.NO_APLICA);
		}

		mostrarPopupCUPS = false;
		listaCUPSEncontrados = null;
	}

	public void confirmarRegistroCUP() {
		mostrarPopupConfirmarRegistrarCUP = false;
		mostrarPopupRegistrarCUP = true;
		procedimientosVO.setCodigoCodificacionProcedimiento(ConstantesWeb.CADENA_VACIA);
	}

	public void cerrarPopupConfirmarRegistroCUP() {
		mostrarPopupConfirmarRegistrarCUP = false;
	}

	public void registrarCUPS() {
		mostrarPopupRegistrarCUP = false;
	}

	public void cerrarRegistrarCUPS() {
		mostrarPopupRegistrarCUP = false;
	}	

	public void adicionarCUP() {
		String descripcionTipoCodificacion = null;
		try {
			
			IngresoSolicitudesPrestacionUtil.validarConsecutivoCodificacionProcedimiento(procedimientosVO);
			IngresoSolicitudesPrestacionUtil.validarCantidadProcedimientos(procedimientosVO);
			IngresoSolicitudesPrestacionUtil.validarConsecutivoLateralidad(procedimientosVO);
			IngresoSolicitudesPrestacionUtil.validarDescripcionCodificacion(procedimientosVO);
			IngresoSolicitudesPrestacionUtil.validarConsecutivoAcceso(procedimientosVO);
				
			for (PrestacionDTO prestacionDTO : listaPrestacionesSolicitadas) {
				IngresoSolicitudesPrestacionUtil.validarCodigoCodificacionProcedimiento(prestacionDTO, procedimientosVO);
			}
			PrestacionDTO prestacionDTO = new PrestacionDTO();
			ViaAccesoVO viaAccesoVO = new ViaAccesoVO();
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(tipoPrestacionSeleccionado);
			descripcionTipoCodificacion = IngresoSolicitudesPrestacionUtil.buscarDescripcionTipoPrestacion(listaTiposPrestaciones, tipoPrestacionSeleccionado);
			
			if(descripcionTipoCodificacion != null){
				tipoCodificacionVO.setDescripcionTipoCodificacion(descripcionTipoCodificacion);
			}			
			
			if (procedimientosVO.getConsecutivoViaAcceso() != null) {
				viaAccesoVO.setConsecutivoViaAcceso(procedimientosVO.getConsecutivoViaAcceso());
				viaAccesoVO =  IngresoSolicitudesPrestacionUtil.ingresarDescripcionViaAcceso(listaViasAccesoMedicamento, viaAccesoVO, procedimientosVO);
			}
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			prestacionDTO.setIndicadorNoPOS(procedimientosVO.getIndicadorNoPOS());
			prestacionDTO.setConsecutivoCodificacionPrestacion(procedimientosVO.getConsecutivoCodificacionProcedimiento());
			prestacionDTO.setDescripcionCodificacionPrestacion(procedimientosVO.getDescripcionCodificacionProcedimiento());
			prestacionDTO.setCodigoCodificacionPrestacion(procedimientosVO.getCodigoCodificacionProcedimiento());			
			if(procedimientosVO.getDescripcionCodificacionProcedimiento() == null){
				prestacionDTO.setCodigoCodificacionPrestacion(procedimientosVO.getCodigoServicio());
				prestacionDTO.setDescripcionCodificacionPrestacion(procedimientosVO.getDescripcionServicio());
			}
			prestacionDTO.setCantidad(procedimientosVO.getCantidadProcedimiento());
			prestacionDTO.setConsecutivoItemPresupuesto(procedimientosVO.getConsecutivoItemPresupuesto());
			prestacionDTO.setViaAccesoVO(viaAccesoVO);
			LateralidadesVO lateralidadesVO = new LateralidadesVO();
			if (procedimientosVO.getConsecutivoLaterialidad() != null) {
				lateralidadesVO.setConsecutivoLaterialidad(procedimientosVO.getConsecutivoLaterialidad());
				lateralidadesVO = IngresoSolicitudesPrestacionUtil.ingresarDescripcionLateralidad(lateralidadesVO, listaLateralidades, procedimientosVO);
			}
			prestacionDTO.setLateralidadesVO(lateralidadesVO);
			prestacionDTO.setProcedimientosVO(procedimientosVO);
			listaPrestacionesSolicitadas.add(prestacionDTO);
			procedimientosVO = new ProcedimientosVO();
			esModificacionPrestacion = false;
			if(marcaTranscribir!=0 ){
				actualizarDocumentosSoporte();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	public void adicionarMedicamentos() {
		String descripcionTipoCodificacion = null;
		try {
			
			IngresoSolicitudesPrestacionUtil.validarConsecutivoCodificacionMedicamento(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarCodificacionMedicamento(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarCantidadMedicamento(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarCantidadDosis(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarDosis(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarHoraFrecuencia(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarConsecutivoFrecuencia(medicamentoVO);
			IngresoSolicitudesPrestacionUtil.validarPrestacionesSolicitadas(medicamentoVO, listaPrestacionesSolicitadas);

			PrestacionDTO prestacionDTO = new PrestacionDTO();
			prestacionDTO.setDosisVO(new DosisVO());
			
			TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();
			tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(tipoPrestacionSeleccionado);
			descripcionTipoCodificacion = IngresoSolicitudesPrestacionUtil.buscarDescripcionTipoPrestacion(listaTiposPrestaciones, tipoPrestacionSeleccionado); 
			
			if(descripcionTipoCodificacion != null){
				tipoCodificacionVO.setDescripcionTipoCodificacion(descripcionTipoCodificacion);
			}		
			
			prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
			prestacionDTO.setConsecutivoCodificacionPrestacion(medicamentoVO.getConsecutivoCodificacionMedicamento());
			prestacionDTO.setDescripcionCodificacionPrestacion(medicamentoVO.getDescripcionCodificacionMedicamento());
			prestacionDTO.setCodigoCodificacionPrestacion(medicamentoVO.getCodigoCodificacionMedicamento());
			if(medicamentoVO.getDescripcionCodificacionMedicamento() == null){
				prestacionDTO.setCodigoCodificacionPrestacion(medicamentoVO.getCodigoMedicamentoPrestador());
				prestacionDTO.setDescripcionCodificacionPrestacion(medicamentoVO.getDescripcionMedicamentoPrestador());				
			}
			prestacionDTO.setCantidad(medicamentoVO.getCantidadMedicamento());
			prestacionDTO.setIndicadorNoPOS(medicamentoVO.getIndicadorNoPOS());

			prestacionDTO.getDosisVO().setConsecutivoCodigoDosis(medicamentoVO.getDosisVO().getConsecutivoCodigoDosis());
			if (medicamentoVO.getDosisVO().getConsecutivoCodigoDosis() != null && medicamentoVO.getDosisVO().getConsecutivoCodigoDosis().intValue() != ConstantesWeb.VALOR_INICIAL) {
				prestacionDTO = IngresoSolicitudesPrestacionUtil.agregarDescripcionDosis(prestacionDTO, listaDosisMedicamentos, medicamentoVO);
			}
			prestacionDTO.setConsecutivoItemPresupuesto(medicamentoVO.getConsecutivoItemPresupuesto());
			prestacionDTO.setViaAccesoVO(new ViaAccesoVO());
			prestacionDTO.setConcentracionVO(medicamentoVO.getConcentracionVO());
			prestacionDTO.setPresentacionVO(medicamentoVO.getPresentacionVO());
			prestacionDTO.setMedicamentosVO(medicamentoVO);
			listaPrestacionesSolicitadas.add(prestacionDTO);
			
			medicamentoVO = new MedicamentosVO();
			medicamentoVO = IngresoSolicitudesPrestacionUtil.crearMedicamentosVO();			
			
			esModificacionPrestacion = false;
			if(marcaTranscribir!=0 ){
				actualizarDocumentosSoporte();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void eliminarPrestacion() {
		try {
			listaPrestacionesSolicitadas.remove(prestacionSeleccionada);
			if(marcaTranscribir!=0 ){
				actualizarDocumentosSoporte();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void modificarPrestacion() {
		try {
			if (prestacionSeleccionada != null && prestacionSeleccionada.getProcedimientosVO() != null) {
				tipoPrestacionSeleccionado = prestacionSeleccionada.getTipoCodificacionVO().getConsecutivoCodigoTipoCodificacion();
				procedimientosVO = prestacionSeleccionada.getProcedimientosVO();
			}
			if (prestacionSeleccionada != null && prestacionSeleccionada.getMedicamentosVO() != null) {
				tipoPrestacionSeleccionado = prestacionSeleccionada.getTipoCodificacionVO().getConsecutivoCodigoTipoCodificacion();
				medicamentoVO = prestacionSeleccionada.getMedicamentosVO();
			}
			esModificacionPrestacion = true;
			
			mostrarPanelCUPS = IngresoSolicitudesPrestacionUtil.cambioDeTipoPrestacionCUPS(consecutivoCUPS, tipoPrestacionSeleccionado);
			mostrarPanelMedicamentos = IngresoSolicitudesPrestacionUtil.cambioDeTipoPrestacionCUMS(consecutivoCUMS, tipoPrestacionSeleccionado);
			
			listaPrestacionesSolicitadas.remove(prestacionSeleccionada);
			if(marcaTranscribir!=0 ){
				actualizarDocumentosSoporte();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	private void actualizarDocumentosSoporte() throws LogicException {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			listaDocumentoSoporteVOs = IngresoSolicitudesSoportesUtil.agruparDocumentosSoporte(registrarSolicitudesController.consultaTiposSoportexPrestacion(listaPrestacionesSolicitadas));
			listaSoporteVOs = IngresoSolicitudesSoportesUtil.crearListaSoportes(listaDocumentoSoporteVOs, listaSoporteVOs);

		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_VALIDADOR_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (listaSoporteVOs != null && !listaSoporteVOs.isEmpty()) {
			deshabilitarBtnDocumentos = false;
		} else if (listaSoporteVOs != null && listaSoporteVOs.isEmpty()) {
			deshabilitarBtnDocumentos = true;
		} else {
			deshabilitarBtnDocumentos = true;
		}
	}
	
	private void validacionesDocumentoSoporte() throws LogicException {
		archivosObligatoriosFaltantes = false;
		for (SoporteVO soporteVO : listaSoporteVOs) {
			// Validacion de si existe archivo cuando el sooprte es
			// obligatorio
			if (soporteVO.getNombreSoporte() == null || soporteVO.getData() == null) {
				archivosObligatoriosFaltantes = true;
			}

			// Validacion de archivos repetidos
			IngresoSolicitudesSoportesUtil.validacionArchivosRepetidos(listaSoporteVOs, soporteVO);

			// Validacion de tamano del archivo
			IngresoSolicitudesSoportesUtil.validacionTamanoArchivo(soporteVO);
		}
	}	

	public void aceptarDocumentosSoporte() {
		try {
			mostrarPopupConfirmacionDocumentos = false;
			validacionesDocumentoSoporte();
			if (archivosObligatoriosFaltantes) {
				mostrarPopupConfirmacionDocumentos = true;
			} else {
				mostrarPopupDocumentosSoporte = false;
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void aceptarDocumentosFaltantes() {
		try {
			validacionesDocumentoSoporte();
			mostrarPopupDocumentosSoporte = false;
			mostrarPopupConfirmacionDocumentos = false;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cancelarDocumentosFaltantes() {
		mostrarPopupDocumentosSoporte = true;
		mostrarPopupConfirmacionDocumentos = false;
	}

	public void mostrarPopupDocumentosSoporte() {
		mostrarPopupDocumentosSoporte = true;
	}

	public void ocultarPopupDocumentosSoporte() {
		mostrarPopupDocumentosSoporte = false;
	}	

	/**
	 * 
	 * @return
	 * @throws LogicException
	 */
	private ValidacionEspecialVO grabarSolicitudValAfiliado() throws LogicException{
		afiliadoVO = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getAfiliadoVO();

		ValidacionEspecialVO validacionEspecialVO = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getValidacionEspecialVO();
		if (afiliadoVO == null || afiliadoVO.getNumeroIdentificacion() == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_AFILIADO), ErrorType.DATO_NO_EXISTE);
		}

		mostrarPopupSolicitudDevuelta = false;

		/**IPS**/
		if(verIps==true){
			afiliadoVO.setTutela(ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
		}else{
			afiliadoVO.setTutela(tutela == ConstantesWeb.TAMANO_LISTA ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
		}
		
		return validacionEspecialVO;
	}

	private void grabarSolicitudValInconcistenciasAdmin(){
		if (listaInconsistenciasAdministrativas != null) {
			for (PrestacionDTO prestacionDTO : listaPrestacionesSolicitadas) {
				prestacionDTO.setMarcaCalidad(ConstantesWeb.MARCA_CALIDAD);
				informacionInconsistenciasPrestacion(prestacionDTO);
			}
		}
	}

	private void informacionInconsistenciasPrestacion(PrestacionDTO prestacionDTO) {
		for (InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO : listaInconsistenciasAdministrativas) {
			if (prestacionDTO.getConsecutivoCodificacionPrestacion().compareTo(inconsistenciasResponseServiceVO.getConsecutivoPrestacion().intValue()) == 0) {
				prestacionDTO.setMarcaCalidad(inconsistenciasResponseServiceVO.isConfirmada() ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
				break;
			}
		}
	}
	

	
	public void limpiarIngresoSolicitud(){
		try{
			numeroSolicitudSOS = "";
			domi=-1;
			txtObservaciones = "";
			listaPrestacionesSolicitadas.clear();
			listaDiagnosticosSeleccionados.clear();
			superaTopeSoat=false;
			consecutivoContingenciaRecobro = ConstantesWeb.VALOR_INICIAL;
			
			recienNacido=ConstantesWeb.VALOR_INICIAL;
			recienNacidoVO = new RecienNacidoVO();
			cambiarOpcionRecienNacido();

			fechaActual = Calendar.getInstance().getTime();
			hospitalizacionVO = GuardarSolicitudesUtil.crearHospitalizacionVO();
			atencionVO = GuardarSolicitudesUtil.crearAtencionVO();			
			medicamentoVO = IngresoSolicitudesPrestacionUtil.crearMedicamentosVO();			
			
			recienNacidoVO.setGeneroVO(new GeneroVO());	
			cargarListaCombos();	
			
			medicoVO = new MedicoVO();
			ipsVO = new IPSVO();
	
			listaInconsistenciasAdministrativas.clear();
			listaSoporteVOs.clear();
			deshabilitarBtnGuardar = false;
			solicitud = new SolicitudVO();
			/**IPS**/
			validaIPS();
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	/**
	 * 
	 */
	public void grabarSolicitud() {
		try {
			
			indDescargaDevolucion = false;
			indDescargaOPS = false;
			mostrarPopupMedicos = false;
			
			RegistrarSolicitudVO registrarSolicitudVO 	= new RegistrarSolicitudVO();
			PrestadorVO prestadorVO 					= new PrestadorVO();
			TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			ContingenciaRecobroVO recobroVO 			= new ContingenciaRecobroVO();
			com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO medicoVOModelo = new com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO();
			
			GuardarSolicitudesUtil.grabarSolicitudValidarDiagnosticos(listaDiagnosticosSeleccionados);
			GuardarSolicitudesUtil.grabarSolicitudValidarObligatorios(medicoVO, ipsVO, listaPrestacionesSolicitadas);
			GuardarSolicitudesUtil.grabarSolicitudValHospitalizacion(listaPrestacionesSolicitadas, deshabilitarSelectTipoPrestacion, deshabilitarBtnHospitalizacion, hospitalizacionVO);
			validacionesDocumentoSoporte();
			GuardarSolicitudesUtil.grabarSolicitudValInconsistencias(listaInconsistenciasAdministrativas);

			if(marcaTranscribir==0){
				GuardarSolicitudesUtil.grabarSolicitudValSoportesTranscripcion(listaSoporteVOs);
			}
			
			ValidacionEspecialVO validacionEspecialVO = grabarSolicitudValAfiliado();

			registrarSolicitudVO.setAfiliadoVO(afiliadoVO);
			registrarSolicitudVO.setObservaciones(txtObservaciones);

			recienNacidoVO.setRecienNacido(recienNacido == ConstantesWeb.TAMANO_LISTA ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);

			if (recienNacido == ConstantesWeb.TAMANO_LISTA) {
				recienNacidoVO.setPartoMultiple(partoMultiple == ConstantesWeb.TAMANO_LISTA ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
			}
			registrarSolicitudVO.setRecienNacidoVO(recienNacidoVO);

			if (domi != null){
			    atencionVO.setDomi(domi == ConstantesWeb.TAMANO_LISTA ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
			}else{
				atencionVO.setDomi(ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
			}	
			
			
			registrarSolicitudVO.setMarcaHospitalizacion(!deshabilitarBtnHospitalizacion ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
			registrarSolicitudVO.setHospitalizacionVO(hospitalizacionVO);
			registrarSolicitudVO.setlDiagnosticosSolicitud(listaDiagnosticosSeleccionados);

			
			medicoVOModelo = GuardarSolicitudesUtil.grabarSolicitudMedico(medicoVOModelo, tiposIdentificacionVO, medicoVO);
			
			if (medicoAdscrito == null) {
				validarMedicoAdscrito();
			}
			medicoVOModelo.setMedicoAdscrito(medicoAdscrito ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);			
			registrarSolicitudVO.setMedicoVO(medicoVOModelo);

			prestadorVO = GuardarSolicitudesUtil.grabarSolicitudValPrestador(prestadorVO, ipsVO, prestadorAdscrito, ciudadVO);			
			registrarSolicitudVO.setPrestadorVO(prestadorVO);

			grabarSolicitudValInconcistenciasAdmin();
			registrarSolicitudVO.setlPrestacionesSolicitud(listaPrestacionesSolicitadas);

			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			GestionarSolicitudController gestionarSolicitudController = new GestionarSolicitudController();
			IngresoSolicitudIpsBean ingresoSolicitudIpsBean = new IngresoSolicitudIpsBean();
			
			if(marcaTranscribir!=0){
				registrarSolicitudVO.setMedioContactoVO(registrarSolicitudesController.consultaMediosContactoxCodigo(ConstantesWeb.MEDIO_CONTACTO_ASI).get(0));
			}else{
				registrarSolicitudVO.setMedioContactoVO(registrarSolicitudesController.consultaMediosContactoxCodigo(ConstantesWeb.MEDIO_CONTACTOS_AFILIADO).get(0));
			}
			
			/**IPS**/
			if(!verIps){
				atencionVO.setSuperaTopeSoat(ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
				recobroVO.setConsecutivoCodigoRecobro(ConstantesWeb.CONSECUTIVO_RECOBRO_NO_RECOBRO);
				registrarSolicitudVO.getMedioContactoVO().setConsecutivoMedioContacto(ConstantesWeb.CONSECUTIVO_CODIGO_MEDIO_CONTACTO_IPS);
				registrarSolicitudVO.getMedioContactoVO().setCodigoMedioContacto(ConstantesWeb.MEDIO_CONTACTOS_IPS);
			}else{
				atencionVO.setSuperaTopeSoat(superaTopeSoat ? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL : ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_NO);
				recobroVO.setConsecutivoCodigoRecobro(consecutivoContingenciaRecobro);
			}
			registrarSolicitudVO.setAtencionVO(atencionVO);
			registrarSolicitudVO.setRecobroVO(recobroVO);

			registrarSolicitudVO.setlSoporteVO(listaSoporteVOs);

			SolicitudService solicitudService = gestionarSolicitudController.crearSolicitudService();

			GrabarSolicitudResType grabarSolicitudResType = gestionarSolicitudController.guardarSolicitud(solicitudService, registrarSolicitudVO, FacesUtils.getUserName(), solicitud,	validacionEspecialVO);
			solicitud = gestionarSolicitudController.consultaResultadoGuardar(grabarSolicitudResType, registrarSolicitudVO, solicitud);
			numeroSolicitudSOS = solicitud.getNumeroSolicitudSOS();
			
			GuardarSolicitudesUtil.validarRespuestaGuardarSolicitud(solicitud);
					
			if (solicitud != null && solicitud.getConsecutivoSolicitud() != null && listaSoporteVOs != null && !listaSoporteVOs.isEmpty()) {
				listaSoporteVOs = guardarDocumentosVisos(afiliadoVO, solicitud, atencionVO, listaSoporteVOs);
			}

			ejecutarMalla();
			deshabilitarBtnGuardar = true;
			
			deshabilitarBtnTerminar = false;
			deshabilitarBtnLeberar = true;
			deshabilitarBtnDevolver = true;
			
			/**IPS**/
			if(verIps!=true){
				ingresoSolicitudIpsBean.gestionRespuestaGrabarSolicitud();
			}else{
				gestionRespuestaGrabarSolicitud();
			}
			

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}

	}

	/**
	 * @throws Exception 
	 * 
	 */
	private void gestionRespuestaGrabarSolicitud() throws LogicException{
		ConsultarSolicitudController consultarSolicitudController;
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		if (!listaInconsistenciasDevueltasMallaService.isEmpty() && listaPrestacionesInconsistencias.isEmpty() && listaInconsistenciasAdministrativas.isEmpty()
				&& listaPrestacionesAuditoria.isEmpty() && listaPrestacionesAprobadas.isEmpty()) {
			// Solo devueltas
			mostrarPopupSolicitudDevuelta = true;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_DEVUELTA) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!listaPrestacionesAuditoria.isEmpty() && listaPrestacionesInconsistencias.isEmpty() && listaInconsistenciasAdministrativas.isEmpty()	&& listaInconsistenciasDevueltasMallaService.isEmpty()) {
			// Solo auditoria
			if(marcaTranscribir!=0){
				instanciarBPM();
			}
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_AUDITORIA) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!listaInconsistenciasAdministrativas.isEmpty()) {
			// Si presenta al menos una de asi de calidad
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_INCONSISTENCIAS_ADMINISTRATIVAS) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
			deshabilitarBtnGuardar = false;

		} else if (!listaPrestacionesInconsistencias.isEmpty() && listaPrestacionesAuditoria.isEmpty() && listaInconsistenciasAdministrativas.isEmpty()	&& listaInconsistenciasDevueltasMallaService.isEmpty()) {
			// Solo inconsistencia diferente a auditoria
			if(marcaTranscribir!=0){
				instanciarBPM();
			}
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_INCONSISTENCIAS) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!listaPrestacionesInconsistencias.isEmpty() || !listaPrestacionesAuditoria.isEmpty()) {
			// Mix auditoria - inconsistencias
			if(marcaTranscribir!=0){
				instanciarBPM();
			}
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);

		} else if (!listaInconsistenciasDevueltasMallaService.isEmpty() && !listaPrestacionesAprobadas.isEmpty() && listaPrestacionesInconsistencias.isEmpty() && listaPrestacionesAuditoria.isEmpty() && listaInconsistenciasAdministrativas.isEmpty()) {
			// Mix devueltas - aprobadas
			
			boolean generaOPS = llamadoAServiciosDireccionamientoFechaEntrega();
			List<ConceptosGastoVO> listaConceptoGastos = consultarSolicitudController.consultaConceptosGastosxSolicitud(solicitud.getConsecutivoSolicitud());
			if(IngresoSolicitudesUtil.validarConceptoCero(listaConceptoGastos)){
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO), ErrorType.PARAMETRO_ERRADO);
			}
			
			if (generaOPS) {
				
				/*
				 * Control de cambios marca acceso directo
				 */
				validarMarcaAccesoDirecto();

				
				mostrarPopupDevolucionOPS = true;
				mostrarPopupGenerarOPS = false;
				mostrarPopupSolicitudDevuelta= false;
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
			}else if (!errorServDireccionamiento){
				mostrarPopupSolicitudDevuelta = true;
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
			} 
			
			/*Control de cambios que valida si alguna de las prestaciones no fué
			 * Direccionada o si no presenta fecha entrega
			 */
			if(validarPrestacionesAltaFrecuenciaBajoCosto()){
				instanciarBPM();
			}
			
		} else if (listaInconsistenciasAdministrativas.isEmpty() && listaPrestacionesInconsistencias.isEmpty() && listaPrestacionesAuditoria.isEmpty()	&& listaInconsistenciasDevueltasMallaService.isEmpty()) {
			// Aprobadas
			mostrarPopupGenerarOPS = llamadoAServiciosDireccionamientoFechaEntrega();
			List<ConceptosGastoVO> listaConceptoGastos = consultarSolicitudController.consultaConceptosGastosxSolicitud(solicitud.getConsecutivoSolicitud());
			if(IngresoSolicitudesUtil.validarConceptoCero(listaConceptoGastos)){
				mostrarPopupGenerarOPS = false;
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO), ErrorType.PARAMETRO_ERRADO);
			}
			
			
			if(mostrarPopupGenerarOPS){
				/*
				 * Control de cambios marca acceso directo
				 */
				validarMarcaAccesoDirecto();				
			}

			if(validarPrestacionesAltaFrecuenciaBajoCosto()){
				instanciarBPM();
			}
			
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_CREADA) + solicitud.getNumeroSolicitudSOS(), "blue");
		} else {
			if(marcaTranscribir!=0){
				instanciarBPM();
			}
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		}
	}

	private void mostrarReciboCaja(DatosSolicitudVO solicitudSeleccionada) {
		GenerarReciboBean reciboBean = (GenerarReciboBean) FacesUtils.getManagedBean("generarReciboBean");
		if (reciboBean == null) {
			reciboBean = new GenerarReciboBean();
		}
		reciboBean.setMostrarModalReciboCaja(false);
		reciboBean.setBeanSolicitud(ConstantesWeb.INGRESO_SOLICITUD);
		reciboBean.setSolicitudSeleccionada(solicitudSeleccionada);

		OPSCajaVO opsCajas = reciboBean.getOpsCajas();

		boolean muestraMensajeCausales = mostrarMensajeCausales(reciboBean);

		if (ConstantesWeb.SI.equalsIgnoreCase(opsCajas.getMuestraMensajeIPS())) {
			MostrarMensaje.mostrarMensaje(ConstantesWeb.CLAVE_MENSAJE_PAGO_IPS, COLOR_AZUL);
		}

		if ((reciboBean.getListInformacion() == null || reciboBean.getListInformacion().isEmpty())
				|| ConstantesWeb.NO.equalsIgnoreCase(opsCajas.getMuestraReciboCaja())) {
			setMostrarPopupGenerarOPS(true);
		} else {
			if (muestraMensajeCausales && reciboBean.getListInformacion() != null
					&& reciboBean.getListInformacion().isEmpty()) {
				setMostrarPopupGenerarOPS(true);
			} else {
				setMostrarPopupGenerarOPS(false);
				reciboBean.setMostrarModalReciboCaja(mostrarModalReciboCaja);
				reciboBean.setTabSeleccionado(ConstantesWeb.TAB_GENERAR_RECIBO);
			}
		}
	}

	private boolean mostrarMensajeCausales(GenerarReciboBean reciboBean) {
		boolean resp = false;
		String causales = reciboBean.getCausales();
		if (Utilidades.validarCamposVacios(causales)) {
			resp = true;
			MostrarMensaje.mostrarMensaje(ConstantesWeb.MENSAJE_CAUSAS_NO_PAGO + causales, COLOR_AZUL);
		}
		return resp;
	}
	
	/**
	 * 
	 */
	public void validarMarcaAccesoDirecto(){
		if (!validarMarcaAccesoDirecto(solicitud.getConsecutivoSolicitud())) {
			MenuBean menu = (MenuBean) FacesUtils.getManagedBean(ConstantesWeb.MENU_BEAN);
			if (!menu.isUsuarioIps()) {
				mostrarModalReciboCaja = true;
				DatosSolicitudVO solicitudSeleccionada = new DatosSolicitudVO();
				solicitudSeleccionada.setAfiliadoVO(afiliadoVO);
				solicitudSeleccionada.setNumeroSolicitud(solicitud.getConsecutivoSolicitud());
				solicitudSeleccionada.setFechaSolicitud(fechaActual);
				this.mostrarReciboCaja(solicitudSeleccionada);
				return;
			} else {
				mostrarPopupAccesoDirecto = true;
			}
		}
		
		if (!esPlanPOS(afiliadoVO.getPlan().getConsectivoPlan().intValue())) {
			if(!excedeSumaPrestacionesSMLV()){
				mostrarPopupAccesoDirecto = true;
				return;
			}
		}else {
			mostrarPopupAccesoDirecto = true;
			return;
		}
				
	}
	
	/**
	 * Metodo para saber si alguna prestación no fue direccionada o no tiene Fecha Entrega
	 * @return
	 */	
	
	private boolean validarPrestacionesAltaFrecuenciaBajoCosto() throws LogicException{
		RegistrarSolicitudController registrarSolicitudesController;
		try {
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (IOException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return registrarSolicitudesController.validarPrestacionesAltaFrecuenciaBajoCosto(solicitud.getConsecutivoSolicitud(), FacesUtils.getUserName());
	}
	
	/**
	 * Metodo para saber si es el afiliado tiene plan POS
	 * @return
	 */
	public boolean esPlanPOS(int consectivoPlan){ //Ajustar regla
		return consectivoPlan == ConstantesWeb.PLAN_POS || 
				consectivoPlan == ConstantesWeb.PLAN_POS_SUBSIDIADO;
	}
	
	/**
	 * Metodo para validar si las prestaciones marcadas 
	 * @return
	 */
	public boolean excedeSumaPrestacionesSMLV(){
		boolean rta = false;
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			rta = valorPrestacionesMarcadas.intValue() > registrarSolicitudesController.consultarSalarioMinimoLV().intValue();
			valorPrestacionesMarcadas = new BigDecimal(0);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}		
		return rta;
	}
	
	/**
	 * Metodo para validar si la prestacion esta marcada como acceso directo
	 * @return
	 * @throws LogicException 
	 */
	public boolean validarMarcaAccesoDirecto(int consecutivoSolicitud){
		
		BigDecimal  valorLiquidacion = new BigDecimal("0");
		valorPrestacionesMarcadas = new BigDecimal(0);
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			valorLiquidacion = registrarSolicitudesController.consultarMarcaAccesoDirectoPrestacion(consecutivoSolicitud);				
			valorPrestacionesMarcadas = valorPrestacionesMarcadas.add(valorLiquidacion);
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}

		return valorPrestacionesMarcadas.intValue() > 0;
	}
	

	/**
	 * Metodo encargado de guardar los documentos en Visos
	 */
	public List<SoporteVO> guardarDocumentosVisos(AfiliadoVO afiliadoVO, SolicitudVO solicitud, AtencionVO atencionVO, List<SoporteVO> listaSoporteVOs) {
		GuardarDocumentosVisosBean guardarDocumentosVisos = new GuardarDocumentosVisosBean();
		try {
			return guardarDocumentosVisos.guardarDocumentos(afiliadoVO, solicitud, atencionVO, listaSoporteVOs);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ConstantesWeb.CADENA_VACIA));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesEJB.ERROR_GRABAR_IDENTIFICADOR_DOCUMENTO), ConstantesWeb.COLOR_ROJO);
		}
		return listaSoporteVOs;
	}

	private boolean llamadoAServiciosDireccionamientoFechaEntrega() throws LogicException {
		DireccionamientoSolicitudVO direccionamientoSolicitudVO;
		boolean generaOPS = false;
		errorServDireccionamiento = false;
		DireccionamientoBean bean = new DireccionamientoBean();
		direccionamientoSolicitudVO = bean.ejecutarDireccionamiento(solicitud);

		if (GuardarSolicitudesUtil.validarDireccionamientoSolicitud(direccionamientoSolicitudVO)) { 
			String respuestaDireccionamiento = ConstantesWeb.CADENA_VACIA;

			for (AdvertenciaVO advertenciaVO : direccionamientoSolicitudVO.getServiceErrorVO().getlAdvertenciaVO()) {
				LOG.error(direccionamientoSolicitudVO.getConsecutivoSolicitud() + ConstantesWeb.CADENA_EN_BLANCO + advertenciaVO.getMensajeAdvertencia());
			}
			respuestaDireccionamiento = ConstantesWeb.ERROR_SERVICIO_DIRECCIONAMIENTO + solicitud.getNumeroSolicitudSOS();
			errorServDireccionamiento = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, respuestaDireccionamiento, ConstantesWeb.CADENA_VACIA));
			if(marcaTranscribir!=0){
				instanciarBPM();		
			}

		} else if (direccionamientoSolicitudVO != null) {

			FechaEntregaBean entregaBean = new FechaEntregaBean();
			fechaEntregaSolicitudVO = entregaBean.ejecutarFechaEntrega(solicitud);

			if (fechaEntregaSolicitudVO == null) {
				fechaEntregaSolicitudVO = new FechaEntregaSolicitudVO();
				fechaEntregaSolicitudVO.setlPrestacionFechaEntregaVO(new ArrayList<PrestacionFechaEntregaVO>());
			}
			LiquidacionSolicitudVO liquidacionSolicitudVO = null;

			liquidacionSolicitudVO = agregarLiquidacionSolicitud(liquidacionSolicitudVO);
			
			//Si no liquida no genera OPS
			if (liquidacionSolicitudVO!=null) {
				generaOPS = generarNumeroUnicoOPS(liquidacionSolicitudVO);
			}
		}  
		return generaOPS;
	}

	private LiquidacionSolicitudVO agregarLiquidacionSolicitud(LiquidacionSolicitudVO liquidacionSolicitudVO) {

		LiquidacionSolicitudVO liquidacionSolicitud = liquidacionSolicitudVO;

		for (PrestacionFechaEntregaVO prestacionFechaEntregaVO : fechaEntregaSolicitudVO.getlPrestacionFechaEntregaVO()) {
			PrestacionDTO prestacionDTOFechaEntre = prestacionFechaEntregaVO.getPrestacionDTO();
			if (prestacionDTOFechaEntre == null) {
				prestacionDTOFechaEntre = new PrestacionDTO();
			}
			for (PrestacionDTO prestacionDTO : listaPrestacionesSolicitadas) { 
				asignarFechaEntregaPrestacion(prestacionDTO, prestacionDTOFechaEntre, prestacionFechaEntregaVO);
			}
			String fechaActualS = Utilidades.obtenerStringDeDate(fechaActual, ConstantesWeb.CARACTER_SEPARADOR, false);
			String fechaEntrega = Utilidades.obtenerStringDeDate(prestacionFechaEntregaVO.getFechaEstimadaEntrega(), ConstantesWeb.CARACTER_SEPARADOR, false);
			if (fechaActualS.equals(fechaEntrega)) {
				try {
					liquidacionSolicitud = GuardarSolicitudesUtil.ejecutarLiquidacion(solicitud);
				} catch (LogicException e) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), ConstantesWeb.CADENA_VACIA));
					LOG.error(e.getMessage(), e);
				} 
			} 
		}
		return liquidacionSolicitud;
	}

	private void asignarFechaEntregaPrestacion(PrestacionDTO prestacionDTO, PrestacionDTO prestacionDTOFechaEntre, PrestacionFechaEntregaVO prestacionFechaEntregaVO){
		for  (PrestacionDTO prestacionApro : listaPrestacionesAprobadas) {
			if (prestacionApro.getConsecutivoCodificacionPrestacion().compareTo(prestacionDTO.getConsecutivoCodificacionPrestacion()) == 0) {
				if (prestacionDTO.getConsecutivoCodificacionPrestacion().compareTo(prestacionDTOFechaEntre.getConsecutivoCodificacionPrestacion()) == 0) {
					prestacionDTO.setFechaEstimadaEntrega(prestacionFechaEntregaVO.getFechaEstimadaEntrega());
					prestacionDTO.setNumeroRadicacionSolicitud(fechaEntregaSolicitudVO.getNumeroRadicacionSolicitud());
					break;
				}
			}
		}
	}	

	/** Metodo que se utiliza para la generacion el numero unico de la OPS*/
	private boolean generarNumeroUnicoOPS(LiquidacionSolicitudVO liquidacionSolicitudVO)throws LogicException {
		ServiceErrorVO serviceErrorVO = null;
		boolean generaNumerOPS = false;

		if (liquidacionSolicitudVO != null && liquidacionSolicitudVO.getResultadoLiquidacion() == ConstantesWeb.RESULTADO_LIQUIDACION) {

			GenerarNumeroUnicoOPSBean numeroUnicoOPSBean = new GenerarNumeroUnicoOPSBean();				
			serviceErrorVO = numeroUnicoOPSBean.generarNumeroUnicoAutorizacionServicio(solicitud.getConsecutivoSolicitud(), FacesUtils.getUserName());			

			if(FuncionesAppWeb.validateServiceError(serviceErrorVO)){
				listaAutorizacionServicioVO = numeroUnicoOPSBean.consultaNumeroAutorizacionxSolicitud(solicitud.getConsecutivoSolicitud(), null);
				generaNumerOPS = true;			
			}		

		} else {
			LOG.error(liquidacionSolicitudVO.getMensajeResultadoLiquidacion());
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, 
							Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE) + liquidacionSolicitudVO.getMensajeResultadoLiquidacion(),	
							ConstantesWeb.CADENA_VACIA));
		}

		return generaNumerOPS;

	}

	/** Metodo que se utiliza para la generacion del reporte de la OPS
	 * @throws LogicException */
	public void generarOPS(){		
		try{
			if (indDescargaOPS) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION), ErrorType.PARAMETRO_ERRADO);
			}

			if (listaAutorizacionServicioVO != null && !listaAutorizacionServicioVO.isEmpty()) {
				GenerarReporteBean generarReporteBean = new GenerarReporteBean();			
				generarReporteBean.formatoOPS(afiliadoVO, solicitud, listaPrestacionesAprobadas, listaAutorizacionServicioVO, generarConObservaciones);
				indDescargaOPS = true;
			}	
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void instanciarBPM() {		
		try {
			GuardarSolicitudesUtil.instanciarBPM(solicitud);           
		} catch (LogicException e) {
			mostrarPopupEjecutarBPM = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SERVICIO_BPM), ConstantesWeb.CADENA_VACIA));
			LOG.error(e.getMessage(), e);
		}		
	}

	public void ejecutarBPMSegundaVez() {
		try {
			GuardarSolicitudesUtil.instanciarBPM(solicitud);
			mostrarPopupEjecutarBPM = false;
		} catch (LogicException ex){
			mostrarPopupEjecutarBPM = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SERVICIO_BPM), ConstantesWeb.CADENA_VACIA));
			LOG.error(ex.getMessage(), ex);
	    } catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.EXCEPCION_SERVICIO_BPM), ConstantesWeb.COLOR_ROJO);
		}
	}

	private void ejecutarMalla() throws LogicException {			
		int numeroInconsistencia    = ConstantesWeb.CONS_ESTADO_INCONSISTENCIAS_MALLA;
		int cnsEstadoDevuelta       = ConstantesWeb.CODIGO_ESTADO_DEVUELTA;
		int cnsEstadoInconsistencia = ConstantesWeb.CONS_ESTADO_INCONSISTENCIAS_MALLA;
		int cnsEstadoAuditoria      = ConstantesWeb.CONS_ESTADO_AUDITORIA_MALLA;
		
		listaInconsistenciasDevueltasMallaService = new ArrayList<InconsistenciasResponseServiceVO>();
		listaInconsistenciasAdministrativas = new ArrayList<InconsistenciasResponseServiceVO>();
		listaPrestacionesInconsistencias = new ArrayList<PrestacionDTO>();
				
		EjecutarMalla ejecutarMalla = new EjecutarMalla();
		MallaService mallaService = ejecutarMalla.crearSolicitudService();
		EjecutarMallaBean bean = new EjecutarMallaBean();
		inconsistenciasVO = bean.ejecutarServicioMalla(mallaService, ejecutarMalla, solicitud);
		listaPrestacionesInconsistenciasMallaService = bean.getListaPrestacionesInconsistenciasMallaService();
	
			
		for (PrestacionDTO prestacionDTO : listaPrestacionesInconsistenciasMallaService) {
			agregarPrestacionAListaPrestaciones(cnsEstadoAuditoria, prestacionDTO);
			agregarPrestacionAListaInconsistencias(cnsEstadoDevuelta, prestacionDTO);

			if (prestacionDTO.getEstadoVO().getConsecutivoEstadoSolicitud().compareTo(cnsEstadoInconsistencia) == 0) {
				for (InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO : prestacionDTO.getlInconsistenciasResponseServiceVO()) {
					numeroInconsistencia = llenarListaInconsistenciasAdministrativas(numeroInconsistencia,	inconsistenciasResponseServiceVO);					
				}
				
				listaPrestacionesInconsistencias.add(prestacionDTO);
				
			}			
		}
	}

	private int llenarListaInconsistenciasAdministrativas(int numeroInconsistencia, InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO) {
		int numInconsistencia = numeroInconsistencia;
		for (MarcaInconsistenciaVO marcaInconsistenciaVO : inconsistenciasResponseServiceVO.getlMarcaInconsistencia()) {
			if (marcaInconsistenciaVO.getConsecutivoMarcaInconsistencia() != null && marcaInconsistenciaVO.getConsecutivoMarcaInconsistencia().compareTo(ConstantesWeb.COD_MARCA_INCONSISTENCIA) == 0) {
				if(inconsistenciasResponseServiceVO.getEstadoVO().getConsecutivoEstadoSolicitud() == ConstantesWeb.CONS_ESTADO_INCONSISTENCIAS_MALLA){
				    inconsistenciasResponseServiceVO.setNumero(numInconsistencia);				
					listaInconsistenciasAdministrativas.add(inconsistenciasResponseServiceVO);
					numInconsistencia++;
				}				
				break;
			}
		}
		return numInconsistencia;
	}

	private void agregarPrestacionAListaInconsistencias(int cnsEstadoDevuelta, PrestacionDTO prestacionDTO) {		
		
		if (prestacionDTO.getEstadoVO().getConsecutivoEstadoSolicitud().compareTo(cnsEstadoDevuelta) == 0) {
			for (InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO : prestacionDTO.getlInconsistenciasResponseServiceVO()) {
				if (inconsistenciasResponseServiceVO.getEstadoVO().getConsecutivoEstadoSolicitud().compareTo(cnsEstadoDevuelta) == 0) {
					informacionInconsistenciasPrestacion(prestacionDTO,	inconsistenciasResponseServiceVO);
					listaInconsistenciasDevueltasMallaService.add(inconsistenciasResponseServiceVO);
				}
			}
		}
	}

	private void informacionInconsistenciasPrestacion(PrestacionDTO prestacionDTO, InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO) {
		String codigoCodificacionPrestacion = prestacionDTO.getCodigoCodificacionPrestacion().trim();
		for (PrestacionDTO prestacion : listaPrestacionesSolicitadas) {
			if (prestacion.getCodigoCodificacionPrestacion().trim().equals(codigoCodificacionPrestacion)) {
				inconsistenciasResponseServiceVO.setDescripcionPrestacion(prestacion.getDescripcionCodificacionPrestacion());
				break;
			}
		}
	}

	private void agregarPrestacionAListaPrestaciones(int cnsEstadoAuditoria, PrestacionDTO prestacionDTO) {
		if (prestacionDTO.getlInconsistenciasResponseServiceVO().isEmpty()) {
			listaPrestacionesAprobadas.add(prestacionDTO);
		}

		if (prestacionDTO.getEstadoVO().getConsecutivoEstadoSolicitud().compareTo(cnsEstadoAuditoria) == 0) {
			listaPrestacionesAuditoria.add(prestacionDTO);
		}
	}	

	
	/** Metodo que se utiliza para la generacion del reporte de devolucion
	 *  @throws LogicException 
	 */
	public void ocultarPopupDevuelta() {
		/**IPS**/
		if(verIps!=false){
			if (indDescargaDevolucion) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_FORMATO_YA_DESCARGADO_NEGACION), ConstantesWeb.CADENA_VACIA));
			}
			
			GenerarReporteBean generarReporteBean = new GenerarReporteBean();	
			generarReporteBean.formatoDevolucion(solicitud.getConsecutivoSolicitud(), 
					afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim(), 
					afiliadoVO.getNumeroIdentificacion().trim(), solicitud.getNumeroSolicitudSOS(), listaPrestacionesInconsistenciasMallaService);
			indDescargaDevolucion = true;			
		}
		mostrarPopupSolicitudDevuelta = false;	
	}

	public void cambiarOpcionRecienNacido() {
		if (recienNacido == Integer.parseInt(ConstantesWeb.RECIEN_NACIDO)) {
			esRecienNacido = true;
		} else {
			esRecienNacido = false;
			recienNacidoVO = new RecienNacidoVO();
			recienNacidoVO.setGeneroVO(new GeneroVO());
			esPartoMultiple = false;
			partoMultiple = null;
		}
	}

	public void cambiarOpcionPartoMultiple() {
		if (partoMultiple == ConstantesWeb.PARTO_MULTIPLE) {
			esPartoMultiple = true;
		} else {
			esPartoMultiple = false;
			recienNacidoVO.setNumeroHijos(null);
		}
	}

	public void mostrarPopupHospitalizacion() {
		mostrarPopupHospitalizacion = true;
	}

	public void ocultarPopupHospitalizacion() {
		mostrarPopupHospitalizacion = false;
		mostrarPopupCorteCuenta = false;
	}

	public void ocultarPopupCorteCuenta() {
		mostrarPopupCorteCuenta = false;
	}

	public void aceptarHospitalizacion() {
		try {
			IngresoSolicitudesUtil.validarFechaEgreso(hospitalizacionVO);
			hospitalizacionVO.setCorteCuenta(decimalFormat.parse(hospitalizacionVO.getCorteCuenta()).toString());
			BigInteger intcorteCuenta = BigInteger.valueOf(new Long(hospitalizacionVO.getCorteCuenta()));
			
			if (atencionVO.getClaseAtencionVO().getConsecutivoClaseAtencion() == ConstantesWeb.CLASE_ATENCION_HOSP_URG){ 
			
				IngresoSolicitudesUtil.validarCorteCuentaMinimo(intcorteCuenta, minimoCorteHospitalizacion);
							
				if (hospitalizacionVO.getCorteCuenta() != null && intcorteCuenta.intValue() > maximoCorteHospitalizacion) {
					mostrarPopupCorteCuenta = true;
				} else {
					mostrarPopupHospitalizacion = false;
					mostrarPopupCorteCuenta = false;
				}
			}else{
				mostrarPopupHospitalizacion = false;
				mostrarPopupCorteCuenta = false;
			}				
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS_HOSPITALIZACION), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cancelarHospitalizacion() {
		mostrarPopupHospitalizacion = false;
		hospitalizacionVO = new HospitalizacionVO();
		hospitalizacionVO = GuardarSolicitudesUtil.crearHospitalizacionVO();
	}

	/**
	 * Metodo encargado de escuchar cuando se a realizado una carga de un
	 * archivo
	 * 
	 * @author camoreno 06/11/2015
	 * @return
	 */
	public void listenerUpload(UploadEvent event) {
		try {
			
			if(documentoSoporteSeleccionado != null){
				UploadItem item = event.getUploadItem();
				byte[] data = Utilidades.extractBytes(item.getFile().getAbsolutePath());

				maxDocumentos = documentoSoporteSeleccionado.getTempCantidadMaximaSoportes() != null ? documentoSoporteSeleccionado.getTempCantidadMaximaSoportes() : 0;
				
				if (maxDocumentos.compareTo(0) != 0) {
					soporteSeleccionado.setNombreSoporte(item.getFileName());
					soporteSeleccionado.setRutaFisica(ConstantesWeb.CADENA_VACIA);
					soporteSeleccionado.setData(data);
					soporteSeleccionado.setTamanoArchivo(item.getFileSize());
					soporteSeleccionado.setSubidoAVisos(false);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		}
	}

	public void preBorrarArchivoCargado() {
		mostrarPopupConfirmacionBorrarArchivo = false;
		boolean encontroVarios;
		if (soporteSeleccionado != null && soporteSeleccionado.getNombreSoporte() != null && soporteSeleccionado.getData() != null) {
			mostrarPopupConfirmacionBorrarArchivo = true;
		} else {
			encontroVarios = IngresoSolicitudesSoportesUtil.buscarArchivosRepetidos(listaSoporteVOs, soporteSeleccionado);
			if (encontroVarios) {
				listaSoporteVOs.remove(soporteSeleccionado);
				if (soporteSeleccionado.getDocumentoSoporteVO() != null) {
					soporteSeleccionado.getDocumentoSoporteVO().setTempCantidadMaximaSoportes(soporteSeleccionado.getDocumentoSoporteVO().getTempCantidadMaximaSoportes() + ConstantesWeb.CANTIDAD_ARCHIVOS_REPETIDOS);
				}
			} else {
				MostrarMensaje.mostrarMensaje(Messages.getValorError(ConstantesWeb.ERROR_ARCHIVOS_BORRAR), "blue");
			}
		}
	}

	public void cancelarBorrarArchivoCargado() {
		mostrarPopupConfirmacionBorrarArchivo = false;
	}

	/**
	 * Metodo encargado de borrar un archivo cargado
	 * 
	 * @author camoreno 06/11/2015
	 * @return
	 */
	public void borrarArchivoCargado() {
		boolean encontroVarios = IngresoSolicitudesSoportesUtil.buscarArchivosRepetidos(listaSoporteVOs, soporteSeleccionado);
		if (encontroVarios) {
			listaSoporteVOs.remove(soporteSeleccionado);
			if (soporteSeleccionado.getDocumentoSoporteVO() != null) {
				soporteSeleccionado.getDocumentoSoporteVO().setTempCantidadMaximaSoportes(soporteSeleccionado.getDocumentoSoporteVO().getTempCantidadMaximaSoportes() + ConstantesWeb.CANTIDAD_ARCHIVOS_REPETIDOS);
			}
		} else {
			soporteSeleccionado.setNombreSoporte(null);
			soporteSeleccionado.setRutaFisica("");
			soporteSeleccionado.setData(null);
			soporteSeleccionado.setTamanoArchivo(null);
		}
	}
	

	/**
	 * Metodo encargado de adicionar un tipo de soporte
	 * 
	 * @author camoreno 06/11/2015
	 * @return
	 */
	public void addicionarSoporte() {
		try {
			if (documentoSoporteSeleccionado != null) {
				maxDocumentos = documentoSoporteSeleccionado.getTempCantidadMaximaSoportes() != null ? documentoSoporteSeleccionado.getTempCantidadMaximaSoportes() : 0;
		
				IngresoSolicitudesSoportesUtil.validarMaximoDocuementos(maxDocumentos);

				SoporteVO soporteVO;
				if (maxDocumentos.compareTo(0) != 0) {

					Date presentDate = new Date(System.currentTimeMillis());
					soporteVO = new SoporteVO();
					soporteVO.setFechaCreacion(presentDate);
					soporteVO.setUsuarioCreacion(FacesUtils.getUserName());
					soporteVO.setFechaUltimaModificacion(presentDate);
					soporteVO.setUsuarioUltimaModificacion(FacesUtils.getUserName());
					soporteVO.setRutaFisica(ConstantesWeb.CADENA_VACIA);
					soporteVO.setNumeroUnicIdentifUsuario(1);
					soporteVO.setConsecutivoCodigoDocumentoSoporte(documentoSoporteSeleccionado.getConsecutivoDocumento());
					soporteVO.setDescripcionDocumentoSoporte(documentoSoporteSeleccionado.getNombreDocumento());
					soporteVO.setDocumentoSoporteVO(documentoSoporteSeleccionado);
					listaSoporteVOs.add(soporteVO);

					maxDocumentos--;
					documentoSoporteSeleccionado.setTempCantidadMaximaSoportes(maxDocumentos);
				}
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	/**
	 * 
	 */
	public void devolverServicioSolicitado(){
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			registrarSolicitudesController.devolverSevicioSolicitado(solicitud.getConsecutivoSolicitud() , afiliadoVO.getPlan().getConsectivoPlan(), FacesUtils.getUserName());
			listaInconsistenciasDevueltasMallaService = registrarSolicitudesController.consultarResultadoMalla(solicitud.getConsecutivoSolicitud());
			
			// Mix devueltas - aprobadas
			mostrarPopupDevolucionOPS = true;
			mostrarPopupGenerarOPS = false;
			mostrarPopupSolicitudDevuelta= false;
			mostrarPopupAccesoDirecto = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
		}
	}
	
	/**
	 * 
	 */
	public void validarDescargaOPS() throws LogicException{
		boolean usrValido = false;
		
		ServicioAutenticacionBean serv = new ServicioAutenticacionBean();	
		usrValido = serv.validarUsuarioServicioAutenticacion(usrDescarga, usrDescargaPass);
			
		if (!usrValido) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.USUARIO_CLAVE_INVALIDA_DESCARGA) + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		}else if(!validarPerfilCoordinador()) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.USUARIO_DESCARGA_PERFIL_COORDINADOR_ERROR) + ConstantesWeb.CADENA_EN_BLANCO + usrDescarga + ConstantesWeb.CADENA_EN_BLANCO + solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		} else if (!listaInconsistenciasDevueltasMallaService.isEmpty() && !listaPrestacionesAprobadas.isEmpty() && listaPrestacionesAuditoria.isEmpty() ) {
			mostrarPopupDevolucionOPS = true;
			mostrarPopupGenerarOPS = false;
			mostrarPopupSolicitudDevuelta = false;
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_MIX_APROBADAS_DEVUELTAS)
					+ solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_ROJO);
		} else if (listaInconsistenciasAdministrativas.isEmpty() && listaPrestacionesInconsistencias.isEmpty()
				&& listaPrestacionesAuditoria.isEmpty() && listaInconsistenciasDevueltasMallaService.isEmpty()) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SOLICITUD_CREADA)
					+ solicitud.getNumeroSolicitudSOS(), ConstantesWeb.COLOR_AZUL);

			MenuBean menu = (MenuBean) FacesUtils.getManagedBean(ConstantesWeb.MENU_BEAN);
			if (!menu.isUsuarioIps()) {
				mostrarModalReciboCaja = true;
				DatosSolicitudVO solicitudSeleccionada = new DatosSolicitudVO();
				solicitudSeleccionada.setAfiliadoVO(afiliadoVO);
				solicitudSeleccionada.setNumeroSolicitud(solicitud.getConsecutivoSolicitud());
				solicitudSeleccionada.setFechaSolicitud(fechaActual);
				this.mostrarReciboCaja(solicitudSeleccionada);
			} else{
				setMostrarPopupGenerarOPS(true);
			}
		}
		mostrarPopupValidarAccesoDirecto = false;
		mostrarPopupAccesoDirecto = false;
	
	}
	
	/**
	 * 
	 * @return
	 * @throws LogicException 
	 */
	private boolean validarPerfilCoordinador() throws LogicException{
		RegistrarSolicitudController registrarSolicitudesController;
		try {
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (ConnectionProviderException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		} catch (IOException e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return registrarSolicitudesController.validarPerfilCordinador(usrDescarga);
	}
	
	/**
	 * 
	 */
	public void abrirPopupValidarAccesoDirecto(){
		mostrarPopupValidarAccesoDirecto = true;
	}
	
	/**
	 * 
	 */
	public void cerrarPopupValidarAccesoDirecto(){
		mostrarPopupValidarAccesoDirecto = false;
	}

	public List<SelectItem> getListaGeneros() {
		return listaGeneros;
	}

	public void setListaGeneros(List<SelectItem> listaGeneros) {
		this.listaGeneros = listaGeneros;
	}

	public List<SelectItem> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public void setListaTiposIdentificacion(List<SelectItem> listaTiposIdentificacion) {
		this.listaTiposIdentificacion = listaTiposIdentificacion;
	}

	public List<SelectItem> getListaOpciones() {
		return listaOpciones;
	}

	public void setListaOpciones(List<SelectItem> listaOpciones) {
		this.listaOpciones = listaOpciones;
	}

	public List<SelectItem> getListaClasesAtencion() {
		return listaClasesAtencion;
	}

	public void setListaClasesAtencion(List<SelectItem> listaClasesAtencion) {
		this.listaClasesAtencion = listaClasesAtencion;
	}

	public List<SelectItem> getListaOrigenesAtencion() {
		return listaOrigenesAtencion;
	}

	public void setListaOrigenesAtencion(List<SelectItem> listaOrigenesAtencion) {
		this.listaOrigenesAtencion = listaOrigenesAtencion;
	}

	public List<SelectItem> getListaTiposServicio() {
		return listaTiposServicio;
	}

	public void setListaTiposServicio(List<SelectItem> listaTiposServicio) {
		this.listaTiposServicio = listaTiposServicio;
	}

	public List<SelectItem> getListaPrioridadesAtencion() {
		return listaPrioridadesAtencion;
	}

	public void setListaPrioridadesAtencion(List<SelectItem> listaPrioridadesAtencion) {
		this.listaPrioridadesAtencion = listaPrioridadesAtencion;
	}

	public List<SelectItem> getListaTiposDiagnostico() {
		return listaTiposDiagnostico;
	}

	public void setListaTiposDiagnostico(List<SelectItem> listaTiposDiagnostico) {
		this.listaTiposDiagnostico = listaTiposDiagnostico;
	}

	public List<SelectItem> getListaServiciosHospitalizacion() {
		return listaServiciosHospitalizacion;
	}

	public void setListaServiciosHospitalizacion(List<SelectItem> listaServiciosHospitalizacion) {
		this.listaServiciosHospitalizacion = listaServiciosHospitalizacion;
	}

	public List<SelectItem> getListaClasesHabitacion() {
		return listaClasesHabitacion;
	}

	public void setListaClasesHabitacion(List<SelectItem> listaClasesHabitacion) {
		this.listaClasesHabitacion = listaClasesHabitacion;
	}

	public List<SelectItem> getListaDosisMedicamentos() {
		return listaDosisMedicamentos;
	}

	public void setListaDosisMedicamentos(List<SelectItem> listaDosisMedicamentos) {
		this.listaDosisMedicamentos = listaDosisMedicamentos;
	}

	public List<SelectItem> getListaFrecuenciaMedicamentos() {
		return listaFrecuenciaMedicamentos;
	}

	public void setListaFrecuenciaMedicamentos(List<SelectItem> listaFrecuenciaMedicamentos) {
		this.listaFrecuenciaMedicamentos = listaFrecuenciaMedicamentos;
	}

	public List<SelectItem> getListaLateralidadesProcedimientos() {
		return listaLateralidadesProcedimientos;
	}

	public void setListaLateralidadesProcedimientos(List<SelectItem> listaLateralidadesProcedimientos) {
		this.listaLateralidadesProcedimientos = listaLateralidadesProcedimientos;
	}

	public List<SelectItem> getListaViasAccesoMedicamento() {
		return listaViasAccesoMedicamento;
	}

	public void setListaViasAccesoMedicamento(List<SelectItem> listaViasAccesoMedicamento) {
		this.listaViasAccesoMedicamento = listaViasAccesoMedicamento;
	}

	public String getTxtCodigoDiagnostico() {
		return txtCodigoDiagnostico;
	}

	public void setTxtCodigoDiagnostico(String txtCodigoDiagnostico) {
		this.txtCodigoDiagnostico = txtCodigoDiagnostico;
	}

	public String getTxtDescripcionDiagnostico() {
		return txtDescripcionDiagnostico;
	}

	public void setTxtDescripcionDiagnostico(String txtDescripcionDiagnostico) {
		this.txtDescripcionDiagnostico = txtDescripcionDiagnostico;
	}

	public boolean isMostrarPopupDiagnosticos() {
		return mostrarPopupDiagnosticos;
	}

	public void setMostrarPopupDiagnosticos(boolean mostrarPopupDiagnosticos) {
		this.mostrarPopupDiagnosticos = mostrarPopupDiagnosticos;
	}

	public List<DiagnosticosVO> getListaDiagnosticosEncontrados() {
		return listaDiagnosticosEncontrados;
	}

	public void setListaDiagnosticosEncontrados(List<DiagnosticosVO> listaDiagnosticosEncontrados) {
		this.listaDiagnosticosEncontrados = listaDiagnosticosEncontrados;
	}

	public DiagnosticosVO getDiagnosticoVoSeleccionado() {
		return diagnosticoVoSeleccionado;
	}

	public void setDiagnosticoVoSeleccionado(DiagnosticosVO diagnosticoVoSeleccionado) {
		this.diagnosticoVoSeleccionado = diagnosticoVoSeleccionado;
	}

	public Integer getConsecutivoTipoDiagnostico() {
		return consecutivoTipoDiagnostico;
	}

	public void setConsecutivoTipoDiagnostico(Integer consecutivoTipoDiagnostico) {
		this.consecutivoTipoDiagnostico = consecutivoTipoDiagnostico;
	}

	public List<DiagnosticoDTO> getListaDiagnosticosSeleccionados() {
		return listaDiagnosticosSeleccionados;
	}

	public void setListaDiagnosticosSeleccionados(List<DiagnosticoDTO> listaDiagnosticosSeleccionados) {
		this.listaDiagnosticosSeleccionados = listaDiagnosticosSeleccionados;
	}

	public List<TiposDiagnosticosVO> getListaTipoDiagnosticoVO() {
		return listaTipoDiagnosticoVO;
	}

	public void setListaTipoDiagnosticoVO(List<TiposDiagnosticosVO> listaTipoDiagnosticoVO) {
		this.listaTipoDiagnosticoVO = listaTipoDiagnosticoVO;
	}

	public DiagnosticoDTO getDiagnosticoDTOSeleccionado() {
		return diagnosticoDTOSeleccionado;
	}

	public void setDiagnosticoDTOSeleccionado(DiagnosticoDTO diagnosticoDTOSeleccionado) {
		this.diagnosticoDTOSeleccionado = diagnosticoDTOSeleccionado;
	}

	public String getTxtContingencia() {
		return txtContingencia;
	}

	public void setTxtContingencia(String txtContingencia) {
		this.txtContingencia = txtContingencia;
	}

	public List<SelectItem> getListaContingenciaRecobros() {
		return listaContingenciaRecobros;
	}

	public void setListaContingenciaRecobros(List<SelectItem> listaContingenciaRecobros) {
		this.listaContingenciaRecobros = listaContingenciaRecobros;
	}

	public Integer getConsecutivoContingenciaRecobro() {
		return consecutivoContingenciaRecobro;
	}

	public void setConsecutivoContingenciaRecobro(Integer consecutivoContingenciaRecobro) {
		this.consecutivoContingenciaRecobro = consecutivoContingenciaRecobro;
	}

	public MedicoVO getMedicoVO() {
		return medicoVO;
	}

	public void setMedicoVO(MedicoVO medicoVO) {
		this.medicoVO = medicoVO;
	}

	/**
	 * @return the listaMedicosEncontrados
	 */
	public List<MedicoVO> getListaMedicosEncontrados() {
		return listaMedicosEncontrados;
	}

	/**
	 * @param listaMedicosEncontrados
	 *            the listaMedicosEncontrados to set
	 */
	public void setListaMedicosEncontrados(List<MedicoVO> listaMedicosEncontrados) {
		this.listaMedicosEncontrados = listaMedicosEncontrados;
	}

	public boolean isMostrarPopupMedicos() {
		return mostrarPopupMedicos;
	}

	public void setMostrarPopupMedicos(boolean mostrarPopupMedicos) {
		this.mostrarPopupMedicos = mostrarPopupMedicos;
	}

	public Boolean getMedicoAdscrito() {
		return medicoAdscrito;
	}

	public void setMedicoAdscrito(Boolean medicoAdscrito) {
		this.medicoAdscrito = medicoAdscrito;
	}

	public boolean isMostrarPopupRegistrarMedicoNoAds() {
		return mostrarPopupRegistrarMedicoNoAds;
	}

	public void setMostrarPopupRegistrarMedicoNoAds(boolean mostrarPopupRegistrarMedicoNoAds) {
		this.mostrarPopupRegistrarMedicoNoAds = mostrarPopupRegistrarMedicoNoAds;
	}

	public List<SelectItem> getListaEspecialidadesMedico() {
		return listaEspecialidadesMedico;
	}

	public void setListaEspecialidadesMedico(List<SelectItem> listaEspecialidadesMedico) {
		this.listaEspecialidadesMedico = listaEspecialidadesMedico;
	}

	public String getTxtConfirmarRegistroMedico() {
		return txtConfirmarRegistroMedico;
	}

	public void setTxtConfirmarRegistroMedico(String txtConfirmarRegistroMedico) {
		this.txtConfirmarRegistroMedico = txtConfirmarRegistroMedico;
	}

	public List<IPSVO> getListaIPSVO() {
		return listaIPSVO;
	}

	public void setListaIPSVO(List<IPSVO> listaIPSVO) {
		this.listaIPSVO = listaIPSVO;
	}

	public boolean isMostrarPopupPrestadores() {
		return mostrarPopupPrestadores;
	}

	public void setMostrarPopupPrestadores(boolean mostrarPopupPrestadores) {
		this.mostrarPopupPrestadores = mostrarPopupPrestadores;
	}

	public IPSVO getIpsVO() {
		return ipsVO;
	}

	public void setIpsVO(IPSVO ipsVO) {
		this.ipsVO = ipsVO;
	}

	public Boolean getPrestadorAdscrito() {
		return prestadorAdscrito;
	}

	public void setPrestadorAdscrito(Boolean prestadorAdscrito) {
		this.prestadorAdscrito = prestadorAdscrito;
	}

	public boolean isMostrarPopupRegistrarPrestador() {
		return mostrarPopupRegistrarPrestador;
	}

	public void setMostrarPopupRegistrarPrestador(boolean mostrarPopupRegistrarPrestador) {
		this.mostrarPopupRegistrarPrestador = mostrarPopupRegistrarPrestador;
	}

	public boolean isMostrarPopupConfirmarRegistrarMedico() {
		return mostrarPopupConfirmarRegistrarMedico;
	}

	public void setMostrarPopupConfirmarRegistrarMedico(boolean mostrarPopupConfirmarRegistrarMedico) {
		this.mostrarPopupConfirmarRegistrarMedico = mostrarPopupConfirmarRegistrarMedico;
	}

	public boolean isMostrarPopupConfirmarRegistrarPrestador() {
		return mostrarPopupConfirmarRegistrarPrestador;
	}

	public void setMostrarPopupConfirmarRegistrarPrestador(boolean mostrarPopupConfirmarRegistrarPrestador) {
		this.mostrarPopupConfirmarRegistrarPrestador = mostrarPopupConfirmarRegistrarPrestador;
	}

	public List<SelectItem> getListaTiposIps() {
		return listaTiposIps;
	}

	public void setListaTiposIps(List<SelectItem> listaTiposIps) {
		this.listaTiposIps = listaTiposIps;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public List<CiudadVO> getListaCiudadesVO() {
		return listaCiudadesVO;
	}

	public void setListaCiudadesVO(List<CiudadVO> listaCiudadesVO) {
		this.listaCiudadesVO = listaCiudadesVO;
	}

	public CiudadVO getCiudadVO() {
		return ciudadVO;
	}

	public void setCiudadVO(CiudadVO ciudadVO) {
		this.ciudadVO = ciudadVO;
	}

	public String getTxtCodigoCiudad() {
		return txtCodigoCiudad;
	}

	public void setTxtCodigoCiudad(String txtCodigoCiudad) {
		this.txtCodigoCiudad = txtCodigoCiudad;
	}

	public String getTxtDescripcionCiudad() {
		return txtDescripcionCiudad;
	}

	public void setTxtDescripcionCiudad(String txtDescripcionCiudad) {
		this.txtDescripcionCiudad = txtDescripcionCiudad;
	}

	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	public boolean isMostrarPanelMedicamentos() {
		return mostrarPanelMedicamentos;
	}

	public void setMostrarPanelMedicamentos(boolean mostrarPanelMedicamentos) {
		this.mostrarPanelMedicamentos = mostrarPanelMedicamentos;
	}

	public boolean isMostrarPanelCUPS() {
		return mostrarPanelCUPS;
	}

	public void setMostrarPanelCUPS(boolean mostrarPanelCUPS) {
		this.mostrarPanelCUPS = mostrarPanelCUPS;
	}

	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	public MedicamentosVO getMedicamentoVO() {
		return medicamentoVO;
	}

	public void setMedicamentoVO(MedicamentosVO medicamentoVO) {
		this.medicamentoVO = medicamentoVO;
	}

	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	public void setListaMedicamentosEncontrados(List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	public boolean isMostrarPopupConfirmarRegistrarMedicamento() {
		return mostrarPopupConfirmarRegistrarMedicamento;
	}

	public void setMostrarPopupConfirmarRegistrarMedicamento(boolean mostrarPopupConfirmarRegistrarMedicamento) {
		this.mostrarPopupConfirmarRegistrarMedicamento = mostrarPopupConfirmarRegistrarMedicamento;
	}

	public boolean isMostrarPopupConfirmarRegistrarCUP() {
		return mostrarPopupConfirmarRegistrarCUP;
	}

	public void setMostrarPopupConfirmarRegistrarCUP(boolean mostrarPopupConfirmarRegistrarCUP) {
		this.mostrarPopupConfirmarRegistrarCUP = mostrarPopupConfirmarRegistrarCUP;
	}

	public List<ProcedimientosVO> getListaCUPSEncontrados() {
		return listaCUPSEncontrados;
	}

	public void setListaCUPSEncontrados(List<ProcedimientosVO> listaCUPSEncontrados) {
		this.listaCUPSEncontrados = listaCUPSEncontrados;
	}

	public ProcedimientosVO getProcedimientosVO() {
		return procedimientosVO;
	}

	public void setProcedimientosVO(ProcedimientosVO procedimientosVO) {
		this.procedimientosVO = procedimientosVO;
	}

	public List<SelectItem> getListaLateralidades() {
		return listaLateralidades;
	}

	public void setListaLateralidades(List<SelectItem> listaLateralidades) {
		this.listaLateralidades = listaLateralidades;
	}

	public boolean isMostrarPopupRegistrarMedicamento() {
		return mostrarPopupRegistrarMedicamento;
	}

	public void setMostrarPopupRegistrarMedicamento(boolean mostrarPopupRegistrarMedicamento) {
		this.mostrarPopupRegistrarMedicamento = mostrarPopupRegistrarMedicamento;
	}

	public boolean isMostrarPopupRegistrarCUP() {
		return mostrarPopupRegistrarCUP;
	}

	public void setMostrarPopupRegistrarCUP(boolean mostrarPopupRegistrarCUP) {
		this.mostrarPopupRegistrarCUP = mostrarPopupRegistrarCUP;
	}

	public List<SelectItem> getListaConcentracionesMedicamento() {
		return listaConcentracionesMedicamento;
	}

	public void setListaConcentracionesMedicamento(List<SelectItem> listaConcentracionesMedicamento) {
		this.listaConcentracionesMedicamento = listaConcentracionesMedicamento;
	}

	public List<SelectItem> getListaPresentacionesMedicamento() {
		return listaPresentacionesMedicamento;
	}

	public void setListaPresentacionesMedicamento(List<SelectItem> listaPresentacionesMedicamento) {
		this.listaPresentacionesMedicamento = listaPresentacionesMedicamento;
	}

	/**
	 * @return the listaPrestacionesSolicitadas
	 */
	public List<PrestacionDTO> getListaPrestacionesSolicitadas() {
		return listaPrestacionesSolicitadas;
	}

	/**
	 * @param listaPrestacionesSolicitadas
	 *            the listaPrestacionesSolicitadas to set
	 */
	public void setListaPrestacionesSolicitadas(List<PrestacionDTO> listaPrestacionesSolicitadas) {
		this.listaPrestacionesSolicitadas = listaPrestacionesSolicitadas;
	}

	public PrestacionDTO getPrestacionSeleccionada() {
		return prestacionSeleccionada;
	}

	public void setPrestacionSeleccionada(PrestacionDTO prestacionSeleccionada) {
		this.prestacionSeleccionada = prestacionSeleccionada;
	}

	/**
	 * @return the recienNacido
	 */
	public Integer getRecienNacido() {
		return recienNacido;
	}

	/**
	 * @param recienNacido
	 *            the recienNacido to set
	 */
	public void setRecienNacido(Integer recienNacido) {
		this.recienNacido = recienNacido;
	}

	/**
	 * @return the mostrarPopupHospitalizacion
	 */
	public boolean isMostrarPopupHospitalizacion() {
		return mostrarPopupHospitalizacion;
	}

	/**
	 * @param mostrarPopupHospitalizacion
	 *            the mostrarPopupHospitalizacion to set
	 */
	public void setMostrarPopupHospitalizacion(boolean mostrarPopupHospitalizacion) {
		this.mostrarPopupHospitalizacion = mostrarPopupHospitalizacion;
	}

	/**
	 * @return the esRecienNacido
	 */
	public boolean isEsRecienNacido() {
		return esRecienNacido;
	}

	/**
	 * @param esRecienNacido
	 *            the esRecienNacido to set
	 */
	public void setEsRecienNacido(boolean esRecienNacido) {
		this.esRecienNacido = esRecienNacido;
	}

	public boolean isDeshabilitarBtnHospitalizacion() {
		return deshabilitarBtnHospitalizacion;
	}

	public void setDeshabilitarBtnHospitalizacion(boolean deshabilitarBtnHospitalizacion) {
		this.deshabilitarBtnHospitalizacion = deshabilitarBtnHospitalizacion;
	}

	public List<DocumentoSoporteVO> getListaDocumentoSoporteVOs() {
		return listaDocumentoSoporteVOs;
	}

	public void setListaDocumentoSoporteVOs(List<DocumentoSoporteVO> listaDocumentoSoporteVOs) {
		this.listaDocumentoSoporteVOs = listaDocumentoSoporteVOs;
	}

	public DocumentoSoporteVO getDocumentoSoporteSeleccionado() {
		return documentoSoporteSeleccionado;
	}

	public void setDocumentoSoporteSeleccionado(DocumentoSoporteVO documentoSoporteSeleccionado) {
		this.documentoSoporteSeleccionado = documentoSoporteSeleccionado;
	}

	public SoporteVO getSoporteSeleccionado() {
		return soporteSeleccionado;
	}

	public void setSoporteSeleccionado(SoporteVO soporteSeleccionado) {
		this.soporteSeleccionado = soporteSeleccionado;
	}

	public List<SoporteVO> getListaSoporteVOs() {
		return listaSoporteVOs;
	}

	public void setListaSoporteVOs(List<SoporteVO> listaSoporteVOs) {
		this.listaSoporteVOs = listaSoporteVOs;
	}

	public byte[] getImgData() {
		return imgData;
	}

	public void setImgData(byte[] imgData) {
		this.imgData = imgData;
	}

	public String getTxtObservaciones() {
		return txtObservaciones;
	}

	public void setTxtObservaciones(String txtObservaciones) {
		this.txtObservaciones = txtObservaciones;
	}

	public RecienNacidoVO getRecienNacidoVO() {
		return recienNacidoVO;
	}

	public void setRecienNacidoVO(RecienNacidoVO recienNacidoVO) {
		this.recienNacidoVO = recienNacidoVO;
	}

	public Integer getPartoMultiple() {
		return partoMultiple;
	}

	public void setPartoMultiple(Integer partoMultiple) {
		this.partoMultiple = partoMultiple;
	}

	public AtencionVO getAtencionVO() {
		return atencionVO;
	}

	public void setAtencionVO(AtencionVO atencionVO) {
		this.atencionVO = atencionVO;
	}

	public Integer getDomi() {
		return domi;
	}

	public void setDomi(Integer domi) {
		this.domi = domi;
	}

	public boolean isSuperaTopeSoat() {
		return superaTopeSoat;
	}

	public void setSuperaTopeSoat(boolean superaTopeSoat) {
		this.superaTopeSoat = superaTopeSoat;
	}

	public HospitalizacionVO getHospitalizacionVO() {
		return hospitalizacionVO;
	}

	public void setHospitalizacionVO(HospitalizacionVO hospitalizacionVO) {
		this.hospitalizacionVO = hospitalizacionVO;
	}

	public Integer getTutela() {
		return tutela;
	}

	public void setTutela(Integer tutela) {
		this.tutela = tutela;
	}

	public boolean isDeshabilitarSelectTipoPrestacion() {
		return deshabilitarSelectTipoPrestacion;
	}

	public void setDeshabilitarSelectTipoPrestacion(boolean deshabilitarSelectTipoPrestacion) {
		this.deshabilitarSelectTipoPrestacion = deshabilitarSelectTipoPrestacion;
	}

	public boolean isDeshabilitarSelectLateralidadCUP() {
		return deshabilitarSelectLateralidadCUP;
	}

	public void setDeshabilitarSelectLateralidadCUP(boolean deshabilitarSelectLateralidadCUP) {
		this.deshabilitarSelectLateralidadCUP = deshabilitarSelectLateralidadCUP;
	}

	public boolean isEsPartoMultiple() {
		return esPartoMultiple;
	}

	public void setEsPartoMultiple(boolean esPartoMultiple) {
		this.esPartoMultiple = esPartoMultiple;
	}

	public int getMaximoCorteHospitalizacion() {
		return maximoCorteHospitalizacion;
	}

	public void setMaximoCorteHospitalizacion(int maximoCorteHospitalizacion) {
		this.maximoCorteHospitalizacion = maximoCorteHospitalizacion;
	}

	public int getMinimoCorteHospitalizacion() {
		return minimoCorteHospitalizacion;
	}

	public void setMinimoCorteHospitalizacion(int minimoCorteHospitalizacion) {
		this.minimoCorteHospitalizacion = minimoCorteHospitalizacion;
	}

	public boolean isMostrarPopupCorteCuenta() {
		return mostrarPopupCorteCuenta;
	}

	public void setMostrarPopupCorteCuenta(boolean mostrarPopupCorteCuenta) {
		this.mostrarPopupCorteCuenta = mostrarPopupCorteCuenta;
	}

	public boolean isMostrarPopupDocumentosSoporte() {
		return mostrarPopupDocumentosSoporte;
	}

	public void setMostrarPopupDocumentosSoporte(boolean mostrarPopupDocumentosSoporte) {
		this.mostrarPopupDocumentosSoporte = mostrarPopupDocumentosSoporte;
	}

	public boolean isMostrarPopupConfirmacionBorrarArchivo() {
		return mostrarPopupConfirmacionBorrarArchivo;
	}

	public void setMostrarPopupConfirmacionBorrarArchivo(boolean mostrarPopupConfirmacionBorrarArchivo) {
		this.mostrarPopupConfirmacionBorrarArchivo = mostrarPopupConfirmacionBorrarArchivo;
	}

	public boolean isMostrarPopupConfirmacionDocumentos() {
		return mostrarPopupConfirmacionDocumentos;
	}

	public void setMostrarPopupConfirmacionDocumentos(boolean mostrarPopupConfirmacionDocumentos) {
		this.mostrarPopupConfirmacionDocumentos = mostrarPopupConfirmacionDocumentos;
	}

	public boolean isDeshabilitarBtnDocumentos() {
		return deshabilitarBtnDocumentos;
	}

	public void setDeshabilitarBtnDocumentos(boolean deshabilitarBtnDocumentos) {
		this.deshabilitarBtnDocumentos = deshabilitarBtnDocumentos;
	}

	public boolean isDeshabilitarCamposMedico() {
		return deshabilitarCamposMedico;
	}

	public void setDeshabilitarCamposMedico(boolean deshabilitarCamposMedico) {
		this.deshabilitarCamposMedico = deshabilitarCamposMedico;
	}

	public List<InconsistenciasResponseServiceVO> getListaInconsistenciasDevueltasMallaService() {
		return listaInconsistenciasDevueltasMallaService;
	}

	public void setListaInconsistenciasDevueltasMallaService(List<InconsistenciasResponseServiceVO> listaInconsistenciasDevueltasMallaService) {
		this.listaInconsistenciasDevueltasMallaService = listaInconsistenciasDevueltasMallaService;
	}

	public boolean isMostrarPopupSolicitudDevuelta() {
		return mostrarPopupSolicitudDevuelta;
	}

	public void setMostrarPopupSolicitudDevuelta(boolean mostrarPopupSolicitudDevuelta) {
		this.mostrarPopupSolicitudDevuelta = mostrarPopupSolicitudDevuelta;
	}

	public InconsistenciasVO getInconsistenciasVO() {
		return inconsistenciasVO;
	}

	public void setInconsistenciasVO(InconsistenciasVO inconsistenciasVO) {
		this.inconsistenciasVO = inconsistenciasVO;
	}

	public boolean isEsModificacionPrestacion() {
		return esModificacionPrestacion;
	}

	public void setEsModificacionPrestacion(boolean esModificacionPrestacion) {
		this.esModificacionPrestacion = esModificacionPrestacion;
	}

	public boolean isDeshabilitarRecobro() {
		return deshabilitarRecobro;
	}

	public void setDeshabilitarRecobro(boolean deshabilitarRecobro) {
		this.deshabilitarRecobro = deshabilitarRecobro;
	}

	public boolean isDeshabilitarBtnGuardar() {
		return deshabilitarBtnGuardar;
	}

	public void setDeshabilitarBtnGuardar(boolean deshabilitarBtnGuardar) {
		this.deshabilitarBtnGuardar = deshabilitarBtnGuardar;
	}

	/**
	 * @return the listaInconsistenciasAdministrativas
	 */
	public List<InconsistenciasResponseServiceVO> getListaInconsistenciasAdministrativas() {
		return listaInconsistenciasAdministrativas;
	}

	/**
	 * @param listaInconsistenciasAdministrativas
	 *            the listaInconsistenciasAdministrativas to set
	 */
	public void setListaInconsistenciasAdministrativas(List<InconsistenciasResponseServiceVO> listaInconsistenciasAdministrativas) {
		this.listaInconsistenciasAdministrativas = listaInconsistenciasAdministrativas;
	}

	/**
	 * @return the listaPrestacionesInconsistenciasMallaService
	 */
	public List<PrestacionDTO> getListaPrestacionesInconsistenciasMallaService() {
		return listaPrestacionesInconsistenciasMallaService;
	}

	/**
	 * @param listaPrestacionesInconsistenciasMallaService
	 *            the listaPrestacionesInconsistenciasMallaService to set
	 */
	public void setListaPrestacionesInconsistenciasMallaService(List<PrestacionDTO> listaPrestacionesInconsistenciasMallaService) {
		this.listaPrestacionesInconsistenciasMallaService = listaPrestacionesInconsistenciasMallaService;
	}

	/**
	 * @return the mostrarPopupEjecutarBPM
	 */
	public boolean isMostrarPopupEjecutarBPM() {
		return mostrarPopupEjecutarBPM;
	}

	/**
	 * @param mostrarPopupEjecutarBPM
	 *            the mostrarPopupEjecutarBPM to set
	 */
	public void setMostrarPopupEjecutarBPM(boolean mostrarPopupEjecutarBPM) {
		this.mostrarPopupEjecutarBPM = mostrarPopupEjecutarBPM;
	}

	/**
	 * @return the numeroSolicitudSOS
	 */
	public String getNumeroSolicitudSOS() {
		return numeroSolicitudSOS;
	}

	/**
	 * @param numeroSolicitudSOS
	 *            the numeroSolicitudSOS to set
	 */
	public void setNumeroSolicitudSOS(String numeroSolicitudSOS) {
		this.numeroSolicitudSOS = numeroSolicitudSOS;
	}

	public boolean isMostrarPopupGenerarOPS() {
		return mostrarPopupGenerarOPS;
	}

	public void setMostrarPopupGenerarOPS(boolean mostrarPopupGenerarOPS) {
		this.mostrarPopupGenerarOPS = mostrarPopupGenerarOPS;
	}

	/**
	 * @return the generarConObservaciones
	 */
	public boolean isGenerarConObservaciones() {
		return generarConObservaciones;
	}

	/**
	 * @param generarConObservaciones
	 *            the generarConObservaciones to set
	 */
	public void setGenerarConObservaciones(boolean generarConObservaciones) {
		this.generarConObservaciones = generarConObservaciones;
	}

	/**
	 * @return the mostrarPopupDevolucionOPS
	 */
	public boolean isMostrarPopupDevolucionOPS() {
		return mostrarPopupDevolucionOPS;
	}

	/**
	 * @param mostrarPopupDevolucionOPS the mostrarPopupDevolucionOPS to set
	 */
	public void setMostrarPopupDevolucionOPS(boolean mostrarPopupDevolucionOPS) {
		this.mostrarPopupDevolucionOPS = mostrarPopupDevolucionOPS;
	}

	public int getMarcaTranscribir() {
		return marcaTranscribir;
	}

	public void setMarcaTranscribir(int marcaTranscribir) {
		this.marcaTranscribir = marcaTranscribir;
	}

	public String getIdTask() {
		return idTask;
	}

	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public SolicitudVO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudVO solicitud) {
		this.solicitud = solicitud;
	}

	public boolean isDeshabilitarBtnTerminar() {
		return deshabilitarBtnTerminar;
	}

	public void setDeshabilitarBtnTerminar(boolean deshabilitarBtnTerminar) {
		this.deshabilitarBtnTerminar = deshabilitarBtnTerminar;
	}

	public boolean isDeshabilitarBtnLeberar() {
		return deshabilitarBtnLeberar;
	}

	public void setDeshabilitarBtnLeberar(boolean deshabilitarBtnLeberar) {
		this.deshabilitarBtnLeberar = deshabilitarBtnLeberar;
	}

	public boolean isDeshabilitarBtnDevolver() {
		return deshabilitarBtnDevolver;
	}

	public void setDeshabilitarBtnDevolver(boolean deshabilitarBtnDevolver) {
		this.deshabilitarBtnDevolver = deshabilitarBtnDevolver;
	}

	/**IPS**/
	public boolean isVerIps() {
		return verIps;
	}

	public void setVerIps(boolean verIps) {
		this.verIps = verIps;
	}

	public List<PrestacionDTO> getListaPrestacionesAuditoria() {
		return listaPrestacionesAuditoria;
	}

	public void setListaPrestacionesAuditoria(
			List<PrestacionDTO> listaPrestacionesAuditoria) {
		this.listaPrestacionesAuditoria = listaPrestacionesAuditoria;
	}

	public List<PrestacionDTO> getListaPrestacionesInconsistencias() {
		return listaPrestacionesInconsistencias;
	}

	public void setListaPrestacionesInconsistencias(
			List<PrestacionDTO> listaPrestacionesInconsistencias) {
		this.listaPrestacionesInconsistencias = listaPrestacionesInconsistencias;
	}

	public List<PrestacionDTO> getListaPrestacionesAprobadas() {
		return listaPrestacionesAprobadas;
	}

	public void setListaPrestacionesAprobadas(
			List<PrestacionDTO> listaPrestacionesAprobadas) {
		this.listaPrestacionesAprobadas = listaPrestacionesAprobadas;
	}

	public boolean isMostrarPopupAccesoDirecto() {
		return mostrarPopupAccesoDirecto;
	}

	public void setMostrarPopupAccesoDirecto(boolean mostrarPopupAccesoDirecto) {
		this.mostrarPopupAccesoDirecto = mostrarPopupAccesoDirecto;
	}

	public boolean isMostrarPopupValidarAccesoDirecto() {
		return mostrarPopupValidarAccesoDirecto;
	}

	public void setMostrarPopupValidarAccesoDirecto(
			boolean mostrarPopupValidarAccesoDirecto) {
		this.mostrarPopupValidarAccesoDirecto = mostrarPopupValidarAccesoDirecto;
	}

	public String getUsrDescarga() {
		return usrDescarga;
	}

	public void setUsrDescarga(String usrDescarga) {
		this.usrDescarga = usrDescarga;
	}

	public String getUsrDescargaPass() {
		return usrDescargaPass;
	}

	public void setUsrDescargaPass(String usrDescargaPass) {
		this.usrDescargaPass = usrDescargaPass;
	}

	
}
