package com.sos.gestionautorizacionessaludweb.bean;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DenominacionVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaxCausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.TotalCierreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.GestionCajaController;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;

/**
 * Bean para realizar cierre de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class CierreCajaBean implements Serializable {
	private static final long serialVersionUID = 6428340416086026461L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultaAfiliadoBean.class);
	private static final String ERROR_EXCEPCION_INESPERADA = ConstantesWeb.ERROR_EXCEPCION_INESPERADA;

	private Date fechaActual;
	private String userLogin;
	private UsuarioWebVO usuarioWebVO;
	private OficinasVO oficinaVO;
	private MovimientoCajaVO movimientosCajaVO;
	private DenominacionVO denominacionSelecciona;
	private TotalCierreVO totalesCierreVO;
	private InformeCierreCajaVO informeCierreCajaVO;
	private Double totalEfectivoRecaudo;
	private Double totalNotasCredito;
	private Double totalRecaudo;
	private Double totalCaja;
	private Double totalFaltante;
	private Double totalSobrante;
	private BigInteger valorDescuadrePopup;
	private List<String> listErrores;
	private List<SelectItem> listSelectItemCausalesDescuadre;
	private List<CausalDescuadreVO> listCausalesDescuadre;
	private List<CausalDescuadreVO> listCausalesDescuadreConValor;
	private List<CausalDescuadreVO> listCausalesDescuadreConValorReporte;
	private CausalDescuadreVO causalDescuadreConValorEliminar;
	private Integer consecutivoCausalDescuadreSeleccionada;
	private List<DenominacionVO> listBilletes;
	private List<DenominacionVO> listMonedas;
	private boolean mostrarModalError;
	private boolean mostrarCausalesDescuadre;
	private boolean mostrarTablaCausalesCierre;
	private Date fechaApertura;
	private boolean mostrarBotonesCierre;
	private boolean recargaPagina = false;

	public Date getFechaApertura() {
		return fechaApertura;
	}

	/**
	 * Mostrar modal error
	 * 
	 * @param errores
	 */
	private void mostrarModalError(List<String> errores) {
		if (!errores.isEmpty()) {
			listErrores = errores;
			mostrarModalError = true;
		}
	}

	/**
	 * Llena los inputtext de cuadre de caja faltante y cuadre de caja sobrante
	 */
	private void llenarFaltanteSobrante() {
		double cuadreCajaValue = totalEfectivoRecaudo - totalCaja;
		if (cuadreCajaValue < 0) {
			totalFaltante = cuadreCajaValue;
			totalSobrante = 0.0;
		} else if (cuadreCajaValue > 0) {
			totalFaltante = 0.0;
			totalSobrante = cuadreCajaValue;
		} else {
			totalFaltante = 0.0;
			totalSobrante = 0.0;
		}
	}

	/**
	 * Buscar la causal seleccionada
	 * 
	 * @param consecutivoCausal
	 * @return
	 */
	private CausalDescuadreVO buscarCausalSeleccionada(Integer consecutivoCausal) {
		for (CausalDescuadreVO causalDescuadreVO : listCausalesDescuadre) {
			if (causalDescuadreVO.getCnsctvoCdgoCslDscdre().intValue() == consecutivoCausal.intValue()) {
				return causalDescuadreVO;
			}
		}
		return null;
	}

	/**
	 * Verifica si añadio ya la causal a la lista de causales
	 * 
	 * @param causalDescuadreVO
	 * @return
	 */
	private boolean existeCausalAnadida(CausalDescuadreVO causalDescuadreVO) {
		for (CausalDescuadreVO causalDescuadreVOTemp : listCausalesDescuadreConValor) {
			if (causalDescuadreVO.getCnsctvoCdgoCslDscdre().intValue() == causalDescuadreVOTemp
					.getCnsctvoCdgoCslDscdre().intValue()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Calcula el total de todas las causales
	 * 
	 * @return
	 */
	private Double sumarCausales() {
		double total = 0.0;
		for (CausalDescuadreVO causalDescuadreVO : listCausalesDescuadreConValor) {
			total += causalDescuadreVO.getValorPopup().doubleValue();
		}
		return total;
	}

	/**
	 * Devuelve las causales por movimiento caja
	 * 
	 * @return
	 */
	private List<MovimientoCajaxCausalDescuadreVO> obtenerMovimientoCajaxCausalDescuadre() {
		List<MovimientoCajaxCausalDescuadreVO> retorno = null;
		if (!Utilidades.validarNullCadena(listCausalesDescuadreConValor).equals(ConstantesWeb.CADENA_VACIA)
				&& !listCausalesDescuadreConValor.isEmpty()) {
			retorno = new ArrayList<MovimientoCajaxCausalDescuadreVO>();
			for (CausalDescuadreVO causalDescuadreVO : listCausalesDescuadreConValor) {
				MovimientoCajaxCausalDescuadreVO movimientoCajaxCausalDescuadreVO = new MovimientoCajaxCausalDescuadreVO();
				movimientoCajaxCausalDescuadreVO.setCnsctvoMvmntoCja(movimientosCajaVO.getCnsctvoMvmntoCja());
				movimientoCajaxCausalDescuadreVO.setCnsctvoCdgoCslCscdre(causalDescuadreVO.getCnsctvoCdgoCslDscdre());
				movimientoCajaxCausalDescuadreVO.setVlrDscdre(causalDescuadreVO.getValorPopup());
				movimientoCajaxCausalDescuadreVO.setUsroCrcn(userLogin);
				retorno.add(movimientoCajaxCausalDescuadreVO);
			}
		}
		return retorno;
	}

	private void limpiarCampos() {
		listCausalesDescuadreConValor = new ArrayList<CausalDescuadreVO>();
		causalDescuadreConValorEliminar = new CausalDescuadreVO();
		mostrarCausalesDescuadre = false;
		totalEfectivoRecaudo = 0.0;
		totalNotasCredito = 0.0;
		totalRecaudo = 0.0;
		totalCaja = 0.0;
		totalFaltante = 0.0;
		totalSobrante = 0.0;
		valorDescuadrePopup = new BigInteger("0");
		denominacionSelecciona = new DenominacionVO();
		fechaActual = new Date();
		userLogin = FacesUtils.getUserName();
	}

	private void mostrarMensajeCierreCaja(Exception e) {
		LOG.error(e.getMessage(), e);
		List<String> errorTemp = new ArrayList<String>();
		errorTemp.add(ERROR_EXCEPCION_INESPERADA);
		mostrarModalError(errorTemp);
	}

	private void mostrarMensajeCierreCajaPantalla(Exception e) {
		LOG.error(e.getMessage(), e);
		List<String> errorTemp = new ArrayList<String>();
		errorTemp.add(ERROR_EXCEPCION_INESPERADA);
		mostrarModalError(errorTemp);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				ConstantesWeb.ERROR_EXCEPCION_INESPERADA, ConstantesWeb.ERROR_EXCEPCION_INESPERADA));
	}

	/**
	 * Limpia la informacion del formulario
	 */
	private void limpiarGuardadoInterno() {
		this.limpiarCampos();
		movimientosCajaVO = null;
		try {
			GestionCajaController cajaController = new GestionCajaController();
			usuarioWebVO = cajaController.consultarUsuarioWeb(userLogin, ConstantesWeb.MODULO_CAJA);
			listBilletes = cajaController.consultarDenominaciones(ConstantesWeb.DENOMINACION_TIPO_BILLETE);
			listMonedas = cajaController.consultarDenominaciones(ConstantesWeb.DENOMINACION_TIPO_MONEDA);
		} catch (DataAccessException e) {
			this.mostrarMensajeCierreCaja(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeCierreCaja(e);
		}
	}

	/**
	 * Guarda el cierre en la base de datos
	 */
	private void guardarCierreBaseDatos() {
		try {
			GestionCajaController cajaController = new GestionCajaController();
			MovimientoCajaVO movimientoCajaModificar = new MovimientoCajaVO();
			movimientoCajaModificar.setCnsctvoMvmntoCja(movimientosCajaVO.getCnsctvoMvmntoCja());
			movimientoCajaModificar.setUsroUltmaMdfccn(userLogin);
			movimientoCajaModificar.setTtlEfctvo(totalEfectivoRecaudo);
			movimientoCajaModificar.setTtlNtaCrdto(totalNotasCredito);
			movimientoCajaModificar.setTtlCja(totalCaja);
			movimientoCajaModificar.setSbrnteCrre(totalSobrante);
			movimientoCajaModificar.setFltnteCrre(totalFaltante * -1);

			cajaController.cerrarCaja(movimientoCajaModificar, obtenerMovimientoCajaxCausalDescuadre());

			List<CausalDescuadreVO> clone = new ArrayList<CausalDescuadreVO>(listCausalesDescuadreConValor);
			listCausalesDescuadreConValorReporte = clone;

			InformeCierreCajaVO informeCierreCaja = new InformeCierreCajaVO();
			informeCierreCaja.setBase(movimientosCajaVO.getBse());
			informeCierreCaja.setFaltante(totalFaltante);
			informeCierreCaja.setFecha(fechaActual);
			informeCierreCaja.setListBilletes(listBilletes);
			informeCierreCaja.setListCausalesDescuadre(listCausalesDescuadreConValorReporte);
			informeCierreCaja.setListMonedas(listMonedas);
			informeCierreCaja.setOficinaNombre(oficinaVO.getDescripcionOficina());
			informeCierreCaja.setResponsable(usuarioWebVO.getNombreCompleto());
			informeCierreCaja.setSobrante(totalSobrante);
			informeCierreCaja.setTotalCaja(totalCaja);
			informeCierreCaja.setTotalEfectivoRecaudado(totalEfectivoRecaudo);
			informeCierreCaja.setTotalNotasCredito(totalNotasCredito);
			informeCierreCaja.setTotalRecaudado(totalRecaudo);
			informeCierreCaja.setUserLogin(userLogin);
			informeCierreCaja.setFechaApertura(movimientosCajaVO.getFchaAprtra());

			this.informeCierreCajaVO = informeCierreCaja;

			limpiarGuardadoInterno();
			mostrarTablaCausalesCierre = true;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					ConstantesWeb.INFO_CERRAR_EXITO, ConstantesWeb.INFO_CERRAR_EXITO));
			mostrarBotonesCierre = true;

		} catch (LogicException e) {
			this.mostrarMensajeCierreCaja(e);
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeCierreCaja(e);
		}
	}

	/**
	 * Post Construct
	 */
	@PostConstruct
	public void init() {
		listCausalesDescuadreConValorReporte = new ArrayList<CausalDescuadreVO>();
		mostrarBotonesCierre = false;
		this.limpiarCampos();
		try {
			GestionCajaController cajaController = new GestionCajaController();
			CargarCombosController cargarCombosController = new CargarCombosController();
			usuarioWebVO = cajaController.consultarUsuarioWeb(userLogin, ConstantesWeb.MODULO_CAJA);
			List<MovimientoCajaVO> listMovimientoApertura = cajaController.consultarMovimientoCajaAbierta(userLogin);
			movimientosCajaVO = listMovimientoApertura.isEmpty() ? null : listMovimientoApertura.get(0);
			if (movimientosCajaVO != null) {
				fechaApertura = movimientosCajaVO.getFchaAprtra();
				totalesCierreVO = cajaController.obtenerTotalesCierre(movimientosCajaVO.getCnsctvoMvmntoCja());
				totalNotasCredito = totalesCierreVO.getSumNotaCredito();
				totalCaja = totalesCierreVO.getSumReciboCaja() + movimientosCajaVO.getBse() - totalNotasCredito;
				totalRecaudo = totalNotasCredito;
				listBilletes = cajaController.consultarDenominaciones(ConstantesWeb.DENOMINACION_TIPO_BILLETE);
				listMonedas = cajaController.consultarDenominaciones(ConstantesWeb.DENOMINACION_TIPO_MONEDA);
				listCausalesDescuadre = cargarCombosController.consultaCausalesDescuadre();
				listSelectItemCausalesDescuadre = CargarCombosUtil.obtenerListaSelectCausalesDescuadre(null,
						cargarCombosController);
				oficinaVO = cajaController.consultarOficinaPorConsecutivo(movimientosCajaVO.getCnsctvoCdgoOfcna());
				calcularEfectivoRecaudo();
				llenarFaltanteSobrante();
			} else {
				totalesCierreVO = new TotalCierreVO();
				totalesCierreVO.setSumNotaCredito(0.0);
				totalesCierreVO.setSumReciboCaja(0.0);

				totalNotasCredito = 0.0;
				totalCaja = 0.0;
				totalRecaudo = 0.0;
				listBilletes = new ArrayList<DenominacionVO>();
				listMonedas = new ArrayList<DenominacionVO>();
				listCausalesDescuadre = new ArrayList<CausalDescuadreVO>();
				listSelectItemCausalesDescuadre = new ArrayList<SelectItem>();
				oficinaVO = new OficinasVO();
			}
			recargaPagina = true;
		} catch (ConnectionProviderException e) {
			this.mostrarMensajeCierreCajaPantalla(e);
		} catch (DataAccessException e) {
			this.mostrarMensajeCierreCajaPantalla(e);
		} catch (LogicException e) {
			this.mostrarMensajeCierreCajaPantalla(e);
		} catch (IOException e) {
			this.mostrarMensajeCierreCajaPantalla(e);
		}
	}

	public void reloadPage(ActionEvent event) {
		if (recargaPagina) {
			this.init();
		}
	}

	/**
	 * Genera el informe
	 */
	public void generarInformeCierre() {
		if (!Utilidades.validarNullCadena(informeCierreCajaVO).equals(ConstantesWeb.CADENA_VACIA)) {
			GenerarReporteBean generarReporteBean = new GenerarReporteBean();
			try {
				generarReporteBean.generarInformeCierreCaja(informeCierreCajaVO);
			} catch (LogicException e) {
				this.mostrarMensajeCierreCajaPantalla(e);
			}
		}
	}

	/**
	 * Elimina causal de decuadre
	 */
	public void eliminaCausalDescuadre() {
		CausalDescuadreVO causalEliminar = new CausalDescuadreVO();
		for (CausalDescuadreVO causalDescuadreVO : listCausalesDescuadreConValor) {
			if (causalDescuadreVO.getCnsctvoCdgoCslDscdre().intValue() == causalDescuadreConValorEliminar
					.getCnsctvoCdgoCslDscdre().intValue()) {
				causalEliminar = causalDescuadreVO;
				break;
			}
		}
		listCausalesDescuadreConValor.remove(causalEliminar);
	}

	/**
	 * Agrega una causal de descuadre con su valor
	 */
	public void agregarCausalDescuadre() {
		if (valorDescuadrePopup.toString().length() > 8 || valorDescuadrePopup.intValue() < 1) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL, ConstantesWeb.ERROR_VALIDACION_VALOR_CAUSAL));
		} else if (Utilidades.validarNullCadena(consecutivoCausalDescuadreSeleccionada)
				.equals(ConstantesWeb.CADENA_VACIA)) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ConstantesWeb.ERROR_VALIDACION_CAUSAL_SELECCIONADA,
							ConstantesWeb.ERROR_VALIDACION_CAUSAL_SELECCIONADA));
		} else {
			CausalDescuadreVO causalDescuadreVO = buscarCausalSeleccionada(consecutivoCausalDescuadreSeleccionada);

			if (existeCausalAnadida(causalDescuadreVO)) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, ConstantesWeb.ERROR_VALIDACION_CAUSAL_REPETIDA,
								ConstantesWeb.ERROR_VALIDACION_CAUSAL_REPETIDA));
			} else {
				causalDescuadreVO.setValorPopup(valorDescuadrePopup.doubleValue());
				listCausalesDescuadreConValor.add(causalDescuadreVO);
				valorDescuadrePopup = new BigInteger("0");
			}

		}
	}

	/**
	 * Guarde el cierre de caja
	 */
	public void guardarCierre() {
		if (!Utilidades.validarNullCadena(movimientosCajaVO).equals(ConstantesWeb.CADENA_VACIA)) {
			Double cuadreCajaValue = totalEfectivoRecaudo - totalCaja;
			cuadreCajaValue = cuadreCajaValue < 0 ? cuadreCajaValue * -1 : cuadreCajaValue;
			if (sumarCausales().equals(cuadreCajaValue)) {
				guardarCierreBaseDatos();
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						ConstantesWeb.ERROR_VALIDACION_CUADRE_CAJA, ConstantesWeb.ERROR_VALIDACION_CUADRE_CAJA));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					ConstantesWeb.ERROR_NO_EXISTE_CAJA, ConstantesWeb.ERROR_NO_EXISTE_CAJA));
		}
	}

	/**
	 * Valida el cierre de la caja
	 */
	public void validarCierre() {
		if (!Utilidades.validarNullCadena(movimientosCajaVO).equals(ConstantesWeb.CADENA_VACIA)) {
			Double cero = 0.0;
			if (totalFaltante.equals(cero) && totalSobrante.equals(cero)) {
				mostrarCausalesDescuadre = false;
				guardarCierreBaseDatos();
			} else {
				mostrarCausalesDescuadre = true;
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					ConstantesWeb.ERROR_NO_EXISTE_CAJA, ConstantesWeb.ERROR_NO_EXISTE_CAJA));
		}
	}

	/**
	 * Calcula el efectivo recaudo
	 */
	public void calcularEfectivoRecaudo() {
		double total = 0;
		for (DenominacionVO denominacionVO : listBilletes) {
			if (!Utilidades.validarNullCadena(denominacionVO.getCantidad()).equals(ConstantesWeb.CADENA_VACIA)) {
				total += denominacionVO.getCantidad() * denominacionVO.getDenominacion();
			}
		}
		for (DenominacionVO denominacionVO : listMonedas) {
			if (!Utilidades.validarNullCadena(denominacionVO.getCantidad()).equals(ConstantesWeb.CADENA_VACIA)) {
				total += denominacionVO.getCantidad() * denominacionVO.getDenominacion();
			}
		}
		totalEfectivoRecaudo = total;
		totalRecaudo = totalNotasCredito + totalEfectivoRecaudo;
		totalCaja = totalesCierreVO.getSumReciboCaja() + movimientosCajaVO.getBse() - totalNotasCredito;
		if (totalCaja != null && totalCaja.doubleValue() < 0) {
			totalCaja = totalCaja.doubleValue() * -1;
		}
		llenarFaltanteSobrante();
	}

	/**
	 * Generar el informe detallado en PDF
	 * 
	 * @return
	 */
	public String verDetalle() {
		GenerarReporteBean reporte = new GenerarReporteBean();
		try {
			reporte.generarReporteInformeDetalleCierreCaja(fechaActual, userLogin, oficinaVO.getDescripcionOficina(),
					usuarioWebVO.getNombreCompleto(), fechaApertura);
		} catch (LogicException e) {
			this.mostrarMensajeCierreCaja(e);
		}
		return null;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public OficinasVO getOficinaVO() {
		return oficinaVO;
	}

	public void setOficinaVO(OficinasVO oficinaVO) {
		this.oficinaVO = oficinaVO;
	}

	public List<String> getListErrores() {
		return listErrores;
	}

	public void setListErrores(List<String> listErrores) {
		this.listErrores = listErrores;
	}

	public boolean isMostrarModalError() {
		return mostrarModalError;
	}

	public void setMostrarModalError(boolean mostrarModalError) {
		this.mostrarModalError = mostrarModalError;
	}

	public MovimientoCajaVO getMovimientosCajaVO() {
		return movimientosCajaVO;
	}

	public void setMovimientosCajaVO(MovimientoCajaVO movimientosCajaVO) {
		this.movimientosCajaVO = movimientosCajaVO;
	}

	public UsuarioWebVO getUsuarioWebVO() {
		return usuarioWebVO;
	}

	public void setUsuarioWebVO(UsuarioWebVO usuarioWebVO) {
		this.usuarioWebVO = usuarioWebVO;
	}

	public List<DenominacionVO> getListBilletes() {
		return listBilletes;
	}

	public void setListBilletes(List<DenominacionVO> listBilletes) {
		this.listBilletes = listBilletes;
	}

	public List<DenominacionVO> getListMonedas() {
		return listMonedas;
	}

	public void setListMonedas(List<DenominacionVO> listMonedas) {
		this.listMonedas = listMonedas;
	}

	public Double getTotalEfectivoRecaudo() {
		return totalEfectivoRecaudo;
	}

	public void setTotalEfectivoRecaudo(Double totalEfectivoRecaudo) {
		this.totalEfectivoRecaudo = totalEfectivoRecaudo;
	}

	public DenominacionVO getDenominacionSelecciona() {
		return denominacionSelecciona;
	}

	public void setDenominacionSelecciona(DenominacionVO denominacionSelecciona) {
		this.denominacionSelecciona = denominacionSelecciona;
	}

	public Double getTotalNotasCredito() {
		return totalNotasCredito;
	}

	public void setTotalNotasCredito(Double totalNotasCredito) {
		this.totalNotasCredito = totalNotasCredito;
	}

	public Double getTotalRecaudo() {
		return totalRecaudo;
	}

	public void setTotalRecaudo(Double totalRecaudo) {
		this.totalRecaudo = totalRecaudo;
	}

	public Double getTotalCaja() {
		return totalCaja;
	}

	public void setTotalCaja(Double totalCaja) {
		this.totalCaja = totalCaja;
	}

	public Double getTotalFaltante() {
		return totalFaltante;
	}

	public void setTotalFaltante(Double totalFaltante) {
		this.totalFaltante = totalFaltante;
	}

	public Double getTotalSobrante() {
		return totalSobrante;
	}

	public void setTotalSobrante(Double totalSobrante) {
		this.totalSobrante = totalSobrante;
	}

	public boolean isMostrarCausalesDescuadre() {
		return mostrarCausalesDescuadre;
	}

	public void setMostrarCausalesDescuadre(boolean mostrarCausalesDescuadre) {
		this.mostrarCausalesDescuadre = mostrarCausalesDescuadre;
	}

	public List<CausalDescuadreVO> getListCausalesDescuadre() {
		return listCausalesDescuadre;
	}

	public void setListCausalesDescuadre(List<CausalDescuadreVO> listCausalesDescuadre) {
		this.listCausalesDescuadre = listCausalesDescuadre;
	}

	public List<SelectItem> getListSelectItemCausalesDescuadre() {
		return listSelectItemCausalesDescuadre;
	}

	public void setListSelectItemCausalesDescuadre(List<SelectItem> listSelectItemCausalesDescuadre) {
		this.listSelectItemCausalesDescuadre = listSelectItemCausalesDescuadre;
	}

	public BigInteger getValorDescuadrePopup() {
		return valorDescuadrePopup;
	}

	public void setValorDescuadrePopup(BigInteger valorDescuadrePopup) {
		this.valorDescuadrePopup = valorDescuadrePopup;
	}

	public List<CausalDescuadreVO> getListCausalesDescuadreConValor() {
		return listCausalesDescuadreConValor;
	}

	public void setListCausalesDescuadreConValor(List<CausalDescuadreVO> listCausalesDescuadreConValor) {
		this.listCausalesDescuadreConValor = listCausalesDescuadreConValor;
	}

	public CausalDescuadreVO getCausalDescuadreConValorEliminar() {
		return causalDescuadreConValorEliminar;
	}

	public void setCausalDescuadreConValorEliminar(CausalDescuadreVO causalDescuadreConValorEliminar) {
		this.causalDescuadreConValorEliminar = causalDescuadreConValorEliminar;
	}

	public Integer getConsecutivoCausalDescuadreSeleccionada() {
		return consecutivoCausalDescuadreSeleccionada;
	}

	public void setConsecutivoCausalDescuadreSeleccionada(Integer consecutivoCausalDescuadreSeleccionada) {
		this.consecutivoCausalDescuadreSeleccionada = consecutivoCausalDescuadreSeleccionada;
	}

	public boolean isMostrarTablaCausalesCierre() {
		return mostrarTablaCausalesCierre;
	}

	public void setMostrarTablaCausalesCierre(boolean mostrarTablaCausalesCierre) {
		this.mostrarTablaCausalesCierre = mostrarTablaCausalesCierre;
	}

	public List<CausalDescuadreVO> getListCausalesDescuadreConValorReporte() {
		return listCausalesDescuadreConValorReporte;
	}

	public void setListCausalesDescuadreConValorReporte(List<CausalDescuadreVO> listCausalesDescuadreConValorReporte) {
		this.listCausalesDescuadreConValorReporte = listCausalesDescuadreConValorReporte;
	}

	public boolean isMostrarBotonesCierre() {
		return mostrarBotonesCierre;
	}

}