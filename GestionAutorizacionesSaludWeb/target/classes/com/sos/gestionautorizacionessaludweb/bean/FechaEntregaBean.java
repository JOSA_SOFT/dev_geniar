package com.sos.gestionautorizacionessaludweb.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.FechaEntregaSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.FechaEntregaService;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;


/**
 * Class que permite ejecutar el servicio de fecha entrega 
 * @author Ing. Victor Hugo Gil
 * @version 03/08/2016
 */
public class FechaEntregaBean {
	
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(FechaEntregaBean.class);	
	
	public FechaEntregaSolicitudVO ejecutarFechaEntrega(SolicitudVO solicitud) {
		FechaEntregaSolicitudVO fechaEntregaSolicitud = null;

		FechaEntregaService fechaEntregaService = new FechaEntregaService();
		try {
			co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService fechaEntregaServiceExc = fechaEntregaService.crearFechaEntregaService();
			fechaEntregaSolicitud = fechaEntregaService.consultaResultadoFechaEntrega(fechaEntregaService.ejecutarFechaEntregaService(fechaEntregaServiceExc, solicitud, FacesUtils.getUserName()),	null);
		} catch (LogicException logicExcepction) {
			LOG.error(logicExcepction.getMessage(), logicExcepction);
		} catch (co.eps.sos.service.exception.ServiceException e) {
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			try {
				co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService fechaEntregaServiceExc = fechaEntregaService.crearFechaEntregaService();
				fechaEntregaSolicitud = fechaEntregaService.consultaResultadoFechaEntrega(null, fechaEntregaService.consultaFechaEntrega(fechaEntregaServiceExc, solicitud, FacesUtils.getUserName()));
			} catch (co.eps.sos.service.exception.ServiceException ser) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), ConstantesWeb.CADENA_VACIA));
				LOG.error(ser.getMessage(), ser);
			} catch (Exception ex) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), ConstantesWeb.CADENA_VACIA));
				LOG.error(ex.getMessage(), ex);
			}
			LOG.error(e.getMessage(), e);
		}

		return fechaEntregaSolicitud;
	}

}
