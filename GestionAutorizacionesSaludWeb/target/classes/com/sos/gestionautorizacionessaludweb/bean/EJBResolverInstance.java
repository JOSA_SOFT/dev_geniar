package com.sos.gestionautorizacionessaludweb.bean;

import javax.ejb.EJB;

import com.sos.gestionautorizacionessaludejb.bean.IpsEJBLocal;
import com.sos.gestionautorizacionessaludejb.bean.MedicoEJBLocal;

/**
 * Estas clase se encarga de gestionar los EJB.
 * @author geniarjag
 *
 */
public class EJBResolverInstance {
	@EJB
	private IpsEJBLocal ipsEJB;
	
	@EJB
	private MedicoEJBLocal medicoEJB;

	public IpsEJBLocal getIpsEJB() {
		return ipsEJB;
	}

	public void setIpsEJB(IpsEJBLocal ipsEJB) {
		this.ipsEJB = ipsEJB;
	}

	public MedicoEJBLocal getMedicoEJB() {
		return medicoEJB;
	}

	public void setMedicoEJB(MedicoEJBLocal medicoEJB) {
		this.medicoEJB = medicoEJB;
	}
}
