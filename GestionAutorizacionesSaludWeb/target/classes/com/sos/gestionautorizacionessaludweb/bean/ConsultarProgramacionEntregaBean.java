package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.PrestacionPisVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ClasificacionEventoXProgramacionVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.EstadoNotificacionVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ParametrosConsultaProgramacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.LiquidacionService;
import com.sos.gestionautorizacionessaludejb.controller.GeneracionAutorizacionServicioController;
import com.sos.gestionautorizacionessaludejb.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.controller.ProgramacionEntregaController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

import co.eps.sos.service.exception.ServiceException;

public class ConsultarProgramacionEntregaBean implements Serializable {
	private static final long serialVersionUID = 6236439213816340435L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultarProgramacionEntregaBean.class);
	private static final String COLOR_AZUL = "blue";

	/** Parametros pagina **/
	private Date fechaActual;

	/** Variables estaticas **/
	private static final String PARAMETRO_CUPS = "consecutivoCups";
	private static final String PARAMETRO_CUMS = "consecutivoCums";
	private static final String PARAMETRO_CUOS = "consecutivoCuos";
	private static final Integer PARAMETRO_CONSULTA_RANGO_PROGRAMACION_ENTERGA = 94;
	private static final Integer PARAMETRO_TIPO_PRESTACION = 1;
	private static final String CONSULTA_AFILIADO_BEAN = "consultaAfiliadoBean";
	private int consecutivoCUMS;
	private int consecutivoCUPS;
	private int consecutivoCUOS;

	/** Vos **/
	private AfiliadoVO afiliadoVO;
	private ProcedimientosVO procedimientoVOSeleccionado;
	private MedicamentosVO medicamentoVOSeleccionado;
	private PrestacionPisVO pisVOSeleccionado;
	private ValidacionEspecialVO validacionEspecialVO;

	/** List **/
	private List<ProcedimientosVO> listaCUPSEncontrados;
	private List<MedicamentosVO> listaMedicamentosEncontrados;
	private List<PrestacionPisVO> listaPisEncontrados;
	private List<AutorizacionServicioVO> listaAutorizacionServicioVO;
	private List<SelectItem> listaTiposPrestaciones;
	private List<SelectItem> lstClasificacionesEvento;
	private List<SelectItem> lstEstadosEntrega;
	private List<ProgramacionEntregaVO> lstProgramacionEntrega;
	private ProgramacionEntregaVO programacionEntregaSel;

	/** Form **/
	private Integer tipoPrestacionSeleccionado;
	private Integer consecutivoPrestacion;
	private String codigoPrestacion;
	private String descripcionPrestacion;
	private Date fechaDesde;
	private Date fechaHasta;
	private Integer consCodClasificacionEvento;
	private Integer consCodEstadoEntrega;

	private boolean mostrarPopupMedicamentos = false;
	private boolean mostrarPopupCUPS = false;
	private boolean mostrarPopupPis = false;
	private boolean mostrarPopupGenerarOPS;
	private boolean generarConObservaciones;

	private int rangoCantidadMeses;
	private String acta;
	private String numeroNotificacion;
	private boolean validarEjecucionConstructor = false;

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {
			inicializarVar();
			fechaActual = Calendar.getInstance().getTime();
			consecutivoCUMS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUMS));
			consecutivoCUPS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUPS));
			consecutivoCUOS = Integer.parseInt(Messages.getValorParametro(PARAMETRO_CUOS));

			List<ParametrosGeneralesVO> listaParametrosGeneralesVOs = ((UsuarioBean) FacesUtils
					.getManagedBean("usuarioBean")).getListaParametrosGenerales();
			listaParametrosGeneralesVOs = ConsultaSolicitudesUtil.validarConsultaParametrosGenerales(listaParametrosGeneralesVOs);
			
			for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGeneralesVOs) {
				if (parametrosGeneralesVO
						.getConsecutivoCodigoParametroGeneral() == PARAMETRO_CONSULTA_RANGO_PROGRAMACION_ENTERGA) {
					rangoCantidadMeses = Integer.parseInt(parametrosGeneralesVO.getValorParametroGeneral().toString());
				}
			}

			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			ProgramacionEntregaController programacionEntregaController = new ProgramacionEntregaController();

			listaTiposPrestaciones = registrarSolicitudesController.obtenerListaSelectTipoPrestaciones(null,
					PARAMETRO_TIPO_PRESTACION);
			lstClasificacionesEvento = obtenerListaSelectClasificacionesEvento(
					programacionEntregaController.consultarClasificacionesEvento());
			lstEstadosEntrega = obtenerListaSelectEstadosEntrega(
					programacionEntregaController.consultarEstadosEntrega());
			
			validarEjecucionConstructor = true;

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_CARGA_PARAMETROS),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	public void cargaInicial(ActionEvent event) {
		try {
			if (validarEjecucionConstructor){
				limpiar();
				afiliadoVO = ((ConsultaAfiliadoBean) FacesUtil.getBean(CONSULTA_AFILIADO_BEAN)).getAfiliadoVO();
				ProgramacionEntregaController programacionEntregaController = new ProgramacionEntregaController();
				lstProgramacionEntrega = programacionEntregaController
						.consultarProgramacionEntregaxDefecto(afiliadoVO.getNumeroUnicoIdentificacion());
				if (lstProgramacionEntrega.isEmpty()) {
					throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_AFILIADO_SIN_PROGRAMACION),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_CARGA_PARAMETROS),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo para inicializar var
	 */
	private void inicializarVar() {
		listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
		listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
		listaPisEncontrados = new ArrayList<PrestacionPisVO>();
		listaAutorizacionServicioVO = new ArrayList<AutorizacionServicioVO>();
		listaTiposPrestaciones = new ArrayList<SelectItem>();
		lstEstadosEntrega = new ArrayList<SelectItem>();
		lstClasificacionesEvento = new ArrayList<SelectItem>();
		lstProgramacionEntrega = new ArrayList<ProgramacionEntregaVO>();

		afiliadoVO = new AfiliadoVO();
		procedimientoVOSeleccionado = new ProcedimientosVO();
		medicamentoVOSeleccionado = new MedicamentosVO();
		pisVOSeleccionado = new PrestacionPisVO();
		validacionEspecialVO = new ValidacionEspecialVO();
	}

	/**
	 * Metodo para limpiar el formulario
	 */
	public void limpiar() {
		lstProgramacionEntrega.clear();
		fechaDesde = null;
		fechaHasta = null;
		consCodClasificacionEvento = null;
		consCodEstadoEntrega = null;
		tipoPrestacionSeleccionado = null;
		consecutivoPrestacion = null;
		codigoPrestacion = "";
		descripcionPrestacion = "";
		numeroNotificacion = "";
		acta = "";
	}

	/**
	 * 
	 * @return
	 */
	private boolean isFormularioVacio() {
		return fechaDesde == null && fechaHasta == null && consCodClasificacionEvento == null
				&& consCodEstadoEntrega == null && tipoPrestacionSeleccionado == null && consecutivoPrestacion == null;
	}

	/**
	 * Metodo para validar Fecha Consulta Programacion
	 * 
	 * @throws LogicException
	 */
	private void validarFechaConsultaProgramacion() throws LogicException {
		if (isFormularioVacio()) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION),
					ErrorType.PARAMETRO_ERRADO);
		}
		if ((fechaDesde != null && fechaHasta == null) || (fechaDesde == null && fechaHasta != null)) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION),
					ErrorType.PARAMETRO_ERRADO);
		}
		if (fechaDesde != null && fechaHasta.compareTo(fechaDesde) < 0) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS_CONSULTA_SOLICITUD),
					ErrorType.PARAMETRO_ERRADO);
		}
		validarRangoFechas();
	}

	/**
	 * Metodo para consultar la programacion
	 */
	public void consultarProgramacion() {
		try {
			lstProgramacionEntrega.clear();
			validarFechaConsultaProgramacion();

			if ((tipoPrestacionSeleccionado != null && consecutivoPrestacion == null)
					|| (tipoPrestacionSeleccionado == null && consecutivoPrestacion != null)) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DILIGENCIAR_UN_FILTRO_BUSQUEDA_PROGRAMACION),
						ErrorType.PARAMETRO_ERRADO);
			}

			ParametrosConsultaProgramacionVO parametrosConsulta = new ParametrosConsultaProgramacionVO();
			parametrosConsulta.setNumeroUnicoIdentificacion(afiliadoVO.getNumeroUnicoIdentificacion());
			parametrosConsulta.setFechaDesde(fechaDesde);
			parametrosConsulta.setFechaHasta(fechaHasta);
			parametrosConsulta.setConsCodClasificacionEvento(consCodClasificacionEvento);
			parametrosConsulta.setConsCodEstadoEntrega(consCodEstadoEntrega);
			parametrosConsulta.setTipoPrestacionSeleccionado(tipoPrestacionSeleccionado);
			parametrosConsulta.setConsecutivoPrestacion(consecutivoPrestacion);
			parametrosConsulta.setActa(acta);
			parametrosConsulta.setNumeroNotificacion(numeroNotificacion);

			ProgramacionEntregaController programacionEntregaController = new ProgramacionEntregaController();
			lstProgramacionEntrega = programacionEntregaController.consultarProgramacionEntrega(parametrosConsulta);

			if (lstProgramacionEntrega.isEmpty()) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_PROGRAMACION_SIN_RESULTADO),
						ErrorType.PARAMETRO_ERRADO);
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * 
	 * @throws LogicException
	 */
	private void validarRangoFechas() throws LogicException {
		if (fechaDesde != null && fechaHasta != null) {
			boolean valRangoFecha = Utilidades.esfechaMayorNMeses(fechaDesde, fechaHasta, rangoCantidadMeses);
			if (valRangoFecha) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHAS_CONSULTA_PROGRAMACION,
								rangoCantidadMeses),
						ErrorType.PARAMETRO_ERRADO);
			}
		}
	}

	/**
	 * Metodo para generar una solicitud
	 */
	public void generar() {
		try {

			int consecutivoSolicitud = 0;

			if (afiliadoSinDerechos() && !ConstantesWeb.SI.equals(afiliadoVO.getValEspecial())) {
				throw new LogicException(
						Messages.getValorExcepcion(
								ConstantesWeb.ERROR_EXCEPCION_GENERAR_PROGRAMACION_AFILIADO_SIN_DERECHOS),
						ErrorType.PARAMETRO_ERRADO);
			}

			if (!ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_SIN_ENTREGAR
					.equals(programacionEntregaSel.getConsEstadoProgramacion())
					&& !ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_REPROGRAMADO
							.equals(programacionEntregaSel.getConsEstadoProgramacion())
					&& !ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_ENTREGADO
							.equals(programacionEntregaSel.getConsEstadoProgramacion())
					&& !ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_ASOCIADO_OPS
							.equals(programacionEntregaSel.getConsEstadoProgramacion())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_ESTADO),
						ErrorType.PARAMETRO_ERRADO);
			}

			if (!Utilidades.esfechaMenorIgualActual(fechaActual, programacionEntregaSel.getFechaEntrega())
					|| Utilidades.esfechaMenorIgualActual(fechaActual, programacionEntregaSel.getFechaFinalizacion())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_GENERAR_PROGRAMACION_ENTREGA_FECHA),
						ErrorType.PARAMETRO_ERRADO);
			}

			ProgramacionEntregaController programacionEntregaController = new ProgramacionEntregaController();
			consecutivoSolicitud = programacionEntregaController.ejecutarCreacionSolicitudProgramacionEntrega(
					programacionEntregaSel.getConsCodProgramacionFecEvento(),
					((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario());
			LOG.debug("SOLICITUD GENERADA POR PROGRAMACION DE ENTREGA # " + consecutivoSolicitud);
			programacionEntregaSel.setConsSolicitud(consecutivoSolicitud);
			programacionEntregaSel.setVerGenerar(true);
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo para una nueva Consulta Prestacion
	 */
	public void nuevaConsultaPrestacion() {
		try {
			tipoPrestacionSeleccionado = null;
			consecutivoPrestacion = null;
			codigoPrestacion = "";
			descripcionPrestacion = "";
			numeroNotificacion = "";
			acta = "";
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo para limpiar al cambiar el tipo de prestacion
	 */
	public void cambioTipoPrestacion() {
		consecutivoPrestacion = null;
		codigoPrestacion = "";
		descripcionPrestacion = "";
	}

	/**
	 * Metodo para buscar la prestacion
	 */
	public void buscarPrestacion() {
		try {
			if (tipoPrestacionSeleccionado == null) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_DATOS_OBLIGATORIOS_BUSQUEDA_PRESTACION),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (tipoPrestacionSeleccionado.compareTo(consecutivoCUMS) == 0) {
				buscarPrestacionMedicamentos();
			} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUPS) == 0) {
				buscarPrestacionCUPS();
			} else if (tipoPrestacionSeleccionado.compareTo(consecutivoCUOS) == 0) {
				buscarPrestacionPis();
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo para buscar cups
	 */
	public void seleccionPopupCUPS() {
		if (procedimientoVOSeleccionado != null) {
			consecutivoPrestacion = procedimientoVOSeleccionado.getConsecutivoCodificacionProcedimiento();
			codigoPrestacion = procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento();
			descripcionPrestacion = procedimientoVOSeleccionado.getDescripcionCodificacionProcedimiento();
			listaCUPSEncontrados = null;
			mostrarPopupCUPS = false;
		}
	}

	/**
	 * Metodo para buscar cups
	 */
	public void buscarPrestacionCUPS() {
		mostrarPopupCUPS = false;
		FuncionesAppWeb funcionesConsulta = new FuncionesAppWeb();
		try {
			Utilidades.validarAdicionarPrestacion(codigoPrestacion, descripcionPrestacion);
			PrestacionController prestacionController = new PrestacionController();
			listaCUPSEncontrados = prestacionController.consultarPrestacionesCUPSPorParametros(
					new ProcedimientosVO(codigoPrestacion, descripcionPrestacion));
			funcionesConsulta.validarAsociacionPrestacion(listaCUPSEncontrados);
			if (funcionesConsulta.isMostrarPopupCUPS()) {
				mostrarPopupCUPS = true;
			} else {
				procedimientoVOSeleccionado = funcionesConsulta.getProcedimientoVOSeleccionado();
				consecutivoPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getConsecutivoCodificacionProcedimiento();
				codigoPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getCodigoCodificacionProcedimiento();
				descripcionPrestacion = funcionesConsulta.getProcedimientoVOSeleccionado()
						.getDescripcionCodificacionProcedimiento();
				mostrarPopupCUPS = false;
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
		}
	}

	/**
	 * Metodo para buscar medicamentos
	 */
	public void buscarPrestacionMedicamentos() {
		mostrarPopupMedicamentos = false;
		try {
			Utilidades.validarAdicionarPrestacion(codigoPrestacion, descripcionPrestacion);
			PrestacionController prestacionController = new PrestacionController();
			listaMedicamentosEncontrados = prestacionController.consultarPrestacionesMedicamentosPorParametros(
					new MedicamentosVO(codigoPrestacion, descripcionPrestacion));
			if (listaMedicamentosEncontrados != null && !listaMedicamentosEncontrados.isEmpty()) {
				if (listaMedicamentosEncontrados.size() == 1) {
					medicamentoVOSeleccionado = listaMedicamentosEncontrados.get(0);
					consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
					codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
					descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
					mostrarPopupMedicamentos = false;
				} else {
					mostrarPopupMedicamentos = true;
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ASOCIAR_PRESTACION),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo cerrar Popup CUPS
	 */
	public void cerrarPopupCUPS() {
		listaCUPSEncontrados = null;
		mostrarPopupCUPS = false;
	}

	/**
	 * Metodo para seleccion Popup Medicamentos
	 */
	public void seleccionPopupMedicamentos() {
		if (medicamentoVOSeleccionado != null) {
			consecutivoPrestacion = medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento();
			codigoPrestacion = medicamentoVOSeleccionado.getCodigoCodificacionMedicamento();
			descripcionPrestacion = medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento();
			listaMedicamentosEncontrados = null;
			mostrarPopupMedicamentos = false;
		}
	}

	/**
	 * Metodo cerrar Popup Medicamentos
	 */
	public void cerrarPopupMedicamentos() {
		listaMedicamentosEncontrados = null;
		mostrarPopupMedicamentos = false;
	}

	/**
	 * Metodo cerrar Popup Pis
	 */
	public void cerrarPopupPis() {
		listaPisEncontrados = null;
		mostrarPopupPis = false;
	}

	/**
	 * Metodo para buscar PIS
	 */
	public void buscarPrestacionPis() {
		mostrarPopupPis = false;
		try {
			PrestacionController prestacionController = new PrestacionController();
			listaPisEncontrados = prestacionController.consultarPrestacionesCUOSPorParametros(
					new PrestacionPisVO(codigoPrestacion, descripcionPrestacion));
			if (listaPisEncontrados != null && !listaPisEncontrados.isEmpty()) {
				if (listaPisEncontrados.size() == 1) {
					pisVOSeleccionado = listaPisEncontrados.get(0);
					consecutivoPrestacion = pisVOSeleccionado.getConsecutivoPrestacionPis();
					codigoPrestacion = pisVOSeleccionado.getCodigoPrestacionPis();
					descripcionPrestacion = pisVOSeleccionado.getDescripcionPrestacionPis();
					mostrarPopupPis = false;
				} else {
					mostrarPopupPis = true;
				}
			} else {
				throw new LogicException(Messages.getValorError(ConstantesWeb.ERROR_ASOCIAR_PRESTACION),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo seleccion Popup Pis
	 */
	public void seleccionPopupPis() {
		if (pisVOSeleccionado != null) {
			consecutivoPrestacion = pisVOSeleccionado.getConsecutivoPrestacionPis();
			codigoPrestacion = pisVOSeleccionado.getCodigoPrestacionPis();
			descripcionPrestacion = pisVOSeleccionado.getDescripcionPrestacionPis();
			listaPisEncontrados = null;
			mostrarPopupPis = false;
		}
	}

	/**
	 * Metodo para validar si el afiliado tiene derechos
	 * 
	 * @return
	 */
	private boolean afiliadoSinDerechos() {
		return ConstantesEJB.getCodigosEstadosSinDerecho().contains(afiliadoVO.getConsecutivoEstadoAfiliado());
	}

	/**
	 * Metodo para validar la descarga de la OPS
	 * 
	 * @throws LogicException
	 */
	public void validarOPSDescarga() throws LogicException {
		String usuarioProceso = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario();
		if (ConstantesWeb.COD_ESTADO_AUTORIZADO.equals(programacionEntregaSel.getConsEstadoOPS())) {
			if (!ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_SIN_ENTREGAR
					.equals(programacionEntregaSel.getConsEstadoProgramacion())
					&& !ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_REPROGRAMADO
							.equals(programacionEntregaSel.getConsEstadoProgramacion())
					&& !ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_ASOCIADO_OPS
							.equals(programacionEntregaSel.getConsEstadoProgramacion())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO),
						ErrorType.PARAMETRO_ERRADO);
			}
		}
		if (ConstantesWeb.COD_ESTADO_ENTREGADA.equals(programacionEntregaSel.getConsEstadoOPS())) {
			if (!ConstantesWeb.COD_ESTADO_PROGRAMACION_ENTREGA_ENTREGADO
					.equals(programacionEntregaSel.getConsEstadoProgramacion())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO),
						ErrorType.PARAMETRO_ERRADO);
			}
			if (Utilidades.esfechaMenorActual(fechaActual, programacionEntregaSel.getFechaVencimientoAutorizacion())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_FECHA_INVALIDA),
						ErrorType.PARAMETRO_ERRADO);
			}
		}
		generarOPS();
		ProgramacionEntregaController programacionEntregaController;
		try {
			programacionEntregaController = new ProgramacionEntregaController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		programacionEntregaController.actualizarEstadoProgramacionEntrega(programacionEntregaSel.getConsSolicitud(),
				programacionEntregaSel.getConsCodProgramacionFecEvento(), usuarioProceso,
				ConstantesWeb.COD_ESTADO_ENTREGADA);
	}

	/**
	 * 
	 * @return
	 * @throws LogicException
	 */
	private boolean validarSolicitudLiquidada() throws LogicException {
		ProgramacionEntregaController programacionEntregaController;
		try {
			programacionEntregaController = new ProgramacionEntregaController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}

		listaAutorizacionServicioVO = programacionEntregaController.consultarInformacionSolicitudxProgramacion(
				programacionEntregaSel.getConsSolicitud(), programacionEntregaSel.getConsPrestacion());
		return listaAutorizacionServicioVO.isEmpty();
	}

	/**
	 * 
	 * @return
	 * @throws LogicException
	 */
	private boolean gestionarRespuestaLiquidacion() throws LogicException {
		boolean rtaLiquidacion;
		if (validarSolicitudLiquidada()) {
			rtaLiquidacion = liquidaryGenerarNumOPS();
			ProgramacionEntregaController programacionEntregaController;
			try {
				programacionEntregaController = new ProgramacionEntregaController();
			} catch (Exception e) {
				throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
			}
			listaAutorizacionServicioVO = programacionEntregaController.consultarInformacionSolicitudxProgramacion(
					programacionEntregaSel.getConsSolicitud(), programacionEntregaSel.getConsPrestacion());
		} else {
			rtaLiquidacion = true;
		}
		return rtaLiquidacion;
	}

	private void generarReciboCaja() throws LogicException {

		ProgramacionEntregaController programacionEntregaController;
		try {
			programacionEntregaController = new ProgramacionEntregaController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}
		List<AutorizacionServicioVO> consultarInformacionSolicitudxProgramacion = programacionEntregaController
				.consultarInformacionSolicitudxProgramacion(programacionEntregaSel.getConsSolicitud(),
						programacionEntregaSel.getConsPrestacion());

		GenerarReciboBean reciboBean = (GenerarReciboBean) FacesUtils.getManagedBean("generarReciboBean");
		if (reciboBean == null) {
			reciboBean = new GenerarReciboBean();
		}
		reciboBean.setMostrarModalReciboCaja(false);
		reciboBean.setBeanSolicitud(ConstantesWeb.PROGRAMACION_ENTREGA);

		DatosSolicitudVO solicitudSeleccionada = new DatosSolicitudVO();
		solicitudSeleccionada.setAfiliadoVO(afiliadoVO);
		solicitudSeleccionada
				.setNumeroSolicitud(consultarInformacionSolicitudxProgramacion.get(0).getConsecutivoSolicitud());
		solicitudSeleccionada.setFechaSolicitud(fechaActual);

		reciboBean.setSolicitudSeleccionada(solicitudSeleccionada);

		OPSCajaVO opsCajas = reciboBean.getOpsCajas();

		boolean muestraMensajeCausales = mostrarMensajeCausalesProgramacion(reciboBean);

		if (ConstantesWeb.SI.equalsIgnoreCase(opsCajas.getMuestraMensajeIPS())) {
			validarOPSDescarga();
			MostrarMensaje.mostrarMensaje(ConstantesWeb.CLAVE_MENSAJE_PAGO_IPS, COLOR_AZUL);
		}

		if ((reciboBean.getListInformacion() == null || reciboBean.getListInformacion().isEmpty())
				|| ConstantesWeb.NO.equalsIgnoreCase(opsCajas.getMuestraReciboCaja())) {
			validarOPSDescarga();
		} else {
			if (muestraMensajeCausales && reciboBean.getListInformacion() != null
					&& reciboBean.getListInformacion().isEmpty()) {
				validarOPSDescarga();
			} else {
				reciboBean.setMostrarModalReciboCaja(true);
				reciboBean.setTabSeleccionado(ConstantesWeb.TAB_GENERAR_RECIBO);
			}
		}
	}

	private boolean mostrarMensajeCausalesProgramacion(GenerarReciboBean reciboBean) {
		boolean resp = false;
		String causales = reciboBean.getCausales();
		if (Utilidades.validarCamposVacios(causales)) {
			resp = true;
			MostrarMensaje.mostrarMensaje(ConstantesWeb.MENSAJE_CAUSAS_NO_PAGO + causales, COLOR_AZUL);
		}
		return resp;
	}

	/**
	 * Metodo para descargar la OPS
	 */
	public void descargar() {
		try {
			if (gestionarRespuestaLiquidacion()) {
				asignarDatosAdicionales();
				if (programacionEntregaSel.isValorConceptoCero() == true) {
					throw new LogicException(
							Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_DESCARGA_OPS_CONCEPTO_CERO),
							ErrorType.PARAMETRO_ERRADO);
				}

				if (ConstantesWeb.COD_ESTADO_AUTORIZADO.equals(programacionEntregaSel.getConsEstadoOPS())) {
					this.generarReciboCaja();
				} else if (ConstantesWeb.COD_ESTADO_ENTREGADA.equals(programacionEntregaSel.getConsEstadoOPS())) {
					validarOPSDescarga();
				} else {
					throw new LogicException(
							Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_DESCARGA_ESTADO_INVALIDO_OPS),
							ErrorType.PARAMETRO_ERRADO);
				}
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo para asignar datos de la solcitud a la programacion
	 */
	private void asignarDatosAdicionales() {
		if (listaAutorizacionServicioVO != null && !listaAutorizacionServicioVO.isEmpty()) {
			AutorizacionServicioVO vo = listaAutorizacionServicioVO.get(0);
			programacionEntregaSel.setConsEstadoOPS(vo.getConsecutivoCodigoEstadoOPS());
			programacionEntregaSel.setFechaVencimientoAutorizacion(vo.getFechaVencimiento());
			programacionEntregaSel.setConsEstadoPrestacion(vo.getConsEstadoPrestacion());
			programacionEntregaSel.setNumRadicadoSolicitud(vo.getNumRadicadoSolicitud());
			programacionEntregaSel.setConsecutivoServicioSolicitado(vo.getConsecutivoServicioSolicitado());
			programacionEntregaSel.setConsecutivoCodServicioSolicitado(vo.getConsecutivoCodServicioSolicitado());
			programacionEntregaSel.setCodServicioSolicitado(vo.getCodServicioSolicitado());
			programacionEntregaSel.setValorConceptoCero(vo.isValorConceptoCero());
		}
	}

	/**
	 * Metodo para liquidar y Generar Num OPS
	 */
	private boolean liquidaryGenerarNumOPS() {
		boolean liquidacionExito = false;
		try {
			LiquidacionSolicitudVO liquidacionSolicitudVO = ejecutarLiquidacion();
			if (liquidacionSolicitudVO != null) {
				liquidacionExito = true;
				generarNumeroUnicoOPS(liquidacionSolicitudVO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return liquidacionExito;
	}

	/**
	 * Metodo ejecutar Liquidacion
	 * 
	 * @return
	 */
	private LiquidacionSolicitudVO ejecutarLiquidacion() {
		try {
			LiquidacionService liquidacionService = new LiquidacionService();
			co.com.sos.liquidacion.v1.LiquidacionService liquidacionServiceExc = liquidacionService
					.crearLiquidacionService();
			SolicitudVO solicitudVO = new SolicitudVO();
			solicitudVO.setConsecutivoSolicitud(programacionEntregaSel.getConsSolicitud());
			return liquidacionService.consultaResultadoLiquidacion(liquidacionService
					.obtenerLiquidacionService(liquidacionServiceExc, solicitudVO, FacesUtils.getUserName()));
		} catch (ServiceException ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_PRESTACION), ConstantesWeb.CADENA_VACIA));
			LOG.error(ex.getMessage(), ex);
			return null;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	/** Metodo que se utiliza para la generacion el numero unico de la OPS */
	private void generarNumeroUnicoOPS(LiquidacionSolicitudVO liquidacionSolicitudVO) throws LogicException {
		ServiceErrorVO serviceErrorVO = null;
		if (liquidacionSolicitudVO != null && liquidacionSolicitudVO.getResultadoLiquidacion() == 1) {
			GenerarNumeroUnicoOPSBean numeroUnicoOPSBean = new GenerarNumeroUnicoOPSBean();
			serviceErrorVO = numeroUnicoOPSBean.generarNumeroUnicoAutorizacionServicio(
					programacionEntregaSel.getConsSolicitud(), FacesUtils.getUserName());
			if (!FuncionesAppWeb.validateServiceError(serviceErrorVO)) {
				throw new LogicException(serviceErrorVO.getMensajeError(), ErrorType.PARAMETRO_ERRADO);
			}
		} else {
			LOG.error(liquidacionSolicitudVO.getMensajeResultadoLiquidacion());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE)
									+ liquidacionSolicitudVO.getMensajeResultadoLiquidacion(),
							ConstantesWeb.CADENA_VACIA));
		}
	}

	/**
	 * Metodo que se utiliza para la generacion del reporte de la OPS
	 * 
	 * @throws LogicException
	 */
	public void generarOPS() throws LogicException {
		try {

			GenerarReporteBean generarReporteBean = new GenerarReporteBean();
			List<PrestacionDTO> lstPrestacionDTO = new ArrayList<PrestacionDTO>();

			PrestacionDTO dto = new PrestacionDTO();
			dto.setConsecutivoCodificacionPrestacion(programacionEntregaSel.getConsecutivoCodServicioSolicitado());
			dto.setCodigoCodificacionPrestacion(programacionEntregaSel.getCodServicioSolicitado());
			lstPrestacionDTO.add(dto);

			generarReporteBean.formatoOPSConsulta(afiliadoVO, programacionEntregaSel.getNumRadicadoSolicitud(),
					programacionEntregaSel.getConsSolicitud(), lstPrestacionDTO, listaAutorizacionServicioVO, false);
			guardarLogAuditoriaValEspecial();
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Guardar log de auditoria si hizo validacion especial
	 * 
	 * @throws LogicException
	 */
	private void guardarLogAuditoriaValEspecial() throws LogicException {
		if (ConstantesWeb.SI.equals(afiliadoVO.getValEspecial())) {
			validacionEspecialVO.setAfiliadoVO(afiliadoVO);
			validacionEspecialVO.setConsecutivoSolicitud(programacionEntregaSel.getConsSolicitud());

			GeneracionAutorizacionServicioController generacionAutorizacionServicioController;
			try {
				generacionAutorizacionServicioController = new GeneracionAutorizacionServicioController();
			} catch (Exception e) {
				throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
			}

			for (AutorizacionServicioVO autorizacionServicioVO : listaAutorizacionServicioVO) {
				validacionEspecialVO.setNumeroUnicoAutorizacion(autorizacionServicioVO.getNumeroUnicoAutorizacion());
				generacionAutorizacionServicioController.registrarValidacionEspecialAfiliado(validacionEspecialVO,
						((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).getNombreUsuario(),
						autorizacionServicioVO.getNumeroUnicoAutorizacion()
								+ ConstantesWeb.ACCION_EJECUTADA_DESCARGA_OPS);
			}

		}
	}

	/** Metodo para convertir una lista de valores en una lista desplegable */
	public List<SelectItem> obtenerListaSelectClasificacionesEvento(List<ClasificacionEventoXProgramacionVO> listaVos)
			throws LogicException {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (ClasificacionEventoXProgramacionVO vo : listaVos) {
				item = new SelectItem(vo.getConsCodClasificacionEvento(), vo.getDesClasificacionEvento());
				listaItems.add(item);
			}
		}
		return listaItems;
	}

	/** Metodo para convertir una lista de valores en una lista desplegable */
	public List<SelectItem> obtenerListaSelectEstadosEntrega(List<EstadoNotificacionVO> listaVos)
			throws LogicException {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (EstadoNotificacionVO vo : listaVos) {
				item = new SelectItem(vo.getConsCodEstadoNotificacion(), vo.getDescripcion());
				listaItems.add(item);
			}
		}
		return listaItems;
	}

	/**
	 * @return the afiliadoVO
	 */
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	/**
	 * @param afiliadoVO
	 *            the afiliadoVO to set
	 */
	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	/**
	 * @return the listaCUPSEncontrados
	 */
	public List<ProcedimientosVO> getListaCUPSEncontrados() {
		return listaCUPSEncontrados;
	}

	/**
	 * @param listaCUPSEncontrados
	 *            the listaCUPSEncontrados to set
	 */
	public void setListaCUPSEncontrados(List<ProcedimientosVO> listaCUPSEncontrados) {
		this.listaCUPSEncontrados = listaCUPSEncontrados;
	}

	/**
	 * @return the listaMedicamentosEncontrados
	 */
	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	/**
	 * @param listaMedicamentosEncontrados
	 *            the listaMedicamentosEncontrados to set
	 */
	public void setListaMedicamentosEncontrados(List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	/**
	 * @return the listaPisEncontrados
	 */
	public List<PrestacionPisVO> getListaPisEncontrados() {
		return listaPisEncontrados;
	}

	/**
	 * @param listaPisEncontrados
	 *            the listaPisEncontrados to set
	 */
	public void setListaPisEncontrados(List<PrestacionPisVO> listaPisEncontrados) {
		this.listaPisEncontrados = listaPisEncontrados;
	}

	/**
	 * @return the procedimientoVOSeleccionado
	 */
	public ProcedimientosVO getProcedimientoVOSeleccionado() {
		return procedimientoVOSeleccionado;
	}

	/**
	 * @param procedimientoVOSeleccionado
	 *            the procedimientoVOSeleccionado to set
	 */
	public void setProcedimientoVOSeleccionado(ProcedimientosVO procedimientoVOSeleccionado) {
		this.procedimientoVOSeleccionado = procedimientoVOSeleccionado;
	}

	/**
	 * @return the medicamentoVOSeleccionado
	 */
	public MedicamentosVO getMedicamentoVOSeleccionado() {
		return medicamentoVOSeleccionado;
	}

	/**
	 * @param medicamentoVOSeleccionado
	 *            the medicamentoVOSeleccionado to set
	 */
	public void setMedicamentoVOSeleccionado(MedicamentosVO medicamentoVOSeleccionado) {
		this.medicamentoVOSeleccionado = medicamentoVOSeleccionado;
	}

	/**
	 * @return the pisVOSeleccionado
	 */
	public PrestacionPisVO getPisVOSeleccionado() {
		return pisVOSeleccionado;
	}

	/**
	 * @param codigoPrestacion
	 *            the codigoPrestacion to set
	 */
	public void setCodigoPrestacion(String codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	/**
	 * @param pisVOSeleccionado
	 *            the pisVOSeleccionado to set
	 */
	public void setPisVOSeleccionado(PrestacionPisVO pisVOSeleccionado) {
		this.pisVOSeleccionado = pisVOSeleccionado;
	}

	/**
	 * @return the tipoPrestacionSeleccionado
	 */
	public Integer getTipoPrestacionSeleccionado() {
		return tipoPrestacionSeleccionado;
	}

	/**
	 * @return the codigoPrestacion
	 */
	public String getCodigoPrestacion() {
		return codigoPrestacion;
	}

	/**
	 * @param tipoPrestacionSeleccionado
	 *            the tipoPrestacionSeleccionado to set
	 */
	public void setTipoPrestacionSeleccionado(Integer tipoPrestacionSeleccionado) {
		this.tipoPrestacionSeleccionado = tipoPrestacionSeleccionado;
	}

	/**
	 * @return the consecutivoPrestacion
	 */
	public Integer getConsecutivoPrestacion() {
		return consecutivoPrestacion;
	}

	/**
	 * @return the fechaActual
	 */
	public Date getFechaActual() {
		return fechaActual;
	}

	/**
	 * @param fechaActual
	 *            the fechaActual to set
	 */
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	/**
	 * @param consecutivoPrestacion
	 *            the consecutivoPrestacion to set
	 */
	public void setConsecutivoPrestacion(Integer consecutivoPrestacion) {
		this.consecutivoPrestacion = consecutivoPrestacion;
	}

	/**
	 * @return the descripcionPrestacion
	 */
	public String getDescripcionPrestacion() {
		return descripcionPrestacion;
	}

	/**
	 * @return the consecutivoCUMS
	 */
	public int getConsecutivoCUMS() {
		return consecutivoCUMS;
	}

	/**
	 * @param consecutivoCUMS
	 *            the consecutivoCUMS to set
	 */
	public void setConsecutivoCUMS(int consecutivoCUMS) {
		this.consecutivoCUMS = consecutivoCUMS;
	}

	/**
	 * @return the consecutivoCUPS
	 */
	public int getConsecutivoCUPS() {
		return consecutivoCUPS;
	}

	/**
	 * @param consecutivoCUPS
	 *            the consecutivoCUPS to set
	 */
	public void setConsecutivoCUPS(int consecutivoCUPS) {
		this.consecutivoCUPS = consecutivoCUPS;
	}

	/**
	 * @return the consecutivoCUOS
	 */
	public int getConsecutivoCUOS() {
		return consecutivoCUOS;
	}

	/**
	 * @param consecutivoCUOS
	 *            the consecutivoCUOS to set
	 */
	public void setConsecutivoCUOS(int consecutivoCUOS) {
		this.consecutivoCUOS = consecutivoCUOS;
	}

	/**
	 * @param descripcionPrestacion
	 *            the descripcionPrestacion to set
	 */
	public void setDescripcionPrestacion(String descripcionPrestacion) {
		this.descripcionPrestacion = descripcionPrestacion;
	}

	/**
	 * @return the generarConObservaciones
	 */
	public boolean isGenerarConObservaciones() {
		return generarConObservaciones;
	}

	/**
	 * @param generarConObservaciones
	 *            the generarConObservaciones to set
	 */
	public void setGenerarConObservaciones(boolean generarConObservaciones) {
		this.generarConObservaciones = generarConObservaciones;
	}

	/**
	 * @return the validacionEspecialVO
	 */
	public ValidacionEspecialVO getValidacionEspecialVO() {
		return validacionEspecialVO;
	}

	/**
	 * @param validacionEspecialVO
	 *            the validacionEspecialVO to set
	 */
	public void setValidacionEspecialVO(ValidacionEspecialVO validacionEspecialVO) {
		this.validacionEspecialVO = validacionEspecialVO;
	}

	/**
	 * @return the listaAutorizacionServicioVO
	 */
	public List<AutorizacionServicioVO> getListaAutorizacionServicioVO() {
		return listaAutorizacionServicioVO;
	}

	/**
	 * @param listaAutorizacionServicioVO
	 *            the listaAutorizacionServicioVO to set
	 */
	public void setListaAutorizacionServicioVO(List<AutorizacionServicioVO> listaAutorizacionServicioVO) {
		this.listaAutorizacionServicioVO = listaAutorizacionServicioVO;
	}

	/**
	 * @return the mostrarPopupMedicamentos
	 */
	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	/**
	 * @param mostrarPopupMedicamentos
	 *            the mostrarPopupMedicamentos to set
	 */
	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	/**
	 * @return the mostrarPopupCUPS
	 */
	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	/**
	 * @param mostrarPopupCUPS
	 *            the mostrarPopupCUPS to set
	 */
	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	/**
	 * @return the mostrarPopupPis
	 */
	public boolean isMostrarPopupPis() {
		return mostrarPopupPis;
	}

	/**
	 * @param mostrarPopupPis
	 *            the mostrarPopupPis to set
	 */
	public void setMostrarPopupPis(boolean mostrarPopupPis) {
		this.mostrarPopupPis = mostrarPopupPis;
	}

	/**
	 * @return the mostrarPopupGenerarOPS
	 */
	public boolean isMostrarPopupGenerarOPS() {
		return mostrarPopupGenerarOPS;
	}

	/**
	 * @param mostrarPopupGenerarOPS
	 *            the mostrarPopupGenerarOPS to set
	 */
	public void setMostrarPopupGenerarOPS(boolean mostrarPopupGenerarOPS) {
		this.mostrarPopupGenerarOPS = mostrarPopupGenerarOPS;
	}

	/**
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * @param fechaDesde
	 *            the fechaDesde to set
	 */
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * @param fechaHasta
	 *            the fechaHasta to set
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * @return the consCodClasificacionEvento
	 */
	public Integer getConsCodClasificacionEvento() {
		return consCodClasificacionEvento;
	}

	/**
	 * @param consCodClasificacionEvento
	 *            the consCodClasificacionEvento to set
	 */
	public void setConsCodClasificacionEvento(Integer consCodClasificacionEvento) {
		this.consCodClasificacionEvento = consCodClasificacionEvento;
	}

	/**
	 * @return the consCodEstadoEntrega
	 */
	public Integer getConsCodEstadoEntrega() {
		return consCodEstadoEntrega;
	}

	/**
	 * @param consCodEstadoEntrega
	 *            the consCodEstadoEntrega to set
	 */
	public void setConsCodEstadoEntrega(Integer consCodEstadoEntrega) {
		this.consCodEstadoEntrega = consCodEstadoEntrega;
	}

	/**
	 * @return the listaTiposPrestaciones
	 */
	public List<SelectItem> getListaTiposPrestaciones() {
		return listaTiposPrestaciones;
	}

	/**
	 * @param listaTiposPrestaciones
	 *            the listaTiposPrestaciones to set
	 */
	public void setListaTiposPrestaciones(List<SelectItem> listaTiposPrestaciones) {
		this.listaTiposPrestaciones = listaTiposPrestaciones;
	}

	/**
	 * @return the lstClasificacionesEvento
	 */
	public List<SelectItem> getLstClasificacionesEvento() {
		return lstClasificacionesEvento;
	}

	/**
	 * @param lstClasificacionesEvento
	 *            the lstClasificacionesEvento to set
	 */
	public void setLstClasificacionesEvento(List<SelectItem> lstClasificacionesEvento) {
		this.lstClasificacionesEvento = lstClasificacionesEvento;
	}

	/**
	 * @return the lstEstadosEntrega
	 */
	public List<SelectItem> getLstEstadosEntrega() {
		return lstEstadosEntrega;
	}

	/**
	 * @param lstEstadosEntrega
	 *            the lstEstadosEntrega to set
	 */
	public void setLstEstadosEntrega(List<SelectItem> lstEstadosEntrega) {
		this.lstEstadosEntrega = lstEstadosEntrega;
	}

	/**
	 * @return the lstProgramacionEntrega
	 */
	public List<ProgramacionEntregaVO> getLstProgramacionEntrega() {
		return lstProgramacionEntrega;
	}

	/**
	 * @param lstProgramacionEntrega
	 *            the lstProgramacionEntrega to set
	 */
	public void setLstProgramacionEntrega(List<ProgramacionEntregaVO> lstProgramacionEntrega) {
		this.lstProgramacionEntrega = lstProgramacionEntrega;
	}

	/**
	 * @return the programacionEntregaSel
	 */
	public ProgramacionEntregaVO getProgramacionEntregaSel() {
		return programacionEntregaSel;
	}

	/**
	 * @param programacionEntregaSel
	 *            the programacionEntregaSel to set
	 */
	public void setProgramacionEntregaSel(ProgramacionEntregaVO programacionEntregaSel) {
		this.programacionEntregaSel = programacionEntregaSel;
	}


	/**
	 * @param acta
	 *            the acta to set
	 */
	public void setActa(String acta) {
		this.acta = acta;
	}

	public String getNumeroNotificacion() {
		return numeroNotificacion;
	}

	public void setNumeroNotificacion(String numeroNotificacion) {
		this.numeroNotificacion = numeroNotificacion;
	}

	public String getActa() {
		return acta;
	}

}
