package com.sos.gestionautorizacionessaludweb.bean;

import java.util.List;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaludejb.controller.GeneracionAutorizacionServicioController;

/**
 * Clase bean que permite la generacion de los numero de autorizacion del servicio
 * @author ing. Victor Hugo Gil Ramos
 * @version 13/05/2016
 *
 */
public class GenerarNumeroUnicoOPSBean {
	
	private ServiceErrorVO serviceErrorVO;
	private List<AutorizacionServicioVO> listaAutorizacionServicioVO;
	
	public ServiceErrorVO generarNumeroUnicoAutorizacionServicio(Integer consecutivoSolicitud, String userSession) throws LogicException{
		
		GeneracionAutorizacionServicioController generacionAutorizacionServicio = new GeneracionAutorizacionServicioController();		
		serviceErrorVO = generacionAutorizacionServicio.generarNumeroUnicoAutorizacionServicio(consecutivoSolicitud, userSession);		
		return serviceErrorVO;		
	}
	
	
	public List<AutorizacionServicioVO> consultaNumeroAutorizacionxSolicitud(Integer consecutivoSolicitud, Integer consecutivoPrestacion)throws LogicException{		
		GeneracionAutorizacionServicioController generacionAutorizacionServicio = new GeneracionAutorizacionServicioController();
		listaAutorizacionServicioVO = generacionAutorizacionServicio.consultaNumeroAutorizacionxSolicitud(consecutivoSolicitud, consecutivoPrestacion);
		return listaAutorizacionServicioVO;
	}
}
