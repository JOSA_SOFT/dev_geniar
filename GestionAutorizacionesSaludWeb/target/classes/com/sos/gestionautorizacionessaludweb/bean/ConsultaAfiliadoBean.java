package com.sos.gestionautorizacionessaludweb.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.ModuloVO;
import com.sos.gestionautorizacionessaluddata.model.ServicioVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.DatosAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoFamiliarVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoPoblacionalVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgosPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TutelasAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.CargarCombosUtil;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MenuBean;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

public class ConsultaAfiliadoBean implements Serializable{
	private static final long serialVersionUID = -5569222007305205541L;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultaAfiliadoBean.class);
	private static final String ERROR_EXCEPCION_INESPERADA = ConstantesWeb.ERROR_EXCEPCION_INESPERADA;
	private static final String COLOR_ROJO = ConstantesWeb.COLOR_ROJO;

	private List<SelectItem> listaTiposIdentificacion = new ArrayList<SelectItem>();
	private List<SelectItem> listaPlanes = new ArrayList<SelectItem>();

	private List<TutelasAfiliadoVO> listaTutelasAfiliado = new ArrayList<TutelasAfiliadoVO>();
	private TutelasAfiliadoVO tutelaSeleccionada = new TutelasAfiliadoVO();

	private List<RiesgosPacienteVO> listaRiesgosPaciente = new ArrayList<RiesgosPacienteVO>();
	private RiesgosPacienteVO riesgoPacienteSeleccionado = new RiesgosPacienteVO();

	private List<AfiliadoVO> listaAfiliadoVO = new ArrayList<AfiliadoVO>();
	private AfiliadoVO afiliadoVO = new AfiliadoVO();
	private DatosAfiliadoVO datosAfiliadoVO = new DatosAfiliadoVO();

	private List<OficinasVO> listaOficinasVOs = new ArrayList<OficinasVO>();
	private OficinasVO oficinasVO = new OficinasVO();

	private List<EmpleadorVO> listaEmpleadores = new ArrayList<EmpleadorVO>();
	private EmpleadorVO empleadorVOSeleccionado = new EmpleadorVO();

	private List<ConveniosCapitacionVO> listaConveniosCapacitacion = new ArrayList<ConveniosCapitacionVO>();
	private ConveniosCapitacionVO convenioCapacitacionSeleccionado = new ConveniosCapitacionVO();

	private List<GrupoFamiliarVO> listaGrupoFamiliarVOs = new ArrayList<GrupoFamiliarVO>();
	private List<GrupoPoblacionalVO> listaGrupoPoblacionalVOs = new ArrayList<GrupoPoblacionalVO>();

	private GrupoFamiliarVO grupoFamiliarVOSeleccionado = new GrupoFamiliarVO();

	private ValidacionEspecialVO validacionEspecialVO = new ValidacionEspecialVO();

	private boolean mostrarPopupOficinas = false;
	private boolean mostrarPopupAfiliados = false;
	private boolean mostrarPopupTutelas = false;
	private boolean mostrarPopupRiesgos = false;
	private boolean consultaInternaAfiliado = false;
	private boolean deshabilitadoPorPermisos = true;
	private boolean deshabilitarPorNoAfiliado = true;
	private Integer consecutivoTipoIdentificacion;
	private Integer consecutivoPlan;
	private BigInteger numeroValidacion;
	private String numeroIdentificacion;
	private String primerNombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private Date fechaConsulta = Calendar.getInstance().getTime();
	private Date fechaActual = Calendar.getInstance().getTime();

	private String descripcionPlan;
	private String codigoTipoIdentificacion;

	

	private int marcaTranscribir = 1;

	/** IPS **/

	private boolean verIps = false;

	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {
			afiliadoVO.setPlan(new PlanVO());
			grupoFamiliarVOSeleccionado.setAfiliadoGrupoFamiliar(new AfiliadoVO());
			CargarCombosController cargarCombosController = new CargarCombosController();
			listaTiposIdentificacion = CargarCombosUtil.obtenerListaSelectTiposIdentificacion(null,
					cargarCombosController);
			listaPlanes = cargarCombosController.obtenerListaPlanes(null);
			deshabilitarOpciones(true);
			habilitarOpcionConsultaProgramacion(true);
			
			List<ParametrosGeneralesVO> listaParametrosGeneralesVOs = ((UsuarioBean) FacesUtils
					.getManagedBean("usuarioBean")).getListaParametrosGenerales();
			listaParametrosGeneralesVOs = ConsultaSolicitudesUtil.validarConsultaParametrosGenerales(listaParametrosGeneralesVOs);
			
			for (ParametrosGeneralesVO parametrosGeneralesVO : listaParametrosGeneralesVOs) {
				if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == 78) {
					FacesUtils.setSessionParameter(ConstantesWeb.MINIMA_FECHA_CONSULTA,
							Integer.parseInt(parametrosGeneralesVO.getValorParametroGeneral().toString()));
				}
				if (parametrosGeneralesVO.getConsecutivoCodigoParametroGeneral() == 79) {
					FacesUtils.setSessionParameter(ConstantesWeb.MINIMA_FECHA_SOLICITUD,
							Integer.parseInt(parametrosGeneralesVO.getValorParametroGeneral().toString()));
				}
			}

			deshabilitadoPorPermisos = false;

//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void limpiarConsultaAfiliado() {
		descripcionPlan = null;
		codigoTipoIdentificacion = null;
	}

	public void cancelarAfiliado() {
		afiliadoVO = new AfiliadoVO();
		afiliadoVO.setCiudadResidencia(new CiudadVO());
		afiliadoVO.setPlan(new PlanVO());
		afiliadoVO.setPlanComplementario(new PlanVO());
		afiliadoVO.setGenero(new GeneroVO());
		afiliadoVO.setTipoIdentificacionAfiliado(new TiposIdentificacionVO());
		oficinasVO = new OficinasVO();
		datosAfiliadoVO = new DatosAfiliadoVO();
		listaEmpleadores = null;
		listaConveniosCapacitacion = null;
		listaGrupoFamiliarVOs = null;
		listaGrupoPoblacionalVOs = null;
		listaTutelasAfiliado = null;
		listaRiesgosPaciente = null;
		consecutivoPlan = null;
		consecutivoTipoIdentificacion = null;
		numeroValidacion = null;
		numeroIdentificacion = null;
		primerNombre = null;
		segundoNombre = null;
		primerApellido = null;
		segundoApellido = null;
		fechaConsulta = Calendar.getInstance().getTime();
		deshabilitarPorNoAfiliado = true;
		grupoFamiliarVOSeleccionado = new GrupoFamiliarVO();
		grupoFamiliarVOSeleccionado.setAfiliadoGrupoFamiliar(new AfiliadoVO());
		deshabilitarOpciones(true);
		habilitarOpcionConsultaProgramacion(true);
	}

	public void consultarAfiliado() {
		try {
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales;
			validarFechaConsulta();
			validarConsecutivoPlan();

			FacesUtils.resetManagedBean("ingresoSolicitudBean");
			deshabilitarOpciones(true);
			if (consecutivoTipoIdentificacion != null && numeroIdentificacion != null
					&& !numeroIdentificacion.isEmpty()) {
				resultadoConsultaAfiliadoDatosAdicionales = consultarAfiliadoPorIdentificacion();
			} else if (primerNombre != null && !primerNombre.trim().isEmpty()) {
				validarPrimerApellido();
				resultadoConsultaAfiliadoDatosAdicionales = consultarAfiliadoPorNombres();
			} else {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_TIPO_NUMERO_IDENTIFICACION),
						ErrorType.PARAMETRO_ERRADO);
			}

			generarMensajeResultadoAfiliacion(resultadoConsultaAfiliadoDatosAdicionales);
			consultaInternaAfiliado = false;

			if (((MenuBean) FacesUtil.getBean(ConstantesWeb.MENU_BEAN)).isUsuarioIps()) {
				verIps = false;
				mostrarPopupTutelas = false;
				mostrarPopupRiesgos = false;
			} else {
				verIps = true;
			}

			deshabilitadoPorPermisos = habilitarCamposValidacion();
			deshabilitarOpciones(deshabilitadoPorPermisos);
			habilitarOpcionConsultaProgramacion(false);
			oficinasVO = new OficinasVO();
			validacionEspecialVO = new ValidacionEspecialVO();
			numeroValidacion = null;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	private boolean habilitarCamposValidacion() {
		return ConstantesEJB.getCodigosEstadosSinDerecho().contains(afiliadoVO.getConsecutivoEstadoAfiliado());
	}

	private void validarPrimerApellido() throws LogicException {
		if (primerApellido == null || primerApellido.trim().isEmpty()) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_CANTIDAD_MINIMA_NOMBRE_APELLIDO),
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	private void generarMensajeResultadoAfiliacion(
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales) {
		if (resultadoConsultaAfiliadoDatosAdicionales != null
				&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
				&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() != 0
				&& consecutivoTipoIdentificacion != null) {
			consultaDatosAdicionalesAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
		} else if (resultadoConsultaAfiliadoDatosAdicionales != null
				&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
				&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() == 0) {
			MostrarMensaje.mostrarMensaje(ConstantesWeb.AFILIADO_NO_EXISTE, COLOR_ROJO);
		} else if (listaAfiliadoVO == null || listaAfiliadoVO.isEmpty()) {
			MostrarMensaje.mostrarMensaje(ConstantesWeb.AFILIADO_NO_EXISTE, COLOR_ROJO);
		}
	}

	private void validarConsecutivoPlan() throws LogicException {
		if (consecutivoPlan == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PLAN), ""));
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PLAN),
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	private void validarFechaConsulta() throws LogicException {
		if (fechaConsulta == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHA_CONSULTA), ""));
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_FECHA_CONSULTA),
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	private ResultadoConsultaAfiliadoDatosAdicionales consultarAfiliadoPorIdentificacion() throws LogicException {
		ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales = null;
		listaAfiliadoVO = null;
		if (!consultaInternaAfiliado) {
			if (marcaTranscribir != 0) {
				buscarDescripcionPlan();
				buscarCodigoTipoIdentificacion();
			}
		}
		ConsultaAfiliadoController consultaAfiliadoController = getConsultaAfiliadoController();
		resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController.consultaRespuestaServicioAfiliado(
				codigoTipoIdentificacion, numeroIdentificacion, descripcionPlan, fechaConsulta,
				FacesUtils.getUserName());
		return resultadoConsultaAfiliadoDatosAdicionales;
	}

	private void buscarCodigoTipoIdentificacion() {
		for (SelectItem item : listaTiposIdentificacion) {
			int tempConsec = Integer.parseInt(item.getValue().toString());
			if (tempConsec == consecutivoTipoIdentificacion) {
				codigoTipoIdentificacion = item.getLabel();
				break;
			}
		}
	}

	private void buscarDescripcionPlan() {
		if (consecutivoPlan != null) {
			for (SelectItem item : listaPlanes) {
				int tempConsec = Integer.parseInt(item.getValue().toString());
				if (tempConsec == consecutivoPlan) {
					descripcionPlan = item.getLabel();
					break;
				}
			}
		}
	}

	private ResultadoConsultaAfiliadoDatosAdicionales consultarAfiliadoPorNombres() throws LogicException {
		ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales = null;
		ConsultaAfiliadoController consultaAfiliadoController = getConsultaAfiliadoController();

		listaAfiliadoVO = consultaAfiliadoController.buscaAfiliadoxNombre(primerNombre, segundoNombre, primerApellido,
				segundoApellido, FacesUtils.getUserName());
		if (listaAfiliadoVO != null && !listaAfiliadoVO.isEmpty() && listaAfiliadoVO.size() == 1) {
			afiliadoVO = listaAfiliadoVO.get(0);
			descripcionPlan = afiliadoVO.getPlan().getDescripcionPlan();
			codigoTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion();
			numeroIdentificacion = afiliadoVO.getNumeroIdentificacion();
			buscarConsecutivoTipoIdentificacionEnLista(codigoTipoIdentificacion);
			resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController.consultaRespuestaServicioAfiliado(
					codigoTipoIdentificacion, numeroIdentificacion, descripcionPlan, fechaConsulta,
					FacesUtils.getUserName());
		} else if (listaAfiliadoVO != null && !listaAfiliadoVO.isEmpty()) {
			mostrarPopupAfiliados = true;
		}
		return resultadoConsultaAfiliadoDatosAdicionales;
	}

	private void deshabilitarOpciones(boolean valor) {
		List<ModuloVO> modulos = ((MenuBean) FacesUtil.getBean("menuBean")).getModulos();
		if (modulos != null) {
			for (ModuloVO moduloVO : modulos) {
				deshabilitarOpcionesIngresarSolicitud(valor, moduloVO);
			}
		}
	}

	private void deshabilitarOpcionesIngresarSolicitud(boolean valor, ModuloVO moduloVO) {
		for (ServicioVO servicioVO : moduloVO.getServicios()) {
			if (ConstantesWeb.OPCION_INGRESAR_SOLICITUD.equals(servicioVO.getAction().trim())) {
				servicioVO.setDeshabilitado(valor);
				break;
			}
		}
	}

	private void habilitarOpcionConsultaProgramacion(boolean valor) {
		List<ModuloVO> modulos = ((MenuBean) FacesUtil.getBean("menuBean")).getModulos();
		for (ModuloVO moduloVO : modulos) {
			for (ServicioVO servicioVO : moduloVO.getServicios()) {
				if (ConstantesWeb.OPCION_PROGRAMACION_ENTREGA.equals(servicioVO.getAction().trim())) {
					servicioVO.setDeshabilitado(valor);
					break;
				}
			}
		}

	}

	private void buscarConsecutivoTipoIdentificacionEnLista(String codigoTipoIdentificacion) {
		for (SelectItem item : listaTiposIdentificacion) {
			if (codigoTipoIdentificacion.equals(item.getLabel())) {
				consecutivoTipoIdentificacion = Integer.parseInt(item.getValue().toString());
				break;
			}
		}
	}

	public void consultarOficina() {
		try {
			listaOficinasVOs = null;
			validarCodigoOficina();
			cargarListaOficinasPorCodigo();
			oficinasVO = new OficinasVO();
			if (listaOficinasVOs != null && !listaOficinasVOs.isEmpty() && listaOficinasVOs.size() == 1) {
				oficinasVO = listaOficinasVOs.get(0);
			} else if (listaOficinasVOs != null && !listaOficinasVOs.isEmpty()) {
				mostrarPopupOficinas = true;
			}
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	private void validarCodigoOficina() throws LogicException {
		if (oficinasVO.getCodigoOficina() == null && oficinasVO.getDescripcionOficina() == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_CONSULTA_OFICINA),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	private void cargarListaOficinasPorCodigo() throws LogicException {
		CargarCombosController cargarCombosController;
		try {
			cargarCombosController = new CargarCombosController();
		} catch (Exception e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

		if (oficinasVO.getCodigoOficina() != null && !oficinasVO.getCodigoOficina().trim().isEmpty()) {
			listaOficinasVOs = cargarCombosController.consultaOficinasxCodigo(oficinasVO.getCodigoOficina());
		} else if (oficinasVO.getDescripcionOficina() != null && !oficinasVO.getDescripcionOficina().trim().isEmpty()) {
			listaOficinasVOs = cargarCombosController.consultaOficinasxDescripcion(oficinasVO.getDescripcionOficina());
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_CONSULTA_OFICINA),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	/**
	 * Metodo validacion especial
	 */
	public void validacionEspecial() {
		try {
			validacionCamposAfiliado();
			validacionNumeroDeValidacion();
			validacionConsecutivoCodigoOficina();
			deshabilitarOpciones(true);

			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();

			validacionEspecialVO = consultaAfiliadoController.validarNumeroAutorizacionAfiliado(
					afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion(),
					afiliadoVO.getNumeroIdentificacion());

			if (!validacionEspecialVO.getNumeroValidacion().equals(numeroValidacion)
					|| !validacionEspecialVO.getOficinasVO().getCodigoOficina().equals(oficinasVO.getCodigoOficina())) {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_VALIDACION_NUM_VERIFICACION_INCORRECTO),
						LogicException.ErrorType.PARAMETRO_ERRADO);
			}
			afiliadoVO.setValEspecial(ConstantesWeb.SI);
			deshabilitarOpciones(false);
			deshabilitadoPorPermisos = false;
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	private void validacionConsecutivoCodigoOficina() throws LogicException {
		if (oficinasVO.getConsecutivoCodigoOficina() == null
				|| oficinasVO.getConsecutivoCodigoOficina().intValue() == 0) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESPECIAL_OFICINA),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	private void validacionNumeroDeValidacion() throws LogicException {
		if (numeroValidacion == null) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESPECIAL_AFILIADO_CAMPO_OBLIGATORIO),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}

		if (numeroValidacion.toString().length() < 3 || numeroValidacion.toString().length() > 18) {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESPECIAL_AFILIADO_MIN_MAX_CARACTERES),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	private void validacionCamposAfiliado() throws LogicException {
		if (afiliadoVO.getTipoIdentificacionAfiliado() == null
				|| afiliadoVO.getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion() == null
				|| afiliadoVO.getNumeroIdentificacion() == null) {
			throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_ESPECIAL_AFILIADO),
					LogicException.ErrorType.PARAMETRO_ERRADO);
		}
	}

	public void cerrarPopupOficinas() {
		mostrarPopupOficinas = false;
	}

	public void cerrarPopupAfiliados() {
		mostrarPopupAfiliados = false;
	}

	public void seleccionarAfiliado() {
		descripcionPlan = afiliadoVO.getPlan().getDescripcionPlan();
		codigoTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion();
		numeroIdentificacion = afiliadoVO.getNumeroIdentificacion();
		buscarConsecutivoTipoIdentificacionEnLista(codigoTipoIdentificacion);
		consultaInternaAfiliado = true;
		consultarAfiliado();
		mostrarPopupAfiliados = false;
	}

	public void seleccionarGrupoFamiliar() {
		try {
			if (grupoFamiliarVOSeleccionado == null || grupoFamiliarVOSeleccionado.getAfiliadoGrupoFamiliar() == null
					|| grupoFamiliarVOSeleccionado.getAfiliadoGrupoFamiliar().getNombreCompleto() == null) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_SELECCION_GRUPO_FAMILIAR),
						LogicException.ErrorType.PARAMETRO_ERRADO);
			}
			afiliadoVO = grupoFamiliarVOSeleccionado.getAfiliadoGrupoFamiliar();
			descripcionPlan = afiliadoVO.getPlan().getDescripcionPlan();
			codigoTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion();
			numeroIdentificacion = afiliadoVO.getNumeroIdentificacion();
			consecutivoTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado()
					.getConsecutivoTipoIdentificacion();
			consultaInternaAfiliado = true;
			consultarAfiliado();
			grupoFamiliarVOSeleccionado = new GrupoFamiliarVO();
			grupoFamiliarVOSeleccionado.setAfiliadoGrupoFamiliar(new AfiliadoVO());
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	public void consultaDatosAdicionalesAfiliado(
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales) {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			quitarEspaciosEnBlancoDatosPersonales(resultadoConsultaAfiliadoDatosAdicionales);
			datosAfiliadoVO = consultaAfiliadoController
					.consultaDatosAdicionalesAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
			cargarListasAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
			deshabilitarPorNoAfiliado = false;
			validarMostrarPopupTutelas();
			deshabilitarOpciones(true);

		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), COLOR_ROJO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ERROR_EXCEPCION_INESPERADA), COLOR_ROJO);
		}
	}

	private void quitarEspaciosEnBlancoDatosPersonales(
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales) throws LogicException {

		ConsultaAfiliadoController consultaAfiliadoController = getConsultaAfiliadoController();

		afiliadoVO = consultaAfiliadoController.consultaInformacionAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
		primerNombre = afiliadoVO.getPrimerNombre() != null ? afiliadoVO.getPrimerNombre().trim() : null;
		segundoNombre = afiliadoVO.getSegundoNombre() != null ? afiliadoVO.getSegundoNombre().trim() : null;
		primerApellido = afiliadoVO.getPrimerApellido() != null ? afiliadoVO.getPrimerApellido().trim() : null;
		segundoApellido = afiliadoVO.getSegundoApellido() != null ? afiliadoVO.getSegundoApellido().trim() : null;
	}

	private void validarMostrarPopupTutelas() {
		if (listaTutelasAfiliado != null && !listaTutelasAfiliado.isEmpty()) {
			mostrarPopupTutelas = true;
		}
		if (listaRiesgosPaciente != null && !listaRiesgosPaciente.isEmpty()) {
			mostrarPopupRiesgos = true;
		}
	}

	private void cargarListasAfiliado(
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales) throws LogicException {

		ConsultaAfiliadoController consultaAfiliadoController = getConsultaAfiliadoController();

		listaEmpleadores = consultaAfiliadoController
				.consultaInformacionEmpleadorAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
		listaConveniosCapacitacion = consultaAfiliadoController
				.consultaConveniosCapitacion(resultadoConsultaAfiliadoDatosAdicionales);
		listaGrupoFamiliarVOs = consultaAfiliadoController.consultaGrupoFamiliarAfiliado(consecutivoTipoIdentificacion,
				numeroIdentificacion);
		listaGrupoPoblacionalVOs = consultaAfiliadoController
				.consultaGrupoPoblacional(afiliadoVO.getNumeroUnicoIdentificacion());
		listaTutelasAfiliado = consultaAfiliadoController.consultaTutelasAfiliado(consecutivoTipoIdentificacion,
				numeroIdentificacion);
		listaRiesgosPaciente = consultaAfiliadoController.consultarRiesgosPaciente(consecutivoTipoIdentificacion,
				numeroIdentificacion);
	}

	private ConsultaAfiliadoController getConsultaAfiliadoController() throws LogicException {
		ConsultaAfiliadoController consultaAfiliadoController;
		try {
			consultaAfiliadoController = new ConsultaAfiliadoController();
		} catch (Exception e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return consultaAfiliadoController;
	}

	public void visualizarPopupTutelas() {
		if (afiliadoVO != null && afiliadoVO.getNumeroUnicoIdentificacion() != null) {
			mostrarPopupTutelas = true;
		}
	}

	public void cerrarPopupTutelas() {
		mostrarPopupTutelas = false;
	}

	public void visualizarPopupRiesgos() {
		if (afiliadoVO != null && afiliadoVO.getNumeroUnicoIdentificacion() != null) {
			mostrarPopupRiesgos = true;
		}
	}

	public void cerrarPopupRiesgos() {
		mostrarPopupRiesgos = false;
	}

	public List<SelectItem> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public void setListaTiposIdentificacion(List<SelectItem> listaTiposIdentificacion) {
		this.listaTiposIdentificacion = listaTiposIdentificacion;
	}

	public List<SelectItem> getListaPlanes() {
		return listaPlanes;
	}

	public void setListaPlanes(List<SelectItem> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}

	public Integer getConsecutivoTipoIdentificacion() {
		return consecutivoTipoIdentificacion;
	}

	public void setConsecutivoTipoIdentificacion(Integer consecutivoTipoIdentificacion) {
		this.consecutivoTipoIdentificacion = consecutivoTipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/**
	 * @return the listaAfiliadoVO
	 */
	public List<AfiliadoVO> getListaAfiliadoVO() {
		return listaAfiliadoVO;
	}

	/**
	 * @param listaAfiliadoVO
	 *            the listaAfiliadoVO to set
	 */
	public void setListaAfiliadoVO(List<AfiliadoVO> listaAfiliadoVO) {
		this.listaAfiliadoVO = listaAfiliadoVO;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	/**
	 * @return the consecutivoPlan
	 */
	public Integer getConsecutivoPlan() {
		return consecutivoPlan;
	}

	/**
	 * @param consecutivoPlan
	 *            the consecutivoPlan to set
	 */
	public void setConsecutivoPlan(Integer consecutivoPlan) {
		this.consecutivoPlan = consecutivoPlan;
	}

	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	/**
	 * @return the afiliadoVO
	 */
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	/**
	 * @param afiliadoVO
	 *            the afiliadoVO to set
	 */
	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	/**
	 * @return the fechaActual
	 */
	public Date getFechaActual() {
		return fechaActual;
	}

	/**
	 * @param fechaActual
	 *            the fechaActual to set
	 */
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	/**
	 * @return the numeroValidacion
	 */
	public BigInteger getNumeroValidacion() {
		return numeroValidacion;
	}

	/**
	 * @param numeroValidacion
	 *            the numeroValidacion to set
	 */
	public void setNumeroValidacion(BigInteger numeroValidacion) {
		this.numeroValidacion = numeroValidacion;
	}

	/**
	 * @return the oficinasVO
	 */
	public OficinasVO getOficinasVO() {
		return oficinasVO;
	}

	/**
	 * @param oficinasVO
	 *            the oficinasVO to set
	 */
	public void setOficinasVO(OficinasVO oficinasVO) {
		this.oficinasVO = oficinasVO;
	}

	/**
	 * @return the listaOficinasVOs
	 */
	public List<OficinasVO> getListaOficinasVOs() {
		return listaOficinasVOs;
	}

	/**
	 * @param listaOficinasVOs
	 *            the listaOficinasVOs to set
	 */
	public void setListaOficinasVOs(List<OficinasVO> listaOficinasVOs) {
		this.listaOficinasVOs = listaOficinasVOs;
	}

	/**
	 * @return the mostrarPopupOficinas
	 */
	public boolean isMostrarPopupOficinas() {
		return mostrarPopupOficinas;
	}

	/**
	 * @param mostrarPopupOficinas
	 *            the mostrarPopupOficinas to set
	 */
	public void setMostrarPopupOficinas(boolean mostrarPopupOficinas) {
		this.mostrarPopupOficinas = mostrarPopupOficinas;
	}

	public List<EmpleadorVO> getListaEmpleadores() {
		return listaEmpleadores;
	}

	public void setListaEmpleadores(List<EmpleadorVO> listaEmpleadores) {
		this.listaEmpleadores = listaEmpleadores;
	}

	public List<ConveniosCapitacionVO> getListaConveniosCapacitacion() {
		return listaConveniosCapacitacion;
	}

	public void setListaConveniosCapacitacion(List<ConveniosCapitacionVO> listaConveniosCapacitacion) {
		this.listaConveniosCapacitacion = listaConveniosCapacitacion;
	}

	/**
	 * @return the mostrarPopupAfiliados
	 */
	public boolean isMostrarPopupAfiliados() {
		return mostrarPopupAfiliados;
	}

	/**
	 * @param mostrarPopupAfiliados
	 *            the mostrarPopupAfiliados to set
	 */
	public void setMostrarPopupAfiliados(boolean mostrarPopupAfiliados) {
		this.mostrarPopupAfiliados = mostrarPopupAfiliados;
	}

	/**
	 * @return the listaGrupoFamiliarVOs
	 */
	public List<GrupoFamiliarVO> getListaGrupoFamiliarVOs() {
		return listaGrupoFamiliarVOs;
	}

	/**
	 * @param listaGrupoFamiliarVOs
	 *            the listaGrupoFamiliarVOs to set
	 */
	public void setListaGrupoFamiliarVOs(List<GrupoFamiliarVO> listaGrupoFamiliarVOs) {
		this.listaGrupoFamiliarVOs = listaGrupoFamiliarVOs;
	}

	/**
	 * @return the grupoFamiliarVOSeleccionado
	 */
	public GrupoFamiliarVO getGrupoFamiliarVOSeleccionado() {
		return grupoFamiliarVOSeleccionado;
	}

	/**
	 * @param grupoFamiliarVOSeleccionado
	 *            the grupoFamiliarVOSeleccionado to set
	 */
	public void setGrupoFamiliarVOSeleccionado(GrupoFamiliarVO grupoFamiliarVOSeleccionado) {
		this.grupoFamiliarVOSeleccionado = grupoFamiliarVOSeleccionado;
	}

	/**
	 * @return the datosAfiliadoVO
	 */
	public DatosAfiliadoVO getDatosAfiliadoVO() {
		return datosAfiliadoVO;
	}

	/**
	 * @param datosAfiliadoVO
	 *            the datosAfiliadoVO to set
	 */
	public void setDatosAfiliadoVO(DatosAfiliadoVO datosAfiliadoVO) {
		this.datosAfiliadoVO = datosAfiliadoVO;
	}

	/**
	 * @return the listaGrupoPoblacionalVOs
	 */
	public List<GrupoPoblacionalVO> getListaGrupoPoblacionalVOs() {
		return listaGrupoPoblacionalVOs;
	}

	/**
	 * @param listaGrupoPoblacionalVOs
	 *            the listaGrupoPoblacionalVOs to set
	 */
	public void setListaGrupoPoblacionalVOs(List<GrupoPoblacionalVO> listaGrupoPoblacionalVOs) {
		this.listaGrupoPoblacionalVOs = listaGrupoPoblacionalVOs;
	}

	/**
	 * @return the empleadorVOSeleccionado
	 */
	public EmpleadorVO getEmpleadorVOSeleccionado() {
		return empleadorVOSeleccionado;
	}

	/**
	 * @param empleadorVOSeleccionado
	 *            the empleadorVOSeleccionado to set
	 */
	public void setEmpleadorVOSeleccionado(EmpleadorVO empleadorVOSeleccionado) {
		this.empleadorVOSeleccionado = empleadorVOSeleccionado;
	}

	/**
	 * @return the convenioCapacitacionSeleccionado
	 */
	public ConveniosCapitacionVO getConvenioCapacitacionSeleccionado() {
		return convenioCapacitacionSeleccionado;
	}

	/**
	 * @param convenioCapacitacionSeleccionado
	 *            the convenioCapacitacionSeleccionado to set
	 */
	public void setConvenioCapacitacionSeleccionado(ConveniosCapitacionVO convenioCapacitacionSeleccionado) {
		this.convenioCapacitacionSeleccionado = convenioCapacitacionSeleccionado;
	}

	/**
	 * @return the listaTutelasAfiliado
	 */
	public List<TutelasAfiliadoVO> getListaTutelasAfiliado() {
		return listaTutelasAfiliado;
	}

	/**
	 * @param listaTutelasAfiliado
	 *            the listaTutelasAfiliado to set
	 */
	public void setListaTutelasAfiliado(List<TutelasAfiliadoVO> listaTutelasAfiliado) {
		this.listaTutelasAfiliado = listaTutelasAfiliado;
	}

	/**
	 * @return the listaRiesgosPaciente
	 */
	public List<RiesgosPacienteVO> getListaRiesgosPaciente() {
		return listaRiesgosPaciente;
	}

	/**
	 * @param listaRiesgosPaciente
	 *            the listaRiesgosPaciente to set
	 */
	public void setListaRiesgosPaciente(List<RiesgosPacienteVO> listaRiesgosPaciente) {
		this.listaRiesgosPaciente = listaRiesgosPaciente;
	}

	/**
	 * @return the tutelaSeleccionada
	 */
	public TutelasAfiliadoVO getTutelaSeleccionada() {
		return tutelaSeleccionada;
	}

	/**
	 * @param tutelaSeleccionada
	 *            the tutelaSeleccionada to set
	 */
	public void setTutelaSeleccionada(TutelasAfiliadoVO tutelaSeleccionada) {
		this.tutelaSeleccionada = tutelaSeleccionada;
	}

	/**
	 * @return the riesgoPacienteSeleccionado
	 */
	public RiesgosPacienteVO getRiesgoPacienteSeleccionado() {
		return riesgoPacienteSeleccionado;
	}

	/**
	 * @param riesgoPacienteSeleccionado
	 *            the riesgoPacienteSeleccionado to set
	 */
	public void setRiesgoPacienteSeleccionado(RiesgosPacienteVO riesgoPacienteSeleccionado) {
		this.riesgoPacienteSeleccionado = riesgoPacienteSeleccionado;
	}

	/**
	 * @return the mostrarPopupTutelas
	 */
	public boolean isMostrarPopupTutelas() {
		return mostrarPopupTutelas;
	}

	/**
	 * @param mostrarPopupTutelas
	 *            the mostrarPopupTutelas to set
	 */
	public void setMostrarPopupTutelas(boolean mostrarPopupTutelas) {
		this.mostrarPopupTutelas = mostrarPopupTutelas;
	}

	/**
	 * @return the mostrarPopupRiesgos
	 */
	public boolean isMostrarPopupRiesgos() {
		return mostrarPopupRiesgos;
	}

	/**
	 * @param mostrarPopupRiesgos
	 *            the mostrarPopupRiesgos to set
	 */
	public void setMostrarPopupRiesgos(boolean mostrarPopupRiesgos) {
		this.mostrarPopupRiesgos = mostrarPopupRiesgos;
	}

	public String getCodigoTipoIdentificacion() {
		return codigoTipoIdentificacion;
	}

	public void setCodigoTipoIdentificacion(String codigoTipoIdentificacion) {
		this.codigoTipoIdentificacion = codigoTipoIdentificacion;
	}

	public boolean isDeshabilitadoPorPermisos() {
		return deshabilitadoPorPermisos;
	}

	public void setDeshabilitadoPorPermisos(boolean deshabilitadoPorPermisos) {
		this.deshabilitadoPorPermisos = deshabilitadoPorPermisos;
	}

	public boolean isDeshabilitarPorNoAfiliado() {
		return deshabilitarPorNoAfiliado;
	}

	public void setDeshabilitarPorNoAfiliado(boolean deshabilitarPorNoAfiliado) {
		this.deshabilitarPorNoAfiliado = deshabilitarPorNoAfiliado;
	}

	public ValidacionEspecialVO getValidacionEspecialVO() {
		return validacionEspecialVO;
	}

	public String getDescripcionPlan() {
		return descripcionPlan;
	}

	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}

	public int getMarcaTranscribir() {
		return marcaTranscribir;
	}

	public void setMarcaTranscribir(int marcaTranscribir) {
		this.marcaTranscribir = marcaTranscribir;
	}

	public boolean isVerIps() {
		return verIps;
	}

	public void setVerIps(boolean verIps) {
		this.verIps = verIps;
	}

}
