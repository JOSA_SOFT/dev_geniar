package com.sos.gestionautorizacionessaludweb.bpm.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.GrupoAuditorVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.HospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionAuditoriaIntegralController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionContratacionController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionDomiciliariaController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionMedicinaTrabajoController;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.PrestacionController;
import com.sos.gestionautorizacionessaludejb.controller.PrestadorController;
import com.sos.gestionautorizacionessaludejb.controller.RegistrarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.SolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.gestionautorizacionessaludweb.bean.IngresoSolicitudBean;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesMedicoPrestadorUtil;
import com.sos.gestionautorizacionessaludweb.util.IngresoSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

import co.eps.sos.dataccess.exception.DataAccessException;
import co.eps.sos.service.exception.ServiceException;
import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

/**
 * Class GestionInconsistenciasBean
 * 
 * @author ing. Rafael Cano
 * @version 11/02/2016
 *
 */

public class GestionContratacionBean implements Serializable {
	private static final long serialVersionUID = 8616049744791679760L;

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionContratacionBean.class);

	/**
	 * Variable gestion de mensajes
	 */
	

	private static final String PARAMETRO_CUPS = "consecutivoCups";
	private static final String PARAMETRO_CUMS = "consecutivoCums";

	private boolean mostrarPopupGestion = false;

	private PrestacionesAprobadasVO prestacionesAprobadasDTO;
	private PrestacionesAprobadasVO prestacionSel;

	/** Informacion de las Empleadores **/
	private List<EmpleadorVO> listaEmpleadores;

	/** Informacion convenios capitacion */
	private List<ConveniosCapitacionVO> listaConveniosCapitacion;

	/** Informacion de Prestaciones Aprobadas **/
	private List<PrestacionesAprobadasVO> listaPrestacionesAprobadas;

	/**
	 * Informacion de los Tipos de Prestaciones private
	 * RegistrarSolicitudController registrarSolicitudesController;
	 */
	IngresoSolicitudBean ingresoSolicitudBean;

	private List<SelectItem> listTipoPrestaciones;

	private AfiliadoVO afiliadoVO;
	private SolicitudBpmVO solicitudVO;
	private List<SelectItem> lstPrestadoresDireccionamiento;
	private List<SelectItem> listaGruposAuditoresContratacion;

	private Integer idSolicitud;
	private String idTask;
	private ClienteRestBpmBean clienteRestBpmBean;
	private GestionAuditoriaIntegralBean gestionAuditoriaIntegralBean;

	private MedicamentosVO medicamentoVOSeleccionado;
	private DiagnosticosVO diagnosticoVoSeleccionado;
	private boolean mostrarPopupCUPS;
	private boolean mostrarPopupMedicamentos;
	private boolean mostrarPopupCofirmacion;
	private boolean mostrarPopupInformacion;
	private boolean validacionNombre = false;
	private boolean validacionCodigo = false;
	private boolean mostrarPopupDiagnosticos;
	private boolean mostrarPopupCofirmacionLiberarContratacion;
	private boolean mostrarPopupDocumentosSoporteAuditoria;
	private boolean mostrarPopupVerDetalleAfiliadoContratacion;
	private boolean checkDiagnosticoPpal;
	private boolean checkDiagnosticoSOS;
	private boolean mostrarPopupReliquidar;
	private boolean mostrarPopupDireccionamiento;
	private List<MedicamentosVO> listaMedicamentosEncontrados;
	private ProcedimientosVO procedimientoVOSeleccionado;
	private List<ProcedimientosVO> listaCUPSEncontrados;
	private String consecutivoCUMS;
	private String consecutivoCUPS;
	private String descripcionGrupoAuditor;
	private String userBpmContratacion;
	private String txtCodigoDiagnostico;
	private String txtDescripcionDiagnostico;
	private List<SelectItem> lstRiesgos;
	private String altoRiesgo;
	private Date fechaConsulta;
	private String consecutivoPrestadorTemp;
	private ConveniosCapitacionVO convenioCapacitacionSeleccionado;
	private String fechaSolicitud;
	private List<DiagnosticoDTO> listaDiagnosticosSeleccionados;
	private List<SelectItem> lstPrestacionesReasignarContratacion = new ArrayList<SelectItem>();
	private String observacionesReasignarContratacion;
	private List<ConceptosGastoVO> listaConceptosGastoVO = new ArrayList<ConceptosGastoVO>();

	private DiagnosticoDTO diagnosticoDTOSeleccionado;
	private boolean mostrarHospitalizacion;
	private HospitalizacionVO hospitalizacionContratacion;
	private boolean mostrarPopupHospitalizacionContratacion;
	private boolean mostrarPopupVerDetalleSolicitudContratacion;
	private List<DocumentoSoporteVO> listaDocumentosAuditoria;
	private DocumentoSoporteVO documentoSoporteVOSeleccionadoContratacion;
	private List<DiagnosticosVO> listaDiagnosticosEncontrados;
	private Integer prestacionesReasignarContratacion;
	private List<PrestadorVO> listaPrestadores;
	private List<PrestadorVO> listaPrestadoresCriterio;
	private PrestadorVO prestadoresVo;
	private PrestadorVO buscarPrestador;
	
	private List<CiudadVO> listaCiudadesDireccionamiento = new ArrayList<CiudadVO>();
	private CiudadVO ciudad;
	private boolean mostrarPopupCiudades;	
	private boolean activarFiltroCiudad;


	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		try {

			inicializarVariables();

			idTask = (String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
			String idSolicitudStr = (String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
			userBpmContratacion = (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);
			String tipoAuditor = (String) FacesUtil.getRequestParameter(ConstantesWeb.TIPO_AUDITOR);
			convenioCapacitacionSeleccionado = new ConveniosCapitacionVO();

			this.idSolicitud = new Integer(idSolicitudStr);
			clienteRestBpmBean = (ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
			gestionAuditoriaIntegralBean = (GestionAuditoriaIntegralBean) FacesUtil
					.getBean(ConstantesWeb.NOMBRE_BEAN_AUDITORIA_INTEGRAL);

			if (tipoAuditor.equals(ConstantesWeb.ID_CONTRATACION)) {
				consecutivoCUMS = Messages.getValorParametro(PARAMETRO_CUMS);
				consecutivoCUPS = Messages.getValorParametro(PARAMETRO_CUPS);

				afiliadoVO = ((GestionInconsistenciasBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS))
						.getDatosBasicosAfiliado() == null ? afiliadoVO
								: ((GestionInconsistenciasBean) FacesUtil
										.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getDatosBasicosAfiliado();
				solicitudVO = ((GestionInconsistenciasBean) FacesUtil
						.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getSolicitudVO() == null ? solicitudVO
								: ((GestionInconsistenciasBean) FacesUtil
										.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getSolicitudVO();

				GestionContratacionController gestionContratacionController = new GestionContratacionController();
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				CargarCombosController cargarCombosController = new CargarCombosController();
				GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController();
				ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();

				listaEmpleadores = gestionContratacionController.consultaEmpleadores(solicitudVO.getNumeroSolicitud());
				listTipoPrestaciones = gestionContratacionController.consultarTiposPrestaciones();
				listaGruposAuditoresContratacion = obtenerListaSelectGruposAuditoresContratacion(
						gestionDomiciliariaController.consultarGruposAuditores(solicitudVO.getNumeroSolicitud(),
								ConstantesWeb.CONS_INCONSISTENCIA_PRESTADOR));
				listaPrestacionesAprobadas = gestionContratacionController
						.consultaPrestacionesAprobadas(solicitudVO.getNumeroSolicitud(), "");
				lstRiesgos = FuncionesAppWeb
						.obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());
				altoRiesgo = afiliadoVO.isAltoRiesgo()? ConstantesWeb.ALTO_RIESGO : "";
				SimpleDateFormat formatoFecha = new SimpleDateFormat(ConstantesWeb.FORMATO_FECHA);
				fechaSolicitud = formatoFecha.format(solicitudVO.getFechaSolicitud());
				listaDiagnosticosSeleccionados = gestionMedicinaTrabajoController.consultaDiagnosticos(idSolicitud);

				consecutivoPrestadorTemp = "";
				llenarListadoPrestacionesSolicitadasContratacion();
				listaConceptosGastoVO = consultarSolicitudControllerAuditoria
						.consultaConceptosGastosxSolicitud(idSolicitud);
				if (ConstantesWeb.CODIGO_ORIGEN_ATENCION_HOSPITALIZACION
						.equals(solicitudVO.getCodigoOrigenAtencion())) {
					mostrarHospitalizacion = true;
				}
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void inicializarVariables() {
		try {
			prestacionSel = new PrestacionesAprobadasVO();
			new ControladorVisosServiceEJB();
			listaDocumentosAuditoria = new ArrayList<DocumentoSoporteVO>();
			listaDiagnosticosEncontrados = new ArrayList<DiagnosticosVO>();

			listTipoPrestaciones = new ArrayList<SelectItem>();
			lstPrestadoresDireccionamiento = new ArrayList<SelectItem>();
			listaPrestacionesAprobadas = new ArrayList<PrestacionesAprobadasVO>();
			listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
			listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
			listaEmpleadores = new ArrayList<EmpleadorVO>();
			listaGruposAuditoresContratacion = new ArrayList<SelectItem>();
			listaDiagnosticosSeleccionados = new ArrayList<DiagnosticoDTO>();
			listaPrestadores = new ArrayList<PrestadorVO>();
			listaPrestadoresCriterio = new ArrayList<PrestadorVO>();
			buscarPrestador = new PrestadorVO();
			solicitudVO = new SolicitudBpmVO();
			afiliadoVO = new AfiliadoVO();
			ciudad = new CiudadVO();
			mostrarPopupCofirmacion = false;
			mostrarPopupInformacion = false;
			mostrarPopupCofirmacionLiberarContratacion = false;
			hospitalizacionContratacion = new HospitalizacionVO();
			fechaConsulta = new Date();			
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para terminar la solicitu */
	public String terminarTarea() {
		String url = "";
		try {
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.REASIGNAR_TAREA,
					ConstantesWeb.GRUPO_AUDITORIA);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
			return url;
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/** Metodo para liberar la solicitu */
	public String liberarTareaContratacion() {
		String url = "";
		try {
			clienteRestBpmBean.liberarTarea(idTask, idSolicitud.toString(), ConstantesWeb.LIBERAR_TAREA,
					ConstantesWeb.CARACTER_SEPARADOR);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/** Metodo para convertir una lista de valores en una lista desplegable */
	public List<SelectItem> obtenerListaSelectGruposAuditoresContratacion(List<GrupoAuditorVO> listaVosContratacion)
			throws LogicException {
		List<SelectItem> listaContratacionItems = new ArrayList<SelectItem>();
		if (listaVosContratacion != null && !listaVosContratacion.isEmpty()) {
			SelectItem itemMedicina;
			for (GrupoAuditorVO auditor : listaVosContratacion) {
				itemMedicina = new SelectItem(auditor.getCodigoGrupoAuditor(), auditor.getDescGrupoAuditor());
				listaContratacionItems.add(itemMedicina);
			}
		}
		return listaContratacionItems;
	}

	/** Metodo para reasignar la solicitud */
	public String reasignarContratacion() {
		String url = "";
		try {
			for (int i = 0; i < listaGruposAuditoresContratacion.size(); i++) {
				if (listaGruposAuditoresContratacion.get(i).getValue().toString().equals(descripcionGrupoAuditor)) {
					descripcionGrupoAuditor = listaGruposAuditoresContratacion.get(i).getLabel();
					break;
				}
			}

			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud.toString(), ConstantesWeb.REASIGNAR_TAREA,
					descripcionGrupoAuditor);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
			return url;
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	public void mostrarConfirmacionContratacion() {
		if (descripcionGrupoAuditor == null) {
			mostrarPopupCofirmacion = false;
			mostrarPopupInformacion = true;
		} else {
			mostrarPopupCofirmacion = true;
			mostrarPopupInformacion = false;
		}
	}

	public void mostrarConfirmacionLiberarContratacion() {
		mostrarPopupCofirmacionLiberarContratacion = true;
		mostrarPopupReliquidar = true;
	}

	public void cerrarPopupReasignarSolicitudContratacion() {
		mostrarPopupReliquidar = false;
	}

	public void cerrarConfirmacionContratacion() {
		mostrarPopupCofirmacion = false;
	}

	public void cerrarInformacionContratacion() {
		mostrarPopupInformacion = false;
	}

	public void cerrarConfirmacionLiberarContratacion() {
		mostrarPopupCofirmacionLiberarContratacion = false;
	}

	/**
	 * 
	 */
	public void mostrarGestion() {
		try {
			PrestadorController prestadorController = new PrestadorController();
			listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(
					prestacionesAprobadasDTO.getConscodigoPrestacion(),
					new Integer(prestacionesAprobadasDTO.getPlanAfiliado()),
					prestacionesAprobadasDTO.getConsecutivoCiudad(), solicitudVO.getNumeroSolicitud());

			lstPrestadoresDireccionamiento = obtenerListaSelectPrestadoresDireccionamiento(listaPrestadores);

			listaPrestadoresCriterio = listaPrestadores;
			prestacionSel.setCodigoTipoPrestacion(prestacionesAprobadasDTO.getCodigoTipoPrestacion());
			prestacionSel.setCodigoPrestacion(prestacionesAprobadasDTO.getCodigoPrestacion());
			prestacionSel.setConscodigoPrestacion(prestacionesAprobadasDTO.getConscodigoPrestacion());
			prestacionSel.setDescripcion(prestacionesAprobadasDTO.getDescripcion());
			if (prestacionesAprobadasDTO.getDireccionamiento() != null) {
				prestacionSel.setConsecutivoPrestador(prestacionesAprobadasDTO.getDireccionamiento().trim());
				prestacionSel.setDescDireccionamiento(prestacionesAprobadasDTO.getDescDireccionamiento());
				consecutivoPrestadorTemp = prestacionesAprobadasDTO.getDireccionamiento().trim();
			}
			prestacionSel.setObservaciones(prestacionesAprobadasDTO.getObservaciones());
			prestacionSel.setConscodigoPrestacionGSA(prestacionesAprobadasDTO.getConscodigoPrestacionGSA());

			mostrarPopupGestion = true;

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	public void guardarGestion() {
		try {

			if (prestacionSel.getCodigoTipoPrestacion() != null && prestacionSel.getConsecutivoPrestador() != null
					&& prestacionSel.getConscodigoPrestacionGSA() != null
					&& prestacionSel.getConscodigoPrestacion() != null) {
				GestionContratacionController gestionContratacionController = new GestionContratacionController();

				gestionContratacionController.guardarGestionPrestacion(
						Integer.parseInt(prestacionSel.getConscodigoPrestacionGSA()), idSolicitud,
						Integer.parseInt(prestacionSel.getCodigoTipoPrestacion()),
						prestacionSel.getConscodigoPrestacion(), prestacionSel.getConsecutivoPrestador(),
						userBpmContratacion, prestacionSel.getObservaciones(), prestacionSel.getDescripcion());

				prestacionesAprobadasDTO.setCodigoTipoPrestacion(prestacionSel.getCodigoTipoPrestacion());
				prestacionesAprobadasDTO.setCodigoPrestacion(prestacionSel.getCodigoPrestacion());
				prestacionesAprobadasDTO.setConscodigoPrestacion(prestacionSel.getConscodigoPrestacion());
				prestacionesAprobadasDTO.setDescripcion(prestacionSel.getDescripcion());
				prestacionesAprobadasDTO.setConsecutivoPrestador(prestacionSel.getConsecutivoPrestador());
				prestacionesAprobadasDTO.setObservaciones(prestacionSel.getObservaciones());
				prestacionesAprobadasDTO
						.setTipoPrestacion(getDescTipoPrestacion(prestacionSel.getCodigoTipoPrestacion()));

				prestacionSel = new PrestacionesAprobadasVO();
				listaPrestacionesAprobadas = gestionContratacionController
						.consultaPrestacionesAprobadas(solicitudVO.getNumeroSolicitud(), ConstantesWeb.CADENA_VACIA);
				mostrarPopupGestion = false;

				if (!prestacionesAprobadasDTO.getConsecutivoPrestador().equals(consecutivoPrestadorTemp)) {
					ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
					ConsultaSolicitudesUtil.liquidarPrestacionesPaFPocCapita(idSolicitud, ConstantesWeb.CADENA_VACIA);
					listaConceptosGastoVO = consultarSolicitudControllerAuditoria
							.consultaConceptosGastosxSolicitud(idSolicitud);
				}
			} else {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_CAMPOS_OBLIGATORIOS_GESTION),
						ErrorType.PARAMETRO_ERRADO);
			}

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param listaVos
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<SelectItem> obtenerListaSelectPrestadoresDireccionamiento(List<PrestadorVO> listaVos)
			throws LogicException {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (PrestadorVO vo : listaVos) {
				item = new SelectItem(vo.getCodigoInterno().trim(), vo.getNombreSucursal());
				listaItems.add(item);
			}
		}
		return listaItems;
	}

	/**
	 * 
	 * @throws LogicException
	 */
	private void cargarListaDireccionamiento() throws LogicException {
		PrestadorController prestadorController;
		try {
			prestadorController = new PrestadorController();
		} catch (Exception e) {
			throw new LogicException(e, ErrorType.ERROR_BASEDATOS);
		}

		listaPrestadores.clear();
		listaPrestadores = prestadorController.consultarPrestadoresDireccionamiento(
				prestacionSel.getConscodigoPrestacion(), new Integer(prestacionesAprobadasDTO.getPlanAfiliado()),
				prestacionesAprobadasDTO.getConsecutivoCiudad(), solicitudVO.getNumeroSolicitud());

		lstPrestadoresDireccionamiento = obtenerListaSelectPrestadoresDireccionamiento(listaPrestadores);
	}

	/**
	 * 
	 * @throws LogicException
	 * @throws ServiceException
	 */
	private void validarBuscarPrestacionMedicamentos() throws LogicException, ServiceException {
		PrestacionController prestacionControllerEjb = new PrestacionController();
		if (prestacionSel.getCodigoPrestacion() != null
				&& !prestacionSel.getCodigoPrestacion().trim().equals(ConstantesWeb.CADENA_VACIA)) {
			if (prestacionSel.getCodigoPrestacion().length() > 1) {
				listaMedicamentosEncontrados = prestacionControllerEjb.consultarPrestacionesMedicamentosPorParametros(
						new MedicamentosVO(prestacionSel.getCodigoPrestacion(), prestacionSel.getDescripcion()));
			} else {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CODIGO),
						ErrorType.PARAMETRO_ERRADO);
			}
		} else if (prestacionSel.getDescripcion() != null
				&& !prestacionSel.getDescripcion().trim().equals(ConstantesWeb.CADENA_VACIA)) {
			if (prestacionSel.getDescripcion().length() > 4) {
				listaMedicamentosEncontrados = prestacionControllerEjb.consultarPrestacionesMedicamentosPorParametros(
						new MedicamentosVO(prestacionSel.getCodigoPrestacion(), prestacionSel.getDescripcion()));
			} else {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRESTACION_BUSQUEDA_MIN_CARACTER),
						ErrorType.PARAMETRO_ERRADO);
			}
		} else {
			throw new LogicException(
					Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_PRESTACION_BUSQUEDA_SEL_ALGUNO),
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	/** Metodo para buscar la prestacion */
	public void buscarPrestacionMedicamentos() {
		try {
			mostrarPopupMedicamentos = false;
			validarBuscarPrestacionMedicamentos();
			if (listaMedicamentosEncontrados.size() > 1) {
				mostrarPopupMedicamentos = true;
			} else if (listaMedicamentosEncontrados.size() == 1) {

				mostrarPopupMedicamentos = false;
				medicamentoVOSeleccionado = listaMedicamentosEncontrados.get(0);
				prestacionSel
						.setConscodigoPrestacion(medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento());
				prestacionSel.setCodigoPrestacion(medicamentoVOSeleccionado.getCodigoCodificacionMedicamento());
				prestacionSel.setDescripcion(medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento());
			} else {
				throw new LogicException(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION__PRESTACION_SIN_DATOS),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo cerrar popup prestaciones */
	public void cerrarPopupMedicamentos() {
		mostrarPopupMedicamentos = false;
		listaMedicamentosEncontrados = null;
	}

	/** Metodo abrir popup agregar prestaciones */
	public void cerrarGestionContratacion() {
		mostrarPopupGestion = false;

	}

	/** Metodo seleccionar prestacion */
	public void seleccionPopupMedicamentos() {
		try {
			if (medicamentoVOSeleccionado != null) {
				prestacionSel
						.setConscodigoPrestacion(medicamentoVOSeleccionado.getConsecutivoCodificacionMedicamento());
				prestacionSel.setCodigoPrestacion(medicamentoVOSeleccionado.getCodigoCodificacionMedicamento());
				prestacionSel.setDescripcion(medicamentoVOSeleccionado.getDescripcionCodificacionMedicamento());
				mostrarPopupMedicamentos = false;
				listaMedicamentosEncontrados = null;
				cargarListaDireccionamiento();
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	/** Metodo para buscar la prestacion CUPS */
	public void buscarPrestacionCUPS() {
		try {
			mostrarPopupCUPS = false;
			PrestacionController prestacionControllerEjb = new PrestacionController();
			Utilidades.validarAdicionarPrestacion(prestacionSel.getCodigoPrestacion(), prestacionSel.getDescripcion());
			listaCUPSEncontrados = prestacionControllerEjb.consultarPrestacionesCUPSPorParametros(
					new ProcedimientosVO(prestacionSel.getCodigoPrestacion(), prestacionSel.getDescripcion()));

			FuncionesAppWeb funcionesAppWeb = new FuncionesAppWeb();
			funcionesAppWeb.validarAsociacionPrestacion(listaCUPSEncontrados);

			if (funcionesAppWeb.isMostrarPopupCUPS()) {
				mostrarPopupCUPS = true;
			} else {

				mostrarPopupCUPS = false;
				procedimientoVOSeleccionado = funcionesAppWeb.getProcedimientoVOSeleccionado();
				prestacionSel.setConscodigoPrestacion(
						funcionesAppWeb.getProcedimientoVOSeleccionado().getConsecutivoCodificacionProcedimiento());
				prestacionSel.setCodigoPrestacion(
						funcionesAppWeb.getProcedimientoVOSeleccionado().getCodigoCodificacionProcedimiento());
				prestacionSel.setDescripcion(
						funcionesAppWeb.getProcedimientoVOSeleccionado().getDescripcionCodificacionProcedimiento());
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void seleccionPopupCUPS() {
		try {
			if (procedimientoVOSeleccionado != null) {
				prestacionSel
						.setConscodigoPrestacion(procedimientoVOSeleccionado.getConsecutivoCodificacionProcedimiento());
				prestacionSel.setCodigoPrestacion(procedimientoVOSeleccionado.getCodigoCodificacionProcedimiento());
				prestacionSel.setDescripcion(procedimientoVOSeleccionado.getDescripcionCodificacionProcedimiento());
				mostrarPopupCUPS = false;
				listaCUPSEncontrados = null;
				cargarListaDireccionamiento();
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void seleccionarPrestacion() {
		try {
			if (prestadoresVo != null) {
				prestacionSel.setConsecutivoPrestador(prestadoresVo.getCodigoInterno());
				prestacionSel.setDescDireccionamiento(prestadoresVo.getNombreSucursal());
				mostrarPopupDireccionamiento = false;
				buscarPrestador = new PrestadorVO();
				activarFiltroCiudad = false;
				ciudad = new CiudadVO();
			}
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void consultarPrestacionesXcriteriosBusqueda() {
		try {
			if(!activarFiltroCiudad) {
				ciudad.setConsecutivoCodigoCiudad(prestacionesAprobadasDTO.getConsecutivoCiudad());				
			} 
			if(!validarCiudad()) {
				return;
			}
			PrestadorController prestadorController = new PrestadorController();
			listaPrestadoresCriterio = prestadorController.consultarPrestadoresDireccionamientoXCriteriosBusqueda(
					prestacionesAprobadasDTO.getConscodigoPrestacion(),
					new Integer(prestacionesAprobadasDTO.getPlanAfiliado()),
					ciudad.getConsecutivoCodigoCiudad(), buscarPrestador.getCodigoInterno(),
					buscarPrestador.getNombreSucursal(), buscarPrestador.getNumeroIdentificacionPrestador(),
					solicitudVO.getNumeroSolicitud());
			if(listaPrestadoresCriterio == null || listaPrestadoresCriterio.isEmpty()) {
				throw new  LogicException(
						Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_PERSTADORES_NO_ENCONTRADOS),
						ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	public void limpiarCiudad() {
		if(!activarFiltroCiudad) 
			ciudad = new CiudadVO();
	}
	
	private boolean validarCiudad() {
		boolean existeCiudad = true;
		try {
			if(ciudad.getConsecutivoCodigoCiudad() == null || 
					ciudad.getConsecutivoCodigoCiudad().intValue() == ConstantesWeb.VALOR_CERO) {
				throw new LogicException(Messages.getValorExcepcion(ConstantesWeb.EXCEPCION_CIUDAD_REQUERIDA), ErrorType.DATO_NO_EXISTE);
			}
		} catch (LogicException e) {
			existeCiudad = false;
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} 
		return existeCiudad;
	}

	public void limpiarPrestadores() {
		listaPrestadoresCriterio = new ArrayList<PrestadorVO>();
		buscarPrestador = new PrestadorVO();
		listaPrestadoresCriterio = listaPrestadores;
		ciudad = new CiudadVO();
		activarFiltroCiudad = false;

	}

	public void cerrarPopupCUPS() {
		mostrarPopupCUPS = false;
		listaCUPSEncontrados = null;
	}

	public void cambioTipoPrestacion() {
		prestacionSel.setCodigoPrestacion(null);
		prestacionSel.setDescripcion(null);
		prestacionSel.setConscodigoPrestacion(null);
	}

	/** Metodo para buscar la prestacion */
	public void buscarPrestacion() {
		if (prestacionSel.getCodigoTipoPrestacion().trim().equals(consecutivoCUMS.trim())) {
			buscarPrestacionMedicamentos();
		} else if (prestacionSel.getCodigoTipoPrestacion().trim().equals(consecutivoCUPS.trim())) {
			buscarPrestacionCUPS();
		}
	}

	public void buscarPrestadores() {
		mostrarPopupDireccionamiento = true;
	}

	public void cerrarPopupPrestadores() {
		mostrarPopupDireccionamiento = false;
		activarFiltroCiudad = false;
		ciudad = new CiudadVO();
	}

	/** Metodo para obtener la descripcion del tipo prestacion */
	private String getDescTipoPrestacion(String codigoTipoPrestacion) {
		String descripcion = "";
		for (SelectItem object : listTipoPrestaciones) {
			if (object.getValue() != null && codigoTipoPrestacion.equals(object.getValue().toString())) {
				descripcion = object.getLabel();
				break;
			}
		}
		return descripcion;

	}

	public void cargarDetalleAfiliadoContratacion() {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController
					.consultaRespuestaServicioAfiliado(
							afiliadoVO.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion(),
							afiliadoVO.getNumeroIdentificacion(), afiliadoVO.getPlan().getDescripcionPlan(),
							fechaConsulta, FacesUtils.getUserName());

			if (resultadoConsultaAfiliadoDatosAdicionales != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() != 0) {

				afiliadoVO = FuncionesAppWeb.datosAdicionalesAfiliadoToAfiliadoVO(
						resultadoConsultaAfiliadoDatosAdicionales.getAfiliado(), afiliadoVO);

				listaEmpleadores = consultaAfiliadoController
						.consultaInformacionEmpleadorAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
				listaConveniosCapitacion = consultaAfiliadoController
						.consultaConveniosCapitacion(resultadoConsultaAfiliadoDatosAdicionales);
			} else if (resultadoConsultaAfiliadoDatosAdicionales != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() == 0) {
				throw new LogicException(Messages.getValorExcepcion("ERROR_AFILIADO_NO_EXISTE"),
						ErrorType.PARAMETRO_ERRADO);
			}
			mostrarPopupVerDetalleAfiliadoContratacion = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void verHospitalizacionContratacion() {
		try {
			ConsultarSolicitudController consultarSolicitudControllerAuditoria = new ConsultarSolicitudController();
			hospitalizacionContratacion = consultarSolicitudControllerAuditoria
					.consultaDetalleSolicitud(solicitudVO.getNumeroRadicado()).get(0).getHospitalizacionVO();
			mostrarPopupHospitalizacionContratacion = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void verSolicitudContratacion() {
		try {
			mostrarPopupVerDetalleSolicitudContratacion = true;
			SolicitudController solicitudController = new SolicitudController();
			SolicitudBpmVO solicitudBpmVO = solicitudController.consultarDetalleSolicitudBPM(idSolicitud);

			solicitudVO.setMedicoSolicitante(solicitudBpmVO.getMedicoSolicitante());
			solicitudVO.setPrestadorSolicitante(solicitudBpmVO.getPrestadorSolicitante());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	public void verPopupSoportesContratacion() {
		listaDocumentosAuditoria = gestionAuditoriaIntegralBean.consultaDocumentosSoportexSolicitud(solicitudVO);
		mostrarPopupDocumentosSoporteAuditoria = true;

	}

	public void descargarSoporteContratacion() {
		try {
			FuncionesAppWeb.descargarSoporteAuditoria(documentoSoporteVOSeleccionadoContratacion.getNombreDocumento(),
					documentoSoporteVOSeleccionadoContratacion.getDatosArchivoSoporte());
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void llenarListadoPrestacionesSolicitadasContratacion() {

		for (PrestacionesAprobadasVO vo : listaPrestacionesAprobadas) {
			SelectItem item = new SelectItem(vo.getConscodigoPrestacionGSA(),
					vo.getCodigoPrestacion() + ConstantesWeb.CADENA_EN_BLANCO + vo.getDescripcion());
			lstPrestacionesReasignarContratacion.add(item);
		}

	}

	public void cerrarPopupDiagnostico() {
		mostrarPopupDiagnosticos = false;
		listaDiagnosticosEncontrados = null;
	}

	public void cerrarPopupReasignarSolicitud() {
		mostrarPopupReliquidar = false;
	}

	public String guardarProcesoReasignacionPrestacion() {
		String url = "";
		try {
			if (descripcionGrupoAuditor != null) {
				ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(observacionesReasignarContratacion,
						prestacionesReasignarContratacion);
				GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
				gestionAuditoriaIntegralController.guardarProcesoReasignacionPrestacionAuditoriaIntegral(
						observacionesReasignarContratacion, prestacionesReasignarContratacion, userBpmContratacion,
						descripcionGrupoAuditor);
				mostrarPopupReliquidar = false;
				url = reasignarContratacion();
			} else {
				MostrarMensaje.mostrarMensaje(
						Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO),
						ConstantesWeb.COLOR_ROJO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA),
					ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/** Metodo Abrir popup ReasignarSolicitud */
	public void abrirPopupReasignarSolicitudContratacion() {
		mostrarPopupReliquidar = true;
	}

	public void cerrarDetalleAfiliadoContratacion() {
		mostrarPopupVerDetalleAfiliadoContratacion = false;
	}

	public void cerrarHospitalizacionContratacion() {
		mostrarPopupHospitalizacionContratacion = false;
	}

	public void cerrarPopupVerSolicitudContratacion() {
		mostrarPopupVerDetalleSolicitudContratacion = false;
	}

	public void cerrarPopupDocumentosSoporte() {
		mostrarPopupDocumentosSoporteAuditoria = false;
	}
	
	/**
	 * Cierra popup con listado de ciudades.
	 */
	public void cerrarPopupCiudades() {
		mostrarPopupCiudades = false;
		
	}
	

	/**
	 * Metodo que realiza la busqueda de la ciudad de acuerdo al codigo o descripcion ingresada
	 */
	public void buscarCiudad() {
		try {
			RegistrarSolicitudController registrarSolicitudesController = new RegistrarSolicitudController();
			mostrarPopupCiudades = false;
			listaCiudadesDireccionamiento = IngresoSolicitudesMedicoPrestadorUtil.buscarCiudades(ciudad, registrarSolicitudesController);			
			ciudad = new CiudadVO();
			if(IngresoSolicitudesUtil.validarTamanoLista(listaCiudadesDireccionamiento)){
				mostrarPopupCiudades = true;
			}else{
				mostrarPopupCiudades = false;
				ciudad = listaCiudadesDireccionamiento.get(0);
			}	
			
		} catch (LogicException e) {
			LOG.error(e.getMessage(), e);
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}		
	}	
	
	public List<EmpleadorVO> getListaEmpleadores() {
		return listaEmpleadores;
	}

	public void setListaEmpleadores(List<EmpleadorVO> listaEmpleadores) {
		this.listaEmpleadores = listaEmpleadores;
	}

	public List<PrestacionesAprobadasVO> getListaPrestacionesAprobadas() {
		return listaPrestacionesAprobadas;
	}

	public void setListaPrestacionesAprobadas(List<PrestacionesAprobadasVO> listaPrestacionesAprobadas) {
		this.listaPrestacionesAprobadas = listaPrestacionesAprobadas;
	}

	public void setMostrarPopupGestion(boolean mostrarPopupGestion) {
		this.mostrarPopupGestion = mostrarPopupGestion;
	}

	public boolean getMostrarPopupGestion() {
		return mostrarPopupGestion;
	}

	public PrestacionesAprobadasVO getPrestacionesAprobadasDTO() {
		return prestacionesAprobadasDTO;
	}

	public void setPrestacionesAprobadasDTO(PrestacionesAprobadasVO prestacionesAprobadasDTO) {
		this.prestacionesAprobadasDTO = prestacionesAprobadasDTO;
	}

	public List<SelectItem> getListTipoPrestaciones() {
		return listTipoPrestaciones;
	}

	public void setListTipoPrestaciones(List<SelectItem> listTipoPrestaciones) {
		this.listTipoPrestaciones = listTipoPrestaciones;
	}

	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	public SolicitudBpmVO getSolicitudVO() {
		return solicitudVO;
	}

	public void setSolicitudVO(SolicitudBpmVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	/**
	 * @return the lstPrestadoresDireccionamiento
	 */
	public List<SelectItem> getLstPrestadoresDireccionamiento() {
		return lstPrestadoresDireccionamiento;
	}

	/**
	 * @param lstPrestadoresDireccionamiento
	 *            the lstPrestadoresDireccionamiento to set
	 */
	public void setLstPrestadoresDireccionamiento(List<SelectItem> lstPrestadoresDireccionamiento) {
		this.lstPrestadoresDireccionamiento = lstPrestadoresDireccionamiento;
	}

	/**
	 * @return the idSolicitud
	 */
	public Integer getIdSolicitud() {
		return idSolicitud;
	}

	/**
	 * @param idSolicitud
	 *            the idSolicitud to set
	 */
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	/**
	 * @return the idTask
	 */
	public String getIdTask() {
		return idTask;
	}

	/**
	 * @param idTask
	 *            the idTask to set
	 */
	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	/**
	 * @return the medicamentoVOSeleccionado
	 */
	public MedicamentosVO getMedicamentoVOSeleccionado() {
		return medicamentoVOSeleccionado;
	}

	/**
	 * @param medicamentoVOSeleccionado
	 *            the medicamentoVOSeleccionado to set
	 */
	public void setMedicamentoVOSeleccionado(MedicamentosVO medicamentoVOSeleccionado) {
		this.medicamentoVOSeleccionado = medicamentoVOSeleccionado;
	}

	/**
	 * @return the diagnosticoVoSeleccionado
	 */
	public DiagnosticosVO getDiagnosticoVoSeleccionado() {
		return diagnosticoVoSeleccionado;
	}

	/**
	 * @param diagnosticoVoSeleccionado
	 *            the diagnosticoVoSeleccionado to set
	 */
	public void setDiagnosticoVoSeleccionado(DiagnosticosVO diagnosticoVoSeleccionado) {
		this.diagnosticoVoSeleccionado = diagnosticoVoSeleccionado;
	}

	/**
	 * @return the mostrarPopupMedicamentos
	 */
	public boolean isMostrarPopupMedicamentos() {
		return mostrarPopupMedicamentos;
	}

	/**
	 * @param mostrarPopupMedicamentos
	 *            the mostrarPopupMedicamentos to set
	 */
	public void setMostrarPopupMedicamentos(boolean mostrarPopupMedicamentos) {
		this.mostrarPopupMedicamentos = mostrarPopupMedicamentos;
	}

	/**
	 * @return the listaMedicamentosEncontrados
	 */
	public List<MedicamentosVO> getListaMedicamentosEncontrados() {
		return listaMedicamentosEncontrados;
	}

	/**
	 * @param listaMedicamentosEncontrados
	 *            the listaMedicamentosEncontrados to set
	 */
	public void setListaMedicamentosEncontrados(List<MedicamentosVO> listaMedicamentosEncontrados) {
		this.listaMedicamentosEncontrados = listaMedicamentosEncontrados;
	}

	/**
	 * @return the mostrarPopupCUPS
	 */
	public boolean isMostrarPopupCUPS() {
		return mostrarPopupCUPS;
	}

	/**
	 * @param mostrarPopupCUPS
	 *            the mostrarPopupCUPS to set
	 */
	public void setMostrarPopupCUPS(boolean mostrarPopupCUPS) {
		this.mostrarPopupCUPS = mostrarPopupCUPS;
	}

	/**
	 * @return the procedimientoVOSeleccionado
	 */
	public ProcedimientosVO getProcedimientoVOSeleccionado() {
		return procedimientoVOSeleccionado;
	}

	/**
	 * @param procedimientoVOSeleccionado
	 *            the procedimientoVOSeleccionado to set
	 */
	public void setProcedimientoVOSeleccionado(ProcedimientosVO procedimientoVOSeleccionado) {
		this.procedimientoVOSeleccionado = procedimientoVOSeleccionado;
	}

	/**
	 * @return the listaCUPSEncontrados
	 */
	public List<ProcedimientosVO> getListaCUPSEncontrados() {
		return listaCUPSEncontrados;
	}

	/**
	 * @param listaCUPSEncontrados
	 *            the listaCUPSEncontrados to set
	 */
	public void setListaCUPSEncontrados(List<ProcedimientosVO> listaCUPSEncontrados) {
		this.listaCUPSEncontrados = listaCUPSEncontrados;
	}

	/**
	 * @return the prestacionSel
	 */
	public PrestacionesAprobadasVO getPrestacionSel() {
		return prestacionSel;
	}

	/**
	 * @param prestacionSel
	 *            the prestacionSel to set
	 */
	public void setPrestacionSel(PrestacionesAprobadasVO prestacionSel) {
		this.prestacionSel = prestacionSel;
	}

	public String getDescripcionGrupoAuditor() {
		return descripcionGrupoAuditor;
	}

	public void setDescripcionGrupoAuditor(String descripcionGrupoAuditor) {
		this.descripcionGrupoAuditor = descripcionGrupoAuditor;
	}

	public List<SelectItem> getListaGruposAuditoresContratacion() {
		return listaGruposAuditoresContratacion;
	}

	public void setListaGruposAuditoresContratacion(List<SelectItem> listaGruposAuditoresContratacion) {
		this.listaGruposAuditoresContratacion = listaGruposAuditoresContratacion;
	}

	public boolean isMostrarPopupCofirmacion() {
		return mostrarPopupCofirmacion;
	}

	public void setMostrarPopupCofirmacion(boolean mostrarPopupCofirmacion) {
		this.mostrarPopupCofirmacion = mostrarPopupCofirmacion;
	}

	public boolean isMostrarPopupInformacion() {
		return mostrarPopupInformacion;
	}

	public void setMostrarPopupInformacion(boolean mostrarPopupInformacion) {
		this.mostrarPopupInformacion = mostrarPopupInformacion;
	}

	public boolean isMostrarPopupCofirmacionLiberarContratacion() {
		return mostrarPopupCofirmacionLiberarContratacion;
	}

	public void setMostrarPopupCofirmacionLiberarContratacion(boolean mostrarPopupCofirmacionLiberarContratacion) {
		this.mostrarPopupCofirmacionLiberarContratacion = mostrarPopupCofirmacionLiberarContratacion;
	}

	public boolean isValidacionNombre() {
		return validacionNombre;
	}

	public void setValidacionNombre(boolean validacionNombre) {
		this.validacionNombre = validacionNombre;
	}

	public boolean isValidacionCodigo() {
		return validacionCodigo;
	}

	public void setValidacionCodigo(boolean validacionCodigo) {
		this.validacionCodigo = validacionCodigo;
	}

	public List<SelectItem> getLstRiesgos() {
		return lstRiesgos;
	}

	public void setLstRiesgos(List<SelectItem> lstRiesgos) {
		this.lstRiesgos = lstRiesgos;
	}

	public String getAltoRiesgo() {
		return altoRiesgo;
	}

	public void setAltoRiesgo(String altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}

	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public boolean isMostrarPopupVerDetalleAfiliadoContratacion() {
		return mostrarPopupVerDetalleAfiliadoContratacion;
	}

	public void setMostrarPopupVerDetalleAfiliadoContratacion(boolean mostrarPopupVerDetalleAfiliadoContratacion) {
		this.mostrarPopupVerDetalleAfiliadoContratacion = mostrarPopupVerDetalleAfiliadoContratacion;
	}

	public ConveniosCapitacionVO getConvenioCapacitacionSeleccionado() {
		return convenioCapacitacionSeleccionado;
	}

	public void setConvenioCapacitacionSeleccionado(ConveniosCapitacionVO convenioCapacitacionSeleccionado) {
		this.convenioCapacitacionSeleccionado = convenioCapacitacionSeleccionado;
	}

	public List<ConveniosCapitacionVO> getListaConveniosCapitacion() {
		return listaConveniosCapitacion;
	}

	public void setListaConveniosCapitacion(List<ConveniosCapitacionVO> listaConveniosCapitacion) {
		this.listaConveniosCapitacion = listaConveniosCapitacion;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public List<DiagnosticoDTO> getListaDiagnosticosSeleccionados() {
		return listaDiagnosticosSeleccionados;
	}

	public void setListaDiagnosticosSeleccionados(List<DiagnosticoDTO> listaDiagnosticosSeleccionados) {
		this.listaDiagnosticosSeleccionados = listaDiagnosticosSeleccionados;
	}

	public DiagnosticoDTO getDiagnosticoDTOSeleccionado() {
		return diagnosticoDTOSeleccionado;
	}

	public void setDiagnosticoDTOSeleccionado(DiagnosticoDTO diagnosticoDTOSeleccionado) {
		this.diagnosticoDTOSeleccionado = diagnosticoDTOSeleccionado;
	}

	public boolean isMostrarHospitalizacion() {
		return mostrarHospitalizacion;
	}

	public void setMostrarHospitalizacion(boolean mostrarHospitalizacion) {
		this.mostrarHospitalizacion = mostrarHospitalizacion;
	}

	public HospitalizacionVO getHospitalizacionContratacion() {
		return hospitalizacionContratacion;
	}

	public void setHospitalizacionContratacion(HospitalizacionVO hospitalizacionContratacion) {
		this.hospitalizacionContratacion = hospitalizacionContratacion;
	}

	public boolean isMostrarPopupHospitalizacionContratacion() {
		return mostrarPopupHospitalizacionContratacion;
	}

	public void setMostrarPopupHospitalizacionContratacion(boolean mostrarPopupHospitalizacionContratacion) {
		this.mostrarPopupHospitalizacionContratacion = mostrarPopupHospitalizacionContratacion;
	}

	public boolean isMostrarPopupVerDetalleSolicitudContratacion() {
		return mostrarPopupVerDetalleSolicitudContratacion;
	}

	public void setMostrarPopupVerDetalleSolicitudContratacion(boolean mostrarPopupVerDetalleSolicitudContratacion) {
		this.mostrarPopupVerDetalleSolicitudContratacion = mostrarPopupVerDetalleSolicitudContratacion;
	}

	public List<DocumentoSoporteVO> getListaDocumentosAuditoria() {
		return listaDocumentosAuditoria;
	}

	public void setListaDocumentosAuditoria(List<DocumentoSoporteVO> listaDocumentosAuditoria) {
		this.listaDocumentosAuditoria = listaDocumentosAuditoria;
	}

	public boolean isMostrarPopupDocumentosSoporteAuditoria() {
		return mostrarPopupDocumentosSoporteAuditoria;
	}

	public void setMostrarPopupDocumentosSoporteAuditoria(boolean mostrarPopupDocumentosSoporteAuditoria) {
		this.mostrarPopupDocumentosSoporteAuditoria = mostrarPopupDocumentosSoporteAuditoria;
	}

	public DocumentoSoporteVO getDocumentoSoporteVOSeleccionadoContratacion() {
		return documentoSoporteVOSeleccionadoContratacion;
	}

	public void setDocumentoSoporteVOSeleccionadoContratacion(
			DocumentoSoporteVO documentoSoporteVOSeleccionadoContratacion) {
		this.documentoSoporteVOSeleccionadoContratacion = documentoSoporteVOSeleccionadoContratacion;
	}

	public String getTxtCodigoDiagnostico() {
		return txtCodigoDiagnostico;
	}

	public void setTxtCodigoDiagnostico(String txtCodigoDiagnostico) {
		this.txtCodigoDiagnostico = txtCodigoDiagnostico;
	}

	public String getTxtDescripcionDiagnostico() {
		return txtDescripcionDiagnostico;
	}

	public void setTxtDescripcionDiagnostico(String txtDescripcionDiagnostico) {
		this.txtDescripcionDiagnostico = txtDescripcionDiagnostico;
	}

	public List<DiagnosticosVO> getListaDiagnosticosEncontrados() {
		return listaDiagnosticosEncontrados;
	}

	public void setListaDiagnosticosEncontrados(List<DiagnosticosVO> listaDiagnosticosEncontrados) {
		this.listaDiagnosticosEncontrados = listaDiagnosticosEncontrados;
	}

	public boolean isMostrarPopupDiagnosticos() {
		return mostrarPopupDiagnosticos;
	}

	public void setMostrarPopupDiagnosticos(boolean mostrarPopupDiagnosticos) {
		this.mostrarPopupDiagnosticos = mostrarPopupDiagnosticos;
	}

	public boolean isCheckDiagnosticoPpal() {
		return checkDiagnosticoPpal;
	}

	public void setCheckDiagnosticoPpal(boolean checkDiagnosticoPpal) {
		this.checkDiagnosticoPpal = checkDiagnosticoPpal;
	}

	public boolean isCheckDiagnosticoSOS() {
		return checkDiagnosticoSOS;
	}

	public void setCheckDiagnosticoSOS(boolean checkDiagnosticoSOS) {
		this.checkDiagnosticoSOS = checkDiagnosticoSOS;
	}

	public boolean isMostrarPopupReliquidar() {
		return mostrarPopupReliquidar;
	}

	public void setMostrarPopupReliquidar(boolean mostrarPopupReliquidar) {
		this.mostrarPopupReliquidar = mostrarPopupReliquidar;
	}

	public Integer getPrestacionesReasignar() {
		return prestacionesReasignarContratacion;
	}

	public void setPrestacionesReasignar(Integer prestacionesReasignarContratacion) {
		this.prestacionesReasignarContratacion = prestacionesReasignarContratacion;
	}

	public List<SelectItem> getLstPrestacionesReasignarContratacion() {
		return lstPrestacionesReasignarContratacion;
	}

	public void setLstPrestacionesReasignarContratacion(List<SelectItem> lstPrestacionesReasignarContratacion) {
		this.lstPrestacionesReasignarContratacion = lstPrestacionesReasignarContratacion;
	}

	public String getObservacionesReasignarContratacion() {
		return observacionesReasignarContratacion;
	}

	public void setObservacionesReasignarContratacion(String observacionesReasignarContratacion) {
		this.observacionesReasignarContratacion = observacionesReasignarContratacion;
	}

	public String getConsecutivoPrestadorTemp() {
		return consecutivoPrestadorTemp;
	}

	public void setConsecutivoPrestadorTemp(String consecutivoPrestadorTemp) {
		this.consecutivoPrestadorTemp = consecutivoPrestadorTemp;
	}

	public List<ConceptosGastoVO> getListaConceptosGastoVO() {
		return listaConceptosGastoVO;
	}

	public void setListaConceptosGastoVO(List<ConceptosGastoVO> listaConceptosGastoVO) {
		this.listaConceptosGastoVO = listaConceptosGastoVO;
	}

	public boolean isMostrarPopupDireccionamiento() {
		return mostrarPopupDireccionamiento;
	}

	public void setMostrarPopupDireccionamiento(boolean mostrarPopupDireccionamiento) {
		this.mostrarPopupDireccionamiento = mostrarPopupDireccionamiento;
	}

	public List<PrestadorVO> getListaPrestadores() {
		return listaPrestadores;
	}

	public void setListaPrestadores(List<PrestadorVO> listaPrestadores) {
		this.listaPrestadores = listaPrestadores;
	}

	public PrestadorVO getPrestadoresVo() {
		return prestadoresVo;
	}

	public void setPrestadoresVo(PrestadorVO prestadoresVo) {
		this.prestadoresVo = prestadoresVo;
	}

	public PrestadorVO getBuscarPrestador() {
		return buscarPrestador;
	}

	public void setBuscarPrestador(PrestadorVO buscarPrestador) {
		this.buscarPrestador = buscarPrestador;
	}

	public List<PrestadorVO> getListaPrestadoresCriterio() {
		return listaPrestadoresCriterio;
	}

	public void setListaPrestadoresCriterio(List<PrestadorVO> listaPrestadoresCriterio) {
		this.listaPrestadoresCriterio = listaPrestadoresCriterio;
	}

	public List<CiudadVO> getListaCiudadesDireccionamiento() {
		return listaCiudadesDireccionamiento;
	}

	public void setListaCiudadesDireccionamiento(
			List<CiudadVO> listaCiudadesDireccionamiento) {
		this.listaCiudadesDireccionamiento = listaCiudadesDireccionamiento;
	}

	public CiudadVO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadVO ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isMostrarPopupCiudades() {
		return mostrarPopupCiudades;
	}

	public void setMostrarPopupCiudades(boolean mostrarPopupCiudades) {
		this.mostrarPopupCiudades = mostrarPopupCiudades;
	}

	public boolean isActivarFiltroCiudad() {
		return activarFiltroCiudad;
	}

	public void setActivarFiltroCiudad(boolean activarFiltroCiudad) {
		this.activarFiltroCiudad = activarFiltroCiudad;
	}

}
