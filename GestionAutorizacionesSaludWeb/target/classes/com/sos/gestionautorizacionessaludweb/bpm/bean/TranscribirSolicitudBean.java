package com.sos.gestionautorizacionessaludweb.bpm.bean;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DevolucionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaludejb.bpm.controller.TranscribirSolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.bean.ConsultaAfiliadoBean;
import com.sos.gestionautorizacionessaludweb.bean.IngresoSolicitudBean;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.FuncionesAppWeb;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;

/**
 * Class TranscribirSolicitudBean
 * @author Rafael Cano
 * @version 29/06/2016
 *
 */
public class TranscribirSolicitudBean implements Serializable{
	private static final long serialVersionUID = 2538769421926724366L;
	/**
	 * Variable instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(TranscribirSolicitudBean.class);
	/**
	 * Variable gestion de mensajes
	 */
	

	/**
	 * Controller
	 */
	
	/**
	 * Beans
	 */
	private IngresoSolicitudBean ingresoSolicitudBean;
	/**
	 * DTO's
	 */

	/**
	 * variables
	 */
	private String idTask;
	private Integer idSolicitud;
	private String userTranscribir;
	private String idSolicitudStr;
	private boolean mostrarPopupDocumentosSoporteTranscribir=false;
	private boolean mostrarPopupDevolucion;
	private SoporteVO documentoSoporteVOTranscribir;
	private DevolucionVO devolucion;
	private String consecutivoCodigoDocumentoSoporte;
	private SelectItem item; 
	private boolean cargarArchivosPrimeraVez=false;
	private boolean deshabilitarBtnGuardar=true;
	private boolean primeraVez = true;
	
	private List<SelectItem> listaMotivos;
	private List<String> listaMotivosSeleccionados;
	private List<SoporteVO> listaDocumentosTranscribir;
	private List<SelectItem> listaTiposDocumentoSoporte;
	
	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {

		try{
				idTask					=(String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
				idSolicitudStr	=(String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
				userTranscribir = (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);
			
				this.idSolicitud = new Integer(idSolicitudStr);
				
				inicializarVariables();

				TranscribirSolicitudController transcribirSolicitudController = new TranscribirSolicitudController();
				List<AfiliadoVO> listaAfiliado = transcribirSolicitudController.consultarInformacionPreSolicitud(idSolicitud, null);
				FacesUtils.resetManagedBean(ConstantesWeb.CONSULTA_AFILIADO_BEAN);
				ConsultaAfiliadoBean consultaAfiliadoBean = (ConsultaAfiliadoBean) FacesUtil.getBean(ConstantesWeb.CONSULTA_AFILIADO_BEAN);
				
				consultaAfiliadoBean.setConsecutivoPlan(listaAfiliado.get(0).getPlan().getConsectivoPlan());
				consultaAfiliadoBean.setConsecutivoTipoIdentificacion(listaAfiliado.get(0).getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion());
				consultaAfiliadoBean.setCodigoTipoIdentificacion(listaAfiliado.get(0).getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion());
				consultaAfiliadoBean.setNumeroIdentificacion(listaAfiliado.get(0).getNumeroIdentificacion());
				consultaAfiliadoBean.setDescripcionPlan(listaAfiliado.get(0).getPlan().getDescripcionPlan());
				consultaAfiliadoBean.setMarcaTranscribir(0);
				consultaAfiliadoBean.consultarAfiliado();
				
				ingresoSolicitudBean = (IngresoSolicitudBean) FacesUtil.getBean(ConstantesWeb.INGRESAR_SOLICITUD_BEAN);
				ingresoSolicitudBean.setMarcaTranscribir(0);
			}
			catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}

	private void inicializarVariables() throws ConnectionProviderException, IOException{
		listaMotivos = new ArrayList<SelectItem>();
		listaMotivosSeleccionados = new ArrayList<String>();
		devolucion = new DevolucionVO();
	}
	
	public void mostrarPopupDocumentosSoporteTranscribir(){
		
		listaTiposDocumentoSoporte = new ArrayList<SelectItem>();
	
		
		try {
			TranscribirSolicitudController transcribirSolicitudController = new TranscribirSolicitudController();
			listaTiposDocumentoSoporte = transcribirSolicitudController.consultarListaTipoDocumentoSoportes(ConstantesWeb.TIPO_DOCUMENTO_SOPORTE_MI_SOLICITUD);
			
			if(!cargarArchivosPrimeraVez){
				listaDocumentosTranscribir 	= new ArrayList<SoporteVO>();
				listaDocumentosTranscribir = transcribirSolicitudController.consultaListaDocumentos(idSolicitud);
				cargarArchivosPrimeraVez=true;
			}
	
			
			ingresoSolicitudBean = (IngresoSolicitudBean) FacesUtil.getBean(ConstantesWeb.INGRESAR_SOLICITUD_BEAN);
			ingresoSolicitudBean.setListaSoporteVOs(listaDocumentosTranscribir);
			ingresoSolicitudBean.setMarcaTranscribir(0);
			ingresoSolicitudBean.setIdTask(idTask);
			ingresoSolicitudBean.setIdSolicitud(idSolicitud.toString());
			mostrarPopupDocumentosSoporteTranscribir=true;
			deshabilitarBtnGuardar=false;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
	
	public String liberarTranscripcion(){
		String url ="";
		try{
			FuncionesAppWeb.gestionTarea(idTask, idSolicitud.toString(), ConstantesWeb.LIBERAR_TAREA);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	public String terminarTranscripcion(){
		return gestionTranscripcion(ConstantesWeb.TERMINA_TAREA);
	}
	
	public String gestionTranscripcion(String accion){
		String url ="";
		String numeroSOS = idSolicitudStr;
		try{
			if(accion != ConstantesWeb.DEVOLVER_TAREA){
				numeroSOS = ingresoSolicitudBean.getSolicitud().getConsecutivoSolicitud().toString();
			}
			FuncionesAppWeb.gestionTarea(idTask, numeroSOS, accion);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	private List<SoporteVO> leerArchivos(List<SoporteVO> listaDocumentosTranscribir) throws IOException{
		
		for (SoporteVO documentoSoporteVO : listaDocumentosTranscribir) {
			String rutaArchivo = documentoSoporteVO.getRutaFisica();
			
			if(rutaArchivo != null){
				if (System.getProperty(ConstantesWeb.SEPARADOR_SLASH).equals(ConstantesWeb.CARACTER_SLASH)) {
	        	    if (rutaArchivo.indexOf(ConstantesWeb.CARACTER_SLASH_DOBLE) > -1) {
	        		rutaArchivo = rutaArchivo.replace(ConstantesWeb.CARACTER_SLASH_DOBLE, ConstantesWeb.CARACTER_SLASH);
	        	    }
	        	} else {
	        	    if (rutaArchivo.indexOf(ConstantesWeb.CARACTER_SLASH) > -1) {
	        		rutaArchivo = rutaArchivo.replace(ConstantesWeb.CARACTER_SLASH, ConstantesWeb.CARACTER_SLASH_DOBLE);
	        	    }
	        	    rutaArchivo = ConstantesWeb.CARACTER_SLASH_DOBLE + rutaArchivo;
	        	}
			
				InputStream is;
        		byte[] archivo = null;
        		File file = new File(rutaArchivo);
    			is = new FileInputStream(file);
    			archivo = new byte[(int) file.length()];
    			is.read(archivo);
    			is.close();
			
			documentoSoporteVO.setData(archivo);
			}
		}
		return listaDocumentosTranscribir;
	}
	
	/**  Metodo Descargar documentos soportes  */
	public void descargarSoporteTranscribir(){
		try {
			if(primeraVez != false){
				listaDocumentosTranscribir = leerArchivos(listaDocumentosTranscribir);
				primeraVez=false;
			}
			ingresoSolicitudBean.setDeshabilitarBtnGuardar(false);
			for (SoporteVO documentoSoporteVO : listaDocumentosTranscribir) {
				if(documentoSoporteVOTranscribir.getConsecutivoSoporte()==documentoSoporteVO.getConsecutivoSoporte()){
					FuncionesAppWeb.descargarSoporteAuditoria(documentoSoporteVO.getNombreSoporte(), documentoSoporteVO.getData());
					break;
				}
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}catch (FileNotFoundException e2) {
			MostrarMensaje.mostrarMensaje(e2.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e2.getMessage(), e2);
		} catch (IOException e3) {
			MostrarMensaje.mostrarMensaje(e3.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e3.getMessage(), e3);
		}
	}
	
	public String devolverPreSolicitud(){
		String cerrarVentana="";
		try{
			devolucion.setConsPresolicitud(idSolicitud);
			devolucion.setUsuario(userTranscribir);
			TranscribirSolicitudController transcribirSolicitudController = new TranscribirSolicitudController();
			transcribirSolicitudController.guardarDevolucion(devolucion);
			cerrarVentana = gestionTranscripcion(ConstantesWeb.DEVOLVER_TAREA);
		}
		catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (ConnectionProviderException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return cerrarVentana;
	}
	
	public void cerrarPopupDocumentosSoporteTranscribir(){
		mostrarPopupDocumentosSoporteTranscribir=false;
	}
	
	public void cerrarPopupDevolucion(){
		devolucion = new DevolucionVO();
		mostrarPopupDevolucion=false;
	}
	
	public void abrirPopupDevolucion(){
		try{
			TranscribirSolicitudController transcribirSolicitudController = new TranscribirSolicitudController();
			listaMotivos = FuncionesAppWeb.obtenerListaMotivos(transcribirSolicitudController.consultarMotivosTranscripcion());
			mostrarPopupDevolucion=true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
	}
		
	public boolean isMostrarPopupDocumentosSoporteTranscribir() {
		return mostrarPopupDocumentosSoporteTranscribir;
	}
	
	public void setMostrarPopupDocumentosSoporteTranscribir(
			boolean mostrarPopupDocumentosSoporteTranscribir) {
		this.mostrarPopupDocumentosSoporteTranscribir = mostrarPopupDocumentosSoporteTranscribir;
	}

	public boolean isMostrarPopupDevolucion() {
		return mostrarPopupDevolucion;
	}

	public void setMostrarPopupDevolucion(boolean mostrarPopupDevolucion) {
		this.mostrarPopupDevolucion = mostrarPopupDevolucion;
	}

	public List<SoporteVO> getListaDocumentosTranscribir() {
		return listaDocumentosTranscribir;
	}

	public void setListaDocumentosTranscribir(
			List<SoporteVO> listaDocumentosTranscribir) {
		this.listaDocumentosTranscribir = listaDocumentosTranscribir;
	}


	public SoporteVO getDocumentoSoporteVOTranscribir() {
		return documentoSoporteVOTranscribir;
	}

	public void setDocumentoSoporteVOTranscribir(
			SoporteVO documentoSoporteVOTranscribir) {
		this.documentoSoporteVOTranscribir = documentoSoporteVOTranscribir;
	}

	public List<SelectItem> getListaMotivos() {
		return listaMotivos;
	}

	public void setListaMotivos(List<SelectItem> listaMotivos) {
		this.listaMotivos = listaMotivos;
	}

	public List<String> getListaMotivosSeleccionados() {
		return listaMotivosSeleccionados;
	}

	public void setListaMotivosSeleccionados(List<String> listaMotivosSeleccionados) {
		this.listaMotivosSeleccionados = listaMotivosSeleccionados;
	}

	public DevolucionVO getDevolucion() {
		return devolucion;
	}

	public void setDevolucion(DevolucionVO devolucion) {
		this.devolucion = devolucion;
	}

	public List<SelectItem> getListaTiposDocumentoSoporte() {
		return listaTiposDocumentoSoporte;
	}

	public void setListaTiposDocumentoSoporte(
			List<SelectItem> listaTiposDocumentoSoporte) {
		this.listaTiposDocumentoSoporte = listaTiposDocumentoSoporte;
	}

	public String getConsecutivoCodigoDocumentoSoporte() {
		return consecutivoCodigoDocumentoSoporte;
	}

	public void setConsecutivoCodigoDocumentoSoporte(
			String consecutivoCodigoDocumentoSoporte) {
		this.consecutivoCodigoDocumentoSoporte = consecutivoCodigoDocumentoSoporte;
	}

	public SelectItem getItem() {
		return item;
	}

	public void setItem(SelectItem item) {
		this.item = item;
	}

	public boolean isDeshabilitarBtnGuardar() {
		return deshabilitarBtnGuardar;
	}

	public void setDeshabilitarBtnGuardar(boolean deshabilitarBtnGuardar) {
		this.deshabilitarBtnGuardar = deshabilitarBtnGuardar;
	}
	
}