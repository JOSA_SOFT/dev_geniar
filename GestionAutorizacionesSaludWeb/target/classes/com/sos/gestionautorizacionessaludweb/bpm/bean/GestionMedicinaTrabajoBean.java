package com.sos.gestionautorizacionessaludweb.bpm.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.GrupoAuditorVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.NotificacionesAtelVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgoAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicoVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ControladorVisosServiceEJB;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionAuditoriaIntegralController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionContratacionController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionDomiciliariaController;
import com.sos.gestionautorizacionessaludejb.bpm.controller.GestionMedicinaTrabajoController;
import com.sos.gestionautorizacionessaludejb.controller.CargarCombosController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultaAfiliadoController;
import com.sos.gestionautorizacionessaludejb.controller.ConsultarSolicitudController;
import com.sos.gestionautorizacionessaludejb.controller.SolicitudController;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.bean.ClienteRestBpmBean;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sos.gestionautorizacionessaludweb.util.ConsultaSolicitudesUtil;
import com.sos.gestionautorizacionessaludweb.util.FacesUtils;
import com.sos.gestionautorizacionessaludweb.util.MostrarMensaje;
import com.sos.gestionautorizacionessaludweb.util.UsuarioBean;
import com.sos.util.jsf.FacesUtil;


/**
 * Class GestionMedicinaTrabajoBean
 * @author ing. Rafael Cano 
 * @version 11/02/2016
 *
 */

public class GestionMedicinaTrabajoBean implements Serializable {
	private static final long serialVersionUID = 4199300902188476698L;
	private String idTask;
	private String idSolicitud;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionMedicinaTrabajoBean.class);

	private AfiliadoVO afiliadoVO;
	private SolicitudBpmVO solicitudVO;
	private MedicoVO medico;
	private PrestadorVO prestador;
	
	private DocumentoSoporteVO documentoSoporteVOSeleccionado;
	private ConveniosCapitacionVO convenioCapacitacionSeleccionado;
	private Integer prestacionesReasignarMtrabajo;
	
	private List<DiagnosticoDTO> listDiagnosticos;
	private List<NotificacionesAtelVO> listaNotificaciones;
	private List<SolicitudBpmVO> solicitudDetallada;
	private List<EmpleadorVO> listaEmpleadores;
	private List<ConveniosCapitacionVO> listaConveniosCapacitacion;
	private List<RiesgoAfiliadoVO> listaRiesgosMedicina;
	private List<PrestacionesAprobadasVO> listaPrestacionesAprobadas;
	private List<DocumentoSoporteVO> listaDocumentos;
	private List<DocumentoSoporteVO> listaIdDocumentos;
	private List<SelectItem> lstRiesgos;
	private List<SelectItem> listaGruposAuditores;
	private List<SelectItem> lstPrestacionesReasignar = new ArrayList<SelectItem>();
	
	private String codigoTipoIdentificacion;
	private String numeroIdentificacion;
	private String descripcionPlan; 
	private String grupoAuditor;
	private Date fechaConsulta = Calendar.getInstance().getTime();
	private boolean mostrarPopupVerSolicitudDetalle = false;
	private boolean mostrarPopupVerAfiliadoDetalle = false;
	private boolean mostrarPopupDocumentosSoporteMedicina = false;
	private boolean mostrarPopupInformacionMedicina=false;
	private boolean mostrarPopupCofirmacionMedicina=false;
	private boolean mostrarPopupCofirmacionLiberarMedicina=false;
	private boolean mostrarPopupVerConvenios=false;
	private String altoRiesgo;
	private String fechaSolicitud;
	private String userBpmMedicina;
	private String desTipoIdentificacion;
	private String desGenero;
	private String observacionesReasignarMtrabajo;
	private boolean mostrarPopupReliquidar;
	
	private ClienteRestBpmBean clienteRestBpmBean=(ClienteRestBpmBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_BPM_REST);
	
	/**
	 * Post constructor por defecto
	 */
	@PostConstruct
	public void init() {
		String tipoAuditor;
		try {
			
			idTask=(String) FacesUtil.getRequestParameter(ConstantesWeb.TASK_ID);
			idSolicitud=(String) FacesUtil.getRequestParameter(ConstantesWeb.SOLICITUD_ID);
			tipoAuditor = (String) FacesUtil.getRequestParameter(ConstantesWeb.TIPO_AUDITOR);
			userBpmMedicina = (String) FacesUtil.getRequestParameter(ConstantesWeb.USER_BPM);

			if(tipoAuditor.equals(ConstantesWeb.ID_MEDICINA)){
				listDiagnosticos = new ArrayList<DiagnosticoDTO>();
				listaNotificaciones = new ArrayList<NotificacionesAtelVO>();
				listaRiesgosMedicina = new ArrayList<RiesgoAfiliadoVO>();
				listaPrestacionesAprobadas = new ArrayList<PrestacionesAprobadasVO>();
				listaIdDocumentos = new ArrayList<DocumentoSoporteVO>();
				listaDocumentos = new ArrayList<DocumentoSoporteVO>();
				solicitudDetallada = new ArrayList<SolicitudBpmVO>();
				listaEmpleadores = new ArrayList<EmpleadorVO>();
				listaConveniosCapacitacion = new ArrayList<ConveniosCapitacionVO>();
				lstRiesgos = new ArrayList<SelectItem>();
				listaGruposAuditores = new ArrayList<SelectItem>();
				
				convenioCapacitacionSeleccionado = new ConveniosCapitacionVO();
				solicitudVO = new SolicitudBpmVO();
				afiliadoVO = new AfiliadoVO();
				afiliadoVO = ((GestionInconsistenciasBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getDatosBasicosAfiliado()==null?afiliadoVO:((GestionInconsistenciasBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getDatosBasicosAfiliado();
				solicitudVO = ((GestionInconsistenciasBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getSolicitudVO()==null?solicitudVO:((GestionInconsistenciasBean) FacesUtil.getBean(ConstantesWeb.NOMBRE_BEAN_INCONSISTENCIAS)).getSolicitudVO();
			
				
				desTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion();
				desGenero = afiliadoVO.getGenero().getDescripcionGenero();
				SimpleDateFormat formatoFecha = new SimpleDateFormat(ConstantesWeb.FORMATO_FECHA);
				fechaSolicitud = formatoFecha.format(solicitudVO.getFechaSolicitud());
				altoRiesgo=afiliadoVO.isAltoRiesgo()?ConstantesWeb.ALTO_RIESGO:"";
				
				GestionDomiciliariaController gestionDomiciliariaController = new GestionDomiciliariaController();
				CargarCombosController cargarCombosController = new CargarCombosController();
				GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController(); 
				GestionContratacionController gestionContratacionController = new GestionContratacionController();
				
				listaGruposAuditores = obtenerListaSelectGruposAuditoresMedicina(gestionDomiciliariaController.consultarGruposAuditores(solicitudVO.getNumeroSolicitud(), ConstantesWeb.CONS_MEDICINA_TRABAJO));
				lstRiesgos = obtenerListaSelectRiesgosAfiliado(cargarCombosController.consultaRiesgosAfiliado());
				listaNotificaciones = gestionMedicinaTrabajoController.consultaNotificaciones(solicitudVO.getNumeroSolicitud());
				listDiagnosticos = gestionMedicinaTrabajoController.consultaDiagnosticos(solicitudVO.getNumeroSolicitud());
				listaPrestacionesAprobadas = gestionContratacionController.consultaPrestacionesAprobadas(solicitudVO.getNumeroSolicitud(),"");
				llenarListadoPrestacionesSolicitadasMtrabajo();
			}
			
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	/**  Metodo para reasignar la solicitud  */
	public String reasignarMedicina(){
		String url="";
		try{
			for(int i=0;i<listaGruposAuditores.size();i++){
				if(listaGruposAuditores.get(i).getValue().toString().equals(grupoAuditor)){
					grupoAuditor = listaGruposAuditores.get(i).getLabel();
					break;
				}
			}
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud, ConstantesWeb.REASIGNAR_TAREA, grupoAuditor);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
			return url;
		}catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	public void mostrarConfirmacionMedicina(){
		if(grupoAuditor == null){
			mostrarPopupCofirmacionMedicina=false;
			mostrarPopupInformacionMedicina=true;
		}else{
			mostrarPopupCofirmacionMedicina=true;
			mostrarPopupInformacionMedicina=false;
		}
	}
	
	public void mostrarConfirmacionLiberarMedicina(){
		mostrarPopupCofirmacionLiberarMedicina=true;
	}
	public void cerrarConfirmacionMedicina(){
		mostrarPopupCofirmacionMedicina=false;
	}
	
	public void cerrarInformacionMedicina(){
		mostrarPopupInformacionMedicina=false;
	}
	
	public void cerrarConfirmacionLiberarMedicina(){
		mostrarPopupCofirmacionLiberarMedicina=false;
	}
	
	/**  Metodo para liberar la solicitu  */
	public String liberarTareaMedicina(){
		String url = "";
		try{
			clienteRestBpmBean.liberarTarea(idTask, idSolicitud, ConstantesWeb.LIBERAR_TAREA, ConstantesWeb.CARACTER_SEPARADOR); 
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}

	/**  Metodo para obtener los check Atel de la tabla de prestaciones  */
	public String guardarAtel(){
		String url="";
		try {
			List<PrestacionesAprobadasVO> listaPrestacionesAprobadasTemp = new ArrayList<PrestacionesAprobadasVO>();
			for(int i=0;i<listaPrestacionesAprobadas.size();i++){
				if(listaPrestacionesAprobadas.get(i).getAtelCheck()){
					listaPrestacionesAprobadas.get(i).setAtel(ConstantesWeb.EQUIVALENTE_VERDADERO);
					listaPrestacionesAprobadasTemp.add(listaPrestacionesAprobadas.get(i));
				}
			}
			GestionMedicinaTrabajoController gestionMedicinaTrabajoController = new GestionMedicinaTrabajoController();
			gestionMedicinaTrabajoController.guardarAtelMedicina(listaPrestacionesAprobadasTemp, userBpmMedicina);
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	/**
	 * 
	 * @param listaVos
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<SelectItem> obtenerListaSelectRiesgosAfiliado(List<RiesgoAfiliadoVO> listaVos) throws LogicException {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		if (listaVos != null && !listaVos.isEmpty()) {
			SelectItem item;
			for (RiesgoAfiliadoVO vo : listaVos) {
				item = new SelectItem(vo.getConsecutivoCodRiesgoDiagnostico(), vo.getDescRiesgoDiagnostico());
				listaItems.add(item);
			}
		}
		return listaItems;
	}
	
	/**  Metodo para convertir una lista de valores en una lista desplegable  */
	public List<SelectItem> obtenerListaSelectGruposAuditoresMedicina(List<GrupoAuditorVO> listaVosMedicina) throws LogicException {
		List<SelectItem> listaMedicinaItems = new ArrayList<SelectItem>();
		if (listaVosMedicina != null && !listaVosMedicina.isEmpty()) {
			SelectItem itemMedicina;
			for (GrupoAuditorVO auditor : listaVosMedicina) {
				itemMedicina = new SelectItem(auditor.getCodigoGrupoAuditor(), auditor.getDescGrupoAuditor());
				listaMedicinaItems.add(itemMedicina);
			} 
		}
		return listaMedicinaItems;
	}
	
	public void cerrarDetalleAfiliado(){
		mostrarPopupVerAfiliadoDetalle=false;
	}
	
	public void cerrarDetalleSolicitud(){
		mostrarPopupVerSolicitudDetalle=false;
	}
	
	/**
	 * 
	 */
	public void cerrarPoppupMedicina(){
		mostrarPopupDocumentosSoporteMedicina=false;
	}
	
	/**
	 * 
	 */
	public void verPopupVerSolicitudDetalle() {
		try{
			solicitudDetallada = new ArrayList<SolicitudBpmVO>();
			SolicitudController solicitudController = new SolicitudController();
			SolicitudBpmVO solicitudBpm = solicitudController.consultarDetalleSolicitudBPM(solicitudVO.getNumeroSolicitud());
			medico = solicitudBpm.getMedicoSolicitante();
			prestador = solicitudBpm.getPrestadorSolicitante();
			mostrarPopupVerSolicitudDetalle = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 */
	public void verPopupSoportesMedicina(){
		try{
			ConsultarSolicitudController consultarSolicitudController = new ConsultarSolicitudController();
			ControladorVisosServiceEJB controladorVisosServiceEJB = new ControladorVisosServiceEJB();
			listaIdDocumentos = consultarSolicitudController.consultaDocumentosSoportexSolicitud(solicitudVO.getNumeroSolicitud());
			controladorVisosServiceEJB.crearConexionVisosService();
			listaDocumentos = controladorVisosServiceEJB.consultarDocumentoAnexoxIdDocumento(listaIdDocumentos.get(0).getConsecutivoDocumentoSoporteVisos());
			for(int i=0;i<listaDocumentos.size();i ++){
				listaDocumentos.get(i).setDescripcionTipoDocumentoSoporte(listaIdDocumentos.get(i).getDescripcionTipoDocumentoSoporte());
			}
			mostrarPopupDocumentosSoporteMedicina = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 */
	public void descargarSoporte(){
		
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse httpServletResponse = (HttpServletResponse) externalContext.getResponse();
		httpServletResponse.setHeader(ConstantesWeb.CONTENT_DISPOSITION,ConstantesWeb.ATTACHMENT_FILENAME + documentoSoporteVOSeleccionado.getNombreDocumento());
		httpServletResponse.setHeader(ConstantesWeb.CACHE_CONTROL,ConstantesWeb.MUST_REVALIDATE+","+ ConstantesWeb.POST_CHECK +","+ ConstantesWeb.PRE_CHECK);

		httpServletResponse.setContentLength((int) documentoSoporteVOSeleccionado.getDatosArchivoSoporte().length);
		try {
			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			servletOutputStream.write(documentoSoporteVOSeleccionado.getDatosArchivoSoporte(), 0, (int) documentoSoporteVOSeleccionado.getDatosArchivoSoporte().length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

		FacesContext.getCurrentInstance().responseComplete();
		 
	}
	
	/**
	 * 
	 */
	public void verPopupVerAfiliadoDetalle(){
		try{
			ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales;
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			
			codigoTipoIdentificacion = afiliadoVO.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion();
			numeroIdentificacion = afiliadoVO.getNumeroIdentificacion();
			descripcionPlan = afiliadoVO.getPlan().getDescripcionPlan();
			

			resultadoConsultaAfiliadoDatosAdicionales = consultaAfiliadoController.consultaRespuestaServicioAfiliado(codigoTipoIdentificacion, numeroIdentificacion, descripcionPlan,
					fechaConsulta, FacesUtils.getUserName());
			
			if (resultadoConsultaAfiliadoDatosAdicionales != null && resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() != 0) {
				consultaDatosAdicionalesAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
			} else if (resultadoConsultaAfiliadoDatosAdicionales != null && resultadoConsultaAfiliadoDatosAdicionales.getAfiliado() != null
					&& resultadoConsultaAfiliadoDatosAdicionales.getAfiliado().getNui() == 0) {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			}
			mostrarPopupVerAfiliadoDetalle = true;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}

	/**
	 * 
	 * @param resultadoConsultaAfiliadoDatosAdicionales
	 */
	public void consultaDatosAdicionalesAfiliado(ResultadoConsultaAfiliadoDatosAdicionales resultadoConsultaAfiliadoDatosAdicionales) {
		try {
			ConsultaAfiliadoController consultaAfiliadoController = new ConsultaAfiliadoController();
			afiliadoVO = consultaAfiliadoController.consultaInformacionAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
			listaEmpleadores = new ArrayList<EmpleadorVO>();
			listaConveniosCapacitacion = new ArrayList<ConveniosCapitacionVO>();
			listaEmpleadores = consultaAfiliadoController.consultaInformacionEmpleadorAfiliado(resultadoConsultaAfiliadoDatosAdicionales);
			listaConveniosCapacitacion = consultaAfiliadoController.consultaConveniosCapitacion(resultadoConsultaAfiliadoDatosAdicionales);

		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 */
	public String terminarMedicinatrabajo() {
		String url="";
		try {
			guardarAtel();
			clienteRestBpmBean.reasignarTarea(idTask, idSolicitud, ConstantesWeb.REASIGNAR_TAREA, ConstantesWeb.GRUPO_AUDITORIA);
			url = ((UsuarioBean) FacesUtil.getBean(ConstantesWeb.USUARIO_BEAN)).logoutBPM();
			return url;
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.ERROR_EXCEPCION_INESPERADA);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	private void llenarListadoPrestacionesSolicitadasMtrabajo() {
		
		for( PrestacionesAprobadasVO vo : listaPrestacionesAprobadas){
			SelectItem item = new SelectItem(vo.getConscodigoPrestacionGSA(), vo.getCodigoPrestacion() + ConstantesWeb.CADENA_EN_BLANCO + vo.getDescripcion());
			lstPrestacionesReasignar.add(item);
		}
		
	}
	
	public void abrirPopupReasignarSolicitudMTrabajo(){
		mostrarPopupReliquidar = true;
	}
	
	public void cerrarPopupReasignarSolicitud(){
		mostrarPopupReliquidar = false;
	}
	
	public String guardarProcesoReasignacionPrestacionMtrabajo(){
		String url ="";
		try {
			if(grupoAuditor != null){
				ConsultaSolicitudesUtil.validarObligatoriosModFechaEntrega(observacionesReasignarMtrabajo, prestacionesReasignarMtrabajo);
				GestionAuditoriaIntegralController gestionAuditoriaIntegralController = new GestionAuditoriaIntegralController();
				gestionAuditoriaIntegralController.guardarProcesoReasignacionPrestacionAuditoriaIntegral(observacionesReasignarMtrabajo, prestacionesReasignarMtrabajo, userBpmMedicina, grupoAuditor);
				mostrarPopupReliquidar = false;
				url = reasignarMedicina();
			}else {
				MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_VALIDACION_REQUIERE_OTRO_AUDITOR_OBLIGATORIO), ConstantesWeb.COLOR_ROJO);
			}
		} catch (LogicException e) {
			MostrarMensaje.mostrarMensaje(e.getMessage(), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			MostrarMensaje.mostrarMensaje(Messages.getValorExcepcion(ConstantesWeb.ERROR_EXCEPCION_INESPERADA), ConstantesWeb.COLOR_ROJO);
			LOG.error(e.getMessage(), e);
		}
		return url;
	}
	
	public void cerrarPopupReasignarSolicitudMTrabajo(){
		mostrarPopupReliquidar = false;
	}
	
	/**
	 * @return the afiliadoVO
	 */
	public AfiliadoVO getAfiliadoVO() {
		return afiliadoVO;
	}

	/**
	 * @param afiliadoVO the afiliadoVO to set
	 */
	public void setAfiliadoVO(AfiliadoVO afiliadoVO) {
		this.afiliadoVO = afiliadoVO;
	}

	/**
	 * @return the solicitudVO
	 */
	public SolicitudBpmVO getSolicitudVO() {
		return solicitudVO;
	}

	/**
	 * @param solicitudVO the solicitudVO to set
	 */
	public void setSolicitudVO(SolicitudBpmVO solicitudVO) {
		this.solicitudVO = solicitudVO;
	}

	/**
	 * @return the medico
	 */
	public MedicoVO getMedico() {
		return medico;
	}

	/**
	 * @param medico the medico to set
	 */
	public void setMedico(MedicoVO medico) {
		this.medico = medico;
	}

	/**
	 * @return the prestador
	 */
	public PrestadorVO getPrestador() {
		return prestador;
	}

	/**
	 * @param prestador the prestador to set
	 */
	public void setPrestador(PrestadorVO prestador) {
		this.prestador = prestador;
	}

	/**
	 * @return the documentoSoporteVOSeleccionado
	 */
	public DocumentoSoporteVO getDocumentoSoporteVOSeleccionado() {
		return documentoSoporteVOSeleccionado;
	}

	/**
	 * @param documentoSoporteVOSeleccionado the documentoSoporteVOSeleccionado to set
	 */
	public void setDocumentoSoporteVOSeleccionado(
			DocumentoSoporteVO documentoSoporteVOSeleccionado) {
		this.documentoSoporteVOSeleccionado = documentoSoporteVOSeleccionado;
	}

	/**
	 * @return the listDiagnosticos
	 */
	public List<DiagnosticoDTO> getListDiagnosticos() {
		return listDiagnosticos;
	}

	/**
	 * @param listDiagnosticos the listDiagnosticos to set
	 */
	public void setListDiagnosticos(List<DiagnosticoDTO> listDiagnosticos) {
		this.listDiagnosticos = listDiagnosticos;
	}

	/**
	 * @return the listaNotificaciones
	 */
	public List<NotificacionesAtelVO> getListaNotificaciones() {
		return listaNotificaciones;
	}

	/**
	 * @param listaNotificaciones the listaNotificaciones to set
	 */
	public void setListaNotificaciones(
			List<NotificacionesAtelVO> listaNotificaciones) {
		this.listaNotificaciones = listaNotificaciones;
	}

	/**
	 * @return the solicitudDetallada
	 */
	public List<SolicitudBpmVO> getSolicitudDetallada() {
		return solicitudDetallada;
	}

	/**
	 * @param solicitudDetallada the solicitudDetallada to set
	 */
	public void setSolicitudDetallada(List<SolicitudBpmVO> solicitudDetallada) {
		this.solicitudDetallada = solicitudDetallada;
	}

	/**
	 * @return the listaEmpleadores
	 */
	public List<EmpleadorVO> getListaEmpleadores() {
		return listaEmpleadores;
	}

	/**
	 * @param listaEmpleadores the listaEmpleadores to set
	 */
	public void setListaEmpleadores(List<EmpleadorVO> listaEmpleadores) {
		this.listaEmpleadores = listaEmpleadores;
	}

	/**
	 * @return the listaConveniosCapacitacion
	 */
	public List<ConveniosCapitacionVO> getListaConveniosCapacitacion() {
		return listaConveniosCapacitacion;
	}

	/**
	 * @param listaConveniosCapacitacion the listaConveniosCapacitacion to set
	 */
	public void setListaConveniosCapacitacion(
			List<ConveniosCapitacionVO> listaConveniosCapacitacion) {
		this.listaConveniosCapacitacion = listaConveniosCapacitacion;
	}

	/**
	 * @return the listaRiesgosMedicina
	 */
	public List<RiesgoAfiliadoVO> getListaRiesgosMedicina() {
		return listaRiesgosMedicina;
	}

	/**
	 * @param listaRiesgosMedicina the listaRiesgosMedicina to set
	 */
	public void setListaRiesgosMedicina(List<RiesgoAfiliadoVO> listaRiesgosMedicina) {
		this.listaRiesgosMedicina = listaRiesgosMedicina;
	}

	/**
	 * @return the listaPrestacionesAprobadas
	 */
	public List<PrestacionesAprobadasVO> getListaPrestacionesAprobadas() {
		return listaPrestacionesAprobadas;
	}

	/**
	 * @param listaPrestacionesAprobadas the listaPrestacionesAprobadas to set
	 */
	public void setListaPrestacionesAprobadas(
			List<PrestacionesAprobadasVO> listaPrestacionesAprobadas) {
		this.listaPrestacionesAprobadas = listaPrestacionesAprobadas;
	}

	/**
	 * @return the listaDocumentos
	 */
	public List<DocumentoSoporteVO> getListaDocumentos() {
		return listaDocumentos;
	}

	/**
	 * @param listaDocumentos the listaDocumentos to set
	 */
	public void setListaDocumentos(List<DocumentoSoporteVO> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	/**
	 * @return the listaIdDocumentos
	 */
	public List<DocumentoSoporteVO> getListaIdDocumentos() {
		return listaIdDocumentos;
	}

	/**
	 * @param listaIdDocumentos the listaIdDocumentos to set
	 */
	public void setListaIdDocumentos(List<DocumentoSoporteVO> listaIdDocumentos) {
		this.listaIdDocumentos = listaIdDocumentos;
	}

	/**
	 * @return the lstRiesgos
	 */
	public List<SelectItem> getLstRiesgos() {
		return lstRiesgos;
	}

	/**
	 * @param lstRiesgos the lstRiesgos to set
	 */
	public void setLstRiesgos(List<SelectItem> lstRiesgos) {
		this.lstRiesgos = lstRiesgos;
	}

	/**
	 * @return the codigoTipoIdentificacion
	 */
	public String getCodigoTipoIdentificacion() {
		return codigoTipoIdentificacion;
	}

	/**
	 * @param codigoTipoIdentificacion the codigoTipoIdentificacion to set
	 */
	public void setCodigoTipoIdentificacion(String codigoTipoIdentificacion) {
		this.codigoTipoIdentificacion = codigoTipoIdentificacion;
	}

	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	/**
	 * @param numeroIdentificacion the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/**
	 * @return the descripcionPlan
	 */
	public String getDescripcionPlan() {
		return descripcionPlan;
	}

	/**
	 * @param descripcionPlan the descripcionPlan to set
	 */
	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}

	/**
	 * @return the fechaConsulta
	 */
	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	/**
	 * @param fechaConsulta the fechaConsulta to set
	 */
	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	/**
	 * @return the mostrarPopupVerSolicitudDetalle
	 */
	public boolean isMostrarPopupVerSolicitudDetalle() {
		return mostrarPopupVerSolicitudDetalle;
	}

	/**
	 * @param mostrarPopupVerSolicitudDetalle the mostrarPopupVerSolicitudDetalle to set
	 */
	public void setMostrarPopupVerSolicitudDetalle(
			boolean mostrarPopupVerSolicitudDetalle) {
		this.mostrarPopupVerSolicitudDetalle = mostrarPopupVerSolicitudDetalle;
	}

	/**
	 * @return the mostrarPopupVerAfiliadoDetalle
	 */
	public boolean isMostrarPopupVerAfiliadoDetalle() {
		return mostrarPopupVerAfiliadoDetalle;
	}

	/**
	 * @param mostrarPopupVerAfiliadoDetalle the mostrarPopupVerAfiliadoDetalle to set
	 */
	public void setMostrarPopupVerAfiliadoDetalle(
			boolean mostrarPopupVerAfiliadoDetalle) {
		this.mostrarPopupVerAfiliadoDetalle = mostrarPopupVerAfiliadoDetalle;
	}

	/**
	 * @return the mostrarPopupDocumentosSoporteMedicina
	 */
	public boolean isMostrarPopupDocumentosSoporteMedicina() {
		return mostrarPopupDocumentosSoporteMedicina;
	}

	/**
	 * @param mostrarPopupDocumentosSoporteMedicina the mostrarPopupDocumentosSoporteMedicina to set
	 */
	public void setMostrarPopupDocumentosSoporteMedicina(
			boolean mostrarPopupDocumentosSoporteMedicina) {
		this.mostrarPopupDocumentosSoporteMedicina = mostrarPopupDocumentosSoporteMedicina;
	}
	
	
	public String getIdTask() {
		return idTask;
	}

	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public List<SelectItem> getListaGruposAuditores() {
		return listaGruposAuditores;
	}

	public void setListaGruposAuditores(List<SelectItem> listaGruposAuditores) {
		this.listaGruposAuditores = listaGruposAuditores;
	}
	
	public String getGrupoAuditor() {
		return grupoAuditor;
	}
	
	public void setGrupoAuditor(String grupoAuditor) {
		this.grupoAuditor = grupoAuditor;
	}
	
	public boolean isMostrarPopupInformacionMedicina() {
		return mostrarPopupInformacionMedicina;
	}
	
	public void setMostrarPopupInformacionMedicina(
			boolean mostrarPopupInformacionMedicina) {
		this.mostrarPopupInformacionMedicina = mostrarPopupInformacionMedicina;
	}
	
	public boolean isMostrarPopupCofirmacionMedicina() {
		return mostrarPopupCofirmacionMedicina;
	}
	
	public void setMostrarPopupCofirmacionMedicina(
			boolean mostrarPopupCofirmacionMedicina) {
		this.mostrarPopupCofirmacionMedicina = mostrarPopupCofirmacionMedicina;
	}
	
	public boolean isMostrarPopupCofirmacionLiberarMedicina() {
		return mostrarPopupCofirmacionLiberarMedicina;
	}
	
	public void setMostrarPopupCofirmacionLiberarMedicina(
			boolean mostrarPopupCofirmacionLiberarMedicina) {
		this.mostrarPopupCofirmacionLiberarMedicina = mostrarPopupCofirmacionLiberarMedicina;
	}
	
	public String getAltoRiesgo() {
		return altoRiesgo;
	}
	
	public void setAltoRiesgo(String altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}
	
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	
	public ConveniosCapitacionVO getConvenioCapacitacionSeleccionado() {
		return convenioCapacitacionSeleccionado;
	}
	
	public void setConvenioCapacitacionSeleccionado(
			ConveniosCapitacionVO convenioCapacitacionSeleccionado) {
		this.convenioCapacitacionSeleccionado = convenioCapacitacionSeleccionado;
	}
	
	public boolean getMostrarPopupVerConvenios() {
		return mostrarPopupVerConvenios;
	}
	
	public void setMostrarPopupVerConvenios(boolean mostrarPopupVerConvenios) {
		this.mostrarPopupVerConvenios = mostrarPopupVerConvenios;
	}
	public String getDesTipoIdentificacion() {
		return desTipoIdentificacion;
	}
	public void setDesTipoIdentificacion(String desTipoIdentificacion) {
		this.desTipoIdentificacion = desTipoIdentificacion;
	}
	public String getDesGenero() {
		return desGenero;
	}
	public void setDesGenero(String desGenero) {
		this.desGenero = desGenero;
	}
	public boolean isMostrarPopupReliquidar() {
		return mostrarPopupReliquidar;
	}
	public void setMostrarPopupReliquidar(boolean mostrarPopupReliquidar) {
		this.mostrarPopupReliquidar = mostrarPopupReliquidar;
	}
	public Integer getPrestacionesReasignarMtrabajo() {
		return prestacionesReasignarMtrabajo;
	}
	public void setPrestacionesReasignarMtrabajo(
			Integer prestacionesReasignarMtrabajo) {
		this.prestacionesReasignarMtrabajo = prestacionesReasignarMtrabajo;
	}
	public List<SelectItem> getLstPrestacionesReasignar() {
		return lstPrestacionesReasignar;
	}
	public void setLstPrestacionesReasignar(
			List<SelectItem> lstPrestacionesReasignar) {
		this.lstPrestacionesReasignar = lstPrestacionesReasignar;
	}
	public String getObservacionesReasignarMtrabajo() {
		return observacionesReasignarMtrabajo;
	}
	public void setObservacionesReasignarMtrabajo(
			String observacionesReasignarMtrabajo) {
		this.observacionesReasignarMtrabajo = observacionesReasignarMtrabajo;
	}
}
