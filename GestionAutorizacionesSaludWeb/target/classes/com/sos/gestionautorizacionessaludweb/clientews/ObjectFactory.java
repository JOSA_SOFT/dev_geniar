
package com.sos.gestionautorizacionessaludweb.clientews;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.sos.gestionautorizacionessaludweb.clientews.EjecutarProceso;
import com.sos.gestionautorizacionessaludweb.clientews.EjecutarProcesoResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sos.proyectobasetestws.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EjecutarProceso_QNAME = new QName("http://com.sos.administracion.subscripcioncm.ws/", "ejecutarProceso");
    private final static QName _EjecutarProcesoResponse_QNAME = new QName("http://com.sos.administracion.subscripcioncm.ws/", "ejecutarProcesoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sos.proyectobasetestws.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EjecutarProceso }
     * 
     */
    public EjecutarProceso createEjecutarProceso() {
        return new EjecutarProceso();
    }

    /**
     * Create an instance of {@link EjecutarProcesoResponse }
     * 
     */
    public EjecutarProcesoResponse createEjecutarProcesoResponse() {
        return new EjecutarProcesoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EjecutarProceso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.sos.administracion.subscripcioncm.ws/", name = "ejecutarProceso")
    public JAXBElement<EjecutarProceso> createEjecutarProceso(EjecutarProceso value) {
        return new JAXBElement<EjecutarProceso>(_EjecutarProceso_QNAME, EjecutarProceso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EjecutarProcesoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.sos.administracion.subscripcioncm.ws/", name = "ejecutarProcesoResponse")
    public JAXBElement<EjecutarProcesoResponse> createEjecutarProcesoResponse(EjecutarProcesoResponse value) {
        return new JAXBElement<EjecutarProcesoResponse>(_EjecutarProcesoResponse_QNAME, EjecutarProcesoResponse.class, null, value);
    }

}
