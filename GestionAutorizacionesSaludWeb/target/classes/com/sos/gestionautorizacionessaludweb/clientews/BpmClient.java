package com.sos.gestionautorizacionessaludweb.clientews;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludweb.util.ConstantesWeb;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.representation.Form;

public class BpmClient {

	private WebResource webResource;
	private Client client;  
	private String idTask;
	private String idsolicitud;
	private String idInstancia;
    
    public BpmClient(String idTask, String id_solicitud) {
    	this.idTask = idTask;
        this.idsolicitud = id_solicitud;
    }
    
    public BpmClient(String idInstancia) {
    	this.idInstancia = idInstancia;
    }

    /**
     * @param requestEntity request data@return response object (instance of
     * responseType class)
     */
    public void enviarBPM(String result, String tipoAuditor, String idSolicitud) throws UniformInterfaceException {
        Form form = new Form();
        form.add(ConstantesWeb.PARAMS, ConstantesWeb.PARAMS_PATH + idSolicitud + ConstantesWeb.PARAMS_PATH_2 + tipoAuditor + ConstantesWeb.PARAMS_PATH_RESULT + result + ConstantesWeb.PARAMS_PATH_CIERRE);
        form.add(ConstantesWeb.ACTION_SOLICITUD, ConstantesWeb.ESTADO_ACTION);
        webResource.type(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class,form);
    }
    
    /**
     * @param requestEntity request data@return response object (instance of
     * responseType class)
     */
    public void enviarBPMAnularInstancia() throws UniformInterfaceException {
        Form form = new Form();
        form.add(ConstantesWeb.ACTION_SOLICITUD, ConstantesWeb.ESTADO_TERMINATE);
        webResource.type(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class,form);
    }
    
    
    public static String obtenerPropiedad(String propiedadGestion) throws LogicException{
    	String propiedadQuery = "";
    	ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();
        PropiedadVO propiedad = new PropiedadVO();
        
        propiedad.setAplicacion(ConsultaProperties.getString(ConstantesWeb.APLICACION_GESTION_SALUD));
        propiedad.setClasificacion(ConsultaProperties.getString(ConstantesWeb.CLASIFICACION_GESTION_SOLICITUD));
        propiedad.setPropiedad(ConsultaProperties.getString(propiedadGestion));        
               		
        try {
        	propiedadQuery = propertyQuery.consultarPropiedad(propiedad);
    	} catch (Exception e) {
    		throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY), e, LogicException.ErrorType.DATO_NO_EXISTE);
    	}              
    	
        return  propiedadQuery;
    }
    
    public String obtenerURI() throws LogicException{    	
    	return obtenerPropiedad(ConstantesWeb.PROPIEDAD_GESTION_SOLICITUD_URL) + ConstantesWeb.PROPIEDAD_GESTION_RESTO_URL;
    }    

      public void crearBpmService() throws LogicException{
        String BASE_URI = obtenerURI();
        webResource = obtenerRecursoRemoto(BASE_URI).path(ConstantesWeb.CARACTER_URI + idTask);
    }
    
    public void crearBpmServiceAnularInstancia() throws LogicException{
        String BASE_URI = obtenerURIAnularInstancia();
        String path = new StringBuilder(BASE_URI).append(ConstantesWeb.CARACTER_URI).append(idInstancia).append(ConstantesWeb.PROPIEDAD_ACTION_ANULAR_INSTANCIA).toString();
        webResource = obtenerRecursoRemoto(path); 
    }
  
    public String obtenerURIAnularInstancia() throws LogicException{    	
    	return obtenerPropiedad(ConstantesWeb.PROPIEDAD_GESTION_SOLICITUD_URL) + ConstantesWeb.PROPIEDAD_GESTION_RESTO_ANULAR_INSTANCIA_URL;
    }
    
    public void close(){
    	if (client != null){
    		client.destroy();
        	client = null;
    	}    
    }
    
    private WebResource obtenerRecursoRemoto(String path) throws LogicException{
    	close();
    	
    	String userTask  = obtenerPropiedad(ConstantesWeb.PROPIEDAD_GESTION_SOLICITUD_USUARIO);
    	String passwTask = obtenerPropiedad(ConstantesWeb.PROPIEDAD_GESTION_SOLICITUD_PASSW); 
    	
        ClientConfig config = new DefaultClientConfig();
        client = Client.create(config);
        client.addFilter(new HTTPBasicAuthFilter(userTask, passwTask));        
        WebResource webResource = client.resource(path); 
        return webResource;
    }
}