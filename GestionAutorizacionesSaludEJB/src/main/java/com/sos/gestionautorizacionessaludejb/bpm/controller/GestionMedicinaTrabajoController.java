package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina.SpASConsultaDatosInformacionDiagnosticoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina.SpASConsultaDatosInformacionNotificacionAtelDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionmedicina.SpASGuardarMarcasMedicinaTrabajoDelegate;
import com.sos.gestionautorizacionessaluddata.model.bpm.NotificacionesAtelVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;


/**
 * Class GestionMedicinaTrabajoController
 * Clase controladora que invoca los delegates para la gestión de Medicina del Trabajo
 * @author ing. Rafael Cano
 * @version 15/02/2016
 *
 */

public class GestionMedicinaTrabajoController {
	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;
	
	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public GestionMedicinaTrabajoController() throws ConnectionProviderException, IOException{		
		if(connProviderSalud == null){
		    connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar las notificaciones
	 * @param numeroSolicitud
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<NotificacionesAtelVO> consultaNotificaciones(int numeroSolicitud) throws LogicException{
		SpASConsultaDatosInformacionNotificacionAtelDelegate listaNotificaciones = new SpASConsultaDatosInformacionNotificacionAtelDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaNotificaciones);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_NOTIFICACIONES_ATEL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		if(listaNotificaciones.getListaNotificaciones().isEmpty() || listaNotificaciones.getListaNotificaciones() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_NOTIFICACIONES_ATEL), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return listaNotificaciones.getListaNotificaciones();		
	}
		
	/**
	 * Metodo para consultar los diagnosticos
	 * @param numeroSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<DiagnosticoDTO> consultaDiagnosticos(int numeroSolicitud) throws LogicException{
		SpASConsultaDatosInformacionDiagnosticoDelegate listaDiagnosticos = new SpASConsultaDatosInformacionDiagnosticoDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaDiagnosticos);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(listaDiagnosticos.getListaDiagnosticos().isEmpty() || listaDiagnosticos.getListaDiagnosticos() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return listaDiagnosticos.getListaDiagnosticos();		
		
	}
	
	/**
	 * Metodo que guarda las notificaciones ATEL de la pantalla de Medicina del Trabajo
	 * @param listaPrestacionesAprobadas
	 * @param usuario
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public void guardarAtelMedicina(List<PrestacionesAprobadasVO> listaPrestacionesAprobadas, String usuario) throws LogicException{
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlNotificacionAtel(listaPrestacionesAprobadas, usuario);
		SpASGuardarMarcasMedicinaTrabajoDelegate guardarAtelMedicina = new SpASGuardarMarcasMedicinaTrabajoDelegate(xml);
		
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, guardarAtelMedicina);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_ATEL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
	}	
}
