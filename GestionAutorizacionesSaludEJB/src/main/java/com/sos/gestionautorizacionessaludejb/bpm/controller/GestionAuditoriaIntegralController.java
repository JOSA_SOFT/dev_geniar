package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpASConsultaDatosInformacionConceptosGastoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpASGuardarInformacionAdicionarPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpASGuardarInformacionGestionAuditoriaIntegralDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpASGuardarInformacionReasignarPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpASConsultaRecobrosAuditoriaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpAsConsultarCausalesDeNoCobroDelegate;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosNegacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.GestionAuditorDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class GestionAuditoriaIntegralController
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionAuditoriaIntegralController {

	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;
	
	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public GestionAuditoriaIntegralController() throws ConnectionProviderException, IOException{			
		if(connProviderSalud == null){
		    connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}
	
	/**
	 * Metodo que consulta los recobros
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<SelectItem> consultaRecobros(Integer numeroSolicitud) throws LogicException{
			
		SpASConsultaRecobrosAuditoriaDelegate recobros = new SpASConsultaRecobrosAuditoriaDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, recobros);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RECOBROS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	
		if(recobros.getListaRecobros() == null || recobros.getListaRecobros().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RECOBROS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
	
		return recobros.getListaRecobros();
	}
	
	/**
	 * Metodo para guardar la gestion de auditoria integral
	 * @param afiliado
	 * @param solicitud
	 * @param lstPrestaciones
	 * @param numPrestacion
	 * @param gestionAuditor
	 * @param datosNegacion
	 * @param usuario
	 * @throws LogicException
	 */
	public void guardarGestionAuditoriaIntegral(AfiliadoVO afiliado, 
			SolicitudBpmVO solicitud, List<PrestacionesAprobadasVO> lstPrestaciones, Integer numPrestacion,
			GestionAuditorDTO gestionAuditor, DatosNegacionDTO datosNegacion, String usuario) throws LogicException {
		
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlGestionDomiciliaria(afiliado, solicitud, lstPrestaciones, numPrestacion, gestionAuditor, datosNegacion, usuario);
		SpASGuardarInformacionGestionAuditoriaIntegralDelegate delegate = new SpASGuardarInformacionGestionAuditoriaIntegralDelegate(xml);
		
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_GESTION_AUDITORIA_INTEGRAL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
	
	/**
	 * metodo para conultar los concepto de gastos
	 * @param consecutivoPrestacion
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<ConceptosGastoVO> consultarConceptoGastos(Integer consecutivoPrestacion) throws LogicException{
		SpASConsultaDatosInformacionConceptosGastoDelegate delegate = new SpASConsultaDatosInformacionConceptosGastoDelegate(consecutivoPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_CONCEPTOS_GASTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	
		return delegate.getLstConceptosGastoVO();
	}
	
	/**
	 * Metodo para guardar la gestion de auditoria integral
	 * @param prestacionNueva

	 * @throws LogicException
	 */
	public void guardarPrestacionAuditoriaIntegral(PrestacionesAprobadasVO prestacionNueva, int numeroSolicitud, String userBpm) throws LogicException{
		SpASGuardarInformacionAdicionarPrestacionDelegate delegate = new SpASGuardarInformacionAdicionarPrestacionDelegate(prestacionNueva, numeroSolicitud, userBpm);
		
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ADICIONAR_PRRESTACION_AUDITORIA_INTEGRAL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
	
	/**
	 * Metodo para guardar la gestion de reasignación auditoria integral
	 * @param prestacionNueva

	 * @throws LogicException
	 */	
	public void guardarProcesoReasignacionPrestacionAuditoriaIntegral(String obsFechaEntrega, Integer causalFechaEntrega, String usuario, String descGrupoAuditor)throws LogicException{
		SpASGuardarInformacionReasignarPrestacionDelegate delegate = new SpASGuardarInformacionReasignarPrestacionDelegate(obsFechaEntrega, causalFechaEntrega, usuario, descGrupoAuditor);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_REASIGNAR_PRESTACION_AUDITORIA_INTEGRAL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
	
	/**
	 * Metodo para Consultar las causales de no cobro de cuotas de recuperacion.
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultarCausalesNoCobroCuota() throws LogicException {
		SpAsConsultarCausalesDeNoCobroDelegate delegate = new SpAsConsultarCausalesDeNoCobroDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		}catch(DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_CAUSAS_NO_COBRO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getCausalesNoCobroCuota();
	}
}
