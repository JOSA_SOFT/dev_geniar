package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASAsociarProgramacionEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionAuditorMedicoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionRequiereOtroAuditorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionAlternativasAuditorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionCausasEstadosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionDireccionamientoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionEstadosGestionAuditorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionFundamentoLegalDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASGuardarInformacionGestionDomiciliariaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpConsultaProgramacionEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.model.UsuarioVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.CausasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.FundamentoLegalVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.GrupoAuditorVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosNegacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.GestionAuditorDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class GestionDomiciliariaController
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionDomiciliariaController {

	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;
	
	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public GestionDomiciliariaController() throws ConnectionProviderException, IOException{			
		if(connProviderSalud == null){
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar el direccionamiento
	 * @param numeroSolicitud
	 * @param consPrestacion
	 * @return
	 * @throws LogicException
	 */
	public DireccionamientoVO consultaDireccionamiento(int numeroSolicitud, int consPrestacion) throws LogicException{

		SpASConsultaDatosInformacionDireccionamientoDelegate direccionamiento = new SpASConsultaDatosInformacionDireccionamientoDelegate(numeroSolicitud, consPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, direccionamiento);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_DIRECCIONAMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return direccionamiento.getDireccionamiento();
	}

	/**
	 * Metodo para consultar los estados
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultaEstados() throws LogicException{

		SpASConsultaDatosInformacionEstadosGestionAuditorDelegate estados = new SpASConsultaDatosInformacionEstadosGestionAuditorDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, estados);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESTADOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if(estados.getListaEstados() == null || estados.getListaEstados().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESTADOS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return estados.getListaEstados();
	}

	/**
	 * Metodo para consultar las causas
	 * @param parametroNegocio
	 * @param numeroSolicitud
	 * @param prestacion
	 * @return
	 * @throws LogicException
	 */
	public List<CausasVO> consultaCausas(int parametroNegocio, int prestacion) throws LogicException{

		SpASConsultaDatosInformacionCausasEstadosDelegate causas = new SpASConsultaDatosInformacionCausasEstadosDelegate(parametroNegocio, prestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, causas);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CAUSAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return causas.getListaCausas();
	}
	
	public List<CausasVO> consultaCausasMalla(int parametroNegocio, int prestacion) throws LogicException{

		SpASConsultaDatosInformacionCausasEstadosDelegate causas = new SpASConsultaDatosInformacionCausasEstadosDelegate(parametroNegocio, prestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, causas);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CAUSAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if(causas.getListaCausas() == null || causas.getListaCausas().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CAUSAS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return causas.getListaCausas();
	}

	/**
	 * Metodo para consultar los fundamentos
	 * @param numeroSolicitud
	 * @param consPrestacion
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<FundamentoLegalVO> consultaFundamentos(int numeroSolicitud, int consPrestacion) throws LogicException{

		SpASConsultaDatosInformacionFundamentoLegalDelegate fundamentos = new SpASConsultaDatosInformacionFundamentoLegalDelegate(numeroSolicitud, consPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, fundamentos);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_FUNDAMENTOS_LEGALES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if(fundamentos.getListaFundamentos() == null || fundamentos.getListaFundamentos().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_FUNDAMENTOS_LEGALES), LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		
		return fundamentos.getListaFundamentos();
	}

	/**
	 * Metodo para consultar las alternativas
	 * @param consPrestacion
	 * @return	
	 * @throws LogicException
	 */
	public List<AlternativasVO> consultaAlternativas(int consPrestacion) throws  LogicException{

		SpASConsultaDatosInformacionAlternativasAuditorDelegate alternativas = new SpASConsultaDatosInformacionAlternativasAuditorDelegate(consPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, alternativas);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ALTERNATIVAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return alternativas.getListaAlternativas();
	}

	/**
	 * Metodo para consultar los medicos auditores
	 * @param numeroSolicitud
	 * @param consPrestacion
	 * @return	 
	 * @throws LogicException
	 */
	public List<UsuarioVO> consultarMedicosAuditores(String strLogin) throws LogicException{
		SpASConsultaDatosInformacionAuditorMedicoDelegate delegate = new SpASConsultaDatosInformacionAuditorMedicoDelegate(strLogin);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AUDITORES_MEDICOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getLstMedicosAuditoresVO();
			
	}

	/**
	 * Metodo para guardar la informacion de la gestion domiciliaria
	 * @param afiliado
	 * @param solicitud
	 * @param lstPrestaciones
	 * @param gestionAuditor
	 * @param datosNegacion
	 * @param usuario
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public void guardarGestionDomiciliaria(AfiliadoVO afiliado, 
			SolicitudBpmVO solicitud, List<PrestacionesAprobadasVO> lstPrestaciones, Integer numPrestacion,
			GestionAuditorDTO gestionAuditor, DatosNegacionDTO datosNegacion, String usuario) throws LogicException {
		
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlGestionDomiciliaria(afiliado, solicitud, lstPrestaciones, numPrestacion, gestionAuditor, datosNegacion, usuario);
		SpASGuardarInformacionGestionDomiciliariaDelegate delegate = new SpASGuardarInformacionGestionDomiciliariaDelegate(xml);
		
		try {	
		    SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_GESTION_DOMICILIARIA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
	}
		

	/**
	 * Metodo que consulta la programacion de entrega
	 * @param nui
	 * @param consecutivoTipoPrestacion
	 * @param consecutivoPrestacion
	 * @return
	 * @throws LogicException
	 */
	public List<ProgramacionEntregaVO> consultarProgramacionEntrega(Integer nui, Integer consecutivoTipoPrestacion, Integer consecutivoPrestacion) throws LogicException{
		SpConsultaProgramacionEntregaDelegate delegate = new SpConsultaProgramacionEntregaDelegate(nui, consecutivoTipoPrestacion, consecutivoPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
				
		return delegate.getLstProgramacionEntregaVO();
	}
	
	/**
	 * Metodo para consultar el grupo de los auditores
	 * @param numeroSolicitud
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<GrupoAuditorVO> consultarGruposAuditores(Integer numeroSolicitud, Integer consegutivoGrupoAuditor) throws LogicException{
		SpASConsultaDatosInformacionRequiereOtroAuditorDelegate delegate = new SpASConsultaDatosInformacionRequiereOtroAuditorDelegate(numeroSolicitud, consegutivoGrupoAuditor);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPO_AUDITOR), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(delegate.getLstGrupoAuditorVO() == null || delegate.getLstGrupoAuditorVO().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPO_AUDITOR), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getLstGrupoAuditorVO();		
	}
	
	/**
	 * Metodo para asociar la programacion de entrega de una prestacion
	 * @param consProgramacionPrestacion
	 * @param numeroPrestacion
	 * @param consecutivoProgramacion
	 * @param usuario
	 * @throws LogicException
	 */
	public void asociarProgramacionEntrega(Integer consSolicitud, Integer consecutivoPrestacionSOS, Integer consTipoIdentificacionAfiliado, String numeroIdentificacionAfiliado, String usuario) throws LogicException {
		try {
			SpASAsociarProgramacionEntregaDelegate delegate = new SpASAsociarProgramacionEntregaDelegate(consSolicitud, consecutivoPrestacionSOS, consTipoIdentificacionAfiliado, numeroIdentificacionAfiliado, usuario);
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ASOCIAR_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

}
