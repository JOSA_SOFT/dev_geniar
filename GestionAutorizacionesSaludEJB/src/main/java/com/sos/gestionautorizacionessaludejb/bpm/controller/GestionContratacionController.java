package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;





import javax.faces.model.SelectItem;





import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion.SpASConsultaDatosInformacionPrestacionSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion.SpASConsultaDatosInformacionEmpleadorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestioncontratacion.SpASGuardarDatosContratacionParametrosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerTipoCodificacionDelegate;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;


/**
 * Class GestionContratacionController
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionContratacionController {

	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;

	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public GestionContratacionController() throws ConnectionProviderException, IOException{		
		if(connProviderSalud == null){
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar los empleadores
	 * @param numeroSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<EmpleadorVO> consultaEmpleadores(int numeroSolicitud) throws LogicException{
		SpASConsultaDatosInformacionEmpleadorDelegate listaEmpleadores = new SpASConsultaDatosInformacionEmpleadorDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaEmpleadores);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_EMPLEADORES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(listaEmpleadores.getListaEmpeladores() == null || listaEmpleadores.getListaEmpeladores().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ANTECEDENTES_PRESTACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		
		return listaEmpleadores.getListaEmpeladores();		
	}

	/**
	 * Metodo para consultar las prestaciones aprobadas
	 * @param numeroSolicitud
	 * @param estado
	 * @return
	 * @throws LogicException
	 */
	public List<PrestacionesAprobadasVO> consultaPrestacionesAprobadas(int numeroSolicitud, String estado) throws LogicException{
		SpASConsultaDatosInformacionPrestacionSolicitudDelegate listaPrestacionesAprobadas = new SpASConsultaDatosInformacionPrestacionSolicitudDelegate(numeroSolicitud, estado);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaPrestacionesAprobadas);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_APROBADAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(listaPrestacionesAprobadas.getListaPrestacionesAprobadas() == null ||listaPrestacionesAprobadas.getListaPrestacionesAprobadas().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_APROBADAS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return listaPrestacionesAprobadas.getListaPrestacionesAprobadas();		
	}

	/**
	 * Metodo para consultar los tipos de prestaciones
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultarTiposPrestaciones() throws LogicException {
		int validacionCuos = 0;
		SpTraerTipoCodificacionDelegate spTraerTipoCodificacionDelegate = new SpTraerTipoCodificacionDelegate(validacionCuos);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, spTraerTipoCodificacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_CODIFICACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(spTraerTipoCodificacionDelegate.getlTipoCodificacionBPM() == null || spTraerTipoCodificacionDelegate.getlTipoCodificacionBPM().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_CODIFICACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return spTraerTipoCodificacionDelegate.getlTipoCodificacionBPM();
	}

	/**
	 * Metodo para guardar la gestion de la prestacion
	 * @param codigoTipoPrestacion
	 * @param numeroSolicitud
	 * @param consecutivoTipoPrestacion
	 * @param consecutivoPrestacion
	 * @param consecutivoPrestador
	 * @param usuario
	 * @param observaciones
	 * @throws LogicException
	 */
	public void guardarGestionPrestacion(int codigoTipoPrestacion, int numeroSolicitud, int consecutivoTipoPrestacion, int consecutivoPrestacion, String consecutivoPrestador, String usuario, String observaciones, String nombrePrestacion) throws LogicException {
		SpASGuardarDatosContratacionParametrosDelegate spASGuardarDatosContratacionParametrosDelegate = new SpASGuardarDatosContratacionParametrosDelegate(codigoTipoPrestacion, numeroSolicitud, consecutivoTipoPrestacion, consecutivoPrestacion, consecutivoPrestador, usuario, observaciones, nombrePrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, spASGuardarDatosContratacionParametrosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
}
