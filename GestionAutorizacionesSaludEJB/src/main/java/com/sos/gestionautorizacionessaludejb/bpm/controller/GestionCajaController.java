package com.sos.gestionautorizacionessaludejb.bpm.controller;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCAperturaCajaDelegate;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;

/**
 * Clase para la gestion de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class GestionCajaController {

	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderCaja;

	/**
	 * Constructor
	 * 
	 * @throws ConnectionProviderException
	 */
	public GestionCajaController() throws ConnectionProviderException {
		if (connProviderCaja == null) {
			connProviderCaja = ConnProviderSiSalud.getConnProvider();
		}
	}

	/**
	 * Permite realizar la apertura de la caja
	 * 
	 * @param bse
	 * @param usroCrcn
	 * @param cnsctvoCdgoOfcna
	 * @param cnsctvoCdgoEstdo
	 * @param drccnIp
	 * @param fchaCrcn
	 * @throws DataAccessException
	 */
	public void abrirCaja(Double bse, String usroCrcn,
			Integer cnsctvoCdgoOfcna, Integer cnsctvoCdgoEstdo, String drccnIp,
			java.util.Date fchaCrcn) throws DataAccessException {
		SpRCAperturaCajaDelegate aperturaCajaDelegate = new SpRCAperturaCajaDelegate(
				bse, usroCrcn, cnsctvoCdgoOfcna, cnsctvoCdgoEstdo, drccnIp,
				fchaCrcn);
		SOSDataAccess.ejecutarSQL(connProviderCaja, aperturaCajaDelegate);
	}
}
