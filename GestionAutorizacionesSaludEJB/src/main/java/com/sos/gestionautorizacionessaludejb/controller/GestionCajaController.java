package com.sos.gestionautorizacionessaludejb.controller;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCActualizarContactoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCActualizarNotaCreditoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCAperturaCajaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCBuscadorUsuarioWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCCierreCajaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCCierreOficinaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultaAfiliadoIDDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultaDatosContactoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultaInformesDetalleDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultaPrestacionesNumeroOPSDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarCajaAbiertaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarCoordinadorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarDenominacionesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarMovimientosCajaPorOficinaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarOPSsValorNotasCreditoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarOficinaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarOficinaPorMovimientoActualDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarUsuarioWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCGenerarInformeNotaCreditoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerCausalesExoneracionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerReciboCajaSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerSoportesDocumentoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerTotalesCierreDelegate;
import com.sos.gestionautorizacionessaluddata.model.caja.AfiliadoNombresVO;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesExentosVO;
import com.sos.gestionautorizacionessaluddata.model.caja.CierreOficinaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.CoordinadorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DenominacionVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeNotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeOPSVO;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.MovimientoCajaxCausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroReporteCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteCajaDetalladoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.TotalCierreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.UsuarioWebVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;

/**
 * Clase para la gestion de caja
 * 
 * @author Fabian Caicedo - Geniar SAS
 *
 */
public class GestionCajaController implements Serializable {
	private static final long serialVersionUID = 6202830919662912083L;
	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderCaja;

	/**
	 * Constructor
	 * 
	 * @throws ConnectionProviderException
	 */
	public GestionCajaController() throws ConnectionProviderException {
		if (connProviderCaja == null) {
			connProviderCaja = ConnProviderSiSalud.getConnProvider();
		}
	}

	/**
	 * Permite realizar la apertura de la caja
	 * 
	 * @param bse
	 * @param usroCrcn
	 * @param cnsctvoCdgoOfcna
	 * @param cnsctvoCdgoEstdo
	 * @param drccnIp
	 * @param fecha
	 * @throws DataAccessException
	 */
	public void abrirCaja(Double bse, String usroCrcn,
			Integer cnsctvoCdgoOfcna, Integer cnsctvoCdgoEstdo, String drccnIp,
			Date fecha) throws DataAccessException {
		SpRCAperturaCajaDelegate aperturaCajaDelegate = new SpRCAperturaCajaDelegate(
				bse, usroCrcn, cnsctvoCdgoOfcna, cnsctvoCdgoEstdo, drccnIp,
				fecha);
		SOSDataAccess.ejecutarSQL(connProviderCaja, aperturaCajaDelegate);
	}

	/**
	 * Consultar usuario web con el user login y modulo
	 * 
	 * @param usroCrcn
	 * @param cnsctvoCdgoMdlo
	 * @return
	 * @throws DataAccessException
	 */
	public UsuarioWebVO consultarUsuarioWeb(String usroCrcn,
			Integer cnsctvoCdgoMdlo) throws DataAccessException {
		SpRCConsultarUsuarioWebDelegate consultarUsuarioWeb = new SpRCConsultarUsuarioWebDelegate(
				usroCrcn, cnsctvoCdgoMdlo);
		SOSDataAccess.ejecutarSQL(connProviderCaja, consultarUsuarioWeb);
		return consultarUsuarioWeb.getUsuarioWebVO();
	}

	/**
	 * Consulta los registrados con estado abierto del usuario
	 * 
	 * @param usroCrcn
	 * @return
	 * @throws DataAccessException
	 */
	public List<MovimientoCajaVO> consultarMovimientoCajaAbierta(String usroCrcn)
			throws DataAccessException {
		SpRCConsultarCajaAbiertaDelegate consultarCajaAbiertaDelegate = new SpRCConsultarCajaAbiertaDelegate(
				usroCrcn);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				consultarCajaAbiertaDelegate);
		return consultarCajaAbiertaDelegate.getListMovimientosCja();
	}

	/**
	 * Consulta las denominaciones registradas
	 * 
	 * @param conseTipoDenominacion
	 * @return
	 * @throws DataAccessException
	 */
	public List<DenominacionVO> consultarDenominaciones(
			Integer conseTipoDenominacion) throws DataAccessException {
		SpRCConsultarDenominacionesDelegate consultarDenominacionesDelegate = new SpRCConsultarDenominacionesDelegate(
				conseTipoDenominacion);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				consultarDenominacionesDelegate);
		return consultarDenominacionesDelegate.getListDenominaciones();
	}

	/**
	 * Suma las notas cerdito en estado cobrado y los recibos caja en estado
	 * generado a partir de un consecutivo de un movimiento que caja en estado
	 * abierto o apertura
	 * 
	 * @param consecutivoMovimientoCaja
	 * @return
	 * @throws DataAccessException
	 */
	public TotalCierreVO obtenerTotalesCierre(Integer consecutivoMovimientoCaja)
			throws DataAccessException {
		SpRCObtenerTotalesCierreDelegate obtenerTotalesCierreDelegate = new SpRCObtenerTotalesCierreDelegate(
				consecutivoMovimientoCaja);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				obtenerTotalesCierreDelegate);
		return obtenerTotalesCierreDelegate.getTotalesCierreVO();
	}

	/**
	 * Cierra la caja
	 * 
	 * @param movimientoCajaModificar
	 * @param causalesXML
	 * @throws LogicException
	 * @throws ParserConfigurationException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 * @throws DataAccessException
	 */
	public void cerrarCaja(MovimientoCajaVO movimientoCajaModificar,
			List<MovimientoCajaxCausalDescuadreVO> causalesXML)
			throws LogicException {
		SpRCCierreCajaDelegate cierreCajaDelegate;
		try {
			cierreCajaDelegate = new SpRCCierreCajaDelegate(
					movimientoCajaModificar, obtenerXMLCausales(causalesXML));
			SOSDataAccess.ejecutarSQL(connProviderCaja.obtenerConexion(),
					cierreCajaDelegate, true);
		} catch (ParserConfigurationException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerFactoryConfigurationError e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (DataAccessException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}

	/**
	 * Consulta la oficina por el consecutivo
	 * 
	 * @param consecutivoOficina
	 * @return
	 * @throws DataAccessException
	 */
	public OficinasVO consultarOficinaPorConsecutivo(Integer consecutivoOficina)
			throws DataAccessException {
		SpRCConsultarOficinaDelegate consultarOficinaDelegate = new SpRCConsultarOficinaDelegate(
				consecutivoOficina);
		SOSDataAccess.ejecutarSQL(connProviderCaja, consultarOficinaDelegate);
		return consultarOficinaDelegate.getOficinasVO();
	}

	/**
	 * Consulta la oficina del movimiento actual
	 * 
	 * @param userLogin
	 * @return
	 * @throws DataAccessException
	 */
	public OficinasVO consultarOficinaPorMovimientoActual(String userLogin)
			throws DataAccessException {
		SpRCConsultarOficinaPorMovimientoActualDelegate consultarOficinaPorMovimientoActualDelegate = new SpRCConsultarOficinaPorMovimientoActualDelegate(
				userLogin);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				consultarOficinaPorMovimientoActualDelegate);
		return consultarOficinaPorMovimientoActualDelegate.getOficinasVO();
	}

	/**
	 * Consulta un coordinador de acuerdo al login
	 * 
	 * @param loginUser
	 * @return
	 * @throws DataAccessException
	 */
	public CoordinadorVO consultarCoordinador(String loginUser)
			throws DataAccessException {
		SpRCConsultarCoordinadorDelegate consultarCoordinadorDelegate = new SpRCConsultarCoordinadorDelegate(
				loginUser);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				consultarCoordinadorDelegate);
		return consultarCoordinadorDelegate.getCoordinadorVO();
	}

	/**
	 * Consulta movimientos caja por oficina
	 * 
	 * @param consecutivoOficina
	 * @return
	 * @throws DataAccessException
	 */
	public List<MovimientoCajaVO> consultarMovimientosCajaPorOficina(
			Integer consecutivoOficina) throws DataAccessException {
		SpRCConsultarMovimientosCajaPorOficinaDelegate consultarMovimientosCajaPorOficinaDelegate = new SpRCConsultarMovimientosCajaPorOficinaDelegate(
				consecutivoOficina);
		SOSDataAccess.ejecutarSQL(connProviderCaja,
				consultarMovimientosCajaPorOficinaDelegate);
		return consultarMovimientosCajaPorOficinaDelegate
				.getListMovimientosCaja();
	}

	/**
	 * Cierra la oficina
	 * 
	 * @param cierreOficinaVO
	 * @param listMovimientosCerrados
	 * @param listMovimientosCierreAdmin
	 * @return
	 * @throws LogicException
	 * @throws ParserConfigurationException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 * @throws DataAccessException
	 */
	public Integer cierreOficina(CierreOficinaVO cierreOficinaVO,
			List<MovimientoCajaVO> listMovimientosCerrados,
			List<MovimientoCajaVO> listMovimientosCierreAdmin)
			throws LogicException {
		SpRCCierreOficinaDelegate cierreOficinaDelegate;
		try {
			cierreOficinaDelegate = new SpRCCierreOficinaDelegate(
					cierreOficinaVO, obtenerXMLMovimientosCja(
							listMovimientosCerrados, 1),
					obtenerXMLMovimientosCja(listMovimientosCierreAdmin, 2));

			SOSDataAccess.ejecutarSQL(connProviderCaja.obtenerConexion(),
					cierreOficinaDelegate, true);
			return cierreOficinaDelegate.getCnsctvoCrreOfcna();
		} catch (ParserConfigurationException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerFactoryConfigurationError e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (DataAccessException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		}

	}

	/**
	 * Devuelve el XML de las causales para enviar a la base de datos a partir
	 * del VO
	 * 
	 * @param list
	 * @return
	 * @throws ParserConfigurationException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	private static String obtenerXMLCausales(
			List<MovimientoCajaxCausalDescuadreVO> list)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		if (list == null || list.isEmpty()) {
			return null;
		}

		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = dFact.newDocumentBuilder();
		Document doc = build.newDocument();

		Element root = doc.createElement(ConstantesEJB.CAUSALES);
		doc.appendChild(root);

		for (MovimientoCajaxCausalDescuadreVO dtl : list) {
			Element details = doc.createElement(ConstantesEJB.CAUSAL);
			root.appendChild(details);

			Element cnsctvoMvmntoCja = doc
					.createElement(ConstantesEJB.NODE_CNSCTVO_MVMNTO_CJA);
			cnsctvoMvmntoCja.appendChild(doc.createTextNode(String.valueOf(dtl
					.getCnsctvoMvmntoCja())));
			details.appendChild(cnsctvoMvmntoCja);

			Element cnsctvoCdgoCslCscdre = doc
					.createElement(ConstantesEJB.NODE_CNSCTVO_CDGO_CSL_DSCDRE);
			cnsctvoCdgoCslCscdre.appendChild(doc.createTextNode(String
					.valueOf(dtl.getCnsctvoCdgoCslCscdre())));
			details.appendChild(cnsctvoCdgoCslCscdre);

			Element vlrDscdre = doc
					.createElement(ConstantesEJB.NODE_VLR_DSCDRE);
			vlrDscdre.appendChild(doc.createTextNode(String.valueOf(dtl
					.getVlrDscdre())));
			details.appendChild(vlrDscdre);

			Element usroCrcn = doc.createElement(ConstantesEJB.NODE_USRO_CRCN);
			usroCrcn.appendChild(doc.createTextNode(String.valueOf(dtl
					.getUsroCrcn())));
			details.appendChild(usroCrcn);

		}
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, ConstantesEJB.YES);

		DOMSource source = new DOMSource(doc);

		transformer.transform(source,
				new javax.xml.transform.stream.StreamResult(writer));

		return writer.toString();
	}
	
	
	private static void obtenerXmlDetalleCierreAdministrativo(List<MovimientoCajaVO> list, Document doc, Element root){
		for (MovimientoCajaVO dtl : list) {
			Element details = doc.createElement(ConstantesEJB.NODE_MOVIMIENTOCJA);
			
			root.appendChild(details);

			Element cnsctvoMvmntoCja = doc
					.createElement(ConstantesEJB.NODE_CNSCTVO_MVMNTO_CJA);
			cnsctvoMvmntoCja.appendChild(doc.createTextNode(String
					.valueOf(dtl.getCnsctvoMvmntoCja())));
			details.appendChild(cnsctvoMvmntoCja);

			Element usroUltmaMdfccn = doc
					.createElement(ConstantesEJB.NODE_USRO_ULTMA_MDFCCN);
			usroUltmaMdfccn.appendChild(doc.createTextNode(String
					.valueOf(dtl.getUsroUltmaMdfccn())));
			details.appendChild(usroUltmaMdfccn);

			Element ttlEfctvo = doc
					.createElement(ConstantesEJB.NODE_TTL_EFCTVO);
			ttlEfctvo.appendChild(doc.createTextNode(String.valueOf(dtl
					.getTtlEfctvo())));
			details.appendChild(ttlEfctvo);

			Element ttlNtaCrdto = doc
					.createElement(ConstantesEJB.NODE_TTL_NTA_CRDTO);
			ttlNtaCrdto.appendChild(doc.createTextNode(String.valueOf(dtl
					.getTtlNtaCrdto())));
			details.appendChild(ttlNtaCrdto);

			Element ttlCja = doc.createElement(ConstantesEJB.NODE_TTL_CJA);
			ttlCja.appendChild(doc.createTextNode(String.valueOf(dtl
					.getTtlCja())));
			details.appendChild(ttlCja);

			Element sbrnteCrre = doc
					.createElement(ConstantesEJB.NODE_SBRNTE_CRRE);
			sbrnteCrre.appendChild(doc.createTextNode(String.valueOf(dtl
					.getSbrnteCrre())));
			details.appendChild(sbrnteCrre);

			Element fltnteCrre = doc
					.createElement(ConstantesEJB.NODE_FLTNTE_CRRE);
			fltnteCrre.appendChild(doc.createTextNode(String.valueOf(dtl
					.getFltnteCrre())));
			details.appendChild(fltnteCrre);
		}
	}
	
	private static void obtenerXmlDetalleCierre(List<MovimientoCajaVO> list, Document doc, Element root){
		for (MovimientoCajaVO dtl : list) {
			Element details = doc
					.createElement(ConstantesEJB.NODE_MOVIMIENTOCJA);
			root.appendChild(details);

			Element cnsctvoMvmntoCja = doc
					.createElement(ConstantesEJB.NODE_CNSCTVO_MVMNTO_CJA);
			cnsctvoMvmntoCja.appendChild(doc.createTextNode(String
					.valueOf(dtl.getCnsctvoMvmntoCja())));
			details.appendChild(cnsctvoMvmntoCja);

			Element usroUltmaMdfccn = doc
					.createElement(ConstantesEJB.NODE_USRO_ULTMA_MDFCCN);
			usroUltmaMdfccn.appendChild(doc.createTextNode(String
					.valueOf(dtl.getUsroUltmaMdfccn())));
			details.appendChild(usroUltmaMdfccn);
		}
	}

	/**
	 * Devuelve un XML con la informacion de los movimientos caja <br/>
	 * Recibe la opcion 1 si son movimientos en estado de 2 (Cerrado)<br/>
	 * Recibe la opcion 2 si son movimientos en estado de 4 (Cierre
	 * Administrativo)
	 * 
	 * @param list
	 * @param opcion
	 * @return
	 * @throws ParserConfigurationException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	private static String obtenerXMLMovimientosCja(List<MovimientoCajaVO> list,
			int opcion) throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		if (list == null || list.isEmpty()) {
			return null;
		}

		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = dFact.newDocumentBuilder();
		Document doc = build.newDocument();

		Element root = doc.createElement(ConstantesEJB.NODE_MOVIMIENTOSCJA);
		doc.appendChild(root);

		if (opcion == ConstantesEJB.ESTADO_CIERRE_ADMINISTRATIVO) {
			obtenerXmlDetalleCierreAdministrativo(list, doc, root);
		}

		if (opcion == ConstantesEJB.ESTADO_CIERRE) {
			obtenerXmlDetalleCierre(list, doc, root);
		}
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, ConstantesEJB.YES);

		DOMSource source = new DOMSource(doc);

		transformer.transform(source,
				new javax.xml.transform.stream.StreamResult(writer));
		return writer.toString();
	}

	/**
	 * Controlador que se encarga de consultar el reporte.
	 * 
	 * @param entrada
	 * @return
	 * @throws DataAccessException
	 */
	public List<ReporteCajaDetalladoVO> consultarReporteCajaDetallado(
			ParametroReporteCierreCajaVO entrada) throws DataAccessException {
		SpRCConsultaInformesDetalleDelegate delegate = new SpRCConsultaInformesDetalleDelegate(
				entrada);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getResultado();
	}

	/**
	 * Consulta el grupo de OPSs y sus valores
	 * 
	 * @param numeroNotaCredito
	 * @return
	 * @throws DataAccessException
	 */
	public List<InformeOPSVO> consultarGrupoOPSconValor(
			Integer numeroNotaCredito) throws DataAccessException {
		SpRCConsultarOPSsValorNotasCreditoDelegate consultarOPSsValorNotasCreditoDelegate = new SpRCConsultarOPSsValorNotasCreditoDelegate(
				numeroNotaCredito);
		SOSDataAccess.ejecutarAutoCommitSQL(connProviderCaja,
				consultarOPSsValorNotasCreditoDelegate);
		return consultarOPSsValorNotasCreditoDelegate.getListInformeOPSVO();
	}
	
	/**
	 * Buscador de usuario y login
	 * @param usuario
	 * @param login
	 * @return
	 * @throws DataAccessException
	 */
	public List<UsuarioWebVO> consultarUsuariosLogin(
			String usuario, String login) throws DataAccessException {
		SpRCBuscadorUsuarioWebDelegate delegate = new SpRCBuscadorUsuarioWebDelegate(usuario, login);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getListaUsuarios();
	}
	
	/**
	 * Consulta afiliado por tipo y numero ID
	 * @param consecutivoTipoID
	 * @param numeroIdentificacion
	 * @return
	 * @throws DataAccessException
	 */
	public AfiliadoNombresVO obtenerAfiliado(int consecutivoTipoID,
			String numeroIdentificacion) throws DataAccessException{
		SpRCConsultaAfiliadoIDDelegate delegate = new SpRCConsultaAfiliadoIDDelegate(consecutivoTipoID,numeroIdentificacion);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getAfiliado();
	}
	
	/**
	 * Consulta las prestaciones asociadas a un numero OPS
	 * @param numeroUnicoOps
	 * @param consecutivoSolicitud
	 * @return
	 * @throws DataAccessException
	 */
	public List<PrestacionDTO> obtenerPrestacionesNumeroUnicoOPS(int numeroUnicoOps) throws DataAccessException{
		SpRCConsultaPrestacionesNumeroOPSDelegate delegate = new SpRCConsultaPrestacionesNumeroOPSDelegate(numeroUnicoOps);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getListaPrestaciones();
	}
	
	/**
	 * Obtiene la causales de no pago de una solicitud
	 * @param consecutivoSolicitud
	 * @return
	 * @throws DataAccessException
	 */
	public List<CausalesExentosVO> obtenerCausalesNoPago(int consecutivoSolicitud) throws DataAccessException{
		SpRCObtenerCausalesExoneracionDelegate delegate = new SpRCObtenerCausalesExoneracionDelegate(consecutivoSolicitud);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getListaCausales();
	}
	
	/**
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param nuiAfiliado
	 * @param estadoDocumento
	 * @return
	 * @throws DataAccessException
	 */
	public List<InformeNotasCreditoVO> consultarReporteNotasCredito(Date fechaInicial, Date fechaFinal,
			Integer nuiAfiliado, Integer estadoDocumento) throws DataAccessException{
		SpRCGenerarInformeNotaCreditoDelegate delegate = new SpRCGenerarInformeNotaCreditoDelegate(fechaInicial, fechaFinal, nuiAfiliado, estadoDocumento);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getResultado();
	}
	
	/**
	 * 
	 * @param numeroDocumento
	 * @return
	 * @throws DataAccessException
	 */
	public OPSCajaVO obtenerInformacionOPSValor(int numeroDocumento)
			throws DataAccessException {
		SpRCObtenerSoportesDocumentoDelegate delegate = new SpRCObtenerSoportesDocumentoDelegate(
				numeroDocumento);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getOps();
	}
	
	/**
	 * 
	 * @param consecutivoContacto
	 * @return
	 * @throws DataAccessException
	 */
	public DatosContactoVO obtenerInformacionDatoContacto(int consecutivoContacto)
			throws DataAccessException {
		SpRCConsultaDatosContactoDelegate delegate = new SpRCConsultaDatosContactoDelegate(
				consecutivoContacto);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getContacto();
	}
	
	/**
	 * 
	 * @param contacto
	 * @param usuario
	 * @return
	 * @throws DataAccessException
	 */
	public int guardarDatosContacto(DatosContactoVO contacto, String usuario)
			throws DataAccessException {
		SpRCActualizarContactoDelegate spContacto = new SpRCActualizarContactoDelegate(
				contacto, usuario);
		SOSDataAccess.ejecutarSQL(connProviderCaja, spContacto);
		return spContacto.getConsecutivoContacto();
	}
	
	/**
	 * 
	 * @param documento
	 * @throws DataAccessException
	 */
	public void actualizarNotaCredito(DocumentoCajaVO documento)
			throws DataAccessException {
		SpRCActualizarNotaCreditoDelegate delegate = new SpRCActualizarNotaCreditoDelegate(documento);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
	}
	
	/**
	 * Controlador que se encarga de obtener el recibo de caja a traves del consecutivo de solicitud.
	 * @param consecutivoSolicitud
	 * @return
	 * @throws DataAccessException
	 */
	public ReciboCajaJasperVO obtenerReciboCajaSolicitud(int consecutivoSolicitud)
			throws DataAccessException {
		SpRCObtenerReciboCajaSolicitudDelegate delegate = new SpRCObtenerReciboCajaSolicitudDelegate(consecutivoSolicitud);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getReciboCajaJasper();
	}
	
}