package com.sos.gestionautorizacionessaludejb.controller;

import java.math.BigInteger;


import co.com.sos.enterprise.solicitud.v1.SolicitudService;
import co.com.sos.grabaridentificadordocumentoanexo.v1.GrabarIdentificadorDocumentoAnexoResType;
import co.com.sos.grabarsolicitud.v1.GrabarSolicitudResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;

import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.GuardarSolicitudService;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

public class GestionarSolicitudController {
	
	private GuardarSolicitudService guardarSolicitudService;
	
	/*Metodos para la ejecucion del servicio de guardar*/
	
	/** Metodo que permite realizar la conexion al servicio de guardar */
	public SolicitudService crearSolicitudService() throws LogicException, ServiceException{
		
		guardarSolicitudService = new GuardarSolicitudService();		
		if(guardarSolicitudService.crearSolicitudService() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_SOLICITUD_SERVICE), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return guardarSolicitudService.crearSolicitudService();
	}
	
	// Metodo que permite guardar la solicitud
	public GrabarSolicitudResType guardarSolicitud(SolicitudService service, RegistrarSolicitudVO solicitudVO, String usuarioSession, SolicitudVO datosGuardarSolicitud, ValidacionEspecialVO validacionEspecialVO) throws LogicException, ServiceException{
	
		if(guardarSolicitudService == null){
			this.crearSolicitudService();
		}
		
		GrabarSolicitudResType grabarSolicitudResType = guardarSolicitudService.guardarSolicitud(service, solicitudVO, usuarioSession, datosGuardarSolicitud, validacionEspecialVO);
		
		if(grabarSolicitudResType == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_SOLICITUD_SERVICE), LogicException.ErrorType.DATO_NO_EXISTE);
	    }		
		return grabarSolicitudResType;
	}
	
	public GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexos(String usuarioSession, BigInteger consecutivoSolicitud, BigInteger identificadorDocumento) throws LogicException, ServiceException{
		if(guardarSolicitudService == null){
			this.crearSolicitudService();
		}
		GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexoResType =guardarSolicitudService.grabarIdentificadorDocumentoAnexos(usuarioSession, consecutivoSolicitud, identificadorDocumento);
		if(grabarIdentificadorDocumentoAnexoResType == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GRABAR_IDENTIFICADOR_DOCUMENTO), LogicException.ErrorType.DATO_NO_EXISTE);
	    }
		return grabarIdentificadorDocumentoAnexoResType;
	}
	
	// Metodo consultar el resultado del servicio de guardar
	public SolicitudVO consultaResultadoGuardar(GrabarSolicitudResType grabarSolicitudResType,  RegistrarSolicitudVO registrarSolicitudVO, SolicitudVO datosGuardarSolicitud) throws LogicException, ServiceException {
		SolicitudVO solicitudVO = null;
		
		if(guardarSolicitudService == null){
			this.crearSolicitudService();
		}				
		solicitudVO = guardarSolicitudService.consultaResultadoGuardar(grabarSolicitudResType, registrarSolicitudVO, datosGuardarSolicitud);
		
		if(grabarSolicitudResType == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_SOLICITUD_SERVICE), LogicException.ErrorType.DATO_NO_EXISTE);
	    }	
		
		return solicitudVO;		
	}	
	
	// Metodo que permite guardar la solicitud
	public GrabarSolicitudResType modificarSolicitud(SolicitudService service, RegistrarSolicitudVO solicitudVO, String usuarioSession, SolicitudVO datosGuardarSolicitud, ValidacionEspecialVO validacionEspecialVO) throws LogicException, ServiceException{
				
		if(guardarSolicitudService == null){
			this.crearSolicitudService();
		}
		
		GrabarSolicitudResType grabarSolicitudResType = guardarSolicitudService.guardarSolicitud(service, solicitudVO, usuarioSession, datosGuardarSolicitud, validacionEspecialVO);
		
		if(grabarSolicitudResType == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_SOLICITUD_SERVICE), LogicException.ErrorType.DATO_NO_EXISTE);
	    }		
		return grabarSolicitudResType;
	}	
}
