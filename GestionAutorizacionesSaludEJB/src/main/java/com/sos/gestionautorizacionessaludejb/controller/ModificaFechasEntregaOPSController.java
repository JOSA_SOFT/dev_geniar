package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.modificarfechaentregaops.SpConsultaResumenOPSDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.modificarfechaentregaops.SpRecalcularDetalleOPSDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpCambiarFechaEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPmTraerTiposPlanDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerEstadosMegaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerGruposEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerTiposAfiliadoDelegate;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.dto.modificarfechaentregaops.CriteriosConsultaOps;
import com.sos.gestionautorizacionessaluddata.model.modificarfechaentregaops.TotalResumenOpsVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.EstadoMegaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GrupoEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.util.ConsultaInformacioOps;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

import co.eps.sos.dataccess.exception.DataAccessException;

public class ModificaFechasEntregaOPSController implements Serializable {

	private static final long serialVersionUID = 8748434112573394390L;
	private Date fechaConsulta;
	private ConnectionProvider connProviderSalud;

	public ModificaFechasEntregaOPSController() throws ConnectionProviderException, IOException {
		fechaConsulta = Utilidades.getFechaActual();

		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}

	// Funcion que obtiene los items de los tipos de planes
	public List<SelectItem> obtenerListaTiposPlan(List<PlanVO> listaVos) throws LogicException {
		List<PlanVO> listaVosResult;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultarListaTiposPlan(fechaConsulta);
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (PlanVO planVO : listaVosResult) {
			item = new SelectItem(planVO.getConsectivoPlan(), planVO.getDescripcionPlan());
			listaItems.add(item);
		}

		return listaItems;
	}

	/**
	 * Se encarga de consultar los tipos de planes
	 * 
	 * @param fechaActual
	 * @return
	 * @throws LogicException
	 */
	public List<PlanVO> consultarListaTiposPlan(Date fechaActual) throws LogicException {

		SpPmTraerTiposPlanDelegate pmTraerTiposPlanDelegate = new SpPmTraerTiposPlanDelegate(fechaActual);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, pmTraerTiposPlanDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_PLAN), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (pmTraerTiposPlanDelegate.getlTiposPlan().isEmpty() || pmTraerTiposPlanDelegate.getlTiposPlan() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_PLAN),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return pmTraerTiposPlanDelegate.getlTiposPlan();
	}

	// Funcion que obtiene los items de los tipos de afiliados
	public List<SelectItem> obtenerListaTiposAfiliado(List<AfiliadoVO> listaVos) throws LogicException {
		List<AfiliadoVO> listaVosResult;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultarListaTiposAfiliado(null, fechaConsulta, null, null);
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (AfiliadoVO afiliadoVO : listaVosResult) {
			item = new SelectItem(afiliadoVO.getConsecutivoTipoAfiliado(), afiliadoVO.getDescripcionTipoAfiliado());
			listaItems.add(item);
		}

		return listaItems;
	}

	/**
	 * Se encarga de consultar los tipos de afiliados
	 * 
	 * @param codigoAfiliado
	 * @param fechaReferencia
	 * @param cadenaSeleccion
	 * @param visibleUsuario
	 * @return
	 * @throws LogicException
	 */
	public List<AfiliadoVO> consultarListaTiposAfiliado(String codigoAfiliado, Date fechaReferencia,
			String cadenaSeleccion, String visibleUsuario) throws LogicException {

		SpTraerTiposAfiliadoDelegate traerTiposAfiliadoDelegate = new SpTraerTiposAfiliadoDelegate(codigoAfiliado,
				fechaReferencia, cadenaSeleccion, visibleUsuario);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, traerTiposAfiliadoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_AFILIADO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (traerTiposAfiliadoDelegate.getlTiposAfiliado().isEmpty()
				|| traerTiposAfiliadoDelegate.getlTiposAfiliado() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_AFILIADO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return traerTiposAfiliadoDelegate.getlTiposAfiliado();
	}

	// Funcion que obtiene los items de los estados de mega
	public List<SelectItem> obtenerListaEstadosMega(List<EstadoMegaVO> listaVos) throws LogicException {
		List<EstadoMegaVO> listaVosResult;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultarListaEstadosMega(fechaConsulta);
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (EstadoMegaVO estadoMegaVO : listaVosResult) {
			item = new SelectItem(estadoMegaVO.getConsecutivoCodigoEstadoMega(),
					estadoMegaVO.getDescripcionEstadoMega());
			listaItems.add(item);
		}

		return listaItems;
	}

	/**
	 * Se encarga de consultar los estados de mega
	 * 
	 * @param fechaActual
	 * @return
	 * @throws LogicException
	 */
	public List<EstadoMegaVO> consultarListaEstadosMega(Date fechaActual) throws LogicException {

		SpTraerEstadosMegaDelegate traerEstadosMegaDelegate = new SpTraerEstadosMegaDelegate(fechaActual);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, traerEstadosMegaDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESTADOS_MEGA), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (traerEstadosMegaDelegate.getlEstadosMega().isEmpty()
				|| traerEstadosMegaDelegate.getlEstadosMega() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESTADOS_MEGA),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return traerEstadosMegaDelegate.getlEstadosMega();
	}

	// Funcion que obtiene los items de los grupos de entrega
	public List<SelectItem> obtenerListaGruposEntrega(List<GrupoEntregaVO> listaVos) throws LogicException {
		List<GrupoEntregaVO> listaVosResult;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultarListaGruposEntrega(fechaConsulta);
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (GrupoEntregaVO grupoEntregaVO : listaVosResult) {
			item = new SelectItem(grupoEntregaVO.getConsecutivoCodigoGrupoEntrega(),
					grupoEntregaVO.getDescripcionGrupoEntrega());
			listaItems.add(item);
		}

		return listaItems;
	}

	/**
	 * Se encarga de consultar los grupos de entrega
	 * 
	 * @param fechaActual
	 * @return
	 * @throws LogicException
	 */
	public List<GrupoEntregaVO> consultarListaGruposEntrega(Date fechaActual) throws LogicException {

		SpTraerGruposEntregaDelegate traerGruposEntregaDelegate = new SpTraerGruposEntregaDelegate(fechaActual);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, traerGruposEntregaDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPOS_ENTREGA), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (traerGruposEntregaDelegate.getlGruposEntrega().isEmpty()
				|| traerGruposEntregaDelegate.getlGruposEntrega() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPOS_ENTREGA),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return traerGruposEntregaDelegate.getlGruposEntrega();
	}
	

	public void cambiarFechasEntrega(Date fchaMdfcr, String usroCrcn) throws LogicException {
		try {
			SpCambiarFechaEntregaDelegate cambioFecha = new SpCambiarFechaEntregaDelegate(fchaMdfcr, usroCrcn);
			SOSDataAccess.ejecutarSQL(connProviderSalud, cambioFecha);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESUMEN_OPS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Se encarga de consultar los tipos de planes
	 * 
	 * @param criteriosConsultaOps
	 * @return
	 * @throws LogicException
	 */
	public List<TotalResumenOpsVO> consultarListaResumenOPS(CriteriosConsultaOps criteriosConsultaOps)
			throws LogicException {

		SpConsultaResumenOPSDelegate consultaResumenOPSDelegate = new SpConsultaResumenOPSDelegate(
				criteriosConsultaOps);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaResumenOPSDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESUMEN_OPS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaResumenOPSDelegate.getlTotalResumenOps().isEmpty()
				|| consultaResumenOPSDelegate.getlTotalResumenOps() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESUMEN_OPS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaResumenOPSDelegate.getlTotalResumenOps();
	}

	/**
	 * Se encarga de recalcular los tipos de planes
	 * 
	 * @param criteriosConsultaOps
	 * @return
	 * @throws LogicException
	 */
	public List<TotalResumenOpsVO> recalcularListaResumenOPS(List<TotalResumenOpsVO> listaTotalResumenOpsVO,
			Double valorBase, Integer prioridad, String usuario) throws LogicException {
		ConsultaInformacioOps consultaInformacioOps = new ConsultaInformacioOps();
		String listaTotalResumenXML = consultaInformacioOps.armarXMLResumenOps(listaTotalResumenOpsVO);

		SpRecalcularDetalleOPSDelegate recalcularResumenOPSDelegate = new SpRecalcularDetalleOPSDelegate(
				listaTotalResumenXML, valorBase, prioridad, usuario);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, recalcularResumenOPSDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESUMEN_OPS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (recalcularResumenOPSDelegate.getlTotalResumenOps().isEmpty()
				|| recalcularResumenOPSDelegate.getlTotalResumenOps() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESUMEN_OPS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return recalcularResumenOPSDelegate.getlTotalResumenOps();
	}

}
