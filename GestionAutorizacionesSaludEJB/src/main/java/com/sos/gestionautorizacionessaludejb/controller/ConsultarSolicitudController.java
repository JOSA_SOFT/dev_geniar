package com.sos.gestionautorizacionessaludejb.controller;

import java.sql.SQLException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaConceptosGastoSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaDetalleSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaInformacionFacturaSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaPrestacionFechaEntregaWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaPrestacionesSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultaSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarDocumentosAnexosxSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarExistenciaReciboCajaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarHistoricoDescargasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarHistoricoModificacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarMotivosCausasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarPerfilUsuario;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarPrestacionesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarPrestacionesOPSDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarResultadoMallayGestionAuditorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASEjecutarAnulacionSolicitudPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASGuardarHistoricoDescargasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASGuardarHistoricoReliquidacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASGuardarInfoFechaEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASGuardarInformacionModificacionRecobroDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASGuardarModificacionesOPSDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASValidarUsuarioAnulaSolicitudyOpsDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASconsultaDatosSolicitudModificarFechaEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsConsultaParamMesFechaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsGuardarGestionAutorizaNoConformidadPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsGuardarInformacionModificacionPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsGuardarInformacionRecobroXPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsGuardarModificacionDireccionamientoSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpAsValidaEstadoPrestReliquidaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpConsultaAfiliadoAlDiaNUIDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpMNConsultaPrestacionPisDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpMNConsultaPrestadorCodigoInternoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestionauditoria.SpAsConsultarCausalesDeNoCobroDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpPMRelacionContingenciaRecobroDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpTraerDiagnosticosDelegate;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EstadoDerechoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConceptosGastoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.ConsultaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DetalleSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.FacturaVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.GestionAutorizacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.GuardarHistoricoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoFormatosVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoModificacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.HistoricoTareaVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.PrestacionPisVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosAdicionalesPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.PrestacionFechaEntregaVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderFacturacion;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class ConsultaSolicitudesController 
 * Clase que permite consultar los delegates de las consultas asociadas con la consulta de la solicitud
 * @author ing. Victor Hugo Gil Ramos
 * @version 10/03/2016
 *
 */

public class ConsultarSolicitudController {	

	private ConnectionProvider connProviderSalud;
	private ConnectionProvider connProviderFacturacion;
	private Date fechaConsulta;

	/**Metodo que permite realizar la conexion a la base de datos */
	public ConsultarSolicitudController() throws ConnectionProviderException{		
		fechaConsulta = Utilidades.getFechaActual();
		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}		
	}

	/**Metodo que consulta el historico de descarga de los formatos */
	public List<HistoricoFormatosVO> consultaHistoricoDescargaFormatos(int consecutivoSolicitud) throws LogicException{
		SpASConsultarHistoricoDescargasDelegate consultarHistoricoDescargasDelegate = new SpASConsultarHistoricoDescargasDelegate(consecutivoSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultarHistoricoDescargasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICO_DESCARGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultarHistoricoDescargasDelegate.getlHistoricoFormatos();
	}

	/**Metodo que guardar en el historico de descargas los formatos 
	 * @throws DataAccessException */
	public void guardarHistoricoDescargaFormato(Integer consecutivoSolicitud, Integer numUnicoAutorizacion, 
			List<PrestacionDTO> lstPrestacionDTO, String consecutivoformato, 
			String nombreDocumento, String usuarioDescarga) throws LogicException{
		
		GuardarHistoricoVO guardarHistoricoVO = new GuardarHistoricoVO();
		guardarHistoricoVO.setConsecutivoSolicitud(consecutivoSolicitud);
		guardarHistoricoVO.setNumeroUnicoAutorizacion(numUnicoAutorizacion);
		guardarHistoricoVO.setUsuarioDescarga(usuarioDescarga);
		guardarHistoricoVO.setCodigoMedioContacto(ConstantesEJB.MEDIO_CONTACTO_ASI);
		guardarHistoricoVO.setFechaDescarga(new Date());

		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlHistoricoDescarga(lstPrestacionDTO, consecutivoformato, nombreDocumento, ConstantesEJB.CADENA_VACIA);
		
		guardarHistoricoVO.setXmlServicios(xml);
		
		SpASGuardarHistoricoDescargasDelegate guardarHistoricoDescargasDelegate = new SpASGuardarHistoricoDescargasDelegate(guardarHistoricoVO);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, guardarHistoricoDescargasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_HISTORICO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**Metodo que consulta el historico de descarga de modificaciones */
	public List<HistoricoModificacionVO> consultaHistoricoModificaciones(int consecutivoSolicitud) throws LogicException{
		SpASConsultarHistoricoModificacionDelegate consultarHistoricoModificacion = new SpASConsultarHistoricoModificacionDelegate(consecutivoSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultarHistoricoModificacion);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICO_MODIFICACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		if (consultarHistoricoModificacion.getlHistoricoModificacion().isEmpty() || consultarHistoricoModificacion.getlHistoricoModificacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICO_MODIFICACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultarHistoricoModificacion.getlHistoricoModificacion();
	}
	
	/**Metodo que consulta el historico de descarga de la solicitud*/
	
	public List<HistoricoFormatosVO> consultaHistoricoDescarga(int consecutivoSolicitud) 
			throws LogicException{
		
		SpASConsultarHistoricoDescargasDelegate consultaHistoricoDelegate = new SpASConsultarHistoricoDescargasDelegate(consecutivoSolicitud);
		
		try 
		{
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaHistoricoDelegate);
		} catch (DataAccessException e) {
			
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_HISTORICO_DESCARGA_SOL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		
		if (consultaHistoricoDelegate.getlHistoricoFormatos().isEmpty() || consultaHistoricoDelegate.getlHistoricoFormatos() == null)
		{
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_HISTORICO_DESCARGA_SOL), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultaHistoricoDelegate.getlHistoricoFormatos();
	}	
	
	/**Metodo que consulta las prestaciones pis por codigo */
	public List<PrestacionPisVO> consultaPrestacionPisxCodigo(String codigoCodificacionPrestacionPis) throws LogicException {

		String descripcionCodificacionPrestacionPis = "";
		SpMNConsultaPrestacionPisDelegate consultaPrestacionPisDelegate = new SpMNConsultaPrestacionPisDelegate(codigoCodificacionPrestacionPis, descripcionCodificacionPrestacionPis);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionPisDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PIS_CODIGO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaPrestacionPisDelegate.getlPrestacionesPis().isEmpty() || consultaPrestacionPisDelegate.getlPrestacionesPis() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PIS_CODIGO), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultaPrestacionPisDelegate.getlPrestacionesPis();
	}

	/**Metodo que consulta las prestaciones pis por descripcion */
	public List<PrestacionPisVO> consultaPrestacionPisxDescripcion(String descripcionCodificacionPrestacionPis) throws  LogicException {

		String codigoCodificacionPrestacionPis = "";
		SpMNConsultaPrestacionPisDelegate consultaPrestacionPisDelegate = new SpMNConsultaPrestacionPisDelegate(codigoCodificacionPrestacionPis, descripcionCodificacionPrestacionPis);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionPisDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PIS_DESCRIPCION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaPrestacionPisDelegate.getlPrestacionesPis().isEmpty() || consultaPrestacionPisDelegate.getlPrestacionesPis() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PIS_DESCRIPCION), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionPisDelegate.getlPrestacionesPis();
	}

	/**Metodo que permite la consulta de documentos soporte por solicitud*/
	public List<DocumentoSoporteVO> consultaDocumentosSoportexSolicitud(Integer numeroSolicitud) throws LogicException{
		SpASConsultarDocumentosAnexosxSolicitudDelegate consultarDocumentosAnexosxSolicitudDelegate = new SpASConsultarDocumentosAnexosxSolicitudDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultarDocumentosAnexosxSolicitudDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTOS_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultarDocumentosAnexosxSolicitudDelegate.getlDocumentosSoporte().isEmpty() || consultarDocumentosAnexosxSolicitudDelegate.getlDocumentosSoporte() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTOS_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultarDocumentosAnexosxSolicitudDelegate.getlDocumentosSoporte();
	}

	/**Metodo que permite la consulta la validacion de derechos afiliado */
	public EstadoDerechoVO validacionEstadoDerecho(int numeroUnicoIdentificacionAfiliado, Date fechaConsulta) throws LogicException{
		SpConsultaAfiliadoAlDiaNUIDelegate consultaAfiliadoAlDiaNUIDelegate = new SpConsultaAfiliadoAlDiaNUIDelegate(numeroUnicoIdentificacionAfiliado, fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaAfiliadoAlDiaNUIDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDACION_ESTADO_DERECHO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaAfiliadoAlDiaNUIDelegate.getlEstadosDerecho().isEmpty() || consultaAfiliadoAlDiaNUIDelegate.getlEstadosDerecho() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDACION_ESTADO_DERECHO), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaAfiliadoAlDiaNUIDelegate.getlEstadosDerecho().get(0);
	}	

	/**Metodo que permite la consulta la informacion de la solicitud */
	public List<DatosSolicitudVO> consultaSolicitud(ConsultaSolicitudVO consultaSolicitudVO) throws LogicException{
		SpASConsultaSolicitudWebDelegate consultaSolicitudDelegate = new SpASConsultaSolicitudWebDelegate(consultaSolicitudVO);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaSolicitudDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaSolicitudDelegate.getlDatosSolicitud().isEmpty() || consultaSolicitudDelegate.getlDatosSolicitud() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}	

		return consultaSolicitudDelegate.getlDatosSolicitud();
	}

	/**Metodo que permite la consulta del detalle de la solicitud  */
	public List<DetalleSolicitudVO> consultaDetalleSolicitud(String NumeroRadicacionSolicitud) throws LogicException{
		SpASConsultaDetalleSolicitudWebDelegate consultaDetalleSolicitudWebDelegate = new SpASConsultaDetalleSolicitudWebDelegate(NumeroRadicacionSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaDetalleSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DETALLE_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaDetalleSolicitudWebDelegate.getlDetalleSolicitud().isEmpty() || consultaDetalleSolicitudWebDelegate.getlDetalleSolicitud() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DETALLE_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}			

		return consultaDetalleSolicitudWebDelegate.getlDetalleSolicitud();
	}

	/**Metodo que permite la consulta de las prestaciones para la solicitud  */	
	public List<PrestacionDTO> consultaInformacionPrestacionesxSolicitud(Integer consecutivoNumeroSolicitud) throws LogicException{
		SpASConsultaPrestacionesSolicitudWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpASConsultaPrestacionesSolicitudWebDelegate(consecutivoNumeroSolicitud);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaPrestacionesSolicitudWebDelegate.getlPrestacionesDTO().isEmpty() || consultaPrestacionesSolicitudWebDelegate.getlPrestacionesDTO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getlPrestacionesDTO();
	}
	
	/**Metodo que permite la consulta de las prestaciones Aprobadas para la solicitud a Modificar Fecha Entrega */	
	public List<PrestacionDTO> consultaPrestacionesAprobadasModFechaEntrega(Integer consecutivoNumeroSolicitud, String numeroRadicado) throws LogicException{
		SpASconsultaDatosSolicitudModificarFechaEntregaDelegate consultaPrestacionesAprobadasWebDelegate = new SpASconsultaDatosSolicitudModificarFechaEntregaDelegate(consecutivoNumeroSolicitud,numeroRadicado);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesAprobadasWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_MOD_FECHA_ENTREGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaPrestacionesAprobadasWebDelegate.getlPrestacionesDTO().isEmpty() || consultaPrestacionesAprobadasWebDelegate.getlPrestacionesDTO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_MOD_FECHA_ENTREGA), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesAprobadasWebDelegate.getlPrestacionesDTO();
	}
	/**Metodo que permite la consulta del parámetro definido en meses permitido mayor a la fecha Actual */	
	public int consultaParametroFechaMayorActual() throws LogicException{
		SpAsConsultaParamMesFechaDelegate consultaParametroFechaDelegate = new SpAsConsultaParamMesFechaDelegate(ConstantesEJB.CODIGO_PARAMETRO_FECHA,ConstantesEJB.USUARIO_VISIBLE,ConstantesEJB.TIPO_PARAMETRO_FECHA);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaParametroFechaDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_MOD_FECHA_ENTREGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaParametroFechaDelegate.getParamMes();
	}	
	
	/**Metodo que permite consultar el numero prestaciones en estado valido para Reliquidar */	
	public int consultaEstadoPrestReliquidar(Integer consecutivoNumeroSolicitud) throws LogicException{
		SpAsValidaEstadoPrestReliquidaDelegate consultaTotalRegistros = new SpAsValidaEstadoPrestReliquidaDelegate(consecutivoNumeroSolicitud);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaTotalRegistros);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_RELIQUIDAR), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		if (consultaTotalRegistros.getTotalRegistros()<=0 || consultaTotalRegistros.getTotalRegistros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_RELIQUIDAR), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultaTotalRegistros.getTotalRegistros();
	}
	
	/**
	 * Metodo para cambiar la fecha de entrega de las prestaciónes de la solicitud
	 * @param lstPrestaciones
	 * @param numSolicitud
	 * @param radicado
	 * @param causal
	 * @param observacion
	 * @param usuario
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public void guardarCambioFechaEntrega(List<PrestacionDTO> lstPrestaciones, Integer numSolicitud,String radicado, Integer causal,String observacion, String usuario) throws LogicException {
		
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlModFechaEntrega(lstPrestaciones, numSolicitud,radicado,causal,observacion, usuario);
		SpASGuardarInfoFechaEntregaDelegate delegate = new SpASGuardarInfoFechaEntregaDelegate(xml);
		
		try {						
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_FECHA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
	}
	
	/**
	 * Metodo para guardar el historico de cambios reliquidación
	 * @param numSolicitud
	 * @param causal
	 * @param observacion
	 * @param usuario
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public void guardarReliquidacion(Integer numSolicitud, Integer causal,String observacion, String usuario) throws LogicException {
		
		SpASGuardarHistoricoReliquidacionDelegate delegate = new SpASGuardarHistoricoReliquidacionDelegate(numSolicitud,causal,observacion,usuario);
		
		try {						
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_RELIQUIDAR), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
	}		

	/**Metodo que permite la consulta de los conceptos de gastos asociados a una solicitud  */	
	public List<ConceptosGastoVO> consultaConceptosGastosxSolicitud(Integer consecutivoNumeroSolicitud) throws LogicException{		

		SpASConsultaConceptosGastoSolicitudWebDelegate consultaConceptosGastoSolicitudWebDelegate = new SpASConsultaConceptosGastoSolicitudWebDelegate(consecutivoNumeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaConceptosGastoSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CONCEPTOS_GASTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaConceptosGastoSolicitudWebDelegate.getlConceptosGastoVO();		
	}

	/**Metodo que permite la consulta del historico de tareas creadas en el BPM para una solicitud  */	
	public List<HistoricoTareaVO> consultaHistoricoTareasxSolicitud(Integer consecutivoNumeroSolicitud) throws LogicException{

		SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate consultaHistoricoTareasxInstancia = new SpASConsultaHistoricoTareasxInstanciaSolicitudWebDelegate(consecutivoNumeroSolicitud);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaHistoricoTareasxInstancia);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICO_TAREAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	

		return consultaHistoricoTareasxInstancia.getlHistoricoTareaVO();
	}

	/** Metodo que consulta la fecha de entrega por solicitud**/
	public List<PrestacionFechaEntregaVO> consultaFechaEntregaxSolicitud(Integer consecutivoNumeroSolicitud)throws LogicException{
		SpASConsultaPrestacionFechaEntregaWebDelegate fechaEntregaWebDelegate = new SpASConsultaPrestacionFechaEntregaWebDelegate(consecutivoNumeroSolicitud);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, fechaEntregaWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_FECHA_ENTREGA_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return fechaEntregaWebDelegate.getlPrestacionFechaEntregaVO();		
	}	

	/**
	 * Metodo para consuñtar los motivos o causas para modificar una solicitud
	 * @param consecutivoCodTipoMotivoCausa
	 * @return
	 * @throws LogicException
	 */
	public List<MotivoCausaVO> consultarMotivosCausas(Integer consecutivoCodTipoMotivoCausa) throws LogicException{
		SpASConsultarMotivosCausasDelegate delegate = new SpASConsultarMotivosCausasDelegate(consecutivoCodTipoMotivoCausa);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVO_CAUSA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

		return delegate.getlstMotivoCausaVO();		
	}

	/**
	 * Metodo para anular la solicitud
	 * @param consecutivoSolicitud
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 * @throws LogicException
	 */
	public void anularSolicitud(DatosSolicitudVO solicitud, List<PrestacionDTO> prestaciones, 
			Integer causalAnulacionSolicitud, String observacion, String usuario, String origenModificacion) throws LogicException {
		ConvertidorXML converter = new ConvertidorXML();
		StringBuilder xml = converter.convertXMLAnulacion(solicitud, prestaciones, causalAnulacionSolicitud, observacion, usuario, origenModificacion);
		try {
			SpASEjecutarAnulacionSolicitudPrestacionDelegate delegate 
						= new SpASEjecutarAnulacionSolicitudPrestacionDelegate(xml); 
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ANULAR_SOLICITUD), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}


	/** Metodo que permite consultar la informacion de la factura asociada la solicitud**/
	public List<FacturaVO> consultarInformacionFacturaxSolicitud(Integer numeroRadicacion, Integer consecutivoOficinaRadicacion) throws LogicException{
		obtenerConexionFacturacion();		
		SpASConsultaInformacionFacturaSolicitudDelegate informacionFacturaSolicitudDelegate = new SpASConsultaInformacionFacturaSolicitudDelegate(numeroRadicacion, consecutivoOficinaRadicacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, informacionFacturaSolicitudDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_FACTURA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

		if (informacionFacturaSolicitudDelegate.getlFacturaVO().isEmpty() || informacionFacturaSolicitudDelegate.getlFacturaVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_FACTURA), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return informacionFacturaSolicitudDelegate.getlFacturaVO();

	}

	/** Metodo que permite establecer la conexion para realizar la consulta de facturacion**/
	private void obtenerConexionFacturacion() throws LogicException {
		if (connProviderFacturacion == null) {
			try {
				connProviderSalud = ConnProviderFacturacion.getConnProvider();
			} catch (ConnectionProviderException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONEXION_BD), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}
		}
	}

	
	/**
	 * Metodo que guarda la informacion del cambio de direccionamiento para OPS
	 * @param consecutivoSolicitud
	 * @param numUnicoAutorizacion
	 * @param codInternoPrestador
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 * @throws LogicException
	 */
	public void guardarModificacionesOPS(Integer consecutivoSolicitud, Integer numUnicoAutorizacion, String codInternoPrestador, Integer consecutivoCodMotivoCausa,
			String observacion, String usuario) throws LogicException {
	   
		SpASGuardarModificacionesOPSDelegate delegate = new SpASGuardarModificacionesOPSDelegate(consecutivoSolicitud, numUnicoAutorizacion, codInternoPrestador, consecutivoCodMotivoCausa, observacion, usuario);
		try {			
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_MOD_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
		try {
			if(!delegate.getRetornoResultado()) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDAR_MOD_OPS), LogicException.ErrorType.ERROR_BASEDATOS);
			}
		} catch (SQLException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_MOD_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
			
	}
	
	
	
	/** Metodo que permite guardar la gestion de autorizacion de prestaciones**/
	public int guardarGestionAutorizacionPrestacionNegada(Integer consecutivoSolicitud, Integer consecutivoPrestacion, Integer consecutivoCodMotivoCausa, String observacion, 
														   String numAutorizacionIPS, String codInternoPrestador, String usuario) throws LogicException {
		SpAsGuardarGestionAutorizaNoConformidadPrestacionDelegate delegate;
		try {
			delegate = new SpAsGuardarGestionAutorizaNoConformidadPrestacionDelegate(consecutivoSolicitud, consecutivoPrestacion,
																														  consecutivoCodMotivoCausa, observacion,
																														  numAutorizacionIPS, codInternoPrestador, usuario) ;
			
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_GESTION_PRESTAC), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getResultado();
	}
	
	public GestionAutorizacionVO obtenerGestionAutorizacionPrestacionNegada(Integer consecutivoSolicitud, Integer consecutivoPrestacion) throws LogicException{
		SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate delegate;
		try{
			
			delegate = new SpAsConsultaDatosGestionAutorizacionPrestacionesDevueltasDelegate(consecutivoSolicitud, consecutivoPrestacion);
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
			
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GESTION_AUTORIZACION), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
		return delegate.getGestionAutorizacionVO();
	}
	
	/**
	 * Metodo para consultar las prestacion de la OPS
	 * @param numeroSolicitud
	 * @param numeroUnicoAutorizacion
	 * @return
	 * @throws LogicException
	 */
	public List<PrestacionDTO> consultarPrestacionesOPS(Integer numeroSolicitud, Integer numeroUnicoAutorizacion) throws LogicException{
		SpASConsultarPrestacionesOPSDelegate delegate = new SpASConsultarPrestacionesOPSDelegate(numeroSolicitud, numeroUnicoAutorizacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_PRESTACION_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getlstPrestacionesDTO();		
	}
	
	

	/**
	 * Metodo para consultar el prestador por codigo interno
	 * @param numeroInternoPrestador
	 * @param tipoPrestador
	 * @return
	 * @throws LogicException
	 */
	public PrestadorVO consultarPrestadorCodigoInterno(String numeroInternoPrestador, String tipoPrestador) throws LogicException{
		SpMNConsultaPrestadorCodigoInternoDelegate delegate = new SpMNConsultaPrestadorCodigoInternoDelegate(numeroInternoPrestador, tipoPrestador);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_PRESTADOR_COD_INTERNO), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getPrestadorVO();		
	}

	/**
	 * Metodo que permite validar si el usuario puede o no anular y mod solicitides y OPS
	 * @param usuario
	 * @return
	 * @throws LogicException
	 */
	public boolean consultarPermisoAnularSolicitudOPS(String usuario) throws LogicException{
		SpASValidarUsuarioAnulaSolicitudyOpsDelegate delegate = new SpASValidarUsuarioAnulaSolicitudyOpsDelegate(usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_PERMISO_ANULAR_SOLICITUD_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getPermisoAnularSolicitudOPS();		
	}
	
	/**
	 * Metodo que permite eliminar acentos, tildes de un String
	 * @param usuario
	 * @return
	 */	
	public String removeDiacriticalMarks(String string) {
	    return Normalizer.normalize(string, Form.NFD)
	        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	}
	
	
	public List<DiagnosticosVO> consultaDiagnosticosxCodigo(String codigoDiagnostico) throws LogicException {

		String descripcionDiagnostico = "";
		SpTraerDiagnosticosDelegate diagnosticosDelegate = new SpTraerDiagnosticosDelegate(fechaConsulta, codigoDiagnostico, descripcionDiagnostico);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, diagnosticosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if (diagnosticosDelegate.getlDiagnosticos().isEmpty() || diagnosticosDelegate.getlDiagnosticos() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return diagnosticosDelegate.getlDiagnosticos();
	}



	public List<ContingenciaRecobroVO> consultaRelContingenciasRecobros(int consecutivoCodigoContingencia) throws LogicException {
		SpPMRelacionContingenciaRecobroDelegate contingenciaRecobroDelegate = new SpPMRelacionContingenciaRecobroDelegate(consecutivoCodigoContingencia);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, contingenciaRecobroDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (contingenciaRecobroDelegate.getlRelContingenciaRecobros().isEmpty() || contingenciaRecobroDelegate.getlRelContingenciaRecobros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return contingenciaRecobroDelegate.getlRelContingenciaRecobros();
	}

	/**
	 * 
	 * @param listaVos
	 * @return
	 */
	public List<SelectItem> obtenerListaSelectRelContingenciasRecobros(List<ContingenciaRecobroVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ContingenciaRecobroVO vo : listaVos) {
			item = new SelectItem(vo.getConsecutivoCodigoRecobro(), vo.getDescripcionRecobro());
			listaItems.add(item);
		}
		return listaItems;
	}
	
	/**
	 * Metodo que guarda la informacion del recobro modificado.
	 * @param numeroSolicitud
	 * @param consecutivoContingenciaRecobro
	 * @param consecutivoCodigoContingencia
	 * @param observaciones
	 * @param usuario
	 * @return
	 * @throws LogicException
	 */
	public int guardarEditarRecobro(Integer numeroSolicitud, Integer consecutivoContingenciaRecobro, 
									 Integer consecutivoCodigoContingencia, String observaciones, String usuario) throws LogicException {
		SpASGuardarInformacionModificacionRecobroDelegate guardarInformacionModificacionRecobroDelegate  
						= new SpASGuardarInformacionModificacionRecobroDelegate(numeroSolicitud, 
																				consecutivoContingenciaRecobro, 
																				consecutivoCodigoContingencia,
																				observaciones, 
																				usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, guardarInformacionModificacionRecobroDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		return guardarInformacionModificacionRecobroDelegate.getResultado();	
	}
	
	/**
	 * Metodo que carga las prestaciones de una solicitud.
	 * @param numeroSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<PrestacionDTO> consultarPrestaciones(Integer numeroSolicitud) throws LogicException{
		SpASConsultarPrestacionesDelegate delegate = new SpASConsultarPrestacionesDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_PRESTACION_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getlstPrestacionesDTO();		
	}
	
	/**
	 * Ejecuta delegate para cambio de direccionamiento de prestaciones sin OPS
	 * @param consecutivoSolicitud
	 * @param consecutivoPrestacion
	 * @param codInternoPrestador
	 * @param consecutivoCodMotivoCausa
	 * @param observacion
	 * @param usuario
	 * @throws LogicException
	 */
	public void guardarCambioDireccionamientoSolicitudSinOPS(	Integer consecutivoSolicitud, Integer consecutivoPrestacion, String codInternoPrestador, Integer consecutivoCodMotivoCausa,
																String observacion, String usuario) throws LogicException {
		try {
			SpAsGuardarModificacionDireccionamientoSolicitudDelegate delegate = 
					new SpAsGuardarModificacionDireccionamientoSolicitudDelegate(consecutivoSolicitud, consecutivoPrestacion, codInternoPrestador, 
																				 consecutivoCodMotivoCausa, observacion, usuario);	
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_DIRECCIONAMIENTO_PRESTACION), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}

	
	/**
	 * 
	 * @param consecutivoPrestacion
	 * @param numSolicitud
	 * @param consecutivoRecobro
	 * @param observacion
	 * @param usuario
	 * @param consecutivoContingencia
	 * @throws LogicException
	 */
	public void guardarModificacionRecobroxPrestacion(Integer consecutivoPrestacion, Integer numSolicitud, 
			Integer consecutivoRecobro, String observacion, String usuario, Integer consecutivoContingencia) throws LogicException {
		SpAsGuardarInformacionRecobroXPrestacionDelegate delegate =
				new SpAsGuardarInformacionRecobroXPrestacionDelegate(
						consecutivoPrestacion, 
						numSolicitud, 
						consecutivoRecobro, 
						observacion, 
						usuario, 
						consecutivoContingencia);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_RECOBRO_X_PRESTACION), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}			
	}
	
	
	/**Metodo que permite realizar la consulta de informacion adicional de las prestaciones asociadas a la solicitud  */	
	public List<DatosAdicionalesPrestacionDTO> consultaInformacionAdicionalPrestacionesxSolicitud(Integer consecutivoNumeroSolicitud) throws LogicException{
		SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate consultaDatosAdicionalesPrestacionesSolicitudWebDelegate  = new SpASConsultaDatosAdicionalesPrestacionesSolicitudWebDelegate(consecutivoNumeroSolicitud);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaDatosAdicionalesPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ADICIONAL_PRESTACIONES_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaDatosAdicionalesPrestacionesSolicitudWebDelegate.getlDatosAdicionesPrestacion().isEmpty() || consultaDatosAdicionalesPrestacionesSolicitudWebDelegate.getlDatosAdicionesPrestacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ADICIONAL_PRESTACIONES_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaDatosAdicionalesPrestacionesSolicitudWebDelegate.getlDatosAdicionesPrestacion();
	}
	
	/**
	 * Metodo para consulta de las causals de no cobro para el coordinador.
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultarCausalesNoCobroCuota() throws LogicException {
		SpAsConsultarCausalesDeNoCobroDelegate delegate = new SpAsConsultarCausalesDeNoCobroDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		}catch(DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_CAUSAS_NO_COBRO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getCausalesNoCobroCuota();
	}
	
	
	/**
	 * Procedimiento que ejecuta llamado a sp de guardado de informacion de la prestacion.
	 * @param consecutivoPrestacion
	 * @param numSolicitud
	 * @param consecutivoRecobro
	 * @param observacion
	 * @param usuario
	 * @param consecutivoContingencia
	 * @param consecutivoCausaNoCobro
	 * @throws LogicException
	 */
	public void guardarModificacionPrestacion(Integer consecutivoPrestacion, Integer numSolicitud, 
											  Integer consecutivoRecobro, String observacion, String usuario, 
											  Integer consecutivoContingencia, Integer consecutivoCausaNoCobro) throws LogicException {
		SpAsGuardarInformacionModificacionPrestacionDelegate delegate = 
				new SpAsGuardarInformacionModificacionPrestacionDelegate(consecutivoRecobro, consecutivoCausaNoCobro, consecutivoPrestacion, numSolicitud, observacion, usuario, consecutivoContingencia);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		}catch(DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_INFORMACION_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
	}
	
	public Integer validarExistenciaReciboCaja(Integer consecutivoSolicitud, Integer consecutivoServicio) throws LogicException {
		SpASConsultarExistenciaReciboCajaDelegate delegate = new SpASConsultarExistenciaReciboCajaDelegate(consecutivoSolicitud, consecutivoServicio);
		Integer resultado = ConstantesEJB.VALOR_CERO;
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
			resultado = delegate.getCantidadRecibos();
		}catch(DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_RECIBOS_CAJA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return resultado;
	}
	
	public String consultarPerfilUsuario(String usuario) throws LogicException{
		SpASConsultarPerfilUsuario delegate = new SpASConsultarPerfilUsuario(usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_PERMISO_ANULAR_SOLICITUD_OPS), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		return delegate.getPerfil();		
	}
	
	
	/**Metodo controlador para consultar la respuesta de la malla y la gestion del auditor */
	public List<InconsistenciasResponseServiceVO> consultarResultadoMallayGestionAuditor(Integer consecutivoNumeroSolicitud) throws LogicException{
		
		SpASConsultarResultadoMallayGestionAuditorDelegate consultarResultadoMallayGestionAuditorDelegate = new SpASConsultarResultadoMallayGestionAuditorDelegate(consecutivoNumeroSolicitud);
		
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultarResultadoMallayGestionAuditorDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultarResultadoMallayGestionAuditorDelegate.getlResultadoMalla().isEmpty() || consultarResultadoMallayGestionAuditorDelegate.getlResultadoMalla() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultarResultadoMallayGestionAuditorDelegate.getlResultadoMalla();		
	}
}
