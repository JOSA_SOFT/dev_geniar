package com.sos.gestionautorizacionessaludejb.controller;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultaCausalesDescuadrePorCajaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCGenerarInformeCierreDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCGuardarDocumentoCajaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCGuardarReciboCajaOrquestadorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerConceptoPagoOPSDelegate;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalesDescuadreValorVO;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerNotasCreditosGrupoFamiliarDelegate;
import com.sos.gestionautorizacionessaluddata.model.caja.ConceptoPagoOpsVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DatosContactoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.DocumentoCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformacionOpsValorVO;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeGeneralReciboVO;
import com.sos.gestionautorizacionessaluddata.model.caja.NotasCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;

/**
 * Clase que se encarga de realizar la gestion de lo recibos de caja.
 * 
 * @author GENIAR <jorge.garcia@geniar.net>
 */
public class ReciboCajaController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient ConnectionProvider connProviderCaja;

	/**
	 * CONSTRUCTOR PRINCIPAL DE LA CLASE, INICIALIZA LA CONEXIÓN.
	 * 
	 * @throws ConnectionProviderException
	 */
	public ReciboCajaController() throws LogicException {
		try {
			if (connProviderCaja == null) {
				connProviderCaja = ConnProviderSiSalud.getConnProvider();
			}
		} catch (Exception e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
	}

	/**
	 * Controlador que consulta los conceptos de pago de una OPS
	 * @param numeroUnicoOps
	 * @return
	 * @throws DataAccessException
	 */
	public List<ConceptoPagoOpsVO> obtenerConceptoPagoOPS(int numeroUnicoOps)
			throws DataAccessException {
		SpRCObtenerConceptoPagoOPSDelegate delegate = new SpRCObtenerConceptoPagoOPSDelegate(numeroUnicoOps);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getListaConceptosGasto();
	}

	/**
	 * Guarda el documento generado
	 * @param documento
	 * @param contacto
	 * @param ops
	 * @return
	 * @throws LogicException
	 */
	public DocumentoCajaVO guardarDocumentoCaja(DocumentoCajaVO documento,
			DatosContactoVO contacto, List<InformacionOpsValorVO> ops, String insertaDetalle) throws LogicException {
		try {
			String listadoOPSXML = this.generarXMLOPS(ops);
			SpRCGuardarDocumentoCajaDelegate delegate = new SpRCGuardarDocumentoCajaDelegate(
					documento, contacto, listadoOPSXML, insertaDetalle);
			SOSDataAccess.ejecutarSQL(connProviderCaja.obtenerConexion(), delegate,
					true);
			return delegate.getDocumento();
		} catch (Exception e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
	}

	/**
	 * Realiza la consulta del sp que genera el informe.
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param consecutivoOficina
	 * @param usuario
	 * @param consecutivoCodigoSede
	 * @return
	 * @throws LogicException
	 */
	public List<InformeGeneralReciboVO> generarInformeCierre(Date fechaInicial,
			Date fechaFinal, Integer consecutivoOficina, String usuario, Integer consecutivoCodigoSede)
			throws LogicException {
		try {
			SpRCGenerarInformeCierreDelegate delegate = new SpRCGenerarInformeCierreDelegate(
					fechaInicial, fechaFinal, consecutivoOficina, usuario, consecutivoCodigoSede);
			SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
			return delegate.getResultado();
		} catch (DataAccessException e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
	}

	/**
	 * Realiza la consulta de las causales de descuadre de una caja
	 * 
	 * @param consecutivoMovimientoCaja
	 * @return
	 * @throws LogicException
	 */
	public List<CausalesDescuadreValorVO> obtenerCausalesDescuadreCaja(
			int consecutivoMovimientoCaja) throws LogicException {
		try {
			SpRCConsultaCausalesDescuadrePorCajaDelegate delegate = new SpRCConsultaCausalesDescuadrePorCajaDelegate(
					consecutivoMovimientoCaja);
			SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
			return delegate.getResultado();
		} catch (Exception e) {
			throw new LogicException(e.getMessage(), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
	}

	/**
	 * Controlador consulta las notas crédito que tiene asociado un afiliado.
	 * 
	 * @param nuiAfiliado
	 *            NUI del afiliado a consultar
	 * @return List<NotasCreditoVO> donde se tienen las notas créditos asociadas
	 *         y la información.
	 * @throws DataAccessException
	 */
	public NotasCreditoVO obtenerNotasCredito(int nuiAfiliado)
			throws DataAccessException {
		SpRCObtenerNotasCreditosGrupoFamiliarDelegate notas = new SpRCObtenerNotasCreditosGrupoFamiliarDelegate(nuiAfiliado);
		SOSDataAccess.ejecutarSQL(connProviderCaja, notas);
		return notas.getNotasCredito();
	}

	/**
	 * funcion que almacena la información de recibo de caja.
	 * @param nuiAfiliado
	 * @param notas
	 * @param recibo
	 * @param usuario
	 * @param opsSeleccionada
	 * @return
	 * @throws LogicException
	 */
	public int guardarReciboCaja(int nuiAfiliado, List<DocumentoCajaVO> notas,
			ReciboCajaVO recibo, String usuario, List<InformacionOpsValorVO> opsSeleccionada) throws LogicException {
		SpRCGuardarReciboCajaOrquestadorDelegate delegate;
		try {
			String listaOPSXML = this.generarXMLOPS(opsSeleccionada);
			String listaNotasXML = this.generarXMLNotasCredito(notas);
			delegate = new SpRCGuardarReciboCajaOrquestadorDelegate(nuiAfiliado, listaNotasXML, listaOPSXML, recibo, usuario);
			SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		}  catch (ParserConfigurationException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerFactoryConfigurationError e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (TransformerException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		} catch (DataAccessException e) {
			throw new LogicException(e.getMessage(), e,
					LogicException.ErrorType.INDEFINIDO);
		}
		return delegate.getNumeroRecibo();
	}
	
	private String generarXMLOPS(List<InformacionOpsValorVO> opsSeleccionada) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException{
		if (opsSeleccionada == null || opsSeleccionada.isEmpty()) {
			return null;
		}

		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = dFact.newDocumentBuilder();
		Document doc = build.newDocument();
		
		Element root = doc.createElement(ConstantesEJB.RAIZ_XML_OPS);
		doc.appendChild(root);
		
		for(InformacionOpsValorVO ops : opsSeleccionada){
			Element details = doc.createElement(ConstantesEJB.NODO_XML_OPS);
			root.appendChild(details);
			
			Element cnsctvoMvmntoCja = doc
					.createElement(ConstantesEJB.ATTR_XML_OPS_DOC);
			cnsctvoMvmntoCja.appendChild(doc.createTextNode(String
					.valueOf(ops.getCodeOPS())));
			details.appendChild(cnsctvoMvmntoCja);

			Element usroUltmaMdfccn = doc
					.createElement(ConstantesEJB.ATTR_XML_OPS_VLR);
			usroUltmaMdfccn.appendChild(doc.createTextNode(String
					.valueOf(ops.getValueOPS())));
			details.appendChild(usroUltmaMdfccn);
		}
		
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, ConstantesEJB.YES);

		DOMSource source = new DOMSource(doc);

		transformer.transform(source,
				new javax.xml.transform.stream.StreamResult(writer));
		return writer.toString();
	}
	
	private String generarXMLNotasCredito(List<DocumentoCajaVO> notas) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException{
		if (notas == null || notas.isEmpty()) {
			return null;
		}

		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = dFact.newDocumentBuilder();
		Document doc = build.newDocument();
		
		Element root = doc.createElement(ConstantesEJB.RAIZ_XML_NOTA);
		doc.appendChild(root);
		
		for(DocumentoCajaVO nota : notas){
			Element details = doc.createElement(ConstantesEJB.NODO_XML_NOTA);
			root.appendChild(details);
			
			Element cnsctvoMvmntoCja = doc
					.createElement(ConstantesEJB.ATTR_XML_NOTA_DOC);
			cnsctvoMvmntoCja.appendChild(doc.createTextNode(String
					.valueOf(nota.getPkDocCaja())));
			details.appendChild(cnsctvoMvmntoCja);

			Element usroUltmaMdfccn = doc
					.createElement(ConstantesEJB.ATTR_XML_NOTA_VLR);
			usroUltmaMdfccn.appendChild(doc.createTextNode(String
					.valueOf(nota.getValorAfiliad())));
			details.appendChild(usroUltmaMdfccn);
		}
		
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, ConstantesEJB.YES);

		DOMSource source = new DOMSource(doc);

		transformer.transform(source,
				new javax.xml.transform.stream.StreamResult(writer));
		return writer.toString();
	}
}
