package com.sos.gestionautorizacionessaludejb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.PrestacionPisVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.ips.modelo.IPSVO;

/**
 * Class PrestacionController
 * 
 * @author Carlos Andres Moreno
 * @version 15/03/2016
 *
 */
public class PrestacionController {

	private static final Logger LOG = Logger.getLogger(PrestacionController.class);
	private RegistrarSolicitudController registrarSolicitudesController;
	private ConsultarSolicitudController consultarSolicitudController;

	public PrestacionController() {
		try {
			consultarSolicitudController = new ConsultarSolicitudController();
			registrarSolicitudesController = new RegistrarSolicitudController();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public List<ProcedimientosVO> consultarPrestacionesCUPSIps(ProcedimientosVO procedimientosVO, IPSVO ipsVO, int consPlan) throws LogicException, ServiceException {
		List<ProcedimientosVO> listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
		List<String> listaCodigoHabilitacion = new ArrayList<String>();
		listaCodigoHabilitacion = registrarSolicitudesController.consultarCodigoHabilitacion(ipsVO.getCodigoInterno());
					
		if (procedimientosVO != null && procedimientosVO.getCodigoCodificacionProcedimiento() != null && !procedimientosVO.getCodigoCodificacionProcedimiento().trim().isEmpty()) {
			listaCUPSEncontrados = registrarSolicitudesController.consultaProcedimientosxCodigoIps(procedimientosVO.getCodigoCodificacionProcedimiento(),ipsVO, Utilidades.valdiarCodigoHabilitaciones(listaCodigoHabilitacion), consPlan);
		} else if (procedimientosVO != null && procedimientosVO.getDescripcionCodificacionProcedimiento() != null && !procedimientosVO.getDescripcionCodificacionProcedimiento().trim().isEmpty()) {
			listaCUPSEncontrados = registrarSolicitudesController.consultaProcedimientosxDescripcionIps(procedimientosVO.getDescripcionCodificacionProcedimiento(),ipsVO, Utilidades.valdiarCodigoHabilitaciones(listaCodigoHabilitacion),consPlan);
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}
		return listaCUPSEncontrados;
	}
	
	public List<ProcedimientosVO> consultarPrestacionesCUPSPorParametros(ProcedimientosVO procedimientosVO) throws LogicException, ServiceException {
		List<ProcedimientosVO> listaCUPSEncontrados = new ArrayList<ProcedimientosVO>();
		if (procedimientosVO != null && procedimientosVO.getCodigoCodificacionProcedimiento() != null && !procedimientosVO.getCodigoCodificacionProcedimiento().trim().isEmpty()) {
			listaCUPSEncontrados = registrarSolicitudesController.consultaProcedimientosxCodigo(procedimientosVO.getCodigoCodificacionProcedimiento());
		} else if (procedimientosVO != null && procedimientosVO.getDescripcionCodificacionProcedimiento() != null && !procedimientosVO.getDescripcionCodificacionProcedimiento().trim().isEmpty()) {
			listaCUPSEncontrados = registrarSolicitudesController.consultaProcedimientosxDescripcion(procedimientosVO.getDescripcionCodificacionProcedimiento());
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}
		return listaCUPSEncontrados;
	}

	public List<MedicamentosVO> consultarPrestacionesMedicamentosIps(MedicamentosVO medicamentoVO, IPSVO ipsVO, int consPlan) throws LogicException, ServiceException {
		List<MedicamentosVO> listaCUMSEncontrados = new ArrayList<MedicamentosVO>();
		List<String> listaCodigoHabilitacion = new ArrayList<String>();
		listaCodigoHabilitacion = registrarSolicitudesController.consultarCodigoHabilitacion(ipsVO.getCodigoInterno());
		if (medicamentoVO != null && medicamentoVO.getCodigoCodificacionMedicamento() != null && !medicamentoVO.getCodigoCodificacionMedicamento().trim().isEmpty()) {
			listaCUMSEncontrados = registrarSolicitudesController.consultaMedicamentosxCodigoIps(medicamentoVO.getCodigoCodificacionMedicamento(),ipsVO, Utilidades.valdiarCodigoHabilitaciones(listaCodigoHabilitacion), consPlan);
		} else if (medicamentoVO != null && medicamentoVO.getDescripcionCodificacionMedicamento() != null && !medicamentoVO.getDescripcionCodificacionMedicamento().trim().isEmpty()) {
			listaCUMSEncontrados = registrarSolicitudesController.consultaMedicamentosxDescripcionIps(medicamentoVO.getDescripcionCodificacionMedicamento(),ipsVO, Utilidades.valdiarCodigoHabilitaciones(listaCodigoHabilitacion),consPlan);
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}
		return listaCUMSEncontrados;
	}
	
	
	public List<MedicamentosVO> consultarPrestacionesMedicamentosPorParametros(MedicamentosVO medicamentoVO) throws LogicException, ServiceException {
		List<MedicamentosVO> listaMedicamentosEncontrados = new ArrayList<MedicamentosVO>();
		if (medicamentoVO != null && medicamentoVO.getCodigoCodificacionMedicamento() != null && !medicamentoVO.getCodigoCodificacionMedicamento().trim().isEmpty()) {
			listaMedicamentosEncontrados = registrarSolicitudesController.consultaMedicamentosxCodigo(medicamentoVO.getCodigoCodificacionMedicamento());
		} else if (medicamentoVO != null && medicamentoVO.getDescripcionCodificacionMedicamento() != null && !medicamentoVO.getDescripcionCodificacionMedicamento().trim().isEmpty()) {
			listaMedicamentosEncontrados = registrarSolicitudesController.consultaMedicamentosxDescripcion(medicamentoVO.getDescripcionCodificacionMedicamento());
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}
		return listaMedicamentosEncontrados;
	}

	public List<PrestacionPisVO> consultarPrestacionesCUOSPorParametros(PrestacionPisVO prestacionPis) throws LogicException, ServiceException {
		List<PrestacionPisVO> listaPisEncontrados = new ArrayList<PrestacionPisVO>();
		if (prestacionPis != null && prestacionPis.getCodigoPrestacionPis() != null && !prestacionPis.getCodigoPrestacionPis().trim().isEmpty()) {
			listaPisEncontrados = consultarSolicitudController.consultaPrestacionPisxCodigo(prestacionPis.getCodigoPrestacionPis());
		} else if (prestacionPis != null && prestacionPis.getDescripcionPrestacionPis() != null && !prestacionPis.getDescripcionPrestacionPis().trim().isEmpty()) {
			listaPisEncontrados = consultarSolicitudController.consultaPrestacionPisxDescripcion(prestacionPis.getDescripcionPrestacionPis());
		} else {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}
		return listaPisEncontrados;
	}
}
