package com.sos.gestionautorizacionessaludejb.controller;

import java.util.List;

import org.apache.log4j.Logger;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpASConsultaProgramacionEntregaParametrosWeb;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpASConsultaProgramacionEntregaWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpAsActualizarEstadoProgramacionEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpMNConsultaClasificacionEventosXProgramacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.programacionentrega.SpMNConsultaEstadoNotificacionDelegate;
import com.sos.gestionautorizacionessaluddata.model.bpm.ProgramacionEntregaVO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ClasificacionEventoXProgramacionVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.EstadoNotificacionVO;
import com.sos.gestionautorizacionessaluddata.model.programacionentrega.ParametrosConsultaProgramacionVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class ConsultaSolicitudesController 
 * Clase que permite consultar los delegates asociado a la programacion entrega
 * @author Julian Hernandez
 * @version 05/07/2016
 *
 */

public class ProgramacionEntregaController {	

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ProgramacionEntregaController.class);
	
	private ConnectionProvider connProviderSalud;

	/**Metodo que permite realizar la conexion a la base de datos */
	public ProgramacionEntregaController() throws ConnectionProviderException{		

		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}		
	}

	/**Metodo que consulta la clasificacion de eventos  */
	public List<ClasificacionEventoXProgramacionVO> consultarClasificacionesEvento() throws LogicException{
		SpMNConsultaClasificacionEventosXProgramacionDelegate delegate = new SpMNConsultaClasificacionEventosXProgramacionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CLASIFICACIONES_EVENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getlstClasificacionEventoXProgramacionVO();
	}
	
	/**Metodo que consulta los estados  */
	public List<EstadoNotificacionVO> consultarEstadosEntrega() throws LogicException{
		SpMNConsultaEstadoNotificacionDelegate delegate = new SpMNConsultaEstadoNotificacionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESTADOS_ENTREGA), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getlstEstadoNotificacionVO();
	}
	
	/**
	 * Metodo que consulta la programacion de entrega
	 * @param nui
	 * @param consecutivoTipoPrestacion
	 * @param consecutivoPrestacion
	 * @return
	 * @throws LogicException
	 */
	public List<ProgramacionEntregaVO> consultarProgramacionEntrega(ParametrosConsultaProgramacionVO parametrosConsulta)throws LogicException{
	
		SpASConsultaProgramacionEntregaParametrosWeb delegate = new SpASConsultaProgramacionEntregaParametrosWeb(parametrosConsulta);
				
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
				
		return delegate.getLstProgramacionEntregaVO();
	}

	/**
	 * Metodo que consulta la programacion de entrega por defecto
	 * @param nui
	 * @return
	 * @throws LogicException
	 */
	public List<ProgramacionEntregaVO> consultarProgramacionEntregaxDefecto(Integer nui) throws LogicException{
		SpASConsultaProgramacionEntregaWebDelegate delegate = new SpASConsultaProgramacionEntregaWebDelegate(nui);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
				
		return delegate.getLstProgramacionEntregaVO();
	}

	/**
	 * Metodo que consulta Informacion Solicitud x Programacion
	 * @param nui
	 * @return
	 * @throws LogicException
	 */
	public List<AutorizacionServicioVO> consultarInformacionSolicitudxProgramacion(Integer consSolicitud, Integer consPrestacion) throws LogicException{
		SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate delegate = new SpASConsultaInformacionSolicitudxProgramacionEntregaWebDelegate(consSolicitud, consPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SOLICITUD_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
		
		return delegate.getlstAutorizacionServicioVO();
	}
	
	/**
	 * Metodo que Ejecutar Creacion Solicitudes Programacion Entrega
	 * @param nui
	 * @return
	 * @throws LogicException
	 */
	public Integer ejecutarCreacionSolicitudProgramacionEntrega(Integer consCodProgramacionFecEvento, String usuario) throws LogicException{
		SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate delegate = new SpAsEjecutarCreacionSolicitudesConProgramacionEntregaDelegate(consCodProgramacionFecEvento, usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) { 
			LOG.error(e.getMessage(),e);
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA_NEGOCIO), LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
				
		return delegate.getSolicitud();
	}
	
	/**
	 * Metodo Sp para actualizar Estado Programacion Entrega
	 * @param consSolicitud
	 * @param consCodProgramacionFecEvento
	 * @param usuario
	 * @throws LogicException
	 */
	public void actualizarEstadoProgramacionEntrega(Integer consSolicitud, Integer consCodProgramacionFecEvento, String usuario, Integer estadoEntregado) throws LogicException{
		SpAsActualizarEstadoProgramacionEntregaDelegate delegate = new SpAsActualizarEstadoProgramacionEntregaDelegate(consSolicitud, consCodProgramacionFecEvento, usuario, estadoEntregado);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);			
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_EJECUTAR_ACTUALIZAR_PROGRAMACION_ENTREGA), e, LogicException.ErrorType.ERROR_BASEDATOS);
		}
	}
	
}
