package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.util.List;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultarPrestadorDireccionamientoCriterioBusquedaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultarPrestadorDireccionamientoDelegate;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

public class PrestadorController {


	private ConnectionProvider connProviderSalud;

	public PrestadorController() throws ConnectionProviderException, IOException{			
		if(connProviderSalud == null){
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/** Metodo que consulta prestadores para el direccionamiento**/
	public List<PrestadorVO> consultarPrestadoresDireccionamiento(Integer conscodigoPrestacion, Integer consecutivoCodPlan, Integer consecutivoCiudad, Integer idSolicitud) throws LogicException{

		SpASConsultarPrestadorDireccionamientoDelegate delegate = new SpASConsultarPrestadorDireccionamientoDelegate(conscodigoPrestacion, consecutivoCodPlan, consecutivoCiudad, idSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_DIRECCIONAMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getLstPrestadoresDireccionamiento();
	}

	/** Metodo que consulta prestadores para el direccionamiento**/
	public List<PrestadorVO> consultarPrestadoresDireccionamientoXCriteriosBusqueda(Integer conscodigoPrestacion, Integer consecutivoCodPlan, Integer consecutivoCiudad, String codigoInterno, String sucursal, String nit, Integer idSolicitud) throws LogicException{

		SpASConsultarPrestadorDireccionamientoCriterioBusquedaDelegate delegate = new SpASConsultarPrestadorDireccionamientoCriterioBusquedaDelegate(conscodigoPrestacion, consecutivoCodPlan, consecutivoCiudad, codigoInterno, sucursal, nit, idSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INFORMACION_DIRECCIONAMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getLstPrestadoresDireccionamiento();
	}
}
