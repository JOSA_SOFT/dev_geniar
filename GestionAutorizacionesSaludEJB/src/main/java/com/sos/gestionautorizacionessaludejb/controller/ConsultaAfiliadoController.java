package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado.SpASConsultaTutelasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado.SpConsultaGrupoPoblacionalDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado.SpMNBuscarRiesgosPacienteDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultaafiliado.SpPmTraerGrupoFamiliarDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpValidaNumeroAutorizacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionHistoricoPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.model.HistoricoPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.DatosAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoFamiliarVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.GrupoPoblacionalVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgosPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TutelasAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaludejb.bean.impl.ConsultaAfiliadoService;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class ConsultaAfiliadoController
 * Clase controladora que invoca los delegates para la consulta del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 24/12/2015
 *
 */
public class ConsultaAfiliadoController {
	
	private Date fechaConsulta;
	private ConnectionProvider connProviderSalud;
	ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado;
	ConsultaAfiliadoService consultaAfiliadoService;
	
	public ConsultaAfiliadoController() throws ConnectionProviderException, IOException{		
		fechaConsulta = Utilidades.getFechaActual();
		consultaAfiliadoService = new ConsultaAfiliadoService();
		
		if(connProviderSalud == null){
		    connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	
	
	/** Metodo que consume el servicio del afiliado */	
	public ResultadoConsultaAfiliadoDatosAdicionales consultaRespuestaServicioAfiliado(String codigoTipoIdentificacion, String numeroIdentificacion, String descripcionPlan, Date fechaConsulta, String usuarioConsulta) throws LogicException{

		if(consultaAfiliadoService == null){
			consultaAfiliadoService = new ConsultaAfiliadoService();
		}		
		consultaAfiliado = consultaAfiliadoService.consultaRespuestaServicioAfiliado(codigoTipoIdentificacion, numeroIdentificacion, descripcionPlan, fechaConsulta, usuarioConsulta);
		
		if(consultaAfiliado == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_VALIDADOR_SERVICE), LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		
		return consultaAfiliado;		
	}
	
	/** Metodo que consulta la informacion del afiliado */	
	public AfiliadoVO consultaInformacionAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado)	throws LogicException {
		
		AfiliadoVO lConsultaAfiliado;
		
		if(this.consultaAfiliado == null){
			this.consultaAfiliado = consultaAfiliado;
		}	
		
		lConsultaAfiliado = consultaAfiliadoService.consultaInformacionAfiliado(this.consultaAfiliado);
		
		return lConsultaAfiliado;
	}
	
	/** Metodo que consulta el servicio del afiliado para los empleados*/
	public List<EmpleadorVO> consultaInformacionEmpleadorAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException {
		
		List<EmpleadorVO> lEmpleadores;
		
		if(this.consultaAfiliado == null){
			this.consultaAfiliado = consultaAfiliado;
		}
		
		lEmpleadores = consultaAfiliadoService.consultaInformacionEmpleadorAfiliado(this.consultaAfiliado);
		
		return lEmpleadores;  
	}
	
	/** Metodo que consulta los datos adicionales del afiliado */
	public List<AfiliadoVO> buscaAfiliadoxNombre(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String usuarioConsulta) throws LogicException{
		
		List<AfiliadoVO> lConsularAfiliadoxNombre;
		
		if(consultaAfiliadoService == null){
			consultaAfiliadoService = new ConsultaAfiliadoService();
		}	
		
		lConsularAfiliadoxNombre = consultaAfiliadoService.buscaAfiliadoxNombre(primerNombre, segundoNombre, primerApellido, segundoApellido, usuarioConsulta);
		
		if(lConsularAfiliadoxNombre.isEmpty() || lConsularAfiliadoxNombre == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_NOMBRE), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return lConsularAfiliadoxNombre;
	}
	
	
	/** Metodo que consulta el servicio del afiliado para los convenio de capitacion*/
	public List<ConveniosCapitacionVO> consultaConveniosCapitacion(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException {
		List<ConveniosCapitacionVO> lConvenioCapitados;
		
		if(this.consultaAfiliado == null){
			this.consultaAfiliado = consultaAfiliado;
		}
		
		lConvenioCapitados = consultaAfiliadoService.consultaConveniosCapitacion(this.consultaAfiliado);
		
		return lConvenioCapitados;
	}
	
	/** Metodo que consulta los datos adicionales del afiliado */	
	public DatosAfiliadoVO consultaDatosAdicionalesAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException{
		DatosAfiliadoVO datosAdicionalesAfiliado;
		
		if(this.consultaAfiliado == null){
			this.consultaAfiliado = consultaAfiliado;
		}
		
		datosAdicionalesAfiliado = consultaAfiliadoService.consultaDatosAdicionalesAfiliado(this.consultaAfiliado);
		
		return datosAdicionalesAfiliado;
	}
		
		
	// Metodo que consulta el grupo familiar del afiliado
	public List<GrupoFamiliarVO> consultaGrupoFamiliarAfiliado(int consecutivoTipoIdentificacion, String numeroIdentificacion) throws LogicException{
		
		SpPmTraerGrupoFamiliarDelegate grupoFamiliarDelegate = new SpPmTraerGrupoFamiliarDelegate(consecutivoTipoIdentificacion, numeroIdentificacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, grupoFamiliarDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPO_FAMILIAR), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return grupoFamiliarDelegate.getlGrupoFamiliar();		
	}
	
	// Metodo que consulta los riesgos que tiene el paciente asociados	
	public List<RiesgosPacienteVO> consultarRiesgosPaciente(int consecutivoTipoIdentificacion, String numeroIdentificacion) throws LogicException{
		
		SpMNBuscarRiesgosPacienteDelegate riesgosPaciente = new SpMNBuscarRiesgosPacienteDelegate(consecutivoTipoIdentificacion, numeroIdentificacion, fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, riesgosPaciente);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RIESGOS_AFILIADOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	
		return riesgosPaciente.getlRiesgosPaciente();
	}
	
	// Metodo que consulta las tutelas que tiene el paciente asociadas
	public List<TutelasAfiliadoVO> consultaTutelasAfiliado(int consecutivoTipoIdentificacion, String numeroIdentificacion) throws LogicException{
		SpASConsultaTutelasDelegate consultaTutelasDelegate = new SpASConsultaTutelasDelegate(consecutivoTipoIdentificacion, numeroIdentificacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaTutelasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TUTELAS_AFILIADO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return consultaTutelasDelegate.getlTutelasAfiliado();		
	}
	
	// Metodo que consulta el grupo poblacional del afiliado
	public List<GrupoPoblacionalVO> consultaGrupoPoblacional(int numeroUnicoIdentificacionAfiliado) throws LogicException{
		SpConsultaGrupoPoblacionalDelegate consultaGrupoPoblacionalDelegate = new SpConsultaGrupoPoblacionalDelegate(numeroUnicoIdentificacionAfiliado);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaGrupoPoblacionalDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GRUPO_POBLACIONAL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		
		return consultaGrupoPoblacionalDelegate.getlGrupoPoblacional();
	}			
	
	/**Metodo que permite realiza validar el  numero de autorizacion del afiliado*/
	public ValidacionEspecialVO validarNumeroAutorizacionAfiliado(int consecutivoTipoIdentificacion, String numeroIdentificacion) throws LogicException{
		ValidacionEspecialVO respuesta = null;
		SpValidaNumeroAutorizacionDelegate numeroAutorizacionDelegate = new SpValidaNumeroAutorizacionDelegate(consecutivoTipoIdentificacion, numeroIdentificacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, numeroAutorizacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDACION_ESPECIAL), e,LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (numeroAutorizacionDelegate.getlValidacionEspecialVO().isEmpty() || numeroAutorizacionDelegate.getlValidacionEspecialVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDACION_ESPECIAL), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		respuesta = numeroAutorizacionDelegate.getlValidacionEspecialVO().get(0);
		
		if (Utilidades.esfechaMayor24HActual(fechaConsulta, respuesta.getFechaValidacion())) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDACION_ESPECIAL_VIGENCIA), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return respuesta;
	}
	
	/** Metodo que consulta del historico de prestaciones **/
	public List<HistoricoPrestacionVO> consultarHistoricosPrestacion(Integer consecutivoTipoDoc, String numDocumento) throws LogicException{

		SpASConsultaDatosInformacionHistoricoPrestacionDelegate delegate = new SpASConsultaDatosInformacionHistoricoPrestacionDelegate(consecutivoTipoDoc, numDocumento);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICOS_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(delegate.getLstHistoricoPrestacionVO().isEmpty() || delegate.getLstHistoricoPrestacionVO() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_HISTORICOS_PRESTACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getLstHistoricoPrestacionVO();
	}
	
}
