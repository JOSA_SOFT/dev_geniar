package com.sos.gestionautorizacionessaludejb.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.AlternativasVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.PrestacionesAprobadasVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.dto.DatosNegacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.GestionAuditorDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;

/**
 * Class PlUtils
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class ConvertidorXML {
	
	
	/** En esta variable guardamos el string con el formato xml */
	private StringBuilder xml;
	
	/**
	 * Genera un archivo xml a partir de un archivo de entrada.
	 * 
	 * @param file
	 * 			Archivo que contiene los datos a cargar
	 * @param fields
	 * 			Nombre de los tags que va a llevar el xml, para cada campo
	 * @return stringbuilder
	 * Un String que contiene el xml con todos los datos del archivo cargado
	 * @throws IOException 
	 */
	public StringBuilder getXmlFromFile(FileReader file, String[] fields) throws IOException{
		xml = new StringBuilder();
		xml.append("<cargue>");
		BufferedReader reader = new BufferedReader(file);
		String register = reader.readLine();
		while(register != null){
			if(register.matches("\\s*")){
				register = reader.readLine();
				continue;
			}
			xml.append("<fila>");
			String[] datos = register.split(";");
			int i=0;
			for(String field:fields){
				xml.append("<"+field+">");
				xml.append(datos[i]);
				xml.append("</"+field+">");
				i++;
			}
			xml.append("</fila>");
			register = reader.readLine();
		}
		xml.append("</cargue>");
		return xml;
	}
	
	/**
	 * Inicia un archivo xml
	 * @param tag
	 */
	public void abrirXml(String tag){
		xml = new StringBuilder();
		xml.append("<"+tag+">");
	}
	
	/**
	 * Inicia un archivo xml
	 * @param tag
	 */
	public void abrirXmlSimple(String tag){
		xml.append("<"+tag+">");
	}
	
	/**
	 * Inseta una nueva fil con los tags indicados en el xml
	 * @param linea
	 * @param tagsLinea
	 * @param tagFila
	 */
	public void insertarFila(String[] datos, String[] tagsLinea, String tagFila){
		xml.append("<"+tagFila+">");
		int i=0;
		for(String tag:tagsLinea){
			xml.append("<"+tag+">");
			xml.append(datos[i].trim());
			xml.append("</"+tag+">");
			i++;
		}
		xml.append("</"+tagFila+">");
	}
	
	/**
	 * Inseta una nueva fil con los tags indicados en el xml
	 * @param linea
	 * @param tagsLinea
	 * @param tagFila
	 */
	public void insertarFilaSimple(String dato, String tagFila){
		xml.append("<"+tagFila+">");
		xml.append(dato.trim());
		xml.append("</"+tagFila+">");
	}
	
	/**
	 * Cierra el archivo xml
	 * @param tag
	 */
	public void cerrarXml(String tag){
		xml.append("</"+tag+">");
	}

	public void setXml(StringBuilder xml) {
		this.xml = xml;
	}

	public StringBuilder getXml() {
		return xml;
	}
	
		
	public StringBuilder convertXml(List<PrestacionDTO> prestacionSeleccionadaVOs) {
		ConvertidorXML convert = new ConvertidorXML();
		
		convert.abrirXml(ConstantesEJB.TIPO_DOCUMENTO);
		
		for (PrestacionDTO prestacionSeleccionada: prestacionSeleccionadaVOs){
			
			convert.insertarFila(
					new String[] {prestacionSeleccionada.getCodigoCodificacionPrestacion(), Integer.toString(prestacionSeleccionada.getConsecutivoItemPresupuesto()), Integer.toString(prestacionSeleccionada.getTipoCodificacionVO().getConsecutivoCodigoTipoCodificacion())},
					new String[] {ConstantesEJB.CODIGO_PRESTACION, ConstantesEJB.ITEM_PRESUPUESTO, ConstantesEJB.TIPO_CODIFICACION}, ConstantesEJB.PRESTACION);
		}
		convert.cerrarXml(ConstantesEJB.TIPO_DOCUMENTO);
		return convert.getXml();
	}
	
	public StringBuilder convertXmlNotificacionAtel(List<PrestacionesAprobadasVO> prestacionesAprobadasVO, String usuario) {
		ConvertidorXML convert = new ConvertidorXML();
		
		convert.abrirXml(ConstantesEJB.MARCA_MEDICINA_TRABAJO);
		for (PrestacionesAprobadasVO prestacionAprobadaVO: prestacionesAprobadasVO){
			convert.insertarFila(new String[] {prestacionAprobadaVO.getConscodigoPrestacionGSA().toString(),prestacionAprobadaVO.getAtel(), usuario },
					new String[] {ConstantesEJB.CODIGO_PRESTACION,ConstantesEJB.MARCA_ATEL,ConstantesEJB.USUARIO_ACTUAL},ConstantesEJB.PRESTACION);

		}
		convert.cerrarXml(ConstantesEJB.MARCA_MEDICINA_TRABAJO);
		return convert.getXml();
	}
	
	public StringBuilder convertXmlDireccionamiento(DireccionamientoVO direccionamientoVO, Integer consecutivoPrestacion, String usuario) {
		ConvertidorXML convert = new ConvertidorXML();
		
		convert.abrirXml(ConstantesEJB.ETIQUETA_GESTION_DOMICILIARIA_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(usuario), ConstantesEJB.ETIQUETA_USUARIO_XML);
		convert.insertarFila(direccionamientoVO.getMotivos().toArray(new String[0]), new String[] {ConstantesEJB.ETIQUETA_GD_CODIGO_MOTIVO_XML}, ConstantesEJB.ETIQUETA_GD_MOTIVOS_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.dateToString(direccionamientoVO.getFechaEsperado()), ConstantesEJB.ETIQUETA_GD_FECHA_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(direccionamientoVO.getConsecutivoPrestador()), ConstantesEJB.ETIQUETA_GD_CODIGO_INTERNO_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(direccionamientoVO.getJustificacion()), ConstantesEJB.ETIQUETA_GD_JUSTIFICACION_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(consecutivoPrestacion), ConstantesEJB.ETIQUETA_GD_CONSECUTIVO_PRESTACION_XML);
		
		convert.cerrarXml(ConstantesEJB.ETIQUETA_GESTION_DOMICILIARIA_XML);
		return convert.getXml();
	}
	
	public StringBuilder convertXmlModFechaEntrega(List<PrestacionDTO> lstPrestaciones, Integer numSolicitud,String radicado, Integer causal,String observacion,String usuario) {
		ConvertidorXML convert = new ConvertidorXML();
		
		convert.abrirXml(ConstantesEJB.ETIQUETA_GESTION_FECHA_ENTREGA);
		convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_PRESTACIONES_XML);
		for (PrestacionDTO vo: lstPrestaciones){
			if(vo.getFechaEntregaModificada()!=null){
				convert.insertarFila(new String[] {vo.getCodigoCodificacionPrestacion(), FuncionesAppEJB.objectToString(vo.getConsecutivoServicioSolicitado()), FuncionesAppEJB.dateToString(vo.getFechaEntregaModificada())},
				new String[] {ConstantesEJB.ETIQUETA_PRESTACION_CODIGO_SOS_XML, ConstantesEJB.ETIQUETA_PRESTACION_CONSECUTIVO_XML, ConstantesEJB.ETIQUETA_GD_FECHA_XML},
				ConstantesEJB.PRESTACION);
			}	
		}
		convert.cerrarXml(ConstantesEJB.ETIQUETA_PRESTACIONES_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(numSolicitud), ConstantesEJB.ETIQUETA_SOLICITUD_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(radicado), ConstantesEJB.ETIQUETA_RADICADO_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(causal), ConstantesEJB.ETIQUETA_CAUSAL_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(usuario), ConstantesEJB.ETIQUETA_USUARIO_FECHA_XML);
		convert.insertarFilaSimple(FuncionesAppEJB.objectToString(observacion), ConstantesEJB.ETIQUETA_GD_JUSTIFICACION_XML);
		
		convert.cerrarXml(ConstantesEJB.ETIQUETA_GESTION_FECHA_ENTREGA);
		return convert.getXml();
	}	
	

	public StringBuilder convertXmlGestionDomiciliaria(AfiliadoVO afiliado,
			SolicitudBpmVO solicitud,
			List<PrestacionesAprobadasVO> lstPrestaciones, Integer numPrestacion,
			GestionAuditorDTO gestionAuditor, DatosNegacionDTO datosNegacion,
			String usuario) {
		
		
		List<SoporteVO> lSoporteVO = solicitud.getlSoporteVO();
		ConvertidorXML convert = new ConvertidorXML();
		
		convert.abrirXml(ConstantesEJB.ETIQUETA_GUARDAR_GESTION_XML);
		
			convert.insertarFilaSimple(FuncionesAppEJB.objectToString(usuario), ConstantesEJB.ETIQUETA_USUARIO_XML);
			
			convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_AFILIADO_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(afiliado.getTipoIdentificacionAfiliado().getDescripcionTipoIdentificacion()), ConstantesEJB.ETIQUETA_AFILIADO_TIPO_DOC_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(afiliado.getNumeroIdentificacion()), ConstantesEJB.ETIQUETA_AFILIADO_NUM_DOC_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(afiliado.isAltoRiesgo()), ConstantesEJB.ETIQUETA_ALTO_RIESGO_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(afiliado.getConsecutivoCodRiesgo()), ConstantesEJB.ETIQUETA_COD_RIESGO_XML);
			convert.cerrarXml(ConstantesEJB.ETIQUETA_AFILIADO_XML);
			
			convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_SOLICITUD_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(solicitud.getNumeroSolicitud()), ConstantesEJB.ETIQUETA_NUM_SOLICITUD_XML);
				convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_DIAGNOSTICOS_XML);
					for (DiagnosticoDTO dto: solicitud.getLstDiagnosticos()){
						convert.insertarFila(new String[] {dto.getPrincipal(), dto.getCodigo(), dto.getAsignadoSOS()},
								new String[] {ConstantesEJB.ETIQUETA_DIAGNOSTICOS_PRINCIPAL_XML,ConstantesEJB.ETIQUETA_DIAGNOSTICOS_COD_DIAGNOSTICO_XML,ConstantesEJB.ETIQUETA_DIAGNOSTICOS_ASIGNADO_SOS_XML},
								ConstantesEJB.ETIQUETA_DIAGNOSTICOS_DIAGNOSTICO_XML);
			
					}
				convert.cerrarXml(ConstantesEJB.ETIQUETA_DIAGNOSTICOS_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(solicitud.getObsSolicitud()), ConstantesEJB.ETIQUETA_OBSERVACION_XML);
			convert.cerrarXml(ConstantesEJB.ETIQUETA_SOLICITUD_XML);
			
			convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_PRESTACIONES_SOLICITADAS_XML);
				for (PrestacionesAprobadasVO vo: lstPrestaciones){
					convert.insertarFila(new String[] {vo.getTipoPrestacion(), vo.getCodigoPrestacion(), vo.getEstadoPrestacion(), vo.getRequiereAuditoria()},
							new String[] {ConstantesEJB.ETIQUETA_PRESTACIONES_PRESTACION_TIPO_XML, ConstantesEJB.CODIGO_PRESTACION, ConstantesEJB.ETIQUETA_PRESTACIONES_PRESTACION_ESTADO_XML, ConstantesEJB.ETIQUETA_REQ_AUDITORIA_XML},
							ConstantesEJB.ETIQUETA_PRESTACIONES_PRESTACION_XML);
		
				}
			convert.cerrarXml(ConstantesEJB.ETIQUETA_PRESTACIONES_SOLICITADAS_XML);
			

			
			convert.insertarFilaSimple(FuncionesAppEJB.objectToString(numPrestacion), ConstantesEJB.ETIQUETA_NUM_PRESTACION);
			
			convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_GESTION_AUDITOR_XML);

				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getCantidadSOS()), ConstantesEJB.ETIQUETA_CANTIDAD_SOS_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getLateralidad()), ConstantesEJB.ETIQUETA_LATERALIDAD_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getViaAcceso()), ConstantesEJB.ETIQUETA_VIA_ACCESO_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getConsRecobro()), ConstantesEJB.ETIQUETA_RECOBRO_XML);
				
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getDosis()), ConstantesEJB.ETIQUETA_DOSIS_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getPosologiaCUMSGeneral()), ConstantesEJB.ETIQUETA_PRESENTACION_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getCadaGeneral()), ConstantesEJB.ETIQUETA_CADA_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getFrecuenciaGeneral()), ConstantesEJB.ETIQUETA_FRECUENCIA_XML);
				
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getConsecutivoEstadoPrestacion()), ConstantesEJB.ETIQUETA_COD_ESTADO_PRESTACION_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getCausalNoCobroCuota()), ConstantesEJB.ETIQUETA_COD_CAUSA_NO_COBRO_CUOTA);
								
				for (String vo: gestionAuditor.getLstCausas()){
					convert.insertarFila(new String[] {vo},
							new String[] {ConstantesEJB.ETIQUETA_CAUSAS_COD_CAUSA_XML},
							ConstantesEJB.ETIQUETA_CAUSAS_XML);
		
				}
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getJustificacion()), ConstantesEJB.ETIQUETA_JUSTIFICACION_XML);
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(gestionAuditor.getObservacion()), ConstantesEJB.ETIQUETA_OBSERVACION_OPS_XML);
			convert.cerrarXml(ConstantesEJB.ETIQUETA_GESTION_AUDITOR_XML);
			
			convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_DATOS_NEGACION_XML);
				for (String vo: datosNegacion.getLstFundamentoLegal()){
					convert.insertarFila(new String[] {vo},
							new String[] {ConstantesEJB.ETIQUETA_FUNDAMENTOS_LEGALES_COD_FUNDAMENTO_XML},
							ConstantesEJB.ETIQUETA_FUNDAMENTOS_LEGALES_XML);
		
				}
				convert.insertarFilaSimple(FuncionesAppEJB.objectToString(datosNegacion.getAuditorMedico().getNumeroIdentificacionMedico()), ConstantesEJB.ETIQUETA_USR_MEDICO_AUDITOR_XML);
				for (AlternativasVO vo: datosNegacion.getLstAlternativas()){
					convert.insertarFila(new String[] {vo.getAlternativa()},
							new String[] {ConstantesEJB.ETIQUETA_ALTERNATIVAS_COD_ALTERNATIVA_XML},
							ConstantesEJB.ETIQUETA_ALTERNATIVAS_XML);
		
				}
			convert.cerrarXml(ConstantesEJB.ETIQUETA_DATOS_NEGACION_XML);
			
			if(lSoporteVO != null ){
				convert.abrirXmlSimple(ConstantesEJB.TAG_XML_INFORMACON_ANEXOS.toLowerCase());
				
					for(SoporteVO vo : lSoporteVO){
						if(vo.getNombreSoporte() != null ){
							convert.insertarFila(new String[] {Long.toString(vo.getConsecutivoCodigoDocumentoSoporte()), Long.toString(vo.getConsecutivoCodigoDocumentoSoporte()), Long.toString(vo.getConsecutivoCodigoDocumentoSoporte()), FuncionesAppEJB.objectToString(usuario)},
												 new String[] {ConstantesEJB.TAG_XML_ID.toLowerCase(), ConstantesEJB.TAG_XML_MODULO_DOCUMENTO_SOPORTE.toLowerCase(), ConstantesEJB.TAG_XML_CONSECUTIVO_CODIGO_DOCUMENTO.toLowerCase(), ConstantesEJB.TAG_XML_USUARIO_CREACION.toLowerCase()},
												 ConstantesEJB.TAG_XML_DOCUMENTOS_ANEXOS.toLowerCase());
						}
					}
				convert.cerrarXml("infrmcn_anxs");
			}
		
		convert.cerrarXml(ConstantesEJB.ETIQUETA_GUARDAR_GESTION_XML);
		
		return convert.getXml();
	}
	
	public StringBuilder convertXmlHistoricoDescarga(List<PrestacionDTO> lstPrestacionDTO, String consecutivoformato, String nombreDocumento, String rutadescarga) {
		ConvertidorXML convert = new ConvertidorXML();

		convert.abrirXml(ConstantesEJB.PRESTACION);
		for (PrestacionDTO vo: lstPrestacionDTO){
			
			convert.insertarFila(new String[] {FuncionesAppEJB.objectToString(vo.getConsecutivoCodificacionPrestacion()), FuncionesAppEJB.objectToString(vo.getCodigoCodificacionPrestacion()), consecutivoformato, nombreDocumento, rutadescarga, ConstantesEJB.CANTIDAD_DESCARGA_HISTORICO},
					new String[] {ConstantesEJB.ETIQUETA_HISTORICO_DESCARGA_CONSECUTIVO_SERVICIO_XML, 
					ConstantesEJB.CODIGO_PRESTACION, 
					ConstantesEJB.ETIQUETA_HISTORICO_DESCARGA_CONSECUTIVO_FORMATO_XML, 
					ConstantesEJB.ETIQUETA_HISTORICO_DESCARGA_NOM_DOCUMENTO_XML,
					ConstantesEJB.ETIQUETA_HISTORICO_DESCARGA_RUTA_DESCARGA_XML, 
					ConstantesEJB.ETIQUETA_HISTORICO_DESCARGA_CANTIDAD_XML}, ConstantesEJB.SERVICIO);			
		}

		convert.cerrarXml(ConstantesEJB.PRESTACION);

		return convert.getXml();
	}
	
	public StringBuilder convertXMLAnulacion(DatosSolicitudVO solicitud, List<PrestacionDTO> prestaciones, 
			Integer causalAnulacionSolicitud, String observacion, String usuario, String origenModificacion) {
		ConvertidorXML convert = new ConvertidorXML();
		convert.abrirXml(ConstantesEJB.ETIQUETA_ANULACION);
		convert.insertarFila(new String[] {FuncionesAppEJB.objectToString(solicitud.getNumeroSolicitud()), FuncionesAppEJB.objectToString(causalAnulacionSolicitud), observacion, usuario, origenModificacion} , 
						     new String[]{ConstantesEJB.ETIQUETA_NUM_SOLICITUD_XML, ConstantesEJB.ETIQUETA_CAUSAL_XML, ConstantesEJB.ETIQUETA_OBSERVACION_ANULACION, ConstantesEJB.USUARIO_ACTUAL, ConstantesEJB.ETIQUETA_ORIGEN_MODIFICACION} , 
						     ConstantesEJB.ETIQUETA_SOLICITUD_XML);
		convert.abrirXmlSimple(ConstantesEJB.ETIQUETA_PRESTACIONES_XML);
		
		for(PrestacionDTO pdto: prestaciones) {
			convert.insertarFila(new String[] {FuncionesAppEJB.objectToString(solicitud.getNumeroSolicitud()), FuncionesAppEJB.objectToString(pdto.getConsecutivoServicioSolicitado())} , 
				     new String[]{ConstantesEJB.ETIQUETA_NUM_SOLICITUD_XML, ConstantesEJB.ETIQUETA_PRESTACION_CONSECUTIVO_XML} , 
				     ConstantesEJB.PRESTACION);
		}
		convert.cerrarXml(ConstantesEJB.ETIQUETA_PRESTACIONES_XML);
		convert.cerrarXml(ConstantesEJB.ETIQUETA_ANULACION);
		return convert.getXml();
		
	}
	
}
