package com.sos.gestionautorizacionessaludejb.util;

import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.dataccess.connectionprovider.ConnectionProviderGeneral;

/**
 * Class ConnProviderFacturacion
 * @author ing. Victor Hugo Gil Ramos
 *
 */
public class ConnProviderFacturacion {
	
	
	
	private ConnProviderFacturacion() {
	}

	public static ConnectionProvider getConnProvider() throws ConnectionProviderException {
		return ConnectionProviderGeneral.getInstance(ConnProviderFacturacion.class
				.getResourceAsStream("/properties/conn_facturacion.properties"));
	}
}
