/*
 * @author Jorge Andres Garcia Erazo - Geniar S.A.S
 * @since 04/09/2013
 * @version 1.0
 */
package com.sos.gestionautorizacionessaludejb.util;

import java.util.ResourceBundle;

// TODO: Auto-generated Javadoc
/**
 * Clase que maneja los mensajes de validacion.
 * 
 * @author Jorge Andres Garcia Erazo - Geniar SAS
 * @since 01/10/2012
 * @version 1.0
 */
public class MessagesEJB {


	/** The Constant EXCEPCIONES. */
	public static final String EXCEPCIONES = "properties.excepcionesejb";


	/** The Constant CONFIG. */
	public static final String CONFIG = "properties.configejb";
	
	/** The Constant DATASOURCES. */
	public static final String DATASOURCE = "properties.conn_bdsec";

	/**
	 * Gets the valor excepcion.
	 * 
	 * @param clave
	 *            the clave
	 * @return the valor excepcion
	 */
	public static String getValorExcepcion(String clave) {
		String mensaje = ResourceBundle.getBundle(EXCEPCIONES).getString(clave);
		return mensaje;
	}
	
	/**
	 * Gets the valor config.
	 * 
	 * @param clave
	 *            the clave
	 * @return the valor config
	 */
	public static String getValorConfig(String clave) {
		String mensaje = ResourceBundle.getBundle(CONFIG).getString(clave);
		return mensaje;
	}
	
	
	/**
	 * Gets the valor datasources
	 * 
	 * @param clave
	 *            the clave
	 * @return the valor config
	 */
	public static String getJNDI(String clave) {
		String mensaje = ResourceBundle.getBundle(DATASOURCE).getString(clave);
		return mensaje;
	}



}
