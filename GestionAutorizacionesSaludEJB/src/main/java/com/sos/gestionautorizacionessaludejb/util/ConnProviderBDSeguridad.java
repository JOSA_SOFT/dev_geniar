package com.sos.gestionautorizacionessaludejb.util;

import java.io.IOException;

import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.dataccess.connectionprovider.ConnectionProviderGeneral;

/**
 * Class ConnProviderBDSeguridad
 * @author Jerson Viveros
 *
 */
public class ConnProviderBDSeguridad {
	public static ConnectionProvider getConnProvider() throws ConnectionProviderException, IOException {
		return ConnectionProviderGeneral.getInstance(ConnProviderBDSeguridad.class
				.getResourceAsStream("/properties/conn_bdsec.properties"));
	}
}
