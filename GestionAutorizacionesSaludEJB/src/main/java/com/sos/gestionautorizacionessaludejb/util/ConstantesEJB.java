package com.sos.gestionautorizacionessaludejb.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Class Constantes Clase donde se relacionan las constantes que maneja la
 * aplicacion
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 14/03/2016
 *
 */
public class ConstantesEJB {

	/**
	 * Atributo que determina si se realiza la validacion
	 */
	public static final String VALIDACION_COMBOS_OPCIONAL = "S";

	/**
	 * Atributo que determina si se realiza la validacion
	 */
	public static final String VALIDACION_COMBOS_OPCIONAL_NO = "N";

	/**
	 * Atributo que determina si el usuario es visible
	 */
	public static final String USUARIO_VISIBLE = "S";
	/**
	 * Atributo que determina el código de Parámetro General
	 */
	public static final String CODIGO_PARAMETRO_FECHA = "109";
	/**
	 * Atributo que determina el tipo de parámetro general a traer
	 */
	public static final String TIPO_PARAMETRO_FECHA = "N";

	/**
	 * Atributo que determina si se realiza la validacion
	 */
	public static final String VALIDACION_COMBOS_OPCIONAL_COMPLETO = "SI";

	/**
	 * Atributo que determina si se realiza la validacion
	 */
	public static final String VALIDACION_COMBOS_OPCIONAL_COMPLETO_NO = "NO";

	/**
	 * Atributo que determina si el medio de contacto es EPS
	 */
	public static final String MEDIO_CONTACTO_ASI = "01";

	/**
	 * Atributo que determina si el medio de contacto es IPS
	 */
	public static final String MEDIO_CONTACTOS_IPS = "02";

	/**
	 * Atributo que determina una cadena vacia
	 */
	public static final String CADENA_VACIA = "";

	/**
	 * Atributo que determina una cadena comodin de busqueda
	 */
	public static final String CADENA_COMODIN = "-";

	/**
	 * Atributo que determina el formato de fecha
	 */
	public static final String FORMATO_FECHA_DIA = "dd/MM/yyyy";

	/**
	 * Atributo que determina el formato de fecha
	 */
	public static final String FORMATO_FECHA_ANO = "yyyy/MM/dd";

	/**
	 * Atributo que determina el mensaje de error en la consulta del documento
	 * soporte
	 */
	public static final String ERROR_CONSULTA_DOCUMENTO_SOPORTE = "ERROR_CONSULTA_DOCUMENTO_SOPORTE";

	/**
	 * Atributo que determina el mensaje de error en la consulta del documento
	 * soporte
	 */
	public static final String ERROR_CONSULTA_DOCUMENTOS_SOLICITUD = "ERROR_CONSULTA_DOCUMENTOS_SOLICITUD";

	/**
	 * Atributo que determina el mensaje de error en la consulta del documento
	 * soporte
	 */
	public static final String ERROR_VISOS_SERVICE = "ERROR_VISOS_SERVICE";

	/**
	 * Atributo que determina el tipo de transaccion de los servicios
	 */
	public static final String TIPO_TRANSACCION_SOLICITUD = "SINCRONO";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String APLICACION_GESTION_SALUD = "APLICACION_GESTION_SALUD";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String CLASIFICACION_GESTION_SERVICE = "CLASIFICACION_GESTION_SERVICE";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String CLASIFICACION_URL_RECURSOS = "CLASIFICACION_URL_RECURSOS";

	/**
	 * Atributo que determina la marca ATEL
	 */
	public static final String MARCA_MEDICINA_TRABAJO = "marcasMedicinaTrabajo";

	/**
	 * Atributo que determina la marca ATEL
	 */
	public static final String CODIGO_PRESTACION = "codigoprestacion";

	/**
	 * Atributo que determina la marca ATEL
	 */
	public static final String MARCA_ATEL = "marcaatel";

	/**
	 * Atributo que determina la uSUARIO
	 */
	public static final String USUARIO_ACTUAL = "usuario";

	/**
	 * Atributo que determina la Prestacion
	 */
	public static final String PRESTACION = "prestacion";

	/**
	 * Atributo que determina la servicio
	 */
	public static final String SERVICIO = "servicio";

	/**
	 * Atributo que determina el TipoDocumento
	 */
	public static final String TIPO_DOCUMENTO = "tiposDocumento";

	/**
	 * Atributo que determina el Item Presupuesto
	 */
	public static final String ITEM_PRESUPUESTO = "itempresupuesto";

	/**
	 * Atributo que determina el Item Presupuesto
	 */
	public static final String TIPO_CODIFICACION = "tipocodificacion";

	/**
	 * Atributo que determina el error en la consulta ATEL
	 */
	public static final String ERROR_CONSULTA_NOTIFICACIONES_ATEL = "ERROR_CONSULTA_NOTIFICACIONES_ATEL";

	/**
	 * Atributo que determina el error en la consulta de diagnosticos
	 */
	public static final String ERROR_CONSULTA_DIAGNOSTICOS = "ERROR_CONSULTA_DIAGNOSTICOS";

	/**
	 * Atributo que determina el error en la consulta de Afiliado
	 */
	public static final String ERROR_CONSULTA_AFILIADO_SOLICITUD = "ERROR_CONSULTA_AFILIADO_SOLICITUD";

	/**
	 * Atributo que determina el error en la consulta de inconsistencias
	 */
	public static final String ERROR_CONSULTA_INCONSISTENCIAS = "ERROR_CONSULTA_INCONSISTENCIAS";

	/**
	 * Atributo que determina el error en la consulta de empleadores
	 */
	public static final String ERROR_CONSULTA_EMPLEADORES = "ERROR_CONSULTA_EMPLEADORES";

	/**
	 * Atributo que determina el error en la consulta de Prestaciones Aprobadas
	 */
	public static final String ERROR_CONSULTA_PRESTACIONES_APROBADAS = "ERROR_CONSULTA_PRESTACIONES_APROBADAS";

	/**
	 * Atributo que determina el error en la consulta de tipos de condificacion
	 */
	public static final String ERROR_CONSULTA_TIPOS_CODIFICACION = "ERROR_CONSULTA_TIPOS_CODIFICACION";

	/**
	 * Atributo que determina el error en la consulta de recobros
	 */
	public static final String ERROR_CONSULTA_RECOBROS = "ERROR_CONSULTA_RECOBROS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_GESTION_SOLICITUD = "PROPIEDAD_GESTION_SOLICITUD";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_URL_RECURSOS = "PROPIEDAD_URL_RECURSOS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_OPS = "PROPIEDAD_RUTA_JASPER_OPS";

	/**
	 * Atributo que determina el sistema origen
	 */
	public static final String SISTEMA_ORIGEN = "GestionAutorizacionesSaludWeb";

	/**
	 * Atributo que determina el diagnostico principal
	 */
	public static final String DIAGNOSTICO_PRINCIPAL = "DIAGNOSTICO_PRINCIPAL";

	/**
	 * Atributo que determina la accion de validacion especial
	 */
	public static final String ACCION_VALIDACION_ESPECIAL = "accionValidacionEspecial";

	/**
	 * Atributo que determina mensaje del consecutivo no encontrado
	 */
	public static final String CONSECUTIVO_SOLICITUD_NO_ENCONTRADO_EN_ANEXO = "ConsecutivoSolicitudNoEncontradoEnAnexo";

	/**
	 * Atributo que determina mensaje del consecutivo no encontrado
	 */
	public static final String CONSECUTIVO_SOLICITUD_NO_ENCONTRADO = "Consecutivo de solicitud no encontrado ";

	/**
	 * Atributo que determina el origen del contrato
	 */
	public static final String ORIGEN_CONSULTA_CONTRATOS = "CONTRATOS";

	/**
	 * Atributo que determina el origen del contrato
	 */
	public static final String ORIGEN_CONSULTA_FORMULARIO = "FORMULARIOS";

	/**
	 * Atributo que determina el origen del contrato
	 */
	public static final String ORIGEN_CONSULTA_FAMISANAR = "FAMISANAR";

	/**
	 * Atributo que determina el origen del contrato
	 */
	public static final String MENSAJE_VISOS = "Mensaje Generado por Visos: ";

	/**
	 * Atributo que determina el mensaje de error en la consulta de prestaciones
	 * PIS
	 */
	public static final String ERROR_CONSULTA_PIS_CODIGO = "ERROR_CONSULTA_PIS_CODIGO";

	/**
	 * Atributo que determina el mensaje de error en la consulta de prestaciones
	 * PIS
	 */
	public static final String ERROR_CONSULTA_PIS_DESCRIPCION = "ERROR_CONSULTA_PIS_DESCRIPCION";

	/**
	 * Atributo que determina el mensaje de error en la de validacion especial
	 */
	public static final String ERROR_VALIDACION_ESPECIAL = "ERROR_VALIDACION_ESPECIAL";

	/**
	 * Atributo que determina el mensaje de error en la consulta de solicitudes
	 */
	public static final String ERROR_CONSULTA_SOLICITUDES = "ERROR_CONSULTA_SOLICITUDES";

	/**
	 * Atributo que determina el mensaje de error en el servicio web
	 */
	public static final String ERROR_CONSULTA_VALIDADOR_SERVICE = "ERROR_CONSULTA_VALIDADOR_SERVICE";

	/**
	 * Atributo que determina el mensaje de error en el servicio web para la
	 * consulta del afiliado x nombre
	 */
	public static final String ERROR_CONSULTA_AFILIADO_NOMBRE = "ERROR_CONSULTA_AFILIADO_NOMBRE";

	/**
	 * Atributo que determina el mensaje para la validacion especial del
	 * afiliado
	 */
	public static final String ERROR_CONSULTA_VALIDACION_ESPECIAL = "ERROR_CONSULTA_VALIDACION_ESPECIAL";

	/**
	 * Atributo que determina el mensaje para la consulta de historicos de
	 * prestacion
	 */
	public static final String ERROR_CONSULTA_HISTORICOS_PRESTACION = "ERROR_CONSULTA_HISTORICOS_PRESTACION";

	/**
	 * Atributo que determina el mensaje para la consulta de planes
	 */
	public static final String ERROR_CONSULTA_PLANES = "ERROR_CONSULTA_PLANES";

	/**
	 * Atributo que determina el mensaje para la consulta de tipos de plan
	 */
	public static final String ERROR_CONSULTA_TIPOS_PLAN = "ERROR_CONSULTA_TIPOS_PLAN";

	/**
	 * Atributo que determina el mensaje para la consulta de tipos de afiliado
	 */
	public static final String ERROR_CONSULTA_TIPOS_AFILIADO = "ERROR_CONSULTA_TIPOS_AFILIADO";

	/**
	 * Atributo que determina el mensaje para la consulta de estados de mega
	 */
	public static final String ERROR_CONSULTA_ESTADOS_MEGA = "ERROR_CONSULTA_ESTADOS_MEGA";

	/**
	 * Atributo que determina el mensaje para la consulta de grupos de entrega
	 */
	public static final String ERROR_CONSULTA_GRUPOS_ENTREGA = "ERROR_CONSULTA_GRUPOS_ENTREGA";

	/**
	 * Atributo que determina el mensaje para la consulta de tipo de
	 * identificacion
	 */
	public static final String ERROR_CONSULTA_TIPOS_IDENTIFICACION = "ERROR_CONSULTA_TIPOS_IDENTIFICACION";

	/**
	 * Atributo que determina el mensaje para la consulta de geneross
	 */
	public static final String ERROR_CONSULTA_GENEROS = "ERROR_CONSULTA_GENEROS";

	/**
	 * Atributo que determina el mensaje para la consulta de geneross
	 */
	public static final String ERROR_CONSULTA_OPCION = "ERROR_CONSULTA_OPCION";

	/**
	 * Atributo que determina el mensaje para la consulta de oficinas
	 */
	public static final String ERROR_CONSULTA_OFICINAS = "ERROR_CONSULTA_OFICINAS";

	/**
	 * Atributo que determina el mensaje para la consulta de la clase de
	 * atencion
	 */
	public static final String ERROR_CONSULTA_CLASE_ATENCION = "ERROR_CONSULTA_CLASE_ATENCION";

	/**
	 * Atributo que determina el mensaje para la consulta de la clase de
	 * atencion
	 */
	public static final String ERROR_CONSULTA_ORIGEN_ATENCION = "ERROR_CONSULTA_ORIGEN_ATENCION";

	/**
	 * Atributo que determina el mensaje para la consulta del tipo de servicio
	 */
	public static final String ERROR_CONSULTA_TIPO_SERVICIO = "ERROR_CONSULTA_TIPO_SERVICIO";

	/**
	 * Atributo que determina el mensaje para la consulta de la prioridad de
	 * atencion
	 */
	public static final String ERROR_CONSULTA_PRIORIDAD_ATENCION = "ERROR_CONSULTA_PRIORIDAD_ATENCION";

	/**
	 * Atributo que determina el mensaje para la consulta de la ubicacion del
	 * paciente
	 */
	public static final String ERROR_CONSULTA_UBICACION_PACIENTE = "ERROR_CONSULTA_UBICACION_PACIENTE";

	/**
	 * Atributo que determina el mensaje para la consulta del tipo diagnostico
	 */
	public static final String ERROR_CONSULTA_TIPO_DIAGNOSTICO = "ERROR_CONSULTA_TIPO_DIAGNOSTICO";

	/**
	 * Atributo que determina el mensaje para la consulta del servicio de
	 * hospitalizacion
	 */
	public static final String ERROR_CONSULTA_SERVICIO_HOSPITALIZACION = "ERROR_CONSULTA_SERVICIO_HOSPITALIZACION";

	/**
	 * Atributo que determina el mensaje para la consulta de la clase de
	 * habitacion
	 */
	public static final String ERROR_CONSULTA_CLASE_HABITACION = "ERROR_CONSULTA_CLASE_HABITACION";

	/**
	 * Atributo que determina el mensaje para la consulta de la dosis del
	 * medicamento
	 */
	public static final String ERROR_CONSULTA_DOSIS_MEDICAMENTO = "ERROR_CONSULTA_DOSIS_MEDICAMENTO";

	/**
	 * Atributo que determina el mensaje para la consulta de la frecuencia del
	 * medicamento
	 */
	public static final String ERROR_CONSULTA_FRECUENCIA_MEDICAMENTO = "ERROR_CONSULTA_FRECUENCIA_MEDICAMENTO";

	/**
	 * Atributo que determina el mensaje para la consulta de la lateralidad del
	 * procedimiento
	 */
	public static final String ERROR_CONSULTA_LATERALIDAD_PROCEDIMIENTO = "ERROR_CONSULTA_LATERALIDAD_PROCEDIMIENTO";

	/**
	 * Atributo que determina el mensaje para la consulta de la via de acceso
	 */
	public static final String ERROR_CONSULTA_VIA_ACCESO = "ERROR_CONSULTA_VIA_ACCESO";

	/**
	 * Atributo que determina el mensaje para la consulta de la concentracion
	 */
	public static final String ERROR_CONSULTA_CONCENTRACION = "ERROR_CONSULTA_CONCENTRACION";

	/**
	 * Atributo que determina el mensaje para la consulta de la presentacion
	 */
	public static final String ERROR_CONSULTA_PRESENTACION = "ERROR_CONSULTA_PRESENTACION";

	/**
	 * Atributo que determina el mensaje para la consulta de parametros
	 * generales
	 */
	public static final String ERROR_CONSULTA_PARAMETROS_GENERALES = "ERROR_CONSULTA_PARAMETROS_GENERALES";

	/**
	 * Atributo que determina el mensaje para la consulta de los riesgos del
	 * afialiado
	 */
	public static final String ERROR_CONSULTA_RIESGOS_AFILIADOS = "ERROR_CONSULTA_RIESGOS_AFILIADOS";

	/**
	 * Atributo que determina el mensaje para la consulta de las tutelas del
	 * afiliado
	 */
	public static final String ERROR_CONSULTA_TUTELAS_AFILIADO = "ERROR_CONSULTA_TUTELAS_AFILIADO";

	/**
	 * Atributo que determina el mensaje para la consulta de los grupos
	 * poblacionales
	 */
	public static final String ERROR_CONSULTA_GRUPO_POBLACIONAL = "ERROR_CONSULTA_GRUPO_POBLACIONAL";

	/**
	 * Atributo que determina el mensaje para la consulta de parametros
	 * generales
	 */
	public static final String ERROR_VALIDACION_ESTADO_DERECHO = "ERROR_VALIDACION_ESTADO_DERECHO";

	/**
	 * Etiquetas para crear XML de los documentos soportes Gestión Auditoria
	 * Atributo que determina el TAG_XML_INFORMACON_ANEXOS
	 */
	public static final String TAG_XML_INFORMACON_ANEXOS = "INFRMCN_ANXS";

	/**
	 * Atributo que determina el TAG_XML_DOCUMENTOS_ANEXOS
	 */
	public static final String TAG_XML_DOCUMENTOS_ANEXOS = "TBCNADOCUMENTOSANEXOSSERVICIOS";

	/**
	 * Atributo que determina el TAG_XML_ID
	 */
	public static final String TAG_XML_ID = "ID";

	/**
	 * Atributo que determina el TAG_XML_MODULO_DOCUMENTO_SOPORTE
	 */
	public static final String TAG_XML_MODULO_DOCUMENTO_SOPORTE = "CNSCTVO_CDGO_MDLO_DCMNTO_SPRTE";

	/**
	 * Atributo que determina el TAG_XML_CONSECUTIVO_CODIGO_DOCUMENTO
	 */
	public static final String TAG_XML_CONSECUTIVO_CODIGO_DOCUMENTO = "CNSCTVO_CDGO_DCMNTO_SPRTE";

	/**
	 * Atributo que determina el TAG_XML_USUARIO_CREACION
	 */
	public static final String TAG_XML_USUARIO_CREACION = "USRO_CRCN";

	/**
	 *  
	 */
	public static final String ETIQUETA_GESTION_DOMICILIARIA_XML = "gestionDomiciliariaDireccionamiento";

	/**
	 *  
	 */
	public static final String ETIQUETA_GESTION_FECHA_ENTREGA = "gestionFechaEntrega";

	/**
	 *  
	 */
	public static final String ETIQUETA_GD_MOTIVOS_XML = "motivos";
	/**
	 *  
	 */
	public static final String ETIQUETA_GD_CODIGO_MOTIVO_XML = "codigoMotivo";

	/**
	 *  
	 */
	public static final String ETIQUETA_GD_CODIGO_INTERNO_XML = "codigoInterno";
	/**
	 *  
	 */
	public static final String ETIQUETA_CAUSAL_XML = "causal";

	/**
	 *  
	 */
	public static final String ETIQUETA_GD_FECHA_XML = "fecha";
	/**
	 *  
	 */
	public static final String ETIQUETA_GD_JUSTIFICACION_XML = "justificacion";
	/**
	 *  
	 */
	public static final String ETIQUETA_GD_CONSECUTIVO_PRESTACION_XML = "consecutivoPrestacion";
	/**
	 *  
	 */
	public static final String ETIQUETA_RADICADO_XML = "radicado";

	/**
	 * Atributo que determina el mensaje para la consulta de la solicitud
	 */
	public static final String ERROR_CONSULTA_SOLICITUD = "ERROR_CONSULTA_SOLICITUD";

	/**
	 * Atributo que determina el mensaje de error del historico de guardar
	 */
	public static final String ERROR_GUARDAR_HISTORICO = "ERROR_GUARDAR_HISTORICO";

	/**
	 * Atributo que determina el mensaje de error del historico al consultar
	 */
	public static final String ERROR_CONSULTA_HISTORICO_DESCARGA = "ERROR_CONSULTA_HISTORICO_DESCARGA";

	/**
	 * Atributo que determina el mensaje de error del historico al consultar
	 */
	public static final String ERROR_CONSULTA_HISTORICO_MODIFICACION = "ERROR_CONSULTA_HISTORICO_MODIFICACION";

	/**
	 * Atributo que determina el mensaje de error al consultar el historico
	 * descargas solicitud
	 */
	public static final String ERROR_HISTORICO_DESCARGA_SOL = "ERROR_HISTORICO_DESCARGA_SOL";

	/**
	 * Atributo que determina el mensaje para la consulta de la ubicacion del
	 * paciente x clase atencion
	 */
	public static final String ERROR_CONSULTA_UBICACION_PACIENTE_CLASE = "ERROR_CONSULTA_UBICACION_PACIENTE_CLASE";

	/**
	 * Atributo que determina el mensaje para la consulta de contingencias
	 */
	public static final String ERROR_CONSULTA_CONTINGENCIAS = "ERROR_CONSULTA_CONTINGENCIAS";

	/**
	 * Atributo que determina el mensaje para la consulta de la relacion de
	 * contingencias con recobros
	 */
	public static final String ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS = "ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS";

	/**
	 * Atributo que determina el mensaje para la consulta de la ips del medico
	 */
	public static final String ERROR_CONSULTA_IPS_MEDICO = "ERROR_CONSULTA_IPS_MEDICO";

	/**
	 * Atributo que determina el mensaje para la consulta de procedimiento x
	 * codigo
	 */
	public static final String ERROR_CONSULTA_PROCEDIMIENTO = "ERROR_CONSULTA_PROCEDIMIENTO";

	/**
	 * Atributo que determina el mensaje para la consulta de procedimiento x
	 * descripcion
	 */
	public static final String ERROR_CONSULTA_PROCEDIMIENTO_DES = "ERROR_CONSULTA_PROCEDIMIENTO_DES";

	/**
	 * Atributo que determina el mensaje para la consulta de medicamento x
	 * codigo
	 */
	public static final String ERROR_CONSULTA_MEDICAMENTO = "ERROR_CONSULTA_MEDICAMENTO";

	/**
	 * Atributo que determina el mensaje para la consulta de medicamento x
	 * descripcion
	 */
	public static final String ERROR_CONSULTA_MEDICAMENTO_DES = "ERROR_CONSULTA_MEDICAMENTO_DES";

	/**
	 * Atributo que determina el mensaje para la consulta de especialidades del
	 * medico
	 */
	public static final String ERROR_CONSULTA_ESPECIALIDADES_MEDICO = "ERROR_CONSULTA_ESPECIALIDADES_MEDICO";

	/**
	 * Atributo que determina el mensaje para la consulta de tipos de ips
	 */
	public static final String ERROR_CONSULTA_TIPOS_IPS = "ERROR_CONSULTA_TIPOS_IPS";

	/**
	 * Atributo que determina el mensaje para la consulta de ciudades x codigo
	 */
	public static final String ERROR_CONSULTA_CIUDADES = "ERROR_CONSULTA_CIUDADES";

	/**
	 * Atributo que determina el mensaje para la consulta de ciudades x
	 * descripcion
	 */
	public static final String ERROR_CONSULTA_CIUDADES_DES = "ERROR_CONSULTA_CIUDADES_DES";

	/**
	 * Atributo que determina el mensaje para la consulta de los tipos de
	 * soporte
	 */
	public static final String ERROR_CONSULTA_TIPOS_SOPORTE = "ERROR_CONSULTA_TIPOS_SOPORTE";

	public static final String ERROR_CONSULTA_MARCA_ACCESO_DIRECTO = "ERROR_CONSULTA_MARCA_ACCESO_DIRECTO";
	public static final String ERROR_DEVOLVER_SERVICIO_SOLICITADO = "ERROR_DEVOLVER_SERVICIO_SOLICITADO";
	public static final String ERROR_CONSULTA_SALARIO_MINIMO = "ERROR_CONSULTA_SALARIO_MINIMO";
	public static final String ERROR_VALIDAR_PERFIL_CORDINADOR = "ERROR_VALIDAR_PERFIL_CORDINADOR";
	public static final String ERROR_VALIDAR_PRESTACIONS_BAJO_COSTO = "ERROR_VALIDAR_PRESTACIONS_BAJO_COSTO";

	/**
	 * Atributo que determina el mensaje para la consulta los medios de contacto
	 */
	public static final String ERROR_MEDIOS_CONTACTO = "ERROR_MEDIOS_CONTACTO";

	/**
	 * Atributo que determina el mensaje para la consulta del servicio de
	 * guardar
	 */
	public static final String ERROR_SOLICITUD_SERVICE = "ERROR_SOLICITUD_SERVICE";

	/**
	 * Atributo que determina el mensaje para la guardar el identificador del
	 * documento
	 */
	public static final String ERROR_GRABAR_IDENTIFICADOR_DOCUMENTO = "ERROR_GRABAR_IDENTIFICADOR_DOCUMENTO";

	/**
	 * Atributo que determina el mensaje para la consulta del servicio de malla
	 */
	public static final String ERROR_MALLA_SERVICE = "ERROR_MALLA_SERVICE";

	/**
	 * Atributo que determina el mensaje para la consulta del servicio de malla
	 */
	public static final String ERROR_CONSULTAR_MALLA_SERVICE = "ERROR_CONSULTAR_MALLA_SERVICE";

	/**
	 * Atributo que determina el mensaje para la consulta del servicio de malla
	 */
	public static final String ERROR_CONSULTAR_RESULTADO_MALLA_SERVICE = "ERROR_CONSULTAR_RESULTADO_MALLA_SERVICE";

	/**
	 * Atributo que determina si se realiza la validacion
	 */
	public static final String NO = "N";

	/**
	 * Atributo que determina el numero telefonico prestador
	 */
	public static final String NUMERO_TELEFONICO_PRESTADOR = "1";

	/**
	 * Atributo que determina error en la invocacion del servicio que crea la
	 * instancia del bpm
	 */
	public static final String ERROR_INSTANCIA_SERVICE_BPM = "ERROR_INSTANCIA_SERVICE_BPM";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_GESTION_MALLA = "PROPIEDAD_GESTION_MALLA";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_INSTANCIA_SERVICE = "PROPIEDAD_INSTANCIA_SERVICE";

	/**
	 * Atributo que determina el error del servicio
	 */
	public static final String ERROR_SERVICE = "ERROR_SERVICE";

	/** **/
	public static final String ETIQUETA_HISTORICO_DESCARGA_CONSECUTIVO_SERVICIO_XML = "consecutivoservicio";
	/** **/
	public static final String ETIQUETA_HISTORICO_DESCARGA_CONSECUTIVO_FORMATO_XML = "consecutivoformato";
	/** **/
	public static final String ETIQUETA_HISTORICO_DESCARGA_NOM_DOCUMENTO_XML = "nombredocumento";
	/** **/
	public static final String ETIQUETA_HISTORICO_DESCARGA_RUTA_DESCARGA_XML = "rutadescarga";
	/** **/
	public static final String ETIQUETA_HISTORICO_DESCARGA_CANTIDAD_XML = "cantidad";

	/**
	 * Atributo que determina el separador del mensaje de error
	 */
	public static final String SEPARADOR_MENSAJE_ERROR = ": ";

	/** **/
	public static final String ETIQUETA_GUARDAR_GESTION_XML = "gestion";
	/** **/
	public static final String ETIQUETA_USUARIO_XML = "usuarioGestion";
	/** **/
	public static final String ETIQUETA_USUARIO_FECHA_XML = "usuarioModificacion";
	/** **/
	public static final String ETIQUETA_AFILIADO_XML = "afiliado";
	/** **/
	public static final String ETIQUETA_AFILIADO_TIPO_DOC_XML = "tipoDocumento";
	/** **/
	public static final String ETIQUETA_AFILIADO_NUM_DOC_XML = "numDocumento";
	/** **/
	public static final String ETIQUETA_ALTO_RIESGO_XML = "altoRiesgo";
	/** **/
	public static final String ETIQUETA_COD_RIESGO_XML = "codRiesgo";
	/** **/
	public static final String ETIQUETA_SOLICITUD_XML = "solicitud";
	/** **/
	public static final String ETIQUETA_NUM_SOLICITUD_XML = "numSolicitud";
	/** **/
	public static final String ETIQUETA_DIAGNOSTICOS_XML = "diagnosticos";
	/** **/
	public static final String ETIQUETA_DIAGNOSTICOS_DIAGNOSTICO_XML = "diagnostico";
	/** **/
	public static final String ETIQUETA_DIAGNOSTICOS_PRINCIPAL_XML = "principal";
	/** **/
	public static final String ETIQUETA_DIAGNOSTICOS_COD_DIAGNOSTICO_XML = "codigoDiagnostico";
	/** **/
	public static final String ETIQUETA_DIAGNOSTICOS_ASIGNADO_SOS_XML = "asignadoSOS";
	/** **/
	public static final String ETIQUETA_OBSERVACION_XML = "observacionSolicitud";
	/** **/
	public static final String ETIQUETA_PRESTACIONES_XML = "prestaciones";
	/** **/
	public static final String ETIQUETA_PRESTACIONES_SOLICITADAS_XML = "prestacionesSolicitadas";
	/** **/
	public static final String ETIQUETA_PRESTACIONES_PRESTACION_XML = "prestacionSolicitada";
	/** **/
	public static final String ETIQUETA_PRESTACIONES_PRESTACION_TIPO_XML = "tipoPrestacion";
	/** **/
	public static final String ETIQUETA_PRESTACION_CODIGO_SOS_XML = "codigoSOS";
	/** **/
	public static final String ETIQUETA_PRESTACION_CONSECUTIVO_XML = "consecutivo";
	/** **/
	public static final String ETIQUETA_PRESTACIONES_PRESTACION_ESTADO_XML = "estadoPrestacion";
	/** **/
	public static final String ETIQUETA_REQ_AUDITORIA_XML = "reqAuditoria";
	/** **/
	public static final String ETIQUETA_NUM_PRESTACION = "numPrestacion";
	/** **/
	public static final String ETIQUETA_GESTION_AUDITOR_XML = "gestionAuditor";
	/** **/
	public static final String ETIQUETA_CANTIDAD_SOS_XML = "cantidadSOS";
	/** **/
	public static final String ETIQUETA_LATERALIDAD_XML = "lateralidad";
	/** **/
	public static final String ETIQUETA_VIA_ACCESO_XML = "viaAcceso";
	/** **/
	public static final String ETIQUETA_RECOBRO_XML = "recobro";
	/** **/
	public static final String ETIQUETA_COD_ESTADO_PRESTACION_XML = "codEstadoPrestacion";
	/** **/
	public static final String ETIQUETA_CAUSAS_XML = "causas";
	/** **/
	public static final String ETIQUETA_CAUSAS_COD_CAUSA_XML = "codCausa";
	/** **/
	public static final String ETIQUETA_JUSTIFICACION_XML = "justificacion";
	/** **/
	public static final String ETIQUETA_OBSERVACION_OPS_XML = "observacionOPS";
	/** **/
	public static final String ETIQUETA_DATOS_NEGACION_XML = "datosNegacion";
	/** **/
	public static final String ETIQUETA_FUNDAMENTOS_LEGALES_XML = "fundamentosLegales";
	/** **/
	public static final String ETIQUETA_FUNDAMENTOS_LEGALES_COD_FUNDAMENTO_XML = "codFundamento";
	/** **/
	public static final String ETIQUETA_USR_MEDICO_AUDITOR_XML = "usrMedicoAuditor";
	/** **/
	public static final String ETIQUETA_ALTERNATIVAS_XML = "alternativas";
	/** **/
	public static final String ETIQUETA_ALTERNATIVAS_COD_ALTERNATIVA_XML = "codAlternativa";

	/**
	 * Atributo que determina el mensaje para la consulta el detalle de la
	 * solicitud
	 */
	public static final String ERROR_CONSULTA_DETALLE_SOLICITUD = "ERROR_CONSULTA_DETALLE_SOLICITUD";

	/**
	 * Atributo que determina el mensaje para la consulta de las prestaciones
	 * asociadas a la solicitud
	 */
	public static final String ERROR_CONSULTA_PRESTACIONES_SOLICITUD = "ERROR_CONSULTA_PRESTACIONES_SOLICITUD";

	/**
	 * Atributo que determina el mensaje para la consulta de las prestaciones
	 * aprobadas de la solicitud a Modificar Fecha Entrega
	 */
	public static final String ERROR_CONSULTA_PRESTACIONES_MOD_FECHA_ENTREGA = "ERROR_CONSULTA_PRESTACIONES_MOD_FECHA_ENTREGA";

	/**
	 * Atributo que determina el mensaje para la consulta de las prestaciones de
	 * la solicitud en estado valido para reliquidar
	 */
	public static final String ERROR_CONSULTA_PRESTACIONES_RELIQUIDAR = "ERROR_CONSULTA_PRESTACIONES_RELIQUIDAR";

	/**
	 * Atributo que determina la consulta de los documentos soporte
	 */
	public static final int MODELO_DOCUMENTO_SOPORTE = 4;

	/**
	 * Atributo que determina el codigo de la EPS
	 */
	public static final String CODIGO_EPS = "CodigoEPS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_DIRECCIONAMIENTO_SERVICE = "PROPIEDAD_DIRECCIONAMIENTO_SERVICE";

	/**
	 * Atributo que determina error en la invocacion del servicio de
	 * direccionamiento
	 */
	public static final String ERROR_DIRECCIONAMIENTO_SERVICE = "ERROR_DIRECCIONAMIENTO_SERVICE";

	/**
	 * Atributo que determina el estado del servicio
	 */
	public static final String OK_SERVICE = "OK";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_FECHA_ENTREGA_SERVICE = "PROPIEDAD_FECHA_ENTREGA_SERVICE";

	/**
	 * Atributo que determina error en la invocacion del servicio de fecha de
	 * entrega
	 */
	public static final String ERROR_FECHA_ENTREGA_SERVICE = "ERROR_FECHA_ENTREGA_SERVICE";

	/**
	 * Atributo que determina el mensaje para la consulta del historico de
	 * tareas asociadas a la solicitud
	 */
	public static final String ERROR_CONSULTA_HISTORICO_TAREAS = "ERROR_CONSULTA_HISTORICO_TAREAS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_LIQUIDACION_SERVICE = "PROPIEDAD_LIQUIDACION_SERVICE";

	/**
	 * Atributo que determina error en la invocacion del servicio de liquidacion
	 */
	public static final String ERROR_LIQUIDACION_SERVICE = "ERROR_LIQUIDACION_SERVICE";

	/**
	 * Atributo que determina error en la invocacion del servicio de liquidacion
	 */
	public static final String ERROR_LIQUIDACION_PRESTACION = "ERROR_LIQUIDACION_PRESTACION";
	/**
	 * Atributo que determina el mensaje para la consulta de los conceptos
	 */
	public static final String ERROR_CONSULTA_CONCEPTOS_GASTO = "ERROR_CONSULTA_CONCEPTOS_GASTO";

	/**
	 * Atributo que determina el mensaje para la consulta de la fecha entrega
	 * por prestacion
	 */
	public static final String ERROR_CONSULTA_FECHA_ENTREGA_PRESTACION = "ERROR_CONSULTA_FECHA_ENTREGA_PRESTACION";

	/**
	 * Atributo que determina el error en la consulta de informacion conceptos
	 * de gasto
	 */
	public static final String ERROR_CONSULTA_INFORMACION_CONCEPTOS_GASTO = "ERROR_CONSULTA_INFORMACION_CONCEPTOS_GASTO";

	/**
	 * Atributo que determina el error en la consulta de informacion
	 * direccionamiento
	 */
	public static final String ERROR_CONSULTA_INFORMACION_DIRECCIONAMIENTO = "ERROR_CONSULTA_INFORMACION_DIRECCIONAMIENTO";

	/**
	 * Atributo que determina el error en la consulta de informacion de los
	 * estados
	 */
	public static final String ERROR_CONSULTA_ESTADOS = "ERROR_CONSULTA_ESTADOS";

	/**
	 * Atributo que determina el error en la consulta de informacion de las
	 * causas
	 */
	public static final String ERROR_CONSULTA_CAUSAS = "ERROR_CONSULTA_CAUSAS";

	/**
	 * Atributo que determina el error en la consulta de los fundamentos legales
	 */
	public static final String ERROR_CONSULTA_FUNDAMENTOS_LEGALES = "ERROR_CONSULTA_FUNDAMENTOS_LEGALES";

	/**
	 * Atributo que determina el error en la consulta de las alternativas
	 */
	public static final String ERROR_CONSULTA_ALTERNATIVAS = "ERROR_CONSULTA_ALTERNATIVAS";

	/**
	 * Atributo que determina el error en la consulta de los auditores medicos
	 */
	public static final String ERROR_CONSULTA_AUDITORES_MEDICOS = "ERROR_CONSULTA_AUDITORES_MEDICOS";

	/**
	 * Atributo que determina el error en la consulta de la programacion de
	 * entrega
	 */
	public static final String ERROR_CONSULTA_PROGRAMACION_ENTREGA = "ERROR_CONSULTA_PROGRAMACION_ENTREGA";

	/**
	 * Atributo que determina el error en la consulta del grupo auditor
	 */
	public static final String ERROR_CONSULTA_GRUPO_AUDITOR = "ERROR_CONSULTA_GRUPO_AUDITOR";

	/**
	 * Atributo que determina el error en la consulta del grupo auditor
	 */
	public static final String ERROR_CONSULTA_MOTIVO_CAUSA = "ERROR_CONSULTA_MOTIVO_CAUSA";
	/**
	 * Atributo que determina el error en la consulta del parametro meses mayor
	 * a fecha Actual
	 */
	public static final String ERROR_CONSULTA_PARAMETRO_FECHA = "ERROR_CONSULTA_PARAMETRO_FECHA";

	/**
	 * Atributo que determina el error en la consulta del grupo auditor
	 */
	public static final String ERROR_ANULAR_SOLICITUD = "ERROR_ANULAR_SOLICITUD";

	/**
	 * Atributo que determina el error en la consulta de prestacion por ops
	 */
	public static final String ERROR_CONSULTAR_PRESTACION_OPS = "ERROR_CONSULTAR_PRESTACION_OPS";

	/**
	 * Atributo que determina el error en la consulta al prestador por codigo
	 * interno
	 */
	public static final String ERROR_CONSULTAR_PRESTADOR_COD_INTERNO = "ERROR_CONSULTAR_PRESTADOR_COD_INTERNO";

	/**
	 * Atributo que determina el error en la consulta al prestador por codigo
	 * interno
	 */
	public static final String ERROR_CONSULTAR_PERMISO_ANULAR_SOLICITUD_OPS = "ERROR_CONSULTAR_PERMISO_ANULAR_SOLICITUD_OPS";

	/**
	 * Atributo que determina el error al guardar
	 */
	public static final String ERROR_GUARDAR_MOD_OPS = "ERROR_GUARDAR_MOD_OPS";

	/**
	 * Atributo que determina el error al consultar la gestión de autorizacion
	 */
	public static final String ERROR_CONSULTA_GESTION_AUTORIZACION = "ERROR_CONSULTA_GESTION_AUTORIZACION";

	/**
	 * Atributo que determina el error al guardar la gestión de autorizacion
	 */
	public static final String ERROR_GUARDAR_GESTION_PRESTAC = "ERROR_GUARDAR_GESTION_PRESTAC";

	/**
	 * Atributo que determina el error en la consulta del grupo auditor
	 */
	public static final String ERROR_ANULAR_OPS = "ERROR_ANULAR_OPS";

	/**
	 * Atributo que determina el error en la consulta del grupo auditor
	 */
	public static final String ERROR_ASOCIAR_PROGRAMACION_ENTREGA = "ERROR_ASOCIAR_PROGRAMACION_ENTREGA";

	/**
	 * Atributo que determina el error en la consulta de motivos de prestacion
	 */
	public static final String ERROR_CONSULTA_MOTIVOS_PRESTACION = "ERROR_CONSULTA_MOTIVOS_PRESTACION";

	/**
	 * Atributo que determina el error en la consulta de los antecedentes de la
	 * prestacion
	 */
	public static final String ERROR_CONSULTA_ANTECEDENTES_PRESTACION = "ERROR_CONSULTA_ANTECEDENTES_PRESTACION";

	/**
	 * Atributo que determina el error en la consulta de la gestion de la
	 * prestacion
	 */
	public static final String ERROR_CONSULTA_GESTION_PRESTACION = "ERROR_CONSULTA_GESTION_PRESTACION";

	/**
	 * Atributo que determina el error en el guardar la gestion domiciliaria
	 */
	public static final String ERROR_GUARDAR_GESTION_DOMICILIARIA = "ERROR_GUARDAR_GESTION_DOMICILIARIA";

	/**
	 * Atributo que determina el error en el guardar la gestion auditoria
	 * integral
	 */
	public static final String ERROR_GUARDAR_GESTION_AUDITORIA_INTEGRAL = "ERROR_GUARDAR_GESTION_AUDITORIA_INTEGRAL";

	/**
	 * Atributo que determina el error en el guardar el cambio en el
	 * direccionamiento
	 */
	public static final String ERROR_GUARDAR_CAMBIO_DIRECCIONAMIENTO = "ERROR_GUARDAR_CAMBIO_DIRECCIONAMIENTO";
	/**
	 * Atributo que determina el error en el guardar el cambio en la fecha de
	 * Entrega
	 */
	public static final String ERROR_GUARDAR_FECHA = "ERROR_GUARDAR_FECHA";
	/**
	 * Atributo que determina el error en el guardar el cambio al reliquidar
	 */
	public static final String ERROR_GUARDAR_RELIQUIDAR = "ERROR_GUARDAR_RELIQUIDAR";

	/**
	 * Atributo que determina el error en el guardar el direccionamiento
	 */
	public static final String ERROR_GUARDAR_DIRECCIONAMIENTO = "ERROR_GUARDAR_DIRECCIONAMIENTO";

	/**
	 * Atributo que determina el error en el guardar prestacion
	 */
	public static final String ERROR_GUARDAR_PRESTACION = "ERROR_GUARDAR_PRESTACION";

	/**
	 * Atributo que determina el error en el guardar ateL
	 */
	public static final String ERROR_GUARDAR_ATEL = "ERROR_GUARDAR_ATEL";

	/**
	 * Atributo que determina el error en el INITIAL_CONTEXT_FACTORY
	 */
	public static final String INITIAL_CONTEXT_FACTORY = "com.ibm.ws.naming.util.WsnInitCtxFactory";

	/**
	 * Atributo que determina el error en el parse error
	 */
	public static final String ERROR_PARSE = "ERROR_PARSE";

	/**
	 * Atributo que determina el error en el parse error
	 */
	public static final String ERROR_CONSULTA_AFILIADO_SERVICE = "ERROR_CONSULTA_AFILIADO_SERVICE";

	/**
	 * Atributo que determina el error en la consulta del grupo familiar
	 */
	public static final String ERROR_CONSULTA_GRUPO_FAMILIAR = "ERROR_CONSULTA_GRUPO_FAMILIAR";

	/**
	 * Atributo que determina el Tipo de estado para las causas
	 */
	public static final String ERROR_PROPERTY_QUERY = "ERROR_PROPERTY_QUERY";

	/**
	 * Atributo que determina el error al grbar un documento
	 */
	public static final String ERROR_GRABAR_DOCUMENTO_SOPORTE = "ERROR_GRABAR_DOCUMENTO_SOPORTE";

	/**
	 * Atributo que determina el error al no anadir un campo para la consulta
	 */
	public static final String EXCEPCION_DATOS_OBLIGATORIOS = "EXCEPCION_DATOS_OBLIGATORIOS";

	/**
	 * Atributo que determina el mensaje de error conexion a la base de datos
	 */
	public static final String ERROR_CONEXION_BD = "ERROR_CONEXION_BD";

	/**
	 * Atributo que determina el mensaje de error de la consulta de ops
	 */
	public static final String ERROR_CONSULTA_NUMERO_AUTORIZACION = "ERROR_CONSULTA_NUMERO_AUTORIZACION";

	/**
	 * Atributo que determina el mensaje de error al generar el reporte de OPS
	 */
	public static final String ERROR_GENERAR_REPORTE_OPS = "ERROR_GENERAR_REPORTE_OPS";

	/**
	 * Atributo que determina el mensaje de error al actualizar el estado de la
	 * ops
	 */
	public static final String ERROR_ACTUALIZAR_OPS = "ERROR_ACTUALIZAR_OPS";

	/**
	 * Atributo que determina el mensaje de error al registrar validacion
	 * especial
	 */
	public static final String ERROR_REGISTRAR_VALIDACION_ESPECIAL = "ERROR_REGISTRAR_VALIDACION_ESPECIAL";

	/**
	 * Atributo que determina el mensaje de error al ejecutar el proceso que
	 * genera los numero unicos de OPS
	 */
	public static final String ERROR_EJECUTAR_NUMERO_UNICO_OPS = "ERROR_EJECUTAR_NUMERO_UNICO_OPS";

	/**
	 * Atributo que determina el mensaje de error al consultar la factura
	 * asociada a la solicitud
	 */
	public static final String ERROR_CONSULTA_INFORMACION_FACTURA = "ERROR_CONSULTA_INFORMACION_FACTURA";

	/**
	 * Atributo que determina la extension del archivo
	 */
	public static final String ARCHIVO_ZIP = ".zip";

	/**
	 * Atributo que determina la extension del archivo
	 */
	public static final String ARCHIVO_PDF = ".pdf";

	/**
	 * Atributo que determina el mensaje de error al consultar el detalle de las
	 * solicitudes
	 */
	public static final String ERROR_CONSULTA_DETALLE_SOLICITUDES = "ERROR_CONSULTA_DETALLE_SOLICITUDES";

	/**
	 * Atributo que determina el parametro de auditoria
	 */
	public static final String EN_AUDITORIA_CONSULTA = "EN_AUDITORIA_CONSULTA";

	/**
	 * Atributo que determina el parametro de aprobacion
	 */
	public static final String APROBADA_CONSULTA = "APROBADA_CONSULTA";

	/**
	 * Atributo que determina el parametro para exporta a ZIP
	 */
	public static final String ZIP = "application/zip";

	/**
	 * Atributo que determina el parametro para exporta a PDF
	 */
	public static final String PDF = "application/pdf";

	/**
	 * Atributo que determina el parametro para exporta a PDF
	 */
	public static final String CONTENT = "Content-Disposition";

	/**
	 * Atributo que determina el parametro para exporta a PDF
	 */
	public static final String ATTACHMENT = "attachment; filename=";

	/**
	 * Atributo que determina el parametro para cache
	 */
	public static final String CACHE = "Cache-Control";

	/**
	 * Atributo que determina el parametro para revalidate
	 */
	public static final String REVALIDATE = "must-revalidate, post-check=0, pre-check=0";

	/**
	 * Atributo que determina el parametro para revalidate
	 */
	public static final String SEPARADOR_ATTACHMENT = "\"";

	/**
	 * Atributo que determina el parametro para generar el archivo
	 */
	public static final String GENERATED_FILES = "generatedFiles";

	/**
	 * Atributo que determina el mensaje de error para la adicion de uan
	 * prestacion
	 */
	public static final String ERROR_ADICIONAR_PRRESTACION_AUDITORIA_INTEGRAL = "ERROR_ADICIONAR_PRRESTACION_AUDITORIA_INTEGRAL";

	/**
	 * Atributo que determina el mensaje de error para la reasignación de uan
	 * prestacion
	 */
	public static final String ERROR_REASIGNAR_PRESTACION_AUDITORIA_INTEGRAL = "ERROR_REASIGNAR_PRESTACION_AUDITORIA_INTEGRAL";

	/**
	 * Atributo que determina una cadena comodin de nombre
	 */
	public static final String CADENA_COMODIN_NOMBRE = "_";

	/**
	 * Atributo que determina el consecutvo del medicamento
	 */
	public static final int CONSECUTIVO_CUMS = 5;

	/**
	 * Atributo que determina el parametro para el Serivicio Ips
	 */
	public static final String IPS_SERVICE = "IPSService";

	/**
	 * Atributo que determina el parametro para el Serivicio Medico
	 */
	public static final String MEDICO_SERVICE = "MedicoService";

	/**
	 * Atributo que determina el consecutvo del procedimiento
	 */
	public static final int VALOR_INICIAL = -1;

	/**
	 * Atributo que determina el parametro de la ruta de propiedades de Sisalud
	 */
	public static final String PROPIEDADES_SI_SALUD = "/properties/conn_sisalud.properties";

	/**
	 * Atributo que determina el parametro de la ruta de propiedades de
	 * afiliacion
	 */
	public static final String PROPIEDADES_AFILIACION = "/properties/conn_afiliacion.properties";

	/**
	 * Atributo que determina el parametro de ERROR_VALIDACION_ESPECIAL_VIGENCIA
	 */
	public static final String ERROR_VALIDACION_ESPECIAL_VIGENCIA = "ERROR_VALIDACION_ESPECIAL_VIGENCIA";
	/**
	 * Atributo que determina el mensaje de consecutivo solicitud
	 */
	public static final String MENSAJE_CONSECUTIVO_SOLICITUD = "Consecutivo solicitud: ";

	/**
	 * Atributo que determina COD_FORMATO_DEVOLUCION
	 */
	public static final String COD_FORMATO_DEVOLUCION = "3";
	/**
	 * Atributo que determina COD_FORMATO_DEVOLUCION
	 */
	public static final String COD_FORMATO_OPS = "1";
	/**
	 * Atributo que determina COD_FORMATO_DEVOLUCION
	 */
	public static final String COD_FORMATO_NEGACION = "2";
	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_LOGO_SUPER_SALUD = "PROPIEDAD_RUTA_LOGO_SUPER_SALUD";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_DEVOLUCION = "PROPIEDAD_RUTA_JASPER_DEVOLUCION";

	/**
	 * Atributo que determina la CANTIDAD_DESCARGA_HISTORICO
	 */
	public static final String CANTIDAD_DESCARGA_HISTORICO = "1";

	/**
	 * Atributo que determina el tag xml de la dosis
	 */
	public static final String ETIQUETA_DOSIS_XML = "dosis";

	/**
	 * Atributo que determina el tag xml de la dosis
	 */
	public static final String ETIQUETA_PRESENTACION_XML = "presentacion";

	/**
	 * Atributo que determina el tag xml de la dosis
	 */
	public static final String ETIQUETA_CADA_XML = "cada";

	/**
	 * Atributo que determina el tag xml de la dosis
	 */
	public static final String ETIQUETA_FRECUENCIA_XML = "frecuencia";
	/**
	 * Atributo que determina el
	 * ERROR_CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION
	 */
	public static final String ERROR_CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION = "ERROR_CONSULTA_CLASIFICACION_EVENTO_X_PROGRAMACION";
	/**
	 * Atributo que determina el ERROR_CONSULTA_ESTADOS_NOTIFICACION
	 */
	public static final String ERROR_CONSULTA_ESTADOS_NOTIFICACION = "ERROR_CONSULTA_ESTADOS_NOTIFICACION";
	/**
	 * Atributo que determina el ERROR_CONSULTA_CLASIFICACIONES_EVENTO
	 */
	public static final String ERROR_CONSULTA_CLASIFICACIONES_EVENTO = "ERROR_CONSULTA_CLASIFICACIONES_EVENTO";
	/**
	 * Atributo que determina el ERROR_CONSULTA_ESTADOS_ENTREGA
	 */
	public static final String ERROR_CONSULTA_ESTADOS_ENTREGA = "ERROR_CONSULTA_ESTADOS_ENTREGA";

	/**
	 * Atributo que determina el ERROR_CONSULTA_SOLICITUD_PROGRAMACION_ENTREGA
	 */
	public static final String ERROR_CONSULTA_SOLICITUD_PROGRAMACION_ENTREGA = "ERROR_CONSULTA_SOLICITUD_PROGRAMACION_ENTREGA";
	/**
	 * Atributo que determina el
	 * ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA
	 */
	public static final String ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA = "ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA";
	/**
	 * Atributo que determina el mensaje de error al adicionar una prestacion
	 */
	public static final String ERROR_ADICIONAR_PRESTACION = "ERROR_ADICIONAR_PRESTACION";
	/**
	 * Atributo que determina la ERROR_CARGA_PARAMETROS
	 */
	public static final String ERROR_CARGA_PARAMETROS = "ERROR_CARGA_PARAMETROS";
	/**
	 * Atributo que determina la
	 * ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA_NEGOCIO
	 */
	public static final String ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA_NEGOCIO = "ERROR_EJECUTAR_CREACION_SOLICITUD_PROGRAMACION_ENTREGA_NEGOCIO";
	/**
	 * Atributo que determina ERROR_EJECUTAR_ACTUALIZAR_PROGRAMACION_ENTREGA
	 */
	public static final String ERROR_EJECUTAR_ACTUALIZAR_PROGRAMACION_ENTREGA = "ERROR_EJECUTAR_ACTUALIZAR_PROGRAMACION_ENTREGA";
	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_NEGACION = "PROPIEDAD_RUTA_JASPER_NEGACION";
	/**
	 * Atributo que determina la propiedad ERROR_GENERAR_REPORTE_NEGACION
	 */
	public static final String ERROR_GENERAR_REPORTE_NEGACION = "ERROR_GENERAR_REPORTE_NEGACION";
	/**
	 * Atributo que determina la propiedad
	 * ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO
	 */
	public static final String ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO = "ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO";
	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_NEGACION_SUB_FUNDAMENTOS = "PROPIEDAD_RUTA_JASPER_NEGACION_SUB_FUNDAMENTOS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_NEGACION_SUB_ALTERNATIVAS = "PROPIEDAD_RUTA_JASPER_NEGACION_SUB_ALTERNATIVAS";

	/**
	 * Atributo que determina el mensaje de error al ejecutar el proceso que
	 * consulta las fechas de vigencia de la OPS
	 */
	public static final String ERROR_CONSULTAR_VIGENCIAS_ESTADO_OPS = "ERROR_CONSULTAR_VIGENCIAS_ESTADO_OPS";

	/**
	 * Atributo que determina el mensaje de error al ejecutar el proceso que
	 * consulta los soportes de la presolicitud
	 */
	public static final String ERROR_CONSULTA_SOPORTES = "ERROR_CONSULTA_SOPORTES";

	/**
	 * Atributo que determina el mensaje de error para la consulta del afiliado
	 * en la presolicitud
	 */
	public static final String ERROR_CONSULTA_AFILIADO = "ERROR_CONSULTA_AFILIADO";

	/**
	 * Atributo que determina el mensaje de error al guardar la devolucion de
	 * una presolicitud
	 */
	public static final String ERROR_GUARDAR_DEVOLUCION = "ERROR_GUARDAR_DEVOLUCION";

	/**
	 * Atributo que determina el mensaje de error al consultar los tipos de
	 * soporte de la presolicitud
	 */
	public static final String ERROR_CONSULTA_TIPO_SOPORTES = "ERROR_CONSULTA_TIPO_SOPORTES";

	/**
	 * Atributo que determina el mensaje de error de consultar causales
	 * descuadre
	 */
	public static final String ERROR_CONSULTA_CAUSALES_DESCUADRE = "ERROR_CONSULTA_CAUSALES_DESCUADRE";
	/**
	 * Cadena para realizar la consulta de los metodos de devolucion
	 */
	public static final String COMBO_METODO_DEVOLUCION = "metodoDevolucion";

	/**
	 * Cadena para realizar la consulta de los bancos.
	 */
	public static final String COMBO_BANCO = "banco";

	/**
	 * Mensaje de error de la consulta de sp de metodos de devolucion
	 */
	public static final String ERROR_CONSULTA_COMBO_METODO_DEVOLUCION = "ERROR_CONSULTA_COMBO_METODO_DEVOLUCION";

	/**
	 * Mensaje de error de onsulta de sp de bancos
	 */
	public static final String ERROR_CONSULTA_COMBO_BANCO = "ERROR_CONSULTA_COMBO_BANCO";

	/**
	 * XML Nodes movimientoscja
	 */
	public static final String NODE_MOVIMIENTOSCJA = "movimientoscja";

	/**
	 * XML Nodes movimientocja
	 */
	public static final String NODE_MOVIMIENTOCJA = "movimientocja";

	/**
	 * XML Nodes cnsctvo_mvmnto_cja
	 */
	public static final String NODE_CNSCTVO_MVMNTO_CJA = "cnsctvo_mvmnto_cja";

	/**
	 * XML Nodes usro_ultma_mdfccn
	 */
	public static final String NODE_USRO_ULTMA_MDFCCN = "usro_ultma_mdfccn";

	/**
	 * XML Nodes ttl_efctvo
	 */
	public static final String NODE_TTL_EFCTVO = "ttl_efctvo";

	/**
	 * XML Nodes ttl_efctvo
	 */
	public static final String NODE_TTL_NTA_CRDTO = "ttl_nta_crdto";

	/**
	 * XML Nodes ttl_efctvo
	 */
	public static final String NODE_TTL_CJA = "ttl_cja";

	/**
	 * XML Nodes sbrnte_crre
	 */
	public static final String NODE_SBRNTE_CRRE = "sbrnte_crre";

	/**
	 * XML Nodes fltnte_crre
	 */
	public static final String NODE_FLTNTE_CRRE = "fltnte_crre";

	/**
	 * XML Nodes cnsctvo_cdgo_csl_dscdre
	 */
	public static final String NODE_CNSCTVO_CDGO_CSL_DSCDRE = "cnsctvo_cdgo_csl_dscdre";

	/**
	 * XML Nodes vlr_dscdre
	 */
	public static final String NODE_VLR_DSCDRE = "vlr_dscdre";

	/**
	 * XML Nodes vlr_dscdre
	 */
	public static final String NODE_USRO_CRCN = "usro_crcn";
	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_CIERRE_CAJA = "PROPIEDAD_RUTA_JASPER_CIERRE_CAJA";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_LOGO_SOS = "PROPIEDAD_RUTA_LOGO_SOS";

	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String RUTA_LOGO_SOS = "RUTA_LOGO";

	/**
	 * Nombre de parametro en el jasper
	 */
	public static final String LOGO_URL_SOS = "LOGO_URL";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_CIERRE_OFICINA = "PROPIEDAD_RUTA_JASPER_CIERRE_OFICINA";

	/**
	 * Mensaje de error al generar el reporte.
	 */
	public static final String ERROR_GENERAR_REPORTE = "ERROR_GENERAR_REPORTE";
	/**
	 * Atributo que determina la extension del archivo
	 */
	public static final String ARCHIVO_XLSX = ".xls";

	public static final String PROPEDAD_RUTA_JASPER_RECIBO = "PROPIEDAD_JASPER_RECIBO_CAJA";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_REPORTE_NOTA_CREDITO = "PROPIEDAD_RUTA_JASPER_REPORTE_NOTA_CREDITO";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String PROPIEDAD_RUTA_JASPER_INFO_CIERRE_CAJA = "PROPIEDAD_RUTA_JASPER_INFOR_CIERRE_CAJA";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS = "RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_RECIBOS = "RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_RECIBOS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_NOTAS = "RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_NOTAS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String RUTA_SUBREPORTE_RECIBOS = "RUTA_SUBREPORTE_RECIBOS";

	/**
	 * Atributo que determina la propiedad con el que se invoca el propertyQuery
	 */
	public static final String RUTA_SUBREPORTE_NOTAS = "RUTA_SUBREPORTE_NOTAS";

	/**
	 * Mensaje error XML
	 */
	public static final String ERROR_XML = "Error Generando XML";

	/**
	 * Estado cierre administrativo
	 */
	public static final int ESTADO_CIERRE_ADMINISTRATIVO = 2;

	/**
	 * Estado del cierre
	 */
	public static final int ESTADO_CIERRE = 1;

	/**
	 * Descripcion para la clase utilidades.
	 */
	public static final String CLASE_UTILIDAD = "CLASE UTILIDADES";

	public static final String RUTA_JASPER_REPORTE_CIERRE_CAJA_DETALLADO = "RUTA_JASPER_REPORTE_CIERRE_CAJA_DETALLADO";

	public static final String CAUSALES = "causales";

	public static final String CAUSAL = "causal";

	public static final String YES = "yes";

	public static final String RAIZ_XML_OPS = "lista_ops";

	public static final String NODO_XML_OPS = "ops";

	public static final String ATTR_XML_OPS_DOC = "nmro_unco_ops";

	public static final String ATTR_XML_OPS_VLR = "vlr_ops";

	public static final String RAIZ_XML_NOTA = "lista_notas";

	public static final String NODO_XML_NOTA = "nota";

	public static final String ATTR_XML_NOTA_DOC = "cnsctvo_dcmnto_cja";

	public static final String ATTR_XML_NOTA_VLR = "vlr_nta_crdto";

	public static final String CLASIFICACION_URL_CAJAS = "CLASIFICACION_URL_CAJAS";

	public static final String ERROR_CONSULTA_COMBO_SEDES = "ERROR_CONSULTA_COMBO_SEDES";

	public static final String COMBO_SEDES = "sedes";

	/**
	 * Atributo que determina el CODS_ESTADOS_CON_DERECHO_AFILIADO
	 */
	public static final Set<Integer> CODS_ESTADOS_CON_DERECHO_AFILIADO = new HashSet<Integer>(Arrays
			.asList(new Integer(1), new Integer(2), new Integer(3), new Integer(10), new Integer(13), new Integer(15)));

	/**
	 * Atributo que determina el CODS_ESTADOS_SIN_DERECHO_AFILIADO
	 */
	public static final Set<Integer> CODS_ESTADOS_SIN_DERECHO_AFILIADO = new HashSet<Integer>(
			Arrays.asList(new Integer(4), new Integer(5), new Integer(6), new Integer(7), new Integer(8),
					new Integer(9), new Integer(12), new Integer(14), new Integer(16), new Integer(17)));

	public static final String MEDIO_CONTACTOS_AFILIADO = "04";

	public static final String ERROR_CONSULTA_RESULTADO_MALLA = "ERROR_CONSULTA_RESULTADO_MALLA";

	public static final String COMBO_ESTADOS_NOTA_CREDITO = "COMBO_ESTADOS_NOTA_CREDITO";

	/**
	 * Atributo que determina la validacion de la consulta de IPS
	 */
	public static final String ERROR_VALIDACION_DATOS_REQUERIDOS_CONSULTA_IPS = "ERROR_VALIDACION_DATOS_REQUERIDOS_CONSULTA_IPS";

	public static final int CONSECUTIVO_CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO = 2;

	public static final String ERROR_OFICINA_NO_ENCONTRADA = "ERROR_OFICINA_NO_ENCONTRADA";

	public static final String EXCEPCION_SERVICIO_DIRECCIONAMIENTO = "EXCEPCION_SERVICIO_DIRECCIONAMIENTO";

	public static final String ERROR_CONSULTAR_PRESTACION_SOLICITUD = "ERROR_CONSULTAR_PRESTACION_SOLICITUD";

	public static final String ERROR_GUARDAR_DIRECCIONAMIENTO_PRESTACION = "ERROR_GUARDAR_DIRECCIONAMIENTO_PRESTACION";

	public static final String ERROR_GUARDAR_RECOBRO_X_PRESTACION = "ERROR_GUARDAR_RECOBRO_X_PRESTACION";

	public static final String ERROR_LIQUIDAR_MOD_OPS = "ERROR_LIQUIDAR_MOD_OPS";

	public static final String ERROR_CONSULTAR_CAUSAS_NO_COBRO = "ERROR_CONSULTAR_CAUSAS_NO_COBRO";

	public static final String ETIQUETA_COD_CAUSA_NO_COBRO_CUOTA = "causaNoCobro";

	/**
	 * Atributo que determina el mensaje para la consulta adicional de las
	 * prestaciones asociadas a la solicitud
	 */
	public static final String ERROR_CONSULTA_ADICIONAL_PRESTACIONES_SOLICITUD = "ERROR_CONSULTA_ADICIONAL_PRESTACIONES_SOLICITUD";

	public static final String ERROR_GUARDAR_INFORMACION_PRESTACION = "ERROR_GUARDAR_INFORMACION_PRESTACION";

	public static final String ERROR_CONSULTAR_RECIBOS_CAJA = "ERROR_CONSULTAR_RECIBOS_CAJA";

	public static final int VALOR_CERO = 0;

	public static final String ERROR_CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR = "ERROR_CONSULTA_RESULTADO_MALLA_GESTION_AUDITOR";

	public static final String CONSULTA_CONVENIOS_IPS = "CONSULTA_CONVENIOS_IPS";

	/**
	 * Atributo que determina el mensaje para la consulta de los convenios de
	 * ips
	 */
	public static final String ERROR_CONSULTA_CONVENIOS_ISP = "ERROR_CONSULTA_CONVENIOS_ISP";

	public static final String ERROR_CONSULTA_PAGOS_FIJOS = "ERROR_CONSULTA_PAGOS_FIJOS";

	public static final String ERROR_CONSULTA_PAGOS_FIJOS_PARAMETROS = "ERROR_CONSULTA_PAGOS_FIJOS_PARAMETROS";

	public static final String ETIQUETA_ANULACION = "anular";

	public static final String ETIQUETA_OBSERVACION_ANULACION = "observacion";

	public static final String ETIQUETA_ORIGEN_MODIFICACION = "origenModificacion";

	/**
	 * Atributos que determinan el valor del consecutivo de una tabla
	 */
	public static final int CONSTANTE_VACIO = 0;

	public static final int CONSTANTE_INEXISTENTE = 99999;

	/**
	 * Atributo que determina el mensaje para la consulta de resumen OPS
	 */
	public static final String ERROR_CONSULTA_RESUMEN_OPS = "ERROR_CONSULTA_RESUMEN_OPS";

	private ConstantesEJB() {
		/* clase utilizaría constructor privado para evitar instancias */
	}

	/**
	 * 
	 * @return
	 */
	public static final Set<Integer> getCodigosEstadosConDerecho() {
		return CODS_ESTADOS_CON_DERECHO_AFILIADO;
	}

	/**
	 * 
	 * @return
	 */
	public static final Set<Integer> getCodigosEstadosSinDerecho() {
		return CODS_ESTADOS_SIN_DERECHO_AFILIADO;
	}
}
