package com.sos.gestionautorizacionessaludejb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sos.validador.bean.resultado.adicionales.DatosAdicionalesAfiliado;

import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;


/**
 * Class Utilitaria para funciones web
 * Clase donde se relacionan las funciones mas utilizadas en los MB
 * @author Julian Hernandez
 * @version 14/03/2016
 *
 */
public class FuncionesAppEJB {
	
	
	private FuncionesAppEJB(){
		/*clase utilizaría constructor privado para evitar instancias */	
	}

	/**
	 * 
	 * @param datosAdicionalesAfiliado
	 * @param afiliadoVO
	 * @return
	 * @throws ParseException 
	 */
	public static final AfiliadoVO datosAdicionalesAfiliadoToAfiliadoVO(DatosAdicionalesAfiliado datosAdicionalesAfiliado, AfiliadoVO afiliadoVO ) throws ParseException{
		afiliadoVO.setDireccionResidencia(datosAdicionalesAfiliado.getDireccion());
		afiliadoVO.setTelefono(datosAdicionalesAfiliado.getTelefono());
		afiliadoVO.setFechaNacimiento(stringToDate(datosAdicionalesAfiliado.getFechaNacimiento(), ConstantesEJB.FORMATO_FECHA_DIA));
		afiliadoVO.setEdadAnos(datosAdicionalesAfiliado.getEdadAnos());
		afiliadoVO.setEdadMeses(datosAdicionalesAfiliado.getEdadMeses());
		afiliadoVO.setEdadDias(datosAdicionalesAfiliado.getEdadDias());
		afiliadoVO.setFechaInicioVigencia(stringToDate(datosAdicionalesAfiliado.getInicioVigencia(),ConstantesEJB.FORMATO_FECHA_DIA));
		afiliadoVO.setFechaFinVigencia(stringToDate(datosAdicionalesAfiliado.getFinVigencia(),ConstantesEJB.FORMATO_FECHA_DIA));
		afiliadoVO.setDescripcionTipoAfiliado(datosAdicionalesAfiliado.getTipoAfiliado());
		afiliadoVO.setSemanasAfiliacionPACSOS(datosAdicionalesAfiliado.getSemanasPacSos());
		afiliadoVO.setSemanasAfiliacionPOSSOS(datosAdicionalesAfiliado.getSemanasAfiliacionPosSOS());
		afiliadoVO.setDescripcionIPSPrimaria(datosAdicionalesAfiliado.getDescripcionIpsPrimaria());
		afiliadoVO.setSemanasAfiliacionPOSAnt(datosAdicionalesAfiliado.getSemanasAfiliacionPosAnterior());
		afiliadoVO.setSemanasAfiliacionPACAnt(datosAdicionalesAfiliado.getSemanasPacAnterior());
		afiliadoVO.setDescripcionRangoSalarial(datosAdicionalesAfiliado.getRangoSalarial());
		return afiliadoVO;
	}

	/**
	 * 
	 * @param fechaString
	 * @param formato
	 * @return
	 * @throws ParseException 
	 */
	public static final Date stringToDate(String fechaString, String formato) throws ParseException{
		Date fechaDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		fechaDate = formatter.parse(fechaString);
		return fechaDate;
	}

	/**
	 * 
	 * @param fechaString
	 * @param formato
	 * @return
	 */
	public static final String dateToString(Date fecha){
		String fechaString = "";
		if (fecha!=null) {
			SimpleDateFormat formatter = new SimpleDateFormat(ConstantesEJB.FORMATO_FECHA_DIA);
			fechaString = formatter.format(fecha);
		} 
		return fechaString;
	}
	
	/**
	 * 
	 * @param fechaString
	 * @param formato
	 * @return
	 */
	public static final String objectToString(Object obj){
		String result = "";
		if (obj!=null) {
			result = obj.toString().trim();
		} 
		return result;
	}

}
