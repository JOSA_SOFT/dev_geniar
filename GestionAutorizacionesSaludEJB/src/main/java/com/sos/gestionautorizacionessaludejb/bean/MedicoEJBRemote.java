package com.sos.gestionautorizacionessaludejb.bean;

import java.util.List;

import javax.ejb.Remote;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.ServiceException;
import com.sos.medico.modelo.ListaMedicoResponse;
import com.sos.medico.modelo.MedicoResponse;
import com.sos.medico.modelo.MedicoVO;

@Remote
public interface MedicoEJBRemote {

	public abstract MedicoResponse consultarMedicoAdscritoNroUnico(Long numeroUnico) throws ServiceException;

	public abstract ListaMedicoResponse consultarMedicoPorId(long consecutivoTipoId, String numeroIdentificacion) throws ServiceException;

	public abstract ListaMedicoResponse consultarMedicoPorNombre(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido) throws ServiceException;

	public abstract ListaMedicoResponse consultarMedicoPorRegistro(String registroMedico) throws ServiceException;

	public abstract List<MedicoVO> consultarMedicoPorParametros(MedicoVO medicoVO) throws LogicException, ServiceException ;
}