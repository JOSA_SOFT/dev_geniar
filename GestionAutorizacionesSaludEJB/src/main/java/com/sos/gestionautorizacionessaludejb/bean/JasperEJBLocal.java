package com.sos.gestionautorizacionessaludejb.bean;

import java.sql.SQLException;
import java.util.Map;

import javax.ejb.Local;

import com.sos.excepciones.LogicException;

@Local
public interface JasperEJBLocal {
	public String generarReporte(Map<String, Object> params, String tipo, String idJasper)throws LogicException, SQLException, Exception;
}
