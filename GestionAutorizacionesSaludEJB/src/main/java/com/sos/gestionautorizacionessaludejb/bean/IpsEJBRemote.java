package com.sos.gestionautorizacionessaludejb.bean;

import java.util.List;

import javax.ejb.Remote;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.ServiceException;
import com.sos.ips.modelo.IPSVO;
import com.sos.ips.modelo.ListaIPSResponse;

@Remote
public interface IpsEJBRemote {

	
	/***
	 * Metodos que permite realizar la consulta de los prestadores
	*/
	public abstract IPSVO consultarIpsPorCodigoInterno(String codigoInterno) throws ServiceException;

	public abstract ListaIPSResponse consultarIpsPorIdentificacionVigencia(Long consecutivoTipoId, String numeroIdentificacion) throws ServiceException;

	public abstract ListaIPSResponse consultarIpsPorNombre(String nombreIPS) throws ServiceException;

	public abstract List<IPSVO> consultarIpsPorParametros(IPSVO ipsVO) throws LogicException, ServiceException;
	
	
	/***
	 * Metodos que permite realizar la consulta de los consultorios 
	*/
	public abstract IPSVO consultarConsultoriosPorCodigoInterno(String codigoInterno) throws ServiceException;

	public abstract ListaIPSResponse consultarConsultoriosPorIdentificacionVigencia(Long consecutivoTipoId, String numeroIdentificacion) throws ServiceException;

	public abstract ListaIPSResponse consultarConsultoriosPorNombre(String nombreIPS) throws ServiceException;

	public abstract List<IPSVO> consultarConsultoriosPorParametros(IPSVO ipsVO) throws ServiceException, LogicException;
}
