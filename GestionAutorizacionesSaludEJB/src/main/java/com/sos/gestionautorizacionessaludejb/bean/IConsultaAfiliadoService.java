package com.sos.gestionautorizacionessaludejb.bean;

import java.util.Date;
import java.util.List;

import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.DatosAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;

/**
 * @type: Interface
 * @name: IConsultaAfiliadoService
 * @description: Interface encargada de exponer los metodos para la consulta del afiliado
 * @author Victor Hugo Gil Ramos
 * @version 29/01/2016
 */
public interface IConsultaAfiliadoService {
	
	/** Metodo que consume el servicio del afiliado */	
	public ResultadoConsultaAfiliadoDatosAdicionales consultaRespuestaServicioAfiliado(String codigoTipoIdentificacion, String numeroIdentificacion, String descripcionPlan, Date fechaConsulta, String usuarioConsulta) throws LogicException;
	
	/** Metodo que consulta la informacion del afiliado */	
	public AfiliadoVO consultaInformacionAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException;
	
	/** Metodo que consulta los datos adicionales del afiliado */	
	public DatosAfiliadoVO consultaDatosAdicionalesAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException;
	
	/** Metodo que consulta el servicio del afiliado para los empleados*/
	public List<EmpleadorVO> consultaInformacionEmpleadorAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException;
		
	/** Metodo que consulta el servicio del afiliado para los convenio de capitacion*/
	public List<ConveniosCapitacionVO> consultaConveniosCapitacion(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException;	
	
	/** Metodo que consulta el afiliado por nombres*/
	public List<AfiliadoVO> buscaAfiliadoxNombre(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String usuarioConsulta)throws LogicException;
}
