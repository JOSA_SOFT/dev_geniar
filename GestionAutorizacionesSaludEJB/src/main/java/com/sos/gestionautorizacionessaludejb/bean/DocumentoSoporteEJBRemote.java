package com.sos.gestionautorizacionessaludejb.bean;

import java.util.List;

import javax.ejb.Remote;

import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;

@Remote
public interface DocumentoSoporteEJBRemote {

	public List<DocumentoSoporteVO> consultarDocumentoSoporte(
			Integer consecutivoPrestacion) throws ServiceException;

}