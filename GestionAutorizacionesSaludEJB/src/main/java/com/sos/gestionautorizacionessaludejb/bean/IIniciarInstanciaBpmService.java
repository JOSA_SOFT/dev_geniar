package com.sos.gestionautorizacionessaludejb.bean;

import co.com.sos.enterprise.inicioinstancia.v1.InicioInstanciaService;
import co.com.sos.inicioinstancia.v1.InicioInstanciaResType;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.IniciarInstanciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;

import co.eps.sos.service.exception.ServiceException;


/**
 * @type: Interface
 * @name: IIniciarInstanciaBpmService
 * @description: Interface encargada de exponer el metodo para iniciar el proceso en el bpm
 * @author Victor Hugo Gil Ramos
 * @version 22/03/2016
 */
public interface IIniciarInstanciaBpmService {
	
	/** Metodo que crea el servicio*/	
	public InicioInstanciaService crearInicioInstanciaService() throws LogicException;
	
	/** Metodo que consume el servicio para iniciar la instancia*/	
	public InicioInstanciaResType inicioInstanciaService(InicioInstanciaService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;
	
	/** Metodo que permite visualizar la respuesta del servicio para iniciar la instancia*/	
	public IniciarInstanciaVO resultadoInicioInstanciaService(InicioInstanciaResType inicioInstanciaResType, String usuarioSession)  throws ServiceException;
}
