package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.net.MalformedURLException;
import java.net.URL;

import co.com.sos.RequestCabeceraType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.consultarresultadodireccionamiento.v1.ConsultarResultadoDireccionamientoReqType;
import co.com.sos.consultarresultadodireccionamiento.v1.ConsultarResultadoDireccionamientoResType;
import co.com.sos.ejecutardireccionamiento.v1.CuerpoReqType;
import co.com.sos.ejecutardireccionamiento.v1.CuerpoResType;
import co.com.sos.ejecutardireccionamiento.v1.EjecutarDireccionamientoReqType;
import co.com.sos.ejecutardireccionamiento.v1.EjecutarDireccionamientoResType;
import co.com.sos.enterprise.direccionamiento.v1.DireccionamientoPortType;
import co.com.sos.solicitud.v1.SolicitudReqType;
import co.com.sos.solicitud.v1.SolicitudesDireccionamientoResType;
import co.com.sos.solicitud.v1.SolicitudesReqType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.IDireccionamientoService;
import com.sos.gestionautorizacionessaludejb.service.controller.AdvertenciaServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.CabeceraServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.ErroresServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.SolicitudesDireccionamientoServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * @name: DireccionamientoService
 * @description: Interface encargada de implementar los metodos para el
 *               direccionamiento de la solictud
 * @author Ing. Victor Hugo Gil Ramos
 * @version 29/03/2016
 */

public class DireccionamientoService implements IDireccionamientoService {

	private co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService service;

	/** Metodo que inicia el servicio de direccionamiento */
	@Override
	public co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService crearDireccionamientoService() throws LogicException {

		String wsdlLocation;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_DIRECCIONAMIENTO_SERVICE));
		try {
			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);
			service = new co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService(new URL(wsdlLocation));

		} catch (MalformedURLException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return service;
	}

	/** Metodo que ejecuta el servicio de direccionamiento */
	@Override
	public EjecutarDireccionamientoResType ejecutarDireccionamiento(co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService service, SolicitudVO solicitudVO, String usuarioSession)
			throws LogicException {

		DireccionamientoPortType direccionamientoPortType = null;
		EjecutarDireccionamientoResType ejecutarDireccionamientoResType = null;

		SolicitudReqType solicitudDireccionamientoReqType = new SolicitudReqType();

		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */

		solicitudDireccionamientoReqType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getConsecutivoSolicitud()));
		solicitudDireccionamientoReqType.setNumeroSolicitudSOS(solicitudVO.getNumeroSolicitudSOS());
		solicitudDireccionamientoReqType.setNumeroSolicitudPrestador(ConstantesEJB.CADENA_VACIA);

		SolicitudesReqType solicitudesReqType = new SolicitudesReqType();
		solicitudesReqType.getSolicitudReq().add(solicitudDireccionamientoReqType);

		/* se adiciona la informacion del cuerpo */
		CuerpoReqType cuerpoReqType = new CuerpoReqType();
		cuerpoReqType.setSolicitudesReq(solicitudesReqType);
		/* fin informacion del cuerpo */

		EjecutarDireccionamientoReqType direccionamientoReqType = new EjecutarDireccionamientoReqType();
		direccionamientoReqType.setRequestCabecera(requestCabeceraType);
		direccionamientoReqType.setCuerpo(cuerpoReqType);

		try {

			direccionamientoPortType = service.getDireccionamientoPort();
			ejecutarDireccionamientoResType = direccionamientoPortType.ejecutarDireccionamiento(direccionamientoReqType);

		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return ejecutarDireccionamientoResType;
	}

	/**
	 * Metodo que consulta el resultado del servicio de direccionamiento
	 * 
	 * @throws ServiceException
	 * @throws LogicException 
	 */
	@Override
	public DireccionamientoSolicitudVO consultaResultadoDireccionamiento(EjecutarDireccionamientoResType ejecutarDireccionamientoResType,
			ConsultarResultadoDireccionamientoResType consultarResultadoDireccionamientoResType) throws ServiceException, LogicException {

		ResponseCabeceraType cabeceraType = null;
		int marcaConsulta = 0;

		if (ejecutarDireccionamientoResType != null) {
			cabeceraType = ejecutarDireccionamientoResType.getResponseCabecera();
			marcaConsulta = 1;
		} else {
			cabeceraType = consultarResultadoDireccionamientoResType.getResponseCabecera();
		}

		DireccionamientoSolicitudVO direccionamientoSolicitudVO = asignarValorDireccionamiento(cabeceraType, marcaConsulta, ejecutarDireccionamientoResType, consultarResultadoDireccionamientoResType);

		if (direccionamientoSolicitudVO.getServiceErrorVO().getCodigoError() != null) {
			throw new ServiceException(direccionamientoSolicitudVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR
					+ direccionamientoSolicitudVO.getServiceErrorVO().getCodigoError() + (char) 13 + direccionamientoSolicitudVO.getServiceErrorVO().getMensajeError(), null);
		}

		return direccionamientoSolicitudVO;
	}

	public DireccionamientoSolicitudVO asignarValorDireccionamiento(ResponseCabeceraType cabeceraType, int marcaConsulta, EjecutarDireccionamientoResType ejecutarDireccionamientoResType,
			ConsultarResultadoDireccionamientoResType consultarResultadoDireccionamientoResType) throws LogicException {
		SolicitudesDireccionamientoResType solicitudesDireccionamientoResType = null;
		DireccionamientoSolicitudVO direccionamientoSolicitudVO = new DireccionamientoSolicitudVO();
		ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
		serviceErrorVO.setEstadoTerminacion(cabeceraType.getEstadoTerminacion().value());
		serviceErrorVO.setMensajeEstadoTerminacion(cabeceraType.getMensaje());

		if (cabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(cabeceraType.getEstadoTerminacion().value())
				&& cabeceraType.getErrores() != null) {
			ErroresServiceType erroresServiceType = new ErroresServiceType(cabeceraType.getErrores());
			serviceErrorVO = erroresServiceType.informacionErrorServicio();
			
		} else if (cabeceraType.getEstadoTerminacion() != null && ConstantesEJB.OK_SERVICE.equals(cabeceraType.getEstadoTerminacion().value())) {
			if (cabeceraType.getAdvertencias() != null) {
				AdvertenciaServiceType advertenciaServiceType = new AdvertenciaServiceType(cabeceraType.getAdvertencias());
				serviceErrorVO = advertenciaServiceType.informacionAdvertenciasServicio();
			}

			if (marcaConsulta == 1) {
				CuerpoResType cuerpoResType = ejecutarDireccionamientoResType.getCuerpo();
				solicitudesDireccionamientoResType = cuerpoResType.getSolicitudesDireccionamientoRes();
			} else {
				co.com.sos.consultarresultadodireccionamiento.v1.CuerpoResType cuerpoResType = consultarResultadoDireccionamientoResType.getCuerpo();
				solicitudesDireccionamientoResType = cuerpoResType.getSolicitudesDireccionamientoRes();
			}
			
			if(solicitudesDireccionamientoResType != null){
				SolicitudesDireccionamientoServiceType direccionamientoServiceType = new SolicitudesDireccionamientoServiceType();
				direccionamientoSolicitudVO = direccionamientoServiceType.informacionDireccionamientoService(solicitudesDireccionamientoResType);
			}else{
				throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.EXCEPCION_SERVICIO_DIRECCIONAMIENTO), LogicException.ErrorType.DATO_NO_EXISTE);
			}				
		}

		direccionamientoSolicitudVO.setServiceErrorVO(serviceErrorVO);
		return direccionamientoSolicitudVO;
	}

	/** Metodo que consulta el metodo de consulta direccionamiento */
	@Override
	public ConsultarResultadoDireccionamientoResType consultaDireccionamiento(co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService service, SolicitudVO solicitudVO, String usuarioSession)
			throws LogicException {
		DireccionamientoPortType direccionamientoPortType = null;
		ConsultarResultadoDireccionamientoResType consultarResultadoDireccionamientoResType = null;

		SolicitudReqType solicitudDireccionamientoReqType = new SolicitudReqType();
		solicitudDireccionamientoReqType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getConsecutivoSolicitud()));
		solicitudDireccionamientoReqType.setNumeroSolicitudSOS(solicitudVO.getNumeroSolicitudSOS());
		solicitudDireccionamientoReqType.setNumeroSolicitudPrestador(ConstantesEJB.CADENA_VACIA);

		SolicitudesReqType solicitudesReqType = new SolicitudesReqType();
		solicitudesReqType.getSolicitudReq().add(solicitudDireccionamientoReqType);

		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */

		/* se adiciona la informacion del cuerpo */
		co.com.sos.consultarresultadodireccionamiento.v1.CuerpoReqType cuerpoReqType = new co.com.sos.consultarresultadodireccionamiento.v1.CuerpoReqType();
		cuerpoReqType.setSolicitudesDireccionamientoReq(solicitudesReqType);
		/* fin informacion del cuerpo */

		ConsultarResultadoDireccionamientoReqType consultarResultadoDireccionamientoReqType = new ConsultarResultadoDireccionamientoReqType();
		consultarResultadoDireccionamientoReqType.setRequestCabecera(requestCabeceraType);
		consultarResultadoDireccionamientoReqType.setCuerpo(cuerpoReqType);

		try {
			direccionamientoPortType = service.getDireccionamientoPort();
			consultarResultadoDireccionamientoResType = direccionamientoPortType.consultarResultadoDireccionamiento(consultarResultadoDireccionamientoReqType);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_DIRECCIONAMIENTO_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultarResultadoDireccionamientoResType;
	}
}
