package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.ServiceException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaludejb.bean.IpsEJBLocal;
import com.sos.gestionautorizacionessaludejb.bean.IpsEJBRemote;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.EJBCaller;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.ips.IPSService;
import com.sos.ips.modelo.IPSResponse;
import com.sos.ips.modelo.IPSVO;
import com.sos.ips.modelo.ListaIPSResponse;

@Stateless
public class IpsEJB implements IpsEJBLocal, IpsEJBRemote, Serializable {
	private static final long serialVersionUID = -5094670054358835284L;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.IpsEJBLocal#
	 * consultarIpsPorCodigoInterno()
	 */
	private static final String IPS_SERVICE = ConstantesEJB.IPS_SERVICE;

	@Override
	public IPSVO consultarIpsPorCodigoInterno(String codigoInterno) throws ServiceException {
		try {
			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			IPSResponse buscarIPSPorCodigoInterno = ipsservice.buscarIPSPorCodigoVigencias(codigoInterno, new Date(System.currentTimeMillis()));
			return buscarIPSPorCodigoInterno.getDatosIPS();
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}
	
	
	/**
	 * Metodo que permite la consulta de los consultorios por codigo interno 
	 */
	@Override
	public IPSVO consultarConsultoriosPorCodigoInterno(String codigoInterno) throws ServiceException {
		try {
			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			IPSResponse buscarIPSPorCodigoInterno = ipsservice.buscarIPSConsultoriosPorCodigoInternoVigentes(codigoInterno, new Date(System.currentTimeMillis()), true);
			return buscarIPSPorCodigoInterno.getDatosIPS();					
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.IpsEJBLocal#
	 * consultarIpsPorIdentificacionVigencia()
	 */
	@Override
	public ListaIPSResponse consultarIpsPorIdentificacionVigencia(Long consecutivoTipoId, String numeroIdentificacion) throws ServiceException {
		try {
			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			return ipsservice.buscarIPSPorIdentificacionVigentes(consecutivoTipoId, numeroIdentificacion, new Date(System.currentTimeMillis()));
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}
	
	/**
	 * Metodo que permite la consulta de los consultorios por identificacion 
	 */
	@Override
	public ListaIPSResponse consultarConsultoriosPorIdentificacionVigencia(Long consecutivoTipoId, String numeroIdentificacion) throws ServiceException {
		try {
			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			return ipsservice.buscarIPSConsultoriosPorIdentificacionVigentes(consecutivoTipoId, numeroIdentificacion, new Date(System.currentTimeMillis()), true);
	
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.IpsEJBLocal#
	 * consultarIpsPorNombre()
	 */
	@Override
	public ListaIPSResponse consultarIpsPorNombre(String nombreIPS) throws ServiceException {
		try {

			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			return ipsservice.buscarIPSPorNombreVigentes(nombreIPS, new Date(System.currentTimeMillis()));

		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}
	

	/**
	 * Metodo que permite la consulta de los consultorios por nombre
	 */
	@Override
	public ListaIPSResponse consultarConsultoriosPorNombre(String nombreIPS) throws ServiceException {
		try {

			IPSService ipsservice = EJBCaller.invoke(Messages.getValorParametro(IPS_SERVICE));
			return ipsservice.buscarIPSConsultoriosPorNombreVigentes(nombreIPS, new Date(System.currentTimeMillis()), true);
			
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, IpsEJB.class);
		}
	}

	
	/**
	 * Metodo que me permite consultar las ips
	 */
	@Override
	public List<IPSVO> consultarIpsPorParametros(IPSVO ipsVO) throws ServiceException, LogicException {
		List<IPSVO> listaIPSVO = new ArrayList<IPSVO>();
		IPSVO ipsvOCodInterno = new IPSVO();
		if (validarCodigoInterno(ipsVO)) {
			ipsvOCodInterno = consultarIpsPorCodigoInterno(ipsVO.getCodigoInterno());
			if (ipsvOCodInterno.getCodigoInterno() != null) {
				listaIPSVO.add(ipsvOCodInterno);
			}
		} else if (validarIpsVO(ipsVO)) {
			listaIPSVO = Arrays.asList(consultarIpsPorIdentificacionVigencia(ipsVO.getConsecCodigoTipoId(), Utilidades.quitarEspaciosCadena(ipsVO.getNumeroIdentificacion())).getDatosIPS());
		} else if (validarSucursal(ipsVO)) {
			listaIPSVO = Arrays.asList(consultarIpsPorNombre(ipsVO.getSucursal()).getDatosIPS());
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_VALIDACION_DATOS_REQUERIDOS_CONSULTA_IPS), ErrorType.PARAMETRO_ERRADO);
		}

		return listaIPSVO;
	}
	
	/**
	 * Metodo que me permite consultar los consultorios medicos
	 */
	@Override
	public List<IPSVO> consultarConsultoriosPorParametros(IPSVO ipsVO) throws ServiceException, LogicException {
		List<IPSVO> listaIPSVO = new ArrayList<IPSVO>();
		IPSVO ipsvOCodInterno = new IPSVO();
		if (validarCodigoInterno(ipsVO)) {
			ipsvOCodInterno = consultarConsultoriosPorCodigoInterno(ipsVO.getCodigoInterno());
			if (ipsvOCodInterno.getCodigoInterno() != null) {
				listaIPSVO.add(ipsvOCodInterno);
			}
		} else if (validarIpsVO(ipsVO)) {
			listaIPSVO = Arrays.asList(consultarConsultoriosPorIdentificacionVigencia(ipsVO.getConsecCodigoTipoId(), Utilidades.quitarEspaciosCadena(ipsVO.getNumeroIdentificacion())).getDatosIPS());
		} else if (validarSucursal(ipsVO)) {
			listaIPSVO = Arrays.asList(consultarConsultoriosPorNombre(ipsVO.getSucursal()).getDatosIPS());
		} else {
			throw new LogicException(Messages.getValorExcepcion(ConstantesEJB.ERROR_VALIDACION_DATOS_REQUERIDOS_CONSULTA_IPS), ErrorType.PARAMETRO_ERRADO);
		}

		return listaIPSVO;
	}
	

	private boolean validarCodigoInterno(IPSVO ipsVO) {
		return ipsVO != null && ipsVO.getCodigoInterno() != null && !ipsVO.getCodigoInterno().trim().isEmpty();
	}

	private boolean validarSucursal(IPSVO ipsVO) {
		return ipsVO != null && ipsVO.getSucursal() != null && !ipsVO.getSucursal().trim().isEmpty();
	}

	private boolean validarIpsVO(IPSVO ipsVO) {
		return ipsVO != null && ipsVO.getConsecCodigoTipoId() > 0 && ipsVO.getNumeroIdentificacion() != null && !ipsVO.getNumeroIdentificacion().trim().isEmpty();
	}

}
