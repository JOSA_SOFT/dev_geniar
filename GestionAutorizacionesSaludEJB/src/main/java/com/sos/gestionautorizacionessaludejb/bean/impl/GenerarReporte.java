package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.caja.InformeCierreCajaVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReciboCajaJasperVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ReporteNotaCreditoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaludejb.bean.IGenerarReporteJasper;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.JasperGenerator;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.sosjasper.generador.FormatType;
import com.sos.sosjasper.generador.ReportsManager;

/**
 * Class GenerarReporte Clase que implementa IGenerarReporteJasper
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 26/04/2016
 *
 */
@Stateless
public class GenerarReporte implements IGenerarReporteJasper {

	/** Ruta del archivo en donde queda guardado el reporte jasper */
	private String dirJasper;
	private ByteArrayOutputStream byteOutPutStream = null;

	/** Datasource que se le pasara al jasper para generar el reporte */
	private ConnectionProvider connProviderSalud;

	public GenerarReporte() throws LogicException {
		if (connProviderSalud == null) {
			try {
				connProviderSalud = ConnProviderSiSalud.getConnProvider();
			} catch (ConnectionProviderException e) {
				throw new LogicException(
						Messages.getValorError(ConstantesEJB.ERROR_CONEXION_BD),
						e, LogicException.ErrorType.DATO_NO_EXISTE);
			}
		}
	}

	/**
	 * Este método permite generar el reporte de OPS, teniendo en cuenta una sola OPS
	 * @param params: Representa los parámetros del reporte
	 * @param tipo: Representa el tipo de Archivo, debe ser PDF 	 
	 * @return string: Ruta del archivo que contiene el archivo generado
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteAutorizacionServicioPDF(
			Map<String, Object> params, FormatType tipo) throws LogicException {

		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();

		// Se extrae la ruta compartida donde se encuentra la parametrizacion de
		// los reportes
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		// Se extrae la ruta donde se encuentra el jasper a utilizar
		String propiedadJasper = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_OPS);
		dirJasper = dirJasper + obtenerRutaReporteJasper(propiedadJasper);

		try {

			ReportsManager reportsManager = new ReportsManager(dirJasper,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);

		} catch (SQLException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return byteOutPutStream;
	}

	/**
	 * Este método genera un reporte jasper, utilizando la plantilla pasada como par�metro.
	 * @param params: Representa los parámetros del reporte
	 * @param tipo: Representa el tipo de Archivo, debe ser ZIP	 
	 * @return string: Ruta del archivo que contiene el archivo generado
	 */
	@SuppressWarnings("rawtypes")
	public File generarReporteAutorizacionServicioMasivo(
			Map<String, Object> params, FormatType tipo, String path,
			String generatedFilesUrl) throws LogicException {

		File file = new File(generatedFilesUrl);
		if (!file.exists()) {
			file.mkdir();
		}

		//Se extrae la ruta compartida donde se encuentra la parametrizacion de los reportes
		String propiedadRutaRecursos = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		// Se extrae la ruta donde se encuentra el jasper a utilizar
		String propiedadJasper = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_OPS);
		dirJasper = dirJasper + obtenerRutaReporteJasper(propiedadJasper);

		try {

			ReportsManager reportsManager = new ReportsManager(dirJasper,
					params, connProviderSalud.obtenerConexion());
			return reportsManager.getReport(path, tipo);

		} catch (DataAccessException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

	}

	/**
	 * Este método permite generar el reporte de devolucion
	 * @param params: Representa los parámetros del reporte
	 * @param tipo: Representa el tipo de Archivo, debe ser PDF
	 * @return string: Ruta del archivo que contiene el archivo generado
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteDevolucionPDF(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();

		// Se extrae la ruta compartida donde se encuentra la parametrizacion de
		// los reportes
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		// Se extrae la ruta donde se encuentra el jasper a utilizar
		String propiedadJasper = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_DEVOLUCION);
		dirJasper = dirJasper + obtenerRutaReporteJasper(propiedadJasper);

		try {
			ReportsManager reportsManager = new ReportsManager(dirJasper,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);

		} catch (SQLException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return byteOutPutStream;
	}

	/**
	 * Este método permite generar el reporte de negacion
	 * @param params: Representa los parámetros del reporte
	 * @param tipo: Representa el tipo de Archivo, debe ser PDF	 
	 * @return string: Ruta del archivo que contiene el archivo generado
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteNegacionPDF(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();

		//Se extrae la ruta compartida donde se encuentra la parametrizacion de los reportes
		String propiedadRutaRecursos = ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);
		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		// Se extrae la ruta donde se encuentra el jasper a utilizar
		String propiedadJasper = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_NEGACION);
		dirJasper = dirJasper + obtenerRutaReporteJasper(propiedadJasper);

		try {
			ReportsManager reportsManager = new ReportsManager(dirJasper,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);

		} catch (SQLException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE_OPS),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return byteOutPutStream;
	}

	/**
	 * Metodo que consulta la ruta del jasper
	 * 
	 * @throws LogicException
	 */
	public String obtenerRutaReporteJasper(String propiedadAplicacion)
			throws LogicException {

		String pathJasper;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties
				.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_RECURSOS));
		propiedad.setPropiedad(propiedadAplicacion);

		try {
			pathJasper = propertyQuery.consultarPropiedad(propiedad);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return pathJasper;
	}

	/**
	 * Invocar response zip.
	 * 
	 * @param httpServletResponse
	 *            : the http servlet response
	 * @param zip
	 *            : the zip
	 * @throws LogicException
	 */
	public void descargaFileZip(HttpServletResponse httpServletResponse,
			byte[] zip, String nombreArchivo) throws LogicException {
		String extArchivoZip = ConstantesEJB.ARCHIVO_ZIP;

		try {

			httpServletResponse.setContentType(ConstantesEJB.ZIP);
			httpServletResponse.setHeader(ConstantesEJB.CONTENT,
					ConstantesEJB.ATTACHMENT
							+ ConstantesEJB.SEPARADOR_ATTACHMENT
							+ nombreArchivo + extArchivoZip);
			httpServletResponse.getOutputStream().write(zip);
			httpServletResponse.getOutputStream().flush();

		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Invocar response pdf.
	 * 
	 * @param httpServletResponse
	 *            : the http servlet response
	 * @param byteOutPutStream
	 *            : the byte out put stream
	 * @throws LogicException
	 */
	public void descargaFilePdf(HttpServletResponse httpServletResponse,
			ByteArrayOutputStream byteOutPutStream, String nombreArchivo)
			throws LogicException {

		String extArchivoPdf = ConstantesEJB.ARCHIVO_PDF;

		try {
			httpServletResponse.setContentType(ConstantesEJB.PDF);
			httpServletResponse.setHeader(ConstantesEJB.CONTENT,
					ConstantesEJB.ATTACHMENT + nombreArchivo + extArchivoPdf);
			httpServletResponse.setHeader(ConstantesEJB.CACHE,
					ConstantesEJB.REVALIDATE);

			httpServletResponse.setContentLength(byteOutPutStream.size());
			httpServletResponse.getOutputStream().write(
					byteOutPutStream.toByteArray(), 0, byteOutPutStream.size());
			httpServletResponse.getOutputStream().flush();
			httpServletResponse.getOutputStream().close();

		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Se encarga de generar el jasper del informe de cajas.
	 * 
	 * @param params
	 * @param tipo
	 * @return
	 * @throws LogicException
	 * @throws DataAccessException
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteInformeCierreCaja(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_CIERRE_CAJA));

		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);
		try {
			ReportsManager reportsManager = new ReportsManager(rutaReporte,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);
		} catch (DataAccessException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return salidaReporte;
	}

	/**
	 * @param params
	 * @param tipo
	 * @return
	 * @throws LogicException
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteInformeDetalleCierreCaja(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();

		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS));

		String rutaSubReporteRecibos = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_RECIBOS));

		String rutaSubReporteNotas = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.RUTA_JASPER_REPORTE_DETALLE_CIERRE_CAJAS_NOTAS));

		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);
		params.put(ConstantesEJB.RUTA_SUBREPORTE_RECIBOS, rutaSubReporteRecibos);
		params.put(ConstantesEJB.RUTA_SUBREPORTE_NOTAS, rutaSubReporteNotas);

		try {
			ReportsManager reportsManager = new ReportsManager(rutaReporte,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);
		} catch (DataAccessException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return salidaReporte;
	}

	/**
	 * Metodo que generar el recibo caja en PDF
	 * 
	 * @param lista
	 * @param params
	 * @return
	 * @throws LogicException
	 */
	public ByteArrayOutputStream generarReciboCaja(
			List<ReciboCajaJasperVO> lista, Map<String, Object> params)
			throws LogicException {
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);
		
		try {
			dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

			String dirJasperRecibo = dirJasper
					+ Utilidades
							.obtenerParametroPropertyQuery(
									clasificadorRutaCajas,
									ConsultaProperties
											.getString(ConstantesEJB.PROPEDAD_RUTA_JASPER_RECIBO));

			File initialFile = new File(dirJasperRecibo);
			InputStream fileIs = new FileInputStream(initialFile);

			String rutaLogo = dirJasper
					+ Utilidades.obtenerParametroPropertyQuery(
							clasificadorRutaCajas,
							ConsultaProperties
									.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

			params.put(ConstantesEJB.LOGO_URL_SOS, rutaLogo);

			return JasperGenerator.generateReport(fileIs, params, lista);
		} catch (LogicException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (FileNotFoundException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (JRException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

	}

	/**
	 * Genera la descarga de un archivo Excel
	 * 
	 * @param httpServletResponse
	 * @param byteOutPutStream
	 * @param nombreArchivo
	 * @throws LogicException
	 */
	public void descargaFileExcel(HttpServletResponse httpServletResponse,
			ByteArrayOutputStream byteOutPutStream, String nombreArchivo)
			throws LogicException {

		String extArchivoXlsx = ConstantesEJB.ARCHIVO_XLSX;

		try {
			httpServletResponse.setContentType(ConstantesEJB.ARCHIVO_XLSX);
			httpServletResponse.setHeader(ConstantesEJB.CONTENT,
					ConstantesEJB.ATTACHMENT + nombreArchivo + extArchivoXlsx);
			httpServletResponse.setHeader(ConstantesEJB.CACHE,
					ConstantesEJB.REVALIDATE);

			httpServletResponse.setContentLength(byteOutPutStream.size());
			httpServletResponse.getOutputStream().write(
					byteOutPutStream.toByteArray(), 0, byteOutPutStream.size());
			httpServletResponse.getOutputStream().flush();
			httpServletResponse.getOutputStream().close();

		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Genera reporte de nota credito
	 * 
	 * @param params
	 * @param reporteNotaCreditoVO
	 * @return
	 * @throws LogicException
	 */
	public ByteArrayOutputStream generarReporteNotaCredito(
			Map<String, Object> params,
			ReporteNotaCreditoVO reporteNotaCreditoVO) throws LogicException {
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_REPORTE_NOTA_CREDITO));

		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(new File(rutaReporte));
			List<ReporteNotaCreditoVO> listaInformacion = new ArrayList<ReporteNotaCreditoVO>();
			listaInformacion.add(reporteNotaCreditoVO);
			return JasperGenerator.generateReport(inputStream, params,
					listaInformacion);
		} catch (FileNotFoundException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (JRException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**
	 * Reporte de cierre de oficina
	 * 
	 * @param params
	 * @param tipo
	 * @return
	 * @throws LogicException
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarInformeCierreOficina(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_CIERRE_OFICINA));

		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);
		try {
			ReportsManager reportsManager = new ReportsManager(rutaReporte,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);
		} catch (DataAccessException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return salidaReporte;
	}

	/**
	 * Reporte de cierre caja
	 * 
	 * @param params
	 * @param informeCierreCajaVO
	 * @return
	 * @throws LogicException
	 * @throws FileNotFoundException
	 * @throws JRException
	 */
	public ByteArrayOutputStream generarInformeCierreCaja(
			Map<String, Object> params, InformeCierreCajaVO informeCierreCajaVO)
			throws LogicException {
		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_JASPER_INFO_CIERRE_CAJA));
		
		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(new File(rutaReporte));
			List<InformeCierreCajaVO> listaInformacion = new ArrayList<InformeCierreCajaVO>();
			listaInformacion.add(informeCierreCajaVO);
			return JasperGenerator.generateReport(inputStream, params,
					listaInformacion);

		} catch (FileNotFoundException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (JRException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

	}

	/**
	 * 
	 * @param params
	 * @param tipo
	 * @return
	 * @throws LogicException
	 */
	@SuppressWarnings("rawtypes")
	public ByteArrayOutputStream generarReporteInformeDetalleCaja(
			Map<String, Object> params, FormatType tipo) throws LogicException {
		ByteArrayOutputStream salidaReporte = new ByteArrayOutputStream();

		String propiedadRutaRecursos = ConsultaProperties
				.getString(ConstantesEJB.PROPIEDAD_URL_RECURSOS);

		String clasificadorRutaCajas = ConsultaProperties
				.getString(ConstantesEJB.CLASIFICACION_URL_CAJAS);

		dirJasper = obtenerRutaReporteJasper(propiedadRutaRecursos);

		String rutaReporte = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.RUTA_JASPER_REPORTE_CIERRE_CAJA_DETALLADO));

		String rutaLogo = dirJasper
				+ Utilidades
						.obtenerParametroPropertyQuery(
								clasificadorRutaCajas,
								ConsultaProperties
										.getString(ConstantesEJB.PROPIEDAD_RUTA_LOGO_SOS));

		params.put(ConstantesEJB.RUTA_LOGO_SOS, rutaLogo);

		try {
			ReportsManager reportsManager = new ReportsManager(rutaReporte,
					params, connProviderSalud.obtenerConexion());
			byteOutPutStream = (ByteArrayOutputStream) reportsManager
					.getReport(salidaReporte, tipo);
		} catch (DataAccessException e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_GENERAR_REPORTE),
					e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return salidaReporte;
	}
}