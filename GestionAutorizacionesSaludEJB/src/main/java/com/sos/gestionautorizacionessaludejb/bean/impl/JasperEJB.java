package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.io.File;
import java.sql.SQLException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaludejb.bean.JasperEJBLocal;
import com.sos.gestionautorizacionessaludejb.util.MessagesEJB;
import com.sos.sosjasper.generador.FormatType;
import com.sos.sosjasper.generador.ReportsManager;


/**
 * Class JasperEJB
 * @author Jerson Viveros
 *
 */
@Stateless
public class JasperEJB implements JasperEJBLocal {
	
	/** Datasource que se le pasara al jasper para generar el reporte */
    private DataSource datasource; 
    /**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(JasperEJB.class);
    
    /**
     * Se llama este metodo en el ciclo de vida para crear el datasource
     */
	@PostConstruct
    public void init(){
    	try {  
    		InitialContext ctx = new InitialContext();
    		datasource = (DataSource) ctx.lookup(MessagesEJB.getJNDI("JNDI"));//Esto va por propiedades!
    	} catch (Exception e) {
    		LOG.error(e);
		}
    }
	
	
	
	/**
	 * Este m�todo genera un reporte jasper, utilizando la plantilla pasada como par�metro.
	 * 
	 * @param params
	 * 	representa los par�metros del reporte
	 * @param tipo
	 * 	Representa el tipo de Archivo, debe ser XLS, PDF o TXT
	 * @param idJasper
	 * 	Representa el nombre del archivo .jasper, como se encuentra en el disco
	 * 
	 * @return string
	 * 	Ruta del archivo que contiene el archivo generado
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public String generarReporte(Map<String, Object> params, String tipo, String idJasper)
	throws LogicException, SQLException, Exception{
		//Esto hay que cambiarlo, por el esquema que se maneja en SOS
		String dirJasper = MessagesEJB.getValorConfig("rutaJasper");
		String dirTmp = MessagesEJB.getValorConfig("rutaTmp");
		
		//Nombre del archivo de salida, se le adiciona un random, para que genere nombres diferentes y no se sobreescriban
		String nArchivo = "reporte"+((int)(Math.random()*1000))+"."+tipo;
		String rutaSalida = dirTmp+File.separator+nArchivo;
		String rutaJasper = dirJasper+File.separator+idJasper;
		ReportsManager reportsManager = new ReportsManager(rutaJasper, params, datasource.getConnection());
		File out  = reportsManager.getReport(rutaSalida, FormatType.valueOf(tipo));
		return rutaSalida;
	}
	
	
	
	
	
	

}
