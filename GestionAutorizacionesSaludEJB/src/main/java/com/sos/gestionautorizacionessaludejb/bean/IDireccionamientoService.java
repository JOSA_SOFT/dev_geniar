package com.sos.gestionautorizacionessaludejb.bean;

import co.com.sos.consultarresultadodireccionamiento.v1.ConsultarResultadoDireccionamientoResType;
import co.com.sos.ejecutardireccionamiento.v1.EjecutarDireccionamientoResType;
import co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;


/**
 * @type: Interface
 * @name: IDireccionamientoService
 * @description: Interface encargada de exponer los metodos para el direccionamiento de la solictud
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/03/2016
 */
public interface IDireccionamientoService {
	
	/** Metodo que inicia el servicio de direccionamiento*/	
	public DireccionamientoService crearDireccionamientoService() throws LogicException;
	
	/** Metodo que ejecuta el servicio de direccionamiento */
	public EjecutarDireccionamientoResType ejecutarDireccionamiento(co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;

	/** Metodo que consulta el resultado del servicio de direccionamiento 
	 * @throws LogicException */
	public DireccionamientoSolicitudVO consultaResultadoDireccionamiento(EjecutarDireccionamientoResType ejecutarDireccionamientoResType, ConsultarResultadoDireccionamientoResType consultarResultadoDireccionamientoResType)  throws ServiceException, LogicException;
	
	/** Metodo que consulta el metodo de consulta direccionamiento */
	public ConsultarResultadoDireccionamientoResType consultaDireccionamiento(co.com.sos.enterprise.direccionamiento.v1.DireccionamientoService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;

}
