package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.sos.dataccess.SOSDataAccess;
import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaluddata.delegate.ConsultarDocumentosSoportePrestacionesDelegate;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaludejb.bean.DocumentoSoporteEJBLocal;
import com.sos.gestionautorizacionessaludejb.bean.DocumentoSoporteEJBRemote;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderBDSeguridad;

/**
 * Implementacion de operaciones sobre documento soporte.
 *
 * @author camoreno
 */
@Stateless
public class DocumentoSoporteEJB implements DocumentoSoporteEJBLocal, DocumentoSoporteEJBRemote {

	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.sos.portaltransaccional.logic.ejb.bean.IDocumentoSoporteEJB#
	 * consultarDocumentoSoporte(java.lang.Integer)
	 */
	/* (non-Javadoc)
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.DocumentoSoporteEJBLocal#consultarDocumentoSoporte(java.lang.Integer)
	 */
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<DocumentoSoporteVO> consultarDocumentoSoporte(
			Integer consecutivoPrestacion) throws ServiceException {
		List<DocumentoSoporteVO> resultado = null;
		try {
			ConsultarDocumentosSoportePrestacionesDelegate spConsultaDocumentoSoporte = new ConsultarDocumentosSoportePrestacionesDelegate(
					consecutivoPrestacion);
			SOSDataAccess.ejecutarSQL(ConnProviderBDSeguridad
					.getConnProvider(), spConsultaDocumentoSoporte);
			resultado = spConsultaDocumentoSoporte.getResultado();
			return resultado != null && !resultado.isEmpty() ? resultado : null;

		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e,
					DocumentoSoporteEJB.class);
		}
	}
}
