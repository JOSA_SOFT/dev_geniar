package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.ServiceException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaludejb.bean.MedicoEJBLocal;
import com.sos.gestionautorizacionessaludejb.bean.MedicoEJBRemote;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.EJBCaller;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.medico.MedicoService;
import com.sos.medico.MedicoServiceEJB2Home;
import com.sos.medico.MedicoServiceEJB2Remote;
import com.sos.medico.modelo.ListaMedicoResponse;
import com.sos.medico.modelo.MedicoResponse;
import com.sos.medico.modelo.MedicoVO;

@Stateless
public class MedicoEJB implements MedicoEJBLocal, MedicoEJBRemote {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.MedicoEJBLocal#
	 * consultaredicoAdscritoNroUnico()
	 */
	private static final String MEDICO_SERVICE = ConstantesEJB.MEDICO_SERVICE;

	public MedicoResponse consultarMedicoAdscritoNroUnico(Long numeroUnico) throws ServiceException {
		try {
			MedicoServiceEJB2Home medHome = EJBCaller.invoke(Messages.getValorParametro(MEDICO_SERVICE));
			MedicoServiceEJB2Remote medService = medHome.create();
			return medService.buscarMedicoAdscritoNumeroUnico(numeroUnico);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, MedicoEJB.class);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.MedicoEJBLocal#
	 * consultarMedicoPorId()
	 */

	public ListaMedicoResponse consultarMedicoPorId(long consecutivoTipoId, String numeroIdentificacion) throws ServiceException {
		try {
			MedicoService medService = EJBCaller.invoke(Messages.getValorParametro(MEDICO_SERVICE));
			return medService.buscarMedicoPorIdentificacionVigentes(consecutivoTipoId, numeroIdentificacion, Calendar.getInstance().getTime());
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, MedicoEJB.class);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.MedicoEJBLocal#
	 * consultarMedicoPorNombre()
	 */

	public ListaMedicoResponse consultarMedicoPorNombre(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido) throws ServiceException {
		try {
			MedicoService medService = EJBCaller.invoke(Messages.getValorParametro(MEDICO_SERVICE));
			return medService.buscarMedicoPorNombreVigentes(primerNombre, segundoNombre, primerApellido, segundoApellido, Calendar.getInstance().getTime());
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, MedicoEJB.class);
		}
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sos.gestionautorizacionessaludejb.bean.impl.MedicoEJBLocal#
	 * consultarMedicoPorRegistro()
	 */

	public ListaMedicoResponse consultarMedicoPorRegistro(String registroMedico) throws ServiceException {
		try {
			MedicoService medService = EJBCaller.invoke(Messages.getValorParametro(MEDICO_SERVICE));
			return medService.buscarMedicoPorRegistroVigentes(registroMedico, Calendar.getInstance().getTime());
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e, MedicoEJB.class);
		}
	}

	public List<MedicoVO> consultarMedicoPorParametros(MedicoVO medicovo) throws LogicException, ServiceException {
		List<MedicoVO> listaMedicosEncontrados = new ArrayList<MedicoVO>();
		MedicoVO medicoVO = new MedicoVO();
		medicoVO = Utilidades.quitarEspaciosMedico(medicovo);
		
		if (validarRegistroMedico(medicoVO)) {
			listaMedicosEncontrados = Arrays.asList(consultarMedicoPorRegistro(medicoVO.getRegistroMedico().trim()).getDatosMedico());
		} else if (medicoVO != null && medicoVO.getConsecCodTipoId() > 0 && medicoVO.getNumeroIdentificacion() != null && !medicoVO.getNumeroIdentificacion().trim().isEmpty()) {
			listaMedicosEncontrados = Arrays.asList(consultarMedicoPorId(medicoVO.getConsecCodTipoId(), medicoVO.getNumeroIdentificacion().trim()).getDatosMedico());
		} else if (validarNombres(medicoVO)
						|| validarCamposVacios(medicoVO.getPrimerApellido()) || validarCamposVacios(medicoVO.getSegundoApellido())) {
			listaMedicosEncontrados = Arrays.asList(consultarMedicoPorNombre(medicoVO.getPrimerNombre(), medicoVO.getSegundoNombre(), medicoVO.getPrimerApellido(), medicoVO.getSegundoApellido())
					.getDatosMedico());
		} else {
			throw new LogicException("Para realizar la búsqueda debe ingresar el tipo y número de identificación o el registro médico o el nombre del médico", ErrorType.PARAMETRO_ERRADO);
		}
		return listaMedicosEncontrados;
	}

	private boolean validarRegistroMedico(MedicoVO medicoVO) {
		return medicoVO != null && medicoVO.getRegistroMedico() != null && !medicoVO.getRegistroMedico().trim().isEmpty();
	}

	private boolean validarNombres(MedicoVO medicoVO) {
		return validarCamposVacios(medicoVO)
				&& validarCamposVacios(medicoVO.getPrimerNombre()) || validarCamposVacios(medicoVO.getSegundoNombre());
	}
	
	private boolean validarCamposVacios(Object object){
		if(object instanceof String){
			String cadena = (String)object;
			return cadena != null && !cadena.trim().isEmpty();
		}
			return object!= null;
	}

}
