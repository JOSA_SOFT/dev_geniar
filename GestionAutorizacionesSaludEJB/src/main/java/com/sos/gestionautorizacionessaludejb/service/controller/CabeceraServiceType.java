package com.sos.gestionautorizacionessaludejb.service.controller;

import co.com.sos.DestinoType;
import co.com.sos.DestinosType;
import co.com.sos.OrigenType;
import co.com.sos.RequestCabeceraType;
import co.com.sos.SeguridadType;

import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;


/**
 * Class CabeceraServiceType
 * Clase que setea la informacion de la cabecera del servicio
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class CabeceraServiceType {	
	private String usuarioSession;
	
	public CabeceraServiceType(String usuarioSession){		
		this.usuarioSession = usuarioSession;
	}
	
	/* Informacion de la cabecera */
	public RequestCabeceraType informacionCabeceraServiceType(){
		
		/* se adiciona la informacion de la cabecera */
		RequestCabeceraType requestCabeceraType = new RequestCabeceraType();
		requestCabeceraType.setTransaccion(ConstantesEJB.TIPO_TRANSACCION_SOLICITUD);
		requestCabeceraType.setMensaje(ConstantesEJB.CADENA_VACIA);
		requestCabeceraType.setIdMensaje(ConstantesEJB.CADENA_VACIA);
		
		OrigenType origenType = new OrigenType();
		origenType.setCodigo(ConstantesEJB.CADENA_VACIA);
		origenType.setNombre(ConstantesEJB.CADENA_VACIA);
		requestCabeceraType.setOrigen(origenType);
		
		DestinosType destinosType = new DestinosType();
		
		DestinoType destinoType = new DestinoType();
		destinoType.setCodigo(ConstantesEJB.CADENA_VACIA);
		destinoType.setNombre(ConstantesEJB.CADENA_VACIA);
		destinosType.getDestino().add(destinoType);
		requestCabeceraType.setDestinos(destinosType);
		
		SeguridadType seguridadType = new SeguridadType();
		seguridadType.setClave(ConstantesEJB.CADENA_VACIA);
		seguridadType.setUsuario(usuarioSession != null ? usuarioSession.trim() : ConstantesEJB.CADENA_VACIA);
		seguridadType.setSistemaOrigen(ConstantesEJB.MEDIO_CONTACTO_ASI);
		
		requestCabeceraType.setSeguridad(seguridadType);
		/* fin informacion de la cabecera */		
		
		return requestCabeceraType;		
	}
}
