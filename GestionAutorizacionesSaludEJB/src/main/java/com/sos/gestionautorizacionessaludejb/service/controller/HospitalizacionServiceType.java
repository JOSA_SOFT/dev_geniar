package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.hospitalizacion.v1.HospitalizacionesType;
import co.com.sos.hospitalizacion.v1.IdentificadorType;
import co.com.sos.hospitalizacion.v1.TipoHabitacionType;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class PrestadorServiceType
 * Clase que setea la informacion de la hospitalizacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class HospitalizacionServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public HospitalizacionServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	public HospitalizacionesType informacionHospitalizacionServiceType(){
		
		co.com.sos.hospitalizacion.v1.HospitalizacionType hospitalizacionType = new co.com.sos.hospitalizacion.v1.HospitalizacionType();

		IdentificadorType identificadorHospitalizacionType = new IdentificadorType();
		identificadorHospitalizacionType.setConsecutivo(solicitudVO.getHospitalizacionVO().getServicioHospitalizacionVO().getConsecutivoSevicioHospitalizacion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getHospitalizacionVO().getServicioHospitalizacionVO().getConsecutivoSevicioHospitalizacion()) : BigInteger.ZERO);
		identificadorHospitalizacionType.setDescripcion(solicitudVO.getHospitalizacionVO().getServicioHospitalizacionVO().getDescripcionSevicioHospitalizacion() != null ? solicitudVO.getHospitalizacionVO().getServicioHospitalizacionVO().getDescripcionSevicioHospitalizacion().trim() : ConstantesEJB.CADENA_VACIA);
		hospitalizacionType.setIdentificador(identificadorHospitalizacionType);
		hospitalizacionType.setCama(solicitudVO.getHospitalizacionVO().getCama() != null ? solicitudVO.getHospitalizacionVO().getCama().trim() : ConstantesEJB.CADENA_VACIA);

		if (ConstantesEJB.MEDIO_CONTACTO_ASI.equals(solicitudVO.getMedioContactoVO().getCodigoMedioContacto()) || ConstantesEJB.MEDIO_CONTACTOS_AFILIADO.equals(solicitudVO.getMedioContactoVO().getCodigoMedioContacto())) {

			TipoHabitacionType tipoHabitacionType = new TipoHabitacionType();
			tipoHabitacionType.setConsecutivo(solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getConsecutivoClaseHabitacion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getConsecutivoClaseHabitacion()) : BigInteger.ZERO);
			tipoHabitacionType.setCodigo(solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getCodigoClaseHabitacion() != null ? solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getCodigoClaseHabitacion().trim() : ConstantesEJB.CADENA_VACIA);
			tipoHabitacionType.setDescripcion(solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getDescripcionClaseHabitacion() != null ? solicitudVO.getHospitalizacionVO().getTipoHabitacionVO().getDescripcionClaseHabitacion().trim() : ConstantesEJB.CADENA_VACIA);

			hospitalizacionType.setTipoHabitacion(tipoHabitacionType);
			hospitalizacionType.setFechaIngreso(Utilidades.asXMLGregorianCalendar(solicitudVO.getHospitalizacionVO().getFechaIngreso()));
			hospitalizacionType.setFechaEgreso(Utilidades.asXMLGregorianCalendar(solicitudVO.getHospitalizacionVO().getFechaEgreso()));
			hospitalizacionType.setCorteCuenta(solicitudVO.getHospitalizacionVO().getCorteCuenta() != null ? new BigInteger(solicitudVO.getHospitalizacionVO().getCorteCuenta()) : BigInteger.ZERO);
		}
		HospitalizacionesType hospitalizacionesType = new HospitalizacionesType();
		hospitalizacionesType.getHospitalizacion().add(hospitalizacionType);
				
		return hospitalizacionesType;		
	}

}
