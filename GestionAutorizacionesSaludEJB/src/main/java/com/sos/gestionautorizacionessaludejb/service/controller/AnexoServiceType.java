package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.anexo.v1.AnexoType;
import co.com.sos.anexo.v1.AnexosType;
import co.com.sos.anexo.v1.IdentificadorType;

import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;



/**
 * Class AnexoServiceType
 * Clase que setea la informacion de los anexos
 * @author ing. Victor Hugo Gil Ramos
 * @version 17/05/2016
 *
 */
public class AnexoServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public AnexoServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}	
	
	// informacion de los anexos
	public AnexosType informacionAnexosServiceType(){
		AnexosType anexosType = new AnexosType();
		AnexoType anexoType;
		IdentificadorType identificadorAnexos;
		
		for (SoporteVO soporteVO : solicitudVO.getlSoporteVO()) {
			if (soporteVO.getData() != null && soporteVO.getData().length > 0 && soporteVO.getNombreSoporte() != null && !soporteVO.getNombreSoporte().trim().isEmpty()) {
				anexoType = new AnexoType();
				identificadorAnexos = new co.com.sos.anexo.v1.IdentificadorType();
				identificadorAnexos.setConsecutivo(soporteVO.getDocumentoSoporteVO().getConsecutivoDocumento() != null ? new BigInteger(soporteVO.getDocumentoSoporteVO().getConsecutivoDocumento()
						.toString()) : null);
				identificadorAnexos.setCodigo(soporteVO.getDocumentoSoporteVO().getCodigoDocumentoSoporte());
				identificadorAnexos.setDescripcion(soporteVO.getDocumentoSoporteVO().getNombreDocumento());
				anexoType.setIdentificador(identificadorAnexos);
				anexosType.getAnexo().add(anexoType);
			}
		}
		
		return anexosType;
	}

}
