package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.solicitud.v1.ClaseAtencionType;
import co.com.sos.solicitud.v1.FormaAtencionType;
import co.com.sos.solicitud.v1.OrigenAtencionType;
import co.com.sos.solicitud.v1.PrioridadType;
import co.com.sos.solicitud.v1.TipoServicioType;
import co.com.sos.solicitud.v1.TipoUbicacionType;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class AtencionServiceType
 * Clase que setea la informacion de la atencion
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class AtencionServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public AtencionServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	// origen de la atencion
	public OrigenAtencionType origenAtencionServiceType(){
		
		OrigenAtencionType origenAtencionType = new OrigenAtencionType();
		origenAtencionType.setConsecutivo(solicitudVO.getAtencionVO().getOrigenAtencionVO().getConsecutivoOrigenAtencion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getOrigenAtencionVO().getConsecutivoOrigenAtencion()) : BigInteger.ZERO);
		origenAtencionType.setCodigo(solicitudVO.getAtencionVO().getOrigenAtencionVO().getCodigoOrigenAtencion() != null ? solicitudVO.getAtencionVO().getOrigenAtencionVO().getCodigoOrigenAtencion(): ConstantesEJB.CADENA_VACIA);
		origenAtencionType.setDescripcion(solicitudVO.getAtencionVO().getOrigenAtencionVO().getDescripcionOrigenAtencion() != null ? solicitudVO.getAtencionVO().getOrigenAtencionVO().getDescripcionOrigenAtencion() : ConstantesEJB.CADENA_VACIA);
		
		return origenAtencionType;		
	}
	
	// tipo de servicio de la atencion
	public TipoServicioType tipoServicioType(){
		
		TipoServicioType tipoServicioType = new TipoServicioType();
		tipoServicioType.setConsecutivo(solicitudVO.getAtencionVO().getTipoServicioVO().getConsecutivoTipoServicio() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getTipoServicioVO().getConsecutivoTipoServicio()) : BigInteger.ZERO);
		tipoServicioType.setDescripcion(solicitudVO.getAtencionVO().getTipoServicioVO().getDescripcionTipoServicio() != null ? solicitudVO.getAtencionVO().getTipoServicioVO().getDescripcionTipoServicio().trim() : ConstantesEJB.CADENA_VACIA);
		
		return tipoServicioType;
	}
	
	//prioridad de la atencion
	public PrioridadType prioridadServiceType(){
		PrioridadType prioridadType = new PrioridadType();
		prioridadType.setConsecutivo(solicitudVO.getAtencionVO().getPrioridadAtencionVO().getConsecutivoPrioridadAtencion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getPrioridadAtencionVO().getConsecutivoPrioridadAtencion()) : BigInteger.ZERO);
		prioridadType.setCodigo(ConstantesEJB.CADENA_VACIA);
		prioridadType.setDescripcion(solicitudVO.getAtencionVO().getPrioridadAtencionVO().getDescripcionPrioridadAtencion() != null ? solicitudVO.getAtencionVO().getPrioridadAtencionVO().getDescripcionPrioridadAtencion() : ConstantesEJB.CADENA_VACIA);
		
		return prioridadType;
	}
	
	//tipo de ubicacion del paciente
	public co.com.sos.solicitud.v1.TipoUbicacionType tipoUbicacionServiceType(){
		co.com.sos.solicitud.v1.TipoUbicacionType tipoUbicacionType = new TipoUbicacionType();
		tipoUbicacionType.setConsecutivo(solicitudVO.getAtencionVO().getUbicacionPacienteVO().getConsecutivoUbicacionPaciente() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getUbicacionPacienteVO().getConsecutivoUbicacionPaciente()) : BigInteger.ZERO);
		tipoUbicacionType.setCodigo(ConstantesEJB.CADENA_VACIA);
		tipoUbicacionType.setDescripcion(solicitudVO.getAtencionVO().getUbicacionPacienteVO().getDescripcionUbicacionPaciente() != null ? solicitudVO.getAtencionVO().getUbicacionPacienteVO().getDescripcionUbicacionPaciente().trim() : ConstantesEJB.CADENA_VACIA);
		
		return tipoUbicacionType;
	}
	
	//clase atencion
	public co.com.sos.solicitud.v1.ClaseAtencionType claseAtencionServiceType(){
		
		co.com.sos.solicitud.v1.ClaseAtencionType claseAtencionType = new ClaseAtencionType();
		claseAtencionType.setConsecutivo(solicitudVO.getAtencionVO().getClaseAtencionVO().getConsecutivoClaseAtencion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getClaseAtencionVO().getConsecutivoClaseAtencion()) : BigInteger.ZERO);
		claseAtencionType.setCodigo(solicitudVO.getAtencionVO().getClaseAtencionVO().getCodigoClaseAtencion() != null ? solicitudVO.getAtencionVO().getClaseAtencionVO().getCodigoClaseAtencion().trim() : ConstantesEJB.CADENA_VACIA);
		claseAtencionType.setDescripcion(solicitudVO.getAtencionVO().getClaseAtencionVO().getDescripcionClaseAtencion() != null ? solicitudVO.getAtencionVO().getClaseAtencionVO().getDescripcionClaseAtencion().trim() : ConstantesEJB.CADENA_VACIA);
		
		return claseAtencionType;
	}
	
	//forma atencion
	public FormaAtencionType formaAtencionServiceType(){
		FormaAtencionType formaAtencionType = new FormaAtencionType();
		formaAtencionType.setConsecutivo(solicitudVO.getAtencionVO().getClaseAtencionVO().getConsecutivoFormaAtencion() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAtencionVO().getClaseAtencionVO().getConsecutivoFormaAtencion()) : BigInteger.ZERO);
		formaAtencionType.setCodigo(solicitudVO.getAtencionVO().getClaseAtencionVO().getCodigoFormaAtencion() != null ? solicitudVO.getAtencionVO().getClaseAtencionVO().getCodigoFormaAtencion().trim() : ConstantesEJB.CADENA_VACIA);
		formaAtencionType.setDescripcion(ConstantesEJB.CADENA_VACIA);
		return formaAtencionType;
	}
}
