package com.sos.gestionautorizacionessaludejb.service.controller;

import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.servicios.ConceptosLiquidacionVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;

import co.com.sos.liquidacion.v1.ConceptosServiciosType;
import co.com.sos.liquidacion.v1.SolicitudesLiquidacionType;


/**
 * Class SolicitudesLiquidacionServiceType
 * Clase que permite consultar el resultado del servicio de liquidacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 */

public class SolicitudesLiquidacionServiceType {
	
	/*Identifica el listado de los conceptos asociados a la liquidacion */
	private List<ConceptosLiquidacionVO> lConceptosLiquidacionVO;
	private List<ConceptosServiciosType> lConceptosServiciosType = null;
	
	public LiquidacionSolicitudVO informacionLiquidacionService(List<SolicitudesLiquidacionType> lSolicitudesLiquidacionType){
		
		LiquidacionSolicitudVO liquidacionSolicitudVO = new LiquidacionSolicitudVO();
		
		for (SolicitudesLiquidacionType solicitudesLiquidacionType : lSolicitudesLiquidacionType) {
			
			liquidacionSolicitudVO.setConsecutivoSolicitud(solicitudesLiquidacionType.getCodigoSolicitud());
			liquidacionSolicitudVO.setValorTotalLiquidacion(solicitudesLiquidacionType.getValorLiquidacion().intValue());
			liquidacionSolicitudVO.setMensajeResultadoLiquidacion(solicitudesLiquidacionType.getMensajeResultado());
			liquidacionSolicitudVO.setResultadoLiquidacion(solicitudesLiquidacionType.getResultadoLiquidacion());
			
			lConceptosLiquidacionVO = new ArrayList<ConceptosLiquidacionVO>();			
			lConceptosServiciosType = solicitudesLiquidacionType.getConceptoSolicitud();
			
			for (ConceptosServiciosType conceptosServiciosType : lConceptosServiciosType) {
				
				ConceptosLiquidacionVO conceptosLiquidacionVO = new ConceptosLiquidacionVO();
				
				conceptosLiquidacionVO.setCodigoConcepto(conceptosServiciosType.getCodigoConcepto());
				conceptosLiquidacionVO.setNombreConcepto(conceptosServiciosType.getNombreConcepto());
				conceptosLiquidacionVO.setResultadoLiquidacion(conceptosServiciosType.getResultadoLiquidacion());
				conceptosLiquidacionVO.setValorLiquidacionConcepto(conceptosServiciosType.getValorLiquidacion().intValue());
								
				lConceptosLiquidacionVO.add(conceptosLiquidacionVO);
			}
			
			liquidacionSolicitudVO.setlConceptosLiquidacionVO(lConceptosLiquidacionVO);
		}
		
		return liquidacionSolicitudVO;	
	}
}
