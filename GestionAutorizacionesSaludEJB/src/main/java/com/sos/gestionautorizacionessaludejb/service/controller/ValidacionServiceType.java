package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.validacionespecial.v1.ObjectFactory;
import co.com.sos.validacionespecial.v1.ValidacionEspecialType;

import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class ValidacionServiceType
 * Clase que setea la informacion de la validacion especial
 * @author ing. Victor Hugo Gil Ramos
 * @version 23/03/2016
 *
 */
public class ValidacionServiceType {
	
	private ValidacionEspecialVO validacionEspecialVO;
	private String usuarioSession;
	
	public ValidacionServiceType(ValidacionEspecialVO validacionEspecialVO, String usuarioSession){
		this.validacionEspecialVO = validacionEspecialVO;
		this.usuarioSession = usuarioSession;
	}
	
	/** Informacion de la validacion especial  */
	public ValidacionEspecialType informacionValidacionEspecialType(){
		
		BigInteger consecutivoOficina = validacionEspecialVO.getOficinasVO() != null && validacionEspecialVO.getOficinasVO().getConsecutivoCodigoOficina() != null ? new BigInteger(validacionEspecialVO.getOficinasVO().getConsecutivoCodigoOficina().toString()) : BigInteger.ZERO;
		ObjectFactory objectFactory = new ObjectFactory();
		
		ValidacionEspecialType validacionEspecialType = new ValidacionEspecialType();
		validacionEspecialType.setConsecutivoCodigoOficina(objectFactory.createValidacionEspecialTypeConsecutivoCodigoOficina(consecutivoOficina));
		validacionEspecialType.setNumeroValidacionEspecial(objectFactory.createValidacionEspecialTypeNumeroValidacionEspecial(validacionEspecialVO.getNumeroValidacion()));
		validacionEspecialType.setFechaValidacionEspecial(Utilidades.asXMLGregorianCalendar(validacionEspecialVO.getFechaValidacion()));
		validacionEspecialType.setUsuarioValidacionEspecial(usuarioSession);
		validacionEspecialType.setAccionValidacionEspecial(Messages.getValorParametro(ConstantesEJB.ACCION_VALIDACION_ESPECIAL));
		
		return validacionEspecialType;
	}
}
