package com.sos.gestionautorizacionessaludejb.bean;

import co.com.sos.ResponseCabeceraType;
import co.com.sos.consultarresultadomalla.v1.ConsultarResultadoMallaResType;
import co.com.sos.ejecutarmalla.v1.EjecutarMallaResType;
import co.com.sos.enterprise.malla.v1.MallaService;
import co.com.sos.solicitud.v1.SolicitudesResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;


/**
 * @type: Interface
 * @name: IEjecutarMalla
 * @description: Interface encargada de ejecutar la malla de validacion
 * @author Victor Hugo Gil Ramos
 * @version 09/02/2016
 */
public interface IEjecutarMalla {
	
	/** Metodo que crea la conexion con el servicio 
	 * @throws LogicException */	
	public MallaService crearSolicitudService() throws LogicException;
	
	//Consulta del servicio
	
	/** Metodo que ejecuta el servicio de malla */
	public EjecutarMallaResType ejecutarMallaService(MallaService mallaService, SolicitudVO solicitudVO, String usuarioSession) throws ServiceException;
	
	/** Metodo que consulta el resultado de ejecucion de la malla */
	public ConsultarResultadoMallaResType consultarResultadoMalla(MallaService mallaService, SolicitudVO solicitudVO, String usuarioSession) throws ServiceException;
	
	/** Metodo que setea la cabecera del servicio */	
	public void cabeceraConsultaService(SolicitudVO solicitudVO, String usuarioSession)throws ServiceException;
	
	//Resultado del servicio
	
	/** Metodo que retorna el resultado y las inconsistencias de la ejecucion de la malla */
	public InconsistenciasVO consultarResultadoInconsistenciasMalla(EjecutarMallaResType ejecutarMallaResType, ConsultarResultadoMallaResType consultarResultadoMallaResType) throws ServiceException;
	
    /** Metodo que setea la cabecera del resultado del servicio 
     * @throws ServiceException */	
	public InconsistenciasVO cabeceraResultadoService(ResponseCabeceraType cabeceraType) throws ServiceException;
	
	/** Metodo que extrae la informacion del cuerpo del mensaje*/	
	public InconsistenciasVO resultadoSolicitudService(SolicitudesResType solicitudesResType) throws ServiceException;

}
