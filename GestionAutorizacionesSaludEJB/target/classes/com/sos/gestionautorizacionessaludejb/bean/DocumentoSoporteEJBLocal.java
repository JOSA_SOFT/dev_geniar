package com.sos.gestionautorizacionessaludejb.bean;

import java.util.List;

import javax.ejb.Local;

import com.sos.excepciones.ServiceException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;

@Local
public interface DocumentoSoporteEJBLocal {

	public List<DocumentoSoporteVO> consultarDocumentoSoporte(
			Integer consecutivoPrestacion) throws ServiceException;

}