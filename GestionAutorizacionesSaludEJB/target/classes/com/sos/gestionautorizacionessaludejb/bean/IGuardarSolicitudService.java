package com.sos.gestionautorizacionessaludejb.bean;

import java.math.BigInteger;

import co.com.sos.enterprise.solicitud.v1.SolicitudService;
import co.com.sos.grabaridentificadordocumentoanexo.v1.GrabarIdentificadorDocumentoAnexoResType;
import co.com.sos.grabarsolicitud.v1.GrabarSolicitudResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;


/**
 * @type: Interface
 * @name: IGuardarSolicitudService
 * @description: Interface encargada de exponer los metodos para guardar la solictud
 * @author Victor Hugo Gil Ramos
 * @version 05/02/2016
 */
public interface IGuardarSolicitudService {
	
	/** Metodo que ejecuta el servicio de guardar la solicitud */	
	public GrabarSolicitudResType guardarSolicitud(SolicitudService service, RegistrarSolicitudVO solicitudVO, String usuarioSession, SolicitudVO datosGuardarSolicitud, ValidacionEspecialVO validacionEspecialVO) throws ServiceException;
	
	/** Metodo que consulta el resultado del servicio de guardar */
	public SolicitudVO consultaResultadoGuardar(GrabarSolicitudResType grabarSolicitudResType, RegistrarSolicitudVO solicitudVO, SolicitudVO datosGuardarSolicitud)throws ServiceException;
		
	/** Metodo que guardar la informacion de los documentos soporte */
	public GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexos(String usuarioSession, BigInteger consecutivoSolicitud, BigInteger identificadorDocumento)throws LogicException, ServiceException;
	
	/** Metodo que inicia el servicio de guardar la solicitud*/	
	public SolicitudService crearSolicitudService() throws ServiceException;
}
