package com.sos.gestionautorizacionessaludejb.bean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;

import javax.ejb.Local;
import javax.servlet.http.HttpServletResponse;

import com.sos.excepciones.LogicException;
import com.sos.sosjasper.generador.FormatType;


@Local
public interface IGenerarReporteJasper {
	
	/**Este método permite generar el reporte de OPS para una solicitud**/
	public ByteArrayOutputStream generarReporteAutorizacionServicioPDF(Map<String, Object> params, FormatType tipo)throws LogicException;
	
	/**Este método permite generar el reporte para varias OPS**/
	public File generarReporteAutorizacionServicioMasivo(Map<String, Object> params, FormatType tipo, String path, String generatedFilesUrl) throws LogicException;
	
	/**Metodo que permite genera el formato de devolucion */
	public ByteArrayOutputStream generarReporteDevolucionPDF(Map<String, Object> params, FormatType tipo) throws LogicException;
	
	/**Metodo que permite genera el formato de negacion */
	public ByteArrayOutputStream generarReporteNegacionPDF(Map<String, Object> params, FormatType tipo) throws LogicException;
	
	/**Este método permite descargar los reportes en .zip**/
	public void descargaFileZip(HttpServletResponse httpServletResponse, byte[] zip, String nombreArchivo) throws LogicException;
	
	/**Este método permite descargar los reportes en pdf**/
	public void descargaFilePdf(HttpServletResponse httpServletResponse, ByteArrayOutputStream byteOutPutStream, String nombreArchivo) throws LogicException;
}
