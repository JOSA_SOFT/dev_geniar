package com.sos.gestionautorizacionessaludejb.bean;

import co.com.sos.consultarresultadofechaentrega.v1.ConsultarResultadoFechaEntregaResType;
import co.com.sos.ejecutarfechaentrega.v1.EjecutarFechaEntregaResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.FechaEntregaSolicitudVO;


/**
 * @type: Interface
 * @name: IFechaEntregaService
 * @description: Interface encargada de exponer los metodos para la fecha de entrega de la solictud
 * @author Ing. Victor Hugo Gil Ramos
 * @version 31/03/2016
 */
public interface IFechaEntregaService {
	
	/** Metodo que inicia el servicio de fecha de entrega*/	
	public co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService crearFechaEntregaService() throws LogicException;
	
	/** Metodo que ejecuta el servicio de fecha de entrega */
	public EjecutarFechaEntregaResType ejecutarFechaEntregaService (co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;
	
	/** Metodo que consulta el resultado del servicio de fecha de entrega */
	public FechaEntregaSolicitudVO consultaResultadoFechaEntrega(EjecutarFechaEntregaResType ejecutarFechaEntregaResType, ConsultarResultadoFechaEntregaResType consultarResultadoFechaEntregaResType) throws ServiceException;
	
	/** Metodo que consulta el metodo de consulta de fecha de entrega */
	public ConsultarResultadoFechaEntregaResType consultaFechaEntrega(co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;
}
