package com.sos.gestionautorizacionessaludejb.bean;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.visos.service.to.containers.IndicesContainer;
import com.sos.visos.service.vo.Indice;

/**
 * @type: Interface
 * @name: ControladorConsultaVisosServiceEJBLocal
 * @description: Interface encargada de exponer los servicios que presenta el
 *               Servicio de Visos: VisosService para ser consumidos por el
 *               cliente
 * @author Victor Hugo Gil Ramos
 * @version 16/01/2015
 */

@Remote
public interface ControladorVisosServiceEJBRemote extends Serializable {

	/** Metodo que permite consultar los documentos anexos, desde visos service */
	public abstract List<DocumentoSoporteVO> consultarDocumentoAnexoxIndices(IndicesContainer container) throws LogicException;

	/**
	 * Metodo que permite consultar los documentos anexos, desde visos service x
	 * id documento
	 */
	public List<DocumentoSoporteVO> consultarDocumentoAnexoxIdDocumento(long idDocumento) throws LogicException;

	/**
	 * Metodo que permite guardar los documentos soporte utilizando el servicio
	 * de visos service
	 */
	public abstract long guardarDocumentosSoporte(List<SoporteVO> lDocumentoSoporte, Indice[] indices, String digitalizador, String tipoDocumento, Integer consecutivoSolicitud) throws LogicException;

	/** Metodo que crea la conexion con el servicio de visos service */
	public abstract void crearConexionVisosService() throws LogicException;
}
