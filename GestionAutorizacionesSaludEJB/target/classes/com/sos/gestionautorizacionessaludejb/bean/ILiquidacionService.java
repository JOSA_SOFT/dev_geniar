package com.sos.gestionautorizacionessaludejb.bean;

import co.com.sos.liquidacion.v1.EnvioLiquidacionResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;

/**
 * @type: Interface
 * @name: ILiquidacionService
 * @description: Interface encargada de exponer los metodos para la liquidacion de la solictud
 * @author Ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 */
public interface ILiquidacionService {
	
	/** Metodo que inicia el servicio de liquidacion*/
	public co.com.sos.liquidacion.v1.LiquidacionService crearLiquidacionService() throws LogicException;
	
	/** Metodo que ejecuta el servicio de liquidacion */
	public EnvioLiquidacionResType obtenerLiquidacionService(co.com.sos.liquidacion.v1.LiquidacionService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException;
	
	/** Metodo que consulta el resultado del servicio de liquidacion */
	public LiquidacionSolicitudVO consultaResultadoLiquidacion(EnvioLiquidacionResType liquidacionResType) throws ServiceException;	
}
