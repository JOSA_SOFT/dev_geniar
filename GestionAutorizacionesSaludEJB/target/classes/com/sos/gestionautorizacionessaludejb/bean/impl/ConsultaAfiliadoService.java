package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sos.validador.bean.resultado.DatosConvenioCapitacion;
import sos.validador.bean.resultado.DatosEmpleador;
import sos.validador.bean.resultado.DatosNombreAfiliado;
import sos.validador.bean.resultado.DatosServicioCapitadoCaja;
import sos.validador.bean.resultado.ResultadoConsultaAfiliadoDatosAdicionales;
import sos.validador.bean.resultado.ResultadoConsultaAfiliadoPorNombre;
import sos.ws.validador.consulta.ConsultaValidadorWebService_SEI;
import sos.ws.validador.util.ValidadorServiceFactory;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ActividadEconomicaVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CargoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ConveniosCapitacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.DatosAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EmpleadorVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.EntidadVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ServiciosCapitadosVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.TipoCotizanteVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaludejb.bean.IConsultaAfiliadoService;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class ConsultaAfiliadoService
 * Clase contraoladora que implementa la consulta de los empleados
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/01/2016
 *
 */

public class ConsultaAfiliadoService implements IConsultaAfiliadoService {
	
	/** Lista que se utiliza almacenar la informacion del afiliado*/	
	List<AfiliadoVO> lConsultaAfiliado;
	/** Lista que se utiliza almacenar la informacion del empleador del afiliado*/
	List<EmpleadorVO> lConsultaEmpleadores;
	/** Lista que se utiliza almacenar la informacion del detalle de los servicios capitados del afiliado*/
	List<ServiciosCapitadosVO> lServiciosCapitados;
	/** Lista que se utiliza almacenar la informacion de los datos adicionales del afiliado*/
	List<DatosAfiliadoVO> lDatosAdicionalesAfiliado;
	/** Lista que se utiliza almacenar la informacion del afiliado buscado por nombre*/	
	List<AfiliadoVO> lConsularAfiliadoxNombre;
	/** Lista que se utiliza almacenar la informacion de los servicios capitados del afiliado*/
	List<ConveniosCapitacionVO> lConveniosCapitados;
	
	
	/** Metodo que consume el servicio del afiliado 
	 * @throws Exception */	
	
	public ResultadoConsultaAfiliadoDatosAdicionales consultaRespuestaServicioAfiliado(String codigoTipoIdentificacion, String numeroIdentificacion, String descripcionPlan, Date fechaConsulta, String usuarioConsulta) throws LogicException {
		
		String fechaConsultaAfiliado = Utilidades.obtenerStringDeDate(fechaConsulta, "", false);
		ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado = null;
		ConsultaValidadorWebService_SEI sistemaConsultaWS;    
		
		try {
			
			sistemaConsultaWS = ValidadorServiceFactory.getValidadorService();
			consultaAfiliado = sistemaConsultaWS.getConsultaAfiliadoDatosAdicionales(codigoTipoIdentificacion, numeroIdentificacion, descripcionPlan, fechaConsultaAfiliado, usuarioConsulta);
		
		} catch (RemoteException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SERVICE), e, ErrorType.INDEFINIDO);				
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SERVICE), e, ErrorType.INDEFINIDO);	
		}		
				
		return consultaAfiliado;
	}

	
	/** Metodo que consulta la informacion del afiliado 
	 * @throws LogicException 
	 * @throws ParseException */
	
	public AfiliadoVO consultaInformacionAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException{
		
		GeneroVO genero = new GeneroVO();
		CiudadVO ciudadVO = new CiudadVO();		
		PlanVO planComplementario = new PlanVO();
		PlanVO planAfiliado = new PlanVO();
		AfiliadoVO afiliadoVO = new AfiliadoVO();
		TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
		
		tiposIdentificacionVO.setConsecutivoTipoIdentificacion(consultaAfiliado.getAfiliado().getConsecutivoTipoIdentificacion());
		tiposIdentificacionVO.setCodigoTipoIdentificacion(consultaAfiliado.getAfiliado().getTipoidentificacion());
		
		afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
		afiliadoVO.setNumeroIdentificacion(consultaAfiliado.getAfiliado().getNumeroIdentificacion());
		afiliadoVO.setPrimerNombre(consultaAfiliado.getAfiliado().getPrimerNombre());
		afiliadoVO.setSegundoNombre(consultaAfiliado.getAfiliado().getSegundoNombre());
		afiliadoVO.setPrimerApellido(consultaAfiliado.getAfiliado().getPrimerApellido());
		afiliadoVO.setSegundoApellido(consultaAfiliado.getAfiliado().getSegundoApellido());
		
		afiliadoVO.setNombreCompleto(afiliadoVO.getPrimerNombre() + " " + afiliadoVO.getSegundoNombre() + " " + afiliadoVO.getPrimerApellido() + " " + afiliadoVO.getSegundoApellido());
		
		afiliadoVO.setEdadAnos(consultaAfiliado.getAfiliado().getEdadAnos());
		afiliadoVO.setEdadMeses(consultaAfiliado.getAfiliado().getEdadMeses());
		afiliadoVO.setEdadDias(consultaAfiliado.getAfiliado().getEdadDias());
		
		
		genero.setCodigoGenero(consultaAfiliado.getAfiliado().getSexo());
		genero.setConsecutivoGenero(consultaAfiliado.getAfiliado().getConsecutivoSexo());
		
		afiliadoVO.setGenero(genero);
		
		afiliadoVO.setDireccionResidencia(consultaAfiliado.getAfiliado().getDireccion());
		afiliadoVO.setTelefono(consultaAfiliado.getAfiliado().getTelefono());
		
		ciudadVO.setCodigoCiudad(consultaAfiliado.getAfiliado().getCodigoCiudad());
		ciudadVO.setDescripcionCiudad(consultaAfiliado.getAfiliado().getDescripcionCiudad());
		
		afiliadoVO.setCiudadResidencia(ciudadVO);		
		afiliadoVO.setDescripcionDepartamento(consultaAfiliado.getAfiliado().getDepartamento());
		
		afiliadoVO.setEmail(consultaAfiliado.getAfiliado().getEmail());
		afiliadoVO.setConsecutivoEstadoCivil(Integer.parseInt(consultaAfiliado.getAfiliado().getCodigoEstadoCivil().trim()));
		afiliadoVO.setDescripcionEstadoCivil(consultaAfiliado.getAfiliado().getDescripcionEstadoCivil());		
		afiliadoVO.setConsecutivoTipoAfiliado(consultaAfiliado.getAfiliado().getConsecutivoTipoAfiliado());
		afiliadoVO.setDescripcionTipoAfiliado(consultaAfiliado.getAfiliado().getTipoAfiliado());		
		afiliadoVO.setConsecutivoEstadoAfiliado(consultaAfiliado.getAfiliado().getConsecutivoEstadoAfiliado());
		afiliadoVO.setDescripcionEstadoAfiliado(consultaAfiliado.getAfiliado().getEstado());
		
		afiliadoVO.setConsecutivoCausaDerecho(consultaAfiliado.getAfiliado().getConsecutivoCausaDerecho());
		afiliadoVO.setDescripcionEstadoDerecho(consultaAfiliado.getAfiliado().getDerecho());
		
		planAfiliado.setConsectivoPlan(consultaAfiliado.getAfiliado().getConsecutivoCodigoPlan());
		planAfiliado.setDescripcionPlan(consultaAfiliado.getAfiliado().getPlan());
		afiliadoVO.setPlan(planAfiliado);
		
		planComplementario.setConsectivoPlan(consultaAfiliado.getAfiliado().getConsecutivoPlanPac());
		planComplementario.setDescripcionPlan(consultaAfiliado.getAfiliado().getPlanComplementario());		
		afiliadoVO.setPlanComplementario(planComplementario);
		
		afiliadoVO.setConsecutivoRangoSalarial(consultaAfiliado.getAfiliado().getConsecutivoRangoSalarial());
		afiliadoVO.setDescripcionRangoSalarial(consultaAfiliado.getAfiliado().getRangoSalarial());		
		afiliadoVO.setConsecutivoParentesco(consultaAfiliado.getAfiliado().getConsecutivoParentesco());
		afiliadoVO.setDescripcionParentesco(consultaAfiliado.getAfiliado().getParentesco());		
		afiliadoVO.setCodigoIPSPrimaria(consultaAfiliado.getAfiliado().getCodigoIpsPrimaria());
		afiliadoVO.setDescripcionIPSPrimaria(consultaAfiliado.getAfiliado().getDescripcionIpsPrimaria());
		afiliadoVO.setDesIPSPrimaria(afiliadoVO.getCodigoIPSPrimaria() + " - " + afiliadoVO.getDescripcionIPSPrimaria());
				
		afiliadoVO.setSemanasCotizadas(consultaAfiliado.getAfiliado().getSemanasCotizacion());
		
		afiliadoVO.setSemanasAfiliacionPOSAnt(consultaAfiliado.getAfiliado().getSemanasAfiliacionPosAnterior());
		afiliadoVO.setSemanasAfiliacionPOSSOS(consultaAfiliado.getAfiliado().getSemanasAfiliacionPosSOS());
		
		afiliadoVO.setSemanasAfiliacionPACAnt(consultaAfiliado.getAfiliado().getSemanasAfiliacionPCAnterior());
		afiliadoVO.setSemanasAfiliacionPACSOS(consultaAfiliado.getAfiliado().getSemanasAfiliacionPCSOS());
		
		if(Integer.parseInt(consultaAfiliado.getAfiliado().getOrigen()) == 1){
			afiliadoVO.setOrigen(ConstantesEJB.ORIGEN_CONSULTA_CONTRATOS);
		}else if(Integer.parseInt(consultaAfiliado.getAfiliado().getOrigen()) == 2)	{
			afiliadoVO.setOrigen(ConstantesEJB.ORIGEN_CONSULTA_FORMULARIO);
		}else{
			afiliadoVO.setOrigen(ConstantesEJB.ORIGEN_CONSULTA_FAMISANAR);
		}
		
		afiliadoVO.setNumeroUnicoIdentificacion(consultaAfiliado.getAfiliado().getNui());
		afiliadoVO.setConsecutivoTipoContrato(consultaAfiliado.getAfiliado().getConsecutivoTipoContrato());
		afiliadoVO.setNumeroContrato(consultaAfiliado.getAfiliado().getNumeroContrato());
		afiliadoVO.setTutela((consultaAfiliado.getAfiliado().getTutela() == 0)? ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_COMPLETO_NO: ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_COMPLETO);
		
		
		try{
			afiliadoVO.setFechaNacimiento(Utilidades.obtenerDateDeString(consultaAfiliado.getAfiliado().getFechaNacimiento(), ConstantesEJB.CADENA_VACIA));
			afiliadoVO.setFechaInicioVigencia(Utilidades.obtenerDateDeString(consultaAfiliado.getAfiliado().getInicioVigencia(), ConstantesEJB.CADENA_VACIA));
			afiliadoVO.setFechaFinVigencia(Utilidades.obtenerDateDeString(consultaAfiliado.getAfiliado().getFinVigencia(), ConstantesEJB.CADENA_VACIA));
		}catch(Exception e){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_PARSE), e, ErrorType.INDEFINIDO);
		}
		
		return afiliadoVO;
	}

	/** Metodo que mapea los campos del servicio del afiliado para los empleados*/
	
	public List<EmpleadorVO> consultaInformacionEmpleadorAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException {
		
		lConsultaEmpleadores = new ArrayList<EmpleadorVO>();		
		DatosEmpleador[] datosEmpleadorAfiliado = consultaAfiliado.getEmpleadores();
			 
		for (DatosEmpleador datosEmpleador : datosEmpleadorAfiliado) {
			 TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();
			 ActividadEconomicaVO actividadEconomicaVO = new ActividadEconomicaVO();
			 EntidadVO entidadVO = new EntidadVO();
			 CargoVO cargoVO = new CargoVO();
			 TipoCotizanteVO tipoCotizanteVO  = new TipoCotizanteVO();
			 EmpleadorVO empleadorVO = new EmpleadorVO();
			 
			 tiposIdentificacionVO.setCodigoTipoIdentificacion(datosEmpleador.getTipoIdEmpleador());			 
			 empleadorVO.setTiposIdentificacionVO(tiposIdentificacionVO);
			 empleadorVO.setNumeroIdentificacionEmpleador(datosEmpleador.getNumeroIdEmpleador());
			 empleadorVO.setRazonSocialEmpleador(datosEmpleador.getRazonSocial());
			 empleadorVO.setConsecutivoSucursalAportante(datosEmpleador.getSucursal());
			 		 			
			 actividadEconomicaVO.setCodigoEntidad(datosEmpleador.getCodigoActividadEconomica());
			 actividadEconomicaVO.setDescripcionEntidad(datosEmpleador.getDescripcionActividadEconomica());
			 
			 empleadorVO.setActividadEconomicaVO(actividadEconomicaVO);
			 
			 entidadVO.setCodigoEntidad(datosEmpleador.getCodigoArp());
			 entidadVO.setDescripcionEntidad(datosEmpleador.getDescripcionArp());
			 
			 empleadorVO.setEntidadVO(entidadVO);
			 
			 cargoVO.setCodigoCargoEmpleado(datosEmpleador.getCodigoCargo());
			 cargoVO.setDescripcionCargoEmpleado(datosEmpleador.getDescripcionCargo());
			 
			 empleadorVO.setCargoVO(cargoVO);
 			 
			 tipoCotizanteVO.setConsecutivoCodigoTipoCotizante(Integer.parseInt(datosEmpleador.getCodigoTipoCotizante().trim()));
			 tipoCotizanteVO.setDescripcionTipoCotizante(datosEmpleador.getDescripcionTipoCotizante());
			 
			 empleadorVO.setTipoCotizanteVO(tipoCotizanteVO);

			 tipoCotizanteVO.setConsecutivoCodigoTipoCotizante(Integer.parseInt(datosEmpleador.getCodigoTipoCotizante().trim()));
			 tipoCotizanteVO.setDescripcionTipoCotizante(datosEmpleador.getDescripcionTipoCotizante());
			 
			 empleadorVO.setTipoCotizanteVO(tipoCotizanteVO);		 
			 empleadorVO.setTextoCopagoTextoCuotaModeradora(datosEmpleador.getInfoCuotaModeradoraCopago());
			 
			 try{
				 empleadorVO.setInicioVigenciaCobranza(Utilidades.obtenerDateDeString(datosEmpleador.getInicioVigenciaCobranza(), ConstantesEJB.CADENA_VACIA));
				 empleadorVO.setFinVigenciaCobranza(Utilidades.obtenerDateDeString(datosEmpleador.getFinVigenciaCobranza(), ConstantesEJB.CADENA_VACIA));		
			 }catch(Exception e){
				 throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_PARSE), e, ErrorType.INDEFINIDO);
			 }
			 
			 lConsultaEmpleadores.add(empleadorVO);		 	
		 }
		 
		return lConsultaEmpleadores;
	}

	
	/** Metodo que mapea los campos del servicio del afiliado para los convenio de capitacion*/
	
	public List<ConveniosCapitacionVO> consultaConveniosCapitacion(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado)throws LogicException {
		lConveniosCapitados = new ArrayList<ConveniosCapitacionVO>();
		ServiciosCapitadosVO capitadosVO = null;
		ConveniosCapitacionVO capitacionVO = null;
		
		DatosConvenioCapitacion[] datosConveniosCapitacion = consultaAfiliado.getConveniosCapitacion();		
		
		for (DatosConvenioCapitacion datosConvenioCapitacion : datosConveniosCapitacion) {
			lServiciosCapitados = new ArrayList<ServiciosCapitadosVO>();
			capitacionVO = new ConveniosCapitacionVO();
			
			capitacionVO.setConsecutivoConvenio(datosConvenioCapitacion.getCodigoConvenio());
			capitacionVO.setDescripcionConvenio(datosConvenioCapitacion.getDescripcionConvenio());
			capitacionVO.setCapita(datosConvenioCapitacion.getCapitado());			
			
			DatosServicioCapitadoCaja[] datos = datosConvenioCapitacion.getServiciosCapitados();			
				
			for (DatosServicioCapitadoCaja datosServicioCapitadoCaja : datos) {
				capitadosVO = new ServiciosCapitadosVO();				
				capitadosVO.setCodigoItemCapitacion(datosServicioCapitadoCaja.getCodigoItemCapitacion());
				capitadosVO.setDescripcionItemCapitacion(datosServicioCapitadoCaja.getDescripcionItemCapitacion());
				capitadosVO.setAccionItemCapitacion(datosServicioCapitadoCaja.getAccion());
				lServiciosCapitados.add(capitadosVO);
			}
			capitacionVO.setlServiciosCapitados(lServiciosCapitados);
			lConveniosCapitados.add(capitacionVO);
		}		
		return lConveniosCapitados;
	}

	/** Metodo que consulta los datos adicionales del afiliado */
		
	public DatosAfiliadoVO consultaDatosAdicionalesAfiliado(ResultadoConsultaAfiliadoDatosAdicionales consultaAfiliado) throws LogicException {
		DatosAfiliadoVO datosAfiliadoVO = new DatosAfiliadoVO();
		
		datosAfiliadoVO.setCotiza(consultaAfiliado.getAfiliado().getCotiza());
		datosAfiliadoVO.setBarrio(consultaAfiliado.getAfiliado().getBarrio());
		datosAfiliadoVO.setCodigoAfp(consultaAfiliado.getAfiliado().getCodigoAfp());
		datosAfiliadoVO.setDescripcionAfp(consultaAfiliado.getAfiliado().getDescripcionAfp());
		datosAfiliadoVO.setIPSCapitacion(consultaAfiliado.getAfiliado().getIpsCapitacion());
			
		return datosAfiliadoVO;
	}


	
	public List<AfiliadoVO> buscaAfiliadoxNombre(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String usuarioConsulta) throws LogicException{
		lConsularAfiliadoxNombre = new ArrayList<AfiliadoVO>();
		
		try{
			ConsultaValidadorWebService_SEI sistemaConsultaWS = ValidadorServiceFactory.getValidadorService();	
			ResultadoConsultaAfiliadoPorNombre resultadoConsultaAfiliadoPorNombre = sistemaConsultaWS.getConsultaAfiliadoxNombre(primerNombre, segundoNombre, primerApellido, segundoApellido, usuarioConsulta);
			
			DatosNombreAfiliado[] resultadoDatosNombreAfiliado = resultadoConsultaAfiliadoPorNombre.getDatosNombreAfiliado();
			
			for (DatosNombreAfiliado datosNombreAfiliado : resultadoDatosNombreAfiliado) {
				TiposIdentificacionVO tiposIdentificacionVO = new TiposIdentificacionVO();						
				AfiliadoVO afiliadoVO = new AfiliadoVO();
							
				String[] planes = datosNombreAfiliado.getListaPlanes();
				
				for (String plan : planes) {
					PlanVO planVO = new PlanVO();
					planVO.setDescripcionPlan(plan);
					afiliadoVO.setPlan(planVO);
				}
							
				tiposIdentificacionVO.setCodigoTipoIdentificacion(datosNombreAfiliado.getCodigoTipoIdentificacion());			
				afiliadoVO.setTipoIdentificacionAfiliado(tiposIdentificacionVO);
				afiliadoVO.setNombreCompleto(datosNombreAfiliado.getNombreCompleto());
				afiliadoVO.setNumeroIdentificacion(datosNombreAfiliado.getNumeroIdentificacion());	
				
				
				lConsularAfiliadoxNombre.add(afiliadoVO);
			}
		
		} catch (RemoteException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SERVICE), e, ErrorType.INDEFINIDO);				
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SERVICE), e, ErrorType.INDEFINIDO);	
		}		
		
		return lConsularAfiliadoxNombre;
	}
}
