package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.net.MalformedURLException;
import java.net.URL;

import co.com.sos.RequestCabeceraType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.consultarresultadofechaentrega.v1.ConsultarResultadoFechaEntregaReqType;
import co.com.sos.consultarresultadofechaentrega.v1.ConsultarResultadoFechaEntregaResType;
import co.com.sos.ejecutarfechaentrega.v1.CuerpoReqType;
import co.com.sos.ejecutarfechaentrega.v1.CuerpoResType;
import co.com.sos.ejecutarfechaentrega.v1.EjecutarFechaEntregaReqType;
import co.com.sos.ejecutarfechaentrega.v1.EjecutarFechaEntregaResType;
import co.com.sos.enterprise.fechaentrega.v1.FechaEntregaPortType;
import co.com.sos.solicitud.v1.SolicitudReqType;
import co.com.sos.solicitud.v1.SolicitudesFechaEntregaResType;
import co.com.sos.solicitud.v1.SolicitudesReqType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.FechaEntregaSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.IFechaEntregaService;
import com.sos.gestionautorizacionessaludejb.service.controller.CabeceraServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.ErroresServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.SolicitudesFechaEntregaServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class FechaEntregaService
 * Clase que permite la implementacion de la interfaz para consumir el servicio de Fecha Entrega
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/03/2016
 *
 */
public class FechaEntregaService implements IFechaEntregaService {
	
	private co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService service;

	
	/** Metodo que inicia el servicio de fecha de entrega*/	
	@Override
	public co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService crearFechaEntregaService() throws LogicException {
		
		String wsdlLocation;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_FECHA_ENTREGA_SERVICE));
		try {
			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);			
			service = new co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService(new URL(wsdlLocation));			
		
		} catch (MalformedURLException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}				
		return service;
	}

	/** Metodo que ejecuta el servicio de fecha de entrega */
	@Override
	public EjecutarFechaEntregaResType ejecutarFechaEntregaService(co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService service, SolicitudVO solicitudVO, String usuarioSession)throws LogicException {
		
		FechaEntregaPortType fechaEntregaPortType = null;
		EjecutarFechaEntregaResType ejecutarFechaEntregaResType = null;
		SolicitudReqType solicitudFechaEntregaReqType = new SolicitudReqType();
		
		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new  CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */
				
		solicitudFechaEntregaReqType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getConsecutivoSolicitud()));
		solicitudFechaEntregaReqType.setNumeroSolicitudSOS(solicitudVO.getNumeroSolicitudSOS());
		solicitudFechaEntregaReqType.setNumeroSolicitudPrestador(ConstantesEJB.CADENA_VACIA);
		
		SolicitudesReqType solicitudesReqType = new SolicitudesReqType();	
		solicitudesReqType.getSolicitudReq().add(solicitudFechaEntregaReqType);		
		
		/* se adiciona la informacion del cuerpo */
		CuerpoReqType cuerpoReqType = new CuerpoReqType();
		cuerpoReqType.setSolicitudesReq(solicitudesReqType);
		/* fin informacion del cuerpo */
		
		EjecutarFechaEntregaReqType ejecutarFechaEntregaReqType = new EjecutarFechaEntregaReqType();
		ejecutarFechaEntregaReqType.setRequestCabecera(requestCabeceraType);	
		ejecutarFechaEntregaReqType.setCuerpo(cuerpoReqType);		
		
		try {
			
			fechaEntregaPortType = service.getFechaEntregaPort();
			ejecutarFechaEntregaResType = fechaEntregaPortType.ejecutarFechaEntrega(ejecutarFechaEntregaReqType);
			
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		
		return ejecutarFechaEntregaResType;
	}

	/** Metodo que consulta el resultado del servicio de fecha de entrega */
	@Override
	public FechaEntregaSolicitudVO consultaResultadoFechaEntrega(EjecutarFechaEntregaResType ejecutarFechaEntregaResType, ConsultarResultadoFechaEntregaResType consultarResultadoFechaEntregaResType) throws ServiceException {
		
		ResponseCabeceraType cabeceraType = null;
		SolicitudesFechaEntregaResType solicitudesFechaEntregaResType = null;
		ServiceErrorVO serviceErrorVO = new ServiceErrorVO();	
		FechaEntregaSolicitudVO fechaEntregaSolicitudVO = new FechaEntregaSolicitudVO();
		int marcaConsulta = 0;
		
		if(ejecutarFechaEntregaResType != null){
			cabeceraType = ejecutarFechaEntregaResType.getResponseCabecera();
			marcaConsulta = 1;			
		}else{			
			cabeceraType = consultarResultadoFechaEntregaResType.getResponseCabecera();
		}
		
		if (cabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(cabeceraType.getEstadoTerminacion().value())) {

			serviceErrorVO.setEstadoTerminacion(cabeceraType.getEstadoTerminacion().value());
			serviceErrorVO.setMensajeEstadoTerminacion(cabeceraType.getMensaje());
			
			if(cabeceraType.getErrores() != null){				
				ErroresServiceType erroresServiceType = new ErroresServiceType(cabeceraType.getErrores());
				serviceErrorVO = erroresServiceType.informacionErrorServicio();	
				
				fechaEntregaSolicitudVO.setServiceErrorVO(serviceErrorVO);
				
				if (fechaEntregaSolicitudVO.getServiceErrorVO() != null) {
					throw new ServiceException(fechaEntregaSolicitudVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR + fechaEntregaSolicitudVO.getServiceErrorVO().getCodigoError() + (char) 13	+ fechaEntregaSolicitudVO.getServiceErrorVO().getMensajeError(), null);
				}
			}			
		}else{
			if(marcaConsulta == 1){
				CuerpoResType cuerpoResType = ejecutarFechaEntregaResType.getCuerpo();
				solicitudesFechaEntregaResType = cuerpoResType.getSolicitudesFechaEntregaRes();				
			}else{
				co.com.sos.consultarresultadofechaentrega.v1.CuerpoResType cuerpoResType = consultarResultadoFechaEntregaResType.getCuerpo();
				solicitudesFechaEntregaResType = cuerpoResType.getSolicitudesFechaEntregaRes();
			}
			
			SolicitudesFechaEntregaServiceType solicitudesFechaEntregaServiceType = new SolicitudesFechaEntregaServiceType();
			fechaEntregaSolicitudVO = solicitudesFechaEntregaServiceType.informacionFechaEntregaService(solicitudesFechaEntregaResType);			
		}	
		
		return fechaEntregaSolicitudVO;
	}

	/** Metodo que consulta el metodo de consulta de fecha de entrega */
	@Override
	public ConsultarResultadoFechaEntregaResType consultaFechaEntrega(co.com.sos.enterprise.fechaentrega.v1.FechaEntregaService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException {
		
		FechaEntregaPortType fechaEntregaPortType = null;		
		ConsultarResultadoFechaEntregaResType consultarResultadoFechaEntregaResType = null;
				
		SolicitudReqType solicitudFechaEntregaReqType = new SolicitudReqType();
				
		solicitudFechaEntregaReqType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getConsecutivoSolicitud()));
		solicitudFechaEntregaReqType.setNumeroSolicitudSOS(solicitudVO.getNumeroSolicitudSOS());
		solicitudFechaEntregaReqType.setNumeroSolicitudPrestador(ConstantesEJB.CADENA_VACIA);
		
		SolicitudesReqType solicitudesReqType = new SolicitudesReqType();	
		solicitudesReqType.getSolicitudReq().add(solicitudFechaEntregaReqType);		
		
		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new  CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */	
				
		/* se adiciona la informacion del cuerpo */
		co.com.sos.consultarresultadofechaentrega.v1.CuerpoReqType cuerpoReqType = new co.com.sos.consultarresultadofechaentrega.v1.CuerpoReqType();
		cuerpoReqType.setSolicitudesFechaEntregaReq(solicitudesReqType);
		/* fin informacion del cuerpo */
		
		ConsultarResultadoFechaEntregaReqType consultarResultadoFechaEntregaReqType = new ConsultarResultadoFechaEntregaReqType();
		consultarResultadoFechaEntregaReqType.setRequestCabecera(requestCabeceraType);
		consultarResultadoFechaEntregaReqType.setCuerpo(cuerpoReqType);
		
		try {
			fechaEntregaPortType = service.getFechaEntregaPort();
			consultarResultadoFechaEntregaResType = fechaEntregaPortType.consultarResultadoFechaEntrega(consultarResultadoFechaEntregaReqType);
			
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_FECHA_ENTREGA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultarResultadoFechaEntregaResType;		       
	}
}
