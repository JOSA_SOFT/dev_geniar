package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.math.BigInteger;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;

import co.com.sos.ErrorType;
import co.com.sos.ErroresType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.anexo.v1.AnexosType;
import co.com.sos.enterprise.solicitud.v1.SolicitudPortType;
import co.com.sos.enterprise.solicitud.v1.SolicitudService;
import co.com.sos.grabaridentificadordocumentoanexo.v1.GrabarIdentificadorDocumentoAnexoReqType;
import co.com.sos.grabaridentificadordocumentoanexo.v1.GrabarIdentificadorDocumentoAnexoResType;
import co.com.sos.grabaridentificadordocumentoanexo.v1.IdentificadorDocumentoAnexoSolicitudReqType;
import co.com.sos.grabarsolicitud.v1.CuerpoReqType;
import co.com.sos.grabarsolicitud.v1.CuerpoResType;
import co.com.sos.grabarsolicitud.v1.GrabarSolicitudReqType;
import co.com.sos.grabarsolicitud.v1.GrabarSolicitudResType;
import co.com.sos.solicitud.v1.GrabarSolicitudesResType;
import co.com.sos.solicitud.v1.MedioContactoType;
import co.com.sos.solicitud.v1.SolicitudType;
import co.com.sos.solicitud.v1.SolicitudesType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.IGuardarSolicitudService;
import com.sos.gestionautorizacionessaludejb.service.controller.AfiliadoServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.AnexoServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.AtencionServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.CabeceraServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.DiagnosticosServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.ErroresServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.HospitalizacionServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.MedicoServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.PrestacionesServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.PrestadorServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.ValidacionServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class GuardarSolicitudService Clase que implementa el servicio de guardar
 * solicitud
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/02/2016
 *
 */
public class GuardarSolicitudService implements IGuardarSolicitudService {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GuardarSolicitudService.class);
	
	private SolicitudService service = null;
	private SolicitudVO solicitudVO;

	// Metodo que permite guardar la solicitud
	public GrabarSolicitudResType guardarSolicitud(SolicitudService service, RegistrarSolicitudVO solicitudVO, String usuarioSession, SolicitudVO datosGuardarSolicitud,
			ValidacionEspecialVO validacionEspecialVO) throws ServiceException {

		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new CabeceraServiceType(usuarioSession);
		/* fin informacion de la cabecera */

		SolicitudType solicitudType = new SolicitudType();

		co.com.sos.solicitud.v1.IdentificadorType identificadorType = new co.com.sos.solicitud.v1.IdentificadorType();
		identificadorType.setConsecutivo(BigInteger.ZERO);

		if (datosGuardarSolicitud != null && datosGuardarSolicitud.getConsecutivoSolicitud() != null) {
			identificadorType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(datosGuardarSolicitud.getConsecutivoSolicitud()));
		}
		solicitudType.setIdentificador(identificadorType);

		solicitudType.setNumeroSolicitud(ConstantesEJB.CADENA_VACIA);
		solicitudType.setFechaSolicitudProfesional(Utilidades.asXMLGregorianCalendar(solicitudVO.getAtencionVO().getFechaSolicitudProfesional()));

		// metodo que ingresa la informacion de la atencion
		AtencionServiceType atencionServiceType = new AtencionServiceType(solicitudVO);

		// origen de la atencion
		solicitudType.setOrigenAtencion(atencionServiceType.origenAtencionServiceType());

		// tipo de servicio de la atencion
		solicitudType.setTipoServicio(atencionServiceType.tipoServicioType());

		// prioridad de la atencion
		solicitudType.setPrioridad(atencionServiceType.prioridadServiceType());

		// ubicacion del paciente
		solicitudType.setTipoUbicacion(atencionServiceType.tipoUbicacionServiceType());

		// forma atencion
		solicitudType.setFormaAtencion(atencionServiceType.formaAtencionServiceType());

		if (solicitudVO.getMedioContactoVO().getCodigoMedioContacto().equals(ConstantesEJB.MEDIO_CONTACTOS_IPS)) {
			solicitudType.setGuiaAtencion(solicitudVO.getAtencionVO().getManejoIntegralGuia() != null ? solicitudVO.getAtencionVO().getManejoIntegralGuia().trim() : ConstantesEJB.CADENA_VACIA);
		}

		solicitudType
		.setJustificacionClinica(solicitudVO.getAtencionVO().getJustificacionClinica() != null ? solicitudVO.getAtencionVO().getJustificacionClinica().trim() : ConstantesEJB.CADENA_VACIA);
		solicitudType.setCodigoEps(Messages.getValorParametro(ConstantesEJB.CODIGO_EPS) != null ? Messages.getValorParametro(ConstantesEJB.CODIGO_EPS).trim() : ConstantesEJB.CADENA_VACIA);

		solicitudType.setNumeroSolicitudAutorizacionSos(ConstantesEJB.CADENA_VACIA);
		solicitudType.setNuam(BigInteger.ZERO);
		solicitudType.setObsrvcnAdcnl(solicitudVO.getObservaciones() != null ? solicitudVO.getObservaciones().trim() : ConstantesEJB.CADENA_VACIA);
		solicitudType.setHospitalizacion(solicitudVO.getMarcaHospitalizacion() != null ? solicitudVO.getMarcaHospitalizacion().trim() : ConstantesEJB.NO);
		solicitudType.setDomi(ConstantesEJB.NO);

		// Se adiciona el medio de contacto de la solicitud
		MedioContactoType medioContactoType = new MedioContactoType();
		medioContactoType = adicionMedioDeContacto(solicitudVO, medioContactoType);
		solicitudType.setMedioContacto(medioContactoType);

		/** Inicio informacion del afiliado */
		AfiliadoServiceType afiliadoType = new AfiliadoServiceType(solicitudVO);
		solicitudType.setAfiliado(afiliadoType.informacionAfiliadoServiceType());
		/** Fin informacion del afiliado */

		/** informacion de la atencion */
		solicitudType.setNumeroSolicitudProveedor(solicitudVO.getAtencionVO().getNumeroSolicitud() != null ? String.valueOf(solicitudVO.getAtencionVO().getNumeroSolicitud()).trim()
				: ConstantesEJB.CADENA_VACIA);

		// informacion de la atencion por medio de contacto de radicacion
		solicitudType = validarPorContactosRadicacion(solicitudVO, solicitudType,
				atencionServiceType);

		/** informacion de la hospitalizacion */
		solicitudType = validacionCombos(solicitudVO, solicitudType);		
		/** fin informacion de la hospitalizacion */

		/** informacion de los diagnosticos */
		DiagnosticosServiceType diagnosticosType = new DiagnosticosServiceType(solicitudVO);
		solicitudType.setDiagnosticos(diagnosticosType.informacionDiagnosticosServiceType());
		/** fin informacion de los diagnosticos */

		/** fin informacion de la atencion */

		/** informacion del medico */
		MedicoServiceType medicoServiceType = new MedicoServiceType(solicitudVO);
		solicitudType.setMedico(medicoServiceType.informacionMedicoServiceType());
		/** Fin informacion del medico */

		/** Inicio informacion del prestador */
		PrestadorServiceType prestadorServiceType = new PrestadorServiceType(solicitudVO);
		solicitudType.setPrestador(prestadorServiceType.informacionPrestadorServiceType());
		/** fin informacion del prestador */

		/** Inicio informacion de las prestaciones */
		PrestacionesServiceType prestacionesServiceType = new PrestacionesServiceType(solicitudVO);
		solicitudType.setPrestaciones(prestacionesServiceType.informacionPrestacionesServiceType());
		/** Fin informacion de las prestaciones */

		co.com.sos.anexo.v1.AnexosType anexosType = new AnexosType();

		anexosType = validacionAnexoType(solicitudVO);
		solicitudType.setAnexos(anexosType);

		validacionEspecial(usuarioSession, validacionEspecialVO, solicitudType);

		SolicitudesType solicitudesType = new SolicitudesType();
		solicitudesType.getSolicitud().add(solicitudType);

		CuerpoReqType cuerpoReqType = new CuerpoReqType();
		cuerpoReqType.setSolicitudes(solicitudesType);

		GrabarSolicitudReqType grabarSolicitudReqType = new GrabarSolicitudReqType();
		grabarSolicitudReqType.setRequestCabecera(cabeceraServiceType.informacionCabeceraServiceType());
		grabarSolicitudReqType.setCuerpo(cuerpoReqType);

		SolicitudPortType portType = service.getSolicitudPort();
		GrabarSolicitudResType grabarSolicitudResType = portType.grabarSolicitud(grabarSolicitudReqType);
		return grabarSolicitudResType;
	}

	private MedioContactoType adicionMedioDeContacto(RegistrarSolicitudVO solicitudVO,
			MedioContactoType medioContactoType) {
		medioContactoType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getMedioContactoVO().getConsecutivoMedioContacto()));
		medioContactoType.setCodigo(solicitudVO.getMedioContactoVO().getCodigoMedioContacto() != null ? solicitudVO.getMedioContactoVO().getCodigoMedioContacto().trim() : ConstantesEJB.CADENA_VACIA);
		medioContactoType.setDescripcion(solicitudVO.getMedioContactoVO().getDescripcionMedioContacto() != null ? solicitudVO.getMedioContactoVO().getDescripcionMedioContacto()
				: ConstantesEJB.CADENA_VACIA);

		return medioContactoType;
	}

	private AnexosType validacionAnexoType(RegistrarSolicitudVO solicitudVO) {
		AnexoServiceType anexoServiceType = new AnexoServiceType(solicitudVO);
		return anexoServiceType.informacionAnexosServiceType();
	}

	private SolicitudType validacionCombos(RegistrarSolicitudVO solicitudVO,
			SolicitudType solicitudType) {
		if (ConstantesEJB.VALIDACION_COMBOS_OPCIONAL.equals(solicitudVO.getMarcaHospitalizacion())
				|| ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_COMPLETO.equals(solicitudVO.getMarcaHospitalizacion().equals(ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_COMPLETO))) {

			HospitalizacionServiceType hospitalizacionServiceType = new HospitalizacionServiceType(solicitudVO);
			solicitudType.setHospitalizaciones(hospitalizacionServiceType.informacionHospitalizacionServiceType());
		}

		return solicitudType;
	}

	/**
	 * Metodo para hacer una validacion especial
	 * @param usuarioSession
	 * @param validacionEspecialVO
	 * @param solicitudType
	 * @return
	 */
	private SolicitudType validacionEspecial(String usuarioSession,
			ValidacionEspecialVO validacionEspecialVO,
			SolicitudType solicitudType) {
		/** informacion de la validacion especial */
		if (validacionEspecialVO != null) {
			ValidacionServiceType validacionEspecialType = new ValidacionServiceType(validacionEspecialVO, usuarioSession);
			solicitudType.setValidacionEspecial(validacionEspecialType.informacionValidacionEspecialType());
		}

		return solicitudType;
	}

	/**
	 * Metodo que valida los contactos de radicacion
	 * @param solicitudVO
	 * @param solicitudType
	 * @param atencionServiceType
	 * @return
	 */
	private SolicitudType validarPorContactosRadicacion(
			RegistrarSolicitudVO solicitudVO, SolicitudType solicitudType,
			AtencionServiceType atencionServiceType) {
		if (ConstantesEJB.MEDIO_CONTACTO_ASI.equals(solicitudVO.getMedioContactoVO().getCodigoMedioContacto()) || ConstantesEJB.MEDIO_CONTACTOS_AFILIADO.equals(solicitudVO.getMedioContactoVO().getCodigoMedioContacto())) {
			solicitudType.setSuperaTopeSoat(solicitudVO.getAtencionVO().getSuperaTopeSoat() != null ? solicitudVO.getAtencionVO().getSuperaTopeSoat().trim() : ConstantesEJB.NO);
			solicitudType.setDomi(solicitudVO.getAtencionVO().getDomi() != null ? solicitudVO.getAtencionVO().getDomi() : ConstantesEJB.CADENA_VACIA);

			solicitudType.setClaseAtencion(atencionServiceType.claseAtencionServiceType());
		}
		
		if (ConstantesEJB.MEDIO_CONTACTOS_IPS.equals(solicitudVO.getMedioContactoVO().getCodigoMedioContacto())){
			solicitudType.setClaseAtencion(atencionServiceType.claseAtencionServiceType());
		}
		return solicitudType;
	}

	/** Metodo que consulta el resultado del servicio de guardar */
	public SolicitudVO consultaResultadoGuardar(GrabarSolicitudResType grabarSolicitudResType, RegistrarSolicitudVO registrarSolicitudVO, SolicitudVO datosGuardarSolicitud) throws ServiceException {

		if (datosGuardarSolicitud != null) {
			solicitudVO = datosGuardarSolicitud;
		} else {
			solicitudVO = new SolicitudVO();
		}

		ResponseCabeceraType cabeceraType = grabarSolicitudResType.getResponseCabecera();

		if (cabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(cabeceraType.getEstadoTerminacion().value())) {
			ServiceErrorVO serviceErrorVO = new ServiceErrorVO();

			serviceErrorVO.setEstadoTerminacion(cabeceraType.getEstadoTerminacion().value());
			serviceErrorVO.setMensajeEstadoTerminacion(cabeceraType.getMensaje());

			ErroresServiceType erroresServiceType = new ErroresServiceType(cabeceraType.getErrores());
			serviceErrorVO = erroresServiceType.informacionErrorServicio();

			solicitudVO.setServiceErrorVO(serviceErrorVO);

			if (solicitudVO != null && solicitudVO.getServiceErrorVO() != null) {
				throw new ServiceException(solicitudVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR + solicitudVO.getServiceErrorVO().getCodigoError()
						+ (char) 13 + solicitudVO.getServiceErrorVO().getMensajeError(), null);
			}

		} else {
			CuerpoResType cuerpoResType = grabarSolicitudResType.getCuerpo();
			GrabarSolicitudesResType resultadoSolicitud = cuerpoResType.getSolicitudes();
			if (resultadoSolicitud != null) {
				List<co.com.sos.solicitud.v1.GrabarSolicitudResType> lGrabarSolicitudRes = resultadoSolicitud.getGrabarSolicitudRes();
				for (co.com.sos.solicitud.v1.GrabarSolicitudResType resultadoGuardarServicio : lGrabarSolicitudRes) {
					solicitudVO = new SolicitudVO();
					solicitudVO.setConsecutivoSolicitud(resultadoGuardarServicio.getConsecutivo().intValue());
					solicitudVO.setNumeroSolicitudSOS(resultadoGuardarServicio.getNumeroSolicitudSOS());
				}
			}
			solicitudVO.setRegistrarSolicitudVO(registrarSolicitudVO);
		}

		return solicitudVO;
	}

	/** Metodo que inicia el servicio de guardar la solicitud */
	public SolicitudService crearSolicitudService() throws ServiceException  {
		try {
			String wsdlLocation;
			ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

			PropiedadVO propiedad = new PropiedadVO();

			propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
			propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
			propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_GESTION_SOLICITUD));

			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);

			service = new SolicitudService(new URL(wsdlLocation));
		} catch (Exception e) {
			LOG.error(e);
			throw new ServiceException(e.getMessage());
		}
		return service;
	}

	@Override
	public GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexos(String usuarioSession, BigInteger consecutivoSolicitud, BigInteger identificadorDocumento)throws LogicException, ServiceException {
		GrabarIdentificadorDocumentoAnexoReqType grabarIdentificadorDocumentoAnexoReqType = new GrabarIdentificadorDocumentoAnexoReqType();

		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new CabeceraServiceType(usuarioSession);
		/* fin informacion de la cabecera */

		grabarIdentificadorDocumentoAnexoReqType.setRequestCabecera(cabeceraServiceType.informacionCabeceraServiceType());

		co.com.sos.grabaridentificadordocumentoanexo.v1.CuerpoReqType cuerpoReqType = new co.com.sos.grabaridentificadordocumentoanexo.v1.CuerpoReqType();
		IdentificadorDocumentoAnexoSolicitudReqType identificadorDocumentoAnexoSolicitudReqType = new IdentificadorDocumentoAnexoSolicitudReqType();
		identificadorDocumentoAnexoSolicitudReqType.setConsecutivoSolicitud(consecutivoSolicitud);
		identificadorDocumentoAnexoSolicitudReqType.setIdentificadorDocumento(identificadorDocumento);
		cuerpoReqType.setIdentificadorDocumentoAnexoReq(identificadorDocumentoAnexoSolicitudReqType);

		grabarIdentificadorDocumentoAnexoReqType.setCuerpo(cuerpoReqType);

		SolicitudPortType portType = service.getSolicitudPort();
		GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexoResType = portType.grabarIdentificadorDocumentoAnexo(grabarIdentificadorDocumentoAnexoReqType);

		validarEstadoGrabacionDocumentoAxeno(grabarIdentificadorDocumentoAnexoResType);

		co.com.sos.grabaridentificadordocumentoanexo.v1.CuerpoResType cuerpoResType = grabarIdentificadorDocumentoAnexoResType.getCuerpo();
		if (cuerpoResType.getIdentificadorDocumentoAnexoRes() != null && cuerpoResType.getIdentificadorDocumentoAnexoRes().getEstado() != null
				&& ConstantesEJB.CONSECUTIVO_SOLICITUD_NO_ENCONTRADO_EN_ANEXO.equalsIgnoreCase(cuerpoResType.getIdentificadorDocumentoAnexoRes().getEstado().trim()))
			throw new LogicException(ConstantesEJB.CONSECUTIVO_SOLICITUD_NO_ENCONTRADO + consecutivoSolicitud, com.sos.excepciones.LogicException.ErrorType.DATO_NO_EXISTE);
		return grabarIdentificadorDocumentoAnexoResType;
	}

	private void validarEstadoGrabacionDocumentoAxeno(
			GrabarIdentificadorDocumentoAnexoResType grabarIdentificadorDocumentoAnexoResType)
					throws ServiceException {
		ResponseCabeceraType responseCabeceraType = grabarIdentificadorDocumentoAnexoResType.getResponseCabecera();
		if (responseCabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(responseCabeceraType.getEstadoTerminacion().value())) {
			ErroresType erroresType = responseCabeceraType.getErrores();
			String error = "";
			if (erroresType != null && erroresType.getError() != null) {
				for (ErrorType errorType : erroresType.getError()) {
					error = errorType.getCodigo() + errorType.getMensaje() + "\n";
				}
			}
			throw new ServiceException(responseCabeceraType.getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR + error, null);
		}
	}
}
