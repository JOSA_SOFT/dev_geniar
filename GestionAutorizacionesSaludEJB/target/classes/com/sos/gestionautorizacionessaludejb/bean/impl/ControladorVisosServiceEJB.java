package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.sos.base.services.to.DatosArchivo;
import com.sos.base.services.to.SOSDateTime;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaludejb.bean.ControladorVisosServiceEJBLocal;
import com.sos.gestionautorizacionessaludejb.bean.ControladorVisosServiceEJBRemote;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.EJBCaller;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.visos.service.ejb.remote.VisosServiceEJB;
import com.sos.visos.service.ejb.remote.VisosServiceEJBHome;
import com.sos.visos.service.to.DocumentoAnexos;
import com.sos.visos.service.to.IndicesAnexo;
import com.sos.visos.service.to.containers.AnexosContainer;
import com.sos.visos.service.to.containers.DocumentosContainer;
import com.sos.visos.service.to.containers.IndicesContainer;
import com.sos.visos.service.vo.DocumentoDigitalizado;
import com.sos.visos.service.vo.IdDocumentoResp;
import com.sos.visos.service.vo.Indice;

/**
 * @class ControladorConsultaVisosServiceEJB Implementacion del ServicioWeb que
 *        provee acceso a componente Visos Service
 * @project GestionSolicitudesCTC
 * @author Victor Hugo Gil Ramos
 * @version 16/01/2015
 *
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ControladorVisosServiceEJB implements ControladorVisosServiceEJBLocal, ControladorVisosServiceEJBRemote {

	/**
	 * Codigo unico de serializacion
	 */
	private static final long serialVersionUID = 785469810873185166L;
	public static final String JNDI_NAME_VISOS_SERVICE = ConsultaProperties.getString("VISOSSERVICE");

	private VisosServiceEJBHome serviceHome;
	private VisosServiceEJB visosServiceEJB;

	private static final Logger LOGGER = Logger.getLogger(ControladorVisosServiceEJB.class);

	/**
	 * Metodo encargado de consultar documentos en el sistema de visos x indices
	 */

	public List<DocumentoSoporteVO> consultarDocumentoAnexoxIndices(IndicesContainer indices) throws LogicException {

		List<DocumentoSoporteVO> listDocumentoSoporte;
		listDocumentoSoporte = new ArrayList<DocumentoSoporteVO>();

		// Contenedor de Anexos
		DocumentosContainer documentosContainer;
		try {
			documentosContainer = visosServiceEJB.getDocumentos(indices);
		} catch (RemoteException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTO_SOPORTE), e, ErrorType.INDEFINIDO);
		}

		if (documentosContainer.getErrorCode() != 0) {
			String message = documentosContainer.getMessage();
			LOGGER.error(message + ": " + documentosContainer.getMessage());
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTO_SOPORTE), ErrorType.INDEFINIDO);
		}

		DocumentoAnexos[] docAnexos = documentosContainer.getDocs();

		for (DocumentoAnexos documentoAnexos : docAnexos) {
			DocumentoSoporteVO documentoSoporte = new DocumentoSoporteVO();
			documentoSoporte.setConsecutivoDocumento(documentoAnexos.getDoc().getIdDocumento());

			IndicesAnexo[] anexos = documentoAnexos.getAnexos();

			for (IndicesAnexo indicesAnexo : anexos) {
				DatosArchivo datosArchivo = indicesAnexo.getAnexo();
				documentoSoporte.setDatosArchivoSoporte(datosArchivo.getBytesArchivo());
				documentoSoporte.setNombreDocumento(datosArchivo.getNombreArchivo());
				documentoSoporte.setPathDocumento(datosArchivo.getUbicacionArchivo());
				documentoSoporte.setConsecutivoCodigoTipoAnexo(indicesAnexo.getTipoAnexo());

				listDocumentoSoporte.add(documentoSoporte);
			}
		}
		return listDocumentoSoporte;
	}

	/**
	 * Metodo encargado de consultar documentos en el sistema de visos x id
	 * Documento
	 */
	public List<DocumentoSoporteVO> consultarDocumentoAnexoxIdDocumento(long idDocumento) throws LogicException {
		List<DocumentoSoporteVO> listDocumentoSoporte = new ArrayList<DocumentoSoporteVO>();

		// Contenedor de Anexos
		AnexosContainer anexosContainer;
		try {
			anexosContainer = visosServiceEJB.getAnexosxIdDoc(idDocumento);
		} catch (RemoteException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTO_SOPORTE), e, ErrorType.INDEFINIDO);
		}

		if (anexosContainer.getErrorCode() != 0) {
			String message = anexosContainer.getMessage();
			LOGGER.error(message + ": " + anexosContainer.getMessage());
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTO_SOPORTE), ErrorType.INDEFINIDO);
		}

		IndicesAnexo[] anexos = anexosContainer.getAnexos();

		for (IndicesAnexo indicesAnexo : anexos) {
			DocumentoSoporteVO documentoSoporte = new DocumentoSoporteVO();

			DatosArchivo datosArchivo = indicesAnexo.getAnexo();
			documentoSoporte.setDatosArchivoSoporte(datosArchivo.getBytesArchivo());
			documentoSoporte.setNombreDocumento(datosArchivo.getNombreArchivo());
			documentoSoporte.setPathDocumento(datosArchivo.getUbicacionArchivo());
			documentoSoporte.setConsecutivoCodigoTipoAnexo(indicesAnexo.getTipoAnexo());
			documentoSoporte.setConsecutivoDocumento(Long.parseLong(Integer.toString(indicesAnexo.getTipoAnexo())));

			listDocumentoSoporte.add(documentoSoporte);
			
		}

		return listDocumentoSoporte;
	}

	/**
	 * Metodo encargado guardar los documentos soporte utilizando el servicio de
	 * visos service
	 */

	public long guardarDocumentosSoporte(List<SoporteVO> lDocumentoSoporte, Indice[] indices, String digitalizador, String tipoDocumento, Integer consecutivoSolicitud) throws LogicException {

		long idDocumento = 0;
		List<IndicesAnexo> listaIndicesAnexo = null;
		DocumentoDigitalizado documentoDigitalizado = new DocumentoDigitalizado();
		AnexosContainer anexosContainer = new AnexosContainer();

		documentoDigitalizado.setIndices(indices);
		documentoDigitalizado.setDigitalizador(digitalizador);
		documentoDigitalizado.setDiaDigitalizacion(new SOSDateTime(new Date()));
		documentoDigitalizado.setTipoDocumento(tipoDocumento);
		
		listaIndicesAnexo =  obtenerDocumentosSoporte(lDocumentoSoporte, consecutivoSolicitud);

		if (listaIndicesAnexo != null && !listaIndicesAnexo.isEmpty()) {

			IndicesAnexo[] indicesAnexos = new IndicesAnexo[listaIndicesAnexo.size()];
			indicesAnexos = listaIndicesAnexo.toArray(indicesAnexos);
			anexosContainer.setAnexos(indicesAnexos);

			// Invocacion del Servicio
			IdDocumentoResp resp;
			try {
				resp = visosServiceEJB.guardarDocumentos(documentoDigitalizado, anexosContainer);
			} catch (RemoteException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOCUMENTO_SOPORTE), e, ErrorType.INDEFINIDO);
			}

			if (resp.getErrorCode() != 0) {
				throw new LogicException(ConstantesEJB.MENSAJE_VISOS + resp.getErrorCode() + " - " + resp.getMessage(), LogicException.ErrorType.INDEFINIDO);
			}

			idDocumento = resp.getIdDocumento();

			if (idDocumento == 0) {
				throw new LogicException(ConstantesEJB.MENSAJE_CONSECUTIVO_SOLICITUD + consecutivoSolicitud + " " + Messages.getValorError(ConstantesEJB.ERROR_GRABAR_DOCUMENTO_SOPORTE), ErrorType.INDEFINIDO);
			}
		}

		return idDocumento;
	}

	/**
	 * @param lDocumentoSoporte
	 * @param consecutivoSolicitud
	 */
	private List<IndicesAnexo> obtenerDocumentosSoporte(List<SoporteVO> lDocumentoSoporte, Integer consecutivoSolicitud) {
		
		List<IndicesAnexo> listaIndicesAnexo = new ArrayList<IndicesAnexo>();
		
		for (SoporteVO soporteVo : lDocumentoSoporte) {
			if (soporteVo.getData() != null && soporteVo.getData().length > 0 && soporteVo.getNombreSoporte() != null && !soporteVo.isSubidoAVisos()) {
				// /Objeto que contine los bytes del archivo fisico y su nombre
				DatosArchivo datosArchivo = new DatosArchivo();
				datosArchivo.setBytesArchivo(soporteVo.getData());
				datosArchivo.setNombreArchivo(consecutivoSolicitud + "-" + soporteVo.getNombreSoporte());

				IndicesAnexo indiceAnexo = new IndicesAnexo();
				indiceAnexo.setAnexo(datosArchivo);
				indiceAnexo.setPrincipal(true);
				indiceAnexo.setTipoAnexo(soporteVo.getConsecutivoCodigoDocumentoSoporte().intValue());
				listaIndicesAnexo.add(indiceAnexo);
			}
		}
		
		return listaIndicesAnexo;
	}

	/**
	 * Metodo que crea la conexion con el servicio de visos service
	 * 
	 * @throws LogicException
	 * @throws Exception
	 * @throws
	 * @throws CreateException
	 */

	public void crearConexionVisosService() throws LogicException {

		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties.getString("APLICACION_VISOS"));
		propiedad.setClasificacion(ConsultaProperties.getString("CLASIFICACION_VISOS"));
		propiedad.setPropiedad(ConsultaProperties.getString("PROPIEDAD_VISOS"));

		try {
			serviceHome = EJBCaller.createService(JNDI_NAME_VISOS_SERVICE, propertyQuery.consultarPropiedad(propiedad));
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VISOS_SERVICE), e, ErrorType.INDEFINIDO);
		}

		try {
			visosServiceEJB = serviceHome.create();
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VISOS_SERVICE), e, ErrorType.INDEFINIDO);
		}
	}
}
