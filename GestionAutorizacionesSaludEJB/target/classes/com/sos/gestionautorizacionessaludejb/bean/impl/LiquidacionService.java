package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import co.com.sos.RequestCabeceraType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.liquidacion.v1.CuerpoLiquidacionReqType;
import co.com.sos.liquidacion.v1.EnvioLiquidacionReqType;
import co.com.sos.liquidacion.v1.EnvioLiquidacionResType;
import co.com.sos.liquidacion.v1.LiquidacionCMPortType;
import co.com.sos.liquidacion.v1.SolicitudesLiquidacionType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;

import com.sos.gestionautorizacionessaluddata.model.servicios.LiquidacionSolicitudVO;
import com.sos.gestionautorizacionessaludejb.bean.ILiquidacionService;
import com.sos.gestionautorizacionessaludejb.service.controller.CabeceraServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.SolicitudesLiquidacionServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class LiquidacionService
 * Clase que permite la implementacion de la interfaz para consumir el servicio de liquidacion
 * @author ing. Victor Hugo Gil Ramos
 * @version 04/04/2016
 *
 */
public class LiquidacionService implements ILiquidacionService {	
	
	private co.com.sos.liquidacion.v1.LiquidacionService service;
	
	/** Metodo que inicia el servicio de liquidacion*/
	@Override
	public co.com.sos.liquidacion.v1.LiquidacionService crearLiquidacionService() throws LogicException {
		
		String wsdlLocation;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_LIQUIDACION_SERVICE));
		
		try {
			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);	
			service = new co.com.sos.liquidacion.v1.LiquidacionService(new URL(wsdlLocation));
			
		} catch (MalformedURLException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		
		return service;
	}

	/** Metodo que ejecuta el servicio de liquidacion */
	@Override
	public EnvioLiquidacionResType obtenerLiquidacionService(co.com.sos.liquidacion.v1.LiquidacionService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException {
		
		LiquidacionCMPortType liquidacionCMPortType = null;		
		EnvioLiquidacionResType envioLiquidacionResType = null;
		String codigoSolicitud = solicitudVO.getConsecutivoSolicitud().toString();
		
		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new  CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */
		
		/* se adiciona la informacion del cuerpo */
		CuerpoLiquidacionReqType cuerpoLiquidacionReqType = new CuerpoLiquidacionReqType();
		cuerpoLiquidacionReqType.setUsuario(usuarioSession);
		cuerpoLiquidacionReqType.getCodigoSolicitud().add(codigoSolicitud);
		/* fin informacion del cuerpo */
		
		EnvioLiquidacionReqType envioLiquidacionReqType = new EnvioLiquidacionReqType();
		envioLiquidacionReqType.setRequestCabecera(requestCabeceraType);
		envioLiquidacionReqType.setCuerpo(cuerpoLiquidacionReqType);
		
		try {
			liquidacionCMPortType = service.getLiquidacionPort();
			envioLiquidacionResType = liquidacionCMPortType.obtenerLiquidacion(envioLiquidacionReqType);
			
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_LIQUIDACION_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return envioLiquidacionResType;
	}

	/** Metodo que consulta el resultado del servicio de liquidacion */
	@Override
	public LiquidacionSolicitudVO consultaResultadoLiquidacion(EnvioLiquidacionResType liquidacionResType) throws ServiceException {
		
		ResponseCabeceraType cabeceraType = null;
		List<SolicitudesLiquidacionType> lSolicitudesLiquidacionType = null;
		LiquidacionSolicitudVO liquidacionSolicitudVO = new LiquidacionSolicitudVO();		
		
		cabeceraType = liquidacionResType.getResponseCabecera();
		
		if (cabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(cabeceraType.getEstadoTerminacion().value())) {
			ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
			serviceErrorVO.setEstadoTerminacion(cabeceraType.getEstadoTerminacion().value());
			serviceErrorVO.setMensajeEstadoTerminacion(cabeceraType.getMensaje());		
			liquidacionSolicitudVO.setServiceErrorVO(serviceErrorVO);		
		}else{
			lSolicitudesLiquidacionType = liquidacionResType.getSolicitudes();			
			SolicitudesLiquidacionServiceType liquidacionServiceType = new SolicitudesLiquidacionServiceType();
			liquidacionSolicitudVO = liquidacionServiceType.informacionLiquidacionService(lSolicitudesLiquidacionType);				
		}
		
		if(liquidacionSolicitudVO.getServiceErrorVO()!= null){
			throw new ServiceException(liquidacionSolicitudVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR + liquidacionSolicitudVO.getServiceErrorVO().getMensajeError(), null);
		}		
				
		return liquidacionSolicitudVO;
	}
}
