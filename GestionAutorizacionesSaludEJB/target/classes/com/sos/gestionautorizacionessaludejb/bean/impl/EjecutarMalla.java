package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import co.com.sos.RequestCabeceraType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.SeguridadType;
import co.com.sos.consultarresultadomalla.v1.ConsultarResultadoMallaReqType;
import co.com.sos.consultarresultadomalla.v1.ConsultarResultadoMallaResType;
import co.com.sos.ejecutarmalla.v1.CuerpoReqType;
import co.com.sos.ejecutarmalla.v1.CuerpoResType;
import co.com.sos.ejecutarmalla.v1.EjecutarMallaReqType;
import co.com.sos.ejecutarmalla.v1.EjecutarMallaResType;
import co.com.sos.enterprise.malla.v1.MallaPortType;
import co.com.sos.enterprise.malla.v1.MallaService;
import co.com.sos.prestacion.v1.InconsistenciaResType;
import co.com.sos.prestacion.v1.InconsistenciasResType;
import co.com.sos.prestacion.v1.MarcaType;
import co.com.sos.prestacion.v1.MarcasType;
import co.com.sos.prestacion.v1.PrestacionResType;
import co.com.sos.prestacion.v1.PrestacionesResType;
import co.com.sos.solicitud.v1.SolicitudReqType;
import co.com.sos.solicitud.v1.SolicitudResType;
import co.com.sos.solicitud.v1.SolicitudesReqType;
import co.com.sos.solicitud.v1.SolicitudesResType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.EstadoVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasVO;
import com.sos.gestionautorizacionessaluddata.model.malla.MarcaInconsistenciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaludejb.bean.IEjecutarMalla;
import com.sos.gestionautorizacionessaludejb.service.controller.ErroresServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

public class EjecutarMalla implements IEjecutarMalla {


	private MallaService mallaService = null;
	private RequestCabeceraType requestCabeceraType = null;
	private ResponseCabeceraType responseCabeceraType = null;
	private SolicitudesReqType solicitudesReqType = null;
	private SolicitudesResType solicitudesResType = null;
	private List<MarcaInconsistenciaVO> lMarcasInconsistenciasPrestacion;
	private List<MarcaInconsistenciaVO> lMarcasInconsistenciasSolicitud;
	private List<PrestacionDTO> listaPrestaciones;

	// Consulta del servicio de malla
	/**
	 * Metodo que ejecuta el servicio de malla
	 * 
	 * @throws Exception
	 */

	public EjecutarMallaResType ejecutarMallaService(MallaService mallaService, SolicitudVO solicitudVO, String usuarioSession) throws ServiceException {
		this.cabeceraConsultaService(solicitudVO, usuarioSession);

		// Se adiciona el cuerpo de la solicitud
		CuerpoReqType cuerpoReqType = new CuerpoReqType();
		cuerpoReqType.setSolicitudesReq(solicitudesReqType);

		EjecutarMallaReqType ejecutarMallaReqType = new EjecutarMallaReqType();
		ejecutarMallaReqType.setRequestCabecera(requestCabeceraType);
		ejecutarMallaReqType.setCuerpo(cuerpoReqType);

		MallaPortType mallaPortType = mallaService.getMallaPort();

		EjecutarMallaResType ejecutarMallaResType = mallaPortType.ejecutarMalla(ejecutarMallaReqType);
		return ejecutarMallaResType;
	}

	/**
	 * Metodo que consulta el resultado de ejecucion de la malla
	 * 
	 * @throws Exception
	 */

	public ConsultarResultadoMallaResType consultarResultadoMalla(MallaService mallaService, SolicitudVO solicitudVO, String usuarioSession) throws ServiceException {

		this.cabeceraConsultaService(solicitudVO, usuarioSession);
		co.com.sos.consultarresultadomalla.v1.CuerpoReqType cuerpoReqType = new co.com.sos.consultarresultadomalla.v1.CuerpoReqType();
		cuerpoReqType.setSolicitudesReq(solicitudesReqType);

		ConsultarResultadoMallaReqType consultarResultadoMallaReqType = new ConsultarResultadoMallaReqType();
		consultarResultadoMallaReqType.setRequestCabecera(requestCabeceraType);
		consultarResultadoMallaReqType.setCuerpo(cuerpoReqType);

		MallaPortType mallaPortType = mallaService.getMallaPort();

		ConsultarResultadoMallaResType consultarResultadoMallaResType = mallaPortType.consultarResultadoMalla(consultarResultadoMallaReqType);

		return consultarResultadoMallaResType;
	}

	/** Metodo que setea la cabecera de los servicios, para ser invocados */

	public void cabeceraConsultaService(SolicitudVO solicitudVO, String usuarioSession) {
		SeguridadType seguridadType = new SeguridadType();
		seguridadType.setClave(ConstantesEJB.CADENA_VACIA);
		seguridadType.setUsuario(usuarioSession);
		seguridadType.setSistemaOrigen(ConstantesEJB.MEDIO_CONTACTO_ASI);

		/* se adiciona la informacion de la cabecera */

		requestCabeceraType = new RequestCabeceraType();
		requestCabeceraType.setTransaccion(ConstantesEJB.TIPO_TRANSACCION_SOLICITUD);
		requestCabeceraType.setSeguridad(seguridadType);

		/* fin informacion de la cabecera */

		// <v11:solicitudReq> Se setea la informacion de la solictud creada
		SolicitudReqType solicitudReqType = new SolicitudReqType();
		solicitudReqType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getConsecutivoSolicitud()));
		solicitudReqType.setNumeroSolicitudSOS(solicitudVO.getNumeroSolicitudSOS());

		solicitudesReqType = new SolicitudesReqType();
		solicitudesReqType.getSolicitudReq().add(solicitudReqType);
	}

	// Resultado del servicio
	/**
	 * Metodo que retorna el resultado y las inconsistencias de la ejecucion de
	 * la malla
	 * 
	 * @throws Exception
	 */

	public InconsistenciasVO consultarResultadoInconsistenciasMalla(EjecutarMallaResType ejecutarMallaResType, ConsultarResultadoMallaResType consultarResultadoMallaResType) throws ServiceException {

		InconsistenciasVO inconsistenciasVO = null;
		int marcaConsulta = 0;

		if (ejecutarMallaResType != null) {
			responseCabeceraType = ejecutarMallaResType.getResponseCabecera();
			marcaConsulta = 1;
		} else {
			responseCabeceraType = consultarResultadoMallaResType.getResponseCabecera();
		}

		inconsistenciasVO = cabeceraResultadoService(responseCabeceraType);

		if (inconsistenciasVO == null) {
			if (marcaConsulta != 0) {
				CuerpoResType cuerpoResType = ejecutarMallaResType.getCuerpo();
				solicitudesResType = cuerpoResType.getSolicitudes();
			} else {
				co.com.sos.consultarresultadomalla.v1.CuerpoResType cuerpoResType = consultarResultadoMallaResType.getCuerpo();
				solicitudesResType = cuerpoResType.getSolicitudes();
			}

			inconsistenciasVO = resultadoSolicitudService(solicitudesResType);
		}

		return inconsistenciasVO;
	}

	/**
	 * Metodo que setea la cabecera del resultado del servicio y valida si se
	 * presenta inconsistencia
	 * 
	 * @throws ServiceException
	 */

	public InconsistenciasVO cabeceraResultadoService(ResponseCabeceraType cabeceraType) throws ServiceException {
		InconsistenciasVO inconsistenciasVO = null;
		if (Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(cabeceraType.getEstadoTerminacion().value())) {
			inconsistenciasVO = new InconsistenciasVO();
			ServiceErrorVO serviceErrorVO = new ServiceErrorVO();

			serviceErrorVO.setEstadoTerminacion(cabeceraType.getEstadoTerminacion().value());
			serviceErrorVO.setMensajeEstadoTerminacion(cabeceraType.getMensaje());

			ErroresServiceType erroresServiceType = new ErroresServiceType(cabeceraType.getErrores());
			serviceErrorVO = erroresServiceType.informacionErrorServicio();
			inconsistenciasVO.setServiceErrorVO(serviceErrorVO);
			if (inconsistenciasVO != null && inconsistenciasVO.getServiceErrorVO() != null) {
				throw new ServiceException(inconsistenciasVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR
						+ inconsistenciasVO.getServiceErrorVO().getCodigoError() + (char) 13 + inconsistenciasVO.getServiceErrorVO().getMensajeError(), null);
			}
		}
		return inconsistenciasVO;
	}

	/**
	 * Metodo que extrae la informacion del resultado de la invocacion al
	 * servicio (cuerpo del mensaje)
	 */

	public InconsistenciasVO resultadoSolicitudService(SolicitudesResType solicitudesResType) {
		
		PrestacionesResType lPrestaciones;
		SolicitudVO solicitudVO = null;
		PrestacionDTO prestacionDTO = null;
		InconsistenciasResponseServiceVO inconsistenciasResponseServiceVO = null;
		List<SolicitudResType> lSolicitudRes = solicitudesResType.getSolicitudRes();
		listaPrestaciones = new ArrayList<PrestacionDTO>();

		for (SolicitudResType solicitudResType : lSolicitudRes) {
			EstadoVO estadoVO = new EstadoVO();
			solicitudVO = new SolicitudVO();

			estadoVO.setDescripcionEstadoSolicitud(solicitudResType.getEstadoSolicitud());

			solicitudVO.setConsecutivoSolicitud(solicitudResType.getConsecutivo().intValue());
			solicitudVO.setNumeroSolicitudSOS(solicitudResType.getNumeroSolicitudSOS());
			solicitudVO.setEstadoVO(estadoVO);

			lPrestaciones = solicitudResType.getPrestaciones();
			List<PrestacionResType> lPrestacionResultado = lPrestaciones.getPrestacionRes();
			lMarcasInconsistenciasSolicitud = new ArrayList<MarcaInconsistenciaVO>();

			for (PrestacionResType prestacion : lPrestacionResultado) {
				MarcaInconsistenciaVO marcaInconsistenciaSolicitudVO = null;
				EstadoVO estadoSolicitudVO = new EstadoVO();
				prestacionDTO = new PrestacionDTO();
				TipoCodificacionVO tipoCodificacionVO = new TipoCodificacionVO();

				tipoCodificacionVO.setConsecutivoCodigoTipoCodificacion(Integer.parseInt(prestacion.getConsecutivoTipo()));
				tipoCodificacionVO.setCodigoTipoCodificacion(prestacion.getTipo());

				estadoSolicitudVO.setConsecutivoEstadoSolicitud(Integer.parseInt(prestacion.getConsecutivoEstado()));
				estadoSolicitudVO.setDescripcionEstadoSolicitud(prestacion.getEstado());

				prestacionDTO.setConsecutivoCodificacionPrestacion(prestacion.getConsecutivo().intValue());
				prestacionDTO.setCodigoCodificacionPrestacion(prestacion.getCodigo());
				prestacionDTO.setTipoCodificacionVO(tipoCodificacionVO);
				prestacionDTO.setEstadoVO(estadoSolicitudVO);
				prestacionDTO.setlInconsistenciasResponseServiceVO(new ArrayList<InconsistenciasResponseServiceVO>());

				MarcasType marcasType = prestacion.getMarcas();

				for (MarcaType marcaType : marcasType.getMarca()) {
					marcaInconsistenciaSolicitudVO = new MarcaInconsistenciaVO();
					marcaInconsistenciaSolicitudVO.setConsecutivoMarcaInconsistencia(marcaType.getConsecutivo().intValue());
					marcaInconsistenciaSolicitudVO.setDescripcionMarcaInconsistencia(marcaType.getNombre());
					lMarcasInconsistenciasSolicitud.add(marcaInconsistenciaSolicitudVO);
				}
				prestacionDTO.setlMarcaInconsistencia(lMarcasInconsistenciasSolicitud);

				InconsistenciasResType inconsistenciasResType = prestacion.getInconsistenciasRes();
				List<InconsistenciaResType> lInconsistencia = inconsistenciasResType.getInconsistenciaRes();
				lMarcasInconsistenciasPrestacion = new ArrayList<MarcaInconsistenciaVO>();

				for (InconsistenciaResType inconsistenciaResType : lInconsistencia) {
					inconsistenciasResponseServiceVO = new InconsistenciasResponseServiceVO();
					EstadoVO estadoPrestacionVO = new EstadoVO();

					estadoPrestacionVO.setConsecutivoEstadoSolicitud(inconsistenciaResType.getConsecutivoEstado().intValue());
					estadoPrestacionVO.setDescripcionEstadoSolicitud(inconsistenciaResType.getEstado());

					inconsistenciasResponseServiceVO.setConsecutivoPrestacion(prestacion.getConsecutivo());
					inconsistenciasResponseServiceVO.setCodigoPrestacion(prestacion.getCodigo() != null ? prestacion.getCodigo().trim() : null);
					inconsistenciasResponseServiceVO.setConsecutivoRegla(inconsistenciaResType.getConsecutivoRegla().intValue());
					inconsistenciasResponseServiceVO.setDescripcionRegla(inconsistenciaResType.getDescripcionRegla());
					inconsistenciasResponseServiceVO.setEstadoVO(estadoPrestacionVO);
					inconsistenciasResponseServiceVO.setMensaje(inconsistenciaResType.getMensaje());
					inconsistenciasResponseServiceVO.setInformacionAdicional(inconsistenciaResType.getInformacionAdicional());
					inconsistenciasResponseServiceVO.setConsecutivoTipoValidacion(inconsistenciaResType.getConsecutivoTipoValidacion().intValue());
					inconsistenciasResponseServiceVO.setDescripcionTipoValidacion(inconsistenciaResType.getTipoValidacion());

					MarcasType marcasPrestacionType = inconsistenciaResType.getMarcas();

					buscarMarcaInconsistencia(marcasPrestacionType);
					inconsistenciasResponseServiceVO.setlMarcaInconsistencia(lMarcasInconsistenciasPrestacion);

					prestacionDTO.getlInconsistenciasResponseServiceVO().add(inconsistenciasResponseServiceVO);
				}
				listaPrestaciones.add(prestacionDTO);
			}
		}
		InconsistenciasVO inconsistenciasVO = new InconsistenciasVO();
		inconsistenciasVO.setSolicitudVO(solicitudVO);
		inconsistenciasVO.setListaPrestaciones(listaPrestaciones);
		return inconsistenciasVO;
	}

	private void buscarMarcaInconsistencia(MarcasType marcasPrestacionType) {
		MarcaInconsistenciaVO marcaInconsistenciaPrestacionVO;
		for (MarcaType marcaType : marcasPrestacionType.getMarca()) {
			marcaInconsistenciaPrestacionVO = new MarcaInconsistenciaVO();
			marcaInconsistenciaPrestacionVO.setConsecutivoMarcaInconsistencia(marcaType.getConsecutivo().intValue());
			marcaInconsistenciaPrestacionVO.setDescripcionMarcaInconsistencia(marcaType.getNombre());
			lMarcasInconsistenciasPrestacion.add(marcaInconsistenciaPrestacionVO);
		}
	}

	/**
	 * Metodo que permite realizar la conexion al servicio de malla
	 * 
	 * @throws Exception
	 */

	public MallaService crearSolicitudService() throws LogicException {
		String wsdlLocation;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();
		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_GESTION_MALLA));
		
		try {
			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);
			mallaService = new MallaService(new URL(wsdlLocation));
		
		}catch (MalformedURLException e){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_MALLA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_MALLA_SERVICE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		return mallaService;
	}
	
	
}
