package com.sos.gestionautorizacionessaludejb.bean.impl;

import java.net.MalformedURLException;
import java.net.URL;
import co.com.sos.RequestCabeceraType;
import co.com.sos.ResponseCabeceraType;
import co.com.sos.enterprise.inicioinstancia.v1.InicioInstanciaPortType;
import co.com.sos.enterprise.inicioinstancia.v1.InicioInstanciaService;
import co.com.sos.inicioinstancia.v1.CuerpoReqType;
import co.com.sos.inicioinstancia.v1.InicioInstanciaReqType;
import co.com.sos.inicioinstancia.v1.InicioInstanciaResType;
import co.com.sos.inicioinstancia.v1.MensajeType;
import co.eps.sos.service.exception.ServiceException;

import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.model.IniciarInstanciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaluddata.model.malla.SolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.gestionautorizacionessaludejb.bean.IIniciarInstanciaBpmService;
import com.sos.gestionautorizacionessaludejb.service.controller.CabeceraServiceType;
import com.sos.gestionautorizacionessaludejb.service.controller.ErroresServiceType;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConsultaProperties;
import com.sos.gestionautorizacionessaludejb.util.ConsultarPropertyQueryEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;


/**
 * Class IniciarInstanciaBpmService
 * Clase que implementa el servicio que inicia la instancia en el bpm
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class IniciarInstanciaBpmService implements IIniciarInstanciaBpmService {
	
	private InicioInstanciaService service;	
		
	/** Metodo que crea el servicio*/	
	@Override
	public InicioInstanciaService crearInicioInstanciaService()	throws LogicException {
		
		String wsdlLocation;
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();

		PropiedadVO propiedad = new PropiedadVO();

		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));
		propiedad.setClasificacion(ConsultaProperties.getString(ConstantesEJB.CLASIFICACION_GESTION_SERVICE));
		propiedad.setPropiedad(ConsultaProperties.getString(ConstantesEJB.PROPIEDAD_INSTANCIA_SERVICE));
		try {
			wsdlLocation = propertyQuery.consultarPropiedad(propiedad);			
			service = new InicioInstanciaService(new URL(wsdlLocation));
			
		} catch (MalformedURLException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_INSTANCIA_SERVICE_BPM), e, LogicException.ErrorType.DATO_NO_EXISTE);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_INSTANCIA_SERVICE_BPM), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
		return service;
	}

	/** Metodo que consume el servicio para iniciar la instancia*/	
	@Override
	public InicioInstanciaResType inicioInstanciaService(InicioInstanciaService service, SolicitudVO solicitudVO, String usuarioSession) throws LogicException {
		
		InicioInstanciaPortType portType = null;
		InicioInstanciaResType inicioInstanciaResType = null;
				
		/* se adiciona la informacion de la cabecera */
		CabeceraServiceType cabeceraServiceType = new  CabeceraServiceType(usuarioSession);
		RequestCabeceraType requestCabeceraType = cabeceraServiceType.informacionCabeceraServiceType();
		/* fin informacion de la cabecera */
		
		/* se adiciona la informacion del cuerpo */
		CuerpoReqType cuerpoReqType = new CuerpoReqType();
		cuerpoReqType.setConsecutivo(solicitudVO.getConsecutivoSolicitud());
		cuerpoReqType.setNumeroSolicitud(solicitudVO.getNumeroSolicitudSOS());
		cuerpoReqType.setUsuarioProceso(usuarioSession);		
		/* fin informacion del cuerpo */
		
		InicioInstanciaReqType inicioInstanciaReqType = new InicioInstanciaReqType();			
		inicioInstanciaReqType.setRequestCabecera(requestCabeceraType);		
		inicioInstanciaReqType.setCuerpo(cuerpoReqType);
		
		
		try {
			portType = service.getInicioInstanciaPort();		
			inicioInstanciaResType = portType.iniciarInstancia(inicioInstanciaReqType);
		} catch (Exception e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_INSTANCIA_SERVICE_BPM), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		return inicioInstanciaResType;		
	}

	@Override
	public IniciarInstanciaVO resultadoInicioInstanciaService(InicioInstanciaResType inicioInstanciaResType, String usuarioSession) throws ServiceException {
		
		IniciarInstanciaVO iniciarInstanciaVO = new IniciarInstanciaVO();
		ResponseCabeceraType responseCabeceraType = inicioInstanciaResType.getResponseCabecera();
		
		if (responseCabeceraType.getEstadoTerminacion() != null && Messages.getValorParametro(ConstantesEJB.ERROR_SERVICE).equals(responseCabeceraType.getEstadoTerminacion().value())) {
			ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
			
			serviceErrorVO.setEstadoTerminacion(responseCabeceraType.getEstadoTerminacion().value());
			serviceErrorVO.setMensajeEstadoTerminacion(responseCabeceraType.getMensaje());

			ErroresServiceType erroresServiceType = new ErroresServiceType(responseCabeceraType.getErrores());
			serviceErrorVO = erroresServiceType.informacionErrorServicio();			
			
			iniciarInstanciaVO.setServiceErrorVO(serviceErrorVO);			
			
			if (iniciarInstanciaVO.getServiceErrorVO() != null) {
				throw new ServiceException(iniciarInstanciaVO.getServiceErrorVO().getEstadoTerminacion() + ConstantesEJB.SEPARADOR_MENSAJE_ERROR + iniciarInstanciaVO.getServiceErrorVO().getCodigoError() + (char) 13	+ iniciarInstanciaVO.getServiceErrorVO().getMensajeError(), null);
			}
		}else{
			co.com.sos.inicioinstancia.v1.CuerpoResType cuerpoResType = inicioInstanciaResType.getCuerpo();
			MensajeType resultado = cuerpoResType.getResultado();
			
			iniciarInstanciaVO.setResultadoCodigo(resultado.getCodigo());
			iniciarInstanciaVO.setResultadoMensaje(resultado.getMensaje());			
		}
		
		return iniciarInstanciaVO;
	}

}
