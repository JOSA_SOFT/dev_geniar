package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.consultarsolicitud.SpASConsultarMotivosCausasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionAntescedentesPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionGestionAuditorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionMotivosPrestacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASGuardarInformacionCambioFechaDomiDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASGuardarInformacionDireccionamientoDomiDelegate;
import com.sos.gestionautorizacionessaluddata.model.AntecedentesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.GestionesPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DireccionamientoVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.MotivoPrestacionVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class GestionInconsistenciasController
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class PrestacionController {

	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;

    /**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public PrestacionController() throws ConnectionProviderException, IOException{			
		if(connProviderSalud == null){
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar los motivos de la prestacion
	 * @return
	 * @throws LogicException
	 */
	public List<MotivoPrestacionVO> consultarMotivosPrestacion() throws LogicException{
		SpASConsultaDatosInformacionMotivosPrestacionDelegate delegate = new SpASConsultaDatosInformacionMotivosPrestacionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVOS_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(delegate.getLstMotivoPrestacionVO() == null || delegate.getLstMotivoPrestacionVO().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVOS_PRESTACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getLstMotivoPrestacionVO();
	}


	/**
	 * Metodo para consultar los motivos 
	 * @return
	 * @throws LogicException
	 */
	public List<MotivoCausaVO> consultarMotivosDireccionamiento(Integer tipoMotivo) throws LogicException{
		SpASConsultarMotivosCausasDelegate delegate = new SpASConsultarMotivosCausasDelegate(tipoMotivo);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVOS_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(delegate.getlstMotivoCausaVO() == null || delegate.getlstMotivoCausaVO().isEmpty()){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MOTIVOS_PRESTACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getlstMotivoCausaVO();
	}
	
	/**
	 * Metodo para consultar los antecedentes de la prestacion
	 * @param consecutivoSolicitud
	 * @param consecutivoPrestacion
	 * @return
	 * @throws LogicException
	 */
	public List<AntecedentesPrestacionVO> consultarAntecedentesPrestacion(Integer consecutivoSolicitud, Integer consecutivoPrestacion) throws LogicException{
		SpASConsultaDatosInformacionAntescedentesPrestacionDelegate delegate = new SpASConsultaDatosInformacionAntescedentesPrestacionDelegate(consecutivoSolicitud, consecutivoPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ANTECEDENTES_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	
		return delegate.getLstAntecedentesPrestacionVO();
		
	}

	/**
	 * Metodo para consultar las gestiones de la prestacion
	 * @param consecutivoSolicitud
	 * @param consecutivoPrestacion
	 * @return
	 * @throws LogicException
	 */
	public List<GestionesPrestacionVO> consultarGestionesPrestacion(Integer consecutivoPrestacion) throws LogicException{
		SpASConsultaDatosInformacionGestionAuditorDelegate delegate = new SpASConsultaDatosInformacionGestionAuditorDelegate(consecutivoPrestacion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GESTION_PRESTACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return delegate.getLstGestionesPrestacionVO();
	}

	/**
	 * Metodo para cambiar la fecha del direccionamiento
	 * @param direccionamientoVO
	 * @param consecutivoPrestacion
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public void guardarCambioFechaDireccionamiento(DireccionamientoVO direccionamientoVO, Integer consecutivoPrestacion, String usuario) throws LogicException {
		
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlDireccionamiento(direccionamientoVO, consecutivoPrestacion, usuario);
		SpASGuardarInformacionCambioFechaDomiDelegate delegate = new SpASGuardarInformacionCambioFechaDomiDelegate(xml);
		
		try {						
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_CAMBIO_DIRECCIONAMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
	}

	/**
	 * Metodo para cambiar el direccionamiento
	 * @param direccionamientoVO
	 * @param consecutivoPrestacion	
	 * @throws LogicException
	 */
	public void guardarDireccionamiento(DireccionamientoVO direccionamientoVO, Integer consecutivoPrestacion, String usuario) throws LogicException {
		
		ConvertidorXML convertidorXML = new ConvertidorXML();
		StringBuilder xml = convertidorXML.convertXmlDireccionamiento(direccionamientoVO, consecutivoPrestacion, usuario);
		try {
			SpASGuardarInformacionDireccionamientoDomiDelegate delegate = new SpASGuardarInformacionDireccionamientoDomiDelegate(xml);
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_DIRECCIONAMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}
}
