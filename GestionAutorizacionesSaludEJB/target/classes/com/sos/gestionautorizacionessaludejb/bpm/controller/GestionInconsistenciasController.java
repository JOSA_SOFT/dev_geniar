package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestioninconsistencias.SpASConsultaDatosInformacionInconsistenciasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestioninconsistencias.SpASConsultaDatosSolicitudServicioDelegate;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;


/**
 * Class GestionInconsistenciasController
 * @author Julian Hernandez
 * @version 22/02/2016
 *
 */
public class GestionInconsistenciasController {
	
	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;
	
	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public GestionInconsistenciasController() throws ConnectionProviderException, IOException{		
		
		if(connProviderSalud == null){
		    connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar los datos del afiliado y de una solicitud especifica
	 * @param numeroSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<Object> consultaAfiliadoYSolicitud(int numeroSolicitud) throws LogicException{
		
		SpASConsultaDatosSolicitudServicioDelegate afiliadoYSolicitud = new SpASConsultaDatosSolicitudServicioDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, afiliadoYSolicitud);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SOLICITUD), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if(afiliadoYSolicitud.getListAfiliadoYSolicitud().isEmpty() || afiliadoYSolicitud.getListAfiliadoYSolicitud() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return afiliadoYSolicitud.getListAfiliadoYSolicitud();		
	}
	
	/**
	 * Metodo para consultar las inconsistencias
	 * @param numeroSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultaIncosistencias(int numeroSolicitud, int estado) throws LogicException{
		
		SpASConsultaDatosInformacionInconsistenciasDelegate listaInconsistencias = new SpASConsultaDatosInformacionInconsistenciasDelegate(numeroSolicitud, estado);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaInconsistencias);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_INCONSISTENCIAS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
				
		return listaInconsistencias.getListaInconsistencias();		
	}	
}
