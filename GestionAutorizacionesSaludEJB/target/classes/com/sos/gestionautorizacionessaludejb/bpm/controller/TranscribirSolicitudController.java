package com.sos.gestionautorizacionessaludejb.bpm.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud.SpASConsultaDatosInformacionPreSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud.SpASConsultaTiposDocumentoSoportePresolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud.SpAsActualizarDatosDevolucionPresolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud.SpINFConsultarSoportesPreSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.transcribirsolicitud.SpPTLConsultarCausalesNegacionPreSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.model.SoporteVO;
import com.sos.gestionautorizacionessaluddata.model.bpm.DevolucionVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.AfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.MotivoCausaVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class TranscribirSolicitudController
 * @author Rafael Cano
 * @version 08/07/2016
 *
 */
public class TranscribirSolicitudController {
	
	/**
	 * Proveedor de conexion
	 */
	private ConnectionProvider connProviderSalud;
	
	/**
	 * Constructor
	 * @throws ConnectionProviderException
	 * @throws IOException
	 */
	public TranscribirSolicitudController() throws ConnectionProviderException, IOException{		
		
		if(connProviderSalud == null){
		    connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}	

	/**
	 * Metodo para consultar los documentos adjuntos de la preSolicitud
	 * @param numeroPreSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<SoporteVO> consultaListaDocumentos(int numeroPreSolicitud) throws LogicException{
		
		SpINFConsultarSoportesPreSolicitudDelegate listaDocumentos = new SpINFConsultarSoportesPreSolicitudDelegate(numeroPreSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaDocumentos);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SOPORTES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return listaDocumentos.getListaDocumentos();		
	}
	
	/**
	 * Metodo para consultar la informacion del afiliado en la presolicitud
	 * @param numeroPreSolicitud
	 * @param numeroInstancia
	 * @return
	 * @throws LogicException
	 */
	public List<AfiliadoVO> consultarInformacionPreSolicitud(int numeroPreSolicitud, String numeroInstancia) throws LogicException{
		
		SpASConsultaDatosInformacionPreSolicitudDelegate afiliado = new SpASConsultaDatosInformacionPreSolicitudDelegate(numeroPreSolicitud, numeroInstancia);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, afiliado);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		if(afiliado.getListaAfiliados().isEmpty() || afiliado.getListaAfiliados() == null){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_AFILIADO_SOLICITUD), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return afiliado.getListaAfiliados();		
	}
	
	/**
	 * Metodo para guardar la informacion de la presolicitud
	 * @param numeroPreSolicitud
	 * @return
	 * @throws LogicException
	 */
	public void guardarDevolucion(DevolucionVO devolucion) throws LogicException{
		
		SpAsActualizarDatosDevolucionPresolicitudDelegate guardarDevolucion = new SpAsActualizarDatosDevolucionPresolicitudDelegate(devolucion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, guardarDevolucion);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_GUARDAR_DEVOLUCION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		
	}
	
	/**
	 * Metodo para consultar los tipos de soporte de una presolicitud
	 * @param tipoDocuemnto
	 * @return
	 * @throws LogicException
	 */
	public List<SelectItem> consultarListaTipoDocumentoSoportes(int tipoDocuemnto) throws LogicException{
		
		SpASConsultaTiposDocumentoSoportePresolicitudDelegate listaTipoDocumentos = new SpASConsultaTiposDocumentoSoportePresolicitudDelegate(tipoDocuemnto);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaTipoDocumentos);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_SOPORTES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		return listaTipoDocumentos.getListaTipoDocumentos();
	}
	
	/**
	 * Metodo para consultar los motivos 
	 * @return
	 * @throws LogicException
	 */
	public List<MotivoCausaVO> consultarMotivosTranscripcion() throws LogicException{
		Date fecha = Utilidades.getFechaActual();
		SpPTLConsultarCausalesNegacionPreSolicitudDelegate listaMotivos = new SpPTLConsultarCausalesNegacionPreSolicitudDelegate(fecha);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, listaMotivos);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_SOPORTES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}	
		return listaMotivos.getListaMotivosTranscribir();
	}
	
	
}
