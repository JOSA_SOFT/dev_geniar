package com.sos.gestionautorizacionessaludejb.controller;

import java.io.Serializable;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerInformacionOPSValorDelegate;
import com.sos.gestionautorizacionessaluddata.model.caja.OPSCajaVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;

/**
 * 
 * @author JOSÉ OLMEDO SOTO AGUIRRE, GENIAR S.A.S 28/10/2016
 *
 */
public class ObtenerInformacionOpsValorController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ConnectionProvider connProviderCaja;

	/**
	 * CONSTRUCTOR PRINCIPAL DE LA CLASE, INICIALIZA LA CONEXIÓN.
	 * 
	 * @throws ConnectionProviderException
	 */
	public ObtenerInformacionOpsValorController()
			throws ConnectionProviderException {
		if (connProviderCaja == null) {
			connProviderCaja = ConnProviderSiSalud.getConnProvider();
		}
	}

	/**
	 * INVOCA EL DELEGADO ENCARGADO DE PROCESAR LA INFORMACIÓN.
	 * 
	 * @param numSolicitud
	 *            NÚMERO DE SOLICITUD A BUSCAR
	 * @return INFORMACIÓN CON EL NÚMERO DE OPS Y EL VALOR
	 * @throws DataAccessException
	 */
	public OPSCajaVO obtenerInformacionOPSValor(int numSolicitud)
			throws DataAccessException {
		SpRCObtenerInformacionOPSValorDelegate delegate = new SpRCObtenerInformacionOPSValorDelegate(
				numSolicitud);
		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);
		return delegate.getOps();
	}

}
