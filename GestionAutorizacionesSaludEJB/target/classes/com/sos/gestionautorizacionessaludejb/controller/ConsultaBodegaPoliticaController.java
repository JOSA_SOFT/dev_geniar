package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpASConsultaPrestacionesTipoWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaAtributosPrestacion;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaConveniosIpsWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaDatosBasicosPrestacionWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaPagosFijosWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaPrestacionPorPlanesWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConsultaTiemposEntregaWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPConveniosPrestacion;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPCupsXPlanXCargo;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPCupsXRiesgo;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPDatosAdicionalesCums;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPDireccionamientoExcepciones;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPDireccionamientoNormal;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPDireccionamientoXRiesgo;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPHomologo;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPMarcasPrestacionWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpBDPPoliticaAuthPrestacionWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.bodegapoliticas.SpTraerTiposCodificacionessxTipoManual;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.AtributosPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioIpsDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.ConvenioPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXPlanXCargoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.CupsXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosAdicionalesCumsDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DatosBasicosBodegaPoliticaDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoExcepcionesDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoNormalDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.DireccionamientoXRiesgoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.HomologacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.MarcasPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PagoFijoDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PoliticasAuthPrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.PrestacionPorPlanesDTO;
import com.sos.gestionautorizacionessaluddata.model.dto.bodegapolitica.TiempoEntregaDTO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

import co.eps.sos.dataccess.exception.DataAccessException;

public class ConsultaBodegaPoliticaController {
	
	private ConnectionProvider connProviderSalud;

	public ConsultaBodegaPoliticaController() throws ConnectionProviderException, IOException {

		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}

	// Funcion que obtiene los items de los tipos de codificaciones
	public List<SelectItem> obtenerListaSelectTipoPrestacionesBodegaPolit(List<TipoCodificacionVO> listaParam,
			int validacionCuos) throws LogicException {
		List<TipoCodificacionVO> listaVo = null;

		if (listaParam == null || listaParam.isEmpty()) {
			listaVo = consultaTiposCodificacion(validacionCuos);
		}
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (TipoCodificacionVO vo : listaVo) {
			item = new SelectItem(vo.getConsecutivoCodigoTipoCodificacion(), vo.getDescripcionTipoCodificacion());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Funcion que consulta los tipos de codificaciones
	public List<TipoCodificacionVO> consultaTiposCodificacion(int validacionCuos) throws LogicException {
		SpTraerTiposCodificacionessxTipoManual spTraerTiposCodificacionessxTipoManual = new SpTraerTiposCodificacionessxTipoManual(
				validacionCuos);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, spTraerTiposCodificacionessxTipoManual);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_CODIFICACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return spTraerTiposCodificacionessxTipoManual.getlTipoCodificacion();
	}

	/** Funcion que permite la consulta de las prestaciones para la solicitud */
	public List<PrestacionDTO> consultaInformacionPrestacionesxTipo(Integer tipoPrestacion, String codigoPrestacion,
			String descripcionPrestacion, Integer cupsNorma) throws LogicException {
		SpASConsultaPrestacionesTipoWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpASConsultaPrestacionesTipoWebDelegate(
				tipoPrestacion, codigoPrestacion, descripcionPrestacion, cupsNorma);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getlPrestacionesDTO();
	}	
	

	/** Funcion que permite la consulta de los datos basicos de la prestacion */
	public DatosBasicosBodegaPoliticaDTO consultaInformacionDatosBasicosPrestacion(Integer consecutivoPrestacion,
			Integer tipoPrestsacion) throws LogicException {
		SpBDPConsultaDatosBasicosPrestacionWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpBDPConsultaDatosBasicosPrestacionWebDelegate(
				tipoPrestsacion, consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getDatosBasicos();
	}

	/** Funcion que permite la consulta la prestacion por planes */
	public List<PrestacionPorPlanesDTO> consultaInformacionPrestacionPorPlanes(Integer consecutivoPrestacion,
			Integer tipoPrestsacion) throws LogicException {
		SpBDPConsultaPrestacionPorPlanesWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpBDPConsultaPrestacionPorPlanesWebDelegate(
				tipoPrestsacion, consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getDatos();
	}

	/** Funcion que permite la consulta los tiempos de entrega */
	public List<TiempoEntregaDTO> consultaInformacionTiemposEntrega(Integer consecutivoPrestacion)
			throws LogicException {
		SpBDPConsultaTiemposEntregaWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpBDPConsultaTiemposEntregaWebDelegate(
				consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getDatosBasicos();
	}

	/** Funcion que permite la consulta las marcas de prestacion */
	public List<MarcasPrestacionDTO> consultaMarcasPrestacion(Integer consecutivoPrestacion) throws LogicException {
		SpBDPMarcasPrestacionWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpBDPMarcasPrestacionWebDelegate(
				consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getDatosBasicos();
	}

	/** Funcion que permite las politicas de autorizacion de prestacion */
	public List<PoliticasAuthPrestacionDTO> consultaAuthPrestacion(Integer consecutivoPrestacion)
			throws LogicException {
		SpBDPPoliticaAuthPrestacionWebDelegate consultaPrestacionesSolicitudWebDelegate = new SpBDPPoliticaAuthPrestacionWebDelegate(
				consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getDatosBasicos();
	}

	/** Funcion que permite los convenios de las prestaciones */
	public List<ConvenioPrestacionDTO> consultaConveniosPrestacion(Integer cnsctvoCdgoTpoCdfccn, Integer cnsctvoCdgoPln,
			Integer cnsctvoCdfccn, Integer cnsctvoCdgoCdd, String cdgoIntrno) throws LogicException {
		SpBDPConveniosPrestacion consultaPrestacionesSolicitudWebDelegate = new SpBDPConveniosPrestacion(
				cnsctvoCdgoTpoCdfccn, cnsctvoCdgoPln, cnsctvoCdfccn, cnsctvoCdgoCdd, cdgoIntrno);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPrestacionesSolicitudWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPrestacionesSolicitudWebDelegate.getlPrestacionesDTO();
	}

	/** Funcion que consulta el direccionamiento normal */
	public List<DireccionamientoNormalDTO> consultaDireccionamientoNormal(Integer cnsctvoCdfccn, Integer cnsctvoCdad)
			throws LogicException {
		SpBDPDireccionamientoNormal sp = new SpBDPDireccionamientoNormal(cnsctvoCdfccn, cnsctvoCdad);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion consutla el direccionamiento por riesgo */
	public List<DireccionamientoXRiesgoDTO> consultaDireccionamientoXRiesgo(Integer cnsctvoCdfccn, Integer cnsctvoCdad)
			throws LogicException {
		SpBDPDireccionamientoXRiesgo sp = new SpBDPDireccionamientoXRiesgo(cnsctvoCdfccn, cnsctvoCdad);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion consulta el direccionamiento de excepciones */
	public List<DireccionamientoExcepcionesDTO> consultaDireccionamientoExcepcion(Integer cnsctvoCdfccn)
			throws LogicException {
		SpBDPDireccionamientoExcepciones sp = new SpBDPDireccionamientoExcepciones(cnsctvoCdfccn);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion consulta el direccionamiento de excepciones */
	public List<HomologacionDTO> consultaHomologo(Integer cnsctvoCdfccn, Integer cnsctvoCdd, String cdgoInterno)
			throws LogicException {
		SpBDPHomologo sp = new SpBDPHomologo(cnsctvoCdfccn, cnsctvoCdd, cdgoInterno);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion que consulta los cups por riesgo */
	public List<CupsXRiesgoDTO> consultaCupsXRiesgo(Integer cnsctvoCdfccn) throws LogicException {
		SpBDPCupsXRiesgo sp = new SpBDPCupsXRiesgo(cnsctvoCdfccn);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion que consulta los cups pr plan por cargo */
	public List<CupsXPlanXCargoDTO> consultaCupsXPlanXCargo(Integer cnsctvoCdfccn) throws LogicException {
		SpBDPCupsXPlanXCargo sp = new SpBDPCupsXPlanXCargo(cnsctvoCdfccn);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}

	/** Funcion que consulta los datos adicionales de cums */
	public DatosAdicionalesCumsDTO consultaDatosCums(Integer cnsctvoCdfccn) throws LogicException {
		SpBDPDatosAdicionalesCums sp = new SpBDPDatosAdicionalesCums(cnsctvoCdfccn);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, sp);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return sp.getlDto();
	}
	
	/** Funcion que permite la consulta los atributos de la prestacion*/
	public List<AtributosPrestacionDTO> consultaAtributosPrestacion(Integer consecutivoPrestacion) throws LogicException {
		SpBDPConsultaAtributosPrestacion consultaAtributosPrestacionWebDelegate = new SpBDPConsultaAtributosPrestacion(consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaAtributosPrestacionWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESTACIONES_SOLICITUD), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaAtributosPrestacionWebDelegate.getDatos();
	}
	
	public List<ConvenioIpsDTO> consultaConvenioIps(String descPrestacion, Integer tipoPrestacion, Integer cupsNorma,
			String codigoInternoPrestador, Integer consecutivoCodigoCiudad) throws LogicException {
		SpBDPConsultaConveniosIpsWebDelegate consultaConveniosIpsDelegate = 
				new SpBDPConsultaConveniosIpsWebDelegate(descPrestacion, tipoPrestacion, cupsNorma, codigoInternoPrestador, consecutivoCodigoCiudad);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaConveniosIpsDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CONVENIOS_ISP), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaConveniosIpsDelegate.getlDto();
	}
	
	public List<PagoFijoDTO> consultaPagosFijos(Integer cnsctvo_prstcn, Integer cnsctvo_cdd, Integer cnsctvo_cdgo_pln,
			String cdgo_intrno_prestador) throws LogicException{
		
		if( cdgo_intrno_prestador == null && cnsctvo_cdd == null ){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PAGOS_FIJOS_PARAMETROS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		SpBDPConsultaPagosFijosWebDelegate consultaPagosWebDelegate = 
				new SpBDPConsultaPagosFijosWebDelegate(cnsctvo_prstcn, cnsctvo_cdd, cnsctvo_cdgo_pln, cdgo_intrno_prestador);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPagosWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PAGOS_FIJOS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPagosWebDelegate.getlDto();
	}
}
