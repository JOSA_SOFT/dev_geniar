package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpASConsultaMediosContactoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerTipoCodificacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpASConsultaMarcaAccesoDirectoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpASConsultaTiposDocumentoSoporteDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpASConsultaUbicacionPacienteDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpASConsultarcodigoHabilitacionbyCodigoInterno;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpASValidarPerfilCordinadorDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsActualizaPrestacionesAltaFrecuenciaBajoCostoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsConsultaMedicamentosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsConsultaProcedimientosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsConsultaSalarioMinimoLVDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsDevolverSevicioSolicitadoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpConsultaMedicamentosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpConsultaProcedimientosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpDMTraerEspecialidadesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpMNSeleccionarTipoIpsDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpMNTraerCiudadesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpMNTraerIpsMedicoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpPMRelacionContingenciaRecobroDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpTraerContingenciasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpTraerDiagnosticosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.registrasolicitud.SpAsConsultaResultadoMallaDelegate;
import com.sos.gestionautorizacionessaluddata.model.DocumentoSoporteVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.CiudadVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.malla.InconsistenciasResponseServiceVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.MedioContactoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoCodificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciaRecobroVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ContingenciasVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.DiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.EspecialidadVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.MedicamentosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.ProcedimientosVO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.TipoIPSVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.ConvertidorXML;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;
import com.sos.ips.modelo.IPSVO;

/**
 * Class ConsultaSolicitudesController Clase controladora que permite consultar
 * los delegates de las consultas asociadas a la radicacion de la solicitud
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/11/2015
 *
 */
public class RegistrarSolicitudController {

	private Date fechaConsulta;
	private StringBuilder xml;
	private ConnectionProvider connProviderSalud;	
	

	public RegistrarSolicitudController() throws ConnectionProviderException, IOException {
		fechaConsulta = Utilidades.getFechaActual();

		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}

	public List<UbicacionPacienteVO> consultaUbicacionPacientexClaseAtencion(Integer consecutivoClaseAtencion) throws LogicException{
		SpASConsultaUbicacionPacienteDelegate consultaUbicacionPacienteDelegate = new SpASConsultaUbicacionPacienteDelegate(fechaConsulta, consecutivoClaseAtencion);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaUbicacionPacienteDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_UBICACION_PACIENTE_CLASE), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return consultaUbicacionPacienteDelegate.getlUbicacionPaciente();
	}
	
	
	// Metodo que consulta los diagnosticos por codigo
	public List<DiagnosticosVO> consultaDiagnosticosxCodigo(String codigoDiagnostico) throws LogicException {

		String descripcionDiagnostico = "";
		SpTraerDiagnosticosDelegate diagnosticosDelegate = new SpTraerDiagnosticosDelegate(fechaConsulta, codigoDiagnostico, descripcionDiagnostico);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, diagnosticosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if (diagnosticosDelegate.getlDiagnosticos().isEmpty() || diagnosticosDelegate.getlDiagnosticos() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return diagnosticosDelegate.getlDiagnosticos();
	}

	// Metodo que consulta los diagnosticos por descripcion
	public List<DiagnosticosVO> consultaDiagnosticosxDescripcion(String descripcionDiagnostico) throws LogicException {

		String codigoDiagnostico = "";
		SpTraerDiagnosticosDelegate diagnosticosDelegate = new SpTraerDiagnosticosDelegate(fechaConsulta, codigoDiagnostico, descripcionDiagnostico);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, diagnosticosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if (diagnosticosDelegate.getlDiagnosticos().isEmpty() || diagnosticosDelegate.getlDiagnosticos() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DIAGNOSTICOS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return diagnosticosDelegate.getlDiagnosticos();
	}

	// Metodo que consulta las contingencias asociadas a los diagnosticos
	public List<ContingenciasVO> consultaContingencias(int consecutivoCodigoContingencia) throws DataAccessException, LogicException {

		SpTraerContingenciasDelegate contingenciasDelegate = new SpTraerContingenciasDelegate(fechaConsulta, consecutivoCodigoContingencia);
		SOSDataAccess.ejecutarSQL(connProviderSalud, contingenciasDelegate);

		if (contingenciasDelegate.getlContingencias().isEmpty() || contingenciasDelegate.getlContingencias() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CONTINGENCIAS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return contingenciasDelegate.getlContingencias();
	}

	// Metodo que obtiene los items de los recobros asociadas a las
	// contingencias de los diagnosticos
	public List<SelectItem> obtenerListaSelectRelContingenciasRecobros(List<ContingenciaRecobroVO> listaVos) {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ContingenciaRecobroVO vo : listaVos) {
			item = new SelectItem(vo.getConsecutivoCodigoRecobro(), vo.getDescripcionRecobro());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que consulta los recobros asociadas a las contingencias de los
	// diagnosticos
	public List<ContingenciaRecobroVO> consultaRelContingenciasRecobros(int consecutivoCodigoContingencia) throws LogicException {
		SpPMRelacionContingenciaRecobroDelegate contingenciaRecobroDelegate = new SpPMRelacionContingenciaRecobroDelegate(consecutivoCodigoContingencia);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, contingenciaRecobroDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (contingenciaRecobroDelegate.getlRelContingenciaRecobros().isEmpty() || contingenciaRecobroDelegate.getlRelContingenciaRecobros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RELACION_CONTINGENCIA_RECOBROS), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return contingenciaRecobroDelegate.getlRelContingenciaRecobros();
	}

	// Metodo que consulta las ipsa asociadas medico
	public List<IPSVO> consultaIPSMedico(int numeroUnicoIdentificacionMedico) throws LogicException {
		SpMNTraerIpsMedicoDelegate ipsMedicoDelegate = new SpMNTraerIpsMedicoDelegate(numeroUnicoIdentificacionMedico);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, ipsMedicoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_IPS_MEDICO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (ipsMedicoDelegate.getlIPSMedico().isEmpty() || ipsMedicoDelegate.getlIPSMedico() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_IPS_MEDICO), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return ipsMedicoDelegate.getlIPSMedico();
	}

	// Metodo que consulta los procedimientos por codigo codificacion
	public List<ProcedimientosVO> consultaProcedimientosxCodigoIps(String codigoCodificacionProcedimiento, IPSVO ipsVo, String codigoHabilitacion, int consPlan) throws LogicException {

		String descripcionCodificacionProcedimiento = ConstantesEJB.CADENA_VACIA;
		SpAsConsultaProcedimientosDelegate consultaProcedimientosDelegate = new SpAsConsultaProcedimientosDelegate(fechaConsulta, codigoCodificacionProcedimiento, descripcionCodificacionProcedimiento, ipsVo.getCodigoTipoId(), Utilidades.validarNumeroIdentificacionIPS(ipsVo), codigoHabilitacion, consPlan);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaProcedimientosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROCEDIMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaProcedimientosDelegate.getlProcedimientos();
	}

	// Metodo que consulta los procedimientos por descripcion
	public List<ProcedimientosVO> consultaProcedimientosxDescripcionIps(String descripcionCodificacionProcedimiento, IPSVO ipsVo,String codigoHabilitacion, int consPlan) throws LogicException {

		String codigoCodificacionProcedimiento = ConstantesEJB.CADENA_VACIA;
		SpAsConsultaProcedimientosDelegate consultaProcedimientosDelegate = new SpAsConsultaProcedimientosDelegate(fechaConsulta, codigoCodificacionProcedimiento, descripcionCodificacionProcedimiento, ipsVo.getCodigoTipoId(), Utilidades.validarNumeroIdentificacionIPS(ipsVo), codigoHabilitacion, consPlan);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaProcedimientosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROCEDIMIENTO_DES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		

		return consultaProcedimientosDelegate.getlProcedimientos();
	}

		
	// Metodo que consulta los procedimientos por codigo codificacion
	public List<ProcedimientosVO> consultaProcedimientosxCodigo(String codigoCodificacionProcedimiento) throws LogicException {

		String descripcionCodificacionProcedimiento = ConstantesEJB.CADENA_VACIA;
		SpConsultaProcedimientosDelegate consultaProcedimientosDelegate = new SpConsultaProcedimientosDelegate(fechaConsulta, codigoCodificacionProcedimiento, descripcionCodificacionProcedimiento);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaProcedimientosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROCEDIMIENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaProcedimientosDelegate.getlProcedimientos();
	}

	public List<ProcedimientosVO> consultaProcedimientosxDescripcion(String descripcionCodificacionProcedimiento) throws LogicException {

		String codigoCodificacionProcedimiento = ConstantesEJB.CADENA_VACIA;
		SpConsultaProcedimientosDelegate consultaProcedimientosDelegate = new SpConsultaProcedimientosDelegate(fechaConsulta, codigoCodificacionProcedimiento, descripcionCodificacionProcedimiento);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaProcedimientosDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PROCEDIMIENTO_DES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}		

		return consultaProcedimientosDelegate.getlProcedimientos();
	}

	// Metodo que consulta los medicamentos por codigo
		public List<MedicamentosVO> consultaMedicamentosxCodigoIps(String codigoCodificacionMedicamento, IPSVO ipsVo,String codigoHabilitacion, int consPlan) throws LogicException {

			String descripcionCodificacionMedicamento = ConstantesEJB.CADENA_VACIA;
			SpAsConsultaMedicamentosDelegate consultaMedicamentosDelegate = new SpAsConsultaMedicamentosDelegate(fechaConsulta, codigoCodificacionMedicamento, descripcionCodificacionMedicamento, ipsVo.getCodigoTipoId(), Utilidades.validarNumeroIdentificacionIPS(ipsVo), codigoHabilitacion, consPlan);
			try {
				SOSDataAccess.ejecutarSQL(connProviderSalud, consultaMedicamentosDelegate);
			} catch (DataAccessException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MEDICAMENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}

			return consultaMedicamentosDelegate.getlMedicamentos();
		}

		// Metodo que consulta los medicamentos por descripcion
		public List<MedicamentosVO> consultaMedicamentosxDescripcionIps(String descripcionCodificacionMedicamento, IPSVO ipsVo,String codigoHabilitacion, int consPlan) throws LogicException {

			String codigoCodificacionMedicamento = ConstantesEJB.CADENA_VACIA;
			SpAsConsultaMedicamentosDelegate consultaMedicamentosDelegate = new SpAsConsultaMedicamentosDelegate(fechaConsulta, codigoCodificacionMedicamento, descripcionCodificacionMedicamento, ipsVo.getCodigoTipoId(), Utilidades.validarNumeroIdentificacionIPS(ipsVo), codigoHabilitacion, consPlan);
			try {
				SOSDataAccess.ejecutarSQL(connProviderSalud, consultaMedicamentosDelegate);
			} catch (DataAccessException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MEDICAMENTO_DES), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}
			
			return consultaMedicamentosDelegate.getlMedicamentos();
		}
	
		// Metodo que consulta los medicamentos por codigo
		public List<MedicamentosVO> consultaMedicamentosxCodigo(String codigoCodificacionMedicamento) throws LogicException {

			String descripcionCodificacionMedicamento = ConstantesEJB.CADENA_VACIA;
			SpConsultaMedicamentosDelegate consultaMedicamentosDelegate = new SpConsultaMedicamentosDelegate(fechaConsulta, codigoCodificacionMedicamento, descripcionCodificacionMedicamento);
			try {
				SOSDataAccess.ejecutarSQL(connProviderSalud, consultaMedicamentosDelegate);
			} catch (DataAccessException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MEDICAMENTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}

			return consultaMedicamentosDelegate.getlMedicamentos();
		}

		// Metodo que consulta los medicamentos por descripcion
		public List<MedicamentosVO> consultaMedicamentosxDescripcion(String descripcionCodificacionMedicamento) throws LogicException {

			String codigoCodificacionMedicamento = ConstantesEJB.CADENA_VACIA;
			SpConsultaMedicamentosDelegate consultaMedicamentosDelegate = new SpConsultaMedicamentosDelegate(fechaConsulta, codigoCodificacionMedicamento, descripcionCodificacionMedicamento);
			try {
				SOSDataAccess.ejecutarSQL(connProviderSalud, consultaMedicamentosDelegate);
			} catch (DataAccessException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MEDICAMENTO_DES), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}
			
			return consultaMedicamentosDelegate.getlMedicamentos();
		}

	// Metodo que obtiene los items de los tipos de codificaciones
	public List<SelectItem> obtenerListaSelectTipoPrestaciones(List<TipoCodificacionVO> listaVos, int validacionCuos) throws LogicException {
		List<TipoCodificacionVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {			
			listaVosResult = consultaTiposCodificacion(validacionCuos);			
		}
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (TipoCodificacionVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoTipoCodificacion(), vo.getDescripcionTipoCodificacion());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que consulta los tipos de codificaciones
	public List<TipoCodificacionVO> consultaTiposCodificacion(int validacionCuos) throws LogicException {
		SpTraerTipoCodificacionDelegate spTraerTipoCodificacionDelegate = new SpTraerTipoCodificacionDelegate(validacionCuos);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, spTraerTipoCodificacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_CODIFICACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (spTraerTipoCodificacionDelegate.getlTipoCodificacion().isEmpty() || spTraerTipoCodificacionDelegate.getlTipoCodificacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_CODIFICACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return spTraerTipoCodificacionDelegate.getlTipoCodificacion();
	}

	// Metodo que obtiene los items de las especialidades del medico
	public List<SelectItem> obtenerListaSelectEspecialidadesMedico(List<EspecialidadVO> listaVos) throws LogicException {
		List<EspecialidadVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultaEspecialidadMedico();
		}
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (EspecialidadVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoEspecialidad(), vo.getDescripcionEspecialidad());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que consulta la especialidades del medico
	public List<EspecialidadVO> consultaEspecialidadMedico() throws LogicException {
		SpDMTraerEspecialidadesDelegate especialidadesDelegate = new SpDMTraerEspecialidadesDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, especialidadesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESPECIALIDADES_MEDICO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (especialidadesDelegate.getlEspecialidades().isEmpty() || especialidadesDelegate.getlEspecialidades() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ESPECIALIDADES_MEDICO), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return especialidadesDelegate.getlEspecialidades();
	}

	// Metodo que obtiene los items de los tipos de ips
	public List<SelectItem> obtenerListaSelectTiposIps(List<TipoIPSVO> listaVos) throws LogicException {
		List<TipoIPSVO> listaVosResult = null;
		
		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultaTiposIps();
		}
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (TipoIPSVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoCodigoTipoIps(), vo.getDescripcionTipoIps());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que consulta los tipos de ips
	public List<TipoIPSVO> consultaTiposIps() throws LogicException {
		SpMNSeleccionarTipoIpsDelegate ipsDelegate = new SpMNSeleccionarTipoIpsDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, ipsDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_IPS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (ipsDelegate.getlTiposIPS().isEmpty() || ipsDelegate.getlTiposIPS() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_IPS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return ipsDelegate.getlTiposIPS();
	}

	// Metodo que consulta las ciudades por codigo
	public List<CiudadVO> consultaCiudadxCodigo(String codigoCiudad) throws LogicException {

		String descripcionCiudad = "";
		SpMNTraerCiudadesDelegate ciudadesDelegate = new SpMNTraerCiudadesDelegate(fechaConsulta, codigoCiudad, descripcionCiudad);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, ciudadesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CIUDADES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (ciudadesDelegate.getlCiudad().isEmpty() || ciudadesDelegate.getlCiudad() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CIUDADES), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return ciudadesDelegate.getlCiudad();
	}

	// Metodo que consulta las ciudades por descripcion
	public List<CiudadVO> consultaCiudadxDescripcion(String descripcionCiudad) throws LogicException {

		String codigoCiudad = "";
		SpMNTraerCiudadesDelegate ciudadesDelegate = new SpMNTraerCiudadesDelegate(fechaConsulta, codigoCiudad, descripcionCiudad);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, ciudadesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CIUDADES_DES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (ciudadesDelegate.getlCiudad().isEmpty() || ciudadesDelegate.getlCiudad() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CIUDADES_DES), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return ciudadesDelegate.getlCiudad();
	}

	// Metodo que consulta los tipos de documento soporte para las prestaciones
	// asociadas a la solicitud
	public List<DocumentoSoporteVO> consultaTiposSoportexPrestacion(List<PrestacionDTO> prestacionSeleccionadaVOs) throws LogicException {

		ConvertidorXML convertidorXML = new ConvertidorXML();
		xml = convertidorXML.convertXml(prestacionSeleccionadaVOs);

		SpASConsultaTiposDocumentoSoporteDelegate consultaTiposDocumentoSoporteDelegate = new SpASConsultaTiposDocumentoSoporteDelegate(xml, ConstantesEJB.MODELO_DOCUMENTO_SOPORTE);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaTiposDocumentoSoporteDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_SOPORTE), e, LogicException.ErrorType.DATO_NO_EXISTE); 
		}

		return consultaTiposDocumentoSoporteDelegate.getlDocumentosSoporte();
	}
	
	// Metodo que consulta los medios de contacto por codigo
	public List<MedioContactoVO> consultaMediosContactoxCodigo(String codigoMedioContacto) throws LogicException{
		int consecutivoCodigoMedioContacto = 0;
		SpASConsultaMediosContactoDelegate mediosContactoDelegate = new SpASConsultaMediosContactoDelegate(fechaConsulta, codigoMedioContacto, consecutivoCodigoMedioContacto);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, mediosContactoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_MEDIOS_CONTACTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		if (mediosContactoDelegate.getlMediosContacto().isEmpty() || mediosContactoDelegate.getlMediosContacto() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_MEDIOS_CONTACTO), LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return mediosContactoDelegate.getlMediosContacto();
	}
	
	public List<String> consultarCodigoHabilitacion(String codigoInterno) throws LogicException{
		
		SpASConsultarcodigoHabilitacionbyCodigoInterno codigoHabilitacion = new SpASConsultarcodigoHabilitacionbyCodigoInterno(codigoInterno);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, codigoHabilitacion);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_MEDIOS_CONTACTO), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return codigoHabilitacion.getlCodigoHabilitacion();
	}
	
	/**
	 * Metodo que consulta si la prestacion esta marcada con acceso directo
	 * @param conServicio
	 * @return
	 * @throws LogicException
	 */
	public BigDecimal consultarMarcaAccesoDirectoPrestacion(Integer conServicio) throws LogicException {

		SpASConsultaMarcaAccesoDirectoDelegate delegate = new SpASConsultaMarcaAccesoDirectoDelegate(conServicio);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_MARCA_ACCESO_DIRECTO), e, LogicException.ErrorType.ERROR_BASEDATOS); 
		}

		return delegate.getValorLiquidacion();
	}
	
	/**
	 * Metodo que consulta el valor del salario minimo LV
	 * @return
	 * @throws LogicException
	 */
	public Integer consultarSalarioMinimoLV() throws LogicException {

		SpAsConsultaSalarioMinimoLVDelegate delegate = new SpAsConsultaSalarioMinimoLVDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SALARIO_MINIMO), e, LogicException.ErrorType.ERROR_BASEDATOS); 
		}

		return delegate.getvalorSalarioMLV();
	}
	
	/**
	 * Metodo para devolver un servicio solicitado
	 * @param conServicio
	 * @param codPLan
	 * @param usuario
	 * @throws LogicException
	 */
	public void devolverSevicioSolicitado(Integer conSolicitud, Integer codPLan, String usuario) throws LogicException {

		SpAsDevolverSevicioSolicitadoDelegate delegate = new SpAsDevolverSevicioSolicitadoDelegate(conSolicitud, codPLan, usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_DEVOLVER_SERVICIO_SOLICITADO), e, LogicException.ErrorType.ERROR_BASEDATOS); 
		}
	}
	
	/**
	 * Metodo que valida si el usuario tiene perfil coordinador
	 * @param usuario
	 * @return
	 * @throws LogicException
	 */
	public boolean validarPerfilCordinador(String usuario) throws LogicException {

		SpASValidarPerfilCordinadorDelegate delegate = new SpASValidarPerfilCordinadorDelegate(usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDAR_PERFIL_CORDINADOR), e, LogicException.ErrorType.DATO_NO_EXISTE); 
		}

		return delegate.getEsPerfilCordinador();
	}
	
	/**
	 * Metodo para saber si alguna prestación no fue direccionada o no tiene Fecha Entrega
	 * @param 
	 * @return
	 * @throws LogicException
	 */
	public boolean validarPrestacionesAltaFrecuenciaBajoCosto(Integer conSolicitud, String usuario) throws LogicException {

		SpAsActualizaPrestacionesAltaFrecuenciaBajoCostoDelegate delegate = new SpAsActualizaPrestacionesAltaFrecuenciaBajoCostoDelegate(conSolicitud,usuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_VALIDAR_PRESTACIONS_BAJO_COSTO), e, LogicException.ErrorType.DATO_NO_EXISTE); 
		}

		return delegate.getRespuesta();
	}
	
	/**
	 * Metodo para resultar el resultado de la malla
	 * @param conSolicitud
	 * @return
	 * @throws LogicException
	 */
	public List<InconsistenciasResponseServiceVO> consultarResultadoMalla(Integer conSolicitud) throws LogicException {

		SpAsConsultaResultadoMallaDelegate delegate = new SpAsConsultaResultadoMallaDelegate(conSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RESULTADO_MALLA), e, LogicException.ErrorType.ERROR_BASEDATOS); 
		}

		return delegate.getlstInconsistencias();
	}
	
}
