package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpASObtenerOficinasDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpPmTraerSedesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCConsultarCausalesDescuadreDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerBancosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerEstadoDocumentosDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCObtenerMetodoDevolucionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaRiesgosAfiliadoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpASConsultaParametrosGeneralesAplicacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpMNConsultaConcentracionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpMNConsultaPresentacionesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpMNSeleccionaDosisDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpMNSeleccionaFrecuenciaDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpMNSeleccionaOpcionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMBuscalateralidadesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMConsultaViaAccesoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMSelecClasesAtencionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMTraerOficinasSedesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMTraerOrigenAtencionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPMTraerTipoServicioSolicitadoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPmServiciosHospitalizacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPmTipoUbicacionPacienteDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpPmTraerPlanesDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerClaseHabitacionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerPrioridadAtencionDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerSexoSaludDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerTiposDiagnosticosSaludDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.parametros.SpTraerTiposIdentificacionDelegate;
import com.sos.gestionautorizacionessaluddata.model.caja.CausalDescuadreVO;
import com.sos.gestionautorizacionessaluddata.model.caja.ParametroVO;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.RiesgoAfiliadoVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ClaseHabitacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ConcentracionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.DosisVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.FrecuenciaVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.GeneroVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.LateralidadesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OficinasVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OpcionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.OrigenAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ParametrosGeneralesVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PlanVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PresentacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.PrioridadAtencionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ServicioHospitalizacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TipoServicioVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposDiagnosticosVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.TiposIdentificacionVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.UbicacionPacienteVO;
import com.sos.gestionautorizacionessaluddata.model.parametros.ViaAccesoVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class CargarCombosController Clase controladora que permite consultar los
 * delegates que permiten cargar los combos de la pantalla de radicacion de la
 * solicitud
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 29/11/2015
 *
 */

public class CargarCombosController implements Serializable {
	private static final long serialVersionUID = 8748434112573394363L;
	private Date fechaConsulta;
	private ConnectionProvider connProviderSalud;

	public CargarCombosController() throws ConnectionProviderException, IOException {
		fechaConsulta = Utilidades.getFechaActual();

		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}
	}

	// Metodo que consulta los planes
	public List<PlanVO> consultarListaPlanes() throws LogicException {

		SpPmTraerPlanesDelegate pmTraerPlanesDelegate = new SpPmTraerPlanesDelegate(fechaConsulta);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, pmTraerPlanesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PLANES), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (pmTraerPlanesDelegate.getlPlanes().isEmpty() || pmTraerPlanesDelegate.getlPlanes() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PLANES),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return pmTraerPlanesDelegate.getlPlanes();
	}

	// Metodo que obtiene los items de los planes
	public List<SelectItem> obtenerListaPlanesConsecutivoDescripcion(List<PlanVO> listaVos) throws LogicException {
		List<PlanVO> listaVosResult = null;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultarListaPlanes();
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (PlanVO planVO : listaVosResult) {
			item = new SelectItem(planVO.getConsectivoPlan(), planVO.getDescripcionPlan());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que obtiene los items de los planes
	public List<SelectItem> obtenerListaPlanes(List<PlanVO> listaVos) throws LogicException {
		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		if (listaVos == null || listaVos.isEmpty()) {
			List<PlanVO> listavos = consultarListaPlanes();
			for (PlanVO planVO : listavos) {
				item = new SelectItem(planVO.getCodigoPlan(), planVO.getDescripcionPlan());
				listaItems.add(item);
			}
		}

		return listaItems;
	}

	// Metodo que consulta los tipos de identificacion
	public List<TiposIdentificacionVO> consultarTiposIdentificacion() throws LogicException {

		SpTraerTiposIdentificacionDelegate tiposIdentificacionDelegate = new SpTraerTiposIdentificacionDelegate(
				fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, tiposIdentificacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_IDENTIFICACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (tiposIdentificacionDelegate.getlTiposIdentificacion().isEmpty()
				|| tiposIdentificacionDelegate.getlTiposIdentificacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPOS_IDENTIFICACION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return tiposIdentificacionDelegate.getlTiposIdentificacion();
	}

	// Metodo que consulta los generos
	public List<GeneroVO> consultaListaGenero() throws LogicException {

		SpTraerSexoSaludDelegate spTraerSexoSaludDelegate = new SpTraerSexoSaludDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, spTraerSexoSaludDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GENEROS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (spTraerSexoSaludDelegate.getlGenero().isEmpty() || spTraerSexoSaludDelegate.getlGenero() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_GENEROS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return spTraerSexoSaludDelegate.getlGenero();
	}

	// Metodo que consulta el listado de opciones
	public List<OpcionVO> consultaListaOpcion() throws LogicException {

		SpMNSeleccionaOpcionDelegate seleccionaOpcionDelegate = new SpMNSeleccionaOpcionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, seleccionaOpcionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OPCION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (seleccionaOpcionDelegate.getlOpcion().isEmpty() || seleccionaOpcionDelegate.getlOpcion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OPCION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return seleccionaOpcionDelegate.getlOpcion();
	}

	// Metodo que consulta las oficinas x codigo
	public List<OficinasVO> consultaOficinasxCodigo(String codigoOficina) throws LogicException {
		String descripcionOficina = "";
		SpPMTraerOficinasSedesDelegate oficinasDelegate = new SpPMTraerOficinasSedesDelegate(fechaConsulta,
				codigoOficina, descripcionOficina);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, oficinasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OFICINAS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		if (oficinasDelegate.getlOficinas().isEmpty()
				|| oficinasDelegate.getlOficinas() == null) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_OFICINA_NO_ENCONTRADA),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return oficinasDelegate.getlOficinas();
	}

	// Metodo que consulta las oficinas x descripcion
	public List<OficinasVO> consultaOficinasxDescripcion(String descripcionOficina) throws LogicException {
		String codigoOficina = "";
		SpPMTraerOficinasSedesDelegate oficinasDelegate = new SpPMTraerOficinasSedesDelegate(fechaConsulta,
				codigoOficina, descripcionOficina);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, oficinasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OFICINAS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (oficinasDelegate.getlOficinas().isEmpty()
				|| oficinasDelegate.getlOficinas() == null) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_OFICINA_NO_ENCONTRADA),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return oficinasDelegate.getlOficinas();
	}

	public List<OficinasVO> consultaOficinas(String codigoOficina, String descripcionOficina) throws LogicException {
		SpPMTraerOficinasSedesDelegate oficinasDelegate = new SpPMTraerOficinasSedesDelegate(fechaConsulta,
				codigoOficina, descripcionOficina);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, oficinasDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OFICINAS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (oficinasDelegate.getlOficinas().isEmpty()
				|| oficinasDelegate.getlOficinas() == null) {
			throw new LogicException(
					Messages.getValorError(ConstantesEJB.ERROR_OFICINA_NO_ENCONTRADA),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return oficinasDelegate.getlOficinas();
	}

	// Metodo que consulta la clase de la atencion
	public List<ClaseAtencionVO> consultaClaseAtencion() throws LogicException {

		SpPMSelecClasesAtencionDelegate clasesAtencionDelegate = new SpPMSelecClasesAtencionDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, clasesAtencionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CLASE_ATENCION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (clasesAtencionDelegate.getlClaseAtencion().isEmpty()
				|| clasesAtencionDelegate.getlClaseAtencion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CLASE_ATENCION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return clasesAtencionDelegate.getlClaseAtencion();
	}

	// Metodo que consulta el origen de la atencion
	public List<OrigenAtencionVO> consultaOrigenAtencion() throws LogicException {

		SpPMTraerOrigenAtencionDelegate origenAtencionDelegate = new SpPMTraerOrigenAtencionDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, origenAtencionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ORIGEN_ATENCION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (origenAtencionDelegate.getlOrigenAtencion().isEmpty()
				|| origenAtencionDelegate.getlOrigenAtencion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_ORIGEN_ATENCION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return origenAtencionDelegate.getlOrigenAtencion();
	}

	// Metodo que consulta el tipo de servicio
	public List<TipoServicioVO> consultaTipoServicio() throws LogicException {

		SpPMTraerTipoServicioSolicitadoDelegate servicioSolicitadoDelegate = new SpPMTraerTipoServicioSolicitadoDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, servicioSolicitadoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_SERVICIO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (servicioSolicitadoDelegate.getlTipoServicio().isEmpty()
				|| servicioSolicitadoDelegate.getlTipoServicio() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_SERVICIO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return servicioSolicitadoDelegate.getlTipoServicio();
	}

	// Metodo que consulta la prioridad de la atencion
	public List<PrioridadAtencionVO> consultaPrioridadAtencion() throws LogicException {

		SpTraerPrioridadAtencionDelegate prioridadAtencionDelegate = new SpTraerPrioridadAtencionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, prioridadAtencionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRIORIDAD_ATENCION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (prioridadAtencionDelegate.getlPrioridadAtencion().isEmpty()
				|| prioridadAtencionDelegate.getlPrioridadAtencion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRIORIDAD_ATENCION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return prioridadAtencionDelegate.getlPrioridadAtencion();
	}

	public List<SelectItem> obtenerListaSelectUbicacionPaciente(List<UbicacionPacienteVO> listaVos)
			throws LogicException {
		List<UbicacionPacienteVO> listaVosResult = null;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultaUbicacionPaciente();
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;
		for (UbicacionPacienteVO vo : listaVosResult) {
			item = new SelectItem(vo.getConsecutivoUbicacionPaciente(), vo.getDescripcionUbicacionPaciente());
			listaItems.add(item);
		}
		return listaItems;
	}

	// Metodo que consulta la ubicacion del paciente
	public List<UbicacionPacienteVO> consultaUbicacionPaciente() throws LogicException {

		SpPmTipoUbicacionPacienteDelegate tipoUbicacionPacienteDelegate = new SpPmTipoUbicacionPacienteDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, tipoUbicacionPacienteDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_UBICACION_PACIENTE), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (tipoUbicacionPacienteDelegate.getlUbicacionPaciente().isEmpty()
				|| tipoUbicacionPacienteDelegate.getlUbicacionPaciente() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_UBICACION_PACIENTE),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return tipoUbicacionPacienteDelegate.getlUbicacionPaciente();
	}

	// Metodo que consulta los tipos de diagnosticos
	public List<TiposDiagnosticosVO> consultaTipoDiagnostico() throws LogicException {

		SpTraerTiposDiagnosticosSaludDelegate diagnosticosSaludDelegate = new SpTraerTiposDiagnosticosSaludDelegate(
				fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, diagnosticosSaludDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_DIAGNOSTICO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (diagnosticosSaludDelegate.getlTiposDiagnosticos().isEmpty()
				|| diagnosticosSaludDelegate.getlTiposDiagnosticos() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_TIPO_DIAGNOSTICO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return diagnosticosSaludDelegate.getlTiposDiagnosticos();
	}

	// Metodo que consulta los servicios de hospitalizacion de la atencion
	public List<ServicioHospitalizacionVO> consultaServicioHospitalizacion() throws LogicException {

		SpPmServiciosHospitalizacionDelegate serviciosHospitalizacionDelegate = new SpPmServiciosHospitalizacionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, serviciosHospitalizacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SERVICIO_HOSPITALIZACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (serviciosHospitalizacionDelegate.getlServicioHospitalizacion().isEmpty()
				|| serviciosHospitalizacionDelegate.getlServicioHospitalizacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_SERVICIO_HOSPITALIZACION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return serviciosHospitalizacionDelegate.getlServicioHospitalizacion();
	}

	// Metodo que consulta la clase de habitacion de la hospitalizacion
	public List<ClaseHabitacionVO> consultaClaseHabitacion() throws LogicException {

		SpTraerClaseHabitacionDelegate claseHabitacionDelegate = new SpTraerClaseHabitacionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, claseHabitacionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CLASE_HABITACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (claseHabitacionDelegate.getlClaseHabitacionVO().isEmpty()
				|| claseHabitacionDelegate.getlClaseHabitacionVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CLASE_HABITACION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return claseHabitacionDelegate.getlClaseHabitacionVO();
	}

	// Metodo que consulta la dosis del medicamento
	public List<DosisVO> consultaDosisMedicamentos() throws LogicException {

		SpMNSeleccionaDosisDelegate dosisDelegate = new SpMNSeleccionaDosisDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, dosisDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOSIS_MEDICAMENTO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (dosisDelegate.getlDosis().isEmpty() || dosisDelegate.getlDosis() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DOSIS_MEDICAMENTO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return dosisDelegate.getlDosis();
	}

	// Metodo que consulta la frecuencia del medicamento
	public List<FrecuenciaVO> consultaFrecuenciaMedicamentos() throws LogicException {

		SpMNSeleccionaFrecuenciaDelegate frecuenciaDelegate = new SpMNSeleccionaFrecuenciaDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, frecuenciaDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_FRECUENCIA_MEDICAMENTO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (frecuenciaDelegate.getlFrecuencia().isEmpty() || frecuenciaDelegate.getlFrecuencia() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_FRECUENCIA_MEDICAMENTO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return frecuenciaDelegate.getlFrecuencia();
	}

	// Metodo que consulta la lateralidad del procedimiento
	public List<LateralidadesVO> consultaLateralidadesProcedimientos() throws LogicException {

		SpPMBuscalateralidadesDelegate buscalateralidadesDelegate = new SpPMBuscalateralidadesDelegate(fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, buscalateralidadesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_LATERALIDAD_PROCEDIMIENTO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (buscalateralidadesDelegate.getlLateralidad().isEmpty()
				|| buscalateralidadesDelegate.getlLateralidad() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_LATERALIDAD_PROCEDIMIENTO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return buscalateralidadesDelegate.getlLateralidad();
	}

	// Metodo que consulta las vias de acceso del medicamento
	public List<ViaAccesoVO> consultaViasAccesoMedicamento() throws LogicException {

		SpPMConsultaViaAccesoDelegate consultaViaAccesoDelegate = new SpPMConsultaViaAccesoDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaViaAccesoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_VIA_ACCESO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaViaAccesoDelegate.getlViaAcceso().isEmpty() || consultaViaAccesoDelegate.getlViaAcceso() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_VIA_ACCESO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return consultaViaAccesoDelegate.getlViaAcceso();
	}

	// Metodo que consulta la concetracion del medicamento
	public List<ConcentracionVO> consultaConcentracionMedicamento() throws LogicException {
		SpMNConsultaConcentracionDelegate concentracionDelegate = new SpMNConsultaConcentracionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, concentracionDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CONCENTRACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (concentracionDelegate.getlConcentracion().isEmpty() || concentracionDelegate.getlConcentracion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CONCENTRACION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return concentracionDelegate.getlConcentracion();
	}

	// Metodo que consulta la presentacion del medicamento
	public List<PresentacionVO> consultaPresentacionMedicamento() throws LogicException {
		SpMNConsultaPresentacionesDelegate consultaPresentacionesDelegate = new SpMNConsultaPresentacionesDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaPresentacionesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESENTACION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaPresentacionesDelegate.getlPresentacion().isEmpty()
				|| consultaPresentacionesDelegate.getlPresentacion() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PRESENTACION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaPresentacionesDelegate.getlPresentacion();
	}

	// Metodo que consulta los parametros generales de la aplicacion
	public List<ParametrosGeneralesVO> consultaParametrosGenerales() throws LogicException {
		SpASConsultaParametrosGeneralesAplicacionDelegate parametrosGeneralesDelegate = new SpASConsultaParametrosGeneralesAplicacionDelegate();

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, parametrosGeneralesDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PARAMETROS_GENERALES), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (parametrosGeneralesDelegate.getlParametrosGenerales().isEmpty()
				|| parametrosGeneralesDelegate.getlParametrosGenerales() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_PARAMETROS_GENERALES),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return parametrosGeneralesDelegate.getlParametrosGenerales();
	}

	/**
	 * Metodo que consulta los riesgos del afiliado *
	 */
	public List<RiesgoAfiliadoVO> consultaRiesgosAfiliado() throws LogicException {
		SpASConsultaRiesgosAfiliadoDelegate delegate = new SpASConsultaRiesgosAfiliadoDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RIESGOS_AFILIADOS), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getLstRiesgoAfiliadoVO().isEmpty() || delegate.getLstRiesgoAfiliadoVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_RIESGOS_AFILIADOS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getLstRiesgoAfiliadoVO();
	}

	/**
	 * Metodo que consulta los causales de descuadre
	 * 
	 * @return
	 * @throws LogicException
	 */
	public List<CausalDescuadreVO> consultaCausalesDescuadre() throws LogicException {
		SpRCConsultarCausalesDescuadreDelegate delegate = new SpRCConsultarCausalesDescuadreDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CAUSALES_DESCUADRE), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getListCausales().isEmpty() || delegate.getListCausales() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_CAUSALES_DESCUADRE),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getListCausales();
	}

	/**
	 * Controlador que se encarga de consultar los bancos vigentes.
	 * 
	 * @return
	 * @throws LogicException
	 */
	public List<ParametroVO> consultaBanco() throws LogicException {
		SpRCObtenerBancosDelegate delegate = new SpRCObtenerBancosDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_BANCO), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getListaResultadoParametros().isEmpty() || delegate.getListaResultadoParametros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_BANCO),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getListaResultadoParametros();
	}

	/**
	 * Controlador que se encarga de consultar los metodos de devolucion
	 * vigentes.
	 * 
	 * @return
	 * @throws LogicException
	 */
	public List<ParametroVO> consultaMetodosDevolucion() throws LogicException {
		SpRCObtenerMetodoDevolucionDelegate delegate = new SpRCObtenerMetodoDevolucionDelegate();
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_METODO_DEVOLUCION), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getListaResultadoParametros().isEmpty() || delegate.getListaResultadoParametros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_METODO_DEVOLUCION),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getListaResultadoParametros();
	}
	
	// Metodo que obtiene los items de los planes
	public List<SelectItem> obtenerListaSedes(List<ParametroVO> listaVos) throws LogicException {
		List<ParametroVO> listaVosResult;

		if (listaVos == null || listaVos.isEmpty()) {
			listaVosResult = consultaSedes(null, fechaConsulta, null, null);
		} else {
			listaVosResult = listaVos;
		}

		List<SelectItem> listaItems = new ArrayList<SelectItem>();
		SelectItem item;

		for (ParametroVO parametroVO : listaVosResult) {
			item = new SelectItem(parametroVO.getConsecutivo(), parametroVO.getDescipcion());
			listaItems.add(item);
		}
		return listaItems;
	}

	/**
	 * Se encarga de consultar las sedes para el combo
	 * 
	 * @param codigoSede
	 * @param fechaReferencia
	 * @param descripcion
	 * @param visibleUsuario
	 * @return
	 * @throws LogicException
	 */
	public List<ParametroVO> consultaSedes(String codigoSede, Date fechaReferencia, String descripcion,
			String visibleUsuario) throws LogicException {
		SpPmTraerSedesDelegate delegate = new SpPmTraerSedesDelegate(codigoSede, fechaReferencia, descripcion,
				visibleUsuario);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_SEDES), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getListaResultadoParametros().isEmpty() || delegate.getListaResultadoParametros() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_SEDES),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getListaResultadoParametros();
	}

	/**
	 * Consulta los estados por tipo de documento.
	 * 
	 * @param codigo
	 * @param fechaReferencia
	 * @param descripcion
	 * @param visibleUsuario
	 * @param tipoDocumento
	 * @return
	 * @throws LogicException
	 */
	public List<ParametroVO> consultaEstadosNotaCredito(String codigo, Date fechaReferencia, String descripcion,
			String visibleUsuario, int tipoDocumento) throws LogicException {
		SpRCObtenerEstadoDocumentosDelegate delegate = new SpRCObtenerEstadoDocumentosDelegate(codigo, fechaReferencia,
				descripcion, visibleUsuario, tipoDocumento);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_COMBO_SEDES), e,
					LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getListaResultadoParametros();
	}

	/**
	 * Se encarga de consultar las oficinas de SOS
	 * 
	 * @param codigoOficina
	 * @param descripcionOficina
	 * @param tipoOficina
	 * @param consecutivoSede
	 * @param fechaConsulta
	 * @return
	 * @throws DataAccessException
	 * @throws LogicException
	 */
	public List<OficinasVO> consultarOficinas(String codigoOficina, String descripcionOficina, Integer tipoOficina,
			Integer consecutivoSede, Date fechaConsulta) throws LogicException {
		SpASObtenerOficinasDelegate delegate = new SpASObtenerOficinasDelegate(codigoOficina, descripcionOficina,
				tipoOficina, consecutivoSede, fechaConsulta);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (Exception e) {
			throw new LogicException(e, LogicException.ErrorType.ERROR_BASEDATOS);
		}

		if (delegate.getResult().isEmpty() || delegate.getResult() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_OFICINAS),
					LogicException.ErrorType.DATO_NO_EXISTE);
		}
		return delegate.getResult();
	}
}
