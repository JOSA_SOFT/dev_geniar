package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.MenuDelegate;
import com.sos.gestionautorizacionessaluddata.model.ModuloVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderBDSeguridad;
import com.sos.gestionautorizacionessaludejb.util.Messages;

/**
 * Class MenuController
 * @author Jerson Viveros
 * @version 21/07/2014
 *
 */
public class MenuController extends ConnProviderBDSeguridad{

	
	/**
	 * Consulta los m�dulos a los cuales tiene acceso el usuario
	 * 
	 * @param string
	 * usuario 
	 * 
	 * @return List
	 * 	Modulos a los que tiene acceso el usuario
	 */
	public List<ModuloVO> getModulos(String usr)throws LogicException, SQLException, ConnectionProviderException, IOException{
		ConnectionProvider connProvider = null;
		connProvider = getConnProvider();
		MenuDelegate delegate = new MenuDelegate(usr, Messages.getValorParametro("CodigoModulo"));
		SOSDataAccess.ejecutarSQL(connProvider, delegate);
		List<ModuloVO> modulos = delegate.getModulos();
		if(modulos.isEmpty()){
			throw new LogicException(Messages.getValorExcepcion("ERROR_EXCEPCION_USUARIO_SIN_MENU"), LogicException.ErrorType.USUARIO_INVALIDO);
		}
		return modulos;
	}
}
