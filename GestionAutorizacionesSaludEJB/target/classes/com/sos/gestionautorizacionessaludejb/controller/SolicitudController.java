package com.sos.gestionautorizacionessaludejb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.gestiondomiciliaria.SpASConsultaDatosInformacionDetalleSolicitudDelegate;
import com.sos.gestionautorizacionessaluddata.model.consultasolicitud.DatosSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.gestioninconsistencias.SolicitudBpmVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

public class SolicitudController {

	private ConnectionProvider connProviderSalud;
	

	public SolicitudController() throws ConnectionProviderException, IOException {
		if (connProviderSalud == null) {
			connProviderSalud = ConnProviderSiSalud.getConnProvider();
		}		
	}

	/** Metodo que consulta el direccionamiento **/
	public SolicitudBpmVO consultarDetalleSolicitudBPM(int numeroSolicitud) throws LogicException {

		SpASConsultaDatosInformacionDetalleSolicitudDelegate delegate = new SpASConsultaDatosInformacionDetalleSolicitudDelegate(numeroSolicitud);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, delegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DETALLE_SOLICITUDES), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (delegate.getDetalleSolicitudBpmVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_DETALLE_SOLICITUDES), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return delegate.getDetalleSolicitudBpmVO();
	}

	public List<DatosSolicitudVO> agruparSolicitudes(List<DatosSolicitudVO> listSolicitudes) throws LogicException {

		List<DatosSolicitudVO> listaSolicitudesDevuelta = new ArrayList<DatosSolicitudVO>();

		Integer tempNumeroSolicitud = 0;
		int contador = 0;
		boolean par = true;
		for (DatosSolicitudVO datosSolicitudVO : listSolicitudes) {
			if (listaSolicitudesDevuelta.isEmpty()) {
				tempNumeroSolicitud = datosSolicitudVO.getNumeroSolicitud();
				datosSolicitudVO.setlPrestaciones(new ArrayList<PrestacionDTO>());
				datosSolicitudVO.getlPrestaciones().add(datosSolicitudVO.getPrestacionDTO());
				datosSolicitudVO.setPar(par);
				listaSolicitudesDevuelta.add(datosSolicitudVO);
			} else {
				if (tempNumeroSolicitud.compareTo(datosSolicitudVO.getNumeroSolicitud()) == 0) {
					datosSolicitudVO.setPar(par);
					listaSolicitudesDevuelta.get(contador).getlPrestaciones().add(datosSolicitudVO.getPrestacionDTO());
				} else {
					par = par ? false : true;
					tempNumeroSolicitud = datosSolicitudVO.getNumeroSolicitud();
					datosSolicitudVO.setlPrestaciones(new ArrayList<PrestacionDTO>());
					datosSolicitudVO.getlPrestaciones().add(datosSolicitudVO.getPrestacionDTO());
					datosSolicitudVO.setPar(par);
					listaSolicitudesDevuelta.add(datosSolicitudVO);
					contador++;
				}
			}
		}
		return listaSolicitudesDevuelta;
	}
}
