package com.sos.gestionautorizacionessaludejb.controller;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.gestionautorizacionessaluddata.delegate.caja.SpRCValidaAperturaCajaDelegate;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
/**
 * 
 * @author JOSÉ OLMEDO SOTO AGUIRRE, GENIAR S.A.S
 * 28/10/2016
 *
 */
public class ValidaAperturaCajaController {

	private ConnectionProvider connProviderCaja;
/**
 * CONSTRUCTOR PRINCIPAL DE LA CLASE, INICIALIZA LA CONEXIÓN.
 * @throws ConnectionProviderException
 */
	public ValidaAperturaCajaController() throws ConnectionProviderException {
		if (connProviderCaja == null) {
			connProviderCaja = ConnProviderSiSalud.getConnProvider();
		}
	}

	/**
	 * INVOCA EL DELEGADO ENCARGADO DE PROCESAR LA INFORMACIÓN.
	 * @param user  USUARIO LOGEADO
	 * @return 0: CAJA CERRADA. 1: CAJA ABIERTA. 2: CAJA ABIERTA PERO CON FECHA DIFERENTE.
	 * @throws DataAccessException
	 */
	public int validaAperturaCaja(String user) throws DataAccessException {

		SpRCValidaAperturaCajaDelegate delegate = new SpRCValidaAperturaCajaDelegate(user);

		SOSDataAccess.ejecutarSQL(connProviderCaja, delegate);

		return delegate.getResult();

	}

}
