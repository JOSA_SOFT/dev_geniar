package com.sos.gestionautorizacionessaludejb.controller;

import java.util.List;

import co.eps.sos.dataccess.exception.DataAccessException;

import com.sos.dataccess.SOSDataAccess;
import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.excepciones.LogicException;
import com.sos.gestionautorizacionessaluddata.delegate.generarops.SpASActualizaEstadoConceptosServicioWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.generarops.SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.generarops.SpASConsultaVigenciaEstadoOPSWebDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.generarops.SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate;
import com.sos.gestionautorizacionessaluddata.delegate.generarops.SpASRegistrarValidacionEspecialWebDelegate;
import com.sos.gestionautorizacionessaluddata.model.consultaafiliado.ValidacionEspecialVO;
import com.sos.gestionautorizacionessaluddata.model.generaops.AutorizacionServicioVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;
import com.sos.gestionautorizacionessaludejb.util.ConnProviderSiSalud;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;

public class GeneracionAutorizacionServicioController {

	private ConnectionProvider connProviderSalud;

	/**Metodo que permite realizar la conexion a la base de datos */
	public GeneracionAutorizacionServicioController() throws LogicException{		

		if (connProviderSalud == null) {
			try {
				connProviderSalud = ConnProviderSiSalud.getConnProvider();
			} catch (ConnectionProviderException e) {
				throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONEXION_BD), e, LogicException.ErrorType.DATO_NO_EXISTE);
			}
		}		
	}

	/**Metodo que permite consultar las ops generadas para una solicitud*/
	public List<AutorizacionServicioVO> consultaNumeroAutorizacionxSolicitud(int consecutivoSolicitud, Integer consecutivoPrestacion) throws LogicException{
		SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate consultaAutorizacionServicioOPSDelegate = new SpASConsultaAutorizacionServicioOPSxSolicitudWebDelegate(consecutivoSolicitud, consecutivoPrestacion);

		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, consultaAutorizacionServicioOPSDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_NUMERO_AUTORIZACION), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (consultaAutorizacionServicioOPSDelegate.getlAutorizacionServicioVO().isEmpty() || consultaAutorizacionServicioOPSDelegate.getlAutorizacionServicioVO() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTA_NUMERO_AUTORIZACION), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return consultaAutorizacionServicioOPSDelegate.getlAutorizacionServicioVO();
	}

	/**Metodo que permite actualiza el estado de la ops impresas*/
	public void actualizarEstadoConceptosServicio(int numeroUnicoAutorizacion) throws LogicException{
		SpASActualizaEstadoConceptosServicioWebDelegate actualizaEstadoConceptosServicio = new SpASActualizaEstadoConceptosServicioWebDelegate(numeroUnicoAutorizacion);
		try {
			SOSDataAccess.ejecutarAutoCommitSQL(connProviderSalud, actualizaEstadoConceptosServicio);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ACTUALIZAR_OPS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**Metodo que permite registrar la validacion especial */
	public void registrarValidacionEspecialAfiliado(ValidacionEspecialVO validacionEspecialVO, String usuarioCreacion, String accionValidacion) throws LogicException{
		SpASRegistrarValidacionEspecialWebDelegate validacionEspecialWebDelegate = new SpASRegistrarValidacionEspecialWebDelegate(validacionEspecialVO, usuarioCreacion, accionValidacion);
		try {
			SOSDataAccess.ejecutarAutoCommitSQL(connProviderSalud, validacionEspecialWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_REGISTRAR_VALIDACION_ESPECIAL), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
	}

	/**Metodo que permite generar los numero unico OPS */
	public ServiceErrorVO generarNumeroUnicoAutorizacionServicio(Integer consecutivoSolicitud, String usuarioSession) throws LogicException{
		SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate generarNumeroUnicoOpsMasivoDelegate = new SpASEjecutarGenerarNumeroUnicoOpsMasivoDelegate(consecutivoSolicitud, usuarioSession);
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, generarNumeroUnicoOpsMasivoDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_EJECUTAR_NUMERO_UNICO_OPS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}

		if (generarNumeroUnicoOpsMasivoDelegate.getServiceError() == null) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_EJECUTAR_NUMERO_UNICO_OPS), LogicException.ErrorType.DATO_NO_EXISTE);
		}

		return generarNumeroUnicoOpsMasivoDelegate.getServiceError();
	}
	
	/**Metodo que permite consultar la fechas de vigencia de la ops*/
	public List<AutorizacionServicioVO> consultaVigenciaEstadoOPSWeb(Integer consecutivoSolicitud, Integer consecutivoPrestacion) throws LogicException{
		SpASConsultaVigenciaEstadoOPSWebDelegate estadoOPSWebDelegate = new SpASConsultaVigenciaEstadoOPSWebDelegate(consecutivoSolicitud, consecutivoPrestacion);
		
		try {
			SOSDataAccess.ejecutarSQL(connProviderSalud, estadoOPSWebDelegate);
		} catch (DataAccessException e) {
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_CONSULTAR_VIGENCIAS_ESTADO_OPS), e, LogicException.ErrorType.DATO_NO_EXISTE);
		}
		
		return estadoOPSWebDelegate.getlAutorizacionServicioVO();
	}
}
