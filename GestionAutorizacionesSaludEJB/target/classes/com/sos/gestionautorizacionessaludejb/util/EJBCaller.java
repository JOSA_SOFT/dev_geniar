package com.sos.gestionautorizacionessaludejb.util;


import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Properties;

import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;

/**
 * 
 * @author SETI-2342
 *
 */
public class EJBCaller {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(EJBCaller.class);

	/**
	 * 
	 * @param jndi
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public static<T> T invoke(String jndi) throws LogicException{
		T ejb = null;
		try {
			Properties properties = new Properties();
			InitialContext context = null;

			final ClassLoader cl = Thread.currentThread().getContextClassLoader();
			final InputStream inStream = cl.getResourceAsStream("properties/invoker.properties");

			if (inStream != null) {
				try {
					properties.load(inStream);
				} finally {
					inStream.close();
				}
			}

			context = new InitialContext(properties);
			ejb = (T) context.lookup(jndi);
		} catch (Exception ex) {
			LOG.error(ex);
			throw new LogicException(ex.getMessage(), ErrorType.INDEFINIDO);
		}
		return ejb;
	}


	/**
	 * Metodo encargado de crear un service a partir de los parametros enviados
	 * @return service
	 * @throws NamingException
	 * @throws RemoteException
	 * @throws CreateException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T createService(String jndi, String ProvidertUrl)
			throws LogicException {
		T ejb = null;
		try {
			Hashtable<String, String> settings = new Hashtable<String, String>();

			settings.put(Context.INITIAL_CONTEXT_FACTORY, ConstantesEJB.INITIAL_CONTEXT_FACTORY);		                                              
			settings.put(Context.PROVIDER_URL,ProvidertUrl);

			Context initialContext = new InitialContext(settings);
			ejb =(T)initialContext.lookup(jndi);

		} catch (Exception ex) {
			LOG.error(ex);
			throw new LogicException(ex.getMessage(), ErrorType.INDEFINIDO);
		}
		return  ejb;			
	}


}
