package com.sos.gestionautorizacionessaludejb.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Clase para generar jasper con listas anidadas.
 * 
 * @author Jose omedo soto aguirre
 *
 */
public class JasperGenerator {
	
	/**
	 * Constructor de la clase
	 */
	private JasperGenerator(){
		throw new IllegalAccessError(ConstantesEJB.CLASE_UTILIDAD);
	}
	
	/**
	 * Metodo para generar jasper con listas
	 * 
	 * @param sourceJasper
	 * @param parameters
	 * @param listaInformacion
	 * @param rutaJasper
	 * @return
	 * @throws JRException 
	 * @throws Exception
	 */
	public static ByteArrayOutputStream generateReport(
			InputStream sourceJasper, Map<String, Object> parameters,
			List<?> listaInformacion) throws JRException  {

		JasperPrint jasperPrint = JasperFillManager.fillReport(sourceJasper,
				parameters, new JRBeanCollectionDataSource(listaInformacion,
						false));

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		JasperExportManager.exportReportToPdfStream(jasperPrint,
				arrayOutputStream);

		return arrayOutputStream;

	}
}
