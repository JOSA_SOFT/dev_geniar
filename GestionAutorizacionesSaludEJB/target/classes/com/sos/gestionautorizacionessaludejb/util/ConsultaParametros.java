package com.sos.gestionautorizacionessaludejb.util;

import com.sos.excepciones.LogicException;

public class ConsultaParametros {

	/**
	 * Utilidad encargada de consultar un listado de valores correspondiente a
	 * un parametro, formatearlos al tipo de dato que se solicita y retornarlos
	 * como respuesta de la peticion
	 */
	private ConsultaParametros() {

	}

	public static Object[] obtenerListaValoresFormateados(String nombreParametro, String tipoDatoParametro, String delimitador) throws LogicException {

		Object[] respuesta = null;
		String valorParametro;

		valorParametro = Messages.getValorParametro(nombreParametro);
		respuesta = formatearListaValoresParametro(valorParametro, tipoDatoParametro, delimitador);

		return respuesta;
	}

	public static String obtenerValorParametro(String nombreParametro) {
		return Messages.getValorParametro(nombreParametro);
	}

	/**
	 * Metodo encargado de formatear un listado de valores con su respectivo
	 * tipo de dato esperado
	 */

	private static Object[] formatearListaValoresParametro(String valorParametro, String tipoDatoParametro, String delimitadorListado) throws LogicException {

		Object[] respuesta = null;
		String[] listaValores;

		listaValores = valorParametro.split(delimitadorListado);

		if ("String".equals(tipoDatoParametro)) {
			respuesta = listaValores;

		} else if ("int".equals(tipoDatoParametro)) {

			respuesta = new Integer[listaValores.length];
			for (int i = 0; i < listaValores.length; i++) {
				respuesta[i] = Integer.parseInt(listaValores[i]);
			}

		} else if ("double".equals(tipoDatoParametro)) {

			respuesta = new Double[listaValores.length];
			for (int i = 0; i < listaValores.length; i++) {
				respuesta[i] = Double.parseDouble(listaValores[i]);
			}
		}
		return respuesta;
	}
}
