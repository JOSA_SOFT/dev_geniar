package com.sos.gestionautorizacionessaludejb.util;

import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.dataccess.connectionprovider.ConnectionProviderGeneral;

/**
 * Class ConnProviderSiSalud
 * @author ing. Victor Hugo Gil Ramos
 *
 */
public class ConnProviderSiSalud {
	
	private ConnProviderSiSalud(){
	}
	
	public static ConnectionProvider getConnProvider() throws ConnectionProviderException {
		return ConnectionProviderGeneral.getInstance(ConnProviderSiSalud.class
				.getResourceAsStream(ConstantesEJB.PROPIEDADES_SI_SALUD));
	}

}
