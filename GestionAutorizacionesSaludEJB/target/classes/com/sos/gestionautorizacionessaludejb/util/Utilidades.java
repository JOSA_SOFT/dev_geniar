package com.sos.gestionautorizacionessaludejb.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.lowagie.text.List;
import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.ips.modelo.IPSVO;
import com.sos.medico.modelo.MedicoVO;
import com.sos.visos.service.vo.Indice;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;

public final class Utilidades {

	private static DatatypeFactory df = null;
	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(Utilidades.class);
	
	private Utilidades(){
		/*clase utilizaría constructor privado para evitar instancias */
	}
	
	/**
	 * Quitar espacion final.
	 *
	 * @param cadena
	 *            the cadena
	 * @return the string
	 */
	public static String quitarEspacionFinal(String cadena) {
		if (cadena.indexOf(' ') > -1) {
			return cadena.substring(0, cadena.indexOf(' '));
		} else {
			return cadena;
		}
	}

	/**
	 * Obtener string de date.
	 *
	 * @param dateToFormat
	 *            the date to format
	 * @param formatoDeseado
	 *            the formato deseado
	 * @return the string
	 */
	public static String obtenerStringDeDate(Date dateToFormat,
			String formatoDeseado, boolean formato) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		if (!formato) {
			if (formatoDeseado != null && formatoDeseado.contains("-")) {
				sdf.applyPattern("yyyy-MM-dd");
			} else if (formatoDeseado != null && formatoDeseado.contains("/")) {
				sdf.applyPattern("yyyy/MM/dd");
			} else {
				sdf.applyPattern("yyyyMMdd");
			}
		} else {
			sdf.applyPattern(formatoDeseado);
		}
		return sdf.format(dateToFormat);
	}

	/**
	 * Obtener date de string.
	 *
	 * @param fecha
	 *            the fecha
	 * @param formatoDeseado
	 *            the formato deseado
	 * @return the date
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Date obtenerDateDeString(String fecha, String formatoDeseado)
			throws ParseException {
		String formatoDeseadoResult = null;
		if (ConstantesEJB.CADENA_VACIA.equals(formatoDeseado)) {
			formatoDeseadoResult = ConstantesEJB.FORMATO_FECHA_DIA;
		} else if (ConstantesEJB.CADENA_COMODIN.equals(formatoDeseado)) {
			formatoDeseadoResult = ConstantesEJB.FORMATO_FECHA_ANO;
		}

		SimpleDateFormat formato = new SimpleDateFormat(formatoDeseadoResult);
		return formato.parse(fecha);
	}

	/**
	 * Verifica si la cadena ingresada es un numero o no lo es.
	 * 
	 * @param cadena
	 * @return
	 */
	public static boolean esNumero(String cadena) {
		try {
			Long.valueOf(cadena);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	// Metodo usado para obtener la fecha actual
	// @return Retorna la fecha actual
	public static Date getFechaActual() {
		return new Date();
	}

	/**
	 * Metodo encargado de convertir un archivo a byte[]
	 */
	public static byte[] extractBytes(String ubicacion) {
		String initialpath = "";
		FileInputStream fis;
		byte[] totalbytes = null;
		try {
			fis = new FileInputStream(initialpath + ubicacion);
			byte[] bytes = new byte[1024 * 1024 * 20];// 20MB
			int len;
			len = fis.read(bytes);
			fis.close();
			totalbytes = new byte[len];
			System.arraycopy(bytes, 0, totalbytes, 0, len);
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return totalbytes;
	}

	/**
	 * Metodo encargado de procesar los indices remitidos por la solicitud para
	 * transformarlos en un listado de Indices acordes (Indice[])
	 */
	public static Indice[] procesarIndices(String[] indicesRemitidos,
			Integer[] codigosIndices) {

		Indice[] indices = new Indice[indicesRemitidos.length];

		for (int i = 0; i < indices.length; i++) {
			indices[i] = new Indice(codigosIndices[i], indicesRemitidos[i]);
		}
		return indices;
	}

	static {
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException dce) {
			throw new IllegalStateException(
					"Exception while obtaining DatatypeFactory instance", dce);
		}
	}

	public static XMLGregorianCalendar asXMLGregorianCalendar(
			java.util.Date fecha) {
		if (fecha == null) {
			return null;
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTimeInMillis(fecha.getTime());
			return df.newXMLGregorianCalendar(gc.get(Calendar.YEAR),
					gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DAY_OF_MONTH),
					gc.get(Calendar.HOUR), gc.get(Calendar.MINUTE),
					gc.get(Calendar.SECOND), gc.get(Calendar.MILLISECOND),
					DatatypeConstants.FIELD_UNDEFINED);
		}
	}

	public static BigInteger convertirNumeroToBigInteger(Integer numero) {
		BigInteger conversionNumero;

		if (numero == null) {
			conversionNumero = null;
		} else {
			conversionNumero = BigInteger.valueOf(numero);
		}
		return conversionNumero;
	}

	public static BigDecimal convertirNumeroToBigDecimal(Integer numero) {
		BigDecimal conversionNumero;

		if (numero == null) {
			conversionNumero = null;
		} else {
			conversionNumero = BigDecimal.valueOf(numero);
		}
		return conversionNumero;
	}

	public static Date toDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	public static MedicoVO quitarEspaciosMedico(MedicoVO medicoVO) {
		if (medicoVO != null) {
			medicoVO.setCodigoTipoId(medicoVO.getCodigoTipoId() != null ? medicoVO
					.getCodigoTipoId().trim() : null);
			medicoVO.setDescripcionEspe(medicoVO.getDescripcionEspe() != null ? medicoVO
					.getDescripcionEspe().trim() : null);
			medicoVO.setNumeroIdentificacion(medicoVO.getNumeroIdentificacion() != null ? medicoVO
					.getNumeroIdentificacion().trim() : null);
			medicoVO.setPrimerApellido(medicoVO.getPrimerApellido() != null ? medicoVO
					.getPrimerApellido().trim() : null);
			medicoVO.setPrimerNombre(medicoVO.getPrimerNombre() != null ? medicoVO
					.getPrimerNombre().trim() : null);
			medicoVO.setRegistroMedico(medicoVO.getRegistroMedico() != null ? medicoVO
					.getRegistroMedico().trim() : null);
			medicoVO.setSegundoApellido(medicoVO.getSegundoApellido() != null ? medicoVO
					.getSegundoApellido().trim() : null);
			medicoVO.setSegundoNombre(medicoVO.getSegundoNombre() != null ? medicoVO
					.getSegundoNombre().trim() : null);
		}

		return medicoVO;
	}

	public static String[] quitarEspaciosMedicoEJB(String primerNombre,
			String segundoNombre, String primerApellido, String segundoApellido) {
		if (primerNombre != null)
			primerNombre.trim();

		if (segundoNombre != null)
			segundoNombre.trim();

		if (primerApellido != null)
			primerApellido.trim();

		if (segundoApellido != null)
			segundoApellido.trim();

		return new String[] { primerNombre, segundoNombre, primerApellido,
				segundoApellido };
	}

	@SuppressWarnings("unchecked")
	public static boolean validarListasVacias(Object lista1, Object lista2,
			Object lista3) {

		if (lista1 instanceof List && lista2 instanceof List
				&& lista3 instanceof List) {
			java.util.List<Object> objLista1 = (java.util.List<Object>) lista1;
			java.util.List<Object> objLista2 = (java.util.List<Object>) lista2;
			java.util.List<Object> objLista3 = (java.util.List<Object>) lista3;

			if (objLista1.isEmpty() && objLista2.isEmpty()
					&& objLista3.isEmpty())
				return true;
		}
		return false;
	}

	public static final boolean validarCamposVacios(Object object) {
		if (object instanceof String) {
			String cadena = (String) object;
			return cadena != null && !cadena.trim().isEmpty();
		}
		return object != null;
	}

	/** Validacion cadena vacia */
	public static String validaCadenaTexto(String cadena) {

		String resultadoCadena;

		if (cadena != null) {
			resultadoCadena = cadena.trim();
		} else {
			resultadoCadena = ConstantesEJB.CADENA_VACIA;
		}

		return resultadoCadena;
	}

	/** Validacion cadena vacia */
	public static String validaCadenaTextoCombo(String cadena, String valor) {

		String resultadoCadena;

		if (cadena != null) {
			resultadoCadena = cadena.trim();
		} else {
			resultadoCadena = valor;
		}

		return resultadoCadena;
	}

	/** Validacion numeroIdetificacion */
	public static BigInteger validaNumeroIdentificacion(String cadena) {

		BigInteger resultadoBigInteger;

		if (cadena != null) {
			resultadoBigInteger = new BigInteger(cadena.trim());
		} else {
			resultadoBigInteger = BigInteger.ZERO;
		}

		return resultadoBigInteger;
	}

	/** Validacion numero */
	public static BigInteger validaNumero(Integer numero, BigInteger valor) {

		BigInteger resultadoBigInteger;

		if (numero != null) {
			resultadoBigInteger = convertirNumeroToBigInteger(numero);
		} else {
			resultadoBigInteger = valor;
		}

		return resultadoBigInteger;
	}

	/** metodo que permite quitar los espacios a una cadena */
	public static String quitarEspaciosCadena(String cadena) {
		return cadena.trim();
	}

	/** Validacion de datos obligatorios para los consecutivos */
	public static void validarDatosObligatoriosInt(long consecutivo,
			String excepcionString) throws LogicException {
		if (consecutivo == ConstantesEJB.VALOR_INICIAL || consecutivo == 0) {
			throw new LogicException(excepcionString,
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	/** Validacion de datos obligatorios para las cadenas de texto */
	public static void validarDatosObligatoriosString(String cadena,
			String excepcionString) throws LogicException {
		if (cadena == null || quitarEspaciosCadena(cadena).isEmpty()) {
			throw new LogicException(excepcionString,
					ErrorType.PARAMETRO_ERRADO);
		}
	}

	/** Validacion de datos obligatorios en las listas */
	public static void validarDatosObligatoriosList(java.util.List<?> lista, String excepcionString) throws LogicException {
		if (lista == null || lista.isEmpty()) {
			throw new LogicException(excepcionString, ErrorType.PARAMETRO_ERRADO);
		}
	}

	
	/** Validacion que la fecha se igual a la fecha actual */
	public static boolean esfechaIgualActual(Date fechaActual, Date fechaingresada) {
        SimpleDateFormat formateador = new SimpleDateFormat(ConstantesEJB.FORMATO_FECHA_ANO);
        String strfechaActual = formateador.format(fechaActual);
        String strfechaingresada = formateador.format(fechaingresada);
		return strfechaActual.equals(strfechaingresada);
	}
	
	/** Validacion que la fecha se igual a la fecha actual 
	 * @throws LogicException */
	public static boolean esfechaMenorActual(Date fechaActual, Date fechaingresada) throws LogicException {
        SimpleDateFormat formateador = new SimpleDateFormat(ConstantesEJB.FORMATO_FECHA_ANO);
        String strfechaActual = formateador.format(fechaActual);
        String strfechaingresada = formateador.format(fechaingresada);
        Date fechaActualFrm;
        Date fechaingresadaFrm;
        try {
			fechaActualFrm = formateador.parse(strfechaActual);
			fechaingresadaFrm = formateador.parse(strfechaingresada);
		} catch (ParseException e) {
			throw new LogicException(e.getMessage(),
					ErrorType.PARAMETRO_ERRADO);
		}
        return fechaActualFrm.compareTo(fechaingresadaFrm)>0;
	}
	
	
	/** Validacion que la fecha se igual a la fecha actual 
	 * @throws LogicException */
	public static boolean esfechaMenorIgualActual(Date p_fechaActual, Date p_fechaingresada) throws LogicException {
        SimpleDateFormat v_formateador = new SimpleDateFormat(ConstantesEJB.FORMATO_FECHA_ANO);
        String v_strfechaingresada = v_formateador.format(p_fechaingresada);
        String v_strfechaActual = v_formateador.format(p_fechaActual);
        Date v_fechaingresadaFrm;
        Date v_fechaActualFrm;
        
        try {
        	v_fechaActualFrm = v_formateador.parse(v_strfechaActual);
        	v_fechaingresadaFrm = v_formateador.parse(v_strfechaingresada);
        	
		} catch (ParseException e) {
			throw new LogicException(e.getMessage(),
					ErrorType.PARAMETRO_ERRADO);
		}
        
        return v_fechaActualFrm.compareTo(v_fechaingresadaFrm) >= 0;
	}
	
	/** Validacion que la fecha se igual a la fecha actual */
	public static boolean esfechaMayor24HActual(Date fechaActual, Date fechaingresada) {
		 long diferencia = fechaActual.getTime() - fechaingresada.getTime();
		 double horas = Math.floor(diferencia / (1000.0 * 60.0 * 60.0)); 
		 return ((int) horas)>24;
	}
	
	/** Validacion que la fecha se igual a la fecha actual */
	public static boolean esfechaMayorNMesesActual(Date fechaActual, Date fechaingresada, Integer nmeses) {
		 long diferencia = fechaActual.getTime() - fechaingresada.getTime();
		 double meses = Math.floor((double)diferencia / new Long(2629750000L)); 
		 return ((int) meses)>nmeses;
	}
	
	/** Validacion que la fecha se igual a la fecha incial */
	public static boolean esfechaMayorNMeses(Date fechaInicial, Date fechaFinal, double nmeses) {
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(fechaInicial);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(fechaFinal);

		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		return diffMonth>nmeses;
	}

	/** Validacion de datos obligatorios para los consecutivos */
	public static boolean validarObservaciones(String obs) {
		return obs.length() < 10 || obs.length() > 250;
	}
	
	/** Validacion de expresiones regulares */
	public static boolean validarExpresionRegular(String cadena, String expresionRegular){
		Pattern pat = Pattern.compile(expresionRegular);
		Matcher mat = pat.matcher(cadena);
		return mat.matches();
	}
	
	
	public static void validarAdicionarPrestacion(String codigoPrestacion, String descripcionPrestacion) throws LogicException{
		
		if((codigoPrestacion == null || quitarEspaciosCadena(codigoPrestacion).isEmpty()) && (descripcionPrestacion == null || quitarEspaciosCadena(descripcionPrestacion).isEmpty())){
			throw new LogicException(Messages.getValorError(ConstantesEJB.ERROR_ADICIONAR_PRESTACION_FALTA_CRITERIO), ErrorType.PARAMETRO_ERRADO);
		}		
	}
	
	/**
	 * Metodo que valdia el Null
	 * @param numero
	 * @return
	 */
	public static String validarNullCadena(Object entrada){
		if(entrada == null){
			return "";
		}else{
			return entrada.toString();
		}
		
	}
	
	/**
	 * Metodo que valdia el Null
	 * @param numero
	 * @return
	 */
	public static int validarNullint(Object entrada){
		if(entrada == null){
			return 0;
		}else{
			return Integer.parseInt(entrada.toString());
		}
		
	}
	
	/**
	 * Metodo que valdia el Null
	 * @param numero
	 * @return
	 */
	public static String valdiarCodigoHabilitaciones(java.util.List<String> listaCodigoHabilitacion){
		String codigoHabilitacion=null;
		if(!listaCodigoHabilitacion.isEmpty()){
			codigoHabilitacion = listaCodigoHabilitacion.get(0);
		}
		return codigoHabilitacion;
	}
	
	/**
	 * Metodo que valida el numero de identificacion de la IPS
	 * @param numero
	 * @return
	 */
	
	public static String validarNumeroIdentificacionIPS(IPSVO ipsVo) {
		String numeroIdentificacionIPS = null;
		
		if(ipsVo.getNumeroIdentificacion() != null){
			numeroIdentificacionIPS = Utilidades.quitarEspaciosCadena(ipsVo.getNumeroIdentificacion());
		}
		
		return numeroIdentificacionIPS;
	}

	/**
	 * Metodo que consulta una propiedad a partir de una clasificacion, la aplicacion 		
	 * siempre es: GestionAutorizacionesSalud		
	 * @param propiedadAplicacion		
	 * @param clasificacion		
	 * @return		
	 * @throws LogicException		
	 */		
	public static String obtenerParametroPropertyQuery(String clasificacion, String propiedadAplicacion)		
			throws LogicException {		
		String cadenaPropiedad;		
		ConsultarPropertyQueryEJB propertyQuery = new ConsultarPropertyQueryEJB();		
		PropiedadVO propiedad = new PropiedadVO();		
		propiedad.setAplicacion(ConsultaProperties.getString(ConstantesEJB.APLICACION_GESTION_SALUD));		
				
		propiedad.setClasificacion(clasificacion);		
		propiedad.setPropiedad(propiedadAplicacion);		
		try {		
			cadenaPropiedad = propertyQuery.consultarPropiedad(propiedad);		
		} catch (Exception e) {		
			throw new LogicException(		
					Messages.getValorError(ConstantesEJB.ERROR_PROPERTY_QUERY),		
					e, LogicException.ErrorType.DATO_NO_EXISTE);		
		}		
		return cadenaPropiedad;		
	}

	/**		
	 * Valida si el correo tiene una estructuta valida.		
	 * @param correo		
	 * @return		
	 */		
	public static boolean validarCorreoElectronico(String correo){		
		final String patronMail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"		
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";		
		Pattern pattern = Pattern.compile(patronMail);		
		Matcher matcher = pattern.matcher(correo);		
        return matcher.matches();		
	}
	
	
	/** Validacion de datos sobre la lista de medicos */
	public static void validarListaMedicoNoAdscrito(java.util.List<?> lista, String excepcionString) throws LogicException {
		if (!lista.isEmpty()) {
			throw new LogicException(excepcionString, ErrorType.PARAMETRO_ERRADO);
		}
	}
	public static String formatoNumerico(Double valor){
		DecimalFormat formatea = new DecimalFormat("###,###.##");
		
		return formatea.format(valor);
	}	
}
