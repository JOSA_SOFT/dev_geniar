package com.sos.gestionautorizacionessaludejb.util;

import org.apache.log4j.Logger;

import com.sos.excepciones.LogicException;
import com.sos.excepciones.LogicException.ErrorType;
import com.sos.gestionautorizacionessaluddata.model.parametros.PropiedadVO;
import com.sos.propertyquery.serviceproxy.PropertyQueryServiceProxy;




/**
 * Implementacion del ejb que provee acceso a componente PropertyQueryService EJB
 * @author waguilera
 * 09/09/2014
 *
 */


public class ConsultarPropertyQueryEJB {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultarPropertyQueryEJB.class);
	
	public String consultarPropiedad(PropiedadVO propiedadVO) throws LogicException {
		PropertyQueryServiceProxy proxy = new PropertyQueryServiceProxy();
		String propiedad = null;
		try {
			propiedad = proxy.getProperty(propiedadVO.getAplicacion(), propiedadVO.getClasificacion(), propiedadVO.getPropiedad());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new LogicException(e.getMessage(), ErrorType.INDEFINIDO);
		}	
		return propiedad;
	}	

}

