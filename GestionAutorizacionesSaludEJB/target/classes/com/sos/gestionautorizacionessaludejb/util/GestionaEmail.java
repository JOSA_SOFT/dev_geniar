package com.sos.gestionautorizacionessaludejb.util;

import java.io.File;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.sos.mail.SOSMailHelper;


public class GestionaEmail {

	/**
	 * Variable que almacena la instancia del Logger
	 */
	private static final Logger LOG = Logger.getLogger(GestionaEmail.class);
	
	/**
	 *  Metodo el cual se encarga de ejecutar las instruciones para enviar correo electronicos 
	 * @param remitente
	 * @param correos
	 * @param asunto
	 * @param mensaje
	 * @return
	 * @throws Exception
	 */
	public static boolean enviarEmail(String remitente,String correos ,String asunto,String mensaje)throws Exception{
		
		
	
		SOSMailHelper mailSession = SOSMailHelper.create("mail/enviocorreosos");
		
		if(remitente == null || remitente.trim().equals("")){
		  remitente = "ogranados@sos.com.co";
		}
		
		if(correos == null || correos.trim().equals("")){
		  correos ="ogranados@sos.com.co";
		}
		
		if(mensaje == null || mensaje.trim().equals("")){
		  mensaje = "Esto es una prueba de envio de correo de Servicios Occidentales de Salud(S.O.S) ";
		}
		
		if(asunto == null || asunto.trim().equals("")){
		  asunto = "CORREOS SOS";
		}
		String[] direcciones = correos.split(","); 
		
		Address destinosCorreo[] =  new Address[direcciones.length];
		Address copiasCorreo[] = null;
		Address copiasCorreoOculto[] = null;
		
		System.out.println("+*-"+correos+"-*+   ---> Tamano: "+direcciones.length+" - "+remitente);
		for(int i=0; i<direcciones.length;i++){
			destinosCorreo[i] = new InternetAddress(direcciones[i].trim());
		}
		
		File[] adjunto = {};
	//	SOSFileOnBytes fileOnBytes = sosFiles.SOSFileOnBytes ;
		mailSession.enviarEmail(remitente, asunto, mensaje, destinosCorreo, copiasCorreo, copiasCorreoOculto, adjunto)  ;
	//	SOSFileOnBytes
		
	
		return true;
		
	}
	
	
	public static void main(String[] arg){
		
		try {
			enviarEmail(null,null,null,null);
		} catch (Exception e) {
			LOG.error(e);
		}
		
		
	}
	
}
