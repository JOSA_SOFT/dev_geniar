package com.sos.gestionautorizacionessaludejb.util;

import com.sos.dataccess.connectionprovider.ConnectionProvider;
import com.sos.dataccess.connectionprovider.ConnectionProviderException;
import com.sos.dataccess.connectionprovider.ConnectionProviderGeneral;

/**
 * Class ConnProviderAfiliacion
 * @author ing. Victor Hugo Gil Ramos
 *
 */
public class ConnProviderAfiliacion {
	
	private ConnProviderAfiliacion() {
	}
	
	public static ConnectionProvider getConnProvider() throws ConnectionProviderException {
		return ConnectionProviderGeneral.getInstance(ConnProviderAfiliacion.class
				.getResourceAsStream(ConstantesEJB.PROPIEDADES_AFILIACION));
	}

}
