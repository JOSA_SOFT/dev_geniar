package com.sos.gestionautorizacionessaludejb.service.controller;

import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;

import co.com.sos.ErrorType;
import co.com.sos.ErroresType;


/**
 * Class ErroresServiceType
 * Clase que setea la informacion de los errores del servicio
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/03/2016
 *
 */
public class ErroresServiceType {
	
	/** Identifica el objeto de erroresType*/
	private ErroresType erroresType;
	
	/** Lista que se utiliza para almacenar el listado de errores*/
	private List<ErrorType> lErrorService;
	
	public ErroresServiceType (ErroresType erroresType){
		this.erroresType = erroresType;
	}
	
	/** Metodo que se utiliza para almacenar el listado de errores*/
	public ServiceErrorVO informacionErrorServicio(){
		
		ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
		
		if (erroresType != null && erroresType.getError() != null) {
			lErrorService = erroresType.getError();

			for (ErrorType errorType : lErrorService) {
				serviceErrorVO.setCodigoError(String.valueOf(errorType.getCodigo()));
				serviceErrorVO.setMensajeError(errorType.getMensaje());
			}
		}
		
		return serviceErrorVO;		
	}	
}
