package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;
import java.util.List;

import co.com.sos.prestacion.v1.FormaFarmaceuticaType;
import co.com.sos.prestacion.v1.GrupoTerapeuticoType;
import co.com.sos.prestacion.v1.LateralidadType;
import co.com.sos.prestacion.v1.PresentacionDosisType;
import co.com.sos.prestacion.v1.PresentacionPrestacionUnidadType;
import co.com.sos.prestacion.v1.PrestacionType;
import co.com.sos.prestacion.v1.PrestacionesType;
import co.com.sos.prestacion.v1.UnidadConcentracionType;
import co.com.sos.prestacion.v1.UnidadTiempoPeriodicidadType;
import co.com.sos.prestacion.v1.ViaAccesoType;
import co.com.sos.prestacion.v1.ViaAdministracionType;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class PrestacionesServiceType
 * Clase que setea la informacion de las prestaciones
 * @author ing. Victor Hugo Gil Ramos
 * @version 23/03/2016
 *
 */
public class PrestacionesServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	private List<PrestacionDTO> lPrestacionesSolicitud;	
	
	public PrestacionesServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	/** Inicio informacion de las prestaciones */
	public PrestacionesType informacionPrestacionesServiceType(){
		lPrestacionesSolicitud = solicitudVO.getlPrestacionesSolicitud();
		PrestacionesType prestacionesType = new PrestacionesType();

		for (PrestacionDTO prestacionDTO : lPrestacionesSolicitud) {
			
			PrestacionType prestacionType = new PrestacionType();

			// identifica el tipo de prestacion
			co.com.sos.prestacion.v1.TipoType tipoPrestacionType = new co.com.sos.prestacion.v1.TipoType();
			tipoPrestacionType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getTipoCodificacionVO().getConsecutivoCodigoTipoCodificacion(), BigInteger.ZERO));			
			tipoPrestacionType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getTipoCodificacionVO().getCodigoTipoCodificacion()));
			tipoPrestacionType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getTipoCodificacionVO().getDescripcionTipoCodificacion()));

			// identifica la prestacion
			co.com.sos.prestacion.v1.IdentificadorType identificadorPrestacionType = new co.com.sos.prestacion.v1.IdentificadorType();
			identificadorPrestacionType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getConsecutivoCodificacionPrestacion(), BigInteger.ZERO));
			identificadorPrestacionType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getCodigoCodificacionPrestacion()));

			prestacionType.setTipo(tipoPrestacionType);
			prestacionType.setIdentificador(identificadorPrestacionType);
			prestacionType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getDescripcionCodificacionPrestacion()));
			prestacionType.setCantidad(Utilidades.validaNumero(prestacionDTO.getCantidad(), BigInteger.ZERO));

			// Via Acceso
			informacionViaAcceso(prestacionDTO, prestacionType);

			if (prestacionDTO.getTipoCodificacionVO().getConsecutivoCodigoTipoCodificacion() == ConstantesEJB.CONSECUTIVO_CUMS) {

				// Concentracion del medicamento
				UnidadConcentracionType unidadConcentracionType = new UnidadConcentracionType();
				unidadConcentracionType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(prestacionDTO.getConcentracionVO().getConsecutivoConcentracion()));
				unidadConcentracionType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getConcentracionVO().getCodigoConcentracion()));
				unidadConcentracionType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getConcentracionVO().getDescripcionConcentracion()));

				prestacionType.setConcentracion(prestacionDTO.getConcentracionVO().getCantidadConcentracion());
				prestacionType.setUnidadConcentracion(unidadConcentracionType);

				// Presentacion del medicamento
				PresentacionPrestacionUnidadType presentacionPrestacionUnidadType = new PresentacionPrestacionUnidadType();
				presentacionPrestacionUnidadType.setConsecutivo(prestacionDTO.getPresentacionVO().getConsecutivoCodigoPresentacion());
				presentacionPrestacionUnidadType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getPresentacionVO().getCodigoPresentacion()));
				presentacionPrestacionUnidadType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getPresentacionVO().getDescripcionPresentacion()));

				prestacionType.setPresentacionPrestacion(Utilidades.validaCadenaTexto(prestacionDTO.getPresentacionVO().getCantidadPresentacion()));
				prestacionType.setPresentacionPrestacionUnidad(presentacionPrestacionUnidadType);

				// Dosis del medicamento
				PresentacionDosisType dosisType = new PresentacionDosisType();
				dosisType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getDosisVO().getConsecutivoCodigoDosis(), BigInteger.ZERO));
				
				dosisType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getDosisVO().getCodigoDosis()));
				dosisType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getDosisVO().getCodigoDosis()));

				prestacionType.setDosis(prestacionDTO.getMedicamentosVO().getCantidadDosis());
				prestacionType.setPresentacionDosis(dosisType);

				// Frecuencia del medicamento
				UnidadTiempoPeriodicidadType unidadTiempoPeriodicidadType = new UnidadTiempoPeriodicidadType();
				unidadTiempoPeriodicidadType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getMedicamentosVO().getFrecuenciaVO().getConsecutivoFrecuencia(), BigInteger.ZERO));
				
				unidadTiempoPeriodicidadType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getFrecuenciaVO().getCodigoFrecuencia()));
				unidadTiempoPeriodicidadType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getFrecuenciaVO().getDescripcionFrecuencia()));

				prestacionType.setPeriodicidadDosis(prestacionDTO.getMedicamentosVO().getHoraFrecuencia());
				prestacionType.setUnidadTiempoPeriodicidad(unidadTiempoPeriodicidadType);

				// Grupo terapeutico
				GrupoTerapeuticoType grupoTerapeuticoType = new GrupoTerapeuticoType();
				grupoTerapeuticoType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getCodigoSubgrupoTerapeutico()));
				grupoTerapeuticoType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getDescripcionClaseFarmacologico()));
				prestacionType.setGrupoTerapeutico(grupoTerapeuticoType);

				// Via Administracion
				informacionViaAdministracion(prestacionDTO, prestacionType);
				
				// Forma Farmaceutica
				FormaFarmaceuticaType formaFarmaceuticaType = new FormaFarmaceuticaType();
				formaFarmaceuticaType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getCodigoFormaFarmaceutica()));
				formaFarmaceuticaType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getDescripcionFormaFarmaceutica()));
				prestacionType.setFormaFarmaceutica(formaFarmaceuticaType);

			} else {

				// Lateralidad
				LateralidadType lateralidadType = new LateralidadType();
				lateralidadType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getLateralidadesVO().getConsecutivoLaterialidad(), BigInteger.ZERO)); 
				lateralidadType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getLateralidadesVO().getCodigoLaterialidad()));
				lateralidadType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getLateralidadesVO().getDescripcionLateralidad()));
				prestacionType.setLateralidad(lateralidadType);
			}
			prestacionType.setMarcaCalidad(prestacionDTO.getMarcaCalidad());
			prestacionesType.getPrestacion().add(prestacionType);
		}
		
		return prestacionesType;		
	}

	
	// Via Acceso
	private void informacionViaAdministracion(PrestacionDTO prestacionDTO,
			PrestacionType prestacionType) {
		if (prestacionDTO.getMedicamentosVO().getCodigoViaAdministracion() == null) {
			ViaAdministracionType viaAdministracionType = new ViaAdministracionType();
			viaAdministracionType.setCodigo(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getCodigoViaAdministracion()));
			viaAdministracionType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getMedicamentosVO().getDescripcionViaAdministracion()));
			prestacionType.setViaAdministracion(viaAdministracionType);
		}
	}

	// Via Administracion
	private void informacionViaAcceso(PrestacionDTO prestacionDTO,
			PrestacionType prestacionType) {
		if (prestacionDTO.getViaAccesoVO().getConsecutivoViaAcceso() != null) {
			ViaAccesoType viaAccesoType = new ViaAccesoType();
			viaAccesoType.setConsecutivo(Utilidades.validaNumero(prestacionDTO.getViaAccesoVO().getConsecutivoViaAcceso(), BigInteger.ONE)); 
			viaAccesoType.setDescripcion(Utilidades.validaCadenaTexto(prestacionDTO.getViaAccesoVO().getDescripcionViaAcceso()));
			prestacionType.setViaAcceso(viaAccesoType);
		}
	}
}
