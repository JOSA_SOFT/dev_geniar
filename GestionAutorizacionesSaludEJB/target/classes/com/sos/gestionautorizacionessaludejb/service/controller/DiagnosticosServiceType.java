package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;
import java.util.List;

import co.com.sos.diagnostico.v1.ContingenciaType;
import co.com.sos.diagnostico.v1.DiagnosticoType;
import co.com.sos.diagnostico.v1.DiagnosticosType;
import co.com.sos.diagnostico.v1.RecobroType;
import co.com.sos.diagnostico.v1.TipoType;

import com.sos.gestionautorizacionessaluddata.model.dto.DiagnosticoDTO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Messages;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class DiagnosticosServiceType
 * Clase que setea la informacion de los diagnosticos
 * @author ing. Victor Hugo Gil Ramos
 * @version 23/03/2016
 *
 */
public class DiagnosticosServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	private List<DiagnosticoDTO> lDiagnosticosSolicitud;
	
	public DiagnosticosServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	/** informacion de los diagnosticos */
	public DiagnosticosType informacionDiagnosticosServiceType(){
		
		lDiagnosticosSolicitud = solicitudVO.getlDiagnosticosSolicitud();
		DiagnosticosType diagnosticosType = new DiagnosticosType();

		for (DiagnosticoDTO diagnosticoDTO : lDiagnosticosSolicitud) {

			// Se adiciona el diagnostico a la lista de diagnosticos
			DiagnosticoType diagnosticoSolicitudType = new DiagnosticoType();

			// identificador del diagnostico
			co.com.sos.diagnostico.v1.IdentificadorType identificadorDiagnosticoType = new co.com.sos.diagnostico.v1.IdentificadorType();
			identificadorDiagnosticoType.setConsecutivo(Utilidades.validaNumero(diagnosticoDTO.getDiagnosticosVO().getConsecutivoCodigoDiagnostico(), BigInteger.ZERO));
			identificadorDiagnosticoType.setCodigo(Utilidades.validaCadenaTexto(diagnosticoDTO.getDiagnosticosVO().getCodigoDiagnostico()));
			identificadorDiagnosticoType.setDescripcion(Utilidades.validaCadenaTexto(diagnosticoDTO.getDiagnosticosVO().getDescripcionDiagnostico()));

			// identificador del tipo de diagnostico
			TipoType tipoDiagnosticoType = new TipoType();
			tipoDiagnosticoType.setConsecutivo(Utilidades.validaNumero(diagnosticoDTO.getTiposDiagnosticosVO().getConsecutivoTipoDiagnostico(), BigInteger.ZERO));			
			tipoDiagnosticoType.setCodigo(Utilidades.validaCadenaTexto(diagnosticoDTO.getTiposDiagnosticosVO().getCodigoTipoDiagnostico()));
			tipoDiagnosticoType.setDescripcion(Utilidades.validaCadenaTexto(diagnosticoDTO.getTiposDiagnosticosVO().getDescripcionTipoDiagnostico()));

			// identificador del recobro

			if (diagnosticoDTO.getTiposDiagnosticosVO().getConsecutivoTipoDiagnostico() == Integer.parseInt(Messages.getValorParametro(ConstantesEJB.DIAGNOSTICO_PRINCIPAL)) && solicitudVO.getRecobroVO() != null	&& solicitudVO.getRecobroVO().getConsecutivoCodigoRecobro() != 0) {
				RecobroType recobroType = new RecobroType();
				recobroType.setConsecutivo(Utilidades.validaNumero(solicitudVO.getRecobroVO().getConsecutivoCodigoRecobro(), BigInteger.ZERO));				
				recobroType.setDescripcion(Utilidades.validaCadenaTexto(diagnosticoDTO.getRecobroVO().getDescripcionRecobro()));
				diagnosticoSolicitudType.setRecobro(recobroType);
			}

			// identificador de la contingencia del recobro
			ContingenciaType contingenciaType = new ContingenciaType();
			if (diagnosticoDTO.getContingenciasVO() != null) {
				contingenciaType.setConsecutivo(Utilidades.validaNumero(diagnosticoDTO.getContingenciasVO().getConsecutivoCodigoContingencia(), BigInteger.ZERO));
				contingenciaType.setCodigo(Utilidades.validaCadenaTexto(diagnosticoDTO.getContingenciasVO().getCodigoContingencia()));
				contingenciaType.setDescripcion(Utilidades.validaCadenaTexto(diagnosticoDTO.getContingenciasVO().getDescripcionContingencia()));
			}
			
			diagnosticoSolicitudType.setIdentificador(identificadorDiagnosticoType);
			diagnosticoSolicitudType.setTipo(tipoDiagnosticoType);
			diagnosticoSolicitudType.setContingencia(contingenciaType);

			diagnosticosType.getDiagnostico().add(diagnosticoSolicitudType);
		}
		
		return diagnosticosType;		
	}
}
