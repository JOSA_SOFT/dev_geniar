package com.sos.gestionautorizacionessaludejb.service.controller;

import java.util.ArrayList;
import java.util.List;

import co.com.sos.prestacion.v1.PrestacionFechaEntregaResType;
import co.com.sos.solicitud.v1.SolicitudFechaEntregaResType;
import co.com.sos.solicitud.v1.SolicitudesFechaEntregaResType;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.servicios.FechaEntregaSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.PrestacionFechaEntregaVO;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;


/**
 * Class SolicitudesFechaEntregaServiceType
 * Clase que permite consultar el resultado del servicio de fecha de entrega
 * @author ing. Victor Hugo Gil Ramos
 * @version 31/03/2016
 *
 */
public class SolicitudesFechaEntregaServiceType {
	
	/*Identifica el listado de las prestaciones asociados a la fecha de entrega */
	private List<PrestacionFechaEntregaVO> lPrestacionFechaEntregaVO;
	private List<SolicitudFechaEntregaResType> lSolicitudesFechaEntregaResType;
	
	public FechaEntregaSolicitudVO informacionFechaEntregaService(SolicitudesFechaEntregaResType solicitudesFechaEntregaResType){
		
		FechaEntregaSolicitudVO fechaEntregaSolicitudVO = null;		
		List<PrestacionFechaEntregaResType> lPrestacionFechaEntregaResType = null;
		PrestacionFechaEntregaVO prestacionFechaEntregaVO = null;
		
		lSolicitudesFechaEntregaResType = solicitudesFechaEntregaResType.getSolicitudFechaEntregaRes();
		
		for (SolicitudFechaEntregaResType solicitudFechaEntregaResType : lSolicitudesFechaEntregaResType) {			
			lPrestacionFechaEntregaVO = new ArrayList<PrestacionFechaEntregaVO>();			
			
			fechaEntregaSolicitudVO = new FechaEntregaSolicitudVO();
			fechaEntregaSolicitudVO.setConsecutivoSolicitud(solicitudFechaEntregaResType.getConsecutivo().intValue());
			fechaEntregaSolicitudVO.setNumeroRadicacionSolicitud(solicitudFechaEntregaResType.getNumeroSolicitudSOS());
			fechaEntregaSolicitudVO.setConsecutivoTipoPlan(solicitudFechaEntregaResType.getConsecutivoTipoPlan());
			fechaEntregaSolicitudVO.setTipoPlan(solicitudFechaEntregaResType.getTipoPlan());
						
			lPrestacionFechaEntregaResType = solicitudFechaEntregaResType.getPrestacionesFechaEntrega().getPrestacionFechaEntregaRes();			
						
			for (PrestacionFechaEntregaResType prestacionFechaEntregaResType : lPrestacionFechaEntregaResType) {
				 PrestacionDTO prestacionDTO = new PrestacionDTO();
				 prestacionFechaEntregaVO = new PrestacionFechaEntregaVO();
				 
				 prestacionDTO.setConsecutivoCodificacionPrestacion(prestacionFechaEntregaResType.getConsecutivo().intValue());
				 prestacionDTO.setCodigoCodificacionPrestacion(prestacionFechaEntregaResType.getCodigo());
		
				 prestacionFechaEntregaVO.setPrestacionDTO(prestacionDTO);
				 prestacionFechaEntregaVO.setConsecutivoGrupoEntrega(prestacionFechaEntregaResType.getConsecutivoGrupoEntrega());
				 prestacionFechaEntregaVO.setGrupoEntrega(prestacionFechaEntregaResType.getGrupoEntrega());
				 prestacionFechaEntregaVO.setFechaSolicitud(Utilidades.toDate(prestacionFechaEntregaResType.getFechaSolicitud()));
				 prestacionFechaEntregaVO.setFechaEstimadaEntrega(Utilidades.toDate(prestacionFechaEntregaResType.getFechaEstimadaEntrega()));
				 
				 lPrestacionFechaEntregaVO.add(prestacionFechaEntregaVO);				 
			}			
			
			fechaEntregaSolicitudVO.setlPrestacionFechaEntregaVO(lPrestacionFechaEntregaVO);			
		}	
		
		return fechaEntregaSolicitudVO;
	}
}
