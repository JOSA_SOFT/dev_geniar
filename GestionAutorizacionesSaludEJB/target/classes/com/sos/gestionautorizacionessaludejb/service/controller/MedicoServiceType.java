package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.medico.v1.EspecialidadType;
import co.com.sos.medico.v1.MedicoType;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class MedicoServiceType
 * Clase que setea la informacion del medico
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class MedicoServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public MedicoServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	/**se setea la informacion del medico */
	public MedicoType informacionMedicoServiceType(){
		
		/** Inicio informacion del medico */
		// Se adiciona el tipo de identificacion del medico al servicio
		co.com.sos.medico.v1.TipoIdentificacionType identificadorTypeMedico = new co.com.sos.medico.v1.TipoIdentificacionType();
		identificadorTypeMedico.setConsecutivo(Utilidades.validaNumero(solicitudVO.getMedicoVO().getTiposIdentificacionMedicoVO().getConsecutivoTipoIdentificacion(),BigInteger.ZERO));
		identificadorTypeMedico.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getTiposIdentificacionMedicoVO().getCodigoTipoIdentificacion()));

		EspecialidadType especialidadType = new EspecialidadType();
		especialidadType.setConsecutivo(Utilidades.validaNumero(solicitudVO.getMedicoVO().getEspecialidadMedicoVO().getConsecutivoCodigoEspecialidad(), BigInteger.ZERO));
		especialidadType.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getEspecialidadMedicoVO().getDescripcionEspecialidad()));

		// Se adiciona la informacion del medico al servicio
		MedicoType medicoType = new MedicoType();
		medicoType.setTipoIdentificacion(identificadorTypeMedico);
		medicoType.setNumeroIdentificacion(Utilidades.validaNumeroIdentificacion(solicitudVO.getMedicoVO().getNumeroIdentificacionMedico()));		
		medicoType.setNumeroUnicoIdentificacion(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getNumeroUnicoIdentificacionMedico()));
		medicoType.setPrimerNombre(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getPrimerNombreMedico()));
		medicoType.setSegundoNombre(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getSegundoNombreMedico()));
		medicoType.setPrimerApellido(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getPrimerApellidoMedico()));
		medicoType.setSegundoApellido(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getSegundoApellidoMedico()));
		medicoType.setNumeroTarjetaProfesional(Utilidades.validaCadenaTexto(solicitudVO.getMedicoVO().getRegistroMedico()));
		
		medicoType.setAdscrito(Utilidades.validaCadenaTextoCombo(solicitudVO.getMedicoVO().getMedicoAdscrito(), ConstantesEJB.NO));
		medicoType.setEspecialidad(especialidadType);
		
		return medicoType;		
	}
}
