package com.sos.gestionautorizacionessaludejb.service.controller;

import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.malla.AdvertenciaVO;
import com.sos.gestionautorizacionessaluddata.model.malla.ServiceErrorVO;

import co.com.sos.AdvertenciaType;
import co.com.sos.AdvertenciasType;

/**
 * Class AdvertenciaServiceType
 * Clase que setea la informacion de las advertencias del servicio
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/03/2016
 *
 */
public class AdvertenciaServiceType {
	
	/** Identifica el objeto de advertenciasType*/
	private AdvertenciasType advertenciasType;
	
	/** Lista que se utiliza para almacenar el listado de advertencias*/
	private List<AdvertenciaType> lAdvertenciaType;
	
	/** Lista que se utiliza para almacenar el listado de advertencias*/
	private List<AdvertenciaVO> lAdvertenciaVO;
	
	
	public AdvertenciaServiceType(AdvertenciasType advertenciasType){
		this.advertenciasType = advertenciasType;
	}
	
	/** Metodo que se utiliza para almacenar el listado de advertencias*/	
	public ServiceErrorVO informacionAdvertenciasServicio(){		
		
		ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
		
		if (advertenciasType != null && advertenciasType.getAdvertencia() != null) {
			
			lAdvertenciaVO = new ArrayList<AdvertenciaVO>();
			
			lAdvertenciaType = advertenciasType.getAdvertencia();			
			for (AdvertenciaType advertenciaType : lAdvertenciaType) {
				AdvertenciaVO advertenciaVO = new AdvertenciaVO();
				advertenciaVO.setCodigoAdvertencia(String.valueOf(advertenciaType.getCodigo()));
				advertenciaVO.setMensajeAdvertencia(advertenciaType.getMensaje());	
				
				lAdvertenciaVO.add(advertenciaVO);
			}
			serviceErrorVO.setlAdvertenciaVO(lAdvertenciaVO);
		}		
		
		return serviceErrorVO;		
	}
}
