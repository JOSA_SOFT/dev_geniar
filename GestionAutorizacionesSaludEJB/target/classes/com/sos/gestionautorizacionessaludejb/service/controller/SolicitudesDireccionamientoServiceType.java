package com.sos.gestionautorizacionessaludejb.service.controller;

import java.util.ArrayList;
import java.util.List;

import com.sos.gestionautorizacionessaluddata.model.dto.PrestacionDTO;
import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.PrestadorVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.DireccionamientoSolicitudVO;
import com.sos.gestionautorizacionessaluddata.model.servicios.PrestacionDireccionamientoVO;

import co.com.sos.prestacion.v1.PrestacionDireccionamientoResType;
import co.com.sos.prestador.v1.PrestadorDireccionamientoResType;
import co.com.sos.solicitud.v1.SolicitudDireccionamientoResType;
import co.com.sos.solicitud.v1.SolicitudesDireccionamientoResType;

/**
 * Class SolicitudesDireccionamientoServiceType Clase que permite consultar el
 * resultado del servicio de direccionamiento
 * 
 * @author ing. Victor Hugo Gil Ramos
 * @version 30/03/2016
 *
 */
public class SolicitudesDireccionamientoServiceType {

	/* Identifica el listado de las prestaciones asociados al direccionamiento */
	private List<PrestacionDireccionamientoVO> lPrestacionDireccionamientoVO;
	private List<SolicitudDireccionamientoResType> lSolicitudDireccionamientoResType;

	public DireccionamientoSolicitudVO informacionDireccionamientoService(SolicitudesDireccionamientoResType solicitudesDireccionamientoResType) {

		DireccionamientoSolicitudVO direccionamientoSolicitudVO = new DireccionamientoSolicitudVO();
		PrestacionDireccionamientoVO prestacionDireccionamientoVO = new PrestacionDireccionamientoVO();
		List<PrestadorVO> lPrestadorVO;

		lSolicitudDireccionamientoResType = solicitudesDireccionamientoResType.getSolicitudDireccionamientoRes();

		for (SolicitudDireccionamientoResType solicitudDireccionamientoResType : lSolicitudDireccionamientoResType) {
			direccionamientoSolicitudVO.setConsecutivoSolicitud(solicitudDireccionamientoResType.getConsecutivo().intValue());
			direccionamientoSolicitudVO.setNumeroRadicacionSolicitud(solicitudDireccionamientoResType.getNumeroSolicitudSOS());
			direccionamientoSolicitudVO.setNumeroSolicitudPrestador(solicitudDireccionamientoResType.getNumeroSolicitudPrestador());

			List<PrestacionDireccionamientoResType> lPrestacionDireccionamiento = solicitudDireccionamientoResType.getPrestacionesDireccionamiento().getPrestacionDireccionamientoRes();
			lPrestacionDireccionamientoVO = new ArrayList<PrestacionDireccionamientoVO>();

			for (PrestacionDireccionamientoResType prestacionDireccionamientoResType : lPrestacionDireccionamiento) {
				PrestacionDTO prestacionDTO = new PrestacionDTO();
				lPrestadorVO = new ArrayList<PrestadorVO>();
				prestacionDTO.setConsecutivoCodificacionPrestacion(prestacionDireccionamientoResType.getConsecutivo() != null ? prestacionDireccionamientoResType.getConsecutivo().intValue() : null);
				prestacionDTO.setCodigoCodificacionPrestacion(prestacionDireccionamientoResType.getCodigo());

				List<PrestadorDireccionamientoResType> lPrestadorDireccinamiento = prestacionDireccionamientoResType.getPrestadoresDireccionamientoRes().getPrestadorDireccionamientoRes();

				for (PrestadorDireccionamientoResType prestadorDireccionamientoResType : lPrestadorDireccinamiento) {
					PrestadorVO prestadorVO = new PrestadorVO();
					prestadorVO.setConsecutivoIpsDestino(prestadorDireccionamientoResType.getConsecutivo() != null ? prestadorDireccionamientoResType.getConsecutivo().intValue() : null);
					prestadorVO.setCodigoInterno(prestadorDireccionamientoResType.getCodigoInternoIpsdestino());

					lPrestadorVO.add(prestadorVO);
				}

				prestacionDireccionamientoVO.setPrestacionDTO(prestacionDTO);
				prestacionDireccionamientoVO.setlPrestadorVO(lPrestadorVO);

				lPrestacionDireccionamientoVO.add(prestacionDireccionamientoVO);
			}
			direccionamientoSolicitudVO.setlPrestacionDireccionamientoVO(lPrestacionDireccionamientoVO);
		}
		return direccionamientoSolicitudVO;
	}
}
