package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.prestador.v1.PrestadorType;
import co.com.sos.prestador.v1.SucursalType;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class PrestadorServiceType
 * Clase que setea la informacion del prestador
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class PrestadorServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public PrestadorServiceType(RegistrarSolicitudVO solicitudVO){
		 this.solicitudVO = solicitudVO;
	}
		
	/** Informacion del prestador */
	public PrestadorType informacionPrestadorServiceType(){		
		
		// Se adiciona el tipo de identificacion del prestador al servicio
		co.com.sos.prestador.v1.TipoIdentificacionType identificadorTypePrestador = new co.com.sos.prestador.v1.TipoIdentificacionType();
		identificadorTypePrestador.setConsecutivo(Utilidades.validaNumero(solicitudVO.getPrestadorVO().getTiposIdentificacionPrestadorVO().getConsecutivoTipoIdentificacion(), BigInteger.ZERO));  
		
		identificadorTypePrestador.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getTiposIdentificacionPrestadorVO().getCodigoTipoIdentificacion()));
		identificadorTypePrestador.setDescripcion(ConstantesEJB.CADENA_VACIA);

		co.com.sos.prestador.v1.CiudadType ciudadTypePrestador = new co.com.sos.prestador.v1.CiudadType();
		ciudadTypePrestador.setConsecutivo(Utilidades.validaNumero(solicitudVO.getPrestadorVO().getCiudadVO().getConsecutivoCodigoCiudad(), BigInteger.ZERO));		
		ciudadTypePrestador.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getCiudadVO().getCodigoCiudad()));
		ciudadTypePrestador.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getCiudadVO().getDescripcionCiudad()));

		// Se adiciona la informacion del prestador al servicio
		PrestadorType prestadorType = new PrestadorType();
		prestadorType.setTipoIdentificacion(identificadorTypePrestador);
		prestadorType.setNumeroIdentificacion(Utilidades.validaNumeroIdentificacion(solicitudVO.getPrestadorVO().getNumeroIdentificacionPrestador()));
		
		prestadorType.setCodigoInterno(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getCodigoInterno()));
		prestadorType.setNombre(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getRazonSocial()));
		
		prestadorType.setAdscrito(Utilidades.validaCadenaTextoCombo(solicitudVO.getPrestadorVO().getPrestadorAdscrito(), ConstantesEJB.NO));
		prestadorType.setCiudad(ciudadTypePrestador);

		if (solicitudVO.getPrestadorVO().getNombreSucursal() != null || !ConstantesEJB.CADENA_VACIA.equals(solicitudVO.getPrestadorVO().getNombreSucursal())) {
			SucursalType sucursalType = new SucursalType();
			sucursalType.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getNombreSucursal()));
			prestadorType.setSucursal(sucursalType);
		}

		prestadorType.setDireccion(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getDireccionPrestador()));
		prestadorType.setTelefono(Utilidades.validaCadenaTexto(solicitudVO.getPrestadorVO().getNumeroTelefonicoPrestador()));
		
		return prestadorType;						
	}
}
