package com.sos.gestionautorizacionessaludejb.service.controller;

import java.math.BigInteger;

import co.com.sos.afiliado.v1.AfiliadoType;
import co.com.sos.afiliado.v1.DepartamentoType;
import co.com.sos.afiliado.v1.EstadoDerechoType;
import co.com.sos.afiliado.v1.IpsPrimariaType;
import co.com.sos.afiliado.v1.PlanType;
import co.com.sos.afiliado.v1.SedeIpsPrimariaType;
import co.com.sos.afiliado.v1.SexoType;
import co.com.sos.afiliado.v1.TipoContratoType;
import co.com.sos.afiliado.v1.TipoIdentificacionType;
import co.com.sos.afiliado.v1.VinculacionType;

import com.sos.gestionautorizacionessaluddata.model.registrasolicitud.RegistrarSolicitudVO;
import com.sos.gestionautorizacionessaludejb.util.ConstantesEJB;
import com.sos.gestionautorizacionessaludejb.util.Utilidades;

/**
 * Class AfiliadoServiceType
 * Clase que setea la informacion del afiliado
 * @author ing. Victor Hugo Gil Ramos
 * @version 22/03/2016
 *
 */
public class AfiliadoServiceType {
	
	private RegistrarSolicitudVO solicitudVO;
	
	public AfiliadoServiceType(RegistrarSolicitudVO solicitudVO){
		this.solicitudVO = solicitudVO;
	}
	
	
	public AfiliadoType informacionAfiliadoServiceType(){
		
		// Se adiciona el tipo de identificacion del afiliado al servicio
		TipoIdentificacionType tipoIdentificacionAfiliado = consultaTipoIdentificacionType();		
		
		co.com.sos.afiliado.v1.CiudadType ciudadAfiliadoType = new co.com.sos.afiliado.v1.CiudadType();
		ciudadAfiliadoType.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getCiudadResidencia().getCodigoCiudad()));
		ciudadAfiliadoType.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getCiudadResidencia().getDescripcionCiudad()));
		
		DepartamentoType departamentoType = new DepartamentoType();
		departamentoType.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getDescripcionDepartamento()));
		
		/** plan Afiliado **/
		PlanType planAfiliadoType = consultaPlanType();
		
		/** tipo de cotizante **/
		VinculacionType vinculacionType = consultaTipoVinculacionType();
		
		// genero afiliado
		SexoType sexoType = new SexoType();
		sexoType.setConsecutivo(Utilidades.validaNumero(solicitudVO.getAfiliadoVO().getGenero().getConsecutivoGenero(), BigInteger.ONE));
		sexoType.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getGenero().getCodigoGenero()));

		// ips primaria
		IpsPrimariaType ipsPrimariaType = new IpsPrimariaType();
		ipsPrimariaType.setCodigo(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getCodigoIPSPrimaria()));
		ipsPrimariaType.setDescripcion(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getDescripcionIPSPrimaria()));
		
		// sede ips primaria
		SedeIpsPrimariaType sedeIpsPrimariaType = new SedeIpsPrimariaType();
		sedeIpsPrimariaType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getConsecutivoCodigoSede()));

		EstadoDerechoType derechoType = new EstadoDerechoType();
		derechoType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getConsecutivoCausaDerecho()));
		derechoType.setDescripcion(solicitudVO.getAfiliadoVO().getDescripcionEstadoDerecho());

		TipoContratoType tipoContratoType = new TipoContratoType();
		tipoContratoType.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getConsecutivoTipoContrato()));
		
		// Se adiciona la informacion del afiliado al servicio
		AfiliadoType afiliadoType = new AfiliadoType();
		afiliadoType.setTipoIdentificacion(tipoIdentificacionAfiliado);
		afiliadoType.setNumeroIdentificacion(Utilidades.validaNumeroIdentificacion(solicitudVO.getAfiliadoVO().getNumeroIdentificacion()));
		
		afiliadoType.setNumeroUnicoIdentificacion(Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getNumeroUnicoIdentificacion()));		
		afiliadoType.setPrimerNombre(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getPrimerNombre()));
		afiliadoType.setSegundoNombre(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getSegundoNombre()));
		afiliadoType.setPrimerApellido(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getPrimerApellido()));		
		afiliadoType.setSegundoApellido(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getSegundoApellido()));		
		afiliadoType.setFechaNacimiento(Utilidades.asXMLGregorianCalendar(solicitudVO.getAfiliadoVO().getFechaNacimiento()));		
		afiliadoType.setDireccion(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getDireccionResidencia()));		
		afiliadoType.setTelefono(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getTelefono()));		
		afiliadoType.setCorreoElectronico(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getEmail()));
		
		afiliadoType.setCiudad(ciudadAfiliadoType);
		afiliadoType.setDepartamento(departamentoType);
		afiliadoType.setPlan(planAfiliadoType);
		afiliadoType.setSexo(sexoType);
		afiliadoType.setIpsPrimaria(ipsPrimariaType);
		afiliadoType.setSedeIpsPrimaria(sedeIpsPrimariaType);
		afiliadoType.setEstadoDerecho(derechoType);
		afiliadoType.setTipoContrato(tipoContratoType);
		
		afiliadoType.setTutela(Utilidades.validaCadenaTextoCombo(solicitudVO.getAfiliadoVO().getTutela(), ConstantesEJB.NO));
		
		afiliadoType.setSemanasCotizadas(Utilidades.validaNumero(solicitudVO.getAfiliadoVO().getSemanasCotizadas(), BigInteger.ZERO));		
		afiliadoType.setNumeroContrato(Utilidades.validaCadenaTexto(solicitudVO.getAfiliadoVO().getNumeroContrato()));
		afiliadoType.setSedeIpsPrimaria(sedeIpsPrimariaType);
		afiliadoType.setTipoVinculacion(vinculacionType);		
		
		afiliadoType.setRecienNacido(Utilidades.validaCadenaTextoCombo(solicitudVO.getRecienNacidoVO().getRecienNacido(), ConstantesEJB.NO));

		consultaRecienNacido(afiliadoType);	
		
		return afiliadoType;
	}


	private void consultaRecienNacido(AfiliadoType afiliadoType) {
		
		// Se adiciona la informacion del recien nacido al servicio
		if(ConstantesEJB.VALIDACION_COMBOS_OPCIONAL.equals(solicitudVO.getRecienNacidoVO().getRecienNacido()) || ConstantesEJB.VALIDACION_COMBOS_OPCIONAL_COMPLETO.equals(solicitudVO.getRecienNacidoVO().getRecienNacido())){
			// genero recien nacido
			SexoType sexoTypeRecienNacido = new SexoType();
			sexoTypeRecienNacido.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getRecienNacidoVO().getGeneroVO().getConsecutivoGenero()));
			sexoTypeRecienNacido.setCodigo(solicitudVO.getRecienNacidoVO().getGeneroVO().getCodigoGenero());
			
			afiliadoType.setFechaNacimientoRecienNacido(Utilidades.asXMLGregorianCalendar(solicitudVO.getRecienNacidoVO().getFechaNacimientoRecienNacido()));
			afiliadoType.setSexoRecienNacido(sexoTypeRecienNacido);
			afiliadoType.setPartoMultiple(solicitudVO.getRecienNacidoVO().getPartoMultiple());
			afiliadoType.setNumeroHijos(String.valueOf(solicitudVO.getRecienNacidoVO().getNumeroHijos()==null?0:solicitudVO.getRecienNacidoVO().getNumeroHijos()));
		}
	}
	
	/**Se adiciona el tipo de identificacion del afiliado al servicio*/
	public TipoIdentificacionType consultaTipoIdentificacionType(){
		
		// Se adiciona el tipo de identificacion del afiliado al servicio
		TipoIdentificacionType tipoIdentificacionAfiliado = new TipoIdentificacionType();
		tipoIdentificacionAfiliado.setConsecutivo(Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getTipoIdentificacionAfiliado().getConsecutivoTipoIdentificacion()));
		tipoIdentificacionAfiliado.setCodigo(solicitudVO.getAfiliadoVO().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion() != null ? solicitudVO.getAfiliadoVO().getTipoIdentificacionAfiliado().getCodigoTipoIdentificacion().trim() : ConstantesEJB.CADENA_VACIA);
		tipoIdentificacionAfiliado.setDescripcion(ConstantesEJB.CADENA_VACIA);
		
		return tipoIdentificacionAfiliado;		
	}
	
	/** plan Afiliado **/
	public PlanType consultaPlanType(){
		
		PlanType planAfiliadoType = new PlanType();
		planAfiliadoType.setConsecutivo(solicitudVO.getAfiliadoVO().getPlan().getConsectivoPlan() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getPlan().getConsectivoPlan()) : BigInteger.ONE);
		planAfiliadoType.setCodigo(solicitudVO.getAfiliadoVO().getPlan().getCodigoPlan() != null ? solicitudVO.getAfiliadoVO().getPlan().getCodigoPlan().trim() :  ConstantesEJB.CADENA_VACIA);
		planAfiliadoType.setDescripcion(solicitudVO.getAfiliadoVO().getPlan().getDescripcionPlan() != null ? solicitudVO.getAfiliadoVO().getPlan().getDescripcionPlan().trim() :  ConstantesEJB.CADENA_VACIA);

		return planAfiliadoType;
	}
	
	/** tipo de cotizante **/
	public VinculacionType consultaTipoVinculacionType(){
		 VinculacionType vinculacionType = new VinculacionType();
		 
		 vinculacionType.setConsecutivo(solicitudVO.getAfiliadoVO().getConsecutivoParentesco() != null ? Utilidades.convertirNumeroToBigInteger(solicitudVO.getAfiliadoVO().getConsecutivoParentesco()): BigInteger.ONE);
		 vinculacionType.setCodigo(ConstantesEJB.CADENA_VACIA);
		 vinculacionType.setDescripcion(solicitudVO.getAfiliadoVO().getDescripcionParentesco() != null ? solicitudVO.getAfiliadoVO().getDescripcionParentesco().trim(): ConstantesEJB.CADENA_VACIA);
		 
		 return vinculacionType;
	}
	
}
